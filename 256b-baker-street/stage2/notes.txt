=================== Stage 2 ===========================================================

the file stage_2.docx can be unzipped, the flag is in the file flag.pdf

$ strings flag.pdf | grep forensic
forensic{15780932}

stage_3.zip is in the docx file as well

$ sha256sum word/stage_3.zip
45d3e73041fa4182f4547a3a11e44efe851fb8d4b0bb5a981e6af96a2695dbb8  word/stage_3.zip
