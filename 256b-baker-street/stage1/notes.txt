=================== Stage 1 ===========================================================

file in archive hints to https://link.springer.com/chapter/10.1007/3-540-60590-8_12

pkzip archives vulnerable to known plaintext attacks

tool that implements the attack: https://github.com/keyunluo/pkcrack

a_tough_nut_to_crack.zip contains the following files:

-rw-r--r--  3.0 unx   532117 BX defN 19-Oct-25 13:14 3-540-60590-8_12.pdf
-rw-r--r--  3.0 unx    52978 BX defN 19-Oct-30 09:54 flag.png
-rw-r--r--  3.0 unx  1595217 BX defN 19-Oct-31 17:39 stage_2.docx

download the original PDF for the attack:

    $ wget https://link.springer.com/content/pdf/10.1007%2F3-540-60590-8_12.pdf -O 3-540-60590-8_12.pdf

    $ sha256sum 3-540-60590-8_12.pdf
    14a05a7ff430df97c648e1d100aa35346ea6e9648085952110f433b1d75737e7  3-540-60590-8_12.pdf

    $ zip -fz- plaintext.zip 3-540-60590-8_12.pdf


now we crack the file:

    $ pkcrack -C a_tough_nut_to_crack.zip -c 3-540-60590-8_12.pdf -P plaintext.zip -p 3-540-60590-8_12.pdf -d cracked.zip -a
    Files read. Starting stage 1 on Sun Dec 22 13:02:04 2019
    Generating 1st generation of possible key2_496518 values...done.
    Found 4194304 possible key2-values.
    Now we're trying to reduce these...
    (...)
    Done. Left with 96 possible Values. bestOffset is 467930.
    Stage 1 completed. Starting stage 2 on Sun Dec 22 13:02:48 2019
    Ta-daaaaa! key0=9d9dc30b, key1=536f4000, key2=8e50c7b2
    Probabilistic test succeeded for 28593 bytes.
    Stage 2 completed. Starting zipdecrypt on Sun Dec 22 13:02:51 2019
    Decrypting 3-540-60590-8_12.pdf (80937ae89d85338603c0db69)... OK!
    Decrypting flag.png (e238e8ba9d89fbf71386c64e)... OK!
    Decrypting stage_2.docx (d358a0a58206d06edda6e68c)... OK!
    Finished on Sun Dec 22 13:02:51 2019

the file cracked.zip contains the unencrypted files flag.png, stage_2.docx


flag.png contains a QR code, uploaded to https://zxing.org/w/decode.jspx

result:

    Steganography is the practice of concealing a file, message, image, or video within another file, message, image, or video. The word steganography combines the Greek words steganos, meaning "covered or concealed", and graphe meaning "writing". 
    (...)
    Source: https://en.wikipedia.org/wiki/Steganography


$ wget http://www.caesum.com/handbook/Stegsolve.jar -O stegsolve.jar
$ java -jar stegsolve.jar

opened the image flag.png, "red plane 0" contains another QR code with the actual flag

forensic{57932084}
