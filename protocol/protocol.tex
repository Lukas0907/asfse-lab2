
%
% Institut für Rechnergestuetzte Automation
% Forschungsgruppe Industrial Software
% Arbeitsgruppe ESSE
% https://security.inso.tuwien.ac.at/
% lva.security@inso.tuwien.ac.at
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[12pt,a4paper,titlepage,oneside]{scrartcl}
\newcommand{\lang}{en}
\usepackage{esseProtocol}

\usepackage{todonotes}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% FOR STUDENTS
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Team number or "0" for Lab0
\newcommand{\team}{07}
% Date
\newcommand{\datum}{2020-01-09}
% valid values: "Lab0", "Lab1" (be sure to use Uppercase for first character)
\newcommand{\lab}{Lab2}

\newcommand{\lvaname}{Advanced Security for Systems Engineering}
\newcommand{\lvanr}{183.645}
\newcommand{\semester}{WS 2019}

% 1st student of team in Lab1
\newcommand{\studentAName}{Andreas Weichselbaum}
\renewcommand{\studentAMatrnr}{01525900}

% 2nd student of team in Lab1
\newcommand{\studentBName}{Robin Wunderbaldinger}
\renewcommand{\studentBMatrnr}{01526065}

% 3rd student of team in Lab1
\newcommand{\studentCName}{Lukas Anzinger}
\renewcommand{\studentCMatrnr}{01426735}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% DO NOT CHANGE THE FOLLOWING PART
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\colormode}{color}
\newcommand{\dokumenttyp}{Report \lab}

\begin{document}

\maketitle
\setcounter{section}{0}
\setcounter{tocdepth}{2}
\tableofcontents

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% CONTENT OF DOCUMENT STARTS HERE
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newpage

\section{256b Baker Street}

\subsection{Stage 1: A tough nut to crack}

An encrypted ZIP file was given. Fortunately the filenames are not encrypted:

\begin{lstlisting}
$ zipinfo a_tough_nut_to_crack.zip
Archive:  a_tough_nut_to_crack.zip
Zip file size: 2143008 bytes, number of entries: 3
-rw-r--r--  3.0 unx   532117 BX defN 19-Oct-25 13:14 3-540-60590-8_12.pdf
-rw-r--r--  3.0 unx    52978 BX defN 19-Oct-30 09:54 flag.png
-rw-r--r--  3.0 unx  1595217 BX defN 19-Oct-31 17:39 stage_2.docx
3 files, 2180312 bytes uncompressed, 2142438 bytes compressed:  1.7%
\end{lstlisting}

One of the filenames is \texttt{3-540-60590-8\_12.pdf}. A quick Internet search shows
that the file is probably the article \textit{A known plaintext attack on the PKZIP stream
cipher} which can be downloaded from
\url{https://link.springer.com/chapter/10.1007/3-540-60590-8_12}. Fortunately the
article can be freely downloaded from the publisher's website (SHA256:
14a05a7ff430df97c648e1d100aa35346ea6e9648085952110f433b1d75737e7f).

After reading the article it becomes clear that there exists an extremely efficient
known-plaintext attack against PKZIP. A tool called \textit{pkcrack} implements the
attack and can be found here: \url{https://github.com/keyunluo/pkcrack.git}

For the tool to work we have to pack the article and provide it to the tool:

\begin{lstlisting}
$ zip -fz- plaintext.zip 3-540-60590-8_12.pdf
\end{lstlisting}

According to the zip man page, the \texttt{-fz-} option can be used to force zip to
create PKZIP 2 compatible archives.

Now we can use \texttt{pkcrack} to decrypt the archive:

\begin{lstlisting}
$ pkcrack -C a_tough_nut_to_crack.zip -c 3-540-60590-8_12.pdf -P plaintext.zip -p 3-540-60590-8_12.pdf -d cracked.zip -a
Files read. Starting stage 1 on Sun Dec 22 13:02:04 2019
Generating 1st generation of possible key2_496518 values...done.
Found 4194304 possible key2-values.
Now we're trying to reduce these...
(...)
Done. Left with 96 possible Values. bestOffset is 467930.
Stage 1 completed. Starting stage 2 on Sun Dec 22 13:02:48 2019
Ta-daaaaa! key0=9d9dc30b, key1=536f4000, key2=8e50c7b2
Probabilistic test succeeded for 28593 bytes.
Stage 2 completed. Starting zipdecrypt on Sun Dec 22 13:02:51 2019
Decrypting 3-540-60590-8_12.pdf (80937ae89d85338603c0db69)... OK!
Decrypting flag.png (e238e8ba9d89fbf71386c64e)... OK!
Decrypting stage_2.docx (d358a0a58206d06edda6e68c)... OK!
Finished on Sun Dec 22 13:02:51 2019
\end{lstlisting}

\texttt{pkcrack} creates the unencrypted archive \texttt{cracked.zip} which contains
the files we wanted: \texttt{flag.png} and \texttt{stage\_2.docx}.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.5\textwidth]{imgs/stage1_flag.png}
  \caption{The image flag.png, extracted from the encrypted ZIP archive.}.
  \label{fig:stage1-flag}
\end{figure}

Figure~\ref{fig:stage1-flag} shows the extracted image flag.png. The figure depicts a QR
code. Unfortunately, decoding the QR code only revealed the first paragraphs of the
Wikipedia article for \textit{Steganography}\footnote{\url{https://en.wikipedia.org/wiki/Steganography}}.
However, the text let us believe that the real flag is somehow hidden inside the image
as well.

Searching for steganography tools for images, we discovered \textit{stegsolve}.

\begin{lstlisting}
$ wget http://www.caesum.com/handbook/Stegsolve.jar -O stegsolve.jar
$ java -jar stegsolve.jar
\end{lstlisting}

Loading the image in StegSolve, we discovered that on \textit{Red plane 0} another QR
code is hidden in the file (see Figure~\ref{fig:stage1-stegsolve}).

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.5\textwidth]{imgs/stage1_flag_stegsolve.png}
  \caption{The image flag.png loaded into StegSolve.}
  \label{fig:stage1-stegsolve}
\end{figure}

This QR code finally revealed the flag: \texttt{forensic\{57932084\}}.

\subsection{Stage 2}

Next to the flag image, there is also the file \texttt{stage\_2.docx} which hashes to
the given SHA256 sum ef9cff958ada5f52fff13d4eec3ba86d95eecb58c0929c897c0d1d94392166c9.

The file format is Microsoft OOXML, which is basically a ZIP file with a certain
structure and contents. Unpacking the file and running \texttt{strings} on
\texttt{flag.pdf} revealed the flag:

\begin{lstlisting}
$ unzip stage_2.docx
$ strings word/flag.pdf | grep forensic
forensic{15780932}
\end{lstlisting}

\subsection{Stage 3}

Next to the flag PDF, there is also the file \texttt{stage\_3.zip} which hashes to
the given SHA256 sum 45d3e73041fa4182f4547a3a11e44efe851fb8d4b0bb5a981e6af96a2695dbb8.

Unfortunately this file seems to be broken:

\begin{lstlisting}
$ unzip stage_3.zip
Archive:  stage_3.zip
file #1:  bad zipfile offset (local header sig):  0
\end{lstlisting}

Investigating the archive in a hex editor shows that the problem is that the file starts
with the wrong header signature. The signature is \texttt{0x06054b50} which is the
\textit{End of central directory record} but it should be \texttt{0x04034b50} for the
\textit{Local file header}.\footnote{\url{https://en.wikipedia.org/wiki/Zip_(file_format)}}

After changing the signature to \texttt{0x04034b50}, the file \texttt{stage\_3.pdf} can
be successfully extracted. The file contains the man page for binwalk.

Running binwalk on the file reveals an image that contains the flag:

\begin{lstlisting}
$ binwalk --extract stage_3.pdf
$ strings _stage_3.pdf.extracted/6A | grep forensic
forensic{29847365}
\end{lstlisting}

\subsection{Stage 4}

The file that contains the flag is also stage 4:

\begin{lstlisting}
$ sha256sum _stage_3.pdf.extracted/6A
916d6d60d941a2d0ca5dc536627d1f0b08991a9afb3491fb2e1bc0537fd0b9a8  _stage_3.pdf.extracted/6A
\end{lstlisting}

The file can be unpacked with binwalk again. It contains a \texttt{Stage4.class} file.

Executing the file with Java leads to the following output:

\begin{lstlisting}
$ java -cp . Stage4 output.txt
$ hd output.txt
00000000  45 73 6f 74 65 72 69 63  ...  |Esoteric program|
00000010  6d 69 6e 67 20 6c 61 6e  ...  |ming language..A|
00000020  6e 20 65 73 6f 74 65 72  ...  |n esoteric progr|
00000030  61 6d 6d 69 6e 67 20 6c  ...  |amming language |
00000040  28 73 6f 6d 65 74 69 6d  ...  |(sometimes short|
00000050  65 6e 65 64 20 74 6f 20  ...  |ened to esolang)|
(...)
00000430  6e 6f 77 6e 2e 0a 20 20  ...  |nown..   ..  .. |
00000440  0a 09 0a 20 20 20 20 20  ...  |...     .. .....|
00000450  09 0a 20 20 20 20 20 09  ...  |..     ...  . ..|
00000460  0a 20 20 20 20 20 09 09  ...  |.     ..  . ....|
00000470  20 20 20 20 20 09 09 20  ...  |     .. ... ... |
00000480  20 20 20 20 09 09 09 20  ...  |    ...  .....  |
00000490  20 20 20 09 09 20 09 20  ...  |   .. .  ....   |
000004a0  20 20 09 09 20 20 20 09  ...  |  ..   .....    |
000004b0  20 09 09 09 09 20 09 09  ...  | .... .....     |
000004c0  09 09 20 20 20 09 0a 09  ...  |..   ....     ..|
000004d0  20 09 09 09 0a 09 0a 20  ...  | ......     .. .|
000004e0  20 20 0a 09 0a 20 20 20  ...  |  ...     .. . .|
000004f0  0a 09 0a 20 20 20 20 20  ...  |...     ..  ....|
00000500  0a 20 20 20 20 20 09 09  ...  |.     ..    ... |
00000510  20 20 20 20 09 09 09 20  ...  |    ...   ...   |
00000520  20 20 09 09 09 20 20 09  ...  |  ...  ....     |
00000530  09 09 09 09 09 20 09 0a  ...  |..... ....  .|
\end{lstlisting}

After the excerpt of the Wikipedia article for esoteric programming languages, there is
a Whitespace program\footnote{\url{https://en.wikipedia.org/wiki/Whitespace_(programming_language)}}
embedded. It can be excuted by running it in an interpeter, e.\,g. at
\url{https://vii5ard.github.io/whitespace/}.

This reveals the flag \texttt{forensic\{17453089\}}.

\newpage

\section{Stream Cipher - LFSR}

\subsection{Linear Feedback Shift Register}

A Linear Feedback Shift Register can be implemented quite easily, for example in Python.
The function \texttt{lfsr()} takes a seed and a tap. The tap masks the bits which should
be considered for XORing. The output of a round is always the least significant bit of
the current state (the state in the beginning is the seed). ANDing \texttt{state} and
\texttt{tap} and XORing the bits with each other gives a new bit \texttt{res} which is
shifted into the state from the left.

\begin{lstlisting}{style=python}
import functools
import operator

def lfsr(seed, tap):
    # reverse the tap sequence
    tap = int(bin(tap)[2:][::-1], 2)

    while True:
        output = []
        for i in range(8):
            output.insert(0, seed & 0x1)
            res = 0
            for b in bin(seed & tap)[2:]:
                res ^= int(b)
            seed = (seed >> 1) | (res << 15)
        yield int("".join(str(b) for b in output), 2)


for i, out in enumerate(lfsr(seed=0xf60f, tap=0x82e1), 1):
    print("{} = {}".format(i, out))
    if i == 1000:
        break
\end{lstlisting}

To get the "flag" for this challenge, we have to calculate the 1000th output, which is
\textit{129}.

\subsection{Linear Feedback Shift Register - Break}

"Breaking" the LFSR is quite easy given a few output values. The first two bytes are the
seed (e.\,g. the state for 200 and 81 would be (200 | (81 << 8)) = 0x51c8) and the tap
can be brute forced quite efficiently.

We found the right tap value if we can reproduce the same values as the given output
values.

\begin{lstlisting}
import functools
import operator


def lfsr_rev(output):
    orig_seed = output[0] | (output[1] << 8)

    for tap in range(2**16):
        flag = True

        seed = orig_seed
        for j in range(len(output)):
            calc_output = []
            for i in range(8):
                calc_output.insert(0, seed & 0x1)
                res = 0
                for b in bin(seed & tap)[2:]:
                    res ^= int(b)
                seed = (seed >> 1) | (res << 15)

            if int("".join(str(b) for b in calc_output), 2) != output[j]:
                # Not the right tap.
                flag = False

            if not flag:
                break

        if flag:
            # reverse the tap
            tap = int(bin(tap)[2:][::-1], 2)
            return (orig_seed, tap)

    return (orig_seed, 0)
\end{lstlisting}

\subsection{Streamcipher}

Using a LFSR as a streamcipher is quite easy: We just have to XOR the output we get from
the LFSR with the given plaintext. The result is the ciphertext.

\begin{lstlisting}
plaintext = "EsseFlag{oeSVOE9s2Y79Jfle7EtCT5DsFnGFrLbv}"
for i, out in enumerate(lfsr(seed=0xdd4d, tap=0x864a), 0):
    print("%02X" % (ord(plaintext[i]) ^ out), end="")
    if i == len(plaintext) - 1:
        break
\end{lstlisting}

The result is

\begin{lstlisting}
08AE8970AB9641BB2E083B0DD083D86998D1022591
DC3D9571A9FDED478D577EAD5CFDFB5C013957AC6E
\end{lstlisting}

\subsection{Streamcipher - Break}

To launch a known plaintext attack, we first have to recover the seed and the tap.
Recovering the seed is easy: We just have to XOR the ciphertext and the plaintext which
reveals the output of the LFSR.  With the output we can then reverse the encryption for
the whole ciphertext by XORing the output and the ciphertext.

\begin{lstlisting}
plaintext = "Log Entry"
ciphertext = binascii.unhexlify("""
C9170F234D143D425F19830323442BDAA9CD314847A39CED104FE3F
14B37AE1AFC078264B3D77D2AF0B780995BFE905821AF351106FFA8
808590051E3750145015E5558E8E15472B5D29566949593A2A77876
B237E1B8B01CEADAFB237EB6A336E89E0DE0C2FBCE0F7FAE80BC7D7
13F092936AEB2E52DDCFFA
""".replace("\n", ""))

# get seed and tap first
output = []
for i in range(len(plaintext)):
    output.append(ciphertext[i] ^ ord(plaintext[i]))

seed, tap = lfsr_rev(output=output)
assert seed != 0
assert tap != 0

plaintext = ""
# now we can decrypt the whole ciphertext
for i, out in enumerate(lfsr(seed=seed, tap=tap), 0):
    plaintext += chr(ciphertext[i] ^ out)
    if i == len(ciphertext) - 1:
        break

print(plaintext[:min(120, len(plaintext))])
\end{lstlisting}

The program prints the solution \texttt{Log Entry \#104 We have seen antibiogram.
However, we remain vigilant. The sugar bowl has been cancerous. Wait for galois}.

\newpage

\section{The Droid is Coming}

\subsection{Errors in the Realm of Zero and One}

For this task an Android application \texttt{BetterCrypt} was given in the form of an APK file. BetterCrypt lets the user encrypt and decrypt files by providing a PIN that should be between 1 and 16 digits long. Files that were decrypted should be stored in the devices Download folder. In the APK there already were two encrypted files: \texttt{bankpassword.crypt} and \texttt{epa.mp4} as shown in Figure~\ref{fig:better-crypt-decrypt}.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.5\textwidth]{imgs/betterCrypt.jpg}
  \caption{The decryption tab view of \texttt{betterCrypt}.}
  \label{fig:better-crypt-decrypt}
\end{figure}

In this task the goal was to figure out the plaintext of the the file \texttt{bankpassword.crypt}
The first thing to do, was to reverse engineer the APK file and browse through the source code of betterCrypt. Since there is quite a number of reverse engineering tools for Android out there \footnote{\url{https://ibotpeaches.github.io/Apktool/}} \footnote{\url{https://github.com/skylot/jadx}} \footnote{\url{https://vaibhavpandey.com/apkstudio/}} we had to decide on what tools we were going to use. In the end we stuck with APK analysis capabilities of Android Studio\footnote{\url{https://developer.android.com/studio}}. Since Android Studio only shows the code of the APK in Smali code, we attached Java code, which we decompiled using dex2jar\footnote{\url{https://github.com/pxb1988/dex2jar}} (for converting the APK file into a JAR file) and procyon\footnote{\url{https://bitbucket.org/mstrobel/procyon/wiki/Java\%20Decompiler}} for converting the Java .class files into .java files, to the .smali files.

Clicking through the app and with the hint in mind that the security issue was due to incorrect input validation, it was fairly easy to find a point in the app that behaved strangly and required further investigation. Interestingly enough, the error message that was presented to the user was different when the input PIN for decryption of \texttt{bankpassword.crypt} was an empty string, or any combination of digits, as seen in Figures \ref{fig:better-crypt-decrypt-empty-string} and \ref{fig:better-crypt-decrypt-non-empty-string}.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.3\textwidth]{imgs/betterCrypt-decrypting-empty.jpg}
  \caption{The error message shown, when trying to decrypt \texttt{bankpassword.crypt} with an empty PIN.}
  \label{fig:better-crypt-decrypt-empty-string}
\end{figure}

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.3\textwidth]{imgs/betterCrypt-decrypting-non-empty.jpg}
  \caption{The error message shown, when trying to decrypt \texttt{bankpassword.crypt} with an non-empty PIN.}
  \label{fig:better-crypt-decrypt-non-empty-string}
\end{figure}

It seemed the input validation for the decryption PIN for \texttt{bankpassword.crypt} was faulty, since it actually lets users try to decrypt the file with a PIN of length 0, even though the PIN should be between 1 and 16 digits long. A quick look into the decompiled Java code provides answers.

\begin{lstlisting}[caption={A snippet from EncryptDialog.java. The if-condition can never be true}, label={lst:encrypt-validation}]
    private final String validateString(final String s) {
        if (s.length() < 0) {
            return "Input too short";
        }
        return null;
    }
\end{lstlisting}

Because of the faulty input validation for encrypting files seen in listing \ref{lst:encrypt-validation}, it is possible to encrypt files using an empty string. This may be, what the user that stored the \texttt{bankpassword.crypt} file did.

Altough the input validation for decryption PINs is implemented correctly in the decryption dialog, i.e.\ the validation fails, if the user provides an empty string, as seen in listing \ref{lst:decrypt-validation}, this validation is only executed, if the input PIN changes. That means that it is possible to provide a PIN with length 0 for decryption by just clicking ``decrypt`` without previously typing any PIN.
\begin{lstlisting}[caption={A snippet from DecryptDialog.java. The validation correctly checks, if the input string is not empty}, label={lst:decrypt-validation}]

    private final String validateString(final String s) {
        if (s.length() < 1) {
            return "Input too short";
        }
        return null;
    }
\end{lstlisting}

Looking for the two different error messages that were shown to the user, when trying to decrypt \texttt{bankpassword.crypt} with an empty PIN and with a non-empty PIN, we found the method for decrypting files, as seen in listing \ref{lst:decrypt-method}.

\begin{lstlisting}[caption={A snippet from DecryptDialog.java. The method shows the two error messages that be previously saw}, label={lst:decrypt-method}]

    private final void decryptFile(final String s, final String child, final String s2, final boolean b) {
        if (b) {
            Toast.makeText(this.getContext(), (CharSequence)"Check your input", 0).show();
            return;
        }
        final byte[] decryptWithAES = this.encryption.decryptWithAES(s, s2);
        if (decryptWithAES == null) {
            Toast.makeText(this.getContext(), (CharSequence)"Failed to decrypt file", 1).show();
            return;
        }
        final java.io.File file = new java.io.File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), child);
        if (!file.canWrite()) {
            Toast.makeText(this.getContext(), (CharSequence)"Missing the write permission.", 1).show();
            return;
        }
        FilesKt__FileReadWriteKt.writeBytes(file, decryptWithAES);
        Toast.makeText(this.getContext(), (CharSequence)"File saved to your 'Downloads' folder.", 1).show();
        this.dismiss();
    }
\end{lstlisting}

The method \textit{decryptFile(...)} shows different error messages that are presented to the user in different scenarios, depending on wether the PIN was correct or incorrent, or the file could be stored or not. Interestingly, providing an empty PIN for decryption ended up showing an error message that assumes that the file has already been decrypted successfully.

It turns out that just providing an empty PIN for decryption already was the solution, but due to some permission errors seen in figure \ref{fig:better-crypt-decrypt-empty-string} the decrypted file could not be stored within the Download folder of the device, even though the Android permission \textit{WRITE\_EXTERNAL\_STORAGE} was granted. Therefore we emulated an Android devices via the functionality of Android Studio and looked at the decrypted file using a debugger, in which the password \textbf{wrongdolphinpilebattery} was stored.

\subsection{To be, or not to be ... Human}
In this submission the task was to decrypt the other previously encrypted file \texttt{epa.mp4}. This task was solved quite quickly, because all of the hints that were needed for solving this task, were provided by the task description itself. 

After searching for the given quote (``... The whole world must learn of our peaceful ways .... by force ...``) on Google, we realised that it comes from robot character Bender of the comic series Futurama. The task description also mentioned that the required PIN might be ``his`` serial number. Another Google search revealed that the serial number of the robot Bender was 2716057, which coincidently was just a sequence of digits, perfectly fitting for a betterCrypt PIN.

Trying Bender's serial number as PIN for decrypting the file \texttt{epa.mp4} lead to the error message \textbf{``Missing the write permission.``}, which means that, as explained in the previous subsection, the PIN was correct, but the file could not be stored in the Download folder. Again, emulating an Android device, debugging betterCrypt and looking at the content of the decrypted file in the debugger, lead to a YouTube Link\footnote{\url{youtu.be/NMRvocMej_s}}, which showed a sequence of an episode of The Simpsons.

\end{document}
