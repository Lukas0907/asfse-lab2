import functools
import operator


def lfsr_rev(output):
    # first two bytes of output sequence in reversed order == seed
    # >>> hex(200 | (81 << 8))
    # '0x51c8'
    orig_seed = output[0] | (output[1] << 8)

    for tap in range(2**16):
        flag = True

        seed = orig_seed
        for j in range(len(output)):
            calc_output = []
            for i in range(8):
                calc_output.insert(0, seed & 0x1)
                res = 0
                for b in bin(seed & tap)[2:]:
                    res ^= int(b)
                seed = (seed >> 1) | (res << 15)

            if int("".join(str(b) for b in calc_output), 2) != output[j]:
                # Not the right tap.
                flag = False

            if not flag:
                break

        if flag:
            # reverse the tap
            tap = int(bin(tap)[2:][::-1], 2)
            return (orig_seed, tap)

    return (orig_seed, 0)


print("seed: {:X}, tap: {:X}".format(*lfsr_rev(output=[200, 81, 135, 30, 188, 170, 110, 142])))
