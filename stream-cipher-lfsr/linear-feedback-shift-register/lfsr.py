import functools
import operator


def lfsr(seed, tap):
    # reverse the tap sequence
    tap = int(bin(tap)[2:][::-1], 2)

    while True:
        output = []
        for i in range(8):
            output.insert(0, seed & 0x1)
            res = 0
            for b in bin(seed & tap)[2:]:
                res ^= int(b)
            seed = (seed >> 1) | (res << 15)
        yield int("".join(str(b) for b in output), 2)


for i, out in enumerate(lfsr(seed=0xf60f, tap=0x82e1), 1):
    print("{} = {}".format(i, out))
    if i == 1000:
        break
