import functools
import operator


def lfsr(seed, tap):
    # reverse the tap sequence
    tap = int(bin(tap)[2:][::-1], 2)

    while True:
        output = []
        for i in range(8):
            output.insert(0, seed & 0x1)
            res = 0
            for b in bin(seed & tap)[2:]:
                res ^= int(b)
            seed = (seed >> 1) | (res << 15)
        yield int("".join(str(b) for b in output), 2)


plaintext = "EsseFlag{oeSVOE9s2Y79Jfle7EtCT5DsFnGFrLbv}"
for i, out in enumerate(lfsr(seed=0xdd4d, tap=0x864a), 0):
    print("%02X" % (ord(plaintext[i]) ^ out), end="")
    if i == len(plaintext) - 1:
        break
