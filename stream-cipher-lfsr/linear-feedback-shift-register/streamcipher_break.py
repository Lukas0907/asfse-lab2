import functools
import operator
import binascii


def lfsr(seed, tap):
    # reverse the tap sequence
    tap = int(bin(tap)[2:][::-1], 2)

    while True:
        output = []
        for i in range(8):
            output.insert(0, seed & 0x1)
            res = 0
            for b in bin(seed & tap)[2:]:
                res ^= int(b)
            seed = (seed >> 1) | (res << 15)
        yield int("".join(str(b) for b in output), 2)


def lfsr_rev(output):
    # first two bytes of output sequence in reversed order == seed
    # >>> hex(200 | (81 << 8))
    # '0x51c8'
    orig_seed = output[0] | (output[1] << 8)

    for tap in range(2**16):
        flag = True

        seed = orig_seed
        for j in range(len(output)):
            calc_output = []
            for i in range(8):
                calc_output.insert(0, seed & 0x1)
                res = 0
                for b in bin(seed & tap)[2:]:
                    res ^= int(b)
                seed = (seed >> 1) | (res << 15)

            if int("".join(str(b) for b in calc_output), 2) != output[j]:
                # Not the right tap.
                flag = False

            if not flag:
                break

        if flag:
            # reverse the tap
            tap = int(bin(tap)[2:][::-1], 2)
            return (orig_seed, tap)

    return (orig_seed, 0)


plaintext = "Log Entry"
ciphertext = binascii.unhexlify("""
C9170F234D143D425F19830323442BDAA9CD314847A39CED104FE3F14B37AE1A
FC078264B3D77D2AF0B780995BFE905821AF351106FFA8808590051E37501450
15E5558E8E15472B5D29566949593A2A77876B237E1B8B01CEADAFB237EB6A33
6E89E0DE0C2FBCE0F7FAE80BC7D713F092936AEB2E52DDCFFA
""".replace("\n", ""))

# get seed and tap first
output = []
for i in range(len(plaintext)):
    output.append(ciphertext[i] ^ ord(plaintext[i]))

seed, tap = lfsr_rev(output=output)
assert seed != 0
assert tap != 0

plaintext = ""
# now we can decrypt the whole ciphertext
for i, out in enumerate(lfsr(seed=seed, tap=tap), 0):
    plaintext += chr(ciphertext[i] ^ out)
    if i == len(ciphertext) - 1:
        break

print(plaintext[:min(120, len(plaintext))])
