// 
// Decompiled by Procyon v0.5.36
// 

package at.esse.betterCrypt.dao;

import java.util.ArrayList;
import java.util.List;
import android.database.Cursor;
import androidx.room.util.CursorUtil;
import android.os.CancellationSignal;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.room.util.DBUtil;
import androidx.room.RoomSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteStatement;
import at.esse.betterCrypt.model.File;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;

public final class FileDao_Impl implements FileDao
{
    private final RoomDatabase __db;
    private final EntityInsertionAdapter<File> __insertionAdapterOfFile;
    
    public FileDao_Impl(final RoomDatabase _db) {
        this.__db = _db;
        this.__insertionAdapterOfFile = new EntityInsertionAdapter<File>(_db) {
            public void bind(final SupportSQLiteStatement supportSQLiteStatement, final File file) {
                supportSQLiteStatement.bindLong(1, file.getId());
                if (file.getName() == null) {
                    supportSQLiteStatement.bindNull(2);
                }
                else {
                    supportSQLiteStatement.bindString(2, file.getName());
                }
                supportSQLiteStatement.bindLong(3, file.getCreation());
                if (file.getData() == null) {
                    supportSQLiteStatement.bindNull(4);
                    return;
                }
                supportSQLiteStatement.bindBlob(4, file.getData());
            }
            
            public String createQuery() {
                return "INSERT OR ABORT INTO `File` (`id`,`name`,`creation`,`data`) VALUES (nullif(?, 0),?,?,?)";
            }
        };
    }
    
    @Override
    public File findById(final long n) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("select * from file where id = ?", 1);
        acquire.bindLong(1, n);
        this.__db.assertNotSuspendingTransaction();
        final RoomDatabase _db = this.__db;
        File file = null;
        final Cursor query = DBUtil.query(_db, acquire, false, null);
        try {
            final int columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "id");
            final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "name");
            final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "creation");
            final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "data");
            if (query.moveToFirst()) {
                file = new File(query.getString(columnIndexOrThrow2), query.getLong(columnIndexOrThrow3), query.getBlob(columnIndexOrThrow4));
                file.setId(query.getLong(columnIndexOrThrow));
            }
            return file;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public List<File> getAll() {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire("select * from file", 0);
        this.__db.assertNotSuspendingTransaction();
        final Cursor query = DBUtil.query(this.__db, acquire, false, null);
        try {
            final int columnIndexOrThrow = CursorUtil.getColumnIndexOrThrow(query, "id");
            final int columnIndexOrThrow2 = CursorUtil.getColumnIndexOrThrow(query, "name");
            final int columnIndexOrThrow3 = CursorUtil.getColumnIndexOrThrow(query, "creation");
            final int columnIndexOrThrow4 = CursorUtil.getColumnIndexOrThrow(query, "data");
            final ArrayList list = new ArrayList<File>(query.getCount());
            while (query.moveToNext()) {
                final File file = new File(query.getString(columnIndexOrThrow2), query.getLong(columnIndexOrThrow3), query.getBlob(columnIndexOrThrow4));
                file.setId(query.getLong(columnIndexOrThrow));
                list.add(file);
            }
            return (List<File>)list;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    @Override
    public void insert(final File file) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfFile.insert(file);
            this.__db.setTransactionSuccessful();
        }
        finally {
            this.__db.endTransaction();
        }
    }
}
