// 
// Decompiled by Procyon v0.5.36
// 

package at.esse.betterCrypt.db;

import at.esse.betterCrypt.dao.FileDao_Impl;
import java.util.HashSet;
import androidx.room.util.TableInfo;
import androidx.room.util.DBUtil;
import androidx.room.RoomOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.room.DatabaseConfiguration;
import java.util.Map;
import androidx.room.RoomDatabase;
import java.util.Set;
import java.util.HashMap;
import androidx.room.InvalidationTracker;
import androidx.sqlite.db.SupportSQLiteDatabase;
import java.util.List;
import at.esse.betterCrypt.dao.FileDao;

public final class AppDatabase_Impl extends AppDatabase
{
    private volatile FileDao _fileDao;
    
    @Override
    public void clearAllTables() {
        super.assertNotMainThread();
        final SupportSQLiteDatabase writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `File`");
            super.setTransactionSuccessful();
        }
        finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }
    
    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return new InvalidationTracker(this, new HashMap<String, String>(0), new HashMap<String, Set<String>>(0), new String[] { "File" });
    }
    
    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(final DatabaseConfiguration databaseConfiguration) {
        return databaseConfiguration.sqliteOpenHelperFactory.create(SupportSQLiteOpenHelper.Configuration.builder(databaseConfiguration.context).name(databaseConfiguration.name).callback(new RoomOpenHelper(databaseConfiguration, (RoomOpenHelper.Delegate)new RoomOpenHelper.Delegate(1) {
            public void createAllTables(final SupportSQLiteDatabase supportSQLiteDatabase) {
                supportSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS `File` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `name` TEXT NOT NULL, `creation` INTEGER NOT NULL, `data` BLOB NOT NULL)");
                supportSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
                supportSQLiteDatabase.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '3f402ef953ad555a48d80e1409c6b4e8')");
            }
            
            public void dropAllTables(final SupportSQLiteDatabase supportSQLiteDatabase) {
                supportSQLiteDatabase.execSQL("DROP TABLE IF EXISTS `File`");
                if (AppDatabase_Impl.this.mCallbacks != null) {
                    for (int i = 0; i < AppDatabase_Impl.this.mCallbacks.size(); ++i) {
                        ((RoomDatabase.Callback)AppDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(supportSQLiteDatabase);
                    }
                }
            }
            
            @Override
            protected void onCreate(final SupportSQLiteDatabase supportSQLiteDatabase) {
                if (AppDatabase_Impl.this.mCallbacks != null) {
                    for (int i = 0; i < AppDatabase_Impl.this.mCallbacks.size(); ++i) {
                        ((RoomDatabase.Callback)AppDatabase_Impl.this.mCallbacks.get(i)).onCreate(supportSQLiteDatabase);
                    }
                }
            }
            
            public void onOpen(final SupportSQLiteDatabase supportSQLiteDatabase) {
                AppDatabase_Impl.this.mDatabase = supportSQLiteDatabase;
                RoomDatabase.this.internalInitInvalidationTracker(supportSQLiteDatabase);
                if (AppDatabase_Impl.this.mCallbacks != null) {
                    for (int i = 0; i < AppDatabase_Impl.this.mCallbacks.size(); ++i) {
                        ((RoomDatabase.Callback)AppDatabase_Impl.this.mCallbacks.get(i)).onOpen(supportSQLiteDatabase);
                    }
                }
            }
            
            public void onPostMigrate(final SupportSQLiteDatabase supportSQLiteDatabase) {
            }
            
            public void onPreMigrate(final SupportSQLiteDatabase supportSQLiteDatabase) {
                DBUtil.dropFtsSyncTriggers(supportSQLiteDatabase);
            }
            
            @Override
            protected ValidationResult onValidateSchema(final SupportSQLiteDatabase supportSQLiteDatabase) {
                final HashMap<String, TableInfo.Column> hashMap = new HashMap<String, TableInfo.Column>(4);
                hashMap.put("id", new TableInfo.Column("id", "INTEGER", true, 1, null, 1));
                hashMap.put("name", new TableInfo.Column("name", "TEXT", true, 0, null, 1));
                hashMap.put("creation", new TableInfo.Column("creation", "INTEGER", true, 0, null, 1));
                hashMap.put("data", new TableInfo.Column("data", "BLOB", true, 0, null, 1));
                final TableInfo obj = new TableInfo("File", hashMap, new HashSet<TableInfo.ForeignKey>(0), new HashSet<TableInfo.Index>(0));
                final TableInfo read = TableInfo.read(supportSQLiteDatabase, "File");
                if (!obj.equals(read)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("File(at.esse.betterCrypt.model.File).\n Expected:\n");
                    sb.append(obj);
                    sb.append("\n Found:\n");
                    sb.append(read);
                    return new RoomOpenHelper.ValidationResult(false, sb.toString());
                }
                return new RoomOpenHelper.ValidationResult(true, null);
            }
        }, "3f402ef953ad555a48d80e1409c6b4e8", "fbbddcdf5dd430921c13e3ce046395d4")).build());
    }
    
    @Override
    public FileDao fileDAO() {
        if (this._fileDao != null) {
            return this._fileDao;
        }
        synchronized (this) {
            if (this._fileDao == null) {
                this._fileDao = new FileDao_Impl(this);
            }
            return this._fileDao;
        }
    }
}
