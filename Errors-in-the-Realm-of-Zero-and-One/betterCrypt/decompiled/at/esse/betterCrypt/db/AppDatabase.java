// 
// Decompiled by Procyon v0.5.36
// 

package at.esse.betterCrypt.db;

import androidx.room.Room;
import kotlin.jvm.internal.Intrinsics;
import android.app.Activity;
import at.esse.betterCrypt.dao.FileDao;
import kotlin.Metadata;
import androidx.room.RoomDatabase;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b'\u0018\u0000 \u00052\u00020\u0001:\u0001\u0005B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H&¨\u0006\u0006" }, d2 = { "Lat/esse/betterCrypt/db/AppDatabase;", "Landroidx/room/RoomDatabase;", "()V", "fileDAO", "Lat/esse/betterCrypt/dao/FileDao;", "Companion", "app_release" }, k = 1, mv = { 1, 1, 15 })
public abstract class AppDatabase extends RoomDatabase
{
    public static final Companion Companion;
    private static AppDatabase instance;
    
    static {
        Companion = new Companion(null);
    }
    
    public static final /* synthetic */ AppDatabase access$getInstance$cp() {
        return AppDatabase.instance;
    }
    
    public static final /* synthetic */ void access$setInstance$cp(final AppDatabase instance) {
        AppDatabase.instance = instance;
    }
    
    public abstract FileDao fileDAO();
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.¢\u0006\u0002\n\u0000¨\u0006\b" }, d2 = { "Lat/esse/betterCrypt/db/AppDatabase$Companion;", "", "()V", "instance", "Lat/esse/betterCrypt/db/AppDatabase;", "newInstance", "activity", "Landroid/app/Activity;", "app_release" }, k = 1, mv = { 1, 1, 15 })
    public static final class Companion
    {
        private Companion() {
        }
        
        public static final /* synthetic */ AppDatabase access$getInstance$li(final Companion companion) {
            return AppDatabase.access$getInstance$cp();
        }
        
        public final AppDatabase newInstance(final Activity activity) {
            Intrinsics.checkParameterIsNotNull(activity, "activity");
            if (access$getInstance$li(AppDatabase.Companion) == null) {
                final AppDatabase build = Room.databaseBuilder(activity.getApplicationContext(), AppDatabase.class, "file-database").allowMainThreadQueries().build();
                Intrinsics.checkExpressionValueIsNotNull(build, "Room.databaseBuilder(\n  \u2026inThreadQueries().build()");
                AppDatabase.access$setInstance$cp(build);
            }
            final AppDatabase access$getInstance$cp = AppDatabase.access$getInstance$cp();
            if (access$getInstance$cp == null) {
                Intrinsics.throwUninitializedPropertyAccessException("instance");
            }
            return access$getInstance$cp;
        }
    }
}
