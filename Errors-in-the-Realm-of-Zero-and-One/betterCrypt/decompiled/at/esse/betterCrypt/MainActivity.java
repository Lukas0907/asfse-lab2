// 
// Decompiled by Procyon v0.5.36
// 

package at.esse.betterCrypt;

import android.widget.Toast;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.PagerAdapter;
import at.esse.betterCrypt.views.ListFragmentImpl;
import androidx.fragment.app.Fragment;
import at.esse.betterCrypt.views.StartFragment;
import at.esse.betterCrypt.tab.TabAdapter;
import at.esse.bettercrypt.R;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import kotlin.jvm.internal.Intrinsics;
import android.view.View;
import java.io.InputStream;
import at.esse.betterCrypt.dao.FileDao;
import android.content.SharedPreferences;
import at.esse.betterCrypt.model.File;
import at.esse.betterCrypt.db.AppDatabase;
import android.app.Activity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.content.Context;
import java.util.HashMap;
import kotlin.Metadata;
import androidx.appcompat.app.AppCompatActivity;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0002J\b\u0010\u0005\u001a\u00020\u0004H\u0002J\b\u0010\u0006\u001a\u00020\u0004H\u0016J\u0012\u0010\u0007\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\tH\u0014J+\u0010\n\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\f2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016¢\u0006\u0002\u0010\u0012¨\u0006\u0013" }, d2 = { "Lat/esse/betterCrypt/MainActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "checkForPermission", "", "firstRunCheck", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onRequestPermissionsResult", "requestCode", "", "permissions", "", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "app_release" }, k = 1, mv = { 1, 1, 15 })
public final class MainActivity extends AppCompatActivity
{
    private HashMap _$_findViewCache;
    
    private final void checkForPermission() {
        if (ContextCompat.checkSelfPermission((Context)this, "android.permission.WRITE_EXTERNAL_STORAGE") != 0) {
            final MainActivity mainActivity = this;
            if (!ActivityCompat.shouldShowRequestPermissionRationale(mainActivity, "android.permission.WRITE_EXTERNAL_STORAGE")) {
                ActivityCompat.requestPermissions(mainActivity, new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" }, 111);
            }
        }
    }
    
    private final void firstRunCheck() {
        final SharedPreferences sharedPreferences = this.getSharedPreferences("at.esse.bettercrypt", 0);
        if (sharedPreferences.getBoolean("first_run", true)) {
            final FileDao fileDAO = AppDatabase.Companion.newInstance(this).fileDAO();
            final InputStream open = this.getAssets().open("bankpassword.crypt");
            final byte[] b = new byte[open.available()];
            open.read(b);
            fileDAO.insert(new File("bankpassword.crypt", 1561128642000L, b));
            final InputStream open2 = this.getAssets().open("epa.mp4");
            final byte[] b2 = new byte[open2.available()];
            open2.read(b2);
            fileDAO.insert(new File("epa.mp4", 1561121642000L, b2));
            sharedPreferences.edit().putBoolean("first_run", false).apply();
        }
    }
    
    public void _$_clearFindViewByIdCache() {
        final HashMap $_findViewCache = this._$_findViewCache;
        if ($_findViewCache != null) {
            $_findViewCache.clear();
        }
    }
    
    public View _$_findCachedViewById(final int n) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View viewById;
        if ((viewById = this._$_findViewCache.get(n)) == null) {
            viewById = this.findViewById(n);
            this._$_findViewCache.put(n, viewById);
        }
        return viewById;
    }
    
    @Override
    public void onBackPressed() {
        final FragmentManager supportFragmentManager = this.getSupportFragmentManager();
        Intrinsics.checkExpressionValueIsNotNull(supportFragmentManager, "supportFragmentManager");
        if (supportFragmentManager.getBackStackEntryCount() > 1) {
            this.getSupportFragmentManager().popBackStack();
        }
    }
    
    @Override
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131427356);
        final ViewPager viewPager = (ViewPager)this._$_findCachedViewById(R.id.viewPager);
        final FragmentManager supportFragmentManager = this.getSupportFragmentManager();
        Intrinsics.checkExpressionValueIsNotNull(supportFragmentManager, "supportFragmentManager");
        final TabAdapter tabAdapter = new TabAdapter(supportFragmentManager);
        tabAdapter.addFragment(new StartFragment(), "Start");
        tabAdapter.addFragment(new ListFragmentImpl(), "Files");
        Intrinsics.checkExpressionValueIsNotNull(viewPager, "pager");
        viewPager.setAdapter(tabAdapter);
        viewPager.addOnPageChangeListener((ViewPager.OnPageChangeListener)new MainActivity$onCreate.MainActivity$onCreate$1(tabAdapter));
        ((TabLayout)this._$_findCachedViewById(R.id.tabLayout)).setupWithViewPager(viewPager);
        this.firstRunCheck();
        this.checkForPermission();
    }
    
    @Override
    public void onRequestPermissionsResult(int n, final String[] array, final int[] array2) {
        Intrinsics.checkParameterIsNotNull(array, "permissions");
        Intrinsics.checkParameterIsNotNull(array2, "grantResults");
        if (n != 111) {
            return;
        }
        if (array2.length == 0) {
            n = 1;
        }
        else {
            n = 0;
        }
        if ((n ^ 0x1) == 0x0 || array2[0] != 0) {
            Toast.makeText((Context)this, (CharSequence)"The permission can still be enabled manually", 1).show();
        }
    }
}
