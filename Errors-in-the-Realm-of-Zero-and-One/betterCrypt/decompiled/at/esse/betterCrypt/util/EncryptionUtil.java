// 
// Decompiled by Procyon v0.5.36
// 

package at.esse.betterCrypt.util;

import kotlin.text.Charsets;
import kotlin.TypeCastException;
import java.security.Key;
import javax.crypto.Cipher;
import org.bouncycastle.util.encoders.Base64;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.nio.charset.Charset;
import java.security.Provider;
import java.security.Security;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u0000 \u000b2\u00020\u0001:\u0001\u000bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006J\u0018\u0010\b\u001a\u0004\u0018\u00010\u00062\u0006\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u0006¨\u0006\f" }, d2 = { "Lat/esse/betterCrypt/util/EncryptionUtil;", "", "()V", "decryptWithAES", "", "key", "", "strToDecrypt", "encrypt", "strToEncrypt", "secret_key", "Companion", "app_release" }, k = 1, mv = { 1, 1, 15 })
public final class EncryptionUtil
{
    public static final Companion Companion;
    private static EncryptionUtil instance;
    
    static {
        Companion = new Companion(null);
    }
    
    private EncryptionUtil() {
    }
    
    public static final /* synthetic */ EncryptionUtil access$getInstance$cp() {
        return EncryptionUtil.instance;
    }
    
    public static final /* synthetic */ void access$setInstance$cp(final EncryptionUtil instance) {
        EncryptionUtil.instance = instance;
    }
    
    public final byte[] decryptWithAES(String string, final String s) {
    Label_0182_Outer:
        while (true) {
            Intrinsics.checkParameterIsNotNull(string, "key");
            Security.addProvider(new BouncyCastleProvider());
            while (true) {
                Charset forName;
                byte[] bytes;
                byte[] digest;
                SecretKeySpec secretKeySpec;
                String s2;
                int n = 0;
                int n3 = 0;
                int n2 = 0;
                int n4;
                Charset forName2;
                byte[] bytes2;
                byte[] decode;
                Cipher instance;
                byte[] array;
                boolean b = false;
                Label_0320:Label_0103_Outer:
                while (true) {
                Label_0122:
                    while (true) {
                        Label_0280: {
                            while (true) {
                                try {
                                    forName = Charset.forName("UTF8");
                                    Intrinsics.checkExpressionValueIsNotNull(forName, "Charset.forName(charsetName)");
                                    bytes = string.getBytes(forName);
                                    Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
                                    digest = MessageDigest.getInstance("SHA-256").digest(bytes);
                                    Intrinsics.checkExpressionValueIsNotNull(digest, "MessageDigest.getInstanc\u2026HA-256\").digest(keyBytes)");
                                    secretKeySpec = new SecretKeySpec(digest, "AES");
                                    if (s != null) {
                                        s2 = s;
                                        n = s2.length() - 1;
                                        n2 = (n3 = 0);
                                        break Label_0253;
                                    }
                                    break Label_0320;
                                    // iftrue(Label_0277:, s2.charAt(n4) > ' ')
                                    while (true) {
                                        Block_9: {
                                            break Block_9;
                                            bytes2 = string.getBytes(forName2);
                                            Intrinsics.checkExpressionValueIsNotNull(bytes2, "(this as java.lang.String).getBytes(charset)");
                                            decode = Base64.decode(bytes2);
                                            synchronized (Cipher.class) {
                                                instance = Cipher.getInstance("AES/ECB/PKCS7Padding");
                                                instance.init(2, secretKeySpec);
                                                array = new byte[instance.getOutputSize(decode.length)];
                                                instance.doFinal(array, instance.update(decode, 0, decode.length, array, 0));
                                                return array;
                                            }
                                            Label_0172: {
                                                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                                            }
                                        }
                                        b = true;
                                        break Label_0280;
                                        string = s2.subSequence(n2, n + 1).toString();
                                        forName2 = Charset.forName("UTF8");
                                        Intrinsics.checkExpressionValueIsNotNull(forName2, "Charset.forName(charsetName)");
                                        continue Label_0182_Outer;
                                    }
                                }
                                // iftrue(Label_0320:, string == null)
                                // iftrue(Label_0172:, string == null)
                                catch (Exception ex) {
                                    ex.printStackTrace();
                                    return null;
                                }
                                if (n2 > n) {
                                    continue Label_0122;
                                }
                                if (n3 == 0) {
                                    n4 = n2;
                                    continue Label_0182_Outer;
                                }
                                n4 = n;
                                continue Label_0182_Outer;
                            }
                            Label_0277: {
                                b = false;
                            }
                        }
                        if (n3 == 0) {
                            if (!b) {
                                n3 = 1;
                                continue Label_0103_Outer;
                            }
                            ++n2;
                            continue Label_0103_Outer;
                        }
                        else {
                            if (!b) {
                                continue Label_0122;
                            }
                            --n;
                            continue Label_0103_Outer;
                        }
                        break;
                    }
                    break;
                }
                bytes2 = null;
                continue;
            }
        }
    }
    
    public final String encrypt(byte[] encode, final String s) {
        Intrinsics.checkParameterIsNotNull(encode, "strToEncrypt");
        Intrinsics.checkParameterIsNotNull(s, "secret_key");
        Security.addProvider(new BouncyCastleProvider());
        try {
            final Charset forName = Charset.forName("UTF8");
            Intrinsics.checkExpressionValueIsNotNull(forName, "Charset.forName(charsetName)");
            final byte[] bytes = s.getBytes(forName);
            Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
            final byte[] digest = MessageDigest.getInstance("SHA-256").digest(bytes);
            Intrinsics.checkExpressionValueIsNotNull(digest, "MessageDigest.getInstanc\u2026HA-256\").digest(keyBytes)");
            final SecretKeySpec secretKeySpec = new SecretKeySpec(digest, "AES");
            synchronized (Cipher.class) {
                final Cipher instance = Cipher.getInstance("AES/ECB/PKCS7Padding");
                instance.init(1, secretKeySpec);
                final byte[] array = new byte[instance.getOutputSize(encode.length)];
                instance.doFinal(array, instance.update(encode, 0, encode.length, array, 0));
                encode = Base64.encode(array);
                Intrinsics.checkExpressionValueIsNotNull(encode, "Base64.encode(cipherText)");
                return new String(encode, Charsets.UTF_8);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0005\u001a\u00020\u0004R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.¢\u0006\u0002\n\u0000¨\u0006\u0006" }, d2 = { "Lat/esse/betterCrypt/util/EncryptionUtil$Companion;", "", "()V", "instance", "Lat/esse/betterCrypt/util/EncryptionUtil;", "newInstance", "app_release" }, k = 1, mv = { 1, 1, 15 })
    public static final class Companion
    {
        private Companion() {
        }
        
        public static final /* synthetic */ EncryptionUtil access$getInstance$li(final Companion companion) {
            return EncryptionUtil.access$getInstance$cp();
        }
        
        public final EncryptionUtil newInstance() {
            if (access$getInstance$li(EncryptionUtil.Companion) == null) {
                EncryptionUtil.access$setInstance$cp(new EncryptionUtil(null));
            }
            final EncryptionUtil access$getInstance$cp = EncryptionUtil.access$getInstance$cp();
            if (access$getInstance$cp == null) {
                Intrinsics.throwUninitializedPropertyAccessException("instance");
            }
            return access$getInstance$cp;
        }
    }
}
