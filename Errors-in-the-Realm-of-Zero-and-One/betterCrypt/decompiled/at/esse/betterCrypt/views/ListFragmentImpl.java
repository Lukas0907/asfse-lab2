// 
// Decompiled by Procyon v0.5.36
// 

package at.esse.betterCrypt.views;

import android.content.Context;
import kotlin.TypeCastException;
import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;
import at.esse.bettercrypt.R;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.View;
import kotlin.jvm.internal.Intrinsics;
import at.esse.betterCrypt.list.ListAdapter;
import java.util.HashMap;
import kotlin.Metadata;
import androidx.fragment.app.Fragment;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u0005¢\u0006\u0002\u0010\u0003J&\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0006\u0010\b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0016J\b\u0010\u000e\u001a\u00020\u000fH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.¢\u0006\u0002\n\u0000¨\u0006\u0010" }, d2 = { "Lat/esse/betterCrypt/views/ListFragmentImpl;", "Landroidx/fragment/app/Fragment;", "Lat/esse/betterCrypt/views/ListFragment;", "()V", "curr", "Lat/esse/betterCrypt/list/ListAdapter;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "triggerUpdate", "", "app_release" }, k = 1, mv = { 1, 1, 15 })
public final class ListFragmentImpl extends Fragment implements ListFragment
{
    private HashMap _$_findViewCache;
    private ListAdapter curr;
    
    public void _$_clearFindViewByIdCache() {
        final HashMap $_findViewCache = this._$_findViewCache;
        if ($_findViewCache != null) {
            $_findViewCache.clear();
        }
    }
    
    public View _$_findCachedViewById(final int n) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View viewById;
        if ((viewById = this._$_findViewCache.get(n)) == null) {
            final View view = this.getView();
            if (view == null) {
                return null;
            }
            viewById = view.findViewById(n);
            this._$_findViewCache.put(n, viewById);
        }
        return viewById;
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        Intrinsics.checkParameterIsNotNull(layoutInflater, "inflater");
        final View inflate = layoutInflater.inflate(2131427375, viewGroup, false);
        Intrinsics.checkExpressionValueIsNotNull(inflate, "view");
        final RecyclerView recyclerView = (RecyclerView)inflate.findViewById(R.id.recycler);
        recyclerView.setLayoutManager((RecyclerView.LayoutManager)new LinearLayoutManager(recyclerView.getContext()));
        final Context context = recyclerView.getContext();
        if (context != null) {
            this.curr = new ListAdapter((Activity)context);
            final ListAdapter curr = this.curr;
            if (curr == null) {
                Intrinsics.throwUninitializedPropertyAccessException("curr");
            }
            recyclerView.setAdapter((RecyclerView.Adapter)curr);
            return inflate;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.app.Activity");
    }
    
    @Override
    public void triggerUpdate() {
        if (this.curr == null) {
            return;
        }
        final ListAdapter curr = this.curr;
        if (curr == null) {
            Intrinsics.throwUninitializedPropertyAccessException("curr");
        }
        if (curr != null) {
            curr.trigger();
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type at.esse.betterCrypt.list.TriggerInterface");
    }
}
