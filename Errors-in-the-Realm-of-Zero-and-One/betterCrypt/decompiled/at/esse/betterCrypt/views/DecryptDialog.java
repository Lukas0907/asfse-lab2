// 
// Decompiled by Procyon v0.5.36
// 

package at.esse.betterCrypt.views;

import android.view.Window;
import android.app.Dialog;
import android.text.TextWatcher;
import android.view.View$OnClickListener;
import android.widget.Button;
import android.app.Activity;
import androidx.fragment.app.FragmentActivity;
import at.esse.betterCrypt.db.AppDatabase;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.View;
import com.google.android.material.textfield.TextInputLayout;
import android.text.SpannableStringBuilder;
import at.esse.bettercrypt.R;
import com.google.android.material.textfield.TextInputEditText;
import android.os.Environment;
import android.widget.Toast;
import kotlin.jvm.internal.Intrinsics;
import at.esse.betterCrypt.model.File;
import at.esse.betterCrypt.util.EncryptionUtil;
import at.esse.betterCrypt.dao.FileDao;
import java.util.HashMap;
import kotlin.Metadata;
import androidx.fragment.app.DialogFragment;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001dB\u0005¢\u0006\u0002\u0010\u0002J(\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u0007\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J&\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0016J\b\u0010\u0018\u001a\u00020\nH\u0016J\u0010\u0010\u0019\u001a\u00020\n2\u0006\u0010\u001a\u001a\u00020\fH\u0002J\u0012\u0010\u001b\u001a\u0004\u0018\u00010\f2\u0006\u0010\u001c\u001a\u00020\fH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082.¢\u0006\u0002\n\u0000¨\u0006\u001e" }, d2 = { "Lat/esse/betterCrypt/views/DecryptDialog;", "Landroidx/fragment/app/DialogFragment;", "()V", "dao", "Lat/esse/betterCrypt/dao/FileDao;", "encryption", "Lat/esse/betterCrypt/util/EncryptionUtil;", "file", "Lat/esse/betterCrypt/model/File;", "decryptFile", "", "password", "", "name", "error", "", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onStart", "showPasswordError", "message", "validateString", "string", "Companion", "app_release" }, k = 1, mv = { 1, 1, 15 })
public final class DecryptDialog extends DialogFragment
{
    public static final Companion Companion;
    private HashMap _$_findViewCache;
    private FileDao dao;
    private final EncryptionUtil encryption;
    private File file;
    
    static {
        Companion = new Companion(null);
    }
    
    public DecryptDialog() {
        this.encryption = EncryptionUtil.Companion.newInstance();
    }
    
    public static final /* synthetic */ void access$setFile$p(final DecryptDialog decryptDialog, final File file) {
        decryptDialog.file = file;
    }
    
    private final void decryptFile(final String s, final String child, final String s2, final boolean b) {
        if (b) {
            Toast.makeText(this.getContext(), (CharSequence)"Check your input", 0).show();
            return;
        }
        final byte[] decryptWithAES = this.encryption.decryptWithAES(s, s2);
        if (decryptWithAES == null) {
            Toast.makeText(this.getContext(), (CharSequence)"Failed to decrypt file", 1).show();
            return;
        }
        final java.io.File file = new java.io.File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), child);
        if (!file.canWrite()) {
            Toast.makeText(this.getContext(), (CharSequence)"Missing the write permission.", 1).show();
            return;
        }
        FilesKt__FileReadWriteKt.writeBytes(file, decryptWithAES);
        Toast.makeText(this.getContext(), (CharSequence)"File saved to your 'Downloads' folder.", 1).show();
        this.dismiss();
    }
    
    private final void showPasswordError(final String s) {
        final TextInputEditText textInputEditText = (TextInputEditText)this._$_findCachedViewById(R.id.pwd_input);
        Intrinsics.checkExpressionValueIsNotNull(textInputEditText, "pwd_input");
        textInputEditText.setText((CharSequence)new SpannableStringBuilder((CharSequence)""));
        final TextInputLayout textInputLayout = (TextInputLayout)this._$_findCachedViewById(R.id.password);
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout, "password");
        textInputLayout.setError(s);
    }
    
    private final String validateString(final String s) {
        if (s.length() < 1) {
            return "Input too short";
        }
        return null;
    }
    
    public void _$_clearFindViewByIdCache() {
        final HashMap $_findViewCache = this._$_findViewCache;
        if ($_findViewCache != null) {
            $_findViewCache.clear();
        }
    }
    
    public View _$_findCachedViewById(final int n) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View viewById;
        if ((viewById = this._$_findViewCache.get(n)) == null) {
            final View view = this.getView();
            if (view == null) {
                return null;
            }
            viewById = view.findViewById(n);
            this._$_findViewCache.put(n, viewById);
        }
        return viewById;
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        Intrinsics.checkParameterIsNotNull(layoutInflater, "inflater");
        final View inflate = layoutInflater.inflate(2131427373, viewGroup, false);
        final AppDatabase.Companion companion = AppDatabase.Companion;
        final FragmentActivity activity = this.getActivity();
        if (activity == null) {
            Intrinsics.throwNpe();
        }
        Intrinsics.checkExpressionValueIsNotNull(activity, "activity!!");
        this.dao = companion.newInstance(activity).fileDAO();
        Intrinsics.checkExpressionValueIsNotNull(inflate, "view");
        ((Button)inflate.findViewById(R.id.cancel)).setOnClickListener((View$OnClickListener)new DecryptDialog$onCreateView.DecryptDialog$onCreateView$1(this));
        ((Button)inflate.findViewById(R.id.decrypt)).setOnClickListener((View$OnClickListener)new DecryptDialog$onCreateView.DecryptDialog$onCreateView$2(this));
        ((TextInputEditText)inflate.findViewById(R.id.pwd_input)).addTextChangedListener((TextWatcher)new DecryptDialog$onCreateView.DecryptDialog$onCreateView$3(this));
        return inflate;
    }
    
    @Override
    public void onStart() {
        super.onStart();
        final Dialog dialog = this.getDialog();
        Intrinsics.checkExpressionValueIsNotNull(dialog, "dialog");
        final Window window = dialog.getWindow();
        if (window == null) {
            Intrinsics.throwNpe();
        }
        window.setLayout(-1, -2);
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007" }, d2 = { "Lat/esse/betterCrypt/views/DecryptDialog$Companion;", "", "()V", "newInstance", "Lat/esse/betterCrypt/views/DecryptDialog;", "file", "Lat/esse/betterCrypt/model/File;", "app_release" }, k = 1, mv = { 1, 1, 15 })
    public static final class Companion
    {
        private Companion() {
        }
        
        public final DecryptDialog newInstance(final File file) {
            Intrinsics.checkParameterIsNotNull(file, "file");
            final DecryptDialog decryptDialog = new DecryptDialog();
            DecryptDialog.access$setFile$p(decryptDialog, file);
            return decryptDialog;
        }
    }
}
