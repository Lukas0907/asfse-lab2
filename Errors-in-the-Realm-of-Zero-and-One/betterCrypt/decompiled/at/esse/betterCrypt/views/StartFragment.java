// 
// Decompiled by Procyon v0.5.36
// 

package at.esse.betterCrypt.views;

import android.view.View$OnClickListener;
import at.esse.bettercrypt.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import kotlin.jvm.internal.Intrinsics;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.View;
import java.util.HashMap;
import kotlin.Metadata;
import androidx.fragment.app.Fragment;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J&\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0016¨\u0006\u000b" }, d2 = { "Lat/esse/betterCrypt/views/StartFragment;", "Landroidx/fragment/app/Fragment;", "()V", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "app_release" }, k = 1, mv = { 1, 1, 15 })
public final class StartFragment extends Fragment
{
    private HashMap _$_findViewCache;
    
    public void _$_clearFindViewByIdCache() {
        final HashMap $_findViewCache = this._$_findViewCache;
        if ($_findViewCache != null) {
            $_findViewCache.clear();
        }
    }
    
    public View _$_findCachedViewById(final int n) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View viewById;
        if ((viewById = this._$_findViewCache.get(n)) == null) {
            final View view = this.getView();
            if (view == null) {
                return null;
            }
            viewById = view.findViewById(n);
            this._$_findViewCache.put(n, viewById);
        }
        return viewById;
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        Intrinsics.checkParameterIsNotNull(layoutInflater, "inflater");
        final View inflate = layoutInflater.inflate(2131427376, viewGroup, false);
        Intrinsics.checkExpressionValueIsNotNull(inflate, "view");
        ((FloatingActionButton)inflate.findViewById(R.id.fab)).setOnClickListener((View$OnClickListener)new StartFragment$onCreateView.StartFragment$onCreateView$1(this));
        return inflate;
    }
}
