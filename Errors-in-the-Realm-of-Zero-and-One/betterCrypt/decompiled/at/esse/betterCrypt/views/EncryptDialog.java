// 
// Decompiled by Procyon v0.5.36
// 

package at.esse.betterCrypt.views;

import android.view.Window;
import android.app.Dialog;
import android.text.TextWatcher;
import android.view.View$OnClickListener;
import android.widget.Button;
import android.app.Activity;
import androidx.fragment.app.FragmentActivity;
import at.esse.betterCrypt.db.AppDatabase;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.database.Cursor;
import android.view.View;
import com.google.android.material.textfield.TextInputLayout;
import android.text.SpannableStringBuilder;
import java.io.InputStream;
import android.content.ContentResolver;
import android.content.Context;
import at.esse.betterCrypt.model.File;
import kotlin.text.Charsets;
import java.net.URLDecoder;
import at.esse.bettercrypt.R;
import com.google.android.material.textfield.TextInputEditText;
import kotlin.io.ByteStreamsKt;
import android.net.Uri;
import kotlin.jvm.internal.Intrinsics;
import android.widget.Toast;
import android.content.Intent;
import at.esse.betterCrypt.util.EncryptionUtil;
import at.esse.betterCrypt.dao.FileDao;
import java.util.HashMap;
import kotlin.Metadata;
import androidx.fragment.app.DialogFragment;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u00020\nH\u0002J\u0018\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\rH\u0002J\"\u0010\u000f\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00112\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0016J&\u0010\u0015\u001a\u0004\u0018\u00010\u00162\u0006\u0010\u0017\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\b\u0010\u001d\u001a\u00020\nH\u0016J\u0010\u0010\u001e\u001a\u00020\n2\u0006\u0010\u001f\u001a\u00020\bH\u0002J\u0010\u0010 \u001a\u00020\n2\u0006\u0010\u001f\u001a\u00020\bH\u0002J\u0012\u0010!\u001a\u0004\u0018\u00010\b2\u0006\u0010\"\u001a\u00020\bH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082.¢\u0006\u0002\n\u0000¨\u0006#" }, d2 = { "Lat/esse/betterCrypt/views/EncryptDialog;", "Landroidx/fragment/app/DialogFragment;", "()V", "dao", "Lat/esse/betterCrypt/dao/FileDao;", "encryption", "Lat/esse/betterCrypt/util/EncryptionUtil;", "files_path", "", "chooseFile", "", "encryptFile", "file_chooser", "", "password", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onStart", "showFileChooserError", "message", "showPasswordError", "validateString", "string", "app_release" }, k = 1, mv = { 1, 1, 15 })
public final class EncryptDialog extends DialogFragment
{
    private HashMap _$_findViewCache;
    private FileDao dao;
    private final EncryptionUtil encryption;
    private String files_path;
    
    public EncryptDialog() {
        this.encryption = EncryptionUtil.Companion.newInstance();
    }
    
    private final void chooseFile() {
        this.startActivityForResult(Intent.createChooser(new Intent().setType("*/*").setAction("android.intent.action.GET_CONTENT"), (CharSequence)"Select a file"), 3223);
    }
    
    private final void encryptFile(final boolean b, final boolean b2) {
        if (b || b2) {
            Toast.makeText(this.getContext(), (CharSequence)"Check your input", 0).show();
            return;
        }
        final Context context = this.getContext();
        byte[] bytes = null;
        Label_0080: {
            if (context != null) {
                final ContentResolver contentResolver = context.getContentResolver();
                if (contentResolver != null) {
                    final String files_path = this.files_path;
                    if (files_path == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("files_path");
                    }
                    final InputStream openInputStream = contentResolver.openInputStream(Uri.parse(files_path));
                    if (openInputStream != null) {
                        bytes = ByteStreamsKt.readBytes(openInputStream);
                        break Label_0080;
                    }
                }
            }
            bytes = null;
        }
        if (bytes == null) {
            Toast.makeText(this.getContext(), (CharSequence)"Failed to read file", 1).show();
            return;
        }
        if (bytes.length > 1024) {
            Toast.makeText(this.getContext(), (CharSequence)"Only supporting files up tp 1024 bytes", 1).show();
            return;
        }
        final EncryptionUtil encryption = this.encryption;
        final TextInputEditText textInputEditText = (TextInputEditText)this._$_findCachedViewById(R.id.pwd_input);
        Intrinsics.checkExpressionValueIsNotNull(textInputEditText, "pwd_input");
        final String encrypt = encryption.encrypt(bytes, String.valueOf(textInputEditText.getText()));
        if (encrypt == null) {
            Toast.makeText(this.getContext(), (CharSequence)"Failed to encrypt file", 1).show();
            return;
        }
        final FileDao dao = this.dao;
        if (dao == null) {
            Intrinsics.throwUninitializedPropertyAccessException("dao");
        }
        final TextInputEditText textInputEditText2 = (TextInputEditText)this._$_findCachedViewById(R.id.file_path);
        Intrinsics.checkExpressionValueIsNotNull(textInputEditText2, "file_path");
        final String decode = URLDecoder.decode(String.valueOf(textInputEditText2.getText()), "UTF-8");
        Intrinsics.checkExpressionValueIsNotNull(decode, "URLDecoder.decode(file_p\u2026text.toString(), \"UTF-8\")");
        final long currentTimeMillis = System.currentTimeMillis();
        final byte[] bytes2 = encrypt.getBytes(Charsets.UTF_8);
        Intrinsics.checkExpressionValueIsNotNull(bytes2, "(this as java.lang.String).getBytes(charset)");
        dao.insert(new File(decode, currentTimeMillis, bytes2));
        this.dismiss();
    }
    
    private final void showFileChooserError(final String s) {
        final TextInputEditText textInputEditText = (TextInputEditText)this._$_findCachedViewById(R.id.file_path);
        Intrinsics.checkExpressionValueIsNotNull(textInputEditText, "file_path");
        textInputEditText.setText((CharSequence)new SpannableStringBuilder((CharSequence)""));
        final TextInputLayout textInputLayout = (TextInputLayout)this._$_findCachedViewById(R.id.file_chooser);
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout, "file_chooser");
        textInputLayout.setError(s);
    }
    
    private final void showPasswordError(final String s) {
        final TextInputEditText textInputEditText = (TextInputEditText)this._$_findCachedViewById(R.id.pwd_input);
        Intrinsics.checkExpressionValueIsNotNull(textInputEditText, "pwd_input");
        textInputEditText.setText((CharSequence)new SpannableStringBuilder((CharSequence)""));
        final TextInputLayout textInputLayout = (TextInputLayout)this._$_findCachedViewById(R.id.password);
        Intrinsics.checkExpressionValueIsNotNull(textInputLayout, "password");
        textInputLayout.setError(s);
    }
    
    private final String validateString(final String s) {
        if (s.length() < 0) {
            return "Input too short";
        }
        return null;
    }
    
    public void _$_clearFindViewByIdCache() {
        final HashMap $_findViewCache = this._$_findViewCache;
        if ($_findViewCache != null) {
            $_findViewCache.clear();
        }
    }
    
    public View _$_findCachedViewById(final int n) {
        if (this._$_findViewCache == null) {
            this._$_findViewCache = new HashMap();
        }
        View viewById;
        if ((viewById = this._$_findViewCache.get(n)) == null) {
            final View view = this.getView();
            if (view == null) {
                return null;
            }
            viewById = view.findViewById(n);
            this._$_findViewCache.put(n, viewById);
        }
        return viewById;
    }
    
    @Override
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        if (n == 3223 && n2 == -1) {
            final TextInputLayout textInputLayout = (TextInputLayout)this._$_findCachedViewById(R.id.file_chooser);
            Intrinsics.checkExpressionValueIsNotNull(textInputLayout, "file_chooser");
            textInputLayout.setError(null);
            Uri data;
            if (intent != null) {
                data = intent.getData();
            }
            else {
                data = null;
            }
            final String value = String.valueOf(data);
            final String validateString = this.validateString(value);
            if (validateString != null) {
                this.showFileChooserError(validateString);
                return;
            }
            this.files_path = value;
            final Context context = this.getContext();
            Cursor query = null;
            Label_0159: {
                if (context != null) {
                    final ContentResolver contentResolver = context.getContentResolver();
                    if (contentResolver != null) {
                        Uri data2;
                        if (intent != null) {
                            data2 = intent.getData();
                        }
                        else {
                            data2 = null;
                        }
                        query = contentResolver.query(data2, new String[] { "_display_name" }, (String)null, (String[])null, (String)null);
                        break Label_0159;
                    }
                }
                query = null;
            }
            if (query != null) {
                if (query.moveToFirst()) {
                    final String string = query.getString(0);
                    if (string != null) {
                        final TextInputEditText textInputEditText = (TextInputEditText)this._$_findCachedViewById(R.id.file_path);
                        Intrinsics.checkExpressionValueIsNotNull(textInputEditText, "file_path");
                        textInputEditText.setText((CharSequence)new SpannableStringBuilder((CharSequence)URLDecoder.decode(StringsKt__StringsKt.substringAfterLast$default(string, "/", null, 2, null), "UTF-8")));
                    }
                }
                query.close();
            }
        }
        else {
            this.showFileChooserError("Failed to get file path.");
        }
    }
    
    @Override
    public View onCreateView(final LayoutInflater layoutInflater, final ViewGroup viewGroup, final Bundle bundle) {
        Intrinsics.checkParameterIsNotNull(layoutInflater, "inflater");
        final View inflate = layoutInflater.inflate(2131427374, viewGroup, false);
        final AppDatabase.Companion companion = AppDatabase.Companion;
        final FragmentActivity activity = this.getActivity();
        if (activity == null) {
            Intrinsics.throwNpe();
        }
        Intrinsics.checkExpressionValueIsNotNull(activity, "activity!!");
        this.dao = companion.newInstance(activity).fileDAO();
        Intrinsics.checkExpressionValueIsNotNull(inflate, "view");
        ((Button)inflate.findViewById(R.id.cancel)).setOnClickListener((View$OnClickListener)new EncryptDialog$onCreateView.EncryptDialog$onCreateView$1(this));
        ((Button)inflate.findViewById(R.id.encrypt)).setOnClickListener((View$OnClickListener)new EncryptDialog$onCreateView.EncryptDialog$onCreateView$2(this));
        ((TextInputEditText)inflate.findViewById(R.id.file_path)).setOnClickListener((View$OnClickListener)new EncryptDialog$onCreateView.EncryptDialog$onCreateView$3(this));
        ((TextInputEditText)inflate.findViewById(R.id.pwd_input)).addTextChangedListener((TextWatcher)new EncryptDialog$onCreateView.EncryptDialog$onCreateView$4(this));
        return inflate;
    }
    
    @Override
    public void onStart() {
        super.onStart();
        final Dialog dialog = this.getDialog();
        Intrinsics.checkExpressionValueIsNotNull(dialog, "dialog");
        final Window window = dialog.getWindow();
        if (window == null) {
            Intrinsics.throwNpe();
        }
        window.setLayout(-1, -2);
    }
}
