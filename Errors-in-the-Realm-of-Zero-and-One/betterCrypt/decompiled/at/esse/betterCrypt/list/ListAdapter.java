// 
// Decompiled by Procyon v0.5.36
// 

package at.esse.betterCrypt.list;

import at.esse.bettercrypt.R;
import android.widget.Button;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.view.View$OnClickListener;
import java.text.SimpleDateFormat;
import at.esse.betterCrypt.db.AppDatabase;
import kotlin.jvm.internal.Intrinsics;
import at.esse.betterCrypt.model.File;
import java.util.List;
import android.app.Activity;
import kotlin.Metadata;
import androidx.recyclerview.widget.RecyclerView;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003:\u0001\u0017B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\f\u001a\u00020\rH\u0016J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\rH\u0017J\u0018\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\rH\u0016J\b\u0010\u0016\u001a\u00020\u000fH\u0016R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u0018" }, d2 = { "Lat/esse/betterCrypt/list/ListAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lat/esse/betterCrypt/list/ListAdapter$ViewHolder;", "Lat/esse/betterCrypt/list/TriggerInterface;", "activity", "Landroid/app/Activity;", "(Landroid/app/Activity;)V", "getActivity", "()Landroid/app/Activity;", "list", "", "Lat/esse/betterCrypt/model/File;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "trigger", "ViewHolder", "app_release" }, k = 1, mv = { 1, 1, 15 })
public final class ListAdapter extends Adapter<ViewHolder> implements TriggerInterface
{
    private final Activity activity;
    private List<File> list;
    
    public ListAdapter(final Activity activity) {
        Intrinsics.checkParameterIsNotNull(activity, "activity");
        this.activity = activity;
        this.list = AppDatabase.Companion.newInstance(this.activity).fileDAO().getAll();
    }
    
    public final Activity getActivity() {
        return this.activity;
    }
    
    @Override
    public int getItemCount() {
        return this.list.size();
    }
    
    public void onBindViewHolder(final ViewHolder viewHolder, final int n) {
        Intrinsics.checkParameterIsNotNull(viewHolder, "holder");
        final TextView name = viewHolder.getName();
        Intrinsics.checkExpressionValueIsNotNull(name, "holder.name");
        name.setText((CharSequence)this.list.get(n).getName());
        final TextView date = viewHolder.getDate();
        Intrinsics.checkExpressionValueIsNotNull(date, "holder.date");
        date.setText((CharSequence)new SimpleDateFormat("dd.MM.yyyy").format(this.list.get(n).getCreation()));
        viewHolder.getDecrypt().setOnClickListener((View$OnClickListener)new ListAdapter$onBindViewHolder.ListAdapter$onBindViewHolder$1(this, n));
    }
    
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int n) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "parent");
        final View inflate = this.activity.getLayoutInflater().inflate(2131427377, viewGroup, false);
        Intrinsics.checkExpressionValueIsNotNull(inflate, "inflate.inflate(R.layout.item_file, parent, false)");
        return new ViewHolder(inflate);
    }
    
    @Override
    public void trigger() {
        this.list = AppDatabase.Companion.newInstance(this.activity).fileDAO().getAll();
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0019\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0019\u0010\n\u001a\n \u0007*\u0004\u0018\u00010\u000b0\u000b¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0019\u0010\u000e\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\t¨\u0006\u0010" }, d2 = { "Lat/esse/betterCrypt/list/ListAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Landroid/view/View;)V", "date", "Landroid/widget/TextView;", "kotlin.jvm.PlatformType", "getDate", "()Landroid/widget/TextView;", "decrypt", "Landroid/widget/Button;", "getDecrypt", "()Landroid/widget/Button;", "name", "getName", "app_release" }, k = 1, mv = { 1, 1, 15 })
    public static final class ViewHolder extends RecyclerView.ViewHolder
    {
        private final TextView date;
        private final Button decrypt;
        private final TextView name;
        
        public ViewHolder(final View view) {
            Intrinsics.checkParameterIsNotNull(view, "view");
            super(view);
            this.date = (TextView)view.findViewById(R.id.file_date);
            this.name = (TextView)view.findViewById(R.id.file_name);
            this.decrypt = (Button)view.findViewById(R.id.decrypt);
        }
        
        public final TextView getDate() {
            return this.date;
        }
        
        public final Button getDecrypt() {
            return this.decrypt;
        }
        
        public final TextView getName() {
            return this.name;
        }
    }
}
