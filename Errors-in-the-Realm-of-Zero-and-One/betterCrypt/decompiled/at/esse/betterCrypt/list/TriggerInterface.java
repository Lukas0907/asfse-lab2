// 
// Decompiled by Procyon v0.5.36
// 

package at.esse.betterCrypt.list;

import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004" }, d2 = { "Lat/esse/betterCrypt/list/TriggerInterface;", "", "trigger", "", "app_release" }, k = 1, mv = { 1, 1, 15 })
public interface TriggerInterface
{
    void trigger();
}
