// 
// Decompiled by Procyon v0.5.36
// 

package at.esse.betterCrypt.model;

import kotlin.TypeCastException;
import java.util.Arrays;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0012\n\u0002\b\u0010\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0007H\u00c6\u0003J'\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0017\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\u001a\u001a\u00020\u001bH\u0016J\t\u0010\u001c\u001a\u00020\u0003H\u00d6\u0001R\u0016\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0016\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u001e\u0010\r\u001a\u00020\u00058\u0006@\u0006X\u0087\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\n\"\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012¨\u0006\u001d" }, d2 = { "Lat/esse/betterCrypt/model/File;", "", "name", "", "creation", "", "data", "", "(Ljava/lang/String;J[B)V", "getCreation", "()J", "getData", "()[B", "id", "getId", "setId", "(J)V", "getName", "()Ljava/lang/String;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "app_release" }, k = 1, mv = { 1, 1, 15 })
public final class File
{
    private final long creation;
    private final byte[] data;
    private long id;
    private final String name;
    
    public File(final String name, final long creation, final byte[] data) {
        Intrinsics.checkParameterIsNotNull(name, "name");
        Intrinsics.checkParameterIsNotNull(data, "data");
        this.name = name;
        this.creation = creation;
        this.data = data;
    }
    
    public final String component1() {
        return this.name;
    }
    
    public final long component2() {
        return this.creation;
    }
    
    public final byte[] component3() {
        return this.data;
    }
    
    public final File copy(final String s, final long n, final byte[] array) {
        Intrinsics.checkParameterIsNotNull(s, "name");
        Intrinsics.checkParameterIsNotNull(array, "data");
        return new File(s, n, array);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        final Class<? extends File> class1 = this.getClass();
        Class<?> class2;
        if (o != null) {
            class2 = o.getClass();
        }
        else {
            class2 = null;
        }
        if (Intrinsics.areEqual(class1, class2) ^ true) {
            return false;
        }
        if (o != null) {
            final File file = (File)o;
            return !(Intrinsics.areEqual(this.name, file.name) ^ true) && this.creation == file.creation && Arrays.equals(this.data, file.data);
        }
        throw new TypeCastException("null cannot be cast to non-null type at.esse.betterCrypt.model.File");
    }
    
    public final long getCreation() {
        return this.creation;
    }
    
    public final byte[] getData() {
        return this.data;
    }
    
    public final long getId() {
        return this.id;
    }
    
    public final String getName() {
        return this.name;
    }
    
    @Override
    public int hashCode() {
        return (this.name.hashCode() * 31 + Long.valueOf(this.creation).hashCode()) * 31 + Arrays.hashCode(this.data);
    }
    
    public final void setId(final long id) {
        this.id = id;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("File(name=");
        sb.append(this.name);
        sb.append(", creation=");
        sb.append(this.creation);
        sb.append(", data=");
        sb.append(Arrays.toString(this.data));
        sb.append(")");
        return sb.toString();
    }
}
