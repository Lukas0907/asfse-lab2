// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function2;
import kotlin.DeprecationLevel;
import kotlin.Deprecated;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&¨\u0006\u0007" }, d2 = { "Lkotlinx/coroutines/CompletableJob;", "Lkotlinx/coroutines/Job;", "complete", "", "completeExceptionally", "exception", "", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public interface CompletableJob extends Job
{
    boolean complete();
    
    boolean completeExceptionally(final Throwable p0);
    
    @Metadata(bv = { 1, 0, 3 }, k = 3, mv = { 1, 1, 15 })
    public static final class DefaultImpls
    {
        public static <R> R fold(final CompletableJob completableJob, final R r, final Function2<? super R, ? super Element, ? extends R> function2) {
            Intrinsics.checkParameterIsNotNull(function2, "operation");
            return Job.DefaultImpls.fold(completableJob, r, function2);
        }
        
        public static <E extends Element> E get(final CompletableJob completableJob, final CoroutineContext.Key<E> key) {
            Intrinsics.checkParameterIsNotNull(key, "key");
            return Job.DefaultImpls.get(completableJob, key);
        }
        
        public static CoroutineContext minusKey(final CompletableJob completableJob, final CoroutineContext.Key<?> key) {
            Intrinsics.checkParameterIsNotNull(key, "key");
            return Job.DefaultImpls.minusKey(completableJob, key);
        }
        
        public static CoroutineContext plus(final CompletableJob completableJob, final CoroutineContext coroutineContext) {
            Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
            return Job.DefaultImpls.plus(completableJob, coroutineContext);
        }
        
        @Deprecated(level = DeprecationLevel.ERROR, message = "Operator '+' on two Job objects is meaningless. Job is a coroutine context element and `+` is a set-sum operator for coroutine contexts. The job to the right of `+` just replaces the job the left of `+`.")
        public static Job plus(final CompletableJob completableJob, final Job job) {
            Intrinsics.checkParameterIsNotNull(job, "other");
            return Job.DefaultImpls.plus(completableJob, job);
        }
    }
}
