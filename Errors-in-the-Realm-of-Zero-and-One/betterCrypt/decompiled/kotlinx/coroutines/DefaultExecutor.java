// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import java.util.concurrent.locks.LockSupport;
import kotlin.jvm.internal.Intrinsics;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u00c0\u0002\u0018\u00002\u00020\u00012\u00060\u0002j\u0002`\u0003B\u0007\b\u0002¢\u0006\u0002\u0010\u0004J\b\u0010\u0019\u001a\u00020\u001aH\u0002J\b\u0010\u001b\u001a\u00020\u0010H\u0002J\r\u0010\u001c\u001a\u00020\u001aH\u0000¢\u0006\u0002\b\u001dJ\u001c\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\b2\n\u0010!\u001a\u00060\u0002j\u0002`\u0003H\u0016J\b\u0010\"\u001a\u00020\u0014H\u0002J\b\u0010#\u001a\u00020\u001aH\u0016J\u000e\u0010$\u001a\u00020\u001a2\u0006\u0010%\u001a\u00020\bR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0006X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0006X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0006X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0086T¢\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e¢\u0006\b\n\u0000\u0012\u0004\b\u0011\u0010\u0004R\u000e\u0010\u0012\u001a\u00020\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u00020\u00148BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0015R\u0014\u0010\u0016\u001a\u00020\u00108TX\u0094\u0004¢\u0006\u0006\u001a\u0004\b\u0017\u0010\u0018¨\u0006&" }, d2 = { "Lkotlinx/coroutines/DefaultExecutor;", "Lkotlinx/coroutines/EventLoopImplBase;", "Ljava/lang/Runnable;", "Lkotlinx/coroutines/Runnable;", "()V", "ACTIVE", "", "DEFAULT_KEEP_ALIVE", "", "FRESH", "KEEP_ALIVE_NANOS", "SHUTDOWN_ACK", "SHUTDOWN_REQ", "THREAD_NAME", "", "_thread", "Ljava/lang/Thread;", "_thread$annotations", "debugStatus", "isShutdownRequested", "", "()Z", "thread", "getThread", "()Ljava/lang/Thread;", "acknowledgeShutdownIfNeeded", "", "createThreadSync", "ensureStarted", "ensureStarted$kotlinx_coroutines_core", "invokeOnTimeout", "Lkotlinx/coroutines/DisposableHandle;", "timeMillis", "block", "notifyStartup", "run", "shutdown", "timeout", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class DefaultExecutor extends EventLoopImplBase implements Runnable
{
    private static final int ACTIVE = 1;
    private static final long DEFAULT_KEEP_ALIVE = 1000L;
    private static final int FRESH = 0;
    public static final DefaultExecutor INSTANCE;
    private static final long KEEP_ALIVE_NANOS;
    private static final int SHUTDOWN_ACK = 3;
    private static final int SHUTDOWN_REQ = 2;
    public static final String THREAD_NAME = "kotlinx.coroutines.DefaultExecutor";
    private static volatile Thread _thread;
    private static volatile int debugStatus;
    
    static {
        EventLoop.incrementUseCount$default(INSTANCE = new DefaultExecutor(), false, 1, null);
        final TimeUnit milliseconds = TimeUnit.MILLISECONDS;
        while (true) {
            try {
                Long n = Long.getLong("kotlinx.coroutines.DefaultExecutor.keepAlive", 1000L);
                while (true) {
                    Intrinsics.checkExpressionValueIsNotNull(n, "try {\n            java.l\u2026AULT_KEEP_ALIVE\n        }");
                    KEEP_ALIVE_NANOS = milliseconds.toNanos(n);
                    return;
                    n = 1000L;
                    continue;
                }
            }
            catch (SecurityException ex) {}
            continue;
        }
    }
    
    private DefaultExecutor() {
    }
    
    private final void acknowledgeShutdownIfNeeded() {
        synchronized (this) {
            if (!this.isShutdownRequested()) {
                return;
            }
            DefaultExecutor.debugStatus = 3;
            this.resetAll();
            this.notifyAll();
        }
    }
    
    private final Thread createThreadSync() {
        synchronized (this) {
            Thread thread = DefaultExecutor._thread;
            if (thread == null) {
                thread = new Thread(this, "kotlinx.coroutines.DefaultExecutor");
                (DefaultExecutor._thread = thread).setDaemon(true);
                thread.start();
            }
            return thread;
        }
    }
    
    private final boolean isShutdownRequested() {
        final int debugStatus = DefaultExecutor.debugStatus;
        return debugStatus == 2 || debugStatus == 3;
    }
    
    private final boolean notifyStartup() {
        synchronized (this) {
            if (this.isShutdownRequested()) {
                return false;
            }
            DefaultExecutor.debugStatus = 1;
            this.notifyAll();
            return true;
        }
    }
    
    public final void ensureStarted$kotlinx_coroutines_core() {
        boolean assertions_ENABLED;
        int n;
        int n2 = 0;
        Label_0071_Outer:Label_0060_Outer:
        while (true) {
            while (true) {
                while (true) {
                    Label_0117: {
                        Label_0115: {
                            while (true) {
                                while (true) {
                                    Label_0108: {
                                        Label_0106: {
                                            synchronized (this) {
                                                assertions_ENABLED = DebugKt.getASSERTIONS_ENABLED();
                                                n = 1;
                                                if (assertions_ENABLED) {
                                                    if (DefaultExecutor._thread == null) {
                                                        n2 = 1;
                                                        break Label_0108;
                                                    }
                                                    break Label_0106;
                                                }
                                                else {
                                                    if (!DebugKt.getASSERTIONS_ENABLED()) {
                                                        DefaultExecutor.debugStatus = 0;
                                                        this.createThreadSync();
                                                        while (DefaultExecutor.debugStatus == 0) {
                                                            this.wait();
                                                        }
                                                        return;
                                                    }
                                                    n2 = n;
                                                    if (DefaultExecutor.debugStatus == 0) {
                                                        break Label_0117;
                                                    }
                                                    if (DefaultExecutor.debugStatus == 3) {
                                                        n2 = n;
                                                        break Label_0117;
                                                    }
                                                    break Label_0115;
                                                }
                                                throw new AssertionError();
                                                throw new AssertionError();
                                            }
                                        }
                                        n2 = 0;
                                    }
                                    if (n2 != 0) {
                                        continue Label_0071_Outer;
                                    }
                                    break;
                                }
                                continue Label_0060_Outer;
                            }
                        }
                        n2 = 0;
                    }
                    if (n2 != 0) {
                        continue Label_0060_Outer;
                    }
                    break;
                }
                continue;
            }
        }
    }
    
    @Override
    protected Thread getThread() {
        final Thread thread = DefaultExecutor._thread;
        if (thread != null) {
            return thread;
        }
        return this.createThreadSync();
    }
    
    @Override
    public DisposableHandle invokeOnTimeout(final long n, final Runnable runnable) {
        Intrinsics.checkParameterIsNotNull(runnable, "block");
        return this.scheduleInvokeOnTimeout(n, runnable);
    }
    
    @Override
    public void run() {
        ThreadLocalEventLoop.INSTANCE.setEventLoop$kotlinx_coroutines_core(this);
        final TimeSource timeSource = TimeSourceKt.getTimeSource();
        if (timeSource != null) {
            timeSource.registerTimeLoopThread();
        }
        try {
            if (!this.notifyStartup()) {
                return;
            }
            long n = Long.MAX_VALUE;
            while (true) {
                Thread.interrupted();
                final long processNextEvent = this.processNextEvent();
                long n2 = n;
                long nanos = processNextEvent;
                if (processNextEvent == Long.MAX_VALUE) {
                    final long n3 = lcmp(n, Long.MAX_VALUE);
                    if (n3 == 0) {
                        final TimeSource timeSource2 = TimeSourceKt.getTimeSource();
                        long n4;
                        if (timeSource2 != null) {
                            n4 = timeSource2.nanoTime();
                        }
                        else {
                            n4 = System.nanoTime();
                        }
                        if (n3 == 0) {
                            n = DefaultExecutor.KEEP_ALIVE_NANOS + n4;
                        }
                        final long n5 = n - n4;
                        if (n5 <= 0L) {
                            return;
                        }
                        nanos = RangesKt___RangesKt.coerceAtMost(processNextEvent, n5);
                        n2 = n;
                    }
                    else {
                        nanos = RangesKt___RangesKt.coerceAtMost(processNextEvent, DefaultExecutor.KEEP_ALIVE_NANOS);
                        n2 = n;
                    }
                }
                n = n2;
                if (nanos > 0L) {
                    if (this.isShutdownRequested()) {
                        return;
                    }
                    final TimeSource timeSource3 = TimeSourceKt.getTimeSource();
                    if (timeSource3 != null) {
                        timeSource3.parkNanos(this, nanos);
                        n = n2;
                    }
                    else {
                        LockSupport.parkNanos(this, nanos);
                        n = n2;
                    }
                }
            }
        }
        finally {
            DefaultExecutor._thread = null;
            this.acknowledgeShutdownIfNeeded();
            final TimeSource timeSource4 = TimeSourceKt.getTimeSource();
            if (timeSource4 != null) {
                timeSource4.unregisterTimeLoopThread();
            }
            if (!this.isEmpty()) {
                this.getThread();
            }
        }
    }
    
    public final void shutdown(final long n) {
        synchronized (this) {
            final long currentTimeMillis = System.currentTimeMillis();
            if (!this.isShutdownRequested()) {
                DefaultExecutor.debugStatus = 2;
            }
            while (DefaultExecutor.debugStatus != 3 && DefaultExecutor._thread != null) {
                final Thread thread = DefaultExecutor._thread;
                if (thread != null) {
                    final TimeSource timeSource = TimeSourceKt.getTimeSource();
                    if (timeSource != null) {
                        timeSource.unpark(thread);
                    }
                    else {
                        LockSupport.unpark(thread);
                    }
                }
                if (currentTimeMillis + n - System.currentTimeMillis() <= 0L) {
                    break;
                }
                this.wait(n);
            }
            DefaultExecutor.debugStatus = 0;
        }
    }
}
