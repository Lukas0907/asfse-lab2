// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.internal;

import kotlinx.coroutines.DebugKt;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0005\b&\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00002\u00020\u0002B\u0007¢\u0006\u0004\b\u0003\u0010\u0004J!\u0010\t\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00028\u00002\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006H&¢\u0006\u0004\b\t\u0010\nJ\u001b\u0010\f\u001a\u0004\u0018\u00010\u00062\b\u0010\u000b\u001a\u0004\u0018\u00010\u0006H\u0002¢\u0006\u0004\b\f\u0010\rJ\u0019\u0010\u000e\u001a\u0004\u0018\u00010\u00062\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b\u000e\u0010\rJ\u0019\u0010\u000f\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0005\u001a\u00028\u0000H&¢\u0006\u0004\b\u000f\u0010\rJ\u0017\u0010\u0011\u001a\u00020\u00102\b\u0010\u000b\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b\u0011\u0010\u0012R\u0013\u0010\u0013\u001a\u00020\u00108F@\u0006¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014¨\u0006\u0015" }, d2 = { "Lkotlinx/coroutines/internal/AtomicOp;", "T", "Lkotlinx/coroutines/internal/OpDescriptor;", "<init>", "()V", "affected", "", "failure", "", "complete", "(Ljava/lang/Object;Ljava/lang/Object;)V", "decision", "decide", "(Ljava/lang/Object;)Ljava/lang/Object;", "perform", "prepare", "", "tryDecide", "(Ljava/lang/Object;)Z", "isDecided", "()Z", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public abstract class AtomicOp<T> extends OpDescriptor
{
    private static final AtomicReferenceFieldUpdater _consensus$FU;
    private volatile Object _consensus;
    
    static {
        _consensus$FU = AtomicReferenceFieldUpdater.newUpdater(AtomicOp.class, Object.class, "_consensus");
    }
    
    public AtomicOp() {
        this._consensus = AtomicKt.access$getNO_DECISION$p();
    }
    
    private final Object decide(final Object o) {
        if (this.tryDecide(o)) {
            return o;
        }
        return this._consensus;
    }
    
    public abstract void complete(final T p0, final Object p1);
    
    public final boolean isDecided() {
        return this._consensus != AtomicKt.access$getNO_DECISION$p();
    }
    
    @Override
    public final Object perform(final Object o) {
        Object o2;
        if ((o2 = this._consensus) == AtomicKt.access$getNO_DECISION$p()) {
            o2 = this.decide(this.prepare(o));
        }
        this.complete(o, o2);
        return o2;
    }
    
    public abstract Object prepare(final T p0);
    
    public final boolean tryDecide(final Object o) {
        if (DebugKt.getASSERTIONS_ENABLED() && o == AtomicKt.access$getNO_DECISION$p()) {
            throw new AssertionError();
        }
        return AtomicOp._consensus$FU.compareAndSet(this, AtomicKt.access$getNO_DECISION$p(), o);
    }
}
