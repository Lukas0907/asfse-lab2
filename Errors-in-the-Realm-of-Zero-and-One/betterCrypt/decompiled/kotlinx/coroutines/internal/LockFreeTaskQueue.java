// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.internal;

import java.util.List;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0004\b\u0010\u0018\u0000*\b\b\u0000\u0010\u0002*\u00020\u00012\u00020\u0001B\u000f\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0015\u0010\b\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00028\u0000¢\u0006\u0004\b\b\u0010\tJ\r\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u000b\u0010\fJ\r\u0010\r\u001a\u00020\u0003¢\u0006\u0004\b\r\u0010\u000eJ-\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00010\u0012\"\u0004\b\u0001\u0010\u000f2\u0012\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0010¢\u0006\u0004\b\u0013\u0010\u0014J\u000f\u0010\u0015\u001a\u0004\u0018\u00018\u0000¢\u0006\u0004\b\u0015\u0010\u0016J&\u0010\u0018\u001a\u0004\u0018\u00018\u00002\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00030\u0010H\u0086\b¢\u0006\u0004\b\u0018\u0010\u0019R\u0013\u0010\u001a\u001a\u00020\u00038F@\u0006¢\u0006\u0006\u001a\u0004\b\u001a\u0010\u000eR\u0013\u0010\u001e\u001a\u00020\u001b8F@\u0006¢\u0006\u0006\u001a\u0004\b\u001c\u0010\u001d¨\u0006\u001f" }, d2 = { "Lkotlinx/coroutines/internal/LockFreeTaskQueue;", "", "E", "", "singleConsumer", "<init>", "(Z)V", "element", "addLast", "(Ljava/lang/Object;)Z", "", "close", "()V", "isClosed", "()Z", "R", "Lkotlin/Function1;", "transform", "", "map", "(Lkotlin/jvm/functions/Function1;)Ljava/util/List;", "removeFirstOrNull", "()Ljava/lang/Object;", "predicate", "removeFirstOrNullIf", "(Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "isEmpty", "", "getSize", "()I", "size", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public class LockFreeTaskQueue<E>
{
    public static final /* synthetic */ AtomicReferenceFieldUpdater _cur$FU$internal;
    public volatile /* synthetic */ Object _cur$internal;
    
    static {
        _cur$FU$internal = AtomicReferenceFieldUpdater.newUpdater(LockFreeTaskQueue.class, Object.class, "_cur$internal");
    }
    
    public LockFreeTaskQueue(final boolean b) {
        this._cur$internal = new LockFreeTaskQueueCore(8, b);
    }
    
    public final boolean addLast(final E e) {
        Intrinsics.checkParameterIsNotNull(e, "element");
        while (true) {
            final LockFreeTaskQueueCore lockFreeTaskQueueCore = (LockFreeTaskQueueCore)this._cur$internal;
            final int addLast = lockFreeTaskQueueCore.addLast(e);
            if (addLast == 0) {
                return true;
            }
            if (addLast != 1) {
                if (addLast != 2) {
                    continue;
                }
                return false;
            }
            else {
                LockFreeTaskQueue._cur$FU$internal.compareAndSet(this, lockFreeTaskQueueCore, lockFreeTaskQueueCore.next());
            }
        }
    }
    
    public final void close() {
        while (true) {
            final LockFreeTaskQueueCore lockFreeTaskQueueCore = (LockFreeTaskQueueCore)this._cur$internal;
            if (lockFreeTaskQueueCore.close()) {
                break;
            }
            LockFreeTaskQueue._cur$FU$internal.compareAndSet(this, lockFreeTaskQueueCore, lockFreeTaskQueueCore.next());
        }
    }
    
    public final int getSize() {
        return ((LockFreeTaskQueueCore)this._cur$internal).getSize();
    }
    
    public final boolean isClosed() {
        return ((LockFreeTaskQueueCore)this._cur$internal).isClosed();
    }
    
    public final boolean isEmpty() {
        return ((LockFreeTaskQueueCore)this._cur$internal).isEmpty();
    }
    
    public final <R> List<R> map(final Function1<? super E, ? extends R> function1) {
        Intrinsics.checkParameterIsNotNull(function1, "transform");
        return ((LockFreeTaskQueueCore)this._cur$internal).map(function1);
    }
    
    public final E removeFirstOrNull() {
        Symbol remove_FROZEN = null;
        while (true) {
            final LockFreeTaskQueueCore lockFreeTaskQueueCore = (LockFreeTaskQueueCore)this._cur$internal;
            Label_0204: {
                Object value = null;
            Label_0200:
                while (true) {
                    final long state$internal = lockFreeTaskQueueCore._state$internal;
                    remove_FROZEN = null;
                    if ((0x1000000000000000L & state$internal) != 0x0L) {
                        remove_FROZEN = LockFreeTaskQueueCore.REMOVE_FROZEN;
                        break Label_0204;
                    }
                    final LockFreeTaskQueueCore.Companion companion = LockFreeTaskQueueCore.Companion;
                    final int n = (int)((0x3FFFFFFFL & state$internal) >> 0);
                    if (((int)((0xFFFFFFFC0000000L & state$internal) >> 30) & LockFreeTaskQueueCore.access$getMask$p(lockFreeTaskQueueCore)) == (LockFreeTaskQueueCore.access$getMask$p(lockFreeTaskQueueCore) & n)) {
                        break Label_0204;
                    }
                    value = lockFreeTaskQueueCore.array$internal.get(LockFreeTaskQueueCore.access$getMask$p(lockFreeTaskQueueCore) & n);
                    if (value == null) {
                        if (LockFreeTaskQueueCore.access$getSingleConsumer$p(lockFreeTaskQueueCore)) {
                            break Label_0204;
                        }
                        continue;
                    }
                    else {
                        if (value instanceof LockFreeTaskQueueCore.Placeholder) {
                            break Label_0204;
                        }
                        final int n2 = n + 1 & 0x3FFFFFFF;
                        if (LockFreeTaskQueueCore._state$FU$internal.compareAndSet(lockFreeTaskQueueCore, state$internal, LockFreeTaskQueueCore.Companion.updateHead(state$internal, n2))) {
                            lockFreeTaskQueueCore.array$internal.set(LockFreeTaskQueueCore.access$getMask$p(lockFreeTaskQueueCore) & n, null);
                            break;
                        }
                        if (!LockFreeTaskQueueCore.access$getSingleConsumer$p(lockFreeTaskQueueCore)) {
                            continue;
                        }
                        LockFreeTaskQueueCore<Object> access$removeSlowPath = (LockFreeTaskQueueCore<Object>)lockFreeTaskQueueCore;
                        while (true) {
                            access$removeSlowPath = LockFreeTaskQueueCore.access$removeSlowPath(access$removeSlowPath, n, n2);
                            if (access$removeSlowPath != null) {
                                continue;
                            }
                            break Label_0200;
                        }
                    }
                }
                remove_FROZEN = (Symbol)value;
            }
            if (remove_FROZEN != LockFreeTaskQueueCore.REMOVE_FROZEN) {
                break;
            }
            LockFreeTaskQueue._cur$FU$internal.compareAndSet(this, lockFreeTaskQueueCore, lockFreeTaskQueueCore.next());
        }
        return (E)remove_FROZEN;
    }
    
    public final E removeFirstOrNullIf(final Function1<? super E, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        Symbol remove_FROZEN = null;
        while (true) {
            final LockFreeTaskQueueCore lockFreeTaskQueueCore = (LockFreeTaskQueueCore)this._cur$internal;
            Label_0236: {
                Object value = null;
            Label_0232:
                while (true) {
                    final long state$internal = lockFreeTaskQueueCore._state$internal;
                    remove_FROZEN = null;
                    if ((0x1000000000000000L & state$internal) != 0x0L) {
                        remove_FROZEN = LockFreeTaskQueueCore.REMOVE_FROZEN;
                        break Label_0236;
                    }
                    final LockFreeTaskQueueCore.Companion companion = LockFreeTaskQueueCore.Companion;
                    final int n = (int)((0x3FFFFFFFL & state$internal) >> 0);
                    if (((int)((0xFFFFFFFC0000000L & state$internal) >> 30) & LockFreeTaskQueueCore.access$getMask$p(lockFreeTaskQueueCore)) == (LockFreeTaskQueueCore.access$getMask$p(lockFreeTaskQueueCore) & n)) {
                        break Label_0236;
                    }
                    value = lockFreeTaskQueueCore.array$internal.get(LockFreeTaskQueueCore.access$getMask$p(lockFreeTaskQueueCore) & n);
                    if (value == null) {
                        if (LockFreeTaskQueueCore.access$getSingleConsumer$p(lockFreeTaskQueueCore)) {
                            break Label_0236;
                        }
                        continue;
                    }
                    else {
                        if (value instanceof LockFreeTaskQueueCore.Placeholder) {
                            break Label_0236;
                        }
                        if (!function1.invoke((E)value)) {
                            break Label_0236;
                        }
                        final int n2 = n + 1 & 0x3FFFFFFF;
                        if (LockFreeTaskQueueCore._state$FU$internal.compareAndSet(lockFreeTaskQueueCore, state$internal, LockFreeTaskQueueCore.Companion.updateHead(state$internal, n2))) {
                            lockFreeTaskQueueCore.array$internal.set(LockFreeTaskQueueCore.access$getMask$p(lockFreeTaskQueueCore) & n, null);
                            break;
                        }
                        if (!LockFreeTaskQueueCore.access$getSingleConsumer$p(lockFreeTaskQueueCore)) {
                            continue;
                        }
                        LockFreeTaskQueueCore<Object> access$removeSlowPath = (LockFreeTaskQueueCore<Object>)lockFreeTaskQueueCore;
                        while (true) {
                            access$removeSlowPath = LockFreeTaskQueueCore.access$removeSlowPath(access$removeSlowPath, n, n2);
                            if (access$removeSlowPath != null) {
                                continue;
                            }
                            break Label_0232;
                        }
                    }
                }
                remove_FROZEN = (Symbol)value;
            }
            if (remove_FROZEN != LockFreeTaskQueueCore.REMOVE_FROZEN) {
                break;
            }
            LockFreeTaskQueue._cur$FU$internal.compareAndSet(this, lockFreeTaskQueueCore, lockFreeTaskQueueCore.next());
        }
        return (E)remove_FROZEN;
    }
}
