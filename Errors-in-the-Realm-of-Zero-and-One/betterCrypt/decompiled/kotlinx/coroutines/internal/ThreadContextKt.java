// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.internal;

import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.ThreadContextElement;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function2;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u00002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u001a\u001a\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004H\u0000\u001a\u0010\u0010\u0010\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u000eH\u0000\u001a\u001c\u0010\u0011\u001a\u0004\u0018\u00010\u00042\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0004H\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000\"$\u0010\u0002\u001a\u0018\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0003X\u0082\u0004¢\u0006\u0002\n\u0000\",\u0010\u0006\u001a \u0012\n\u0012\b\u0012\u0002\b\u0003\u0018\u00010\u0007\u0012\u0004\u0012\u00020\u0005\u0012\n\u0012\b\u0012\u0002\b\u0003\u0018\u00010\u00070\u0003X\u0082\u0004¢\u0006\u0002\n\u0000\" \u0010\b\u001a\u0014\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\t0\u0003X\u0082\u0004¢\u0006\u0002\n\u0000\" \u0010\n\u001a\u0014\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\t0\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013" }, d2 = { "ZERO", "Lkotlinx/coroutines/internal/Symbol;", "countAll", "Lkotlin/Function2;", "", "Lkotlin/coroutines/CoroutineContext$Element;", "findOne", "Lkotlinx/coroutines/ThreadContextElement;", "restoreState", "Lkotlinx/coroutines/internal/ThreadState;", "updateState", "restoreThreadContext", "", "context", "Lkotlin/coroutines/CoroutineContext;", "oldState", "threadContextElements", "updateThreadContext", "countOrElement", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class ThreadContextKt
{
    private static final Symbol ZERO;
    private static final Function2<Object, CoroutineContext.Element, Object> countAll;
    private static final Function2<ThreadContextElement<?>, CoroutineContext.Element, ThreadContextElement<?>> findOne;
    private static final Function2<ThreadState, CoroutineContext.Element, ThreadState> restoreState;
    private static final Function2<ThreadState, CoroutineContext.Element, ThreadState> updateState;
    
    static {
        ZERO = new Symbol("ZERO");
        countAll = (Function2)ThreadContextKt$countAll.ThreadContextKt$countAll$1.INSTANCE;
        findOne = (Function2)ThreadContextKt$findOne.ThreadContextKt$findOne$1.INSTANCE;
        updateState = (Function2)ThreadContextKt$updateState.ThreadContextKt$updateState$1.INSTANCE;
        restoreState = (Function2)ThreadContextKt$restoreState.ThreadContextKt$restoreState$1.INSTANCE;
    }
    
    public static final void restoreThreadContext(final CoroutineContext coroutineContext, final Object o) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        if (o == ThreadContextKt.ZERO) {
            return;
        }
        if (o instanceof ThreadState) {
            ((ThreadState)o).start();
            coroutineContext.fold(o, (Function2<? super Object, ? super CoroutineContext.Element, ?>)ThreadContextKt.restoreState);
            return;
        }
        final ThreadContextElement<Object> fold = coroutineContext.fold((ThreadContextElement<Object>)null, (Function2<? super ThreadContextElement<Object>, ? super CoroutineContext.Element, ? extends ThreadContextElement<Object>>)ThreadContextKt.findOne);
        if (fold != null) {
            fold.restoreThreadContext(coroutineContext, o);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>");
    }
    
    public static final Object threadContextElements(final CoroutineContext coroutineContext) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        final Integer fold = coroutineContext.fold(0, (Function2<? super Integer, ? super CoroutineContext.Element, ? extends Integer>)ThreadContextKt.countAll);
        if (fold == null) {
            Intrinsics.throwNpe();
        }
        return fold;
    }
    
    public static final Object updateThreadContext(final CoroutineContext coroutineContext, Object threadContextElements) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        if (threadContextElements == null) {
            threadContextElements = threadContextElements(coroutineContext);
        }
        if (threadContextElements == Integer.valueOf(0)) {
            return ThreadContextKt.ZERO;
        }
        if (threadContextElements instanceof Integer) {
            return coroutineContext.fold(new ThreadState(coroutineContext, ((Number)threadContextElements).intValue()), (Function2<? super ThreadState, ? super CoroutineContext.Element, ? extends ThreadState>)ThreadContextKt.updateState);
        }
        if (threadContextElements != null) {
            return ((ThreadContextElement)threadContextElements).updateThreadContext(coroutineContext);
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>");
    }
}
