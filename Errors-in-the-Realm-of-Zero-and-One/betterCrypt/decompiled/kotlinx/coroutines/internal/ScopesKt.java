// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.internal;

import kotlin.coroutines.Continuation;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.AbstractCoroutine;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u000e\n\u0000\n\u0002\u0010\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u0018\u0010\u0000\u001a\u00020\u0001*\u0006\u0012\u0002\b\u00030\u00022\u0006\u0010\u0003\u001a\u00020\u0001H\u0000¨\u0006\u0004" }, d2 = { "tryRecover", "", "Lkotlinx/coroutines/AbstractCoroutine;", "exception", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class ScopesKt
{
    public static final Throwable tryRecover(final AbstractCoroutine<?> abstractCoroutine, final Throwable t) {
        Intrinsics.checkParameterIsNotNull(abstractCoroutine, "$this$tryRecover");
        Intrinsics.checkParameterIsNotNull(t, "exception");
        ScopeCoroutine scopeCoroutine = (ScopeCoroutine)abstractCoroutine;
        if (!(abstractCoroutine instanceof ScopeCoroutine)) {
            scopeCoroutine = null;
        }
        final ScopeCoroutine scopeCoroutine2 = scopeCoroutine;
        if (scopeCoroutine2 != null) {
            final Continuation<T> uCont = scopeCoroutine2.uCont;
            if (uCont != null) {
                return StackTraceRecoveryKt.recoverStackTrace(t, uCont);
            }
        }
        return t;
    }
}
