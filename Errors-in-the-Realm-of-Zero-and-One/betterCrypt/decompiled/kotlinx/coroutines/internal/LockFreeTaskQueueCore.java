// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.internal;

import kotlin.jvm.functions.Function2;
import java.util.ArrayList;
import java.util.List;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.DebugKt;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0019\b\u0000\u0018\u0000 2*\b\b\u0000\u0010\u0002*\u00020\u00012\u00020\u0001:\u000223B\u0017\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\u0007\u0010\bJ\u0015\u0010\n\u001a\u00020\u00032\u0006\u0010\t\u001a\u00028\u0000¢\u0006\u0004\b\n\u0010\u000bJ'\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00028\u00000\u0000j\b\u0012\u0004\u0012\u00028\u0000`\u000e2\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J'\u0010\u0011\u001a\u0012\u0012\u0004\u0012\u00028\u00000\u0000j\b\u0012\u0004\u0012\u00028\u0000`\u000e2\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u0011\u0010\u0010J\r\u0010\u0012\u001a\u00020\u0005¢\u0006\u0004\b\u0012\u0010\u0013J3\u0010\u0015\u001a\u0016\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0000j\n\u0012\u0004\u0012\u00028\u0000\u0018\u0001`\u000e2\u0006\u0010\u0014\u001a\u00020\u00032\u0006\u0010\t\u001a\u00028\u0000H\u0002¢\u0006\u0004\b\u0015\u0010\u0016J\r\u0010\u0017\u001a\u00020\u0005¢\u0006\u0004\b\u0017\u0010\u0013J-\u0010\u001c\u001a\b\u0012\u0004\u0012\u00028\u00010\u001b\"\u0004\b\u0001\u0010\u00182\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0019¢\u0006\u0004\b\u001c\u0010\u001dJ\u000f\u0010\u001e\u001a\u00020\fH\u0002¢\u0006\u0004\b\u001e\u0010\u001fJ\u0013\u0010 \u001a\b\u0012\u0004\u0012\u00028\u00000\u0000¢\u0006\u0004\b \u0010!J\u000f\u0010\"\u001a\u0004\u0018\u00010\u0001¢\u0006\u0004\b\"\u0010#J&\u0010%\u001a\u0004\u0018\u00010\u00012\u0012\u0010$\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00050\u0019H\u0086\b¢\u0006\u0004\b%\u0010&J3\u0010)\u001a\u0016\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0000j\n\u0012\u0004\u0012\u00028\u0000\u0018\u0001`\u000e2\u0006\u0010'\u001a\u00020\u00032\u0006\u0010(\u001a\u00020\u0003H\u0002¢\u0006\u0004\b)\u0010*R\u0016\u0010\u0004\u001a\u00020\u00038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0004\u0010+R\u0013\u0010,\u001a\u00020\u00058F@\u0006¢\u0006\u0006\u001a\u0004\b,\u0010\u0013R\u0016\u0010-\u001a\u00020\u00038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b-\u0010+R\u0016\u0010\u0006\u001a\u00020\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010.R\u0013\u00101\u001a\u00020\u00038F@\u0006¢\u0006\u0006\u001a\u0004\b/\u00100¨\u00064" }, d2 = { "Lkotlinx/coroutines/internal/LockFreeTaskQueueCore;", "", "E", "", "capacity", "", "singleConsumer", "<init>", "(IZ)V", "element", "addLast", "(Ljava/lang/Object;)I", "", "state", "Lkotlinx/coroutines/internal/Core;", "allocateNextCopy", "(J)Lkotlinx/coroutines/internal/LockFreeTaskQueueCore;", "allocateOrGetNextCopy", "close", "()Z", "index", "fillPlaceholder", "(ILjava/lang/Object;)Lkotlinx/coroutines/internal/LockFreeTaskQueueCore;", "isClosed", "R", "Lkotlin/Function1;", "transform", "", "map", "(Lkotlin/jvm/functions/Function1;)Ljava/util/List;", "markFrozen", "()J", "next", "()Lkotlinx/coroutines/internal/LockFreeTaskQueueCore;", "removeFirstOrNull", "()Ljava/lang/Object;", "predicate", "removeFirstOrNullIf", "(Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "oldHead", "newHead", "removeSlowPath", "(II)Lkotlinx/coroutines/internal/LockFreeTaskQueueCore;", "I", "isEmpty", "mask", "Z", "getSize", "()I", "size", "Companion", "Placeholder", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class LockFreeTaskQueueCore<E>
{
    public static final int ADD_CLOSED = 2;
    public static final int ADD_FROZEN = 1;
    public static final int ADD_SUCCESS = 0;
    public static final int CAPACITY_BITS = 30;
    public static final long CLOSED_MASK = 2305843009213693952L;
    public static final int CLOSED_SHIFT = 61;
    public static final Companion Companion;
    public static final long FROZEN_MASK = 1152921504606846976L;
    public static final int FROZEN_SHIFT = 60;
    public static final long HEAD_MASK = 1073741823L;
    public static final int HEAD_SHIFT = 0;
    public static final int INITIAL_CAPACITY = 8;
    public static final int MAX_CAPACITY_MASK = 1073741823;
    public static final int MIN_ADD_SPIN_CAPACITY = 1024;
    public static final Symbol REMOVE_FROZEN;
    public static final long TAIL_MASK = 1152921503533105152L;
    public static final int TAIL_SHIFT = 30;
    private static final AtomicReferenceFieldUpdater _next$FU;
    public static final /* synthetic */ AtomicLongFieldUpdater _state$FU$internal;
    private volatile Object _next;
    public volatile /* synthetic */ long _state$internal;
    public /* synthetic */ AtomicReferenceArray array$internal;
    private final int capacity;
    private final int mask;
    private final boolean singleConsumer;
    
    static {
        Companion = new Companion(null);
        REMOVE_FROZEN = new Symbol("REMOVE_FROZEN");
        _next$FU = AtomicReferenceFieldUpdater.newUpdater(LockFreeTaskQueueCore.class, Object.class, "_next");
        _state$FU$internal = AtomicLongFieldUpdater.newUpdater(LockFreeTaskQueueCore.class, "_state$internal");
    }
    
    public LockFreeTaskQueueCore(int n, final boolean singleConsumer) {
        this.capacity = n;
        this.singleConsumer = singleConsumer;
        n = this.capacity;
        this.mask = n - 1;
        this._next = null;
        this._state$internal = 0L;
        this.array$internal = new AtomicReferenceArray(n);
        n = this.mask;
        final int n2 = 0;
        if (n <= 1073741823) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (n == 0) {
            throw new IllegalStateException("Check failed.".toString());
        }
        n = n2;
        if ((this.capacity & this.mask) == 0x0) {
            n = 1;
        }
        if (n != 0) {
            return;
        }
        throw new IllegalStateException("Check failed.".toString());
    }
    
    public static final /* synthetic */ int access$getMask$p(final LockFreeTaskQueueCore lockFreeTaskQueueCore) {
        return lockFreeTaskQueueCore.mask;
    }
    
    public static final /* synthetic */ boolean access$getSingleConsumer$p(final LockFreeTaskQueueCore lockFreeTaskQueueCore) {
        return lockFreeTaskQueueCore.singleConsumer;
    }
    
    private final LockFreeTaskQueueCore<E> allocateNextCopy(final long n) {
        final LockFreeTaskQueueCore lockFreeTaskQueueCore = new LockFreeTaskQueueCore(this.capacity * 2, this.singleConsumer);
        final Companion companion = LockFreeTaskQueueCore.Companion;
        int n2 = (int)((0x3FFFFFFFL & n) >> 0);
        final int n3 = (int)((0xFFFFFFFC0000000L & n) >> 30);
        while (true) {
            final int mask = this.mask;
            if ((n2 & mask) == (n3 & mask)) {
                break;
            }
            Object value = this.array$internal.get(mask & n2);
            if (value == null) {
                value = new Placeholder(n2);
            }
            lockFreeTaskQueueCore.array$internal.set(lockFreeTaskQueueCore.mask & n2, value);
            ++n2;
        }
        lockFreeTaskQueueCore._state$internal = LockFreeTaskQueueCore.Companion.wo(n, 1152921504606846976L);
        return lockFreeTaskQueueCore;
    }
    
    private final LockFreeTaskQueueCore<E> allocateOrGetNextCopy(final long n) {
        LockFreeTaskQueueCore lockFreeTaskQueueCore;
        while (true) {
            lockFreeTaskQueueCore = (LockFreeTaskQueueCore)this._next;
            if (lockFreeTaskQueueCore != null) {
                break;
            }
            LockFreeTaskQueueCore._next$FU.compareAndSet(this, null, this.allocateNextCopy(n));
        }
        return lockFreeTaskQueueCore;
    }
    
    private final LockFreeTaskQueueCore<E> fillPlaceholder(final int n, final E newValue) {
        final Placeholder value = this.array$internal.get(this.mask & n);
        if (value instanceof Placeholder && value.index == n) {
            this.array$internal.set(n & this.mask, newValue);
            return this;
        }
        return null;
    }
    
    private final long markFrozen() {
        long state$internal;
        long n;
        do {
            state$internal = this._state$internal;
            if ((state$internal & 0x1000000000000000L) != 0x0L) {
                return state$internal;
            }
            n = (state$internal | 0x1000000000000000L);
        } while (!LockFreeTaskQueueCore._state$FU$internal.compareAndSet(this, state$internal, n));
        return n;
    }
    
    private final LockFreeTaskQueueCore<E> removeSlowPath(final int n, final int n2) {
        long state$internal;
        int n3;
        do {
            state$internal = this._state$internal;
            final Companion companion = LockFreeTaskQueueCore.Companion;
            boolean b = false;
            n3 = (int)((0x3FFFFFFFL & state$internal) >> 0);
            if (DebugKt.getASSERTIONS_ENABLED()) {
                if (n3 == n) {
                    b = true;
                }
                if (!b) {
                    throw new AssertionError();
                }
            }
            if ((0x1000000000000000L & state$internal) != 0x0L) {
                return this.next();
            }
        } while (!LockFreeTaskQueueCore._state$FU$internal.compareAndSet(this, state$internal, LockFreeTaskQueueCore.Companion.updateHead(state$internal, n2)));
        this.array$internal.set(this.mask & n3, null);
        return null;
    }
    
    public final int addLast(final E newValue) {
        Intrinsics.checkParameterIsNotNull(newValue, "element");
        while (true) {
            final long state$internal = this._state$internal;
            if ((0x3000000000000000L & state$internal) != 0x0L) {
                return LockFreeTaskQueueCore.Companion.addFailReason(state$internal);
            }
            final Companion companion = LockFreeTaskQueueCore.Companion;
            final int n = (int)((0x3FFFFFFFL & state$internal) >> 0);
            final int n2 = (int)((0xFFFFFFFC0000000L & state$internal) >> 30);
            final int mask = this.mask;
            if ((n2 + 2 & mask) == (n & mask)) {
                return 1;
            }
            if (!this.singleConsumer && this.array$internal.get(n2 & mask) != null) {
                final int capacity = this.capacity;
                if (capacity < 1024 || (n2 - n & 0x3FFFFFFF) > capacity >> 1) {
                    return 1;
                }
                continue;
            }
            else {
                if (LockFreeTaskQueueCore._state$FU$internal.compareAndSet(this, state$internal, LockFreeTaskQueueCore.Companion.updateTail(state$internal, n2 + 1 & 0x3FFFFFFF))) {
                    this.array$internal.set(n2 & mask, newValue);
                    LockFreeTaskQueueCore<Object> fillPlaceholder = (LockFreeTaskQueueCore<Object>)this;
                    while ((fillPlaceholder._state$internal & 0x1000000000000000L) != 0x0L) {
                        fillPlaceholder = fillPlaceholder.next().fillPlaceholder(n2, newValue);
                        if (fillPlaceholder != null) {
                            continue;
                        }
                        return 0;
                    }
                    return 0;
                }
                continue;
            }
        }
    }
    
    public final boolean close() {
        long state$internal;
        do {
            state$internal = this._state$internal;
            if ((state$internal & 0x2000000000000000L) != 0x0L) {
                return true;
            }
            if ((0x1000000000000000L & state$internal) != 0x0L) {
                return false;
            }
        } while (!LockFreeTaskQueueCore._state$FU$internal.compareAndSet(this, state$internal, state$internal | 0x2000000000000000L));
        return true;
    }
    
    public final int getSize() {
        final Companion companion = LockFreeTaskQueueCore.Companion;
        final long state$internal = this._state$internal;
        return (int)((state$internal & 0xFFFFFFFC0000000L) >> 30) - (int)((0x3FFFFFFFL & state$internal) >> 0) & 0x3FFFFFFF;
    }
    
    public final boolean isClosed() {
        return (this._state$internal & 0x2000000000000000L) != 0x0L;
    }
    
    public final boolean isEmpty() {
        final Companion companion = LockFreeTaskQueueCore.Companion;
        final long state$internal = this._state$internal;
        boolean b = false;
        if ((int)((0x3FFFFFFFL & state$internal) >> 0) == (int)((state$internal & 0xFFFFFFFC0000000L) >> 30)) {
            b = true;
        }
        return b;
    }
    
    public final <R> List<R> map(final Function1<? super E, ? extends R> function1) {
        Intrinsics.checkParameterIsNotNull(function1, "transform");
        final ArrayList<R> list = new ArrayList<R>(this.capacity);
        final Companion companion = LockFreeTaskQueueCore.Companion;
        final long state$internal = this._state$internal;
        int n = (int)((0x3FFFFFFFL & state$internal) >> 0);
        final int n2 = (int)((state$internal & 0xFFFFFFFC0000000L) >> 30);
        while (true) {
            final int mask = this.mask;
            if ((n & mask) == (n2 & mask)) {
                break;
            }
            final Object value = this.array$internal.get(mask & n);
            if (value != null && !(value instanceof Placeholder)) {
                list.add((R)function1.invoke((Object)value));
            }
            ++n;
        }
        return list;
    }
    
    public final LockFreeTaskQueueCore<E> next() {
        return this.allocateOrGetNextCopy(this.markFrozen());
    }
    
    public final Object removeFirstOrNull() {
        Object value = null;
    Label_0173:
        while (true) {
            final long state$internal = this._state$internal;
            if ((0x1000000000000000L & state$internal) != 0x0L) {
                return LockFreeTaskQueueCore.REMOVE_FROZEN;
            }
            final Companion companion = LockFreeTaskQueueCore.Companion;
            final int n = (int)((0x3FFFFFFFL & state$internal) >> 0);
            if (((int)((0xFFFFFFFC0000000L & state$internal) >> 30) & access$getMask$p(this)) == (access$getMask$p(this) & n)) {
                return null;
            }
            value = this.array$internal.get(access$getMask$p(this) & n);
            if (value == null) {
                if (access$getSingleConsumer$p(this)) {
                    return null;
                }
                continue;
            }
            else {
                if (value instanceof Placeholder) {
                    return null;
                }
                final int n2 = n + 1 & 0x3FFFFFFF;
                if (LockFreeTaskQueueCore._state$FU$internal.compareAndSet(this, state$internal, LockFreeTaskQueueCore.Companion.updateHead(state$internal, n2))) {
                    this.array$internal.set(access$getMask$p(this) & n, null);
                    break;
                }
                if (!access$getSingleConsumer$p(this)) {
                    continue;
                }
                LockFreeTaskQueueCore access$removeSlowPath = this;
                while (true) {
                    access$removeSlowPath = access$removeSlowPath.removeSlowPath(n, n2);
                    if (access$removeSlowPath != null) {
                        continue;
                    }
                    break Label_0173;
                }
            }
        }
        return value;
    }
    
    public final Object removeFirstOrNullIf(final Function1<? super E, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        while (true) {
            final long state$internal = this._state$internal;
            if ((0x1000000000000000L & state$internal) != 0x0L) {
                return LockFreeTaskQueueCore.REMOVE_FROZEN;
            }
            final Companion companion = LockFreeTaskQueueCore.Companion;
            final int n = (int)((0x3FFFFFFFL & state$internal) >> 0);
            if (((int)((0xFFFFFFFC0000000L & state$internal) >> 30) & access$getMask$p(this)) == (access$getMask$p(this) & n)) {
                return null;
            }
            final Object value = this.array$internal.get(access$getMask$p(this) & n);
            if (value == null) {
                if (access$getSingleConsumer$p(this)) {
                    return null;
                }
                continue;
            }
            else {
                if (value instanceof Placeholder) {
                    return null;
                }
                if (!function1.invoke((Object)value)) {
                    return null;
                }
                final int n2 = n + 1 & 0x3FFFFFFF;
                if (LockFreeTaskQueueCore._state$FU$internal.compareAndSet(this, state$internal, LockFreeTaskQueueCore.Companion.updateHead(state$internal, n2))) {
                    this.array$internal.set(n & access$getMask$p(this), null);
                    return value;
                }
                if (!access$getSingleConsumer$p(this)) {
                    continue;
                }
                LockFreeTaskQueueCore access$removeSlowPath = this;
                do {
                    access$removeSlowPath = access$removeSlowPath.removeSlowPath(n, n2);
                } while (access$removeSlowPath != null);
                return value;
            }
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\n\u0010\u0016\u001a\u00020\u0004*\u00020\tJ\u0012\u0010\u0017\u001a\u00020\t*\u00020\t2\u0006\u0010\u0018\u001a\u00020\u0004J\u0012\u0010\u0019\u001a\u00020\t*\u00020\t2\u0006\u0010\u001a\u001a\u00020\u0004JP\u0010\u001b\u001a\u0002H\u001c\"\u0004\b\u0001\u0010\u001c*\u00020\t26\u0010\u001d\u001a2\u0012\u0013\u0012\u00110\u0004¢\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(!\u0012\u0013\u0012\u00110\u0004¢\u0006\f\b\u001f\u0012\b\b \u0012\u0004\b\b(\"\u0012\u0004\u0012\u0002H\u001c0\u001eH\u0086\b¢\u0006\u0002\u0010#J\u0015\u0010$\u001a\u00020\t*\u00020\t2\u0006\u0010%\u001a\u00020\tH\u0086\u0004R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\tX\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\tX\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u00020\u00138\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\tX\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006&" }, d2 = { "Lkotlinx/coroutines/internal/LockFreeTaskQueueCore$Companion;", "", "()V", "ADD_CLOSED", "", "ADD_FROZEN", "ADD_SUCCESS", "CAPACITY_BITS", "CLOSED_MASK", "", "CLOSED_SHIFT", "FROZEN_MASK", "FROZEN_SHIFT", "HEAD_MASK", "HEAD_SHIFT", "INITIAL_CAPACITY", "MAX_CAPACITY_MASK", "MIN_ADD_SPIN_CAPACITY", "REMOVE_FROZEN", "Lkotlinx/coroutines/internal/Symbol;", "TAIL_MASK", "TAIL_SHIFT", "addFailReason", "updateHead", "newHead", "updateTail", "newTail", "withState", "T", "block", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "head", "tail", "(JLkotlin/jvm/functions/Function2;)Ljava/lang/Object;", "wo", "other", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    public static final class Companion
    {
        private Companion() {
        }
        
        public final int addFailReason(final long n) {
            if ((n & 0x2000000000000000L) != 0x0L) {
                return 2;
            }
            return 1;
        }
        
        public final long updateHead(final long n, final int n2) {
            return this.wo(n, 1073741823L) | (long)n2 << 0;
        }
        
        public final long updateTail(final long n, final int n2) {
            return this.wo(n, 1152921503533105152L) | (long)n2 << 30;
        }
        
        public final <T> T withState(final long n, final Function2<? super Integer, ? super Integer, ? extends T> function2) {
            Intrinsics.checkParameterIsNotNull(function2, "block");
            return (T)function2.invoke((int)((0x3FFFFFFFL & n) >> 0), (int)((n & 0xFFFFFFFC0000000L) >> 30));
        }
        
        public final long wo(final long n, final long n2) {
            return n & n2;
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0005" }, d2 = { "Lkotlinx/coroutines/internal/LockFreeTaskQueueCore$Placeholder;", "", "index", "", "(I)V", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    public static final class Placeholder
    {
        public final int index;
        
        public Placeholder(final int index) {
            this.index = index;
        }
    }
}
