// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.internal;

import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0016\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J-\u0010\u0006\u001a\u00020\u0007\"\u000e\b\u0000\u0010\b\u0018\u0001*\u00060\u0001j\u0002`\t2\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u0002H\b\u0012\u0004\u0012\u00020\u00070\u000bH\u0086\bJ\u0006\u0010\f\u001a\u00020\u0004J\r\u0010\r\u001a\u00020\u0007H\u0000¢\u0006\u0002\b\u000eR\u0011\u0010\u0003\u001a\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0005¨\u0006\u000f" }, d2 = { "Lkotlinx/coroutines/internal/LockFreeLinkedListHead;", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "()V", "isEmpty", "", "()Z", "forEach", "", "T", "Lkotlinx/coroutines/internal/Node;", "block", "Lkotlin/Function1;", "remove", "validate", "validate$kotlinx_coroutines_core", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public class LockFreeLinkedListHead extends LockFreeLinkedListNode
{
    public final boolean isEmpty() {
        return this.getNext() == this;
    }
    
    @Override
    public final boolean remove() {
        throw new UnsupportedOperationException();
    }
    
    public final void validate$kotlinx_coroutines_core() {
        LockFreeLinkedListNode lockFreeLinkedListNode = this;
        final Object next = this.getNext();
        if (next == null) {
            throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
        }
        LockFreeLinkedListNode nextNode;
        for (LockFreeLinkedListNode lockFreeLinkedListNode2 = (LockFreeLinkedListNode)next; Intrinsics.areEqual(lockFreeLinkedListNode2, this) ^ true; lockFreeLinkedListNode2 = nextNode) {
            nextNode = lockFreeLinkedListNode2.getNextNode();
            lockFreeLinkedListNode2.validateNode$kotlinx_coroutines_core(lockFreeLinkedListNode, nextNode);
            lockFreeLinkedListNode = lockFreeLinkedListNode2;
        }
        final Object next2 = this.getNext();
        if (next2 != null) {
            this.validateNode$kotlinx_coroutines_core(lockFreeLinkedListNode, (LockFreeLinkedListNode)next2);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }
}
