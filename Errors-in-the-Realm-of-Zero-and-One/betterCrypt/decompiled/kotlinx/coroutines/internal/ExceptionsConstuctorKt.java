// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.internal;

import java.util.Iterator;
import java.util.Comparator;
import kotlin.Unit;
import kotlinx.coroutines.CopyableThrowable;
import kotlin.ResultKt;
import kotlin.Result;
import kotlin.jvm.JvmClassMappingKt;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import kotlin.jvm.internal.Intrinsics;
import java.lang.reflect.Constructor;
import kotlin.jvm.functions.Function1;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000.\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\f\u001a*\u0010\n\u001a\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0018\u00010\u0006j\u0004\u0018\u0001`\u00072\n\u0010\u000b\u001a\u0006\u0012\u0002\b\u00030\fH\u0002\u001a1\u0010\r\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u0006j\u0002`\u00072\u0014\b\u0004\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0006H\u0082\b\u001a!\u0010\u000f\u001a\u0004\u0018\u0001H\u0010\"\b\b\u0000\u0010\u0010*\u00020\u00052\u0006\u0010\u0011\u001a\u0002H\u0010H\u0000¢\u0006\u0002\u0010\u0012\u001a\u001b\u0010\u0013\u001a\u00020\t*\u0006\u0012\u0002\b\u00030\u00042\b\b\u0002\u0010\u0014\u001a\u00020\tH\u0082\u0010\u001a\u0018\u0010\u0015\u001a\u00020\t*\u0006\u0012\u0002\b\u00030\u00042\u0006\u0010\u0016\u001a\u00020\tH\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000\"4\u0010\u0002\u001a(\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\u00050\u0004\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u0006j\u0002`\u00070\u0003X\u0082\u0004¢\u0006\u0002\n\u0000\"\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000*(\b\u0002\u0010\u0017\"\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u00062\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u0006¨\u0006\u0018" }, d2 = { "cacheLock", "Ljava/util/concurrent/locks/ReentrantReadWriteLock;", "exceptionCtors", "Ljava/util/WeakHashMap;", "Ljava/lang/Class;", "", "Lkotlin/Function1;", "Lkotlinx/coroutines/internal/Ctor;", "throwableFields", "", "createConstructor", "constructor", "Ljava/lang/reflect/Constructor;", "safeCtor", "block", "tryCopyException", "E", "exception", "(Ljava/lang/Throwable;)Ljava/lang/Throwable;", "fieldsCount", "accumulator", "fieldsCountOrDefault", "defaultValue", "Ctor", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class ExceptionsConstuctorKt
{
    private static final ReentrantReadWriteLock cacheLock;
    private static final WeakHashMap<Class<? extends Throwable>, Function1<Throwable, Throwable>> exceptionCtors;
    private static final int throwableFields;
    
    static {
        throwableFields = fieldsCountOrDefault(Throwable.class, -1);
        cacheLock = new ReentrantReadWriteLock();
        exceptionCtors = new WeakHashMap<Class<? extends Throwable>, Function1<Throwable, Throwable>>();
    }
    
    private static final Function1<Throwable, Throwable> createConstructor(final Constructor<?> constructor) {
        final Class[] parameterTypes = constructor.getParameterTypes();
        final int length = parameterTypes.length;
        final Function1<Throwable, Throwable> function1 = null;
        Function1<Throwable, Throwable> function2;
        if (length != 0) {
            if (length != 1) {
                if (length != 2) {
                    return null;
                }
                function2 = function1;
                if (Intrinsics.areEqual(parameterTypes[0], String.class)) {
                    function2 = function1;
                    if (Intrinsics.areEqual(parameterTypes[1], Throwable.class)) {
                        return (Function1<Throwable, Throwable>)new ExceptionsConstuctorKt$createConstructor$$inlined$safeCtor.ExceptionsConstuctorKt$createConstructor$$inlined$safeCtor$1((Constructor)constructor);
                    }
                }
            }
            else {
                final Class clazz = parameterTypes[0];
                if (Intrinsics.areEqual(clazz, Throwable.class)) {
                    return (Function1<Throwable, Throwable>)new ExceptionsConstuctorKt$createConstructor$$inlined$safeCtor.ExceptionsConstuctorKt$createConstructor$$inlined$safeCtor$2((Constructor)constructor);
                }
                function2 = function1;
                if (Intrinsics.areEqual(clazz, String.class)) {
                    return (Function1<Throwable, Throwable>)new ExceptionsConstuctorKt$createConstructor$$inlined$safeCtor.ExceptionsConstuctorKt$createConstructor$$inlined$safeCtor$3((Constructor)constructor);
                }
            }
        }
        else {
            function2 = (Function1<Throwable, Throwable>)new ExceptionsConstuctorKt$createConstructor$$inlined$safeCtor.ExceptionsConstuctorKt$createConstructor$$inlined$safeCtor$4((Constructor)constructor);
        }
        return function2;
    }
    
    private static final int fieldsCount(Class<?> superclass, int n) {
        do {
            final Field[] declaredFields = superclass.getDeclaredFields();
            Intrinsics.checkExpressionValueIsNotNull(declaredFields, "declaredFields");
            final int length = declaredFields.length;
            int i = 0;
            int n2 = 0;
            while (i < length) {
                final Field field = declaredFields[i];
                Intrinsics.checkExpressionValueIsNotNull(field, "it");
                int n3 = n2;
                if (Modifier.isStatic(field.getModifiers()) ^ true) {
                    n3 = n2 + 1;
                }
                ++i;
                n2 = n3;
            }
            n += n2;
            superclass = superclass.getSuperclass();
        } while (superclass != null);
        return n;
    }
    
    static /* synthetic */ int fieldsCount$default(final Class clazz, int n, final int n2, final Object o) {
        if ((n2 & 0x1) != 0x0) {
            n = 0;
        }
        return fieldsCount(clazz, n);
    }
    
    private static final int fieldsCountOrDefault(final Class<?> clazz, final int i) {
        JvmClassMappingKt.getKotlinClass(clazz);
        Object constructor-impl = null;
        try {
            final Result.Companion companion = Result.Companion;
            Result.constructor-impl(fieldsCount$default(clazz, 0, 1, null));
        }
        finally {
            final Result.Companion companion2 = Result.Companion;
            final Throwable t;
            constructor-impl = Result.constructor-impl(ResultKt.createFailure(t));
        }
        Object value = constructor-impl;
        if (Result.isFailure-impl(constructor-impl)) {
            value = i;
        }
        return ((Number)value).intValue();
    }
    
    private static final Function1<Throwable, Throwable> safeCtor(final Function1<? super Throwable, ? extends Throwable> function1) {
        return (Function1<Throwable, Throwable>)new ExceptionsConstuctorKt$safeCtor.ExceptionsConstuctorKt$safeCtor$1((Function1)function1);
    }
    
    public static final <E extends Throwable> E tryCopyException(final E e) {
        Intrinsics.checkParameterIsNotNull(e, "exception");
        final boolean b = e instanceof CopyableThrowable;
        final Throwable t = null;
        if (b) {
            Object constructor-impl = null;
            try {
                final Result.Companion companion = Result.Companion;
                Result.constructor-impl(((CopyableThrowable)e).createCopy());
            }
            finally {
                final Result.Companion companion2 = Result.Companion;
                final Throwable t2;
                constructor-impl = Result.constructor-impl(ResultKt.createFailure(t2));
            }
            Object o = constructor-impl;
            if (Result.isFailure-impl(constructor-impl)) {
                o = null;
            }
            return (E)o;
        }
        Object o2 = ExceptionsConstuctorKt.cacheLock.readLock();
        ((ReentrantReadWriteLock.ReadLock)o2).lock();
        try {
            final Function1<Throwable, Throwable> function1 = ExceptionsConstuctorKt.exceptionCtors.get(e.getClass());
            ((ReentrantReadWriteLock.ReadLock)o2).unlock();
            if (function1 != null) {
                return (E)function1.invoke(e);
            }
            final int throwableFields = ExceptionsConstuctorKt.throwableFields;
            o2 = e.getClass();
            final int n = 0;
            final int n2 = 0;
            final int n3 = 0;
            if (throwableFields != fieldsCountOrDefault((Class<?>)o2, 0)) {
                final ReentrantReadWriteLock cacheLock = ExceptionsConstuctorKt.cacheLock;
                o2 = cacheLock.readLock();
                int readHoldCount;
                if (cacheLock.getWriteHoldCount() == 0) {
                    readHoldCount = cacheLock.getReadHoldCount();
                }
                else {
                    readHoldCount = 0;
                }
                for (int i = 0; i < readHoldCount; ++i) {
                    ((ReentrantReadWriteLock.ReadLock)o2).unlock();
                }
                final ReentrantReadWriteLock.WriteLock writeLock = cacheLock.writeLock();
                writeLock.lock();
                try {
                    ExceptionsConstuctorKt.exceptionCtors.put(e.getClass(), (Function1<Throwable, Throwable>)ExceptionsConstuctorKt$tryCopyException$4.ExceptionsConstuctorKt$tryCopyException$4$1.INSTANCE);
                    final Unit instance = Unit.INSTANCE;
                    return null;
                }
                finally {
                    for (int j = n; j < readHoldCount; ++j) {
                        ((ReentrantReadWriteLock.ReadLock)o2).lock();
                    }
                    writeLock.unlock();
                }
            }
            o2 = null;
            final Constructor<?>[] constructors = e.getClass().getConstructors();
            Intrinsics.checkExpressionValueIsNotNull(constructors, "exception.javaClass.constructors");
            final Iterator<Constructor<?>> iterator = ArraysKt___ArraysKt.sortedWith(constructors, (Comparator<? super Constructor<?>>)new ExceptionsConstuctorKt$tryCopyException$$inlined$sortedByDescending.ExceptionsConstuctorKt$tryCopyException$$inlined$sortedByDescending$1()).iterator();
            while (iterator.hasNext()) {
                o2 = iterator.next();
                Intrinsics.checkExpressionValueIsNotNull(o2, "constructor");
                final Function1<Throwable, Throwable> constructor = createConstructor((Constructor<?>)o2);
                if ((o2 = constructor) != null) {
                    o2 = constructor;
                    break;
                }
            }
            final ReentrantReadWriteLock cacheLock2 = ExceptionsConstuctorKt.cacheLock;
            final ReentrantReadWriteLock.ReadLock lock = cacheLock2.readLock();
            int readHoldCount2;
            if (cacheLock2.getWriteHoldCount() == 0) {
                readHoldCount2 = cacheLock2.getReadHoldCount();
            }
            else {
                readHoldCount2 = 0;
            }
            for (int k = 0; k < readHoldCount2; ++k) {
                lock.unlock();
            }
            final ReentrantReadWriteLock.WriteLock writeLock2 = cacheLock2.writeLock();
            writeLock2.lock();
            try {
                final WeakHashMap<Class<? extends Throwable>, Function1<Throwable, Throwable>> weakHashMap = ExceptionsConstuctorKt.exceptionCtors;
                final Class<? extends Throwable> class1 = e.getClass();
                Object o3;
                if (o2 != null) {
                    o3 = o2;
                }
                else {
                    o3 = ExceptionsConstuctorKt$tryCopyException$5.ExceptionsConstuctorKt$tryCopyException$5$1.INSTANCE;
                }
                weakHashMap.put(class1, (Function1<Throwable, Throwable>)o3);
                final Unit instance2 = Unit.INSTANCE;
                for (int l = n2; l < readHoldCount2; ++l) {
                    lock.lock();
                }
                writeLock2.unlock();
                Throwable t3 = t;
                if (o2 != null) {
                    t3 = ((Function1<E, E>)o2).invoke(e);
                }
                return (E)t3;
            }
            finally {
                for (int n4 = n3; n4 < readHoldCount2; ++n4) {
                    lock.lock();
                }
                writeLock2.unlock();
            }
        }
        finally {
            ((ReentrantReadWriteLock.ReadLock)o2).unlock();
        }
    }
}
