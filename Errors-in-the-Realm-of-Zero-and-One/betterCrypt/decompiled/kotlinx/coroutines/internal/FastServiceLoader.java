// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.internal;

import java.util.zip.ZipFile;
import java.util.Iterator;
import java.util.Enumeration;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ServiceLoader;
import kotlin.jvm.internal.InlineMarker;
import kotlin.jvm.functions.Function1;
import kotlin.TypeCastException;
import java.util.Set;
import java.util.LinkedHashSet;
import java.io.Closeable;
import kotlin.io.CloseableKt;
import java.io.Reader;
import java.util.zip.ZipEntry;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.jar.JarFile;
import kotlin.jvm.internal.Intrinsics;
import java.util.List;
import java.net.URL;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J1\u0010\u0005\u001a\u0002H\u0006\"\u0004\b\u0000\u0010\u00062\u0006\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\u00060\u000bH\u0002¢\u0006\u0002\u0010\fJ/\u0010\r\u001a\b\u0012\u0004\u0012\u0002H\u00060\u000e\"\u0004\b\u0000\u0010\u00062\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\u00060\u000b2\u0006\u0010\b\u001a\u00020\tH\u0000¢\u0006\u0002\b\u000fJ/\u0010\u0010\u001a\b\u0012\u0004\u0012\u0002H\u00060\u000e\"\u0004\b\u0000\u0010\u00062\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\u00060\u000b2\u0006\u0010\b\u001a\u00020\tH\u0000¢\u0006\u0002\b\u0011J\u0016\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00040\u000e2\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0016\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00040\u000e2\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J,\u0010\u0018\u001a\u0002H\u0019\"\u0004\b\u0000\u0010\u0019*\u00020\u001a2\u0012\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u0002H\u00190\u001cH\u0082\b¢\u0006\u0002\u0010\u001dR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u001e" }, d2 = { "Lkotlinx/coroutines/internal/FastServiceLoader;", "", "()V", "PREFIX", "", "getProviderInstance", "S", "name", "loader", "Ljava/lang/ClassLoader;", "service", "Ljava/lang/Class;", "(Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/Object;", "load", "", "load$kotlinx_coroutines_core", "loadProviders", "loadProviders$kotlinx_coroutines_core", "parse", "url", "Ljava/net/URL;", "parseFile", "r", "Ljava/io/BufferedReader;", "use", "R", "Ljava/util/jar/JarFile;", "block", "Lkotlin/Function1;", "(Ljava/util/jar/JarFile;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class FastServiceLoader
{
    public static final FastServiceLoader INSTANCE;
    private static final String PREFIX = "META-INF/services/";
    
    static {
        INSTANCE = new FastServiceLoader();
    }
    
    private FastServiceLoader() {
    }
    
    private final <S> S getProviderInstance(final String name, final ClassLoader loader, final Class<S> obj) {
        final Class<?> forName = Class.forName(name, false, loader);
        if (obj.isAssignableFrom(forName)) {
            return obj.cast(forName.getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]));
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected service of class ");
        sb.append(obj);
        sb.append(", but found ");
        sb.append(forName);
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    private final List<String> parse(URL url) {
        final String string = url.toString();
        Intrinsics.checkExpressionValueIsNotNull(string, "url.toString()");
        if (StringsKt__StringsJVMKt.startsWith$default(string, "jar", false, 2, null)) {
            final String substringBefore$default = StringsKt__StringsKt.substringBefore$default(StringsKt__StringsKt.substringAfter$default(string, "jar:file:", null, 2, null), '!', null, 2, null);
            final String substringAfter$default = StringsKt__StringsKt.substringAfter$default(string, "!/", null, 2, null);
            url = (URL)new JarFile(substringBefore$default, false);
            final Throwable t = null;
            try {
                final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(((JarFile)url).getInputStream(new ZipEntry(substringAfter$default)), "UTF-8"));
                final Throwable t2 = null;
                try {
                    final List<String> file = FastServiceLoader.INSTANCE.parseFile(bufferedReader);
                    CloseableKt.closeFinally(bufferedReader, t2);
                    try {
                        ((ZipFile)url).close();
                        return file;
                    }
                    finally {}
                }
                finally {
                    try {}
                    finally {
                        final Throwable t3;
                        CloseableKt.closeFinally(bufferedReader, t3);
                    }
                }
            }
            finally {
                try {}
                finally {
                    try {
                        ((ZipFile)url).close();
                    }
                    finally {
                        final Throwable t4;
                        final Throwable t5;
                        ExceptionsKt__ExceptionsKt.addSuppressed(t4, t5);
                    }
                }
            }
        }
        url = (URL)new BufferedReader(new InputStreamReader(url.openStream()));
        final Throwable t6 = null;
        try {
            final List<String> file2 = FastServiceLoader.INSTANCE.parseFile((BufferedReader)url);
            CloseableKt.closeFinally((Closeable)url, t6);
            return file2;
        }
        finally {
            try {}
            finally {
                final Throwable t7;
                CloseableKt.closeFinally((Closeable)url, t7);
            }
        }
    }
    
    private final List<String> parseFile(final BufferedReader bufferedReader) {
        final LinkedHashSet<String> set = new LinkedHashSet<String>();
    Label_0012:
        while (true) {
            final String line = bufferedReader.readLine();
            if (line == null) {
                return (List<String>)CollectionsKt___CollectionsKt.toList((Iterable<?>)set);
            }
            final String substringBefore$default = StringsKt__StringsKt.substringBefore$default(line, "#", null, 2, null);
            if (substringBefore$default != null) {
                final String string = StringsKt__StringsKt.trim((CharSequence)substringBefore$default).toString();
                final String s = string;
                final int n = 0;
                int i = 0;
                while (true) {
                    while (i < s.length()) {
                        final char char1 = s.charAt(i);
                        if (char1 != '.' && !Character.isJavaIdentifierPart(char1)) {
                            final boolean b = false;
                            if (!b) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Illegal service provider class name: ");
                                sb.append(string);
                                throw new IllegalArgumentException(sb.toString().toString());
                            }
                            int n2 = n;
                            if (s.length() > 0) {
                                n2 = 1;
                            }
                            if (n2 != 0) {
                                set.add(string);
                                continue Label_0012;
                            }
                            continue Label_0012;
                        }
                        else {
                            ++i;
                        }
                    }
                    final boolean b = true;
                    continue;
                }
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }
    
    private final <R> R use(final JarFile jarFile, final Function1<? super JarFile, ? extends R> function1) {
        final Throwable t = null;
        try {
            final R invoke = function1.invoke(jarFile);
            InlineMarker.finallyStart(1);
            try {
                jarFile.close();
                InlineMarker.finallyEnd(1);
                return invoke;
            }
            finally {}
        }
        finally {
            try {}
            finally {
                InlineMarker.finallyStart(1);
                try {
                    jarFile.close();
                    InlineMarker.finallyEnd(1);
                }
                finally {
                    final Throwable t2;
                    ExceptionsKt__ExceptionsKt.addSuppressed((Throwable)function1, t2);
                }
            }
        }
    }
    
    public final <S> List<S> load$kotlinx_coroutines_core(Class<S> load, final ClassLoader loader) {
        Intrinsics.checkParameterIsNotNull(load, "service");
        Intrinsics.checkParameterIsNotNull(loader, "loader");
        while (true) {
            try {
                return (List<S>)this.loadProviders$kotlinx_coroutines_core((Class<S>)load, loader);
                load = ServiceLoader.load((Class<S>)load, loader);
                Intrinsics.checkExpressionValueIsNotNull(load, "ServiceLoader.load(service, loader)");
                return (List<S>)CollectionsKt___CollectionsKt.toList((Iterable<?>)load);
            }
            finally {
                continue;
            }
            break;
        }
    }
    
    public final <S> List<S> loadProviders$kotlinx_coroutines_core(final Class<S> clazz, final ClassLoader classLoader) {
        Intrinsics.checkParameterIsNotNull(clazz, "service");
        Intrinsics.checkParameterIsNotNull(classLoader, "loader");
        final StringBuilder sb = new StringBuilder();
        sb.append("META-INF/services/");
        sb.append(clazz.getName());
        final Enumeration<URL> resources = classLoader.getResources(sb.toString());
        Intrinsics.checkExpressionValueIsNotNull(resources, "urls");
        final ArrayList<URL> list = Collections.list(resources);
        Intrinsics.checkExpressionValueIsNotNull(list, "java.util.Collections.list(this)");
        final ArrayList<URL> list2 = list;
        final ArrayList<Object> list3 = new ArrayList<Object>();
        for (final URL url : list2) {
            final FastServiceLoader instance = FastServiceLoader.INSTANCE;
            Intrinsics.checkExpressionValueIsNotNull(url, "it");
            CollectionsKt__MutableCollectionsKt.addAll((Collection<? super Object>)list3, (Iterable<?>)instance.parse(url));
        }
        final Set<Object> set = CollectionsKt___CollectionsKt.toSet((Iterable<?>)list3);
        if (((Set<? extends T>)set).isEmpty() ^ true) {
            final Set<? extends T> set2 = (Set<? extends T>)set;
            final ArrayList<S> list4 = new ArrayList<S>(CollectionsKt__IterablesKt.collectionSizeOrDefault((Iterable<?>)set2, 10));
            final Iterator<String> iterator2 = set2.iterator();
            while (iterator2.hasNext()) {
                list4.add(FastServiceLoader.INSTANCE.getProviderInstance(iterator2.next(), classLoader, clazz));
            }
            return list4;
        }
        throw new IllegalArgumentException("No providers were loaded with FastServiceLoader".toString());
    }
}
