// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.internal;

import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "kotlinx/coroutines/internal/SystemPropsKt__SystemPropsKt", "kotlinx/coroutines/internal/SystemPropsKt__SystemProps_commonKt" }, k = 4, mv = { 1, 1, 15 })
public final class SystemPropsKt
{
    public static final int getAVAILABLE_PROCESSORS() {
        return SystemPropsKt__SystemPropsKt.getAVAILABLE_PROCESSORS();
    }
    
    public static final int systemProp(final String s, final int n, final int n2, final int n3) {
        return SystemPropsKt__SystemProps_commonKt.systemProp(s, n, n2, n3);
    }
    
    public static final long systemProp(final String s, final long n, final long n2, final long n3) {
        return SystemPropsKt__SystemProps_commonKt.systemProp(s, n, n2, n3);
    }
    
    public static final String systemProp(final String s) {
        return SystemPropsKt__SystemPropsKt.systemProp(s);
    }
    
    public static final boolean systemProp(final String s, final boolean b) {
        return SystemPropsKt__SystemProps_commonKt.systemProp(s, b);
    }
}
