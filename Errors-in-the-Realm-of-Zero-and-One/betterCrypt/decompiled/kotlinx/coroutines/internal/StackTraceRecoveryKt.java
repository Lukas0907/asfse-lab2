// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.internal;

import kotlin.jvm.internal.InlineMarker;
import kotlinx.coroutines.DebugKt;
import kotlin.coroutines.Continuation;
import java.util.Iterator;
import kotlin.TypeCastException;
import java.util.ArrayDeque;
import kotlin.TuplesKt;
import kotlin.Pair;
import kotlin.coroutines.jvm.internal.CoroutineStackFrame;
import kotlin.ResultKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Result;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000f\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0010\u0001\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\u001a\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0001H\u0007\u001a9\u0010\t\u001a\u0002H\n\"\b\b\u0000\u0010\n*\u00020\u000b2\u0006\u0010\f\u001a\u0002H\n2\u0006\u0010\r\u001a\u0002H\n2\u0010\u0010\u000e\u001a\f\u0012\b\u0012\u00060\u0007j\u0002`\u00100\u000fH\u0002¢\u0006\u0002\u0010\u0011\u001a\u001e\u0010\u0012\u001a\f\u0012\b\u0012\u00060\u0007j\u0002`\u00100\u000f2\n\u0010\u0013\u001a\u00060\u0014j\u0002`\u0015H\u0002\u001a1\u0010\u0016\u001a\u00020\u00172\u0010\u0010\u0018\u001a\f\u0012\b\u0012\u00060\u0007j\u0002`\u00100\u00192\u0010\u0010\r\u001a\f\u0012\b\u0012\u00060\u0007j\u0002`\u00100\u000fH\u0002¢\u0006\u0002\u0010\u001a\u001a\u0019\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u000bH\u0080H\u00f8\u0001\u0000¢\u0006\u0002\u0010\u001e\u001a+\u0010\u001f\u001a\u0002H\n\"\b\b\u0000\u0010\n*\u00020\u000b2\u0006\u0010\u001d\u001a\u0002H\n2\n\u0010\u0013\u001a\u00060\u0014j\u0002`\u0015H\u0002¢\u0006\u0002\u0010 \u001a\u001f\u0010!\u001a\u0002H\n\"\b\b\u0000\u0010\n*\u00020\u000b2\u0006\u0010\u001d\u001a\u0002H\nH\u0000¢\u0006\u0002\u0010\"\u001a+\u0010!\u001a\u0002H\n\"\b\b\u0000\u0010\n*\u00020\u000b2\u0006\u0010\u001d\u001a\u0002H\n2\n\u0010\u0013\u001a\u0006\u0012\u0002\b\u00030#H\u0000¢\u0006\u0002\u0010$\u001a\u001f\u0010%\u001a\u0002H\n\"\b\b\u0000\u0010\n*\u00020\u000b2\u0006\u0010\u001d\u001a\u0002H\nH\u0000¢\u0006\u0002\u0010\"\u001a1\u0010&\u001a\u0018\u0012\u0004\u0012\u0002H\n\u0012\u000e\u0012\f\u0012\b\u0012\u00060\u0007j\u0002`\u00100\u00190'\"\b\b\u0000\u0010\n*\u00020\u000b*\u0002H\nH\u0002¢\u0006\u0002\u0010(\u001a\u001c\u0010)\u001a\u00020**\u00060\u0007j\u0002`\u00102\n\u0010+\u001a\u00060\u0007j\u0002`\u0010H\u0002\u001a#\u0010,\u001a\u00020-*\f\u0012\b\u0012\u00060\u0007j\u0002`\u00100\u00192\u0006\u0010.\u001a\u00020\u0001H\u0002¢\u0006\u0002\u0010/\u001a\u0010\u00100\u001a\u00020**\u00060\u0007j\u0002`\u0010H\u0000\u001a\u001b\u00101\u001a\u0002H\n\"\b\b\u0000\u0010\n*\u00020\u000b*\u0002H\nH\u0002¢\u0006\u0002\u0010\"\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u0016\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001X\u0082\u0004¢\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u0016\u0010\u0005\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001X\u0082\u0004¢\u0006\u0002\n\u0000*\f\b\u0000\u00102\"\u00020\u00142\u00020\u0014*\f\b\u0000\u00103\"\u00020\u00072\u00020\u0007\u0082\u0002\u0004\n\u0002\b\u0019¨\u00064" }, d2 = { "baseContinuationImplClass", "", "baseContinuationImplClassName", "kotlin.jvm.PlatformType", "stackTraceRecoveryClass", "stackTraceRecoveryClassName", "artificialFrame", "Ljava/lang/StackTraceElement;", "message", "createFinalException", "E", "", "cause", "result", "resultStackTrace", "Ljava/util/ArrayDeque;", "Lkotlinx/coroutines/internal/StackTraceElement;", "(Ljava/lang/Throwable;Ljava/lang/Throwable;Ljava/util/ArrayDeque;)Ljava/lang/Throwable;", "createStackTrace", "continuation", "Lkotlin/coroutines/jvm/internal/CoroutineStackFrame;", "Lkotlinx/coroutines/internal/CoroutineStackFrame;", "mergeRecoveredTraces", "", "recoveredStacktrace", "", "([Ljava/lang/StackTraceElement;Ljava/util/ArrayDeque;)V", "recoverAndThrow", "", "exception", "(Ljava/lang/Throwable;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "recoverFromStackFrame", "(Ljava/lang/Throwable;Lkotlin/coroutines/jvm/internal/CoroutineStackFrame;)Ljava/lang/Throwable;", "recoverStackTrace", "(Ljava/lang/Throwable;)Ljava/lang/Throwable;", "Lkotlin/coroutines/Continuation;", "(Ljava/lang/Throwable;Lkotlin/coroutines/Continuation;)Ljava/lang/Throwable;", "unwrap", "causeAndStacktrace", "Lkotlin/Pair;", "(Ljava/lang/Throwable;)Lkotlin/Pair;", "elementWiseEquals", "", "e", "frameIndex", "", "methodName", "([Ljava/lang/StackTraceElement;Ljava/lang/String;)I", "isArtificial", "sanitizeStackTrace", "CoroutineStackFrame", "StackTraceElement", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class StackTraceRecoveryKt
{
    private static final String baseContinuationImplClass = "kotlin.coroutines.jvm.internal.BaseContinuationImpl";
    private static final String baseContinuationImplClassName;
    private static final String stackTraceRecoveryClass = "kotlinx.coroutines.internal.StackTraceRecoveryKt";
    private static final String stackTraceRecoveryClassName;
    
    static {
        final String s = "kotlinx.coroutines.internal.StackTraceRecoveryKt";
        Object o = "kotlin.coroutines.jvm.internal.BaseContinuationImpl";
        Object constructor-impl = null;
        try {
            final Result.Companion companion = Result.Companion;
            final Class<?> forName = Class.forName("kotlin.coroutines.jvm.internal.BaseContinuationImpl");
            Intrinsics.checkExpressionValueIsNotNull(forName, "Class.forName(baseContinuationImplClass)");
            Result.constructor-impl(forName.getCanonicalName());
        }
        finally {
            final Result.Companion companion2 = Result.Companion;
            final Throwable t;
            constructor-impl = Result.constructor-impl(ResultKt.createFailure(t));
        }
        if (Result.exceptionOrNull-impl(constructor-impl) == null) {
            o = constructor-impl;
        }
        baseContinuationImplClassName = (String)o;
        Object constructor-impl2 = null;
        try {
            final Result.Companion companion3 = Result.Companion;
            final Class<?> forName2 = Class.forName("kotlinx.coroutines.internal.StackTraceRecoveryKt");
            Intrinsics.checkExpressionValueIsNotNull(forName2, "Class.forName(stackTraceRecoveryClass)");
            Result.constructor-impl(forName2.getCanonicalName());
        }
        finally {
            final Result.Companion companion4 = Result.Companion;
            final Throwable t2;
            constructor-impl2 = Result.constructor-impl(ResultKt.createFailure(t2));
        }
        Object o2 = s;
        if (Result.exceptionOrNull-impl(constructor-impl2) == null) {
            o2 = constructor-impl2;
        }
        stackTraceRecoveryClassName = (String)o2;
    }
    
    public static final StackTraceElement artificialFrame(final String str) {
        Intrinsics.checkParameterIsNotNull(str, "message");
        final StringBuilder sb = new StringBuilder();
        sb.append("\b\b\b(");
        sb.append(str);
        return new StackTraceElement(sb.toString(), "\b", "\b", -1);
    }
    
    private static final <E extends Throwable> Pair<E, StackTraceElement[]> causeAndStacktrace(final E e) {
        final Throwable cause = e.getCause();
        if (cause != null && Intrinsics.areEqual(cause.getClass(), e.getClass())) {
            final StackTraceElement[] stackTrace = e.getStackTrace();
            Intrinsics.checkExpressionValueIsNotNull(stackTrace, "currentTrace");
            final int length = stackTrace.length;
            int i = 0;
            while (true) {
                while (i < length) {
                    final StackTraceElement stackTraceElement = stackTrace[i];
                    Intrinsics.checkExpressionValueIsNotNull(stackTraceElement, "it");
                    if (isArtificial(stackTraceElement)) {
                        final boolean b = true;
                        if (b) {
                            return TuplesKt.to(cause, stackTrace);
                        }
                        return TuplesKt.to(e, new StackTraceElement[0]);
                    }
                    else {
                        ++i;
                    }
                }
                final boolean b = false;
                continue;
            }
        }
        return TuplesKt.to(e, new StackTraceElement[0]);
    }
    
    private static final <E extends Throwable> E createFinalException(final E e, final E e2, final ArrayDeque<StackTraceElement> arrayDeque) {
        arrayDeque.addFirst(artificialFrame("Coroutine boundary"));
        final StackTraceElement[] stackTrace = e.getStackTrace();
        Intrinsics.checkExpressionValueIsNotNull(stackTrace, "causeTrace");
        final String baseContinuationImplClassName = StackTraceRecoveryKt.baseContinuationImplClassName;
        Intrinsics.checkExpressionValueIsNotNull(baseContinuationImplClassName, "baseContinuationImplClassName");
        final int frameIndex = frameIndex(stackTrace, baseContinuationImplClassName);
        final int n = 0;
        if (frameIndex != -1) {
            final StackTraceElement[] stackTrace2 = new StackTraceElement[arrayDeque.size() + frameIndex];
            for (int i = 0; i < frameIndex; ++i) {
                stackTrace2[i] = stackTrace[i];
            }
            final Iterator<StackTraceElement> iterator = (Iterator<StackTraceElement>)arrayDeque.iterator();
            int n2 = n;
            while (iterator.hasNext()) {
                stackTrace2[frameIndex + n2] = iterator.next();
                ++n2;
            }
            e2.setStackTrace(stackTrace2);
            return e2;
        }
        final StackTraceElement[] array = arrayDeque.toArray(new StackTraceElement[0]);
        if (array != null) {
            e2.setStackTrace(array);
            return e2;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }
    
    private static final ArrayDeque<StackTraceElement> createStackTrace(CoroutineStackFrame callerFrame) {
        final ArrayDeque<StackTraceElement> arrayDeque = new ArrayDeque<StackTraceElement>();
        final StackTraceElement stackTraceElement = callerFrame.getStackTraceElement();
        CoroutineStackFrame coroutineStackFrame = callerFrame;
        if (stackTraceElement != null) {
            arrayDeque.add(stackTraceElement);
            coroutineStackFrame = callerFrame;
        }
        while (true) {
            callerFrame = coroutineStackFrame;
            if (!(coroutineStackFrame instanceof CoroutineStackFrame)) {
                callerFrame = null;
            }
            if (callerFrame == null) {
                break;
            }
            callerFrame = callerFrame.getCallerFrame();
            if (callerFrame == null) {
                break;
            }
            final StackTraceElement stackTraceElement2 = callerFrame.getStackTraceElement();
            coroutineStackFrame = callerFrame;
            if (stackTraceElement2 == null) {
                continue;
            }
            arrayDeque.add(stackTraceElement2);
            coroutineStackFrame = callerFrame;
        }
        return arrayDeque;
    }
    
    private static final boolean elementWiseEquals(final StackTraceElement stackTraceElement, final StackTraceElement stackTraceElement2) {
        return stackTraceElement.getLineNumber() == stackTraceElement2.getLineNumber() && Intrinsics.areEqual(stackTraceElement.getMethodName(), stackTraceElement2.getMethodName()) && Intrinsics.areEqual(stackTraceElement.getFileName(), stackTraceElement2.getFileName()) && Intrinsics.areEqual(stackTraceElement.getClassName(), stackTraceElement2.getClassName());
    }
    
    private static final int frameIndex(final StackTraceElement[] array, final String s) {
        for (int length = array.length, i = 0; i < length; ++i) {
            if (Intrinsics.areEqual(s, array[i].getClassName())) {
                return i;
            }
        }
        return -1;
    }
    
    public static final boolean isArtificial(final StackTraceElement stackTraceElement) {
        Intrinsics.checkParameterIsNotNull(stackTraceElement, "$this$isArtificial");
        final String className = stackTraceElement.getClassName();
        Intrinsics.checkExpressionValueIsNotNull(className, "className");
        return StringsKt__StringsJVMKt.startsWith$default(className, "\b\b\b", false, 2, null);
    }
    
    private static final void mergeRecoveredTraces(final StackTraceElement[] array, final ArrayDeque<StackTraceElement> arrayDeque) {
        while (true) {
            for (int length = array.length, i = 0; i < length; ++i) {
                if (isArtificial(array[i])) {
                    final int n = i + 1;
                    int n2 = array.length - 1;
                    if (n2 >= n) {
                        while (true) {
                            final StackTraceElement stackTraceElement = array[n2];
                            final StackTraceElement last = arrayDeque.getLast();
                            Intrinsics.checkExpressionValueIsNotNull(last, "result.last");
                            if (elementWiseEquals(stackTraceElement, last)) {
                                arrayDeque.removeLast();
                            }
                            arrayDeque.addFirst(array[n2]);
                            if (n2 == n) {
                                break;
                            }
                            --n2;
                        }
                    }
                    return;
                }
            }
            int i = -1;
            continue;
        }
    }
    
    public static final Object recoverAndThrow(final Throwable t, final Continuation<?> continuation) {
        if (!DebugKt.getRECOVER_STACK_TRACES()) {
            throw t;
        }
        if (!(continuation instanceof CoroutineStackFrame)) {
            throw t;
        }
        throw recoverFromStackFrame(t, (CoroutineStackFrame)continuation);
    }
    
    private static final Object recoverAndThrow$$forInline(final Throwable t, final Continuation continuation) {
        if (!DebugKt.getRECOVER_STACK_TRACES()) {
            throw t;
        }
        InlineMarker.mark(0);
        if (!(continuation instanceof CoroutineStackFrame)) {
            throw t;
        }
        throw recoverFromStackFrame(t, (CoroutineStackFrame)continuation);
    }
    
    private static final <E extends Throwable> E recoverFromStackFrame(final E e, final CoroutineStackFrame coroutineStackFrame) {
        final Pair<E, StackTraceElement[]> causeAndStacktrace = causeAndStacktrace(e);
        final Throwable t = causeAndStacktrace.component1();
        final StackTraceElement[] array = causeAndStacktrace.component2();
        final Throwable tryCopyException = ExceptionsConstuctorKt.tryCopyException(t);
        Throwable finalException = e;
        if (tryCopyException != null) {
            final ArrayDeque<StackTraceElement> stackTrace = createStackTrace(coroutineStackFrame);
            if (stackTrace.isEmpty()) {
                return e;
            }
            if (t != e) {
                mergeRecoveredTraces(array, stackTrace);
            }
            finalException = createFinalException(t, tryCopyException, stackTrace);
        }
        return (E)finalException;
    }
    
    public static final <E extends Throwable> E recoverStackTrace(E sanitizeStackTrace) {
        Intrinsics.checkParameterIsNotNull(sanitizeStackTrace, "exception");
        if (!DebugKt.getRECOVER_STACK_TRACES()) {
            return sanitizeStackTrace;
        }
        final Throwable tryCopyException = ExceptionsConstuctorKt.tryCopyException(sanitizeStackTrace);
        if (tryCopyException != null) {
            sanitizeStackTrace = (E)sanitizeStackTrace(tryCopyException);
        }
        return sanitizeStackTrace;
    }
    
    public static final <E extends Throwable> E recoverStackTrace(final E e, final Continuation<?> continuation) {
        Intrinsics.checkParameterIsNotNull(e, "exception");
        Intrinsics.checkParameterIsNotNull(continuation, "continuation");
        Throwable recoverFromStackFrame = e;
        if (DebugKt.getRECOVER_STACK_TRACES()) {
            if (!(continuation instanceof CoroutineStackFrame)) {
                return e;
            }
            recoverFromStackFrame = recoverFromStackFrame(e, (CoroutineStackFrame)continuation);
        }
        return (E)recoverFromStackFrame;
    }
    
    private static final <E extends Throwable> E sanitizeStackTrace(final E e) {
        final StackTraceElement[] stackTrace = e.getStackTrace();
        final int length = stackTrace.length;
        Intrinsics.checkExpressionValueIsNotNull(stackTrace, "stackTrace");
        final String stackTraceRecoveryClassName = StackTraceRecoveryKt.stackTraceRecoveryClassName;
        Intrinsics.checkExpressionValueIsNotNull(stackTraceRecoveryClassName, "stackTraceRecoveryClassName");
        final int frameIndex = frameIndex(stackTrace, stackTraceRecoveryClassName);
        final String baseContinuationImplClassName = StackTraceRecoveryKt.baseContinuationImplClassName;
        Intrinsics.checkExpressionValueIsNotNull(baseContinuationImplClassName, "baseContinuationImplClassName");
        final int frameIndex2 = frameIndex(stackTrace, baseContinuationImplClassName);
        final int n = 0;
        int n2;
        if (frameIndex2 == -1) {
            n2 = 0;
        }
        else {
            n2 = length - frameIndex2;
        }
        final int n3 = length - frameIndex - n2;
        final StackTraceElement[] stackTrace2 = new StackTraceElement[n3];
        for (int i = n; i < n3; ++i) {
            StackTraceElement artificialFrame;
            if (i == 0) {
                artificialFrame = artificialFrame("Coroutine boundary");
            }
            else {
                artificialFrame = stackTrace[frameIndex + 1 + i - 1];
            }
            stackTrace2[i] = artificialFrame;
        }
        e.setStackTrace(stackTrace2);
        return e;
    }
    
    public static final <E extends Throwable> E unwrap(final E e) {
        Intrinsics.checkParameterIsNotNull(e, "exception");
        if (!DebugKt.getRECOVER_STACK_TRACES()) {
            return e;
        }
        final Throwable cause = e.getCause();
        if (cause != null) {
            final boolean equal = Intrinsics.areEqual(cause.getClass(), e.getClass());
            final int n = 1;
            if (equal ^ true) {
                return e;
            }
            final StackTraceElement[] stackTrace = e.getStackTrace();
            Intrinsics.checkExpressionValueIsNotNull(stackTrace, "exception.stackTrace");
            final int length = stackTrace.length;
            int i = 0;
            while (true) {
                while (i < length) {
                    final StackTraceElement stackTraceElement = stackTrace[i];
                    Intrinsics.checkExpressionValueIsNotNull(stackTraceElement, "it");
                    if (isArtificial(stackTraceElement)) {
                        final int n2 = n;
                        if (n2 != 0) {
                            return (E)cause;
                        }
                        return e;
                    }
                    else {
                        ++i;
                    }
                }
                final int n2 = 0;
                continue;
            }
        }
        return e;
    }
}
