// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.internal;

import java.util.List;
import java.util.Iterator;
import java.util.ServiceLoader;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.MainCoroutineDispatcher;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0007\u001a\u00020\u0006H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00068\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\b" }, d2 = { "Lkotlinx/coroutines/internal/MainDispatcherLoader;", "", "()V", "FAST_SERVICE_LOADER_ENABLED", "", "dispatcher", "Lkotlinx/coroutines/MainCoroutineDispatcher;", "loadMainDispatcher", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class MainDispatcherLoader
{
    private static final boolean FAST_SERVICE_LOADER_ENABLED;
    public static final MainDispatcherLoader INSTANCE;
    public static final MainCoroutineDispatcher dispatcher;
    
    static {
        final MainDispatcherLoader mainDispatcherLoader = INSTANCE = new MainDispatcherLoader();
        FAST_SERVICE_LOADER_ENABLED = SystemPropsKt.systemProp("kotlinx.coroutines.fast.service.loader", true);
        dispatcher = mainDispatcherLoader.loadMainDispatcher();
    }
    
    private MainDispatcherLoader() {
    }
    
    private final MainCoroutineDispatcher loadMainDispatcher() {
        try {
            List<? extends MainDispatcherFactory> list;
            if (MainDispatcherLoader.FAST_SERVICE_LOADER_ENABLED) {
                final FastServiceLoader instance = FastServiceLoader.INSTANCE;
                final ClassLoader classLoader = MainDispatcherFactory.class.getClassLoader();
                Intrinsics.checkExpressionValueIsNotNull(classLoader, "clz.classLoader");
                list = instance.load$kotlinx_coroutines_core(MainDispatcherFactory.class, classLoader);
            }
            else {
                final Iterator<MainDispatcherFactory> iterator = ServiceLoader.load(MainDispatcherFactory.class, MainDispatcherFactory.class.getClassLoader()).iterator();
                Intrinsics.checkExpressionValueIsNotNull(iterator, "ServiceLoader.load(\n    \u2026             ).iterator()");
                list = (List<? extends MainDispatcherFactory>)SequencesKt___SequencesKt.toList(SequencesKt__SequencesKt.asSequence((Iterator<?>)iterator));
            }
            final Iterator<Object> iterator2 = ((List<Object>)list).iterator();
            MainDispatcherFactory next;
            if (!iterator2.hasNext()) {
                next = null;
            }
            else {
                next = iterator2.next();
                if (iterator2.hasNext()) {
                    int loadPriority = next.getLoadPriority();
                    MainDispatcherFactory mainDispatcherFactory = next;
                    do {
                        final MainDispatcherFactory next2 = iterator2.next();
                        final int loadPriority2 = next2.getLoadPriority();
                        next = mainDispatcherFactory;
                        int n;
                        if ((n = loadPriority) < loadPriority2) {
                            next = next2;
                            n = loadPriority2;
                        }
                        mainDispatcherFactory = next;
                        loadPriority = n;
                    } while (iterator2.hasNext());
                }
            }
            final MainDispatcherFactory mainDispatcherFactory2 = next;
            if (mainDispatcherFactory2 != null) {
                final MainCoroutineDispatcher tryCreateDispatcher = MainDispatchersKt.tryCreateDispatcher(mainDispatcherFactory2, list);
                if (tryCreateDispatcher != null) {
                    return tryCreateDispatcher;
                }
            }
            return new MissingMainCoroutineDispatcher(null, null, 2, null);
        }
        finally {
            final Throwable t;
            return new MissingMainCoroutineDispatcher(t, null, 2, null);
        }
    }
}
