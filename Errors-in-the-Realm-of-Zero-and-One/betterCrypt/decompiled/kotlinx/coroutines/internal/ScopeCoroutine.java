// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.internal;

import kotlinx.coroutines.Job;
import kotlinx.coroutines.ResumeModeKt;
import kotlinx.coroutines.CompletedExceptionally;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.Continuation;
import kotlin.Metadata;
import kotlin.coroutines.jvm.internal.CoroutineStackFrame;
import kotlinx.coroutines.AbstractCoroutine;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000R\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0010\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00002\b\u0012\u0004\u0012\u0002H\u00010\u00022\u00060\u0003j\u0002`\u0004B\u001b\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\b¢\u0006\u0002\u0010\tJ\u001a\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001b2\u0006\u0010\u001c\u001a\u00020\u000eH\u0014J\u000e\u0010\u001d\u001a\n\u0018\u00010\u001ej\u0004\u0018\u0001`\u001fR\u0019\u0010\n\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00048F¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\u00020\u000e8PX\u0090\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u00128DX\u0084\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0013R\u0016\u0010\u0014\u001a\u0004\u0018\u00010\u00158@X\u0080\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\b8\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006 " }, d2 = { "Lkotlinx/coroutines/internal/ScopeCoroutine;", "T", "Lkotlinx/coroutines/AbstractCoroutine;", "Lkotlin/coroutines/jvm/internal/CoroutineStackFrame;", "Lkotlinx/coroutines/internal/CoroutineStackFrame;", "context", "Lkotlin/coroutines/CoroutineContext;", "uCont", "Lkotlin/coroutines/Continuation;", "(Lkotlin/coroutines/CoroutineContext;Lkotlin/coroutines/Continuation;)V", "callerFrame", "getCallerFrame", "()Lkotlin/coroutines/jvm/internal/CoroutineStackFrame;", "defaultResumeMode", "", "getDefaultResumeMode$kotlinx_coroutines_core", "()I", "isScopedCoroutine", "", "()Z", "parent", "Lkotlinx/coroutines/Job;", "getParent$kotlinx_coroutines_core", "()Lkotlinx/coroutines/Job;", "afterCompletionInternal", "", "state", "", "mode", "getStackTraceElement", "Ljava/lang/StackTraceElement;", "Lkotlinx/coroutines/internal/StackTraceElement;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public class ScopeCoroutine<T> extends AbstractCoroutine<T> implements CoroutineStackFrame
{
    public final Continuation<T> uCont;
    
    public ScopeCoroutine(final CoroutineContext coroutineContext, final Continuation<? super T> uCont) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        Intrinsics.checkParameterIsNotNull(uCont, "uCont");
        super(coroutineContext, true);
        this.uCont = (Continuation<T>)uCont;
    }
    
    @Override
    protected void afterCompletionInternal(final Object o, final int n) {
        if (o instanceof CompletedExceptionally) {
            Throwable t = ((CompletedExceptionally)o).cause;
            if (n != 4) {
                t = StackTraceRecoveryKt.recoverStackTrace(t, this.uCont);
            }
            ResumeModeKt.resumeUninterceptedWithExceptionMode((Continuation<? super Object>)this.uCont, t, n);
            return;
        }
        ResumeModeKt.resumeUninterceptedMode((Continuation<? super Object>)this.uCont, o, n);
    }
    
    @Override
    public final CoroutineStackFrame getCallerFrame() {
        return (CoroutineStackFrame)this.uCont;
    }
    
    @Override
    public int getDefaultResumeMode$kotlinx_coroutines_core() {
        return 2;
    }
    
    public final Job getParent$kotlinx_coroutines_core() {
        return this.parentContext.get((CoroutineContext.Key<Job>)Job.Key);
    }
    
    @Override
    public final StackTraceElement getStackTraceElement() {
        return null;
    }
    
    @Override
    protected final boolean isScopedCoroutine() {
        return true;
    }
}
