// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.internal;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import kotlin.coroutines.CoroutineContext;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0081\b\u0018\u00002\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00020\u0001B\u0011\u0012\n\u0010\u0003\u001a\u0006\u0012\u0002\b\u00030\u0004¢\u0006\u0002\u0010\u0005J\r\u0010\u0006\u001a\u0006\u0012\u0002\b\u00030\u0004H\u00c2\u0003J\u0017\u0010\u0007\u001a\u00020\u00002\f\b\u0002\u0010\u0003\u001a\u0006\u0012\u0002\b\u00030\u0004H\u00c6\u0001J\u0013\u0010\b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u00d6\u0003J\t\u0010\f\u001a\u00020\rH\u00d6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001R\u0012\u0010\u0003\u001a\u0006\u0012\u0002\b\u00030\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010" }, d2 = { "Lkotlinx/coroutines/internal/ThreadLocalKey;", "Lkotlin/coroutines/CoroutineContext$Key;", "Lkotlinx/coroutines/internal/ThreadLocalElement;", "threadLocal", "Ljava/lang/ThreadLocal;", "(Ljava/lang/ThreadLocal;)V", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class ThreadLocalKey implements Key<ThreadLocalElement<?>>
{
    private final ThreadLocal<?> threadLocal;
    
    public ThreadLocalKey(final ThreadLocal<?> threadLocal) {
        Intrinsics.checkParameterIsNotNull(threadLocal, "threadLocal");
        this.threadLocal = threadLocal;
    }
    
    private final ThreadLocal<?> component1() {
        return this.threadLocal;
    }
    
    public final ThreadLocalKey copy(final ThreadLocal<?> threadLocal) {
        Intrinsics.checkParameterIsNotNull(threadLocal, "threadLocal");
        return new ThreadLocalKey(threadLocal);
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof ThreadLocalKey && Intrinsics.areEqual(this.threadLocal, ((ThreadLocalKey)o).threadLocal));
    }
    
    @Override
    public int hashCode() {
        final ThreadLocal<?> threadLocal = this.threadLocal;
        if (threadLocal != null) {
            return threadLocal.hashCode();
        }
        return 0;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ThreadLocalKey(threadLocal=");
        sb.append(this.threadLocal);
        sb.append(")");
        return sb.toString();
    }
}
