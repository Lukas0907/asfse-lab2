// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.internal;

import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.DebugKt;
import kotlin.TypeCastException;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0012\b\u0017\u0018\u00002\u00020\u0001:\u0004KLMNB\u0007¢\u0006\u0004\b\u0002\u0010\u0003J\u0019\u0010\u0007\u001a\u00020\u00062\n\u0010\u0005\u001a\u00060\u0000j\u0002`\u0004¢\u0006\u0004\b\u0007\u0010\bJ,\u0010\f\u001a\u00020\n2\n\u0010\u0005\u001a\u00060\u0000j\u0002`\u00042\u000e\b\u0004\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0086\b¢\u0006\u0004\b\f\u0010\rJ4\u0010\u0010\u001a\u00020\n2\n\u0010\u0005\u001a\u00060\u0000j\u0002`\u00042\u0016\u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\u0000j\u0002`\u0004\u0012\u0004\u0012\u00020\n0\u000eH\u0086\b¢\u0006\u0004\b\u0010\u0010\u0011JD\u0010\u0012\u001a\u00020\n2\n\u0010\u0005\u001a\u00060\u0000j\u0002`\u00042\u0016\u0010\u000f\u001a\u0012\u0012\b\u0012\u00060\u0000j\u0002`\u0004\u0012\u0004\u0012\u00020\n0\u000e2\u000e\b\u0004\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0086\b¢\u0006\u0004\b\u0012\u0010\u0013J'\u0010\u0015\u001a\u00020\n2\n\u0010\u0005\u001a\u00060\u0000j\u0002`\u00042\n\u0010\u0014\u001a\u00060\u0000j\u0002`\u0004H\u0001¢\u0006\u0004\b\u0015\u0010\u0016J\u0019\u0010\u0017\u001a\u00020\n2\n\u0010\u0005\u001a\u00060\u0000j\u0002`\u0004¢\u0006\u0004\b\u0017\u0010\u0018J-\u0010\u001c\u001a\n\u0018\u00010\u0000j\u0004\u0018\u0001`\u00042\n\u0010\u0019\u001a\u00060\u0000j\u0002`\u00042\b\u0010\u001b\u001a\u0004\u0018\u00010\u001aH\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ)\u0010 \u001a\b\u0012\u0004\u0012\u00028\u00000\u001f\"\f\b\u0000\u0010\u001e*\u00060\u0000j\u0002`\u00042\u0006\u0010\u0005\u001a\u00028\u0000¢\u0006\u0004\b \u0010!J\u0017\u0010#\u001a\f\u0012\b\u0012\u00060\u0000j\u0002`\u00040\"¢\u0006\u0004\b#\u0010$J\u0013\u0010%\u001a\u00060\u0000j\u0002`\u0004H\u0002¢\u0006\u0004\b%\u0010&J\u001b\u0010'\u001a\u00020\u00062\n\u0010\u0014\u001a\u00060\u0000j\u0002`\u0004H\u0002¢\u0006\u0004\b'\u0010\bJ\u001b\u0010(\u001a\u00020\u00062\n\u0010\u0014\u001a\u00060\u0000j\u0002`\u0004H\u0002¢\u0006\u0004\b(\u0010\bJ\u000f\u0010)\u001a\u00020\u0006H\u0001¢\u0006\u0004\b)\u0010\u0003J\r\u0010*\u001a\u00020\u0006¢\u0006\u0004\b*\u0010\u0003J,\u0010,\u001a\u00020+2\n\u0010\u0005\u001a\u00060\u0000j\u0002`\u00042\u000e\b\u0004\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0081\b¢\u0006\u0004\b,\u0010-J\u0013\u0010.\u001a\u00060\u0000j\u0002`\u0004H\u0002¢\u0006\u0004\b.\u0010&J\u000f\u0010/\u001a\u00020\nH\u0016¢\u0006\u0004\b/\u00100J\u001a\u00101\u001a\u0004\u0018\u00018\u0000\"\u0006\b\u0000\u0010\u001e\u0018\u0001H\u0086\b¢\u0006\u0004\b1\u00102J.\u00103\u001a\u0004\u0018\u00018\u0000\"\u0006\b\u0000\u0010\u001e\u0018\u00012\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\n0\u000eH\u0086\b¢\u0006\u0004\b3\u00104J\u0015\u00105\u001a\n\u0018\u00010\u0000j\u0004\u0018\u0001`\u0004¢\u0006\u0004\b5\u0010&J\u000f\u00107\u001a\u000206H\u0002¢\u0006\u0004\b7\u00108J\u000f\u0010:\u001a\u000209H\u0016¢\u0006\u0004\b:\u0010;J/\u0010>\u001a\u00020=2\n\u0010\u0005\u001a\u00060\u0000j\u0002`\u00042\n\u0010\u0014\u001a\u00060\u0000j\u0002`\u00042\u0006\u0010<\u001a\u00020+H\u0001¢\u0006\u0004\b>\u0010?J'\u0010C\u001a\u00020\u00062\n\u0010@\u001a\u00060\u0000j\u0002`\u00042\n\u0010\u0014\u001a\u00060\u0000j\u0002`\u0004H\u0000¢\u0006\u0004\bA\u0010BR\u0013\u0010D\u001a\u00020\n8F@\u0006¢\u0006\u0006\u001a\u0004\bD\u00100R\u0013\u0010\u0014\u001a\u00020\u00018F@\u0006¢\u0006\u0006\u001a\u0004\bE\u00102R\u0017\u0010G\u001a\u00060\u0000j\u0002`\u00048F@\u0006¢\u0006\u0006\u001a\u0004\bF\u0010&R\u0013\u0010@\u001a\u00020\u00018F@\u0006¢\u0006\u0006\u001a\u0004\bH\u00102R\u0017\u0010J\u001a\u00060\u0000j\u0002`\u00048F@\u0006¢\u0006\u0006\u001a\u0004\bI\u0010&¨\u0006O" }, d2 = { "Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "", "<init>", "()V", "Lkotlinx/coroutines/internal/Node;", "node", "", "addLast", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;)V", "Lkotlin/Function0;", "", "condition", "addLastIf", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;Lkotlin/jvm/functions/Function0;)Z", "Lkotlin/Function1;", "predicate", "addLastIfPrev", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;Lkotlin/jvm/functions/Function1;)Z", "addLastIfPrevAndIf", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Z", "next", "addNext", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;Lkotlinx/coroutines/internal/LockFreeLinkedListNode;)Z", "addOneIfEmpty", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;)Z", "_prev", "Lkotlinx/coroutines/internal/OpDescriptor;", "op", "correctPrev", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;Lkotlinx/coroutines/internal/OpDescriptor;)Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "T", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode$AddLastDesc;", "describeAddLast", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;)Lkotlinx/coroutines/internal/LockFreeLinkedListNode$AddLastDesc;", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode$RemoveFirstDesc;", "describeRemoveFirst", "()Lkotlinx/coroutines/internal/LockFreeLinkedListNode$RemoveFirstDesc;", "findHead", "()Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "finishAdd", "finishRemove", "helpDelete", "helpRemove", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode$CondAddOp;", "makeCondAddOp", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;Lkotlin/jvm/functions/Function0;)Lkotlinx/coroutines/internal/LockFreeLinkedListNode$CondAddOp;", "markPrev", "remove", "()Z", "removeFirstIfIsInstanceOf", "()Ljava/lang/Object;", "removeFirstIfIsInstanceOfOrPeekIf", "(Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "removeFirstOrNull", "Lkotlinx/coroutines/internal/Removed;", "removed", "()Lkotlinx/coroutines/internal/Removed;", "", "toString", "()Ljava/lang/String;", "condAdd", "", "tryCondAddNext", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;Lkotlinx/coroutines/internal/LockFreeLinkedListNode;Lkotlinx/coroutines/internal/LockFreeLinkedListNode$CondAddOp;)I", "prev", "validateNode$kotlinx_coroutines_core", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;Lkotlinx/coroutines/internal/LockFreeLinkedListNode;)V", "validateNode", "isRemoved", "getNext", "getNextNode", "nextNode", "getPrev", "getPrevNode", "prevNode", "AbstractAtomicDesc", "AddLastDesc", "CondAddOp", "RemoveFirstDesc", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public class LockFreeLinkedListNode
{
    static final AtomicReferenceFieldUpdater _next$FU;
    static final AtomicReferenceFieldUpdater _prev$FU;
    private static final AtomicReferenceFieldUpdater _removedRef$FU;
    volatile Object _next;
    volatile Object _prev;
    private volatile Object _removedRef;
    
    static {
        _next$FU = AtomicReferenceFieldUpdater.newUpdater(LockFreeLinkedListNode.class, Object.class, "_next");
        _prev$FU = AtomicReferenceFieldUpdater.newUpdater(LockFreeLinkedListNode.class, Object.class, "_prev");
        _removedRef$FU = AtomicReferenceFieldUpdater.newUpdater(LockFreeLinkedListNode.class, Object.class, "_removedRef");
    }
    
    public LockFreeLinkedListNode() {
        this._next = this;
        this._prev = this;
        this._removedRef = null;
    }
    
    private final LockFreeLinkedListNode correctPrev(LockFreeLinkedListNode unwrap, final OpDescriptor opDescriptor) {
        final LockFreeLinkedListNode lockFreeLinkedListNode = null;
        while (true) {
            LockFreeLinkedListNode lockFreeLinkedListNode2 = lockFreeLinkedListNode;
            while (true) {
                final Object next = unwrap._next;
                if (next == opDescriptor) {
                    return unwrap;
                }
                if (next instanceof OpDescriptor) {
                    ((OpDescriptor)next).perform(unwrap);
                }
                else if (next instanceof Removed) {
                    if (lockFreeLinkedListNode2 != null) {
                        unwrap.markPrev();
                        LockFreeLinkedListNode._next$FU.compareAndSet(lockFreeLinkedListNode2, unwrap, ((Removed)next).ref);
                        unwrap = lockFreeLinkedListNode2;
                        break;
                    }
                    unwrap = LockFreeLinkedListKt.unwrap(unwrap._prev);
                }
                else {
                    final Object prev = this._prev;
                    if (prev instanceof Removed) {
                        return null;
                    }
                    if (next != this) {
                        if (next == null) {
                            throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                        }
                        final LockFreeLinkedListNode lockFreeLinkedListNode3 = (LockFreeLinkedListNode)next;
                        lockFreeLinkedListNode2 = unwrap;
                        unwrap = lockFreeLinkedListNode3;
                    }
                    else {
                        if (prev == unwrap) {
                            return null;
                        }
                        if (LockFreeLinkedListNode._prev$FU.compareAndSet(this, prev, unwrap) && !(unwrap._prev instanceof Removed)) {
                            return null;
                        }
                        continue;
                    }
                }
            }
        }
    }
    
    private final LockFreeLinkedListNode findHead() {
        LockFreeLinkedListNode nextNode;
        LockFreeLinkedListNode lockFreeLinkedListNode2;
        for (LockFreeLinkedListNode lockFreeLinkedListNode = nextNode = this; !(nextNode instanceof LockFreeLinkedListHead); nextNode = lockFreeLinkedListNode2) {
            lockFreeLinkedListNode2 = (nextNode = nextNode.getNextNode());
            if (DebugKt.getASSERTIONS_ENABLED()) {
                if (lockFreeLinkedListNode2 == lockFreeLinkedListNode) {
                    throw new AssertionError();
                }
            }
        }
        return nextNode;
    }
    
    private final void finishAdd(final LockFreeLinkedListNode lockFreeLinkedListNode) {
        Object prev;
        do {
            prev = lockFreeLinkedListNode._prev;
            if (prev instanceof Removed) {
                return;
            }
            if (this.getNext() != lockFreeLinkedListNode) {
                return;
            }
        } while (!LockFreeLinkedListNode._prev$FU.compareAndSet(lockFreeLinkedListNode, prev, this));
        if (!(this.getNext() instanceof Removed)) {
            return;
        }
        if (prev != null) {
            lockFreeLinkedListNode.correctPrev((LockFreeLinkedListNode)prev, null);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }
    
    private final void finishRemove(final LockFreeLinkedListNode lockFreeLinkedListNode) {
        this.helpDelete();
        lockFreeLinkedListNode.correctPrev(LockFreeLinkedListKt.unwrap(this._prev), null);
    }
    
    private final LockFreeLinkedListNode markPrev() {
        Object prev;
        LockFreeLinkedListNode head;
        do {
            prev = this._prev;
            if (prev instanceof Removed) {
                return ((Removed)prev).ref;
            }
            if (prev == this) {
                head = this.findHead();
            }
            else {
                if (prev == null) {
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                }
                head = (LockFreeLinkedListNode)prev;
            }
        } while (!LockFreeLinkedListNode._prev$FU.compareAndSet(this, prev, head.removed()));
        return (LockFreeLinkedListNode)prev;
    }
    
    private final Removed removed() {
        final Removed removed = (Removed)this._removedRef;
        if (removed != null) {
            return removed;
        }
        final Removed removed2 = new Removed(this);
        LockFreeLinkedListNode._removedRef$FU.lazySet(this, removed2);
        return removed2;
    }
    
    public final void addLast(final LockFreeLinkedListNode lockFreeLinkedListNode) {
        Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "node");
        Object prev;
        do {
            prev = this.getPrev();
            if (prev != null) {
                continue;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
        } while (!((LockFreeLinkedListNode)prev).addNext(lockFreeLinkedListNode, this));
    }
    
    public final boolean addLastIf(final LockFreeLinkedListNode lockFreeLinkedListNode, final Function0<Boolean> function0) {
        Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "node");
        Intrinsics.checkParameterIsNotNull(function0, "condition");
        final CondAddOp condAddOp = (CondAddOp)new LockFreeLinkedListNode$makeCondAddOp.LockFreeLinkedListNode$makeCondAddOp$1((Function0)function0, lockFreeLinkedListNode, lockFreeLinkedListNode);
        while (true) {
            final Object prev = this.getPrev();
            if (prev == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
            final int tryCondAddNext = ((LockFreeLinkedListNode)prev).tryCondAddNext(lockFreeLinkedListNode, this, condAddOp);
            if (tryCondAddNext == 1) {
                return true;
            }
            if (tryCondAddNext != 2) {
                continue;
            }
            return false;
        }
    }
    
    public final boolean addLastIfPrev(final LockFreeLinkedListNode lockFreeLinkedListNode, final Function1<? super LockFreeLinkedListNode, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "node");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        LockFreeLinkedListNode lockFreeLinkedListNode2;
        do {
            final Object prev = this.getPrev();
            if (prev == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
            lockFreeLinkedListNode2 = (LockFreeLinkedListNode)prev;
            if (!function1.invoke(lockFreeLinkedListNode2)) {
                return false;
            }
        } while (!lockFreeLinkedListNode2.addNext(lockFreeLinkedListNode, this));
        return true;
    }
    
    public final boolean addLastIfPrevAndIf(final LockFreeLinkedListNode lockFreeLinkedListNode, final Function1<? super LockFreeLinkedListNode, Boolean> function1, final Function0<Boolean> function2) {
        Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "node");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        Intrinsics.checkParameterIsNotNull(function2, "condition");
        final CondAddOp condAddOp = (CondAddOp)new LockFreeLinkedListNode$makeCondAddOp.LockFreeLinkedListNode$makeCondAddOp$1((Function0)function2, lockFreeLinkedListNode, lockFreeLinkedListNode);
        while (true) {
            final Object prev = this.getPrev();
            if (prev == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
            final LockFreeLinkedListNode lockFreeLinkedListNode2 = (LockFreeLinkedListNode)prev;
            if (!function1.invoke(lockFreeLinkedListNode2)) {
                return false;
            }
            final int tryCondAddNext = lockFreeLinkedListNode2.tryCondAddNext(lockFreeLinkedListNode, this, condAddOp);
            if (tryCondAddNext == 1) {
                return true;
            }
            if (tryCondAddNext != 2) {
                continue;
            }
            return false;
        }
    }
    
    public final boolean addNext(final LockFreeLinkedListNode lockFreeLinkedListNode, final LockFreeLinkedListNode lockFreeLinkedListNode2) {
        Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "node");
        Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode2, "next");
        LockFreeLinkedListNode._prev$FU.lazySet(lockFreeLinkedListNode, this);
        LockFreeLinkedListNode._next$FU.lazySet(lockFreeLinkedListNode, lockFreeLinkedListNode2);
        if (!LockFreeLinkedListNode._next$FU.compareAndSet(this, lockFreeLinkedListNode2, lockFreeLinkedListNode)) {
            return false;
        }
        lockFreeLinkedListNode.finishAdd(lockFreeLinkedListNode2);
        return true;
    }
    
    public final boolean addOneIfEmpty(final LockFreeLinkedListNode lockFreeLinkedListNode) {
        Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "node");
        LockFreeLinkedListNode._prev$FU.lazySet(lockFreeLinkedListNode, this);
        LockFreeLinkedListNode._next$FU.lazySet(lockFreeLinkedListNode, this);
        while (this.getNext() == this) {
            if (LockFreeLinkedListNode._next$FU.compareAndSet(this, this, lockFreeLinkedListNode)) {
                lockFreeLinkedListNode.finishAdd(this);
                return true;
            }
        }
        return false;
    }
    
    public final <T extends LockFreeLinkedListNode> AddLastDesc<T> describeAddLast(final T t) {
        Intrinsics.checkParameterIsNotNull(t, "node");
        return new AddLastDesc<T>(this, t);
    }
    
    public final RemoveFirstDesc<LockFreeLinkedListNode> describeRemoveFirst() {
        return new RemoveFirstDesc<LockFreeLinkedListNode>(this);
    }
    
    public final Object getNext() {
        Object next;
        while (true) {
            next = this._next;
            if (!(next instanceof OpDescriptor)) {
                break;
            }
            ((OpDescriptor)next).perform(this);
        }
        return next;
    }
    
    public final LockFreeLinkedListNode getNextNode() {
        return LockFreeLinkedListKt.unwrap(this.getNext());
    }
    
    public final Object getPrev() {
        while (true) {
            final Object prev = this._prev;
            if (prev instanceof Removed) {
                return prev;
            }
            if (prev == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
            final LockFreeLinkedListNode lockFreeLinkedListNode = (LockFreeLinkedListNode)prev;
            if (lockFreeLinkedListNode.getNext() == this) {
                return prev;
            }
            this.correctPrev(lockFreeLinkedListNode, null);
        }
    }
    
    public final LockFreeLinkedListNode getPrevNode() {
        return LockFreeLinkedListKt.unwrap(this.getPrev());
    }
    
    public final void helpDelete() {
        final LockFreeLinkedListNode lockFreeLinkedListNode = null;
        LockFreeLinkedListNode lockFreeLinkedListNode2 = this.markPrev();
        final Object next = this._next;
        if (next == null) {
            throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Removed");
        }
        LockFreeLinkedListNode lockFreeLinkedListNode3 = ((Removed)next).ref;
        while (true) {
            LockFreeLinkedListNode lockFreeLinkedListNode4 = lockFreeLinkedListNode;
            while (true) {
                final Object next2 = lockFreeLinkedListNode3.getNext();
                if (next2 instanceof Removed) {
                    lockFreeLinkedListNode3.markPrev();
                    lockFreeLinkedListNode3 = ((Removed)next2).ref;
                }
                else {
                    final Object next3 = lockFreeLinkedListNode2.getNext();
                    if (next3 instanceof Removed) {
                        if (lockFreeLinkedListNode4 != null) {
                            lockFreeLinkedListNode2.markPrev();
                            LockFreeLinkedListNode._next$FU.compareAndSet(lockFreeLinkedListNode4, lockFreeLinkedListNode2, ((Removed)next3).ref);
                            lockFreeLinkedListNode2 = lockFreeLinkedListNode4;
                            break;
                        }
                        lockFreeLinkedListNode2 = LockFreeLinkedListKt.unwrap(lockFreeLinkedListNode2._prev);
                    }
                    else if (next3 != this) {
                        if (next3 == null) {
                            throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                        }
                        final LockFreeLinkedListNode lockFreeLinkedListNode5 = (LockFreeLinkedListNode)next3;
                        if (lockFreeLinkedListNode5 == lockFreeLinkedListNode3) {
                            return;
                        }
                        lockFreeLinkedListNode4 = lockFreeLinkedListNode2;
                        lockFreeLinkedListNode2 = lockFreeLinkedListNode5;
                    }
                    else {
                        if (LockFreeLinkedListNode._next$FU.compareAndSet(lockFreeLinkedListNode2, this, lockFreeLinkedListNode3)) {
                            return;
                        }
                        continue;
                    }
                }
            }
        }
    }
    
    public final void helpRemove() {
        Object next;
        if (!((next = this.getNext()) instanceof Removed)) {
            next = null;
        }
        final Removed removed = (Removed)next;
        if (removed != null) {
            this.finishRemove(removed.ref);
            return;
        }
        throw new IllegalStateException("Must be invoked on a removed node".toString());
    }
    
    public final boolean isRemoved() {
        return this.getNext() instanceof Removed;
    }
    
    public final CondAddOp makeCondAddOp(final LockFreeLinkedListNode lockFreeLinkedListNode, final Function0<Boolean> function0) {
        Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "node");
        Intrinsics.checkParameterIsNotNull(function0, "condition");
        return (CondAddOp)new LockFreeLinkedListNode$makeCondAddOp.LockFreeLinkedListNode$makeCondAddOp$1((Function0)function0, lockFreeLinkedListNode, lockFreeLinkedListNode);
    }
    
    public boolean remove() {
        Object next;
        LockFreeLinkedListNode lockFreeLinkedListNode;
        do {
            next = this.getNext();
            if (next instanceof Removed) {
                return false;
            }
            if (next == this) {
                return false;
            }
            if (next == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
            lockFreeLinkedListNode = (LockFreeLinkedListNode)next;
        } while (!LockFreeLinkedListNode._next$FU.compareAndSet(this, next, lockFreeLinkedListNode.removed()));
        this.finishRemove(lockFreeLinkedListNode);
        return true;
    }
    
    public final LockFreeLinkedListNode removeFirstOrNull() {
        while (true) {
            final Object next = this.getNext();
            if (next == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
            final LockFreeLinkedListNode lockFreeLinkedListNode = (LockFreeLinkedListNode)next;
            if (lockFreeLinkedListNode == this) {
                return null;
            }
            if (lockFreeLinkedListNode.remove()) {
                return lockFreeLinkedListNode;
            }
            lockFreeLinkedListNode.helpDelete();
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append('@');
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        return sb.toString();
    }
    
    public final int tryCondAddNext(final LockFreeLinkedListNode lockFreeLinkedListNode, final LockFreeLinkedListNode oldNext, final CondAddOp condAddOp) {
        Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "node");
        Intrinsics.checkParameterIsNotNull(oldNext, "next");
        Intrinsics.checkParameterIsNotNull(condAddOp, "condAdd");
        LockFreeLinkedListNode._prev$FU.lazySet(lockFreeLinkedListNode, this);
        LockFreeLinkedListNode._next$FU.lazySet(lockFreeLinkedListNode, oldNext);
        condAddOp.oldNext = oldNext;
        if (!LockFreeLinkedListNode._next$FU.compareAndSet(this, oldNext, condAddOp)) {
            return 0;
        }
        if (condAddOp.perform(this) == null) {
            return 1;
        }
        return 2;
    }
    
    public final void validateNode$kotlinx_coroutines_core(final LockFreeLinkedListNode lockFreeLinkedListNode, final LockFreeLinkedListNode lockFreeLinkedListNode2) {
        Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "prev");
        Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode2, "next");
        final boolean assertions_ENABLED = DebugKt.getASSERTIONS_ENABLED();
        final int n = 1;
        if (assertions_ENABLED && lockFreeLinkedListNode != this._prev) {
            throw new AssertionError();
        }
        if (!DebugKt.getASSERTIONS_ENABLED()) {
            return;
        }
        int n2;
        if (lockFreeLinkedListNode2 == this._next) {
            n2 = n;
        }
        else {
            n2 = 0;
        }
        if (n2 != 0) {
            return;
        }
        throw new AssertionError();
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b&\u0018\u00002\u00020\u0001:\u0001\u001aB\u0005¢\u0006\u0002\u0010\u0002J\u001c\u0010\n\u001a\u00020\u000b2\n\u0010\f\u001a\u0006\u0012\u0002\b\u00030\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fJ\u0016\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\n\u0010\u0010\u001a\u00060\u0004j\u0002`\u0005H\u0014J \u0010\u0011\u001a\u00020\u000b2\n\u0010\u0010\u001a\u00060\u0004j\u0002`\u00052\n\u0010\u0012\u001a\u00060\u0004j\u0002`\u0005H$J\"\u0010\u0013\u001a\u0004\u0018\u00010\u000f2\n\u0010\u0010\u001a\u00060\u0004j\u0002`\u00052\n\u0010\u0012\u001a\u00060\u0004j\u0002`\u0005H$J\u0014\u0010\u0014\u001a\u0004\u0018\u00010\u000f2\n\u0010\f\u001a\u0006\u0012\u0002\b\u00030\rJ\u001c\u0010\u0015\u001a\u00020\u00162\n\u0010\u0010\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\u0012\u001a\u00020\u000fH\u0014J\u0014\u0010\u0017\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010\f\u001a\u00020\u0018H\u0014J \u0010\u0019\u001a\u00020\u000f2\n\u0010\u0010\u001a\u00060\u0004j\u0002`\u00052\n\u0010\u0012\u001a\u00060\u0004j\u0002`\u0005H$R\u001a\u0010\u0003\u001a\n\u0018\u00010\u0004j\u0004\u0018\u0001`\u0005X¤\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u001a\u0010\b\u001a\n\u0018\u00010\u0004j\u0004\u0018\u0001`\u0005X¤\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\u0007¨\u0006\u001b" }, d2 = { "Lkotlinx/coroutines/internal/LockFreeLinkedListNode$AbstractAtomicDesc;", "Lkotlinx/coroutines/internal/AtomicDesc;", "()V", "affectedNode", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "Lkotlinx/coroutines/internal/Node;", "getAffectedNode", "()Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "originalNext", "getOriginalNext", "complete", "", "op", "Lkotlinx/coroutines/internal/AtomicOp;", "failure", "", "affected", "finishOnSuccess", "next", "onPrepare", "prepare", "retry", "", "takeAffectedNode", "Lkotlinx/coroutines/internal/OpDescriptor;", "updatedNext", "PrepareOp", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    public abstract static class AbstractAtomicDesc extends AtomicDesc
    {
        @Override
        public final void complete(final AtomicOp<?> atomicOp, Object updatedNext) {
            Intrinsics.checkParameterIsNotNull(atomicOp, "op");
            final boolean b = updatedNext == null;
            final LockFreeLinkedListNode affectedNode = this.getAffectedNode();
            if (affectedNode != null) {
                final LockFreeLinkedListNode originalNext = this.getOriginalNext();
                if (originalNext != null) {
                    if (b) {
                        updatedNext = this.updatedNext(affectedNode, originalNext);
                    }
                    else {
                        updatedNext = originalNext;
                    }
                    if (LockFreeLinkedListNode._next$FU.compareAndSet(affectedNode, atomicOp, updatedNext) && b) {
                        this.finishOnSuccess(affectedNode, originalNext);
                    }
                    return;
                }
                final AbstractAtomicDesc abstractAtomicDesc = this;
                if (!DebugKt.getASSERTIONS_ENABLED()) {
                    return;
                }
                if (b ^ true) {
                    return;
                }
                throw new AssertionError();
            }
            else {
                final AbstractAtomicDesc abstractAtomicDesc2 = this;
                if (!DebugKt.getASSERTIONS_ENABLED()) {
                    return;
                }
                if (b ^ true) {
                    return;
                }
                throw new AssertionError();
            }
        }
        
        protected Object failure(final LockFreeLinkedListNode lockFreeLinkedListNode) {
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "affected");
            return null;
        }
        
        protected abstract void finishOnSuccess(final LockFreeLinkedListNode p0, final LockFreeLinkedListNode p1);
        
        protected abstract LockFreeLinkedListNode getAffectedNode();
        
        protected abstract LockFreeLinkedListNode getOriginalNext();
        
        protected abstract Object onPrepare(final LockFreeLinkedListNode p0, final LockFreeLinkedListNode p1);
        
        @Override
        public final Object prepare(final AtomicOp<?> atomicOp) {
            Intrinsics.checkParameterIsNotNull(atomicOp, "op");
            while (true) {
                final LockFreeLinkedListNode takeAffectedNode = this.takeAffectedNode(atomicOp);
                final Object next = takeAffectedNode._next;
                if (next == atomicOp) {
                    return null;
                }
                if (atomicOp.isDecided()) {
                    return null;
                }
                if (next instanceof OpDescriptor) {
                    ((AtomicOp<? super LockFreeLinkedListNode>)next).perform(takeAffectedNode);
                }
                else {
                    final Object failure = this.failure(takeAffectedNode);
                    if (failure != null) {
                        return failure;
                    }
                    if (this.retry(takeAffectedNode, next)) {
                        continue;
                    }
                    if (next == null) {
                        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                    }
                    final PrepareOp prepareOp = new PrepareOp((LockFreeLinkedListNode)next, (AtomicOp<? super LockFreeLinkedListNode>)atomicOp, this);
                    if (!LockFreeLinkedListNode._next$FU.compareAndSet(takeAffectedNode, next, prepareOp)) {
                        continue;
                    }
                    final Object perform = prepareOp.perform(takeAffectedNode);
                    if (perform == LockFreeLinkedListKt.access$getREMOVE_PREPARED$p()) {
                        continue;
                    }
                    return perform;
                }
            }
        }
        
        protected boolean retry(final LockFreeLinkedListNode lockFreeLinkedListNode, final Object o) {
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "affected");
            Intrinsics.checkParameterIsNotNull(o, "next");
            return false;
        }
        
        protected LockFreeLinkedListNode takeAffectedNode(final OpDescriptor opDescriptor) {
            Intrinsics.checkParameterIsNotNull(opDescriptor, "op");
            final LockFreeLinkedListNode affectedNode = this.getAffectedNode();
            if (affectedNode == null) {
                Intrinsics.throwNpe();
            }
            return affectedNode;
        }
        
        protected abstract Object updatedNext(final LockFreeLinkedListNode p0, final LockFreeLinkedListNode p1);
        
        @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B+\u0012\n\u0010\u0002\u001a\u00060\u0003j\u0002`\u0004\u0012\u0010\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\u0014\u0010\n\u001a\u0004\u0018\u00010\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u000bH\u0016R\u0010\u0010\u0007\u001a\u00020\b8\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u00060\u0003j\u0002`\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\f\u0012\b\u0012\u00060\u0003j\u0002`\u00040\u00068\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\r" }, d2 = { "Lkotlinx/coroutines/internal/LockFreeLinkedListNode$AbstractAtomicDesc$PrepareOp;", "Lkotlinx/coroutines/internal/OpDescriptor;", "next", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "Lkotlinx/coroutines/internal/Node;", "op", "Lkotlinx/coroutines/internal/AtomicOp;", "desc", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode$AbstractAtomicDesc;", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;Lkotlinx/coroutines/internal/AtomicOp;Lkotlinx/coroutines/internal/LockFreeLinkedListNode$AbstractAtomicDesc;)V", "perform", "", "affected", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
        private static final class PrepareOp extends OpDescriptor
        {
            public final AbstractAtomicDesc desc;
            public final LockFreeLinkedListNode next;
            public final AtomicOp<LockFreeLinkedListNode> op;
            
            public PrepareOp(final LockFreeLinkedListNode next, final AtomicOp<? super LockFreeLinkedListNode> op, final AbstractAtomicDesc desc) {
                Intrinsics.checkParameterIsNotNull(next, "next");
                Intrinsics.checkParameterIsNotNull(op, "op");
                Intrinsics.checkParameterIsNotNull(desc, "desc");
                this.next = next;
                this.op = (AtomicOp<LockFreeLinkedListNode>)op;
                this.desc = desc;
            }
            
            @Override
            public Object perform(Object onPrepare) {
                if (onPrepare == null) {
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                }
                final LockFreeLinkedListNode lockFreeLinkedListNode = (LockFreeLinkedListNode)onPrepare;
                onPrepare = this.desc.onPrepare(lockFreeLinkedListNode, this.next);
                if (onPrepare != null) {
                    if (onPrepare == LockFreeLinkedListKt.access$getREMOVE_PREPARED$p()) {
                        if (LockFreeLinkedListNode._next$FU.compareAndSet(lockFreeLinkedListNode, this, this.next.removed())) {
                            lockFreeLinkedListNode.helpDelete();
                            return onPrepare;
                        }
                    }
                    else {
                        this.op.tryDecide(onPrepare);
                        LockFreeLinkedListNode._next$FU.compareAndSet(lockFreeLinkedListNode, this, this.next);
                    }
                    return onPrepare;
                }
                Object o;
                if (this.op.isDecided()) {
                    o = this.next;
                }
                else {
                    o = this.op;
                }
                LockFreeLinkedListNode._next$FU.compareAndSet(lockFreeLinkedListNode, this, o);
                return null;
            }
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0016\u0018\u0000*\f\b\u0000\u0010\u0003*\u00060\u0001j\u0002`\u00022\u00020\u0004B\u001b\u0012\n\u0010\u0005\u001a\u00060\u0001j\u0002`\u0002\u0012\u0006\u0010\u0006\u001a\u00028\u0000¢\u0006\u0004\b\u0007\u0010\bJ'\u0010\f\u001a\u00020\u000b2\n\u0010\t\u001a\u00060\u0001j\u0002`\u00022\n\u0010\n\u001a\u00060\u0001j\u0002`\u0002H\u0014¢\u0006\u0004\b\f\u0010\bJ)\u0010\u000e\u001a\u0004\u0018\u00010\r2\n\u0010\t\u001a\u00060\u0001j\u0002`\u00022\n\u0010\n\u001a\u00060\u0001j\u0002`\u0002H\u0014¢\u0006\u0004\b\u000e\u0010\u000fJ#\u0010\u0011\u001a\u00020\u00102\n\u0010\t\u001a\u00060\u0001j\u0002`\u00022\u0006\u0010\n\u001a\u00020\rH\u0014¢\u0006\u0004\b\u0011\u0010\u0012J\u001b\u0010\u0015\u001a\u00060\u0001j\u0002`\u00022\u0006\u0010\u0014\u001a\u00020\u0013H\u0004¢\u0006\u0004\b\u0015\u0010\u0016J'\u0010\u0017\u001a\u00020\r2\n\u0010\t\u001a\u00060\u0001j\u0002`\u00022\n\u0010\n\u001a\u00060\u0001j\u0002`\u0002H\u0014¢\u0006\u0004\b\u0017\u0010\u000fR\u001e\u0010\u001a\u001a\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00028D@\u0004X\u0084\u0004¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019R\u0016\u0010\u0006\u001a\u00028\u00008\u0006@\u0007X\u0087\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u001bR\u001e\u0010\u001d\u001a\n\u0018\u00010\u0001j\u0004\u0018\u0001`\u00028D@\u0004X\u0084\u0004¢\u0006\u0006\u001a\u0004\b\u001c\u0010\u0019R\u001a\u0010\u0005\u001a\u00060\u0001j\u0002`\u00028\u0006@\u0007X\u0087\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u001b¨\u0006\u001e" }, d2 = { "Lkotlinx/coroutines/internal/LockFreeLinkedListNode$AddLastDesc;", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "Lkotlinx/coroutines/internal/Node;", "T", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode$AbstractAtomicDesc;", "queue", "node", "<init>", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;Lkotlinx/coroutines/internal/LockFreeLinkedListNode;)V", "affected", "next", "", "finishOnSuccess", "", "onPrepare", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;Lkotlinx/coroutines/internal/LockFreeLinkedListNode;)Ljava/lang/Object;", "", "retry", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;Ljava/lang/Object;)Z", "Lkotlinx/coroutines/internal/OpDescriptor;", "op", "takeAffectedNode", "(Lkotlinx/coroutines/internal/OpDescriptor;)Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "updatedNext", "getAffectedNode", "()Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "affectedNode", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "getOriginalNext", "originalNext", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    public static class AddLastDesc<T extends LockFreeLinkedListNode> extends AbstractAtomicDesc
    {
        private static final AtomicReferenceFieldUpdater _affectedNode$FU;
        private volatile Object _affectedNode;
        public final T node;
        public final LockFreeLinkedListNode queue;
        
        static {
            _affectedNode$FU = AtomicReferenceFieldUpdater.newUpdater(AddLastDesc.class, Object.class, "_affectedNode");
        }
        
        public AddLastDesc(final LockFreeLinkedListNode queue, final T node) {
            Intrinsics.checkParameterIsNotNull(queue, "queue");
            Intrinsics.checkParameterIsNotNull(node, "node");
            this.queue = queue;
            this.node = node;
            if (DebugKt.getASSERTIONS_ENABLED()) {
                final Object next = this.node._next;
                final LockFreeLinkedListNode node2 = this.node;
                if (next != node2 || node2._prev != this.node) {
                    throw new AssertionError();
                }
            }
            this._affectedNode = null;
        }
        
        @Override
        protected void finishOnSuccess(final LockFreeLinkedListNode lockFreeLinkedListNode, final LockFreeLinkedListNode lockFreeLinkedListNode2) {
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "affected");
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode2, "next");
            this.node.finishAdd(this.queue);
        }
        
        @Override
        protected final LockFreeLinkedListNode getAffectedNode() {
            return (LockFreeLinkedListNode)this._affectedNode;
        }
        
        @Override
        protected final LockFreeLinkedListNode getOriginalNext() {
            return this.queue;
        }
        
        @Override
        protected Object onPrepare(final LockFreeLinkedListNode lockFreeLinkedListNode, final LockFreeLinkedListNode lockFreeLinkedListNode2) {
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "affected");
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode2, "next");
            AddLastDesc._affectedNode$FU.compareAndSet(this, null, lockFreeLinkedListNode);
            return null;
        }
        
        @Override
        protected boolean retry(final LockFreeLinkedListNode lockFreeLinkedListNode, final Object o) {
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "affected");
            Intrinsics.checkParameterIsNotNull(o, "next");
            return o != this.queue;
        }
        
        @Override
        protected final LockFreeLinkedListNode takeAffectedNode(final OpDescriptor opDescriptor) {
            Intrinsics.checkParameterIsNotNull(opDescriptor, "op");
            while (true) {
                final Object prev = this.queue._prev;
                if (prev == null) {
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                }
                final LockFreeLinkedListNode lockFreeLinkedListNode = (LockFreeLinkedListNode)prev;
                final Object next = lockFreeLinkedListNode._next;
                final LockFreeLinkedListNode queue = this.queue;
                if (next == queue) {
                    return lockFreeLinkedListNode;
                }
                if (next == opDescriptor) {
                    return lockFreeLinkedListNode;
                }
                if (next instanceof OpDescriptor) {
                    ((OpDescriptor)next).perform(lockFreeLinkedListNode);
                }
                else {
                    final LockFreeLinkedListNode access$correctPrev = queue.correctPrev(lockFreeLinkedListNode, opDescriptor);
                    if (access$correctPrev != null) {
                        return access$correctPrev;
                    }
                    continue;
                }
            }
        }
        
        @Override
        protected Object updatedNext(LockFreeLinkedListNode node, LockFreeLinkedListNode node2) {
            Intrinsics.checkParameterIsNotNull(node, "affected");
            Intrinsics.checkParameterIsNotNull(node2, "next");
            node2 = this.node;
            LockFreeLinkedListNode._prev$FU.compareAndSet(node2, this.node, node);
            node = this.node;
            LockFreeLinkedListNode._next$FU.compareAndSet(node, this.node, this.queue);
            return this.node;
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\b!\u0018\u00002\f\u0012\b\u0012\u00060\u0002j\u0002`\u00030\u0001B\u0011\u0012\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u0003¢\u0006\u0002\u0010\u0005J\u001e\u0010\u0007\u001a\u00020\b2\n\u0010\t\u001a\u00060\u0002j\u0002`\u00032\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0016R\u0014\u0010\u0004\u001a\u00060\u0002j\u0002`\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00038\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000¨\u0006\f" }, d2 = { "Lkotlinx/coroutines/internal/LockFreeLinkedListNode$CondAddOp;", "Lkotlinx/coroutines/internal/AtomicOp;", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "Lkotlinx/coroutines/internal/Node;", "newNode", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;)V", "oldNext", "complete", "", "affected", "failure", "", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    public abstract static class CondAddOp extends AtomicOp<LockFreeLinkedListNode>
    {
        public final LockFreeLinkedListNode newNode;
        public LockFreeLinkedListNode oldNext;
        
        public CondAddOp(final LockFreeLinkedListNode newNode) {
            Intrinsics.checkParameterIsNotNull(newNode, "newNode");
            this.newNode = newNode;
        }
        
        @Override
        public void complete(LockFreeLinkedListNode newNode, final Object o) {
            Intrinsics.checkParameterIsNotNull(newNode, "affected");
            final boolean b = o == null;
            LockFreeLinkedListNode lockFreeLinkedListNode;
            if (b) {
                lockFreeLinkedListNode = this.newNode;
            }
            else {
                lockFreeLinkedListNode = this.oldNext;
            }
            if (lockFreeLinkedListNode != null && LockFreeLinkedListNode._next$FU.compareAndSet(newNode, this, lockFreeLinkedListNode) && b) {
                newNode = this.newNode;
                final LockFreeLinkedListNode oldNext = this.oldNext;
                if (oldNext == null) {
                    Intrinsics.throwNpe();
                }
                newNode.finishAdd(oldNext);
            }
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u00006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0013\b\u0016\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B\u0013\u0012\n\u0010\u0005\u001a\u00060\u0003j\u0002`\u0004¢\u0006\u0004\b\u0006\u0010\u0007J\u001d\u0010\n\u001a\u0004\u0018\u00010\t2\n\u0010\b\u001a\u00060\u0003j\u0002`\u0004H\u0014¢\u0006\u0004\b\n\u0010\u000bJ'\u0010\u000e\u001a\u00020\r2\n\u0010\b\u001a\u00060\u0003j\u0002`\u00042\n\u0010\f\u001a\u00060\u0003j\u0002`\u0004H\u0004¢\u0006\u0004\b\u000e\u0010\u000fJ)\u0010\u0010\u001a\u0004\u0018\u00010\t2\n\u0010\b\u001a\u00060\u0003j\u0002`\u00042\n\u0010\f\u001a\u00060\u0003j\u0002`\u0004H\u0004¢\u0006\u0004\b\u0010\u0010\u0011J#\u0010\u0013\u001a\u00020\u00122\n\u0010\b\u001a\u00060\u0003j\u0002`\u00042\u0006\u0010\f\u001a\u00020\tH\u0004¢\u0006\u0004\b\u0013\u0010\u0014J\u001b\u0010\u0017\u001a\u00060\u0003j\u0002`\u00042\u0006\u0010\u0016\u001a\u00020\u0015H\u0004¢\u0006\u0004\b\u0017\u0010\u0018J'\u0010\u0019\u001a\u00020\t2\n\u0010\b\u001a\u00060\u0003j\u0002`\u00042\n\u0010\f\u001a\u00060\u0003j\u0002`\u0004H\u0004¢\u0006\u0004\b\u0019\u0010\u0011J\u0017\u0010\u001b\u001a\u00020\u00122\u0006\u0010\u001a\u001a\u00028\u0000H\u0014¢\u0006\u0004\b\u001b\u0010\u001cR\u001e\u0010\u001f\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00048D@\u0004X\u0084\u0004¢\u0006\u0006\u001a\u0004\b\u001d\u0010\u001eR\u001e\u0010!\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u00048D@\u0004X\u0084\u0004¢\u0006\u0006\u001a\u0004\b \u0010\u001eR\u001a\u0010\u0005\u001a\u00060\u0003j\u0002`\u00048\u0006@\u0007X\u0087\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\"R\u0019\u0010'\u001a\u00028\u00008F@\u0006¢\u0006\f\u0012\u0004\b%\u0010&\u001a\u0004\b#\u0010$¨\u0006(" }, d2 = { "Lkotlinx/coroutines/internal/LockFreeLinkedListNode$RemoveFirstDesc;", "T", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode$AbstractAtomicDesc;", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "Lkotlinx/coroutines/internal/Node;", "queue", "<init>", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;)V", "affected", "", "failure", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;)Ljava/lang/Object;", "next", "", "finishOnSuccess", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;Lkotlinx/coroutines/internal/LockFreeLinkedListNode;)V", "onPrepare", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;Lkotlinx/coroutines/internal/LockFreeLinkedListNode;)Ljava/lang/Object;", "", "retry", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;Ljava/lang/Object;)Z", "Lkotlinx/coroutines/internal/OpDescriptor;", "op", "takeAffectedNode", "(Lkotlinx/coroutines/internal/OpDescriptor;)Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "updatedNext", "node", "validatePrepared", "(Ljava/lang/Object;)Z", "getAffectedNode", "()Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "affectedNode", "getOriginalNext", "originalNext", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "getResult", "()Ljava/lang/Object;", "result$annotations", "()V", "result", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    public static class RemoveFirstDesc<T> extends AbstractAtomicDesc
    {
        private static final AtomicReferenceFieldUpdater _affectedNode$FU;
        private static final AtomicReferenceFieldUpdater _originalNext$FU;
        private volatile Object _affectedNode;
        private volatile Object _originalNext;
        public final LockFreeLinkedListNode queue;
        
        static {
            _affectedNode$FU = AtomicReferenceFieldUpdater.newUpdater(RemoveFirstDesc.class, Object.class, "_affectedNode");
            _originalNext$FU = AtomicReferenceFieldUpdater.newUpdater(RemoveFirstDesc.class, Object.class, "_originalNext");
        }
        
        public RemoveFirstDesc(final LockFreeLinkedListNode queue) {
            Intrinsics.checkParameterIsNotNull(queue, "queue");
            this.queue = queue;
            this._affectedNode = null;
            this._originalNext = null;
        }
        
        @Override
        protected Object failure(final LockFreeLinkedListNode lockFreeLinkedListNode) {
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "affected");
            if (lockFreeLinkedListNode == this.queue) {
                return LockFreeLinkedListKt.getLIST_EMPTY();
            }
            return null;
        }
        
        @Override
        protected final void finishOnSuccess(final LockFreeLinkedListNode lockFreeLinkedListNode, final LockFreeLinkedListNode lockFreeLinkedListNode2) {
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "affected");
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode2, "next");
            lockFreeLinkedListNode.finishRemove(lockFreeLinkedListNode2);
        }
        
        @Override
        protected final LockFreeLinkedListNode getAffectedNode() {
            return (LockFreeLinkedListNode)this._affectedNode;
        }
        
        @Override
        protected final LockFreeLinkedListNode getOriginalNext() {
            return (LockFreeLinkedListNode)this._originalNext;
        }
        
        public final T getResult() {
            final LockFreeLinkedListNode affectedNode = this.getAffectedNode();
            if (affectedNode == null) {
                Intrinsics.throwNpe();
            }
            return (T)affectedNode;
        }
        
        @Override
        protected final Object onPrepare(final LockFreeLinkedListNode lockFreeLinkedListNode, final LockFreeLinkedListNode lockFreeLinkedListNode2) {
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "affected");
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode2, "next");
            if (DebugKt.getASSERTIONS_ENABLED() && !(lockFreeLinkedListNode instanceof LockFreeLinkedListHead ^ true)) {
                throw new AssertionError();
            }
            if (!this.validatePrepared(lockFreeLinkedListNode)) {
                return LockFreeLinkedListKt.access$getREMOVE_PREPARED$p();
            }
            RemoveFirstDesc._affectedNode$FU.compareAndSet(this, null, lockFreeLinkedListNode);
            RemoveFirstDesc._originalNext$FU.compareAndSet(this, null, lockFreeLinkedListNode2);
            return null;
        }
        
        @Override
        protected final boolean retry(final LockFreeLinkedListNode lockFreeLinkedListNode, final Object o) {
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "affected");
            Intrinsics.checkParameterIsNotNull(o, "next");
            if (!(o instanceof Removed)) {
                return false;
            }
            lockFreeLinkedListNode.helpDelete();
            return true;
        }
        
        @Override
        protected final LockFreeLinkedListNode takeAffectedNode(final OpDescriptor opDescriptor) {
            Intrinsics.checkParameterIsNotNull(opDescriptor, "op");
            final Object next = this.queue.getNext();
            if (next != null) {
                return (LockFreeLinkedListNode)next;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
        }
        
        @Override
        protected final Object updatedNext(final LockFreeLinkedListNode lockFreeLinkedListNode, final LockFreeLinkedListNode lockFreeLinkedListNode2) {
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "affected");
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode2, "next");
            return lockFreeLinkedListNode2.removed();
        }
        
        protected boolean validatePrepared(final T t) {
            return true;
        }
    }
}
