// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.internal;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0002\u001a$\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u00022\u000e\u0010\u0003\u001a\n\u0012\u0006\u0012\u0004\u0018\u0001H\u00020\u0004H\u0080\b¢\u0006\u0002\u0010\u0005¨\u0006\u0006" }, d2 = { "clear", "", "T", "a", "", "([Ljava/lang/Object;)V", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class ThreadSafeHeapKt
{
    public static final <T> void clear(final T[] array) {
        Intrinsics.checkParameterIsNotNull(array, "a");
        ArraysKt___ArraysJvmKt.fill$default(array, null, 0, 0, 6, null);
    }
}
