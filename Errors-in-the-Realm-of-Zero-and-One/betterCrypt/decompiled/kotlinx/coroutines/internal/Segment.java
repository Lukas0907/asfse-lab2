// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.internal;

import kotlinx.coroutines.DebugKt;
import kotlin.TypeCastException;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u000e\b \u0018\u0000*\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u00028\u00000\u00002\u00020\u0002B\u0019\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00018\u0000¢\u0006\u0004\b\u0006\u0010\u0007J!\u0010\u000b\u001a\u00020\n2\b\u0010\b\u001a\u0004\u0018\u00018\u00002\b\u0010\t\u001a\u0004\u0018\u00018\u0000¢\u0006\u0004\b\u000b\u0010\fJ\u0017\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\r\u001a\u00028\u0000H\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0005\u001a\u00028\u0000H\u0002¢\u0006\u0004\b\u0011\u0010\u0010J\r\u0010\u0012\u001a\u00020\u000e¢\u0006\u0004\b\u0012\u0010\u0013R\u0019\u0010\u0004\u001a\u00020\u00038\u0006@\u0006¢\u0006\f\n\u0004\b\u0004\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u0015\u0010\r\u001a\u0004\u0018\u00018\u00008F@\u0006¢\u0006\u0006\u001a\u0004\b\u0017\u0010\u0018R\u0016\u0010\u001b\u001a\u00020\n8&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u001a¨\u0006\u001c" }, d2 = { "Lkotlinx/coroutines/internal/Segment;", "S", "", "", "id", "prev", "<init>", "(JLkotlinx/coroutines/internal/Segment;)V", "expected", "value", "", "casNext", "(Lkotlinx/coroutines/internal/Segment;Lkotlinx/coroutines/internal/Segment;)Z", "next", "", "moveNextToRight", "(Lkotlinx/coroutines/internal/Segment;)V", "movePrevToLeft", "remove", "()V", "J", "getId", "()J", "getNext", "()Lkotlinx/coroutines/internal/Segment;", "getRemoved", "()Z", "removed", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public abstract class Segment<S extends Segment<S>>
{
    private static final AtomicReferenceFieldUpdater _next$FU;
    static final AtomicReferenceFieldUpdater prev$FU;
    private volatile Object _next;
    private final long id;
    volatile Object prev;
    
    static {
        _next$FU = AtomicReferenceFieldUpdater.newUpdater(Segment.class, Object.class, "_next");
        prev$FU = AtomicReferenceFieldUpdater.newUpdater(Segment.class, Object.class, "prev");
    }
    
    public Segment(final long id, final S prev) {
        this.id = id;
        this._next = null;
        this.prev = null;
        this.prev = prev;
    }
    
    private final void moveNextToRight(final S n) {
        Segment segment;
        do {
            final Object next = this._next;
            if (next == null) {
                throw new TypeCastException("null cannot be cast to non-null type S");
            }
            segment = (Segment)next;
            if (n.id <= segment.id) {
                return;
            }
        } while (!Segment._next$FU.compareAndSet(this, segment, n));
    }
    
    private final void movePrevToLeft(final S n) {
        Segment segment;
        do {
            segment = (Segment)this.prev;
            if (segment == null) {
                break;
            }
            if (segment.id <= n.id) {
                return;
            }
        } while (!Segment.prev$FU.compareAndSet(this, segment, n));
    }
    
    public final boolean casNext(final S n, final S n2) {
        return Segment._next$FU.compareAndSet(this, n, n2);
    }
    
    public final long getId() {
        return this.id;
    }
    
    public final S getNext() {
        return (S)this._next;
    }
    
    public abstract boolean getRemoved();
    
    public final void remove() {
        if (DebugKt.getASSERTIONS_ENABLED() && !this.getRemoved()) {
            throw new AssertionError();
        }
        Segment<Segment> next = (Segment<Segment>)this._next;
        if (next != null) {
            Segment<Segment<Segment>> segment = (Segment<Segment<Segment>>)this.prev;
            if (segment != null) {
                segment.moveNextToRight(next);
                while (segment.getRemoved()) {
                    final Segment segment2 = (Segment)segment.prev;
                    if (segment2 == null) {
                        break;
                    }
                    segment2.moveNextToRight(next);
                    segment = (Segment<Segment<Segment>>)segment2;
                }
                next.movePrevToLeft(segment);
                while (next.getRemoved()) {
                    next = next.getNext();
                    if (next == null) {
                        break;
                    }
                    next.movePrevToLeft(segment);
                }
            }
        }
    }
}
