// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0011\u0010\u0000\u001a\u00020\u0001H\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0002\u001a\f\u0010\u0003\u001a\u00020\u0001*\u00020\u0004H\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0005" }, d2 = { "yield", "", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "checkCompletion", "Lkotlin/coroutines/CoroutineContext;", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class YieldKt
{
    public static final void checkCompletion(final CoroutineContext coroutineContext) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "$this$checkCompletion");
        final Job job = coroutineContext.get((CoroutineContext.Key<Job>)Job.Key);
        if (job == null) {
            return;
        }
        if (job.isActive()) {
            return;
        }
        throw job.getCancellationException();
    }
    
    public static final Object yield(final Continuation<? super Unit> continuation) {
        final CoroutineContext context = continuation.getContext();
        checkCompletion(context);
        Continuation<Unit> intercepted;
        if (!((intercepted = IntrinsicsKt__IntrinsicsJvmKt.intercepted(continuation)) instanceof DispatchedContinuation)) {
            intercepted = null;
        }
        final DispatchedContinuation<Unit> dispatchedContinuation = (DispatchedContinuation<Unit>)intercepted;
        Object o;
        if (dispatchedContinuation != null) {
            if (!dispatchedContinuation.dispatcher.isDispatchNeeded(context)) {
                if (DispatchedKt.yieldUndispatched(dispatchedContinuation)) {
                    o = IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
                }
                else {
                    o = Unit.INSTANCE;
                }
            }
            else {
                dispatchedContinuation.dispatchYield$kotlinx_coroutines_core(Unit.INSTANCE);
                o = IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
            }
        }
        else {
            o = Unit.INSTANCE;
        }
        if (o == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            DebugProbesKt.probeCoroutineSuspended(continuation);
        }
        return o;
    }
}
