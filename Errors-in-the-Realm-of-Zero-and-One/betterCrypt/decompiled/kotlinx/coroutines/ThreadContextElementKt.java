// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.jvm.internal.InlineMarker;
import kotlin.coroutines.jvm.internal.Boxing;
import kotlinx.coroutines.internal.ThreadLocalKey;
import kotlin.coroutines.CoroutineContext;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.internal.ThreadLocalElement;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\u001a+\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u0002H\u0002¢\u0006\u0002\u0010\u0005\u001a\u0019\u0010\u0006\u001a\u00020\u0007*\u0006\u0012\u0002\b\u00030\u0003H\u0086H\u00f8\u0001\u0000¢\u0006\u0002\u0010\b\u001a\u0019\u0010\t\u001a\u00020\n*\u0006\u0012\u0002\b\u00030\u0003H\u0086H\u00f8\u0001\u0000¢\u0006\u0002\u0010\b\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u000b" }, d2 = { "asContextElement", "Lkotlinx/coroutines/ThreadContextElement;", "T", "Ljava/lang/ThreadLocal;", "value", "(Ljava/lang/ThreadLocal;Ljava/lang/Object;)Lkotlinx/coroutines/ThreadContextElement;", "ensurePresent", "", "(Ljava/lang/ThreadLocal;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "isPresent", "", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class ThreadContextElementKt
{
    public static final <T> ThreadContextElement<T> asContextElement(final ThreadLocal<T> threadLocal, final T t) {
        Intrinsics.checkParameterIsNotNull(threadLocal, "$this$asContextElement");
        return new ThreadLocalElement<T>(t, threadLocal);
    }
    
    public static final Object ensurePresent(final ThreadLocal<?> obj, final Continuation<? super Unit> continuation) {
        if (Boxing.boxBoolean(continuation.getContext().get((CoroutineContext.Key<CoroutineContext.Element>)new ThreadLocalKey(obj)) != null)) {
            return Unit.INSTANCE;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("ThreadLocal ");
        sb.append(obj);
        sb.append(" is missing from context ");
        sb.append(continuation.getContext());
        throw new IllegalStateException(sb.toString().toString());
    }
    
    private static final Object ensurePresent$$forInline(final ThreadLocal threadLocal, final Continuation continuation) {
        InlineMarker.mark(3);
        throw new NullPointerException();
    }
    
    public static final Object isPresent(final ThreadLocal<?> threadLocal, final Continuation<? super Boolean> continuation) {
        return Boxing.boxBoolean(continuation.getContext().get((CoroutineContext.Key<CoroutineContext.Element>)new ThreadLocalKey(threadLocal)) != null);
    }
    
    private static final Object isPresent$$forInline(final ThreadLocal threadLocal, final Continuation continuation) {
        InlineMarker.mark(3);
        throw new NullPointerException();
    }
}
