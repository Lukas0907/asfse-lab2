// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function2;
import kotlin.Metadata;
import kotlin.DeprecationLevel;
import kotlin.Deprecated;

@Deprecated(level = DeprecationLevel.ERROR, message = "This is internal API and may be removed in the future releases")
@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bg\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H'¨\u0006\u0006" }, d2 = { "Lkotlinx/coroutines/ChildJob;", "Lkotlinx/coroutines/Job;", "parentCancelled", "", "parentJob", "Lkotlinx/coroutines/ParentJob;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public interface ChildJob extends Job
{
    void parentCancelled(final ParentJob p0);
    
    @Metadata(bv = { 1, 0, 3 }, k = 3, mv = { 1, 1, 15 })
    public static final class DefaultImpls
    {
        public static <R> R fold(final ChildJob childJob, final R r, final Function2<? super R, ? super Element, ? extends R> function2) {
            Intrinsics.checkParameterIsNotNull(function2, "operation");
            return Job.DefaultImpls.fold(childJob, r, function2);
        }
        
        public static <E extends Element> E get(final ChildJob childJob, final CoroutineContext.Key<E> key) {
            Intrinsics.checkParameterIsNotNull(key, "key");
            return Job.DefaultImpls.get(childJob, key);
        }
        
        public static CoroutineContext minusKey(final ChildJob childJob, final CoroutineContext.Key<?> key) {
            Intrinsics.checkParameterIsNotNull(key, "key");
            return Job.DefaultImpls.minusKey(childJob, key);
        }
        
        public static CoroutineContext plus(final ChildJob childJob, final CoroutineContext coroutineContext) {
            Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
            return Job.DefaultImpls.plus(childJob, coroutineContext);
        }
        
        @Deprecated(level = DeprecationLevel.ERROR, message = "Operator '+' on two Job objects is meaningless. Job is a coroutine context element and `+` is a set-sum operator for coroutine contexts. The job to the right of `+` just replaces the job the left of `+`.")
        public static Job plus(final ChildJob childJob, final Job job) {
            Intrinsics.checkParameterIsNotNull(job, "other");
            return Job.DefaultImpls.plus(childJob, job);
        }
    }
}
