// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import java.util.concurrent.CancellationException;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0000\u0018\u00002\u00060\u0001j\u0002`\u00022\b\u0012\u0004\u0012\u00020\u00000\u0003B\u001f\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\n\u0010\u000b\u001a\u0004\u0018\u00010\u0000H\u0016J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0096\u0002J\b\u0010\u0010\u001a\u00020\u0007H\u0016J\b\u0010\u0011\u001a\u00020\u0012H\u0016J\b\u0010\u0013\u001a\u00020\u0005H\u0016R\u0010\u0010\b\u001a\u00020\t8\u0000X\u0081\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014" }, d2 = { "Lkotlinx/coroutines/JobCancellationException;", "Ljava/util/concurrent/CancellationException;", "Lkotlinx/coroutines/CancellationException;", "Lkotlinx/coroutines/CopyableThrowable;", "message", "", "cause", "", "job", "Lkotlinx/coroutines/Job;", "(Ljava/lang/String;Ljava/lang/Throwable;Lkotlinx/coroutines/Job;)V", "createCopy", "equals", "", "other", "", "fillInStackTrace", "hashCode", "", "toString", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class JobCancellationException extends CancellationException implements CopyableThrowable<JobCancellationException>
{
    public final Job job;
    
    public JobCancellationException(final String message, final Throwable cause, final Job job) {
        Intrinsics.checkParameterIsNotNull(message, "message");
        Intrinsics.checkParameterIsNotNull(job, "job");
        super(message);
        this.job = job;
        if (cause != null) {
            this.initCause(cause);
        }
    }
    
    @Override
    public JobCancellationException createCopy() {
        if (DebugKt.getDEBUG()) {
            final String message = this.getMessage();
            if (message == null) {
                Intrinsics.throwNpe();
            }
            return new JobCancellationException(message, this, this.job);
        }
        return null;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o != this) {
            if (o instanceof JobCancellationException) {
                final JobCancellationException ex = (JobCancellationException)o;
                if (Intrinsics.areEqual(ex.getMessage(), this.getMessage()) && Intrinsics.areEqual(ex.job, this.job) && Intrinsics.areEqual(ex.getCause(), this.getCause())) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }
    
    @Override
    public Throwable fillInStackTrace() {
        if (DebugKt.getDEBUG()) {
            final Throwable fillInStackTrace = super.fillInStackTrace();
            Intrinsics.checkExpressionValueIsNotNull(fillInStackTrace, "super.fillInStackTrace()");
            return fillInStackTrace;
        }
        return this;
    }
    
    @Override
    public int hashCode() {
        final String message = this.getMessage();
        if (message == null) {
            Intrinsics.throwNpe();
        }
        final int hashCode = message.hashCode();
        final int hashCode2 = this.job.hashCode();
        final Throwable cause = this.getCause();
        int hashCode3;
        if (cause != null) {
            hashCode3 = cause.hashCode();
        }
        else {
            hashCode3 = 0;
        }
        return (hashCode * 31 + hashCode2) * 31 + hashCode3;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("; job=");
        sb.append(this.job);
        return sb.toString();
    }
}
