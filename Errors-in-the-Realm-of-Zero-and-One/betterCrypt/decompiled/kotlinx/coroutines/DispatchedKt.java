// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlinx.coroutines.internal.ThreadContextKt;
import kotlin.ResultKt;
import kotlin.Result;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.internal.StackTraceRecoveryKt;
import kotlin.jvm.internal.InlineMarker;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.Continuation;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlinx.coroutines.internal.Symbol;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000N\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0003\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\"\u0010\u0004\u001a\u00020\u0005\"\u0004\b\u0000\u0010\u0006*\b\u0012\u0004\u0012\u0002H\u00060\u00072\b\b\u0002\u0010\b\u001a\u00020\tH\u0000\u001a;\u0010\n\u001a\u00020\u000b*\u0006\u0012\u0002\b\u00030\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\u000f\u001a\u00020\u000b2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00050\u0011H\u0082\b\u001a.\u0010\u0012\u001a\u00020\u0005\"\u0004\b\u0000\u0010\u0006*\b\u0012\u0004\u0012\u0002H\u00060\u00072\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u0002H\u00060\u00142\u0006\u0010\u0015\u001a\u00020\tH\u0000\u001a%\u0010\u0016\u001a\u00020\u0005\"\u0004\b\u0000\u0010\u0006*\b\u0012\u0004\u0012\u0002H\u00060\u00142\u0006\u0010\u0017\u001a\u0002H\u0006H\u0000¢\u0006\u0002\u0010\u0018\u001a \u0010\u0019\u001a\u00020\u0005\"\u0004\b\u0000\u0010\u0006*\b\u0012\u0004\u0012\u0002H\u00060\u00142\u0006\u0010\u001a\u001a\u00020\u001bH\u0000\u001a%\u0010\u001c\u001a\u00020\u0005\"\u0004\b\u0000\u0010\u0006*\b\u0012\u0004\u0012\u0002H\u00060\u00142\u0006\u0010\u0017\u001a\u0002H\u0006H\u0000¢\u0006\u0002\u0010\u0018\u001a \u0010\u001d\u001a\u00020\u0005\"\u0004\b\u0000\u0010\u0006*\b\u0012\u0004\u0012\u0002H\u00060\u00142\u0006\u0010\u001a\u001a\u00020\u001bH\u0000\u001a\u0010\u0010\u001e\u001a\u00020\u0005*\u0006\u0012\u0002\b\u00030\u0007H\u0002\u001a\u0019\u0010\u001f\u001a\u00020\u0005*\u0006\u0012\u0002\b\u00030\u00142\u0006\u0010\u001a\u001a\u00020\u001bH\u0080\b\u001a'\u0010 \u001a\u00020\u0005*\u0006\u0012\u0002\b\u00030\u00072\u0006\u0010!\u001a\u00020\"2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00050\u0011H\u0082\b\u001a\u0012\u0010#\u001a\u00020\u000b*\b\u0012\u0004\u0012\u00020\u00050\fH\u0000\"\u0016\u0010\u0000\u001a\u00020\u00018\u0002X\u0083\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u0002\u0010\u0003¨\u0006$" }, d2 = { "UNDEFINED", "Lkotlinx/coroutines/internal/Symbol;", "UNDEFINED$annotations", "()V", "dispatch", "", "T", "Lkotlinx/coroutines/DispatchedTask;", "mode", "", "executeUnconfined", "", "Lkotlinx/coroutines/DispatchedContinuation;", "contState", "", "doYield", "block", "Lkotlin/Function0;", "resume", "delegate", "Lkotlin/coroutines/Continuation;", "useMode", "resumeCancellable", "value", "(Lkotlin/coroutines/Continuation;Ljava/lang/Object;)V", "resumeCancellableWithException", "exception", "", "resumeDirect", "resumeDirectWithException", "resumeUnconfined", "resumeWithStackTrace", "runUnconfinedEventLoop", "eventLoop", "Lkotlinx/coroutines/EventLoop;", "yieldUndispatched", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class DispatchedKt
{
    private static final Symbol UNDEFINED;
    
    static {
        UNDEFINED = new Symbol("UNDEFINED");
    }
    
    public static final <T> void dispatch(final DispatchedTask<? super T> dispatchedTask, final int n) {
        Intrinsics.checkParameterIsNotNull(dispatchedTask, "$this$dispatch");
        final Continuation<? super Object> delegate$kotlinx_coroutines_core = dispatchedTask.getDelegate$kotlinx_coroutines_core();
        if (!ResumeModeKt.isDispatchedMode(n) || !(delegate$kotlinx_coroutines_core instanceof DispatchedContinuation) || ResumeModeKt.isCancellableMode(n) != ResumeModeKt.isCancellableMode(dispatchedTask.resumeMode)) {
            resume((DispatchedTask<? super Object>)dispatchedTask, delegate$kotlinx_coroutines_core, n);
            return;
        }
        final CoroutineDispatcher dispatcher = ((DispatchedContinuation<? super Object>)delegate$kotlinx_coroutines_core).dispatcher;
        final CoroutineContext context = delegate$kotlinx_coroutines_core.getContext();
        if (dispatcher.isDispatchNeeded(context)) {
            dispatcher.dispatch(context, dispatchedTask);
            return;
        }
        resumeUnconfined(dispatchedTask);
    }
    
    private static final boolean executeUnconfined(DispatchedContinuation<?> dispatchedContinuation, final Object state, final int resumeMode, final boolean b, final Function0<Unit> function0) {
        final EventLoop eventLoop$kotlinx_coroutines_core = ThreadLocalEventLoop.INSTANCE.getEventLoop$kotlinx_coroutines_core();
        if (b && eventLoop$kotlinx_coroutines_core.isUnconfinedQueueEmpty()) {
            return false;
        }
        if (eventLoop$kotlinx_coroutines_core.isUnconfinedLoopActive()) {
            dispatchedContinuation._state = state;
            dispatchedContinuation.resumeMode = resumeMode;
            eventLoop$kotlinx_coroutines_core.dispatchUnconfined(dispatchedContinuation);
            return true;
        }
        dispatchedContinuation = dispatchedContinuation;
        eventLoop$kotlinx_coroutines_core.incrementUseCount(true);
        final Throwable t2;
        try {
            function0.invoke();
            while (eventLoop$kotlinx_coroutines_core.processUnconfinedEvent()) {}
            InlineMarker.finallyStart(1);
            return false;
        }
        finally {
            final DispatchedContinuation<?> dispatchedContinuation2 = dispatchedContinuation;
            final Throwable t = t2;
            final Throwable t3 = null;
            dispatchedContinuation2.handleFatalException$kotlinx_coroutines_core(t, t3);
        }
        try {
            final DispatchedContinuation<?> dispatchedContinuation2 = dispatchedContinuation;
            final Throwable t = t2;
            final Throwable t3 = null;
            dispatchedContinuation2.handleFatalException$kotlinx_coroutines_core(t, t3);
            return false;
        }
        finally {
            InlineMarker.finallyStart(1);
            eventLoop$kotlinx_coroutines_core.decrementUseCount(true);
            InlineMarker.finallyEnd(1);
        }
    }
    
    public static final <T> void resume(final DispatchedTask<? super T> dispatchedTask, final Continuation<? super T> continuation, final int n) {
        Intrinsics.checkParameterIsNotNull(dispatchedTask, "$this$resume");
        Intrinsics.checkParameterIsNotNull(continuation, "delegate");
        final Object takeState$kotlinx_coroutines_core = dispatchedTask.takeState$kotlinx_coroutines_core();
        final Throwable exceptionalResult$kotlinx_coroutines_core = dispatchedTask.getExceptionalResult$kotlinx_coroutines_core(takeState$kotlinx_coroutines_core);
        if (exceptionalResult$kotlinx_coroutines_core != null) {
            Throwable recoverStackTrace;
            if (continuation instanceof DispatchedTask) {
                recoverStackTrace = exceptionalResult$kotlinx_coroutines_core;
            }
            else {
                recoverStackTrace = StackTraceRecoveryKt.recoverStackTrace(exceptionalResult$kotlinx_coroutines_core, continuation);
            }
            ResumeModeKt.resumeWithExceptionMode((Continuation<? super Object>)continuation, recoverStackTrace, n);
            return;
        }
        ResumeModeKt.resumeMode((Continuation<? super Object>)continuation, dispatchedTask.getSuccessfulResult$kotlinx_coroutines_core(takeState$kotlinx_coroutines_core), n);
    }
    
    public static final <T> void resumeCancellable(Continuation<? super T> eventLoop$kotlinx_coroutines_core, final T t) {
        Intrinsics.checkParameterIsNotNull(eventLoop$kotlinx_coroutines_core, "$this$resumeCancellable");
        if (eventLoop$kotlinx_coroutines_core instanceof DispatchedContinuation) {
            Object continuation = eventLoop$kotlinx_coroutines_core;
            if (((DispatchedContinuation)continuation).dispatcher.isDispatchNeeded(((DispatchedContinuation)continuation).getContext())) {
                ((DispatchedContinuation)continuation)._state = t;
                ((DispatchedContinuation)continuation).resumeMode = 1;
                ((DispatchedContinuation)continuation).dispatcher.dispatch(((DispatchedContinuation)continuation).getContext(), (Runnable)continuation);
                return;
            }
            eventLoop$kotlinx_coroutines_core = ThreadLocalEventLoop.INSTANCE.getEventLoop$kotlinx_coroutines_core();
            if (eventLoop$kotlinx_coroutines_core.isUnconfinedLoopActive()) {
                ((DispatchedContinuation)continuation)._state = t;
                ((DispatchedContinuation)continuation).resumeMode = 1;
                eventLoop$kotlinx_coroutines_core.dispatchUnconfined((DispatchedTask<?>)continuation);
                return;
            }
            while (true) {
                final DispatchedContinuation<?> dispatchedContinuation = (DispatchedContinuation<?>)continuation;
                eventLoop$kotlinx_coroutines_core.incrementUseCount(true);
                while (true) {
                    Label_0307: {
                        try {
                            final Job job = ((DispatchedContinuation)continuation).getContext().get((CoroutineContext.Key<Job>)Job.Key);
                            if (job != null && !job.isActive()) {
                                final CancellationException ex = job.getCancellationException();
                                final Result.Companion companion = Result.Companion;
                                ((Continuation)continuation).resumeWith(Result.constructor-impl(ResultKt.createFailure(ex)));
                                final int n = 1;
                                if (n == 0) {
                                    final CoroutineContext context = ((DispatchedContinuation)continuation).getContext();
                                    final Object updateThreadContext = ThreadContextKt.updateThreadContext(context, ((DispatchedContinuation)continuation).countOrElement);
                                    try {
                                        continuation = ((DispatchedContinuation)continuation).continuation;
                                        final Result.Companion companion2 = Result.Companion;
                                        ((Continuation)continuation).resumeWith(Result.constructor-impl(t));
                                        final Unit instance = Unit.INSTANCE;
                                    }
                                    finally {
                                        ThreadContextKt.restoreThreadContext(context, updateThreadContext);
                                    }
                                }
                                while (eventLoop$kotlinx_coroutines_core.processUnconfinedEvent()) {}
                                return;
                            }
                            break Label_0307;
                        }
                        finally {
                            final DispatchedContinuation<?> dispatchedContinuation2 = dispatchedContinuation;
                            final T t2 = t;
                            final Throwable t3 = null;
                            dispatchedContinuation2.handleFatalException$kotlinx_coroutines_core((Throwable)t2, t3);
                        }
                        try {
                            final DispatchedContinuation<?> dispatchedContinuation2 = dispatchedContinuation;
                            final T t2 = t;
                            final Throwable t3 = null;
                            dispatchedContinuation2.handleFatalException$kotlinx_coroutines_core((Throwable)t2, t3);
                            return;
                        }
                        finally {
                            eventLoop$kotlinx_coroutines_core.decrementUseCount(true);
                        }
                        break;
                    }
                    final int n = 0;
                    continue;
                }
            }
        }
        final Result.Companion companion3 = Result.Companion;
        ((Continuation)eventLoop$kotlinx_coroutines_core).resumeWith(Result.constructor-impl(t));
    }
    
    public static final <T> void resumeCancellableWithException(Continuation<? super T> eventLoop$kotlinx_coroutines_core, final Throwable t) {
        Intrinsics.checkParameterIsNotNull(eventLoop$kotlinx_coroutines_core, "$this$resumeCancellableWithException");
        Intrinsics.checkParameterIsNotNull(t, "exception");
        if (eventLoop$kotlinx_coroutines_core instanceof DispatchedContinuation) {
            final DispatchedContinuation<?> dispatchedContinuation = (DispatchedContinuation<?>)eventLoop$kotlinx_coroutines_core;
            final CoroutineContext context = dispatchedContinuation.continuation.getContext();
            final boolean b = false;
            final CompletedExceptionally state = new CompletedExceptionally(t, false, 2, null);
            if (dispatchedContinuation.dispatcher.isDispatchNeeded(context)) {
                dispatchedContinuation._state = new CompletedExceptionally(t, false, 2, null);
                dispatchedContinuation.resumeMode = 1;
                dispatchedContinuation.dispatcher.dispatch(context, dispatchedContinuation);
                return;
            }
            eventLoop$kotlinx_coroutines_core = ThreadLocalEventLoop.INSTANCE.getEventLoop$kotlinx_coroutines_core();
            if (eventLoop$kotlinx_coroutines_core.isUnconfinedLoopActive()) {
                dispatchedContinuation._state = state;
                dispatchedContinuation.resumeMode = 1;
                eventLoop$kotlinx_coroutines_core.dispatchUnconfined(dispatchedContinuation);
                return;
            }
            final DispatchedContinuation<?> dispatchedContinuation2 = dispatchedContinuation;
            eventLoop$kotlinx_coroutines_core.incrementUseCount(true);
            try {
                final Job job = dispatchedContinuation.getContext().get((CoroutineContext.Key<Job>)Job.Key);
                int n = b ? 1 : 0;
                if (job != null) {
                    n = (b ? 1 : 0);
                    if (!job.isActive()) {
                        final CancellationException ex = job.getCancellationException();
                        final Result.Companion companion = Result.Companion;
                        dispatchedContinuation.resumeWith(Result.constructor-impl(ResultKt.createFailure(ex)));
                        n = 1;
                    }
                }
                if (n == 0) {
                    final CoroutineContext context2 = dispatchedContinuation.getContext();
                    final Object updateThreadContext = ThreadContextKt.updateThreadContext(context2, dispatchedContinuation.countOrElement);
                    try {
                        final Continuation<?> continuation = dispatchedContinuation.continuation;
                        final Result.Companion companion2 = Result.Companion;
                        continuation.resumeWith(Result.constructor-impl(ResultKt.createFailure(StackTraceRecoveryKt.recoverStackTrace(t, continuation))));
                        final Unit instance = Unit.INSTANCE;
                    }
                    finally {
                        ThreadContextKt.restoreThreadContext(context2, updateThreadContext);
                    }
                }
                while (eventLoop$kotlinx_coroutines_core.processUnconfinedEvent()) {}
                return;
            }
            finally {
                final DispatchedContinuation<?> dispatchedContinuation3 = dispatchedContinuation2;
                final Throwable t2 = t;
                final Throwable t3 = null;
                dispatchedContinuation3.handleFatalException$kotlinx_coroutines_core(t2, t3);
            }
            try {
                final DispatchedContinuation<?> dispatchedContinuation3 = dispatchedContinuation2;
                final Throwable t2 = t;
                final Throwable t3 = null;
                dispatchedContinuation3.handleFatalException$kotlinx_coroutines_core(t2, t3);
                return;
            }
            finally {
                eventLoop$kotlinx_coroutines_core.decrementUseCount(true);
            }
        }
        final Result.Companion companion3 = Result.Companion;
        ((Continuation)eventLoop$kotlinx_coroutines_core).resumeWith(Result.constructor-impl(ResultKt.createFailure(StackTraceRecoveryKt.recoverStackTrace(t, (Continuation<?>)eventLoop$kotlinx_coroutines_core))));
    }
    
    public static final <T> void resumeDirect(final Continuation<? super T> continuation, final T t) {
        Intrinsics.checkParameterIsNotNull(continuation, "$this$resumeDirect");
        if (continuation instanceof DispatchedContinuation) {
            final Continuation<T> continuation2 = ((DispatchedContinuation)continuation).continuation;
            final Result.Companion companion = Result.Companion;
            continuation2.resumeWith(Result.constructor-impl(t));
            return;
        }
        final Result.Companion companion2 = Result.Companion;
        continuation.resumeWith(Result.constructor-impl(t));
    }
    
    public static final <T> void resumeDirectWithException(final Continuation<? super T> continuation, final Throwable t) {
        Intrinsics.checkParameterIsNotNull(continuation, "$this$resumeDirectWithException");
        Intrinsics.checkParameterIsNotNull(t, "exception");
        if (continuation instanceof DispatchedContinuation) {
            final Continuation<?> continuation2 = ((DispatchedContinuation<?>)continuation).continuation;
            final Result.Companion companion = Result.Companion;
            continuation2.resumeWith(Result.constructor-impl(ResultKt.createFailure(StackTraceRecoveryKt.recoverStackTrace(t, continuation2))));
            return;
        }
        final Result.Companion companion2 = Result.Companion;
        continuation.resumeWith(Result.constructor-impl(ResultKt.createFailure(StackTraceRecoveryKt.recoverStackTrace(t, continuation))));
    }
    
    private static final void resumeUnconfined(final DispatchedTask<?> dispatchedTask) {
        final EventLoop eventLoop$kotlinx_coroutines_core = ThreadLocalEventLoop.INSTANCE.getEventLoop$kotlinx_coroutines_core();
        if (eventLoop$kotlinx_coroutines_core.isUnconfinedLoopActive()) {
            eventLoop$kotlinx_coroutines_core.dispatchUnconfined(dispatchedTask);
            return;
        }
        eventLoop$kotlinx_coroutines_core.incrementUseCount(true);
        final Throwable t2;
        try {
            resume((DispatchedTask<? super Object>)dispatchedTask, dispatchedTask.getDelegate$kotlinx_coroutines_core(), 3);
            while (eventLoop$kotlinx_coroutines_core.processUnconfinedEvent()) {}
            return;
        }
        finally {
            final DispatchedTask<? super Object> dispatchedTask2 = (DispatchedTask<? super Object>)dispatchedTask;
            final Throwable t = t2;
            final Throwable t3 = null;
            dispatchedTask2.handleFatalException$kotlinx_coroutines_core(t, t3);
        }
        try {
            final DispatchedTask<? super Object> dispatchedTask2 = (DispatchedTask<? super Object>)dispatchedTask;
            final Throwable t = t2;
            final Throwable t3 = null;
            dispatchedTask2.handleFatalException$kotlinx_coroutines_core(t, t3);
        }
        finally {
            eventLoop$kotlinx_coroutines_core.decrementUseCount(true);
        }
    }
    
    public static final void resumeWithStackTrace(final Continuation<?> continuation, final Throwable t) {
        Intrinsics.checkParameterIsNotNull(continuation, "$this$resumeWithStackTrace");
        Intrinsics.checkParameterIsNotNull(t, "exception");
        final Result.Companion companion = Result.Companion;
        continuation.resumeWith(Result.constructor-impl(ResultKt.createFailure(StackTraceRecoveryKt.recoverStackTrace(t, continuation))));
    }
    
    private static final void runUnconfinedEventLoop(final DispatchedTask<?> dispatchedTask, final EventLoop eventLoop, final Function0<Unit> function0) {
        eventLoop.incrementUseCount(true);
        final Throwable t2;
        try {
            function0.invoke();
            while (eventLoop.processUnconfinedEvent()) {}
            InlineMarker.finallyStart(1);
            return;
        }
        finally {
            final DispatchedTask<?> dispatchedTask2 = dispatchedTask;
            final Throwable t = t2;
            final Throwable t3 = null;
            dispatchedTask2.handleFatalException$kotlinx_coroutines_core(t, t3);
        }
        try {
            final DispatchedTask<?> dispatchedTask2 = dispatchedTask;
            final Throwable t = t2;
            final Throwable t3 = null;
            dispatchedTask2.handleFatalException$kotlinx_coroutines_core(t, t3);
        }
        finally {
            InlineMarker.finallyStart(1);
            eventLoop.decrementUseCount(true);
            InlineMarker.finallyEnd(1);
        }
    }
    
    public static final boolean yieldUndispatched(final DispatchedContinuation<? super Unit> dispatchedContinuation) {
        Intrinsics.checkParameterIsNotNull(dispatchedContinuation, "$this$yieldUndispatched");
        final Unit instance = Unit.INSTANCE;
        final EventLoop eventLoop$kotlinx_coroutines_core = ThreadLocalEventLoop.INSTANCE.getEventLoop$kotlinx_coroutines_core();
        if (eventLoop$kotlinx_coroutines_core.isUnconfinedQueueEmpty()) {
            return false;
        }
        if (eventLoop$kotlinx_coroutines_core.isUnconfinedLoopActive()) {
            dispatchedContinuation._state = instance;
            dispatchedContinuation.resumeMode = 1;
            eventLoop$kotlinx_coroutines_core.dispatchUnconfined(dispatchedContinuation);
            return true;
        }
        final DispatchedContinuation<? super Unit> dispatchedContinuation2 = dispatchedContinuation;
        eventLoop$kotlinx_coroutines_core.incrementUseCount(true);
        final Throwable t2;
        try {
            dispatchedContinuation.run();
            while (eventLoop$kotlinx_coroutines_core.processUnconfinedEvent()) {}
            return false;
        }
        finally {
            final DispatchedContinuation<? super Unit> dispatchedContinuation3 = dispatchedContinuation2;
            final Throwable t = t2;
            final Throwable t3 = null;
            dispatchedContinuation3.handleFatalException$kotlinx_coroutines_core(t, t3);
        }
        try {
            final DispatchedContinuation<? super Unit> dispatchedContinuation3 = dispatchedContinuation2;
            final Throwable t = t2;
            final Throwable t3 = null;
            dispatchedContinuation3.handleFatalException$kotlinx_coroutines_core(t, t3);
            return false;
        }
        finally {
            eventLoop$kotlinx_coroutines_core.decrementUseCount(true);
        }
    }
}
