// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.Result;
import java.util.ArrayList;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.coroutines.jvm.internal.Boxing;
import java.util.List;
import kotlin.coroutines.Continuation;
import kotlin.jvm.internal.Intrinsics;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0006\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002:\u0002\f\rB\u001d\u0012\u0014\u0010\u0005\u001a\u0010\u0012\f\b\u0001\u0012\b\u0012\u0004\u0012\u00028\u00000\u00040\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0019\u0010\t\u001a\b\u0012\u0004\u0012\u00028\u00000\bH\u0086@\u00f8\u0001\u0000¢\u0006\u0004\b\t\u0010\nR$\u0010\u0005\u001a\u0010\u0012\f\b\u0001\u0012\b\u0012\u0004\u0012\u00028\u00000\u00040\u00038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u000b\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u000e" }, d2 = { "Lkotlinx/coroutines/AwaitAll;", "T", "", "", "Lkotlinx/coroutines/Deferred;", "deferreds", "<init>", "([Lkotlinx/coroutines/Deferred;)V", "", "await", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "[Lkotlinx/coroutines/Deferred;", "AwaitAllNode", "DisposeHandlersOnCancel", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
final class AwaitAll<T>
{
    static final AtomicIntegerFieldUpdater notCompletedCount$FU;
    private final Deferred<T>[] deferreds;
    volatile int notCompletedCount;
    
    static {
        notCompletedCount$FU = AtomicIntegerFieldUpdater.newUpdater(AwaitAll.class, "notCompletedCount");
    }
    
    public AwaitAll(final Deferred<? extends T>[] deferreds) {
        Intrinsics.checkParameterIsNotNull(deferreds, "deferreds");
        this.deferreds = (Deferred<T>[])deferreds;
        this.notCompletedCount = this.deferreds.length;
    }
    
    public static final /* synthetic */ Deferred[] access$getDeferreds$p(final AwaitAll awaitAll) {
        return awaitAll.deferreds;
    }
    
    public final Object await(final Continuation<? super List<? extends T>> continuation) {
        final CancellableContinuationImpl<Object> cancellableContinuationImpl = new CancellableContinuationImpl<Object>(IntrinsicsKt__IntrinsicsJvmKt.intercepted((Continuation<? super Object>)continuation), 1);
        final CancellableContinuationImpl<Object> cancellableContinuationImpl2 = cancellableContinuationImpl;
        final int length = access$getDeferreds$p(this).length;
        final AwaitAllNode[] array = new AwaitAllNode[length];
        final int n = 0;
        for (int i = 0; i < length; ++i) {
            final Deferred deferred = access$getDeferreds$p(this)[Boxing.boxInt(i).intValue()];
            deferred.start();
            final AwaitAllNode awaitAllNode = new AwaitAllNode(cancellableContinuationImpl2, deferred);
            awaitAllNode.setHandle(deferred.invokeOnCompletion(awaitAllNode));
            array[i] = awaitAllNode;
        }
        final DisposeHandlersOnCancel disposer = new DisposeHandlersOnCancel(array);
        for (int length2 = array.length, j = n; j < length2; ++j) {
            array[j].setDisposer(disposer);
        }
        if (cancellableContinuationImpl2.isCompleted()) {
            disposer.disposeAll();
        }
        else {
            cancellableContinuationImpl2.invokeOnCancellation(disposer);
        }
        final Object result = cancellableContinuationImpl.getResult();
        if (result == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            DebugProbesKt.probeCoroutineSuspended(continuation);
        }
        return result;
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\b\u0082\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B!\u0012\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0002¢\u0006\u0002\u0010\u0007J\u0013\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0096\u0002R\u001a\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u00050\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R&\u0010\b\u001a\u000e\u0018\u00010\tR\b\u0012\u0004\u0012\u00028\u00000\nX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\u0010X\u0086.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014¨\u0006\u0019" }, d2 = { "Lkotlinx/coroutines/AwaitAll$AwaitAllNode;", "Lkotlinx/coroutines/JobNode;", "Lkotlinx/coroutines/Job;", "continuation", "Lkotlinx/coroutines/CancellableContinuation;", "", "job", "(Lkotlinx/coroutines/AwaitAll;Lkotlinx/coroutines/CancellableContinuation;Lkotlinx/coroutines/Job;)V", "disposer", "Lkotlinx/coroutines/AwaitAll$DisposeHandlersOnCancel;", "Lkotlinx/coroutines/AwaitAll;", "getDisposer", "()Lkotlinx/coroutines/AwaitAll$DisposeHandlersOnCancel;", "setDisposer", "(Lkotlinx/coroutines/AwaitAll$DisposeHandlersOnCancel;)V", "handle", "Lkotlinx/coroutines/DisposableHandle;", "getHandle", "()Lkotlinx/coroutines/DisposableHandle;", "setHandle", "(Lkotlinx/coroutines/DisposableHandle;)V", "invoke", "", "cause", "", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private final class AwaitAllNode extends JobNode<Job>
    {
        private final CancellableContinuation<List<? extends T>> continuation;
        private volatile DisposeHandlersOnCancel disposer;
        public DisposableHandle handle;
        
        public AwaitAllNode(final CancellableContinuation<? super List<? extends T>> continuation, final Job job) {
            Intrinsics.checkParameterIsNotNull(continuation, "continuation");
            Intrinsics.checkParameterIsNotNull(job, "job");
            super(job);
            this.continuation = (CancellableContinuation<List<? extends T>>)continuation;
        }
        
        public final DisposeHandlersOnCancel getDisposer() {
            return this.disposer;
        }
        
        public final DisposableHandle getHandle() {
            final DisposableHandle handle = this.handle;
            if (handle == null) {
                Intrinsics.throwUninitializedPropertyAccessException("handle");
            }
            return handle;
        }
        
        @Override
        public void invoke(final Throwable t) {
            if (t != null) {
                final Object tryResumeWithException = this.continuation.tryResumeWithException(t);
                if (tryResumeWithException != null) {
                    this.continuation.completeResume(tryResumeWithException);
                    final DisposeHandlersOnCancel disposer = this.disposer;
                    if (disposer != null) {
                        disposer.disposeAll();
                    }
                }
            }
            else if (AwaitAll.notCompletedCount$FU.decrementAndGet(AwaitAll.this) == 0) {
                final CancellableContinuation<List<? extends T>> cancellableContinuation = this.continuation;
                final Deferred[] access$getDeferreds$p = AwaitAll.access$getDeferreds$p(AwaitAll.this);
                final ArrayList<Object> list = new ArrayList<Object>(access$getDeferreds$p.length);
                for (int length = access$getDeferreds$p.length, i = 0; i < length; ++i) {
                    list.add(access$getDeferreds$p[i].getCompleted());
                }
                final ArrayList<Object> list2 = list;
                final Result.Companion companion = Result.Companion;
                cancellableContinuation.resumeWith(Result.constructor-impl(list2));
            }
        }
        
        public final void setDisposer(final DisposeHandlersOnCancel disposer) {
            this.disposer = disposer;
        }
        
        public final void setHandle(final DisposableHandle handle) {
            Intrinsics.checkParameterIsNotNull(handle, "<set-?>");
            this.handle = handle;
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0082\u0004\u0018\u00002\u00020\u0001B\u001d\u0012\u0016\u0010\u0002\u001a\u0012\u0012\u000e\u0012\f0\u0004R\b\u0012\u0004\u0012\u00028\u00000\u00050\u0003¢\u0006\u0002\u0010\u0006J\u0006\u0010\b\u001a\u00020\tJ\u0013\u0010\n\u001a\u00020\t2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0096\u0002J\b\u0010\r\u001a\u00020\u000eH\u0016R \u0010\u0002\u001a\u0012\u0012\u000e\u0012\f0\u0004R\b\u0012\u0004\u0012\u00028\u00000\u00050\u0003X\u0082\u0004¢\u0006\u0004\n\u0002\u0010\u0007¨\u0006\u000f" }, d2 = { "Lkotlinx/coroutines/AwaitAll$DisposeHandlersOnCancel;", "Lkotlinx/coroutines/CancelHandler;", "nodes", "", "Lkotlinx/coroutines/AwaitAll$AwaitAllNode;", "Lkotlinx/coroutines/AwaitAll;", "(Lkotlinx/coroutines/AwaitAll;[Lkotlinx/coroutines/AwaitAll$AwaitAllNode;)V", "[Lkotlinx/coroutines/AwaitAll$AwaitAllNode;", "disposeAll", "", "invoke", "cause", "", "toString", "", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private final class DisposeHandlersOnCancel extends CancelHandler
    {
        private final AwaitAllNode[] nodes;
        
        public DisposeHandlersOnCancel(final AwaitAllNode[] nodes) {
            Intrinsics.checkParameterIsNotNull(nodes, "nodes");
            this.nodes = nodes;
        }
        
        public final void disposeAll() {
            final AwaitAllNode[] nodes = this.nodes;
            for (int length = nodes.length, i = 0; i < length; ++i) {
                nodes[i].getHandle().dispose();
            }
        }
        
        @Override
        public void invoke(final Throwable t) {
            this.disposeAll();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("DisposeHandlersOnCancel[");
            sb.append(this.nodes);
            sb.append(']');
            return sb.toString();
        }
    }
}
