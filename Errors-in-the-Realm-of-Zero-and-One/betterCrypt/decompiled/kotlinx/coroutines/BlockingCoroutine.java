// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import java.util.concurrent.locks.LockSupport;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000<\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B\u001f\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\u0002\u0010\tJ\u001a\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0014J\u000b\u0010\u0013\u001a\u00028\u0000¢\u0006\u0002\u0010\u0014R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u00020\u000b8TX\u0094\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\f¨\u0006\u0015" }, d2 = { "Lkotlinx/coroutines/BlockingCoroutine;", "T", "Lkotlinx/coroutines/AbstractCoroutine;", "parentContext", "Lkotlin/coroutines/CoroutineContext;", "blockedThread", "Ljava/lang/Thread;", "eventLoop", "Lkotlinx/coroutines/EventLoop;", "(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Thread;Lkotlinx/coroutines/EventLoop;)V", "isScopedCoroutine", "", "()Z", "afterCompletionInternal", "", "state", "", "mode", "", "joinBlocking", "()Ljava/lang/Object;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
final class BlockingCoroutine<T> extends AbstractCoroutine<T>
{
    private final Thread blockedThread;
    private final EventLoop eventLoop;
    
    public BlockingCoroutine(final CoroutineContext coroutineContext, final Thread blockedThread, final EventLoop eventLoop) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "parentContext");
        Intrinsics.checkParameterIsNotNull(blockedThread, "blockedThread");
        super(coroutineContext, true);
        this.blockedThread = blockedThread;
        this.eventLoop = eventLoop;
    }
    
    @Override
    protected void afterCompletionInternal(final Object o, final int n) {
        if (Intrinsics.areEqual(Thread.currentThread(), this.blockedThread) ^ true) {
            LockSupport.unpark(this.blockedThread);
        }
    }
    
    @Override
    protected boolean isScopedCoroutine() {
        return true;
    }
    
    public final T joinBlocking() {
        final TimeSource timeSource = TimeSourceKt.getTimeSource();
        if (timeSource != null) {
            timeSource.registerTimeLoopThread();
        }
        while (true) {
            while (true) {
                Label_0256: {
                    try {
                        final EventLoop eventLoop = this.eventLoop;
                        CompletedExceptionally completedExceptionally = null;
                        if (eventLoop != null) {
                            EventLoop.incrementUseCount$default(eventLoop, false, 1, null);
                        }
                        try {
                            while (!Thread.interrupted()) {
                                final EventLoop eventLoop2 = this.eventLoop;
                                if (eventLoop2 == null) {
                                    break Label_0256;
                                }
                                final long processNextEvent = eventLoop2.processNextEvent();
                                if (this.isCompleted()) {
                                    final EventLoop eventLoop3 = this.eventLoop;
                                    if (eventLoop3 != null) {
                                        EventLoop.decrementUseCount$default(eventLoop3, false, 1, null);
                                    }
                                    final TimeSource timeSource2 = TimeSourceKt.getTimeSource();
                                    if (timeSource2 != null) {
                                        timeSource2.unregisterTimeLoopThread();
                                    }
                                    final Object unboxState = JobSupportKt.unboxState(this.getState$kotlinx_coroutines_core());
                                    if (unboxState instanceof CompletedExceptionally) {
                                        completedExceptionally = (CompletedExceptionally)unboxState;
                                    }
                                    final CompletedExceptionally completedExceptionally2 = completedExceptionally;
                                    if (completedExceptionally2 == null) {
                                        return (T)unboxState;
                                    }
                                    throw completedExceptionally2.cause;
                                }
                                else {
                                    final TimeSource timeSource3 = TimeSourceKt.getTimeSource();
                                    if (timeSource3 != null) {
                                        timeSource3.parkNanos(this, processNextEvent);
                                    }
                                    else {
                                        LockSupport.parkNanos(this, processNextEvent);
                                    }
                                }
                            }
                            final InterruptedException ex = new InterruptedException();
                            this.cancelCoroutine(ex);
                            throw ex;
                        }
                        finally {
                            final EventLoop eventLoop4 = this.eventLoop;
                            if (eventLoop4 != null) {
                                EventLoop.decrementUseCount$default(eventLoop4, false, 1, null);
                            }
                        }
                    }
                    finally {
                        final TimeSource timeSource4 = TimeSourceKt.getTimeSource();
                        if (timeSource4 != null) {
                            timeSource4.unregisterTimeLoopThread();
                        }
                    }
                }
                final long processNextEvent = Long.MAX_VALUE;
                continue;
            }
        }
    }
}
