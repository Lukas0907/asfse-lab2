// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.channels;

import kotlin.coroutines.EmptyCoroutineContext;
import kotlinx.coroutines.CoroutineScope;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.Dispatchers;
import kotlinx.coroutines.GlobalScope;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.TimeSource;
import kotlinx.coroutines.EventLoop_commonKt;
import kotlin.coroutines.jvm.internal.Boxing;
import kotlinx.coroutines.TimeSourceKt;
import kotlinx.coroutines.DelayKt;
import kotlin.ResultKt;
import kotlin.coroutines.Continuation;
import kotlin.Unit;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000*\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a/\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00032\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00010\u0006H\u0082@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0007\u001a/\u0010\b\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00032\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00010\u0006H\u0082@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0007\u001a4\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00010\n2\u0006\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u000b\u001a\u00020\f2\b\b\u0002\u0010\r\u001a\u00020\u000eH\u0007\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u000f" }, d2 = { "fixedDelayTicker", "", "delayMillis", "", "initialDelayMillis", "channel", "Lkotlinx/coroutines/channels/SendChannel;", "(JJLkotlinx/coroutines/channels/SendChannel;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "fixedPeriodTicker", "ticker", "Lkotlinx/coroutines/channels/ReceiveChannel;", "context", "Lkotlin/coroutines/CoroutineContext;", "mode", "Lkotlinx/coroutines/channels/TickerMode;", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class TickerChannelsKt
{
    public static final ReceiveChannel<Unit> ticker(final long lng, final long lng2, final CoroutineContext coroutineContext, final TickerMode tickerMode) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        Intrinsics.checkParameterIsNotNull(tickerMode, "mode");
        final int n = 1;
        if (lng < 0L) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Expected non-negative delay, but has ");
            sb.append(lng);
            sb.append(" ms");
            throw new IllegalArgumentException(sb.toString().toString());
        }
        int n2;
        if (lng2 >= 0L) {
            n2 = n;
        }
        else {
            n2 = 0;
        }
        if (n2 != 0) {
            return ProduceKt.produce((CoroutineScope)GlobalScope.INSTANCE, Dispatchers.getUnconfined().plus(coroutineContext), 0, (Function2<? super ProducerScope<? super Unit>, ? super Continuation<? super Unit>, ?>)new TickerChannelsKt$ticker.TickerChannelsKt$ticker$3(tickerMode, lng, lng2, (Continuation)null));
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Expected non-negative initial delay, but has ");
        sb2.append(lng2);
        sb2.append(" ms");
        throw new IllegalArgumentException(sb2.toString().toString());
    }
}
