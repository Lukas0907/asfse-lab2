// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.channels;

import kotlinx.coroutines.CancelHandler;
import kotlin.coroutines.ContinuationKt;
import kotlin.ResultKt;
import kotlin.coroutines.jvm.internal.Boxing;
import kotlin.Result;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlinx.coroutines.internal.AtomicDesc;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.selects.SelectClause1;
import kotlinx.coroutines.DebugKt;
import kotlinx.coroutines.DebugStringsKt;
import java.util.concurrent.CancellationException;
import kotlin.DeprecationLevel;
import kotlin.Deprecated;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.intrinsics.UndispatchedKt;
import kotlinx.coroutines.selects.SelectKt;
import kotlinx.coroutines.internal.StackTraceRecoveryKt;
import kotlinx.coroutines.DisposableHandle;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.internal.LockFreeLinkedListHead;
import kotlin.TypeCastException;
import kotlinx.coroutines.internal.LockFreeLinkedListNode;
import kotlinx.coroutines.CancellableContinuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.selects.SelectInstance;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0003\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\b \u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003:\u0007IJKLMNOB\u0005¢\u0006\u0002\u0010\u0004J\u0012\u0010\u0016\u001a\u00020\u00062\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0007J\u0016\u0010\u0016\u001a\u00020\u00192\u000e\u0010\u0017\u001a\n\u0018\u00010\u001aj\u0004\u0018\u0001`\u001bJ\u0017\u0010\u001c\u001a\u00020\u00062\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0010¢\u0006\u0002\b\u001dJ\b\u0010\u001e\u001a\u00020\u0019H\u0014J\u000e\u0010\u001f\u001a\b\u0012\u0004\u0012\u00028\u00000 H\u0004J\u0016\u0010!\u001a\u00020\u00062\f\u0010\"\u001a\b\u0012\u0004\u0012\u00028\u00000#H\u0002JR\u0010$\u001a\u00020\u0006\"\u0004\b\u0001\u0010%2\f\u0010&\u001a\b\u0012\u0004\u0012\u0002H%0'2$\u0010(\u001a \b\u0001\u0012\u0006\u0012\u0004\u0018\u00010*\u0012\n\u0012\b\u0012\u0004\u0012\u0002H%0+\u0012\u0006\u0012\u0004\u0018\u00010*0)2\u0006\u0010,\u001a\u00020-H\u0002\u00f8\u0001\u0000¢\u0006\u0002\u0010.J\u000f\u0010/\u001a\b\u0012\u0004\u0012\u00028\u000000H\u0086\u0002J\b\u00101\u001a\u00020\u0019H\u0014J\b\u00102\u001a\u00020\u0019H\u0014J\r\u00103\u001a\u0004\u0018\u00018\u0000¢\u0006\u0002\u00104J\n\u00105\u001a\u0004\u0018\u00010*H\u0014J\u0016\u00106\u001a\u0004\u0018\u00010*2\n\u0010&\u001a\u0006\u0012\u0002\b\u00030'H\u0014J\u0011\u0010\"\u001a\u00028\u0000H\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u00107J\u001a\u00108\u001a\b\u0012\u0004\u0012\u00028\u00000\u0012H\u0086@\u00f8\u0001\u0000\u00f8\u0001\u0000¢\u0006\u0002\u00107J\u0013\u00109\u001a\u0004\u0018\u00018\u0000H\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u00107J\u0019\u0010:\u001a\u0004\u0018\u00018\u00002\b\u0010;\u001a\u0004\u0018\u00010*H\u0002¢\u0006\u0002\u0010<J\u0017\u0010=\u001a\u00028\u00002\b\u0010;\u001a\u0004\u0018\u00010*H\u0002¢\u0006\u0002\u0010<J\u001f\u0010>\u001a\u0002H%\"\u0004\b\u0001\u0010%2\u0006\u0010,\u001a\u00020-H\u0082@\u00f8\u0001\u0000¢\u0006\u0002\u0010?JH\u0010@\u001a\u00020\u0019\"\u0004\b\u0001\u0010%2\f\u0010&\u001a\b\u0012\u0004\u0012\u0002H%0'2\"\u0010(\u001a\u001e\b\u0001\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u0002H%0+\u0012\u0006\u0012\u0004\u0018\u00010*0)H\u0002\u00f8\u0001\u0000¢\u0006\u0002\u0010AJQ\u0010B\u001a\u00020\u0019\"\u0004\b\u0001\u0010%2\f\u0010&\u001a\b\u0012\u0004\u0012\u0002H%0'2(\u0010(\u001a$\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0012\u0012\n\u0012\b\u0012\u0004\u0012\u0002H%0+\u0012\u0006\u0012\u0004\u0018\u00010*0)H\u0002\u00f8\u0001\u0000\u00f8\u0001\u0000¢\u0006\u0002\u0010AJJ\u0010C\u001a\u00020\u0019\"\u0004\b\u0001\u0010%2\f\u0010&\u001a\b\u0012\u0004\u0012\u0002H%0'2$\u0010(\u001a \b\u0001\u0012\u0006\u0012\u0004\u0018\u00018\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u0002H%0+\u0012\u0006\u0012\u0004\u0018\u00010*0)H\u0002\u00f8\u0001\u0000¢\u0006\u0002\u0010AJ \u0010D\u001a\u00020\u00192\n\u0010E\u001a\u0006\u0012\u0002\b\u00030F2\n\u0010\"\u001a\u0006\u0012\u0002\b\u00030#H\u0002J\u0010\u0010G\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010HH\u0014R\u0014\u0010\u0005\u001a\u00020\u00068DX\u0084\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0012\u0010\t\u001a\u00020\u0006X¤\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\bR\u0012\u0010\n\u001a\u00020\u0006X¤\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\bR\u0011\u0010\u000b\u001a\u00020\u00068F¢\u0006\u0006\u001a\u0004\b\u000b\u0010\bR\u0011\u0010\f\u001a\u00020\u00068F¢\u0006\u0006\u001a\u0004\b\f\u0010\bR\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00000\u000e8F¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R#\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u00120\u000e8VX\u0096\u0004\u00f8\u0001\u0000¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0010R\u0019\u0010\u0014\u001a\n\u0012\u0006\u0012\u0004\u0018\u00018\u00000\u000e8F¢\u0006\u0006\u001a\u0004\b\u0015\u0010\u0010\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006P" }, d2 = { "Lkotlinx/coroutines/channels/AbstractChannel;", "E", "Lkotlinx/coroutines/channels/AbstractSendChannel;", "Lkotlinx/coroutines/channels/Channel;", "()V", "hasReceiveOrClosed", "", "getHasReceiveOrClosed", "()Z", "isBufferAlwaysEmpty", "isBufferEmpty", "isClosedForReceive", "isEmpty", "onReceive", "Lkotlinx/coroutines/selects/SelectClause1;", "getOnReceive", "()Lkotlinx/coroutines/selects/SelectClause1;", "onReceiveOrClosed", "Lkotlinx/coroutines/channels/ValueOrClosed;", "getOnReceiveOrClosed", "onReceiveOrNull", "getOnReceiveOrNull", "cancel", "cause", "", "", "Ljava/util/concurrent/CancellationException;", "Lkotlinx/coroutines/CancellationException;", "cancelInternal", "cancelInternal$kotlinx_coroutines_core", "cleanupSendQueueOnCancel", "describeTryPoll", "Lkotlinx/coroutines/channels/AbstractChannel$TryPollDesc;", "enqueueReceive", "receive", "Lkotlinx/coroutines/channels/Receive;", "enqueueReceiveSelect", "R", "select", "Lkotlinx/coroutines/selects/SelectInstance;", "block", "Lkotlin/Function2;", "", "Lkotlin/coroutines/Continuation;", "receiveMode", "", "(Lkotlinx/coroutines/selects/SelectInstance;Lkotlin/jvm/functions/Function2;I)Z", "iterator", "Lkotlinx/coroutines/channels/ChannelIterator;", "onReceiveDequeued", "onReceiveEnqueued", "poll", "()Ljava/lang/Object;", "pollInternal", "pollSelectInternal", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "receiveOrClosed", "receiveOrNull", "receiveOrNullResult", "result", "(Ljava/lang/Object;)Ljava/lang/Object;", "receiveResult", "receiveSuspend", "(ILkotlin/coroutines/Continuation;)Ljava/lang/Object;", "registerSelectReceive", "(Lkotlinx/coroutines/selects/SelectInstance;Lkotlin/jvm/functions/Function2;)V", "registerSelectReceiveOrClosed", "registerSelectReceiveOrNull", "removeReceiveOnCancel", "cont", "Lkotlinx/coroutines/CancellableContinuation;", "takeFirstReceiveOrPeekClosed", "Lkotlinx/coroutines/channels/ReceiveOrClosed;", "IdempotentTokenValue", "Itr", "ReceiveElement", "ReceiveHasNext", "ReceiveSelect", "RemoveReceiveOnCancel", "TryPollDesc", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public abstract class AbstractChannel<E> extends AbstractSendChannel<E> implements Channel<E>
{
    private final boolean enqueueReceive(final Receive<? super E> receive) {
        final boolean bufferAlwaysEmpty = this.isBufferAlwaysEmpty();
        final boolean b = false;
        boolean b2 = false;
        Label_0170: {
            if (bufferAlwaysEmpty) {
                final LockFreeLinkedListHead queue = this.getQueue();
                LockFreeLinkedListNode lockFreeLinkedListNode;
                do {
                    final Object prev = queue.getPrev();
                    if (prev == null) {
                        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                    }
                    lockFreeLinkedListNode = (LockFreeLinkedListNode)prev;
                    if (!(lockFreeLinkedListNode instanceof Send ^ true)) {
                        b2 = b;
                        break Label_0170;
                    }
                } while (!lockFreeLinkedListNode.addNext(receive, queue));
            }
            else {
                final LockFreeLinkedListHead queue2 = this.getQueue();
                final Receive<? super E> receive2 = receive;
                final LockFreeLinkedListNode.CondAddOp condAddOp = (LockFreeLinkedListNode.CondAddOp)new AbstractChannel$enqueueReceive$$inlined$addLastIfPrevAndIf.AbstractChannel$enqueueReceive$$inlined$addLastIfPrevAndIf$1((LockFreeLinkedListNode)receive2, (LockFreeLinkedListNode)receive2, this);
                while (true) {
                    final Object prev2 = queue2.getPrev();
                    if (prev2 == null) {
                        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                    }
                    final LockFreeLinkedListNode lockFreeLinkedListNode2 = (LockFreeLinkedListNode)prev2;
                    if (!(lockFreeLinkedListNode2 instanceof Send ^ true)) {
                        b2 = b;
                        break Label_0170;
                    }
                    final int tryCondAddNext = lockFreeLinkedListNode2.tryCondAddNext(receive2, queue2, condAddOp);
                    if (tryCondAddNext == 1) {
                        break;
                    }
                    b2 = b;
                    if (tryCondAddNext != 2) {
                        continue;
                    }
                    break Label_0170;
                }
            }
            b2 = true;
        }
        if (b2) {
            this.onReceiveEnqueued();
        }
        return b2;
    }
    
    private final <R> boolean enqueueReceiveSelect(final SelectInstance<? super R> selectInstance, final Function2<Object, ? super Continuation<? super R>, ?> function2, final int n) {
        final ReceiveSelect receiveSelect = new ReceiveSelect<Object, Object>(this, selectInstance, function2, n);
        final boolean enqueueReceive = this.enqueueReceive(receiveSelect);
        if (enqueueReceive) {
            selectInstance.disposeOnSelect(receiveSelect);
        }
        return enqueueReceive;
    }
    
    private final E receiveOrNullResult(final Object o) {
        if (!(o instanceof Closed)) {
            return (E)o;
        }
        final Closed closed = (Closed)o;
        if (closed.closeCause == null) {
            return null;
        }
        throw StackTraceRecoveryKt.recoverStackTrace(closed.closeCause);
    }
    
    private final E receiveResult(final Object o) {
        if (!(o instanceof Closed)) {
            return (E)o;
        }
        throw StackTraceRecoveryKt.recoverStackTrace(((Closed)o).getReceiveException());
    }
    
    private final <R> void registerSelectReceive(final SelectInstance<? super R> selectInstance, final Function2<? super E, ? super Continuation<? super R>, ?> function2) {
        while (!selectInstance.isSelected()) {
            if (this.isEmpty()) {
                if (function2 == null) {
                    throw new TypeCastException("null cannot be cast to non-null type suspend (kotlin.Any?) -> R");
                }
                if (this.enqueueReceiveSelect((SelectInstance<? super Object>)selectInstance, (Function2<Object, ? super Continuation<? super Object>, ?>)function2, 0)) {
                    return;
                }
                continue;
            }
            else {
                final Object pollSelectInternal = this.pollSelectInternal(selectInstance);
                if (pollSelectInternal == SelectKt.getALREADY_SELECTED()) {
                    return;
                }
                if (pollSelectInternal == AbstractChannelKt.POLL_FAILED) {
                    continue;
                }
                if (!(pollSelectInternal instanceof Closed)) {
                    UndispatchedKt.startCoroutineUnintercepted((Function2<? super Closed, ? super Continuation<? super Object>, ?>)function2, (Closed)pollSelectInternal, selectInstance.getCompletion());
                    return;
                }
                throw StackTraceRecoveryKt.recoverStackTrace(((Closed)pollSelectInternal).getReceiveException());
            }
        }
    }
    
    private final <R> void registerSelectReceiveOrClosed(final SelectInstance<? super R> selectInstance, final Function2<? super ValueOrClosed<? extends E>, ? super Continuation<? super R>, ?> function2) {
        while (!selectInstance.isSelected()) {
            if (this.isEmpty()) {
                if (function2 == null) {
                    throw new TypeCastException("null cannot be cast to non-null type suspend (kotlin.Any?) -> R");
                }
                if (this.enqueueReceiveSelect((SelectInstance<? super Object>)selectInstance, (Function2<Object, ? super Continuation<? super Object>, ?>)function2, 2)) {
                    return;
                }
                continue;
            }
            else {
                final Object pollSelectInternal = this.pollSelectInternal(selectInstance);
                if (pollSelectInternal == SelectKt.getALREADY_SELECTED()) {
                    return;
                }
                if (pollSelectInternal == AbstractChannelKt.POLL_FAILED) {
                    continue;
                }
                if (!(pollSelectInternal instanceof Closed)) {
                    final ValueOrClosed.Companion companion = ValueOrClosed.Companion;
                    UndispatchedKt.startCoroutineUnintercepted((Function2<? super ValueOrClosed<Object>, ? super Continuation<? super Object>, ?>)function2, ValueOrClosed.box-impl(ValueOrClosed.constructor-impl(pollSelectInternal)), selectInstance.getCompletion());
                    return;
                }
                final ValueOrClosed.Companion companion2 = ValueOrClosed.Companion;
                UndispatchedKt.startCoroutineUnintercepted((Function2<? super ValueOrClosed<Object>, ? super Continuation<? super Object>, ?>)function2, ValueOrClosed.box-impl(ValueOrClosed.constructor-impl(new ValueOrClosed.Closed(((Closed)pollSelectInternal).closeCause))), selectInstance.getCompletion());
            }
        }
    }
    
    private final <R> void registerSelectReceiveOrNull(final SelectInstance<? super R> selectInstance, final Function2<? super E, ? super Continuation<? super R>, ?> function2) {
        while (!selectInstance.isSelected()) {
            if (this.isEmpty()) {
                if (function2 == null) {
                    throw new TypeCastException("null cannot be cast to non-null type suspend (kotlin.Any?) -> R");
                }
                if (this.enqueueReceiveSelect((SelectInstance<? super Object>)selectInstance, (Function2<Object, ? super Continuation<? super Object>, ?>)function2, 1)) {
                    return;
                }
                continue;
            }
            else {
                final Object pollSelectInternal = this.pollSelectInternal(selectInstance);
                if (pollSelectInternal == SelectKt.getALREADY_SELECTED()) {
                    return;
                }
                if (pollSelectInternal == AbstractChannelKt.POLL_FAILED) {
                    continue;
                }
                if (!(pollSelectInternal instanceof Closed)) {
                    UndispatchedKt.startCoroutineUnintercepted((Function2<? super Closed, ? super Continuation<? super Object>, ?>)function2, (Closed)pollSelectInternal, selectInstance.getCompletion());
                    return;
                }
                final Closed closed = (Closed)pollSelectInternal;
                if (closed.closeCause == null) {
                    if (selectInstance.trySelect(null)) {
                        UndispatchedKt.startCoroutineUnintercepted((Function2<? super Object, ? super Continuation<? super Object>, ?>)function2, (Object)null, selectInstance.getCompletion());
                    }
                    return;
                }
                throw StackTraceRecoveryKt.recoverStackTrace(closed.closeCause);
            }
        }
    }
    
    private final void removeReceiveOnCancel(final CancellableContinuation<?> cancellableContinuation, final Receive<?> receive) {
        cancellableContinuation.invokeOnCancellation(new RemoveReceiveOnCancel(receive));
    }
    
    @Override
    public final void cancel(CancellationException ex) {
        if (ex == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(DebugStringsKt.getClassSimpleName(this));
            sb.append(" was cancelled");
            ex = new CancellationException(sb.toString());
        }
        this.cancelInternal$kotlinx_coroutines_core(ex);
    }
    
    public boolean cancelInternal$kotlinx_coroutines_core(final Throwable t) {
        final boolean close = this.close(t);
        this.cleanupSendQueueOnCancel();
        return close;
    }
    
    protected void cleanupSendQueueOnCancel() {
        final Closed<?> closedForSend = this.getClosedForSend();
        if (closedForSend == null) {
            throw new IllegalStateException("Cannot happen".toString());
        }
        while (true) {
            final Send takeFirstSendOrPeekClosed = this.takeFirstSendOrPeekClosed();
            if (takeFirstSendOrPeekClosed == null) {
                throw new IllegalStateException("Cannot happen".toString());
            }
            if (takeFirstSendOrPeekClosed instanceof Closed) {
                if (!DebugKt.getASSERTIONS_ENABLED()) {
                    return;
                }
                if (takeFirstSendOrPeekClosed == closedForSend) {
                    return;
                }
                throw new AssertionError();
            }
            else {
                takeFirstSendOrPeekClosed.resumeSendClosed(closedForSend);
            }
        }
    }
    
    protected final TryPollDesc<E> describeTryPoll() {
        return new TryPollDesc<E>(this.getQueue());
    }
    
    protected final boolean getHasReceiveOrClosed() {
        return this.getQueue().getNextNode() instanceof ReceiveOrClosed;
    }
    
    @Override
    public final SelectClause1<E> getOnReceive() {
        return (SelectClause1<E>)new AbstractChannel$onReceive.AbstractChannel$onReceive$1(this);
    }
    
    @Override
    public SelectClause1<ValueOrClosed<E>> getOnReceiveOrClosed() {
        return (SelectClause1<ValueOrClosed<E>>)new AbstractChannel$onReceiveOrClosed.AbstractChannel$onReceiveOrClosed$1(this);
    }
    
    @Override
    public final SelectClause1<E> getOnReceiveOrNull() {
        return (SelectClause1<E>)new AbstractChannel$onReceiveOrNull.AbstractChannel$onReceiveOrNull$1(this);
    }
    
    protected abstract boolean isBufferAlwaysEmpty();
    
    protected abstract boolean isBufferEmpty();
    
    @Override
    public final boolean isClosedForReceive() {
        return this.getClosedForReceive() != null && this.isBufferEmpty();
    }
    
    @Override
    public final boolean isEmpty() {
        return !(this.getQueue().getNextNode() instanceof Send) && this.isBufferEmpty();
    }
    
    @Override
    public final ChannelIterator<E> iterator() {
        return new Itr<E>(this);
    }
    
    protected void onReceiveDequeued() {
    }
    
    protected void onReceiveEnqueued() {
    }
    
    @Override
    public final E poll() {
        final Object pollInternal = this.pollInternal();
        if (pollInternal == AbstractChannelKt.POLL_FAILED) {
            return null;
        }
        return this.receiveOrNullResult(pollInternal);
    }
    
    protected Object pollInternal() {
        Object tryResumeSend;
        Send takeFirstSendOrPeekClosed;
        do {
            takeFirstSendOrPeekClosed = this.takeFirstSendOrPeekClosed();
            if (takeFirstSendOrPeekClosed == null) {
                return AbstractChannelKt.POLL_FAILED;
            }
            tryResumeSend = takeFirstSendOrPeekClosed.tryResumeSend(null);
        } while (tryResumeSend == null);
        takeFirstSendOrPeekClosed.completeResumeSend(tryResumeSend);
        return takeFirstSendOrPeekClosed.getPollResult();
    }
    
    protected Object pollSelectInternal(final SelectInstance<?> selectInstance) {
        Intrinsics.checkParameterIsNotNull(selectInstance, "select");
        final TryPollDesc<E> describeTryPoll = this.describeTryPoll();
        final Object performAtomicTrySelect = selectInstance.performAtomicTrySelect(describeTryPoll);
        if (performAtomicTrySelect != null) {
            return performAtomicTrySelect;
        }
        final Send send = describeTryPoll.getResult();
        final Object resumeToken = describeTryPoll.resumeToken;
        if (resumeToken == null) {
            Intrinsics.throwNpe();
        }
        send.completeResumeSend(resumeToken);
        return describeTryPoll.pollResult;
    }
    
    @Override
    public final Object receive(final Continuation<? super E> continuation) {
        final Object pollInternal = this.pollInternal();
        if (pollInternal != AbstractChannelKt.POLL_FAILED) {
            return this.receiveResult(pollInternal);
        }
        return this.receiveSuspend(0, (Continuation<? super Object>)continuation);
    }
    
    @Override
    public final Object receiveOrClosed(final Continuation<? super ValueOrClosed<? extends E>> continuation) {
        final Object pollInternal = this.pollInternal();
        if (pollInternal != AbstractChannelKt.POLL_FAILED) {
            Object o;
            if (pollInternal instanceof Closed) {
                final ValueOrClosed.Companion companion = ValueOrClosed.Companion;
                o = ValueOrClosed.constructor-impl(new ValueOrClosed.Closed(((Closed)pollInternal).closeCause));
            }
            else {
                final ValueOrClosed.Companion companion2 = ValueOrClosed.Companion;
                o = ValueOrClosed.constructor-impl(pollInternal);
            }
            return ValueOrClosed.box-impl(o);
        }
        return this.receiveSuspend(2, (Continuation<? super Object>)continuation);
    }
    
    @Override
    public final Object receiveOrNull(final Continuation<? super E> continuation) {
        final Object pollInternal = this.pollInternal();
        if (pollInternal != AbstractChannelKt.POLL_FAILED) {
            return this.receiveOrNullResult(pollInternal);
        }
        return this.receiveSuspend(1, (Continuation<? super Object>)continuation);
    }
    
    final /* synthetic */  <R> Object receiveSuspend(final int n, final Continuation<? super R> continuation) {
        final CancellableContinuationImpl<Object> cancellableContinuationImpl = new CancellableContinuationImpl<Object>(IntrinsicsKt__IntrinsicsJvmKt.intercepted((Continuation<? super Object>)continuation), 0);
        final CancellableContinuationImpl<Object> cancellableContinuationImpl2 = cancellableContinuationImpl;
        final ReceiveElement receiveElement = new ReceiveElement<Closed<?>>(cancellableContinuationImpl2, n);
        while (true) {
            Object pollInternal;
            do {
                final ReceiveElement<Closed> receiveElement2 = (ReceiveElement<Closed>)receiveElement;
                if (this.enqueueReceive(receiveElement2)) {
                    this.removeReceiveOnCancel(cancellableContinuationImpl2, receiveElement2);
                }
                else {
                    pollInternal = this.pollInternal();
                    if (!(pollInternal instanceof Closed)) {
                        continue;
                    }
                    receiveElement.resumeReceiveClosed((Closed<?>)pollInternal);
                }
                final Object result = cancellableContinuationImpl.getResult();
                if (result == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                    DebugProbesKt.probeCoroutineSuspended(continuation);
                }
                return result;
            } while (pollInternal == AbstractChannelKt.POLL_FAILED);
            final CancellableContinuationImpl<Object> cancellableContinuationImpl3 = cancellableContinuationImpl2;
            final Object resumeValue = receiveElement.resumeValue((Closed<?>)pollInternal);
            final Result.Companion companion = Result.Companion;
            cancellableContinuationImpl3.resumeWith(Result.constructor-impl(resumeValue));
            continue;
        }
    }
    
    @Override
    protected ReceiveOrClosed<E> takeFirstReceiveOrPeekClosed() {
        final ReceiveOrClosed<E> takeFirstReceiveOrPeekClosed = super.takeFirstReceiveOrPeekClosed();
        if (takeFirstReceiveOrPeekClosed != null && !(takeFirstReceiveOrPeekClosed instanceof Closed)) {
            this.onReceiveDequeued();
        }
        return takeFirstReceiveOrPeekClosed;
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0005\b\u0002\u0018\u0000*\u0006\b\u0001\u0010\u0001 \u00012\u00020\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u0004\u001a\u00028\u0001¢\u0006\u0002\u0010\u0005R\u0010\u0010\u0003\u001a\u00020\u00028\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u00028\u00018\u0006X\u0087\u0004¢\u0006\u0004\n\u0002\u0010\u0006\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0007" }, d2 = { "Lkotlinx/coroutines/channels/AbstractChannel$IdempotentTokenValue;", "E", "", "token", "value", "(Ljava/lang/Object;Ljava/lang/Object;)V", "Ljava/lang/Object;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class IdempotentTokenValue<E>
    {
        public final Object token;
        public final E value;
        
        public IdempotentTokenValue(final Object token, final E value) {
            Intrinsics.checkParameterIsNotNull(token, "token");
            this.token = token;
            this.value = value;
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\b\u0002\u0018\u0000*\u0004\b\u0001\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B\u0013\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00010\u0004¢\u0006\u0002\u0010\u0005J\u0011\u0010\u000e\u001a\u00020\u000fH\u0096B\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0010J\u0012\u0010\u0011\u001a\u00020\u000f2\b\u0010\b\u001a\u0004\u0018\u00010\tH\u0002J\u0011\u0010\u0012\u001a\u00020\u000fH\u0082@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0010J\u000e\u0010\u0013\u001a\u00028\u0001H\u0096\u0002¢\u0006\u0002\u0010\u000bR\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u001c\u0010\b\u001a\u0004\u0018\u00010\tX\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\r\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0014" }, d2 = { "Lkotlinx/coroutines/channels/AbstractChannel$Itr;", "E", "Lkotlinx/coroutines/channels/ChannelIterator;", "channel", "Lkotlinx/coroutines/channels/AbstractChannel;", "(Lkotlinx/coroutines/channels/AbstractChannel;)V", "getChannel", "()Lkotlinx/coroutines/channels/AbstractChannel;", "result", "", "getResult", "()Ljava/lang/Object;", "setResult", "(Ljava/lang/Object;)V", "hasNext", "", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "hasNextResult", "hasNextSuspend", "next", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class Itr<E> implements ChannelIterator<E>
    {
        private final AbstractChannel<E> channel;
        private Object result;
        
        public Itr(final AbstractChannel<E> channel) {
            Intrinsics.checkParameterIsNotNull(channel, "channel");
            this.channel = channel;
            this.result = AbstractChannelKt.POLL_FAILED;
        }
        
        private final boolean hasNextResult(final Object o) {
            if (!(o instanceof Closed)) {
                return true;
            }
            final Closed closed = (Closed)o;
            if (closed.closeCause == null) {
                return false;
            }
            throw StackTraceRecoveryKt.recoverStackTrace(closed.getReceiveException());
        }
        
        public final AbstractChannel<E> getChannel() {
            return this.channel;
        }
        
        public final Object getResult() {
            return this.result;
        }
        
        @Override
        public Object hasNext(final Continuation<? super Boolean> continuation) {
            if (this.result != AbstractChannelKt.POLL_FAILED) {
                return Boxing.boxBoolean(this.hasNextResult(this.result));
            }
            this.result = this.channel.pollInternal();
            if (this.result != AbstractChannelKt.POLL_FAILED) {
                return Boxing.boxBoolean(this.hasNextResult(this.result));
            }
            return this.hasNextSuspend(continuation);
        }
        
        final /* synthetic */ Object hasNextSuspend(final Continuation<? super Boolean> continuation) {
            final CancellableContinuationImpl<Object> cancellableContinuationImpl = new CancellableContinuationImpl<Object>(IntrinsicsKt__IntrinsicsJvmKt.intercepted((Continuation<? super Object>)continuation), 0);
            final CancellableContinuationImpl<Object> cancellableContinuationImpl2 = cancellableContinuationImpl;
            final ReceiveHasNext receiveHasNext = new ReceiveHasNext((Itr<Object>)this, cancellableContinuationImpl2);
            while (true) {
                Object pollInternal;
                do {
                    final AbstractChannel<Object> channel = this.getChannel();
                    final ReceiveHasNext receiveHasNext2 = receiveHasNext;
                    if (channel.enqueueReceive(receiveHasNext2)) {
                        this.getChannel().removeReceiveOnCancel(cancellableContinuationImpl2, receiveHasNext2);
                    }
                    else {
                        pollInternal = this.getChannel().pollInternal();
                        this.setResult(pollInternal);
                        if (!(pollInternal instanceof Closed)) {
                            continue;
                        }
                        final Closed closed = (Closed)pollInternal;
                        if (closed.closeCause == null) {
                            final CancellableContinuationImpl<Object> cancellableContinuationImpl3 = cancellableContinuationImpl2;
                            final Boolean boxBoolean = Boxing.boxBoolean(false);
                            final Result.Companion companion = Result.Companion;
                            cancellableContinuationImpl3.resumeWith(Result.constructor-impl(boxBoolean));
                        }
                        else {
                            final CancellableContinuationImpl<Object> cancellableContinuationImpl4 = cancellableContinuationImpl2;
                            final Throwable receiveException = closed.getReceiveException();
                            final Result.Companion companion2 = Result.Companion;
                            cancellableContinuationImpl4.resumeWith(Result.constructor-impl(ResultKt.createFailure(receiveException)));
                        }
                    }
                    final Object result = cancellableContinuationImpl.getResult();
                    if (result == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                        DebugProbesKt.probeCoroutineSuspended(continuation);
                    }
                    return result;
                } while (pollInternal == AbstractChannelKt.POLL_FAILED);
                final CancellableContinuationImpl<Object> cancellableContinuationImpl5 = cancellableContinuationImpl2;
                final Boolean boxBoolean2 = Boxing.boxBoolean(true);
                final Result.Companion companion3 = Result.Companion;
                cancellableContinuationImpl5.resumeWith(Result.constructor-impl(boxBoolean2));
                continue;
            }
        }
        
        @Override
        public E next() {
            final Object result = this.result;
            if (result instanceof Closed) {
                throw StackTraceRecoveryKt.recoverStackTrace(((Closed)result).getReceiveException());
            }
            if (result != AbstractChannelKt.POLL_FAILED) {
                this.result = AbstractChannelKt.POLL_FAILED;
                return (E)result;
            }
            throw new IllegalStateException("'hasNext' should be called prior to 'next' invocation");
        }
        
        public final void setResult(final Object result) {
            this.result = result;
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u00006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0002\u0018\u0000*\u0006\b\u0001\u0010\u0001 \u00002\b\u0012\u0004\u0012\u0002H\u00010\u0002B\u001d\u0012\u000e\u0010\u0003\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0005H\u0016J\u0014\u0010\f\u001a\u00020\n2\n\u0010\r\u001a\u0006\u0012\u0002\b\u00030\u000eH\u0016J\u0015\u0010\u000f\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0010\u001a\u00028\u0001¢\u0006\u0002\u0010\u0011J\b\u0010\u0012\u001a\u00020\u0013H\u0016J!\u0010\u0014\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0010\u001a\u00028\u00012\b\u0010\u0015\u001a\u0004\u0018\u00010\u0005H\u0016¢\u0006\u0002\u0010\u0016R\u0018\u0010\u0003\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00050\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004¢\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0017" }, d2 = { "Lkotlinx/coroutines/channels/AbstractChannel$ReceiveElement;", "E", "Lkotlinx/coroutines/channels/Receive;", "cont", "Lkotlinx/coroutines/CancellableContinuation;", "", "receiveMode", "", "(Lkotlinx/coroutines/CancellableContinuation;I)V", "completeResumeReceive", "", "token", "resumeReceiveClosed", "closed", "Lkotlinx/coroutines/channels/Closed;", "resumeValue", "value", "(Ljava/lang/Object;)Ljava/lang/Object;", "toString", "", "tryResumeReceive", "idempotent", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class ReceiveElement<E> extends Receive<E>
    {
        public final CancellableContinuation<Object> cont;
        public final int receiveMode;
        
        public ReceiveElement(final CancellableContinuation<Object> cont, final int receiveMode) {
            Intrinsics.checkParameterIsNotNull(cont, "cont");
            this.cont = cont;
            this.receiveMode = receiveMode;
        }
        
        @Override
        public void completeResumeReceive(final Object o) {
            Intrinsics.checkParameterIsNotNull(o, "token");
            this.cont.completeResume(o);
        }
        
        @Override
        public void resumeReceiveClosed(final Closed<?> closed) {
            Intrinsics.checkParameterIsNotNull(closed, "closed");
            if (this.receiveMode == 1 && closed.closeCause == null) {
                final CancellableContinuation<Object> cancellableContinuation = this.cont;
                final Result.Companion companion = Result.Companion;
                cancellableContinuation.resumeWith(Result.constructor-impl(null));
                return;
            }
            if (this.receiveMode == 2) {
                final CancellableContinuation<Object> cancellableContinuation2 = this.cont;
                final ValueOrClosed.Companion companion2 = ValueOrClosed.Companion;
                final ValueOrClosed<Object> box-impl = ValueOrClosed.box-impl(ValueOrClosed.constructor-impl(new ValueOrClosed.Closed(closed.closeCause)));
                final Result.Companion companion3 = Result.Companion;
                cancellableContinuation2.resumeWith(Result.constructor-impl(box-impl));
                return;
            }
            final CancellableContinuation<Object> cancellableContinuation3 = this.cont;
            final Throwable receiveException = closed.getReceiveException();
            final Result.Companion companion4 = Result.Companion;
            cancellableContinuation3.resumeWith(Result.constructor-impl(ResultKt.createFailure(receiveException)));
        }
        
        public final Object resumeValue(final E e) {
            if (this.receiveMode != 2) {
                return e;
            }
            final ValueOrClosed.Companion companion = ValueOrClosed.Companion;
            return ValueOrClosed.box-impl(ValueOrClosed.constructor-impl(e));
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("ReceiveElement[receiveMode=");
            sb.append(this.receiveMode);
            sb.append(']');
            return sb.toString();
        }
        
        @Override
        public Object tryResumeReceive(final E e, final Object o) {
            return this.cont.tryResume(this.resumeValue(e), o);
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0002\u0018\u0000*\u0004\b\u0001\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B!\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00010\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0002\u0010\bJ\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u0014\u0010\r\u001a\u00020\n2\n\u0010\u000e\u001a\u0006\u0012\u0002\b\u00030\u000fH\u0016J\b\u0010\u0010\u001a\u00020\u0011H\u0016J!\u0010\u0012\u001a\u0004\u0018\u00010\f2\u0006\u0010\u0013\u001a\u00028\u00012\b\u0010\u0014\u001a\u0004\u0018\u00010\fH\u0016¢\u0006\u0002\u0010\u0015R\u0016\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00010\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0016" }, d2 = { "Lkotlinx/coroutines/channels/AbstractChannel$ReceiveHasNext;", "E", "Lkotlinx/coroutines/channels/Receive;", "iterator", "Lkotlinx/coroutines/channels/AbstractChannel$Itr;", "cont", "Lkotlinx/coroutines/CancellableContinuation;", "", "(Lkotlinx/coroutines/channels/AbstractChannel$Itr;Lkotlinx/coroutines/CancellableContinuation;)V", "completeResumeReceive", "", "token", "", "resumeReceiveClosed", "closed", "Lkotlinx/coroutines/channels/Closed;", "toString", "", "tryResumeReceive", "value", "idempotent", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class ReceiveHasNext<E> extends Receive<E>
    {
        public final CancellableContinuation<Boolean> cont;
        public final Itr<E> iterator;
        
        public ReceiveHasNext(final Itr<E> iterator, final CancellableContinuation<? super Boolean> cont) {
            Intrinsics.checkParameterIsNotNull(iterator, "iterator");
            Intrinsics.checkParameterIsNotNull(cont, "cont");
            this.iterator = iterator;
            this.cont = (CancellableContinuation<Boolean>)cont;
        }
        
        @Override
        public void completeResumeReceive(final Object o) {
            Intrinsics.checkParameterIsNotNull(o, "token");
            if (o instanceof IdempotentTokenValue) {
                final Itr<E> iterator = this.iterator;
                final IdempotentTokenValue idempotentTokenValue = (IdempotentTokenValue)o;
                iterator.setResult(idempotentTokenValue.value);
                this.cont.completeResume(idempotentTokenValue.token);
                return;
            }
            this.cont.completeResume(o);
        }
        
        @Override
        public void resumeReceiveClosed(final Closed<?> result) {
            Intrinsics.checkParameterIsNotNull(result, "closed");
            Object o;
            if (result.closeCause == null) {
                o = CancellableContinuation.DefaultImpls.tryResume$default(this.cont, false, null, 2, null);
            }
            else {
                o = this.cont.tryResumeWithException(StackTraceRecoveryKt.recoverStackTrace(result.getReceiveException(), this.cont));
            }
            if (o != null) {
                this.iterator.setResult(result);
                this.cont.completeResume(o);
            }
        }
        
        @Override
        public String toString() {
            return "ReceiveHasNext";
        }
        
        @Override
        public Object tryResumeReceive(final E result, final Object o) {
            final Object tryResume = this.cont.tryResume(true, o);
            if (tryResume != null) {
                if (o != null) {
                    return new IdempotentTokenValue(tryResume, result);
                }
                this.iterator.setResult(result);
            }
            return tryResume;
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000J\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0002\u0018\u0000*\u0004\b\u0001\u0010\u0001*\u0004\b\u0002\u0010\u00022\b\u0012\u0004\u0012\u0002H\u00020\u00032\u00020\u0004BR\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00028\u00020\u0006\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00010\b\u0012$\u0010\t\u001a \b\u0001\u0012\u0006\u0012\u0004\u0018\u00010\u000b\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00010\f\u0012\u0006\u0012\u0004\u0018\u00010\u000b0\n\u0012\u0006\u0010\r\u001a\u00020\u000e\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000fJ\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u000bH\u0016J\b\u0010\u0014\u001a\u00020\u0012H\u0016J\u0014\u0010\u0015\u001a\u00020\u00122\n\u0010\u0016\u001a\u0006\u0012\u0002\b\u00030\u0017H\u0016J\b\u0010\u0018\u001a\u00020\u0019H\u0016J!\u0010\u001a\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\u001b\u001a\u00028\u00022\b\u0010\u001c\u001a\u0004\u0018\u00010\u000bH\u0016¢\u0006\u0002\u0010\u001dR3\u0010\t\u001a \b\u0001\u0012\u0006\u0012\u0004\u0018\u00010\u000b\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00010\f\u0012\u0006\u0012\u0004\u0018\u00010\u000b0\n8\u0006X\u0087\u0004\u00f8\u0001\u0000¢\u0006\u0004\n\u0002\u0010\u0010R\u0016\u0010\u0005\u001a\b\u0012\u0004\u0012\u00028\u00020\u00068\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u00020\u000e8\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00010\b8\u0006X\u0087\u0004¢\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u001e" }, d2 = { "Lkotlinx/coroutines/channels/AbstractChannel$ReceiveSelect;", "R", "E", "Lkotlinx/coroutines/channels/Receive;", "Lkotlinx/coroutines/DisposableHandle;", "channel", "Lkotlinx/coroutines/channels/AbstractChannel;", "select", "Lkotlinx/coroutines/selects/SelectInstance;", "block", "Lkotlin/Function2;", "", "Lkotlin/coroutines/Continuation;", "receiveMode", "", "(Lkotlinx/coroutines/channels/AbstractChannel;Lkotlinx/coroutines/selects/SelectInstance;Lkotlin/jvm/functions/Function2;I)V", "Lkotlin/jvm/functions/Function2;", "completeResumeReceive", "", "token", "dispose", "resumeReceiveClosed", "closed", "Lkotlinx/coroutines/channels/Closed;", "toString", "", "tryResumeReceive", "value", "idempotent", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class ReceiveSelect<R, E> extends Receive<E> implements DisposableHandle
    {
        public final Function2<Object, Continuation<? super R>, Object> block;
        public final AbstractChannel<E> channel;
        public final int receiveMode;
        public final SelectInstance<R> select;
        
        public ReceiveSelect(final AbstractChannel<E> channel, final SelectInstance<? super R> select, final Function2<Object, ? super Continuation<? super R>, ?> block, final int receiveMode) {
            Intrinsics.checkParameterIsNotNull(channel, "channel");
            Intrinsics.checkParameterIsNotNull(select, "select");
            Intrinsics.checkParameterIsNotNull(block, "block");
            this.channel = channel;
            this.select = (SelectInstance<R>)select;
            this.block = (Function2<Object, Continuation<? super R>, Object>)block;
            this.receiveMode = receiveMode;
        }
        
        @Override
        public void completeResumeReceive(Object box-impl) {
            Intrinsics.checkParameterIsNotNull(box-impl, "token");
            Object o = box-impl;
            if (box-impl == AbstractChannelKt.NULL_VALUE) {
                o = null;
            }
            final Function2<Object, Continuation<? super R>, Object> block = this.block;
            box-impl = o;
            if (this.receiveMode == 2) {
                final ValueOrClosed.Companion companion = ValueOrClosed.Companion;
                box-impl = ValueOrClosed.box-impl(ValueOrClosed.constructor-impl(o));
            }
            ContinuationKt.startCoroutine((Function2<? super Object, ? super Continuation<? super Object>, ?>)block, box-impl, (Continuation<? super Object>)this.select.getCompletion());
        }
        
        @Override
        public void dispose() {
            if (this.remove()) {
                this.channel.onReceiveDequeued();
            }
        }
        
        @Override
        public void resumeReceiveClosed(final Closed<?> closed) {
            Intrinsics.checkParameterIsNotNull(closed, "closed");
            if (!this.select.trySelect(null)) {
                return;
            }
            final int receiveMode = this.receiveMode;
            if (receiveMode == 0) {
                this.select.resumeSelectCancellableWithException(closed.getReceiveException());
                return;
            }
            if (receiveMode != 1) {
                if (receiveMode != 2) {
                    return;
                }
                final Function2<Object, Continuation<? super R>, Object> block = this.block;
                final ValueOrClosed.Companion companion = ValueOrClosed.Companion;
                ContinuationKt.startCoroutine((Function2<? super ValueOrClosed<Object>, ? super Continuation<? super Object>, ?>)block, ValueOrClosed.box-impl(ValueOrClosed.constructor-impl(new ValueOrClosed.Closed(closed.closeCause))), (Continuation<? super Object>)this.select.getCompletion());
            }
            else {
                if (closed.closeCause == null) {
                    ContinuationKt.startCoroutine((Function2<? super Object, ? super Continuation<? super Object>, ?>)this.block, (Object)null, (Continuation<? super Object>)this.select.getCompletion());
                    return;
                }
                this.select.resumeSelectCancellableWithException(closed.getReceiveException());
            }
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("ReceiveSelect[");
            sb.append(this.select);
            sb.append(",receiveMode=");
            sb.append(this.receiveMode);
            sb.append(']');
            return sb.toString();
        }
        
        @Override
        public Object tryResumeReceive(final E e, final Object o) {
            if (!this.select.trySelect(o)) {
                return null;
            }
            if (e != null) {
                return e;
            }
            return AbstractChannelKt.NULL_VALUE;
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0011\u0012\n\u0010\u0002\u001a\u0006\u0012\u0002\b\u00030\u0003¢\u0006\u0002\u0010\u0004J\u0013\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0096\u0002J\b\u0010\t\u001a\u00020\nH\u0016R\u0012\u0010\u0002\u001a\u0006\u0012\u0002\b\u00030\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b" }, d2 = { "Lkotlinx/coroutines/channels/AbstractChannel$RemoveReceiveOnCancel;", "Lkotlinx/coroutines/CancelHandler;", "receive", "Lkotlinx/coroutines/channels/Receive;", "(Lkotlinx/coroutines/channels/AbstractChannel;Lkotlinx/coroutines/channels/Receive;)V", "invoke", "", "cause", "", "toString", "", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private final class RemoveReceiveOnCancel extends CancelHandler
    {
        private final Receive<?> receive;
        
        public RemoveReceiveOnCancel(final Receive<?> receive) {
            Intrinsics.checkParameterIsNotNull(receive, "receive");
            this.receive = receive;
        }
        
        @Override
        public void invoke(final Throwable t) {
            if (this.receive.remove()) {
                AbstractChannel.this.onReceiveDequeued();
            }
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("RemoveReceiveOnCancel[");
            sb.append(this.receive);
            sb.append(']');
            return sb.toString();
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u00002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0004\u0018\u0000*\u0004\b\u0001\u0010\u00012\u0012\u0012\u0004\u0012\u00020\u00030\u0002j\b\u0012\u0004\u0012\u00020\u0003`\u0004B\r\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0012\u0010\f\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\r\u001a\u00020\u000eH\u0014J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0003H\u0014R\u0016\u0010\b\u001a\u0004\u0018\u00018\u00018\u0006@\u0006X\u0087\u000e¢\u0006\u0004\n\u0002\u0010\tR\u0014\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000¨\u0006\u0012" }, d2 = { "Lkotlinx/coroutines/channels/AbstractChannel$TryPollDesc;", "E", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode$RemoveFirstDesc;", "Lkotlinx/coroutines/channels/Send;", "Lkotlinx/coroutines/internal/RemoveFirstDesc;", "queue", "Lkotlinx/coroutines/internal/LockFreeLinkedListHead;", "(Lkotlinx/coroutines/internal/LockFreeLinkedListHead;)V", "pollResult", "Ljava/lang/Object;", "resumeToken", "", "failure", "affected", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "validatePrepared", "", "node", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    protected static final class TryPollDesc<E> extends RemoveFirstDesc<Send>
    {
        public E pollResult;
        public Object resumeToken;
        
        public TryPollDesc(final LockFreeLinkedListHead lockFreeLinkedListHead) {
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListHead, "queue");
            super(lockFreeLinkedListHead);
        }
        
        @Override
        protected Object failure(final LockFreeLinkedListNode lockFreeLinkedListNode) {
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "affected");
            if (lockFreeLinkedListNode instanceof Closed) {
                return lockFreeLinkedListNode;
            }
            if (!(lockFreeLinkedListNode instanceof Send)) {
                return AbstractChannelKt.POLL_FAILED;
            }
            return null;
        }
        
        protected boolean validatePrepared(final Send send) {
            Intrinsics.checkParameterIsNotNull(send, "node");
            final Object tryResumeSend = send.tryResumeSend(this);
            if (tryResumeSend != null) {
                this.resumeToken = tryResumeSend;
                this.pollResult = (E)send.getPollResult();
                return true;
            }
            return false;
        }
    }
}
