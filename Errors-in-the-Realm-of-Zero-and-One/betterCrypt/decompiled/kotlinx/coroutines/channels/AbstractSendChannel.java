// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.channels;

import kotlin.coroutines.ContinuationKt;
import kotlinx.coroutines.DebugKt;
import kotlinx.coroutines.DebugStringsKt;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.ResultKt;
import kotlin.Result;
import kotlinx.coroutines.CancellableContinuationKt;
import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlinx.coroutines.YieldKt;
import kotlinx.coroutines.internal.AtomicDesc;
import kotlin.Unit;
import kotlinx.coroutines.selects.SelectClause2;
import kotlinx.coroutines.intrinsics.UndispatchedKt;
import kotlinx.coroutines.selects.SelectKt;
import kotlinx.coroutines.internal.StackTraceRecoveryKt;
import kotlinx.coroutines.DisposableHandle;
import kotlin.jvm.internal.TypeIntrinsics;
import kotlin.jvm.functions.Function1;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.internal.LockFreeLinkedListNode;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.selects.SelectInstance;
import kotlinx.coroutines.internal.LockFreeLinkedListHead;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u009c\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\f\b \u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u00028\u00000\u0002:\u0005_`abcB\u0007¢\u0006\u0004\b\u0003\u0010\u0004J\u0019\u0010\b\u001a\u00020\u00072\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0016¢\u0006\u0004\b\b\u0010\tJ\u000f\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\u000b\u0010\fJ#\u0010\u0010\u001a\u000e\u0012\u0002\b\u00030\u000ej\u0006\u0012\u0002\b\u0003`\u000f2\u0006\u0010\r\u001a\u00028\u0000H\u0004¢\u0006\u0004\b\u0010\u0010\u0011J#\u0010\u0012\u001a\u000e\u0012\u0002\b\u00030\u000ej\u0006\u0012\u0002\b\u0003`\u000f2\u0006\u0010\r\u001a\u00028\u0000H\u0004¢\u0006\u0004\b\u0012\u0010\u0011J\u001d\u0010\u0014\u001a\b\u0012\u0004\u0012\u00028\u00000\u00132\u0006\u0010\r\u001a\u00028\u0000H\u0004¢\u0006\u0004\b\u0014\u0010\u0015J\u0019\u0010\u0019\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0017\u001a\u00020\u0016H\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u001b\u0010\u001e\u001a\u00020\u001d2\n\u0010\u001c\u001a\u0006\u0012\u0002\b\u00030\u001bH\u0002¢\u0006\u0004\b\u001e\u0010\u001fJ)\u0010#\u001a\u00020\u001d2\u0018\u0010\"\u001a\u0014\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0012\u0004\u0012\u00020\u001d0 j\u0002`!H\u0016¢\u0006\u0004\b#\u0010$J\u0019\u0010%\u001a\u00020\u001d2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0002¢\u0006\u0004\b%\u0010&J\u0015\u0010'\u001a\u00020\u00072\u0006\u0010\r\u001a\u00028\u0000¢\u0006\u0004\b'\u0010(J\u0017\u0010)\u001a\u00020\u00182\u0006\u0010\r\u001a\u00028\u0000H\u0014¢\u0006\u0004\b)\u0010*J#\u0010-\u001a\u00020\u00182\u0006\u0010\r\u001a\u00028\u00002\n\u0010,\u001a\u0006\u0012\u0002\b\u00030+H\u0014¢\u0006\u0004\b-\u0010.J\u0017\u00100\u001a\u00020\u001d2\u0006\u0010\u001c\u001a\u00020/H\u0014¢\u0006\u0004\b0\u00101JX\u00106\u001a\u00020\u001d\"\u0004\b\u0001\u001022\f\u0010,\u001a\b\u0012\u0004\u0012\u00028\u00010+2\u0006\u0010\r\u001a\u00028\u00002(\u00105\u001a$\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u000104\u0012\u0006\u0012\u0004\u0018\u00010\u001803H\u0002\u00f8\u0001\u0000¢\u0006\u0004\b6\u00107J\u001b\u0010\u0017\u001a\u00020\u001d2\u0006\u0010\r\u001a\u00028\u0000H\u0086@\u00f8\u0001\u0000¢\u0006\u0004\b\u0017\u00108J\u001d\u0010:\u001a\b\u0012\u0002\b\u0003\u0018\u0001092\u0006\u0010\r\u001a\u00028\u0000H\u0004¢\u0006\u0004\b:\u0010;J\u001b\u0010=\u001a\u00020\u001d2\u0006\u0010\r\u001a\u00028\u0000H\u0080@\u00f8\u0001\u0000¢\u0006\u0004\b<\u00108J\u001b\u0010>\u001a\u00020\u001d2\u0006\u0010\r\u001a\u00028\u0000H\u0082@\u00f8\u0001\u0000¢\u0006\u0004\b>\u00108J\u0017\u0010?\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u000109H\u0014¢\u0006\u0004\b?\u0010@J\u0011\u0010A\u001a\u0004\u0018\u00010\u0016H\u0004¢\u0006\u0004\bA\u0010BJ\u000f\u0010D\u001a\u00020CH\u0016¢\u0006\u0004\bD\u0010ER\u0016\u0010G\u001a\u00020C8T@\u0014X\u0094\u0004¢\u0006\u0006\u001a\u0004\bF\u0010ER\u001c\u0010J\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u001b8D@\u0004X\u0084\u0004¢\u0006\u0006\u001a\u0004\bH\u0010IR\u001c\u0010L\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u001b8D@\u0004X\u0084\u0004¢\u0006\u0006\u001a\u0004\bK\u0010IR\u0016\u0010O\u001a\u00020\u00078B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\bM\u0010NR\u0016\u0010P\u001a\u00020\u00078$@$X¤\u0004¢\u0006\u0006\u001a\u0004\bP\u0010NR\u0016\u0010Q\u001a\u00020\u00078$@$X¤\u0004¢\u0006\u0006\u001a\u0004\bQ\u0010NR\u0013\u0010R\u001a\u00020\u00078F@\u0006¢\u0006\u0006\u001a\u0004\bR\u0010NR\u0013\u0010S\u001a\u00020\u00078F@\u0006¢\u0006\u0006\u001a\u0004\bS\u0010NR%\u0010W\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u00020T8F@\u0006¢\u0006\u0006\u001a\u0004\bU\u0010VR\u001c\u0010Y\u001a\u00020X8\u0004@\u0004X\u0084\u0004¢\u0006\f\n\u0004\bY\u0010Z\u001a\u0004\b[\u0010\\R\u0016\u0010^\u001a\u00020C8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b]\u0010E\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006d" }, d2 = { "Lkotlinx/coroutines/channels/AbstractSendChannel;", "E", "Lkotlinx/coroutines/channels/SendChannel;", "<init>", "()V", "", "cause", "", "close", "(Ljava/lang/Throwable;)Z", "", "countQueueSize", "()I", "element", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode$AddLastDesc;", "Lkotlinx/coroutines/internal/AddLastDesc;", "describeSendBuffered", "(Ljava/lang/Object;)Lkotlinx/coroutines/internal/LockFreeLinkedListNode$AddLastDesc;", "describeSendConflated", "Lkotlinx/coroutines/channels/AbstractSendChannel$TryOfferDesc;", "describeTryOffer", "(Ljava/lang/Object;)Lkotlinx/coroutines/channels/AbstractSendChannel$TryOfferDesc;", "Lkotlinx/coroutines/channels/Send;", "send", "", "enqueueSend", "(Lkotlinx/coroutines/channels/Send;)Ljava/lang/Object;", "Lkotlinx/coroutines/channels/Closed;", "closed", "", "helpClose", "(Lkotlinx/coroutines/channels/Closed;)V", "Lkotlin/Function1;", "Lkotlinx/coroutines/channels/Handler;", "handler", "invokeOnClose", "(Lkotlin/jvm/functions/Function1;)V", "invokeOnCloseHandler", "(Ljava/lang/Throwable;)V", "offer", "(Ljava/lang/Object;)Z", "offerInternal", "(Ljava/lang/Object;)Ljava/lang/Object;", "Lkotlinx/coroutines/selects/SelectInstance;", "select", "offerSelectInternal", "(Ljava/lang/Object;Lkotlinx/coroutines/selects/SelectInstance;)Ljava/lang/Object;", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "onClosedIdempotent", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;)V", "R", "Lkotlin/Function2;", "Lkotlin/coroutines/Continuation;", "block", "registerSelectSend", "(Lkotlinx/coroutines/selects/SelectInstance;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)V", "(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Lkotlinx/coroutines/channels/ReceiveOrClosed;", "sendBuffered", "(Ljava/lang/Object;)Lkotlinx/coroutines/channels/ReceiveOrClosed;", "sendFair$kotlinx_coroutines_core", "sendFair", "sendSuspend", "takeFirstReceiveOrPeekClosed", "()Lkotlinx/coroutines/channels/ReceiveOrClosed;", "takeFirstSendOrPeekClosed", "()Lkotlinx/coroutines/channels/Send;", "", "toString", "()Ljava/lang/String;", "getBufferDebugString", "bufferDebugString", "getClosedForReceive", "()Lkotlinx/coroutines/channels/Closed;", "closedForReceive", "getClosedForSend", "closedForSend", "getFull", "()Z", "full", "isBufferAlwaysFull", "isBufferFull", "isClosedForSend", "isFull", "Lkotlinx/coroutines/selects/SelectClause2;", "getOnSend", "()Lkotlinx/coroutines/selects/SelectClause2;", "onSend", "Lkotlinx/coroutines/internal/LockFreeLinkedListHead;", "queue", "Lkotlinx/coroutines/internal/LockFreeLinkedListHead;", "getQueue", "()Lkotlinx/coroutines/internal/LockFreeLinkedListHead;", "getQueueDebugStateString", "queueDebugStateString", "SendBuffered", "SendBufferedDesc", "SendConflatedDesc", "SendSelect", "TryOfferDesc", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public abstract class AbstractSendChannel<E> implements SendChannel<E>
{
    private static final AtomicReferenceFieldUpdater onCloseHandler$FU;
    private volatile Object onCloseHandler;
    private final LockFreeLinkedListHead queue;
    
    static {
        onCloseHandler$FU = AtomicReferenceFieldUpdater.newUpdater(AbstractSendChannel.class, Object.class, "onCloseHandler");
    }
    
    public AbstractSendChannel() {
        this.queue = new LockFreeLinkedListHead();
        this.onCloseHandler = null;
    }
    
    private final int countQueueSize() {
        final LockFreeLinkedListHead queue = this.queue;
        final Object next = queue.getNext();
        if (next != null) {
            LockFreeLinkedListNode nextNode = (LockFreeLinkedListNode)next;
            int n = 0;
            while (Intrinsics.areEqual(nextNode, queue) ^ true) {
                int n2 = n;
                if (nextNode instanceof LockFreeLinkedListNode) {
                    n2 = n + 1;
                }
                nextNode = nextNode.getNextNode();
                n = n2;
            }
            return n;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }
    
    private final Object enqueueSend(final Send send) {
        if (!this.isBufferAlwaysFull()) {
            final LockFreeLinkedListHead queue = this.queue;
            final Send send2 = send;
            final LockFreeLinkedListNode.CondAddOp condAddOp = (LockFreeLinkedListNode.CondAddOp)new AbstractSendChannel$enqueueSend$$inlined$addLastIfPrevAndIf.AbstractSendChannel$enqueueSend$$inlined$addLastIfPrevAndIf$1((LockFreeLinkedListNode)send2, (LockFreeLinkedListNode)send2, this);
            boolean b;
            while (true) {
                final Object prev = queue.getPrev();
                if (prev == null) {
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                }
                final LockFreeLinkedListNode lockFreeLinkedListNode = (LockFreeLinkedListNode)prev;
                if (lockFreeLinkedListNode instanceof ReceiveOrClosed) {
                    return lockFreeLinkedListNode;
                }
                final int tryCondAddNext = lockFreeLinkedListNode.tryCondAddNext(send2, queue, condAddOp);
                b = true;
                if (tryCondAddNext == 1) {
                    break;
                }
                if (tryCondAddNext != 2) {
                    continue;
                }
                b = false;
                break;
            }
            if (!b) {
                return AbstractChannelKt.ENQUEUE_FAILED;
            }
            return null;
        }
        final LockFreeLinkedListHead queue2 = this.queue;
        LockFreeLinkedListNode lockFreeLinkedListNode2;
        do {
            final Object prev2 = queue2.getPrev();
            if (prev2 == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
            lockFreeLinkedListNode2 = (LockFreeLinkedListNode)prev2;
            if (lockFreeLinkedListNode2 instanceof ReceiveOrClosed) {
                return lockFreeLinkedListNode2;
            }
        } while (!lockFreeLinkedListNode2.addNext(send, queue2));
        return null;
    }
    
    private final boolean getFull() {
        return !(this.queue.getNextNode() instanceof ReceiveOrClosed) && this.isBufferFull();
    }
    
    private final String getQueueDebugStateString() {
        final LockFreeLinkedListNode nextNode = this.queue.getNextNode();
        if (nextNode == this.queue) {
            return "EmptyQueue";
        }
        String str;
        if (nextNode instanceof Closed) {
            str = nextNode.toString();
        }
        else if (nextNode instanceof Receive) {
            str = "ReceiveQueued";
        }
        else if (nextNode instanceof Send) {
            str = "SendQueued";
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("UNEXPECTED:");
            sb.append(nextNode);
            str = sb.toString();
        }
        final LockFreeLinkedListNode prevNode = this.queue.getPrevNode();
        String str2 = str;
        if (prevNode != nextNode) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append(",queueSize=");
            sb2.append(this.countQueueSize());
            str2 = sb2.toString();
            if (prevNode instanceof Closed) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(str2);
                sb3.append(",closedForSend=");
                sb3.append(prevNode);
                str2 = sb3.toString();
            }
        }
        return str2;
    }
    
    private final void helpClose(final Closed<?> closed) {
        while (true) {
            final LockFreeLinkedListNode prevNode = closed.getPrevNode();
            if (prevNode instanceof LockFreeLinkedListHead || !(prevNode instanceof Receive)) {
                break;
            }
            if (!prevNode.remove()) {
                prevNode.helpRemove();
            }
            else {
                ((Receive)prevNode).resumeReceiveClosed(closed);
            }
        }
        this.onClosedIdempotent(closed);
    }
    
    private final void invokeOnCloseHandler(final Throwable t) {
        final Object onCloseHandler = this.onCloseHandler;
        if (onCloseHandler != null && onCloseHandler != AbstractChannelKt.HANDLER_INVOKED && AbstractSendChannel.onCloseHandler$FU.compareAndSet(this, onCloseHandler, AbstractChannelKt.HANDLER_INVOKED)) {
            ((Function1)TypeIntrinsics.beforeCheckcastToFunctionOfArity(onCloseHandler, 1)).invoke(t);
        }
    }
    
    private final <R> void registerSelectSend(final SelectInstance<? super R> selectInstance, final E e, final Function2<? super SendChannel<? super E>, ? super Continuation<? super R>, ?> function2) {
        while (!selectInstance.isSelected()) {
            if (this.getFull()) {
                final SendSelect<Object, Object> sendSelect = new SendSelect<Object, Object>(e, (SendChannel<? super Object>)this, (SelectInstance<? super Object>)selectInstance, function2);
                final Object enqueueSend = this.enqueueSend(sendSelect);
                if (enqueueSend == null) {
                    selectInstance.disposeOnSelect(sendSelect);
                    return;
                }
                if (enqueueSend instanceof Closed) {
                    final Closed<?> closed = (Closed<?>)enqueueSend;
                    this.helpClose(closed);
                    throw StackTraceRecoveryKt.recoverStackTrace(closed.getSendException());
                }
                if (enqueueSend != AbstractChannelKt.ENQUEUE_FAILED) {
                    if (!(enqueueSend instanceof Receive)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("enqueueSend returned ");
                        sb.append(enqueueSend);
                        sb.append(' ');
                        throw new IllegalStateException(sb.toString().toString());
                    }
                }
            }
            final Object offerSelectInternal = this.offerSelectInternal(e, selectInstance);
            if (offerSelectInternal == SelectKt.getALREADY_SELECTED()) {
                return;
            }
            if (offerSelectInternal == AbstractChannelKt.OFFER_FAILED) {
                continue;
            }
            if (offerSelectInternal == AbstractChannelKt.OFFER_SUCCESS) {
                UndispatchedKt.startCoroutineUnintercepted((Function2<? super AbstractSendChannel, ? super Continuation<? super Object>, ?>)function2, this, selectInstance.getCompletion());
                return;
            }
            if (offerSelectInternal instanceof Closed) {
                final Closed<?> closed2 = (Closed<?>)offerSelectInternal;
                this.helpClose(closed2);
                throw StackTraceRecoveryKt.recoverStackTrace(closed2.getSendException());
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("offerSelectInternal returned ");
            sb2.append(offerSelectInternal);
            throw new IllegalStateException(sb2.toString().toString());
        }
    }
    
    @Override
    public boolean close(final Throwable t) {
        final Closed<Object> closed = new Closed<Object>(t);
        final LockFreeLinkedListHead queue = this.queue;
        while (true) {
            LockFreeLinkedListNode lockFreeLinkedListNode;
            do {
                final Object prev = queue.getPrev();
                if (prev == null) {
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                }
                lockFreeLinkedListNode = (LockFreeLinkedListNode)prev;
                if (lockFreeLinkedListNode instanceof Closed ^ true) {
                    continue;
                }
                final boolean b = false;
                if (b) {
                    this.helpClose(closed);
                    this.invokeOnCloseHandler(t);
                    return true;
                }
                final LockFreeLinkedListNode prevNode = this.queue.getPrevNode();
                if (prevNode != null) {
                    this.helpClose((Closed<?>)prevNode);
                    return false;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.channels.Closed<*>");
            } while (!lockFreeLinkedListNode.addNext(closed, queue));
            final boolean b = true;
            continue;
        }
    }
    
    protected final LockFreeLinkedListNode.AddLastDesc<?> describeSendBuffered(final E e) {
        return new SendBufferedDesc(this.queue, e);
    }
    
    protected final LockFreeLinkedListNode.AddLastDesc<?> describeSendConflated(final E e) {
        return new SendConflatedDesc(this.queue, e);
    }
    
    protected final TryOfferDesc<E> describeTryOffer(final E e) {
        return new TryOfferDesc<E>(e, this.queue);
    }
    
    protected String getBufferDebugString() {
        return "";
    }
    
    protected final Closed<?> getClosedForReceive() {
        LockFreeLinkedListNode nextNode;
        if (!((nextNode = this.queue.getNextNode()) instanceof Closed)) {
            nextNode = null;
        }
        final Closed<?> closed = (Closed<?>)nextNode;
        if (closed != null) {
            this.helpClose(closed);
            return closed;
        }
        return null;
    }
    
    protected final Closed<?> getClosedForSend() {
        LockFreeLinkedListNode prevNode;
        if (!((prevNode = this.queue.getPrevNode()) instanceof Closed)) {
            prevNode = null;
        }
        final Closed<?> closed = (Closed<?>)prevNode;
        if (closed != null) {
            this.helpClose(closed);
            return closed;
        }
        return null;
    }
    
    @Override
    public final SelectClause2<E, SendChannel<E>> getOnSend() {
        return (SelectClause2<E, SendChannel<E>>)new AbstractSendChannel$onSend.AbstractSendChannel$onSend$1(this);
    }
    
    protected final LockFreeLinkedListHead getQueue() {
        return this.queue;
    }
    
    @Override
    public void invokeOnClose(final Function1<? super Throwable, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(function1, "handler");
        if (AbstractSendChannel.onCloseHandler$FU.compareAndSet(this, null, function1)) {
            final Closed<?> closedForSend = this.getClosedForSend();
            if (closedForSend != null && AbstractSendChannel.onCloseHandler$FU.compareAndSet(this, function1, AbstractChannelKt.HANDLER_INVOKED)) {
                function1.invoke(closedForSend.closeCause);
            }
            return;
        }
        final Object onCloseHandler = this.onCloseHandler;
        if (onCloseHandler == AbstractChannelKt.HANDLER_INVOKED) {
            throw new IllegalStateException("Another handler was already registered and successfully invoked");
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Another handler was already registered: ");
        sb.append(onCloseHandler);
        throw new IllegalStateException(sb.toString());
    }
    
    protected abstract boolean isBufferAlwaysFull();
    
    protected abstract boolean isBufferFull();
    
    @Override
    public final boolean isClosedForSend() {
        return this.getClosedForSend() != null;
    }
    
    @Override
    public final boolean isFull() {
        return this.getFull();
    }
    
    @Override
    public final boolean offer(final E e) {
        final Object offerInternal = this.offerInternal(e);
        if (offerInternal == AbstractChannelKt.OFFER_SUCCESS) {
            return true;
        }
        if (offerInternal == AbstractChannelKt.OFFER_FAILED) {
            final Closed<?> closedForSend = this.getClosedForSend();
            if (closedForSend != null) {
                final Throwable sendException = closedForSend.getSendException();
                if (sendException != null) {
                    final Throwable recoverStackTrace = StackTraceRecoveryKt.recoverStackTrace(sendException);
                    if (recoverStackTrace != null) {
                        throw recoverStackTrace;
                    }
                }
            }
            return false;
        }
        if (offerInternal instanceof Closed) {
            throw StackTraceRecoveryKt.recoverStackTrace(((Closed)offerInternal).getSendException());
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("offerInternal returned ");
        sb.append(offerInternal);
        throw new IllegalStateException(sb.toString().toString());
    }
    
    protected Object offerInternal(final E e) {
        Object tryResumeReceive;
        ReceiveOrClosed<E> takeFirstReceiveOrPeekClosed;
        do {
            takeFirstReceiveOrPeekClosed = this.takeFirstReceiveOrPeekClosed();
            if (takeFirstReceiveOrPeekClosed == null) {
                return AbstractChannelKt.OFFER_FAILED;
            }
            tryResumeReceive = takeFirstReceiveOrPeekClosed.tryResumeReceive(e, null);
        } while (tryResumeReceive == null);
        takeFirstReceiveOrPeekClosed.completeResumeReceive(tryResumeReceive);
        return takeFirstReceiveOrPeekClosed.getOfferResult();
    }
    
    protected Object offerSelectInternal(final E e, final SelectInstance<?> selectInstance) {
        Intrinsics.checkParameterIsNotNull(selectInstance, "select");
        final TryOfferDesc<?> describeTryOffer = this.describeTryOffer(e);
        final Object performAtomicTrySelect = selectInstance.performAtomicTrySelect(describeTryOffer);
        if (performAtomicTrySelect != null) {
            return performAtomicTrySelect;
        }
        final ReceiveOrClosed receiveOrClosed = describeTryOffer.getResult();
        final Object resumeToken = describeTryOffer.resumeToken;
        if (resumeToken == null) {
            Intrinsics.throwNpe();
        }
        receiveOrClosed.completeResumeReceive(resumeToken);
        return receiveOrClosed.getOfferResult();
    }
    
    protected void onClosedIdempotent(final LockFreeLinkedListNode lockFreeLinkedListNode) {
        Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "closed");
    }
    
    @Override
    public final Object send(final E e, final Continuation<? super Unit> continuation) {
        if (this.offer(e)) {
            return Unit.INSTANCE;
        }
        return this.sendSuspend(e, continuation);
    }
    
    protected final ReceiveOrClosed<?> sendBuffered(final E e) {
        final LockFreeLinkedListHead queue = this.queue;
        LockFreeLinkedListNode lockFreeLinkedListNode;
        do {
            final Object prev = queue.getPrev();
            if (prev == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
            lockFreeLinkedListNode = (LockFreeLinkedListNode)prev;
            if (lockFreeLinkedListNode instanceof ReceiveOrClosed) {
                return (ReceiveOrClosed<?>)lockFreeLinkedListNode;
            }
        } while (!lockFreeLinkedListNode.addNext(new SendBuffered<Object>(e), queue));
        return null;
    }
    
    public final Object sendFair$kotlinx_coroutines_core(final E e, final Continuation<? super Unit> continuation) {
        if (this.offer(e)) {
            return YieldKt.yield(continuation);
        }
        return this.sendSuspend(e, continuation);
    }
    
    final /* synthetic */ Object sendSuspend(final E e, final Continuation<? super Unit> continuation) {
        final CancellableContinuationImpl<Object> cancellableContinuationImpl = new CancellableContinuationImpl<Object>(IntrinsicsKt__IntrinsicsJvmKt.intercepted((Continuation<? super Object>)continuation), 0);
        final CancellableContinuationImpl<Object> cancellableContinuationImpl2 = cancellableContinuationImpl;
        while (true) {
            if (this.getFull()) {
                final SendElement sendElement = new SendElement(e, cancellableContinuationImpl2);
                final Object access$enqueueSend = this.enqueueSend(sendElement);
                if (access$enqueueSend == null) {
                    CancellableContinuationKt.removeOnCancellation(cancellableContinuationImpl2, sendElement);
                    break;
                }
                if (access$enqueueSend instanceof Closed) {
                    final Closed closed = (Closed)access$enqueueSend;
                    this.helpClose(closed);
                    final CancellableContinuationImpl<Object> cancellableContinuationImpl3 = cancellableContinuationImpl2;
                    final Throwable sendException = closed.getSendException();
                    final Result.Companion companion = Result.Companion;
                    cancellableContinuationImpl3.resumeWith(Result.constructor-impl(ResultKt.createFailure(sendException)));
                    break;
                }
                if (access$enqueueSend != AbstractChannelKt.ENQUEUE_FAILED) {
                    if (!(access$enqueueSend instanceof Receive)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("enqueueSend returned ");
                        sb.append(access$enqueueSend);
                        throw new IllegalStateException(sb.toString().toString());
                    }
                }
            }
            final Object offerInternal = this.offerInternal(e);
            if (offerInternal == AbstractChannelKt.OFFER_SUCCESS) {
                final CancellableContinuationImpl<Object> cancellableContinuationImpl4 = cancellableContinuationImpl2;
                final Unit instance = Unit.INSTANCE;
                final Result.Companion companion2 = Result.Companion;
                cancellableContinuationImpl4.resumeWith(Result.constructor-impl(instance));
                break;
            }
            if (offerInternal == AbstractChannelKt.OFFER_FAILED) {
                continue;
            }
            if (offerInternal instanceof Closed) {
                final Closed closed2 = (Closed)offerInternal;
                this.helpClose(closed2);
                final CancellableContinuationImpl<Object> cancellableContinuationImpl5 = cancellableContinuationImpl2;
                final Throwable sendException2 = closed2.getSendException();
                final Result.Companion companion3 = Result.Companion;
                cancellableContinuationImpl5.resumeWith(Result.constructor-impl(ResultKt.createFailure(sendException2)));
                break;
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("offerInternal returned ");
            sb2.append(offerInternal);
            throw new IllegalStateException(sb2.toString().toString());
        }
        final Object result = cancellableContinuationImpl.getResult();
        if (result == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            DebugProbesKt.probeCoroutineSuspended(continuation);
        }
        return result;
    }
    
    protected ReceiveOrClosed<E> takeFirstReceiveOrPeekClosed() {
        final LockFreeLinkedListHead queue = this.queue;
        Object o;
        while (true) {
            final Object next = queue.getNext();
            if (next == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
            o = next;
            final LockFreeLinkedListHead lockFreeLinkedListHead = queue;
            final ReceiveOrClosed<E> receiveOrClosed = null;
            if (o == lockFreeLinkedListHead) {
                o = receiveOrClosed;
                break;
            }
            if (!(o instanceof ReceiveOrClosed)) {
                o = receiveOrClosed;
                break;
            }
            if (((ReceiveOrClosed)o) instanceof Closed) {
                break;
            }
            if (((LockFreeLinkedListNode)o).remove()) {
                break;
            }
            ((LockFreeLinkedListNode)o).helpDelete();
        }
        return (ReceiveOrClosed<E>)o;
    }
    
    protected final Send takeFirstSendOrPeekClosed() {
        final LockFreeLinkedListHead queue = this.queue;
        LockFreeLinkedListNode lockFreeLinkedListNode;
        while (true) {
            final Object next = queue.getNext();
            if (next == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
            lockFreeLinkedListNode = (LockFreeLinkedListNode)next;
            final LockFreeLinkedListHead lockFreeLinkedListHead = queue;
            final Send send = null;
            if (lockFreeLinkedListNode == lockFreeLinkedListHead) {
                lockFreeLinkedListNode = send;
                break;
            }
            if (!(lockFreeLinkedListNode instanceof Send)) {
                lockFreeLinkedListNode = send;
                break;
            }
            if (((Send)lockFreeLinkedListNode) instanceof Closed) {
                break;
            }
            if (lockFreeLinkedListNode.remove()) {
                break;
            }
            lockFreeLinkedListNode.helpDelete();
        }
        return (Send)lockFreeLinkedListNode;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(DebugStringsKt.getClassSimpleName(this));
        sb.append('@');
        sb.append(DebugStringsKt.getHexAddress(this));
        sb.append('{');
        sb.append(this.getQueueDebugStateString());
        sb.append('}');
        sb.append(this.getBufferDebugString());
        return sb.toString();
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u0000*\u0006\b\u0001\u0010\u0001 \u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00028\u0001¢\u0006\u0002\u0010\u0004J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u0007H\u0016J\u0014\u0010\r\u001a\u00020\u000b2\n\u0010\u000e\u001a\u0006\u0012\u0002\b\u00030\u000fH\u0016J\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00072\b\u0010\u0011\u001a\u0004\u0018\u00010\u0007H\u0016R\u0012\u0010\u0003\u001a\u00028\u00018\u0006X\u0087\u0004¢\u0006\u0004\n\u0002\u0010\u0005R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\t\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0012" }, d2 = { "Lkotlinx/coroutines/channels/AbstractSendChannel$SendBuffered;", "E", "Lkotlinx/coroutines/channels/Send;", "element", "(Ljava/lang/Object;)V", "Ljava/lang/Object;", "pollResult", "", "getPollResult", "()Ljava/lang/Object;", "completeResumeSend", "", "token", "resumeSendClosed", "closed", "Lkotlinx/coroutines/channels/Closed;", "tryResumeSend", "idempotent", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    public static final class SendBuffered<E> extends Send
    {
        public final E element;
        
        public SendBuffered(final E element) {
            this.element = element;
        }
        
        @Override
        public void completeResumeSend(final Object o) {
            Intrinsics.checkParameterIsNotNull(o, "token");
            if (!DebugKt.getASSERTIONS_ENABLED()) {
                return;
            }
            if (o == AbstractChannelKt.SEND_RESUMED) {
                return;
            }
            throw new AssertionError();
        }
        
        @Override
        public Object getPollResult() {
            return this.element;
        }
        
        @Override
        public void resumeSendClosed(final Closed<?> closed) {
            Intrinsics.checkParameterIsNotNull(closed, "closed");
        }
        
        @Override
        public Object tryResumeSend(final Object o) {
            return AbstractChannelKt.SEND_RESUMED;
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0012\u0018\u0000*\u0004\b\u0001\u0010\u00012\u001e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00010\u00030\u0002j\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00010\u0003`\u0004B\u0015\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00028\u0001¢\u0006\u0002\u0010\bJ\u0012\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\fH\u0014¨\u0006\r" }, d2 = { "Lkotlinx/coroutines/channels/AbstractSendChannel$SendBufferedDesc;", "E", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode$AddLastDesc;", "Lkotlinx/coroutines/channels/AbstractSendChannel$SendBuffered;", "Lkotlinx/coroutines/internal/AddLastDesc;", "queue", "Lkotlinx/coroutines/internal/LockFreeLinkedListHead;", "element", "(Lkotlinx/coroutines/internal/LockFreeLinkedListHead;Ljava/lang/Object;)V", "failure", "", "affected", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static class SendBufferedDesc<E> extends AddLastDesc<SendBuffered<? extends E>>
    {
        public SendBufferedDesc(final LockFreeLinkedListHead lockFreeLinkedListHead, final E e) {
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListHead, "queue");
            super(lockFreeLinkedListHead, (SendBuffered<? extends E>)new SendBuffered(e));
        }
        
        @Override
        protected Object failure(final LockFreeLinkedListNode lockFreeLinkedListNode) {
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "affected");
            if (lockFreeLinkedListNode instanceof Closed) {
                return lockFreeLinkedListNode;
            }
            if (lockFreeLinkedListNode instanceof ReceiveOrClosed) {
                return AbstractChannelKt.OFFER_FAILED;
            }
            return null;
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0002\u0018\u0000*\u0004\b\u0001\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00028\u0001¢\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH\u0014¨\u0006\f" }, d2 = { "Lkotlinx/coroutines/channels/AbstractSendChannel$SendConflatedDesc;", "E", "Lkotlinx/coroutines/channels/AbstractSendChannel$SendBufferedDesc;", "queue", "Lkotlinx/coroutines/internal/LockFreeLinkedListHead;", "element", "(Lkotlinx/coroutines/internal/LockFreeLinkedListHead;Ljava/lang/Object;)V", "finishOnSuccess", "", "affected", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "next", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class SendConflatedDesc<E> extends SendBufferedDesc<E>
    {
        public SendConflatedDesc(final LockFreeLinkedListHead lockFreeLinkedListHead, final E e) {
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListHead, "queue");
            super(lockFreeLinkedListHead, e);
        }
        
        @Override
        protected void finishOnSuccess(final LockFreeLinkedListNode lockFreeLinkedListNode, LockFreeLinkedListNode lockFreeLinkedListNode2) {
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "affected");
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode2, "next");
            super.finishOnSuccess(lockFreeLinkedListNode, lockFreeLinkedListNode2);
            lockFreeLinkedListNode2 = lockFreeLinkedListNode;
            if (!(lockFreeLinkedListNode instanceof SendBuffered)) {
                lockFreeLinkedListNode2 = null;
            }
            final SendBuffered sendBuffered = (SendBuffered)lockFreeLinkedListNode2;
            if (sendBuffered != null) {
                sendBuffered.remove();
            }
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000F\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0002\u0018\u0000*\u0004\b\u0001\u0010\u0001*\u0004\b\u0002\u0010\u00022\u00020\u00032\u00020\u0004BX\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00010\b\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00028\u00020\n\u0012(\u0010\u000b\u001a$\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00010\b\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00020\r\u0012\u0006\u0012\u0004\u0018\u00010\u00060\f\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000eJ\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0006H\u0016J\b\u0010\u0015\u001a\u00020\u0013H\u0016J\u0014\u0010\u0016\u001a\u00020\u00132\n\u0010\u0017\u001a\u0006\u0012\u0002\b\u00030\u0018H\u0016J\b\u0010\u0019\u001a\u00020\u001aH\u0016J\u0014\u0010\u001b\u001a\u0004\u0018\u00010\u00062\b\u0010\u001c\u001a\u0004\u0018\u00010\u0006H\u0016R7\u0010\u000b\u001a$\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00010\b\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00020\r\u0012\u0006\u0012\u0004\u0018\u00010\u00060\f8\u0006X\u0087\u0004\u00f8\u0001\u0000¢\u0006\u0004\n\u0002\u0010\u000fR\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00010\b8\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0016\u0010\t\u001a\b\u0012\u0004\u0012\u00028\u00020\n8\u0006X\u0087\u0004¢\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u001d" }, d2 = { "Lkotlinx/coroutines/channels/AbstractSendChannel$SendSelect;", "E", "R", "Lkotlinx/coroutines/channels/Send;", "Lkotlinx/coroutines/DisposableHandle;", "pollResult", "", "channel", "Lkotlinx/coroutines/channels/SendChannel;", "select", "Lkotlinx/coroutines/selects/SelectInstance;", "block", "Lkotlin/Function2;", "Lkotlin/coroutines/Continuation;", "(Ljava/lang/Object;Lkotlinx/coroutines/channels/SendChannel;Lkotlinx/coroutines/selects/SelectInstance;Lkotlin/jvm/functions/Function2;)V", "Lkotlin/jvm/functions/Function2;", "getPollResult", "()Ljava/lang/Object;", "completeResumeSend", "", "token", "dispose", "resumeSendClosed", "closed", "Lkotlinx/coroutines/channels/Closed;", "toString", "", "tryResumeSend", "idempotent", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class SendSelect<E, R> extends Send implements DisposableHandle
    {
        public final Function2<SendChannel<? super E>, Continuation<? super R>, Object> block;
        public final SendChannel<E> channel;
        private final Object pollResult;
        public final SelectInstance<R> select;
        
        public SendSelect(final Object pollResult, final SendChannel<? super E> channel, final SelectInstance<? super R> select, final Function2<? super SendChannel<? super E>, ? super Continuation<? super R>, ?> block) {
            Intrinsics.checkParameterIsNotNull(channel, "channel");
            Intrinsics.checkParameterIsNotNull(select, "select");
            Intrinsics.checkParameterIsNotNull(block, "block");
            this.pollResult = pollResult;
            this.channel = (SendChannel<E>)channel;
            this.select = (SelectInstance<R>)select;
            this.block = (Function2<SendChannel<? super E>, Continuation<? super R>, Object>)block;
        }
        
        @Override
        public void completeResumeSend(final Object o) {
            Intrinsics.checkParameterIsNotNull(o, "token");
            if (DebugKt.getASSERTIONS_ENABLED() && o != AbstractChannelKt.SELECT_STARTED) {
                throw new AssertionError();
            }
            ContinuationKt.startCoroutine((Function2<? super SendChannel<E>, ? super Continuation<? super Object>, ?>)this.block, this.channel, (Continuation<? super Object>)this.select.getCompletion());
        }
        
        @Override
        public void dispose() {
            this.remove();
        }
        
        @Override
        public Object getPollResult() {
            return this.pollResult;
        }
        
        @Override
        public void resumeSendClosed(final Closed<?> closed) {
            Intrinsics.checkParameterIsNotNull(closed, "closed");
            if (this.select.trySelect(null)) {
                this.select.resumeSelectCancellableWithException(closed.getSendException());
            }
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("SendSelect(");
            sb.append(this.getPollResult());
            sb.append(")[");
            sb.append(this.channel);
            sb.append(", ");
            sb.append(this.select);
            sb.append(']');
            return sb.toString();
        }
        
        @Override
        public Object tryResumeSend(final Object o) {
            if (this.select.trySelect(o)) {
                return AbstractChannelKt.SELECT_STARTED;
            }
            return null;
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u00004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0004\u0018\u0000*\u0004\b\u0001\u0010\u00012\u001e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00010\u00030\u0002j\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00010\u0003`\u0004B\u0015\u0012\u0006\u0010\u0005\u001a\u00028\u0001\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0012\u0010\f\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\r\u001a\u00020\u000eH\u0014J\u0016\u0010\u000f\u001a\u00020\u00102\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00010\u0003H\u0014R\u0012\u0010\u0005\u001a\u00028\u00018\u0006X\u0087\u0004¢\u0006\u0004\n\u0002\u0010\tR\u0014\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000¨\u0006\u0012" }, d2 = { "Lkotlinx/coroutines/channels/AbstractSendChannel$TryOfferDesc;", "E", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode$RemoveFirstDesc;", "Lkotlinx/coroutines/channels/ReceiveOrClosed;", "Lkotlinx/coroutines/internal/RemoveFirstDesc;", "element", "queue", "Lkotlinx/coroutines/internal/LockFreeLinkedListHead;", "(Ljava/lang/Object;Lkotlinx/coroutines/internal/LockFreeLinkedListHead;)V", "Ljava/lang/Object;", "resumeToken", "", "failure", "affected", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "validatePrepared", "", "node", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    protected static final class TryOfferDesc<E> extends RemoveFirstDesc<ReceiveOrClosed<? super E>>
    {
        public final E element;
        public Object resumeToken;
        
        public TryOfferDesc(final E element, final LockFreeLinkedListHead lockFreeLinkedListHead) {
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListHead, "queue");
            super(lockFreeLinkedListHead);
            this.element = element;
        }
        
        @Override
        protected Object failure(final LockFreeLinkedListNode lockFreeLinkedListNode) {
            Intrinsics.checkParameterIsNotNull(lockFreeLinkedListNode, "affected");
            if (lockFreeLinkedListNode instanceof Closed) {
                return lockFreeLinkedListNode;
            }
            if (!(lockFreeLinkedListNode instanceof ReceiveOrClosed)) {
                return AbstractChannelKt.OFFER_FAILED;
            }
            return null;
        }
        
        protected boolean validatePrepared(final ReceiveOrClosed<? super E> receiveOrClosed) {
            Intrinsics.checkParameterIsNotNull(receiveOrClosed, "node");
            final Object tryResumeReceive = receiveOrClosed.tryResumeReceive(this.element, this);
            if (tryResumeReceive != null) {
                this.resumeToken = tryResumeReceive;
                return true;
            }
            return false;
        }
    }
}
