// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.channels;

import kotlinx.coroutines.selects.SelectKt;
import kotlinx.coroutines.DebugKt;
import kotlinx.coroutines.internal.AtomicDesc;
import kotlinx.coroutines.internal.LockFreeLinkedListNode;
import kotlinx.coroutines.selects.SelectInstance;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import java.util.concurrent.locks.ReentrantLock;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000L\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0010\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\b\u0010\u001b\u001a\u00020\u001cH\u0014J\u0010\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u001e\u001a\u00020\u0004H\u0002J\u0015\u0010\u001f\u001a\u00020\b2\u0006\u0010 \u001a\u00028\u0000H\u0014¢\u0006\u0002\u0010!J!\u0010\"\u001a\u00020\b2\u0006\u0010 \u001a\u00028\u00002\n\u0010#\u001a\u0006\u0012\u0002\b\u00030$H\u0014¢\u0006\u0002\u0010%J\n\u0010&\u001a\u0004\u0018\u00010\bH\u0014J\u0016\u0010'\u001a\u0004\u0018\u00010\b2\n\u0010#\u001a\u0006\u0012\u0002\b\u00030$H\u0014R\u0018\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\b0\u0007X\u0082\u000e¢\u0006\u0004\n\u0002\u0010\tR\u0014\u0010\n\u001a\u00020\u000b8TX\u0094\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u00020\u00128DX\u0084\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0013R\u0014\u0010\u0014\u001a\u00020\u00128DX\u0084\u0004¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0013R\u0014\u0010\u0015\u001a\u00020\u00128DX\u0084\u0004¢\u0006\u0006\u001a\u0004\b\u0015\u0010\u0013R\u0014\u0010\u0016\u001a\u00020\u00128DX\u0084\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0013R\u0012\u0010\u0017\u001a\u00060\u0018j\u0002`\u0019X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006(" }, d2 = { "Lkotlinx/coroutines/channels/ArrayChannel;", "E", "Lkotlinx/coroutines/channels/AbstractChannel;", "capacity", "", "(I)V", "buffer", "", "", "[Ljava/lang/Object;", "bufferDebugString", "", "getBufferDebugString", "()Ljava/lang/String;", "getCapacity", "()I", "head", "isBufferAlwaysEmpty", "", "()Z", "isBufferAlwaysFull", "isBufferEmpty", "isBufferFull", "lock", "Ljava/util/concurrent/locks/ReentrantLock;", "Lkotlinx/coroutines/internal/ReentrantLock;", "size", "cleanupSendQueueOnCancel", "", "ensureCapacity", "currentSize", "offerInternal", "element", "(Ljava/lang/Object;)Ljava/lang/Object;", "offerSelectInternal", "select", "Lkotlinx/coroutines/selects/SelectInstance;", "(Ljava/lang/Object;Lkotlinx/coroutines/selects/SelectInstance;)Ljava/lang/Object;", "pollInternal", "pollSelectInternal", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public class ArrayChannel<E> extends AbstractChannel<E>
{
    private Object[] buffer;
    private final int capacity;
    private int head;
    private final ReentrantLock lock;
    private volatile int size;
    
    public ArrayChannel(int capacity) {
        this.capacity = capacity;
        final int capacity2 = this.capacity;
        capacity = 1;
        if (capacity2 < 1) {
            capacity = 0;
        }
        if (capacity != 0) {
            this.lock = new ReentrantLock();
            this.buffer = new Object[Math.min(this.capacity, 8)];
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("ArrayChannel capacity must be at least 1, but ");
        sb.append(this.capacity);
        sb.append(" was specified");
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    private final void ensureCapacity(final int n) {
        final Object[] buffer = this.buffer;
        if (n >= buffer.length) {
            final Object[] buffer2 = new Object[Math.min(buffer.length * 2, this.capacity)];
            for (int i = 0; i < n; ++i) {
                final Object[] buffer3 = this.buffer;
                buffer2[i] = buffer3[(this.head + i) % buffer3.length];
            }
            this.buffer = buffer2;
            this.head = 0;
        }
    }
    
    @Override
    protected void cleanupSendQueueOnCancel() {
        final ReentrantLock reentrantLock = this.lock;
        reentrantLock.lock();
        try {
            for (int size = this.size, i = 0; i < size; ++i) {
                this.buffer[this.head] = 0;
                this.head = (this.head + 1) % this.buffer.length;
            }
            this.size = 0;
            final Unit instance = Unit.INSTANCE;
            reentrantLock.unlock();
            super.cleanupSendQueueOnCancel();
        }
        finally {
            reentrantLock.unlock();
        }
    }
    
    @Override
    protected String getBufferDebugString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("(buffer:capacity=");
        sb.append(this.capacity);
        sb.append(",size=");
        sb.append(this.size);
        sb.append(')');
        return sb.toString();
    }
    
    public final int getCapacity() {
        return this.capacity;
    }
    
    @Override
    protected final boolean isBufferAlwaysEmpty() {
        return false;
    }
    
    @Override
    protected final boolean isBufferAlwaysFull() {
        return false;
    }
    
    @Override
    protected final boolean isBufferEmpty() {
        return this.size == 0;
    }
    
    @Override
    protected final boolean isBufferFull() {
        return this.size == this.capacity;
    }
    
    @Override
    protected Object offerInternal(final E e) {
        final ReceiveOrClosed receiveOrClosed = null;
        final ReentrantLock reentrantLock = this.lock;
        reentrantLock.lock();
        try {
            final int size = this.size;
            final Closed<?> closedForSend = this.getClosedForSend();
            if (closedForSend != null) {
                return closedForSend;
            }
            if (size < this.capacity) {
                this.size = size + 1;
                Label_0176: {
                    if (size == 0) {
                        Object tryResumeReceive;
                        ReceiveOrClosed<E> takeFirstReceiveOrPeekClosed;
                        do {
                            takeFirstReceiveOrPeekClosed = this.takeFirstReceiveOrPeekClosed();
                            if (takeFirstReceiveOrPeekClosed == null) {
                                break Label_0176;
                            }
                            if (takeFirstReceiveOrPeekClosed instanceof Closed) {
                                this.size = size;
                                if (takeFirstReceiveOrPeekClosed == null) {
                                    Intrinsics.throwNpe();
                                }
                                return takeFirstReceiveOrPeekClosed;
                            }
                            if (takeFirstReceiveOrPeekClosed == null) {
                                Intrinsics.throwNpe();
                            }
                            tryResumeReceive = takeFirstReceiveOrPeekClosed.tryResumeReceive(e, null);
                        } while (tryResumeReceive == null);
                        this.size = size;
                        final Unit instance = Unit.INSTANCE;
                        reentrantLock.unlock();
                        if (takeFirstReceiveOrPeekClosed == null) {
                            Intrinsics.throwNpe();
                        }
                        takeFirstReceiveOrPeekClosed.completeResumeReceive(tryResumeReceive);
                        if (takeFirstReceiveOrPeekClosed == null) {
                            Intrinsics.throwNpe();
                        }
                        return takeFirstReceiveOrPeekClosed.getOfferResult();
                    }
                }
                this.ensureCapacity(size);
                this.buffer[(this.head + size) % this.buffer.length] = e;
                return AbstractChannelKt.OFFER_SUCCESS;
            }
            return AbstractChannelKt.OFFER_FAILED;
        }
        finally {
            reentrantLock.unlock();
        }
    }
    
    @Override
    protected Object offerSelectInternal(final E e, SelectInstance<?> resumeToken) {
    Label_0133_Outer:
        while (true) {
            Intrinsics.checkParameterIsNotNull(resumeToken, "select");
            final ReceiveOrClosed receiveOrClosed = null;
            final ReentrantLock reentrantLock = this.lock;
            reentrantLock.lock();
            while (true) {
                while (true) {
                    int size = 0;
                    Label_0365: {
                        Label_0363: {
                            try {
                                size = this.size;
                                final Closed<?> closedForSend = this.getClosedForSend();
                                if (closedForSend != null) {
                                    return closedForSend;
                                }
                                if (size >= this.capacity) {
                                    return AbstractChannelKt.OFFER_FAILED;
                                }
                                this.size = size + 1;
                                if (size == 0) {
                                    final TryOfferDesc<E> describeTryOffer = this.describeTryOffer(e);
                                    final Object performAtomicTrySelect = ((SelectInstance)resumeToken).performAtomicTrySelect(describeTryOffer);
                                    if (performAtomicTrySelect == null) {
                                        this.size = size;
                                        final ReceiveOrClosed receiveOrClosed2 = describeTryOffer.getResult();
                                        resumeToken = describeTryOffer.resumeToken;
                                        if (!DebugKt.getASSERTIONS_ENABLED()) {
                                            final Unit instance = Unit.INSTANCE;
                                            reentrantLock.unlock();
                                            if (receiveOrClosed2 == null) {
                                                Intrinsics.throwNpe();
                                            }
                                            if (resumeToken == null) {
                                                Intrinsics.throwNpe();
                                            }
                                            receiveOrClosed2.completeResumeReceive(resumeToken);
                                            if (receiveOrClosed2 == null) {
                                                Intrinsics.throwNpe();
                                            }
                                            return receiveOrClosed2.getOfferResult();
                                        }
                                        if (resumeToken != null) {
                                            size = 1;
                                            break Label_0365;
                                        }
                                        break Label_0363;
                                    }
                                    else if (performAtomicTrySelect != AbstractChannelKt.OFFER_FAILED) {
                                        if (performAtomicTrySelect != SelectKt.getALREADY_SELECTED() && !(performAtomicTrySelect instanceof Closed)) {
                                            final StringBuilder sb = new StringBuilder();
                                            sb.append("performAtomicTrySelect(describeTryOffer) returned ");
                                            sb.append(performAtomicTrySelect);
                                            throw new IllegalStateException(sb.toString().toString());
                                        }
                                        this.size = size;
                                        return performAtomicTrySelect;
                                    }
                                }
                                if (!((SelectInstance)resumeToken).trySelect(null)) {
                                    this.size = size;
                                    return SelectKt.getALREADY_SELECTED();
                                }
                                this.ensureCapacity(size);
                                this.buffer[(this.head + size) % this.buffer.length] = e;
                                return AbstractChannelKt.OFFER_SUCCESS;
                                throw new AssertionError();
                            }
                            finally {
                                reentrantLock.unlock();
                            }
                        }
                        size = 0;
                    }
                    if (size != 0) {
                        continue Label_0133_Outer;
                    }
                    break;
                }
                continue;
            }
        }
    }
    
    @Override
    protected Object pollInternal() {
    Label_0153_Outer:
        while (true) {
            Send send = null;
            final ReentrantLock reentrantLock = this.lock;
            reentrantLock.lock();
            while (true) {
                Object poll_FAILED = null;
            Label_0256:
                while (true) {
                    Object o3 = null;
                    Label_0250: {
                        try {
                            final int size = this.size;
                            if (size == 0) {
                                Object o = this.getClosedForSend();
                                if (o == null) {
                                    o = AbstractChannelKt.POLL_FAILED;
                                }
                                return o;
                            }
                            final Object o2 = this.buffer[this.head];
                            this.buffer[this.head] = null;
                            this.size = size - 1;
                            poll_FAILED = AbstractChannelKt.POLL_FAILED;
                            if (size == this.capacity) {
                                Object tryResumeSend = null;
                                o3 = this.takeFirstSendOrPeekClosed();
                                Object pollResult = poll_FAILED;
                                if (o3 != null) {
                                    if (o3 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    tryResumeSend = ((Send)o3).tryResumeSend(null);
                                    if (tryResumeSend == null) {
                                        break Label_0250;
                                    }
                                    if (o3 == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    pollResult = ((Send)o3).getPollResult();
                                    send = (Send)o3;
                                }
                                if (pollResult != AbstractChannelKt.POLL_FAILED && !(pollResult instanceof Closed)) {
                                    this.size = size;
                                    this.buffer[(this.head + size) % this.buffer.length] = pollResult;
                                }
                                this.head = (this.head + 1) % this.buffer.length;
                                o3 = Unit.INSTANCE;
                                reentrantLock.unlock();
                                if (tryResumeSend != null) {
                                    if (send == null) {
                                        Intrinsics.throwNpe();
                                    }
                                    send.completeResumeSend(tryResumeSend);
                                }
                                return o2;
                            }
                            break Label_0256;
                        }
                        finally {
                            reentrantLock.unlock();
                        }
                    }
                    send = (Send)o3;
                    continue Label_0153_Outer;
                }
                Object tryResumeSend = null;
                Object pollResult = poll_FAILED;
                continue;
            }
        }
    }
    
    @Override
    protected Object pollSelectInternal(final SelectInstance<?> selectInstance) {
    Label_0303_Outer:
        while (true) {
            Intrinsics.checkParameterIsNotNull(selectInstance, "select");
            Send send = null;
            final ReentrantLock reentrantLock = this.lock;
            reentrantLock.lock();
            while (true) {
                Object poll_FAILED = null;
                Label_0454: {
                    while (true) {
                        while (true) {
                            boolean b = false;
                            Label_0447: {
                                Label_0445: {
                                    try {
                                        final int size = this.size;
                                        if (size == 0) {
                                            Object o = this.getClosedForSend();
                                            if (o == null) {
                                                o = AbstractChannelKt.POLL_FAILED;
                                            }
                                            return o;
                                        }
                                        final Object o2 = this.buffer[this.head];
                                        this.buffer[this.head] = null;
                                        this.size = size - 1;
                                        poll_FAILED = AbstractChannelKt.POLL_FAILED;
                                        if (size == this.capacity) {
                                            Object o3 = this.describeTryPoll();
                                            Object obj = selectInstance.performAtomicTrySelect((AtomicDesc)o3);
                                            if (obj == null) {
                                                send = ((LockFreeLinkedListNode.RemoveFirstDesc<Send>)o3).getResult();
                                                o3 = ((TryPollDesc)o3).resumeToken;
                                                if (DebugKt.getASSERTIONS_ENABLED()) {
                                                    if (o3 != null) {
                                                        b = true;
                                                        break Label_0447;
                                                    }
                                                    break Label_0445;
                                                }
                                                else {
                                                    if (send == null) {
                                                        Intrinsics.throwNpe();
                                                    }
                                                    obj = send.getPollResult();
                                                }
                                            }
                                            else {
                                                if (obj == AbstractChannelKt.POLL_FAILED) {
                                                    break Label_0454;
                                                }
                                                if (obj == SelectKt.getALREADY_SELECTED()) {
                                                    this.size = size;
                                                    this.buffer[this.head] = o2;
                                                    return obj;
                                                }
                                                if (!(obj instanceof Closed)) {
                                                    final StringBuilder sb = new StringBuilder();
                                                    sb.append("performAtomicTrySelect(describeTryOffer) returned ");
                                                    sb.append(obj);
                                                    throw new IllegalStateException(sb.toString().toString());
                                                }
                                                send = (Closed<?>)obj;
                                                o3 = ((Closed<?>)obj).tryResumeSend(null);
                                            }
                                            if (obj != AbstractChannelKt.POLL_FAILED && !(obj instanceof Closed)) {
                                                this.size = size;
                                                this.buffer[(this.head + size) % this.buffer.length] = obj;
                                            }
                                            else if (!selectInstance.trySelect(null)) {
                                                this.size = size;
                                                this.buffer[this.head] = o2;
                                                return SelectKt.getALREADY_SELECTED();
                                            }
                                            this.head = (this.head + 1) % this.buffer.length;
                                            final Unit instance = Unit.INSTANCE;
                                            reentrantLock.unlock();
                                            if (o3 != null) {
                                                if (send == null) {
                                                    Intrinsics.throwNpe();
                                                }
                                                send.completeResumeSend(o3);
                                            }
                                            return o2;
                                        }
                                        break Label_0454;
                                        throw new AssertionError();
                                    }
                                    finally {
                                        reentrantLock.unlock();
                                    }
                                }
                                b = false;
                            }
                            if (b) {
                                continue Label_0303_Outer;
                            }
                            break;
                        }
                        continue;
                    }
                }
                Object o3 = null;
                Object obj = poll_FAILED;
                continue;
            }
        }
    }
}
