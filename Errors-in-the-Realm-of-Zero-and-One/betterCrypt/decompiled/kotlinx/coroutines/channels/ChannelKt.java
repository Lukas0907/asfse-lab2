// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.channels;

import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\u001a\u001c\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u00022\b\b\u0002\u0010\u0003\u001a\u00020\u0004¨\u0006\u0005" }, d2 = { "Channel", "Lkotlinx/coroutines/channels/Channel;", "E", "capacity", "", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class ChannelKt
{
    public static final <E> Channel<E> Channel(final int n) {
        if (n == -2) {
            return new ArrayChannel<E>(Channel.Factory.getCHANNEL_DEFAULT_CAPACITY$kotlinx_coroutines_core());
        }
        if (n == -1) {
            return new ConflatedChannel<E>();
        }
        if (n == 0) {
            return new RendezvousChannel<E>();
        }
        if (n != Integer.MAX_VALUE) {
            return new ArrayChannel<E>(n);
        }
        return new LinkedListChannel<E>();
    }
}
