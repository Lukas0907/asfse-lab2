// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.channels;

import kotlinx.coroutines.intrinsics.CancellableKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002BM\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006\u0012-\u0010\u0007\u001a)\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\t\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n\u0012\u0006\u0012\u0004\u0018\u00010\f0\b¢\u0006\u0002\b\r\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000eJ\b\u0010\u0010\u001a\u00020\u000bH\u0014J\u000e\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00000\u0012H\u0016R<\u0010\u0007\u001a+\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\t\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n\u0012\u0006\u0012\u0004\u0018\u00010\f\u0018\u00010\b¢\u0006\u0002\b\rX\u0082\u000e\u00f8\u0001\u0000¢\u0006\u0004\n\u0002\u0010\u000f\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0013" }, d2 = { "Lkotlinx/coroutines/channels/LazyBroadcastCoroutine;", "E", "Lkotlinx/coroutines/channels/BroadcastCoroutine;", "parentContext", "Lkotlin/coroutines/CoroutineContext;", "channel", "Lkotlinx/coroutines/channels/BroadcastChannel;", "block", "Lkotlin/Function2;", "Lkotlinx/coroutines/channels/ProducerScope;", "Lkotlin/coroutines/Continuation;", "", "", "Lkotlin/ExtensionFunctionType;", "(Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/channels/BroadcastChannel;Lkotlin/jvm/functions/Function2;)V", "Lkotlin/jvm/functions/Function2;", "onStart", "openSubscription", "Lkotlinx/coroutines/channels/ReceiveChannel;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
final class LazyBroadcastCoroutine<E> extends BroadcastCoroutine<E>
{
    private Function2<? super ProducerScope<? super E>, ? super Continuation<? super Unit>, ?> block;
    
    public LazyBroadcastCoroutine(final CoroutineContext coroutineContext, final BroadcastChannel<E> broadcastChannel, final Function2<? super ProducerScope<? super E>, ? super Continuation<? super Unit>, ?> block) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "parentContext");
        Intrinsics.checkParameterIsNotNull(broadcastChannel, "channel");
        Intrinsics.checkParameterIsNotNull(block, "block");
        super(coroutineContext, broadcastChannel, false);
        this.block = block;
    }
    
    @Override
    protected void onStart() {
        final Function2<? super ProducerScope<? super E>, ? super Continuation<? super Unit>, ?> block = this.block;
        if (block != null) {
            this.block = null;
            CancellableKt.startCoroutineCancellable((Function2<? super LazyBroadcastCoroutine, ? super Continuation<? super Object>, ?>)block, this, (Continuation<? super Object>)this);
            return;
        }
        throw new IllegalStateException("Already started".toString());
    }
    
    @Override
    public ReceiveChannel<E> openSubscription() {
        final ReceiveChannel<E> openSubscription = this.get_channel().openSubscription();
        this.start();
        return openSubscription;
    }
}
