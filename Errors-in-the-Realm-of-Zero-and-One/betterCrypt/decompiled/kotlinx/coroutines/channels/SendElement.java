// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.channels;

import kotlin.ResultKt;
import kotlin.Result;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlinx.coroutines.CancellableContinuation;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0000\u0018\u00002\u00020\u0001B\u001d\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\u0010\u0010\n\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u0003H\u0016J\u0014\u0010\f\u001a\u00020\u00062\n\u0010\r\u001a\u0006\u0012\u0002\b\u00030\u000eH\u0016J\b\u0010\u000f\u001a\u00020\u0010H\u0016J\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u00032\b\u0010\u0012\u001a\u0004\u0018\u00010\u0003H\u0016R\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\u0013" }, d2 = { "Lkotlinx/coroutines/channels/SendElement;", "Lkotlinx/coroutines/channels/Send;", "pollResult", "", "cont", "Lkotlinx/coroutines/CancellableContinuation;", "", "(Ljava/lang/Object;Lkotlinx/coroutines/CancellableContinuation;)V", "getPollResult", "()Ljava/lang/Object;", "completeResumeSend", "token", "resumeSendClosed", "closed", "Lkotlinx/coroutines/channels/Closed;", "toString", "", "tryResumeSend", "idempotent", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class SendElement extends Send
{
    public final CancellableContinuation<Unit> cont;
    private final Object pollResult;
    
    public SendElement(final Object pollResult, final CancellableContinuation<? super Unit> cont) {
        Intrinsics.checkParameterIsNotNull(cont, "cont");
        this.pollResult = pollResult;
        this.cont = (CancellableContinuation<Unit>)cont;
    }
    
    @Override
    public void completeResumeSend(final Object o) {
        Intrinsics.checkParameterIsNotNull(o, "token");
        this.cont.completeResume(o);
    }
    
    @Override
    public Object getPollResult() {
        return this.pollResult;
    }
    
    @Override
    public void resumeSendClosed(final Closed<?> closed) {
        Intrinsics.checkParameterIsNotNull(closed, "closed");
        final CancellableContinuation<Unit> cancellableContinuation = this.cont;
        final Throwable sendException = closed.getSendException();
        final Result.Companion companion = Result.Companion;
        cancellableContinuation.resumeWith(Result.constructor-impl(ResultKt.createFailure(sendException)));
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SendElement(");
        sb.append(this.getPollResult());
        sb.append(')');
        return sb.toString();
    }
    
    @Override
    public Object tryResumeSend(final Object o) {
        return this.cont.tryResume(Unit.INSTANCE, o);
    }
}
