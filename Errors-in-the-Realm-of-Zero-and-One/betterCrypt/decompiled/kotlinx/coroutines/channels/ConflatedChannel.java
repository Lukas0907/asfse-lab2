// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.channels;

import kotlinx.coroutines.selects.SelectKt;
import kotlinx.coroutines.internal.AtomicDesc;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.selects.SelectInstance;
import kotlinx.coroutines.internal.LockFreeLinkedListHead;
import kotlin.TypeCastException;
import kotlinx.coroutines.internal.LockFreeLinkedListNode;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000@\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0010\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B\u0005¢\u0006\u0002\u0010\u0003J\u0016\u0010\n\u001a\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00028\u00000\rH\u0002J\u0015\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00028\u0000H\u0014¢\u0006\u0002\u0010\u0011J!\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00028\u00002\n\u0010\u0013\u001a\u0006\u0012\u0002\b\u00030\u0014H\u0014¢\u0006\u0002\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u000b2\u0006\u0010\u0017\u001a\u00020\u0018H\u0014J\u001b\u0010\u0019\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u001a2\u0006\u0010\u0010\u001a\u00028\u0000H\u0002¢\u0006\u0002\u0010\u001bR\u0014\u0010\u0004\u001a\u00020\u00058DX\u0084\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u00058DX\u0084\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\u0006R\u0014\u0010\b\u001a\u00020\u00058DX\u0084\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\u0006R\u0014\u0010\t\u001a\u00020\u00058DX\u0084\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\u0006¨\u0006\u001c" }, d2 = { "Lkotlinx/coroutines/channels/ConflatedChannel;", "E", "Lkotlinx/coroutines/channels/AbstractChannel;", "()V", "isBufferAlwaysEmpty", "", "()Z", "isBufferAlwaysFull", "isBufferEmpty", "isBufferFull", "conflatePreviousSendBuffered", "", "node", "Lkotlinx/coroutines/channels/AbstractSendChannel$SendBuffered;", "offerInternal", "", "element", "(Ljava/lang/Object;)Ljava/lang/Object;", "offerSelectInternal", "select", "Lkotlinx/coroutines/selects/SelectInstance;", "(Ljava/lang/Object;Lkotlinx/coroutines/selects/SelectInstance;)Ljava/lang/Object;", "onClosedIdempotent", "closed", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "sendConflated", "Lkotlinx/coroutines/channels/ReceiveOrClosed;", "(Ljava/lang/Object;)Lkotlinx/coroutines/channels/ReceiveOrClosed;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public class ConflatedChannel<E> extends AbstractChannel<E>
{
    private final void conflatePreviousSendBuffered(final SendBuffered<? extends E> sendBuffered) {
        for (LockFreeLinkedListNode lockFreeLinkedListNode = sendBuffered.getPrevNode(); lockFreeLinkedListNode instanceof SendBuffered; lockFreeLinkedListNode = lockFreeLinkedListNode.getPrevNode()) {
            if (!lockFreeLinkedListNode.remove()) {
                lockFreeLinkedListNode.helpRemove();
            }
        }
    }
    
    private final ReceiveOrClosed<?> sendConflated(final E e) {
        final SendBuffered sendBuffered = new SendBuffered<Object>(e);
        final LockFreeLinkedListHead queue = this.getQueue();
        LockFreeLinkedListNode lockFreeLinkedListNode;
        do {
            final Object prev = queue.getPrev();
            if (prev == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
            lockFreeLinkedListNode = (LockFreeLinkedListNode)prev;
            if (lockFreeLinkedListNode instanceof ReceiveOrClosed) {
                return (ReceiveOrClosed<?>)lockFreeLinkedListNode;
            }
        } while (!lockFreeLinkedListNode.addNext(sendBuffered, queue));
        this.conflatePreviousSendBuffered((SendBuffered<? extends E>)sendBuffered);
        return null;
    }
    
    @Override
    protected final boolean isBufferAlwaysEmpty() {
        return true;
    }
    
    @Override
    protected final boolean isBufferAlwaysFull() {
        return false;
    }
    
    @Override
    protected final boolean isBufferEmpty() {
        return true;
    }
    
    @Override
    protected final boolean isBufferFull() {
        return false;
    }
    
    @Override
    protected Object offerInternal(final E e) {
        ReceiveOrClosed<?> sendConflated;
        do {
            final Object offerInternal = super.offerInternal(e);
            if (offerInternal == AbstractChannelKt.OFFER_SUCCESS) {
                return AbstractChannelKt.OFFER_SUCCESS;
            }
            if (offerInternal == AbstractChannelKt.OFFER_FAILED) {
                sendConflated = this.sendConflated(e);
                if (sendConflated == null) {
                    return AbstractChannelKt.OFFER_SUCCESS;
                }
                continue;
            }
            else {
                if (offerInternal instanceof Closed) {
                    return offerInternal;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid offerInternal result ");
                sb.append(offerInternal);
                throw new IllegalStateException(sb.toString().toString());
            }
        } while (!(sendConflated instanceof Closed));
        return sendConflated;
    }
    
    @Override
    protected Object offerSelectInternal(final E e, final SelectInstance<?> selectInstance) {
        Intrinsics.checkParameterIsNotNull(selectInstance, "select");
        while (true) {
            Object obj;
            if (this.getHasReceiveOrClosed()) {
                obj = super.offerSelectInternal(e, selectInstance);
            }
            else {
                obj = selectInstance.performAtomicTrySelect(this.describeSendConflated(e));
                if (obj == null) {
                    obj = AbstractChannelKt.OFFER_SUCCESS;
                }
            }
            if (obj == SelectKt.getALREADY_SELECTED()) {
                return SelectKt.getALREADY_SELECTED();
            }
            if (obj == AbstractChannelKt.OFFER_SUCCESS) {
                return AbstractChannelKt.OFFER_SUCCESS;
            }
            if (obj == AbstractChannelKt.OFFER_FAILED) {
                continue;
            }
            if (obj instanceof Closed) {
                return obj;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid result ");
            sb.append(obj);
            throw new IllegalStateException(sb.toString().toString());
        }
    }
    
    @Override
    protected void onClosedIdempotent(LockFreeLinkedListNode prevNode) {
        Intrinsics.checkParameterIsNotNull(prevNode, "closed");
        if (!((prevNode = prevNode.getPrevNode()) instanceof SendBuffered)) {
            prevNode = null;
        }
        final SendBuffered sendBuffered = (SendBuffered)prevNode;
        if (sendBuffered != null) {
            this.conflatePreviousSendBuffered(sendBuffered);
        }
    }
}
