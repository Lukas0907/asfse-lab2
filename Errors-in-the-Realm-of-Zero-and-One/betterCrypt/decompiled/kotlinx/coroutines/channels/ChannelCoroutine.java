// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.channels;

import kotlin.TypeCastException;
import kotlin.ReplaceWith;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.selects.SelectClause2;
import kotlinx.coroutines.selects.SelectClause1;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.JobCancellationException;
import kotlinx.coroutines.DebugStringsKt;
import kotlinx.coroutines.JobSupport;
import kotlin.DeprecationLevel;
import kotlin.Deprecated;
import java.util.concurrent.CancellationException;
import kotlin.coroutines.Continuation;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;
import kotlin.Unit;
import kotlinx.coroutines.AbstractCoroutine;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000d\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\b\u0010\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u00020\u00030\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0004B#\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\b\u0010\"\u001a\u00020\u0003H\u0016J\u0012\u0010\"\u001a\u00020\t2\b\u0010#\u001a\u0004\u0018\u00010$H\u0007J\u0016\u0010\"\u001a\u00020\u00032\u000e\u0010#\u001a\n\u0018\u00010%j\u0004\u0018\u0001`&J\u0012\u0010'\u001a\u00020\t2\b\u0010#\u001a\u0004\u0018\u00010$H\u0016J\u0013\u0010(\u001a\u00020\t2\b\u0010#\u001a\u0004\u0018\u00010$H\u0096\u0001J.\u0010)\u001a\u00020\u00032#\u0010*\u001a\u001f\u0012\u0015\u0012\u0013\u0018\u00010$¢\u0006\f\b,\u0012\b\b-\u0012\u0004\b\b(#\u0012\u0004\u0012\u00020\u00030+H\u0097\u0001J\u000f\u0010.\u001a\b\u0012\u0004\u0012\u00028\u00000/H\u0096\u0003J\u0016\u00100\u001a\u00020\t2\u0006\u00101\u001a\u00028\u0000H\u0096\u0001¢\u0006\u0002\u00102J\u0010\u00103\u001a\u0004\u0018\u00018\u0000H\u0096\u0001¢\u0006\u0002\u00104J\u0011\u00105\u001a\u00028\u0000H\u0096A\u00f8\u0001\u0000¢\u0006\u0002\u00106J\u001a\u00107\u001a\b\u0012\u0004\u0012\u00028\u00000\u0019H\u0097A\u00f8\u0001\u0000\u00f8\u0001\u0000¢\u0006\u0002\u00106J\u0013\u00108\u001a\u0004\u0018\u00018\u0000H\u0097A\u00f8\u0001\u0000¢\u0006\u0002\u00106J\u0019\u00109\u001a\u00020\u00032\u0006\u00101\u001a\u00028\u0000H\u0096A\u00f8\u0001\u0000¢\u0006\u0002\u0010:J\u0019\u0010;\u001a\u00020\u00032\u0006\u00101\u001a\u00028\u0000H\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u0010:R\u001a\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004X\u0084\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00000\u00048F¢\u0006\u0006\u001a\u0004\b\u000e\u0010\fR\u0014\u0010\u000f\u001a\u00020\t8\u0016X\u0097\u0005¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\t8\u0016X\u0097\u0005¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0010R\u0014\u0010\u0012\u001a\u00020\t8\u0016X\u0097\u0005¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0010R\u0014\u0010\u0013\u001a\u00020\t8\u0016X\u0097\u0005¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0010R\u0018\u0010\u0014\u001a\b\u0012\u0004\u0012\u00028\u00000\u0015X\u0096\u0005¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0017R#\u0010\u0018\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u00190\u00158\u0016X\u0097\u0005\u00f8\u0001\u0000¢\u0006\u0006\u001a\u0004\b\u001a\u0010\u0017R\u001c\u0010\u001b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00018\u00000\u00158\u0016X\u0097\u0005¢\u0006\u0006\u001a\u0004\b\u001c\u0010\u0017R$\u0010\u001d\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u001f0\u001eX\u0096\u0005¢\u0006\u0006\u001a\u0004\b \u0010!\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006<" }, d2 = { "Lkotlinx/coroutines/channels/ChannelCoroutine;", "E", "Lkotlinx/coroutines/AbstractCoroutine;", "", "Lkotlinx/coroutines/channels/Channel;", "parentContext", "Lkotlin/coroutines/CoroutineContext;", "_channel", "active", "", "(Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/channels/Channel;Z)V", "get_channel", "()Lkotlinx/coroutines/channels/Channel;", "channel", "getChannel", "isClosedForReceive", "()Z", "isClosedForSend", "isEmpty", "isFull", "onReceive", "Lkotlinx/coroutines/selects/SelectClause1;", "getOnReceive", "()Lkotlinx/coroutines/selects/SelectClause1;", "onReceiveOrClosed", "Lkotlinx/coroutines/channels/ValueOrClosed;", "getOnReceiveOrClosed", "onReceiveOrNull", "getOnReceiveOrNull", "onSend", "Lkotlinx/coroutines/selects/SelectClause2;", "Lkotlinx/coroutines/channels/SendChannel;", "getOnSend", "()Lkotlinx/coroutines/selects/SelectClause2;", "cancel", "cause", "", "Ljava/util/concurrent/CancellationException;", "Lkotlinx/coroutines/CancellationException;", "cancelInternal", "close", "invokeOnClose", "handler", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "iterator", "Lkotlinx/coroutines/channels/ChannelIterator;", "offer", "element", "(Ljava/lang/Object;)Z", "poll", "()Ljava/lang/Object;", "receive", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "receiveOrClosed", "receiveOrNull", "send", "(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "sendFair", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public class ChannelCoroutine<E> extends AbstractCoroutine<Unit> implements Channel<E>
{
    private final Channel<E> _channel;
    
    public ChannelCoroutine(final CoroutineContext coroutineContext, final Channel<E> channel, final boolean b) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "parentContext");
        Intrinsics.checkParameterIsNotNull(channel, "_channel");
        super(coroutineContext, b);
        this._channel = channel;
    }
    
    static /* synthetic */ Object receive$suspendImpl(final ChannelCoroutine channelCoroutine, final Continuation continuation) {
        return channelCoroutine._channel.receive(continuation);
    }
    
    static /* synthetic */ Object receiveOrClosed$suspendImpl(final ChannelCoroutine channelCoroutine, final Continuation continuation) {
        return channelCoroutine._channel.receiveOrClosed(continuation);
    }
    
    static /* synthetic */ Object receiveOrNull$suspendImpl(final ChannelCoroutine channelCoroutine, final Continuation continuation) {
        return channelCoroutine._channel.receiveOrNull(continuation);
    }
    
    static /* synthetic */ Object send$suspendImpl(final ChannelCoroutine channelCoroutine, final Object o, final Continuation continuation) {
        return channelCoroutine._channel.send((E)o, continuation);
    }
    
    @Override
    public final void cancel(final CancellationException ex) {
        this.cancelInternal(ex);
    }
    
    @Override
    public boolean cancelInternal(final Throwable t) {
        CancellationException cancellationException$default = null;
        Label_0064: {
            if (t != null) {
                cancellationException$default = JobSupport.toCancellationException$default(this, t, null, 1, null);
                if (cancellationException$default != null) {
                    break Label_0064;
                }
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(DebugStringsKt.getClassSimpleName(this));
            sb.append(" was cancelled");
            cancellationException$default = new JobCancellationException(sb.toString(), null, this);
        }
        this._channel.cancel(cancellationException$default);
        this.cancelCoroutine(cancellationException$default);
        return true;
    }
    
    @Override
    public boolean close(final Throwable t) {
        return this._channel.close(t);
    }
    
    public final Channel<E> getChannel() {
        return this;
    }
    
    @Override
    public SelectClause1<E> getOnReceive() {
        return this._channel.getOnReceive();
    }
    
    @Override
    public SelectClause1<ValueOrClosed<E>> getOnReceiveOrClosed() {
        return this._channel.getOnReceiveOrClosed();
    }
    
    @Override
    public SelectClause1<E> getOnReceiveOrNull() {
        return this._channel.getOnReceiveOrNull();
    }
    
    @Override
    public SelectClause2<E, SendChannel<E>> getOnSend() {
        return this._channel.getOnSend();
    }
    
    protected final Channel<E> get_channel() {
        return this._channel;
    }
    
    @Override
    public void invokeOnClose(final Function1<? super Throwable, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(function1, "handler");
        this._channel.invokeOnClose(function1);
    }
    
    @Override
    public boolean isClosedForReceive() {
        return this._channel.isClosedForReceive();
    }
    
    @Override
    public boolean isClosedForSend() {
        return this._channel.isClosedForSend();
    }
    
    @Override
    public boolean isEmpty() {
        return this._channel.isEmpty();
    }
    
    @Override
    public boolean isFull() {
        return this._channel.isFull();
    }
    
    @Override
    public ChannelIterator<E> iterator() {
        return this._channel.iterator();
    }
    
    @Override
    public boolean offer(final E e) {
        return this._channel.offer(e);
    }
    
    @Override
    public E poll() {
        return this._channel.poll();
    }
    
    @Override
    public Object receive(final Continuation<? super E> continuation) {
        return receive$suspendImpl(this, continuation);
    }
    
    @Override
    public Object receiveOrClosed(final Continuation<? super ValueOrClosed<? extends E>> continuation) {
        return receiveOrClosed$suspendImpl(this, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Deprecated in favor of receiveOrClosed and receiveOrNull extension", replaceWith = @ReplaceWith(expression = "receiveOrNull", imports = { "kotlinx.coroutines.channels.receiveOrNull" }))
    @Override
    public Object receiveOrNull(final Continuation<? super E> continuation) {
        return receiveOrNull$suspendImpl(this, continuation);
    }
    
    @Override
    public Object send(final E e, final Continuation<? super Unit> continuation) {
        return send$suspendImpl(this, e, continuation);
    }
    
    public final Object sendFair(final E e, final Continuation<? super Unit> continuation) {
        final Channel<E> channel = this._channel;
        if (channel != null) {
            return ((AbstractSendChannel<E>)channel).sendFair$kotlinx_coroutines_core(e, continuation);
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.channels.AbstractSendChannel<E>");
    }
}
