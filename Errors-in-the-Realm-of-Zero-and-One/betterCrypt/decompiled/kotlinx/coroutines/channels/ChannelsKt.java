// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.channels;

import java.util.Set;
import kotlinx.coroutines.selects.SelectClause1;
import java.util.Comparator;
import java.util.List;
import java.util.Collection;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.functions.Function2;
import kotlin.coroutines.CoroutineContext;
import kotlin.collections.IndexedValue;
import kotlin.Unit;
import java.util.Map;
import kotlin.Pair;
import kotlin.DeprecationLevel;
import kotlin.Deprecated;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function1;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "kotlinx/coroutines/channels/ChannelsKt__ChannelsKt", "kotlinx/coroutines/channels/ChannelsKt__Channels_commonKt" }, k = 4, mv = { 1, 1, 15 })
public final class ChannelsKt
{
    public static final String DEFAULT_CLOSE_MESSAGE = "Channel was closed";
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object all(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, Boolean> function1, final Continuation<? super Boolean> continuation) {
        return ChannelsKt__Channels_commonKt.all((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object all$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.all((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Boolean>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object any(final ReceiveChannel<? extends E> receiveChannel, final Continuation<? super Boolean> continuation) {
        return ChannelsKt__Channels_commonKt.any((ReceiveChannel<?>)receiveChannel, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object any(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, Boolean> function1, final Continuation<? super Boolean> continuation) {
        return ChannelsKt__Channels_commonKt.any((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object any$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.any((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Boolean>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, K, V> Object associate(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, ? extends Pair<? extends K, ? extends V>> function1, final Continuation<? super Map<K, ? extends V>> continuation) {
        return ChannelsKt__Channels_commonKt.associate((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, ? extends Pair<?, ?>>)function1, (Continuation<? super Map<Object, ?>>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object associate$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.associate((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, ? extends Pair<?, ?>>)function1, (Continuation<? super Map<Object, ?>>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, K> Object associateBy(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, ? extends K> function1, final Continuation<? super Map<K, ? extends E>> continuation) {
        return ChannelsKt__Channels_commonKt.associateBy((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, ?>)function1, (Continuation<? super Map<Object, ?>>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, K, V> Object associateBy(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, ? extends K> function1, final Function1<? super E, ? extends V> function2, final Continuation<? super Map<K, ? extends V>> continuation) {
        return ChannelsKt__Channels_commonKt.associateBy((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, ?>)function1, (Function1<? super Object, ?>)function2, (Continuation<? super Map<Object, ?>>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object associateBy$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.associateBy((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, ?>)function1, (Continuation<? super Map<Object, ?>>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object associateBy$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Function1 function2, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.associateBy((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, ?>)function1, (Function1<? super Object, ?>)function2, (Continuation<? super Map<Object, ?>>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, K, M extends Map<? super K, ? super E>> Object associateByTo(final ReceiveChannel<? extends E> receiveChannel, final M m, final Function1<? super E, ? extends K> function1, final Continuation<? super M> continuation) {
        return ChannelsKt__Channels_commonKt.associateByTo((ReceiveChannel<?>)receiveChannel, m, (Function1<? super Object, ?>)function1, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, K, V, M extends Map<? super K, ? super V>> Object associateByTo(final ReceiveChannel<? extends E> receiveChannel, final M m, final Function1<? super E, ? extends K> function1, final Function1<? super E, ? extends V> function2, final Continuation<? super M> continuation) {
        return ChannelsKt__Channels_commonKt.associateByTo((ReceiveChannel<?>)receiveChannel, m, (Function1<? super Object, ?>)function1, (Function1<? super Object, ?>)function2, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object associateByTo$$forInline(final ReceiveChannel receiveChannel, final Map map, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.associateByTo((ReceiveChannel<?>)receiveChannel, map, (Function1<? super Object, ?>)function1, (Continuation<? super Map>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object associateByTo$$forInline(final ReceiveChannel receiveChannel, final Map map, final Function1 function1, final Function1 function2, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.associateByTo((ReceiveChannel<?>)receiveChannel, map, (Function1<? super Object, ?>)function1, (Function1<? super Object, ?>)function2, (Continuation<? super Map>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, K, V, M extends Map<? super K, ? super V>> Object associateTo(final ReceiveChannel<? extends E> receiveChannel, final M m, final Function1<? super E, ? extends Pair<? extends K, ? extends V>> function1, final Continuation<? super M> continuation) {
        return ChannelsKt__Channels_commonKt.associateTo((ReceiveChannel<?>)receiveChannel, m, (Function1<? super Object, ? extends Pair<?, ?>>)function1, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object associateTo$$forInline(final ReceiveChannel receiveChannel, final Map map, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.associateTo((ReceiveChannel<?>)receiveChannel, map, (Function1<? super Object, ? extends Pair<?, ?>>)function1, (Continuation<? super Map>)continuation);
    }
    
    public static final void cancelConsumed(final ReceiveChannel<?> receiveChannel, final Throwable t) {
        ChannelsKt__Channels_commonKt.cancelConsumed(receiveChannel, t);
    }
    
    public static final <E, R> R consume(final BroadcastChannel<E> broadcastChannel, final Function1<? super ReceiveChannel<? extends E>, ? extends R> function1) {
        return ChannelsKt__Channels_commonKt.consume(broadcastChannel, function1);
    }
    
    public static final <E, R> R consume(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super ReceiveChannel<? extends E>, ? extends R> function1) {
        return ChannelsKt__Channels_commonKt.consume((ReceiveChannel<?>)receiveChannel, (Function1<? super ReceiveChannel<?>, ? extends R>)function1);
    }
    
    public static final <E> Object consumeEach(final BroadcastChannel<E> broadcastChannel, final Function1<? super E, Unit> function1, final Continuation<? super Unit> continuation) {
        return ChannelsKt__Channels_commonKt.consumeEach(broadcastChannel, function1, continuation);
    }
    
    public static final <E> Object consumeEach(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, Unit> function1, final Continuation<? super Unit> continuation) {
        return ChannelsKt__Channels_commonKt.consumeEach((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Unit>)function1, continuation);
    }
    
    private static final Object consumeEach$$forInline(final BroadcastChannel broadcastChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.consumeEach((BroadcastChannel<Object>)broadcastChannel, function1, continuation);
    }
    
    private static final Object consumeEach$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.consumeEach((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Unit>)function1, (Continuation<? super Unit>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object consumeEachIndexed(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super IndexedValue<? extends E>, Unit> function1, final Continuation<? super Unit> continuation) {
        return ChannelsKt__Channels_commonKt.consumeEachIndexed((ReceiveChannel<?>)receiveChannel, (Function1<? super IndexedValue<?>, Unit>)function1, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object consumeEachIndexed$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.consumeEachIndexed((ReceiveChannel<?>)receiveChannel, (Function1<? super IndexedValue<?>, Unit>)function1, (Continuation<? super Unit>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final Function1<Throwable, Unit> consumes(final ReceiveChannel<?> receiveChannel) {
        return ChannelsKt__Channels_commonKt.consumes(receiveChannel);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final Function1<Throwable, Unit> consumesAll(final ReceiveChannel<?>... array) {
        return ChannelsKt__Channels_commonKt.consumesAll(array);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object count(final ReceiveChannel<? extends E> receiveChannel, final Continuation<? super Integer> continuation) {
        return ChannelsKt__Channels_commonKt.count((ReceiveChannel<?>)receiveChannel, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object count(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, Boolean> function1, final Continuation<? super Integer> continuation) {
        return ChannelsKt__Channels_commonKt.count((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object count$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.count((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Integer>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> ReceiveChannel<E> distinct(final ReceiveChannel<? extends E> receiveChannel) {
        return ChannelsKt__Channels_commonKt.distinct(receiveChannel);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, K> ReceiveChannel<E> distinctBy(final ReceiveChannel<? extends E> receiveChannel, final CoroutineContext coroutineContext, final Function2<? super E, ? super Continuation<? super K>, ?> function2) {
        return ChannelsKt__Channels_commonKt.distinctBy(receiveChannel, coroutineContext, (Function2<? super E, ? super Continuation<? super Object>, ?>)function2);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> ReceiveChannel<E> drop(final ReceiveChannel<? extends E> receiveChannel, final int n, final CoroutineContext coroutineContext) {
        return ChannelsKt__Channels_commonKt.drop(receiveChannel, n, coroutineContext);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> ReceiveChannel<E> dropWhile(final ReceiveChannel<? extends E> receiveChannel, final CoroutineContext coroutineContext, final Function2<? super E, ? super Continuation<? super Boolean>, ?> function2) {
        return ChannelsKt__Channels_commonKt.dropWhile(receiveChannel, coroutineContext, function2);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object elementAt(final ReceiveChannel<? extends E> receiveChannel, final int n, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.elementAt((ReceiveChannel<?>)receiveChannel, n, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object elementAtOrElse(final ReceiveChannel<? extends E> receiveChannel, final int n, final Function1<? super Integer, ? extends E> function1, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.elementAtOrElse((ReceiveChannel<?>)receiveChannel, n, (Function1<? super Integer, ?>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object elementAtOrElse$$forInline(final ReceiveChannel receiveChannel, final int n, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.elementAtOrElse((ReceiveChannel<?>)receiveChannel, n, (Function1<? super Integer, ?>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object elementAtOrNull(final ReceiveChannel<? extends E> receiveChannel, final int n, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.elementAtOrNull((ReceiveChannel<?>)receiveChannel, n, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> ReceiveChannel<E> filter(final ReceiveChannel<? extends E> receiveChannel, final CoroutineContext coroutineContext, final Function2<? super E, ? super Continuation<? super Boolean>, ?> function2) {
        return ChannelsKt__Channels_commonKt.filter(receiveChannel, coroutineContext, function2);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> ReceiveChannel<E> filterIndexed(final ReceiveChannel<? extends E> receiveChannel, final CoroutineContext coroutineContext, final Function3<? super Integer, ? super E, ? super Continuation<? super Boolean>, ?> function3) {
        return ChannelsKt__Channels_commonKt.filterIndexed(receiveChannel, coroutineContext, function3);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, C extends Collection<? super E>> Object filterIndexedTo(final ReceiveChannel<? extends E> receiveChannel, final C c, final Function2<? super Integer, ? super E, Boolean> function2, final Continuation<? super C> continuation) {
        return ChannelsKt__Channels_commonKt.filterIndexedTo((ReceiveChannel<?>)receiveChannel, c, (Function2<? super Integer, ? super Object, Boolean>)function2, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, C extends SendChannel<? super E>> Object filterIndexedTo(final ReceiveChannel<? extends E> receiveChannel, final C c, final Function2<? super Integer, ? super E, Boolean> function2, final Continuation<? super C> continuation) {
        return ChannelsKt__Channels_commonKt.filterIndexedTo((ReceiveChannel<?>)receiveChannel, c, (Function2<? super Integer, ? super Object, Boolean>)function2, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object filterIndexedTo$$forInline(final ReceiveChannel receiveChannel, final Collection collection, final Function2 function2, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.filterIndexedTo((ReceiveChannel<?>)receiveChannel, collection, (Function2<? super Integer, ? super Object, Boolean>)function2, (Continuation<? super Collection>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object filterIndexedTo$$forInline(final ReceiveChannel receiveChannel, final SendChannel sendChannel, final Function2 function2, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.filterIndexedTo((ReceiveChannel<?>)receiveChannel, sendChannel, (Function2<? super Integer, ? super Object, Boolean>)function2, (Continuation<? super SendChannel>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> ReceiveChannel<E> filterNot(final ReceiveChannel<? extends E> receiveChannel, final CoroutineContext coroutineContext, final Function2<? super E, ? super Continuation<? super Boolean>, ?> function2) {
        return ChannelsKt__Channels_commonKt.filterNot(receiveChannel, coroutineContext, function2);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> ReceiveChannel<E> filterNotNull(final ReceiveChannel<? extends E> receiveChannel) {
        return ChannelsKt__Channels_commonKt.filterNotNull(receiveChannel);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, C extends Collection<? super E>> Object filterNotNullTo(final ReceiveChannel<? extends E> receiveChannel, final C c, final Continuation<? super C> continuation) {
        return ChannelsKt__Channels_commonKt.filterNotNullTo((ReceiveChannel<?>)receiveChannel, c, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, C extends SendChannel<? super E>> Object filterNotNullTo(final ReceiveChannel<? extends E> receiveChannel, final C c, final Continuation<? super C> continuation) {
        return ChannelsKt__Channels_commonKt.filterNotNullTo((ReceiveChannel<?>)receiveChannel, c, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, C extends Collection<? super E>> Object filterNotTo(final ReceiveChannel<? extends E> receiveChannel, final C c, final Function1<? super E, Boolean> function1, final Continuation<? super C> continuation) {
        return ChannelsKt__Channels_commonKt.filterNotTo((ReceiveChannel<?>)receiveChannel, c, (Function1<? super Object, Boolean>)function1, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, C extends SendChannel<? super E>> Object filterNotTo(final ReceiveChannel<? extends E> receiveChannel, final C c, final Function1<? super E, Boolean> function1, final Continuation<? super C> continuation) {
        return ChannelsKt__Channels_commonKt.filterNotTo((ReceiveChannel<?>)receiveChannel, c, (Function1<? super Object, Boolean>)function1, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object filterNotTo$$forInline(final ReceiveChannel receiveChannel, final Collection collection, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.filterNotTo((ReceiveChannel<?>)receiveChannel, collection, (Function1<? super Object, Boolean>)function1, (Continuation<? super Collection>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object filterNotTo$$forInline(final ReceiveChannel receiveChannel, final SendChannel sendChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.filterNotTo((ReceiveChannel<?>)receiveChannel, sendChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super SendChannel>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, C extends Collection<? super E>> Object filterTo(final ReceiveChannel<? extends E> receiveChannel, final C c, final Function1<? super E, Boolean> function1, final Continuation<? super C> continuation) {
        return ChannelsKt__Channels_commonKt.filterTo((ReceiveChannel<?>)receiveChannel, c, (Function1<? super Object, Boolean>)function1, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, C extends SendChannel<? super E>> Object filterTo(final ReceiveChannel<? extends E> receiveChannel, final C c, final Function1<? super E, Boolean> function1, final Continuation<? super C> continuation) {
        return ChannelsKt__Channels_commonKt.filterTo((ReceiveChannel<?>)receiveChannel, c, (Function1<? super Object, Boolean>)function1, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object filterTo$$forInline(final ReceiveChannel receiveChannel, final Collection collection, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.filterTo((ReceiveChannel<?>)receiveChannel, collection, (Function1<? super Object, Boolean>)function1, (Continuation<? super Collection>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object filterTo$$forInline(final ReceiveChannel receiveChannel, final SendChannel sendChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.filterTo((ReceiveChannel<?>)receiveChannel, sendChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super SendChannel>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object find(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, Boolean> function1, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.find((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object find$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.find((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object findLast(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, Boolean> function1, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.findLast((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object findLast$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.findLast((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object first(final ReceiveChannel<? extends E> receiveChannel, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.first((ReceiveChannel<?>)receiveChannel, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object first(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, Boolean> function1, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.first((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object first$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.first((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object firstOrNull(final ReceiveChannel<? extends E> receiveChannel, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.firstOrNull((ReceiveChannel<?>)receiveChannel, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object firstOrNull(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, Boolean> function1, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.firstOrNull((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object firstOrNull$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.firstOrNull((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, R> ReceiveChannel<R> flatMap(final ReceiveChannel<? extends E> receiveChannel, final CoroutineContext coroutineContext, final Function2<? super E, ? super Continuation<? super ReceiveChannel<? extends R>>, ?> function2) {
        return ChannelsKt__Channels_commonKt.flatMap((ReceiveChannel<?>)receiveChannel, coroutineContext, (Function2<? super Object, ? super Continuation<? super ReceiveChannel<? extends R>>, ?>)function2);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, R> Object fold(final ReceiveChannel<? extends E> receiveChannel, final R r, final Function2<? super R, ? super E, ? extends R> function2, final Continuation<? super R> continuation) {
        return ChannelsKt__Channels_commonKt.fold((ReceiveChannel<?>)receiveChannel, r, (Function2<? super R, ? super Object, ? extends R>)function2, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object fold$$forInline(final ReceiveChannel receiveChannel, final Object o, final Function2 function2, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.fold((ReceiveChannel<?>)receiveChannel, o, (Function2<? super Object, ? super Object, ?>)function2, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, R> Object foldIndexed(final ReceiveChannel<? extends E> receiveChannel, final R r, final Function3<? super Integer, ? super R, ? super E, ? extends R> function3, final Continuation<? super R> continuation) {
        return ChannelsKt__Channels_commonKt.foldIndexed((ReceiveChannel<?>)receiveChannel, r, (Function3<? super Integer, ? super R, ? super Object, ? extends R>)function3, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object foldIndexed$$forInline(final ReceiveChannel receiveChannel, final Object o, final Function3 function3, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.foldIndexed((ReceiveChannel<?>)receiveChannel, o, (Function3<? super Integer, ? super Object, ? super Object, ?>)function3, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, K> Object groupBy(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, ? extends K> function1, final Continuation<? super Map<K, ? extends List<? extends E>>> continuation) {
        return ChannelsKt__Channels_commonKt.groupBy((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, ?>)function1, (Continuation<? super Map<Object, ? extends List<?>>>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, K, V> Object groupBy(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, ? extends K> function1, final Function1<? super E, ? extends V> function2, final Continuation<? super Map<K, ? extends List<? extends V>>> continuation) {
        return ChannelsKt__Channels_commonKt.groupBy((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, ?>)function1, (Function1<? super Object, ?>)function2, (Continuation<? super Map<Object, ? extends List<?>>>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object groupBy$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.groupBy((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, ?>)function1, (Continuation<? super Map<Object, ? extends List<?>>>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object groupBy$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Function1 function2, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.groupBy((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, ?>)function1, (Function1<? super Object, ?>)function2, (Continuation<? super Map<Object, ? extends List<?>>>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, K, M extends Map<? super K, List<E>>> Object groupByTo(final ReceiveChannel<? extends E> receiveChannel, final M m, final Function1<? super E, ? extends K> function1, final Continuation<? super M> continuation) {
        return ChannelsKt__Channels_commonKt.groupByTo((ReceiveChannel<?>)receiveChannel, m, (Function1<? super Object, ?>)function1, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, K, V, M extends Map<? super K, List<V>>> Object groupByTo(final ReceiveChannel<? extends E> receiveChannel, final M m, final Function1<? super E, ? extends K> function1, final Function1<? super E, ? extends V> function2, final Continuation<? super M> continuation) {
        return ChannelsKt__Channels_commonKt.groupByTo((ReceiveChannel<?>)receiveChannel, m, (Function1<? super Object, ?>)function1, (Function1<? super Object, ?>)function2, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object groupByTo$$forInline(final ReceiveChannel receiveChannel, final Map map, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.groupByTo((ReceiveChannel<?>)receiveChannel, map, (Function1<? super Object, ?>)function1, (Continuation<? super Map>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object groupByTo$$forInline(final ReceiveChannel receiveChannel, final Map map, final Function1 function1, final Function1 function2, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.groupByTo((ReceiveChannel<?>)receiveChannel, map, (Function1<? super Object, ?>)function1, (Function1<? super Object, ?>)function2, (Continuation<? super Map>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object indexOf(final ReceiveChannel<? extends E> receiveChannel, final E e, final Continuation<? super Integer> continuation) {
        return ChannelsKt__Channels_commonKt.indexOf(receiveChannel, e, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object indexOfFirst(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, Boolean> function1, final Continuation<? super Integer> continuation) {
        return ChannelsKt__Channels_commonKt.indexOfFirst((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object indexOfFirst$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.indexOfFirst((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Integer>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object indexOfLast(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, Boolean> function1, final Continuation<? super Integer> continuation) {
        return ChannelsKt__Channels_commonKt.indexOfLast((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object indexOfLast$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.indexOfLast((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Integer>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object last(final ReceiveChannel<? extends E> receiveChannel, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.last((ReceiveChannel<?>)receiveChannel, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object last(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, Boolean> function1, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.last((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object last$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.last((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object lastIndexOf(final ReceiveChannel<? extends E> receiveChannel, final E e, final Continuation<? super Integer> continuation) {
        return ChannelsKt__Channels_commonKt.lastIndexOf(receiveChannel, e, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object lastOrNull(final ReceiveChannel<? extends E> receiveChannel, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.lastOrNull((ReceiveChannel<?>)receiveChannel, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object lastOrNull(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, Boolean> function1, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.lastOrNull((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object lastOrNull$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.lastOrNull((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, R> ReceiveChannel<R> map(final ReceiveChannel<? extends E> receiveChannel, final CoroutineContext coroutineContext, final Function2<? super E, ? super Continuation<? super R>, ?> function2) {
        return ChannelsKt__Channels_commonKt.map((ReceiveChannel<?>)receiveChannel, coroutineContext, (Function2<? super Object, ? super Continuation<? super R>, ?>)function2);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, R> ReceiveChannel<R> mapIndexed(final ReceiveChannel<? extends E> receiveChannel, final CoroutineContext coroutineContext, final Function3<? super Integer, ? super E, ? super Continuation<? super R>, ?> function3) {
        return ChannelsKt__Channels_commonKt.mapIndexed((ReceiveChannel<?>)receiveChannel, coroutineContext, (Function3<? super Integer, ? super Object, ? super Continuation<? super R>, ?>)function3);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, R> ReceiveChannel<R> mapIndexedNotNull(final ReceiveChannel<? extends E> receiveChannel, final CoroutineContext coroutineContext, final Function3<? super Integer, ? super E, ? super Continuation<? super R>, ?> function3) {
        return ChannelsKt__Channels_commonKt.mapIndexedNotNull((ReceiveChannel<?>)receiveChannel, coroutineContext, (Function3<? super Integer, ? super Object, ? super Continuation<? super R>, ?>)function3);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, R, C extends Collection<? super R>> Object mapIndexedNotNullTo(final ReceiveChannel<? extends E> receiveChannel, final C c, final Function2<? super Integer, ? super E, ? extends R> function2, final Continuation<? super C> continuation) {
        return ChannelsKt__Channels_commonKt.mapIndexedNotNullTo((ReceiveChannel<?>)receiveChannel, c, (Function2<? super Integer, ? super Object, ?>)function2, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, R, C extends SendChannel<? super R>> Object mapIndexedNotNullTo(final ReceiveChannel<? extends E> receiveChannel, final C c, final Function2<? super Integer, ? super E, ? extends R> function2, final Continuation<? super C> continuation) {
        return ChannelsKt__Channels_commonKt.mapIndexedNotNullTo((ReceiveChannel<?>)receiveChannel, c, (Function2<? super Integer, ? super Object, ?>)function2, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object mapIndexedNotNullTo$$forInline(final ReceiveChannel receiveChannel, final Collection collection, final Function2 function2, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.mapIndexedNotNullTo((ReceiveChannel<?>)receiveChannel, collection, (Function2<? super Integer, ? super Object, ?>)function2, (Continuation<? super Collection>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object mapIndexedNotNullTo$$forInline(final ReceiveChannel receiveChannel, final SendChannel sendChannel, final Function2 function2, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.mapIndexedNotNullTo((ReceiveChannel<?>)receiveChannel, sendChannel, (Function2<? super Integer, ? super Object, ?>)function2, (Continuation<? super SendChannel>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, R, C extends Collection<? super R>> Object mapIndexedTo(final ReceiveChannel<? extends E> receiveChannel, final C c, final Function2<? super Integer, ? super E, ? extends R> function2, final Continuation<? super C> continuation) {
        return ChannelsKt__Channels_commonKt.mapIndexedTo((ReceiveChannel<?>)receiveChannel, c, (Function2<? super Integer, ? super Object, ?>)function2, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, R, C extends SendChannel<? super R>> Object mapIndexedTo(final ReceiveChannel<? extends E> receiveChannel, final C c, final Function2<? super Integer, ? super E, ? extends R> function2, final Continuation<? super C> continuation) {
        return ChannelsKt__Channels_commonKt.mapIndexedTo((ReceiveChannel<?>)receiveChannel, c, (Function2<? super Integer, ? super Object, ?>)function2, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object mapIndexedTo$$forInline(final ReceiveChannel receiveChannel, final Collection collection, final Function2 function2, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.mapIndexedTo((ReceiveChannel<?>)receiveChannel, collection, (Function2<? super Integer, ? super Object, ?>)function2, (Continuation<? super Collection>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object mapIndexedTo$$forInline(final ReceiveChannel receiveChannel, final SendChannel sendChannel, final Function2 function2, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.mapIndexedTo((ReceiveChannel<?>)receiveChannel, sendChannel, (Function2<? super Integer, ? super Object, ?>)function2, (Continuation<? super SendChannel>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, R> ReceiveChannel<R> mapNotNull(final ReceiveChannel<? extends E> receiveChannel, final CoroutineContext coroutineContext, final Function2<? super E, ? super Continuation<? super R>, ?> function2) {
        return ChannelsKt__Channels_commonKt.mapNotNull((ReceiveChannel<?>)receiveChannel, coroutineContext, (Function2<? super Object, ? super Continuation<? super R>, ?>)function2);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, R, C extends Collection<? super R>> Object mapNotNullTo(final ReceiveChannel<? extends E> receiveChannel, final C c, final Function1<? super E, ? extends R> function1, final Continuation<? super C> continuation) {
        return ChannelsKt__Channels_commonKt.mapNotNullTo((ReceiveChannel<?>)receiveChannel, c, (Function1<? super Object, ?>)function1, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, R, C extends SendChannel<? super R>> Object mapNotNullTo(final ReceiveChannel<? extends E> receiveChannel, final C c, final Function1<? super E, ? extends R> function1, final Continuation<? super C> continuation) {
        return ChannelsKt__Channels_commonKt.mapNotNullTo((ReceiveChannel<?>)receiveChannel, c, (Function1<? super Object, ?>)function1, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object mapNotNullTo$$forInline(final ReceiveChannel receiveChannel, final Collection collection, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.mapNotNullTo((ReceiveChannel<?>)receiveChannel, collection, (Function1<? super Object, ?>)function1, (Continuation<? super Collection>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object mapNotNullTo$$forInline(final ReceiveChannel receiveChannel, final SendChannel sendChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.mapNotNullTo((ReceiveChannel<?>)receiveChannel, sendChannel, (Function1<? super Object, ?>)function1, (Continuation<? super SendChannel>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, R, C extends Collection<? super R>> Object mapTo(final ReceiveChannel<? extends E> receiveChannel, final C c, final Function1<? super E, ? extends R> function1, final Continuation<? super C> continuation) {
        return ChannelsKt__Channels_commonKt.mapTo((ReceiveChannel<?>)receiveChannel, c, (Function1<? super Object, ?>)function1, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, R, C extends SendChannel<? super R>> Object mapTo(final ReceiveChannel<? extends E> receiveChannel, final C c, final Function1<? super E, ? extends R> function1, final Continuation<? super C> continuation) {
        return ChannelsKt__Channels_commonKt.mapTo((ReceiveChannel<?>)receiveChannel, c, (Function1<? super Object, ?>)function1, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object mapTo$$forInline(final ReceiveChannel receiveChannel, final Collection collection, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.mapTo((ReceiveChannel<?>)receiveChannel, collection, (Function1<? super Object, ?>)function1, (Continuation<? super Collection>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object mapTo$$forInline(final ReceiveChannel receiveChannel, final SendChannel sendChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.mapTo((ReceiveChannel<?>)receiveChannel, sendChannel, (Function1<? super Object, ?>)function1, (Continuation<? super SendChannel>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, R extends Comparable<? super R>> Object maxBy(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, ? extends R> function1, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.maxBy((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, ? extends Comparable>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object maxBy$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.maxBy((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, ? extends Comparable>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object maxWith(final ReceiveChannel<? extends E> receiveChannel, final Comparator<? super E> comparator, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.maxWith((ReceiveChannel<?>)receiveChannel, (Comparator<? super Object>)comparator, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, R extends Comparable<? super R>> Object minBy(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, ? extends R> function1, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.minBy((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, ? extends Comparable>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object minBy$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.minBy((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, ? extends Comparable>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object minWith(final ReceiveChannel<? extends E> receiveChannel, final Comparator<? super E> comparator, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.minWith((ReceiveChannel<?>)receiveChannel, (Comparator<? super Object>)comparator, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object none(final ReceiveChannel<? extends E> receiveChannel, final Continuation<? super Boolean> continuation) {
        return ChannelsKt__Channels_commonKt.none((ReceiveChannel<?>)receiveChannel, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object none(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, Boolean> function1, final Continuation<? super Boolean> continuation) {
        return ChannelsKt__Channels_commonKt.none((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object none$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.none((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Boolean>)continuation);
    }
    
    public static final <E> SelectClause1<E> onReceiveOrNull(final ReceiveChannel<? extends E> receiveChannel) {
        return ChannelsKt__Channels_commonKt.onReceiveOrNull(receiveChannel);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object partition(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, Boolean> function1, final Continuation<? super Pair<? extends List<? extends E>, ? extends List<? extends E>>> continuation) {
        return ChannelsKt__Channels_commonKt.partition((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Pair<? extends List<?>, ? extends List<?>>>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object partition$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.partition((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Pair<? extends List<?>, ? extends List<?>>>)continuation);
    }
    
    public static final <E> Object receiveOrNull(final ReceiveChannel<? extends E> receiveChannel, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.receiveOrNull((ReceiveChannel<?>)receiveChannel, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <S, E extends S> Object reduce(final ReceiveChannel<? extends E> receiveChannel, final Function2<? super S, ? super E, ? extends S> function2, final Continuation<? super S> continuation) {
        return ChannelsKt__Channels_commonKt.reduce((ReceiveChannel<?>)receiveChannel, (Function2<? super Object, ? super Object, ?>)function2, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object reduce$$forInline(final ReceiveChannel receiveChannel, final Function2 function2, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.reduce((ReceiveChannel<?>)receiveChannel, (Function2<? super Object, ? super Object, ?>)function2, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <S, E extends S> Object reduceIndexed(final ReceiveChannel<? extends E> receiveChannel, final Function3<? super Integer, ? super S, ? super E, ? extends S> function3, final Continuation<? super S> continuation) {
        return ChannelsKt__Channels_commonKt.reduceIndexed((ReceiveChannel<?>)receiveChannel, (Function3<? super Integer, ? super Object, ? super Object, ?>)function3, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object reduceIndexed$$forInline(final ReceiveChannel receiveChannel, final Function3 function3, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.reduceIndexed((ReceiveChannel<?>)receiveChannel, (Function3<? super Integer, ? super Object, ? super Object, ?>)function3, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> ReceiveChannel<E> requireNoNulls(final ReceiveChannel<? extends E> receiveChannel) {
        return ChannelsKt__Channels_commonKt.requireNoNulls(receiveChannel);
    }
    
    public static final <E> void sendBlocking(final SendChannel<? super E> sendChannel, final E e) {
        ChannelsKt__ChannelsKt.sendBlocking(sendChannel, e);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object single(final ReceiveChannel<? extends E> receiveChannel, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.single((ReceiveChannel<?>)receiveChannel, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object single(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, Boolean> function1, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.single((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object single$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.single((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object singleOrNull(final ReceiveChannel<? extends E> receiveChannel, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.singleOrNull((ReceiveChannel<?>)receiveChannel, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object singleOrNull(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, Boolean> function1, final Continuation<? super E> continuation) {
        return ChannelsKt__Channels_commonKt.singleOrNull((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object singleOrNull$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.singleOrNull((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Boolean>)function1, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object sumBy(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, Integer> function1, final Continuation<? super Integer> continuation) {
        return ChannelsKt__Channels_commonKt.sumBy((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Integer>)function1, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object sumBy$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.sumBy((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Integer>)function1, (Continuation<? super Integer>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object sumByDouble(final ReceiveChannel<? extends E> receiveChannel, final Function1<? super E, Double> function1, final Continuation<? super Double> continuation) {
        return ChannelsKt__Channels_commonKt.sumByDouble((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Double>)function1, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    private static final Object sumByDouble$$forInline(final ReceiveChannel receiveChannel, final Function1 function1, final Continuation continuation) {
        return ChannelsKt__Channels_commonKt.sumByDouble((ReceiveChannel<?>)receiveChannel, (Function1<? super Object, Double>)function1, (Continuation<? super Double>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> ReceiveChannel<E> take(final ReceiveChannel<? extends E> receiveChannel, final int n, final CoroutineContext coroutineContext) {
        return ChannelsKt__Channels_commonKt.take(receiveChannel, n, coroutineContext);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> ReceiveChannel<E> takeWhile(final ReceiveChannel<? extends E> receiveChannel, final CoroutineContext coroutineContext, final Function2<? super E, ? super Continuation<? super Boolean>, ?> function2) {
        return ChannelsKt__Channels_commonKt.takeWhile(receiveChannel, coroutineContext, function2);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, C extends SendChannel<? super E>> Object toChannel(final ReceiveChannel<? extends E> receiveChannel, final C c, final Continuation<? super C> continuation) {
        return ChannelsKt__Channels_commonKt.toChannel((ReceiveChannel<?>)receiveChannel, c, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, C extends Collection<? super E>> Object toCollection(final ReceiveChannel<? extends E> receiveChannel, final C c, final Continuation<? super C> continuation) {
        return ChannelsKt__Channels_commonKt.toCollection((ReceiveChannel<?>)receiveChannel, c, continuation);
    }
    
    public static final <E> Object toList(final ReceiveChannel<? extends E> receiveChannel, final Continuation<? super List<? extends E>> continuation) {
        return ChannelsKt__Channels_commonKt.toList((ReceiveChannel<?>)receiveChannel, (Continuation<? super List<?>>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <K, V, M extends Map<? super K, ? super V>> Object toMap(final ReceiveChannel<? extends Pair<? extends K, ? extends V>> receiveChannel, final M m, final Continuation<? super M> continuation) {
        return ChannelsKt__Channels_commonKt.toMap((ReceiveChannel<? extends Pair<?, ?>>)receiveChannel, m, continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <K, V> Object toMap(final ReceiveChannel<? extends Pair<? extends K, ? extends V>> receiveChannel, final Continuation<? super Map<K, ? extends V>> continuation) {
        return ChannelsKt__Channels_commonKt.toMap((ReceiveChannel<? extends Pair<?, ?>>)receiveChannel, (Continuation<? super Map<Object, ?>>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object toMutableList(final ReceiveChannel<? extends E> receiveChannel, final Continuation<? super List<E>> continuation) {
        return ChannelsKt__Channels_commonKt.toMutableList((ReceiveChannel<?>)receiveChannel, (Continuation<? super List<Object>>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object toMutableSet(final ReceiveChannel<? extends E> receiveChannel, final Continuation<? super Set<E>> continuation) {
        return ChannelsKt__Channels_commonKt.toMutableSet((ReceiveChannel<?>)receiveChannel, (Continuation<? super Set<Object>>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> Object toSet(final ReceiveChannel<? extends E> receiveChannel, final Continuation<? super Set<? extends E>> continuation) {
        return ChannelsKt__Channels_commonKt.toSet((ReceiveChannel<?>)receiveChannel, (Continuation<? super Set<?>>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E> ReceiveChannel<IndexedValue<E>> withIndex(final ReceiveChannel<? extends E> receiveChannel, final CoroutineContext coroutineContext) {
        return ChannelsKt__Channels_commonKt.withIndex(receiveChannel, coroutineContext);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, R> ReceiveChannel<Pair<E, R>> zip(final ReceiveChannel<? extends E> receiveChannel, final ReceiveChannel<? extends R> receiveChannel2) {
        return ChannelsKt__Channels_commonKt.zip(receiveChannel, receiveChannel2);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Channel operators are deprecated in favour of Flow and will be removed in 1.4")
    public static final <E, R, V> ReceiveChannel<V> zip(final ReceiveChannel<? extends E> receiveChannel, final ReceiveChannel<? extends R> receiveChannel2, final CoroutineContext coroutineContext, final Function2<? super E, ? super R, ? extends V> function2) {
        return ChannelsKt__Channels_commonKt.zip((ReceiveChannel<?>)receiveChannel, (ReceiveChannel<?>)receiveChannel2, coroutineContext, (Function2<? super Object, ? super Object, ? extends V>)function2);
    }
}
