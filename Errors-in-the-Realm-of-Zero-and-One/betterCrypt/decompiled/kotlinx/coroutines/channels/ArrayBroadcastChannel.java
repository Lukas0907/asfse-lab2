// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.channels;

import java.util.concurrent.locks.Lock;
import kotlinx.coroutines.selects.SelectKt;
import kotlinx.coroutines.selects.SelectInstance;
import kotlin.DeprecationLevel;
import kotlin.Deprecated;
import java.util.concurrent.CancellationException;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import java.util.Iterator;
import kotlinx.coroutines.internal.ConcurrentKt;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000z\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0000\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003:\u00019B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0012\u0010 \u001a\u00020\u00172\b\u0010!\u001a\u0004\u0018\u00010\"H\u0017J\u0018\u0010 \u001a\u00020#2\u000e\u0010!\u001a\n\u0018\u00010$j\u0004\u0018\u0001`%H\u0016J\u0012\u0010&\u001a\u00020\u00172\b\u0010!\u001a\u0004\u0018\u00010\"H\u0002J\b\u0010'\u001a\u00020#H\u0002J\u0012\u0010(\u001a\u00020\u00172\b\u0010!\u001a\u0004\u0018\u00010\"H\u0016J\b\u0010)\u001a\u00020\u0015H\u0002J\u0015\u0010*\u001a\u00028\u00002\u0006\u0010+\u001a\u00020\u0015H\u0002¢\u0006\u0002\u0010,J\u0015\u0010-\u001a\u00020\t2\u0006\u0010.\u001a\u00028\u0000H\u0014¢\u0006\u0002\u0010/J!\u00100\u001a\u00020\t2\u0006\u0010.\u001a\u00028\u00002\n\u00101\u001a\u0006\u0012\u0002\b\u000302H\u0014¢\u0006\u0002\u00103J\u000e\u00104\u001a\b\u0012\u0004\u0012\u00028\u000005H\u0016J-\u00106\u001a\u00020#2\u0010\b\u0002\u00107\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u001d2\u0010\b\u0002\u00108\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u001dH\u0082\u0010R\u0018\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\t0\bX\u0082\u0004¢\u0006\u0004\n\u0002\u0010\nR\u0014\u0010\u000b\u001a\u00020\f8TX\u0094\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u0012\u0010\u000f\u001a\u00060\u0010j\u0002`\u0011X\u0082\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\u00020\u00178TX\u0094\u0004¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0018R\u0014\u0010\u0019\u001a\u00020\u00178TX\u0094\u0004¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u0018R\u000e\u0010\u001a\u001a\u00020\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R*\u0010\u001b\u001a\u001e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u001d0\u001cj\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u001d`\u001eX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0015X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006:" }, d2 = { "Lkotlinx/coroutines/channels/ArrayBroadcastChannel;", "E", "Lkotlinx/coroutines/channels/AbstractSendChannel;", "Lkotlinx/coroutines/channels/BroadcastChannel;", "capacity", "", "(I)V", "buffer", "", "", "[Ljava/lang/Object;", "bufferDebugString", "", "getBufferDebugString", "()Ljava/lang/String;", "bufferLock", "Ljava/util/concurrent/locks/ReentrantLock;", "Lkotlinx/coroutines/internal/ReentrantLock;", "getCapacity", "()I", "head", "", "isBufferAlwaysFull", "", "()Z", "isBufferFull", "size", "subscribers", "", "Lkotlinx/coroutines/channels/ArrayBroadcastChannel$Subscriber;", "Lkotlinx/coroutines/internal/SubscribersList;", "tail", "cancel", "cause", "", "", "Ljava/util/concurrent/CancellationException;", "Lkotlinx/coroutines/CancellationException;", "cancelInternal", "checkSubOffers", "close", "computeMinHead", "elementAt", "index", "(J)Ljava/lang/Object;", "offerInternal", "element", "(Ljava/lang/Object;)Ljava/lang/Object;", "offerSelectInternal", "select", "Lkotlinx/coroutines/selects/SelectInstance;", "(Ljava/lang/Object;Lkotlinx/coroutines/selects/SelectInstance;)Ljava/lang/Object;", "openSubscription", "Lkotlinx/coroutines/channels/ReceiveChannel;", "updateHead", "addSub", "removeSub", "Subscriber", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class ArrayBroadcastChannel<E> extends AbstractSendChannel<E> implements BroadcastChannel<E>
{
    private final Object[] buffer;
    private final ReentrantLock bufferLock;
    private final int capacity;
    private volatile long head;
    private volatile int size;
    private final List<Subscriber<E>> subscribers;
    private volatile long tail;
    
    public ArrayBroadcastChannel(int capacity) {
        this.capacity = capacity;
        final int capacity2 = this.capacity;
        capacity = 1;
        if (capacity2 < 1) {
            capacity = 0;
        }
        if (capacity != 0) {
            this.bufferLock = new ReentrantLock();
            this.buffer = new Object[this.capacity];
            this.subscribers = ConcurrentKt.subscriberList();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("ArrayBroadcastChannel capacity must be at least 1, but ");
        sb.append(this.capacity);
        sb.append(" was specified");
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public static final /* synthetic */ long access$getTail$p(final ArrayBroadcastChannel arrayBroadcastChannel) {
        return arrayBroadcastChannel.tail;
    }
    
    private final boolean cancelInternal(final Throwable t) {
        final boolean close = this.close(t);
        final Iterator<Subscriber<E>> iterator = this.subscribers.iterator();
        while (iterator.hasNext()) {
            iterator.next().cancelInternal$kotlinx_coroutines_core(t);
        }
        return close;
    }
    
    private final void checkSubOffers() {
        final Iterator<Subscriber<E>> iterator = this.subscribers.iterator();
        boolean b = false;
        boolean b2 = false;
        while (iterator.hasNext()) {
            if (iterator.next().checkOffer()) {
                b = true;
            }
            b2 = true;
        }
        if (b || !b2) {
            updateHead$default(this, null, null, 3, null);
        }
    }
    
    private final long computeMinHead() {
        final Iterator<Subscriber<E>> iterator = this.subscribers.iterator();
        long coerceAtMost = Long.MAX_VALUE;
        while (iterator.hasNext()) {
            coerceAtMost = RangesKt___RangesKt.coerceAtMost(coerceAtMost, iterator.next().subHead);
        }
        return coerceAtMost;
    }
    
    private final E elementAt(final long n) {
        return (E)this.buffer[(int)(n % this.capacity)];
    }
    
    private final void updateHead(Subscriber<E> subscriber, Subscriber<E> subscriber2) {
    Label_0000:
        while (true) {
            final Send send = null;
            final ReentrantLock reentrantLock = this.bufferLock;
            reentrantLock.lock();
            Label_0073: {
                if (subscriber != null) {
                    Label_0383: {
                        try {
                            subscriber.subHead = this.tail;
                            final boolean empty = this.subscribers.isEmpty();
                            this.subscribers.add(subscriber);
                            if (!empty) {
                                reentrantLock.unlock();
                                return;
                            }
                        }
                        finally {
                            break Label_0383;
                        }
                        break Label_0073;
                    }
                    reentrantLock.unlock();
                }
            }
            if (subscriber2 != null) {
                this.subscribers.remove(subscriber2);
                if (this.head != subscriber2.subHead) {
                    reentrantLock.unlock();
                    return;
                }
            }
            final long computeMinHead = this.computeMinHead();
            final long tail = this.tail;
            long head = this.head;
            final long coerceAtMost = RangesKt___RangesKt.coerceAtMost(computeMinHead, tail);
            if (coerceAtMost <= head) {
                reentrantLock.unlock();
                return;
            }
            int size = this.size;
        Label_0164:
            while (head < coerceAtMost) {
                this.buffer[(int)(head % this.capacity)] = null;
                int n;
                if (size >= this.capacity) {
                    n = 1;
                }
                else {
                    n = 0;
                }
                final long head2 = head + 1L;
                this.head = head2;
                final int size2 = size - 1;
                this.size = size2;
                head = head2;
                size = size2;
                if (n != 0) {
                    Object tryResumeSend;
                    Send takeFirstSendOrPeekClosed;
                    do {
                        takeFirstSendOrPeekClosed = this.takeFirstSendOrPeekClosed();
                        head = head2;
                        size = size2;
                        if (takeFirstSendOrPeekClosed == null) {
                            continue Label_0164;
                        }
                        if (takeFirstSendOrPeekClosed instanceof Closed) {
                            head = head2;
                            size = size2;
                            continue Label_0164;
                        }
                        if (takeFirstSendOrPeekClosed == null) {
                            Intrinsics.throwNpe();
                        }
                        tryResumeSend = takeFirstSendOrPeekClosed.tryResumeSend(null);
                    } while (tryResumeSend == null);
                    final Object[] buffer = this.buffer;
                    final int n2 = (int)(tail % this.capacity);
                    if (takeFirstSendOrPeekClosed != null) {
                        buffer[n2] = takeFirstSendOrPeekClosed.getPollResult();
                        this.size = size2 + 1;
                        this.tail = tail + 1L;
                        final Unit instance = Unit.INSTANCE;
                        reentrantLock.unlock();
                        if (takeFirstSendOrPeekClosed == null) {
                            Intrinsics.throwNpe();
                        }
                        takeFirstSendOrPeekClosed.completeResumeSend(tryResumeSend);
                        this.checkSubOffers();
                        subscriber = (subscriber2 = null);
                        continue Label_0000;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.channels.Send");
                }
            }
            reentrantLock.unlock();
        }
    }
    
    static /* synthetic */ void updateHead$default(final ArrayBroadcastChannel arrayBroadcastChannel, Subscriber subscriber, Subscriber subscriber2, final int n, final Object o) {
        if ((n & 0x1) != 0x0) {
            subscriber = null;
        }
        if ((n & 0x2) != 0x0) {
            subscriber2 = null;
        }
        arrayBroadcastChannel.updateHead(subscriber, subscriber2);
    }
    
    @Override
    public void cancel(final CancellationException ex) {
        this.cancelInternal(ex);
    }
    
    @Override
    public boolean close(final Throwable t) {
        if (!super.close(t)) {
            return false;
        }
        this.checkSubOffers();
        return true;
    }
    
    @Override
    protected String getBufferDebugString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("(buffer:capacity=");
        sb.append(this.buffer.length);
        sb.append(",size=");
        sb.append(this.size);
        sb.append(')');
        return sb.toString();
    }
    
    public final int getCapacity() {
        return this.capacity;
    }
    
    @Override
    protected boolean isBufferAlwaysFull() {
        return false;
    }
    
    @Override
    protected boolean isBufferFull() {
        return this.size >= this.capacity;
    }
    
    @Override
    protected Object offerInternal(final E e) {
        final ReentrantLock reentrantLock = this.bufferLock;
        reentrantLock.lock();
        try {
            final Closed<?> closedForSend = this.getClosedForSend();
            if (closedForSend != null) {
                return closedForSend;
            }
            final int size = this.size;
            if (size >= this.capacity) {
                return AbstractChannelKt.OFFER_FAILED;
            }
            final long tail = this.tail;
            this.buffer[(int)(tail % this.capacity)] = e;
            this.size = size + 1;
            this.tail = tail + 1L;
            final Unit instance = Unit.INSTANCE;
            reentrantLock.unlock();
            this.checkSubOffers();
            return AbstractChannelKt.OFFER_SUCCESS;
        }
        finally {
            reentrantLock.unlock();
        }
    }
    
    @Override
    protected Object offerSelectInternal(final E e, final SelectInstance<?> selectInstance) {
        Intrinsics.checkParameterIsNotNull(selectInstance, "select");
        final ReentrantLock reentrantLock = this.bufferLock;
        reentrantLock.lock();
        try {
            final Closed<?> closedForSend = this.getClosedForSend();
            if (closedForSend != null) {
                return closedForSend;
            }
            final int size = this.size;
            if (size >= this.capacity) {
                return AbstractChannelKt.OFFER_FAILED;
            }
            if (!selectInstance.trySelect(null)) {
                return SelectKt.getALREADY_SELECTED();
            }
            final long tail = this.tail;
            this.buffer[(int)(tail % this.capacity)] = e;
            this.size = size + 1;
            this.tail = tail + 1L;
            final Unit instance = Unit.INSTANCE;
            reentrantLock.unlock();
            this.checkSubOffers();
            return AbstractChannelKt.OFFER_SUCCESS;
        }
        finally {
            reentrantLock.unlock();
        }
    }
    
    @Override
    public ReceiveChannel<E> openSubscription() {
        final Subscriber<E> subscriber = new Subscriber<E>(this);
        updateHead$default(this, subscriber, null, 2, null);
        return subscriber;
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000P\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u0000*\u0004\b\u0001\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\u0013\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0017\u0010\u0012\u001a\u00020\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0010¢\u0006\u0002\b\u0015J\u0006\u0010\u0016\u001a\u00020\bJ\b\u0010\u0017\u001a\u00020\u0018H\u0002J\b\u0010\u0019\u001a\u00020\bH\u0002J\n\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0002J\n\u0010\u001c\u001a\u0004\u0018\u00010\u001bH\u0014J\u0016\u0010\u001d\u001a\u0004\u0018\u00010\u001b2\n\u0010\u001e\u001a\u0006\u0012\u0002\b\u00030\u001fH\u0014R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\b8TX\u0094\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\tR\u0014\u0010\n\u001a\u00020\b8TX\u0094\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\tR\u0014\u0010\u000b\u001a\u00020\b8TX\u0094\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\tR\u0014\u0010\f\u001a\u00020\b8TX\u0094\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\tR\u0012\u0010\r\u001a\u00020\u000e8\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u000f\u001a\u00060\u0010j\u0002`\u0011X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006 " }, d2 = { "Lkotlinx/coroutines/channels/ArrayBroadcastChannel$Subscriber;", "E", "Lkotlinx/coroutines/channels/AbstractChannel;", "Lkotlinx/coroutines/channels/ReceiveChannel;", "broadcastChannel", "Lkotlinx/coroutines/channels/ArrayBroadcastChannel;", "(Lkotlinx/coroutines/channels/ArrayBroadcastChannel;)V", "isBufferAlwaysEmpty", "", "()Z", "isBufferAlwaysFull", "isBufferEmpty", "isBufferFull", "subHead", "", "subLock", "Ljava/util/concurrent/locks/ReentrantLock;", "Lkotlinx/coroutines/internal/ReentrantLock;", "cancelInternal", "cause", "", "cancelInternal$kotlinx_coroutines_core", "checkOffer", "clearBuffer", "", "needsToCheckOfferWithoutLock", "peekUnderLock", "", "pollInternal", "pollSelectInternal", "select", "Lkotlinx/coroutines/selects/SelectInstance;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class Subscriber<E> extends AbstractChannel<E> implements ReceiveChannel<E>
    {
        private final ArrayBroadcastChannel<E> broadcastChannel;
        public volatile long subHead;
        private final ReentrantLock subLock;
        
        public Subscriber(final ArrayBroadcastChannel<E> broadcastChannel) {
            Intrinsics.checkParameterIsNotNull(broadcastChannel, "broadcastChannel");
            this.broadcastChannel = broadcastChannel;
            this.subLock = new ReentrantLock();
        }
        
        private final void clearBuffer() {
            final ReentrantLock reentrantLock = this.subLock;
            reentrantLock.lock();
            try {
                this.subHead = ArrayBroadcastChannel.access$getTail$p((ArrayBroadcastChannel<Object>)this.broadcastChannel);
                final Unit instance = Unit.INSTANCE;
            }
            finally {
                reentrantLock.unlock();
            }
        }
        
        private final boolean needsToCheckOfferWithoutLock() {
            return this.getClosedForReceive() == null && (!this.isBufferEmpty() || this.broadcastChannel.getClosedForReceive() != null);
        }
        
        private final Object peekUnderLock() {
            final long subHead = this.subHead;
            Closed<?> closed = this.broadcastChannel.getClosedForReceive();
            if (subHead >= ArrayBroadcastChannel.access$getTail$p((ArrayBroadcastChannel<Object>)this.broadcastChannel)) {
                if (closed == null) {
                    closed = this.getClosedForReceive();
                }
                if (closed != null) {
                    return closed;
                }
                return AbstractChannelKt.POLL_FAILED;
            }
            else {
                final Object access$element = ((ArrayBroadcastChannel<Object>)this.broadcastChannel).elementAt(subHead);
                final Closed<?> closedForReceive = this.getClosedForReceive();
                if (closedForReceive != null) {
                    return closedForReceive;
                }
                return access$element;
            }
        }
        
        @Override
        public boolean cancelInternal$kotlinx_coroutines_core(final Throwable t) {
            final boolean close = this.close(t);
            if (close) {
                ArrayBroadcastChannel.updateHead$default((ArrayBroadcastChannel<Object>)this.broadcastChannel, null, this, 1, null);
            }
            this.clearBuffer();
            return close;
        }
        
        public final boolean checkOffer() {
            final Closed closed = null;
            boolean b = false;
            Closed closed2;
            while (true) {
                closed2 = closed;
                if (!this.needsToCheckOfferWithoutLock()) {
                    break;
                }
                if (!this.subLock.tryLock()) {
                    closed2 = closed;
                    break;
                }
                try {
                    final Object peekUnderLock = this.peekUnderLock();
                    if (peekUnderLock == AbstractChannelKt.POLL_FAILED) {
                        continue;
                    }
                    if (peekUnderLock instanceof Closed) {
                        final Closed closed3 = (Closed)peekUnderLock;
                    }
                    else {
                        final ReceiveOrClosed<E> takeFirstReceiveOrPeekClosed = this.takeFirstReceiveOrPeekClosed();
                        if (takeFirstReceiveOrPeekClosed != null) {
                            if (!(takeFirstReceiveOrPeekClosed instanceof Closed)) {
                                final Object tryResumeReceive = takeFirstReceiveOrPeekClosed.tryResumeReceive((E)peekUnderLock, null);
                                if (tryResumeReceive == null) {
                                    continue;
                                }
                                ++this.subHead;
                                b = true;
                                this.subLock.unlock();
                                if (takeFirstReceiveOrPeekClosed == null) {
                                    Intrinsics.throwNpe();
                                }
                                takeFirstReceiveOrPeekClosed.completeResumeReceive(tryResumeReceive);
                                continue;
                            }
                        }
                    }
                    break;
                    continue;
                }
                finally {
                    this.subLock.unlock();
                }
                break;
            }
            if (closed2 != null) {
                this.close(closed2.closeCause);
            }
            return b;
        }
        
        @Override
        protected boolean isBufferAlwaysEmpty() {
            return false;
        }
        
        @Override
        protected boolean isBufferAlwaysFull() {
            throw new IllegalStateException("Should not be used".toString());
        }
        
        @Override
        protected boolean isBufferEmpty() {
            return this.subHead >= ArrayBroadcastChannel.access$getTail$p((ArrayBroadcastChannel<Object>)this.broadcastChannel);
        }
        
        @Override
        protected boolean isBufferFull() {
            throw new IllegalStateException("Should not be used".toString());
        }
        
        @Override
        protected Object pollInternal() {
            while (true) {
                Object o = this.subLock;
                ((Lock)o).lock();
                while (true) {
                    try {
                        final Object peekUnderLock = this.peekUnderLock();
                        if (!(peekUnderLock instanceof Closed)) {
                            if (peekUnderLock != AbstractChannelKt.POLL_FAILED) {
                                ++this.subHead;
                                int n = 1;
                                ((Lock)o).unlock();
                                if (!(peekUnderLock instanceof Closed)) {
                                    o = null;
                                }
                                else {
                                    o = peekUnderLock;
                                }
                                o = o;
                                if (o != null) {
                                    this.close(((Closed)o).closeCause);
                                }
                                if (this.checkOffer()) {
                                    n = 1;
                                }
                                if (n != 0) {
                                    ArrayBroadcastChannel.updateHead$default((ArrayBroadcastChannel<Object>)this.broadcastChannel, null, null, 3, null);
                                }
                                return peekUnderLock;
                            }
                        }
                    }
                    finally {
                        ((Lock)o).unlock();
                    }
                    int n = 0;
                    continue;
                }
            }
        }
        
        @Override
        protected Object pollSelectInternal(final SelectInstance<?> selectInstance) {
            Intrinsics.checkParameterIsNotNull(selectInstance, "select");
            final ReentrantLock reentrantLock = this.subLock;
            reentrantLock.lock();
            try {
                final Object peekUnderLock = this.peekUnderLock();
                final boolean b = peekUnderLock instanceof Closed;
                final int n = 1;
                int n2 = 0;
                Object already_SELECTED;
                if (b) {
                    already_SELECTED = peekUnderLock;
                }
                else if (peekUnderLock == AbstractChannelKt.POLL_FAILED) {
                    already_SELECTED = peekUnderLock;
                }
                else if (!selectInstance.trySelect(null)) {
                    already_SELECTED = SelectKt.getALREADY_SELECTED();
                }
                else {
                    ++this.subHead;
                    n2 = 1;
                    already_SELECTED = peekUnderLock;
                }
                reentrantLock.unlock();
                Object o;
                if (!(already_SELECTED instanceof Closed)) {
                    o = null;
                }
                else {
                    o = already_SELECTED;
                }
                final Closed closed = (Closed)o;
                if (closed != null) {
                    this.close(closed.closeCause);
                }
                if (this.checkOffer()) {
                    n2 = n;
                }
                if (n2 != 0) {
                    ArrayBroadcastChannel.updateHead$default((ArrayBroadcastChannel<Object>)this.broadcastChannel, null, null, 3, null);
                }
                return already_SELECTED;
            }
            finally {
                reentrantLock.unlock();
            }
        }
    }
}
