// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.channels;

import kotlin.Unit;
import kotlinx.coroutines.selects.SelectClause2;
import kotlin.DeprecationLevel;
import kotlin.Deprecated;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.DebugKt;
import kotlinx.coroutines.intrinsics.UndispatchedKt;
import kotlin.jvm.internal.TypeIntrinsics;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.TypeCastException;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.selects.SelectInstance;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlinx.coroutines.internal.Symbol;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000v\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\r\b\u0007\u0018\u0000 B*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u00028\u00000\u0002:\u0004CBDEB\u0011\b\u0016\u0012\u0006\u0010\u0003\u001a\u00028\u0000¢\u0006\u0004\b\u0004\u0010\u0005B\u0007¢\u0006\u0004\b\u0004\u0010\u0006J?\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\b0\u00072\u0014\u0010\t\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\b\u0018\u00010\u00072\f\u0010\n\u001a\b\u0012\u0004\u0012\u00028\u00000\bH\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u0019\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\rH\u0017¢\u0006\u0004\b\u0010\u0010\u0011J\u001f\u0010\u0010\u001a\u00020\u00142\u000e\u0010\u000e\u001a\n\u0018\u00010\u0012j\u0004\u0018\u0001`\u0013H\u0016¢\u0006\u0004\b\u0010\u0010\u0015J\u0019\u0010\u0016\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\rH\u0016¢\u0006\u0004\b\u0016\u0010\u0011J\u001d\u0010\u0017\u001a\u00020\u00142\f\u0010\n\u001a\b\u0012\u0004\u0012\u00028\u00000\bH\u0002¢\u0006\u0004\b\u0017\u0010\u0018J)\u0010\u001c\u001a\u00020\u00142\u0018\u0010\u001b\u001a\u0014\u0012\u0006\u0012\u0004\u0018\u00010\r\u0012\u0004\u0012\u00020\u00140\u0019j\u0002`\u001aH\u0016¢\u0006\u0004\b\u001c\u0010\u001dJ\u0019\u0010\u001e\u001a\u00020\u00142\b\u0010\u000e\u001a\u0004\u0018\u00010\rH\u0002¢\u0006\u0004\b\u001e\u0010\u001fJ\u0017\u0010!\u001a\u00020\u000f2\u0006\u0010 \u001a\u00028\u0000H\u0016¢\u0006\u0004\b!\u0010\"J\u0019\u0010$\u001a\u0004\u0018\u00010#2\u0006\u0010 \u001a\u00028\u0000H\u0002¢\u0006\u0004\b$\u0010%J\u0015\u0010'\u001a\b\u0012\u0004\u0012\u00028\u00000&H\u0016¢\u0006\u0004\b'\u0010(JX\u00101\u001a\u00020\u0014\"\u0004\b\u0001\u0010)2\f\u0010+\u001a\b\u0012\u0004\u0012\u00028\u00010*2\u0006\u0010 \u001a\u00028\u00002(\u00100\u001a$\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000-\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00010.\u0012\u0006\u0012\u0004\u0018\u00010/0,H\u0002\u00f8\u0001\u0000¢\u0006\u0004\b1\u00102J?\u00103\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\b\u0018\u00010\u00072\u0012\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\b0\u00072\f\u0010\n\u001a\b\u0012\u0004\u0012\u00028\u00000\bH\u0002¢\u0006\u0004\b3\u0010\fJ\u001b\u00104\u001a\u00020\u00142\u0006\u0010 \u001a\u00028\u0000H\u0096@\u00f8\u0001\u0000¢\u0006\u0004\b4\u00105R\u0016\u00106\u001a\u00020\u000f8V@\u0016X\u0096\u0004¢\u0006\u0006\u001a\u0004\b6\u00107R\u0016\u00108\u001a\u00020\u000f8V@\u0016X\u0096\u0004¢\u0006\u0006\u001a\u0004\b8\u00107R(\u0010<\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000-098V@\u0016X\u0096\u0004¢\u0006\u0006\u001a\u0004\b:\u0010;R\u0019\u0010\u0003\u001a\u00028\u00008F@\u0006¢\u0006\f\u0012\u0004\b?\u0010\u0006\u001a\u0004\b=\u0010>R\u0015\u0010A\u001a\u0004\u0018\u00018\u00008F@\u0006¢\u0006\u0006\u001a\u0004\b@\u0010>\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006F" }, d2 = { "Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;", "E", "Lkotlinx/coroutines/channels/BroadcastChannel;", "value", "<init>", "(Ljava/lang/Object;)V", "()V", "", "Lkotlinx/coroutines/channels/ConflatedBroadcastChannel$Subscriber;", "list", "subscriber", "addSubscriber", "([Lkotlinx/coroutines/channels/ConflatedBroadcastChannel$Subscriber;Lkotlinx/coroutines/channels/ConflatedBroadcastChannel$Subscriber;)[Lkotlinx/coroutines/channels/ConflatedBroadcastChannel$Subscriber;", "", "cause", "", "cancel", "(Ljava/lang/Throwable;)Z", "Ljava/util/concurrent/CancellationException;", "Lkotlinx/coroutines/CancellationException;", "", "(Ljava/util/concurrent/CancellationException;)V", "close", "closeSubscriber", "(Lkotlinx/coroutines/channels/ConflatedBroadcastChannel$Subscriber;)V", "Lkotlin/Function1;", "Lkotlinx/coroutines/channels/Handler;", "handler", "invokeOnClose", "(Lkotlin/jvm/functions/Function1;)V", "invokeOnCloseHandler", "(Ljava/lang/Throwable;)V", "element", "offer", "(Ljava/lang/Object;)Z", "Lkotlinx/coroutines/channels/ConflatedBroadcastChannel$Closed;", "offerInternal", "(Ljava/lang/Object;)Lkotlinx/coroutines/channels/ConflatedBroadcastChannel$Closed;", "Lkotlinx/coroutines/channels/ReceiveChannel;", "openSubscription", "()Lkotlinx/coroutines/channels/ReceiveChannel;", "R", "Lkotlinx/coroutines/selects/SelectInstance;", "select", "Lkotlin/Function2;", "Lkotlinx/coroutines/channels/SendChannel;", "Lkotlin/coroutines/Continuation;", "", "block", "registerSelectSend", "(Lkotlinx/coroutines/selects/SelectInstance;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)V", "removeSubscriber", "send", "(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "isClosedForSend", "()Z", "isFull", "Lkotlinx/coroutines/selects/SelectClause2;", "getOnSend", "()Lkotlinx/coroutines/selects/SelectClause2;", "onSend", "getValue", "()Ljava/lang/Object;", "value$annotations", "getValueOrNull", "valueOrNull", "Companion", "Closed", "State", "Subscriber", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class ConflatedBroadcastChannel<E> implements BroadcastChannel<E>
{
    private static final Closed CLOSED;
    private static final Companion Companion;
    private static final State<Object> INITIAL_STATE;
    private static final Symbol UNDEFINED;
    private static final AtomicReferenceFieldUpdater _state$FU;
    private static final AtomicIntegerFieldUpdater _updating$FU;
    private static final AtomicReferenceFieldUpdater onCloseHandler$FU;
    private volatile Object _state;
    private volatile int _updating;
    private volatile Object onCloseHandler;
    
    static {
        Companion = new Companion(null);
        CLOSED = new Closed(null);
        UNDEFINED = new Symbol("UNDEFINED");
        INITIAL_STATE = new State<Object>(ConflatedBroadcastChannel.UNDEFINED, null);
        _state$FU = AtomicReferenceFieldUpdater.newUpdater(ConflatedBroadcastChannel.class, Object.class, "_state");
        _updating$FU = AtomicIntegerFieldUpdater.newUpdater(ConflatedBroadcastChannel.class, "_updating");
        onCloseHandler$FU = AtomicReferenceFieldUpdater.newUpdater(ConflatedBroadcastChannel.class, Object.class, "onCloseHandler");
    }
    
    public ConflatedBroadcastChannel() {
        this._state = ConflatedBroadcastChannel.INITIAL_STATE;
        this._updating = 0;
        this.onCloseHandler = null;
    }
    
    public ConflatedBroadcastChannel(final E e) {
        this();
        ConflatedBroadcastChannel._state$FU.lazySet(this, new State(e, null));
    }
    
    private final Subscriber<E>[] addSubscriber(final Subscriber<E>[] array, final Subscriber<E> subscriber) {
        if (array == null) {
            final Subscriber[] array2 = { null };
            for (int i = 0; i < 1; ++i) {
                array2[i] = subscriber;
            }
            return (Subscriber<E>[])array2;
        }
        return ArraysKt___ArraysJvmKt.plus(array, subscriber);
    }
    
    private final void closeSubscriber(final Subscriber<E> subscriber) {
        Object state;
        Object value;
        Subscriber<E>[] subscribers;
        do {
            state = this._state;
            if (state instanceof Closed) {
                return;
            }
            if (!(state instanceof State)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid state ");
                sb.append(state);
                throw new IllegalStateException(sb.toString().toString());
            }
            final State state2 = (State)state;
            value = state2.value;
            if (state == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.channels.ConflatedBroadcastChannel.State<E>");
            }
            subscribers = state2.subscribers;
            if (subscribers != null) {
                continue;
            }
            Intrinsics.throwNpe();
        } while (!ConflatedBroadcastChannel._state$FU.compareAndSet(this, state, new State(value, this.removeSubscriber((Subscriber<Object>[])subscribers, (Subscriber<Object>)subscriber))));
    }
    
    private final void invokeOnCloseHandler(final Throwable t) {
        final Object onCloseHandler = this.onCloseHandler;
        if (onCloseHandler != null && onCloseHandler != AbstractChannelKt.HANDLER_INVOKED && ConflatedBroadcastChannel.onCloseHandler$FU.compareAndSet(this, onCloseHandler, AbstractChannelKt.HANDLER_INVOKED)) {
            ((Function1)TypeIntrinsics.beforeCheckcastToFunctionOfArity(onCloseHandler, 1)).invoke(t);
        }
    }
    
    private final Closed offerInternal(final E e) {
        if (!ConflatedBroadcastChannel._updating$FU.compareAndSet(this, 0, 1)) {
            return null;
        }
        Label_0014: {
            break Label_0014;
            try {
                Object state;
                do {
                    state = this._state;
                    if (state instanceof Closed) {
                        return (Closed)state;
                    }
                    if (!(state instanceof State)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Invalid state ");
                        sb.append(state);
                        throw new IllegalStateException(sb.toString().toString());
                    }
                    if (state != null) {
                        continue;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.channels.ConflatedBroadcastChannel.State<E>");
                } while (!ConflatedBroadcastChannel._state$FU.compareAndSet(this, state, new State(e, (Subscriber<Object>[])((State)state).subscribers)));
                final Subscriber<E>[] subscribers = ((State)state).subscribers;
                if (subscribers != null) {
                    for (int length = subscribers.length, i = 0; i < length; ++i) {
                        subscribers[i].offerInternal((E)e);
                    }
                }
                return null;
            }
            finally {
                this._updating = 0;
            }
        }
    }
    
    private final <R> void registerSelectSend(final SelectInstance<? super R> selectInstance, final E e, final Function2<? super SendChannel<? super E>, ? super Continuation<? super R>, ?> function2) {
        if (!selectInstance.trySelect(null)) {
            return;
        }
        final Closed offerInternal = this.offerInternal(e);
        if (offerInternal != null) {
            selectInstance.resumeSelectCancellableWithException(offerInternal.getSendException());
            return;
        }
        UndispatchedKt.startCoroutineUnintercepted((Function2<? super ConflatedBroadcastChannel, ? super Continuation<? super Object>, ?>)function2, this, (Continuation<? super Object>)selectInstance.getCompletion());
    }
    
    private final Subscriber<E>[] removeSubscriber(final Subscriber<E>[] array, final Subscriber<E> subscriber) {
        final int length = array.length;
        final int index = ArraysKt___ArraysKt.indexOf(array, subscriber);
        if (DebugKt.getASSERTIONS_ENABLED() && index < 0) {
            throw new AssertionError();
        }
        if (length == 1) {
            return null;
        }
        final Subscriber[] array2 = new Subscriber[length - 1];
        ArraysKt___ArraysJvmKt.copyInto$default(array, array2, 0, 0, index, 6, null);
        ArraysKt___ArraysJvmKt.copyInto$default(array, array2, index, index + 1, 0, 8, null);
        return (Subscriber<E>[])array2;
    }
    
    @Override
    public void cancel(final CancellationException ex) {
        this.close(ex);
    }
    
    @Override
    public boolean close(final Throwable t) {
        Object state;
        Closed closed;
        int i;
        do {
            state = this._state;
            final boolean b = state instanceof Closed;
            i = 0;
            if (b) {
                return false;
            }
            if (!(state instanceof State)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid state ");
                sb.append(state);
                throw new IllegalStateException(sb.toString().toString());
            }
            if (t == null) {
                closed = ConflatedBroadcastChannel.CLOSED;
            }
            else {
                closed = new Closed(t);
            }
        } while (!ConflatedBroadcastChannel._state$FU.compareAndSet(this, state, closed));
        if (state != null) {
            final Subscriber<E>[] subscribers = ((State)state).subscribers;
            if (subscribers != null) {
                while (i < subscribers.length) {
                    subscribers[i].close(t);
                    ++i;
                }
            }
            this.invokeOnCloseHandler(t);
            return true;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.channels.ConflatedBroadcastChannel.State<E>");
    }
    
    @Override
    public SelectClause2<E, SendChannel<E>> getOnSend() {
        return (SelectClause2<E, SendChannel<E>>)new ConflatedBroadcastChannel$onSend.ConflatedBroadcastChannel$onSend$1(this);
    }
    
    public final E getValue() {
        final Object state = this._state;
        if (state instanceof Closed) {
            throw ((Closed)state).getValueException();
        }
        if (!(state instanceof State)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid state ");
            sb.append(state);
            throw new IllegalStateException(sb.toString().toString());
        }
        final State state2 = (State)state;
        if (state2.value != ConflatedBroadcastChannel.UNDEFINED) {
            return (E)state2.value;
        }
        throw new IllegalStateException("No value");
    }
    
    public final E getValueOrNull() {
        final Object state = this._state;
        if (state instanceof Closed) {
            return null;
        }
        if (!(state instanceof State)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid state ");
            sb.append(state);
            throw new IllegalStateException(sb.toString().toString());
        }
        final Symbol undefined = ConflatedBroadcastChannel.UNDEFINED;
        final Object value = ((State)state).value;
        if (value == undefined) {
            return null;
        }
        return (E)value;
    }
    
    @Override
    public void invokeOnClose(final Function1<? super Throwable, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(function1, "handler");
        if (ConflatedBroadcastChannel.onCloseHandler$FU.compareAndSet(this, null, function1)) {
            final Object state = this._state;
            if (state instanceof Closed && ConflatedBroadcastChannel.onCloseHandler$FU.compareAndSet(this, function1, AbstractChannelKt.HANDLER_INVOKED)) {
                function1.invoke(((Closed)state).closeCause);
            }
            return;
        }
        final Object onCloseHandler = this.onCloseHandler;
        if (onCloseHandler == AbstractChannelKt.HANDLER_INVOKED) {
            throw new IllegalStateException("Another handler was already registered and successfully invoked");
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Another handler was already registered: ");
        sb.append(onCloseHandler);
        throw new IllegalStateException(sb.toString());
    }
    
    @Override
    public boolean isClosedForSend() {
        return this._state instanceof Closed;
    }
    
    @Override
    public boolean isFull() {
        return false;
    }
    
    @Override
    public boolean offer(final E e) {
        final Closed offerInternal = this.offerInternal(e);
        if (offerInternal == null) {
            return true;
        }
        throw offerInternal.getSendException();
    }
    
    @Override
    public ReceiveChannel<E> openSubscription() {
        final Subscriber<E> subscriber = new Subscriber<E>(this);
        Object state;
        Object value;
        State state2;
        do {
            state = this._state;
            if (state instanceof Closed) {
                subscriber.close(((Closed)state).closeCause);
                return subscriber;
            }
            if (!(state instanceof State)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid state ");
                sb.append(state);
                throw new IllegalStateException(sb.toString().toString());
            }
            state2 = (State)state;
            if (state2.value != ConflatedBroadcastChannel.UNDEFINED) {
                subscriber.offerInternal((E)state2.value);
            }
            value = state2.value;
            if (state != null) {
                continue;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.channels.ConflatedBroadcastChannel.State<E>");
        } while (!ConflatedBroadcastChannel._state$FU.compareAndSet(this, state, new State(value, (Subscriber<Object>[])this.addSubscriber(state2.subscribers, (Subscriber<E>)subscriber))));
        return subscriber;
    }
    
    @Override
    public Object send(final E e, final Continuation<? super Unit> continuation) {
        final Closed offerInternal = this.offerInternal(e);
        if (offerInternal == null) {
            return Unit.INSTANCE;
        }
        throw offerInternal.getSendException();
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0007\b\u0002\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004R\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\b\u001a\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\t\u0010\u0007¨\u0006\n" }, d2 = { "Lkotlinx/coroutines/channels/ConflatedBroadcastChannel$Closed;", "", "closeCause", "", "(Ljava/lang/Throwable;)V", "sendException", "getSendException", "()Ljava/lang/Throwable;", "valueException", "getValueException", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class Closed
    {
        public final Throwable closeCause;
        
        public Closed(final Throwable closeCause) {
            this.closeCause = closeCause;
        }
        
        public final Throwable getSendException() {
            final Throwable closeCause = this.closeCause;
            if (closeCause != null) {
                return closeCause;
            }
            return new ClosedSendChannelException("Channel was closed");
        }
        
        public final Throwable getValueException() {
            final Throwable closeCause = this.closeCause;
            if (closeCause != null) {
                return closeCause;
            }
            return new IllegalStateException("Channel was closed");
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0082\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\u00020\u00048\u0002X\u0083\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u0005\u0010\u0002R\u0016\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\u00020\t8\u0002X\u0083\u0004¢\u0006\b\n\u0000\u0012\u0004\b\n\u0010\u0002¨\u0006\u000b" }, d2 = { "Lkotlinx/coroutines/channels/ConflatedBroadcastChannel$Companion;", "", "()V", "CLOSED", "Lkotlinx/coroutines/channels/ConflatedBroadcastChannel$Closed;", "CLOSED$annotations", "INITIAL_STATE", "Lkotlinx/coroutines/channels/ConflatedBroadcastChannel$State;", "UNDEFINED", "Lkotlinx/coroutines/internal/Symbol;", "UNDEFINED$annotations", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class Companion
    {
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0002\u0018\u0000*\u0004\b\u0001\u0010\u00012\u00020\u0002B%\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\u0014\u0010\u0004\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00010\u0006\u0018\u00010\u0005¢\u0006\u0002\u0010\u0007R \u0010\u0004\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00010\u0006\u0018\u00010\u00058\u0006X\u0087\u0004¢\u0006\u0004\n\u0002\u0010\bR\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\t" }, d2 = { "Lkotlinx/coroutines/channels/ConflatedBroadcastChannel$State;", "E", "", "value", "subscribers", "", "Lkotlinx/coroutines/channels/ConflatedBroadcastChannel$Subscriber;", "(Ljava/lang/Object;[Lkotlinx/coroutines/channels/ConflatedBroadcastChannel$Subscriber;)V", "[Lkotlinx/coroutines/channels/ConflatedBroadcastChannel$Subscriber;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class State<E>
    {
        public final Subscriber<E>[] subscribers;
        public final Object value;
        
        public State(final Object value, final Subscriber<E>[] subscribers) {
            this.value = value;
            this.subscribers = subscribers;
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\b\u0002\u0018\u0000*\u0004\b\u0001\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\u0013\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0010¢\u0006\u0002\b\u000bJ\u0015\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00028\u0001H\u0016¢\u0006\u0002\u0010\u000fR\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00010\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010" }, d2 = { "Lkotlinx/coroutines/channels/ConflatedBroadcastChannel$Subscriber;", "E", "Lkotlinx/coroutines/channels/ConflatedChannel;", "Lkotlinx/coroutines/channels/ReceiveChannel;", "broadcastChannel", "Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;", "(Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;)V", "cancelInternal", "", "cause", "", "cancelInternal$kotlinx_coroutines_core", "offerInternal", "", "element", "(Ljava/lang/Object;)Ljava/lang/Object;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class Subscriber<E> extends ConflatedChannel<E> implements ReceiveChannel<E>
    {
        private final ConflatedBroadcastChannel<E> broadcastChannel;
        
        public Subscriber(final ConflatedBroadcastChannel<E> broadcastChannel) {
            Intrinsics.checkParameterIsNotNull(broadcastChannel, "broadcastChannel");
            this.broadcastChannel = broadcastChannel;
        }
        
        @Override
        public boolean cancelInternal$kotlinx_coroutines_core(final Throwable t) {
            final boolean close = this.close(t);
            if (close) {
                ((ConflatedBroadcastChannel<Object>)this.broadcastChannel).closeSubscriber(this);
            }
            return close;
        }
        
        public Object offerInternal(final E e) {
            return super.offerInternal(e);
        }
    }
}
