// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.channels;

import kotlinx.coroutines.ExceptionsKt;
import kotlinx.coroutines.DebugStringsKt;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.CoroutineExceptionHandlerKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u00002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0012\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B#\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\rH\u0014J\u0012\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\rH\u0014¨\u0006\u0011" }, d2 = { "Lkotlinx/coroutines/channels/ActorCoroutine;", "E", "Lkotlinx/coroutines/channels/ChannelCoroutine;", "Lkotlinx/coroutines/channels/ActorScope;", "parentContext", "Lkotlin/coroutines/CoroutineContext;", "channel", "Lkotlinx/coroutines/channels/Channel;", "active", "", "(Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/channels/Channel;Z)V", "handleJobException", "exception", "", "onCancelling", "", "cause", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
class ActorCoroutine<E> extends ChannelCoroutine<E> implements ActorScope<E>
{
    public ActorCoroutine(final CoroutineContext coroutineContext, final Channel<E> channel, final boolean b) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "parentContext");
        Intrinsics.checkParameterIsNotNull(channel, "channel");
        super(coroutineContext, channel, b);
    }
    
    @Override
    protected boolean handleJobException(final Throwable t) {
        Intrinsics.checkParameterIsNotNull(t, "exception");
        CoroutineExceptionHandlerKt.handleCoroutineException(this.getContext(), t);
        return true;
    }
    
    @Override
    protected void onCancelling(final Throwable t) {
        final Channel<E> get_channel = this.get_channel();
        CancellationException cancellationException = null;
        final CancellationException ex = null;
        if (t != null) {
            Throwable t2;
            if (!(t instanceof CancellationException)) {
                t2 = ex;
            }
            else {
                t2 = t;
            }
            cancellationException = (CancellationException)t2;
            if (cancellationException == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(DebugStringsKt.getClassSimpleName(this));
                sb.append(" was cancelled");
                cancellationException = ExceptionsKt.CancellationException(sb.toString(), t);
            }
        }
        get_channel.cancel(cancellationException);
    }
}
