// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.channels;

import kotlin.coroutines.EmptyCoroutineContext;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.CoroutineContextKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineScope;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlinx.coroutines.CancellableContinuation;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.Job;
import kotlin.ResultKt;
import kotlin.coroutines.Continuation;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000T\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a)\u0010\u0000\u001a\u00020\u0001*\u0006\u0012\u0002\b\u00030\u00022\u000e\b\u0002\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00010\u0004H\u0087@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0005\u001a\u0094\u0001\u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\b0\u0007\"\u0004\b\u0000\u0010\b*\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\r2-\b\u0002\u0010\u000e\u001a'\u0012\u0015\u0012\u0013\u0018\u00010\u0010¢\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\u0013\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u000fj\u0004\u0018\u0001`\u00142/\b\u0001\u0010\u0003\u001a)\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\b0\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00010\u0016\u0012\u0006\u0012\u0004\u0018\u00010\u00170\u0015¢\u0006\u0002\b\u0018H\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0019\u001ae\u0010\u0006\u001a\b\u0012\u0004\u0012\u0002H\b0\u0007\"\u0004\b\u0000\u0010\b*\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\r2/\b\u0001\u0010\u0003\u001a)\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\b0\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00010\u0016\u0012\u0006\u0012\u0004\u0018\u00010\u00170\u0015¢\u0006\u0002\b\u0018H\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u001a\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u001b" }, d2 = { "awaitClose", "", "Lkotlinx/coroutines/channels/ProducerScope;", "block", "Lkotlin/Function0;", "(Lkotlinx/coroutines/channels/ProducerScope;Lkotlin/jvm/functions/Function0;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "produce", "Lkotlinx/coroutines/channels/ReceiveChannel;", "E", "Lkotlinx/coroutines/CoroutineScope;", "context", "Lkotlin/coroutines/CoroutineContext;", "capacity", "", "onCompletion", "Lkotlin/Function1;", "", "Lkotlin/ParameterName;", "name", "cause", "Lkotlinx/coroutines/CompletionHandler;", "Lkotlin/Function2;", "Lkotlin/coroutines/Continuation;", "", "Lkotlin/ExtensionFunctionType;", "(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;ILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/channels/ReceiveChannel;", "(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;ILkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/channels/ReceiveChannel;", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class ProduceKt
{
    public static final Object awaitClose(final ProducerScope<?> l$0, final Function0<Unit> l$2, Continuation<? super Unit> continuation) {
        ProduceKt$awaitClose.ProduceKt$awaitClose$1 produceKt$awaitClose$1 = null;
        Label_0050: {
            if (continuation instanceof ProduceKt$awaitClose.ProduceKt$awaitClose$1) {
                produceKt$awaitClose$1 = (ProduceKt$awaitClose.ProduceKt$awaitClose$1)continuation;
                if ((produceKt$awaitClose$1.label & Integer.MIN_VALUE) != 0x0) {
                    produceKt$awaitClose$1.label += Integer.MIN_VALUE;
                    break Label_0050;
                }
            }
            produceKt$awaitClose$1 = new ProduceKt$awaitClose.ProduceKt$awaitClose$1(continuation);
        }
        final Object result = produceKt$awaitClose$1.result;
        final Object coroutine_SUSPENDED = IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
        final int label = produceKt$awaitClose$1.label;
        final Function0<Unit> function2;
        Label_0268: {
            if (label != 0) {
                if (label == 1) {
                    final Function0 function0 = (Function0)produceKt$awaitClose$1.L$1;
                    final ProducerScope producerScope = (ProducerScope)produceKt$awaitClose$1.L$0;
                    continuation = (Continuation)function0;
                    Label_0279: {
                        try {
                            ResultKt.throwOnFailure(result);
                            break Label_0268;
                        }
                        finally {
                            break Label_0279;
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    ((Function0)continuation).invoke();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ResultKt.throwOnFailure(result);
            if ((Job)((Continuation)produceKt$awaitClose$1).getContext().get((CoroutineContext.Key<CoroutineContext.Element>)Job.Key) != l$0) {
                throw new IllegalStateException("awaitClose() can be invoke only from the producer context".toString());
            }
            produceKt$awaitClose$1.L$0 = l$0;
            produceKt$awaitClose$1.L$1 = l$2;
            produceKt$awaitClose$1.label = 1;
            final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt__IntrinsicsJvmKt.intercepted((Continuation<? super Object>)produceKt$awaitClose$1), 1);
            l$0.invokeOnClose((Function1<? super Throwable, Unit>)new ProduceKt$awaitClose$4.ProduceKt$awaitClose$4$1((CancellableContinuation)cancellableContinuationImpl));
            final Object result2 = cancellableContinuationImpl.getResult();
            if (result2 == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                DebugProbesKt.probeCoroutineSuspended((Continuation<?>)produceKt$awaitClose$1);
            }
            function2 = l$2;
            if (result2 == coroutine_SUSPENDED) {
                return coroutine_SUSPENDED;
            }
        }
        function2.invoke();
        return Unit.INSTANCE;
    }
    
    public static final <E> ReceiveChannel<E> produce(final CoroutineScope coroutineScope, final CoroutineContext coroutineContext, final int n, final Function1<? super Throwable, Unit> function1, final Function2<? super ProducerScope<? super E>, ? super Continuation<? super Unit>, ?> function2) {
        Intrinsics.checkParameterIsNotNull(coroutineScope, "$this$produce");
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        Intrinsics.checkParameterIsNotNull(function2, "block");
        final ProducerCoroutine<E> producerCoroutine = new ProducerCoroutine<E>(CoroutineContextKt.newCoroutineContext(coroutineScope, coroutineContext), ChannelKt.Channel(n));
        if (function1 != null) {
            producerCoroutine.invokeOnCompletion(function1);
        }
        producerCoroutine.start(CoroutineStart.DEFAULT, producerCoroutine, (Function2<? super ProducerCoroutine<E>, ? super Continuation<? super Object>, ?>)function2);
        return producerCoroutine;
    }
    
    public static final <E> ReceiveChannel<E> produce(final CoroutineScope coroutineScope, final CoroutineContext coroutineContext, final int n, final Function2<? super ProducerScope<? super E>, ? super Continuation<? super Unit>, ?> function2) {
        Intrinsics.checkParameterIsNotNull(coroutineScope, "$this$produce");
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        Intrinsics.checkParameterIsNotNull(function2, "block");
        final ProducerCoroutine<E> producerCoroutine = new ProducerCoroutine<E>(CoroutineContextKt.newCoroutineContext(coroutineScope, coroutineContext), ChannelKt.Channel(n));
        producerCoroutine.start(CoroutineStart.DEFAULT, producerCoroutine, (Function2<? super ProducerCoroutine<E>, ? super Continuation<? super Object>, ?>)function2);
        return producerCoroutine;
    }
}
