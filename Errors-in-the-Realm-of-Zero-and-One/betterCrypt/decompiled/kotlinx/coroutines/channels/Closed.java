// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.channels;

import kotlinx.coroutines.DebugKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\f\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0000\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00002\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\u000f\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0014\u0010\u0016\u001a\u00020\u00122\n\u0010\u0017\u001a\u0006\u0012\u0002\b\u00030\u0000H\u0016J\b\u0010\u0018\u001a\u00020\u0019H\u0016J!\u0010\u001a\u001a\u0004\u0018\u00010\u00142\u0006\u0010\u001b\u001a\u00028\u00002\b\u0010\u001c\u001a\u0004\u0018\u00010\u0014H\u0016¢\u0006\u0002\u0010\u001dJ\u0014\u0010\u001e\u001a\u0004\u0018\u00010\u00142\b\u0010\u001c\u001a\u0004\u0018\u00010\u0014H\u0016R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\u00008VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\tR\u001a\u0010\n\u001a\b\u0012\u0004\u0012\u00028\u00000\u00008VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\tR\u0011\u0010\f\u001a\u00020\u00058F¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u000f\u001a\u00020\u00058F¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u000e¨\u0006\u001f" }, d2 = { "Lkotlinx/coroutines/channels/Closed;", "E", "Lkotlinx/coroutines/channels/Send;", "Lkotlinx/coroutines/channels/ReceiveOrClosed;", "closeCause", "", "(Ljava/lang/Throwable;)V", "offerResult", "getOfferResult", "()Lkotlinx/coroutines/channels/Closed;", "pollResult", "getPollResult", "receiveException", "getReceiveException", "()Ljava/lang/Throwable;", "sendException", "getSendException", "completeResumeReceive", "", "token", "", "completeResumeSend", "resumeSendClosed", "closed", "toString", "", "tryResumeReceive", "value", "idempotent", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "tryResumeSend", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class Closed<E> extends Send implements ReceiveOrClosed<E>
{
    public final Throwable closeCause;
    
    public Closed(final Throwable closeCause) {
        this.closeCause = closeCause;
    }
    
    @Override
    public void completeResumeReceive(final Object o) {
        Intrinsics.checkParameterIsNotNull(o, "token");
        if (!DebugKt.getASSERTIONS_ENABLED()) {
            return;
        }
        if (o == AbstractChannelKt.CLOSE_RESUMED) {
            return;
        }
        throw new AssertionError();
    }
    
    @Override
    public void completeResumeSend(final Object o) {
        Intrinsics.checkParameterIsNotNull(o, "token");
        if (!DebugKt.getASSERTIONS_ENABLED()) {
            return;
        }
        if (o == AbstractChannelKt.CLOSE_RESUMED) {
            return;
        }
        throw new AssertionError();
    }
    
    @Override
    public Closed<E> getOfferResult() {
        return this;
    }
    
    @Override
    public Closed<E> getPollResult() {
        return this;
    }
    
    public final Throwable getReceiveException() {
        final Throwable closeCause = this.closeCause;
        if (closeCause != null) {
            return closeCause;
        }
        return new ClosedReceiveChannelException("Channel was closed");
    }
    
    public final Throwable getSendException() {
        final Throwable closeCause = this.closeCause;
        if (closeCause != null) {
            return closeCause;
        }
        return new ClosedSendChannelException("Channel was closed");
    }
    
    @Override
    public void resumeSendClosed(final Closed<?> closed) {
        Intrinsics.checkParameterIsNotNull(closed, "closed");
        if (DebugKt.getASSERTIONS_ENABLED()) {
            throw new AssertionError();
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Closed[");
        sb.append(this.closeCause);
        sb.append(']');
        return sb.toString();
    }
    
    @Override
    public Object tryResumeReceive(final E e, final Object o) {
        return AbstractChannelKt.CLOSE_RESUMED;
    }
    
    @Override
    public Object tryResumeSend(final Object o) {
        return AbstractChannelKt.CLOSE_RESUMED;
    }
}
