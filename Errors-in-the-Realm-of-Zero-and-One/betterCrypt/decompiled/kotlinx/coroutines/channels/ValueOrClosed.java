// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.channels;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000,\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0010\u0003\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0087@\u0018\u0000 \u001f*\u0006\b\u0000\u0010\u0001 \u00012\u00020\u0002:\u0002\u001e\u001fB\u0016\b\u0000\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u00f8\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005J\u0013\u0010\u0016\u001a\u00020\r2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\u000f\u0010\u001a\u001a\u00020\u001bH\u0016¢\u0006\u0004\b\u001c\u0010\u001dR\u0019\u0010\u0006\u001a\u0004\u0018\u00010\u00078F¢\u0006\f\u0012\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0002X\u0082\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\f\u001a\u00020\r8F¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR\u0017\u0010\u0010\u001a\u00028\u00008F¢\u0006\f\u0012\u0004\b\u0011\u0010\t\u001a\u0004\b\u0012\u0010\u0005R\u0019\u0010\u0013\u001a\u0004\u0018\u00018\u00008F¢\u0006\f\u0012\u0004\b\u0014\u0010\t\u001a\u0004\b\u0015\u0010\u0005\u00f8\u0001\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006 " }, d2 = { "Lkotlinx/coroutines/channels/ValueOrClosed;", "T", "", "holder", "constructor-impl", "(Ljava/lang/Object;)Ljava/lang/Object;", "closeCause", "", "closeCause$annotations", "()V", "getCloseCause-impl", "(Ljava/lang/Object;)Ljava/lang/Throwable;", "isClosed", "", "isClosed-impl", "(Ljava/lang/Object;)Z", "value", "value$annotations", "getValue-impl", "valueOrNull", "valueOrNull$annotations", "getValueOrNull-impl", "equals", "other", "hashCode", "", "toString", "", "toString-impl", "(Ljava/lang/Object;)Ljava/lang/String;", "Closed", "Companion", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class ValueOrClosed<T>
{
    public static final Companion Companion;
    private final Object holder = holder;
    
    static {
        Companion = new Companion(null);
    }
    
    public static Object constructor-impl(final Object o) {
        return o;
    }
    
    public static boolean equals-impl(final Object o, final Object o2) {
        return o2 instanceof ValueOrClosed && Intrinsics.areEqual(o, ((ValueOrClosed)o2).unbox-impl());
    }
    
    public static final boolean equals-impl0(final Object o, final Object o2) {
        throw null;
    }
    
    public static final Throwable getCloseCause-impl(final Object o) {
        if (o instanceof Closed) {
            return ((Closed)o).cause;
        }
        throw new IllegalStateException("Channel was not closed".toString());
    }
    
    public static final T getValue-impl(final Object o) {
        if (!(o instanceof Closed)) {
            return (T)o;
        }
        throw new IllegalStateException("Channel was closed".toString());
    }
    
    public static final T getValueOrNull-impl(final Object o) {
        Object o2 = o;
        if (o instanceof Closed) {
            o2 = null;
        }
        return (T)o2;
    }
    
    public static int hashCode-impl(final Object o) {
        if (o != null) {
            return o.hashCode();
        }
        return 0;
    }
    
    public static final boolean isClosed-impl(final Object o) {
        return o instanceof Closed;
    }
    
    public static String toString-impl(final Object obj) {
        if (obj instanceof Closed) {
            return obj.toString();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Value(");
        sb.append(obj);
        sb.append(')');
        return sb.toString();
    }
    
    @Override
    public boolean equals(final Object o) {
        return equals-impl(this.holder, o);
    }
    
    @Override
    public int hashCode() {
        return hashCode-impl(this.holder);
    }
    
    @Override
    public String toString() {
        return toString-impl(this.holder);
    }
    
    public final /* synthetic */ Object unbox-impl() {
        return this.holder;
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0000\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004J\u0013\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\b\u0010\b\u001a\u00020\tH\u0016J\b\u0010\n\u001a\u00020\u000bH\u0016R\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\f" }, d2 = { "Lkotlinx/coroutines/channels/ValueOrClosed$Closed;", "", "cause", "", "(Ljava/lang/Throwable;)V", "equals", "", "other", "hashCode", "", "toString", "", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    public static final class Closed
    {
        public final Throwable cause;
        
        public Closed(final Throwable cause) {
            this.cause = cause;
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof Closed && Intrinsics.areEqual(this.cause, ((Closed)o).cause);
        }
        
        @Override
        public int hashCode() {
            final Throwable cause = this.cause;
            if (cause != null) {
                return cause.hashCode();
            }
            return 0;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Closed(");
            sb.append(this.cause);
            sb.append(')');
            return sb.toString();
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0006\b\u0080\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J)\u0010\u0003\u001a\b\u0012\u0004\u0012\u0002H\u00050\u0004\"\u0004\b\u0001\u0010\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u0080\b\u00f8\u0001\u0000¢\u0006\u0004\b\b\u0010\tJ'\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\u00050\u0004\"\u0004\b\u0001\u0010\u00052\u0006\u0010\n\u001a\u0002H\u0005H\u0080\b\u00f8\u0001\u0000¢\u0006\u0004\b\u000b\u0010\f\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\r" }, d2 = { "Lkotlinx/coroutines/channels/ValueOrClosed$Companion;", "", "()V", "closed", "Lkotlinx/coroutines/channels/ValueOrClosed;", "E", "cause", "", "closed$kotlinx_coroutines_core", "(Ljava/lang/Throwable;)Ljava/lang/Object;", "value", "value$kotlinx_coroutines_core", "(Ljava/lang/Object;)Ljava/lang/Object;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    public static final class Companion
    {
        private Companion() {
        }
        
        public final <E> Object closed$kotlinx_coroutines_core(final Throwable t) {
            return ValueOrClosed.constructor-impl(new Closed(t));
        }
        
        public final <E> Object value$kotlinx_coroutines_core(final E e) {
            return ValueOrClosed.constructor-impl(e);
        }
    }
}
