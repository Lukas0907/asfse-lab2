// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.channels;

import kotlinx.coroutines.CoroutineExceptionHandlerKt;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.selects.SelectClause2;
import kotlinx.coroutines.JobSupport;
import kotlin.DeprecationLevel;
import kotlin.Deprecated;
import java.util.concurrent.CancellationException;
import kotlin.coroutines.Continuation;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;
import kotlin.Unit;
import kotlinx.coroutines.AbstractCoroutine;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\\\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0012\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u00020\u00030\u00022\b\u0012\u0004\u0012\u0002H\u00010\u00042\b\u0012\u0004\u0012\u0002H\u00010\u0005B#\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005\u0012\u0006\u0010\t\u001a\u00020\n¢\u0006\u0002\u0010\u000bJ\u0012\u0010\u001a\u001a\u00020\n2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0007J\u0016\u0010\u001a\u001a\u00020\u00032\u000e\u0010\u001b\u001a\n\u0018\u00010\u001dj\u0004\u0018\u0001`\u001eJ\u0012\u0010\u001f\u001a\u00020\n2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\u0013\u0010 \u001a\u00020\n2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0096\u0001J.\u0010!\u001a\u00020\u00032#\u0010\"\u001a\u001f\u0012\u0015\u0012\u0013\u0018\u00010\u001c¢\u0006\f\b$\u0012\b\b%\u0012\u0004\b\b(\u001b\u0012\u0004\u0012\u00020\u00030#H\u0097\u0001J\u0016\u0010&\u001a\u00020\n2\u0006\u0010'\u001a\u00028\u0000H\u0096\u0001¢\u0006\u0002\u0010(J\u0018\u0010)\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010*\u001a\u00020\nH\u0014J\u0015\u0010+\u001a\u00020\u00032\u0006\u0010,\u001a\u00020\u0003H\u0014¢\u0006\u0002\u0010-J\u000f\u0010.\u001a\b\u0012\u0004\u0012\u00028\u00000/H\u0096\u0001J\u0019\u00100\u001a\u00020\u00032\u0006\u0010'\u001a\u00028\u0000H\u0096A\u00f8\u0001\u0000¢\u0006\u0002\u00101R\u001a\u0010\b\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005X\u0084\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u001a\u0010\u000e\u001a\b\u0012\u0004\u0012\u00028\u00000\u000f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0012\u001a\u00020\n8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u0014\u0010\u0014\u001a\u00020\n8\u0016X\u0097\u0005¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0013R\u0014\u0010\u0015\u001a\u00020\n8\u0016X\u0097\u0005¢\u0006\u0006\u001a\u0004\b\u0015\u0010\u0013R$\u0010\u0016\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u000f0\u0017X\u0096\u0005¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019\u0082\u0002\u0004\n\u0002\b\u0019¨\u00062" }, d2 = { "Lkotlinx/coroutines/channels/BroadcastCoroutine;", "E", "Lkotlinx/coroutines/AbstractCoroutine;", "", "Lkotlinx/coroutines/channels/ProducerScope;", "Lkotlinx/coroutines/channels/BroadcastChannel;", "parentContext", "Lkotlin/coroutines/CoroutineContext;", "_channel", "active", "", "(Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/channels/BroadcastChannel;Z)V", "get_channel", "()Lkotlinx/coroutines/channels/BroadcastChannel;", "channel", "Lkotlinx/coroutines/channels/SendChannel;", "getChannel", "()Lkotlinx/coroutines/channels/SendChannel;", "isActive", "()Z", "isClosedForSend", "isFull", "onSend", "Lkotlinx/coroutines/selects/SelectClause2;", "getOnSend", "()Lkotlinx/coroutines/selects/SelectClause2;", "cancel", "cause", "", "Ljava/util/concurrent/CancellationException;", "Lkotlinx/coroutines/CancellationException;", "cancelInternal", "close", "invokeOnClose", "handler", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "offer", "element", "(Ljava/lang/Object;)Z", "onCancelled", "handled", "onCompleted", "value", "(Lkotlin/Unit;)V", "openSubscription", "Lkotlinx/coroutines/channels/ReceiveChannel;", "send", "(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
class BroadcastCoroutine<E> extends AbstractCoroutine<Unit> implements ProducerScope<E>, BroadcastChannel<E>
{
    private final BroadcastChannel<E> _channel;
    
    public BroadcastCoroutine(final CoroutineContext coroutineContext, final BroadcastChannel<E> channel, final boolean b) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "parentContext");
        Intrinsics.checkParameterIsNotNull(channel, "_channel");
        super(coroutineContext, b);
        this._channel = channel;
    }
    
    static /* synthetic */ Object send$suspendImpl(final BroadcastCoroutine broadcastCoroutine, final Object o, final Continuation continuation) {
        return broadcastCoroutine._channel.send((E)o, continuation);
    }
    
    @Override
    public final void cancel(final CancellationException ex) {
        this.cancelInternal(ex);
    }
    
    @Override
    public boolean cancelInternal(final Throwable t) {
        final BroadcastChannel<E> channel = this._channel;
        CancellationException cancellationException$default = null;
        if (t != null) {
            cancellationException$default = JobSupport.toCancellationException$default(this, t, null, 1, null);
        }
        channel.cancel(cancellationException$default);
        this.cancelCoroutine(t);
        return true;
    }
    
    @Override
    public boolean close(final Throwable t) {
        return this._channel.close(t);
    }
    
    @Override
    public SendChannel<E> getChannel() {
        return this;
    }
    
    @Override
    public SelectClause2<E, SendChannel<E>> getOnSend() {
        return this._channel.getOnSend();
    }
    
    protected final BroadcastChannel<E> get_channel() {
        return this._channel;
    }
    
    @Override
    public void invokeOnClose(final Function1<? super Throwable, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(function1, "handler");
        this._channel.invokeOnClose(function1);
    }
    
    @Override
    public boolean isActive() {
        return super.isActive();
    }
    
    @Override
    public boolean isClosedForSend() {
        return this._channel.isClosedForSend();
    }
    
    @Override
    public boolean isFull() {
        return this._channel.isFull();
    }
    
    @Override
    public boolean offer(final E e) {
        return this._channel.offer(e);
    }
    
    @Override
    protected void onCancelled(final Throwable t, final boolean b) {
        Intrinsics.checkParameterIsNotNull(t, "cause");
        if (!this._channel.close(t) && !b) {
            CoroutineExceptionHandlerKt.handleCoroutineException(this.getContext(), t);
        }
    }
    
    @Override
    protected void onCompleted(final Unit unit) {
        Intrinsics.checkParameterIsNotNull(unit, "value");
        SendChannel.DefaultImpls.close$default(this._channel, null, 1, null);
    }
    
    @Override
    public ReceiveChannel<E> openSubscription() {
        return this._channel.openSubscription();
    }
    
    @Override
    public Object send(final E e, final Continuation<? super Unit> continuation) {
        return send$suspendImpl(this, e, continuation);
    }
}
