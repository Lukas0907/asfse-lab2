// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.sync;

import kotlinx.coroutines.internal.Symbol;
import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.internal.Segment;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.Result;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import kotlin.Metadata;
import kotlinx.coroutines.internal.SegmentQueue;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0002\u0018\u00002\u00020\u00012\b\u0012\u0004\u0012\u00020\u00030\u0002B\u0017\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bJ\u0013\u0010\n\u001a\u00020\tH\u0096@\u00f8\u0001\u0000¢\u0006\u0004\b\n\u0010\u000bJ\u0013\u0010\f\u001a\u00020\tH\u0082@\u00f8\u0001\u0000¢\u0006\u0004\b\f\u0010\u000bJ\r\u0010\r\u001a\u00020\u0004¢\u0006\u0004\b\r\u0010\u000eJ!\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u000f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0003H\u0016¢\u0006\u0004\b\u0012\u0010\u0013J\u000f\u0010\u0014\u001a\u00020\tH\u0016¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0017\u001a\u00020\tH\u0000¢\u0006\u0004\b\u0016\u0010\u0015J\u000f\u0010\u0019\u001a\u00020\u0018H\u0016¢\u0006\u0004\b\u0019\u0010\u001aR\u0016\u0010\u001c\u001a\u00020\u00048V@\u0016X\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001b\u0010\u000eR\u0016\u0010\u0005\u001a\u00020\u00048\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u001d\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u001e" }, d2 = { "Lkotlinx/coroutines/sync/SemaphoreImpl;", "Lkotlinx/coroutines/sync/Semaphore;", "Lkotlinx/coroutines/internal/SegmentQueue;", "Lkotlinx/coroutines/sync/SemaphoreSegment;", "", "permits", "acquiredPermits", "<init>", "(II)V", "", "acquire", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "addToQueueAndSuspend", "incPermits", "()I", "", "id", "prev", "newSegment", "(JLkotlinx/coroutines/sync/SemaphoreSegment;)Lkotlinx/coroutines/sync/SemaphoreSegment;", "release", "()V", "resumeNextFromQueue$kotlinx_coroutines_core", "resumeNextFromQueue", "", "tryAcquire", "()Z", "getAvailablePermits", "availablePermits", "I", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
final class SemaphoreImpl extends SegmentQueue<SemaphoreSegment> implements Semaphore
{
    private static final AtomicIntegerFieldUpdater _availablePermits$FU;
    private static final AtomicLongFieldUpdater deqIdx$FU;
    static final AtomicLongFieldUpdater enqIdx$FU;
    private volatile int _availablePermits;
    private volatile long deqIdx;
    volatile long enqIdx;
    private final int permits;
    
    static {
        _availablePermits$FU = AtomicIntegerFieldUpdater.newUpdater(SemaphoreImpl.class, "_availablePermits");
        enqIdx$FU = AtomicLongFieldUpdater.newUpdater(SemaphoreImpl.class, "enqIdx");
        deqIdx$FU = AtomicLongFieldUpdater.newUpdater(SemaphoreImpl.class, "deqIdx");
    }
    
    public SemaphoreImpl(int permits, final int n) {
        this.permits = permits;
        permits = this.permits;
        final int n2 = 1;
        if (permits > 0) {
            permits = 1;
        }
        else {
            permits = 0;
        }
        if (permits == 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Semaphore should have at least 1 permit, but had ");
            sb.append(this.permits);
            throw new IllegalArgumentException(sb.toString().toString());
        }
        permits = this.permits;
        Label_0055: {
            if (n >= 0) {
                if (permits >= n) {
                    permits = n2;
                    break Label_0055;
                }
            }
            permits = 0;
        }
        if (permits != 0) {
            this._availablePermits = this.permits - n;
            this.enqIdx = 0L;
            this.deqIdx = 0L;
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("The number of acquired permits should be in 0..");
        sb2.append(this.permits);
        throw new IllegalArgumentException(sb2.toString().toString());
    }
    
    @Override
    public Object acquire(final Continuation<? super Unit> continuation) {
        if (SemaphoreImpl._availablePermits$FU.getAndDecrement(this) > 0) {
            return Unit.INSTANCE;
        }
        return this.addToQueueAndSuspend(continuation);
    }
    
    final /* synthetic */ Object addToQueueAndSuspend(final Continuation<? super Unit> continuation) {
        final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt__IntrinsicsJvmKt.intercepted((Continuation<? super T>)continuation), 0);
        final CancellableContinuationImpl newValue = cancellableContinuationImpl;
        final SemaphoreSegment access$getTail$p = SegmentQueue.this.getTail();
        final long andIncrement = SemaphoreImpl.enqIdx$FU.getAndIncrement(this);
        final SemaphoreSegment access$getSegment = SegmentQueue.this.getSegment(access$getTail$p, andIncrement / SemaphoreKt.access$getSEGMENT_SIZE$p());
        final int n = (int)(andIncrement % SemaphoreKt.access$getSEGMENT_SIZE$p());
        if (access$getSegment != null && access$getSegment.acquirers.get(n) != SemaphoreKt.access$getRESUMED$p() && access$getSegment.acquirers.compareAndSet(n, null, newValue)) {
            newValue.invokeOnCancellation(new CancelSemaphoreAcquisitionHandler(this, access$getSegment, n));
        }
        else {
            final CancellableContinuationImpl cancellableContinuationImpl2 = newValue;
            final Unit instance = Unit.INSTANCE;
            final Result.Companion companion = Result.Companion;
            cancellableContinuationImpl2.resumeWith(Result.constructor-impl(instance));
        }
        final Object result = cancellableContinuationImpl.getResult();
        if (result == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            DebugProbesKt.probeCoroutineSuspended(continuation);
        }
        return result;
    }
    
    @Override
    public int getAvailablePermits() {
        return Math.max(this._availablePermits, 0);
    }
    
    public final int incPermits() {
        int availablePermits;
        do {
            availablePermits = this._availablePermits;
            if (availablePermits < this.permits) {
                continue;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("The number of released permits cannot be greater than ");
            sb.append(this.permits);
            throw new IllegalStateException(sb.toString().toString());
        } while (!SemaphoreImpl._availablePermits$FU.compareAndSet(this, availablePermits, availablePermits + 1));
        return availablePermits;
    }
    
    @Override
    public SemaphoreSegment newSegment(final long n, final SemaphoreSegment semaphoreSegment) {
        return new SemaphoreSegment(n, semaphoreSegment);
    }
    
    @Override
    public void release() {
        if (this.incPermits() >= 0) {
            return;
        }
        this.resumeNextFromQueue$kotlinx_coroutines_core();
    }
    
    public final void resumeNextFromQueue$kotlinx_coroutines_core() {
        while (true) {
            final SemaphoreSegment semaphoreSegment = this.getHead();
            final long andIncrement = SemaphoreImpl.deqIdx$FU.getAndIncrement(this);
            final SemaphoreSegment semaphoreSegment2 = this.getSegmentAndMoveHead(semaphoreSegment, andIncrement / SemaphoreKt.access$getSEGMENT_SIZE$p());
            if (semaphoreSegment2 != null) {
                final Symbol andSet = semaphoreSegment2.acquirers.getAndSet((int)(andIncrement % SemaphoreKt.access$getSEGMENT_SIZE$p()), SemaphoreKt.access$getRESUMED$p());
                if (andSet == null) {
                    return;
                }
                if (andSet == SemaphoreKt.access$getCANCELLED$p()) {
                    continue;
                }
                final CancellableContinuation cancellableContinuation = (CancellableContinuation)andSet;
                final Unit instance = Unit.INSTANCE;
                final Result.Companion companion = Result.Companion;
                cancellableContinuation.resumeWith(Result.constructor-impl(instance));
            }
        }
    }
    
    @Override
    public boolean tryAcquire() {
        int availablePermits;
        do {
            availablePermits = this._availablePermits;
            if (availablePermits <= 0) {
                return false;
            }
        } while (!SemaphoreImpl._availablePermits$FU.compareAndSet(this, availablePermits, availablePermits - 1));
        return true;
    }
}
