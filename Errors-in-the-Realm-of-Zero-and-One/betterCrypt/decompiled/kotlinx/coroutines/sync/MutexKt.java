// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.sync;

import kotlin.jvm.internal.InlineMarker;
import kotlin.Unit;
import kotlin.ResultKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function0;
import kotlinx.coroutines.internal.Symbol;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000.\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u0010\u0010\u0013\u001a\u00020\u00142\b\b\u0002\u0010\u0015\u001a\u00020\u0016\u001a5\u0010\u0017\u001a\u0002H\u0018\"\u0004\b\u0000\u0010\u0018*\u00020\u00142\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u0002H\u00180\u001cH\u0086H\u00f8\u0001\u0000¢\u0006\u0002\u0010\u001d\"\u0016\u0010\u0000\u001a\u00020\u00018\u0002X\u0083\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u0002\u0010\u0003\"\u0016\u0010\u0004\u001a\u00020\u00018\u0002X\u0083\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u0005\u0010\u0003\"\u0016\u0010\u0006\u001a\u00020\u00078\u0002X\u0083\u0004¢\u0006\b\n\u0000\u0012\u0004\b\b\u0010\u0003\"\u0016\u0010\t\u001a\u00020\u00078\u0002X\u0083\u0004¢\u0006\b\n\u0000\u0012\u0004\b\n\u0010\u0003\"\u0016\u0010\u000b\u001a\u00020\u00078\u0002X\u0083\u0004¢\u0006\b\n\u0000\u0012\u0004\b\f\u0010\u0003\"\u0016\u0010\r\u001a\u00020\u00078\u0002X\u0083\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u000e\u0010\u0003\"\u0016\u0010\u000f\u001a\u00020\u00078\u0002X\u0083\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u0010\u0010\u0003\"\u0016\u0010\u0011\u001a\u00020\u00078\u0002X\u0083\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u0012\u0010\u0003\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u001e" }, d2 = { "EMPTY_LOCKED", "Lkotlinx/coroutines/sync/Empty;", "EMPTY_LOCKED$annotations", "()V", "EMPTY_UNLOCKED", "EMPTY_UNLOCKED$annotations", "ENQUEUE_FAIL", "Lkotlinx/coroutines/internal/Symbol;", "ENQUEUE_FAIL$annotations", "LOCKED", "LOCKED$annotations", "LOCK_FAIL", "LOCK_FAIL$annotations", "SELECT_SUCCESS", "SELECT_SUCCESS$annotations", "UNLOCKED", "UNLOCKED$annotations", "UNLOCK_FAIL", "UNLOCK_FAIL$annotations", "Mutex", "Lkotlinx/coroutines/sync/Mutex;", "locked", "", "withLock", "T", "owner", "", "action", "Lkotlin/Function0;", "(Lkotlinx/coroutines/sync/Mutex;Ljava/lang/Object;Lkotlin/jvm/functions/Function0;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class MutexKt
{
    private static final Empty EMPTY_LOCKED;
    private static final Empty EMPTY_UNLOCKED;
    private static final Symbol ENQUEUE_FAIL;
    private static final Symbol LOCKED;
    private static final Symbol LOCK_FAIL;
    private static final Symbol SELECT_SUCCESS;
    private static final Symbol UNLOCKED;
    private static final Symbol UNLOCK_FAIL;
    
    static {
        LOCK_FAIL = new Symbol("LOCK_FAIL");
        ENQUEUE_FAIL = new Symbol("ENQUEUE_FAIL");
        UNLOCK_FAIL = new Symbol("UNLOCK_FAIL");
        SELECT_SUCCESS = new Symbol("SELECT_SUCCESS");
        LOCKED = new Symbol("LOCKED");
        UNLOCKED = new Symbol("UNLOCKED");
        EMPTY_LOCKED = new Empty(MutexKt.LOCKED);
        EMPTY_UNLOCKED = new Empty(MutexKt.UNLOCKED);
    }
    
    public static final Mutex Mutex(final boolean b) {
        return new MutexImpl(b);
    }
    
    public static final <T> Object withLock(final Mutex l$0, final Object l$2, Function0<? extends T> l$3, Continuation<? super T> mutex) {
        MutexKt$withLock.MutexKt$withLock$1 mutexKt$withLock$1 = null;
        Label_0050: {
            if (mutex instanceof MutexKt$withLock.MutexKt$withLock$1) {
                mutexKt$withLock$1 = (MutexKt$withLock.MutexKt$withLock$1)mutex;
                if ((mutexKt$withLock$1.label & Integer.MIN_VALUE) != 0x0) {
                    mutexKt$withLock$1.label += Integer.MIN_VALUE;
                    break Label_0050;
                }
            }
            mutexKt$withLock$1 = new MutexKt$withLock.MutexKt$withLock$1((Continuation)mutex);
        }
        final Object result = mutexKt$withLock$1.result;
        final Object coroutine_SUSPENDED = IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
        final int label = mutexKt$withLock$1.label;
        Object l$4;
        if (label != 0) {
            if (label != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            l$3 = (Function0)mutexKt$withLock$1.L$2;
            l$4 = mutexKt$withLock$1.L$1;
            mutex = (Mutex)mutexKt$withLock$1.L$0;
            ResultKt.throwOnFailure(result);
        }
        else {
            ResultKt.throwOnFailure(result);
            mutexKt$withLock$1.L$0 = l$0;
            mutexKt$withLock$1.L$1 = l$2;
            mutexKt$withLock$1.L$2 = l$3;
            mutexKt$withLock$1.label = 1;
            mutex = l$0;
            l$4 = l$2;
            if (l$0.lock(l$2, (Continuation<? super Unit>)mutexKt$withLock$1) == coroutine_SUSPENDED) {
                return coroutine_SUSPENDED;
            }
        }
        try {
            return l$3.invoke();
        }
        finally {
            InlineMarker.finallyStart(1);
            mutex.unlock(l$4);
            InlineMarker.finallyEnd(1);
        }
    }
    
    private static final Object withLock$$forInline(final Mutex mutex, final Object o, final Function0 function0, final Continuation continuation) {
        InlineMarker.mark(0);
        mutex.lock(o, continuation);
        InlineMarker.mark(2);
        InlineMarker.mark(1);
        try {
            return function0.invoke();
        }
        finally {
            InlineMarker.finallyStart(1);
            mutex.unlock(o);
            InlineMarker.finallyEnd(1);
        }
    }
}
