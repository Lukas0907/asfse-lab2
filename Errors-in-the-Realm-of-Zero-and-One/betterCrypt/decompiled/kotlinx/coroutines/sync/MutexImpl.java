// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.sync;

import kotlinx.coroutines.internal.AtomicOp;
import kotlinx.coroutines.internal.LockFreeLinkedListHead;
import kotlin.coroutines.ContinuationKt;
import kotlinx.coroutines.DebugKt;
import kotlinx.coroutines.DisposableHandle;
import kotlinx.coroutines.selects.SelectKt;
import kotlinx.coroutines.intrinsics.UndispatchedKt;
import kotlinx.coroutines.internal.AtomicDesc;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.selects.SelectInstance;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlinx.coroutines.CancellableContinuationKt;
import kotlin.TypeCastException;
import kotlinx.coroutines.internal.LockFreeLinkedListNode;
import kotlin.Result;
import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.internal.OpDescriptor;
import kotlin.jvm.internal.Intrinsics;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Metadata;
import kotlinx.coroutines.selects.SelectClause2;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0013\b\u0000\u0018\u00002\u00020\u00012\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0003\u0012\u0004\u0012\u00020\u00010\u0002:\u0006$%&'()B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0006\u0010\u0007J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\t\u0010\nJ\u001d\u0010\f\u001a\u00020\u000b2\b\u0010\b\u001a\u0004\u0018\u00010\u0003H\u0096@\u00f8\u0001\u0000¢\u0006\u0004\b\f\u0010\rJ\u001d\u0010\u000e\u001a\u00020\u000b2\b\u0010\b\u001a\u0004\u0018\u00010\u0003H\u0082@\u00f8\u0001\u0000¢\u0006\u0004\b\u000e\u0010\rJT\u0010\u0015\u001a\u00020\u000b\"\u0004\b\u0000\u0010\u000f2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00000\u00102\b\u0010\b\u001a\u0004\u0018\u00010\u00032\"\u0010\u0014\u001a\u001e\b\u0001\u0012\u0004\u0012\u00020\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0013\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0012H\u0016\u00f8\u0001\u0000¢\u0006\u0004\b\u0015\u0010\u0016J\u000f\u0010\u0018\u001a\u00020\u0017H\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\u0019\u0010\u001a\u001a\u00020\u00042\b\u0010\b\u001a\u0004\u0018\u00010\u0003H\u0016¢\u0006\u0004\b\u001a\u0010\nJ\u0019\u0010\u001b\u001a\u00020\u000b2\b\u0010\b\u001a\u0004\u0018\u00010\u0003H\u0016¢\u0006\u0004\b\u001b\u0010\u001cR\u0016\u0010\u001d\u001a\u00020\u00048V@\u0016X\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001d\u0010\u001eR\u0016\u0010 \u001a\u00020\u00048@@\u0000X\u0080\u0004¢\u0006\u0006\u001a\u0004\b\u001f\u0010\u001eR$\u0010#\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0003\u0012\u0004\u0012\u00020\u00010\u00028V@\u0016X\u0096\u0004¢\u0006\u0006\u001a\u0004\b!\u0010\"\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006*" }, d2 = { "Lkotlinx/coroutines/sync/MutexImpl;", "Lkotlinx/coroutines/sync/Mutex;", "Lkotlinx/coroutines/selects/SelectClause2;", "", "", "locked", "<init>", "(Z)V", "owner", "holdsLock", "(Ljava/lang/Object;)Z", "", "lock", "(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "lockSuspend", "R", "Lkotlinx/coroutines/selects/SelectInstance;", "select", "Lkotlin/Function2;", "Lkotlin/coroutines/Continuation;", "block", "registerSelectClause2", "(Lkotlinx/coroutines/selects/SelectInstance;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)V", "", "toString", "()Ljava/lang/String;", "tryLock", "unlock", "(Ljava/lang/Object;)V", "isLocked", "()Z", "isLockedEmptyQueueState$kotlinx_coroutines_core", "isLockedEmptyQueueState", "getOnLock", "()Lkotlinx/coroutines/selects/SelectClause2;", "onLock", "LockCont", "LockSelect", "LockWaiter", "LockedQueue", "TryLockDesc", "UnlockOp", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class MutexImpl implements Mutex, SelectClause2<Object, Mutex>
{
    static final AtomicReferenceFieldUpdater _state$FU;
    volatile Object _state;
    
    static {
        _state$FU = AtomicReferenceFieldUpdater.newUpdater(MutexImpl.class, Object.class, "_state");
    }
    
    public MutexImpl(final boolean b) {
        Empty state;
        if (b) {
            state = MutexKt.access$getEMPTY_LOCKED$p();
        }
        else {
            state = MutexKt.access$getEMPTY_UNLOCKED$p();
        }
        this._state = state;
    }
    
    @Override
    public SelectClause2<Object, Mutex> getOnLock() {
        return this;
    }
    
    @Override
    public boolean holdsLock(final Object o) {
        Intrinsics.checkParameterIsNotNull(o, "owner");
        final Object state = this._state;
        if (state instanceof Empty) {
            if (((Empty)state).locked != o) {
                return false;
            }
        }
        else if (!(state instanceof LockedQueue) || ((LockedQueue)state).owner != o) {
            return false;
        }
        return true;
    }
    
    @Override
    public boolean isLocked() {
        while (true) {
            final Object state = this._state;
            if (state instanceof Empty) {
                return ((Empty)state).locked != MutexKt.access$getUNLOCKED$p();
            }
            if (state instanceof LockedQueue) {
                return true;
            }
            if (!(state instanceof OpDescriptor)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Illegal state ");
                sb.append(state);
                throw new IllegalStateException(sb.toString().toString());
            }
            ((OpDescriptor)state).perform(this);
        }
    }
    
    public final boolean isLockedEmptyQueueState$kotlinx_coroutines_core() {
        final Object state = this._state;
        return state instanceof LockedQueue && ((LockedQueue)state).isEmpty();
    }
    
    @Override
    public Object lock(final Object o, final Continuation<? super Unit> continuation) {
        if (this.tryLock(o)) {
            return Unit.INSTANCE;
        }
        return this.lockSuspend(o, continuation);
    }
    
    final /* synthetic */ Object lockSuspend(Object result, final Continuation<? super Unit> continuation) {
        final CancellableContinuationImpl<Object> cancellableContinuationImpl = new CancellableContinuationImpl<Object>(IntrinsicsKt__IntrinsicsJvmKt.intercepted((Continuation<? super Object>)continuation), 0);
        final CancellableContinuationImpl<Object> cancellableContinuationImpl2 = cancellableContinuationImpl;
        final LockCont lockCont = new LockCont(result, cancellableContinuationImpl2);
        while (true) {
            final Object state = this._state;
            if (state instanceof Empty) {
                final Empty empty = (Empty)state;
                if (empty.locked != MutexKt.access$getUNLOCKED$p()) {
                    MutexImpl._state$FU.compareAndSet(this, state, new LockedQueue(empty.locked));
                }
                else {
                    Empty access$getEMPTY_LOCKED$p;
                    if (result == null) {
                        access$getEMPTY_LOCKED$p = MutexKt.access$getEMPTY_LOCKED$p();
                    }
                    else {
                        access$getEMPTY_LOCKED$p = new Empty(result);
                    }
                    if (MutexImpl._state$FU.compareAndSet(this, state, access$getEMPTY_LOCKED$p)) {
                        final CancellableContinuationImpl<Object> cancellableContinuationImpl3 = cancellableContinuationImpl2;
                        final Unit instance = Unit.INSTANCE;
                        final Result.Companion companion = Result.Companion;
                        cancellableContinuationImpl3.resumeWith(Result.constructor-impl(instance));
                        break;
                    }
                    continue;
                }
            }
            else if (state instanceof LockedQueue) {
                final LockedQueue lockedQueue = (LockedQueue)state;
                final Object owner = lockedQueue.owner;
                final int n = 1;
                if (owner != result) {
                    final LockCont lockCont2 = lockCont;
                    final LockFreeLinkedListNode.CondAddOp condAddOp = (LockFreeLinkedListNode.CondAddOp)new MutexImpl$lockSuspend$$inlined$suspendAtomicCancellableCoroutine$lambda.MutexImpl$lockSuspend$$inlined$suspendAtomicCancellableCoroutine$lambda$1((LockFreeLinkedListNode)lockCont2, (LockFreeLinkedListNode)lockCont2, state, (CancellableContinuation)cancellableContinuationImpl2, lockCont, this, result);
                    int n2;
                    while (true) {
                        final Object prev = lockedQueue.getPrev();
                        if (prev == null) {
                            throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                        }
                        final int tryCondAddNext = ((LockFreeLinkedListNode)prev).tryCondAddNext(lockCont2, lockedQueue, condAddOp);
                        n2 = n;
                        if (tryCondAddNext == 1) {
                            break;
                        }
                        if (tryCondAddNext != 2) {
                            continue;
                        }
                        n2 = 0;
                        break;
                    }
                    if (n2 != 0) {
                        CancellableContinuationKt.removeOnCancellation(cancellableContinuationImpl2, lockCont2);
                        break;
                    }
                    continue;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Already locked by ");
                sb.append(result);
                throw new IllegalStateException(sb.toString().toString());
            }
            else {
                if (!(state instanceof OpDescriptor)) {
                    result = new StringBuilder();
                    ((StringBuilder)result).append("Illegal state ");
                    ((StringBuilder)result).append(state);
                    throw new IllegalStateException(((StringBuilder)result).toString().toString());
                }
                ((OpDescriptor)state).perform(this);
            }
        }
        result = cancellableContinuationImpl.getResult();
        if (result == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            DebugProbesKt.probeCoroutineSuspended(continuation);
        }
        return result;
    }
    
    @Override
    public <R> void registerSelectClause2(final SelectInstance<? super R> selectInstance, final Object obj, final Function2<? super Mutex, ? super Continuation<? super R>, ?> function2) {
        Intrinsics.checkParameterIsNotNull(selectInstance, "select");
        Intrinsics.checkParameterIsNotNull(function2, "block");
        while (!selectInstance.isSelected()) {
            final Object state = this._state;
            if (state instanceof Empty) {
                final Empty empty = (Empty)state;
                if (empty.locked != MutexKt.access$getUNLOCKED$p()) {
                    MutexImpl._state$FU.compareAndSet(this, state, new LockedQueue(empty.locked));
                }
                else {
                    final Object performAtomicTrySelect = selectInstance.performAtomicTrySelect(new TryLockDesc(this, obj));
                    if (performAtomicTrySelect == null) {
                        UndispatchedKt.startCoroutineUnintercepted((Function2<? super MutexImpl, ? super Continuation<? super Object>, ?>)function2, this, selectInstance.getCompletion());
                        return;
                    }
                    if (performAtomicTrySelect == SelectKt.getALREADY_SELECTED()) {
                        return;
                    }
                    if (performAtomicTrySelect == MutexKt.access$getLOCK_FAIL$p()) {
                        continue;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("performAtomicTrySelect(TryLockDesc) returned ");
                    sb.append(performAtomicTrySelect);
                    throw new IllegalStateException(sb.toString().toString());
                }
            }
            else if (state instanceof LockedQueue) {
                final LockedQueue lockedQueue = (LockedQueue)state;
                final Object owner = lockedQueue.owner;
                final int n = 0;
                if (owner != obj) {
                    final LockSelect<Object> lockSelect = new LockSelect<Object>(obj, this, (SelectInstance<? super Object>)selectInstance, function2);
                    final LockSelect<Object> lockSelect2 = lockSelect;
                    final LockFreeLinkedListNode.CondAddOp condAddOp = (LockFreeLinkedListNode.CondAddOp)new MutexImpl$registerSelectClause2$$inlined$addLastIf.MutexImpl$registerSelectClause2$$inlined$addLastIf$1((LockFreeLinkedListNode)lockSelect2, (LockFreeLinkedListNode)lockSelect2, this, state);
                    int tryCondAddNext;
                    int n2;
                    do {
                        final Object prev = lockedQueue.getPrev();
                        if (prev == null) {
                            throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                        }
                        tryCondAddNext = ((LockFreeLinkedListNode)prev).tryCondAddNext(lockSelect2, lockedQueue, condAddOp);
                        if (tryCondAddNext == 1) {
                            n2 = 1;
                            break;
                        }
                        n2 = n;
                    } while (tryCondAddNext != 2);
                    if (n2 != 0) {
                        selectInstance.disposeOnSelect(lockSelect);
                        return;
                    }
                    continue;
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Already locked by ");
                sb2.append(obj);
                throw new IllegalStateException(sb2.toString().toString());
            }
            else {
                if (!(state instanceof OpDescriptor)) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Illegal state ");
                    sb3.append(state);
                    throw new IllegalStateException(sb3.toString().toString());
                }
                ((OpDescriptor)state).perform(this);
            }
        }
    }
    
    @Override
    public String toString() {
        while (true) {
            final Object state = this._state;
            if (state instanceof Empty) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Mutex[");
                sb.append(((Empty)state).locked);
                sb.append(']');
                return sb.toString();
            }
            if (state instanceof OpDescriptor) {
                ((OpDescriptor)state).perform(this);
            }
            else {
                if (state instanceof LockedQueue) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Mutex[");
                    sb2.append(((LockedQueue)state).owner);
                    sb2.append(']');
                    return sb2.toString();
                }
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Illegal state ");
                sb3.append(state);
                throw new IllegalStateException(sb3.toString().toString());
            }
        }
    }
    
    @Override
    public boolean tryLock(Object obj) {
        while (true) {
            final Object state = this._state;
            final boolean b = state instanceof Empty;
            boolean b2 = true;
            if (b) {
                if (((Empty)state).locked != MutexKt.access$getUNLOCKED$p()) {
                    return false;
                }
                Empty access$getEMPTY_LOCKED$p;
                if (obj == null) {
                    access$getEMPTY_LOCKED$p = MutexKt.access$getEMPTY_LOCKED$p();
                }
                else {
                    access$getEMPTY_LOCKED$p = new Empty(obj);
                }
                if (MutexImpl._state$FU.compareAndSet(this, state, access$getEMPTY_LOCKED$p)) {
                    return true;
                }
                continue;
            }
            else if (state instanceof LockedQueue) {
                if (((LockedQueue)state).owner == obj) {
                    b2 = false;
                }
                if (b2) {
                    return false;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Already locked by ");
                sb.append(obj);
                throw new IllegalStateException(sb.toString().toString());
            }
            else {
                if (!(state instanceof OpDescriptor)) {
                    obj = new StringBuilder();
                    ((StringBuilder)obj).append("Illegal state ");
                    ((StringBuilder)obj).append(state);
                    throw new IllegalStateException(((StringBuilder)obj).toString().toString());
                }
                ((OpDescriptor)state).perform(this);
            }
        }
    }
    
    @Override
    public void unlock(Object owner) {
        while (true) {
            final Object state = this._state;
            final boolean b = state instanceof Empty;
            final int n = 1;
            final int n2 = 1;
            boolean b2 = true;
            if (b) {
                if (owner == null) {
                    if (((Empty)state).locked == MutexKt.access$getUNLOCKED$p()) {
                        b2 = false;
                    }
                    if (!b2) {
                        throw new IllegalStateException("Mutex is not locked".toString());
                    }
                }
                else {
                    final Empty empty = (Empty)state;
                    int n3;
                    if (empty.locked == owner) {
                        n3 = n;
                    }
                    else {
                        n3 = 0;
                    }
                    if (n3 == 0) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Mutex is locked by ");
                        sb.append(empty.locked);
                        sb.append(" but expected ");
                        sb.append(owner);
                        throw new IllegalStateException(sb.toString().toString());
                    }
                }
                if (MutexImpl._state$FU.compareAndSet(this, state, MutexKt.access$getEMPTY_UNLOCKED$p())) {
                    return;
                }
                continue;
            }
            else if (state instanceof OpDescriptor) {
                ((OpDescriptor)state).perform(this);
            }
            else {
                if (!(state instanceof LockedQueue)) {
                    owner = new StringBuilder();
                    ((StringBuilder)owner).append("Illegal state ");
                    ((StringBuilder)owner).append(state);
                    throw new IllegalStateException(((StringBuilder)owner).toString().toString());
                }
                if (owner != null) {
                    final LockedQueue lockedQueue = (LockedQueue)state;
                    int n4;
                    if (lockedQueue.owner == owner) {
                        n4 = n2;
                    }
                    else {
                        n4 = 0;
                    }
                    if (n4 == 0) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Mutex is locked by ");
                        sb2.append(lockedQueue.owner);
                        sb2.append(" but expected ");
                        sb2.append(owner);
                        throw new IllegalStateException(sb2.toString().toString());
                    }
                }
                final LockedQueue lockedQueue2 = (LockedQueue)state;
                final LockFreeLinkedListNode removeFirstOrNull = lockedQueue2.removeFirstOrNull();
                if (removeFirstOrNull == null) {
                    final UnlockOp unlockOp = new UnlockOp(lockedQueue2);
                    if (MutexImpl._state$FU.compareAndSet(this, state, unlockOp) && unlockOp.perform(this) == null) {
                        return;
                    }
                    continue;
                }
                else {
                    final LockWaiter lockWaiter = (LockWaiter)removeFirstOrNull;
                    final Object tryResumeLockWaiter = lockWaiter.tryResumeLockWaiter();
                    if (tryResumeLockWaiter != null) {
                        owner = lockWaiter.owner;
                        if (owner == null) {
                            owner = MutexKt.access$getLOCKED$p();
                        }
                        lockedQueue2.owner = owner;
                        lockWaiter.completeResumeLockWaiter(tryResumeLockWaiter);
                        return;
                    }
                    continue;
                }
            }
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B\u001d\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\u0010\u0010\b\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\u0003H\u0016J\b\u0010\n\u001a\u00020\u000bH\u0016J\n\u0010\f\u001a\u0004\u0018\u00010\u0003H\u0016R\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00058\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\r" }, d2 = { "Lkotlinx/coroutines/sync/MutexImpl$LockCont;", "Lkotlinx/coroutines/sync/MutexImpl$LockWaiter;", "owner", "", "cont", "Lkotlinx/coroutines/CancellableContinuation;", "", "(Ljava/lang/Object;Lkotlinx/coroutines/CancellableContinuation;)V", "completeResumeLockWaiter", "token", "toString", "", "tryResumeLockWaiter", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class LockCont extends LockWaiter
    {
        public final CancellableContinuation<Unit> cont;
        
        public LockCont(final Object o, final CancellableContinuation<? super Unit> cont) {
            Intrinsics.checkParameterIsNotNull(cont, "cont");
            super(o);
            this.cont = (CancellableContinuation<Unit>)cont;
        }
        
        @Override
        public void completeResumeLockWaiter(final Object o) {
            Intrinsics.checkParameterIsNotNull(o, "token");
            this.cont.completeResume(o);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("LockCont[");
            sb.append(this.owner);
            sb.append(", ");
            sb.append(this.cont);
            sb.append(']');
            return sb.toString();
        }
        
        @Override
        public Object tryResumeLockWaiter() {
            return CancellableContinuation.DefaultImpls.tryResume$default(this.cont, Unit.INSTANCE, null, 2, null);
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002BL\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\b\u0012\"\u0010\t\u001a\u001e\b\u0001\u0012\u0004\u0012\u00020\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u000b\u0012\u0006\u0012\u0004\u0018\u00010\u00040\n\u00f8\u0001\u0000¢\u0006\u0002\u0010\fJ\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0004H\u0016J\b\u0010\u0011\u001a\u00020\u0012H\u0016J\n\u0010\u0013\u001a\u0004\u0018\u00010\u0004H\u0016R1\u0010\t\u001a\u001e\b\u0001\u0012\u0004\u0012\u00020\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u000b\u0012\u0006\u0012\u0004\u0018\u00010\u00040\n8\u0006X\u0087\u0004\u00f8\u0001\u0000¢\u0006\u0004\n\u0002\u0010\rR\u0010\u0010\u0005\u001a\u00020\u00068\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\b8\u0006X\u0087\u0004¢\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0014" }, d2 = { "Lkotlinx/coroutines/sync/MutexImpl$LockSelect;", "R", "Lkotlinx/coroutines/sync/MutexImpl$LockWaiter;", "owner", "", "mutex", "Lkotlinx/coroutines/sync/Mutex;", "select", "Lkotlinx/coroutines/selects/SelectInstance;", "block", "Lkotlin/Function2;", "Lkotlin/coroutines/Continuation;", "(Ljava/lang/Object;Lkotlinx/coroutines/sync/Mutex;Lkotlinx/coroutines/selects/SelectInstance;Lkotlin/jvm/functions/Function2;)V", "Lkotlin/jvm/functions/Function2;", "completeResumeLockWaiter", "", "token", "toString", "", "tryResumeLockWaiter", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class LockSelect<R> extends LockWaiter
    {
        public final Function2<Mutex, Continuation<? super R>, Object> block;
        public final Mutex mutex;
        public final SelectInstance<R> select;
        
        public LockSelect(final Object o, final Mutex mutex, final SelectInstance<? super R> select, final Function2<? super Mutex, ? super Continuation<? super R>, ?> block) {
            Intrinsics.checkParameterIsNotNull(mutex, "mutex");
            Intrinsics.checkParameterIsNotNull(select, "select");
            Intrinsics.checkParameterIsNotNull(block, "block");
            super(o);
            this.mutex = mutex;
            this.select = (SelectInstance<R>)select;
            this.block = (Function2<Mutex, Continuation<? super R>, Object>)block;
        }
        
        @Override
        public void completeResumeLockWaiter(final Object o) {
            Intrinsics.checkParameterIsNotNull(o, "token");
            if (DebugKt.getASSERTIONS_ENABLED() && o != MutexKt.access$getSELECT_SUCCESS$p()) {
                throw new AssertionError();
            }
            ContinuationKt.startCoroutine((Function2<? super Mutex, ? super Continuation<? super Object>, ?>)this.block, this.mutex, (Continuation<? super Object>)this.select.getCompletion());
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("LockSelect[");
            sb.append(this.owner);
            sb.append(", ");
            sb.append(this.mutex);
            sb.append(", ");
            sb.append(this.select);
            sb.append(']');
            return sb.toString();
        }
        
        @Override
        public Object tryResumeLockWaiter() {
            final SelectInstance<R> select = this.select;
            Object access$getSELECT_SUCCESS$p = null;
            if (select.trySelect(null)) {
                access$getSELECT_SUCCESS$p = MutexKt.access$getSELECT_SUCCESS$p();
            }
            return access$getSELECT_SUCCESS$p;
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\b\"\u0018\u00002\u00020\u00012\u00020\u0002B\u000f\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0004H&J\u0006\u0010\t\u001a\u00020\u0007J\n\u0010\n\u001a\u0004\u0018\u00010\u0004H&R\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b" }, d2 = { "Lkotlinx/coroutines/sync/MutexImpl$LockWaiter;", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "Lkotlinx/coroutines/DisposableHandle;", "owner", "", "(Ljava/lang/Object;)V", "completeResumeLockWaiter", "", "token", "dispose", "tryResumeLockWaiter", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private abstract static class LockWaiter extends LockFreeLinkedListNode implements DisposableHandle
    {
        public final Object owner;
        
        public LockWaiter(final Object owner) {
            this.owner = owner;
        }
        
        public abstract void completeResumeLockWaiter(final Object p0);
        
        @Override
        public final void dispose() {
            this.remove();
        }
        
        public abstract Object tryResumeLockWaiter();
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0006H\u0016R\u0012\u0010\u0002\u001a\u00020\u00038\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000¨\u0006\u0007" }, d2 = { "Lkotlinx/coroutines/sync/MutexImpl$LockedQueue;", "Lkotlinx/coroutines/internal/LockFreeLinkedListHead;", "owner", "", "(Ljava/lang/Object;)V", "toString", "", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class LockedQueue extends LockFreeLinkedListHead
    {
        public Object owner;
        
        public LockedQueue(final Object owner) {
            Intrinsics.checkParameterIsNotNull(owner, "owner");
            this.owner = owner;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("LockedQueue[");
            sb.append(this.owner);
            sb.append(']');
            return sb.toString();
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0002\u0018\u00002\u00020\u0001:\u0001\rB\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u001e\u0010\u0007\u001a\u00020\b2\n\u0010\t\u001a\u0006\u0012\u0002\b\u00030\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0005H\u0016J\u0016\u0010\f\u001a\u0004\u0018\u00010\u00052\n\u0010\t\u001a\u0006\u0012\u0002\b\u00030\nH\u0016R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e" }, d2 = { "Lkotlinx/coroutines/sync/MutexImpl$TryLockDesc;", "Lkotlinx/coroutines/internal/AtomicDesc;", "mutex", "Lkotlinx/coroutines/sync/MutexImpl;", "owner", "", "(Lkotlinx/coroutines/sync/MutexImpl;Ljava/lang/Object;)V", "complete", "", "op", "Lkotlinx/coroutines/internal/AtomicOp;", "failure", "prepare", "PrepareOp", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class TryLockDesc extends AtomicDesc
    {
        public final MutexImpl mutex;
        public final Object owner;
        
        public TryLockDesc(final MutexImpl mutex, final Object owner) {
            Intrinsics.checkParameterIsNotNull(mutex, "mutex");
            this.mutex = mutex;
            this.owner = owner;
        }
        
        @Override
        public void complete(final AtomicOp<?> atomicOp, Object owner) {
            Intrinsics.checkParameterIsNotNull(atomicOp, "op");
            Empty empty;
            if (owner != null) {
                empty = MutexKt.access$getEMPTY_UNLOCKED$p();
            }
            else {
                owner = this.owner;
                if (owner == null) {
                    empty = MutexKt.access$getEMPTY_LOCKED$p();
                }
                else {
                    empty = new Empty(owner);
                }
            }
            MutexImpl._state$FU.compareAndSet(this.mutex, atomicOp, empty);
        }
        
        @Override
        public Object prepare(final AtomicOp<?> atomicOp) {
            Intrinsics.checkParameterIsNotNull(atomicOp, "op");
            final PrepareOp prepareOp = new PrepareOp(atomicOp);
            if (!MutexImpl._state$FU.compareAndSet(this.mutex, MutexKt.access$getEMPTY_UNLOCKED$p(), prepareOp)) {
                return MutexKt.access$getLOCK_FAIL$p();
            }
            return prepareOp.perform(this.mutex);
        }
        
        @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0011\u0012\n\u0010\u0002\u001a\u0006\u0012\u0002\b\u00030\u0003¢\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006H\u0016R\u0012\u0010\u0002\u001a\u0006\u0012\u0002\b\u00030\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\b" }, d2 = { "Lkotlinx/coroutines/sync/MutexImpl$TryLockDesc$PrepareOp;", "Lkotlinx/coroutines/internal/OpDescriptor;", "op", "Lkotlinx/coroutines/internal/AtomicOp;", "(Lkotlinx/coroutines/sync/MutexImpl$TryLockDesc;Lkotlinx/coroutines/internal/AtomicOp;)V", "perform", "", "affected", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
        private final class PrepareOp extends OpDescriptor
        {
            private final AtomicOp<?> op;
            
            public PrepareOp(final AtomicOp<?> op) {
                Intrinsics.checkParameterIsNotNull(op, "op");
                this.op = op;
            }
            
            @Override
            public Object perform(final Object o) {
                Object o2;
                if (this.op.isDecided()) {
                    o2 = MutexKt.access$getEMPTY_UNLOCKED$p();
                }
                else {
                    o2 = this.op;
                }
                if (o != null) {
                    MutexImpl._state$FU.compareAndSet(o, this, o2);
                    return null;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.sync.MutexImpl");
            }
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006H\u0016R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\b" }, d2 = { "Lkotlinx/coroutines/sync/MutexImpl$UnlockOp;", "Lkotlinx/coroutines/internal/OpDescriptor;", "queue", "Lkotlinx/coroutines/sync/MutexImpl$LockedQueue;", "(Lkotlinx/coroutines/sync/MutexImpl$LockedQueue;)V", "perform", "", "affected", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class UnlockOp extends OpDescriptor
    {
        public final LockedQueue queue;
        
        public UnlockOp(final LockedQueue queue) {
            Intrinsics.checkParameterIsNotNull(queue, "queue");
            this.queue = queue;
        }
        
        @Override
        public Object perform(final Object o) {
            Object o2;
            if (this.queue.isEmpty()) {
                o2 = MutexKt.access$getEMPTY_UNLOCKED$p();
            }
            else {
                o2 = this.queue;
            }
            if (o == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.sync.MutexImpl");
            }
            final MutexImpl mutexImpl = (MutexImpl)o;
            MutexImpl._state$FU.compareAndSet(mutexImpl, this, o2);
            if (mutexImpl._state == this.queue) {
                return MutexKt.access$getUNLOCK_FAIL$p();
            }
            return null;
        }
    }
}
