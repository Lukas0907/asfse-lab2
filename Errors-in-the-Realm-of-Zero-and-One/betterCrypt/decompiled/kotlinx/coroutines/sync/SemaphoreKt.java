// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.sync;

import kotlin.jvm.internal.InlineMarker;
import kotlin.Unit;
import kotlin.ResultKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function0;
import kotlinx.coroutines.internal.SystemPropsKt;
import kotlinx.coroutines.internal.Symbol;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u00072\b\b\u0002\u0010\f\u001a\u00020\u0007\u001a)\u0010\r\u001a\u0002H\u000e\"\u0004\b\u0000\u0010\u000e*\u00020\n2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\u000e0\u0010H\u0086H\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0011\"\u0016\u0010\u0000\u001a\u00020\u00018\u0002X\u0083\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u0002\u0010\u0003\"\u0016\u0010\u0004\u001a\u00020\u00018\u0002X\u0083\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u0005\u0010\u0003\"\u0016\u0010\u0006\u001a\u00020\u00078\u0002X\u0083\u0004¢\u0006\b\n\u0000\u0012\u0004\b\b\u0010\u0003\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0012" }, d2 = { "CANCELLED", "Lkotlinx/coroutines/internal/Symbol;", "CANCELLED$annotations", "()V", "RESUMED", "RESUMED$annotations", "SEGMENT_SIZE", "", "SEGMENT_SIZE$annotations", "Semaphore", "Lkotlinx/coroutines/sync/Semaphore;", "permits", "acquiredPermits", "withPermit", "T", "action", "Lkotlin/Function0;", "(Lkotlinx/coroutines/sync/Semaphore;Lkotlin/jvm/functions/Function0;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class SemaphoreKt
{
    private static final Symbol CANCELLED;
    private static final Symbol RESUMED;
    private static final int SEGMENT_SIZE;
    
    static {
        RESUMED = new Symbol("RESUMED");
        CANCELLED = new Symbol("CANCELLED");
        SEGMENT_SIZE = SystemPropsKt.systemProp$default("kotlinx.coroutines.semaphore.segmentSize", 16, 0, 0, 12, null);
    }
    
    public static final Semaphore Semaphore(final int n, final int n2) {
        return new SemaphoreImpl(n, n2);
    }
    
    public static final <T> Object withPermit(final Semaphore l$0, Function0<? extends T> l$2, Continuation<? super T> semaphore) {
        SemaphoreKt$withPermit.SemaphoreKt$withPermit$1 semaphoreKt$withPermit$1 = null;
        Label_0050: {
            if (semaphore instanceof SemaphoreKt$withPermit.SemaphoreKt$withPermit$1) {
                semaphoreKt$withPermit$1 = (SemaphoreKt$withPermit.SemaphoreKt$withPermit$1)semaphore;
                if ((semaphoreKt$withPermit$1.label & Integer.MIN_VALUE) != 0x0) {
                    semaphoreKt$withPermit$1.label += Integer.MIN_VALUE;
                    break Label_0050;
                }
            }
            semaphoreKt$withPermit$1 = new SemaphoreKt$withPermit.SemaphoreKt$withPermit$1((Continuation)semaphore);
        }
        final Object result = semaphoreKt$withPermit$1.result;
        final Object coroutine_SUSPENDED = IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
        final int label = semaphoreKt$withPermit$1.label;
        if (label != 0) {
            if (label != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            l$2 = (Function0)semaphoreKt$withPermit$1.L$1;
            semaphore = (Semaphore)semaphoreKt$withPermit$1.L$0;
            ResultKt.throwOnFailure(result);
        }
        else {
            ResultKt.throwOnFailure(result);
            semaphoreKt$withPermit$1.L$0 = l$0;
            semaphoreKt$withPermit$1.L$1 = l$2;
            semaphoreKt$withPermit$1.label = 1;
            semaphore = l$0;
            if (l$0.acquire((Continuation<? super Unit>)semaphoreKt$withPermit$1) == coroutine_SUSPENDED) {
                return coroutine_SUSPENDED;
            }
        }
        try {
            return l$2.invoke();
        }
        finally {
            InlineMarker.finallyStart(1);
            semaphore.release();
            InlineMarker.finallyEnd(1);
        }
    }
    
    private static final Object withPermit$$forInline(final Semaphore semaphore, final Function0 function0, final Continuation continuation) {
        InlineMarker.mark(0);
        semaphore.acquire(continuation);
        InlineMarker.mark(2);
        InlineMarker.mark(1);
        try {
            return function0.invoke();
        }
        finally {
            InlineMarker.finallyStart(1);
            semaphore.release();
            InlineMarker.finallyEnd(1);
        }
    }
}
