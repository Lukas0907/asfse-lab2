// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.jvm.internal.Intrinsics;
import java.util.concurrent.Executor;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007" }, d2 = { "Lkotlinx/coroutines/ExecutorCoroutineDispatcherImpl;", "Lkotlinx/coroutines/ExecutorCoroutineDispatcherBase;", "executor", "Ljava/util/concurrent/Executor;", "(Ljava/util/concurrent/Executor;)V", "getExecutor", "()Ljava/util/concurrent/Executor;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
final class ExecutorCoroutineDispatcherImpl extends ExecutorCoroutineDispatcherBase
{
    private final Executor executor;
    
    public ExecutorCoroutineDispatcherImpl(final Executor executor) {
        Intrinsics.checkParameterIsNotNull(executor, "executor");
        this.executor = executor;
        this.initFutureCancellation$kotlinx_coroutines_core();
    }
    
    @Override
    public Executor getExecutor() {
        return this.executor;
    }
}
