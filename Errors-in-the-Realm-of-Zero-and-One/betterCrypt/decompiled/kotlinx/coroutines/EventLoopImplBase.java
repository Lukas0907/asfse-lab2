// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlinx.coroutines.internal.Symbol;
import kotlinx.coroutines.internal.ThreadSafeHeap;
import kotlinx.coroutines.internal.ThreadSafeHeapNode;
import kotlin.coroutines.CoroutineContext;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.internal.Intrinsics;
import kotlin.TypeCastException;
import kotlinx.coroutines.internal.LockFreeTaskQueueCore;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0011\b \u0018\u00002\u00020\u00012\u00020\u0002:\u00044567B\u0007¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0006\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0006\u0010\u0004J\u0017\u0010\t\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\bH\u0002¢\u0006\u0004\b\t\u0010\nJ!\u0010\u000e\u001a\u00020\u00052\u0006\u0010\f\u001a\u00020\u000b2\n\u0010\r\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b\u000e\u0010\u000fJ\u0019\u0010\u0011\u001a\u00020\u00052\n\u0010\u0010\u001a\u00060\u0007j\u0002`\b¢\u0006\u0004\b\u0011\u0010\u0012J\u001b\u0010\u0014\u001a\u00020\u00132\n\u0010\u0010\u001a\u00060\u0007j\u0002`\bH\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0017\u001a\u00020\u0016H\u0016¢\u0006\u0004\b\u0017\u0010\u0018J\u000f\u0010\u0019\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0019\u0010\u0004J\u000f\u0010\u001a\u001a\u00020\u0005H\u0004¢\u0006\u0004\b\u001a\u0010\u0004J\u001d\u0010\u001e\u001a\u00020\u00052\u0006\u0010\u001b\u001a\u00020\u00162\u0006\u0010\u001d\u001a\u00020\u001c¢\u0006\u0004\b\u001e\u0010\u001fJ\u001f\u0010!\u001a\u00020 2\u0006\u0010\u001b\u001a\u00020\u00162\u0006\u0010\u001d\u001a\u00020\u001cH\u0002¢\u0006\u0004\b!\u0010\"J#\u0010%\u001a\u00020$2\u0006\u0010#\u001a\u00020\u00162\n\u0010\r\u001a\u00060\u0007j\u0002`\bH\u0004¢\u0006\u0004\b%\u0010&J%\u0010)\u001a\u00020\u00052\u0006\u0010#\u001a\u00020\u00162\f\u0010(\u001a\b\u0012\u0004\u0012\u00020\u00050'H\u0016¢\u0006\u0004\b)\u0010*J\u0017\u0010+\u001a\u00020\u00132\u0006\u0010\u0010\u001a\u00020\u001cH\u0002¢\u0006\u0004\b+\u0010,J\u000f\u0010-\u001a\u00020\u0005H\u0014¢\u0006\u0004\b-\u0010\u0004R\u0016\u0010.\u001a\u00020\u00138\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b.\u0010/R\u0016\u00100\u001a\u00020\u00138T@\u0014X\u0094\u0004¢\u0006\u0006\u001a\u0004\b0\u00101R\u0016\u00103\u001a\u00020\u00168T@\u0014X\u0094\u0004¢\u0006\u0006\u001a\u0004\b2\u0010\u0018¨\u00068" }, d2 = { "Lkotlinx/coroutines/EventLoopImplBase;", "Lkotlinx/coroutines/EventLoopImplPlatform;", "Lkotlinx/coroutines/Delay;", "<init>", "()V", "", "closeQueue", "Ljava/lang/Runnable;", "Lkotlinx/coroutines/Runnable;", "dequeue", "()Ljava/lang/Runnable;", "Lkotlin/coroutines/CoroutineContext;", "context", "block", "dispatch", "(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Runnable;)V", "task", "enqueue", "(Ljava/lang/Runnable;)V", "", "enqueueImpl", "(Ljava/lang/Runnable;)Z", "", "processNextEvent", "()J", "rescheduleAllDelayed", "resetAll", "now", "Lkotlinx/coroutines/EventLoopImplBase$DelayedTask;", "delayedTask", "schedule", "(JLkotlinx/coroutines/EventLoopImplBase$DelayedTask;)V", "", "scheduleImpl", "(JLkotlinx/coroutines/EventLoopImplBase$DelayedTask;)I", "timeMillis", "Lkotlinx/coroutines/DisposableHandle;", "scheduleInvokeOnTimeout", "(JLjava/lang/Runnable;)Lkotlinx/coroutines/DisposableHandle;", "Lkotlinx/coroutines/CancellableContinuation;", "continuation", "scheduleResumeAfterDelay", "(JLkotlinx/coroutines/CancellableContinuation;)V", "shouldUnpark", "(Lkotlinx/coroutines/EventLoopImplBase$DelayedTask;)Z", "shutdown", "isCompleted", "Z", "isEmpty", "()Z", "getNextTime", "nextTime", "DelayedResumeTask", "DelayedRunnableTask", "DelayedTask", "DelayedTaskQueue", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public abstract class EventLoopImplBase extends EventLoopImplPlatform implements Delay
{
    private static final AtomicReferenceFieldUpdater _delayed$FU;
    private static final AtomicReferenceFieldUpdater _queue$FU;
    private volatile Object _delayed;
    private volatile Object _queue;
    private volatile boolean isCompleted;
    
    static {
        _queue$FU = AtomicReferenceFieldUpdater.newUpdater(EventLoopImplBase.class, Object.class, "_queue");
        _delayed$FU = AtomicReferenceFieldUpdater.newUpdater(EventLoopImplBase.class, Object.class, "_delayed");
    }
    
    public EventLoopImplBase() {
        this._queue = null;
        this._delayed = null;
    }
    
    public static final /* synthetic */ boolean access$isCompleted$p(final EventLoopImplBase eventLoopImplBase) {
        return eventLoopImplBase.isCompleted;
    }
    
    private final void closeQueue() {
        if (DebugKt.getASSERTIONS_ENABLED()) {
            if (!this.isCompleted) {
                throw new AssertionError();
            }
        }
        while (true) {
            final Object queue = this._queue;
            if (queue == null) {
                if (EventLoopImplBase._queue$FU.compareAndSet(this, null, EventLoop_commonKt.access$getCLOSED_EMPTY$p())) {
                    return;
                }
                continue;
            }
            else {
                if (queue instanceof LockFreeTaskQueueCore) {
                    ((LockFreeTaskQueueCore)queue).close();
                    return;
                }
                if (queue == EventLoop_commonKt.access$getCLOSED_EMPTY$p()) {
                    return;
                }
                final LockFreeTaskQueueCore<Runnable> lockFreeTaskQueueCore = new LockFreeTaskQueueCore<Runnable>(8, true);
                if (queue == null) {
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
                }
                lockFreeTaskQueueCore.addLast((Runnable)queue);
                if (EventLoopImplBase._queue$FU.compareAndSet(this, queue, lockFreeTaskQueueCore)) {
                    return;
                }
                continue;
            }
        }
    }
    
    private final Runnable dequeue() {
        while (true) {
            final Object queue = this._queue;
            if (queue == null) {
                return null;
            }
            if (queue instanceof LockFreeTaskQueueCore) {
                if (queue == null) {
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.Queue<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> /* = kotlinx.coroutines.internal.LockFreeTaskQueueCore<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> */");
                }
                final LockFreeTaskQueueCore lockFreeTaskQueueCore = (LockFreeTaskQueueCore)queue;
                final Object removeFirstOrNull = lockFreeTaskQueueCore.removeFirstOrNull();
                if (removeFirstOrNull != LockFreeTaskQueueCore.REMOVE_FROZEN) {
                    return (Runnable)removeFirstOrNull;
                }
                EventLoopImplBase._queue$FU.compareAndSet(this, queue, lockFreeTaskQueueCore.next());
            }
            else {
                if (queue == EventLoop_commonKt.access$getCLOSED_EMPTY$p()) {
                    return null;
                }
                if (!EventLoopImplBase._queue$FU.compareAndSet(this, queue, null)) {
                    continue;
                }
                if (queue != null) {
                    return (Runnable)queue;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
            }
        }
    }
    
    private final boolean enqueueImpl(final Runnable runnable) {
        while (true) {
            final Object queue = this._queue;
            if (this.isCompleted) {
                return false;
            }
            if (queue == null) {
                if (EventLoopImplBase._queue$FU.compareAndSet(this, null, runnable)) {
                    return true;
                }
                continue;
            }
            else if (queue instanceof LockFreeTaskQueueCore) {
                if (queue == null) {
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.Queue<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> /* = kotlinx.coroutines.internal.LockFreeTaskQueueCore<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> */");
                }
                final LockFreeTaskQueueCore<Runnable> lockFreeTaskQueueCore = (LockFreeTaskQueueCore<Runnable>)queue;
                final int addLast = lockFreeTaskQueueCore.addLast(runnable);
                if (addLast == 0) {
                    return true;
                }
                if (addLast != 1) {
                    if (addLast != 2) {
                        continue;
                    }
                    return false;
                }
                else {
                    EventLoopImplBase._queue$FU.compareAndSet(this, queue, lockFreeTaskQueueCore.next());
                }
            }
            else {
                if (queue == EventLoop_commonKt.access$getCLOSED_EMPTY$p()) {
                    return false;
                }
                final LockFreeTaskQueueCore<Runnable> lockFreeTaskQueueCore2 = new LockFreeTaskQueueCore<Runnable>(8, true);
                if (queue == null) {
                    throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
                }
                lockFreeTaskQueueCore2.addLast((Runnable)queue);
                lockFreeTaskQueueCore2.addLast(runnable);
                if (EventLoopImplBase._queue$FU.compareAndSet(this, queue, lockFreeTaskQueueCore2)) {
                    return true;
                }
                continue;
            }
        }
    }
    
    private final void rescheduleAllDelayed() {
        final TimeSource timeSource = TimeSourceKt.getTimeSource();
        long n;
        if (timeSource != null) {
            n = timeSource.nanoTime();
        }
        else {
            n = System.nanoTime();
        }
        while (true) {
            final DelayedTaskQueue delayedTaskQueue = (DelayedTaskQueue)this._delayed;
            if (delayedTaskQueue == null) {
                break;
            }
            final DelayedTask delayedTask = delayedTaskQueue.removeFirstOrNull();
            if (delayedTask == null) {
                break;
            }
            this.reschedule(n, delayedTask);
        }
    }
    
    private final int scheduleImpl(final long n, final DelayedTask delayedTask) {
        if (this.isCompleted) {
            return 1;
        }
        DelayedTaskQueue delayedTaskQueue = (DelayedTaskQueue)this._delayed;
        if (delayedTaskQueue == null) {
            final EventLoopImplBase eventLoopImplBase = this;
            EventLoopImplBase._delayed$FU.compareAndSet(eventLoopImplBase, null, new DelayedTaskQueue(n));
            final Object delayed = eventLoopImplBase._delayed;
            if (delayed == null) {
                Intrinsics.throwNpe();
            }
            delayedTaskQueue = (DelayedTaskQueue)delayed;
        }
        return delayedTask.scheduleTask(n, delayedTaskQueue, this);
    }
    
    private final boolean shouldUnpark(final DelayedTask delayedTask) {
        final DelayedTaskQueue delayedTaskQueue = (DelayedTaskQueue)this._delayed;
        DelayedTask delayedTask2;
        if (delayedTaskQueue != null) {
            delayedTask2 = delayedTaskQueue.peek();
        }
        else {
            delayedTask2 = null;
        }
        return delayedTask2 == delayedTask;
    }
    
    @Override
    public Object delay(final long n, final Continuation<? super Unit> continuation) {
        return Delay.DefaultImpls.delay(this, n, continuation);
    }
    
    @Override
    public final void dispatch(final CoroutineContext coroutineContext, final Runnable runnable) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        Intrinsics.checkParameterIsNotNull(runnable, "block");
        this.enqueue(runnable);
    }
    
    public final void enqueue(final Runnable runnable) {
        Intrinsics.checkParameterIsNotNull(runnable, "task");
        if (this.enqueueImpl(runnable)) {
            this.unpark();
            return;
        }
        DefaultExecutor.INSTANCE.enqueue(runnable);
    }
    
    @Override
    protected long getNextTime() {
        if (super.getNextTime() == 0L) {
            return 0L;
        }
        final Object queue = this._queue;
        if (queue != null) {
            if (queue instanceof LockFreeTaskQueueCore) {
                if (!((LockFreeTaskQueueCore)queue).isEmpty()) {
                    return 0L;
                }
            }
            else {
                if (queue == EventLoop_commonKt.access$getCLOSED_EMPTY$p()) {
                    return Long.MAX_VALUE;
                }
                return 0L;
            }
        }
        final DelayedTaskQueue delayedTaskQueue = (DelayedTaskQueue)this._delayed;
        if (delayedTaskQueue != null) {
            final DelayedTask delayedTask = delayedTaskQueue.peek();
            if (delayedTask != null) {
                final long nanoTime = delayedTask.nanoTime;
                final TimeSource timeSource = TimeSourceKt.getTimeSource();
                long n;
                if (timeSource != null) {
                    n = timeSource.nanoTime();
                }
                else {
                    n = System.nanoTime();
                }
                return RangesKt___RangesKt.coerceAtLeast(nanoTime - n, 0L);
            }
        }
        return Long.MAX_VALUE;
    }
    
    @Override
    public DisposableHandle invokeOnTimeout(final long n, final Runnable runnable) {
        Intrinsics.checkParameterIsNotNull(runnable, "block");
        return Delay.DefaultImpls.invokeOnTimeout(this, n, runnable);
    }
    
    @Override
    protected boolean isEmpty() {
        if (!this.isUnconfinedQueueEmpty()) {
            return false;
        }
        final DelayedTaskQueue delayedTaskQueue = (DelayedTaskQueue)this._delayed;
        if (delayedTaskQueue != null && !delayedTaskQueue.isEmpty()) {
            return false;
        }
        final Object queue = this._queue;
        if (queue != null) {
            if (queue instanceof LockFreeTaskQueueCore) {
                return ((LockFreeTaskQueueCore)queue).isEmpty();
            }
            if (queue != EventLoop_commonKt.access$getCLOSED_EMPTY$p()) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public long processNextEvent() {
        if (this.processUnconfinedEvent()) {
            return this.getNextTime();
        }
        final DelayedTaskQueue delayedTaskQueue = (DelayedTaskQueue)this._delayed;
        if (delayedTaskQueue != null && !delayedTaskQueue.isEmpty()) {
            final TimeSource timeSource = TimeSourceKt.getTimeSource();
            long n;
            if (timeSource != null) {
                n = timeSource.nanoTime();
            }
            else {
                n = System.nanoTime();
            }
        Label_0109_Outer:
            while (true) {
                while (true) {
                    Label_0176: {
                        synchronized (delayedTaskQueue) {
                            final DelayedTask firstImpl = delayedTaskQueue.firstImpl();
                            final DelayedTask delayedTask = null;
                            Runnable removeAtImpl = null;
                            if (firstImpl != null) {
                                final DelayedTask delayedTask2 = firstImpl;
                                if (!delayedTask2.timeToExecute(n)) {
                                    break Label_0176;
                                }
                                final int enqueueImpl = this.enqueueImpl(delayedTask2) ? 1 : 0;
                                if (enqueueImpl != 0) {
                                    removeAtImpl = ((ThreadSafeHeap<Runnable>)delayedTaskQueue).removeAtImpl(0);
                                }
                            }
                            // monitorexit(delayedTaskQueue)
                            else {
                                // monitorexit(delayedTaskQueue)
                                removeAtImpl = delayedTask;
                            }
                            if (removeAtImpl != null) {
                                continue Label_0109_Outer;
                            }
                        }
                        break;
                    }
                    final int enqueueImpl = 0;
                    continue;
                }
            }
        }
        final Runnable dequeue = this.dequeue();
        if (dequeue != null) {
            dequeue.run();
        }
        return this.getNextTime();
    }
    
    protected final void resetAll() {
        this._queue = null;
        this._delayed = null;
    }
    
    public final void schedule(final long n, final DelayedTask delayedTask) {
        Intrinsics.checkParameterIsNotNull(delayedTask, "delayedTask");
        final int scheduleImpl = this.scheduleImpl(n, delayedTask);
        if (scheduleImpl == 0) {
            if (this.shouldUnpark(delayedTask)) {
                this.unpark();
            }
            return;
        }
        if (scheduleImpl == 1) {
            this.reschedule(n, delayedTask);
            return;
        }
        if (scheduleImpl == 2) {
            return;
        }
        throw new IllegalStateException("unexpected result".toString());
    }
    
    protected final DisposableHandle scheduleInvokeOnTimeout(long n, final Runnable runnable) {
        Intrinsics.checkParameterIsNotNull(runnable, "block");
        final long delayToNanos = EventLoop_commonKt.delayToNanos(n);
        if (delayToNanos < 4611686018427387903L) {
            final TimeSource timeSource = TimeSourceKt.getTimeSource();
            if (timeSource != null) {
                n = timeSource.nanoTime();
            }
            else {
                n = System.nanoTime();
            }
            final DelayedRunnableTask delayedRunnableTask = new DelayedRunnableTask(delayToNanos + n, runnable);
            this.schedule(n, (DelayedTask)delayedRunnableTask);
            return delayedRunnableTask;
        }
        return NonDisposableHandle.INSTANCE;
    }
    
    @Override
    public void scheduleResumeAfterDelay(long n, final CancellableContinuation<? super Unit> cancellableContinuation) {
        Intrinsics.checkParameterIsNotNull(cancellableContinuation, "continuation");
        final long delayToNanos = EventLoop_commonKt.delayToNanos(n);
        if (delayToNanos < 4611686018427387903L) {
            final TimeSource timeSource = TimeSourceKt.getTimeSource();
            if (timeSource != null) {
                n = timeSource.nanoTime();
            }
            else {
                n = System.nanoTime();
            }
            final DelayedResumeTask delayedResumeTask = new DelayedResumeTask(delayToNanos + n, cancellableContinuation);
            CancellableContinuationKt.disposeOnCancellation(cancellableContinuation, delayedResumeTask);
            this.schedule(n, (DelayedTask)delayedResumeTask);
        }
    }
    
    @Override
    protected void shutdown() {
        ThreadLocalEventLoop.INSTANCE.resetEventLoop$kotlinx_coroutines_core();
        this.isCompleted = true;
        this.closeQueue();
        while (this.processNextEvent() <= 0L) {}
        this.rescheduleAllDelayed();
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\u0082\u0004\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\b\u0010\b\u001a\u00020\u0006H\u0016J\b\u0010\t\u001a\u00020\nH\u0016R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b" }, d2 = { "Lkotlinx/coroutines/EventLoopImplBase$DelayedResumeTask;", "Lkotlinx/coroutines/EventLoopImplBase$DelayedTask;", "nanoTime", "", "cont", "Lkotlinx/coroutines/CancellableContinuation;", "", "(Lkotlinx/coroutines/EventLoopImplBase;JLkotlinx/coroutines/CancellableContinuation;)V", "run", "toString", "", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private final class DelayedResumeTask extends DelayedTask
    {
        private final CancellableContinuation<Unit> cont;
        
        public DelayedResumeTask(final long n, final CancellableContinuation<? super Unit> cont) {
            Intrinsics.checkParameterIsNotNull(cont, "cont");
            super(n);
            this.cont = (CancellableContinuation<Unit>)cont;
        }
        
        @Override
        public void run() {
            this.cont.resumeUndispatched(EventLoopImplBase.this, Unit.INSTANCE);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(super.toString());
            sb.append(this.cont.toString());
            return sb.toString();
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0010\u0004\u001a\u00060\u0005j\u0002`\u0006¢\u0006\u0002\u0010\u0007J\b\u0010\b\u001a\u00020\tH\u0016J\b\u0010\n\u001a\u00020\u000bH\u0016R\u0012\u0010\u0004\u001a\u00060\u0005j\u0002`\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\f" }, d2 = { "Lkotlinx/coroutines/EventLoopImplBase$DelayedRunnableTask;", "Lkotlinx/coroutines/EventLoopImplBase$DelayedTask;", "nanoTime", "", "block", "Ljava/lang/Runnable;", "Lkotlinx/coroutines/Runnable;", "(JLjava/lang/Runnable;)V", "run", "", "toString", "", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class DelayedRunnableTask extends DelayedTask
    {
        private final Runnable block;
        
        public DelayedRunnableTask(final long n, final Runnable block) {
            Intrinsics.checkParameterIsNotNull(block, "block");
            super(n);
            this.block = block;
        }
        
        @Override
        public void run() {
            this.block.run();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(super.toString());
            sb.append(this.block.toString());
            return sb.toString();
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\b \u0018\u00002\u00060\u0001j\u0002`\u00022\b\u0012\u0004\u0012\u00020\u00000\u00032\u00020\u00042\u00020\u0005B\r\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0011\u0010\u0018\u001a\u00020\u00132\u0006\u0010\u0019\u001a\u00020\u0000H\u0096\u0002J\u0006\u0010\u001a\u001a\u00020\u001bJ\u001e\u0010\u001c\u001a\u00020\u00132\u0006\u0010\u001d\u001a\u00020\u00072\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!J\u000e\u0010\"\u001a\u00020#2\u0006\u0010\u001d\u001a\u00020\u0007J\b\u0010$\u001a\u00020%H\u0016R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e¢\u0006\u0002\n\u0000R0\u0010\r\u001a\b\u0012\u0002\b\u0003\u0018\u00010\f2\f\u0010\u000b\u001a\b\u0012\u0002\b\u0003\u0018\u00010\f8V@VX\u0096\u000e¢\u0006\f\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001a\u0010\u0012\u001a\u00020\u0013X\u0096\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u0012\u0010\u0006\u001a\u00020\u00078\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000¨\u0006&" }, d2 = { "Lkotlinx/coroutines/EventLoopImplBase$DelayedTask;", "Ljava/lang/Runnable;", "Lkotlinx/coroutines/Runnable;", "", "Lkotlinx/coroutines/DisposableHandle;", "Lkotlinx/coroutines/internal/ThreadSafeHeapNode;", "nanoTime", "", "(J)V", "_heap", "", "value", "Lkotlinx/coroutines/internal/ThreadSafeHeap;", "heap", "getHeap", "()Lkotlinx/coroutines/internal/ThreadSafeHeap;", "setHeap", "(Lkotlinx/coroutines/internal/ThreadSafeHeap;)V", "index", "", "getIndex", "()I", "setIndex", "(I)V", "compareTo", "other", "dispose", "", "scheduleTask", "now", "delayed", "Lkotlinx/coroutines/EventLoopImplBase$DelayedTaskQueue;", "eventLoop", "Lkotlinx/coroutines/EventLoopImplBase;", "timeToExecute", "", "toString", "", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    public abstract static class DelayedTask implements Runnable, Comparable<DelayedTask>, DisposableHandle, ThreadSafeHeapNode
    {
        private Object _heap;
        private int index;
        public long nanoTime;
        
        public DelayedTask(final long nanoTime) {
            this.nanoTime = nanoTime;
            this.index = -1;
        }
        
        @Override
        public int compareTo(final DelayedTask delayedTask) {
            Intrinsics.checkParameterIsNotNull(delayedTask, "other");
            final long n = lcmp(this.nanoTime - delayedTask.nanoTime, 0L);
            if (n > 0) {
                return 1;
            }
            if (n < 0) {
                return -1;
            }
            return 0;
        }
        
        @Override
        public final void dispose() {
            synchronized (this) {
                final Object heap = this._heap;
                if (heap == EventLoop_commonKt.access$getDISPOSED_TASK$p()) {
                    return;
                }
                Object o = heap;
                if (!(heap instanceof DelayedTaskQueue)) {
                    o = null;
                }
                final DelayedTaskQueue delayedTaskQueue = (DelayedTaskQueue)o;
                if (delayedTaskQueue != null) {
                    delayedTaskQueue.remove((DelayedTask)this);
                }
                this._heap = EventLoop_commonKt.access$getDISPOSED_TASK$p();
            }
        }
        
        @Override
        public ThreadSafeHeap<?> getHeap() {
            Object heap;
            if (!((heap = this._heap) instanceof ThreadSafeHeap)) {
                heap = null;
            }
            return (ThreadSafeHeap<?>)heap;
        }
        
        @Override
        public int getIndex() {
            return this.index;
        }
        
        public final int scheduleTask(long n, final DelayedTaskQueue delayedTaskQueue, final EventLoopImplBase eventLoopImplBase) {
            while (true) {
                while (true) {
                    long nanoTime = 0L;
                    Label_0166: {
                        synchronized (this) {
                            Intrinsics.checkParameterIsNotNull(delayedTaskQueue, "delayed");
                            Intrinsics.checkParameterIsNotNull(eventLoopImplBase, "eventLoop");
                            Object heap = this._heap;
                            if (heap == EventLoop_commonKt.access$getDISPOSED_TASK$p()) {
                                return 2;
                            }
                            heap = this;
                            synchronized (delayedTaskQueue) {
                                final DelayedTask delayedTask = delayedTaskQueue.firstImpl();
                                if (EventLoopImplBase.access$isCompleted$p(eventLoopImplBase)) {
                                    return 1;
                                }
                                if (delayedTask == null) {
                                    delayedTaskQueue.timeNow = n;
                                }
                                else {
                                    nanoTime = delayedTask.nanoTime;
                                    if (nanoTime - n < 0L) {
                                        break Label_0166;
                                    }
                                    if (n - delayedTaskQueue.timeNow > 0L) {
                                        delayedTaskQueue.timeNow = n;
                                    }
                                }
                                if (this.nanoTime - delayedTaskQueue.timeNow < 0L) {
                                    this.nanoTime = delayedTaskQueue.timeNow;
                                }
                                ((ThreadSafeHeap<Symbol>)delayedTaskQueue).addImpl((Symbol)heap);
                                return 0;
                            }
                        }
                    }
                    n = nanoTime;
                    continue;
                }
            }
        }
        
        @Override
        public void setHeap(final ThreadSafeHeap<?> heap) {
            if (this._heap != EventLoop_commonKt.access$getDISPOSED_TASK$p()) {
                this._heap = heap;
                return;
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }
        
        @Override
        public void setIndex(final int index) {
            this.index = index;
        }
        
        public final boolean timeToExecute(final long n) {
            return n - this.nanoTime >= 0L;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Delayed[nanos=");
            sb.append(this.nanoTime);
            sb.append(']');
            return sb.toString();
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\b\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005R\u0012\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000¨\u0006\u0006" }, d2 = { "Lkotlinx/coroutines/EventLoopImplBase$DelayedTaskQueue;", "Lkotlinx/coroutines/internal/ThreadSafeHeap;", "Lkotlinx/coroutines/EventLoopImplBase$DelayedTask;", "timeNow", "", "(J)V", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    public static final class DelayedTaskQueue extends ThreadSafeHeap<DelayedTask>
    {
        public long timeNow;
        
        public DelayedTaskQueue(final long timeNow) {
            this.timeNow = timeNow;
        }
    }
}
