// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.selects.SelectInstance;
import kotlin.ResultKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;
import kotlinx.coroutines.selects.SelectClause1;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000@\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\b\u0012\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u00032\b\u0012\u0004\u0012\u0002H\u00010\u0004B\u0015\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\u0011\u0010\r\u001a\u00028\u0000H\u0096@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000eJ\r\u0010\u000f\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0010JH\u0010\u0011\u001a\u00020\u0012\"\u0004\b\u0001\u0010\u00132\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u0002H\u00130\u00152\"\u0010\u0016\u001a\u001e\b\u0001\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00130\u0018\u0012\u0006\u0012\u0004\u0018\u00010\u00190\u0017H\u0016\u00f8\u0001\u0000¢\u0006\u0002\u0010\u001aR\u001a\u0010\n\u001a\b\u0012\u0004\u0012\u00028\u00000\u00048VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\f\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u001b" }, d2 = { "Lkotlinx/coroutines/DeferredCoroutine;", "T", "Lkotlinx/coroutines/AbstractCoroutine;", "Lkotlinx/coroutines/Deferred;", "Lkotlinx/coroutines/selects/SelectClause1;", "parentContext", "Lkotlin/coroutines/CoroutineContext;", "active", "", "(Lkotlin/coroutines/CoroutineContext;Z)V", "onAwait", "getOnAwait", "()Lkotlinx/coroutines/selects/SelectClause1;", "await", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getCompleted", "()Ljava/lang/Object;", "registerSelectClause1", "", "R", "select", "Lkotlinx/coroutines/selects/SelectInstance;", "block", "Lkotlin/Function2;", "Lkotlin/coroutines/Continuation;", "", "(Lkotlinx/coroutines/selects/SelectInstance;Lkotlin/jvm/functions/Function2;)V", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
class DeferredCoroutine<T> extends AbstractCoroutine<T> implements Deferred<T>, SelectClause1<T>
{
    public DeferredCoroutine(final CoroutineContext coroutineContext, final boolean b) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "parentContext");
        super(coroutineContext, b);
    }
    
    static /* synthetic */ Object await$suspendImpl(DeferredCoroutine l$0, final Continuation continuation) {
        DeferredCoroutine$await.DeferredCoroutine$await$1 deferredCoroutine$await$2 = null;
        Label_0048: {
            if (continuation instanceof DeferredCoroutine$await.DeferredCoroutine$await$1) {
                final DeferredCoroutine$await.DeferredCoroutine$await$1 deferredCoroutine$await$1 = (DeferredCoroutine$await.DeferredCoroutine$await$1)continuation;
                if ((deferredCoroutine$await$1.label & Integer.MIN_VALUE) != 0x0) {
                    deferredCoroutine$await$1.label += Integer.MIN_VALUE;
                    deferredCoroutine$await$2 = deferredCoroutine$await$1;
                    break Label_0048;
                }
            }
            deferredCoroutine$await$2 = new DeferredCoroutine$await.DeferredCoroutine$await$1(l$0, continuation);
        }
        final Object result = deferredCoroutine$await$2.result;
        final Object coroutine_SUSPENDED = IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
        final int label = deferredCoroutine$await$2.label;
        if (label != 0) {
            if (label == 1) {
                l$0 = (DeferredCoroutine)deferredCoroutine$await$2.L$0;
                ResultKt.throwOnFailure(result);
                return result;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        else {
            ResultKt.throwOnFailure(result);
            deferredCoroutine$await$2.L$0 = l$0;
            deferredCoroutine$await$2.label = 1;
            final Object awaitInternal$kotlinx_coroutines_core = l$0.awaitInternal$kotlinx_coroutines_core((Continuation)deferredCoroutine$await$2);
            if (awaitInternal$kotlinx_coroutines_core == coroutine_SUSPENDED) {
                return coroutine_SUSPENDED;
            }
            return awaitInternal$kotlinx_coroutines_core;
        }
    }
    
    @Override
    public Object await(final Continuation<? super T> continuation) {
        return await$suspendImpl(this, continuation);
    }
    
    @Override
    public T getCompleted() {
        return (T)this.getCompletedInternal$kotlinx_coroutines_core();
    }
    
    @Override
    public SelectClause1<T> getOnAwait() {
        return this;
    }
    
    @Override
    public <R> void registerSelectClause1(final SelectInstance<? super R> selectInstance, final Function2<? super T, ? super Continuation<? super R>, ?> function2) {
        Intrinsics.checkParameterIsNotNull(selectInstance, "select");
        Intrinsics.checkParameterIsNotNull(function2, "block");
        this.registerSelectClause1Internal$kotlinx_coroutines_core((SelectInstance<? super Object>)selectInstance, (Function2<? super Object, ? super Continuation<? super Object>, ?>)function2);
    }
}
