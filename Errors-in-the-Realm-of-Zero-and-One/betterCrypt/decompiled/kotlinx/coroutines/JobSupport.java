// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import java.util.ArrayList;
import kotlinx.coroutines.intrinsics.CancellableKt;
import kotlinx.coroutines.intrinsics.UndispatchedKt;
import kotlinx.coroutines.selects.SelectInstance;
import kotlinx.coroutines.internal.OpDescriptor;
import kotlin.sequences.SequenceScope;
import kotlin.sequences.Sequence;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function2;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.coroutines.jvm.internal.CoroutineStackFrame;
import kotlin.jvm.internal.InlineMarker;
import kotlin.coroutines.Continuation;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.internal.StackTraceRecoveryKt;
import kotlinx.coroutines.internal.ConcurrentKt;
import java.util.List;
import kotlin.TypeCastException;
import kotlinx.coroutines.internal.LockFreeLinkedListNode;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Metadata;
import kotlin.DeprecationLevel;
import kotlin.Deprecated;
import kotlinx.coroutines.selects.SelectClause0;

@Deprecated(level = DeprecationLevel.ERROR, message = "This is internal API and may be removed in the future releases")
@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u00da\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u001b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0001\n\u0002\b\n\n\u0002\u0010\u000e\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u001a\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0013\b\u0017\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0006\u00d0\u0001\u00d1\u0001\u00d2\u0001B\u000f\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b\u0007\u0010\bJ+\u0010\u000f\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u000b2\n\u0010\u000e\u001a\u0006\u0012\u0002\b\u00030\rH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J%\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0012\u001a\u00020\u00112\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00110\u0013H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J!\u0010\u001b\u001a\u00020\u00152\b\u0010\u0018\u001a\u0004\u0018\u00010\t2\u0006\u0010\u001a\u001a\u00020\u0019H\u0014¢\u0006\u0004\b\u001b\u0010\u001cJ\u0015\u0010\u001f\u001a\u00020\u001e2\u0006\u0010\u001d\u001a\u00020\u0002¢\u0006\u0004\b\u001f\u0010 J\u0015\u0010#\u001a\u0004\u0018\u00010\tH\u0080@\u00f8\u0001\u0000¢\u0006\u0004\b!\u0010\"J\u0015\u0010$\u001a\u0004\u0018\u00010\tH\u0082@\u00f8\u0001\u0000¢\u0006\u0004\b$\u0010\"J\u0019\u0010&\u001a\u00020\u00052\b\u0010%\u001a\u0004\u0018\u00010\u0011H\u0017¢\u0006\u0004\b&\u0010'J\u001f\u0010&\u001a\u00020\u00152\u000e\u0010%\u001a\n\u0018\u00010(j\u0004\u0018\u0001`)H\u0016¢\u0006\u0004\b&\u0010*J\u0017\u0010+\u001a\u00020\u00052\b\u0010%\u001a\u0004\u0018\u00010\u0011¢\u0006\u0004\b+\u0010'J\u0019\u0010.\u001a\u00020\u00052\b\u0010%\u001a\u0004\u0018\u00010\tH\u0000¢\u0006\u0004\b,\u0010-J\u0019\u0010/\u001a\u00020\u00052\b\u0010%\u001a\u0004\u0018\u00010\u0011H\u0016¢\u0006\u0004\b/\u0010'J\u0019\u00100\u001a\u00020\u00052\b\u0010%\u001a\u0004\u0018\u00010\tH\u0002¢\u0006\u0004\b0\u0010-J\u0017\u00101\u001a\u00020\u00052\u0006\u0010%\u001a\u00020\u0011H\u0002¢\u0006\u0004\b1\u0010'J\u0017\u00102\u001a\u00020\u00052\u0006\u0010%\u001a\u00020\u0011H\u0016¢\u0006\u0004\b2\u0010'J)\u00105\u001a\u00020\u00152\u0006\u0010\u0018\u001a\u0002032\b\u00104\u001a\u0004\u0018\u00010\t2\u0006\u0010\u001a\u001a\u00020\u0019H\u0002¢\u0006\u0004\b5\u00106J)\u0010;\u001a\u00020\u00152\u0006\u0010\u0018\u001a\u0002072\u0006\u00109\u001a\u0002082\b\u0010:\u001a\u0004\u0018\u00010\tH\u0002¢\u0006\u0004\b;\u0010<J\u0019\u0010=\u001a\u00020\u00112\b\u0010%\u001a\u0004\u0018\u00010\tH\u0002¢\u0006\u0004\b=\u0010>J\u000f\u0010@\u001a\u00020?H\u0002¢\u0006\u0004\b@\u0010AJ\u0019\u0010B\u001a\u0004\u0018\u0001082\u0006\u0010\u0018\u001a\u000203H\u0002¢\u0006\u0004\bB\u0010CJ\u0011\u0010D\u001a\u00060(j\u0002`)¢\u0006\u0004\bD\u0010EJ\u0013\u0010F\u001a\u00060(j\u0002`)H\u0016¢\u0006\u0004\bF\u0010EJ\u0011\u0010I\u001a\u0004\u0018\u00010\tH\u0000¢\u0006\u0004\bG\u0010HJ\u000f\u0010J\u001a\u0004\u0018\u00010\u0011¢\u0006\u0004\bJ\u0010KJ'\u0010L\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0018\u001a\u0002072\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00110\u0013H\u0002¢\u0006\u0004\bL\u0010MJ\u0019\u0010N\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\u0018\u001a\u000203H\u0002¢\u0006\u0004\bN\u0010OJ\u0017\u0010Q\u001a\u00020\u00052\u0006\u0010P\u001a\u00020\u0011H\u0014¢\u0006\u0004\bQ\u0010'J\u0017\u0010T\u001a\u00020\u00152\u0006\u0010P\u001a\u00020\u0011H\u0010¢\u0006\u0004\bR\u0010SJ\u0019\u0010X\u001a\u00020\u00152\b\u0010U\u001a\u0004\u0018\u00010\u0001H\u0000¢\u0006\u0004\bV\u0010WJF\u0010a\u001a\u00020`2\u0006\u0010Y\u001a\u00020\u00052\u0006\u0010Z\u001a\u00020\u00052'\u0010_\u001a#\u0012\u0015\u0012\u0013\u0018\u00010\u0011¢\u0006\f\b\\\u0012\b\b]\u0012\u0004\b\b(%\u0012\u0004\u0012\u00020\u00150[j\u0002`^¢\u0006\u0004\ba\u0010bJ6\u0010a\u001a\u00020`2'\u0010_\u001a#\u0012\u0015\u0012\u0013\u0018\u00010\u0011¢\u0006\f\b\\\u0012\b\b]\u0012\u0004\b\b(%\u0012\u0004\u0012\u00020\u00150[j\u0002`^¢\u0006\u0004\ba\u0010cJ\u0013\u0010d\u001a\u00020\u0015H\u0086@\u00f8\u0001\u0000¢\u0006\u0004\bd\u0010\"J\u000f\u0010e\u001a\u00020\u0005H\u0002¢\u0006\u0004\be\u0010fJ\u0013\u0010g\u001a\u00020\u0015H\u0082@\u00f8\u0001\u0000¢\u0006\u0004\bg\u0010\"J&\u0010j\u001a\u00020i2\u0014\u0010h\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\t\u0012\u0004\u0012\u00020\u00150[H\u0082\b¢\u0006\u0004\bj\u0010kJ\u0019\u0010l\u001a\u00020\u00052\b\u0010%\u001a\u0004\u0018\u00010\tH\u0002¢\u0006\u0004\bl\u0010-J\u0019\u0010n\u001a\u00020\u00052\b\u0010:\u001a\u0004\u0018\u00010\tH\u0000¢\u0006\u0004\bm\u0010-J!\u0010q\u001a\u00020\u00052\b\u0010:\u001a\u0004\u0018\u00010\t2\u0006\u0010\u001a\u001a\u00020\u0019H\u0000¢\u0006\u0004\bo\u0010pJD\u0010r\u001a\u0006\u0012\u0002\b\u00030\r2'\u0010_\u001a#\u0012\u0015\u0012\u0013\u0018\u00010\u0011¢\u0006\f\b\\\u0012\b\b]\u0012\u0004\b\b(%\u0012\u0004\u0012\u00020\u00150[j\u0002`^2\u0006\u0010Y\u001a\u00020\u0005H\u0002¢\u0006\u0004\br\u0010sJ\u000f\u0010w\u001a\u00020tH\u0010¢\u0006\u0004\bu\u0010vJ\u001f\u0010x\u001a\u00020\u00152\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010%\u001a\u00020\u0011H\u0002¢\u0006\u0004\bx\u0010yJ2\u0010{\u001a\u00020\u0015\"\u000e\b\u0000\u0010z\u0018\u0001*\u0006\u0012\u0002\b\u00030\r2\u0006\u0010\f\u001a\u00020\u000b2\b\u0010%\u001a\u0004\u0018\u00010\u0011H\u0082\b¢\u0006\u0004\b{\u0010yJ\u0019\u0010Y\u001a\u00020\u00152\b\u0010%\u001a\u0004\u0018\u00010\u0011H\u0014¢\u0006\u0004\bY\u0010SJ\u0019\u0010|\u001a\u00020\u00152\b\u0010\u0018\u001a\u0004\u0018\u00010\tH\u0014¢\u0006\u0004\b|\u0010}J\u0010\u0010\u0080\u0001\u001a\u00020\u0015H\u0010¢\u0006\u0004\b~\u0010\u007fJ\u0019\u0010\u0082\u0001\u001a\u00020\u00152\u0007\u0010\u0081\u0001\u001a\u00020\u0003¢\u0006\u0006\b\u0082\u0001\u0010\u0083\u0001J\u001b\u0010\u0085\u0001\u001a\u00020\u00152\u0007\u0010\u0018\u001a\u00030\u0084\u0001H\u0002¢\u0006\u0006\b\u0085\u0001\u0010\u0086\u0001J\u001e\u0010\u0087\u0001\u001a\u00020\u00152\n\u0010\u0018\u001a\u0006\u0012\u0002\b\u00030\rH\u0002¢\u0006\u0006\b\u0087\u0001\u0010\u0088\u0001JI\u0010\u008d\u0001\u001a\u00020\u0015\"\u0005\b\u0000\u0010\u0089\u00012\u000e\u0010\u008b\u0001\u001a\t\u0012\u0004\u0012\u00028\u00000\u008a\u00012\u001d\u0010h\u001a\u0019\b\u0001\u0012\u000b\u0012\t\u0012\u0004\u0012\u00028\u00000\u008c\u0001\u0012\u0006\u0012\u0004\u0018\u00010\t0[\u00f8\u0001\u0000¢\u0006\u0006\b\u008d\u0001\u0010\u008e\u0001JX\u0010\u0092\u0001\u001a\u00020\u0015\"\u0004\b\u0000\u0010z\"\u0005\b\u0001\u0010\u0089\u00012\u000e\u0010\u008b\u0001\u001a\t\u0012\u0004\u0012\u00028\u00010\u008a\u00012$\u0010h\u001a \b\u0001\u0012\u0004\u0012\u00028\u0000\u0012\u000b\u0012\t\u0012\u0004\u0012\u00028\u00010\u008c\u0001\u0012\u0006\u0012\u0004\u0018\u00010\t0\u008f\u0001H\u0000\u00f8\u0001\u0000¢\u0006\u0006\b\u0090\u0001\u0010\u0091\u0001J\u001e\u0010\u0094\u0001\u001a\u00020\u00152\n\u0010\u000e\u001a\u0006\u0012\u0002\b\u00030\rH\u0000¢\u0006\u0006\b\u0093\u0001\u0010\u0088\u0001JX\u0010\u0096\u0001\u001a\u00020\u0015\"\u0004\b\u0000\u0010z\"\u0005\b\u0001\u0010\u0089\u00012\u000e\u0010\u008b\u0001\u001a\t\u0012\u0004\u0012\u00028\u00010\u008a\u00012$\u0010h\u001a \b\u0001\u0012\u0004\u0012\u00028\u0000\u0012\u000b\u0012\t\u0012\u0004\u0012\u00028\u00010\u008c\u0001\u0012\u0006\u0012\u0004\u0018\u00010\t0\u008f\u0001H\u0000\u00f8\u0001\u0000¢\u0006\u0006\b\u0095\u0001\u0010\u0091\u0001J\u000f\u0010\u0097\u0001\u001a\u00020\u0005¢\u0006\u0005\b\u0097\u0001\u0010fJ\u001c\u0010\u0098\u0001\u001a\u00020\u00192\b\u0010\u0018\u001a\u0004\u0018\u00010\tH\u0002¢\u0006\u0006\b\u0098\u0001\u0010\u0099\u0001J\u001c\u0010\u009a\u0001\u001a\u00020t2\b\u0010\u0018\u001a\u0004\u0018\u00010\tH\u0002¢\u0006\u0006\b\u009a\u0001\u0010\u009b\u0001J\u0011\u0010\u009c\u0001\u001a\u00020tH\u0007¢\u0006\u0005\b\u009c\u0001\u0010vJ\u0011\u0010\u009d\u0001\u001a\u00020tH\u0016¢\u0006\u0005\b\u009d\u0001\u0010vJ,\u0010\u009e\u0001\u001a\u00020\u00052\u0006\u0010\u0018\u001a\u0002072\b\u0010:\u001a\u0004\u0018\u00010\t2\u0006\u0010\u001a\u001a\u00020\u0019H\u0002¢\u0006\u0006\b\u009e\u0001\u0010\u009f\u0001J,\u0010 \u0001\u001a\u00020\u00052\u0006\u0010\u0018\u001a\u0002032\b\u00104\u001a\u0004\u0018\u00010\t2\u0006\u0010\u001a\u001a\u00020\u0019H\u0002¢\u0006\u0006\b \u0001\u0010¡\u0001J\"\u0010¢\u0001\u001a\u00020\u00052\u0006\u0010\u0018\u001a\u0002032\u0006\u0010\u0012\u001a\u00020\u0011H\u0002¢\u0006\u0006\b¢\u0001\u0010£\u0001J.\u0010¤\u0001\u001a\u00020\u00192\b\u0010\u0018\u001a\u0004\u0018\u00010\t2\b\u0010:\u001a\u0004\u0018\u00010\t2\u0006\u0010\u001a\u001a\u00020\u0019H\u0002¢\u0006\u0006\b¤\u0001\u0010¥\u0001J,\u0010¦\u0001\u001a\u00020\u00192\u0006\u0010\u0018\u001a\u0002032\b\u0010:\u001a\u0004\u0018\u00010\t2\u0006\u0010\u001a\u001a\u00020\u0019H\u0002¢\u0006\u0006\b¦\u0001\u0010§\u0001J-\u0010¨\u0001\u001a\u00020\u00052\u0006\u0010\u0018\u001a\u0002072\u0006\u0010\u001d\u001a\u0002082\b\u0010:\u001a\u0004\u0018\u00010\tH\u0082\u0010¢\u0006\u0006\b¨\u0001\u0010©\u0001J\u0019\u0010«\u0001\u001a\u0004\u0018\u000108*\u00030ª\u0001H\u0002¢\u0006\u0006\b«\u0001\u0010¬\u0001J\u001f\u0010\u00ad\u0001\u001a\u00020\u0015*\u00020\u000b2\b\u0010%\u001a\u0004\u0018\u00010\u0011H\u0002¢\u0006\u0005\b\u00ad\u0001\u0010yJ'\u0010¯\u0001\u001a\u00060(j\u0002`)*\u00020\u00112\u000b\b\u0002\u0010®\u0001\u001a\u0004\u0018\u00010tH\u0004¢\u0006\u0006\b¯\u0001\u0010°\u0001R\u001d\u0010´\u0001\u001a\t\u0012\u0004\u0012\u00020\u00010±\u00018F@\u0006¢\u0006\b\u001a\u0006\b²\u0001\u0010³\u0001R\u001a\u0010¶\u0001\u001a\u0004\u0018\u00010\u00118D@\u0004X\u0084\u0004¢\u0006\u0007\u001a\u0005\bµ\u0001\u0010KR\u0018\u0010¸\u0001\u001a\u00020\u00058D@\u0004X\u0084\u0004¢\u0006\u0007\u001a\u0005\b·\u0001\u0010fR\u0018\u0010º\u0001\u001a\u00020\u00058P@\u0010X\u0090\u0004¢\u0006\u0007\u001a\u0005\b¹\u0001\u0010fR\u0018\u0010»\u0001\u001a\u00020\u00058V@\u0016X\u0096\u0004¢\u0006\u0007\u001a\u0005\b»\u0001\u0010fR\u0015\u0010¼\u0001\u001a\u00020\u00058F@\u0006¢\u0006\u0007\u001a\u0005\b¼\u0001\u0010fR\u0015\u0010½\u0001\u001a\u00020\u00058F@\u0006¢\u0006\u0007\u001a\u0005\b½\u0001\u0010fR\u0015\u0010¾\u0001\u001a\u00020\u00058F@\u0006¢\u0006\u0007\u001a\u0005\b¾\u0001\u0010fR\u0018\u0010¿\u0001\u001a\u00020\u00058T@\u0014X\u0094\u0004¢\u0006\u0007\u001a\u0005\b¿\u0001\u0010fR\u001b\u0010\u00c3\u0001\u001a\u0007\u0012\u0002\b\u00030\u00c0\u00018F@\u0006¢\u0006\b\u001a\u0006\b\u00c1\u0001\u0010\u00c2\u0001R\u0018\u0010\u00c5\u0001\u001a\u00020\u00058P@\u0010X\u0090\u0004¢\u0006\u0007\u001a\u0005\b\u00c4\u0001\u0010fR\u0016\u0010\u00c8\u0001\u001a\u00020\u00048F@\u0006¢\u0006\b\u001a\u0006\b\u00c6\u0001\u0010\u00c7\u0001R\u001b\u0010\u00c9\u0001\u001a\u0004\u0018\u00010\u001e8\u0000@\u0000X\u0081\u000e¢\u0006\b\n\u0006\b\u00c9\u0001\u0010\u00ca\u0001R\u0019\u0010\u0018\u001a\u0004\u0018\u00010\t8@@\u0000X\u0080\u0004¢\u0006\u0007\u001a\u0005\b\u00cb\u0001\u0010HR \u0010\u00cd\u0001\u001a\u0004\u0018\u00010\u0011*\u0004\u0018\u00010\t8B@\u0002X\u0082\u0004¢\u0006\u0007\u001a\u0005\b\u00cc\u0001\u0010>R\u001d\u0010\u00ce\u0001\u001a\u00020\u0005*\u0002038B@\u0002X\u0082\u0004¢\u0006\b\u001a\u0006\b\u00ce\u0001\u0010\u00cf\u0001\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u00d3\u0001" }, d2 = { "Lkotlinx/coroutines/JobSupport;", "Lkotlinx/coroutines/Job;", "Lkotlinx/coroutines/ChildJob;", "Lkotlinx/coroutines/ParentJob;", "Lkotlinx/coroutines/selects/SelectClause0;", "", "active", "<init>", "(Z)V", "", "expect", "Lkotlinx/coroutines/NodeList;", "list", "Lkotlinx/coroutines/JobNode;", "node", "addLastAtomic", "(Ljava/lang/Object;Lkotlinx/coroutines/NodeList;Lkotlinx/coroutines/JobNode;)Z", "", "rootCause", "", "exceptions", "", "addSuppressedExceptions", "(Ljava/lang/Throwable;Ljava/util/List;)V", "state", "", "mode", "afterCompletionInternal", "(Ljava/lang/Object;I)V", "child", "Lkotlinx/coroutines/ChildHandle;", "attachChild", "(Lkotlinx/coroutines/ChildJob;)Lkotlinx/coroutines/ChildHandle;", "awaitInternal$kotlinx_coroutines_core", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "awaitInternal", "awaitSuspend", "cause", "cancel", "(Ljava/lang/Throwable;)Z", "Ljava/util/concurrent/CancellationException;", "Lkotlinx/coroutines/CancellationException;", "(Ljava/util/concurrent/CancellationException;)V", "cancelCoroutine", "cancelImpl$kotlinx_coroutines_core", "(Ljava/lang/Object;)Z", "cancelImpl", "cancelInternal", "cancelMakeCompleting", "cancelParent", "childCancelled", "Lkotlinx/coroutines/Incomplete;", "update", "completeStateFinalization", "(Lkotlinx/coroutines/Incomplete;Ljava/lang/Object;I)V", "Lkotlinx/coroutines/JobSupport$Finishing;", "Lkotlinx/coroutines/ChildHandleNode;", "lastChild", "proposedUpdate", "continueCompleting", "(Lkotlinx/coroutines/JobSupport$Finishing;Lkotlinx/coroutines/ChildHandleNode;Ljava/lang/Object;)V", "createCauseException", "(Ljava/lang/Object;)Ljava/lang/Throwable;", "Lkotlinx/coroutines/JobCancellationException;", "createJobCancellationException", "()Lkotlinx/coroutines/JobCancellationException;", "firstChild", "(Lkotlinx/coroutines/Incomplete;)Lkotlinx/coroutines/ChildHandleNode;", "getCancellationException", "()Ljava/util/concurrent/CancellationException;", "getChildJobCancellationCause", "getCompletedInternal$kotlinx_coroutines_core", "()Ljava/lang/Object;", "getCompletedInternal", "getCompletionExceptionOrNull", "()Ljava/lang/Throwable;", "getFinalRootCause", "(Lkotlinx/coroutines/JobSupport$Finishing;Ljava/util/List;)Ljava/lang/Throwable;", "getOrPromoteCancellingList", "(Lkotlinx/coroutines/Incomplete;)Lkotlinx/coroutines/NodeList;", "exception", "handleJobException", "handleOnCompletionException$kotlinx_coroutines_core", "(Ljava/lang/Throwable;)V", "handleOnCompletionException", "parent", "initParentJobInternal$kotlinx_coroutines_core", "(Lkotlinx/coroutines/Job;)V", "initParentJobInternal", "onCancelling", "invokeImmediately", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "Lkotlinx/coroutines/CompletionHandler;", "handler", "Lkotlinx/coroutines/DisposableHandle;", "invokeOnCompletion", "(ZZLkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/DisposableHandle;", "(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/DisposableHandle;", "join", "joinInternal", "()Z", "joinSuspend", "block", "", "loopOnState", "(Lkotlin/jvm/functions/Function1;)Ljava/lang/Void;", "makeCancelling", "makeCompleting$kotlinx_coroutines_core", "makeCompleting", "makeCompletingOnce$kotlinx_coroutines_core", "(Ljava/lang/Object;I)Z", "makeCompletingOnce", "makeNode", "(Lkotlin/jvm/functions/Function1;Z)Lkotlinx/coroutines/JobNode;", "", "nameString$kotlinx_coroutines_core", "()Ljava/lang/String;", "nameString", "notifyCancelling", "(Lkotlinx/coroutines/NodeList;Ljava/lang/Throwable;)V", "T", "notifyHandlers", "onCompletionInternal", "(Ljava/lang/Object;)V", "onStartInternal$kotlinx_coroutines_core", "()V", "onStartInternal", "parentJob", "parentCancelled", "(Lkotlinx/coroutines/ParentJob;)V", "Lkotlinx/coroutines/Empty;", "promoteEmptyToNodeList", "(Lkotlinx/coroutines/Empty;)V", "promoteSingleToNodeList", "(Lkotlinx/coroutines/JobNode;)V", "R", "Lkotlinx/coroutines/selects/SelectInstance;", "select", "Lkotlin/coroutines/Continuation;", "registerSelectClause0", "(Lkotlinx/coroutines/selects/SelectInstance;Lkotlin/jvm/functions/Function1;)V", "Lkotlin/Function2;", "registerSelectClause1Internal$kotlinx_coroutines_core", "(Lkotlinx/coroutines/selects/SelectInstance;Lkotlin/jvm/functions/Function2;)V", "registerSelectClause1Internal", "removeNode$kotlinx_coroutines_core", "removeNode", "selectAwaitCompletion$kotlinx_coroutines_core", "selectAwaitCompletion", "start", "startInternal", "(Ljava/lang/Object;)I", "stateString", "(Ljava/lang/Object;)Ljava/lang/String;", "toDebugString", "toString", "tryFinalizeFinishingState", "(Lkotlinx/coroutines/JobSupport$Finishing;Ljava/lang/Object;I)Z", "tryFinalizeSimpleState", "(Lkotlinx/coroutines/Incomplete;Ljava/lang/Object;I)Z", "tryMakeCancelling", "(Lkotlinx/coroutines/Incomplete;Ljava/lang/Throwable;)Z", "tryMakeCompleting", "(Ljava/lang/Object;Ljava/lang/Object;I)I", "tryMakeCompletingSlowPath", "(Lkotlinx/coroutines/Incomplete;Ljava/lang/Object;I)I", "tryWaitForChild", "(Lkotlinx/coroutines/JobSupport$Finishing;Lkotlinx/coroutines/ChildHandleNode;Ljava/lang/Object;)Z", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "nextChild", "(Lkotlinx/coroutines/internal/LockFreeLinkedListNode;)Lkotlinx/coroutines/ChildHandleNode;", "notifyCompletion", "message", "toCancellationException", "(Ljava/lang/Throwable;Ljava/lang/String;)Ljava/util/concurrent/CancellationException;", "Lkotlin/sequences/Sequence;", "getChildren", "()Lkotlin/sequences/Sequence;", "children", "getCompletionCause", "completionCause", "getCompletionCauseHandled", "completionCauseHandled", "getHandlesException$kotlinx_coroutines_core", "handlesException", "isActive", "isCancelled", "isCompleted", "isCompletedExceptionally", "isScopedCoroutine", "Lkotlin/coroutines/CoroutineContext$Key;", "getKey", "()Lkotlin/coroutines/CoroutineContext$Key;", "key", "getOnCancelComplete$kotlinx_coroutines_core", "onCancelComplete", "getOnJoin", "()Lkotlinx/coroutines/selects/SelectClause0;", "onJoin", "parentHandle", "Lkotlinx/coroutines/ChildHandle;", "getState$kotlinx_coroutines_core", "getExceptionOrNull", "exceptionOrNull", "isCancelling", "(Lkotlinx/coroutines/Incomplete;)Z", "AwaitContinuation", "ChildCompletion", "Finishing", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public class JobSupport implements Job, ChildJob, ParentJob, SelectClause0
{
    private static final AtomicReferenceFieldUpdater _state$FU;
    private volatile Object _state;
    public volatile ChildHandle parentHandle;
    
    static {
        _state$FU = AtomicReferenceFieldUpdater.newUpdater(JobSupport.class, Object.class, "_state");
    }
    
    public JobSupport(final boolean b) {
        Empty state;
        if (b) {
            state = JobSupportKt.access$getEMPTY_ACTIVE$p();
        }
        else {
            state = JobSupportKt.access$getEMPTY_NEW$p();
        }
        this._state = state;
    }
    
    private final boolean addLastAtomic(final Object o, final NodeList list, final JobNode<?> jobNode) {
        final JobNode<?> jobNode2 = jobNode;
        final LockFreeLinkedListNode.CondAddOp condAddOp = (LockFreeLinkedListNode.CondAddOp)new JobSupport$addLastAtomic$$inlined$addLastIf.JobSupport$addLastAtomic$$inlined$addLastIf$1((LockFreeLinkedListNode)jobNode2, (LockFreeLinkedListNode)jobNode2, this, o);
        boolean b;
        while (true) {
            final Object prev = list.getPrev();
            if (prev == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            }
            final int tryCondAddNext = ((LockFreeLinkedListNode)prev).tryCondAddNext(jobNode2, list, condAddOp);
            b = true;
            if (tryCondAddNext == 1) {
                break;
            }
            if (tryCondAddNext != 2) {
                continue;
            }
            b = false;
            break;
        }
        return b;
    }
    
    private final void addSuppressedExceptions(final Throwable t, final List<? extends Throwable> list) {
        if (list.size() <= 1) {
            return;
        }
        final Set<Throwable> identitySet = ConcurrentKt.identitySet(list.size());
        final Throwable unwrap = StackTraceRecoveryKt.unwrap(t);
        final Iterator<? extends Throwable> iterator = list.iterator();
        while (iterator.hasNext()) {
            final Throwable unwrap2 = StackTraceRecoveryKt.unwrap((Throwable)iterator.next());
            if (unwrap2 != t && unwrap2 != unwrap && !(unwrap2 instanceof CancellationException) && identitySet.add(unwrap2)) {
                ExceptionsKt__ExceptionsKt.addSuppressed(t, unwrap2);
            }
        }
    }
    
    private final boolean cancelMakeCompleting(final Object o) {
        while (true) {
            final Object state$kotlinx_coroutines_core = this.getState$kotlinx_coroutines_core();
            if (!(state$kotlinx_coroutines_core instanceof Incomplete)) {
                break;
            }
            if (state$kotlinx_coroutines_core instanceof Finishing && ((Finishing)state$kotlinx_coroutines_core).isCompleting) {
                return false;
            }
            final int tryMakeCompleting = this.tryMakeCompleting(state$kotlinx_coroutines_core, new CompletedExceptionally(this.createCauseException(o), false, 2, null), 0);
            if (tryMakeCompleting == 0) {
                break;
            }
            if (tryMakeCompleting == 1 || tryMakeCompleting == 2) {
                return true;
            }
            if (tryMakeCompleting == 3) {
                continue;
            }
            throw new IllegalStateException("unexpected result".toString());
        }
        return false;
    }
    
    private final boolean cancelParent(final Throwable t) {
        final boolean scopedCoroutine = this.isScopedCoroutine();
        boolean b = true;
        if (scopedCoroutine) {
            return true;
        }
        final boolean b2 = t instanceof CancellationException;
        final ChildHandle parentHandle = this.parentHandle;
        if (parentHandle == null) {
            return b2;
        }
        if (parentHandle == NonDisposableHandle.INSTANCE) {
            return b2;
        }
        if (!parentHandle.childCancelled(t)) {
            if (b2) {
                return true;
            }
            b = false;
        }
        return b;
    }
    
    private final void completeStateFinalization(final Incomplete obj, final Object o, final int n) {
        final ChildHandle parentHandle = this.parentHandle;
        if (parentHandle != null) {
            parentHandle.dispose();
            this.parentHandle = NonDisposableHandle.INSTANCE;
        }
        final boolean b = o instanceof CompletedExceptionally;
        final Throwable t = null;
        Object o2;
        if (!b) {
            o2 = null;
        }
        else {
            o2 = o;
        }
        final CompletedExceptionally completedExceptionally = (CompletedExceptionally)o2;
        Throwable cause = t;
        if (completedExceptionally != null) {
            cause = completedExceptionally.cause;
        }
        if (obj instanceof JobNode) {
            try {
                ((JobNode)obj).invoke(cause);
            }
            finally {
                final StringBuilder sb = new StringBuilder();
                sb.append("Exception in completion handler ");
                sb.append(obj);
                sb.append(" for ");
                sb.append(this);
                final Throwable t2;
                this.handleOnCompletionException$kotlinx_coroutines_core(new CompletionHandlerException(sb.toString(), t2));
            }
        }
        else {
            final NodeList list = obj.getList();
            if (list != null) {
                this.notifyCompletion(list, cause);
            }
        }
        this.afterCompletionInternal(o, n);
    }
    
    private final void continueCompleting(final Finishing finishing, ChildHandleNode nextChild, final Object o) {
        if (this.getState$kotlinx_coroutines_core() != finishing) {
            throw new IllegalArgumentException("Failed requirement.".toString());
        }
        nextChild = this.nextChild(nextChild);
        if (nextChild != null && this.tryWaitForChild(finishing, nextChild, o)) {
            return;
        }
        if (this.tryFinalizeFinishingState(finishing, o, 0)) {}
    }
    
    private final Throwable createCauseException(Object jobCancellationException) {
        if (jobCancellationException == null || jobCancellationException instanceof Throwable) {
            if (jobCancellationException == null) {
                jobCancellationException = this.createJobCancellationException();
            }
            return (Throwable)jobCancellationException;
        }
        if (jobCancellationException != null) {
            return ((ParentJob)jobCancellationException).getChildJobCancellationCause();
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.ParentJob");
    }
    
    private final JobCancellationException createJobCancellationException() {
        return new JobCancellationException("Job was cancelled", null, this);
    }
    
    private final ChildHandleNode firstChild(final Incomplete incomplete) {
        final boolean b = incomplete instanceof ChildHandleNode;
        final ChildHandleNode childHandleNode = null;
        Incomplete incomplete2;
        if (!b) {
            incomplete2 = null;
        }
        else {
            incomplete2 = incomplete;
        }
        final ChildHandleNode childHandleNode2 = (ChildHandleNode)incomplete2;
        if (childHandleNode2 != null) {
            return childHandleNode2;
        }
        final NodeList list = incomplete.getList();
        ChildHandleNode nextChild = childHandleNode;
        if (list != null) {
            nextChild = this.nextChild(list);
        }
        return nextChild;
    }
    
    private final Throwable getExceptionOrNull(Object o) {
        final boolean b = o instanceof CompletedExceptionally;
        final Throwable t = null;
        if (!b) {
            o = null;
        }
        final CompletedExceptionally completedExceptionally = (CompletedExceptionally)o;
        Throwable cause = t;
        if (completedExceptionally != null) {
            cause = completedExceptionally.cause;
        }
        return cause;
    }
    
    private final Throwable getFinalRootCause(final Finishing finishing, final List<? extends Throwable> list) {
        if (!list.isEmpty()) {
            while (true) {
                for (final Throwable next : list) {
                    if (next instanceof CancellationException ^ true) {
                        final Throwable t = next;
                        if (t != null) {
                            return t;
                        }
                        return (Throwable)list.get(0);
                    }
                }
                Throwable next = null;
                continue;
            }
        }
        if (finishing.isCancelling()) {
            return this.createJobCancellationException();
        }
        return null;
    }
    
    private final NodeList getOrPromoteCancellingList(final Incomplete obj) {
        final NodeList list = obj.getList();
        if (list != null) {
            return list;
        }
        if (obj instanceof Empty) {
            return new NodeList();
        }
        if (obj instanceof JobNode) {
            this.promoteSingleToNodeList((JobNode<?>)obj);
            return null;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("State should have list: ");
        sb.append(obj);
        throw new IllegalStateException(sb.toString().toString());
    }
    
    private final boolean isCancelling(final Incomplete incomplete) {
        return incomplete instanceof Finishing && ((Finishing)incomplete).isCancelling();
    }
    
    private final boolean joinInternal() {
        Object state$kotlinx_coroutines_core;
        do {
            state$kotlinx_coroutines_core = this.getState$kotlinx_coroutines_core();
            if (!(state$kotlinx_coroutines_core instanceof Incomplete)) {
                return false;
            }
        } while (this.startInternal(state$kotlinx_coroutines_core) < 0);
        return true;
    }
    
    private final Void loopOnState(final Function1<Object, Unit> function1) {
        while (true) {
            function1.invoke(this.getState$kotlinx_coroutines_core());
        }
    }
    
    private final boolean makeCancelling(Object o) {
        Throwable causeException = null;
        while (true) {
            final Object state$kotlinx_coroutines_core = this.getState$kotlinx_coroutines_core();
            if (state$kotlinx_coroutines_core instanceof Finishing) {
            Label_0061_Outer:
                while (true) {
                Label_0068_Outer:
                    while (true) {
                        while (true) {
                            Label_0295: {
                                synchronized (state$kotlinx_coroutines_core) {
                                    boolean b = ((Finishing)state$kotlinx_coroutines_core).isSealed();
                                    if (b) {
                                        return false;
                                    }
                                    b = ((Finishing)state$kotlinx_coroutines_core).isCancelling();
                                    if (o != null || !b) {
                                        break Label_0295;
                                    }
                                    while (true) {
                                        Throwable rootCause = ((Finishing)state$kotlinx_coroutines_core).rootCause;
                                        if (!(b ^ true)) {
                                            rootCause = null;
                                        }
                                        if (rootCause != null) {
                                            this.notifyCancelling(((Finishing)state$kotlinx_coroutines_core).getList(), rootCause);
                                        }
                                        return true;
                                        final Throwable t;
                                        causeException = this.createCauseException(t);
                                        ((Finishing)state$kotlinx_coroutines_core).addExceptionLocked(causeException);
                                        continue Label_0061_Outer;
                                    }
                                }
                                // monitorexit(state$kotlinx_coroutines_core)
                                break;
                            }
                            if (causeException != null) {
                                continue;
                            }
                            break;
                        }
                        continue Label_0068_Outer;
                    }
                }
            }
            if (!(state$kotlinx_coroutines_core instanceof Incomplete)) {
                return false;
            }
            Throwable causeException2;
            if (causeException != null) {
                causeException2 = causeException;
            }
            else {
                causeException2 = this.createCauseException(o);
            }
            final Finishing finishing = (Finishing)state$kotlinx_coroutines_core;
            if (finishing.isActive()) {
                causeException = causeException2;
                if (this.tryMakeCancelling(finishing, causeException2)) {
                    return true;
                }
                continue;
            }
            else {
                final int tryMakeCompleting = this.tryMakeCompleting(state$kotlinx_coroutines_core, new CompletedExceptionally(causeException2, false, 2, null), 0);
                if (tryMakeCompleting == 0) {
                    o = new StringBuilder();
                    ((StringBuilder)o).append("Cannot happen in ");
                    ((StringBuilder)o).append(state$kotlinx_coroutines_core);
                    throw new IllegalStateException(((StringBuilder)o).toString().toString());
                }
                if (tryMakeCompleting == 1 || tryMakeCompleting == 2) {
                    return true;
                }
                if (tryMakeCompleting != 3) {
                    throw new IllegalStateException("unexpected result".toString());
                }
                causeException = causeException2;
            }
        }
    }
    
    private final JobNode<?> makeNode(final Function1<? super Throwable, Unit> function1, final boolean b) {
        final int n = 1;
        boolean b2 = true;
        final JobNode<?> jobNode = null;
        JobCancellingNode<?> jobCancellingNode = null;
        if (b) {
            if (function1 instanceof JobCancellingNode) {
                jobCancellingNode = (JobCancellingNode<?>)function1;
            }
            final JobCancellingNode<?> jobCancellingNode2 = jobCancellingNode;
            if (jobCancellingNode2 != null) {
                if (jobCancellingNode2.job != this) {
                    b2 = false;
                }
                if (!b2) {
                    throw new IllegalArgumentException("Failed requirement.".toString());
                }
                if (jobCancellingNode2 != null) {
                    return jobCancellingNode2;
                }
            }
            return new InvokeOnCancelling(this, function1);
        }
        JobNode<?> jobNode2;
        if (!(function1 instanceof JobNode)) {
            jobNode2 = jobNode;
        }
        else {
            jobNode2 = (JobNode<?>)function1;
        }
        final JobNode<?> jobNode3 = jobNode2;
        if (jobNode3 != null) {
            int n2;
            if (jobNode3.job == this && !(jobNode3 instanceof JobCancellingNode)) {
                n2 = n;
            }
            else {
                n2 = 0;
            }
            if (n2 == 0) {
                throw new IllegalArgumentException("Failed requirement.".toString());
            }
            if (jobNode3 != null) {
                return jobNode3;
            }
        }
        return new InvokeOnCompletion(this, function1);
    }
    
    private final ChildHandleNode nextChild(LockFreeLinkedListNode lockFreeLinkedListNode) {
        LockFreeLinkedListNode lockFreeLinkedListNode2;
        while (true) {
            lockFreeLinkedListNode2 = lockFreeLinkedListNode;
            if (!lockFreeLinkedListNode.isRemoved()) {
                break;
            }
            lockFreeLinkedListNode = lockFreeLinkedListNode.getPrevNode();
        }
        while (true) {
            lockFreeLinkedListNode = lockFreeLinkedListNode2.getNextNode();
            if (lockFreeLinkedListNode.isRemoved()) {
                lockFreeLinkedListNode2 = lockFreeLinkedListNode;
            }
            else {
                if (lockFreeLinkedListNode instanceof ChildHandleNode) {
                    return (ChildHandleNode)lockFreeLinkedListNode;
                }
                lockFreeLinkedListNode2 = lockFreeLinkedListNode;
                if (lockFreeLinkedListNode instanceof NodeList) {
                    return null;
                }
                continue;
            }
        }
    }
    
    private final void notifyCancelling(final NodeList list, final Throwable t) {
        this.onCancelling(t);
        Object o = null;
        final Object next = list.getNext();
        if (next != null) {
            Object o2;
            for (LockFreeLinkedListNode nextNode = (LockFreeLinkedListNode)next; Intrinsics.areEqual(nextNode, list) ^ true; nextNode = nextNode.getNextNode(), o = o2) {
                o2 = o;
                if (nextNode instanceof JobCancellingNode) {
                    Object obj = nextNode;
                    try {
                        ((CompletionHandlerBase)obj).invoke(t);
                        obj = o;
                    }
                    finally {
                        final Throwable t2;
                        if (o != null) {
                            ExceptionsKt__ExceptionsKt.addSuppressed((Throwable)o, t2);
                            if (o != null) {
                                o2 = o;
                                continue;
                            }
                        }
                        final JobSupport obj2 = this;
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Exception in completion handler ");
                        sb.append(obj);
                        sb.append(" for ");
                        sb.append(obj2);
                        o2 = new CompletionHandlerException(sb.toString(), t2);
                        final Unit instance = Unit.INSTANCE;
                    }
                }
            }
            if (o != null) {
                this.handleOnCompletionException$kotlinx_coroutines_core((Throwable)o);
            }
            this.cancelParent(t);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }
    
    private final void notifyCompletion(final NodeList list, final Throwable t) {
        Object o = null;
        final Object next = list.getNext();
        if (next != null) {
            Object o2;
            for (LockFreeLinkedListNode nextNode = (LockFreeLinkedListNode)next; Intrinsics.areEqual(nextNode, list) ^ true; nextNode = nextNode.getNextNode(), o = o2) {
                o2 = o;
                if (nextNode instanceof JobNode) {
                    Object obj = nextNode;
                    try {
                        ((CompletionHandlerBase)obj).invoke(t);
                        obj = o;
                    }
                    finally {
                        final Throwable t2;
                        if (o != null) {
                            ExceptionsKt__ExceptionsKt.addSuppressed((Throwable)o, t2);
                            if (o != null) {
                                o2 = o;
                                continue;
                            }
                        }
                        final JobSupport obj2 = this;
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Exception in completion handler ");
                        sb.append(obj);
                        sb.append(" for ");
                        sb.append(obj2);
                        o2 = new CompletionHandlerException(sb.toString(), t2);
                        final Unit instance = Unit.INSTANCE;
                    }
                }
            }
            if (o != null) {
                this.handleOnCompletionException$kotlinx_coroutines_core((Throwable)o);
            }
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }
    
    private final void promoteEmptyToNodeList(final Empty empty) {
        final NodeList list = new NodeList();
        Incomplete incomplete;
        if (empty.isActive()) {
            incomplete = list;
        }
        else {
            incomplete = new InactiveNodeList(list);
        }
        JobSupport._state$FU.compareAndSet(this, empty, incomplete);
    }
    
    private final void promoteSingleToNodeList(final JobNode<?> jobNode) {
        jobNode.addOneIfEmpty(new NodeList());
        JobSupport._state$FU.compareAndSet(this, jobNode, jobNode.getNextNode());
    }
    
    private final int startInternal(final Object o) {
        if (o instanceof Empty) {
            if (((Empty)o).isActive()) {
                return 0;
            }
            if (!JobSupport._state$FU.compareAndSet(this, o, JobSupportKt.access$getEMPTY_ACTIVE$p())) {
                return -1;
            }
            this.onStartInternal$kotlinx_coroutines_core();
            return 1;
        }
        else {
            if (!(o instanceof InactiveNodeList)) {
                return 0;
            }
            if (!JobSupport._state$FU.compareAndSet(this, o, ((InactiveNodeList)o).getList())) {
                return -1;
            }
            this.onStartInternal$kotlinx_coroutines_core();
            return 1;
        }
    }
    
    private final String stateString(final Object o) {
        final boolean b = o instanceof Finishing;
        final String s = "Active";
        String s2;
        if (b) {
            final Finishing finishing = (Finishing)o;
            if (finishing.isCancelling()) {
                return "Cancelling";
            }
            s2 = s;
            if (finishing.isCompleting) {
                return "Completing";
            }
        }
        else if (o instanceof Incomplete) {
            if (((Incomplete)o).isActive()) {
                return "Active";
            }
            return "New";
        }
        else {
            if (o instanceof CompletedExceptionally) {
                return "Cancelled";
            }
            s2 = "Completed";
        }
        return s2;
    }
    
    public static /* synthetic */ CancellationException toCancellationException$default(final JobSupport jobSupport, final Throwable t, String s, final int n, final Object o) {
        if (o == null) {
            if ((n & 0x1) != 0x0) {
                s = null;
            }
            return jobSupport.toCancellationException(t, s);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: toCancellationException");
    }
    
    private final boolean tryFinalizeFinishingState(final Finishing obj, Object obj2, final int n) {
        final Object state$kotlinx_coroutines_core = this.getState$kotlinx_coroutines_core();
        final int n2 = 0;
        if (state$kotlinx_coroutines_core != obj) {
            throw new IllegalArgumentException("Failed requirement.".toString());
        }
        if (obj.isSealed() ^ true) {
            if (obj.isCompleting) {
                Object o;
                if (!(obj2 instanceof CompletedExceptionally)) {
                    o = null;
                }
                else {
                    o = obj2;
                }
                final CompletedExceptionally completedExceptionally = (CompletedExceptionally)o;
                Throwable cause;
                if (completedExceptionally != null) {
                    cause = completedExceptionally.cause;
                }
                else {
                    cause = null;
                }
                synchronized (obj) {
                    final boolean cancelling = obj.isCancelling();
                    final List<Throwable> sealLocked = obj.sealLocked(cause);
                    final Throwable finalRootCause = this.getFinalRootCause(obj, sealLocked);
                    if (finalRootCause != null) {
                        this.addSuppressedExceptions(finalRootCause, sealLocked);
                    }
                    // monitorexit(obj)
                    if (finalRootCause != null) {
                        if (finalRootCause != cause) {
                            obj2 = new CompletedExceptionally(finalRootCause, false, 2, null);
                        }
                    }
                    if (finalRootCause != null) {
                        int n3 = 0;
                        Label_0187: {
                            if (!this.cancelParent(finalRootCause)) {
                                n3 = n2;
                                if (!this.handleJobException(finalRootCause)) {
                                    break Label_0187;
                                }
                            }
                            n3 = 1;
                        }
                        if (n3 != 0) {
                            if (obj2 == null) {
                                throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.CompletedExceptionally");
                            }
                            ((CompletedExceptionally)obj2).makeHandled();
                        }
                    }
                    if (!cancelling) {
                        this.onCancelling(finalRootCause);
                    }
                    this.onCompletionInternal(obj2);
                    if (JobSupport._state$FU.compareAndSet(this, obj, JobSupportKt.boxIncomplete(obj2))) {
                        this.completeStateFinalization(obj, obj2, n);
                        return true;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unexpected state: ");
                    sb.append(this._state);
                    sb.append(", expected: ");
                    sb.append(obj);
                    sb.append(", update: ");
                    sb.append(obj2);
                    throw new IllegalArgumentException(sb.toString().toString());
                }
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }
    
    private final boolean tryFinalizeSimpleState(final Incomplete incomplete, final Object o, final int n) {
        if (DebugKt.getASSERTIONS_ENABLED() && (!(incomplete instanceof Empty) && !(incomplete instanceof JobNode))) {
            throw new AssertionError();
        }
        if (DebugKt.getASSERTIONS_ENABLED() && !(o instanceof CompletedExceptionally ^ true)) {
            throw new AssertionError();
        }
        if (!JobSupport._state$FU.compareAndSet(this, incomplete, JobSupportKt.boxIncomplete(o))) {
            return false;
        }
        this.onCancelling(null);
        this.onCompletionInternal(o);
        this.completeStateFinalization(incomplete, o, n);
        return true;
    }
    
    private final boolean tryMakeCancelling(final Incomplete incomplete, final Throwable t) {
        if (DebugKt.getASSERTIONS_ENABLED() && !(incomplete instanceof Finishing ^ true)) {
            throw new AssertionError();
        }
        if (DebugKt.getASSERTIONS_ENABLED() && !incomplete.isActive()) {
            throw new AssertionError();
        }
        final NodeList orPromoteCancellingList = this.getOrPromoteCancellingList(incomplete);
        if (orPromoteCancellingList == null) {
            return false;
        }
        if (!JobSupport._state$FU.compareAndSet(this, incomplete, new Finishing(orPromoteCancellingList, false, t))) {
            return false;
        }
        this.notifyCancelling(orPromoteCancellingList, t);
        return true;
    }
    
    private final int tryMakeCompleting(final Object o, final Object o2, final int n) {
        if (!(o instanceof Incomplete)) {
            return 0;
        }
        if ((!(o instanceof Empty) && !(o instanceof JobNode)) || o instanceof ChildHandleNode || o2 instanceof CompletedExceptionally) {
            return this.tryMakeCompletingSlowPath((Incomplete)o, o2, n);
        }
        if (!this.tryFinalizeSimpleState((Incomplete)o, o2, n)) {
            return 3;
        }
        return 1;
    }
    
    private final int tryMakeCompletingSlowPath(final Incomplete incomplete, final Object o, final int n) {
        final NodeList orPromoteCancellingList = this.getOrPromoteCancellingList(incomplete);
        if (orPromoteCancellingList != null) {
            boolean b = incomplete instanceof Finishing;
            Object instance = null;
            Incomplete incomplete2;
            if (!b) {
                incomplete2 = null;
            }
            else {
                incomplete2 = incomplete;
            }
            Finishing finishing = (Finishing)incomplete2;
            if (finishing == null) {
                finishing = new Finishing(orPromoteCancellingList, false, null);
            }
            while (true) {
                final Throwable t = null;
                while (true) {
                    Label_0281: {
                        synchronized (finishing) {
                            b = finishing.isCompleting;
                            if (b) {
                                return 0;
                            }
                            finishing.isCompleting = true;
                            if (finishing != incomplete) {
                                b = JobSupport._state$FU.compareAndSet(this, incomplete, finishing);
                                if (!b) {
                                    return 3;
                                }
                            }
                            if (!(finishing.isSealed() ^ true)) {
                                throw new IllegalArgumentException("Failed requirement.".toString());
                            }
                            b = finishing.isCancelling();
                            if (o instanceof CompletedExceptionally) {
                                break Label_0281;
                            }
                            final Object o2 = null;
                            final CompletedExceptionally completedExceptionally = (CompletedExceptionally)o2;
                            if (completedExceptionally != null) {
                                finishing.addExceptionLocked(completedExceptionally.cause);
                            }
                            final Throwable rootCause = finishing.rootCause;
                            Object o3 = instance;
                            if (b ^ true) {
                                o3 = rootCause;
                            }
                            instance = Unit.INSTANCE;
                            // monitorexit(finishing)
                            if (o3 != null) {
                                this.notifyCancelling(orPromoteCancellingList, (Throwable)o3);
                            }
                            final ChildHandleNode firstChild = this.firstChild(incomplete);
                            if (firstChild != null && this.tryWaitForChild(finishing, firstChild, o)) {
                                return 2;
                            }
                            if (this.tryFinalizeFinishingState(finishing, o, n)) {
                                return 1;
                            }
                            return 3;
                        }
                        break;
                    }
                    final Object o2 = o;
                    continue;
                }
            }
        }
        return 3;
    }
    
    private final boolean tryWaitForChild(final Finishing finishing, ChildHandleNode nextChild, final Object o) {
        while (Job.DefaultImpls.invokeOnCompletion$default(nextChild.childJob, false, false, new ChildCompletion(this, finishing, nextChild, o), 1, null) == NonDisposableHandle.INSTANCE) {
            nextChild = this.nextChild(nextChild);
            if (nextChild != null) {
                continue;
            }
            return false;
        }
        return true;
    }
    
    protected void afterCompletionInternal(final Object o, final int n) {
    }
    
    @Override
    public final ChildHandle attachChild(final ChildJob childJob) {
        Intrinsics.checkParameterIsNotNull(childJob, "child");
        final DisposableHandle invokeOnCompletion$default = Job.DefaultImpls.invokeOnCompletion$default(this, true, false, new ChildHandleNode(this, childJob), 2, null);
        if (invokeOnCompletion$default != null) {
            return (ChildHandle)invokeOnCompletion$default;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.ChildHandle");
    }
    
    public final Object awaitInternal$kotlinx_coroutines_core(final Continuation<Object> continuation) {
        Object state$kotlinx_coroutines_core;
        do {
            state$kotlinx_coroutines_core = this.getState$kotlinx_coroutines_core();
            if (!(state$kotlinx_coroutines_core instanceof Incomplete)) {
                if (!(state$kotlinx_coroutines_core instanceof CompletedExceptionally)) {
                    return JobSupportKt.unboxState(state$kotlinx_coroutines_core);
                }
                final Throwable cause = ((CompletedExceptionally)state$kotlinx_coroutines_core).cause;
                if (!DebugKt.getRECOVER_STACK_TRACES()) {
                    throw cause;
                }
                InlineMarker.mark(0);
                if (!(continuation instanceof CoroutineStackFrame)) {
                    throw cause;
                }
                throw StackTraceRecoveryKt.access$recoverFromStackFrame(cause, (CoroutineStackFrame)continuation);
            }
        } while (this.startInternal(state$kotlinx_coroutines_core) < 0);
        return this.awaitSuspend(continuation);
    }
    
    final /* synthetic */ Object awaitSuspend(final Continuation<Object> continuation) {
        final AwaitContinuation<Object> awaitContinuation = new AwaitContinuation<Object>(IntrinsicsKt__IntrinsicsJvmKt.intercepted(continuation), this);
        CancellableContinuationKt.disposeOnCancellation(awaitContinuation, this.invokeOnCompletion(new ResumeAwaitOnCompletion<Object>(this, awaitContinuation)));
        final Object result = awaitContinuation.getResult();
        if (result == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            DebugProbesKt.probeCoroutineSuspended(continuation);
        }
        return result;
    }
    
    @Override
    public void cancel(final CancellationException ex) {
        this.cancelInternal(ex);
    }
    
    public final boolean cancelCoroutine(final Throwable t) {
        return this.cancelImpl$kotlinx_coroutines_core(t);
    }
    
    public final boolean cancelImpl$kotlinx_coroutines_core(final Object o) {
        return (this.getOnCancelComplete$kotlinx_coroutines_core() && this.cancelMakeCompleting(o)) || this.makeCancelling(o);
    }
    
    public boolean cancelInternal(final Throwable t) {
        return this.cancelImpl$kotlinx_coroutines_core(t) && this.getHandlesException$kotlinx_coroutines_core();
    }
    
    public boolean childCancelled(final Throwable t) {
        Intrinsics.checkParameterIsNotNull(t, "cause");
        return t instanceof CancellationException || (this.cancelImpl$kotlinx_coroutines_core(t) && this.getHandlesException$kotlinx_coroutines_core());
    }
    
    @Override
    public <R> R fold(final R r, final Function2<? super R, ? super Element, ? extends R> function2) {
        Intrinsics.checkParameterIsNotNull(function2, "operation");
        return Job.DefaultImpls.fold(this, r, function2);
    }
    
    @Override
    public <E extends Element> E get(final CoroutineContext.Key<E> key) {
        Intrinsics.checkParameterIsNotNull(key, "key");
        return Job.DefaultImpls.get(this, key);
    }
    
    @Override
    public final CancellationException getCancellationException() {
        final Object state$kotlinx_coroutines_core = this.getState$kotlinx_coroutines_core();
        if (state$kotlinx_coroutines_core instanceof Finishing) {
            final Throwable rootCause = ((Finishing)state$kotlinx_coroutines_core).rootCause;
            if (rootCause != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(DebugStringsKt.getClassSimpleName(this));
                sb.append(" is cancelling");
                final CancellationException cancellationException = this.toCancellationException(rootCause, sb.toString());
                if (cancellationException != null) {
                    return cancellationException;
                }
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Job is still new or active: ");
            sb2.append(this);
            throw new IllegalStateException(sb2.toString().toString());
        }
        if (state$kotlinx_coroutines_core instanceof Incomplete) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Job is still new or active: ");
            sb3.append(this);
            throw new IllegalStateException(sb3.toString().toString());
        }
        if (state$kotlinx_coroutines_core instanceof CompletedExceptionally) {
            return toCancellationException$default(this, ((CompletedExceptionally)state$kotlinx_coroutines_core).cause, null, 1, null);
        }
        final StringBuilder sb4 = new StringBuilder();
        sb4.append(DebugStringsKt.getClassSimpleName(this));
        sb4.append(" has completed normally");
        return new JobCancellationException(sb4.toString(), null, this);
    }
    
    @Override
    public CancellationException getChildJobCancellationCause() {
        final Object state$kotlinx_coroutines_core = this.getState$kotlinx_coroutines_core();
        final boolean b = state$kotlinx_coroutines_core instanceof Finishing;
        CancellationException ex = null;
        Throwable t;
        if (b) {
            t = ((Finishing)state$kotlinx_coroutines_core).rootCause;
        }
        else if (state$kotlinx_coroutines_core instanceof CompletedExceptionally) {
            t = ((CompletedExceptionally)state$kotlinx_coroutines_core).cause;
        }
        else {
            if (state$kotlinx_coroutines_core instanceof Incomplete) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Cannot be cancelling child in this state: ");
                sb.append(state$kotlinx_coroutines_core);
                throw new IllegalStateException(sb.toString().toString());
            }
            t = null;
        }
        if (t instanceof CancellationException) {
            ex = (CancellationException)t;
        }
        final CancellationException ex2 = ex;
        if (ex2 != null) {
            return ex2;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Parent job is ");
        sb2.append(this.stateString(state$kotlinx_coroutines_core));
        return new JobCancellationException(sb2.toString(), t, this);
    }
    
    @Override
    public final Sequence<Job> getChildren() {
        return SequencesKt__SequenceBuilderKt.sequence((Function2<? super SequenceScope<? super Job>, ? super Continuation<? super Unit>, ?>)new JobSupport$children.JobSupport$children$1(this, (Continuation)null));
    }
    
    public final Object getCompletedInternal$kotlinx_coroutines_core() {
        final Object state$kotlinx_coroutines_core = this.getState$kotlinx_coroutines_core();
        if (!(state$kotlinx_coroutines_core instanceof Incomplete ^ true)) {
            throw new IllegalStateException("This job has not completed yet".toString());
        }
        if (!(state$kotlinx_coroutines_core instanceof CompletedExceptionally)) {
            return JobSupportKt.unboxState(state$kotlinx_coroutines_core);
        }
        throw ((CompletedExceptionally)state$kotlinx_coroutines_core).cause;
    }
    
    protected final Throwable getCompletionCause() {
        final Object state$kotlinx_coroutines_core = this.getState$kotlinx_coroutines_core();
        if (state$kotlinx_coroutines_core instanceof Finishing) {
            final Throwable rootCause = ((Finishing)state$kotlinx_coroutines_core).rootCause;
            if (rootCause != null) {
                return rootCause;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Job is still new or active: ");
            sb.append(this);
            throw new IllegalStateException(sb.toString().toString());
        }
        else {
            if (state$kotlinx_coroutines_core instanceof Incomplete) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Job is still new or active: ");
                sb2.append(this);
                throw new IllegalStateException(sb2.toString().toString());
            }
            if (state$kotlinx_coroutines_core instanceof CompletedExceptionally) {
                return ((CompletedExceptionally)state$kotlinx_coroutines_core).cause;
            }
            return null;
        }
    }
    
    protected final boolean getCompletionCauseHandled() {
        final Object state$kotlinx_coroutines_core = this.getState$kotlinx_coroutines_core();
        return state$kotlinx_coroutines_core instanceof CompletedExceptionally && ((CompletedExceptionally)state$kotlinx_coroutines_core).getHandled();
    }
    
    public final Throwable getCompletionExceptionOrNull() {
        final Object state$kotlinx_coroutines_core = this.getState$kotlinx_coroutines_core();
        if (state$kotlinx_coroutines_core instanceof Incomplete ^ true) {
            return this.getExceptionOrNull(state$kotlinx_coroutines_core);
        }
        throw new IllegalStateException("This job has not completed yet".toString());
    }
    
    public boolean getHandlesException$kotlinx_coroutines_core() {
        return true;
    }
    
    @Override
    public final CoroutineContext.Key<?> getKey() {
        return Job.Key;
    }
    
    public boolean getOnCancelComplete$kotlinx_coroutines_core() {
        return false;
    }
    
    @Override
    public final SelectClause0 getOnJoin() {
        return this;
    }
    
    public final Object getState$kotlinx_coroutines_core() {
        Object state;
        while (true) {
            state = this._state;
            if (!(state instanceof OpDescriptor)) {
                break;
            }
            ((OpDescriptor)state).perform(this);
        }
        return state;
    }
    
    protected boolean handleJobException(final Throwable t) {
        Intrinsics.checkParameterIsNotNull(t, "exception");
        return false;
    }
    
    public void handleOnCompletionException$kotlinx_coroutines_core(final Throwable t) {
        Intrinsics.checkParameterIsNotNull(t, "exception");
        throw t;
    }
    
    public final void initParentJobInternal$kotlinx_coroutines_core(final Job job) {
        if (DebugKt.getASSERTIONS_ENABLED() && this.parentHandle != null) {
            throw new AssertionError();
        }
        if (job == null) {
            this.parentHandle = NonDisposableHandle.INSTANCE;
            return;
        }
        job.start();
        final ChildHandle attachChild = job.attachChild(this);
        this.parentHandle = attachChild;
        if (this.isCompleted()) {
            attachChild.dispose();
            this.parentHandle = NonDisposableHandle.INSTANCE;
        }
    }
    
    @Override
    public final DisposableHandle invokeOnCompletion(final Function1<? super Throwable, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(function1, "handler");
        return this.invokeOnCompletion(false, true, function1);
    }
    
    @Override
    public final DisposableHandle invokeOnCompletion(final boolean b, final boolean b2, final Function1<? super Throwable, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(function1, "handler");
        final Throwable t = null;
        Object o = null;
    Label_0016:
        while (true) {
            final Object state$kotlinx_coroutines_core = this.getState$kotlinx_coroutines_core();
            if (state$kotlinx_coroutines_core instanceof Empty) {
                final Empty empty = (Empty)state$kotlinx_coroutines_core;
                if (empty.isActive()) {
                    Object node;
                    if (o != null) {
                        node = o;
                    }
                    else {
                        node = this.makeNode(function1, b);
                    }
                    o = node;
                    if (JobSupport._state$FU.compareAndSet(this, state$kotlinx_coroutines_core, node)) {
                        return (JobNode<?>)node;
                    }
                    continue;
                }
                else {
                    this.promoteEmptyToNodeList(empty);
                }
            }
            else {
                if (!(state$kotlinx_coroutines_core instanceof Incomplete)) {
                    if (b2) {
                        Object o2 = state$kotlinx_coroutines_core;
                        if (!(state$kotlinx_coroutines_core instanceof CompletedExceptionally)) {
                            o2 = null;
                        }
                        final CompletedExceptionally completedExceptionally = (CompletedExceptionally)o2;
                        Throwable cause = t;
                        if (completedExceptionally != null) {
                            cause = completedExceptionally.cause;
                        }
                        function1.invoke(cause);
                    }
                    return NonDisposableHandle.INSTANCE;
                }
                final NodeList list = ((Empty)state$kotlinx_coroutines_core).getList();
                if (list == null) {
                    if (state$kotlinx_coroutines_core == null) {
                        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.JobNode<*>");
                    }
                    this.promoteSingleToNodeList((JobNode<?>)state$kotlinx_coroutines_core);
                }
                else {
                    final Throwable t2 = null;
                    final NonDisposableHandle nonDisposableHandle = NonDisposableHandle.INSTANCE;
                    Object node2 = o;
                    Throwable rootCause = t2;
                    DisposableHandle disposableHandle = nonDisposableHandle;
                    if (b) {
                        node2 = o;
                        rootCause = t2;
                        disposableHandle = nonDisposableHandle;
                        if (state$kotlinx_coroutines_core instanceof Finishing) {
                        Label_0256_Outer:
                            while (true) {
                            Label_0264_Outer:
                                while (true) {
                                    while (true) {
                                        Label_0449: {
                                            synchronized (state$kotlinx_coroutines_core) {
                                                rootCause = ((Finishing)state$kotlinx_coroutines_core).rootCause;
                                                // iftrue(Label_0287:, this.addLastAtomic(state$kotlinx_coroutines_core, list, (JobNode<?>)o))
                                            Block_23_Outer:
                                                while (true) {
                                                    if (rootCause != null) {
                                                        node2 = o;
                                                        disposableHandle = nonDisposableHandle;
                                                        if (function1 instanceof ChildHandleNode) {
                                                            node2 = o;
                                                            disposableHandle = nonDisposableHandle;
                                                            if (!((Finishing)state$kotlinx_coroutines_core).isCompleting) {
                                                                break Label_0449;
                                                            }
                                                        }
                                                        o = Unit.INSTANCE;
                                                        break;
                                                    }
                                                    break Label_0449;
                                                    while (true) {
                                                        continue Label_0016;
                                                        o = this.makeNode(function1, b);
                                                        continue Label_0256_Outer;
                                                    }
                                                    Label_0303: {
                                                        disposableHandle = (JobNode<?>)o;
                                                    }
                                                    node2 = o;
                                                    continue Block_23_Outer;
                                                }
                                                Label_0287: {
                                                    return (JobNode<?>)o;
                                                }
                                            }
                                            // iftrue(Label_0303:, rootCause != null)
                                            break;
                                        }
                                        if (o != null) {
                                            continue;
                                        }
                                        break;
                                    }
                                    continue Label_0264_Outer;
                                }
                            }
                        }
                    }
                    if (rootCause != null) {
                        if (b2) {
                            function1.invoke(rootCause);
                        }
                        return disposableHandle;
                    }
                    if (node2 == null) {
                        node2 = this.makeNode(function1, b);
                    }
                    o = node2;
                    if (this.addLastAtomic(state$kotlinx_coroutines_core, list, (JobNode<?>)node2)) {
                        return (JobNode<?>)node2;
                    }
                    continue;
                }
            }
        }
    }
    
    @Override
    public boolean isActive() {
        final Object state$kotlinx_coroutines_core = this.getState$kotlinx_coroutines_core();
        return state$kotlinx_coroutines_core instanceof Incomplete && ((Incomplete)state$kotlinx_coroutines_core).isActive();
    }
    
    @Override
    public final boolean isCancelled() {
        final Object state$kotlinx_coroutines_core = this.getState$kotlinx_coroutines_core();
        return state$kotlinx_coroutines_core instanceof CompletedExceptionally || (state$kotlinx_coroutines_core instanceof Finishing && ((Finishing)state$kotlinx_coroutines_core).isCancelling());
    }
    
    @Override
    public final boolean isCompleted() {
        return this.getState$kotlinx_coroutines_core() instanceof Incomplete ^ true;
    }
    
    public final boolean isCompletedExceptionally() {
        return this.getState$kotlinx_coroutines_core() instanceof CompletedExceptionally;
    }
    
    protected boolean isScopedCoroutine() {
        return false;
    }
    
    @Override
    public final Object join(final Continuation<? super Unit> continuation) {
        if (!this.joinInternal()) {
            YieldKt.checkCompletion(continuation.getContext());
            return Unit.INSTANCE;
        }
        return this.joinSuspend(continuation);
    }
    
    final /* synthetic */ Object joinSuspend(final Continuation<? super Unit> continuation) {
        final CancellableContinuationImpl<Object> cancellableContinuationImpl = new CancellableContinuationImpl<Object>(IntrinsicsKt__IntrinsicsJvmKt.intercepted((Continuation<? super Object>)continuation), 1);
        final CancellableContinuationImpl<Object> cancellableContinuationImpl2 = cancellableContinuationImpl;
        CancellableContinuationKt.disposeOnCancellation(cancellableContinuationImpl2, this.invokeOnCompletion(new ResumeOnCompletion(this, cancellableContinuationImpl2)));
        final Object result = cancellableContinuationImpl.getResult();
        if (result == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            DebugProbesKt.probeCoroutineSuspended(continuation);
        }
        return result;
    }
    
    public final boolean makeCompleting$kotlinx_coroutines_core(final Object o) {
        boolean b;
        while (true) {
            final Object state$kotlinx_coroutines_core = this.getState$kotlinx_coroutines_core();
            b = false;
            final int tryMakeCompleting = this.tryMakeCompleting(state$kotlinx_coroutines_core, o, 0);
            if (tryMakeCompleting == 0) {
                break;
            }
            b = true;
            if (tryMakeCompleting == 1) {
                break;
            }
            b = b;
            if (tryMakeCompleting == 2) {
                break;
            }
            if (tryMakeCompleting == 3) {
                continue;
            }
            throw new IllegalStateException("unexpected result".toString());
        }
        return b;
    }
    
    public final boolean makeCompletingOnce$kotlinx_coroutines_core(final Object obj, final int n) {
        while (true) {
            final int tryMakeCompleting = this.tryMakeCompleting(this.getState$kotlinx_coroutines_core(), obj, n);
            if (tryMakeCompleting == 0) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Job ");
                sb.append(this);
                sb.append(" is already complete or completing, ");
                sb.append("but is being completed with ");
                sb.append(obj);
                throw new IllegalStateException(sb.toString(), this.getExceptionOrNull(obj));
            }
            if (tryMakeCompleting == 1) {
                return true;
            }
            if (tryMakeCompleting == 2) {
                return false;
            }
            if (tryMakeCompleting == 3) {
                continue;
            }
            throw new IllegalStateException("unexpected result".toString());
        }
    }
    
    @Override
    public CoroutineContext minusKey(final CoroutineContext.Key<?> key) {
        Intrinsics.checkParameterIsNotNull(key, "key");
        return Job.DefaultImpls.minusKey(this, key);
    }
    
    public String nameString$kotlinx_coroutines_core() {
        return DebugStringsKt.getClassSimpleName(this);
    }
    
    protected void onCancelling(final Throwable t) {
    }
    
    protected void onCompletionInternal(final Object o) {
    }
    
    public void onStartInternal$kotlinx_coroutines_core() {
    }
    
    @Override
    public final void parentCancelled(final ParentJob parentJob) {
        Intrinsics.checkParameterIsNotNull(parentJob, "parentJob");
        this.cancelImpl$kotlinx_coroutines_core(parentJob);
    }
    
    @Override
    public CoroutineContext plus(final CoroutineContext coroutineContext) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        return Job.DefaultImpls.plus(this, coroutineContext);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Operator '+' on two Job objects is meaningless. Job is a coroutine context element and `+` is a set-sum operator for coroutine contexts. The job to the right of `+` just replaces the job the left of `+`.")
    @Override
    public Job plus(final Job job) {
        Intrinsics.checkParameterIsNotNull(job, "other");
        return Job.DefaultImpls.plus(this, job);
    }
    
    @Override
    public final <R> void registerSelectClause0(final SelectInstance<? super R> selectInstance, final Function1<? super Continuation<? super R>, ?> function1) {
        Intrinsics.checkParameterIsNotNull(selectInstance, "select");
        Intrinsics.checkParameterIsNotNull(function1, "block");
        Object state$kotlinx_coroutines_core;
        do {
            state$kotlinx_coroutines_core = this.getState$kotlinx_coroutines_core();
            if (selectInstance.isSelected()) {
                return;
            }
            if (!(state$kotlinx_coroutines_core instanceof Incomplete)) {
                if (selectInstance.trySelect(null)) {
                    UndispatchedKt.startCoroutineUnintercepted((Function1<? super Continuation<? super Object>, ?>)function1, selectInstance.getCompletion());
                }
                return;
            }
        } while (this.startInternal(state$kotlinx_coroutines_core) != 0);
        selectInstance.disposeOnSelect(this.invokeOnCompletion(new SelectJoinOnCompletion<Object>(this, (SelectInstance<? super Object>)selectInstance, function1)));
    }
    
    public final <T, R> void registerSelectClause1Internal$kotlinx_coroutines_core(final SelectInstance<? super R> selectInstance, final Function2<? super T, ? super Continuation<? super R>, ?> function2) {
        Intrinsics.checkParameterIsNotNull(selectInstance, "select");
        Intrinsics.checkParameterIsNotNull(function2, "block");
        Object state$kotlinx_coroutines_core;
        do {
            state$kotlinx_coroutines_core = this.getState$kotlinx_coroutines_core();
            if (selectInstance.isSelected()) {
                return;
            }
            if (!(state$kotlinx_coroutines_core instanceof Incomplete)) {
                if (selectInstance.trySelect(null)) {
                    if (state$kotlinx_coroutines_core instanceof CompletedExceptionally) {
                        selectInstance.resumeSelectCancellableWithException(((CompletedExceptionally)state$kotlinx_coroutines_core).cause);
                        return;
                    }
                    UndispatchedKt.startCoroutineUnintercepted((Function2<? super Object, ? super Continuation<? super Object>, ?>)function2, JobSupportKt.unboxState(state$kotlinx_coroutines_core), selectInstance.getCompletion());
                }
                return;
            }
        } while (this.startInternal(state$kotlinx_coroutines_core) != 0);
        selectInstance.disposeOnSelect(this.invokeOnCompletion(new SelectAwaitOnCompletion<Object, Object>(this, (SelectInstance<? super Object>)selectInstance, (Function2<? super Object, ? super Continuation<? super Object>, ?>)function2)));
    }
    
    public final void removeNode$kotlinx_coroutines_core(final JobNode<?> jobNode) {
        Intrinsics.checkParameterIsNotNull(jobNode, "node");
        Object state$kotlinx_coroutines_core;
        do {
            state$kotlinx_coroutines_core = this.getState$kotlinx_coroutines_core();
            if (!(state$kotlinx_coroutines_core instanceof JobNode)) {
                if (state$kotlinx_coroutines_core instanceof Incomplete && ((JobNode<?>)state$kotlinx_coroutines_core).getList() != null) {
                    jobNode.remove();
                }
                return;
            }
            if (state$kotlinx_coroutines_core != jobNode) {
                return;
            }
        } while (!JobSupport._state$FU.compareAndSet(this, state$kotlinx_coroutines_core, JobSupportKt.access$getEMPTY_ACTIVE$p()));
    }
    
    public final <T, R> void selectAwaitCompletion$kotlinx_coroutines_core(final SelectInstance<? super R> selectInstance, final Function2<? super T, ? super Continuation<? super R>, ?> function2) {
        Intrinsics.checkParameterIsNotNull(selectInstance, "select");
        Intrinsics.checkParameterIsNotNull(function2, "block");
        final Object state$kotlinx_coroutines_core = this.getState$kotlinx_coroutines_core();
        if (state$kotlinx_coroutines_core instanceof CompletedExceptionally) {
            selectInstance.resumeSelectCancellableWithException(((CompletedExceptionally)state$kotlinx_coroutines_core).cause);
            return;
        }
        CancellableKt.startCoroutineCancellable((Function2<? super Object, ? super Continuation<? super Object>, ?>)function2, JobSupportKt.unboxState(state$kotlinx_coroutines_core), (Continuation<? super Object>)selectInstance.getCompletion());
    }
    
    @Override
    public final boolean start() {
        while (true) {
            final int startInternal = this.startInternal(this.getState$kotlinx_coroutines_core());
            if (startInternal == 0) {
                return false;
            }
            if (startInternal != 1) {
                continue;
            }
            return true;
        }
    }
    
    protected final CancellationException toCancellationException(final Throwable t, String string) {
        Intrinsics.checkParameterIsNotNull(t, "$this$toCancellationException");
        Throwable t2;
        if (!(t instanceof CancellationException)) {
            t2 = null;
        }
        else {
            t2 = t;
        }
        final CancellationException ex = (CancellationException)t2;
        if (ex != null) {
            return ex;
        }
        if (string == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(DebugStringsKt.getClassSimpleName(t));
            sb.append(" was cancelled");
            string = sb.toString();
        }
        return new JobCancellationException(string, t, this);
    }
    
    public final String toDebugString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.nameString$kotlinx_coroutines_core());
        sb.append('{');
        sb.append(this.stateString(this.getState$kotlinx_coroutines_core()));
        sb.append('}');
        return sb.toString();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.toDebugString());
        sb.append('@');
        sb.append(DebugStringsKt.getHexAddress(this));
        return sb.toString();
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000,\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B\u001b\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\b\u0010\f\u001a\u00020\rH\u0014R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e" }, d2 = { "Lkotlinx/coroutines/JobSupport$AwaitContinuation;", "T", "Lkotlinx/coroutines/CancellableContinuationImpl;", "delegate", "Lkotlin/coroutines/Continuation;", "job", "Lkotlinx/coroutines/JobSupport;", "(Lkotlin/coroutines/Continuation;Lkotlinx/coroutines/JobSupport;)V", "getContinuationCancellationCause", "", "parent", "Lkotlinx/coroutines/Job;", "nameString", "", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class AwaitContinuation<T> extends CancellableContinuationImpl<T>
    {
        private final JobSupport job;
        
        public AwaitContinuation(final Continuation<? super T> continuation, final JobSupport job) {
            Intrinsics.checkParameterIsNotNull(continuation, "delegate");
            Intrinsics.checkParameterIsNotNull(job, "job");
            super(continuation, 1);
            this.job = job;
        }
        
        @Override
        public Throwable getContinuationCancellationCause(final Job job) {
            Intrinsics.checkParameterIsNotNull(job, "parent");
            final Object state$kotlinx_coroutines_core = this.job.getState$kotlinx_coroutines_core();
            if (state$kotlinx_coroutines_core instanceof Finishing) {
                final Throwable rootCause = ((Finishing)state$kotlinx_coroutines_core).rootCause;
                if (rootCause != null) {
                    return rootCause;
                }
            }
            if (state$kotlinx_coroutines_core instanceof CompletedExceptionally) {
                return ((CompletedExceptionally)state$kotlinx_coroutines_core).cause;
            }
            return job.getCancellationException();
        }
        
        @Override
        protected String nameString() {
            return "AwaitContinuation";
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B'\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\u0010\t\u001a\u0004\u0018\u00010\n¢\u0006\u0002\u0010\u000bJ\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0096\u0002J\b\u0010\u0010\u001a\u00020\u0011H\u0016R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012" }, d2 = { "Lkotlinx/coroutines/JobSupport$ChildCompletion;", "Lkotlinx/coroutines/JobNode;", "Lkotlinx/coroutines/Job;", "parent", "Lkotlinx/coroutines/JobSupport;", "state", "Lkotlinx/coroutines/JobSupport$Finishing;", "child", "Lkotlinx/coroutines/ChildHandleNode;", "proposedUpdate", "", "(Lkotlinx/coroutines/JobSupport;Lkotlinx/coroutines/JobSupport$Finishing;Lkotlinx/coroutines/ChildHandleNode;Ljava/lang/Object;)V", "invoke", "", "cause", "", "toString", "", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class ChildCompletion extends JobNode<Job>
    {
        private final ChildHandleNode child;
        private final JobSupport parent;
        private final Object proposedUpdate;
        private final Finishing state;
        
        public ChildCompletion(final JobSupport parent, final Finishing state, final ChildHandleNode child, final Object proposedUpdate) {
            Intrinsics.checkParameterIsNotNull(parent, "parent");
            Intrinsics.checkParameterIsNotNull(state, "state");
            Intrinsics.checkParameterIsNotNull(child, "child");
            super(child.childJob);
            this.parent = parent;
            this.state = state;
            this.child = child;
            this.proposedUpdate = proposedUpdate;
        }
        
        @Override
        public void invoke(final Throwable t) {
            this.parent.continueCompleting(this.state, this.child, this.proposedUpdate);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("ChildCompletion[");
            sb.append(this.child);
            sb.append(", ");
            sb.append(this.proposedUpdate);
            sb.append(']');
            return sb.toString();
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0003\n\u0002\b\t\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0002\u0018\u00002\u00060\u0001j\u0002`\u00022\u00020\u0003B\u001f\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\u0002\u0010\nJ\u000e\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\tJ\u0018\u0010\u0015\u001a\u0012\u0012\u0004\u0012\u00020\t0\u0016j\b\u0012\u0004\u0012\u00020\t`\u0017H\u0002J\u0016\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\t0\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\tJ\b\u0010\u001b\u001a\u00020\u001cH\u0016R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u0001X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u00020\u00078VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\rR\u0011\u0010\u000e\u001a\u00020\u00078F¢\u0006\u0006\u001a\u0004\b\u000e\u0010\rR\u0012\u0010\u0006\u001a\u00020\u00078\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\u000f\u001a\u00020\u00078F¢\u0006\u0006\u001a\u0004\b\u000f\u0010\rR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\b\u001a\u0004\u0018\u00010\t8\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000¨\u0006\u001d" }, d2 = { "Lkotlinx/coroutines/JobSupport$Finishing;", "", "Lkotlinx/coroutines/internal/SynchronizedObject;", "Lkotlinx/coroutines/Incomplete;", "list", "Lkotlinx/coroutines/NodeList;", "isCompleting", "", "rootCause", "", "(Lkotlinx/coroutines/NodeList;ZLjava/lang/Throwable;)V", "_exceptionsHolder", "isActive", "()Z", "isCancelling", "isSealed", "getList", "()Lkotlinx/coroutines/NodeList;", "addExceptionLocked", "", "exception", "allocateList", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "sealLocked", "", "proposedException", "toString", "", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class Finishing implements Incomplete
    {
        private volatile Object _exceptionsHolder;
        public volatile boolean isCompleting;
        private final NodeList list;
        public volatile Throwable rootCause;
        
        public Finishing(final NodeList list, final boolean isCompleting, final Throwable rootCause) {
            Intrinsics.checkParameterIsNotNull(list, "list");
            this.list = list;
            this.isCompleting = isCompleting;
            this.rootCause = rootCause;
        }
        
        private final ArrayList<Throwable> allocateList() {
            return new ArrayList<Throwable>(4);
        }
        
        public final void addExceptionLocked(final Throwable t) {
            Intrinsics.checkParameterIsNotNull(t, "exception");
            final Throwable rootCause = this.rootCause;
            if (rootCause == null) {
                this.rootCause = t;
                return;
            }
            if (t == rootCause) {
                return;
            }
            final Object exceptionsHolder = this._exceptionsHolder;
            if (exceptionsHolder == null) {
                this._exceptionsHolder = t;
                return;
            }
            if (exceptionsHolder instanceof Throwable) {
                if (t == exceptionsHolder) {
                    return;
                }
                final ArrayList<Throwable> allocateList = this.allocateList();
                allocateList.add((Throwable)exceptionsHolder);
                allocateList.add(t);
                this._exceptionsHolder = allocateList;
            }
            else {
                if (exceptionsHolder instanceof ArrayList) {
                    ((ArrayList<Throwable>)exceptionsHolder).add(t);
                    return;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("State is ");
                sb.append(exceptionsHolder);
                throw new IllegalStateException(sb.toString().toString());
            }
        }
        
        @Override
        public NodeList getList() {
            return this.list;
        }
        
        @Override
        public boolean isActive() {
            return this.rootCause == null;
        }
        
        public final boolean isCancelling() {
            return this.rootCause != null;
        }
        
        public final boolean isSealed() {
            return this._exceptionsHolder == JobSupportKt.access$getSEALED$p();
        }
        
        public final List<Throwable> sealLocked(final Throwable e) {
            final Object exceptionsHolder = this._exceptionsHolder;
            List<E> list;
            if (exceptionsHolder == null) {
                list = (List<E>)this.allocateList();
            }
            else if (exceptionsHolder instanceof Throwable) {
                list = (List<E>)this.allocateList();
                ((ArrayList<ArrayList<Object>>)list).add((ArrayList<Object>)exceptionsHolder);
            }
            else {
                if (!(exceptionsHolder instanceof ArrayList)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("State is ");
                    sb.append(exceptionsHolder);
                    throw new IllegalStateException(sb.toString().toString());
                }
                list = (List<E>)exceptionsHolder;
            }
            final Throwable rootCause = this.rootCause;
            if (rootCause != null) {
                ((ArrayList<Throwable>)list).add(0, rootCause);
            }
            if (e != null && (Intrinsics.areEqual(e, rootCause) ^ true)) {
                ((ArrayList<Throwable>)list).add(e);
            }
            this._exceptionsHolder = JobSupportKt.access$getSEALED$p();
            return (ArrayList<Throwable>)list;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Finishing[cancelling=");
            sb.append(this.isCancelling());
            sb.append(", completing=");
            sb.append(this.isCompleting);
            sb.append(", rootCause=");
            sb.append(this.rootCause);
            sb.append(", exceptions=");
            sb.append(this._exceptionsHolder);
            sb.append(", list=");
            sb.append(this.getList());
            sb.append(']');
            return sb.toString();
        }
    }
}
