// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.TypeCastException;
import kotlinx.coroutines.internal.LockFreeLinkedListNode;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import kotlinx.coroutines.internal.LockFreeLinkedListHead;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u0005¢\u0006\u0002\u0010\u0003J\u000e\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u000bJ\b\u0010\r\u001a\u00020\u000bH\u0016R\u0014\u0010\u0004\u001a\u00020\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u00008VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\t¨\u0006\u000e" }, d2 = { "Lkotlinx/coroutines/NodeList;", "Lkotlinx/coroutines/internal/LockFreeLinkedListHead;", "Lkotlinx/coroutines/Incomplete;", "()V", "isActive", "", "()Z", "list", "getList", "()Lkotlinx/coroutines/NodeList;", "getString", "", "state", "toString", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class NodeList extends LockFreeLinkedListHead implements Incomplete
{
    @Override
    public NodeList getList() {
        return this;
    }
    
    public final String getString(String string) {
        Intrinsics.checkParameterIsNotNull(string, "state");
        final StringBuilder sb = new StringBuilder();
        sb.append("List{");
        sb.append(string);
        sb.append("}[");
        final Object next = this.getNext();
        if (next != null) {
            LockFreeLinkedListNode nextNode = (LockFreeLinkedListNode)next;
            int n = 1;
            while (Intrinsics.areEqual(nextNode, this) ^ true) {
                int n2 = n;
                if (nextNode instanceof JobNode) {
                    final JobNode obj = (JobNode)nextNode;
                    if (n != 0) {
                        n = 0;
                    }
                    else {
                        sb.append(", ");
                    }
                    sb.append(obj);
                    n2 = n;
                }
                nextNode = nextNode.getNextNode();
                n = n2;
            }
            sb.append("]");
            string = sb.toString();
            Intrinsics.checkExpressionValueIsNotNull(string, "StringBuilder().apply(builderAction).toString()");
            return string;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }
    
    @Override
    public boolean isActive() {
        return true;
    }
    
    @Override
    public String toString() {
        if (DebugKt.getDEBUG()) {
            return this.getString("Active");
        }
        return super.toString();
    }
}
