// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlinx.coroutines.internal.StackTraceRecoveryKt;
import java.util.concurrent.CancellationException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import kotlin.Metadata;
import kotlin.coroutines.jvm.internal.CoroutineStackFrame;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000¦\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0011\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00002\b\u0012\u0004\u0012\u00028\u00000\u00022\b\u0012\u0004\u0012\u00028\u00000\u00032\u00060\u0004j\u0002`\u0005B\u001d\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006\u0012\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b\n\u0010\u000bJ\u0019\u0010\u000f\u001a\u00020\u000e2\b\u0010\r\u001a\u0004\u0018\u00010\fH\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u0019\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011H\u0016¢\u0006\u0004\b\u0014\u0010\u0015J!\u0010\u0019\u001a\u00020\u000e2\b\u0010\u0016\u001a\u0004\u0018\u00010\f2\u0006\u0010\u0012\u001a\u00020\u0011H\u0010¢\u0006\u0004\b\u0017\u0010\u0018J\u0017\u0010\u001b\u001a\u00020\u000e2\u0006\u0010\u001a\u001a\u00020\fH\u0016¢\u0006\u0004\b\u001b\u0010\u0010J\u0017\u0010\u001d\u001a\u00020\u000e2\u0006\u0010\u001c\u001a\u00020\bH\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ\u000f\u0010\u001f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u001f\u0010 J\u0017\u0010#\u001a\u00020\u00112\u0006\u0010\"\u001a\u00020!H\u0016¢\u0006\u0004\b#\u0010$J\u0011\u0010%\u001a\u0004\u0018\u00010\fH\u0001¢\u0006\u0004\b%\u0010&J\u0017\u0010)\u001a\n\u0018\u00010'j\u0004\u0018\u0001`(H\u0016¢\u0006\u0004\b)\u0010*J\u001f\u0010-\u001a\u00028\u0001\"\u0004\b\u0001\u0010\u00012\b\u0010\u0016\u001a\u0004\u0018\u00010\fH\u0010¢\u0006\u0004\b+\u0010,J\u000f\u0010.\u001a\u00020\u000eH\u0016¢\u0006\u0004\b.\u0010 J\u000f\u0010/\u001a\u00020\u000eH\u0002¢\u0006\u0004\b/\u0010 J\u001e\u00102\u001a\u00020\u000e2\f\u00101\u001a\b\u0012\u0004\u0012\u00020\u000e00H\u0082\b¢\u0006\u0004\b2\u00103J8\u00109\u001a\u00020\u000e2'\u00108\u001a#\u0012\u0015\u0012\u0013\u0018\u00010\u0011¢\u0006\f\b5\u0012\b\b6\u0012\u0004\b\b(\u0012\u0012\u0004\u0012\u00020\u000e04j\u0002`7H\u0016¢\u0006\u0004\b9\u0010:J8\u0010<\u001a\u00020;2'\u00108\u001a#\u0012\u0015\u0012\u0013\u0018\u00010\u0011¢\u0006\f\b5\u0012\b\b6\u0012\u0004\b\b(\u0012\u0012\u0004\u0012\u00020\u000e04j\u0002`7H\u0002¢\u0006\u0004\b<\u0010=JB\u0010>\u001a\u00020\u000e2'\u00108\u001a#\u0012\u0015\u0012\u0013\u0018\u00010\u0011¢\u0006\f\b5\u0012\b\b6\u0012\u0004\b\b(\u0012\u0012\u0004\u0012\u00020\u000e04j\u0002`72\b\u0010\u0016\u001a\u0004\u0018\u00010\fH\u0002¢\u0006\u0004\b>\u0010?J\u000f\u0010A\u001a\u00020@H\u0014¢\u0006\u0004\bA\u0010BJ:\u0010E\u001a\u00020\u000e2\u0006\u0010C\u001a\u00028\u00002!\u0010D\u001a\u001d\u0012\u0013\u0012\u00110\u0011¢\u0006\f\b5\u0012\b\b6\u0012\u0004\b\b(\u0012\u0012\u0004\u0012\u00020\u000e04H\u0016¢\u0006\u0004\bE\u0010FJ#\u0010H\u001a\u0004\u0018\u00010G2\b\u0010\r\u001a\u0004\u0018\u00010\f2\u0006\u0010\t\u001a\u00020\bH\u0002¢\u0006\u0004\bH\u0010IJ \u0010L\u001a\u00020\u000e2\f\u0010K\u001a\b\u0012\u0004\u0012\u00028\u00000JH\u0016\u00f8\u0001\u0000¢\u0006\u0004\bL\u0010\u0010J!\u0010P\u001a\u0004\u0018\u00010G2\u0006\u0010M\u001a\u00020\u00112\u0006\u0010\u001c\u001a\u00020\bH\u0000¢\u0006\u0004\bN\u0010OJ\u0011\u0010R\u001a\u0004\u0018\u00010\fH\u0010¢\u0006\u0004\bQ\u0010&J\u000f\u0010S\u001a\u00020@H\u0016¢\u0006\u0004\bS\u0010BJ\u000f\u0010T\u001a\u00020\u0013H\u0002¢\u0006\u0004\bT\u0010UJ#\u0010T\u001a\u0004\u0018\u00010\f2\u0006\u0010C\u001a\u00028\u00002\b\u0010V\u001a\u0004\u0018\u00010\fH\u0016¢\u0006\u0004\bT\u0010WJ\u0019\u0010X\u001a\u0004\u0018\u00010\f2\u0006\u0010M\u001a\u00020\u0011H\u0016¢\u0006\u0004\bX\u0010YJ\u000f\u0010Z\u001a\u00020\u0013H\u0002¢\u0006\u0004\bZ\u0010UJ\u001b\u0010\\\u001a\u00020\u000e*\u00020[2\u0006\u0010C\u001a\u00028\u0000H\u0016¢\u0006\u0004\b\\\u0010]J\u001b\u0010^\u001a\u00020\u000e*\u00020[2\u0006\u0010M\u001a\u00020\u0011H\u0016¢\u0006\u0004\b^\u0010_R\u001e\u0010b\u001a\n\u0018\u00010\u0004j\u0004\u0018\u0001`\u00058V@\u0016X\u0096\u0004¢\u0006\u0006\u001a\u0004\b`\u0010aR\u001c\u0010d\u001a\u00020c8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\bd\u0010e\u001a\u0004\bf\u0010gR\"\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\u00068\u0000@\u0000X\u0080\u0004¢\u0006\f\n\u0004\b\u0007\u0010h\u001a\u0004\bi\u0010jR\u0016\u0010k\u001a\u00020\u00138V@\u0016X\u0096\u0004¢\u0006\u0006\u001a\u0004\bk\u0010UR\u0016\u0010l\u001a\u00020\u00138V@\u0016X\u0096\u0004¢\u0006\u0006\u001a\u0004\bl\u0010UR\u0016\u0010m\u001a\u00020\u00138V@\u0016X\u0096\u0004¢\u0006\u0006\u001a\u0004\bm\u0010UR\u0018\u0010o\u001a\u0004\u0018\u00010n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bo\u0010pR\u0018\u0010\u0016\u001a\u0004\u0018\u00010\f8@@\u0000X\u0080\u0004¢\u0006\u0006\u001a\u0004\bq\u0010&\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006r" }, d2 = { "Lkotlinx/coroutines/CancellableContinuationImpl;", "T", "Lkotlinx/coroutines/DispatchedTask;", "Lkotlinx/coroutines/CancellableContinuation;", "Lkotlin/coroutines/jvm/internal/CoroutineStackFrame;", "Lkotlinx/coroutines/internal/CoroutineStackFrame;", "Lkotlin/coroutines/Continuation;", "delegate", "", "resumeMode", "<init>", "(Lkotlin/coroutines/Continuation;I)V", "", "proposedUpdate", "", "alreadyResumedError", "(Ljava/lang/Object;)V", "", "cause", "", "cancel", "(Ljava/lang/Throwable;)Z", "state", "cancelResult$kotlinx_coroutines_core", "(Ljava/lang/Object;Ljava/lang/Throwable;)V", "cancelResult", "token", "completeResume", "mode", "dispatchResume", "(I)V", "disposeParentHandle", "()V", "Lkotlinx/coroutines/Job;", "parent", "getContinuationCancellationCause", "(Lkotlinx/coroutines/Job;)Ljava/lang/Throwable;", "getResult", "()Ljava/lang/Object;", "Ljava/lang/StackTraceElement;", "Lkotlinx/coroutines/internal/StackTraceElement;", "getStackTraceElement", "()Ljava/lang/StackTraceElement;", "getSuccessfulResult$kotlinx_coroutines_core", "(Ljava/lang/Object;)Ljava/lang/Object;", "getSuccessfulResult", "initCancellability", "installParentCancellationHandler", "Lkotlin/Function0;", "block", "invokeHandlerSafely", "(Lkotlin/jvm/functions/Function0;)V", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "Lkotlinx/coroutines/CompletionHandler;", "handler", "invokeOnCancellation", "(Lkotlin/jvm/functions/Function1;)V", "Lkotlinx/coroutines/CancelHandler;", "makeHandler", "(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/CancelHandler;", "multipleHandlersError", "(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V", "", "nameString", "()Ljava/lang/String;", "value", "onCancellation", "resume", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)V", "Lkotlinx/coroutines/CancelledContinuation;", "resumeImpl", "(Ljava/lang/Object;I)Lkotlinx/coroutines/CancelledContinuation;", "Lkotlin/Result;", "result", "resumeWith", "exception", "resumeWithExceptionMode$kotlinx_coroutines_core", "(Ljava/lang/Throwable;I)Lkotlinx/coroutines/CancelledContinuation;", "resumeWithExceptionMode", "takeState$kotlinx_coroutines_core", "takeState", "toString", "tryResume", "()Z", "idempotent", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "tryResumeWithException", "(Ljava/lang/Throwable;)Ljava/lang/Object;", "trySuspend", "Lkotlinx/coroutines/CoroutineDispatcher;", "resumeUndispatched", "(Lkotlinx/coroutines/CoroutineDispatcher;Ljava/lang/Object;)V", "resumeUndispatchedWithException", "(Lkotlinx/coroutines/CoroutineDispatcher;Ljava/lang/Throwable;)V", "getCallerFrame", "()Lkotlin/coroutines/jvm/internal/CoroutineStackFrame;", "callerFrame", "Lkotlin/coroutines/CoroutineContext;", "context", "Lkotlin/coroutines/CoroutineContext;", "getContext", "()Lkotlin/coroutines/CoroutineContext;", "Lkotlin/coroutines/Continuation;", "getDelegate$kotlinx_coroutines_core", "()Lkotlin/coroutines/Continuation;", "isActive", "isCancelled", "isCompleted", "Lkotlinx/coroutines/DisposableHandle;", "parentHandle", "Lkotlinx/coroutines/DisposableHandle;", "getState$kotlinx_coroutines_core", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public class CancellableContinuationImpl<T> extends DispatchedTask<T> implements CancellableContinuation<T>, CoroutineStackFrame
{
    private static final AtomicIntegerFieldUpdater _decision$FU;
    private static final AtomicReferenceFieldUpdater _state$FU;
    private volatile int _decision;
    private volatile Object _state;
    private final CoroutineContext context;
    private final Continuation<T> delegate;
    private volatile DisposableHandle parentHandle;
    
    static {
        _decision$FU = AtomicIntegerFieldUpdater.newUpdater(CancellableContinuationImpl.class, "_decision");
        _state$FU = AtomicReferenceFieldUpdater.newUpdater(CancellableContinuationImpl.class, Object.class, "_state");
    }
    
    public CancellableContinuationImpl(final Continuation<? super T> delegate, final int n) {
        Intrinsics.checkParameterIsNotNull(delegate, "delegate");
        super(n);
        this.delegate = (Continuation<T>)delegate;
        this.context = this.delegate.getContext();
        this._decision = 0;
        this._state = Active.INSTANCE;
    }
    
    private final void alreadyResumedError(final Object obj) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Already resumed, but proposed with update ");
        sb.append(obj);
        throw new IllegalStateException(sb.toString().toString());
    }
    
    private final void dispatchResume(final int n) {
        if (this.tryResume()) {
            return;
        }
        DispatchedKt.dispatch((DispatchedTask<? super Object>)this, n);
    }
    
    private final void disposeParentHandle() {
        final DisposableHandle parentHandle = this.parentHandle;
        if (parentHandle != null) {
            parentHandle.dispose();
            this.parentHandle = NonDisposableHandle.INSTANCE;
        }
    }
    
    private final void installParentCancellationHandler() {
        if (this.isCompleted()) {
            return;
        }
        final Job job = this.delegate.getContext().get((CoroutineContext.Key<Job>)Job.Key);
        if (job != null) {
            job.start();
            final DisposableHandle invokeOnCompletion$default = Job.DefaultImpls.invokeOnCompletion$default(job, true, false, new ChildContinuation(job, this), 2, null);
            this.parentHandle = invokeOnCompletion$default;
            if (this.isCompleted()) {
                invokeOnCompletion$default.dispose();
                this.parentHandle = NonDisposableHandle.INSTANCE;
            }
        }
    }
    
    private final void invokeHandlerSafely(final Function0<Unit> function0) {
        try {
            function0.invoke();
        }
        finally {
            final CoroutineContext context = this.getContext();
            final StringBuilder sb = new StringBuilder();
            sb.append("Exception in cancellation handler for ");
            sb.append(this);
            final Throwable t;
            CoroutineExceptionHandlerKt.handleCoroutineException(context, new CompletionHandlerException(sb.toString(), t));
        }
    }
    
    private final CancelHandler makeHandler(final Function1<? super Throwable, Unit> function1) {
        if (function1 instanceof CancelHandler) {
            return (CancelHandler)function1;
        }
        return new InvokeOnCancel(function1);
    }
    
    private final void multipleHandlersError(final Function1<? super Throwable, Unit> obj, final Object obj2) {
        final StringBuilder sb = new StringBuilder();
        sb.append("It's prohibited to register multiple handlers, tried to register ");
        sb.append(obj);
        sb.append(", already has ");
        sb.append(obj2);
        throw new IllegalStateException(sb.toString().toString());
    }
    
    private final CancelledContinuation resumeImpl(final Object o, final int n) {
        while (true) {
            final Object state = this._state;
            if (state instanceof NotCompleted) {
                if (!CancellableContinuationImpl._state$FU.compareAndSet(this, state, o)) {
                    continue;
                }
                this.disposeParentHandle();
                this.dispatchResume(n);
                return null;
            }
            else {
                if (state instanceof CancelledContinuation) {
                    final CancelledContinuation cancelledContinuation = (CancelledContinuation)state;
                    if (cancelledContinuation.makeResumed()) {
                        return cancelledContinuation;
                    }
                }
                this.alreadyResumedError(o);
            }
        }
    }
    
    private final boolean tryResume() {
        do {
            final int decision = this._decision;
            if (decision != 0) {
                if (decision == 1) {
                    return false;
                }
                throw new IllegalStateException("Already resumed".toString());
            }
        } while (!CancellableContinuationImpl._decision$FU.compareAndSet(this, 0, 2));
        return true;
    }
    
    private final boolean trySuspend() {
        do {
            final int decision = this._decision;
            if (decision != 0) {
                if (decision == 2) {
                    return false;
                }
                throw new IllegalStateException("Already suspended".toString());
            }
        } while (!CancellableContinuationImpl._decision$FU.compareAndSet(this, 0, 1));
        return true;
    }
    
    @Override
    public boolean cancel(final Throwable t) {
        while (true) {
            final Object state = this._state;
            if (!(state instanceof NotCompleted)) {
                return false;
            }
            final CancellableContinuationImpl cancellableContinuationImpl = this;
            final boolean b = state instanceof CancelHandler;
            if (!CancellableContinuationImpl._state$FU.compareAndSet(this, state, new CancelledContinuation(cancellableContinuationImpl, t, b))) {
                continue;
            }
            if (b) {
                try {
                    ((CancelHandler)state).invoke(t);
                }
                finally {
                    final CoroutineContext context = this.getContext();
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Exception in cancellation handler for ");
                    sb.append(this);
                    CoroutineExceptionHandlerKt.handleCoroutineException(context, new CompletionHandlerException(sb.toString(), t));
                }
            }
            this.disposeParentHandle();
            this.dispatchResume(0);
            return true;
        }
    }
    
    @Override
    public void cancelResult$kotlinx_coroutines_core(final Object o, final Throwable t) {
        Intrinsics.checkParameterIsNotNull(t, "cause");
        if (o instanceof CompletedWithCancellation) {
            try {
                ((CompletedWithCancellation)o).onCancellation.invoke(t);
            }
            finally {
                final CoroutineContext context = this.getContext();
                final StringBuilder sb = new StringBuilder();
                sb.append("Exception in cancellation handler for ");
                sb.append(this);
                final Throwable t2;
                CoroutineExceptionHandlerKt.handleCoroutineException(context, new CompletionHandlerException(sb.toString(), t2));
            }
        }
    }
    
    @Override
    public void completeResume(final Object o) {
        Intrinsics.checkParameterIsNotNull(o, "token");
        this.dispatchResume(this.resumeMode);
    }
    
    @Override
    public CoroutineStackFrame getCallerFrame() {
        Object delegate;
        if (!((delegate = this.delegate) instanceof CoroutineStackFrame)) {
            delegate = null;
        }
        return (CoroutineStackFrame)delegate;
    }
    
    @Override
    public CoroutineContext getContext() {
        return this.context;
    }
    
    public Throwable getContinuationCancellationCause(final Job job) {
        Intrinsics.checkParameterIsNotNull(job, "parent");
        return job.getCancellationException();
    }
    
    @Override
    public final Continuation<T> getDelegate$kotlinx_coroutines_core() {
        return this.delegate;
    }
    
    public final Object getResult() {
        this.installParentCancellationHandler();
        if (this.trySuspend()) {
            return IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
        }
        final Object state$kotlinx_coroutines_core = this.getState$kotlinx_coroutines_core();
        if (!(state$kotlinx_coroutines_core instanceof CompletedExceptionally)) {
            if (this.resumeMode == 1) {
                final Job job = this.getContext().get((CoroutineContext.Key<Job>)Job.Key);
                if (job != null) {
                    if (!job.isActive()) {
                        final CancellationException ex = job.getCancellationException();
                        this.cancelResult$kotlinx_coroutines_core(state$kotlinx_coroutines_core, ex);
                        throw StackTraceRecoveryKt.recoverStackTrace(ex, this);
                    }
                }
            }
            return this.getSuccessfulResult$kotlinx_coroutines_core(state$kotlinx_coroutines_core);
        }
        throw StackTraceRecoveryKt.recoverStackTrace(((CompletedExceptionally)state$kotlinx_coroutines_core).cause, this);
    }
    
    @Override
    public StackTraceElement getStackTraceElement() {
        return null;
    }
    
    public final Object getState$kotlinx_coroutines_core() {
        return this._state;
    }
    
    @Override
    public <T> T getSuccessfulResult$kotlinx_coroutines_core(final Object o) {
        if (o instanceof CompletedIdempotentResult) {
            return (T)((CompletedIdempotentResult)o).result;
        }
        Object result = o;
        if (o instanceof CompletedWithCancellation) {
            result = ((CompletedWithCancellation)o).result;
        }
        return (T)result;
    }
    
    @Override
    public void invokeOnCancellation(final Function1<? super Throwable, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(function1, "handler");
        final Throwable t = null;
        CancelHandler cancelHandler = null;
        while (true) {
            final Object state = this._state;
            if (state instanceof Active) {
                CancelHandler handler;
                if (cancelHandler != null) {
                    handler = cancelHandler;
                }
                else {
                    handler = this.makeHandler(function1);
                }
                cancelHandler = handler;
                if (CancellableContinuationImpl._state$FU.compareAndSet(this, state, handler)) {
                    return;
                }
                continue;
            }
            else {
                if (!(state instanceof CancelHandler)) {
                    if (state instanceof CancelledContinuation) {
                        if (!((CancelledContinuation)state).makeHandled()) {
                            this.multipleHandlersError(function1, state);
                        }
                        CompletedExceptionally completedExceptionally = (CompletedExceptionally)state;
                        try {
                            if (!(state instanceof CompletedExceptionally)) {
                                completedExceptionally = null;
                            }
                            final CompletedExceptionally completedExceptionally2 = completedExceptionally;
                            Throwable cause = t;
                            if (completedExceptionally2 != null) {
                                cause = completedExceptionally2.cause;
                            }
                            function1.invoke(cause);
                        }
                        finally {
                            final CoroutineContext context = this.getContext();
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Exception in cancellation handler for ");
                            sb.append(this);
                            final Throwable t2;
                            CoroutineExceptionHandlerKt.handleCoroutineException(context, new CompletionHandlerException(sb.toString(), t2));
                        }
                    }
                    return;
                }
                this.multipleHandlersError(function1, state);
            }
        }
    }
    
    @Override
    public boolean isActive() {
        return this.getState$kotlinx_coroutines_core() instanceof NotCompleted;
    }
    
    @Override
    public boolean isCancelled() {
        return this.getState$kotlinx_coroutines_core() instanceof CancelledContinuation;
    }
    
    @Override
    public boolean isCompleted() {
        return this.getState$kotlinx_coroutines_core() instanceof NotCompleted ^ true;
    }
    
    protected String nameString() {
        return "CancellableContinuation";
    }
    
    @Override
    public void resume(final T t, final Function1<? super Throwable, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(function1, "onCancellation");
        final CancelledContinuation resumeImpl = this.resumeImpl(new CompletedWithCancellation(t, function1), this.resumeMode);
        if (resumeImpl != null) {
            try {
                function1.invoke(resumeImpl.cause);
            }
            finally {
                final CoroutineContext context = this.getContext();
                final StringBuilder sb = new StringBuilder();
                sb.append("Exception in cancellation handler for ");
                sb.append(this);
                final Throwable t2;
                CoroutineExceptionHandlerKt.handleCoroutineException(context, new CompletionHandlerException(sb.toString(), t2));
            }
        }
    }
    
    @Override
    public void resumeUndispatched(final CoroutineDispatcher coroutineDispatcher, final T t) {
        Intrinsics.checkParameterIsNotNull(coroutineDispatcher, "$this$resumeUndispatched");
        Continuation<T> delegate = this.delegate;
        final boolean b = delegate instanceof DispatchedContinuation;
        final CoroutineDispatcher coroutineDispatcher2 = null;
        if (!b) {
            delegate = null;
        }
        final DispatchedContinuation dispatchedContinuation = (DispatchedContinuation<T>)delegate;
        CoroutineDispatcher dispatcher = coroutineDispatcher2;
        if (dispatchedContinuation != null) {
            dispatcher = dispatchedContinuation.dispatcher;
        }
        int resumeMode;
        if (dispatcher == coroutineDispatcher) {
            resumeMode = 3;
        }
        else {
            resumeMode = this.resumeMode;
        }
        this.resumeImpl(t, resumeMode);
    }
    
    @Override
    public void resumeUndispatchedWithException(final CoroutineDispatcher coroutineDispatcher, final Throwable t) {
        Intrinsics.checkParameterIsNotNull(coroutineDispatcher, "$this$resumeUndispatchedWithException");
        Intrinsics.checkParameterIsNotNull(t, "exception");
        Continuation<T> delegate = this.delegate;
        final boolean b = delegate instanceof DispatchedContinuation;
        final CoroutineDispatcher coroutineDispatcher2 = null;
        if (!b) {
            delegate = null;
        }
        final DispatchedContinuation dispatchedContinuation = (DispatchedContinuation<T>)delegate;
        final CompletedExceptionally completedExceptionally = new CompletedExceptionally(t, false, 2, null);
        CoroutineDispatcher dispatcher = coroutineDispatcher2;
        if (dispatchedContinuation != null) {
            dispatcher = dispatchedContinuation.dispatcher;
        }
        int resumeMode;
        if (dispatcher == coroutineDispatcher) {
            resumeMode = 3;
        }
        else {
            resumeMode = this.resumeMode;
        }
        this.resumeImpl(completedExceptionally, resumeMode);
    }
    
    @Override
    public void resumeWith(final Object o) {
        this.resumeImpl(CompletedExceptionallyKt.toState(o), this.resumeMode);
    }
    
    public final CancelledContinuation resumeWithExceptionMode$kotlinx_coroutines_core(final Throwable t, final int n) {
        Intrinsics.checkParameterIsNotNull(t, "exception");
        return this.resumeImpl(new CompletedExceptionally(t, false, 2, null), n);
    }
    
    @Override
    public Object takeState$kotlinx_coroutines_core() {
        return this.getState$kotlinx_coroutines_core();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.nameString());
        sb.append('(');
        sb.append(DebugStringsKt.toDebugString(this.delegate));
        sb.append("){");
        sb.append(this.getState$kotlinx_coroutines_core());
        sb.append("}@");
        sb.append(DebugStringsKt.getHexAddress(this));
        return sb.toString();
    }
    
    @Override
    public Object tryResume(final T t, final Object o) {
        while (true) {
            final Object state = this._state;
            if (!(state instanceof NotCompleted)) {
                final boolean b = state instanceof CompletedIdempotentResult;
                Object token = null;
                if (b) {
                    final CompletedIdempotentResult completedIdempotentResult = (CompletedIdempotentResult)state;
                    token = token;
                    if (completedIdempotentResult.idempotentResume == o) {
                        if (DebugKt.getASSERTIONS_ENABLED() && completedIdempotentResult.result != t) {
                            throw new AssertionError();
                        }
                        token = completedIdempotentResult.token;
                    }
                }
                return token;
            }
            Object o2;
            if (o == null) {
                o2 = t;
            }
            else {
                o2 = new CompletedIdempotentResult(o, t, (NotCompleted)state);
            }
            if (!CancellableContinuationImpl._state$FU.compareAndSet(this, state, o2)) {
                continue;
            }
            this.disposeParentHandle();
            return state;
        }
    }
    
    @Override
    public Object tryResumeWithException(final Throwable t) {
        Intrinsics.checkParameterIsNotNull(t, "exception");
        while (true) {
            final Object state = this._state;
            if (!(state instanceof NotCompleted)) {
                return null;
            }
            if (!CancellableContinuationImpl._state$FU.compareAndSet(this, state, new CompletedExceptionally(t, false, 2, null))) {
                continue;
            }
            this.disposeParentHandle();
            return state;
        }
    }
}
