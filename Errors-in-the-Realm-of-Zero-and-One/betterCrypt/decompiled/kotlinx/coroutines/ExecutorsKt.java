// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import java.util.concurrent.ExecutorService;
import kotlin.jvm.internal.Intrinsics;
import java.util.concurrent.Executor;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u0011\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0007¢\u0006\u0002\b\u0003\u001a\u0011\u0010\u0000\u001a\u00020\u0004*\u00020\u0005H\u0007¢\u0006\u0002\b\u0003\u001a\n\u0010\u0006\u001a\u00020\u0002*\u00020\u0001¨\u0006\u0007" }, d2 = { "asCoroutineDispatcher", "Lkotlinx/coroutines/CoroutineDispatcher;", "Ljava/util/concurrent/Executor;", "from", "Lkotlinx/coroutines/ExecutorCoroutineDispatcher;", "Ljava/util/concurrent/ExecutorService;", "asExecutor", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class ExecutorsKt
{
    public static final Executor asExecutor(final CoroutineDispatcher coroutineDispatcher) {
        Intrinsics.checkParameterIsNotNull(coroutineDispatcher, "$this$asExecutor");
        CoroutineDispatcher coroutineDispatcher2;
        if (!(coroutineDispatcher instanceof ExecutorCoroutineDispatcher)) {
            coroutineDispatcher2 = null;
        }
        else {
            coroutineDispatcher2 = coroutineDispatcher;
        }
        final ExecutorCoroutineDispatcher executorCoroutineDispatcher = (ExecutorCoroutineDispatcher)coroutineDispatcher2;
        if (executorCoroutineDispatcher != null) {
            final Executor executor = executorCoroutineDispatcher.getExecutor();
            if (executor != null) {
                return executor;
            }
        }
        return new DispatcherExecutor(coroutineDispatcher);
    }
    
    public static final CoroutineDispatcher from(final Executor executor) {
        Intrinsics.checkParameterIsNotNull(executor, "$this$asCoroutineDispatcher");
        Executor executor2;
        if (!(executor instanceof DispatcherExecutor)) {
            executor2 = null;
        }
        else {
            executor2 = executor;
        }
        final DispatcherExecutor dispatcherExecutor = (DispatcherExecutor)executor2;
        if (dispatcherExecutor != null) {
            final CoroutineDispatcher dispatcher = dispatcherExecutor.dispatcher;
            if (dispatcher != null) {
                return dispatcher;
            }
        }
        return new ExecutorCoroutineDispatcherImpl(executor);
    }
    
    public static final ExecutorCoroutineDispatcher from(final ExecutorService executorService) {
        Intrinsics.checkParameterIsNotNull(executorService, "$this$asCoroutineDispatcher");
        return new ExecutorCoroutineDispatcherImpl(executorService);
    }
}
