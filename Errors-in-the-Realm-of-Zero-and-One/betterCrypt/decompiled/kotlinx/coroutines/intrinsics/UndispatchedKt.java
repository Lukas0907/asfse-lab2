// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.intrinsics;

import kotlin.jvm.functions.Function0;
import kotlinx.coroutines.TimeoutCancellationException;
import kotlinx.coroutines.JobSupportKt;
import kotlinx.coroutines.internal.ScopesKt;
import kotlinx.coroutines.CompletedExceptionally;
import kotlinx.coroutines.AbstractCoroutine;
import kotlin.jvm.functions.Function2;
import kotlin.ResultKt;
import kotlin.Result;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.jvm.internal.TypeIntrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.internal.ThreadContextKt;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function1;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000@\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a9\u0010\u0000\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u00022\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u0002H\u00020\u00042\u001a\u0010\u0005\u001a\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u0006H\u0082\b\u001a>\u0010\b\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\u0018\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u00062\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0004H\u0000\u00f8\u0001\u0000¢\u0006\u0002\u0010\t\u001aR\u0010\b\u001a\u00020\u0001\"\u0004\b\u0000\u0010\n\"\u0004\b\u0001\u0010\u0002*\u001e\b\u0001\u0012\u0004\u0012\u0002H\n\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u000b2\u0006\u0010\f\u001a\u0002H\n2\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0004H\u0000\u00f8\u0001\u0000¢\u0006\u0002\u0010\r\u001a>\u0010\u000e\u001a\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\u0018\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u00062\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0004H\u0000\u00f8\u0001\u0000¢\u0006\u0002\u0010\t\u001aR\u0010\u000e\u001a\u00020\u0001\"\u0004\b\u0000\u0010\n\"\u0004\b\u0001\u0010\u0002*\u001e\b\u0001\u0012\u0004\u0012\u0002H\n\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u000b2\u0006\u0010\f\u001a\u0002H\n2\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0004H\u0000\u00f8\u0001\u0000¢\u0006\u0002\u0010\r\u001aY\u0010\u000f\u001a\u0004\u0018\u00010\u0007\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\n*\b\u0012\u0004\u0012\u0002H\u00020\u00102\u0006\u0010\f\u001a\u0002H\n2'\u0010\u0005\u001a#\b\u0001\u0012\u0004\u0012\u0002H\n\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u000b¢\u0006\u0002\b\u0011H\u0000\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0012\u001aY\u0010\u0013\u001a\u0004\u0018\u00010\u0007\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\n*\b\u0012\u0004\u0012\u0002H\u00020\u00102\u0006\u0010\f\u001a\u0002H\n2'\u0010\u0005\u001a#\b\u0001\u0012\u0004\u0012\u0002H\n\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u000b¢\u0006\u0002\b\u0011H\u0000\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0012\u001a?\u0010\u0014\u001a\u0004\u0018\u00010\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00102\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00170\u00062\u000e\u0010\u0018\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u0019H\u0082\b\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u001a" }, d2 = { "startDirect", "", "T", "completion", "Lkotlin/coroutines/Continuation;", "block", "Lkotlin/Function1;", "", "startCoroutineUndispatched", "(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V", "R", "Lkotlin/Function2;", "receiver", "(Lkotlin/jvm/functions/Function2;Ljava/lang/Object;Lkotlin/coroutines/Continuation;)V", "startCoroutineUnintercepted", "startUndispatchedOrReturn", "Lkotlinx/coroutines/AbstractCoroutine;", "Lkotlin/ExtensionFunctionType;", "(Lkotlinx/coroutines/AbstractCoroutine;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;", "startUndispatchedOrReturnIgnoreTimeout", "undispatchedResult", "shouldThrow", "", "", "startBlock", "Lkotlin/Function0;", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class UndispatchedKt
{
    public static final <T> void startCoroutineUndispatched(final Function1<? super Continuation<? super T>, ?> function1, Continuation<? super T> o) {
        Intrinsics.checkParameterIsNotNull(function1, "$this$startCoroutineUndispatched");
        Intrinsics.checkParameterIsNotNull(o, "completion");
        final Continuation<Object> probeCoroutineCreated = DebugProbesKt.probeCoroutineCreated((Continuation<? super Object>)o);
        try {
            o = ((Continuation)o).getContext();
            final Object updateThreadContext = ThreadContextKt.updateThreadContext((CoroutineContext)o, null);
            try {
                final Object invoke = ((Function1)TypeIntrinsics.beforeCheckcastToFunctionOfArity(function1, 1)).invoke(probeCoroutineCreated);
                ThreadContextKt.restoreThreadContext((CoroutineContext)o, updateThreadContext);
                if (invoke != IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                    o = Result.Companion;
                    probeCoroutineCreated.resumeWith(Result.constructor-impl(invoke));
                }
            }
            finally {
                ThreadContextKt.restoreThreadContext((CoroutineContext)o, updateThreadContext);
            }
        }
        finally {
            final Result.Companion companion = Result.Companion;
            final Throwable t;
            probeCoroutineCreated.resumeWith(Result.constructor-impl(ResultKt.createFailure(t)));
        }
    }
    
    public static final <R, T> void startCoroutineUndispatched(final Function2<? super R, ? super Continuation<? super T>, ?> function2, final R r, Continuation<? super T> context) {
        Intrinsics.checkParameterIsNotNull(function2, "$this$startCoroutineUndispatched");
        Intrinsics.checkParameterIsNotNull(context, "completion");
        final Continuation<Object> probeCoroutineCreated = DebugProbesKt.probeCoroutineCreated((Continuation<? super Object>)context);
        try {
            context = ((Continuation)context).getContext();
            final Object updateThreadContext = ThreadContextKt.updateThreadContext(context, null);
            try {
                final Object invoke = ((Function2)TypeIntrinsics.beforeCheckcastToFunctionOfArity(function2, 2)).invoke(r, probeCoroutineCreated);
                ThreadContextKt.restoreThreadContext(context, updateThreadContext);
                if (invoke != IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                    final Result.Companion companion = Result.Companion;
                    probeCoroutineCreated.resumeWith(Result.constructor-impl(invoke));
                }
            }
            finally {
                ThreadContextKt.restoreThreadContext(context, updateThreadContext);
            }
        }
        finally {
            final Result.Companion companion2 = Result.Companion;
            final Throwable t;
            probeCoroutineCreated.resumeWith(Result.constructor-impl(ResultKt.createFailure(t)));
        }
    }
    
    public static final <T> void startCoroutineUnintercepted(final Function1<? super Continuation<? super T>, ?> function1, Continuation<? super T> probeCoroutineCreated) {
        Intrinsics.checkParameterIsNotNull(function1, "$this$startCoroutineUnintercepted");
        Intrinsics.checkParameterIsNotNull(probeCoroutineCreated, "completion");
        probeCoroutineCreated = DebugProbesKt.probeCoroutineCreated(probeCoroutineCreated);
        try {
            final Object invoke = ((Function1)TypeIntrinsics.beforeCheckcastToFunctionOfArity(function1, 1)).invoke(probeCoroutineCreated);
            if (invoke != IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                final Result.Companion companion = Result.Companion;
                probeCoroutineCreated.resumeWith(Result.constructor-impl(invoke));
            }
        }
        finally {
            final Result.Companion companion2 = Result.Companion;
            final Throwable t;
            probeCoroutineCreated.resumeWith(Result.constructor-impl(ResultKt.createFailure(t)));
        }
    }
    
    public static final <R, T> void startCoroutineUnintercepted(final Function2<? super R, ? super Continuation<? super T>, ?> function2, final R r, Continuation<? super T> probeCoroutineCreated) {
        Intrinsics.checkParameterIsNotNull(function2, "$this$startCoroutineUnintercepted");
        Intrinsics.checkParameterIsNotNull(probeCoroutineCreated, "completion");
        probeCoroutineCreated = DebugProbesKt.probeCoroutineCreated(probeCoroutineCreated);
        try {
            final Object invoke = ((Function2)TypeIntrinsics.beforeCheckcastToFunctionOfArity(function2, 2)).invoke(r, probeCoroutineCreated);
            if (invoke != IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                final Result.Companion companion = Result.Companion;
                probeCoroutineCreated.resumeWith(Result.constructor-impl(invoke));
            }
        }
        finally {
            final Result.Companion companion2 = Result.Companion;
            final Throwable t;
            probeCoroutineCreated.resumeWith(Result.constructor-impl(ResultKt.createFailure(t)));
        }
    }
    
    private static final <T> void startDirect(Continuation<? super T> probeCoroutineCreated, final Function1<? super Continuation<? super T>, ?> function1) {
        probeCoroutineCreated = DebugProbesKt.probeCoroutineCreated(probeCoroutineCreated);
        try {
            final Object invoke = function1.invoke(probeCoroutineCreated);
            if (invoke != IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                final Result.Companion companion = Result.Companion;
                probeCoroutineCreated.resumeWith(Result.constructor-impl(invoke));
            }
        }
        finally {
            final Result.Companion companion2 = Result.Companion;
            final Throwable t;
            probeCoroutineCreated.resumeWith(Result.constructor-impl(ResultKt.createFailure(t)));
        }
    }
    
    public static final <T, R> Object startUndispatchedOrReturn(final AbstractCoroutine<? super T> abstractCoroutine, final R r, final Function2<? super R, ? super Continuation<? super T>, ?> function2) {
        Intrinsics.checkParameterIsNotNull(abstractCoroutine, "$this$startUndispatchedOrReturn");
        Intrinsics.checkParameterIsNotNull(function2, "block");
        abstractCoroutine.initParentJob$kotlinx_coroutines_core();
        CompletedExceptionally completedExceptionally = null;
        try {
            ((Function2)TypeIntrinsics.beforeCheckcastToFunctionOfArity(function2, 2)).invoke(r, abstractCoroutine);
        }
        finally {
            final Throwable t;
            completedExceptionally = new CompletedExceptionally(t, false, 2, null);
        }
        if (completedExceptionally == IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            return IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
        }
        if (!abstractCoroutine.makeCompletingOnce$kotlinx_coroutines_core(completedExceptionally, 4)) {
            return IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
        }
        final Object state$kotlinx_coroutines_core = abstractCoroutine.getState$kotlinx_coroutines_core();
        if (state$kotlinx_coroutines_core instanceof CompletedExceptionally) {
            final CompletedExceptionally completedExceptionally2 = (CompletedExceptionally)state$kotlinx_coroutines_core;
            final Throwable cause = completedExceptionally2.cause;
            throw ScopesKt.tryRecover(abstractCoroutine, completedExceptionally2.cause);
        }
        return JobSupportKt.unboxState(state$kotlinx_coroutines_core);
    }
    
    public static final <T, R> Object startUndispatchedOrReturnIgnoreTimeout(final AbstractCoroutine<? super T> abstractCoroutine, final R r, final Function2<? super R, ? super Continuation<? super T>, ?> function2) {
        Intrinsics.checkParameterIsNotNull(abstractCoroutine, "$this$startUndispatchedOrReturnIgnoreTimeout");
        Intrinsics.checkParameterIsNotNull(function2, "block");
        abstractCoroutine.initParentJob$kotlinx_coroutines_core();
        boolean b = false;
        CompletedExceptionally completedExceptionally = null;
        try {
            ((Function2)TypeIntrinsics.beforeCheckcastToFunctionOfArity(function2, 2)).invoke(r, abstractCoroutine);
        }
        finally {
            final Throwable t;
            completedExceptionally = new CompletedExceptionally(t, false, 2, null);
        }
        if (completedExceptionally == IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            return IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
        }
        if (!abstractCoroutine.makeCompletingOnce$kotlinx_coroutines_core(completedExceptionally, 4)) {
            return IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
        }
        final Object state$kotlinx_coroutines_core = abstractCoroutine.getState$kotlinx_coroutines_core();
        if (!(state$kotlinx_coroutines_core instanceof CompletedExceptionally)) {
            return JobSupportKt.unboxState(state$kotlinx_coroutines_core);
        }
        final CompletedExceptionally completedExceptionally2 = (CompletedExceptionally)state$kotlinx_coroutines_core;
        final Throwable cause = completedExceptionally2.cause;
        if (!(cause instanceof TimeoutCancellationException) || ((TimeoutCancellationException)cause).coroutine != abstractCoroutine) {
            b = true;
        }
        if (b) {
            throw ScopesKt.tryRecover(abstractCoroutine, completedExceptionally2.cause);
        }
        if (!(completedExceptionally instanceof CompletedExceptionally)) {
            return completedExceptionally;
        }
        throw ScopesKt.tryRecover(abstractCoroutine, completedExceptionally.cause);
    }
    
    private static final <T> Object undispatchedResult(final AbstractCoroutine<? super T> abstractCoroutine, final Function1<? super Throwable, Boolean> function1, final Function0<?> function2) {
        CompletedExceptionally completedExceptionally = null;
        try {
            function2.invoke();
        }
        finally {
            final Throwable t;
            completedExceptionally = new CompletedExceptionally(t, false, 2, null);
        }
        if (completedExceptionally == IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            return IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
        }
        if (!abstractCoroutine.makeCompletingOnce$kotlinx_coroutines_core(completedExceptionally, 4)) {
            return IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
        }
        final Object state$kotlinx_coroutines_core = abstractCoroutine.getState$kotlinx_coroutines_core();
        if (!(state$kotlinx_coroutines_core instanceof CompletedExceptionally)) {
            return JobSupportKt.unboxState(state$kotlinx_coroutines_core);
        }
        final CompletedExceptionally completedExceptionally2 = (CompletedExceptionally)state$kotlinx_coroutines_core;
        if (function1.invoke(completedExceptionally2.cause)) {
            throw ScopesKt.tryRecover(abstractCoroutine, completedExceptionally2.cause);
        }
        if (!(completedExceptionally instanceof CompletedExceptionally)) {
            return completedExceptionally;
        }
        throw ScopesKt.tryRecover(abstractCoroutine, completedExceptionally.cause);
    }
}
