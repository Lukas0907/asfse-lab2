// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import kotlin.coroutines.jvm.internal.CoroutineStackFrame;
import kotlin.coroutines.Continuation;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000Z\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0012\u0018\u0000*\u0004\b\u0000\u0010\u0001*\n\b\u0001\u0010\u0002 \u0000*\u0002H\u00012\b\u0012\u0004\u0012\u0002H\u00020\u00032\u00060\u0004j\u0002`\u00052\b\u0012\u0004\u0012\u0002H\u00020\u00062\u00060\u0007j\u0002`\bB\u001b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006¢\u0006\u0002\u0010\fJ\u001a\u0010\u0017\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\u0006\u0010\u001b\u001a\u00020\u0011H\u0014J\u0010\u0010\u001c\u001a\n\u0018\u00010\u001dj\u0004\u0018\u0001`\u001eH\u0016J\r\u0010\u001f\u001a\u00020 H\u0010¢\u0006\u0002\b!J\b\u0010\"\u001a\u00020\u0018H\u0016R\u001c\u0010\r\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR\u0014\u0010\u0010\u001a\u00020\u00118PX\u0090\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0013R\u0014\u0010\u0014\u001a\u00020\u00158TX\u0094\u0004¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u0016R\u0010\u0010\t\u001a\u00020\n8\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\b\u0012\u0004\u0012\u00028\u00000\u00068\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006#" }, d2 = { "Lkotlinx/coroutines/TimeoutCoroutine;", "U", "T", "Lkotlinx/coroutines/AbstractCoroutine;", "Ljava/lang/Runnable;", "Lkotlinx/coroutines/Runnable;", "Lkotlin/coroutines/Continuation;", "Lkotlin/coroutines/jvm/internal/CoroutineStackFrame;", "Lkotlinx/coroutines/internal/CoroutineStackFrame;", "time", "", "uCont", "(JLkotlin/coroutines/Continuation;)V", "callerFrame", "getCallerFrame", "()Lkotlin/coroutines/jvm/internal/CoroutineStackFrame;", "defaultResumeMode", "", "getDefaultResumeMode$kotlinx_coroutines_core", "()I", "isScopedCoroutine", "", "()Z", "afterCompletionInternal", "", "state", "", "mode", "getStackTraceElement", "Ljava/lang/StackTraceElement;", "Lkotlinx/coroutines/internal/StackTraceElement;", "nameString", "", "nameString$kotlinx_coroutines_core", "run", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
class TimeoutCoroutine<U, T extends U> extends AbstractCoroutine<T> implements Runnable, Continuation<T>, CoroutineStackFrame
{
    public final long time;
    public final Continuation<U> uCont;
    
    public TimeoutCoroutine(final long time, final Continuation<? super U> uCont) {
        Intrinsics.checkParameterIsNotNull(uCont, "uCont");
        super(uCont.getContext(), true);
        this.time = time;
        this.uCont = (Continuation<U>)uCont;
    }
    
    @Override
    protected void afterCompletionInternal(final Object o, final int n) {
        if (o instanceof CompletedExceptionally) {
            ResumeModeKt.resumeUninterceptedWithExceptionMode((Continuation<? super Object>)this.uCont, ((CompletedExceptionally)o).cause, n);
            return;
        }
        ResumeModeKt.resumeUninterceptedMode((Continuation<? super Object>)this.uCont, o, n);
    }
    
    @Override
    public CoroutineStackFrame getCallerFrame() {
        Object uCont;
        if (!((uCont = this.uCont) instanceof CoroutineStackFrame)) {
            uCont = null;
        }
        return (CoroutineStackFrame)uCont;
    }
    
    @Override
    public int getDefaultResumeMode$kotlinx_coroutines_core() {
        return 2;
    }
    
    @Override
    public StackTraceElement getStackTraceElement() {
        return null;
    }
    
    @Override
    protected boolean isScopedCoroutine() {
        return true;
    }
    
    @Override
    public String nameString$kotlinx_coroutines_core() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.nameString$kotlinx_coroutines_core());
        sb.append("(timeMillis=");
        sb.append(this.time);
        sb.append(')');
        return sb.toString();
    }
    
    @Override
    public void run() {
        this.cancelCoroutine(TimeoutKt.TimeoutCancellationException(this.time, this));
    }
}
