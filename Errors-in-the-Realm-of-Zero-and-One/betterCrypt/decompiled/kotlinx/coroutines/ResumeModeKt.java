// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlinx.coroutines.internal.StackTraceRecoveryKt;
import kotlin.ResultKt;
import kotlin.coroutines.CoroutineContext;
import kotlin.Unit;
import kotlinx.coroutines.internal.ThreadContextKt;
import kotlin.Result;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.Continuation;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000(\n\u0000\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0003\n\u0002\b\u0002\u001a-\u0010\u0010\u001a\u00020\u0011\"\u0004\b\u0000\u0010\u0012*\b\u0012\u0004\u0012\u0002H\u00120\u00132\u0006\u0010\u0014\u001a\u0002H\u00122\u0006\u0010\u0015\u001a\u00020\u0001H\u0000¢\u0006\u0002\u0010\u0016\u001a-\u0010\u0017\u001a\u00020\u0011\"\u0004\b\u0000\u0010\u0012*\b\u0012\u0004\u0012\u0002H\u00120\u00132\u0006\u0010\u0014\u001a\u0002H\u00122\u0006\u0010\u0015\u001a\u00020\u0001H\u0000¢\u0006\u0002\u0010\u0016\u001a(\u0010\u0018\u001a\u00020\u0011\"\u0004\b\u0000\u0010\u0012*\b\u0012\u0004\u0012\u0002H\u00120\u00132\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0015\u001a\u00020\u0001H\u0000\u001a(\u0010\u001b\u001a\u00020\u0011\"\u0004\b\u0000\u0010\u0012*\b\u0012\u0004\u0012\u0002H\u00120\u00132\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0015\u001a\u00020\u0001H\u0000\"\u0016\u0010\u0000\u001a\u00020\u00018\u0000X\u0081T¢\u0006\b\n\u0000\u0012\u0004\b\u0002\u0010\u0003\"\u0016\u0010\u0004\u001a\u00020\u00018\u0000X\u0081T¢\u0006\b\n\u0000\u0012\u0004\b\u0005\u0010\u0003\"\u0016\u0010\u0006\u001a\u00020\u00018\u0000X\u0081T¢\u0006\b\n\u0000\u0012\u0004\b\u0007\u0010\u0003\"\u0016\u0010\b\u001a\u00020\u00018\u0000X\u0081T¢\u0006\b\n\u0000\u0012\u0004\b\t\u0010\u0003\"\u0016\u0010\n\u001a\u00020\u00018\u0000X\u0081T¢\u0006\b\n\u0000\u0012\u0004\b\u000b\u0010\u0003\"\u0018\u0010\f\u001a\u00020\r*\u00020\u00018@X\u0080\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\u000e\"\u0018\u0010\u000f\u001a\u00020\r*\u00020\u00018@X\u0080\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u000e¨\u0006\u001c" }, d2 = { "MODE_ATOMIC_DEFAULT", "", "MODE_ATOMIC_DEFAULT$annotations", "()V", "MODE_CANCELLABLE", "MODE_CANCELLABLE$annotations", "MODE_DIRECT", "MODE_DIRECT$annotations", "MODE_IGNORE", "MODE_IGNORE$annotations", "MODE_UNDISPATCHED", "MODE_UNDISPATCHED$annotations", "isCancellableMode", "", "(I)Z", "isDispatchedMode", "resumeMode", "", "T", "Lkotlin/coroutines/Continuation;", "value", "mode", "(Lkotlin/coroutines/Continuation;Ljava/lang/Object;I)V", "resumeUninterceptedMode", "resumeUninterceptedWithExceptionMode", "exception", "", "resumeWithExceptionMode", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class ResumeModeKt
{
    public static final int MODE_ATOMIC_DEFAULT = 0;
    public static final int MODE_CANCELLABLE = 1;
    public static final int MODE_DIRECT = 2;
    public static final int MODE_IGNORE = 4;
    public static final int MODE_UNDISPATCHED = 3;
    
    public static final boolean isCancellableMode(final int n) {
        return n == 1;
    }
    
    public static final boolean isDispatchedMode(final int n) {
        boolean b = true;
        if (n != 0) {
            if (n == 1) {
                return true;
            }
            b = false;
        }
        return b;
    }
    
    public static final <T> void resumeMode(Continuation<? super T> context, final T t, final int i) {
        Intrinsics.checkParameterIsNotNull(context, "$this$resumeMode");
        if (i == 0) {
            final Result.Companion companion = Result.Companion;
            ((Continuation)context).resumeWith(Result.constructor-impl(t));
            return;
        }
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    if (i == 4) {
                        return;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Invalid mode ");
                    sb.append(i);
                    throw new IllegalStateException(sb.toString().toString());
                }
                else {
                    final DispatchedContinuation dispatchedContinuation = (DispatchedContinuation)context;
                    context = dispatchedContinuation.getContext();
                    final Object updateThreadContext = ThreadContextKt.updateThreadContext(context, dispatchedContinuation.countOrElement);
                    try {
                        final Continuation<T> continuation = dispatchedContinuation.continuation;
                        final Result.Companion companion2 = Result.Companion;
                        continuation.resumeWith(Result.constructor-impl(t));
                        final Unit instance = Unit.INSTANCE;
                        return;
                    }
                    finally {
                        ThreadContextKt.restoreThreadContext(context, updateThreadContext);
                    }
                }
            }
            DispatchedKt.resumeDirect((Continuation<? super T>)context, t);
            return;
        }
        DispatchedKt.resumeCancellable((Continuation<? super T>)context, t);
    }
    
    public static final <T> void resumeUninterceptedMode(final Continuation<? super T> continuation, final T t, final int i) {
        Intrinsics.checkParameterIsNotNull(continuation, "$this$resumeUninterceptedMode");
        if (i == 0) {
            final Continuation<Object> intercepted = IntrinsicsKt__IntrinsicsJvmKt.intercepted((Continuation<? super Object>)continuation);
            final Result.Companion companion = Result.Companion;
            intercepted.resumeWith(Result.constructor-impl(t));
            return;
        }
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    if (i == 4) {
                        return;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Invalid mode ");
                    sb.append(i);
                    throw new IllegalStateException(sb.toString().toString());
                }
                else {
                    final CoroutineContext context = continuation.getContext();
                    final Object updateThreadContext = ThreadContextKt.updateThreadContext(context, null);
                    try {
                        final Result.Companion companion2 = Result.Companion;
                        continuation.resumeWith(Result.constructor-impl(t));
                        final Unit instance = Unit.INSTANCE;
                        return;
                    }
                    finally {
                        ThreadContextKt.restoreThreadContext(context, updateThreadContext);
                    }
                }
            }
            final Result.Companion companion3 = Result.Companion;
            continuation.resumeWith(Result.constructor-impl(t));
            return;
        }
        DispatchedKt.resumeCancellable(IntrinsicsKt__IntrinsicsJvmKt.intercepted(continuation), t);
    }
    
    public static final <T> void resumeUninterceptedWithExceptionMode(final Continuation<? super T> continuation, final Throwable t, final int i) {
        Intrinsics.checkParameterIsNotNull(continuation, "$this$resumeUninterceptedWithExceptionMode");
        Intrinsics.checkParameterIsNotNull(t, "exception");
        if (i == 0) {
            final Continuation<Object> intercepted = IntrinsicsKt__IntrinsicsJvmKt.intercepted((Continuation<? super Object>)continuation);
            final Result.Companion companion = Result.Companion;
            intercepted.resumeWith(Result.constructor-impl(ResultKt.createFailure(t)));
            return;
        }
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    if (i == 4) {
                        return;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Invalid mode ");
                    sb.append(i);
                    throw new IllegalStateException(sb.toString().toString());
                }
                else {
                    final CoroutineContext context = continuation.getContext();
                    final Object updateThreadContext = ThreadContextKt.updateThreadContext(context, null);
                    try {
                        final Result.Companion companion2 = Result.Companion;
                        continuation.resumeWith(Result.constructor-impl(ResultKt.createFailure(t)));
                        final Unit instance = Unit.INSTANCE;
                        return;
                    }
                    finally {
                        ThreadContextKt.restoreThreadContext(context, updateThreadContext);
                    }
                }
            }
            final Result.Companion companion3 = Result.Companion;
            continuation.resumeWith(Result.constructor-impl(ResultKt.createFailure(t)));
            return;
        }
        DispatchedKt.resumeCancellableWithException(IntrinsicsKt__IntrinsicsJvmKt.intercepted((Continuation<? super Object>)continuation), t);
    }
    
    public static final <T> void resumeWithExceptionMode(Continuation<? super T> context, final Throwable t, final int i) {
        Intrinsics.checkParameterIsNotNull(context, "$this$resumeWithExceptionMode");
        Intrinsics.checkParameterIsNotNull(t, "exception");
        if (i == 0) {
            final Result.Companion companion = Result.Companion;
            ((Continuation)context).resumeWith(Result.constructor-impl(ResultKt.createFailure(t)));
            return;
        }
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    if (i == 4) {
                        return;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Invalid mode ");
                    sb.append(i);
                    throw new IllegalStateException(sb.toString().toString());
                }
                else {
                    final DispatchedContinuation dispatchedContinuation = (DispatchedContinuation)context;
                    context = dispatchedContinuation.getContext();
                    final Object updateThreadContext = ThreadContextKt.updateThreadContext(context, dispatchedContinuation.countOrElement);
                    try {
                        final Continuation<T> continuation = dispatchedContinuation.continuation;
                        final Result.Companion companion2 = Result.Companion;
                        continuation.resumeWith(Result.constructor-impl(ResultKt.createFailure(StackTraceRecoveryKt.recoverStackTrace(t, continuation))));
                        final Unit instance = Unit.INSTANCE;
                        return;
                    }
                    finally {
                        ThreadContextKt.restoreThreadContext(context, updateThreadContext);
                    }
                }
            }
            DispatchedKt.resumeDirectWithException((Continuation<? super Object>)context, t);
            return;
        }
        DispatchedKt.resumeCancellableWithException((Continuation<? super Object>)context, t);
    }
}
