// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "kotlinx/coroutines/BuildersKt__BuildersKt", "kotlinx/coroutines/BuildersKt__Builders_commonKt" }, k = 4, mv = { 1, 1, 15 })
public final class BuildersKt
{
    public static final <T> Deferred<T> async(final CoroutineScope coroutineScope, final CoroutineContext coroutineContext, final CoroutineStart coroutineStart, final Function2<? super CoroutineScope, ? super Continuation<? super T>, ?> function2) {
        return BuildersKt__Builders_commonKt.async(coroutineScope, coroutineContext, coroutineStart, function2);
    }
    
    public static final <T> Object invoke(final CoroutineDispatcher coroutineDispatcher, final Function2<? super CoroutineScope, ? super Continuation<? super T>, ?> function2, final Continuation<? super T> continuation) {
        return BuildersKt__Builders_commonKt.invoke(coroutineDispatcher, (Function2<? super CoroutineScope, ? super Continuation<? super Object>, ?>)function2, (Continuation<? super Object>)continuation);
    }
    
    private static final Object invoke$$forInline(final CoroutineDispatcher coroutineDispatcher, final Function2 function2, final Continuation continuation) {
        return BuildersKt__Builders_commonKt.invoke(coroutineDispatcher, (Function2<? super CoroutineScope, ? super Continuation<? super Object>, ?>)function2, (Continuation<? super Object>)continuation);
    }
    
    public static final Job launch(final CoroutineScope coroutineScope, final CoroutineContext coroutineContext, final CoroutineStart coroutineStart, final Function2<? super CoroutineScope, ? super Continuation<? super Unit>, ?> function2) {
        return BuildersKt__Builders_commonKt.launch(coroutineScope, coroutineContext, coroutineStart, function2);
    }
    
    public static final <T> T runBlocking(final CoroutineContext coroutineContext, final Function2<? super CoroutineScope, ? super Continuation<? super T>, ?> function2) throws InterruptedException {
        return BuildersKt__BuildersKt.runBlocking(coroutineContext, function2);
    }
    
    public static final <T> Object withContext(final CoroutineContext coroutineContext, final Function2<? super CoroutineScope, ? super Continuation<? super T>, ?> function2, final Continuation<? super T> continuation) {
        return BuildersKt__Builders_commonKt.withContext(coroutineContext, (Function2<? super CoroutineScope, ? super Continuation<? super Object>, ?>)function2, (Continuation<? super Object>)continuation);
    }
}
