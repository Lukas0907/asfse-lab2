// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function2;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;
import kotlin.coroutines.AbstractCoroutineContextElement;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0080\b\u0018\u0000 \u00182\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003:\u0001\u0018B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\t\u001a\u00020\u0005H\u00c6\u0003J\u0013\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u00d6\u0003J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001J\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0002H\u0016J\b\u0010\u0016\u001a\u00020\u0002H\u0016J\u0010\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0014H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b¨\u0006\u0019" }, d2 = { "Lkotlinx/coroutines/CoroutineId;", "Lkotlinx/coroutines/ThreadContextElement;", "", "Lkotlin/coroutines/AbstractCoroutineContextElement;", "id", "", "(J)V", "getId", "()J", "component1", "copy", "equals", "", "other", "", "hashCode", "", "restoreThreadContext", "", "context", "Lkotlin/coroutines/CoroutineContext;", "oldState", "toString", "updateThreadContext", "Key", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class CoroutineId extends AbstractCoroutineContextElement implements ThreadContextElement<String>
{
    public static final Key Key;
    private final long id;
    
    static {
        Key = new Key(null);
    }
    
    public CoroutineId(final long id) {
        super(CoroutineId.Key);
        this.id = id;
    }
    
    public final long component1() {
        return this.id;
    }
    
    public final CoroutineId copy(final long n) {
        return new CoroutineId(n);
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof CoroutineId && this.id == ((CoroutineId)o).id);
    }
    
    @Override
    public <R> R fold(final R r, final Function2<? super R, ? super Element, ? extends R> function2) {
        Intrinsics.checkParameterIsNotNull(function2, "operation");
        return ThreadContextElement.DefaultImpls.fold((ThreadContextElement<Object>)this, r, function2);
    }
    
    @Override
    public <E extends Element> E get(final CoroutineContext.Key<E> key) {
        Intrinsics.checkParameterIsNotNull(key, "key");
        return ThreadContextElement.DefaultImpls.get((ThreadContextElement<Object>)this, key);
    }
    
    public final long getId() {
        return this.id;
    }
    
    @Override
    public int hashCode() {
        final long id = this.id;
        return (int)(id ^ id >>> 32);
    }
    
    @Override
    public CoroutineContext minusKey(final CoroutineContext.Key<?> key) {
        Intrinsics.checkParameterIsNotNull(key, "key");
        return ThreadContextElement.DefaultImpls.minusKey((ThreadContextElement<Object>)this, key);
    }
    
    @Override
    public CoroutineContext plus(final CoroutineContext coroutineContext) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        return ThreadContextElement.DefaultImpls.plus((ThreadContextElement<Object>)this, coroutineContext);
    }
    
    @Override
    public void restoreThreadContext(final CoroutineContext coroutineContext, final String name) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        Intrinsics.checkParameterIsNotNull(name, "oldState");
        final Thread currentThread = Thread.currentThread();
        Intrinsics.checkExpressionValueIsNotNull(currentThread, "Thread.currentThread()");
        currentThread.setName(name);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CoroutineId(");
        sb.append(this.id);
        sb.append(')');
        return sb.toString();
    }
    
    @Override
    public String updateThreadContext(final CoroutineContext coroutineContext) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        final CoroutineName coroutineName = coroutineContext.get((CoroutineContext.Key<CoroutineName>)CoroutineName.Key);
        String name = null;
        Label_0041: {
            if (coroutineName != null) {
                name = coroutineName.getName();
                if (name != null) {
                    break Label_0041;
                }
            }
            name = "coroutine";
        }
        final Thread currentThread = Thread.currentThread();
        Intrinsics.checkExpressionValueIsNotNull(currentThread, "currentThread");
        final String name2 = currentThread.getName();
        Intrinsics.checkExpressionValueIsNotNull(name2, "oldName");
        int endIndex;
        if ((endIndex = StringsKt__StringsKt.lastIndexOf$default(name2, " @", 0, false, 6, null)) < 0) {
            endIndex = name2.length();
        }
        final StringBuilder sb = new StringBuilder(name.length() + endIndex + 10);
        final String substring = name2.substring(0, endIndex);
        Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        sb.append(substring);
        sb.append(" @");
        sb.append(name);
        sb.append('#');
        sb.append(this.id);
        final String string = sb.toString();
        Intrinsics.checkExpressionValueIsNotNull(string, "StringBuilder(capacity).\u2026builderAction).toString()");
        currentThread.setName(string);
        return name2;
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0003¨\u0006\u0004" }, d2 = { "Lkotlinx/coroutines/CoroutineId$Key;", "Lkotlin/coroutines/CoroutineContext$Key;", "Lkotlinx/coroutines/CoroutineId;", "()V", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    public static final class Key implements CoroutineContext.Key<CoroutineId>
    {
        private Key() {
        }
    }
}
