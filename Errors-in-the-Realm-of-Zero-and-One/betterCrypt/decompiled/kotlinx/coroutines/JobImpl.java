// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u0003\n\u0000\b\u0010\u0018\u00002\u00020\u00012\u00020\u0002B\u000f\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004¢\u0006\u0002\u0010\u0005J\b\u0010\f\u001a\u00020\u0007H\u0016J\u0010\u0010\r\u001a\u00020\u00072\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\b\u0010\u0006\u001a\u00020\u0007H\u0003R\u0014\u0010\u0006\u001a\u00020\u0007X\u0090\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\n\u001a\u00020\u00078PX\u0090\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\t¨\u0006\u0010" }, d2 = { "Lkotlinx/coroutines/JobImpl;", "Lkotlinx/coroutines/JobSupport;", "Lkotlinx/coroutines/CompletableJob;", "parent", "Lkotlinx/coroutines/Job;", "(Lkotlinx/coroutines/Job;)V", "handlesException", "", "getHandlesException$kotlinx_coroutines_core", "()Z", "onCancelComplete", "getOnCancelComplete$kotlinx_coroutines_core", "complete", "completeExceptionally", "exception", "", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public class JobImpl extends JobSupport implements CompletableJob
{
    private final boolean handlesException;
    
    public JobImpl(final Job job) {
        super(true);
        this.initParentJobInternal$kotlinx_coroutines_core(job);
        this.handlesException = this.handlesException();
    }
    
    private final boolean handlesException() {
        ChildHandle parentHandle;
        if (!((parentHandle = this.parentHandle) instanceof ChildHandleNode)) {
            parentHandle = null;
        }
        final ChildHandleNode childHandleNode = (ChildHandleNode)parentHandle;
        if (childHandleNode != null) {
            JobSupport jobSupport = (JobSupport)childHandleNode.job;
            if (jobSupport != null) {
                while (!jobSupport.getHandlesException$kotlinx_coroutines_core()) {
                    ChildHandle parentHandle2;
                    if (!((parentHandle2 = jobSupport.parentHandle) instanceof ChildHandleNode)) {
                        parentHandle2 = null;
                    }
                    final ChildHandleNode childHandleNode2 = (ChildHandleNode)parentHandle2;
                    if (childHandleNode2 == null) {
                        return false;
                    }
                    jobSupport = (JobSupport)childHandleNode2.job;
                    if (jobSupport != null) {
                        continue;
                    }
                    return false;
                }
                return true;
            }
        }
        return false;
    }
    
    @Override
    public boolean complete() {
        return this.makeCompleting$kotlinx_coroutines_core(Unit.INSTANCE);
    }
    
    @Override
    public boolean completeExceptionally(final Throwable t) {
        Intrinsics.checkParameterIsNotNull(t, "exception");
        return this.makeCompleting$kotlinx_coroutines_core(new CompletedExceptionally(t, false, 2, null));
    }
    
    @Override
    public boolean getHandlesException$kotlinx_coroutines_core() {
        return this.handlesException;
    }
    
    @Override
    public boolean getOnCancelComplete$kotlinx_coroutines_core() {
        return true;
    }
}
