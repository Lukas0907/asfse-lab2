// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.ResultKt;
import kotlin.Result;
import kotlin.coroutines.Continuation;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0014\n\u0000\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0007\u001a\u00020\u0001*\u0006\u0012\u0002\b\u00030\bH\u0000\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028@X\u0080\u0004¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004\"\u0018\u0010\u0005\u001a\u00020\u0001*\u00020\u00028@X\u0080\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0004¨\u0006\t" }, d2 = { "classSimpleName", "", "", "getClassSimpleName", "(Ljava/lang/Object;)Ljava/lang/String;", "hexAddress", "getHexAddress", "toDebugString", "Lkotlin/coroutines/Continuation;", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class DebugStringsKt
{
    public static final String getClassSimpleName(final Object o) {
        Intrinsics.checkParameterIsNotNull(o, "$this$classSimpleName");
        final String simpleName = o.getClass().getSimpleName();
        Intrinsics.checkExpressionValueIsNotNull(simpleName, "this::class.java.simpleName");
        return simpleName;
    }
    
    public static final String getHexAddress(final Object o) {
        Intrinsics.checkParameterIsNotNull(o, "$this$hexAddress");
        final String hexString = Integer.toHexString(System.identityHashCode(o));
        Intrinsics.checkExpressionValueIsNotNull(hexString, "Integer.toHexString(System.identityHashCode(this))");
        return hexString;
    }
    
    public static final String toDebugString(final Continuation<?> obj) {
        Intrinsics.checkParameterIsNotNull(obj, "$this$toDebugString");
        if (obj instanceof DispatchedContinuation) {
            return obj.toString();
        }
        Object o = null;
        try {
            final Result.Companion companion = Result.Companion;
            final StringBuilder sb = new StringBuilder();
            sb.append(obj);
            sb.append('@');
            sb.append(getHexAddress(obj));
            Result.constructor-impl(sb.toString());
        }
        finally {
            final Result.Companion companion2 = Result.Companion;
            final Throwable t;
            o = Result.constructor-impl(ResultKt.createFailure(t));
        }
        if (Result.exceptionOrNull-impl(o) != null) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(obj.getClass().getName());
            sb2.append('@');
            sb2.append(getHexAddress(obj));
            o = sb2.toString();
        }
        return (String)o;
    }
}
