// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.jvm.internal.Intrinsics;
import kotlin.ResultKt;
import kotlin.Result;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0010\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\"\u0010\u0000\u001a\u0004\u0018\u00010\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H\u0000\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0004\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0005" }, d2 = { "toState", "", "T", "Lkotlin/Result;", "(Ljava/lang/Object;)Ljava/lang/Object;", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class CompletedExceptionallyKt
{
    public static final <T> Object toState(final Object o) {
        if (Result.isSuccess-impl(o)) {
            ResultKt.throwOnFailure(o);
            return o;
        }
        final Throwable exceptionOrNull-impl = Result.exceptionOrNull-impl(o);
        if (exceptionOrNull-impl == null) {
            Intrinsics.throwNpe();
        }
        return new CompletedExceptionally(exceptionOrNull-impl, false, 2, null);
    }
}
