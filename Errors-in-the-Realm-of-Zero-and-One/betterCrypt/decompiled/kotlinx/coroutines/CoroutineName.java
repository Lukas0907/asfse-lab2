// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import kotlin.coroutines.AbstractCoroutineContextElement;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\b\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\b\u0010\u000f\u001a\u00020\u0003H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011" }, d2 = { "Lkotlinx/coroutines/CoroutineName;", "Lkotlin/coroutines/AbstractCoroutineContextElement;", "name", "", "(Ljava/lang/String;)V", "getName", "()Ljava/lang/String;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Key", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class CoroutineName extends AbstractCoroutineContextElement
{
    public static final Key Key;
    private final String name;
    
    static {
        Key = new Key(null);
    }
    
    public CoroutineName(final String name) {
        Intrinsics.checkParameterIsNotNull(name, "name");
        super(CoroutineName.Key);
        this.name = name;
    }
    
    public final String component1() {
        return this.name;
    }
    
    public final CoroutineName copy(final String s) {
        Intrinsics.checkParameterIsNotNull(s, "name");
        return new CoroutineName(s);
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof CoroutineName && Intrinsics.areEqual(this.name, ((CoroutineName)o).name));
    }
    
    public final String getName() {
        return this.name;
    }
    
    @Override
    public int hashCode() {
        final String name = this.name;
        if (name != null) {
            return name.hashCode();
        }
        return 0;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CoroutineName(");
        sb.append(this.name);
        sb.append(')');
        return sb.toString();
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0003¨\u0006\u0004" }, d2 = { "Lkotlinx/coroutines/CoroutineName$Key;", "Lkotlin/coroutines/CoroutineContext$Key;", "Lkotlinx/coroutines/CoroutineName;", "()V", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    public static final class Key implements CoroutineContext.Key<CoroutineName>
    {
        private Key() {
        }
    }
}
