// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlinx.coroutines.intrinsics.UndispatchedKt;
import kotlinx.coroutines.internal.ScopeCoroutine;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.internal.ContextScope;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000F\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\u000e\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\b\u001a\u0006\u0010\t\u001a\u00020\u0002\u001a@\u0010\n\u001a\u0002H\u000b\"\u0004\b\u0000\u0010\u000b2'\u0010\f\u001a#\b\u0001\u0012\u0004\u0012\u00020\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u000b0\u000e\u0012\u0006\u0012\u0004\u0018\u00010\u000f0\r¢\u0006\u0002\b\u0010H\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0011\u001a\u001e\u0010\u0012\u001a\u00020\u0013*\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u00152\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0017\u001a\u001c\u0010\u0012\u001a\u00020\u0013*\u00020\u00022\u0010\b\u0002\u0010\u0016\u001a\n\u0018\u00010\u0018j\u0004\u0018\u0001`\u0019\u001a\n\u0010\u001a\u001a\u00020\u0013*\u00020\u0002\u001a\u0015\u0010\u001b\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\bH\u0086\u0002\"\u001b\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F¢\u0006\f\u0012\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0000\u0010\u0005\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u001c" }, d2 = { "isActive", "", "Lkotlinx/coroutines/CoroutineScope;", "isActive$annotations", "(Lkotlinx/coroutines/CoroutineScope;)V", "(Lkotlinx/coroutines/CoroutineScope;)Z", "CoroutineScope", "context", "Lkotlin/coroutines/CoroutineContext;", "MainScope", "coroutineScope", "R", "block", "Lkotlin/Function2;", "Lkotlin/coroutines/Continuation;", "", "Lkotlin/ExtensionFunctionType;", "(Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "cancel", "", "message", "", "cause", "", "Ljava/util/concurrent/CancellationException;", "Lkotlinx/coroutines/CancellationException;", "ensureActive", "plus", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class CoroutineScopeKt
{
    public static final CoroutineScope CoroutineScope(CoroutineContext plus) {
        Intrinsics.checkParameterIsNotNull(plus, "context");
        if (plus.get((CoroutineContext.Key<CoroutineContext.Element>)Job.Key) == null) {
            plus = plus.plus(JobKt.Job$default((Job)null, 1, (Object)null));
        }
        return new ContextScope(plus);
    }
    
    public static final CoroutineScope MainScope() {
        return new ContextScope(SupervisorKt.SupervisorJob$default((Job)null, 1, (Object)null).plus(Dispatchers.getMain()));
    }
    
    public static final void cancel(final CoroutineScope coroutineScope, final String s, final Throwable t) {
        Intrinsics.checkParameterIsNotNull(coroutineScope, "$this$cancel");
        Intrinsics.checkParameterIsNotNull(s, "message");
        cancel(coroutineScope, ExceptionsKt.CancellationException(s, t));
    }
    
    public static final void cancel(final CoroutineScope obj, final CancellationException ex) {
        Intrinsics.checkParameterIsNotNull(obj, "$this$cancel");
        final Job job = obj.getCoroutineContext().get((CoroutineContext.Key<Job>)Job.Key);
        if (job != null) {
            job.cancel(ex);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Scope cannot be cancelled because it does not have a job: ");
        sb.append(obj);
        throw new IllegalStateException(sb.toString().toString());
    }
    
    public static final <R> Object coroutineScope(final Function2<? super CoroutineScope, ? super Continuation<? super R>, ?> function2, final Continuation<? super R> continuation) {
        final ScopeCoroutine<Object> scopeCoroutine = new ScopeCoroutine<Object>(continuation.getContext(), (Continuation<? super Object>)continuation);
        final Object startUndispatchedOrReturn = UndispatchedKt.startUndispatchedOrReturn((AbstractCoroutine<? super Object>)scopeCoroutine, scopeCoroutine, (Function2<? super ScopeCoroutine<Object>, ? super Continuation<? super Object>, ?>)function2);
        if (startUndispatchedOrReturn == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            DebugProbesKt.probeCoroutineSuspended(continuation);
        }
        return startUndispatchedOrReturn;
    }
    
    public static final void ensureActive(final CoroutineScope coroutineScope) {
        Intrinsics.checkParameterIsNotNull(coroutineScope, "$this$ensureActive");
        JobKt.ensureActive(coroutineScope.getCoroutineContext());
    }
    
    public static final boolean isActive(final CoroutineScope coroutineScope) {
        Intrinsics.checkParameterIsNotNull(coroutineScope, "$this$isActive");
        final Job job = coroutineScope.getCoroutineContext().get((CoroutineContext.Key<Job>)Job.Key);
        return job == null || job.isActive();
    }
    
    public static final CoroutineScope plus(final CoroutineScope coroutineScope, final CoroutineContext coroutineContext) {
        Intrinsics.checkParameterIsNotNull(coroutineScope, "$this$plus");
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        return new ContextScope(coroutineScope.getCoroutineContext().plus(coroutineContext));
    }
}
