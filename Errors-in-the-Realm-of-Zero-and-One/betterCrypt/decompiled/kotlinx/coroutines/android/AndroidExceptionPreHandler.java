// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.android;

import java.lang.reflect.Modifier;
import android.os.Build$VERSION;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.PropertyReference1;
import kotlin.reflect.KDeclarationContainer;
import kotlin.jvm.internal.PropertyReference1Impl;
import kotlin.jvm.internal.Reflection;
import kotlin.reflect.KProperty1;
import kotlin.Lazy;
import kotlin.reflect.KProperty;
import kotlin.Metadata;
import java.lang.reflect.Method;
import kotlin.jvm.functions.Function0;
import kotlinx.coroutines.CoroutineExceptionHandler;
import kotlin.coroutines.AbstractCoroutineContextElement;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\b\u0001\u0018\u00002\u00020\u00012\u00020\u00022\n\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0003B\u0005¢\u0006\u0002\u0010\u0005J\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0004H\u0096\u0002R\u001d\u0010\u0006\u001a\u0004\u0018\u00010\u00048BX\u0082\u0084\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\b¨\u0006\u0012" }, d2 = { "Lkotlinx/coroutines/android/AndroidExceptionPreHandler;", "Lkotlin/coroutines/AbstractCoroutineContextElement;", "Lkotlinx/coroutines/CoroutineExceptionHandler;", "Lkotlin/Function0;", "Ljava/lang/reflect/Method;", "()V", "preHandler", "getPreHandler", "()Ljava/lang/reflect/Method;", "preHandler$delegate", "Lkotlin/Lazy;", "handleException", "", "context", "Lkotlin/coroutines/CoroutineContext;", "exception", "", "invoke", "kotlinx-coroutines-android" }, k = 1, mv = { 1, 1, 15 })
public final class AndroidExceptionPreHandler extends AbstractCoroutineContextElement implements CoroutineExceptionHandler, Function0<Method>
{
    static final /* synthetic */ KProperty[] $$delegatedProperties;
    private final Lazy preHandler$delegate;
    
    static {
        $$delegatedProperties = new KProperty[] { Reflection.property1(new PropertyReference1Impl(Reflection.getOrCreateKotlinClass(AndroidExceptionPreHandler.class), "preHandler", "getPreHandler()Ljava/lang/reflect/Method;")) };
    }
    
    public AndroidExceptionPreHandler() {
        super(CoroutineExceptionHandler.Key);
        this.preHandler$delegate = LazyKt__LazyJVMKt.lazy((Function0<?>)this);
    }
    
    private final Method getPreHandler() {
        final Lazy preHandler$delegate = this.preHandler$delegate;
        final KProperty kProperty = AndroidExceptionPreHandler.$$delegatedProperties[0];
        return preHandler$delegate.getValue();
    }
    
    @Override
    public void handleException(final CoroutineContext coroutineContext, final Throwable t) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        Intrinsics.checkParameterIsNotNull(t, "exception");
        final Thread currentThread = Thread.currentThread();
        if (Build$VERSION.SDK_INT >= 28) {
            Intrinsics.checkExpressionValueIsNotNull(currentThread, "thread");
            currentThread.getUncaughtExceptionHandler().uncaughtException(currentThread, t);
            return;
        }
        final Method preHandler = this.getPreHandler();
        Object invoke;
        if (preHandler != null) {
            invoke = preHandler.invoke(null, new Object[0]);
        }
        else {
            invoke = null;
        }
        Object o = invoke;
        if (!(invoke instanceof Thread.UncaughtExceptionHandler)) {
            o = null;
        }
        final Thread.UncaughtExceptionHandler uncaughtExceptionHandler = (Thread.UncaughtExceptionHandler)o;
        if (uncaughtExceptionHandler != null) {
            uncaughtExceptionHandler.uncaughtException(currentThread, t);
        }
    }
    
    @Override
    public Method invoke() {
        Method method = null;
        final boolean b = false;
        try {
            final Method declaredMethod = Thread.class.getDeclaredMethod("getUncaughtExceptionPreHandler", (Class<?>[])new Class[0]);
            Intrinsics.checkExpressionValueIsNotNull(declaredMethod, "it");
            int n = b ? 1 : 0;
            if (Modifier.isPublic(declaredMethod.getModifiers())) {
                final boolean static1 = Modifier.isStatic(declaredMethod.getModifiers());
                n = (b ? 1 : 0);
                if (static1) {
                    n = 1;
                }
            }
            if (n != 0) {
                method = declaredMethod;
            }
            return method;
        }
        finally {
            return null;
        }
    }
}
