// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.android;

import android.view.Choreographer$FrameCallback;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlinx.coroutines.Dispatchers;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlin.coroutines.Continuation;
import java.lang.reflect.Constructor;
import android.os.Handler$Callback;
import kotlin.TypeCastException;
import android.os.Build$VERSION;
import android.os.Handler;
import kotlinx.coroutines.CancellableContinuation;
import kotlin.DeprecationLevel;
import kotlin.Deprecated;
import kotlin.ResultKt;
import kotlin.jvm.internal.Intrinsics;
import android.os.Looper;
import kotlin.Result;
import android.view.Choreographer;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000@\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u001a\u0011\u0010\b\u001a\u00020\u0001H\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u0010\t\u001a\u001e\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0006\u001a\u00020\u00072\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00010\rH\u0002\u001a\u0016\u0010\u000e\u001a\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00010\rH\u0002\u001a\u001d\u0010\u000f\u001a\u00020\u0003*\u00020\u00102\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0007¢\u0006\u0002\b\u0013\u001a\u0014\u0010\u0014\u001a\u00020\u0010*\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0001\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u0018\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0000X\u0081\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u0004\u0010\u0005\"\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e¢\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0018" }, d2 = { "MAX_DELAY", "", "Main", "Lkotlinx/coroutines/android/HandlerDispatcher;", "Main$annotations", "()V", "choreographer", "Landroid/view/Choreographer;", "awaitFrame", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "postFrameCallback", "", "cont", "Lkotlinx/coroutines/CancellableContinuation;", "updateChoreographerAndPostFrameCallback", "asCoroutineDispatcher", "Landroid/os/Handler;", "name", "", "from", "asHandler", "Landroid/os/Looper;", "async", "", "kotlinx-coroutines-android" }, k = 2, mv = { 1, 1, 15 })
public final class HandlerDispatcherKt
{
    private static final long MAX_DELAY = 4611686018427387903L;
    public static final HandlerDispatcher Main;
    private static volatile Choreographer choreographer;
    
    static {
        Object constructor-impl = null;
        try {
            final Result.Companion companion = Result.Companion;
            final Looper mainLooper = Looper.getMainLooper();
            Intrinsics.checkExpressionValueIsNotNull(mainLooper, "Looper.getMainLooper()");
            Result.constructor-impl(new HandlerContext(asHandler(mainLooper, true), "Main"));
        }
        finally {
            final Result.Companion companion2 = Result.Companion;
            final Throwable t;
            constructor-impl = Result.constructor-impl(ResultKt.createFailure(t));
        }
        Object o = constructor-impl;
        if (Result.isFailure-impl(constructor-impl)) {
            o = null;
        }
        Main = (HandlerDispatcher)o;
    }
    
    public static final Handler asHandler(Looper instance, final boolean b) {
        Intrinsics.checkParameterIsNotNull(instance, "$this$asHandler");
        Label_0153: {
            if (!b || Build$VERSION.SDK_INT < 16) {
                break Label_0153;
            }
            if (Build$VERSION.SDK_INT >= 28) {
                final Object invoke = Handler.class.getDeclaredMethod("createAsync", Looper.class).invoke(null, instance);
                if (invoke != null) {
                    return (Handler)invoke;
                }
                throw new TypeCastException("null cannot be cast to non-null type android.os.Handler");
            }
            while (true) {
                while (true) {
                    try {
                        final Constructor<Handler> declaredConstructor = Handler.class.getDeclaredConstructor(Looper.class, Handler$Callback.class, Boolean.TYPE);
                        Intrinsics.checkExpressionValueIsNotNull(declaredConstructor, "Handler::class.java.getD\u2026:class.javaPrimitiveType)");
                        instance = (Looper)declaredConstructor.newInstance(instance, null, true);
                        Intrinsics.checkExpressionValueIsNotNull(instance, "constructor.newInstance(this, null, true)");
                        return (Handler)instance;
                        return new Handler(instance);
                        return new Handler(instance);
                    }
                    catch (NoSuchMethodException ex) {}
                    continue;
                }
            }
        }
    }
    
    public static final Object awaitFrame(final Continuation<? super Long> continuation) {
        final Choreographer choreographer = HandlerDispatcherKt.choreographer;
        if (choreographer != null) {
            final CancellableContinuationImpl cancellableContinuationImpl = new CancellableContinuationImpl(IntrinsicsKt__IntrinsicsJvmKt.intercepted((Continuation<? super T>)continuation), 1);
            postFrameCallback(choreographer, cancellableContinuationImpl);
            final Object result = cancellableContinuationImpl.getResult();
            if (result == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                DebugProbesKt.probeCoroutineSuspended(continuation);
            }
            return result;
        }
        final CancellableContinuationImpl cancellableContinuationImpl2 = new CancellableContinuationImpl(IntrinsicsKt__IntrinsicsJvmKt.intercepted((Continuation<? super T>)continuation), 1);
        Dispatchers.getMain().dispatch(EmptyCoroutineContext.INSTANCE, (Runnable)new HandlerDispatcherKt$$special$$inlined$Runnable.HandlerDispatcherKt$$special$$inlined$Runnable$1((CancellableContinuation)cancellableContinuationImpl2));
        final Object result2 = cancellableContinuationImpl2.getResult();
        if (result2 == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            DebugProbesKt.probeCoroutineSuspended(continuation);
        }
        return result2;
    }
    
    public static final HandlerDispatcher from(final Handler handler) {
        return from$default(handler, null, 1, null);
    }
    
    public static final HandlerDispatcher from(final Handler handler, final String s) {
        Intrinsics.checkParameterIsNotNull(handler, "$this$asCoroutineDispatcher");
        return new HandlerContext(handler, s);
    }
    
    public static /* synthetic */ HandlerDispatcher from$default(final Handler handler, String s, final int n, final Object o) {
        if ((n & 0x1) != 0x0) {
            s = null;
        }
        return from(handler, s);
    }
    
    private static final void postFrameCallback(final Choreographer choreographer, final CancellableContinuation<? super Long> cancellableContinuation) {
        choreographer.postFrameCallback((Choreographer$FrameCallback)new HandlerDispatcherKt$postFrameCallback.HandlerDispatcherKt$postFrameCallback$1((CancellableContinuation)cancellableContinuation));
    }
    
    private static final void updateChoreographerAndPostFrameCallback(final CancellableContinuation<? super Long> cancellableContinuation) {
        Choreographer choreographer = HandlerDispatcherKt.choreographer;
        if (choreographer == null) {
            choreographer = Choreographer.getInstance();
            if (choreographer == null) {
                Intrinsics.throwNpe();
            }
            HandlerDispatcherKt.choreographer = choreographer;
        }
        postFrameCallback(choreographer, cancellableContinuation);
    }
}
