// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.android;

import kotlin.jvm.functions.Function1;
import kotlin.Unit;
import kotlinx.coroutines.CancellableContinuation;
import android.os.Looper;
import kotlinx.coroutines.DisposableHandle;
import kotlinx.coroutines.MainCoroutineDispatcher;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Intrinsics;
import android.os.Handler;
import kotlin.Metadata;
import kotlinx.coroutines.Delay;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u001b\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\u0007B!\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u001c\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\n\u0010\u0013\u001a\u00060\u0014j\u0002`\u0015H\u0016J\u0013\u0010\u0016\u001a\u00020\t2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0096\u0002J\b\u0010\u0019\u001a\u00020\u001aH\u0016J\u001c\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\n\u0010\u0013\u001a\u00060\u0014j\u0002`\u0015H\u0016J\u0010\u0010\u001f\u001a\u00020\t2\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u001e\u0010 \u001a\u00020\u00102\u0006\u0010\u001d\u001a\u00020\u001e2\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00100\"H\u0016J\b\u0010#\u001a\u00020\u0006H\u0016R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u0000X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u00020\u0000X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u000e\u0010\b\u001a\u00020\tX\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006$" }, d2 = { "Lkotlinx/coroutines/android/HandlerContext;", "Lkotlinx/coroutines/android/HandlerDispatcher;", "Lkotlinx/coroutines/Delay;", "handler", "Landroid/os/Handler;", "name", "", "(Landroid/os/Handler;Ljava/lang/String;)V", "invokeImmediately", "", "(Landroid/os/Handler;Ljava/lang/String;Z)V", "_immediate", "immediate", "getImmediate", "()Lkotlinx/coroutines/android/HandlerContext;", "dispatch", "", "context", "Lkotlin/coroutines/CoroutineContext;", "block", "Ljava/lang/Runnable;", "Lkotlinx/coroutines/Runnable;", "equals", "other", "", "hashCode", "", "invokeOnTimeout", "Lkotlinx/coroutines/DisposableHandle;", "timeMillis", "", "isDispatchNeeded", "scheduleResumeAfterDelay", "continuation", "Lkotlinx/coroutines/CancellableContinuation;", "toString", "kotlinx-coroutines-android" }, k = 1, mv = { 1, 1, 15 })
public final class HandlerContext extends HandlerDispatcher implements Delay
{
    private volatile HandlerContext _immediate;
    private final Handler handler;
    private final HandlerContext immediate;
    private final boolean invokeImmediately;
    private final String name;
    
    public HandlerContext(final Handler handler, final String s) {
        Intrinsics.checkParameterIsNotNull(handler, "handler");
        this(handler, s, false);
    }
    
    private HandlerContext(final Handler handler, final String name, final boolean invokeImmediately) {
        final HandlerContext handlerContext = null;
        super((DefaultConstructorMarker)null);
        this.handler = handler;
        this.name = name;
        this.invokeImmediately = invokeImmediately;
        HandlerContext immediate = handlerContext;
        if (this.invokeImmediately) {
            immediate = this;
        }
        this._immediate = immediate;
        HandlerContext immediate2 = this._immediate;
        if (immediate2 == null) {
            immediate2 = new HandlerContext(this.handler, this.name, true);
            this._immediate = immediate2;
        }
        this.immediate = immediate2;
    }
    
    @Override
    public void dispatch(final CoroutineContext coroutineContext, final Runnable runnable) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        Intrinsics.checkParameterIsNotNull(runnable, "block");
        this.handler.post(runnable);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof HandlerContext && ((HandlerContext)o).handler == this.handler;
    }
    
    @Override
    public HandlerContext getImmediate() {
        return this.immediate;
    }
    
    @Override
    public int hashCode() {
        return System.identityHashCode(this.handler);
    }
    
    @Override
    public DisposableHandle invokeOnTimeout(final long n, final Runnable runnable) {
        Intrinsics.checkParameterIsNotNull(runnable, "block");
        this.handler.postDelayed(runnable, RangesKt___RangesKt.coerceAtMost(n, 4611686018427387903L));
        return (DisposableHandle)new HandlerContext$invokeOnTimeout.HandlerContext$invokeOnTimeout$1(this, runnable);
    }
    
    @Override
    public boolean isDispatchNeeded(final CoroutineContext coroutineContext) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        final boolean invokeImmediately = this.invokeImmediately;
        boolean b = true;
        if (invokeImmediately) {
            if (Intrinsics.areEqual(Looper.myLooper(), this.handler.getLooper()) ^ true) {
                return true;
            }
            b = false;
        }
        return b;
    }
    
    @Override
    public void scheduleResumeAfterDelay(final long n, final CancellableContinuation<? super Unit> cancellableContinuation) {
        Intrinsics.checkParameterIsNotNull(cancellableContinuation, "continuation");
        final Runnable runnable = (Runnable)new HandlerContext$scheduleResumeAfterDelay$$inlined$Runnable.HandlerContext$scheduleResumeAfterDelay$$inlined$Runnable$1(this, (CancellableContinuation)cancellableContinuation);
        this.handler.postDelayed(runnable, RangesKt___RangesKt.coerceAtMost(n, 4611686018427387903L));
        cancellableContinuation.invokeOnCancellation((Function1)new HandlerContext$scheduleResumeAfterDelay.HandlerContext$scheduleResumeAfterDelay$1(this, runnable));
    }
    
    @Override
    public String toString() {
        String s = this.name;
        if (s != null) {
            if (this.invokeImmediately) {
                final StringBuilder sb = new StringBuilder();
                sb.append(this.name);
                sb.append(" [immediate]");
                return sb.toString();
            }
        }
        else {
            s = this.handler.toString();
            Intrinsics.checkExpressionValueIsNotNull(s, "handler.toString()");
        }
        return s;
    }
}
