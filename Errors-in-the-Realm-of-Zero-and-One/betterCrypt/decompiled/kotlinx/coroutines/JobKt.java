// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import java.util.concurrent.Future;
import kotlin.coroutines.Continuation;
import java.util.concurrent.CancellationException;
import kotlin.coroutines.CoroutineContext;
import kotlin.DeprecationLevel;
import kotlin.Deprecated;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "kotlinx/coroutines/JobKt__FutureKt", "kotlinx/coroutines/JobKt__JobKt" }, k = 4, mv = { 1, 1, 15 })
public final class JobKt
{
    public static final DisposableHandle DisposableHandle(final Function0<Unit> function0) {
        return JobKt__JobKt.DisposableHandle(function0);
    }
    
    public static final CompletableJob Job(final Job job) {
        return JobKt__JobKt.Job(job);
    }
    
    public static final void cancel(final CoroutineContext coroutineContext, final CancellationException ex) {
        JobKt__JobKt.cancel(coroutineContext, ex);
    }
    
    public static final void cancel(final Job job, final String s, final Throwable t) {
        JobKt__JobKt.cancel(job, s, t);
    }
    
    public static final Object cancelAndJoin(final Job job, final Continuation<? super Unit> continuation) {
        return JobKt__JobKt.cancelAndJoin(job, continuation);
    }
    
    public static final void cancelChildren(final CoroutineContext coroutineContext, final CancellationException ex) {
        JobKt__JobKt.cancelChildren(coroutineContext, ex);
    }
    
    public static final void cancelChildren(final Job job, final CancellationException ex) {
        JobKt__JobKt.cancelChildren(job, ex);
    }
    
    public static final void cancelFutureOnCancellation(final CancellableContinuation<?> cancellableContinuation, final Future<?> future) {
        JobKt__FutureKt.cancelFutureOnCancellation(cancellableContinuation, future);
    }
    
    public static final DisposableHandle cancelFutureOnCompletion(final Job job, final Future<?> future) {
        return JobKt__FutureKt.cancelFutureOnCompletion(job, future);
    }
    
    public static final DisposableHandle disposeOnCompletion(final Job job, final DisposableHandle disposableHandle) {
        return JobKt__JobKt.disposeOnCompletion(job, disposableHandle);
    }
    
    public static final void ensureActive(final CoroutineContext coroutineContext) {
        JobKt__JobKt.ensureActive(coroutineContext);
    }
    
    public static final void ensureActive(final Job job) {
        JobKt__JobKt.ensureActive(job);
    }
    
    public static final boolean isActive(final CoroutineContext coroutineContext) {
        return JobKt__JobKt.isActive(coroutineContext);
    }
}
