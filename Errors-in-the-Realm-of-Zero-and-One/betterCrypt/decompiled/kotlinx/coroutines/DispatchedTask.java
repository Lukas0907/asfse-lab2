// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlinx.coroutines.scheduling.TaskContext;
import kotlin.TypeCastException;
import kotlin.Unit;
import kotlin.ResultKt;
import kotlinx.coroutines.internal.StackTraceRecoveryKt;
import kotlin.Result;
import java.util.concurrent.CancellationException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.internal.ThreadContextKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import kotlinx.coroutines.scheduling.Task;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u00004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u000e\b \u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00002\u00060\u0002j\u0002`\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u001f\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0010¢\u0006\u0002\b\u0011J\u0019\u0010\u0012\u001a\u0004\u0018\u00010\u00102\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0000¢\u0006\u0002\b\u0013J\u001f\u0010\u0014\u001a\u0002H\u0001\"\u0004\b\u0001\u0010\u00012\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0010¢\u0006\u0004\b\u0015\u0010\u0016J!\u0010\u0017\u001a\u00020\f2\b\u0010\u0018\u001a\u0004\u0018\u00010\u00102\b\u0010\u0019\u001a\u0004\u0018\u00010\u0010H\u0000¢\u0006\u0002\b\u001aJ\u0006\u0010\u001b\u001a\u00020\fJ\u000f\u0010\u001c\u001a\u0004\u0018\u00010\u000eH ¢\u0006\u0002\b\u001dR\u0018\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\bX \u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u0012\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000¨\u0006\u001e" }, d2 = { "Lkotlinx/coroutines/DispatchedTask;", "T", "Lkotlinx/coroutines/scheduling/Task;", "Lkotlinx/coroutines/SchedulerTask;", "resumeMode", "", "(I)V", "delegate", "Lkotlin/coroutines/Continuation;", "getDelegate$kotlinx_coroutines_core", "()Lkotlin/coroutines/Continuation;", "cancelResult", "", "state", "", "cause", "", "cancelResult$kotlinx_coroutines_core", "getExceptionalResult", "getExceptionalResult$kotlinx_coroutines_core", "getSuccessfulResult", "getSuccessfulResult$kotlinx_coroutines_core", "(Ljava/lang/Object;)Ljava/lang/Object;", "handleFatalException", "exception", "finallyException", "handleFatalException$kotlinx_coroutines_core", "run", "takeState", "takeState$kotlinx_coroutines_core", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public abstract class DispatchedTask<T> extends Task
{
    public int resumeMode;
    
    public DispatchedTask(final int resumeMode) {
        this.resumeMode = resumeMode;
    }
    
    public void cancelResult$kotlinx_coroutines_core(final Object o, final Throwable t) {
        Intrinsics.checkParameterIsNotNull(t, "cause");
    }
    
    public abstract Continuation<T> getDelegate$kotlinx_coroutines_core();
    
    public final Throwable getExceptionalResult$kotlinx_coroutines_core(Object o) {
        final boolean b = o instanceof CompletedExceptionally;
        final Throwable t = null;
        if (!b) {
            o = null;
        }
        final CompletedExceptionally completedExceptionally = (CompletedExceptionally)o;
        Throwable cause = t;
        if (completedExceptionally != null) {
            cause = completedExceptionally.cause;
        }
        return cause;
    }
    
    public <T> T getSuccessfulResult$kotlinx_coroutines_core(final Object o) {
        return (T)o;
    }
    
    public final void handleFatalException$kotlinx_coroutines_core(Throwable t, final Throwable t2) {
        if (t == null && t2 == null) {
            return;
        }
        if (t != null && t2 != null) {
            ExceptionsKt__ExceptionsKt.addSuppressed(t, t2);
        }
        if (t == null) {
            t = t2;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Fatal exception in coroutines machinery for ");
        sb.append(this);
        sb.append(". ");
        sb.append("Please read KDoc to 'handleFatalException' method and report this incident to maintainers");
        final String string = sb.toString();
        if (t == null) {
            Intrinsics.throwNpe();
        }
        CoroutineExceptionHandlerKt.handleCoroutineException(this.getDelegate$kotlinx_coroutines_core().getContext(), new CoroutinesInternalError(string, t));
    }
    
    @Override
    public final void run() {
        Object o = this.taskContext;
        Job job = null;
        final Throwable t = null;
        try {
            final Continuation<T> delegate$kotlinx_coroutines_core = this.getDelegate$kotlinx_coroutines_core();
            if (delegate$kotlinx_coroutines_core != null) {
                final DispatchedContinuation dispatchedContinuation = (DispatchedContinuation<T>)delegate$kotlinx_coroutines_core;
                final Continuation<T> continuation = dispatchedContinuation.continuation;
                final CoroutineContext context = continuation.getContext();
                final Object takeState$kotlinx_coroutines_core = this.takeState$kotlinx_coroutines_core();
                final Object updateThreadContext = ThreadContextKt.updateThreadContext(context, dispatchedContinuation.countOrElement);
                try {
                    final Throwable exceptionalResult$kotlinx_coroutines_core = this.getExceptionalResult$kotlinx_coroutines_core(takeState$kotlinx_coroutines_core);
                    if (ResumeModeKt.isCancellableMode(this.resumeMode)) {
                        job = context.get((CoroutineContext.Key<Job>)Job.Key);
                    }
                    if (exceptionalResult$kotlinx_coroutines_core == null && job != null && !job.isActive()) {
                        final CancellationException cancellationException = job.getCancellationException();
                        this.cancelResult$kotlinx_coroutines_core(takeState$kotlinx_coroutines_core, cancellationException);
                        final Result.Companion companion = Result.Companion;
                        continuation.resumeWith(Result.constructor-impl(ResultKt.createFailure(StackTraceRecoveryKt.recoverStackTrace(cancellationException, continuation))));
                    }
                    else if (exceptionalResult$kotlinx_coroutines_core != null) {
                        final Result.Companion companion2 = Result.Companion;
                        continuation.resumeWith(Result.constructor-impl(ResultKt.createFailure(StackTraceRecoveryKt.recoverStackTrace(exceptionalResult$kotlinx_coroutines_core, continuation))));
                    }
                    else {
                        final Object successfulResult$kotlinx_coroutines_core = this.getSuccessfulResult$kotlinx_coroutines_core(takeState$kotlinx_coroutines_core);
                        final Result.Companion companion3 = Result.Companion;
                        continuation.resumeWith(Result.constructor-impl(successfulResult$kotlinx_coroutines_core));
                    }
                    final Unit instance = Unit.INSTANCE;
                    ThreadContextKt.restoreThreadContext(context, updateThreadContext);
                    Object constructor-impl = null;
                    try {
                        final Result.Companion companion4 = Result.Companion;
                        final DispatchedTask dispatchedTask = this;
                        ((TaskContext)o).afterTask();
                        Result.constructor-impl(Unit.INSTANCE);
                    }
                    finally {
                        o = Result.Companion;
                        final Throwable t2;
                        constructor-impl = Result.constructor-impl(ResultKt.createFailure(t2));
                    }
                    this.handleFatalException$kotlinx_coroutines_core(t, Result.exceptionOrNull-impl(constructor-impl));
                    return;
                }
                finally {
                    ThreadContextKt.restoreThreadContext(context, updateThreadContext);
                }
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.DispatchedContinuation<T>");
        }
        finally {
            Object constructor-impl2 = null;
            try {
                final Result.Companion companion5 = Result.Companion;
                final DispatchedTask dispatchedTask2 = this;
                ((TaskContext)o).afterTask();
                Result.constructor-impl(Unit.INSTANCE);
            }
            finally {
                final Result.Companion companion6 = Result.Companion;
                final Throwable t3;
                constructor-impl2 = Result.constructor-impl(ResultKt.createFailure(t3));
            }
            final Throwable t4;
            this.handleFatalException$kotlinx_coroutines_core(t4, Result.exceptionOrNull-impl(constructor-impl2));
        }
    }
    
    public abstract Object takeState$kotlinx_coroutines_core();
}
