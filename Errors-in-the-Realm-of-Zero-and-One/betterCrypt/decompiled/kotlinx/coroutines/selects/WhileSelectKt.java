// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.selects;

import kotlin.jvm.internal.InlineMarker;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.ResultKt;
import kotlin.coroutines.Continuation;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a2\u0010\u0000\u001a\u00020\u00012\u001f\b\u0004\u0010\u0002\u001a\u0019\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0004\u0012\u00020\u00010\u0003¢\u0006\u0002\b\u0006H\u0087H\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0007\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\b" }, d2 = { "whileSelect", "", "builder", "Lkotlin/Function1;", "Lkotlinx/coroutines/selects/SelectBuilder;", "", "Lkotlin/ExtensionFunctionType;", "(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class WhileSelectKt
{
    public static final Object whileSelect(Function1<? super SelectBuilder<? super Boolean>, Unit> l$0, Continuation<? super Unit> o) {
        Label_0047: {
            if (o instanceof WhileSelectKt$whileSelect.WhileSelectKt$whileSelect$1) {
                final WhileSelectKt$whileSelect.WhileSelectKt$whileSelect$1 whileSelectKt$whileSelect$1 = (WhileSelectKt$whileSelect.WhileSelectKt$whileSelect$1)o;
                if ((whileSelectKt$whileSelect$1.label & Integer.MIN_VALUE) != 0x0) {
                    whileSelectKt$whileSelect$1.label += Integer.MIN_VALUE;
                    o = whileSelectKt$whileSelect$1;
                    break Label_0047;
                }
            }
            o = new WhileSelectKt$whileSelect.WhileSelectKt$whileSelect$1((Continuation)o);
        }
        Object o2 = ((WhileSelectKt$whileSelect.WhileSelectKt$whileSelect$1)o).result;
        final Object coroutine_SUSPENDED = IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
        final int label = ((WhileSelectKt$whileSelect.WhileSelectKt$whileSelect$1)o).label;
        while (true) {
            Label_0171: {
                if (label != 0) {
                    if (label == 1) {
                        l$0 = (Function1)((WhileSelectKt$whileSelect.WhileSelectKt$whileSelect$1)o).L$0;
                        ResultKt.throwOnFailure(o2);
                        break Label_0171;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                else {
                    ResultKt.throwOnFailure(o2);
                }
                ((WhileSelectKt$whileSelect.WhileSelectKt$whileSelect$1)o).L$0 = l$0;
                ((WhileSelectKt$whileSelect.WhileSelectKt$whileSelect$1)o).label = 1;
                final Continuation<?> continuation = (Continuation<?>)o;
                final SelectBuilderImpl selectBuilderImpl = new SelectBuilderImpl((Continuation<? super Object>)continuation);
                try {
                    l$0.invoke(selectBuilderImpl);
                }
                finally {
                    final Throwable t;
                    selectBuilderImpl.handleBuilderException(t);
                }
                o2 = selectBuilderImpl.getResult();
                if (o2 == IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                    DebugProbesKt.probeCoroutineSuspended(continuation);
                }
                if (o2 == coroutine_SUSPENDED) {
                    return coroutine_SUSPENDED;
                }
            }
            if (o2) {
                continue;
            }
            break;
        }
        return Unit.INSTANCE;
    }
    
    private static final Object whileSelect$$forInline(final Function1 function1, final Continuation continuation) {
        Object result;
        do {
            InlineMarker.mark(0);
            final SelectBuilderImpl selectBuilderImpl = new SelectBuilderImpl(continuation);
            try {
                function1.invoke(selectBuilderImpl);
            }
            finally {
                final Throwable t;
                selectBuilderImpl.handleBuilderException(t);
            }
            result = selectBuilderImpl.getResult();
            if (result == IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                DebugProbesKt.probeCoroutineSuspended(continuation);
            }
            InlineMarker.mark(1);
        } while (result);
        return Unit.INSTANCE;
    }
}
