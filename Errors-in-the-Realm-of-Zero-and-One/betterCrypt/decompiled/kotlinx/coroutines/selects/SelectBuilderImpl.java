// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.selects;

import kotlinx.coroutines.JobCancellingNode;
import kotlinx.coroutines.internal.AtomicOp;
import kotlinx.coroutines.CompletedExceptionallyKt;
import kotlinx.coroutines.DispatchedKt;
import kotlinx.coroutines.internal.AtomicDesc;
import kotlinx.coroutines.DelayKt;
import kotlinx.coroutines.intrinsics.UndispatchedKt;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineExceptionHandlerKt;
import kotlinx.coroutines.internal.StackTraceRecoveryKt;
import java.util.concurrent.CancellationException;
import kotlin.ResultKt;
import kotlin.Result;
import kotlinx.coroutines.CompletedExceptionally;
import kotlin.jvm.functions.Function1;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.internal.OpDescriptor;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlinx.coroutines.DebugKt;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.TypeCastException;
import kotlinx.coroutines.internal.LockFreeLinkedListNode;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.DisposableHandle;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Metadata;
import kotlin.coroutines.jvm.internal.CoroutineStackFrame;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.internal.LockFreeLinkedListHead;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u009a\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0004\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u000e\b\u0001\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00002\u00020\u00022\b\u0012\u0004\u0012\u00028\u00000\u00032\b\u0012\u0004\u0012\u00028\u00000\u00042\b\u0012\u0004\u0012\u00028\u00000\u00052\u00060\u0006j\u0002`\u0007:\u0003RSTB\u0015\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005¢\u0006\u0004\b\t\u0010\nJ\u0017\u0010\u000e\u001a\u00020\r2\u0006\u0010\f\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ\u000f\u0010\u0010\u001a\u00020\rH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J.\u0010\u0016\u001a\u00020\r2\u000e\u0010\u0014\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00130\u00122\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\r0\u0012H\u0082\b¢\u0006\u0004\b\u0016\u0010\u0017J\u0011\u0010\u0018\u001a\u0004\u0018\u00010\u0013H\u0001¢\u0006\u0004\b\u0018\u0010\u0019J\u0017\u0010\u001c\u001a\n\u0018\u00010\u001aj\u0004\u0018\u0001`\u001bH\u0016¢\u0006\u0004\b\u001c\u0010\u001dJ\u0017\u0010 \u001a\u00020\r2\u0006\u0010\u001f\u001a\u00020\u001eH\u0001¢\u0006\u0004\b \u0010!J\u000f\u0010\"\u001a\u00020\rH\u0002¢\u0006\u0004\b\"\u0010\u0011J8\u0010&\u001a\u00020\r2\u0006\u0010$\u001a\u00020#2\u001c\u0010\u0015\u001a\u0018\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u00130%H\u0016\u00f8\u0001\u0000¢\u0006\u0004\b&\u0010'J\u0019\u0010*\u001a\u0004\u0018\u00010\u00132\u0006\u0010)\u001a\u00020(H\u0016¢\u0006\u0004\b*\u0010+J\u0017\u0010-\u001a\u00020\r2\u0006\u0010,\u001a\u00020\u001eH\u0016¢\u0006\u0004\b-\u0010!J \u00100\u001a\u00020\r2\f\u0010/\u001a\b\u0012\u0004\u0012\u00028\u00000.H\u0016\u00f8\u0001\u0000¢\u0006\u0004\b0\u00101J\u0019\u00104\u001a\u0002032\b\u00102\u001a\u0004\u0018\u00010\u0013H\u0016¢\u0006\u0004\b4\u00105J5\u00107\u001a\u00020\r*\u0002062\u001c\u0010\u0015\u001a\u0018\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u00130%H\u0096\u0002\u00f8\u0001\u0000¢\u0006\u0004\b7\u00108JG\u00107\u001a\u00020\r\"\u0004\b\u0001\u00109*\b\u0012\u0004\u0012\u00028\u00010:2\"\u0010\u0015\u001a\u001e\b\u0001\u0012\u0004\u0012\u00028\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u00130;H\u0096\u0002\u00f8\u0001\u0000¢\u0006\u0004\b7\u0010<J[\u00107\u001a\u00020\r\"\u0004\b\u0001\u0010=\"\u0004\b\u0002\u00109*\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020>2\u0006\u0010?\u001a\u00028\u00012\"\u0010\u0015\u001a\u001e\b\u0001\u0012\u0004\u0012\u00028\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u00130;H\u0096\u0002\u00f8\u0001\u0000¢\u0006\u0004\b7\u0010@R\u001e\u0010C\u001a\n\u0018\u00010\u0006j\u0004\u0018\u0001`\u00078V@\u0016X\u0096\u0004¢\u0006\u0006\u001a\u0004\bA\u0010BR\u001c\u0010F\u001a\b\u0012\u0004\u0012\u00028\u00000\u00058V@\u0016X\u0096\u0004¢\u0006\u0006\u001a\u0004\bD\u0010ER\u0016\u0010J\u001a\u00020G8V@\u0016X\u0096\u0004¢\u0006\u0006\u001a\u0004\bH\u0010IR\u0016\u0010K\u001a\u0002038V@\u0016X\u0096\u0004¢\u0006\u0006\u001a\u0004\bK\u0010LR\u0018\u0010M\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bM\u0010NR\u0018\u0010P\u001a\u0004\u0018\u00010\u00138B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\bO\u0010\u0019R\u001c\u0010\b\u001a\b\u0012\u0004\u0012\u00028\u00000\u00058\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010Q\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006U" }, d2 = { "Lkotlinx/coroutines/selects/SelectBuilderImpl;", "R", "Lkotlinx/coroutines/internal/LockFreeLinkedListHead;", "Lkotlinx/coroutines/selects/SelectBuilder;", "Lkotlinx/coroutines/selects/SelectInstance;", "Lkotlin/coroutines/Continuation;", "Lkotlin/coroutines/jvm/internal/CoroutineStackFrame;", "Lkotlinx/coroutines/internal/CoroutineStackFrame;", "uCont", "<init>", "(Lkotlin/coroutines/Continuation;)V", "Lkotlinx/coroutines/DisposableHandle;", "handle", "", "disposeOnSelect", "(Lkotlinx/coroutines/DisposableHandle;)V", "doAfterSelect", "()V", "Lkotlin/Function0;", "", "value", "block", "doResume", "(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "getResult", "()Ljava/lang/Object;", "Ljava/lang/StackTraceElement;", "Lkotlinx/coroutines/internal/StackTraceElement;", "getStackTraceElement", "()Ljava/lang/StackTraceElement;", "", "e", "handleBuilderException", "(Ljava/lang/Throwable;)V", "initCancellability", "", "timeMillis", "Lkotlin/Function1;", "onTimeout", "(JLkotlin/jvm/functions/Function1;)V", "Lkotlinx/coroutines/internal/AtomicDesc;", "desc", "performAtomicTrySelect", "(Lkotlinx/coroutines/internal/AtomicDesc;)Ljava/lang/Object;", "exception", "resumeSelectCancellableWithException", "Lkotlin/Result;", "result", "resumeWith", "(Ljava/lang/Object;)V", "idempotent", "", "trySelect", "(Ljava/lang/Object;)Z", "Lkotlinx/coroutines/selects/SelectClause0;", "invoke", "(Lkotlinx/coroutines/selects/SelectClause0;Lkotlin/jvm/functions/Function1;)V", "Q", "Lkotlinx/coroutines/selects/SelectClause1;", "Lkotlin/Function2;", "(Lkotlinx/coroutines/selects/SelectClause1;Lkotlin/jvm/functions/Function2;)V", "P", "Lkotlinx/coroutines/selects/SelectClause2;", "param", "(Lkotlinx/coroutines/selects/SelectClause2;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)V", "getCallerFrame", "()Lkotlin/coroutines/jvm/internal/CoroutineStackFrame;", "callerFrame", "getCompletion", "()Lkotlin/coroutines/Continuation;", "completion", "Lkotlin/coroutines/CoroutineContext;", "getContext", "()Lkotlin/coroutines/CoroutineContext;", "context", "isSelected", "()Z", "parentHandle", "Lkotlinx/coroutines/DisposableHandle;", "getState", "state", "Lkotlin/coroutines/Continuation;", "AtomicSelectOp", "DisposeNode", "SelectOnCancelling", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class SelectBuilderImpl<R> extends LockFreeLinkedListHead implements SelectBuilder<R>, SelectInstance<R>, Continuation<R>, CoroutineStackFrame
{
    static final AtomicReferenceFieldUpdater _result$FU;
    static final AtomicReferenceFieldUpdater _state$FU;
    volatile Object _result;
    volatile Object _state;
    private volatile DisposableHandle parentHandle;
    private final Continuation<R> uCont;
    
    static {
        _state$FU = AtomicReferenceFieldUpdater.newUpdater(SelectBuilderImpl.class, Object.class, "_state");
        _result$FU = AtomicReferenceFieldUpdater.newUpdater(SelectBuilderImpl.class, Object.class, "_result");
    }
    
    public SelectBuilderImpl(final Continuation<? super R> uCont) {
        Intrinsics.checkParameterIsNotNull(uCont, "uCont");
        this.uCont = (Continuation<R>)uCont;
        this._state = this;
        this._result = SelectKt.access$getUNDECIDED$p();
    }
    
    private final void doAfterSelect() {
        final DisposableHandle parentHandle = this.parentHandle;
        if (parentHandle != null) {
            parentHandle.dispose();
        }
        final Object next = this.getNext();
        if (next != null) {
            for (LockFreeLinkedListNode nextNode = (LockFreeLinkedListNode)next; Intrinsics.areEqual(nextNode, this) ^ true; nextNode = nextNode.getNextNode()) {
                if (nextNode instanceof DisposeNode) {
                    ((DisposeNode)nextNode).handle.dispose();
                }
            }
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }
    
    private final void doResume(final Function0<?> function0, final Function0<Unit> function2) {
        if (DebugKt.getASSERTIONS_ENABLED()) {
            if (!this.isSelected()) {
                throw new AssertionError();
            }
        }
        while (true) {
            final Object result = this._result;
            if (result == SelectKt.access$getUNDECIDED$p()) {
                if (SelectBuilderImpl._result$FU.compareAndSet(this, SelectKt.access$getUNDECIDED$p(), function0.invoke())) {
                    return;
                }
                continue;
            }
            else {
                if (result != IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                    throw new IllegalStateException("Already resumed");
                }
                if (SelectBuilderImpl._result$FU.compareAndSet(this, IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED(), SelectKt.access$getRESUMED$p())) {
                    function2.invoke();
                    return;
                }
                continue;
            }
        }
    }
    
    private final Object getState() {
        Object state;
        while (true) {
            state = this._state;
            if (!(state instanceof OpDescriptor)) {
                break;
            }
            ((OpDescriptor)state).perform(this);
        }
        return state;
    }
    
    private final void initCancellability() {
        final Job job = this.getContext().get((CoroutineContext.Key<Job>)Job.Key);
        if (job != null) {
            final DisposableHandle invokeOnCompletion$default = Job.DefaultImpls.invokeOnCompletion$default(job, true, false, new SelectOnCancelling(job), 2, null);
            this.parentHandle = invokeOnCompletion$default;
            if (this.isSelected()) {
                invokeOnCompletion$default.dispose();
            }
        }
    }
    
    @Override
    public void disposeOnSelect(final DisposableHandle disposableHandle) {
        Intrinsics.checkParameterIsNotNull(disposableHandle, "handle");
        final DisposeNode disposeNode = new DisposeNode(disposableHandle);
        if (!this.isSelected()) {
            this.addLast(disposeNode);
            if (!this.isSelected()) {
                return;
            }
        }
        disposableHandle.dispose();
    }
    
    @Override
    public CoroutineStackFrame getCallerFrame() {
        Object uCont;
        if (!((uCont = this.uCont) instanceof CoroutineStackFrame)) {
            uCont = null;
        }
        return (CoroutineStackFrame)uCont;
    }
    
    @Override
    public Continuation<R> getCompletion() {
        return this;
    }
    
    @Override
    public CoroutineContext getContext() {
        return this.uCont.getContext();
    }
    
    public final Object getResult() {
        if (!this.isSelected()) {
            this.initCancellability();
        }
        Object o;
        if ((o = this._result) == SelectKt.access$getUNDECIDED$p()) {
            if (SelectBuilderImpl._result$FU.compareAndSet(this, SelectKt.access$getUNDECIDED$p(), IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED())) {
                return IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
            }
            o = this._result;
        }
        if (o == SelectKt.access$getRESUMED$p()) {
            throw new IllegalStateException("Already resumed");
        }
        if (!(o instanceof CompletedExceptionally)) {
            return o;
        }
        throw ((CompletedExceptionally)o).cause;
    }
    
    @Override
    public StackTraceElement getStackTraceElement() {
        return null;
    }
    
    public final void handleBuilderException(final Throwable t) {
        Intrinsics.checkParameterIsNotNull(t, "e");
        if (this.trySelect(null)) {
            final Result.Companion companion = Result.Companion;
            this.resumeWith(Result.constructor-impl(ResultKt.createFailure(t)));
            return;
        }
        if (!(t instanceof CancellationException)) {
            final Object result = this.getResult();
            if (!(result instanceof CompletedExceptionally) || StackTraceRecoveryKt.unwrap(((CompletedExceptionally)result).cause) != StackTraceRecoveryKt.unwrap(t)) {
                CoroutineExceptionHandlerKt.handleCoroutineException(this.getContext(), t);
            }
        }
    }
    
    @Override
    public void invoke(final SelectClause0 selectClause0, final Function1<? super Continuation<? super R>, ?> function1) {
        Intrinsics.checkParameterIsNotNull(selectClause0, "$this$invoke");
        Intrinsics.checkParameterIsNotNull(function1, "block");
        selectClause0.registerSelectClause0((SelectInstance<? super Object>)this, (Function1<? super Continuation<? super Object>, ?>)function1);
    }
    
    @Override
    public <Q> void invoke(final SelectClause1<? extends Q> selectClause1, final Function2<? super Q, ? super Continuation<? super R>, ?> function2) {
        Intrinsics.checkParameterIsNotNull(selectClause1, "$this$invoke");
        Intrinsics.checkParameterIsNotNull(function2, "block");
        selectClause1.registerSelectClause1((SelectInstance<? super Object>)this, (Function2<? super Q, ? super Continuation<? super Object>, ?>)function2);
    }
    
    @Override
    public <P, Q> void invoke(final SelectClause2<? super P, ? extends Q> selectClause2, final P p3, final Function2<? super Q, ? super Continuation<? super R>, ?> function2) {
        Intrinsics.checkParameterIsNotNull(selectClause2, "$this$invoke");
        Intrinsics.checkParameterIsNotNull(function2, "block");
        selectClause2.registerSelectClause2((SelectInstance<? super Object>)this, p3, (Function2<? super Q, ? super Continuation<? super Object>, ?>)function2);
    }
    
    @Override
    public <P, Q> void invoke(final SelectClause2<? super P, ? extends Q> selectClause2, final Function2<? super Q, ? super Continuation<? super R>, ?> function2) {
        Intrinsics.checkParameterIsNotNull(selectClause2, "$this$invoke");
        Intrinsics.checkParameterIsNotNull(function2, "block");
        DefaultImpls.invoke((SelectBuilder<? super Object>)this, (SelectClause2<? super Object, ?>)selectClause2, (Function2<? super Object, ? super Continuation<? super Object>, ?>)function2);
    }
    
    @Override
    public boolean isSelected() {
        return this.getState() != this;
    }
    
    @Override
    public void onTimeout(final long n, final Function1<? super Continuation<? super R>, ?> function1) {
        Intrinsics.checkParameterIsNotNull(function1, "block");
        if (n <= 0L) {
            if (this.trySelect(null)) {
                UndispatchedKt.startCoroutineUnintercepted((Function1<? super Continuation<? super Object>, ?>)function1, this.getCompletion());
            }
            return;
        }
        this.disposeOnSelect(DelayKt.getDelay(this.getContext()).invokeOnTimeout(n, (Runnable)new SelectBuilderImpl$onTimeout$$inlined$Runnable.SelectBuilderImpl$onTimeout$$inlined$Runnable$1(this, (Function1)function1)));
    }
    
    @Override
    public Object performAtomicTrySelect(final AtomicDesc atomicDesc) {
        Intrinsics.checkParameterIsNotNull(atomicDesc, "desc");
        return new AtomicSelectOp(atomicDesc).perform(null);
    }
    
    @Override
    public void resumeSelectCancellableWithException(final Throwable t) {
        Intrinsics.checkParameterIsNotNull(t, "exception");
        if (DebugKt.getASSERTIONS_ENABLED()) {
            if (!this.isSelected()) {
                throw new AssertionError();
            }
        }
        while (true) {
            final Object result = this._result;
            if (result == SelectKt.access$getUNDECIDED$p()) {
                if (SelectBuilderImpl._result$FU.compareAndSet(this, SelectKt.access$getUNDECIDED$p(), new CompletedExceptionally(t, false, 2, null))) {
                    return;
                }
                continue;
            }
            else {
                if (result != IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                    throw new IllegalStateException("Already resumed");
                }
                if (SelectBuilderImpl._result$FU.compareAndSet(this, IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED(), SelectKt.access$getRESUMED$p())) {
                    DispatchedKt.resumeCancellableWithException(IntrinsicsKt__IntrinsicsJvmKt.intercepted((Continuation<? super Object>)this.uCont), t);
                    return;
                }
                continue;
            }
        }
    }
    
    @Override
    public void resumeWith(final Object o) {
        if (DebugKt.getASSERTIONS_ENABLED()) {
            if (!this.isSelected()) {
                throw new AssertionError();
            }
        }
        while (true) {
            final Object result = this._result;
            if (result == SelectKt.access$getUNDECIDED$p()) {
                if (SelectBuilderImpl._result$FU.compareAndSet(this, SelectKt.access$getUNDECIDED$p(), CompletedExceptionallyKt.toState(o))) {
                    return;
                }
                continue;
            }
            else {
                if (result != IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                    throw new IllegalStateException("Already resumed");
                }
                if (!SelectBuilderImpl._result$FU.compareAndSet(this, IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED(), SelectKt.access$getRESUMED$p())) {
                    continue;
                }
                if (Result.isFailure-impl(o)) {
                    final Continuation<R> uCont = this.uCont;
                    final Throwable exceptionOrNull-impl = Result.exceptionOrNull-impl(o);
                    if (exceptionOrNull-impl == null) {
                        Intrinsics.throwNpe();
                    }
                    final Result.Companion companion = Result.Companion;
                    uCont.resumeWith(Result.constructor-impl(ResultKt.createFailure(StackTraceRecoveryKt.recoverStackTrace(exceptionOrNull-impl, uCont))));
                    return;
                }
                this.uCont.resumeWith(o);
            }
        }
    }
    
    @Override
    public boolean trySelect(final Object o) {
        if (DebugKt.getASSERTIONS_ENABLED()) {
            if (!(o instanceof OpDescriptor ^ true)) {
                throw new AssertionError();
            }
        }
        do {
            final Object state = this.getState();
            if (state == this) {
                continue;
            }
            return o != null && state == o;
        } while (!SelectBuilderImpl._state$FU.compareAndSet(this, this, o));
        this.doAfterSelect();
        return true;
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\b\u0082\u0004\u0018\u00002\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u001c\u0010\u0006\u001a\u00020\u00072\b\u0010\b\u001a\u0004\u0018\u00010\u00022\b\u0010\t\u001a\u0004\u0018\u00010\u0002H\u0016J\u0012\u0010\n\u001a\u00020\u00072\b\u0010\t\u001a\u0004\u0018\u00010\u0002H\u0002J\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00022\b\u0010\b\u001a\u0004\u0018\u00010\u0002H\u0016J\b\u0010\f\u001a\u0004\u0018\u00010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\r" }, d2 = { "Lkotlinx/coroutines/selects/SelectBuilderImpl$AtomicSelectOp;", "Lkotlinx/coroutines/internal/AtomicOp;", "", "desc", "Lkotlinx/coroutines/internal/AtomicDesc;", "(Lkotlinx/coroutines/selects/SelectBuilderImpl;Lkotlinx/coroutines/internal/AtomicDesc;)V", "complete", "", "affected", "failure", "completeSelect", "prepare", "prepareIfNotSelected", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private final class AtomicSelectOp extends AtomicOp<Object>
    {
        public final AtomicDesc desc;
        
        public AtomicSelectOp(final AtomicDesc desc) {
            Intrinsics.checkParameterIsNotNull(desc, "desc");
            this.desc = desc;
        }
        
        private final void completeSelect(final Object o) {
            final boolean b = o == null;
            Object this$0;
            if (b) {
                this$0 = null;
            }
            else {
                this$0 = SelectBuilderImpl.this;
            }
            if (SelectBuilderImpl._state$FU.compareAndSet(SelectBuilderImpl.this, this, this$0) && b) {
                SelectBuilderImpl.this.doAfterSelect();
            }
        }
        
        @Override
        public void complete(final Object o, final Object o2) {
            this.completeSelect(o2);
            this.desc.complete(this, o2);
        }
        
        @Override
        public Object prepare(Object prepareIfNotSelected) {
            if (prepareIfNotSelected == null) {
                prepareIfNotSelected = this.prepareIfNotSelected();
                if (prepareIfNotSelected != null) {
                    return prepareIfNotSelected;
                }
            }
            return this.desc.prepare(this);
        }
        
        public final Object prepareIfNotSelected() {
            final SelectBuilderImpl this$0 = SelectBuilderImpl.this;
            while (true) {
                final Object state = this$0._state;
                if (state == this) {
                    return null;
                }
                if (state instanceof OpDescriptor) {
                    ((AtomicSelectOp)state).perform(SelectBuilderImpl.this);
                }
                else {
                    final SelectBuilderImpl this$2 = SelectBuilderImpl.this;
                    if (state != this$2) {
                        return SelectKt.getALREADY_SELECTED();
                    }
                    if (SelectBuilderImpl._state$FU.compareAndSet(this$2, SelectBuilderImpl.this, this)) {
                        return null;
                    }
                    continue;
                }
            }
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0005" }, d2 = { "Lkotlinx/coroutines/selects/SelectBuilderImpl$DisposeNode;", "Lkotlinx/coroutines/internal/LockFreeLinkedListNode;", "handle", "Lkotlinx/coroutines/DisposableHandle;", "(Lkotlinx/coroutines/DisposableHandle;)V", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private static final class DisposeNode extends LockFreeLinkedListNode
    {
        public final DisposableHandle handle;
        
        public DisposeNode(final DisposableHandle handle) {
            Intrinsics.checkParameterIsNotNull(handle, "handle");
            this.handle = handle;
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0082\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0002\u0010\u0004J\u0013\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0096\u0002J\b\u0010\t\u001a\u00020\nH\u0016¨\u0006\u000b" }, d2 = { "Lkotlinx/coroutines/selects/SelectBuilderImpl$SelectOnCancelling;", "Lkotlinx/coroutines/JobCancellingNode;", "Lkotlinx/coroutines/Job;", "job", "(Lkotlinx/coroutines/selects/SelectBuilderImpl;Lkotlinx/coroutines/Job;)V", "invoke", "", "cause", "", "toString", "", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private final class SelectOnCancelling extends JobCancellingNode<Job>
    {
        public SelectOnCancelling(final Job job) {
            Intrinsics.checkParameterIsNotNull(job, "job");
            super(job);
        }
        
        @Override
        public void invoke(final Throwable t) {
            if (SelectBuilderImpl.this.trySelect(null)) {
                SelectBuilderImpl.this.resumeSelectCancellableWithException(this.job.getCancellationException());
            }
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("SelectOnCancelling[");
            sb.append(SelectBuilderImpl.this);
            sb.append(']');
            return sb.toString();
        }
    }
}
