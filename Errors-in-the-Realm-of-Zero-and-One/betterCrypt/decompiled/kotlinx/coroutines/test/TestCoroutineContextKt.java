// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.test;

import kotlin.ReplaceWith;
import kotlin.DeprecationLevel;
import kotlin.Deprecated;
import java.util.Iterator;
import java.util.concurrent.CancellationException;
import java.util.Collection;
import java.util.List;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a+\u0010\u0000\u001a\u00020\u00012\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u0017\u0010\u0004\u001a\u0013\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00010\u0005¢\u0006\u0002\b\u0006H\u0007¨\u0006\u0007" }, d2 = { "withTestContext", "", "testContext", "Lkotlinx/coroutines/test/TestCoroutineContext;", "testBody", "Lkotlin/Function1;", "Lkotlin/ExtensionFunctionType;", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class TestCoroutineContextKt
{
    @Deprecated(level = DeprecationLevel.WARNING, message = "This API has been deprecated to integrate with Structured Concurrency.", replaceWith = @ReplaceWith(expression = "testContext.runBlockingTest(testBody)", imports = { "kotlin.coroutines.test" }))
    public static final void withTestContext(final TestCoroutineContext testCoroutineContext, final Function1<? super TestCoroutineContext, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(testCoroutineContext, "testContext");
        Intrinsics.checkParameterIsNotNull(function1, "testBody");
        function1.invoke(testCoroutineContext);
        final List<Throwable> list = testCoroutineContext.getExceptions();
        final boolean b = list instanceof Collection;
        final boolean b2 = true;
        int n = 0;
        Label_0093: {
            if (b && list.isEmpty()) {
                n = (b2 ? 1 : 0);
            }
            else {
                final Iterator<Object> iterator = list.iterator();
                do {
                    n = (b2 ? 1 : 0);
                    if (iterator.hasNext()) {
                        continue;
                    }
                    break Label_0093;
                } while (((Throwable)iterator.next()) instanceof CancellationException);
                n = 0;
            }
        }
        if (n != 0) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Coroutine encountered unhandled exceptions:\n");
        sb.append(testCoroutineContext.getExceptions());
        throw new AssertionError((Object)sb.toString());
    }
}
