// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.test;

import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.DisposableHandle;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.Delay;
import kotlinx.coroutines.EventLoop;
import kotlinx.coroutines.DebugStringsKt;
import kotlin.TypeCastException;
import kotlin.coroutines.ContinuationInterceptor;
import kotlin.jvm.functions.Function2;
import java.util.Iterator;
import java.util.Collection;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function1;
import java.util.concurrent.TimeUnit;
import java.util.ArrayList;
import java.util.List;
import kotlinx.coroutines.internal.ThreadSafeHeap;
import kotlinx.coroutines.CoroutineExceptionHandler;
import kotlin.Metadata;
import kotlin.ReplaceWith;
import kotlin.DeprecationLevel;
import kotlin.Deprecated;
import kotlin.coroutines.CoroutineContext;

@Deprecated(level = DeprecationLevel.WARNING, message = "This API has been deprecated to integrate with Structured Concurrency.", replaceWith = @ReplaceWith(expression = "TestCoroutineScope", imports = { "kotlin.coroutines.test" }))
@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\b\u0007\u0018\u00002\u00020\u0001:\u0001<B\u0011\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004J\u0018\u0010\u0016\u001a\u00020\u00062\u0006\u0010\u0017\u001a\u00020\u00062\b\b\u0002\u0010\u0018\u001a\u00020\u0019J\u0018\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u00062\b\b\u0002\u0010\u0018\u001a\u00020\u0019J$\u0010\u001d\u001a\u00020\u001b2\b\b\u0002\u0010\u001e\u001a\u00020\u00032\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020!0 J$\u0010\"\u001a\u00020\u001b2\b\b\u0002\u0010\u001e\u001a\u00020\u00032\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020!0 J*\u0010#\u001a\u00020\u001b2\b\b\u0002\u0010\u001e\u001a\u00020\u00032\u0018\u0010\u001f\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\f\u0012\u0004\u0012\u00020!0 J$\u0010$\u001a\u00020\u001b2\b\b\u0002\u0010\u001e\u001a\u00020\u00032\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020!0 J\u0006\u0010%\u001a\u00020\u001bJ\u0014\u0010&\u001a\u00020\u001b2\n\u0010'\u001a\u00060(j\u0002`)H\u0002J5\u0010*\u001a\u0002H+\"\u0004\b\u0000\u0010+2\u0006\u0010,\u001a\u0002H+2\u0018\u0010-\u001a\u0014\u0012\u0004\u0012\u0002H+\u0012\u0004\u0012\u00020/\u0012\u0004\u0012\u0002H+0.H\u0016¢\u0006\u0002\u00100J(\u00101\u001a\u0004\u0018\u0001H2\"\b\b\u0000\u00102*\u00020/2\f\u00103\u001a\b\u0012\u0004\u0012\u0002H204H\u0096\u0002¢\u0006\u0002\u00105J\u0014\u00106\u001a\u00020\u00012\n\u00103\u001a\u0006\u0012\u0002\b\u000304H\u0016J\u0010\u00107\u001a\u00020\u00062\b\b\u0002\u0010\u0018\u001a\u00020\u0019J\u001c\u00108\u001a\u00020\u00122\n\u0010'\u001a\u00060(j\u0002`)2\u0006\u0010\u0017\u001a\u00020\u0006H\u0002J\b\u00109\u001a\u00020\u0006H\u0002J\b\u0010:\u001a\u00020\u0003H\u0016J\u0006\u0010;\u001a\u00020\u001bJ\u0010\u0010;\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0006H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u00060\bR\u00020\u0000X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f8F¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\r0\u0015X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006=" }, d2 = { "Lkotlinx/coroutines/test/TestCoroutineContext;", "Lkotlin/coroutines/CoroutineContext;", "name", "", "(Ljava/lang/String;)V", "counter", "", "ctxDispatcher", "Lkotlinx/coroutines/test/TestCoroutineContext$Dispatcher;", "ctxHandler", "Lkotlinx/coroutines/CoroutineExceptionHandler;", "exceptions", "", "", "getExceptions", "()Ljava/util/List;", "queue", "Lkotlinx/coroutines/internal/ThreadSafeHeap;", "Lkotlinx/coroutines/test/TimedRunnableObsolete;", "time", "uncaughtExceptions", "", "advanceTimeBy", "delayTime", "unit", "Ljava/util/concurrent/TimeUnit;", "advanceTimeTo", "", "targetTime", "assertAllUnhandledExceptions", "message", "predicate", "Lkotlin/Function1;", "", "assertAnyUnhandledException", "assertExceptions", "assertUnhandledException", "cancelAllActions", "enqueue", "block", "Ljava/lang/Runnable;", "Lkotlinx/coroutines/Runnable;", "fold", "R", "initial", "operation", "Lkotlin/Function2;", "Lkotlin/coroutines/CoroutineContext$Element;", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;", "get", "E", "key", "Lkotlin/coroutines/CoroutineContext$Key;", "(Lkotlin/coroutines/CoroutineContext$Key;)Lkotlin/coroutines/CoroutineContext$Element;", "minusKey", "now", "postDelayed", "processNextEvent", "toString", "triggerActions", "Dispatcher", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class TestCoroutineContext implements CoroutineContext
{
    private long counter;
    private final Dispatcher ctxDispatcher;
    private final CoroutineExceptionHandler ctxHandler;
    private final String name;
    private final ThreadSafeHeap<TimedRunnableObsolete> queue;
    private long time;
    private final List<Throwable> uncaughtExceptions;
    
    public TestCoroutineContext() {
        this(null, 1, null);
    }
    
    public TestCoroutineContext(final String name) {
        this.name = name;
        this.uncaughtExceptions = new ArrayList<Throwable>();
        this.ctxDispatcher = new Dispatcher();
        this.ctxHandler = (CoroutineExceptionHandler)new TestCoroutineContext$$special$$inlined$CoroutineExceptionHandler.TestCoroutineContext$$special$$inlined$CoroutineExceptionHandler$1((Key)CoroutineExceptionHandler.Key, this);
        this.queue = new ThreadSafeHeap<TimedRunnableObsolete>();
    }
    
    private final void enqueue(final Runnable runnable) {
        final ThreadSafeHeap<TimedRunnableObsolete> queue = this.queue;
        final long counter = this.counter;
        this.counter = 1L + counter;
        queue.addLast(new TimedRunnableObsolete(runnable, counter, 0L, 4, null));
    }
    
    private final TimedRunnableObsolete postDelayed(final Runnable runnable, final long duration) {
        final long counter = this.counter;
        this.counter = 1L + counter;
        final TimedRunnableObsolete timedRunnableObsolete = new TimedRunnableObsolete(runnable, counter, this.time + TimeUnit.MILLISECONDS.toNanos(duration));
        this.queue.addLast(timedRunnableObsolete);
        return timedRunnableObsolete;
    }
    
    private final long processNextEvent() {
        final TimedRunnableObsolete timedRunnableObsolete = this.queue.peek();
        if (timedRunnableObsolete != null) {
            this.triggerActions(timedRunnableObsolete.time);
        }
        if (this.queue.isEmpty()) {
            return Long.MAX_VALUE;
        }
        return 0L;
    }
    
    private final void triggerActions(final long n) {
    Label_0045_Outer:
        while (true) {
            final ThreadSafeHeap<TimedRunnableObsolete> queue = this.queue;
            while (true) {
                Label_0118: {
                    synchronized (queue) {
                        final TimedRunnableObsolete firstImpl = queue.firstImpl();
                        final TimedRunnableObsolete timedRunnableObsolete = null;
                        TimedRunnableObsolete removeAtImpl = null;
                        if (firstImpl != null) {
                            if (firstImpl.time > n) {
                                break Label_0118;
                            }
                            final int n2 = 1;
                            if (n2 != 0) {
                                removeAtImpl = queue.removeAtImpl(0);
                            }
                        }
                        // monitorexit(queue)
                        else {
                            // monitorexit(queue)
                            removeAtImpl = timedRunnableObsolete;
                        }
                        final TimedRunnableObsolete timedRunnableObsolete2 = removeAtImpl;
                        if (timedRunnableObsolete2 != null) {
                            if (timedRunnableObsolete2.time != 0L) {
                                this.time = timedRunnableObsolete2.time;
                            }
                            timedRunnableObsolete2.run();
                            continue Label_0045_Outer;
                        }
                        return;
                    }
                }
                final int n2 = 0;
                continue;
            }
        }
    }
    
    public final long advanceTimeBy(final long duration, final TimeUnit timeUnit) {
        Intrinsics.checkParameterIsNotNull(timeUnit, "unit");
        final long time = this.time;
        this.advanceTimeTo(timeUnit.toNanos(duration) + time, TimeUnit.NANOSECONDS);
        return timeUnit.convert(this.time - time, TimeUnit.NANOSECONDS);
    }
    
    public final void advanceTimeTo(long nanos, final TimeUnit timeUnit) {
        Intrinsics.checkParameterIsNotNull(timeUnit, "unit");
        nanos = timeUnit.toNanos(nanos);
        this.triggerActions(nanos);
        if (nanos > this.time) {
            this.time = nanos;
        }
    }
    
    public final void assertAllUnhandledExceptions(final String detailMessage, final Function1<? super Throwable, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(detailMessage, "message");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        final List<Throwable> list = this.uncaughtExceptions;
        final boolean b = list instanceof Collection;
        final boolean b2 = true;
        int n = 0;
        Label_0102: {
            if (b && list.isEmpty()) {
                n = (b2 ? 1 : 0);
            }
            else {
                final Iterator<Object> iterator = list.iterator();
                do {
                    n = (b2 ? 1 : 0);
                    if (iterator.hasNext()) {
                        continue;
                    }
                    break Label_0102;
                } while (function1.invoke((Object)iterator.next()));
                n = 0;
            }
        }
        if (n != 0) {
            this.uncaughtExceptions.clear();
            return;
        }
        throw new AssertionError((Object)detailMessage);
    }
    
    public final void assertAnyUnhandledException(final String detailMessage, final Function1<? super Throwable, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(detailMessage, "message");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        final List<Throwable> list = this.uncaughtExceptions;
        final boolean b = list instanceof Collection;
        final boolean b2 = false;
        int n = 0;
        Label_0102: {
            if (b && list.isEmpty()) {
                n = (b2 ? 1 : 0);
            }
            else {
                final Iterator<Object> iterator = list.iterator();
                do {
                    n = (b2 ? 1 : 0);
                    if (iterator.hasNext()) {
                        continue;
                    }
                    break Label_0102;
                } while (!function1.invoke((Object)iterator.next()));
                n = 1;
            }
        }
        if (n != 0) {
            this.uncaughtExceptions.clear();
            return;
        }
        throw new AssertionError((Object)detailMessage);
    }
    
    public final void assertExceptions(final String detailMessage, final Function1<? super List<? extends Throwable>, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(detailMessage, "message");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        if (function1.invoke(this.uncaughtExceptions)) {
            this.uncaughtExceptions.clear();
            return;
        }
        throw new AssertionError((Object)detailMessage);
    }
    
    public final void assertUnhandledException(final String detailMessage, final Function1<? super Throwable, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(detailMessage, "message");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        if (this.uncaughtExceptions.size() == 1 && function1.invoke(this.uncaughtExceptions.get(0))) {
            this.uncaughtExceptions.clear();
            return;
        }
        throw new AssertionError((Object)detailMessage);
    }
    
    public final void cancelAllActions() {
        if (!this.queue.isEmpty()) {
            this.queue.clear();
        }
    }
    
    @Override
    public <R> R fold(final R r, final Function2<? super R, ? super Element, ? extends R> function2) {
        Intrinsics.checkParameterIsNotNull(function2, "operation");
        return (R)function2.invoke((Object)function2.invoke(r, this.ctxDispatcher), this.ctxHandler);
    }
    
    @Override
    public <E extends Element> E get(final Key<E> key) {
        Intrinsics.checkParameterIsNotNull(key, "key");
        if (key == ContinuationInterceptor.Key) {
            final Dispatcher ctxDispatcher = this.ctxDispatcher;
            if (ctxDispatcher != null) {
                return (E)ctxDispatcher;
            }
            throw new TypeCastException("null cannot be cast to non-null type E");
        }
        else {
            if (key != CoroutineExceptionHandler.Key) {
                return null;
            }
            final CoroutineExceptionHandler ctxHandler = this.ctxHandler;
            if (ctxHandler != null) {
                return (E)ctxHandler;
            }
            throw new TypeCastException("null cannot be cast to non-null type E");
        }
    }
    
    public final List<Throwable> getExceptions() {
        return this.uncaughtExceptions;
    }
    
    @Override
    public CoroutineContext minusKey(final Key<?> key) {
        Intrinsics.checkParameterIsNotNull(key, "key");
        if (key == ContinuationInterceptor.Key) {
            return this.ctxHandler;
        }
        if (key == CoroutineExceptionHandler.Key) {
            return this.ctxDispatcher;
        }
        return this;
    }
    
    public final long now(final TimeUnit timeUnit) {
        Intrinsics.checkParameterIsNotNull(timeUnit, "unit");
        return timeUnit.convert(this.time, TimeUnit.NANOSECONDS);
    }
    
    @Override
    public CoroutineContext plus(final CoroutineContext coroutineContext) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        return CoroutineContext.DefaultImpls.plus(this, coroutineContext);
    }
    
    @Override
    public String toString() {
        final String name = this.name;
        if (name != null) {
            return name;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("TestCoroutineContext@");
        sb.append(DebugStringsKt.getHexAddress(this));
        return sb.toString();
    }
    
    public final void triggerActions() {
        this.triggerActions(this.time);
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0082\u0004\u0018\u00002\u00020\u00012\u00020\u0002B\u0005¢\u0006\u0002\u0010\u0003J\u001c\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\n\u0010\b\u001a\u00060\tj\u0002`\nH\u0016J\u001c\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\n\u0010\b\u001a\u00060\tj\u0002`\nH\u0016J\b\u0010\u000f\u001a\u00020\u000eH\u0016J\u001e\u0010\u0010\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\u000e2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00050\u0012H\u0016J\b\u0010\u0013\u001a\u00020\u0014H\u0016J\b\u0010\u0015\u001a\u00020\u0016H\u0016¨\u0006\u0017" }, d2 = { "Lkotlinx/coroutines/test/TestCoroutineContext$Dispatcher;", "Lkotlinx/coroutines/EventLoop;", "Lkotlinx/coroutines/Delay;", "(Lkotlinx/coroutines/test/TestCoroutineContext;)V", "dispatch", "", "context", "Lkotlin/coroutines/CoroutineContext;", "block", "Ljava/lang/Runnable;", "Lkotlinx/coroutines/Runnable;", "invokeOnTimeout", "Lkotlinx/coroutines/DisposableHandle;", "timeMillis", "", "processNextEvent", "scheduleResumeAfterDelay", "continuation", "Lkotlinx/coroutines/CancellableContinuation;", "shouldBeProcessedFromContext", "", "toString", "", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    private final class Dispatcher extends EventLoop implements Delay
    {
        public Dispatcher() {
            EventLoop.incrementUseCount$default(this, false, 1, null);
        }
        
        @Override
        public Object delay(final long n, final Continuation<? super Unit> continuation) {
            return Delay.DefaultImpls.delay(this, n, continuation);
        }
        
        @Override
        public void dispatch(final CoroutineContext coroutineContext, final Runnable runnable) {
            Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
            Intrinsics.checkParameterIsNotNull(runnable, "block");
            TestCoroutineContext.this.enqueue(runnable);
        }
        
        @Override
        public DisposableHandle invokeOnTimeout(final long n, final Runnable runnable) {
            Intrinsics.checkParameterIsNotNull(runnable, "block");
            return (DisposableHandle)new TestCoroutineContext$Dispatcher$invokeOnTimeout.TestCoroutineContext$Dispatcher$invokeOnTimeout$1(this, TestCoroutineContext.this.postDelayed(runnable, n));
        }
        
        @Override
        public long processNextEvent() {
            return TestCoroutineContext.this.processNextEvent();
        }
        
        @Override
        public void scheduleResumeAfterDelay(final long n, final CancellableContinuation<? super Unit> cancellableContinuation) {
            Intrinsics.checkParameterIsNotNull(cancellableContinuation, "continuation");
            TestCoroutineContext.this.postDelayed((Runnable)new TestCoroutineContext$Dispatcher$scheduleResumeAfterDelay$$inlined$Runnable.TestCoroutineContext$Dispatcher$scheduleResumeAfterDelay$$inlined$Runnable$1(this, (CancellableContinuation)cancellableContinuation), n);
        }
        
        @Override
        public boolean shouldBeProcessedFromContext() {
            return true;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Dispatcher(");
            sb.append(TestCoroutineContext.this);
            sb.append(')');
            return sb.toString();
        }
    }
}
