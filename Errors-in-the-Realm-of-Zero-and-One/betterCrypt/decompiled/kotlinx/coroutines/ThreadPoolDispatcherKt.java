// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u001a\u0018\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0007\u001a\u0010\u0010\u0006\u001a\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\u0007¨\u0006\u0007" }, d2 = { "newFixedThreadPoolContext", "Lkotlinx/coroutines/ExecutorCoroutineDispatcher;", "nThreads", "", "name", "", "newSingleThreadContext", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class ThreadPoolDispatcherKt
{
    public static final ExecutorCoroutineDispatcher newFixedThreadPoolContext(final int i, final String s) {
        Intrinsics.checkParameterIsNotNull(s, "name");
        boolean b = true;
        if (i < 1) {
            b = false;
        }
        if (b) {
            return new ThreadPoolDispatcher(i, s);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected at least one thread, but ");
        sb.append(i);
        sb.append(" specified");
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public static final ExecutorCoroutineDispatcher newSingleThreadContext(final String s) {
        Intrinsics.checkParameterIsNotNull(s, "name");
        return newFixedThreadPoolContext(1, s);
    }
}
