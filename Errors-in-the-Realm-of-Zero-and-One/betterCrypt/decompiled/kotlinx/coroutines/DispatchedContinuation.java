// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlinx.coroutines.internal.StackTraceRecoveryKt;
import kotlin.jvm.internal.InlineMarker;
import kotlin.Unit;
import kotlin.ResultKt;
import kotlin.Result;
import java.util.concurrent.CancellationException;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.internal.ThreadContextKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.CoroutineStackFrame;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000`\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\u0000\u0018\u0000*\u0006\b\u0000\u0010\u0001 \u00002\b\u0012\u0004\u0012\u0002H\u00010\u00022\u00060\u0003j\u0002`\u00042\b\u0012\u0004\u0012\u0002H\u00010\u0005B\u001b\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005¢\u0006\u0002\u0010\tJ\u0017\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00028\u0000H\u0000¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001e\u001a\n\u0018\u00010\u001fj\u0004\u0018\u0001` H\u0016J\u0016\u0010!\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00028\u0000H\u0086\b¢\u0006\u0002\u0010\u001dJ\u0011\u0010\"\u001a\u00020\u001a2\u0006\u0010#\u001a\u00020$H\u0086\bJ\t\u0010%\u001a\u00020&H\u0086\bJ\u0016\u0010'\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00028\u0000H\u0086\b¢\u0006\u0002\u0010\u001dJ\u0011\u0010(\u001a\u00020\u001a2\u0006\u0010#\u001a\u00020$H\u0086\bJ\u001e\u0010)\u001a\u00020\u001a2\f\u0010*\u001a\b\u0012\u0004\u0012\u00028\u00000+H\u0016\u00f8\u0001\u0000¢\u0006\u0002\u0010\u001dJ\u000f\u0010,\u001a\u0004\u0018\u00010\u000bH\u0010¢\u0006\u0002\b-J\b\u0010.\u001a\u00020/H\u0016R\u001a\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0000@\u0000X\u0081\u000e¢\u0006\b\n\u0000\u0012\u0004\b\f\u0010\rR\u001c\u0010\u000e\u001a\n\u0018\u00010\u0003j\u0004\u0018\u0001`\u0004X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0012\u0010\u0011\u001a\u00020\u0012X\u0096\u0005¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014R\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00028\u00000\u00058\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u00020\u000b8\u0000X\u0081\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0016\u001a\b\u0012\u0004\u0012\u00028\u00000\u00058PX\u0090\u0004¢\u0006\u0006\u001a\u0004\b\u0017\u0010\u0018R\u0010\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004¢\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u00060" }, d2 = { "Lkotlinx/coroutines/DispatchedContinuation;", "T", "Lkotlinx/coroutines/DispatchedTask;", "Lkotlin/coroutines/jvm/internal/CoroutineStackFrame;", "Lkotlinx/coroutines/internal/CoroutineStackFrame;", "Lkotlin/coroutines/Continuation;", "dispatcher", "Lkotlinx/coroutines/CoroutineDispatcher;", "continuation", "(Lkotlinx/coroutines/CoroutineDispatcher;Lkotlin/coroutines/Continuation;)V", "_state", "", "_state$annotations", "()V", "callerFrame", "getCallerFrame", "()Lkotlin/coroutines/jvm/internal/CoroutineStackFrame;", "context", "Lkotlin/coroutines/CoroutineContext;", "getContext", "()Lkotlin/coroutines/CoroutineContext;", "countOrElement", "delegate", "getDelegate$kotlinx_coroutines_core", "()Lkotlin/coroutines/Continuation;", "dispatchYield", "", "value", "dispatchYield$kotlinx_coroutines_core", "(Ljava/lang/Object;)V", "getStackTraceElement", "Ljava/lang/StackTraceElement;", "Lkotlinx/coroutines/internal/StackTraceElement;", "resumeCancellable", "resumeCancellableWithException", "exception", "", "resumeCancelled", "", "resumeUndispatched", "resumeUndispatchedWithException", "resumeWith", "result", "Lkotlin/Result;", "takeState", "takeState$kotlinx_coroutines_core", "toString", "", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class DispatchedContinuation<T> extends DispatchedTask<T> implements CoroutineStackFrame, Continuation<T>
{
    public Object _state;
    private final CoroutineStackFrame callerFrame;
    public final Continuation<T> continuation;
    public final Object countOrElement;
    public final CoroutineDispatcher dispatcher;
    
    public DispatchedContinuation(final CoroutineDispatcher dispatcher, final Continuation<? super T> continuation) {
        Intrinsics.checkParameterIsNotNull(dispatcher, "dispatcher");
        Intrinsics.checkParameterIsNotNull(continuation, "continuation");
        super(0);
        this.dispatcher = dispatcher;
        this.continuation = (Continuation<T>)continuation;
        this._state = DispatchedKt.access$getUNDEFINED$p();
        Object continuation2;
        if (!((continuation2 = this.continuation) instanceof CoroutineStackFrame)) {
            continuation2 = null;
        }
        this.callerFrame = (CoroutineStackFrame)continuation2;
        this.countOrElement = ThreadContextKt.threadContextElements(this.getContext());
    }
    
    public final void dispatchYield$kotlinx_coroutines_core(final T state) {
        final CoroutineContext context = this.continuation.getContext();
        this._state = state;
        this.resumeMode = 1;
        this.dispatcher.dispatchYield(context, this);
    }
    
    @Override
    public CoroutineStackFrame getCallerFrame() {
        return this.callerFrame;
    }
    
    @Override
    public CoroutineContext getContext() {
        return this.continuation.getContext();
    }
    
    @Override
    public Continuation<T> getDelegate$kotlinx_coroutines_core() {
        return this;
    }
    
    @Override
    public StackTraceElement getStackTraceElement() {
        return null;
    }
    
    public final void resumeCancellable(final T t) {
        if (this.dispatcher.isDispatchNeeded(this.getContext())) {
            this._state = t;
            this.resumeMode = 1;
            this.dispatcher.dispatch(this.getContext(), this);
            return;
        }
        final EventLoop eventLoop$kotlinx_coroutines_core = ThreadLocalEventLoop.INSTANCE.getEventLoop$kotlinx_coroutines_core();
        if (eventLoop$kotlinx_coroutines_core.isUnconfinedLoopActive()) {
            this._state = t;
            this.resumeMode = 1;
            eventLoop$kotlinx_coroutines_core.dispatchUnconfined(this);
            return;
        }
        while (true) {
            final DispatchedContinuation dispatchedContinuation = this;
            eventLoop$kotlinx_coroutines_core.incrementUseCount(true);
            while (true) {
                Label_0299: {
                    try {
                        final Job job = this.getContext().get((CoroutineContext.Key<Job>)Job.Key);
                        if (job != null && !job.isActive()) {
                            final CancellationException ex = job.getCancellationException();
                            final Result.Companion companion = Result.Companion;
                            this.resumeWith(Result.constructor-impl(ResultKt.createFailure(ex)));
                            final int n = 1;
                            if (n == 0) {
                                final CoroutineContext context = this.getContext();
                                final Object updateThreadContext = ThreadContextKt.updateThreadContext(context, this.countOrElement);
                                try {
                                    final Continuation<T> continuation = this.continuation;
                                    final Result.Companion companion2 = Result.Companion;
                                    continuation.resumeWith(Result.constructor-impl(t));
                                    final Unit instance = Unit.INSTANCE;
                                }
                                finally {
                                    InlineMarker.finallyStart(1);
                                    ThreadContextKt.restoreThreadContext(context, updateThreadContext);
                                    InlineMarker.finallyEnd(1);
                                }
                            }
                            while (eventLoop$kotlinx_coroutines_core.processUnconfinedEvent()) {}
                            InlineMarker.finallyStart(1);
                            return;
                        }
                        break Label_0299;
                    }
                    finally {
                        final DispatchedContinuation dispatchedContinuation2 = dispatchedContinuation;
                        final T t2 = t;
                        final Throwable t3 = null;
                        dispatchedContinuation2.handleFatalException$kotlinx_coroutines_core((Throwable)t2, t3);
                    }
                    try {
                        final DispatchedContinuation dispatchedContinuation2 = dispatchedContinuation;
                        final T t2 = t;
                        final Throwable t3 = null;
                        dispatchedContinuation2.handleFatalException$kotlinx_coroutines_core((Throwable)t2, t3);
                        return;
                    }
                    finally {
                        InlineMarker.finallyStart(1);
                        eventLoop$kotlinx_coroutines_core.decrementUseCount(true);
                        InlineMarker.finallyEnd(1);
                    }
                }
                final int n = 0;
                continue;
            }
        }
    }
    
    public final void resumeCancellableWithException(final Throwable t) {
        Intrinsics.checkParameterIsNotNull(t, "exception");
        final CoroutineContext context = this.continuation.getContext();
        final boolean b = false;
        final CompletedExceptionally state = new CompletedExceptionally(t, false, 2, null);
        if (this.dispatcher.isDispatchNeeded(context)) {
            this._state = new CompletedExceptionally(t, false, 2, null);
            this.resumeMode = 1;
            this.dispatcher.dispatch(context, this);
            return;
        }
        final EventLoop eventLoop$kotlinx_coroutines_core = ThreadLocalEventLoop.INSTANCE.getEventLoop$kotlinx_coroutines_core();
        if (eventLoop$kotlinx_coroutines_core.isUnconfinedLoopActive()) {
            this._state = state;
            this.resumeMode = 1;
            eventLoop$kotlinx_coroutines_core.dispatchUnconfined(this);
            return;
        }
        final DispatchedContinuation dispatchedContinuation = this;
        eventLoop$kotlinx_coroutines_core.incrementUseCount(true);
        try {
            final Job job = this.getContext().get((CoroutineContext.Key<Job>)Job.Key);
            int n = b ? 1 : 0;
            if (job != null) {
                n = (b ? 1 : 0);
                if (!job.isActive()) {
                    final CancellationException ex = job.getCancellationException();
                    final Result.Companion companion = Result.Companion;
                    this.resumeWith(Result.constructor-impl(ResultKt.createFailure(ex)));
                    n = 1;
                }
            }
            if (n == 0) {
                final CoroutineContext context2 = this.getContext();
                final Object updateThreadContext = ThreadContextKt.updateThreadContext(context2, this.countOrElement);
                try {
                    final Continuation<T> continuation = this.continuation;
                    final Result.Companion companion2 = Result.Companion;
                    continuation.resumeWith(Result.constructor-impl(ResultKt.createFailure(StackTraceRecoveryKt.recoverStackTrace(t, continuation))));
                    final Unit instance = Unit.INSTANCE;
                }
                finally {
                    InlineMarker.finallyStart(1);
                    ThreadContextKt.restoreThreadContext(context2, updateThreadContext);
                    InlineMarker.finallyEnd(1);
                }
            }
            while (eventLoop$kotlinx_coroutines_core.processUnconfinedEvent()) {}
            InlineMarker.finallyStart(1);
            return;
        }
        finally {
            final DispatchedContinuation dispatchedContinuation2 = dispatchedContinuation;
            final Throwable t2 = t;
            final Throwable t3 = null;
            dispatchedContinuation2.handleFatalException$kotlinx_coroutines_core(t2, t3);
        }
        try {
            final DispatchedContinuation dispatchedContinuation2 = dispatchedContinuation;
            final Throwable t2 = t;
            final Throwable t3 = null;
            dispatchedContinuation2.handleFatalException$kotlinx_coroutines_core(t2, t3);
        }
        finally {
            InlineMarker.finallyStart(1);
            eventLoop$kotlinx_coroutines_core.decrementUseCount(true);
            InlineMarker.finallyEnd(1);
        }
    }
    
    public final boolean resumeCancelled() {
        final Job job = this.getContext().get((CoroutineContext.Key<Job>)Job.Key);
        if (job != null && !job.isActive()) {
            final CancellationException ex = job.getCancellationException();
            final Result.Companion companion = Result.Companion;
            this.resumeWith(Result.constructor-impl(ResultKt.createFailure(ex)));
            return true;
        }
        return false;
    }
    
    public final void resumeUndispatched(final T t) {
        final CoroutineContext context = this.getContext();
        final Object updateThreadContext = ThreadContextKt.updateThreadContext(context, this.countOrElement);
        try {
            final Continuation<T> continuation = this.continuation;
            final Result.Companion companion = Result.Companion;
            continuation.resumeWith(Result.constructor-impl(t));
            final Unit instance = Unit.INSTANCE;
        }
        finally {
            InlineMarker.finallyStart(1);
            ThreadContextKt.restoreThreadContext(context, updateThreadContext);
            InlineMarker.finallyEnd(1);
        }
    }
    
    public final void resumeUndispatchedWithException(final Throwable t) {
        Intrinsics.checkParameterIsNotNull(t, "exception");
        final CoroutineContext context = this.getContext();
        final Object updateThreadContext = ThreadContextKt.updateThreadContext(context, this.countOrElement);
        try {
            final Continuation<T> continuation = this.continuation;
            final Result.Companion companion = Result.Companion;
            continuation.resumeWith(Result.constructor-impl(ResultKt.createFailure(StackTraceRecoveryKt.recoverStackTrace(t, continuation))));
            final Unit instance = Unit.INSTANCE;
        }
        finally {
            InlineMarker.finallyStart(1);
            ThreadContextKt.restoreThreadContext(context, updateThreadContext);
            InlineMarker.finallyEnd(1);
        }
    }
    
    @Override
    public void resumeWith(final Object o) {
        final CoroutineContext context = this.continuation.getContext();
        final Object state = CompletedExceptionallyKt.toState(o);
        if (this.dispatcher.isDispatchNeeded(context)) {
            this._state = state;
            this.resumeMode = 0;
            this.dispatcher.dispatch(context, this);
            return;
        }
        final EventLoop eventLoop$kotlinx_coroutines_core = ThreadLocalEventLoop.INSTANCE.getEventLoop$kotlinx_coroutines_core();
        if (eventLoop$kotlinx_coroutines_core.isUnconfinedLoopActive()) {
            this._state = state;
            this.resumeMode = 0;
            eventLoop$kotlinx_coroutines_core.dispatchUnconfined(this);
            return;
        }
        final DispatchedContinuation dispatchedContinuation = this;
        eventLoop$kotlinx_coroutines_core.incrementUseCount(true);
        final Throwable t2;
        try {
            final CoroutineContext context2 = this.getContext();
            final Object updateThreadContext = ThreadContextKt.updateThreadContext(context2, this.countOrElement);
            try {
                this.continuation.resumeWith(o);
                final Unit instance = Unit.INSTANCE;
                ThreadContextKt.restoreThreadContext(context2, updateThreadContext);
                while (eventLoop$kotlinx_coroutines_core.processUnconfinedEvent()) {}
            }
            finally {
                ThreadContextKt.restoreThreadContext(context2, updateThreadContext);
            }
        }
        finally {
            final DispatchedContinuation dispatchedContinuation2 = dispatchedContinuation;
            final Throwable t = t2;
            final Throwable t3 = null;
            dispatchedContinuation2.handleFatalException$kotlinx_coroutines_core(t, t3);
        }
        try {
            final DispatchedContinuation dispatchedContinuation2 = dispatchedContinuation;
            final Throwable t = t2;
            final Throwable t3 = null;
            dispatchedContinuation2.handleFatalException$kotlinx_coroutines_core(t, t3);
        }
        finally {
            eventLoop$kotlinx_coroutines_core.decrementUseCount(true);
        }
    }
    
    @Override
    public Object takeState$kotlinx_coroutines_core() {
        final Object state = this._state;
        if (DebugKt.getASSERTIONS_ENABLED() && state == DispatchedKt.access$getUNDEFINED$p()) {
            throw new AssertionError();
        }
        this._state = DispatchedKt.access$getUNDEFINED$p();
        return state;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DispatchedContinuation[");
        sb.append(this.dispatcher);
        sb.append(", ");
        sb.append(DebugStringsKt.toDebugString(this.continuation));
        sb.append(']');
        return sb.toString();
    }
}
