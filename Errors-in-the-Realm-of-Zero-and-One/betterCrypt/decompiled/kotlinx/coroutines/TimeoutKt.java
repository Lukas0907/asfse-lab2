// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.ResultKt;
import kotlin.jvm.internal.Ref;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlinx.coroutines.intrinsics.UndispatchedKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u00006\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u001a\u0018\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0000\u001a_\u0010\u0006\u001a\u0004\u0018\u00010\u0007\"\u0004\b\u0000\u0010\b\"\b\b\u0001\u0010\t*\u0002H\b2\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u0002H\b\u0012\u0004\u0012\u0002H\t0\n2'\u0010\u000b\u001a#\b\u0001\u0012\u0004\u0012\u00020\r\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\t0\u000e\u0012\u0006\u0012\u0004\u0018\u00010\u00070\f¢\u0006\u0002\b\u000fH\u0002\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0010\u001aH\u0010\u0011\u001a\u0002H\t\"\u0004\b\u0000\u0010\t2\u0006\u0010\u0012\u001a\u00020\u00032'\u0010\u000b\u001a#\b\u0001\u0012\u0004\u0012\u00020\r\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\t0\u000e\u0012\u0006\u0012\u0004\u0018\u00010\u00070\f¢\u0006\u0002\b\u000fH\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0013\u001aJ\u0010\u0014\u001a\u0004\u0018\u0001H\t\"\u0004\b\u0000\u0010\t2\u0006\u0010\u0012\u001a\u00020\u00032'\u0010\u000b\u001a#\b\u0001\u0012\u0004\u0012\u00020\r\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\t0\u000e\u0012\u0006\u0012\u0004\u0018\u00010\u00070\f¢\u0006\u0002\b\u000fH\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0013\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0015" }, d2 = { "TimeoutCancellationException", "Lkotlinx/coroutines/TimeoutCancellationException;", "time", "", "coroutine", "Lkotlinx/coroutines/Job;", "setupTimeout", "", "U", "T", "Lkotlinx/coroutines/TimeoutCoroutine;", "block", "Lkotlin/Function2;", "Lkotlinx/coroutines/CoroutineScope;", "Lkotlin/coroutines/Continuation;", "Lkotlin/ExtensionFunctionType;", "(Lkotlinx/coroutines/TimeoutCoroutine;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;", "withTimeout", "timeMillis", "(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "withTimeoutOrNull", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class TimeoutKt
{
    public static final TimeoutCancellationException TimeoutCancellationException(final long lng, final Job job) {
        Intrinsics.checkParameterIsNotNull(job, "coroutine");
        final StringBuilder sb = new StringBuilder();
        sb.append("Timed out waiting for ");
        sb.append(lng);
        sb.append(" ms");
        return new TimeoutCancellationException(sb.toString(), job);
    }
    
    private static final <U, T extends U> Object setupTimeout(final TimeoutCoroutine<U, ? super T> timeoutCoroutine, final Function2<? super CoroutineScope, ? super Continuation<? super T>, ?> function2) {
        JobKt.disposeOnCompletion(timeoutCoroutine, DelayKt.getDelay(timeoutCoroutine.uCont.getContext()).invokeOnTimeout(timeoutCoroutine.time, timeoutCoroutine));
        return UndispatchedKt.startUndispatchedOrReturnIgnoreTimeout((AbstractCoroutine<? super Object>)timeoutCoroutine, (AbstractCoroutine<? super Object>)timeoutCoroutine, (Function2<? super AbstractCoroutine<? super Object>, ? super Continuation<? super Object>, ?>)function2);
    }
    
    public static final <T> Object withTimeout(final long n, final Function2<? super CoroutineScope, ? super Continuation<? super T>, ?> function2, final Continuation<? super T> continuation) {
        if (n > 0L) {
            final Object access$setupTimeout = setupTimeout((TimeoutCoroutine<Object, ? super Object>)new TimeoutCoroutine(n, (Continuation<? super U>)continuation), (Function2<? super CoroutineScope, ? super Continuation<? super Object>, ?>)function2);
            if (access$setupTimeout == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                DebugProbesKt.probeCoroutineSuspended(continuation);
            }
            return access$setupTimeout;
        }
        throw new TimeoutCancellationException("Timed out immediately");
    }
    
    public static final <T> Object withTimeoutOrNull(long j$0, Function2<? super CoroutineScope, ? super Continuation<? super T>, ?> l$0, final Continuation<? super T> continuation) {
        TimeoutKt$withTimeoutOrNull.TimeoutKt$withTimeoutOrNull$1 timeoutKt$withTimeoutOrNull$2 = null;
        Label_0052: {
            if (continuation instanceof TimeoutKt$withTimeoutOrNull.TimeoutKt$withTimeoutOrNull$1) {
                final TimeoutKt$withTimeoutOrNull.TimeoutKt$withTimeoutOrNull$1 timeoutKt$withTimeoutOrNull$1 = (TimeoutKt$withTimeoutOrNull.TimeoutKt$withTimeoutOrNull$1)continuation;
                if ((timeoutKt$withTimeoutOrNull$1.label & Integer.MIN_VALUE) != 0x0) {
                    timeoutKt$withTimeoutOrNull$1.label += Integer.MIN_VALUE;
                    timeoutKt$withTimeoutOrNull$2 = timeoutKt$withTimeoutOrNull$1;
                    break Label_0052;
                }
            }
            timeoutKt$withTimeoutOrNull$2 = new TimeoutKt$withTimeoutOrNull.TimeoutKt$withTimeoutOrNull$1((Continuation)continuation);
        }
        final Object result = timeoutKt$withTimeoutOrNull$2.result;
        final Object coroutine_SUSPENDED = IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
        final int label = timeoutKt$withTimeoutOrNull$2.label;
        Label_0233: {
            if (label != 0) {
                if (label == 1) {
                    l$0 = timeoutKt$withTimeoutOrNull$2.L$1;
                    final Function2 function2 = (Function2)timeoutKt$withTimeoutOrNull$2.L$0;
                    j$0 = timeoutKt$withTimeoutOrNull$2.J$0;
                    try {
                        ResultKt.throwOnFailure(result);
                        return result;
                    }
                    catch (TimeoutCancellationException ex) {
                        break Label_0233;
                    }
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ResultKt.throwOnFailure(result);
            if (j$0 <= 0L) {
                return null;
            }
            final Ref.ObjectRef l$2 = new Ref.ObjectRef();
            l$2.element = null;
            try {
                timeoutKt$withTimeoutOrNull$2.J$0 = j$0;
                timeoutKt$withTimeoutOrNull$2.L$0 = l$0;
                timeoutKt$withTimeoutOrNull$2.L$1 = l$2;
                timeoutKt$withTimeoutOrNull$2.label = 1;
                final TimeoutCoroutine element = new TimeoutCoroutine(j$0, (Continuation<? super Object>)timeoutKt$withTimeoutOrNull$2);
                l$2.element = (T)element;
                final Object access$setupTimeout = setupTimeout((TimeoutCoroutine<Object, ? super Object>)element, (Function2<? super CoroutineScope, ? super Continuation<? super Object>, ?>)l$0);
                if (access$setupTimeout == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
                    DebugProbesKt.probeCoroutineSuspended((Continuation<?>)timeoutKt$withTimeoutOrNull$2);
                }
                if (access$setupTimeout == coroutine_SUSPENDED) {
                    return coroutine_SUSPENDED;
                }
                return access$setupTimeout;
            }
            catch (TimeoutCancellationException ex) {
                l$0 = l$2;
            }
        }
        final TimeoutCancellationException ex;
        if (ex.coroutine == (TimeoutCoroutine<?, ?>)((Ref.ObjectRef)l$0).element) {
            return null;
        }
        throw ex;
    }
}
