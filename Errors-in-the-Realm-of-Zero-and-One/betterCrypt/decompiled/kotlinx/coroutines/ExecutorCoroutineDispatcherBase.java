// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import java.util.concurrent.Future;
import kotlinx.coroutines.internal.ConcurrentKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b \u0018\u00002\u00020\u00012\u00020\u0002B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0006\u001a\u00020\u0007H\u0016J\u001c\u0010\b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\n2\n\u0010\u000b\u001a\u00060\fj\u0002`\rH\u0016J\u0013\u0010\u000e\u001a\u00020\u00052\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0096\u0002J\b\u0010\u0011\u001a\u00020\u0012H\u0016J\r\u0010\u0013\u001a\u00020\u0007H\u0000¢\u0006\u0002\b\u0014J\u001c\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\n\u0010\u000b\u001a\u00060\fj\u0002`\rH\u0016J*\u0010\u0019\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u001a2\n\u0010\u000b\u001a\u00060\fj\u0002`\r2\u0006\u0010\u001b\u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u001e\u0010\u001e\u001a\u00020\u00072\u0006\u0010\u0017\u001a\u00020\u00182\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00070 H\u0016J\b\u0010!\u001a\u00020\"H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006#" }, d2 = { "Lkotlinx/coroutines/ExecutorCoroutineDispatcherBase;", "Lkotlinx/coroutines/ExecutorCoroutineDispatcher;", "Lkotlinx/coroutines/Delay;", "()V", "removesFutureOnCancellation", "", "close", "", "dispatch", "context", "Lkotlin/coroutines/CoroutineContext;", "block", "Ljava/lang/Runnable;", "Lkotlinx/coroutines/Runnable;", "equals", "other", "", "hashCode", "", "initFutureCancellation", "initFutureCancellation$kotlinx_coroutines_core", "invokeOnTimeout", "Lkotlinx/coroutines/DisposableHandle;", "timeMillis", "", "scheduleBlock", "Ljava/util/concurrent/ScheduledFuture;", "time", "unit", "Ljava/util/concurrent/TimeUnit;", "scheduleResumeAfterDelay", "continuation", "Lkotlinx/coroutines/CancellableContinuation;", "toString", "", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public abstract class ExecutorCoroutineDispatcherBase extends ExecutorCoroutineDispatcher implements Delay
{
    private boolean removesFutureOnCancellation;
    
    private final ScheduledFuture<?> scheduleBlock(final Runnable runnable, final long n, final TimeUnit timeUnit) {
        final ScheduledFuture<?> scheduledFuture = null;
        try {
            Executor executor;
            if (!((executor = this.getExecutor()) instanceof ScheduledExecutorService)) {
                executor = null;
            }
            final ScheduledExecutorService scheduledExecutorService = (ScheduledExecutorService)executor;
            ScheduledFuture<?> schedule = scheduledFuture;
            if (scheduledExecutorService != null) {
                schedule = scheduledExecutorService.schedule(runnable, n, timeUnit);
            }
            return schedule;
        }
        catch (RejectedExecutionException ex) {
            return null;
        }
    }
    
    @Override
    public void close() {
        Executor executor;
        if (!((executor = this.getExecutor()) instanceof ExecutorService)) {
            executor = null;
        }
        final ExecutorService executorService = (ExecutorService)executor;
        if (executorService != null) {
            executorService.shutdown();
        }
    }
    
    @Override
    public Object delay(final long n, final Continuation<? super Unit> continuation) {
        return Delay.DefaultImpls.delay(this, n, continuation);
    }
    
    @Override
    public void dispatch(final CoroutineContext coroutineContext, final Runnable runnable) {
        while (true) {
            Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
            Intrinsics.checkParameterIsNotNull(runnable, "block");
        Label_0048_Outer:
            while (true) {
                Label_0074: {
                    while (true) {
                        try {
                            final Executor executor = this.getExecutor();
                            final TimeSource timeSource = TimeSourceKt.getTimeSource();
                            if (timeSource == null) {
                                break Label_0074;
                            }
                            final Runnable wrapTask = timeSource.wrapTask(runnable);
                            if (wrapTask != null) {
                                executor.execute(wrapTask);
                                return;
                            }
                            break Label_0074;
                            while (true) {
                                DefaultExecutor.INSTANCE.enqueue(runnable);
                                return;
                                final TimeSource timeSource2 = TimeSourceKt.getTimeSource();
                                timeSource2.unTrackTask();
                                continue Label_0048_Outer;
                            }
                        }
                        // iftrue(Label_0062:, timeSource2 == null)
                        catch (RejectedExecutionException ex) {
                            continue;
                        }
                        break;
                    }
                }
                final Runnable wrapTask = runnable;
                continue;
            }
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof ExecutorCoroutineDispatcherBase && ((ExecutorCoroutineDispatcherBase)o).getExecutor() == this.getExecutor();
    }
    
    @Override
    public int hashCode() {
        return System.identityHashCode(this.getExecutor());
    }
    
    public final void initFutureCancellation$kotlinx_coroutines_core() {
        this.removesFutureOnCancellation = ConcurrentKt.removeFutureOnCancel(this.getExecutor());
    }
    
    @Override
    public DisposableHandle invokeOnTimeout(final long n, final Runnable runnable) {
        Intrinsics.checkParameterIsNotNull(runnable, "block");
        ScheduledFuture<?> scheduleBlock;
        if (this.removesFutureOnCancellation) {
            scheduleBlock = this.scheduleBlock(runnable, n, TimeUnit.MILLISECONDS);
        }
        else {
            scheduleBlock = null;
        }
        if (scheduleBlock != null) {
            return new DisposableFutureHandle(scheduleBlock);
        }
        return DefaultExecutor.INSTANCE.invokeOnTimeout(n, runnable);
    }
    
    @Override
    public void scheduleResumeAfterDelay(final long n, final CancellableContinuation<? super Unit> cancellableContinuation) {
        Intrinsics.checkParameterIsNotNull(cancellableContinuation, "continuation");
        ScheduledFuture<?> scheduleBlock;
        if (this.removesFutureOnCancellation) {
            scheduleBlock = this.scheduleBlock(new ResumeUndispatchedRunnable(this, cancellableContinuation), n, TimeUnit.MILLISECONDS);
        }
        else {
            scheduleBlock = null;
        }
        if (scheduleBlock != null) {
            JobKt.cancelFutureOnCancellation(cancellableContinuation, scheduleBlock);
            return;
        }
        DefaultExecutor.INSTANCE.scheduleResumeAfterDelay(n, cancellableContinuation);
    }
    
    @Override
    public String toString() {
        return this.getExecutor().toString();
    }
}
