// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.flow;

import kotlinx.coroutines.channels.SendChannel;
import kotlinx.coroutines.flow.internal.SendingCollector;
import kotlinx.coroutines.channels.ProducerScope;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.channels.BroadcastChannel;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.CoroutineScope;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.channels.ReceiveChannel;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import kotlin.Metadata;
import kotlinx.coroutines.flow.internal.ChannelFlow;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000P\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u00028\u00000\u0002B)\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0003\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0005\u0012\b\b\u0002\u0010\b\u001a\u00020\u0007¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\f\u001a\u00020\u000bH\u0016¢\u0006\u0004\b\f\u0010\rJ%\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00000\u00122\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0010H\u0016¢\u0006\u0004\b\u0013\u0010\u0014J!\u0010\u0018\u001a\u00020\u00172\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00028\u00000\u0015H\u0096@\u00f8\u0001\u0000¢\u0006\u0004\b\u0018\u0010\u0019J!\u0010\u001b\u001a\u00020\u00172\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00000\u001aH\u0094@\u00f8\u0001\u0000¢\u0006\u0004\b\u001b\u0010\u001cJ%\u0010\u001d\u001a\b\u0012\u0004\u0012\u00028\u00000\u00022\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u0007H\u0014¢\u0006\u0004\b\u001d\u0010\u001eJ\u000f\u0010\u001f\u001a\u00020\u0017H\u0002¢\u0006\u0004\b\u001f\u0010 J\u001d\u0010!\u001a\b\u0012\u0004\u0012\u00028\u00000\u00032\u0006\u0010\u000f\u001a\u00020\u000eH\u0016¢\u0006\u0004\b!\u0010\"R\u001c\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u00038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0004\u0010#\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006$" }, d2 = { "Lkotlinx/coroutines/flow/ConsumeAsFlow;", "T", "Lkotlinx/coroutines/flow/internal/ChannelFlow;", "Lkotlinx/coroutines/channels/ReceiveChannel;", "channel", "Lkotlin/coroutines/CoroutineContext;", "context", "", "capacity", "<init>", "(Lkotlinx/coroutines/channels/ReceiveChannel;Lkotlin/coroutines/CoroutineContext;I)V", "", "additionalToStringProps", "()Ljava/lang/String;", "Lkotlinx/coroutines/CoroutineScope;", "scope", "Lkotlinx/coroutines/CoroutineStart;", "start", "Lkotlinx/coroutines/channels/BroadcastChannel;", "broadcastImpl", "(Lkotlinx/coroutines/CoroutineScope;Lkotlinx/coroutines/CoroutineStart;)Lkotlinx/coroutines/channels/BroadcastChannel;", "Lkotlinx/coroutines/flow/FlowCollector;", "collector", "", "collect", "(Lkotlinx/coroutines/flow/FlowCollector;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Lkotlinx/coroutines/channels/ProducerScope;", "collectTo", "(Lkotlinx/coroutines/channels/ProducerScope;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "create", "(Lkotlin/coroutines/CoroutineContext;I)Lkotlinx/coroutines/flow/internal/ChannelFlow;", "markConsumed", "()V", "produceImpl", "(Lkotlinx/coroutines/CoroutineScope;)Lkotlinx/coroutines/channels/ReceiveChannel;", "Lkotlinx/coroutines/channels/ReceiveChannel;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
final class ConsumeAsFlow<T> extends ChannelFlow<T>
{
    private static final AtomicIntegerFieldUpdater consumed$FU;
    private final ReceiveChannel<T> channel;
    private volatile int consumed;
    
    static {
        consumed$FU = AtomicIntegerFieldUpdater.newUpdater(ConsumeAsFlow.class, "consumed");
    }
    
    public ConsumeAsFlow(final ReceiveChannel<? extends T> channel, final CoroutineContext coroutineContext, final int n) {
        Intrinsics.checkParameterIsNotNull(channel, "channel");
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        super(coroutineContext, n);
        this.channel = (ReceiveChannel<T>)channel;
        this.consumed = 0;
    }
    
    private final void markConsumed() {
        final AtomicIntegerFieldUpdater consumed$FU = ConsumeAsFlow.consumed$FU;
        boolean b = true;
        if (consumed$FU.getAndSet(this, 1) != 0) {
            b = false;
        }
        if (b) {
            return;
        }
        throw new IllegalStateException("ReceiveChannel.consumeAsFlow can be collected just once".toString());
    }
    
    @Override
    public String additionalToStringProps() {
        final StringBuilder sb = new StringBuilder();
        sb.append("channel=");
        sb.append(this.channel);
        sb.append(", ");
        return sb.toString();
    }
    
    @Override
    public BroadcastChannel<T> broadcastImpl(final CoroutineScope coroutineScope, final CoroutineStart coroutineStart) {
        Intrinsics.checkParameterIsNotNull(coroutineScope, "scope");
        Intrinsics.checkParameterIsNotNull(coroutineStart, "start");
        this.markConsumed();
        return super.broadcastImpl(coroutineScope, coroutineStart);
    }
    
    @Override
    public Object collect(final FlowCollector<? super T> flowCollector, final Continuation<? super Unit> continuation) {
        if (this.capacity == -3) {
            this.markConsumed();
            return FlowKt.emitAll((FlowCollector<? super Object>)flowCollector, (ReceiveChannel<?>)this.channel, continuation);
        }
        return super.collect(flowCollector, continuation);
    }
    
    @Override
    protected Object collectTo(final ProducerScope<? super T> producerScope, final Continuation<? super Unit> continuation) {
        return FlowKt.emitAll((FlowCollector<? super Object>)new SendingCollector<Object>((SendChannel<? super Object>)producerScope), (ReceiveChannel<?>)this.channel, continuation);
    }
    
    @Override
    protected ChannelFlow<T> create(final CoroutineContext coroutineContext, final int n) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        return new ConsumeAsFlow(this.channel, coroutineContext, n);
    }
    
    @Override
    public ReceiveChannel<T> produceImpl(final CoroutineScope coroutineScope) {
        Intrinsics.checkParameterIsNotNull(coroutineScope, "scope");
        this.markConsumed();
        if (this.capacity == -3) {
            return this.channel;
        }
        return super.produceImpl(coroutineScope);
    }
}
