// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.flow.internal;

import kotlinx.coroutines.DebugKt;
import kotlinx.coroutines.DebugStringsKt;
import kotlinx.coroutines.channels.ProduceKt;
import kotlinx.coroutines.channels.ReceiveChannel;
import kotlinx.coroutines.channels.ProducerScope;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.channels.BroadcastKt;
import kotlinx.coroutines.channels.BroadcastChannel;
import kotlinx.coroutines.CoroutineStart;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.CoroutineScope;
import kotlin.jvm.functions.Function2;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.flow.FlowCollector;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;
import kotlinx.coroutines.flow.Flow;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\\\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\b'\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\b\u0010\u0013\u001a\u00020\u0014H\u0016J\u001e\u0010\u0015\u001a\b\u0012\u0004\u0012\u00028\u00000\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u001f\u0010\u001b\u001a\u00020\f2\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00028\u00000\u001dH\u0096@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u001eJ\u001f\u0010\u001f\u001a\u00020\f2\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00028\u00000\nH¤@\u00f8\u0001\u0000¢\u0006\u0002\u0010 J\u001e\u0010!\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H$J\u0016\u0010\"\u001a\b\u0012\u0004\u0012\u00028\u00000#2\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\b\u0010$\u001a\u00020\u0014H\u0016J \u0010%\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u0006R\u0010\u0010\u0005\u001a\u00020\u00068\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R9\u0010\b\u001a$\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\n\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b\u0012\u0006\u0012\u0004\u0018\u00010\r0\t8@X\u0080\u0004\u00f8\u0001\u0000¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000fR\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u00020\u00068BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0012\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006&" }, d2 = { "Lkotlinx/coroutines/flow/internal/ChannelFlow;", "T", "Lkotlinx/coroutines/flow/Flow;", "context", "Lkotlin/coroutines/CoroutineContext;", "capacity", "", "(Lkotlin/coroutines/CoroutineContext;I)V", "collectToFun", "Lkotlin/Function2;", "Lkotlinx/coroutines/channels/ProducerScope;", "Lkotlin/coroutines/Continuation;", "", "", "getCollectToFun$kotlinx_coroutines_core", "()Lkotlin/jvm/functions/Function2;", "produceCapacity", "getProduceCapacity", "()I", "additionalToStringProps", "", "broadcastImpl", "Lkotlinx/coroutines/channels/BroadcastChannel;", "scope", "Lkotlinx/coroutines/CoroutineScope;", "start", "Lkotlinx/coroutines/CoroutineStart;", "collect", "collector", "Lkotlinx/coroutines/flow/FlowCollector;", "(Lkotlinx/coroutines/flow/FlowCollector;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "collectTo", "(Lkotlinx/coroutines/channels/ProducerScope;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "create", "produceImpl", "Lkotlinx/coroutines/channels/ReceiveChannel;", "toString", "update", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public abstract class ChannelFlow<T> implements Flow<T>
{
    public final int capacity;
    public final CoroutineContext context;
    
    public ChannelFlow(final CoroutineContext context, final int capacity) {
        Intrinsics.checkParameterIsNotNull(context, "context");
        this.context = context;
        this.capacity = capacity;
    }
    
    static /* synthetic */ Object collect$suspendImpl(final ChannelFlow channelFlow, final FlowCollector flowCollector, final Continuation continuation) {
        return CoroutineScopeKt.coroutineScope((Function2<? super CoroutineScope, ? super Continuation<? super Object>, ?>)new ChannelFlow$collect.ChannelFlow$collect$2(channelFlow, flowCollector, (Continuation)null), (Continuation<? super Object>)continuation);
    }
    
    private final int getProduceCapacity() {
        int capacity;
        if ((capacity = this.capacity) == -3) {
            capacity = -2;
        }
        return capacity;
    }
    
    public String additionalToStringProps() {
        return "";
    }
    
    public BroadcastChannel<T> broadcastImpl(final CoroutineScope coroutineScope, final CoroutineStart coroutineStart) {
        Intrinsics.checkParameterIsNotNull(coroutineScope, "scope");
        Intrinsics.checkParameterIsNotNull(coroutineStart, "start");
        return (BroadcastChannel<T>)BroadcastKt.broadcast$default(coroutineScope, this.context, this.getProduceCapacity(), coroutineStart, null, this.getCollectToFun$kotlinx_coroutines_core(), 8, null);
    }
    
    @Override
    public Object collect(final FlowCollector<? super T> flowCollector, final Continuation<? super Unit> continuation) {
        return collect$suspendImpl(this, flowCollector, continuation);
    }
    
    protected abstract Object collectTo(final ProducerScope<? super T> p0, final Continuation<? super Unit> p1);
    
    protected abstract ChannelFlow<T> create(final CoroutineContext p0, final int p1);
    
    public final Function2<ProducerScope<? super T>, Continuation<? super Unit>, Object> getCollectToFun$kotlinx_coroutines_core() {
        return (Function2<ProducerScope<? super T>, Continuation<? super Unit>, Object>)new ChannelFlow$collectToFun.ChannelFlow$collectToFun$1(this, (Continuation)null);
    }
    
    public ReceiveChannel<T> produceImpl(final CoroutineScope coroutineScope) {
        Intrinsics.checkParameterIsNotNull(coroutineScope, "scope");
        return ProduceKt.produce(coroutineScope, this.context, this.getProduceCapacity(), (Function2<? super ProducerScope<? super T>, ? super Continuation<? super Unit>, ?>)this.getCollectToFun$kotlinx_coroutines_core());
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(DebugStringsKt.getClassSimpleName(this));
        sb.append('[');
        sb.append(this.additionalToStringProps());
        sb.append("context=");
        sb.append(this.context);
        sb.append(", capacity=");
        sb.append(this.capacity);
        sb.append(']');
        return sb.toString();
    }
    
    public final ChannelFlow<T> update(CoroutineContext plus, int n) {
        Intrinsics.checkParameterIsNotNull(plus, "context");
        plus = plus.plus(this.context);
        final int capacity = this.capacity;
        Label_0176: {
            if (capacity != -3) {
                if (n != -3) {
                    if (capacity == -2) {
                        break Label_0176;
                    }
                    if (n != -2) {
                        if (capacity == -1 || n == -1) {
                            n = -1;
                            break Label_0176;
                        }
                        final boolean assertions_ENABLED = DebugKt.getASSERTIONS_ENABLED();
                        final int n2 = 1;
                        if (assertions_ENABLED && this.capacity < 0) {
                            throw new AssertionError();
                        }
                        if (DebugKt.getASSERTIONS_ENABLED()) {
                            int n3;
                            if (n >= 0) {
                                n3 = n2;
                            }
                            else {
                                n3 = 0;
                            }
                            if (n3 == 0) {
                                throw new AssertionError();
                            }
                        }
                        n += this.capacity;
                        if (n >= 0) {
                            break Label_0176;
                        }
                        n = Integer.MAX_VALUE;
                        break Label_0176;
                    }
                }
                n = capacity;
            }
        }
        if (Intrinsics.areEqual(plus, this.context) && n == this.capacity) {
            return this;
        }
        return this.create(plus, n);
    }
}
