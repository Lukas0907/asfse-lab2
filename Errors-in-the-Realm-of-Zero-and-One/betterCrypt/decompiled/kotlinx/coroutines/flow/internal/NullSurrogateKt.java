// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.flow.internal;

import kotlinx.coroutines.internal.Symbol;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\"\u0016\u0010\u0000\u001a\u00020\u00018\u0000X\u0081\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u0002\u0010\u0003\"\u0016\u0010\u0004\u001a\u00020\u00018\u0000X\u0081\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u0005\u0010\u0003¨\u0006\u0006" }, d2 = { "DONE", "Lkotlinx/coroutines/internal/Symbol;", "DONE$annotations", "()V", "NULL", "NULL$annotations", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class NullSurrogateKt
{
    public static final Symbol DONE;
    public static final Symbol NULL;
    
    static {
        NULL = new Symbol("NULL");
        DONE = new Symbol("DONE");
    }
}
