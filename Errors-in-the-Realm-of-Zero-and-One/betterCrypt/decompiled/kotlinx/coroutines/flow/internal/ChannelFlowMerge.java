// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.flow.internal;

import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.channels.ReceiveChannel;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.channels.SendChannel;
import kotlinx.coroutines.sync.SemaphoreKt;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.flow.FlowCollector;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.channels.ProducerScope;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.flow.Flow;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000>\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B5\u0012\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u00040\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b\u0012\b\b\u0002\u0010\t\u001a\u00020\u0006¢\u0006\u0002\u0010\nJ\b\u0010\u000b\u001a\u00020\fH\u0016J\u001f\u0010\r\u001a\u00020\u000e2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00000\u0010H\u0094@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0011J\u001e\u0010\u0012\u001a\b\u0012\u0004\u0012\u00028\u00000\u00022\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0006H\u0014J\u0016\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00000\u00142\u0006\u0010\u000f\u001a\u00020\u0015H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00028\u00000\u00040\u0004X\u0082\u0004¢\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0016" }, d2 = { "Lkotlinx/coroutines/flow/internal/ChannelFlowMerge;", "T", "Lkotlinx/coroutines/flow/internal/ChannelFlow;", "flow", "Lkotlinx/coroutines/flow/Flow;", "concurrency", "", "context", "Lkotlin/coroutines/CoroutineContext;", "capacity", "(Lkotlinx/coroutines/flow/Flow;ILkotlin/coroutines/CoroutineContext;I)V", "additionalToStringProps", "", "collectTo", "", "scope", "Lkotlinx/coroutines/channels/ProducerScope;", "(Lkotlinx/coroutines/channels/ProducerScope;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "create", "produceImpl", "Lkotlinx/coroutines/channels/ReceiveChannel;", "Lkotlinx/coroutines/CoroutineScope;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class ChannelFlowMerge<T> extends ChannelFlow<T>
{
    private final int concurrency;
    private final Flow<Flow<T>> flow;
    
    public ChannelFlowMerge(final Flow<? extends Flow<? extends T>> flow, final int concurrency, final CoroutineContext coroutineContext, final int n) {
        Intrinsics.checkParameterIsNotNull(flow, "flow");
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        super(coroutineContext, n);
        this.flow = (Flow<Flow<T>>)flow;
        this.concurrency = concurrency;
    }
    
    @Override
    public String additionalToStringProps() {
        final StringBuilder sb = new StringBuilder();
        sb.append("concurrency=");
        sb.append(this.concurrency);
        sb.append(", ");
        return sb.toString();
    }
    
    @Override
    protected Object collectTo(final ProducerScope<? super T> producerScope, final Continuation<? super Unit> continuation) {
        return this.flow.collect((FlowCollector<? super Flow<T>>)new ChannelFlowMerge$collectTo$$inlined$collect.ChannelFlowMerge$collectTo$$inlined$collect$1((Job)continuation.getContext().get((CoroutineContext.Key<Job>)Job.Key), SemaphoreKt.Semaphore$default(this.concurrency, 0, 2, null), (ProducerScope)producerScope, new SendingCollector((SendChannel<? super T>)producerScope)), continuation);
    }
    
    @Override
    protected ChannelFlow<T> create(final CoroutineContext coroutineContext, final int n) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        return new ChannelFlowMerge(this.flow, this.concurrency, coroutineContext, n);
    }
    
    @Override
    public ReceiveChannel<T> produceImpl(final CoroutineScope coroutineScope) {
        Intrinsics.checkParameterIsNotNull(coroutineScope, "scope");
        return FlowCoroutineKt.flowProduce(coroutineScope, this.context, this.capacity, (Function2<? super ProducerScope<? super T>, ? super Continuation<? super Unit>, ?>)this.getCollectToFun$kotlinx_coroutines_core());
    }
}
