// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.flow.internal;

import kotlin.TypeCastException;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.jvm.internal.TypeIntrinsics;
import kotlinx.coroutines.internal.ThreadContextKt;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.flow.Flow;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.flow.FlowCollector;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000.\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a[\u0010\u0000\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u0001\"\u0004\b\u0001\u0010\u00022\u0006\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u00062\"\u0010\u0007\u001a\u001e\b\u0001\u0012\u0004\u0012\u0002H\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00010\t\u0012\u0006\u0012\u0004\u0018\u00010\u00060\b2\u0006\u0010\n\u001a\u0002H\u0002H\u0082@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000b\u001a\u001e\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\u00010\r\"\u0004\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u000eH\u0000\u001a&\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\u00010\u0010\"\u0004\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u0004H\u0002\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0012" }, d2 = { "withContextUndispatched", "T", "V", "newContext", "Lkotlin/coroutines/CoroutineContext;", "countOrElement", "", "block", "Lkotlin/Function2;", "Lkotlin/coroutines/Continuation;", "value", "(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "asChannelFlow", "Lkotlinx/coroutines/flow/internal/ChannelFlow;", "Lkotlinx/coroutines/flow/Flow;", "withUndispatchedContextCollector", "Lkotlinx/coroutines/flow/FlowCollector;", "emitContext", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class ChannelFlowKt
{
    public static final <T> ChannelFlow<T> asChannelFlow(final Flow<? extends T> flow) {
        Intrinsics.checkParameterIsNotNull(flow, "$this$asChannelFlow");
        ChannelFlow<T> channelFlow;
        if (!(flow instanceof ChannelFlow)) {
            channelFlow = null;
        }
        else {
            channelFlow = (ChannelFlow<T>)flow;
        }
        final ChannelFlow<T> channelFlow2 = channelFlow;
        if (channelFlow2 != null) {
            return channelFlow2;
        }
        return (ChannelFlow<T>)new ChannelFlowOperatorImpl(flow, null, 0, 6, null);
    }
    
    private static final <T> FlowCollector<T> withUndispatchedContextCollector(final FlowCollector<? super T> flowCollector, final CoroutineContext coroutineContext) {
        if (flowCollector instanceof SendingCollector) {
            return (FlowCollector<T>)flowCollector;
        }
        if (flowCollector instanceof NopCollector) {
            return (FlowCollector<T>)flowCollector;
        }
        return new UndispatchedContextCollector<T>(flowCollector, coroutineContext);
    }
}
