// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.flow.internal;

import kotlinx.coroutines.flow.Flow;
import kotlinx.coroutines.flow.FlowCollector;
import kotlin.jvm.functions.Function3;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlinx.coroutines.AbstractCoroutine;
import kotlinx.coroutines.intrinsics.UndispatchedKt;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.channels.ChannelKt;
import kotlinx.coroutines.CoroutineContextKt;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.channels.ReceiveChannel;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.channels.ProducerScope;
import kotlin.jvm.functions.Function2;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineScope;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000J\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\u001aB\u0010\u0000\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u00012)\b\u0001\u0010\u0002\u001a#\b\u0001\u0012\u0004\u0012\u00020\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00010\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u0003¢\u0006\u0002\b\u0007H\u0080@\u00f8\u0001\u0000¢\u0006\u0002\u0010\b\u001aS\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\u00010\n\"\u0004\b\u0000\u0010\u000125\b\u0001\u0010\u0002\u001a/\b\u0001\u0012\u0004\u0012\u00020\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00010\f\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u000b¢\u0006\u0002\b\u0007H\u0000\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000e\u001ac\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\u00110\u0010\"\u0004\b\u0000\u0010\u0011*\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u00132\b\b\u0002\u0010\u0014\u001a\u00020\u00152/\b\u0001\u0010\u0002\u001a)\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00110\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u0003¢\u0006\u0002\b\u0007H\u0000\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0017\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0018" }, d2 = { "flowScope", "R", "block", "Lkotlin/Function2;", "Lkotlinx/coroutines/CoroutineScope;", "Lkotlin/coroutines/Continuation;", "", "Lkotlin/ExtensionFunctionType;", "(Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "scopedFlow", "Lkotlinx/coroutines/flow/Flow;", "Lkotlin/Function3;", "Lkotlinx/coroutines/flow/FlowCollector;", "", "(Lkotlin/jvm/functions/Function3;)Lkotlinx/coroutines/flow/Flow;", "flowProduce", "Lkotlinx/coroutines/channels/ReceiveChannel;", "T", "context", "Lkotlin/coroutines/CoroutineContext;", "capacity", "", "Lkotlinx/coroutines/channels/ProducerScope;", "(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;ILkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/channels/ReceiveChannel;", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class FlowCoroutineKt
{
    public static final <T> ReceiveChannel<T> flowProduce(final CoroutineScope coroutineScope, final CoroutineContext coroutineContext, final int n, final Function2<? super ProducerScope<? super T>, ? super Continuation<? super Unit>, ?> function2) {
        Intrinsics.checkParameterIsNotNull(coroutineScope, "$this$flowProduce");
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        Intrinsics.checkParameterIsNotNull(function2, "block");
        final FlowProduceCoroutine<T> flowProduceCoroutine = new FlowProduceCoroutine<T>(CoroutineContextKt.newCoroutineContext(coroutineScope, coroutineContext), ChannelKt.Channel(n));
        flowProduceCoroutine.start(CoroutineStart.DEFAULT, flowProduceCoroutine, (Function2<? super FlowProduceCoroutine<T>, ? super Continuation<? super Object>, ?>)function2);
        return flowProduceCoroutine;
    }
    
    public static final <R> Object flowScope(final Function2<? super CoroutineScope, ? super Continuation<? super R>, ?> function2, final Continuation<? super R> continuation) {
        final FlowCoroutine<Object> flowCoroutine = new FlowCoroutine<Object>(continuation.getContext(), (Continuation<? super Object>)continuation);
        final Object startUndispatchedOrReturn = UndispatchedKt.startUndispatchedOrReturn((AbstractCoroutine<? super Object>)flowCoroutine, flowCoroutine, (Function2<? super FlowCoroutine<Object>, ? super Continuation<? super Object>, ?>)function2);
        if (startUndispatchedOrReturn == IntrinsicsKt.getCOROUTINE_SUSPENDED()) {
            DebugProbesKt.probeCoroutineSuspended(continuation);
        }
        return startUndispatchedOrReturn;
    }
    
    public static final <R> Flow<R> scopedFlow(final Function3<? super CoroutineScope, ? super FlowCollector<? super R>, ? super Continuation<? super Unit>, ?> function3) {
        Intrinsics.checkParameterIsNotNull(function3, "block");
        return (Flow<R>)new FlowCoroutineKt$scopedFlow$$inlined$unsafeFlow.FlowCoroutineKt$scopedFlow$$inlined$unsafeFlow$1((Function3)function3);
    }
}
