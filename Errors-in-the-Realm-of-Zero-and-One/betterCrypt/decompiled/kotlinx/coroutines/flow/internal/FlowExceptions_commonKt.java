// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.flow.internal;

import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\n\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\u001a\u0011\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0081\b¨\u0006\u0003" }, d2 = { "checkIndexOverflow", "", "index", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class FlowExceptions_commonKt
{
    public static final int checkIndexOverflow(final int n) {
        if (n >= 0) {
            return n;
        }
        throw new ArithmeticException("Index overflow has happened");
    }
}
