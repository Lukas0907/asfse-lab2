// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.flow.internal;

import kotlin.jvm.functions.Function2;
import kotlin.Unit;
import kotlinx.coroutines.channels.SendChannel;
import kotlinx.coroutines.channels.ProducerScope;
import kotlin.coroutines.ContinuationInterceptor;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.flow.FlowCollector;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.flow.Flow;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000>\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0000\b \u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\b\u0012\u0004\u0012\u0002H\u00020\u0003B#\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u001f\u0010\u000b\u001a\u00020\f2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00010\u000eH\u0096@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000fJ\u001f\u0010\u0010\u001a\u00020\f2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00028\u00010\u0012H\u0094@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0013J'\u0010\u0014\u001a\u00020\f2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00010\u000e2\u0006\u0010\u0015\u001a\u00020\u0007H\u0082@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0016J\u001f\u0010\u0017\u001a\u00020\f2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00010\u000eH¤@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000fJ\b\u0010\u0018\u001a\u00020\u0019H\u0016R\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u00058\u0006X\u0087\u0004¢\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u001a" }, d2 = { "Lkotlinx/coroutines/flow/internal/ChannelFlowOperator;", "S", "T", "Lkotlinx/coroutines/flow/internal/ChannelFlow;", "flow", "Lkotlinx/coroutines/flow/Flow;", "context", "Lkotlin/coroutines/CoroutineContext;", "capacity", "", "(Lkotlinx/coroutines/flow/Flow;Lkotlin/coroutines/CoroutineContext;I)V", "collect", "", "collector", "Lkotlinx/coroutines/flow/FlowCollector;", "(Lkotlinx/coroutines/flow/FlowCollector;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "collectTo", "scope", "Lkotlinx/coroutines/channels/ProducerScope;", "(Lkotlinx/coroutines/channels/ProducerScope;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "collectWithContextUndispatched", "newContext", "(Lkotlinx/coroutines/flow/FlowCollector;Lkotlin/coroutines/CoroutineContext;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "flowCollect", "toString", "", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public abstract class ChannelFlowOperator<S, T> extends ChannelFlow<T>
{
    public final Flow<S> flow;
    
    public ChannelFlowOperator(final Flow<? extends S> flow, final CoroutineContext coroutineContext, final int n) {
        Intrinsics.checkParameterIsNotNull(flow, "flow");
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        super(coroutineContext, n);
        this.flow = (Flow<S>)flow;
    }
    
    static /* synthetic */ Object collect$suspendImpl(final ChannelFlowOperator channelFlowOperator, final FlowCollector flowCollector, final Continuation continuation) {
        if (channelFlowOperator.capacity == -3) {
            final CoroutineContext context = continuation.getContext();
            final CoroutineContext plus = context.plus(channelFlowOperator.context);
            if (Intrinsics.areEqual(plus, context)) {
                return channelFlowOperator.flowCollect(flowCollector, continuation);
            }
            if (Intrinsics.areEqual(plus.get((CoroutineContext.Key<ContinuationInterceptor>)ContinuationInterceptor.Key), context.get((CoroutineContext.Key<ContinuationInterceptor>)ContinuationInterceptor.Key))) {
                return channelFlowOperator.collectWithContextUndispatched(flowCollector, plus, continuation);
            }
        }
        return channelFlowOperator.collect(flowCollector, continuation);
    }
    
    static /* synthetic */ Object collectTo$suspendImpl(final ChannelFlowOperator channelFlowOperator, final ProducerScope producerScope, final Continuation continuation) {
        return channelFlowOperator.flowCollect(new SendingCollector(producerScope), continuation);
    }
    
    @Override
    public Object collect(final FlowCollector<? super T> flowCollector, final Continuation<? super Unit> continuation) {
        return collect$suspendImpl(this, flowCollector, continuation);
    }
    
    @Override
    protected Object collectTo(final ProducerScope<? super T> producerScope, final Continuation<? super Unit> continuation) {
        return collectTo$suspendImpl(this, producerScope, continuation);
    }
    
    protected abstract Object flowCollect(final FlowCollector<? super T> p0, final Continuation<? super Unit> p1);
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.flow);
        sb.append(" -> ");
        sb.append(super.toString());
        return sb.toString();
    }
}
