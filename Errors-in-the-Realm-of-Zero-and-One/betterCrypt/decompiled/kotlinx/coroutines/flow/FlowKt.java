// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.flow;

import kotlin.collections.IndexedValue;
import java.util.Set;
import java.util.List;
import java.util.Collection;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.channels.SendChannel;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.channels.ReceiveChannel;
import kotlin.jvm.functions.Function7;
import kotlin.ReplaceWith;
import kotlin.DeprecationLevel;
import kotlin.Deprecated;
import kotlin.jvm.functions.Function6;
import kotlin.jvm.functions.Function5;
import kotlin.jvm.functions.Function4;
import kotlin.jvm.functions.Function3;
import kotlin.Unit;
import kotlinx.coroutines.channels.ProducerScope;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.channels.BroadcastChannel;
import kotlin.sequences.Sequence;
import kotlin.ranges.LongRange;
import kotlin.ranges.IntRange;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function0;
import java.util.Iterator;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "kotlinx/coroutines/flow/FlowKt__BuildersKt", "kotlinx/coroutines/flow/FlowKt__ChannelsKt", "kotlinx/coroutines/flow/FlowKt__CollectKt", "kotlinx/coroutines/flow/FlowKt__CollectionKt", "kotlinx/coroutines/flow/FlowKt__ContextKt", "kotlinx/coroutines/flow/FlowKt__CountKt", "kotlinx/coroutines/flow/FlowKt__DelayKt", "kotlinx/coroutines/flow/FlowKt__DistinctKt", "kotlinx/coroutines/flow/FlowKt__EmittersKt", "kotlinx/coroutines/flow/FlowKt__ErrorsKt", "kotlinx/coroutines/flow/FlowKt__LimitKt", "kotlinx/coroutines/flow/FlowKt__MergeKt", "kotlinx/coroutines/flow/FlowKt__MigrationKt", "kotlinx/coroutines/flow/FlowKt__ReduceKt", "kotlinx/coroutines/flow/FlowKt__TransformKt", "kotlinx/coroutines/flow/FlowKt__ZipKt" }, k = 4, mv = { 1, 1, 15 })
public final class FlowKt
{
    public static final String DEFAULT_CONCURRENCY_PROPERTY_NAME = "kotlinx.coroutines.flow.defaultConcurrency";
    
    public static final <T> Flow<T> asFlow(final Iterable<? extends T> iterable) {
        return FlowKt__BuildersKt.asFlow(iterable);
    }
    
    public static final <T> Flow<T> asFlow(final Iterator<? extends T> iterator) {
        return FlowKt__BuildersKt.asFlow(iterator);
    }
    
    public static final <T> Flow<T> asFlow(final Function0<? extends T> function0) {
        return FlowKt__BuildersKt.asFlow(function0);
    }
    
    public static final <T> Flow<T> asFlow(final Function1<? super Continuation<? super T>, ?> function1) {
        return FlowKt__BuildersKt.asFlow(function1);
    }
    
    public static final Flow<Integer> asFlow(final IntRange intRange) {
        return FlowKt__BuildersKt.asFlow(intRange);
    }
    
    public static final Flow<Long> asFlow(final LongRange longRange) {
        return FlowKt__BuildersKt.asFlow(longRange);
    }
    
    public static final <T> Flow<T> asFlow(final Sequence<? extends T> sequence) {
        return FlowKt__BuildersKt.asFlow(sequence);
    }
    
    public static final <T> Flow<T> asFlow(final BroadcastChannel<T> broadcastChannel) {
        return FlowKt__ChannelsKt.asFlow(broadcastChannel);
    }
    
    public static final Flow<Integer> asFlow(final int[] array) {
        return FlowKt__BuildersKt.asFlow(array);
    }
    
    public static final Flow<Long> asFlow(final long[] array) {
        return FlowKt__BuildersKt.asFlow(array);
    }
    
    public static final <T> Flow<T> asFlow(final T[] array) {
        return FlowKt__BuildersKt.asFlow(array);
    }
    
    public static final <T> BroadcastChannel<T> broadcastIn(final Flow<? extends T> flow, final CoroutineScope coroutineScope, final CoroutineStart coroutineStart) {
        return FlowKt__ChannelsKt.broadcastIn(flow, coroutineScope, coroutineStart);
    }
    
    public static final <T> Flow<T> buffer(final Flow<? extends T> flow, final int n) {
        return FlowKt__ContextKt.buffer(flow, n);
    }
    
    public static final <T> Flow<T> callbackFlow(final Function2<? super ProducerScope<? super T>, ? super Continuation<? super Unit>, ?> function2) {
        return FlowKt__BuildersKt.callbackFlow(function2);
    }
    
    public static final <T> Flow<T> catch(final Flow<? extends T> flow, final Function3<? super FlowCollector<? super T>, ? super Throwable, ? super Continuation<? super Unit>, ?> function3) {
        return FlowKt__ErrorsKt.catch(flow, function3);
    }
    
    public static final <T> Object catchImpl(final Flow<? extends T> flow, final FlowCollector<? super T> flowCollector, final Continuation<? super Throwable> continuation) {
        return FlowKt__ErrorsKt.catchImpl((Flow<?>)flow, (FlowCollector<? super Object>)flowCollector, continuation);
    }
    
    public static final <T> Flow<T> channelFlow(final Function2<? super ProducerScope<? super T>, ? super Continuation<? super Unit>, ?> function2) {
        return FlowKt__BuildersKt.channelFlow(function2);
    }
    
    public static final Object collect(final Flow<?> flow, final Continuation<? super Unit> continuation) {
        return FlowKt__CollectKt.collect(flow, continuation);
    }
    
    public static final <T> Object collect(final Flow<? extends T> flow, final Function2<? super T, ? super Continuation<? super Unit>, ?> function2, final Continuation<? super Unit> continuation) {
        return FlowKt__CollectKt.collect((Flow<?>)flow, (Function2<? super Object, ? super Continuation<? super Unit>, ?>)function2, continuation);
    }
    
    private static final Object collect$$forInline(final Flow flow, final Function2 function2, final Continuation continuation) {
        return FlowKt__CollectKt.collect((Flow<?>)flow, (Function2<? super Object, ? super Continuation<? super Unit>, ?>)function2, (Continuation<? super Unit>)continuation);
    }
    
    public static final <T> Object collectIndexed(final Flow<? extends T> flow, final Function3<? super Integer, ? super T, ? super Continuation<? super Unit>, ?> function3, final Continuation<? super Unit> continuation) {
        return FlowKt__CollectKt.collectIndexed((Flow<?>)flow, (Function3<? super Integer, ? super Object, ? super Continuation<? super Unit>, ?>)function3, continuation);
    }
    
    private static final Object collectIndexed$$forInline(final Flow flow, final Function3 function3, final Continuation continuation) {
        return FlowKt__CollectKt.collectIndexed((Flow<?>)flow, (Function3<? super Integer, ? super Object, ? super Continuation<? super Unit>, ?>)function3, (Continuation<? super Unit>)continuation);
    }
    
    public static final <T> Object collectLatest(final Flow<? extends T> flow, final Function2<? super T, ? super Continuation<? super Unit>, ?> function2, final Continuation<? super Unit> continuation) {
        return FlowKt__CollectKt.collectLatest((Flow<?>)flow, (Function2<? super Object, ? super Continuation<? super Unit>, ?>)function2, continuation);
    }
    
    public static final <T1, T2, R> Flow<R> combine(final Flow<? extends T1> flow, final Flow<? extends T2> flow2, final Function3<? super T1, ? super T2, ? super Continuation<? super R>, ?> function3) {
        return FlowKt__ZipKt.combine((Flow<?>)flow, (Flow<?>)flow2, (Function3<? super Object, ? super Object, ? super Continuation<? super R>, ?>)function3);
    }
    
    public static final <T1, T2, T3, R> Flow<R> combine(final Flow<? extends T1> flow, final Flow<? extends T2> flow2, final Flow<? extends T3> flow3, final Function4<? super T1, ? super T2, ? super T3, ? super Continuation<? super R>, ?> function4) {
        return FlowKt__ZipKt.combine((Flow<?>)flow, (Flow<?>)flow2, (Flow<?>)flow3, (Function4<? super Object, ? super Object, ? super Object, ? super Continuation<? super R>, ?>)function4);
    }
    
    public static final <T1, T2, T3, T4, R> Flow<R> combine(final Flow<? extends T1> flow, final Flow<? extends T2> flow2, final Flow<? extends T3> flow3, final Flow<? extends T4> flow4, final Function5<? super T1, ? super T2, ? super T3, ? super T4, ? super Continuation<? super R>, ?> function5) {
        return FlowKt__ZipKt.combine((Flow<?>)flow, (Flow<?>)flow2, (Flow<?>)flow3, (Flow<?>)flow4, (Function5<? super Object, ? super Object, ? super Object, ? super Object, ? super Continuation<? super R>, ?>)function5);
    }
    
    public static final <T1, T2, T3, T4, T5, R> Flow<R> combine(final Flow<? extends T1> flow, final Flow<? extends T2> flow2, final Flow<? extends T3> flow3, final Flow<? extends T4> flow4, final Flow<? extends T5> flow5, final Function6<? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? super Continuation<? super R>, ?> function6) {
        return FlowKt__ZipKt.combine((Flow<?>)flow, (Flow<?>)flow2, (Flow<?>)flow3, (Flow<?>)flow4, (Flow<?>)flow5, (Function6<? super Object, ? super Object, ? super Object, ? super Object, ? super Object, ? super Continuation<? super R>, ?>)function6);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow analogue of 'combineLatest' is 'combine'", replaceWith = @ReplaceWith(expression = "this.combine(other, transform)", imports = {}))
    public static final <T1, T2, R> Flow<R> combineLatest(final Flow<? extends T1> flow, final Flow<? extends T2> flow2, final Function3<? super T1, ? super T2, ? super Continuation<? super R>, ?> function3) {
        return FlowKt__MigrationKt.combineLatest((Flow<?>)flow, (Flow<?>)flow2, (Function3<? super Object, ? super Object, ? super Continuation<? super R>, ?>)function3);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow analogue of 'combineLatest' is 'combine'", replaceWith = @ReplaceWith(expression = "combine(this, other, other2, transform)", imports = {}))
    public static final <T1, T2, T3, R> Flow<R> combineLatest(final Flow<? extends T1> flow, final Flow<? extends T2> flow2, final Flow<? extends T3> flow3, final Function4<? super T1, ? super T2, ? super T3, ? super Continuation<? super R>, ?> function4) {
        return FlowKt__MigrationKt.combineLatest((Flow<?>)flow, (Flow<?>)flow2, (Flow<?>)flow3, (Function4<? super Object, ? super Object, ? super Object, ? super Continuation<? super R>, ?>)function4);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow analogue of 'combineLatest' is 'combine'", replaceWith = @ReplaceWith(expression = "combine(this, other, other2, other3, transform)", imports = {}))
    public static final <T1, T2, T3, T4, R> Flow<R> combineLatest(final Flow<? extends T1> flow, final Flow<? extends T2> flow2, final Flow<? extends T3> flow3, final Flow<? extends T4> flow4, final Function5<? super T1, ? super T2, ? super T3, ? super T4, ? super Continuation<? super R>, ?> function5) {
        return FlowKt__MigrationKt.combineLatest((Flow<?>)flow, (Flow<?>)flow2, (Flow<?>)flow3, (Flow<?>)flow4, (Function5<? super Object, ? super Object, ? super Object, ? super Object, ? super Continuation<? super R>, ?>)function5);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow analogue of 'combineLatest' is 'combine'", replaceWith = @ReplaceWith(expression = "combine(this, other, other2, other3, transform)", imports = {}))
    public static final <T1, T2, T3, T4, T5, R> Flow<R> combineLatest(final Flow<? extends T1> flow, final Flow<? extends T2> flow2, final Flow<? extends T3> flow3, final Flow<? extends T4> flow4, final Flow<? extends T5> flow5, final Function6<? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? super Continuation<? super R>, ?> function6) {
        return FlowKt__MigrationKt.combineLatest((Flow<?>)flow, (Flow<?>)flow2, (Flow<?>)flow3, (Flow<?>)flow4, (Flow<?>)flow5, (Function6<? super Object, ? super Object, ? super Object, ? super Object, ? super Object, ? super Continuation<? super R>, ?>)function6);
    }
    
    public static final <T1, T2, R> Flow<R> combineTransform(final Flow<? extends T1> flow, final Flow<? extends T2> flow2, final Function4<? super FlowCollector<? super R>, ? super T1, ? super T2, ? super Continuation<? super Unit>, ?> function4) {
        return FlowKt__ZipKt.combineTransform((Flow<?>)flow, (Flow<?>)flow2, (Function4<? super FlowCollector<? super R>, ? super Object, ? super Object, ? super Continuation<? super Unit>, ?>)function4);
    }
    
    public static final <T1, T2, T3, R> Flow<R> combineTransform(final Flow<? extends T1> flow, final Flow<? extends T2> flow2, final Flow<? extends T3> flow3, final Function5<? super FlowCollector<? super R>, ? super T1, ? super T2, ? super T3, ? super Continuation<? super Unit>, ?> function5) {
        return FlowKt__ZipKt.combineTransform((Flow<?>)flow, (Flow<?>)flow2, (Flow<?>)flow3, (Function5<? super FlowCollector<? super R>, ? super Object, ? super Object, ? super Object, ? super Continuation<? super Unit>, ?>)function5);
    }
    
    public static final <T1, T2, T3, T4, R> Flow<R> combineTransform(final Flow<? extends T1> flow, final Flow<? extends T2> flow2, final Flow<? extends T3> flow3, final Flow<? extends T4> flow4, final Function6<? super FlowCollector<? super R>, ? super T1, ? super T2, ? super T3, ? super T4, ? super Continuation<? super Unit>, ?> function6) {
        return FlowKt__ZipKt.combineTransform((Flow<?>)flow, (Flow<?>)flow2, (Flow<?>)flow3, (Flow<?>)flow4, (Function6<? super FlowCollector<? super R>, ? super Object, ? super Object, ? super Object, ? super Object, ? super Continuation<? super Unit>, ?>)function6);
    }
    
    public static final <T1, T2, T3, T4, T5, R> Flow<R> combineTransform(final Flow<? extends T1> flow, final Flow<? extends T2> flow2, final Flow<? extends T3> flow3, final Flow<? extends T4> flow4, final Flow<? extends T5> flow5, final Function7<? super FlowCollector<? super R>, ? super T1, ? super T2, ? super T3, ? super T4, ? super T5, ? super Continuation<? super Unit>, ?> function7) {
        return FlowKt__ZipKt.combineTransform((Flow<?>)flow, (Flow<?>)flow2, (Flow<?>)flow3, (Flow<?>)flow4, (Flow<?>)flow5, (Function7<? super FlowCollector<? super R>, ? super Object, ? super Object, ? super Object, ? super Object, ? super Object, ? super Continuation<? super Unit>, ?>)function7);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow analogue of 'compose' is 'let'", replaceWith = @ReplaceWith(expression = "let(transformer)", imports = {}))
    public static final <T, R> Flow<R> compose(final Flow<? extends T> flow, final Function1<? super Flow<? extends T>, ? extends Flow<? extends R>> function1) {
        return FlowKt__MigrationKt.compose((Flow<?>)flow, (Function1<? super Flow<?>, ? extends Flow<? extends R>>)function1);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow analogue of 'concatMap' is 'flatMapConcat'", replaceWith = @ReplaceWith(expression = "flatMapConcat(mapper)", imports = {}))
    public static final <T, R> Flow<R> concatMap(final Flow<? extends T> flow, final Function1<? super T, ? extends Flow<? extends R>> function1) {
        return FlowKt__MigrationKt.concatMap((Flow<?>)flow, (Function1<? super Object, ? extends Flow<? extends R>>)function1);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow analogue of 'concatWith' is 'onCompletion'. Use 'onCompletion { emit(value) }'", replaceWith = @ReplaceWith(expression = "onCompletion { emit(value) }", imports = {}))
    public static final <T> Flow<T> concatWith(final Flow<? extends T> flow, final T t) {
        return FlowKt__MigrationKt.concatWith(flow, t);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow analogue of 'concatWith' is 'onCompletion'. Use 'onCompletion { emitAll(other) }'", replaceWith = @ReplaceWith(expression = "onCompletion { emitAll(other) }", imports = {}))
    public static final <T> Flow<T> concatWith(final Flow<? extends T> flow, final Flow<? extends T> flow2) {
        return FlowKt__MigrationKt.concatWith(flow, flow2);
    }
    
    public static final <T> Flow<T> conflate(final Flow<? extends T> flow) {
        return FlowKt__ContextKt.conflate(flow);
    }
    
    public static final <T> Flow<T> consumeAsFlow(final ReceiveChannel<? extends T> receiveChannel) {
        return FlowKt__ChannelsKt.consumeAsFlow(receiveChannel);
    }
    
    public static final <T> Object count(final Flow<? extends T> flow, final Continuation<? super Integer> continuation) {
        return FlowKt__CountKt.count((Flow<?>)flow, continuation);
    }
    
    public static final <T> Object count(final Flow<? extends T> flow, final Function2<? super T, ? super Continuation<? super Boolean>, ?> function2, final Continuation<? super Integer> continuation) {
        return FlowKt__CountKt.count((Flow<?>)flow, (Function2<? super Object, ? super Continuation<? super Boolean>, ?>)function2, continuation);
    }
    
    public static final <T> Flow<T> debounce(final Flow<? extends T> flow, final long n) {
        return FlowKt__DelayKt.debounce(flow, n);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Use 'onEach { delay(timeMillis) }'", replaceWith = @ReplaceWith(expression = "onEach { delay(timeMillis) }", imports = {}))
    public static final <T> Flow<T> delayEach(final Flow<? extends T> flow, final long n) {
        return FlowKt__MigrationKt.delayEach(flow, n);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Use 'onStart { delay(timeMillis) }'", replaceWith = @ReplaceWith(expression = "onStart { delay(timeMillis) }", imports = {}))
    public static final <T> Flow<T> delayFlow(final Flow<? extends T> flow, final long n) {
        return FlowKt__MigrationKt.delayFlow(flow, n);
    }
    
    public static final <T> Flow<T> distinctUntilChanged(final Flow<? extends T> flow) {
        return FlowKt__DistinctKt.distinctUntilChanged(flow);
    }
    
    public static final <T> Flow<T> distinctUntilChanged(final Flow<? extends T> flow, final Function2<? super T, ? super T, Boolean> function2) {
        return FlowKt__DistinctKt.distinctUntilChanged(flow, function2);
    }
    
    public static final <T, K> Flow<T> distinctUntilChangedBy(final Flow<? extends T> flow, final Function1<? super T, ? extends K> function1) {
        return FlowKt__DistinctKt.distinctUntilChangedBy(flow, (Function1<? super T, ?>)function1);
    }
    
    public static final <T> Flow<T> drop(final Flow<? extends T> flow, final int n) {
        return FlowKt__LimitKt.drop(flow, n);
    }
    
    public static final <T> Flow<T> dropWhile(final Flow<? extends T> flow, final Function2<? super T, ? super Continuation<? super Boolean>, ?> function2) {
        return FlowKt__LimitKt.dropWhile(flow, function2);
    }
    
    public static final <T> Object emitAll(final FlowCollector<? super T> flowCollector, final ReceiveChannel<? extends T> receiveChannel, final Continuation<? super Unit> continuation) {
        return FlowKt__ChannelsKt.emitAll((FlowCollector<? super Object>)flowCollector, (ReceiveChannel<?>)receiveChannel, continuation);
    }
    
    public static final <T> Object emitAll(final FlowCollector<? super T> flowCollector, final Flow<? extends T> flow, final Continuation<? super Unit> continuation) {
        return FlowKt__CollectKt.emitAll((FlowCollector<? super Object>)flowCollector, (Flow<?>)flow, continuation);
    }
    
    private static final Object emitAll$$forInline(final FlowCollector flowCollector, final Flow flow, final Continuation continuation) {
        return FlowKt__CollectKt.emitAll((FlowCollector<? super Object>)flowCollector, (Flow<?>)flow, (Continuation<? super Unit>)continuation);
    }
    
    public static final <T> Flow<T> emptyFlow() {
        return FlowKt__BuildersKt.emptyFlow();
    }
    
    public static final <T> Flow<T> filter(final Flow<? extends T> flow, final Function2<? super T, ? super Continuation<? super Boolean>, ?> function2) {
        return FlowKt__TransformKt.filter(flow, function2);
    }
    
    public static final <T> Flow<T> filterNot(final Flow<? extends T> flow, final Function2<? super T, ? super Continuation<? super Boolean>, ?> function2) {
        return FlowKt__TransformKt.filterNot(flow, function2);
    }
    
    public static final <T> Flow<T> filterNotNull(final Flow<? extends T> flow) {
        return FlowKt__TransformKt.filterNotNull(flow);
    }
    
    public static final <T> Object first(final Flow<? extends T> flow, final Continuation<? super T> continuation) {
        return FlowKt__ReduceKt.first((Flow<?>)flow, (Continuation<? super Object>)continuation);
    }
    
    public static final <T> Object first(final Flow<? extends T> flow, final Function2<? super T, ? super Continuation<? super Boolean>, ?> function2, final Continuation<? super T> continuation) {
        return FlowKt__ReduceKt.first((Flow<?>)flow, (Function2<? super Object, ? super Continuation<? super Boolean>, ?>)function2, (Continuation<? super Object>)continuation);
    }
    
    public static final ReceiveChannel<Unit> fixedPeriodTicker(final CoroutineScope coroutineScope, final long n, final long n2) {
        return FlowKt__DelayKt.fixedPeriodTicker(coroutineScope, n, n2);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow analogue is named flatMapConcat", replaceWith = @ReplaceWith(expression = "flatMapConcat(mapper)", imports = {}))
    public static final <T, R> Flow<R> flatMap(final Flow<? extends T> flow, final Function2<? super T, ? super Continuation<? super Flow<? extends R>>, ?> function2) {
        return FlowKt__MigrationKt.flatMap((Flow<?>)flow, (Function2<? super Object, ? super Continuation<? super Flow<? extends R>>, ?>)function2);
    }
    
    public static final <T, R> Flow<R> flatMapConcat(final Flow<? extends T> flow, final Function2<? super T, ? super Continuation<? super Flow<? extends R>>, ?> function2) {
        return FlowKt__MergeKt.flatMapConcat((Flow<?>)flow, (Function2<? super Object, ? super Continuation<? super Flow<? extends R>>, ?>)function2);
    }
    
    public static final <T, R> Flow<R> flatMapLatest(final Flow<? extends T> flow, final Function2<? super T, ? super Continuation<? super Flow<? extends R>>, ?> function2) {
        return FlowKt__MergeKt.flatMapLatest((Flow<?>)flow, (Function2<? super Object, ? super Continuation<? super Flow<? extends R>>, ?>)function2);
    }
    
    public static final <T, R> Flow<R> flatMapMerge(final Flow<? extends T> flow, final int n, final Function2<? super T, ? super Continuation<? super Flow<? extends R>>, ?> function2) {
        return FlowKt__MergeKt.flatMapMerge((Flow<?>)flow, n, (Function2<? super Object, ? super Continuation<? super Flow<? extends R>>, ?>)function2);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow analogue of 'flatten' is 'flattenConcat'", replaceWith = @ReplaceWith(expression = "flattenConcat()", imports = {}))
    public static final <T> Flow<T> flatten(final Flow<? extends Flow<? extends T>> flow) {
        return FlowKt__MigrationKt.flatten(flow);
    }
    
    public static final <T> Flow<T> flattenConcat(final Flow<? extends Flow<? extends T>> flow) {
        return FlowKt__MergeKt.flattenConcat(flow);
    }
    
    public static final <T> Flow<T> flattenMerge(final Flow<? extends Flow<? extends T>> flow, final int n) {
        return FlowKt__MergeKt.flattenMerge(flow, n);
    }
    
    public static final <T> Flow<T> flow(final Function2<? super FlowCollector<? super T>, ? super Continuation<? super Unit>, ?> function2) {
        return FlowKt__BuildersKt.flow(function2);
    }
    
    public static final <T1, T2, R> Flow<R> flowCombine(final Flow<? extends T1> flow, final Flow<? extends T2> flow2, final Function3<? super T1, ? super T2, ? super Continuation<? super R>, ?> function3) {
        return FlowKt__ZipKt.flowCombine((Flow<?>)flow, (Flow<?>)flow2, (Function3<? super Object, ? super Object, ? super Continuation<? super R>, ?>)function3);
    }
    
    public static final <T1, T2, R> Flow<R> flowCombineTransform(final Flow<? extends T1> flow, final Flow<? extends T2> flow2, final Function4<? super FlowCollector<? super R>, ? super T1, ? super T2, ? super Continuation<? super Unit>, ?> function4) {
        return FlowKt__ZipKt.flowCombineTransform((Flow<?>)flow, (Flow<?>)flow2, (Function4<? super FlowCollector<? super R>, ? super Object, ? super Object, ? super Continuation<? super Unit>, ?>)function4);
    }
    
    public static final <T> Flow<T> flowOf(final T t) {
        return FlowKt__BuildersKt.flowOf(t);
    }
    
    public static final <T> Flow<T> flowOf(final T... array) {
        return FlowKt__BuildersKt.flowOf(array);
    }
    
    public static final <T> Flow<T> flowOn(final Flow<? extends T> flow, final CoroutineContext coroutineContext) {
        return FlowKt__ContextKt.flowOn(flow, coroutineContext);
    }
    
    @Deprecated(level = DeprecationLevel.WARNING, message = "Use channelFlow with awaitClose { } instead of flowViaChannel and invokeOnClose { }.")
    public static final <T> Flow<T> flowViaChannel(final int n, final Function2<? super CoroutineScope, ? super SendChannel<? super T>, Unit> function2) {
        return FlowKt__BuildersKt.flowViaChannel(n, function2);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "flowWith is deprecated without replacement, please refer to its KDoc for an explanation")
    public static final <T, R> Flow<R> flowWith(final Flow<? extends T> flow, final CoroutineContext coroutineContext, final int n, final Function1<? super Flow<? extends T>, ? extends Flow<? extends R>> function1) {
        return FlowKt__ContextKt.flowWith((Flow<?>)flow, coroutineContext, n, (Function1<? super Flow<?>, ? extends Flow<? extends R>>)function1);
    }
    
    public static final <T, R> Object fold(final Flow<? extends T> flow, final R r, final Function3<? super R, ? super T, ? super Continuation<? super R>, ?> function3, final Continuation<? super R> continuation) {
        return FlowKt__ReduceKt.fold((Flow<?>)flow, r, (Function3<? super R, ? super Object, ? super Continuation<? super R>, ?>)function3, continuation);
    }
    
    private static final Object fold$$forInline(final Flow flow, final Object o, final Function3 function3, final Continuation continuation) {
        return FlowKt__ReduceKt.fold((Flow<?>)flow, o, (Function3<? super Object, ? super Object, ? super Continuation<? super Object>, ?>)function3, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow analogue of 'forEach' is 'collect'", replaceWith = @ReplaceWith(expression = "collect(block)", imports = {}))
    public static final <T> void forEach(final Flow<? extends T> flow, final Function2<? super T, ? super Continuation<? super Unit>, ?> function2) {
        FlowKt__MigrationKt.forEach((Flow<?>)flow, (Function2<? super Object, ? super Continuation<? super Unit>, ?>)function2);
    }
    
    public static final int getDEFAULT_CONCURRENCY() {
        return FlowKt__MergeKt.getDEFAULT_CONCURRENCY();
    }
    
    public static final <T> Job launchIn(final Flow<? extends T> flow, final CoroutineScope coroutineScope) {
        return FlowKt__CollectKt.launchIn((Flow<?>)flow, coroutineScope);
    }
    
    public static final <T, R> Flow<R> map(final Flow<? extends T> flow, final Function2<? super T, ? super Continuation<? super R>, ?> function2) {
        return FlowKt__TransformKt.map((Flow<?>)flow, (Function2<? super Object, ? super Continuation<? super R>, ?>)function2);
    }
    
    public static final <T, R> Flow<R> mapLatest(final Flow<? extends T> flow, final Function2<? super T, ? super Continuation<? super R>, ?> function2) {
        return FlowKt__MergeKt.mapLatest((Flow<?>)flow, (Function2<? super Object, ? super Continuation<? super R>, ?>)function2);
    }
    
    public static final <T, R> Flow<R> mapNotNull(final Flow<? extends T> flow, final Function2<? super T, ? super Continuation<? super R>, ?> function2) {
        return FlowKt__TransformKt.mapNotNull((Flow<?>)flow, (Function2<? super Object, ? super Continuation<? super R>, ?>)function2);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow analogue of 'merge' is 'flattenConcat'", replaceWith = @ReplaceWith(expression = "flattenConcat()", imports = {}))
    public static final <T> Flow<T> merge(final Flow<? extends Flow<? extends T>> flow) {
        return FlowKt__MigrationKt.merge(flow);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Collect flow in the desired context instead")
    public static final <T> Flow<T> observeOn(final Flow<? extends T> flow, final CoroutineContext coroutineContext) {
        return FlowKt__MigrationKt.observeOn(flow, coroutineContext);
    }
    
    public static final <T> Flow<T> onCompletion(final Flow<? extends T> flow, final Function3<? super FlowCollector<? super T>, ? super Throwable, ? super Continuation<? super Unit>, ?> function3) {
        return FlowKt__EmittersKt.onCompletion(flow, function3);
    }
    
    public static final <T> Flow<T> onEach(final Flow<? extends T> flow, final Function2<? super T, ? super Continuation<? super Unit>, ?> function2) {
        return FlowKt__TransformKt.onEach(flow, function2);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Use catch { e -> if (predicate(e)) emitAll(fallback) else throw e }", replaceWith = @ReplaceWith(expression = "catch { e -> if (predicate(e)) emitAll(fallback) else throw e }", imports = {}))
    public static final <T> Flow<T> onErrorCollect(final Flow<? extends T> flow, final Flow<? extends T> flow2, final Function1<? super Throwable, Boolean> function1) {
        return FlowKt__ErrorsKt.onErrorCollect(flow, flow2, function1);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow analogue of 'onErrorXxx' is 'catch'. Use 'catch { emitAll(fallback) }'", replaceWith = @ReplaceWith(expression = "catch { emitAll(fallback) }", imports = {}))
    public static final <T> Flow<T> onErrorResume(final Flow<? extends T> flow, final Flow<? extends T> flow2) {
        return FlowKt__MigrationKt.onErrorResume(flow, flow2);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow analogue of 'onErrorXxx' is 'catch'. Use 'catch { emitAll(fallback) }'", replaceWith = @ReplaceWith(expression = "catch { emitAll(fallback) }", imports = {}))
    public static final <T> Flow<T> onErrorResumeNext(final Flow<? extends T> flow, final Flow<? extends T> flow2) {
        return FlowKt__MigrationKt.onErrorResumeNext(flow, flow2);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow analogue of 'onErrorXxx' is 'catch'. Use 'catch { emit(fallback) }'", replaceWith = @ReplaceWith(expression = "catch { emit(fallback) }", imports = {}))
    public static final <T> Flow<T> onErrorReturn(final Flow<? extends T> flow, final T t) {
        return FlowKt__MigrationKt.onErrorReturn(flow, t);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow analogue of 'onErrorXxx' is 'catch'. Use 'catch { e -> if (predicate(e)) emit(fallback) else throw e }'", replaceWith = @ReplaceWith(expression = "catch { e -> if (predicate(e)) emit(fallback) else throw e }", imports = {}))
    public static final <T> Flow<T> onErrorReturn(final Flow<? extends T> flow, final T t, final Function1<? super Throwable, Boolean> function1) {
        return FlowKt__MigrationKt.onErrorReturn(flow, t, function1);
    }
    
    public static final <T> Flow<T> onStart(final Flow<? extends T> flow, final Function2<? super FlowCollector<? super T>, ? super Continuation<? super Unit>, ?> function2) {
        return FlowKt__EmittersKt.onStart(flow, function2);
    }
    
    public static final <T> ReceiveChannel<T> produceIn(final Flow<? extends T> flow, final CoroutineScope coroutineScope) {
        return FlowKt__ChannelsKt.produceIn(flow, coroutineScope);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Collect flow in the desired context instead")
    public static final <T> Flow<T> publishOn(final Flow<? extends T> flow, final CoroutineContext coroutineContext) {
        return FlowKt__MigrationKt.publishOn(flow, coroutineContext);
    }
    
    public static final <S, T extends S> Object reduce(final Flow<? extends T> flow, final Function3<? super S, ? super T, ? super Continuation<? super S>, ?> function3, final Continuation<? super S> continuation) {
        return FlowKt__ReduceKt.reduce((Flow<?>)flow, (Function3<? super Object, ? super Object, ? super Continuation<? super Object>, ?>)function3, (Continuation<? super Object>)continuation);
    }
    
    public static final <T> Flow<T> retry(final Flow<? extends T> flow, final long n, final Function2<? super Throwable, ? super Continuation<? super Boolean>, ?> function2) {
        return FlowKt__ErrorsKt.retry(flow, n, function2);
    }
    
    public static final <T> Flow<T> retryWhen(final Flow<? extends T> flow, final Function4<? super FlowCollector<? super T>, ? super Throwable, ? super Long, ? super Continuation<? super Boolean>, ?> function4) {
        return FlowKt__ErrorsKt.retryWhen(flow, function4);
    }
    
    public static final <T> Flow<T> sample(final Flow<? extends T> flow, final long n) {
        return FlowKt__DelayKt.sample(flow, n);
    }
    
    public static final <T, R> Flow<R> scan(final Flow<? extends T> flow, final R r, final Function3<? super R, ? super T, ? super Continuation<? super R>, ?> function3) {
        return FlowKt__TransformKt.scan((Flow<?>)flow, r, (Function3<? super R, ? super Object, ? super Continuation<? super R>, ?>)function3);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow has less verbose 'scan' shortcut", replaceWith = @ReplaceWith(expression = "scan(initial, operation)", imports = {}))
    public static final <T, R> Flow<R> scanFold(final Flow<? extends T> flow, final R r, final Function3<? super R, ? super T, ? super Continuation<? super R>, ?> function3) {
        return FlowKt__MigrationKt.scanFold((Flow<?>)flow, r, (Function3<? super R, ? super Object, ? super Continuation<? super R>, ?>)function3);
    }
    
    public static final <T> Flow<T> scanReduce(final Flow<? extends T> flow, final Function3<? super T, ? super T, ? super Continuation<? super T>, ?> function3) {
        return FlowKt__TransformKt.scanReduce(flow, function3);
    }
    
    public static final <T> Object single(final Flow<? extends T> flow, final Continuation<? super T> continuation) {
        return FlowKt__ReduceKt.single((Flow<?>)flow, (Continuation<? super Object>)continuation);
    }
    
    public static final <T> Object singleOrNull(final Flow<? extends T> flow, final Continuation<? super T> continuation) {
        return FlowKt__ReduceKt.singleOrNull((Flow<?>)flow, (Continuation<? super Object>)continuation);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow analogue of 'skip' is 'drop'", replaceWith = @ReplaceWith(expression = "drop(count)", imports = {}))
    public static final <T> Flow<T> skip(final Flow<? extends T> flow, final int n) {
        return FlowKt__MigrationKt.skip(flow, n);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow analogue of 'startWith' is 'onStart'. Use 'onStart { emit(value) }'", replaceWith = @ReplaceWith(expression = "onStart { emit(value) }", imports = {}))
    public static final <T> Flow<T> startWith(final Flow<? extends T> flow, final T t) {
        return FlowKt__MigrationKt.startWith(flow, t);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow analogue of 'startWith' is 'onStart'. Use 'onStart { emitAll(other) }'", replaceWith = @ReplaceWith(expression = "onStart { emitAll(other) }", imports = {}))
    public static final <T> Flow<T> startWith(final Flow<? extends T> flow, final Flow<? extends T> flow2) {
        return FlowKt__MigrationKt.startWith(flow, flow2);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Use launchIn with onEach, onCompletion and catch operators instead")
    public static final <T> void subscribe(final Flow<? extends T> flow) {
        FlowKt__MigrationKt.subscribe((Flow<?>)flow);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Use launchIn with onEach, onCompletion and catch operators instead")
    public static final <T> void subscribe(final Flow<? extends T> flow, final Function2<? super T, ? super Continuation<? super Unit>, ?> function2) {
        FlowKt__MigrationKt.subscribe((Flow<?>)flow, (Function2<? super Object, ? super Continuation<? super Unit>, ?>)function2);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Use launchIn with onEach, onCompletion and catch operators instead")
    public static final <T> void subscribe(final Flow<? extends T> flow, final Function2<? super T, ? super Continuation<? super Unit>, ?> function2, final Function2<? super Throwable, ? super Continuation<? super Unit>, ?> function3) {
        FlowKt__MigrationKt.subscribe((Flow<?>)flow, (Function2<? super Object, ? super Continuation<? super Unit>, ?>)function2, function3);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Use flowOn instead")
    public static final <T> Flow<T> subscribeOn(final Flow<? extends T> flow, final CoroutineContext coroutineContext) {
        return FlowKt__MigrationKt.subscribeOn(flow, coroutineContext);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Flow analogues of 'switchMap' are 'transformLatest', 'flatMapLatest' and 'mapLatest'", replaceWith = @ReplaceWith(expression = "this.flatMapLatest(transform)", imports = {}))
    public static final <T, R> Flow<R> switchMap(final Flow<? extends T> flow, final Function2<? super T, ? super Continuation<? super Flow<? extends R>>, ?> function2) {
        return FlowKt__MigrationKt.switchMap((Flow<?>)flow, (Function2<? super Object, ? super Continuation<? super Flow<? extends R>>, ?>)function2);
    }
    
    public static final <T> Flow<T> take(final Flow<? extends T> flow, final int n) {
        return FlowKt__LimitKt.take(flow, n);
    }
    
    public static final <T> Flow<T> takeWhile(final Flow<? extends T> flow, final Function2<? super T, ? super Continuation<? super Boolean>, ?> function2) {
        return FlowKt__LimitKt.takeWhile(flow, function2);
    }
    
    public static final <T, C extends Collection<? super T>> Object toCollection(final Flow<? extends T> flow, final C c, final Continuation<? super C> continuation) {
        return FlowKt__CollectionKt.toCollection((Flow<?>)flow, c, continuation);
    }
    
    public static final <T> Object toList(final Flow<? extends T> flow, final List<T> list, final Continuation<? super List<? extends T>> continuation) {
        return FlowKt__CollectionKt.toList(flow, list, continuation);
    }
    
    public static final <T> Object toSet(final Flow<? extends T> flow, final Set<T> set, final Continuation<? super Set<? extends T>> continuation) {
        return FlowKt__CollectionKt.toSet(flow, set, continuation);
    }
    
    public static final <T, R> Flow<R> transform(final Flow<? extends T> flow, final Function3<? super FlowCollector<? super R>, ? super T, ? super Continuation<? super Unit>, ?> function3) {
        return FlowKt__EmittersKt.transform((Flow<?>)flow, (Function3<? super FlowCollector<? super R>, ? super Object, ? super Continuation<? super Unit>, ?>)function3);
    }
    
    public static final <T, R> Flow<R> transformLatest(final Flow<? extends T> flow, final Function3<? super FlowCollector<? super R>, ? super T, ? super Continuation<? super Unit>, ?> function3) {
        return FlowKt__MergeKt.transformLatest((Flow<?>)flow, (Function3<? super FlowCollector<? super R>, ? super Object, ? super Continuation<? super Unit>, ?>)function3);
    }
    
    public static final <T, R> Flow<R> unsafeTransform(final Flow<? extends T> flow, final Function3<? super FlowCollector<? super R>, ? super T, ? super Continuation<? super Unit>, ?> function3) {
        return FlowKt__EmittersKt.unsafeTransform((Flow<?>)flow, (Function3<? super FlowCollector<? super R>, ? super Object, ? super Continuation<? super Unit>, ?>)function3);
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "withContext in flow body is deprecated, use flowOn instead")
    public static final <T, R> void withContext(final FlowCollector<? super T> flowCollector, final CoroutineContext coroutineContext, final Function1<? super Continuation<? super R>, ?> function1) {
        FlowKt__MigrationKt.withContext((FlowCollector<? super Object>)flowCollector, coroutineContext, (Function1<? super Continuation<? super Object>, ?>)function1);
    }
    
    public static final <T> Flow<IndexedValue<T>> withIndex(final Flow<? extends T> flow) {
        return FlowKt__TransformKt.withIndex(flow);
    }
    
    public static final <T1, T2, R> Flow<R> zip(final Flow<? extends T1> flow, final Flow<? extends T2> flow2, final Function3<? super T1, ? super T2, ? super Continuation<? super R>, ?> function3) {
        return FlowKt__ZipKt.zip((Flow<?>)flow, (Flow<?>)flow2, (Function3<? super Object, ? super Object, ? super Continuation<? super R>, ?>)function3);
    }
}
