// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function2;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0002\u0010\u0002\n\u0002\b\u0007\u001a%\u0010\u0000\u001a\u00020\u00012\u001a\b\u0004\u0010\u0002\u001a\u0014\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0003H\u0086\b\u001a\u0018\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u0005H\u0007\u001a\u0018\u0010\n\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\u00052\u0006\u0010\f\u001a\u00020\u0005H\u0000¨\u0006\r" }, d2 = { "CoroutineExceptionHandler", "Lkotlinx/coroutines/CoroutineExceptionHandler;", "handler", "Lkotlin/Function2;", "Lkotlin/coroutines/CoroutineContext;", "", "", "handleCoroutineException", "context", "exception", "handlerException", "originalException", "thrownException", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class CoroutineExceptionHandlerKt
{
    public static final CoroutineExceptionHandler CoroutineExceptionHandler(final Function2<? super CoroutineContext, ? super Throwable, Unit> function2) {
        Intrinsics.checkParameterIsNotNull(function2, "handler");
        return (CoroutineExceptionHandler)new CoroutineExceptionHandlerKt$CoroutineExceptionHandler.CoroutineExceptionHandlerKt$CoroutineExceptionHandler$1((Function2)function2, (CoroutineContext.Key)CoroutineExceptionHandler.Key);
    }
    
    public static final void handleCoroutineException(final CoroutineContext coroutineContext, final Throwable t) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        Intrinsics.checkParameterIsNotNull(t, "exception");
        try {
            final CoroutineExceptionHandler coroutineExceptionHandler = coroutineContext.get((CoroutineContext.Key<CoroutineExceptionHandler>)CoroutineExceptionHandler.Key);
            if (coroutineExceptionHandler != null) {
                coroutineExceptionHandler.handleException(coroutineContext, t);
                return;
            }
            CoroutineExceptionHandlerImplKt.handleCoroutineExceptionImpl(coroutineContext, t);
        }
        finally {
            final Throwable t2;
            CoroutineExceptionHandlerImplKt.handleCoroutineExceptionImpl(coroutineContext, handlerException(t, t2));
        }
    }
    
    public static final Throwable handlerException(final Throwable t, Throwable cause) {
        Intrinsics.checkParameterIsNotNull(t, "originalException");
        Intrinsics.checkParameterIsNotNull(cause, "thrownException");
        if (t == cause) {
            return t;
        }
        cause = new RuntimeException("Exception while trying to handle coroutine exception", cause);
        ExceptionsKt__ExceptionsKt.addSuppressed(cause, t);
        return cause;
    }
}
