// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.RejectedExecutionException;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Intrinsics;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ExecutorService;
import kotlin.jvm.functions.Function0;
import java.util.concurrent.Executor;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0004\b\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J$\u0010\u0011\u001a\u0004\u0018\u0001H\u0012\"\u0004\b\u0000\u0010\u00122\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u0002H\u00120\u0014H\u0082\b¢\u0006\u0002\u0010\u0015J\b\u0010\u0016\u001a\u00020\u0017H\u0016J\b\u0010\u0018\u001a\u00020\u0019H\u0002J\b\u0010\u001a\u001a\u00020\u0019H\u0002J\u001c\u0010\u001b\u001a\u00020\u00172\u0006\u0010\u001c\u001a\u00020\u001d2\n\u0010\u0013\u001a\u00060\u001ej\u0002`\u001fH\u0016J\b\u0010 \u001a\u00020\u0006H\u0002J!\u0010!\u001a\u00020\u00102\n\u0010\"\u001a\u0006\u0012\u0002\b\u00030#2\u0006\u0010\u0005\u001a\u00020\u0019H\u0000¢\u0006\u0002\b$J\r\u0010%\u001a\u00020\u0017H\u0000¢\u0006\u0002\b&J\u0015\u0010'\u001a\u00020\u00172\u0006\u0010(\u001a\u00020)H\u0000¢\u0006\u0002\b*J\b\u0010+\u001a\u00020\u0004H\u0016J\r\u0010\u000f\u001a\u00020\u0017H\u0000¢\u0006\u0002\b,R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u00068VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\u00020\n8BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0010\u0010\r\u001a\u0004\u0018\u00010\u0006X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006-" }, d2 = { "Lkotlinx/coroutines/CommonPool;", "Lkotlinx/coroutines/ExecutorCoroutineDispatcher;", "()V", "DEFAULT_PARALLELISM_PROPERTY_NAME", "", "executor", "Ljava/util/concurrent/Executor;", "getExecutor", "()Ljava/util/concurrent/Executor;", "parallelism", "", "getParallelism", "()I", "pool", "requestedParallelism", "usePrivatePool", "", "Try", "T", "block", "Lkotlin/Function0;", "(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "close", "", "createPlainPool", "Ljava/util/concurrent/ExecutorService;", "createPool", "dispatch", "context", "Lkotlin/coroutines/CoroutineContext;", "Ljava/lang/Runnable;", "Lkotlinx/coroutines/Runnable;", "getOrCreatePoolSync", "isGoodCommonPool", "fjpClass", "Ljava/lang/Class;", "isGoodCommonPool$kotlinx_coroutines_core", "restore", "restore$kotlinx_coroutines_core", "shutdown", "timeout", "", "shutdown$kotlinx_coroutines_core", "toString", "usePrivatePool$kotlinx_coroutines_core", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class CommonPool extends ExecutorCoroutineDispatcher
{
    public static final String DEFAULT_PARALLELISM_PROPERTY_NAME = "kotlinx.coroutines.default.parallelism";
    public static final CommonPool INSTANCE;
    private static volatile Executor pool;
    private static final int requestedParallelism;
    private static boolean usePrivatePool;
    
    static {
        INSTANCE = new CommonPool();
        final CommonPool instance = CommonPool.INSTANCE;
        while (true) {
            try {
                String property = System.getProperty("kotlinx.coroutines.default.parallelism");
                while (true) {
                    int intValue;
                    if (property != null) {
                        final Integer intOrNull = StringsKt__StringNumberConversionsKt.toIntOrNull(property);
                        if (intOrNull == null || intOrNull < 1) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Expected positive number in kotlinx.coroutines.default.parallelism, but has ");
                            sb.append(property);
                            throw new IllegalStateException(sb.toString().toString());
                        }
                        intValue = intOrNull;
                    }
                    else {
                        intValue = -1;
                    }
                    requestedParallelism = intValue;
                    return;
                    property = null;
                    continue;
                }
            }
            finally {}
            continue;
        }
    }
    
    private CommonPool() {
    }
    
    private final <T> T Try(final Function0<? extends T> function0) {
        try {
            return (T)function0.invoke();
        }
        finally {
            return null;
        }
    }
    
    private final ExecutorService createPlainPool() {
        final ExecutorService fixedThreadPool = Executors.newFixedThreadPool(this.getParallelism(), (ThreadFactory)new CommonPool$createPlainPool.CommonPool$createPlainPool$1(new AtomicInteger()));
        Intrinsics.checkExpressionValueIsNotNull(fixedThreadPool, "Executors.newFixedThread\u2026Daemon = true }\n        }");
        return fixedThreadPool;
    }
    
    private final ExecutorService createPool() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: ifnull          11
        //     6: aload_0        
        //     7: invokespecial   kotlinx/coroutines/CommonPool.createPlainPool:()Ljava/util/concurrent/ExecutorService;
        //    10: areturn        
        //    11: aconst_null    
        //    12: astore          4
        //    14: ldc             "java.util.concurrent.ForkJoinPool"
        //    16: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //    19: astore_2       
        //    20: goto            25
        //    23: aconst_null    
        //    24: astore_2       
        //    25: aload_2        
        //    26: ifnull          177
        //    29: getstatic       kotlinx/coroutines/CommonPool.usePrivatePool:Z
        //    32: ifne            116
        //    35: getstatic       kotlinx/coroutines/CommonPool.requestedParallelism:I
        //    38: ifge            116
        //    41: aload_2        
        //    42: ldc             "commonPool"
        //    44: iconst_0       
        //    45: anewarray       Ljava/lang/Class;
        //    48: invokevirtual   java/lang/Class.getMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //    51: astore_1       
        //    52: aload_1        
        //    53: ifnull          197
        //    56: aload_1        
        //    57: aconst_null    
        //    58: iconst_0       
        //    59: anewarray       Ljava/lang/Object;
        //    62: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //    65: astore_1       
        //    66: goto            69
        //    69: aload_1        
        //    70: astore_3       
        //    71: aload_1        
        //    72: instanceof      Ljava/util/concurrent/ExecutorService;
        //    75: ifne            80
        //    78: aconst_null    
        //    79: astore_3       
        //    80: aload_3        
        //    81: checkcast       Ljava/util/concurrent/ExecutorService;
        //    84: astore_1       
        //    85: goto            90
        //    88: aconst_null    
        //    89: astore_1       
        //    90: aload_1        
        //    91: ifnull          116
        //    94: getstatic       kotlinx/coroutines/CommonPool.INSTANCE:Lkotlinx/coroutines/CommonPool;
        //    97: aload_2        
        //    98: aload_1        
        //    99: invokevirtual   kotlinx/coroutines/CommonPool.isGoodCommonPool$kotlinx_coroutines_core:(Ljava/lang/Class;Ljava/util/concurrent/ExecutorService;)Z
        //   102: ifeq            108
        //   105: goto            110
        //   108: aconst_null    
        //   109: astore_1       
        //   110: aload_1        
        //   111: ifnull          116
        //   114: aload_1        
        //   115: areturn        
        //   116: aload_2        
        //   117: iconst_1       
        //   118: anewarray       Ljava/lang/Class;
        //   121: dup            
        //   122: iconst_0       
        //   123: getstatic       java/lang/Integer.TYPE:Ljava/lang/Class;
        //   126: aastore        
        //   127: invokevirtual   java/lang/Class.getConstructor:([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
        //   130: iconst_1       
        //   131: anewarray       Ljava/lang/Object;
        //   134: dup            
        //   135: iconst_0       
        //   136: getstatic       kotlinx/coroutines/CommonPool.INSTANCE:Lkotlinx/coroutines/CommonPool;
        //   139: invokespecial   kotlinx/coroutines/CommonPool.getParallelism:()I
        //   142: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //   145: aastore        
        //   146: invokevirtual   java/lang/reflect/Constructor.newInstance:([Ljava/lang/Object;)Ljava/lang/Object;
        //   149: astore_2       
        //   150: aload_2        
        //   151: astore_1       
        //   152: aload_2        
        //   153: instanceof      Ljava/util/concurrent/ExecutorService;
        //   156: ifne            161
        //   159: aconst_null    
        //   160: astore_1       
        //   161: aload_1        
        //   162: checkcast       Ljava/util/concurrent/ExecutorService;
        //   165: astore_1       
        //   166: aload_1        
        //   167: ifnull          172
        //   170: aload_1        
        //   171: areturn        
        //   172: aload_0        
        //   173: invokespecial   kotlinx/coroutines/CommonPool.createPlainPool:()Ljava/util/concurrent/ExecutorService;
        //   176: areturn        
        //   177: aload_0        
        //   178: invokespecial   kotlinx/coroutines/CommonPool.createPlainPool:()Ljava/util/concurrent/ExecutorService;
        //   181: areturn        
        //   182: astore_1       
        //   183: goto            23
        //   186: astore_1       
        //   187: goto            88
        //   190: astore_1       
        //   191: aload           4
        //   193: astore_1       
        //   194: goto            166
        //   197: aconst_null    
        //   198: astore_1       
        //   199: goto            69
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  14     20     182    25     Any
        //  41     52     186    90     Any
        //  56     66     186    90     Any
        //  71     78     186    90     Any
        //  80     85     186    90     Any
        //  116    150    190    197    Any
        //  152    159    190    197    Any
        //  161    166    190    197    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 110 out of bounds for length 110
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:373)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:426)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private final Executor getOrCreatePoolSync() {
        synchronized (this) {
            Executor pool = CommonPool.pool;
            if (pool == null) {
                final ExecutorService pool2 = this.createPool();
                CommonPool.pool = pool2;
                pool = pool2;
            }
            return pool;
        }
    }
    
    private final int getParallelism() {
        Integer value = CommonPool.requestedParallelism;
        if (value.intValue() <= 0) {
            value = null;
        }
        if (value != null) {
            return value;
        }
        return RangesKt___RangesKt.coerceAtLeast(Runtime.getRuntime().availableProcessors() - 1, 1);
    }
    
    @Override
    public void close() {
        throw new IllegalStateException("Close cannot be invoked on CommonPool".toString());
    }
    
    @Override
    public void dispatch(final CoroutineContext coroutineContext, final Runnable runnable) {
        while (true) {
            Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
            Intrinsics.checkParameterIsNotNull(runnable, "block");
            while (true) {
                Label_0085: {
                    while (true) {
                        try {
                            Executor executor = CommonPool.pool;
                            if (executor == null) {
                                executor = this.getOrCreatePoolSync();
                            }
                            final TimeSource timeSource = TimeSourceKt.getTimeSource();
                            if (timeSource == null) {
                                break Label_0085;
                            }
                            final Runnable wrapTask = timeSource.wrapTask(runnable);
                            if (wrapTask != null) {
                                executor.execute(wrapTask);
                                return;
                            }
                            break Label_0085;
                            final TimeSource timeSource2 = TimeSourceKt.getTimeSource();
                            // iftrue(Label_0073:, timeSource2 == null)
                            timeSource2.unTrackTask();
                            Label_0073: {
                                DefaultExecutor.INSTANCE.enqueue(runnable);
                            }
                            return;
                        }
                        catch (RejectedExecutionException ex) {
                            continue;
                        }
                        break;
                    }
                }
                final Runnable wrapTask = runnable;
                continue;
            }
        }
    }
    
    @Override
    public Executor getExecutor() {
        final Executor pool = CommonPool.pool;
        if (pool != null) {
            return pool;
        }
        return this.getOrCreatePoolSync();
    }
    
    public final boolean isGoodCommonPool$kotlinx_coroutines_core(final Class<?> clazz, final ExecutorService obj) {
        Intrinsics.checkParameterIsNotNull(clazz, "fjpClass");
        Intrinsics.checkParameterIsNotNull(obj, "executor");
        obj.submit((Runnable)CommonPool$isGoodCommonPool.CommonPool$isGoodCommonPool$1.INSTANCE);
        while (true) {
            try {
                Object invoke;
                if (!((invoke = clazz.getMethod("getPoolSize", (Class[])new Class[0]).invoke(obj, new Object[0])) instanceof Integer)) {
                    invoke = null;
                }
                Integer n = (Integer)invoke;
                return n != null && n >= 1;
                n = null;
                return n != null && n >= 1;
            }
            finally {}
            continue;
        }
    }
    
    public final void restore$kotlinx_coroutines_core() {
        synchronized (this) {
            this.shutdown$kotlinx_coroutines_core(0L);
            CommonPool.usePrivatePool = false;
            CommonPool.pool = null;
        }
    }
    
    public final void shutdown$kotlinx_coroutines_core(final long n) {
        synchronized (this) {
            Executor pool;
            if (!((pool = CommonPool.pool) instanceof ExecutorService)) {
                pool = null;
            }
            final ExecutorService executorService = (ExecutorService)pool;
            if (executorService != null) {
                executorService.shutdown();
                if (n > 0L) {
                    executorService.awaitTermination(n, TimeUnit.MILLISECONDS);
                }
                final List<Runnable> shutdownNow = executorService.shutdownNow();
                Intrinsics.checkExpressionValueIsNotNull(shutdownNow, "shutdownNow()");
                for (final Runnable runnable : shutdownNow) {
                    final DefaultExecutor instance = DefaultExecutor.INSTANCE;
                    Intrinsics.checkExpressionValueIsNotNull(runnable, "it");
                    instance.enqueue(runnable);
                }
            }
            CommonPool.pool = (Executor)CommonPool$shutdown.CommonPool$shutdown$2.INSTANCE;
        }
    }
    
    @Override
    public String toString() {
        return "CommonPool";
    }
    
    public final void usePrivatePool$kotlinx_coroutines_core() {
        synchronized (this) {
            this.shutdown$kotlinx_coroutines_core(0L);
            CommonPool.usePrivatePool = true;
            CommonPool.pool = null;
        }
    }
}
