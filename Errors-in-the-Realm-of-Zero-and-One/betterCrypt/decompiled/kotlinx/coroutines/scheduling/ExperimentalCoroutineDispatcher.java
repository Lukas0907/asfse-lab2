// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.scheduling;

import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import kotlinx.coroutines.DefaultExecutor;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineDispatcher;
import kotlin.jvm.internal.Intrinsics;
import kotlin.DeprecationLevel;
import kotlin.Deprecated;
import kotlin.Metadata;
import kotlinx.coroutines.ExecutorCoroutineDispatcher;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\b\u0017\u0018\u00002\u00020\u0001B%\b\u0016\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007B\u001b\b\u0017\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\bB'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\n\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u000bJ\u0010\u0010\u0012\u001a\u00020\u00132\b\b\u0002\u0010\u0014\u001a\u00020\u0003J\b\u0010\u0015\u001a\u00020\u0016H\u0016J\b\u0010\u0017\u001a\u00020\rH\u0002J\u001c\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u001a2\n\u0010\u001b\u001a\u00060\u001cj\u0002`\u001dH\u0016J)\u0010\u001e\u001a\u00020\u00162\n\u0010\u001b\u001a\u00060\u001cj\u0002`\u001d2\u0006\u0010\u0019\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!H\u0000¢\u0006\u0002\b\"J\u001c\u0010#\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u001a2\n\u0010\u001b\u001a\u00060\u001cj\u0002`\u001dH\u0016J\u000e\u0010$\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0003J\r\u0010%\u001a\u00020\u0016H\u0000¢\u0006\u0002\b&J\u0015\u0010'\u001a\u00020\u00162\u0006\u0010(\u001a\u00020\nH\u0000¢\u0006\u0002\b)J\b\u0010*\u001a\u00020\u0006H\u0016J\r\u0010+\u001a\u00020\u0016H\u0000¢\u0006\u0002\b,R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u00020\u000f8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006-" }, d2 = { "Lkotlinx/coroutines/scheduling/ExperimentalCoroutineDispatcher;", "Lkotlinx/coroutines/ExecutorCoroutineDispatcher;", "corePoolSize", "", "maxPoolSize", "schedulerName", "", "(IILjava/lang/String;)V", "(II)V", "idleWorkerKeepAliveNs", "", "(IIJLjava/lang/String;)V", "coroutineScheduler", "Lkotlinx/coroutines/scheduling/CoroutineScheduler;", "executor", "Ljava/util/concurrent/Executor;", "getExecutor", "()Ljava/util/concurrent/Executor;", "blocking", "Lkotlinx/coroutines/CoroutineDispatcher;", "parallelism", "close", "", "createScheduler", "dispatch", "context", "Lkotlin/coroutines/CoroutineContext;", "block", "Ljava/lang/Runnable;", "Lkotlinx/coroutines/Runnable;", "dispatchWithContext", "Lkotlinx/coroutines/scheduling/TaskContext;", "fair", "", "dispatchWithContext$kotlinx_coroutines_core", "dispatchYield", "limited", "restore", "restore$kotlinx_coroutines_core", "shutdown", "timeout", "shutdown$kotlinx_coroutines_core", "toString", "usePrivateScheduler", "usePrivateScheduler$kotlinx_coroutines_core", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public class ExperimentalCoroutineDispatcher extends ExecutorCoroutineDispatcher
{
    private final int corePoolSize;
    private CoroutineScheduler coroutineScheduler;
    private final long idleWorkerKeepAliveNs;
    private final int maxPoolSize;
    private final String schedulerName;
    
    public ExperimentalCoroutineDispatcher(final int corePoolSize, final int maxPoolSize, final long idleWorkerKeepAliveNs, final String schedulerName) {
        Intrinsics.checkParameterIsNotNull(schedulerName, "schedulerName");
        this.corePoolSize = corePoolSize;
        this.maxPoolSize = maxPoolSize;
        this.idleWorkerKeepAliveNs = idleWorkerKeepAliveNs;
        this.schedulerName = schedulerName;
        this.coroutineScheduler = this.createScheduler();
    }
    
    public ExperimentalCoroutineDispatcher(final int n, final int n2, final String s) {
        Intrinsics.checkParameterIsNotNull(s, "schedulerName");
        this(n, n2, TasksKt.IDLE_WORKER_KEEP_ALIVE_NS, s);
    }
    
    private final CoroutineScheduler createScheduler() {
        return new CoroutineScheduler(this.corePoolSize, this.maxPoolSize, this.idleWorkerKeepAliveNs, this.schedulerName);
    }
    
    public final CoroutineDispatcher blocking(final int i) {
        if (i > 0) {
            return new LimitingDispatcher(this, i, TaskMode.PROBABLY_BLOCKING);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected positive parallelism level, but have ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    @Override
    public void close() {
        this.coroutineScheduler.close();
    }
    
    @Override
    public void dispatch(final CoroutineContext coroutineContext, final Runnable runnable) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        Intrinsics.checkParameterIsNotNull(runnable, "block");
        while (true) {
            try {
                CoroutineScheduler.dispatch$default(this.coroutineScheduler, runnable, null, false, 6, null);
                return;
                DefaultExecutor.INSTANCE.dispatch(coroutineContext, runnable);
            }
            catch (RejectedExecutionException ex) {
                continue;
            }
            break;
        }
    }
    
    public final void dispatchWithContext$kotlinx_coroutines_core(final Runnable runnable, final TaskContext taskContext, final boolean b) {
        Intrinsics.checkParameterIsNotNull(runnable, "block");
        Intrinsics.checkParameterIsNotNull(taskContext, "context");
        while (true) {
            try {
                this.coroutineScheduler.dispatch(runnable, taskContext, b);
                return;
                DefaultExecutor.INSTANCE.enqueue(this.coroutineScheduler.createTask$kotlinx_coroutines_core(runnable, taskContext));
            }
            catch (RejectedExecutionException ex) {
                continue;
            }
            break;
        }
    }
    
    @Override
    public void dispatchYield(final CoroutineContext coroutineContext, final Runnable runnable) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        Intrinsics.checkParameterIsNotNull(runnable, "block");
        while (true) {
            try {
                CoroutineScheduler.dispatch$default(this.coroutineScheduler, runnable, null, true, 2, null);
                return;
                DefaultExecutor.INSTANCE.dispatchYield(coroutineContext, runnable);
            }
            catch (RejectedExecutionException ex) {
                continue;
            }
            break;
        }
    }
    
    @Override
    public Executor getExecutor() {
        return this.coroutineScheduler;
    }
    
    public final CoroutineDispatcher limited(final int n) {
        final int n2 = 1;
        if (n <= 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Expected positive parallelism level, but have ");
            sb.append(n);
            throw new IllegalArgumentException(sb.toString().toString());
        }
        int n3;
        if (n <= this.corePoolSize) {
            n3 = n2;
        }
        else {
            n3 = 0;
        }
        if (n3 != 0) {
            return new LimitingDispatcher(this, n, TaskMode.NON_BLOCKING);
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Expected parallelism level lesser than core pool size (");
        sb2.append(this.corePoolSize);
        sb2.append("), but have ");
        sb2.append(n);
        throw new IllegalArgumentException(sb2.toString().toString());
    }
    
    public final void restore$kotlinx_coroutines_core() {
        this.usePrivateScheduler$kotlinx_coroutines_core();
    }
    
    public final void shutdown$kotlinx_coroutines_core(final long n) {
        synchronized (this) {
            this.coroutineScheduler.shutdown(n);
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("[scheduler = ");
        sb.append(this.coroutineScheduler);
        sb.append(']');
        return sb.toString();
    }
    
    public final void usePrivateScheduler$kotlinx_coroutines_core() {
        synchronized (this) {
            this.coroutineScheduler.shutdown(1000L);
            this.coroutineScheduler = this.createScheduler();
        }
    }
}
