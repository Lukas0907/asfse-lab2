// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.scheduling;

import kotlinx.coroutines.internal.LockFreeTaskQueueCore;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import kotlinx.coroutines.internal.LockFreeTaskQueue;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b\u0010\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007" }, d2 = { "Lkotlinx/coroutines/scheduling/GlobalQueue;", "Lkotlinx/coroutines/internal/LockFreeTaskQueue;", "Lkotlinx/coroutines/scheduling/Task;", "()V", "removeFirstWithModeOrNull", "mode", "Lkotlinx/coroutines/scheduling/TaskMode;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public class GlobalQueue extends LockFreeTaskQueue<Task>
{
    public GlobalQueue() {
        super(false);
    }
    
    public final Task removeFirstWithModeOrNull(final TaskMode taskMode) {
        Intrinsics.checkParameterIsNotNull(taskMode, "mode");
        Object remove_FROZEN = null;
        while (true) {
            final LockFreeTaskQueueCore lockFreeTaskQueueCore = (LockFreeTaskQueueCore)super._cur$internal;
            Label_0239: {
                Object value = null;
            Label_0235:
                while (true) {
                    final long state$internal = lockFreeTaskQueueCore._state$internal;
                    remove_FROZEN = null;
                    if ((0x1000000000000000L & state$internal) != 0x0L) {
                        remove_FROZEN = LockFreeTaskQueueCore.REMOVE_FROZEN;
                        break Label_0239;
                    }
                    final LockFreeTaskQueueCore.Companion companion = LockFreeTaskQueueCore.Companion;
                    boolean b = false;
                    final int n = (int)((0x3FFFFFFFL & state$internal) >> 0);
                    if (((int)((0xFFFFFFFC0000000L & state$internal) >> 30) & LockFreeTaskQueueCore.access$getMask$p(lockFreeTaskQueueCore)) == (LockFreeTaskQueueCore.access$getMask$p(lockFreeTaskQueueCore) & n)) {
                        break Label_0239;
                    }
                    value = lockFreeTaskQueueCore.array$internal.get(LockFreeTaskQueueCore.access$getMask$p(lockFreeTaskQueueCore) & n);
                    if (value == null) {
                        if (LockFreeTaskQueueCore.access$getSingleConsumer$p(lockFreeTaskQueueCore)) {
                            break Label_0239;
                        }
                        continue;
                    }
                    else {
                        if (value instanceof LockFreeTaskQueueCore.Placeholder) {
                            break Label_0239;
                        }
                        if (((Task)value).getMode() == taskMode) {
                            b = true;
                        }
                        if (!b) {
                            break Label_0239;
                        }
                        final int n2 = n + 1 & 0x3FFFFFFF;
                        if (LockFreeTaskQueueCore._state$FU$internal.compareAndSet(lockFreeTaskQueueCore, state$internal, LockFreeTaskQueueCore.Companion.updateHead(state$internal, n2))) {
                            lockFreeTaskQueueCore.array$internal.set(LockFreeTaskQueueCore.access$getMask$p(lockFreeTaskQueueCore) & n, null);
                            break;
                        }
                        if (!LockFreeTaskQueueCore.access$getSingleConsumer$p(lockFreeTaskQueueCore)) {
                            continue;
                        }
                        LockFreeTaskQueueCore<Object> access$removeSlowPath = (LockFreeTaskQueueCore<Object>)lockFreeTaskQueueCore;
                        while (true) {
                            access$removeSlowPath = LockFreeTaskQueueCore.access$removeSlowPath(access$removeSlowPath, n, n2);
                            if (access$removeSlowPath != null) {
                                continue;
                            }
                            break Label_0235;
                        }
                    }
                }
                remove_FROZEN = value;
            }
            if (remove_FROZEN != LockFreeTaskQueueCore.REMOVE_FROZEN) {
                break;
            }
            LockFreeTaskQueue._cur$FU$internal.compareAndSet(this, lockFreeTaskQueueCore, lockFreeTaskQueueCore.next());
        }
        return (Task)remove_FROZEN;
    }
}
