// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.scheduling;

import kotlin.Unit;
import kotlin.jvm.JvmStatic;
import kotlinx.coroutines.DebugStringsKt;
import java.util.ArrayList;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.locks.LockSupport;
import kotlinx.coroutines.TimeSource;
import kotlinx.coroutines.TimeSourceKt;
import kotlinx.coroutines.DebugKt;
import kotlin.jvm.internal.Intrinsics;
import java.util.concurrent.TimeUnit;
import kotlinx.coroutines.internal.SystemPropsKt;
import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import kotlinx.coroutines.internal.Symbol;
import kotlin.Metadata;
import java.io.Closeable;
import java.util.concurrent.Executor;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b!\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\b\u0006\b\u0000\u0018\u0000 U2\u00020\u00012\u00020\u0002:\u0003UVWB+\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0006\u0012\b\b\u0002\u0010\t\u001a\u00020\b¢\u0006\u0004\b\n\u0010\u000bJ\u0018\u0010\r\u001a\u00020\u00032\u0006\u0010\f\u001a\u00020\u0006H\u0082\b¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u0010\u001a\u00020\u000fH\u0016¢\u0006\u0004\b\u0010\u0010\u0011J\u000f\u0010\u0012\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0012\u0010\u0013J#\u0010\u001c\u001a\u00020\u00192\n\u0010\u0016\u001a\u00060\u0014j\u0002`\u00152\u0006\u0010\u0018\u001a\u00020\u0017H\u0000¢\u0006\u0004\b\u001a\u0010\u001bJ\u0018\u0010\u001d\u001a\u00020\u00032\u0006\u0010\f\u001a\u00020\u0006H\u0082\b¢\u0006\u0004\b\u001d\u0010\u000eJ\u0015\u0010\u001f\u001a\b\u0018\u00010\u001eR\u00020\u0000H\u0002¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010!\u001a\u00020\u000fH\u0082\b¢\u0006\u0004\b!\u0010\u0011J\u0010\u0010\"\u001a\u00020\u0003H\u0082\b¢\u0006\u0004\b\"\u0010\u0013J-\u0010%\u001a\u00020\u000f2\n\u0010\u0016\u001a\u00060\u0014j\u0002`\u00152\b\b\u0002\u0010\u0018\u001a\u00020\u00172\b\b\u0002\u0010$\u001a\u00020#¢\u0006\u0004\b%\u0010&J\u001b\u0010(\u001a\u00020\u000f2\n\u0010'\u001a\u00060\u0014j\u0002`\u0015H\u0016¢\u0006\u0004\b(\u0010)J\u0010\u0010*\u001a\u00020\u000fH\u0082\b¢\u0006\u0004\b*\u0010\u0011J\u0010\u0010+\u001a\u00020\u0003H\u0082\b¢\u0006\u0004\b+\u0010\u0013J\u001b\u0010-\u001a\u00020\u00032\n\u0010,\u001a\u00060\u001eR\u00020\u0000H\u0002¢\u0006\u0004\b-\u0010.J\u0015\u0010/\u001a\b\u0018\u00010\u001eR\u00020\u0000H\u0002¢\u0006\u0004\b/\u0010 J\u001b\u00100\u001a\u00020\u000f2\n\u0010,\u001a\u00060\u001eR\u00020\u0000H\u0002¢\u0006\u0004\b0\u00101J+\u00104\u001a\u00020\u000f2\n\u0010,\u001a\u00060\u001eR\u00020\u00002\u0006\u00102\u001a\u00020\u00032\u0006\u00103\u001a\u00020\u0003H\u0002¢\u0006\u0004\b4\u00105J\u000f\u00106\u001a\u00020\u000fH\u0002¢\u0006\u0004\b6\u0010\u0011J\u0017\u00108\u001a\u00020\u000f2\u0006\u00107\u001a\u00020\u0019H\u0002¢\u0006\u0004\b8\u00109J\u0015\u0010;\u001a\u00020\u000f2\u0006\u0010:\u001a\u00020\u0006¢\u0006\u0004\b;\u0010<J\u001f\u0010=\u001a\u00020\u00032\u0006\u00107\u001a\u00020\u00192\u0006\u0010$\u001a\u00020#H\u0002¢\u0006\u0004\b=\u0010>J\u000f\u0010?\u001a\u00020\bH\u0016¢\u0006\u0004\b?\u0010@J\u000f\u0010A\u001a\u00020#H\u0002¢\u0006\u0004\bA\u0010BR\u0017\u0010\r\u001a\u00020\u00038\u00c2\u0002@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\bC\u0010\u0013R\u0016\u0010\u0004\u001a\u00020\u00038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0004\u0010DR\u0016\u0010F\u001a\u00020E8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bF\u0010GR\u0017\u0010\u001d\u001a\u00020\u00038\u00c2\u0002@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\bH\u0010\u0013R\u0016\u0010J\u001a\u00020I8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bJ\u0010KR\u0016\u0010\u0007\u001a\u00020\u00068\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0007\u0010LR\u0016\u0010M\u001a\u00020#8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\bM\u0010BR\u0016\u0010\u0005\u001a\u00020\u00038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010DR\u0016\u0010O\u001a\u00020N8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bO\u0010PR\u0016\u0010\t\u001a\u00020\b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\t\u0010QR\"\u0010S\u001a\u000e\u0012\n\u0012\b\u0018\u00010\u001eR\u00020\u00000R8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bS\u0010T¨\u0006X" }, d2 = { "Lkotlinx/coroutines/scheduling/CoroutineScheduler;", "Ljava/util/concurrent/Executor;", "Ljava/io/Closeable;", "", "corePoolSize", "maxPoolSize", "", "idleWorkerKeepAliveNs", "", "schedulerName", "<init>", "(IIJLjava/lang/String;)V", "state", "blockingWorkers", "(J)I", "", "close", "()V", "createNewWorker", "()I", "Ljava/lang/Runnable;", "Lkotlinx/coroutines/Runnable;", "block", "Lkotlinx/coroutines/scheduling/TaskContext;", "taskContext", "Lkotlinx/coroutines/scheduling/Task;", "createTask$kotlinx_coroutines_core", "(Ljava/lang/Runnable;Lkotlinx/coroutines/scheduling/TaskContext;)Lkotlinx/coroutines/scheduling/Task;", "createTask", "createdWorkers", "Lkotlinx/coroutines/scheduling/CoroutineScheduler$Worker;", "currentWorker", "()Lkotlinx/coroutines/scheduling/CoroutineScheduler$Worker;", "decrementBlockingWorkers", "decrementCreatedWorkers", "", "fair", "dispatch", "(Ljava/lang/Runnable;Lkotlinx/coroutines/scheduling/TaskContext;Z)V", "command", "execute", "(Ljava/lang/Runnable;)V", "incrementBlockingWorkers", "incrementCreatedWorkers", "worker", "parkedWorkersStackNextIndex", "(Lkotlinx/coroutines/scheduling/CoroutineScheduler$Worker;)I", "parkedWorkersStackPop", "parkedWorkersStackPush", "(Lkotlinx/coroutines/scheduling/CoroutineScheduler$Worker;)V", "oldIndex", "newIndex", "parkedWorkersStackTopUpdate", "(Lkotlinx/coroutines/scheduling/CoroutineScheduler$Worker;II)V", "requestCpuWorker", "task", "runSafely", "(Lkotlinx/coroutines/scheduling/Task;)V", "timeout", "shutdown", "(J)V", "submitToLocalQueue", "(Lkotlinx/coroutines/scheduling/Task;Z)I", "toString", "()Ljava/lang/String;", "tryUnpark", "()Z", "getBlockingWorkers", "I", "Ljava/util/concurrent/Semaphore;", "cpuPermits", "Ljava/util/concurrent/Semaphore;", "getCreatedWorkers", "Lkotlinx/coroutines/scheduling/GlobalQueue;", "globalQueue", "Lkotlinx/coroutines/scheduling/GlobalQueue;", "J", "isTerminated", "Ljava/util/Random;", "random", "Ljava/util/Random;", "Ljava/lang/String;", "", "workers", "[Lkotlinx/coroutines/scheduling/CoroutineScheduler$Worker;", "Companion", "Worker", "WorkerState", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class CoroutineScheduler implements Executor, Closeable
{
    private static final int ADDED = -1;
    private static final int ADDED_REQUIRES_HELP = 0;
    private static final int ALLOWED = 0;
    private static final long BLOCKING_MASK = 4398044413952L;
    private static final int BLOCKING_SHIFT = 21;
    private static final long CREATED_MASK = 2097151L;
    public static final Companion Companion;
    private static final int FORBIDDEN = -1;
    private static final int MAX_PARK_TIME_NS;
    private static final int MAX_SPINS;
    public static final int MAX_SUPPORTED_POOL_SIZE = 2097150;
    private static final int MAX_YIELDS;
    private static final int MIN_PARK_TIME_NS;
    public static final int MIN_SUPPORTED_POOL_SIZE = 1;
    private static final int NOT_ADDED = 1;
    private static final Symbol NOT_IN_STACK;
    private static final long PARKED_INDEX_MASK = 2097151L;
    private static final long PARKED_VERSION_INC = 2097152L;
    private static final long PARKED_VERSION_MASK = -2097152L;
    private static final int TERMINATED = 1;
    private static final AtomicIntegerFieldUpdater _isTerminated$FU;
    static final AtomicLongFieldUpdater controlState$FU;
    private static final AtomicLongFieldUpdater parkedWorkersStack$FU;
    private volatile int _isTerminated;
    volatile long controlState;
    private final int corePoolSize;
    private final Semaphore cpuPermits;
    private final GlobalQueue globalQueue;
    private final long idleWorkerKeepAliveNs;
    private final int maxPoolSize;
    private volatile long parkedWorkersStack;
    private final Random random;
    private final String schedulerName;
    private final Worker[] workers;
    
    static {
        Companion = new Companion(null);
        MAX_SPINS = SystemPropsKt.systemProp$default("kotlinx.coroutines.scheduler.spins", 1000, 1, 0, 8, null);
        MAX_YIELDS = CoroutineScheduler.MAX_SPINS + SystemPropsKt.systemProp$default("kotlinx.coroutines.scheduler.yields", 0, 0, 0, 8, null);
        MAX_PARK_TIME_NS = (int)TimeUnit.SECONDS.toNanos(1L);
        MIN_PARK_TIME_NS = (int)RangesKt___RangesKt.coerceAtMost(RangesKt___RangesKt.coerceAtLeast(TasksKt.WORK_STEALING_TIME_RESOLUTION_NS / 4, 10L), CoroutineScheduler.MAX_PARK_TIME_NS);
        NOT_IN_STACK = new Symbol("NOT_IN_STACK");
        parkedWorkersStack$FU = AtomicLongFieldUpdater.newUpdater(CoroutineScheduler.class, "parkedWorkersStack");
        controlState$FU = AtomicLongFieldUpdater.newUpdater(CoroutineScheduler.class, "controlState");
        _isTerminated$FU = AtomicIntegerFieldUpdater.newUpdater(CoroutineScheduler.class, "_isTerminated");
    }
    
    public CoroutineScheduler(int corePoolSize, final int maxPoolSize, final long idleWorkerKeepAliveNs, final String schedulerName) {
        Intrinsics.checkParameterIsNotNull(schedulerName, "schedulerName");
        this.corePoolSize = corePoolSize;
        this.maxPoolSize = maxPoolSize;
        this.idleWorkerKeepAliveNs = idleWorkerKeepAliveNs;
        this.schedulerName = schedulerName;
        if (this.corePoolSize >= 1) {
            corePoolSize = 1;
        }
        else {
            corePoolSize = 0;
        }
        if (corePoolSize == 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Core pool size ");
            sb.append(this.corePoolSize);
            sb.append(" should be at least 1");
            throw new IllegalArgumentException(sb.toString().toString());
        }
        if (this.maxPoolSize >= this.corePoolSize) {
            corePoolSize = 1;
        }
        else {
            corePoolSize = 0;
        }
        if (corePoolSize == 0) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Max pool size ");
            sb2.append(this.maxPoolSize);
            sb2.append(" should be greater than or equals to core pool size ");
            sb2.append(this.corePoolSize);
            throw new IllegalArgumentException(sb2.toString().toString());
        }
        if (this.maxPoolSize <= 2097150) {
            corePoolSize = 1;
        }
        else {
            corePoolSize = 0;
        }
        if (corePoolSize == 0) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Max pool size ");
            sb3.append(this.maxPoolSize);
            sb3.append(" should not exceed maximal supported number of threads 2097150");
            throw new IllegalArgumentException(sb3.toString().toString());
        }
        if (this.idleWorkerKeepAliveNs > 0L) {
            corePoolSize = 1;
        }
        else {
            corePoolSize = 0;
        }
        if (corePoolSize != 0) {
            this.globalQueue = new GlobalQueue();
            this.cpuPermits = new Semaphore(this.corePoolSize, false);
            this.parkedWorkersStack = 0L;
            this.workers = new Worker[this.maxPoolSize + 1];
            this.controlState = 0L;
            this.random = new Random();
            this._isTerminated = 0;
            return;
        }
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("Idle worker keep alive time ");
        sb4.append(this.idleWorkerKeepAliveNs);
        sb4.append(" must be positive");
        throw new IllegalArgumentException(sb4.toString().toString());
    }
    
    public static final /* synthetic */ int access$getCorePoolSize$p(final CoroutineScheduler coroutineScheduler) {
        return coroutineScheduler.corePoolSize;
    }
    
    public static final /* synthetic */ Semaphore access$getCpuPermits$p(final CoroutineScheduler coroutineScheduler) {
        return coroutineScheduler.cpuPermits;
    }
    
    public static final /* synthetic */ GlobalQueue access$getGlobalQueue$p(final CoroutineScheduler coroutineScheduler) {
        return coroutineScheduler.globalQueue;
    }
    
    public static final /* synthetic */ long access$getIdleWorkerKeepAliveNs$p(final CoroutineScheduler coroutineScheduler) {
        return coroutineScheduler.idleWorkerKeepAliveNs;
    }
    
    public static final /* synthetic */ int access$getMAX_PARK_TIME_NS$cp() {
        return CoroutineScheduler.MAX_PARK_TIME_NS;
    }
    
    public static final /* synthetic */ int access$getMAX_SPINS$cp() {
        return CoroutineScheduler.MAX_SPINS;
    }
    
    public static final /* synthetic */ int access$getMAX_YIELDS$cp() {
        return CoroutineScheduler.MAX_YIELDS;
    }
    
    public static final /* synthetic */ int access$getMIN_PARK_TIME_NS$cp() {
        return CoroutineScheduler.MIN_PARK_TIME_NS;
    }
    
    public static final /* synthetic */ Symbol access$getNOT_IN_STACK$cp() {
        return CoroutineScheduler.NOT_IN_STACK;
    }
    
    public static final /* synthetic */ Random access$getRandom$p(final CoroutineScheduler coroutineScheduler) {
        return coroutineScheduler.random;
    }
    
    public static final /* synthetic */ String access$getSchedulerName$p(final CoroutineScheduler coroutineScheduler) {
        return coroutineScheduler.schedulerName;
    }
    
    public static final /* synthetic */ Worker[] access$getWorkers$p(final CoroutineScheduler coroutineScheduler) {
        return coroutineScheduler.workers;
    }
    
    private final int blockingWorkers(final long n) {
        return (int)((n & 0x3FFFFE00000L) >> 21);
    }
    
    private final int createNewWorker() {
        while (true) {
            while (true) {
                synchronized (this.workers) {
                    if (this.isTerminated()) {
                        return -1;
                    }
                    final long controlState = this.controlState;
                    final int n = (int)(controlState & 0x1FFFFFL);
                    final int n2 = n - (int)((controlState & 0x3FFFFE00000L) >> 21);
                    int corePoolSize = this.corePoolSize;
                    final int n3 = 0;
                    if (n2 >= corePoolSize) {
                        return 0;
                    }
                    if (n >= this.maxPoolSize || this.cpuPermits.availablePermits() == 0) {
                        return 0;
                    }
                    corePoolSize = (int)(this.controlState & 0x1FFFFFL) + 1;
                    if (corePoolSize > 0 && this.workers[corePoolSize] == null) {
                        final int n4 = 1;
                        if (n4 == 0) {
                            throw new IllegalArgumentException("Failed requirement.".toString());
                        }
                        final Worker worker = new Worker(corePoolSize);
                        worker.start();
                        int n5 = n3;
                        if (corePoolSize == (int)(0x1FFFFFL & CoroutineScheduler.controlState$FU.incrementAndGet(this))) {
                            n5 = 1;
                        }
                        if (n5 != 0) {
                            this.workers[corePoolSize] = worker;
                            // monitorexit(this.workers)
                            return n2 + 1;
                        }
                        throw new IllegalArgumentException("Failed requirement.".toString());
                    }
                }
                final int n4 = 0;
                continue;
            }
        }
    }
    
    private final int createdWorkers(final long n) {
        return (int)(n & 0x1FFFFFL);
    }
    
    private final Worker currentWorker() {
        Thread currentThread = Thread.currentThread();
        final boolean b = currentThread instanceof Worker;
        final Worker worker = null;
        if (!b) {
            currentThread = null;
        }
        final Worker worker2 = (Worker)currentThread;
        Worker worker3 = worker;
        if (worker2 != null) {
            worker3 = worker;
            if (Intrinsics.areEqual(worker2.getScheduler(), this)) {
                worker3 = worker2;
            }
        }
        return worker3;
    }
    
    private final void decrementBlockingWorkers() {
        CoroutineScheduler.controlState$FU.addAndGet(this, -2097152L);
    }
    
    private final int decrementCreatedWorkers() {
        return (int)(CoroutineScheduler.controlState$FU.getAndDecrement(this) & 0x1FFFFFL);
    }
    
    public static /* synthetic */ void dispatch$default(final CoroutineScheduler coroutineScheduler, final Runnable runnable, TaskContext taskContext, boolean b, final int n, final Object o) {
        if ((n & 0x2) != 0x0) {
            taskContext = NonBlockingContext.INSTANCE;
        }
        if ((n & 0x4) != 0x0) {
            b = false;
        }
        coroutineScheduler.dispatch(runnable, taskContext, b);
    }
    
    private final int getBlockingWorkers() {
        return (int)((this.controlState & 0x3FFFFE00000L) >> 21);
    }
    
    private final int getCreatedWorkers() {
        return (int)(this.controlState & 0x1FFFFFL);
    }
    
    private final void incrementBlockingWorkers() {
        CoroutineScheduler.controlState$FU.addAndGet(this, 2097152L);
    }
    
    private final int incrementCreatedWorkers() {
        return (int)(CoroutineScheduler.controlState$FU.incrementAndGet(this) & 0x1FFFFFL);
    }
    
    private final boolean isTerminated() {
        return this._isTerminated != 0;
    }
    
    private final int parkedWorkersStackNextIndex(Worker worker) {
        for (Object o = worker.getNextParkedWorker(); o != CoroutineScheduler.NOT_IN_STACK; o = worker.getNextParkedWorker()) {
            if (o == null) {
                return 0;
            }
            worker = (Worker)o;
            final int indexInArray = worker.getIndexInArray();
            if (indexInArray != 0) {
                return indexInArray;
            }
        }
        return -1;
    }
    
    private final Worker parkedWorkersStackPop() {
        while (true) {
            final long parkedWorkersStack = this.parkedWorkersStack;
            final Worker worker = this.workers[(int)(0x1FFFFFL & parkedWorkersStack)];
            if (worker == null) {
                return null;
            }
            final int parkedWorkersStackNextIndex = this.parkedWorkersStackNextIndex(worker);
            if (parkedWorkersStackNextIndex < 0) {
                continue;
            }
            if (CoroutineScheduler.parkedWorkersStack$FU.compareAndSet(this, parkedWorkersStack, (long)parkedWorkersStackNextIndex | (2097152L + parkedWorkersStack & 0xFFFFFFFFFFE00000L))) {
                worker.setNextParkedWorker(CoroutineScheduler.NOT_IN_STACK);
                return worker;
            }
        }
    }
    
    private final void parkedWorkersStackPush(final Worker worker) {
        if (worker.getNextParkedWorker() != CoroutineScheduler.NOT_IN_STACK) {
            return;
        }
        long parkedWorkersStack;
        int indexInArray;
        do {
            parkedWorkersStack = this.parkedWorkersStack;
            final int n = (int)(0x1FFFFFL & parkedWorkersStack);
            indexInArray = worker.getIndexInArray();
            if (DebugKt.getASSERTIONS_ENABLED() && indexInArray == 0) {
                throw new AssertionError();
            }
            worker.setNextParkedWorker(this.workers[n]);
        } while (!CoroutineScheduler.parkedWorkersStack$FU.compareAndSet(this, parkedWorkersStack, (long)indexInArray | (2097152L + parkedWorkersStack & 0xFFFFFFFFFFE00000L)));
    }
    
    private final void parkedWorkersStackTopUpdate(final Worker worker, final int n, final int n2) {
        while (true) {
            final long parkedWorkersStack = this.parkedWorkersStack;
            int parkedWorkersStackNextIndex;
            if ((parkedWorkersStackNextIndex = (int)(0x1FFFFFL & parkedWorkersStack)) == n) {
                if (n2 == 0) {
                    parkedWorkersStackNextIndex = this.parkedWorkersStackNextIndex(worker);
                }
                else {
                    parkedWorkersStackNextIndex = n2;
                }
            }
            if (parkedWorkersStackNextIndex < 0) {
                continue;
            }
            if (CoroutineScheduler.parkedWorkersStack$FU.compareAndSet(this, parkedWorkersStack, (2097152L + parkedWorkersStack & 0xFFFFFFFFFFE00000L) | (long)parkedWorkersStackNextIndex)) {
                break;
            }
        }
    }
    
    private final void requestCpuWorker() {
        if (this.cpuPermits.availablePermits() == 0) {
            this.tryUnpark();
            return;
        }
        if (this.tryUnpark()) {
            return;
        }
        final long controlState = this.controlState;
        if ((int)(0x1FFFFFL & controlState) - (int)((controlState & 0x3FFFFE00000L) >> 21) < this.corePoolSize) {
            final int newWorker = this.createNewWorker();
            if (newWorker == 1 && this.corePoolSize > 1) {
                this.createNewWorker();
            }
            if (newWorker > 0) {
                return;
            }
        }
        this.tryUnpark();
    }
    
    private final void runSafely(final Task task) {
        while (true) {
            try {
                task.run();
                final TimeSource timeSource = TimeSourceKt.getTimeSource();
                if (timeSource != null) {
                    timeSource.unTrackTask();
                }
            }
            finally {
                try {
                    final Thread currentThread = Thread.currentThread();
                    Intrinsics.checkExpressionValueIsNotNull(currentThread, "thread");
                    final Throwable t;
                    currentThread.getUncaughtExceptionHandler().uncaughtException(currentThread, t);
                    if (TimeSourceKt.getTimeSource() != null) {
                        continue;
                    }
                }
                finally {
                    final TimeSource timeSource2 = TimeSourceKt.getTimeSource();
                    if (timeSource2 != null) {
                        timeSource2.unTrackTask();
                    }
                }
            }
            break;
        }
    }
    
    private final int submitToLocalQueue(final Task task, final boolean b) {
        final Worker currentWorker = this.currentWorker();
        if (currentWorker == null) {
            return 1;
        }
        if (currentWorker.getState() == WorkerState.TERMINATED) {
            return 1;
        }
        int n = -1;
        if (task.getMode() == TaskMode.NON_BLOCKING) {
            if (currentWorker.isBlocking()) {
                n = 0;
            }
            else {
                n = n;
                if (!currentWorker.tryAcquireCpuPermit()) {
                    return 1;
                }
            }
        }
        boolean b2;
        if (b) {
            b2 = currentWorker.getLocalQueue().addLast(task, this.globalQueue);
        }
        else {
            b2 = currentWorker.getLocalQueue().add(task, this.globalQueue);
        }
        if (!b2) {
            return 0;
        }
        if (currentWorker.getLocalQueue().getBufferSize$kotlinx_coroutines_core() > TasksKt.QUEUE_SIZE_OFFLOAD_THRESHOLD) {
            return 0;
        }
        return n;
    }
    
    private final boolean tryUnpark() {
        while (true) {
            final Worker parkedWorkersStackPop = this.parkedWorkersStackPop();
            if (parkedWorkersStackPop == null) {
                return false;
            }
            parkedWorkersStackPop.idleResetBeforeUnpark();
            final boolean parking = parkedWorkersStackPop.isParking();
            LockSupport.unpark(parkedWorkersStackPop);
            if (!parking) {
                continue;
            }
            if (!parkedWorkersStackPop.tryForbidTermination()) {
                continue;
            }
            return true;
        }
    }
    
    @Override
    public void close() {
        this.shutdown(10000L);
    }
    
    public final Task createTask$kotlinx_coroutines_core(final Runnable runnable, final TaskContext taskContext) {
        Intrinsics.checkParameterIsNotNull(runnable, "block");
        Intrinsics.checkParameterIsNotNull(taskContext, "taskContext");
        final long nanoTime = TasksKt.schedulerTimeSource.nanoTime();
        if (runnable instanceof Task) {
            final Task task = (Task)runnable;
            task.submissionTime = nanoTime;
            task.taskContext = taskContext;
            return task;
        }
        return new TaskImpl(runnable, nanoTime, taskContext);
    }
    
    public final void dispatch(final Runnable runnable, final TaskContext taskContext, final boolean b) {
        Intrinsics.checkParameterIsNotNull(runnable, "block");
        Intrinsics.checkParameterIsNotNull(taskContext, "taskContext");
        final TimeSource timeSource = TimeSourceKt.getTimeSource();
        if (timeSource != null) {
            timeSource.trackTask();
        }
        final Task task$kotlinx_coroutines_core = this.createTask$kotlinx_coroutines_core(runnable, taskContext);
        final int submitToLocalQueue = this.submitToLocalQueue(task$kotlinx_coroutines_core, b);
        if (submitToLocalQueue == -1) {
            return;
        }
        if (submitToLocalQueue != 1) {
            this.requestCpuWorker();
            return;
        }
        if (this.globalQueue.addLast(task$kotlinx_coroutines_core)) {
            this.requestCpuWorker();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(this.schedulerName);
        sb.append(" was terminated");
        throw new RejectedExecutionException(sb.toString());
    }
    
    @Override
    public void execute(final Runnable runnable) {
        Intrinsics.checkParameterIsNotNull(runnable, "command");
        dispatch$default(this, runnable, null, false, 6, null);
    }
    
    public final void shutdown(final long millis) {
        final AtomicIntegerFieldUpdater isTerminated$FU = CoroutineScheduler._isTerminated$FU;
        final int n = 0;
        if (!isTerminated$FU.compareAndSet(this, 0, 1)) {
            return;
        }
        final Worker currentWorker = this.currentWorker();
        Object o = this.workers;
        synchronized (o) {
            final int n2 = (int)(this.controlState & 0x1FFFFFL);
            // monitorexit(o)
            if (1 <= n2) {
                int n3 = 1;
                while (true) {
                    o = this.workers[n3];
                    if (o == null) {
                        Intrinsics.throwNpe();
                    }
                    if (o != currentWorker) {
                        while (((Thread)o).isAlive()) {
                            LockSupport.unpark((Thread)o);
                            ((Thread)o).join(millis);
                        }
                        final WorkerState state = ((Worker)o).getState();
                        if (DebugKt.getASSERTIONS_ENABLED() && state != WorkerState.TERMINATED) {
                            throw new AssertionError();
                        }
                        ((Worker)o).getLocalQueue().offloadAllWork$kotlinx_coroutines_core(this.globalQueue);
                    }
                    if (n3 == n2) {
                        break;
                    }
                    ++n3;
                }
            }
            this.globalQueue.close();
            while (true) {
                Label_0222: {
                    if (currentWorker != null) {
                        o = currentWorker.findTask$kotlinx_coroutines_core();
                        if (o != null) {
                            break Label_0222;
                        }
                    }
                    o = this.globalQueue.removeFirstOrNull();
                }
                if (o == null) {
                    break;
                }
                this.runSafely((Task)o);
            }
            if (currentWorker != null) {
                currentWorker.tryReleaseCpu$kotlinx_coroutines_core(WorkerState.TERMINATED);
            }
            if (DebugKt.getASSERTIONS_ENABLED()) {
                int n4 = n;
                if (this.cpuPermits.availablePermits() == this.corePoolSize) {
                    n4 = 1;
                }
                if (n4 == 0) {
                    throw new AssertionError();
                }
            }
            this.parkedWorkersStack = 0L;
            this.controlState = 0L;
        }
    }
    
    @Override
    public String toString() {
        final ArrayList<String> obj = new ArrayList<String>();
        final Worker[] workers = this.workers;
        final int length = workers.length;
        int i = 0;
        int j = 0;
        int l;
        int k = l = j;
        int i2;
        int m = i2 = l;
        while (i < length) {
            final Worker worker = workers[i];
            int n;
            int n2;
            int n3;
            int n4;
            if (worker == null) {
                n = j;
                n2 = k;
                n3 = l;
                n4 = i2;
            }
            else {
                final int size$kotlinx_coroutines_core = worker.getLocalQueue().size$kotlinx_coroutines_core();
                final int n5 = CoroutineScheduler$WhenMappings.$EnumSwitchMapping$0[worker.getState().ordinal()];
                if (n5 != 1) {
                    if (n5 != 2) {
                        if (n5 != 3) {
                            if (n5 != 4) {
                                if (n5 != 5) {
                                    n = j;
                                    n2 = k;
                                    n3 = l;
                                    n4 = i2;
                                }
                                else {
                                    n4 = i2 + 1;
                                    n = j;
                                    n2 = k;
                                    n3 = l;
                                }
                            }
                            else {
                                final int n6 = m + 1;
                                n = j;
                                n2 = k;
                                n3 = l;
                                m = n6;
                                n4 = i2;
                                if (size$kotlinx_coroutines_core > 0) {
                                    final ArrayList<String> list = obj;
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append(String.valueOf(size$kotlinx_coroutines_core));
                                    sb.append("r");
                                    list.add(sb.toString());
                                    n = j;
                                    n2 = k;
                                    n3 = l;
                                    m = n6;
                                    n4 = i2;
                                }
                            }
                        }
                        else {
                            n = j + 1;
                            final ArrayList<String> list2 = obj;
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append(String.valueOf(size$kotlinx_coroutines_core));
                            sb2.append("c");
                            list2.add(sb2.toString());
                            n2 = k;
                            n3 = l;
                            n4 = i2;
                        }
                    }
                    else {
                        n2 = k + 1;
                        final ArrayList<String> list3 = obj;
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append(String.valueOf(size$kotlinx_coroutines_core));
                        sb3.append("b");
                        list3.add(sb3.toString());
                        n = j;
                        n3 = l;
                        n4 = i2;
                    }
                }
                else {
                    n3 = l + 1;
                    n4 = i2;
                    n2 = k;
                    n = j;
                }
            }
            ++i;
            j = n;
            k = n2;
            l = n3;
            i2 = n4;
        }
        final long controlState = this.controlState;
        final StringBuilder sb4 = new StringBuilder();
        sb4.append(this.schedulerName);
        sb4.append('@');
        sb4.append(DebugStringsKt.getHexAddress(this));
        sb4.append('[');
        sb4.append("Pool Size {");
        sb4.append("core = ");
        sb4.append(this.corePoolSize);
        sb4.append(", ");
        sb4.append("max = ");
        sb4.append(this.maxPoolSize);
        sb4.append("}, ");
        sb4.append("Worker States {");
        sb4.append("CPU = ");
        sb4.append(j);
        sb4.append(", ");
        sb4.append("blocking = ");
        sb4.append(k);
        sb4.append(", ");
        sb4.append("parked = ");
        sb4.append(l);
        sb4.append(", ");
        sb4.append("retired = ");
        sb4.append(m);
        sb4.append(", ");
        sb4.append("terminated = ");
        sb4.append(i2);
        sb4.append("}, ");
        sb4.append("running workers queues = ");
        sb4.append(obj);
        sb4.append(", ");
        sb4.append("global queue size = ");
        sb4.append(this.globalQueue.getSize());
        sb4.append(", ");
        sb4.append("Control State Workers {");
        sb4.append("created = ");
        sb4.append((int)(0x1FFFFFL & controlState));
        sb4.append(", ");
        sb4.append("blocking = ");
        sb4.append((int)((controlState & 0x3FFFFE00000L) >> 21));
        sb4.append('}');
        sb4.append("]");
        return sb4.toString();
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\bX\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u0016\u0010\f\u001a\u00020\u00048\u0002X\u0083\u0004¢\u0006\b\n\u0000\u0012\u0004\b\r\u0010\u0002R\u000e\u0010\u000e\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0080T¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0011\u001a\u00020\u00048\u0002X\u0083\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u0012\u0010\u0002R\u000e\u0010\u0013\u001a\u00020\u0004X\u0080T¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\bX\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\bX\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\bX\u0082T¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u001b" }, d2 = { "Lkotlinx/coroutines/scheduling/CoroutineScheduler$Companion;", "", "()V", "ADDED", "", "ADDED_REQUIRES_HELP", "ALLOWED", "BLOCKING_MASK", "", "BLOCKING_SHIFT", "CREATED_MASK", "FORBIDDEN", "MAX_PARK_TIME_NS", "MAX_PARK_TIME_NS$annotations", "MAX_SPINS", "MAX_SUPPORTED_POOL_SIZE", "MAX_YIELDS", "MIN_PARK_TIME_NS", "MIN_PARK_TIME_NS$annotations", "MIN_SUPPORTED_POOL_SIZE", "NOT_ADDED", "NOT_IN_STACK", "Lkotlinx/coroutines/internal/Symbol;", "PARKED_INDEX_MASK", "PARKED_VERSION_INC", "PARKED_VERSION_MASK", "TERMINATED", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    public static final class Companion
    {
        private Companion() {
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\f\b\u0080\u0004\u0018\u00002\u00020\u0001B\u0011\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0004\u0010\u0005B\t\b\u0002¢\u0006\u0004\b\u0004\u0010\u0006J\u0017\u0010\n\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\n\u0010\u000bJ\u001f\u0010\u000e\u001a\u00020\t2\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\fH\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\u000f\u0010\u0011\u001a\u00020\u0010H\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0013\u001a\u00020\tH\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u000f\u0010\u0015\u001a\u00020\tH\u0002¢\u0006\u0004\b\u0015\u0010\u0014J\u0017\u0010\u0017\u001a\u00020\u00102\u0006\u0010\u0016\u001a\u00020\fH\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\u0011\u0010\u001c\u001a\u0004\u0018\u00010\u0019H\u0000¢\u0006\u0004\b\u001a\u0010\u001bJ\u0011\u0010\u001d\u001a\u0004\u0018\u00010\u0019H\u0002¢\u0006\u0004\b\u001d\u0010\u001bJ\u0017\u0010\u001f\u001a\u00020\t2\u0006\u0010\u001e\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u001f\u0010\u000bJ\r\u0010 \u001a\u00020\t¢\u0006\u0004\b \u0010\u0014J\u0017\u0010$\u001a\u00020\u00022\u0006\u0010!\u001a\u00020\u0002H\u0000¢\u0006\u0004\b\"\u0010#J\u000f\u0010%\u001a\u00020\tH\u0016¢\u0006\u0004\b%\u0010\u0014J\r\u0010&\u001a\u00020\u0010¢\u0006\u0004\b&\u0010\u0012J\r\u0010'\u001a\u00020\u0010¢\u0006\u0004\b'\u0010\u0012J\u0017\u0010,\u001a\u00020\u00102\u0006\u0010)\u001a\u00020(H\u0000¢\u0006\u0004\b*\u0010+J\u0011\u0010-\u001a\u0004\u0018\u00010\u0019H\u0002¢\u0006\u0004\b-\u0010\u001bJ\u000f\u0010.\u001a\u00020\tH\u0002¢\u0006\u0004\b.\u0010\u0014R*\u0010/\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00028\u0006@FX\u0086\u000e¢\u0006\u0012\n\u0004\b/\u00100\u001a\u0004\b1\u00102\"\u0004\b3\u00104R\u0013\u00105\u001a\u00020\u00108F@\u0006¢\u0006\u0006\u001a\u0004\b5\u0010\u0012R\u0013\u00106\u001a\u00020\u00108F@\u0006¢\u0006\u0006\u001a\u0004\b6\u0010\u0012R\u0016\u00107\u001a\u00020\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b7\u00108R\u0016\u00109\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b9\u00100R\u0019\u0010;\u001a\u00020:8\u0006@\u0006¢\u0006\f\n\u0004\b;\u0010<\u001a\u0004\b=\u0010>R$\u0010@\u001a\u0004\u0018\u00010?8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b@\u0010A\u001a\u0004\bB\u0010C\"\u0004\bD\u0010ER\u0016\u0010F\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bF\u00100R\u0016\u0010G\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bG\u00100R\u0013\u0010K\u001a\u00020H8F@\u0006¢\u0006\u0006\u001a\u0004\bI\u0010JR\u0016\u0010L\u001a\u00020\u00028\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bL\u00100R\"\u0010M\u001a\u00020(8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\bM\u0010N\u001a\u0004\bO\u0010P\"\u0004\bQ\u0010RR\u0016\u0010S\u001a\u00020\f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bS\u00108¨\u0006T" }, d2 = { "Lkotlinx/coroutines/scheduling/CoroutineScheduler$Worker;", "Ljava/lang/Thread;", "", "index", "<init>", "(Lkotlinx/coroutines/scheduling/CoroutineScheduler;I)V", "(Lkotlinx/coroutines/scheduling/CoroutineScheduler;)V", "Lkotlinx/coroutines/scheduling/TaskMode;", "taskMode", "", "afterTask", "(Lkotlinx/coroutines/scheduling/TaskMode;)V", "", "taskSubmissionTime", "beforeTask", "(Lkotlinx/coroutines/scheduling/TaskMode;J)V", "", "blockingQuiescence", "()Z", "blockingWorkerIdle", "()V", "cpuWorkerIdle", "nanos", "doPark", "(J)Z", "Lkotlinx/coroutines/scheduling/Task;", "findTask$kotlinx_coroutines_core", "()Lkotlinx/coroutines/scheduling/Task;", "findTask", "findTaskWithCpuPermit", "mode", "idleReset", "idleResetBeforeUnpark", "upperBound", "nextInt$kotlinx_coroutines_core", "(I)I", "nextInt", "run", "tryAcquireCpuPermit", "tryForbidTermination", "Lkotlinx/coroutines/scheduling/CoroutineScheduler$WorkerState;", "newState", "tryReleaseCpu$kotlinx_coroutines_core", "(Lkotlinx/coroutines/scheduling/CoroutineScheduler$WorkerState;)Z", "tryReleaseCpu", "trySteal", "tryTerminateWorker", "indexInArray", "I", "getIndexInArray", "()I", "setIndexInArray", "(I)V", "isBlocking", "isParking", "lastExhaustionTime", "J", "lastStealIndex", "Lkotlinx/coroutines/scheduling/WorkQueue;", "localQueue", "Lkotlinx/coroutines/scheduling/WorkQueue;", "getLocalQueue", "()Lkotlinx/coroutines/scheduling/WorkQueue;", "", "nextParkedWorker", "Ljava/lang/Object;", "getNextParkedWorker", "()Ljava/lang/Object;", "setNextParkedWorker", "(Ljava/lang/Object;)V", "parkTimeNs", "rngState", "Lkotlinx/coroutines/scheduling/CoroutineScheduler;", "getScheduler", "()Lkotlinx/coroutines/scheduling/CoroutineScheduler;", "scheduler", "spins", "state", "Lkotlinx/coroutines/scheduling/CoroutineScheduler$WorkerState;", "getState", "()Lkotlinx/coroutines/scheduling/CoroutineScheduler$WorkerState;", "setState", "(Lkotlinx/coroutines/scheduling/CoroutineScheduler$WorkerState;)V", "terminationDeadline", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    public final class Worker extends Thread
    {
        private static final AtomicIntegerFieldUpdater terminationState$FU;
        private volatile int indexInArray;
        private long lastExhaustionTime;
        private int lastStealIndex;
        private final WorkQueue localQueue;
        private volatile Object nextParkedWorker;
        private int parkTimeNs;
        private int rngState;
        private volatile int spins;
        private volatile WorkerState state;
        private long terminationDeadline;
        private volatile int terminationState;
        
        static {
            terminationState$FU = AtomicIntegerFieldUpdater.newUpdater(Worker.class, "terminationState");
        }
        
        private Worker() {
            this.setDaemon(true);
            this.localQueue = new WorkQueue();
            this.state = WorkerState.RETIRING;
            this.terminationState = 0;
            this.nextParkedWorker = CoroutineScheduler.access$getNOT_IN_STACK$cp();
            this.parkTimeNs = CoroutineScheduler.access$getMIN_PARK_TIME_NS$cp();
            this.rngState = CoroutineScheduler.access$getRandom$p(CoroutineScheduler.this).nextInt();
        }
        
        public Worker(final CoroutineScheduler coroutineScheduler, final int indexInArray) {
            this();
            this.setIndexInArray(indexInArray);
        }
        
        private final void afterTask(final TaskMode taskMode) {
            if (taskMode != TaskMode.NON_BLOCKING) {
                CoroutineScheduler.controlState$FU.addAndGet(CoroutineScheduler.this, -2097152L);
                final WorkerState state = this.state;
                if (state != WorkerState.TERMINATED) {
                    if (DebugKt.getASSERTIONS_ENABLED() && state != WorkerState.BLOCKING) {
                        throw new AssertionError();
                    }
                    this.state = WorkerState.RETIRING;
                }
            }
        }
        
        private final void beforeTask(final TaskMode taskMode, final long n) {
            if (taskMode != TaskMode.NON_BLOCKING) {
                CoroutineScheduler.controlState$FU.addAndGet(CoroutineScheduler.this, 2097152L);
                if (this.tryReleaseCpu$kotlinx_coroutines_core(WorkerState.BLOCKING)) {
                    CoroutineScheduler.this.requestCpuWorker();
                }
                return;
            }
            if (CoroutineScheduler.access$getCpuPermits$p(CoroutineScheduler.this).availablePermits() == 0) {
                return;
            }
            final long nanoTime = TasksKt.schedulerTimeSource.nanoTime();
            if (nanoTime - n >= TasksKt.WORK_STEALING_TIME_RESOLUTION_NS && nanoTime - this.lastExhaustionTime >= TasksKt.WORK_STEALING_TIME_RESOLUTION_NS * 5) {
                this.lastExhaustionTime = nanoTime;
                CoroutineScheduler.this.requestCpuWorker();
            }
        }
        
        private final boolean blockingQuiescence() {
            final Task removeFirstWithModeOrNull = CoroutineScheduler.access$getGlobalQueue$p(CoroutineScheduler.this).removeFirstWithModeOrNull(TaskMode.PROBABLY_BLOCKING);
            if (removeFirstWithModeOrNull != null) {
                this.localQueue.add(removeFirstWithModeOrNull, CoroutineScheduler.access$getGlobalQueue$p(CoroutineScheduler.this));
                return false;
            }
            return true;
        }
        
        private final void blockingWorkerIdle() {
            this.tryReleaseCpu$kotlinx_coroutines_core(WorkerState.PARKING);
            if (!this.blockingQuiescence()) {
                return;
            }
            this.terminationState = 0;
            if (this.terminationDeadline == 0L) {
                this.terminationDeadline = System.nanoTime() + CoroutineScheduler.access$getIdleWorkerKeepAliveNs$p(CoroutineScheduler.this);
            }
            if (!this.doPark(CoroutineScheduler.access$getIdleWorkerKeepAliveNs$p(CoroutineScheduler.this))) {
                return;
            }
            if (System.nanoTime() - this.terminationDeadline >= 0L) {
                this.terminationDeadline = 0L;
                this.tryTerminateWorker();
            }
        }
        
        private final void cpuWorkerIdle() {
            final int spins = this.spins;
            if (spins <= CoroutineScheduler.access$getMAX_YIELDS$cp()) {
                this.spins = spins + 1;
                if (spins >= CoroutineScheduler.access$getMAX_SPINS$cp()) {
                    Thread.yield();
                }
            }
            else {
                if (this.parkTimeNs < CoroutineScheduler.access$getMAX_PARK_TIME_NS$cp()) {
                    this.parkTimeNs = RangesKt___RangesKt.coerceAtMost(this.parkTimeNs * 3 >>> 1, CoroutineScheduler.access$getMAX_PARK_TIME_NS$cp());
                }
                this.tryReleaseCpu$kotlinx_coroutines_core(WorkerState.PARKING);
                this.doPark(this.parkTimeNs);
            }
        }
        
        private final boolean doPark(final long nanos) {
            CoroutineScheduler.this.parkedWorkersStackPush(this);
            if (!this.blockingQuiescence()) {
                return false;
            }
            LockSupport.parkNanos(nanos);
            return true;
        }
        
        private final Task findTaskWithCpuPermit() {
            final boolean b = this.nextInt$kotlinx_coroutines_core(CoroutineScheduler.access$getCorePoolSize$p(CoroutineScheduler.this) * 2) == 0;
            if (b) {
                final Task removeFirstWithModeOrNull = CoroutineScheduler.access$getGlobalQueue$p(CoroutineScheduler.this).removeFirstWithModeOrNull(TaskMode.NON_BLOCKING);
                if (removeFirstWithModeOrNull != null) {
                    return removeFirstWithModeOrNull;
                }
            }
            final Task poll = this.localQueue.poll();
            if (poll != null) {
                return poll;
            }
            if (!b) {
                final Task task = CoroutineScheduler.access$getGlobalQueue$p(CoroutineScheduler.this).removeFirstOrNull();
                if (task != null) {
                    return task;
                }
            }
            return this.trySteal();
        }
        
        private final void idleReset(final TaskMode taskMode) {
            this.terminationDeadline = 0L;
            this.lastStealIndex = 0;
            if (this.state == WorkerState.PARKING) {
                if (DebugKt.getASSERTIONS_ENABLED() && taskMode != TaskMode.PROBABLY_BLOCKING) {
                    throw new AssertionError();
                }
                this.state = WorkerState.BLOCKING;
                this.parkTimeNs = CoroutineScheduler.access$getMIN_PARK_TIME_NS$cp();
            }
            this.spins = 0;
        }
        
        private final Task trySteal() {
            final int access$getCreatedWorkers$p = CoroutineScheduler.this.getCreatedWorkers();
            if (access$getCreatedWorkers$p < 2) {
                return null;
            }
            int n;
            if ((n = this.lastStealIndex) == 0) {
                n = this.nextInt$kotlinx_coroutines_core(access$getCreatedWorkers$p);
            }
            int lastStealIndex;
            if ((lastStealIndex = n + 1) > access$getCreatedWorkers$p) {
                lastStealIndex = 1;
            }
            this.lastStealIndex = lastStealIndex;
            final Worker worker = CoroutineScheduler.access$getWorkers$p(CoroutineScheduler.this)[lastStealIndex];
            if (worker != null && worker != this && this.localQueue.trySteal(worker.localQueue, CoroutineScheduler.access$getGlobalQueue$p(CoroutineScheduler.this))) {
                return this.localQueue.poll();
            }
            return null;
        }
        
        private final void tryTerminateWorker() {
            synchronized (CoroutineScheduler.access$getWorkers$p(CoroutineScheduler.this)) {
                if (CoroutineScheduler.this.isTerminated()) {
                    return;
                }
                if (CoroutineScheduler.this.getCreatedWorkers() <= CoroutineScheduler.access$getCorePoolSize$p(CoroutineScheduler.this)) {
                    return;
                }
                if (!this.blockingQuiescence()) {
                    return;
                }
                if (!Worker.terminationState$FU.compareAndSet(this, 0, 1)) {
                    return;
                }
                final int indexInArray = this.indexInArray;
                this.setIndexInArray(0);
                CoroutineScheduler.this.parkedWorkersStackTopUpdate(this, indexInArray, 0);
                final int n = (int)(CoroutineScheduler.controlState$FU.getAndDecrement(CoroutineScheduler.this) & 0x1FFFFFL);
                if (n != indexInArray) {
                    final Worker worker = CoroutineScheduler.access$getWorkers$p(CoroutineScheduler.this)[n];
                    if (worker == null) {
                        Intrinsics.throwNpe();
                    }
                    (CoroutineScheduler.access$getWorkers$p(CoroutineScheduler.this)[indexInArray] = worker).setIndexInArray(indexInArray);
                    CoroutineScheduler.this.parkedWorkersStackTopUpdate(worker, n, indexInArray);
                }
                CoroutineScheduler.access$getWorkers$p(CoroutineScheduler.this)[n] = null;
                final Unit instance = Unit.INSTANCE;
                // monitorexit(CoroutineScheduler.access$getWorkers$p(this.this$0))
                this.state = WorkerState.TERMINATED;
            }
        }
        
        public final Task findTask$kotlinx_coroutines_core() {
            if (this.tryAcquireCpuPermit()) {
                return this.findTaskWithCpuPermit();
            }
            final Task poll = this.localQueue.poll();
            if (poll != null) {
                return poll;
            }
            return CoroutineScheduler.access$getGlobalQueue$p(CoroutineScheduler.this).removeFirstWithModeOrNull(TaskMode.PROBABLY_BLOCKING);
        }
        
        public final int getIndexInArray() {
            return this.indexInArray;
        }
        
        public final WorkQueue getLocalQueue() {
            return this.localQueue;
        }
        
        public final Object getNextParkedWorker() {
            return this.nextParkedWorker;
        }
        
        public final CoroutineScheduler getScheduler() {
            return CoroutineScheduler.this;
        }
        
        public final WorkerState getState() {
            return this.state;
        }
        
        public final void idleResetBeforeUnpark() {
            this.parkTimeNs = CoroutineScheduler.access$getMIN_PARK_TIME_NS$cp();
            this.spins = 0;
        }
        
        public final boolean isBlocking() {
            return this.state == WorkerState.BLOCKING;
        }
        
        public final boolean isParking() {
            return this.state == WorkerState.PARKING;
        }
        
        public final int nextInt$kotlinx_coroutines_core(final int n) {
            final int rngState = this.rngState;
            this.rngState = (rngState ^ rngState << 13);
            final int rngState2 = this.rngState;
            this.rngState = (rngState2 ^ rngState2 >> 17);
            final int rngState3 = this.rngState;
            this.rngState = (rngState3 ^ rngState3 << 5);
            final int n2 = n - 1;
            if ((n2 & n) == 0x0) {
                return this.rngState & n2;
            }
            return (this.rngState & Integer.MAX_VALUE) % n;
        }
        
        @Override
        public void run() {
            int n = 0;
            while (!CoroutineScheduler.this.isTerminated() && this.state != WorkerState.TERMINATED) {
                final Task task$kotlinx_coroutines_core = this.findTask$kotlinx_coroutines_core();
                if (task$kotlinx_coroutines_core == null) {
                    if (this.state == WorkerState.CPU_ACQUIRED) {
                        this.cpuWorkerIdle();
                    }
                    else {
                        this.blockingWorkerIdle();
                    }
                    n = 1;
                }
                else {
                    final TaskMode mode = task$kotlinx_coroutines_core.getMode();
                    int n2;
                    if ((n2 = n) != 0) {
                        this.idleReset(mode);
                        n2 = 0;
                    }
                    this.beforeTask(mode, task$kotlinx_coroutines_core.submissionTime);
                    CoroutineScheduler.this.runSafely(task$kotlinx_coroutines_core);
                    this.afterTask(mode);
                    n = n2;
                }
            }
            this.tryReleaseCpu$kotlinx_coroutines_core(WorkerState.TERMINATED);
        }
        
        public final void setIndexInArray(final int n) {
            final StringBuilder sb = new StringBuilder();
            sb.append(CoroutineScheduler.access$getSchedulerName$p(CoroutineScheduler.this));
            sb.append("-worker-");
            String value;
            if (n == 0) {
                value = "TERMINATED";
            }
            else {
                value = String.valueOf(n);
            }
            sb.append(value);
            this.setName(sb.toString());
            this.indexInArray = n;
        }
        
        public final void setNextParkedWorker(final Object nextParkedWorker) {
            this.nextParkedWorker = nextParkedWorker;
        }
        
        public final void setState(final WorkerState state) {
            Intrinsics.checkParameterIsNotNull(state, "<set-?>");
            this.state = state;
        }
        
        public final boolean tryAcquireCpuPermit() {
            if (this.state == WorkerState.CPU_ACQUIRED) {
                return true;
            }
            if (CoroutineScheduler.access$getCpuPermits$p(CoroutineScheduler.this).tryAcquire()) {
                this.state = WorkerState.CPU_ACQUIRED;
                return true;
            }
            return false;
        }
        
        public final boolean tryForbidTermination() {
            final int terminationState = this.terminationState;
            if (terminationState == 1) {
                return false;
            }
            if (terminationState == -1) {
                return false;
            }
            if (terminationState == 0) {
                return Worker.terminationState$FU.compareAndSet(this, 0, -1);
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid terminationState = ");
            sb.append(terminationState);
            throw new IllegalStateException(sb.toString().toString());
        }
        
        public final boolean tryReleaseCpu$kotlinx_coroutines_core(final WorkerState state) {
            Intrinsics.checkParameterIsNotNull(state, "newState");
            final WorkerState state2 = this.state;
            final boolean b = state2 == WorkerState.CPU_ACQUIRED;
            if (b) {
                CoroutineScheduler.access$getCpuPermits$p(CoroutineScheduler.this).release();
            }
            if (state2 != state) {
                this.state = state;
            }
            return b;
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b" }, d2 = { "Lkotlinx/coroutines/scheduling/CoroutineScheduler$WorkerState;", "", "(Ljava/lang/String;I)V", "CPU_ACQUIRED", "BLOCKING", "PARKING", "RETIRING", "TERMINATED", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
    public enum WorkerState
    {
        BLOCKING, 
        CPU_ACQUIRED, 
        PARKING, 
        RETIRING, 
        TERMINATED;
    }
}
