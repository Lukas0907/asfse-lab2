// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.scheduling;

import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.internal.Intrinsics;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import kotlin.Metadata;
import java.util.concurrent.Executor;
import kotlinx.coroutines.ExecutorCoroutineDispatcher;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0002\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u001f\u0012\u0006\u0010\u0005\u001a\u00020\u0004\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b\n\u0010\u000bJ\u000f\u0010\r\u001a\u00020\fH\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\fH\u0016¢\u0006\u0004\b\u000f\u0010\u000eJ#\u0010\u0015\u001a\u00020\f2\u0006\u0010\u0011\u001a\u00020\u00102\n\u0010\u0014\u001a\u00060\u0012j\u0002`\u0013H\u0016¢\u0006\u0004\b\u0015\u0010\u0016J#\u0010\u0015\u001a\u00020\f2\n\u0010\u0014\u001a\u00060\u0012j\u0002`\u00132\u0006\u0010\u0018\u001a\u00020\u0017H\u0002¢\u0006\u0004\b\u0015\u0010\u0019J\u001b\u0010\u001b\u001a\u00020\f2\n\u0010\u001a\u001a\u00060\u0012j\u0002`\u0013H\u0016¢\u0006\u0004\b\u001b\u0010\u001cJ\u000f\u0010\u001e\u001a\u00020\u001dH\u0016¢\u0006\u0004\b\u001e\u0010\u001fR\u0019\u0010\u0005\u001a\u00020\u00048\u0006@\u0006¢\u0006\f\n\u0004\b\u0005\u0010 \u001a\u0004\b!\u0010\"R\u0016\u0010%\u001a\u00020\u00038V@\u0016X\u0096\u0004¢\u0006\u0006\u001a\u0004\b#\u0010$R\u0019\u0010\u0007\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010&\u001a\u0004\b'\u0010(R \u0010*\u001a\f\u0012\b\u0012\u00060\u0012j\u0002`\u00130)8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010+R\u001c\u0010\t\u001a\u00020\b8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\t\u0010,\u001a\u0004\b-\u0010.¨\u0006/" }, d2 = { "Lkotlinx/coroutines/scheduling/LimitingDispatcher;", "Lkotlinx/coroutines/ExecutorCoroutineDispatcher;", "Lkotlinx/coroutines/scheduling/TaskContext;", "Ljava/util/concurrent/Executor;", "Lkotlinx/coroutines/scheduling/ExperimentalCoroutineDispatcher;", "dispatcher", "", "parallelism", "Lkotlinx/coroutines/scheduling/TaskMode;", "taskMode", "<init>", "(Lkotlinx/coroutines/scheduling/ExperimentalCoroutineDispatcher;ILkotlinx/coroutines/scheduling/TaskMode;)V", "", "afterTask", "()V", "close", "Lkotlin/coroutines/CoroutineContext;", "context", "Ljava/lang/Runnable;", "Lkotlinx/coroutines/Runnable;", "block", "dispatch", "(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Runnable;)V", "", "fair", "(Ljava/lang/Runnable;Z)V", "command", "execute", "(Ljava/lang/Runnable;)V", "", "toString", "()Ljava/lang/String;", "Lkotlinx/coroutines/scheduling/ExperimentalCoroutineDispatcher;", "getDispatcher", "()Lkotlinx/coroutines/scheduling/ExperimentalCoroutineDispatcher;", "getExecutor", "()Ljava/util/concurrent/Executor;", "executor", "I", "getParallelism", "()I", "Ljava/util/concurrent/ConcurrentLinkedQueue;", "queue", "Ljava/util/concurrent/ConcurrentLinkedQueue;", "Lkotlinx/coroutines/scheduling/TaskMode;", "getTaskMode", "()Lkotlinx/coroutines/scheduling/TaskMode;", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
final class LimitingDispatcher extends ExecutorCoroutineDispatcher implements TaskContext, Executor
{
    private static final AtomicIntegerFieldUpdater inFlightTasks$FU;
    private final ExperimentalCoroutineDispatcher dispatcher;
    private volatile int inFlightTasks;
    private final int parallelism;
    private final ConcurrentLinkedQueue<Runnable> queue;
    private final TaskMode taskMode;
    
    static {
        inFlightTasks$FU = AtomicIntegerFieldUpdater.newUpdater(LimitingDispatcher.class, "inFlightTasks");
    }
    
    public LimitingDispatcher(final ExperimentalCoroutineDispatcher dispatcher, final int parallelism, final TaskMode taskMode) {
        Intrinsics.checkParameterIsNotNull(dispatcher, "dispatcher");
        Intrinsics.checkParameterIsNotNull(taskMode, "taskMode");
        this.dispatcher = dispatcher;
        this.parallelism = parallelism;
        this.taskMode = taskMode;
        this.queue = new ConcurrentLinkedQueue<Runnable>();
        this.inFlightTasks = 0;
    }
    
    private final void dispatch(Runnable e, final boolean b) {
        while (LimitingDispatcher.inFlightTasks$FU.incrementAndGet(this) > this.parallelism) {
            this.queue.add(e);
            if (LimitingDispatcher.inFlightTasks$FU.decrementAndGet(this) >= this.parallelism) {
                return;
            }
            e = this.queue.poll();
            if (e != null) {
                continue;
            }
            return;
        }
        this.dispatcher.dispatchWithContext$kotlinx_coroutines_core(e, this, b);
    }
    
    @Override
    public void afterTask() {
        final Runnable runnable = this.queue.poll();
        if (runnable != null) {
            this.dispatcher.dispatchWithContext$kotlinx_coroutines_core(runnable, this, true);
            return;
        }
        LimitingDispatcher.inFlightTasks$FU.decrementAndGet(this);
        final Runnable runnable2 = this.queue.poll();
        if (runnable2 != null) {
            this.dispatch(runnable2, true);
        }
    }
    
    @Override
    public void close() {
        throw new IllegalStateException("Close cannot be invoked on LimitingBlockingDispatcher".toString());
    }
    
    @Override
    public void dispatch(final CoroutineContext coroutineContext, final Runnable runnable) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        Intrinsics.checkParameterIsNotNull(runnable, "block");
        this.dispatch(runnable, false);
    }
    
    @Override
    public void execute(final Runnable runnable) {
        Intrinsics.checkParameterIsNotNull(runnable, "command");
        this.dispatch(runnable, false);
    }
    
    public final ExperimentalCoroutineDispatcher getDispatcher() {
        return this.dispatcher;
    }
    
    @Override
    public Executor getExecutor() {
        return this;
    }
    
    public final int getParallelism() {
        return this.parallelism;
    }
    
    @Override
    public TaskMode getTaskMode() {
        return this.taskMode;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("[dispatcher = ");
        sb.append(this.dispatcher);
        sb.append(']');
        return sb.toString();
    }
}
