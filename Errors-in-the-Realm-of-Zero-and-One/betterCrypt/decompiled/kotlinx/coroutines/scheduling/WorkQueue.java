// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines.scheduling;

import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function1;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0000\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0002\u0010\u0003J\u001d\u0010\t\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\t\u0010\nJ\u001d\u0010\u000b\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b\u000b\u0010\nJ\u001f\u0010\r\u001a\u00020\f2\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\f2\u0006\u0010\u0007\u001a\u00020\u0006H\u0000¢\u0006\u0004\b\u000f\u0010\u0010J\u0017\u0010\u0012\u001a\u00020\f2\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b\u0012\u0010\u0010J\u000f\u0010\u0013\u001a\u0004\u0018\u00010\u0004¢\u0006\u0004\b\u0013\u0010\u0014J(\u0010\u0017\u001a\u0004\u0018\u00010\u00042\u0014\b\u0002\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\b0\u0015H\u0082\b¢\u0006\u0004\b\u0017\u0010\u0018J\u000f\u0010\u001c\u001a\u00020\u0019H\u0000¢\u0006\u0004\b\u001a\u0010\u001bJ\u0017\u0010\u001d\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u0004H\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ\u001d\u0010 \u001a\u00020\b2\u0006\u0010\u001f\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0004\b \u0010!J'\u0010$\u001a\u00020\b2\u0006\u0010#\u001a\u00020\"2\u0006\u0010\u001f\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\u0006H\u0002¢\u0006\u0004\b$\u0010%R\u001e\u0010'\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00040&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u0016\u0010*\u001a\u00020\u00198@@\u0000X\u0080\u0004¢\u0006\u0006\u001a\u0004\b)\u0010\u001b¨\u0006+" }, d2 = { "Lkotlinx/coroutines/scheduling/WorkQueue;", "", "<init>", "()V", "Lkotlinx/coroutines/scheduling/Task;", "task", "Lkotlinx/coroutines/scheduling/GlobalQueue;", "globalQueue", "", "add", "(Lkotlinx/coroutines/scheduling/Task;Lkotlinx/coroutines/scheduling/GlobalQueue;)Z", "addLast", "", "addToGlobalQueue", "(Lkotlinx/coroutines/scheduling/GlobalQueue;Lkotlinx/coroutines/scheduling/Task;)V", "offloadAllWork$kotlinx_coroutines_core", "(Lkotlinx/coroutines/scheduling/GlobalQueue;)V", "offloadAllWork", "offloadWork", "poll", "()Lkotlinx/coroutines/scheduling/Task;", "Lkotlin/Function1;", "predicate", "pollExternal", "(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/scheduling/Task;", "", "size$kotlinx_coroutines_core", "()I", "size", "tryAddLast", "(Lkotlinx/coroutines/scheduling/Task;)Z", "victim", "trySteal", "(Lkotlinx/coroutines/scheduling/WorkQueue;Lkotlinx/coroutines/scheduling/GlobalQueue;)Z", "", "time", "tryStealLastScheduled", "(JLkotlinx/coroutines/scheduling/WorkQueue;Lkotlinx/coroutines/scheduling/GlobalQueue;)Z", "Ljava/util/concurrent/atomic/AtomicReferenceArray;", "buffer", "Ljava/util/concurrent/atomic/AtomicReferenceArray;", "getBufferSize$kotlinx_coroutines_core", "bufferSize", "kotlinx-coroutines-core" }, k = 1, mv = { 1, 1, 15 })
public final class WorkQueue
{
    static final AtomicIntegerFieldUpdater consumerIndex$FU;
    private static final AtomicReferenceFieldUpdater lastScheduledTask$FU;
    static final AtomicIntegerFieldUpdater producerIndex$FU;
    private final AtomicReferenceArray<Task> buffer;
    volatile int consumerIndex;
    private volatile Object lastScheduledTask;
    volatile int producerIndex;
    
    static {
        lastScheduledTask$FU = AtomicReferenceFieldUpdater.newUpdater(WorkQueue.class, Object.class, "lastScheduledTask");
        producerIndex$FU = AtomicIntegerFieldUpdater.newUpdater(WorkQueue.class, "producerIndex");
        consumerIndex$FU = AtomicIntegerFieldUpdater.newUpdater(WorkQueue.class, "consumerIndex");
    }
    
    public WorkQueue() {
        this.buffer = new AtomicReferenceArray<Task>(128);
        this.lastScheduledTask = null;
        this.producerIndex = 0;
        this.consumerIndex = 0;
    }
    
    public static final /* synthetic */ AtomicReferenceArray access$getBuffer$p(final WorkQueue workQueue) {
        return workQueue.buffer;
    }
    
    private final void addToGlobalQueue(final GlobalQueue globalQueue, final Task task) {
        if (globalQueue.addLast(task)) {
            return;
        }
        throw new IllegalStateException("GlobalQueue could not be closed yet".toString());
    }
    
    private final void offloadWork(final GlobalQueue globalQueue) {
        final int coerceAtLeast = RangesKt___RangesKt.coerceAtLeast(this.getBufferSize$kotlinx_coroutines_core() / 2, 1);
        int i = 0;
    Label_0116:
        while (i < coerceAtLeast) {
            while (true) {
                int n;
                int consumerIndex;
                do {
                    consumerIndex = this.consumerIndex;
                    final int producerIndex = this.producerIndex;
                    final Task task = null;
                    if (consumerIndex - producerIndex == 0) {
                        if (task != null) {
                            this.addToGlobalQueue(globalQueue, task);
                            ++i;
                            continue Label_0116;
                        }
                        break Label_0116;
                    }
                    else {
                        n = (consumerIndex & 0x7F);
                    }
                } while (access$getBuffer$p(this).get(n) == null || !WorkQueue.consumerIndex$FU.compareAndSet(this, consumerIndex, consumerIndex + 1));
                final Task task = access$getBuffer$p(this).getAndSet(n, null);
                continue;
            }
        }
    }
    
    private final Task pollExternal(final Function1<? super Task, Boolean> function1) {
        while (true) {
            final int consumerIndex = this.consumerIndex;
            if (consumerIndex - this.producerIndex == 0) {
                return null;
            }
            final int n = consumerIndex & 0x7F;
            final Task task = access$getBuffer$p(this).get(n);
            if (task == null) {
                continue;
            }
            if (!function1.invoke(task)) {
                return null;
            }
            if (WorkQueue.consumerIndex$FU.compareAndSet(this, consumerIndex, consumerIndex + 1)) {
                return access$getBuffer$p(this).getAndSet(n, null);
            }
        }
    }
    
    private final boolean tryAddLast(final Task newValue) {
        if (this.getBufferSize$kotlinx_coroutines_core() == 127) {
            return false;
        }
        final int n = this.producerIndex & 0x7F;
        if (this.buffer.get(n) != null) {
            return false;
        }
        this.buffer.lazySet(n, newValue);
        WorkQueue.producerIndex$FU.incrementAndGet(this);
        return true;
    }
    
    private final boolean tryStealLastScheduled(final long n, final WorkQueue workQueue, final GlobalQueue globalQueue) {
        final Task task = (Task)workQueue.lastScheduledTask;
        if (task != null) {
            if (n - task.submissionTime < TasksKt.WORK_STEALING_TIME_RESOLUTION_NS) {
                return false;
            }
            if (WorkQueue.lastScheduledTask$FU.compareAndSet(workQueue, task, null)) {
                this.add(task, globalQueue);
                return true;
            }
        }
        return false;
    }
    
    public final boolean add(Task newValue, final GlobalQueue globalQueue) {
        Intrinsics.checkParameterIsNotNull(newValue, "task");
        Intrinsics.checkParameterIsNotNull(globalQueue, "globalQueue");
        newValue = WorkQueue.lastScheduledTask$FU.getAndSet(this, newValue);
        return newValue == null || this.addLast(newValue, globalQueue);
    }
    
    public final boolean addLast(final Task task, final GlobalQueue globalQueue) {
        Intrinsics.checkParameterIsNotNull(task, "task");
        Intrinsics.checkParameterIsNotNull(globalQueue, "globalQueue");
        boolean b = true;
        while (!this.tryAddLast(task)) {
            this.offloadWork(globalQueue);
            b = false;
        }
        return b;
    }
    
    public final int getBufferSize$kotlinx_coroutines_core() {
        return this.producerIndex - this.consumerIndex;
    }
    
    public final void offloadAllWork$kotlinx_coroutines_core(final GlobalQueue globalQueue) {
        Intrinsics.checkParameterIsNotNull(globalQueue, "globalQueue");
        final Task task = WorkQueue.lastScheduledTask$FU.getAndSet(this, null);
        if (task != null) {
            this.addToGlobalQueue(globalQueue, task);
        }
        while (true) {
            final int consumerIndex = this.consumerIndex;
            Task task2;
            if (consumerIndex - this.producerIndex == 0) {
                task2 = null;
            }
            else {
                final int n = consumerIndex & 0x7F;
                if (access$getBuffer$p(this).get(n) == null || !WorkQueue.consumerIndex$FU.compareAndSet(this, consumerIndex, consumerIndex + 1)) {
                    continue;
                }
                task2 = access$getBuffer$p(this).getAndSet(n, null);
            }
            if (task2 == null) {
                break;
            }
            this.addToGlobalQueue(globalQueue, task2);
        }
    }
    
    public final Task poll() {
        final Task task = WorkQueue.lastScheduledTask$FU.getAndSet(this, null);
        if (task != null) {
            return task;
        }
        int n;
        int consumerIndex;
        do {
            consumerIndex = this.consumerIndex;
            if (consumerIndex - this.producerIndex == 0) {
                return null;
            }
            n = (consumerIndex & 0x7F);
        } while (access$getBuffer$p(this).get(n) == null || !WorkQueue.consumerIndex$FU.compareAndSet(this, consumerIndex, consumerIndex + 1));
        return access$getBuffer$p(this).getAndSet(n, null);
    }
    
    public final int size$kotlinx_coroutines_core() {
        if (this.lastScheduledTask != null) {
            return this.getBufferSize$kotlinx_coroutines_core() + 1;
        }
        return this.getBufferSize$kotlinx_coroutines_core();
    }
    
    public final boolean trySteal(final WorkQueue workQueue, final GlobalQueue globalQueue) {
        throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge Z and I\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
    }
}
