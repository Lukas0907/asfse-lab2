// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import java.util.Iterator;
import kotlin.Unit;
import kotlin.TypeCastException;
import kotlin.ResultKt;
import java.util.List;
import kotlin.coroutines.Continuation;
import java.util.Collection;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000*\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\b\u0002\u001a=\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u00022\u001e\u0010\u0003\u001a\u0010\u0012\f\b\u0001\u0012\b\u0012\u0004\u0012\u0002H\u00020\u00050\u0004\"\b\u0012\u0004\u0012\u0002H\u00020\u0005H\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0006\u001a%\u0010\u0007\u001a\u00020\b2\u0012\u0010\t\u001a\n\u0012\u0006\b\u0001\u0012\u00020\n0\u0004\"\u00020\nH\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000b\u001a-\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u00050\fH\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u0010\r\u001a\u001b\u0010\u0007\u001a\u00020\b*\b\u0012\u0004\u0012\u00020\n0\fH\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u0010\r\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u000e" }, d2 = { "awaitAll", "", "T", "deferreds", "", "Lkotlinx/coroutines/Deferred;", "([Lkotlinx/coroutines/Deferred;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "joinAll", "", "jobs", "Lkotlinx/coroutines/Job;", "([Lkotlinx/coroutines/Job;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "", "(Ljava/util/Collection;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class AwaitKt
{
    public static final <T> Object awaitAll(final Collection<? extends Deferred<? extends T>> l$0, final Continuation<? super List<? extends T>> continuation) {
        Object o = null;
        Label_0047: {
            if (continuation instanceof AwaitKt$awaitAll.AwaitKt$awaitAll$2) {
                final AwaitKt$awaitAll.AwaitKt$awaitAll$2 awaitKt$awaitAll$2 = (AwaitKt$awaitAll.AwaitKt$awaitAll$2)continuation;
                if ((awaitKt$awaitAll$2.label & Integer.MIN_VALUE) != 0x0) {
                    awaitKt$awaitAll$2.label += Integer.MIN_VALUE;
                    o = awaitKt$awaitAll$2;
                    break Label_0047;
                }
            }
            o = new AwaitKt$awaitAll.AwaitKt$awaitAll$2((Continuation)continuation);
        }
        final Object result = ((AwaitKt$awaitAll.AwaitKt$awaitAll$2)o).result;
        final Object coroutine_SUSPENDED = IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
        final int label = ((AwaitKt$awaitAll.AwaitKt$awaitAll$2)o).label;
        Object await;
        if (label != 0) {
            if (label != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            final Collection collection = (Collection)((AwaitKt$awaitAll.AwaitKt$awaitAll$2)o).L$0;
            ResultKt.throwOnFailure(result);
            await = result;
        }
        else {
            ResultKt.throwOnFailure(result);
            if (l$0.isEmpty()) {
                return CollectionsKt__CollectionsKt.emptyList();
            }
            final Deferred<?>[] array = l$0.toArray(new Deferred[0]);
            if (array == null) {
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
            final AwaitAll awaitAll = new AwaitAll(array);
            ((AwaitKt$awaitAll.AwaitKt$awaitAll$2)o).L$0 = l$0;
            ((AwaitKt$awaitAll.AwaitKt$awaitAll$2)o).label = 1;
            if ((await = awaitAll.await((Continuation)o)) == coroutine_SUSPENDED) {
                return coroutine_SUSPENDED;
            }
        }
        return await;
    }
    
    public static final <T> Object awaitAll(final Deferred<? extends T>[] l$0, final Continuation<? super List<? extends T>> continuation) {
        Object o = null;
        Label_0047: {
            if (continuation instanceof AwaitKt$awaitAll.AwaitKt$awaitAll$1) {
                final AwaitKt$awaitAll.AwaitKt$awaitAll$1 awaitKt$awaitAll$1 = (AwaitKt$awaitAll.AwaitKt$awaitAll$1)continuation;
                if ((awaitKt$awaitAll$1.label & Integer.MIN_VALUE) != 0x0) {
                    awaitKt$awaitAll$1.label += Integer.MIN_VALUE;
                    o = awaitKt$awaitAll$1;
                    break Label_0047;
                }
            }
            o = new AwaitKt$awaitAll.AwaitKt$awaitAll$1((Continuation)continuation);
        }
        final Object result = ((AwaitKt$awaitAll.AwaitKt$awaitAll$1)o).result;
        final Object coroutine_SUSPENDED = IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
        final int label = ((AwaitKt$awaitAll.AwaitKt$awaitAll$1)o).label;
        Object await;
        if (label != 0) {
            if (label != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            final Deferred[] array = (Deferred[])((AwaitKt$awaitAll.AwaitKt$awaitAll$1)o).L$0;
            ResultKt.throwOnFailure(result);
            await = result;
        }
        else {
            ResultKt.throwOnFailure(result);
            if (l$0.length == 0) {
                return CollectionsKt__CollectionsKt.emptyList();
            }
            final AwaitAll awaitAll = new AwaitAll(l$0);
            ((AwaitKt$awaitAll.AwaitKt$awaitAll$1)o).L$0 = l$0;
            ((AwaitKt$awaitAll.AwaitKt$awaitAll$1)o).label = 1;
            if ((await = awaitAll.await((Continuation)o)) == coroutine_SUSPENDED) {
                return coroutine_SUSPENDED;
            }
        }
        return await;
    }
    
    public static final Object joinAll(final Collection<? extends Job> collection, final Continuation<? super Unit> continuation) {
        Object o = null;
        Label_0047: {
            if (continuation instanceof AwaitKt$joinAll.AwaitKt$joinAll$3) {
                final AwaitKt$joinAll.AwaitKt$joinAll$3 awaitKt$joinAll$3 = (AwaitKt$joinAll.AwaitKt$joinAll$3)continuation;
                if ((awaitKt$joinAll$3.label & Integer.MIN_VALUE) != 0x0) {
                    awaitKt$joinAll$3.label += Integer.MIN_VALUE;
                    o = awaitKt$joinAll$3;
                    break Label_0047;
                }
            }
            o = new AwaitKt$joinAll.AwaitKt$joinAll$3((Continuation)continuation);
        }
        final Object result = ((AwaitKt$joinAll.AwaitKt$joinAll$3)o).result;
        final Object coroutine_SUSPENDED = IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
        final int label = ((AwaitKt$joinAll.AwaitKt$joinAll$3)o).label;
        Iterator<Job> iterator;
        Iterable<? extends Job> l$4;
        Collection<? extends Job> l$5;
        if (label != 0) {
            if (label != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            final Job job = (Job)((AwaitKt$joinAll.AwaitKt$joinAll$3)o).L$4;
            final Object l$3 = ((AwaitKt$joinAll.AwaitKt$joinAll$3)o).L$3;
            iterator = (Iterator<Job>)((AwaitKt$joinAll.AwaitKt$joinAll$3)o).L$2;
            l$4 = (Iterable<? extends Job>)((AwaitKt$joinAll.AwaitKt$joinAll$3)o).L$1;
            l$5 = (Collection<? extends Job>)((AwaitKt$joinAll.AwaitKt$joinAll$3)o).L$0;
            ResultKt.throwOnFailure(result);
        }
        else {
            ResultKt.throwOnFailure(result);
            final Collection<? extends Job> collection2 = collection;
            iterator = collection2.iterator();
            l$5 = collection;
            l$4 = collection2;
        }
        while (iterator.hasNext()) {
            final Job next = iterator.next();
            final Job l$6 = next;
            ((AwaitKt$joinAll.AwaitKt$joinAll$3)o).L$0 = l$5;
            ((AwaitKt$joinAll.AwaitKt$joinAll$3)o).L$1 = l$4;
            ((AwaitKt$joinAll.AwaitKt$joinAll$3)o).L$2 = iterator;
            ((AwaitKt$joinAll.AwaitKt$joinAll$3)o).L$3 = next;
            ((AwaitKt$joinAll.AwaitKt$joinAll$3)o).L$4 = l$6;
            ((AwaitKt$joinAll.AwaitKt$joinAll$3)o).label = 1;
            if (l$6.join((Continuation<? super Unit>)o) == coroutine_SUSPENDED) {
                return coroutine_SUSPENDED;
            }
        }
        return Unit.INSTANCE;
    }
    
    public static final Object joinAll(Job[] l$0, final Continuation<? super Unit> continuation) {
        Object o = null;
        Label_0052: {
            if (continuation instanceof AwaitKt$joinAll.AwaitKt$joinAll$1) {
                final AwaitKt$joinAll.AwaitKt$joinAll$1 awaitKt$joinAll$1 = (AwaitKt$joinAll.AwaitKt$joinAll$1)continuation;
                if ((awaitKt$joinAll$1.label & Integer.MIN_VALUE) != 0x0) {
                    awaitKt$joinAll$1.label += Integer.MIN_VALUE;
                    o = awaitKt$joinAll$1;
                    break Label_0052;
                }
            }
            o = new AwaitKt$joinAll.AwaitKt$joinAll$1((Continuation)continuation);
        }
        final Object result = ((AwaitKt$joinAll.AwaitKt$joinAll$1)o).result;
        Object coroutine_SUSPENDED = IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
        final int label = ((AwaitKt$joinAll.AwaitKt$joinAll$1)o).label;
        while (true) {
            int i$1 = 0;
            Label_0268: {
                int i$2;
                Job[] l$2;
                Job[] l$3;
                Object o2;
                AwaitKt$joinAll.AwaitKt$joinAll$1 awaitKt$joinAll$2;
                if (label != 0) {
                    if (label == 1) {
                        final Job job = (Job)((AwaitKt$joinAll.AwaitKt$joinAll$1)o).L$4;
                        final Job job2 = (Job)((AwaitKt$joinAll.AwaitKt$joinAll$1)o).L$3;
                        i$1 = ((AwaitKt$joinAll.AwaitKt$joinAll$1)o).I$1;
                        i$2 = ((AwaitKt$joinAll.AwaitKt$joinAll$1)o).I$0;
                        final Job[] array = (Job[])((AwaitKt$joinAll.AwaitKt$joinAll$1)o).L$2;
                        l$2 = (Job[])((AwaitKt$joinAll.AwaitKt$joinAll$1)o).L$1;
                        l$0 = (Job[])((AwaitKt$joinAll.AwaitKt$joinAll$1)o).L$0;
                        ResultKt.throwOnFailure(result);
                        l$3 = array;
                        break Label_0268;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                else {
                    ResultKt.throwOnFailure(result);
                    i$2 = l$0.length;
                    o2 = coroutine_SUSPENDED;
                    final Job[] array2 = l$0;
                    i$1 = 0;
                    l$3 = array2;
                    l$2 = l$0;
                    l$0 = array2;
                    awaitKt$joinAll$2 = (AwaitKt$joinAll.AwaitKt$joinAll$1)o;
                }
                if (i$1 >= i$2) {
                    return Unit.INSTANCE;
                }
                final Job job3 = l$3[i$1];
                awaitKt$joinAll$2.L$0 = l$0;
                awaitKt$joinAll$2.L$1 = l$2;
                awaitKt$joinAll$2.L$2 = l$3;
                awaitKt$joinAll$2.I$0 = i$2;
                awaitKt$joinAll$2.I$1 = i$1;
                awaitKt$joinAll$2.L$3 = job3;
                awaitKt$joinAll$2.L$4 = job3;
                awaitKt$joinAll$2.label = 1;
                o = awaitKt$joinAll$2;
                if (job3.join((Continuation<? super Unit>)awaitKt$joinAll$2) == (coroutine_SUSPENDED = o2)) {
                    return o2;
                }
            }
            ++i$1;
            AwaitKt$joinAll.AwaitKt$joinAll$1 awaitKt$joinAll$2 = (AwaitKt$joinAll.AwaitKt$joinAll$1)o;
            Object o2 = coroutine_SUSPENDED;
            continue;
        }
    }
}
