// 
// Decompiled by Procyon v0.5.36
// 

package kotlinx.coroutines;

import kotlin.coroutines.CoroutineContext;
import java.util.Iterator;
import kotlin.jvm.internal.Intrinsics;
import java.util.ServiceLoader;
import java.util.List;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\u001a\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0000\"\u0014\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\t" }, d2 = { "handlers", "", "Lkotlinx/coroutines/CoroutineExceptionHandler;", "handleCoroutineExceptionImpl", "", "context", "Lkotlin/coroutines/CoroutineContext;", "exception", "", "kotlinx-coroutines-core" }, k = 2, mv = { 1, 1, 15 })
public final class CoroutineExceptionHandlerImplKt
{
    private static final List<CoroutineExceptionHandler> handlers;
    
    static {
        final Iterator<CoroutineExceptionHandler> iterator = ServiceLoader.load(CoroutineExceptionHandler.class, CoroutineExceptionHandler.class.getClassLoader()).iterator();
        Intrinsics.checkExpressionValueIsNotNull(iterator, "ServiceLoader.load(\n    \u2026.classLoader\n).iterator()");
        handlers = SequencesKt___SequencesKt.toList(SequencesKt__SequencesKt.asSequence((Iterator<?>)iterator));
    }
    
    public static final void handleCoroutineExceptionImpl(final CoroutineContext coroutineContext, final Throwable t) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "context");
        Intrinsics.checkParameterIsNotNull(t, "exception");
        for (final CoroutineExceptionHandler coroutineExceptionHandler : CoroutineExceptionHandlerImplKt.handlers) {
            try {
                coroutineExceptionHandler.handleException(coroutineContext, t);
            }
            finally {
                final Thread currentThread = Thread.currentThread();
                Intrinsics.checkExpressionValueIsNotNull(currentThread, "currentThread");
                final Throwable t2;
                currentThread.getUncaughtExceptionHandler().uncaughtException(currentThread, CoroutineExceptionHandlerKt.handlerException(t, t2));
            }
        }
        final Thread currentThread2 = Thread.currentThread();
        Intrinsics.checkExpressionValueIsNotNull(currentThread2, "currentThread");
        currentThread2.getUncaughtExceptionHandler().uncaughtException(currentThread2, t);
    }
}
