// 
// Decompiled by Procyon v0.5.36
// 

package androidx.appcompat.widget;

import java.lang.reflect.InvocationTargetException;
import androidx.core.view.ViewCompat;
import android.util.Log;
import android.graphics.Rect;
import android.view.View;
import android.os.Build$VERSION;
import java.lang.reflect.Method;

public class ViewUtils
{
    private static final String TAG = "ViewUtils";
    private static Method sComputeFitSystemWindowsMethod;
    
    static {
        if (Build$VERSION.SDK_INT < 18) {
            return;
        }
        while (true) {
            while (true) {
                try {
                    ViewUtils.sComputeFitSystemWindowsMethod = View.class.getDeclaredMethod("computeFitSystemWindows", Rect.class, Rect.class);
                    if (!ViewUtils.sComputeFitSystemWindowsMethod.isAccessible()) {
                        ViewUtils.sComputeFitSystemWindowsMethod.setAccessible(true);
                        return;
                    }
                    return;
                    Log.d("ViewUtils", "Could not find method computeFitSystemWindows. Oh well.");
                    return;
                }
                catch (NoSuchMethodException ex) {}
                continue;
            }
        }
    }
    
    private ViewUtils() {
    }
    
    public static void computeFitSystemWindows(final View obj, final Rect rect, final Rect rect2) {
        final Method sComputeFitSystemWindowsMethod = ViewUtils.sComputeFitSystemWindowsMethod;
        if (sComputeFitSystemWindowsMethod != null) {
            try {
                sComputeFitSystemWindowsMethod.invoke(obj, rect, rect2);
            }
            catch (Exception ex) {
                Log.d("ViewUtils", "Could not invoke computeFitSystemWindows", (Throwable)ex);
            }
        }
    }
    
    public static boolean isLayoutRtl(final View view) {
        return ViewCompat.getLayoutDirection(view) == 1;
    }
    
    public static void makeOptionalFitsSystemWindows(final View obj) {
        if (Build$VERSION.SDK_INT >= 16) {
            try {
                final Method method = obj.getClass().getMethod("makeOptionalFitsSystemWindows", (Class<?>[])new Class[0]);
                if (!method.isAccessible()) {
                    method.setAccessible(true);
                }
                method.invoke(obj, new Object[0]);
                return;
            }
            catch (IllegalAccessException ex) {
                Log.d("ViewUtils", "Could not invoke makeOptionalFitsSystemWindows", (Throwable)ex);
                return;
            }
            catch (InvocationTargetException ex2) {
                Log.d("ViewUtils", "Could not invoke makeOptionalFitsSystemWindows", (Throwable)ex2);
                return;
            }
            catch (NoSuchMethodException ex3) {}
            goto Label_0067;
        }
        goto Label_0075;
    }
}
