// 
// Decompiled by Procyon v0.5.36
// 

package androidx.appcompat.widget;

import android.net.Uri$Builder;
import android.view.ViewGroup;
import java.io.FileNotFoundException;
import android.view.View;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;
import android.widget.ImageView;
import android.content.res.Resources$NotFoundException;
import androidx.core.content.ContextCompat;
import android.net.Uri;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager$NameNotFoundException;
import android.util.Log;
import android.content.ComponentName;
import android.text.style.TextAppearanceSpan;
import android.text.SpannableString;
import androidx.appcompat.R;
import android.util.TypedValue;
import android.graphics.drawable.Drawable;
import android.database.Cursor;
import android.content.res.ColorStateList;
import android.app.SearchableInfo;
import android.app.SearchManager;
import android.content.Context;
import android.graphics.drawable.Drawable$ConstantState;
import java.util.WeakHashMap;
import android.view.View$OnClickListener;
import androidx.cursoradapter.widget.ResourceCursorAdapter;

class SuggestionsAdapter extends ResourceCursorAdapter implements View$OnClickListener
{
    private static final boolean DBG = false;
    static final int INVALID_INDEX = -1;
    private static final String LOG_TAG = "SuggestionsAdapter";
    private static final int QUERY_LIMIT = 50;
    static final int REFINE_ALL = 2;
    static final int REFINE_BY_ENTRY = 1;
    static final int REFINE_NONE = 0;
    private boolean mClosed;
    private final int mCommitIconResId;
    private int mFlagsCol;
    private int mIconName1Col;
    private int mIconName2Col;
    private final WeakHashMap<String, Drawable$ConstantState> mOutsideDrawablesCache;
    private final Context mProviderContext;
    private int mQueryRefinement;
    private final SearchManager mSearchManager;
    private final SearchView mSearchView;
    private final SearchableInfo mSearchable;
    private int mText1Col;
    private int mText2Col;
    private int mText2UrlCol;
    private ColorStateList mUrlColor;
    
    public SuggestionsAdapter(final Context mProviderContext, final SearchView mSearchView, final SearchableInfo mSearchable, final WeakHashMap<String, Drawable$ConstantState> mOutsideDrawablesCache) {
        super(mProviderContext, mSearchView.getSuggestionRowLayout(), null, true);
        this.mClosed = false;
        this.mQueryRefinement = 1;
        this.mText1Col = -1;
        this.mText2Col = -1;
        this.mText2UrlCol = -1;
        this.mIconName1Col = -1;
        this.mIconName2Col = -1;
        this.mFlagsCol = -1;
        this.mSearchManager = (SearchManager)this.mContext.getSystemService("search");
        this.mSearchView = mSearchView;
        this.mSearchable = mSearchable;
        this.mCommitIconResId = mSearchView.getSuggestionCommitIconResId();
        this.mProviderContext = mProviderContext;
        this.mOutsideDrawablesCache = mOutsideDrawablesCache;
    }
    
    private Drawable checkIconCache(final String key) {
        final Drawable$ConstantState drawable$ConstantState = this.mOutsideDrawablesCache.get(key);
        if (drawable$ConstantState == null) {
            return null;
        }
        return drawable$ConstantState.newDrawable();
    }
    
    private CharSequence formatUrl(final CharSequence charSequence) {
        if (this.mUrlColor == null) {
            final TypedValue typedValue = new TypedValue();
            this.mContext.getTheme().resolveAttribute(R.attr.textColorSearchUrl, typedValue, true);
            this.mUrlColor = this.mContext.getResources().getColorStateList(typedValue.resourceId);
        }
        final SpannableString spannableString = new SpannableString(charSequence);
        spannableString.setSpan((Object)new TextAppearanceSpan((String)null, 0, 0, this.mUrlColor, (ColorStateList)null), 0, charSequence.length(), 33);
        return (CharSequence)spannableString;
    }
    
    private Drawable getActivityIcon(final ComponentName componentName) {
        final PackageManager packageManager = this.mContext.getPackageManager();
        try {
            final ActivityInfo activityInfo = packageManager.getActivityInfo(componentName, 128);
            final int iconResource = activityInfo.getIconResource();
            if (iconResource == 0) {
                return null;
            }
            final Drawable drawable = packageManager.getDrawable(componentName.getPackageName(), iconResource, activityInfo.applicationInfo);
            if (drawable == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid icon resource ");
                sb.append(iconResource);
                sb.append(" for ");
                sb.append(componentName.flattenToShortString());
                Log.w("SuggestionsAdapter", sb.toString());
                return null;
            }
            return drawable;
        }
        catch (PackageManager$NameNotFoundException ex) {
            Log.w("SuggestionsAdapter", ex.toString());
            return null;
        }
    }
    
    private Drawable getActivityIconWithCache(final ComponentName componentName) {
        final String flattenToShortString = componentName.flattenToShortString();
        final boolean containsKey = this.mOutsideDrawablesCache.containsKey(flattenToShortString);
        final Drawable$ConstantState drawable$ConstantState = null;
        if (!containsKey) {
            final Drawable activityIcon = this.getActivityIcon(componentName);
            Drawable$ConstantState constantState;
            if (activityIcon == null) {
                constantState = drawable$ConstantState;
            }
            else {
                constantState = activityIcon.getConstantState();
            }
            this.mOutsideDrawablesCache.put(flattenToShortString, constantState);
            return activityIcon;
        }
        final Drawable$ConstantState drawable$ConstantState2 = this.mOutsideDrawablesCache.get(flattenToShortString);
        if (drawable$ConstantState2 == null) {
            return null;
        }
        return drawable$ConstantState2.newDrawable(this.mProviderContext.getResources());
    }
    
    public static String getColumnString(final Cursor cursor, final String s) {
        return getStringOrNull(cursor, cursor.getColumnIndex(s));
    }
    
    private Drawable getDefaultIcon1(final Cursor cursor) {
        final Drawable activityIconWithCache = this.getActivityIconWithCache(this.mSearchable.getSearchActivity());
        if (activityIconWithCache != null) {
            return activityIconWithCache;
        }
        return this.mContext.getPackageManager().getDefaultActivityIcon();
    }
    
    private Drawable getDrawable(final Uri p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: aload_1        
        //     4: invokevirtual   android/net/Uri.getScheme:()Ljava/lang/String;
        //     7: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    10: istore_2       
        //    11: iload_2        
        //    12: ifeq            57
        //    15: aload_0        
        //    16: aload_1        
        //    17: invokevirtual   androidx/appcompat/widget/SuggestionsAdapter.getDrawableFromResourceUri:(Landroid/net/Uri;)Landroid/graphics/drawable/Drawable;
        //    20: astore_3       
        //    21: aload_3        
        //    22: areturn        
        //    23: new             Ljava/lang/StringBuilder;
        //    26: dup            
        //    27: invokespecial   java/lang/StringBuilder.<init>:()V
        //    30: astore_3       
        //    31: aload_3        
        //    32: ldc_w           "Resource does not exist: "
        //    35: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    38: pop            
        //    39: aload_3        
        //    40: aload_1        
        //    41: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //    44: pop            
        //    45: new             Ljava/io/FileNotFoundException;
        //    48: dup            
        //    49: aload_3        
        //    50: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    53: invokespecial   java/io/FileNotFoundException.<init>:(Ljava/lang/String;)V
        //    56: athrow         
        //    57: aload_0        
        //    58: getfield        androidx/appcompat/widget/SuggestionsAdapter.mProviderContext:Landroid/content/Context;
        //    61: invokevirtual   android/content/Context.getContentResolver:()Landroid/content/ContentResolver;
        //    64: aload_1        
        //    65: invokevirtual   android/content/ContentResolver.openInputStream:(Landroid/net/Uri;)Ljava/io/InputStream;
        //    68: astore          4
        //    70: aload           4
        //    72: ifnull          182
        //    75: aload           4
        //    77: aconst_null    
        //    78: invokestatic    android/graphics/drawable/Drawable.createFromStream:(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
        //    81: astore_3       
        //    82: aload           4
        //    84: invokevirtual   java/io/InputStream.close:()V
        //    87: aload_3        
        //    88: areturn        
        //    89: astore          4
        //    91: new             Ljava/lang/StringBuilder;
        //    94: dup            
        //    95: invokespecial   java/lang/StringBuilder.<init>:()V
        //    98: astore          5
        //   100: aload           5
        //   102: ldc_w           "Error closing icon stream for "
        //   105: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   108: pop            
        //   109: aload           5
        //   111: aload_1        
        //   112: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   115: pop            
        //   116: ldc             "SuggestionsAdapter"
        //   118: aload           5
        //   120: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   123: aload           4
        //   125: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   128: pop            
        //   129: aload_3        
        //   130: areturn        
        //   131: astore_3       
        //   132: aload           4
        //   134: invokevirtual   java/io/InputStream.close:()V
        //   137: goto            180
        //   140: astore          4
        //   142: new             Ljava/lang/StringBuilder;
        //   145: dup            
        //   146: invokespecial   java/lang/StringBuilder.<init>:()V
        //   149: astore          5
        //   151: aload           5
        //   153: ldc_w           "Error closing icon stream for "
        //   156: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   159: pop            
        //   160: aload           5
        //   162: aload_1        
        //   163: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   166: pop            
        //   167: ldc             "SuggestionsAdapter"
        //   169: aload           5
        //   171: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   174: aload           4
        //   176: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   179: pop            
        //   180: aload_3        
        //   181: athrow         
        //   182: new             Ljava/lang/StringBuilder;
        //   185: dup            
        //   186: invokespecial   java/lang/StringBuilder.<init>:()V
        //   189: astore_3       
        //   190: aload_3        
        //   191: ldc_w           "Failed to open "
        //   194: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   197: pop            
        //   198: aload_3        
        //   199: aload_1        
        //   200: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   203: pop            
        //   204: new             Ljava/io/FileNotFoundException;
        //   207: dup            
        //   208: aload_3        
        //   209: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   212: invokespecial   java/io/FileNotFoundException.<init>:(Ljava/lang/String;)V
        //   215: athrow         
        //   216: astore_3       
        //   217: new             Ljava/lang/StringBuilder;
        //   220: dup            
        //   221: invokespecial   java/lang/StringBuilder.<init>:()V
        //   224: astore          4
        //   226: aload           4
        //   228: ldc_w           "Icon not found: "
        //   231: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   234: pop            
        //   235: aload           4
        //   237: aload_1        
        //   238: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   241: pop            
        //   242: aload           4
        //   244: ldc_w           ", "
        //   247: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   250: pop            
        //   251: aload           4
        //   253: aload_3        
        //   254: invokevirtual   java/io/FileNotFoundException.getMessage:()Ljava/lang/String;
        //   257: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   260: pop            
        //   261: ldc             "SuggestionsAdapter"
        //   263: aload           4
        //   265: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   268: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //   271: pop            
        //   272: aconst_null    
        //   273: areturn        
        //   274: astore_3       
        //   275: goto            23
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                             
        //  -----  -----  -----  -----  -------------------------------------------------
        //  0      11     216    274    Ljava/io/FileNotFoundException;
        //  15     21     274    57     Landroid/content/res/Resources$NotFoundException;
        //  15     21     216    274    Ljava/io/FileNotFoundException;
        //  23     57     216    274    Ljava/io/FileNotFoundException;
        //  57     70     216    274    Ljava/io/FileNotFoundException;
        //  75     82     131    182    Any
        //  82     87     89     131    Ljava/io/IOException;
        //  82     87     216    274    Ljava/io/FileNotFoundException;
        //  91     129    216    274    Ljava/io/FileNotFoundException;
        //  132    137    140    180    Ljava/io/IOException;
        //  132    137    216    274    Ljava/io/FileNotFoundException;
        //  142    180    216    274    Ljava/io/FileNotFoundException;
        //  180    182    216    274    Ljava/io/FileNotFoundException;
        //  182    216    216    274    Ljava/io/FileNotFoundException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0023:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private Drawable getDrawableFromResourceValue(final String str) {
        Drawable drawable2;
        final Drawable drawable = drawable2 = null;
        if (str == null) {
            return drawable2;
        }
        drawable2 = drawable;
        if (str.isEmpty()) {
            return drawable2;
        }
        if ("0".equals(str)) {
            return null;
        }
        while (true) {
            while (true) {
                try {
                    final int int1 = Integer.parseInt(str);
                    final StringBuilder sb = new StringBuilder();
                    sb.append("android.resource://");
                    sb.append(this.mProviderContext.getPackageName());
                    sb.append("/");
                    sb.append(int1);
                    final String string = sb.toString();
                    final Drawable checkIconCache = this.checkIconCache(string);
                    if (checkIconCache != null) {
                        return checkIconCache;
                    }
                    final Drawable drawable3 = ContextCompat.getDrawable(this.mProviderContext, int1);
                    this.storeInIconCache(string, drawable3);
                    return drawable3;
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Icon resource not found: ");
                    sb2.append(str);
                    Log.w("SuggestionsAdapter", sb2.toString());
                    return null;
                    drawable2 = this.getDrawable(Uri.parse(str));
                    this.storeInIconCache(str, drawable2);
                    return drawable2;
                }
                catch (NumberFormatException ex) {}
                catch (Resources$NotFoundException ex2) {}
                continue;
            }
        }
    }
    
    private Drawable getIcon1(final Cursor cursor) {
        final int mIconName1Col = this.mIconName1Col;
        if (mIconName1Col == -1) {
            return null;
        }
        final Drawable drawableFromResourceValue = this.getDrawableFromResourceValue(cursor.getString(mIconName1Col));
        if (drawableFromResourceValue != null) {
            return drawableFromResourceValue;
        }
        return this.getDefaultIcon1(cursor);
    }
    
    private Drawable getIcon2(final Cursor cursor) {
        final int mIconName2Col = this.mIconName2Col;
        if (mIconName2Col == -1) {
            return null;
        }
        return this.getDrawableFromResourceValue(cursor.getString(mIconName2Col));
    }
    
    private static String getStringOrNull(final Cursor cursor, final int n) {
        if (n == -1) {
            return null;
        }
        try {
            return cursor.getString(n);
        }
        catch (Exception ex) {
            Log.e("SuggestionsAdapter", "unexpected error retrieving valid column from cursor, did the remote process die?", (Throwable)ex);
            return null;
        }
    }
    
    private void setViewDrawable(final ImageView imageView, final Drawable imageDrawable, final int visibility) {
        imageView.setImageDrawable(imageDrawable);
        if (imageDrawable == null) {
            imageView.setVisibility(visibility);
            return;
        }
        imageView.setVisibility(0);
        imageDrawable.setVisible(false, false);
        imageDrawable.setVisible(true, false);
    }
    
    private void setViewText(final TextView textView, final CharSequence text) {
        textView.setText(text);
        if (TextUtils.isEmpty(text)) {
            textView.setVisibility(8);
            return;
        }
        textView.setVisibility(0);
    }
    
    private void storeInIconCache(final String key, final Drawable drawable) {
        if (drawable != null) {
            this.mOutsideDrawablesCache.put(key, drawable.getConstantState());
        }
    }
    
    private void updateSpinnerState(final Cursor cursor) {
        Bundle extras;
        if (cursor != null) {
            extras = cursor.getExtras();
        }
        else {
            extras = null;
        }
        if (extras == null || extras.getBoolean("in_progress")) {}
    }
    
    public void bindView(final View view, final Context context, final Cursor cursor) {
        final ChildViewCache childViewCache = (ChildViewCache)view.getTag();
        final int mFlagsCol = this.mFlagsCol;
        int int1;
        if (mFlagsCol != -1) {
            int1 = cursor.getInt(mFlagsCol);
        }
        else {
            int1 = 0;
        }
        if (childViewCache.mText1 != null) {
            this.setViewText(childViewCache.mText1, getStringOrNull(cursor, this.mText1Col));
        }
        if (childViewCache.mText2 != null) {
            final String stringOrNull = getStringOrNull(cursor, this.mText2UrlCol);
            CharSequence charSequence;
            if (stringOrNull != null) {
                charSequence = this.formatUrl(stringOrNull);
            }
            else {
                charSequence = getStringOrNull(cursor, this.mText2Col);
            }
            if (TextUtils.isEmpty(charSequence)) {
                if (childViewCache.mText1 != null) {
                    childViewCache.mText1.setSingleLine(false);
                    childViewCache.mText1.setMaxLines(2);
                }
            }
            else if (childViewCache.mText1 != null) {
                childViewCache.mText1.setSingleLine(true);
                childViewCache.mText1.setMaxLines(1);
            }
            this.setViewText(childViewCache.mText2, charSequence);
        }
        if (childViewCache.mIcon1 != null) {
            this.setViewDrawable(childViewCache.mIcon1, this.getIcon1(cursor), 4);
        }
        if (childViewCache.mIcon2 != null) {
            this.setViewDrawable(childViewCache.mIcon2, this.getIcon2(cursor), 8);
        }
        final int mQueryRefinement = this.mQueryRefinement;
        if (mQueryRefinement != 2 && (mQueryRefinement != 1 || (int1 & 0x1) == 0x0)) {
            childViewCache.mIconRefine.setVisibility(8);
            return;
        }
        childViewCache.mIconRefine.setVisibility(0);
        childViewCache.mIconRefine.setTag((Object)childViewCache.mText1.getText());
        childViewCache.mIconRefine.setOnClickListener((View$OnClickListener)this);
    }
    
    public void changeCursor(final Cursor cursor) {
        if (this.mClosed) {
            Log.w("SuggestionsAdapter", "Tried to change cursor after adapter was closed.");
            if (cursor != null) {
                cursor.close();
            }
            return;
        }
        try {
            super.changeCursor(cursor);
            if (cursor != null) {
                this.mText1Col = cursor.getColumnIndex("suggest_text_1");
                this.mText2Col = cursor.getColumnIndex("suggest_text_2");
                this.mText2UrlCol = cursor.getColumnIndex("suggest_text_2_url");
                this.mIconName1Col = cursor.getColumnIndex("suggest_icon_1");
                this.mIconName2Col = cursor.getColumnIndex("suggest_icon_2");
                this.mFlagsCol = cursor.getColumnIndex("suggest_flags");
            }
        }
        catch (Exception ex) {
            Log.e("SuggestionsAdapter", "error changing cursor and caching columns", (Throwable)ex);
        }
    }
    
    public void close() {
        this.changeCursor(null);
        this.mClosed = true;
    }
    
    public CharSequence convertToString(final Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        final String columnString = getColumnString(cursor, "suggest_intent_query");
        if (columnString != null) {
            return columnString;
        }
        if (this.mSearchable.shouldRewriteQueryFromData()) {
            final String columnString2 = getColumnString(cursor, "suggest_intent_data");
            if (columnString2 != null) {
                return columnString2;
            }
        }
        if (this.mSearchable.shouldRewriteQueryFromText()) {
            final String columnString3 = getColumnString(cursor, "suggest_text_1");
            if (columnString3 != null) {
                return columnString3;
            }
        }
        return null;
    }
    
    Drawable getDrawableFromResourceUri(final Uri p0) throws FileNotFoundException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     4: astore_3       
        //     5: aload_3        
        //     6: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //     9: ifne            282
        //    12: aload_0        
        //    13: getfield        androidx/appcompat/widget/SuggestionsAdapter.mContext:Landroid/content/Context;
        //    16: invokevirtual   android/content/Context.getPackageManager:()Landroid/content/pm/PackageManager;
        //    19: aload_3        
        //    20: invokevirtual   android/content/pm/PackageManager.getResourcesForApplication:(Ljava/lang/String;)Landroid/content/res/Resources;
        //    23: astore          4
        //    25: aload_1        
        //    26: invokevirtual   android/net/Uri.getPathSegments:()Ljava/util/List;
        //    29: astore          5
        //    31: aload           5
        //    33: ifnull          214
        //    36: aload           5
        //    38: invokeinterface java/util/List.size:()I
        //    43: istore_2       
        //    44: iload_2        
        //    45: iconst_1       
        //    46: if_icmpne       101
        //    49: aload           5
        //    51: iconst_0       
        //    52: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //    57: checkcast       Ljava/lang/String;
        //    60: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //    63: istore_2       
        //    64: goto            135
        //    67: new             Ljava/lang/StringBuilder;
        //    70: dup            
        //    71: invokespecial   java/lang/StringBuilder.<init>:()V
        //    74: astore_3       
        //    75: aload_3        
        //    76: ldc_w           "Single path segment is not a resource ID: "
        //    79: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    82: pop            
        //    83: aload_3        
        //    84: aload_1        
        //    85: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //    88: pop            
        //    89: new             Ljava/io/FileNotFoundException;
        //    92: dup            
        //    93: aload_3        
        //    94: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    97: invokespecial   java/io/FileNotFoundException.<init>:(Ljava/lang/String;)V
        //   100: athrow         
        //   101: iload_2        
        //   102: iconst_2       
        //   103: if_icmpne       180
        //   106: aload           4
        //   108: aload           5
        //   110: iconst_1       
        //   111: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   116: checkcast       Ljava/lang/String;
        //   119: aload           5
        //   121: iconst_0       
        //   122: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   127: checkcast       Ljava/lang/String;
        //   130: aload_3        
        //   131: invokevirtual   android/content/res/Resources.getIdentifier:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
        //   134: istore_2       
        //   135: iload_2        
        //   136: ifeq            146
        //   139: aload           4
        //   141: iload_2        
        //   142: invokevirtual   android/content/res/Resources.getDrawable:(I)Landroid/graphics/drawable/Drawable;
        //   145: areturn        
        //   146: new             Ljava/lang/StringBuilder;
        //   149: dup            
        //   150: invokespecial   java/lang/StringBuilder.<init>:()V
        //   153: astore_3       
        //   154: aload_3        
        //   155: ldc_w           "No resource found for: "
        //   158: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   161: pop            
        //   162: aload_3        
        //   163: aload_1        
        //   164: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   167: pop            
        //   168: new             Ljava/io/FileNotFoundException;
        //   171: dup            
        //   172: aload_3        
        //   173: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   176: invokespecial   java/io/FileNotFoundException.<init>:(Ljava/lang/String;)V
        //   179: athrow         
        //   180: new             Ljava/lang/StringBuilder;
        //   183: dup            
        //   184: invokespecial   java/lang/StringBuilder.<init>:()V
        //   187: astore_3       
        //   188: aload_3        
        //   189: ldc_w           "More than two path segments: "
        //   192: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   195: pop            
        //   196: aload_3        
        //   197: aload_1        
        //   198: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   201: pop            
        //   202: new             Ljava/io/FileNotFoundException;
        //   205: dup            
        //   206: aload_3        
        //   207: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   210: invokespecial   java/io/FileNotFoundException.<init>:(Ljava/lang/String;)V
        //   213: athrow         
        //   214: new             Ljava/lang/StringBuilder;
        //   217: dup            
        //   218: invokespecial   java/lang/StringBuilder.<init>:()V
        //   221: astore_3       
        //   222: aload_3        
        //   223: ldc_w           "No path: "
        //   226: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   229: pop            
        //   230: aload_3        
        //   231: aload_1        
        //   232: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   235: pop            
        //   236: new             Ljava/io/FileNotFoundException;
        //   239: dup            
        //   240: aload_3        
        //   241: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   244: invokespecial   java/io/FileNotFoundException.<init>:(Ljava/lang/String;)V
        //   247: athrow         
        //   248: new             Ljava/lang/StringBuilder;
        //   251: dup            
        //   252: invokespecial   java/lang/StringBuilder.<init>:()V
        //   255: astore_3       
        //   256: aload_3        
        //   257: ldc_w           "No package found for authority: "
        //   260: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   263: pop            
        //   264: aload_3        
        //   265: aload_1        
        //   266: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   269: pop            
        //   270: new             Ljava/io/FileNotFoundException;
        //   273: dup            
        //   274: aload_3        
        //   275: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   278: invokespecial   java/io/FileNotFoundException.<init>:(Ljava/lang/String;)V
        //   281: athrow         
        //   282: new             Ljava/lang/StringBuilder;
        //   285: dup            
        //   286: invokespecial   java/lang/StringBuilder.<init>:()V
        //   289: astore_3       
        //   290: aload_3        
        //   291: ldc_w           "No authority: "
        //   294: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   297: pop            
        //   298: aload_3        
        //   299: aload_1        
        //   300: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   303: pop            
        //   304: new             Ljava/io/FileNotFoundException;
        //   307: dup            
        //   308: aload_3        
        //   309: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   312: invokespecial   java/io/FileNotFoundException.<init>:(Ljava/lang/String;)V
        //   315: athrow         
        //   316: astore_3       
        //   317: goto            248
        //   320: astore_3       
        //   321: goto            67
        //    Exceptions:
        //  throws java.io.FileNotFoundException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                                     
        //  -----  -----  -----  -----  ---------------------------------------------------------
        //  12     25     316    282    Landroid/content/pm/PackageManager$NameNotFoundException;
        //  49     64     320    101    Ljava/lang/NumberFormatException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0067:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public View getDropDownView(final int n, View dropDownView, final ViewGroup viewGroup) {
        try {
            dropDownView = super.getDropDownView(n, dropDownView, viewGroup);
            return dropDownView;
        }
        catch (RuntimeException ex) {
            Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", (Throwable)ex);
            final View dropDownView2 = this.newDropDownView(this.mContext, this.mCursor, viewGroup);
            if (dropDownView2 != null) {
                ((ChildViewCache)dropDownView2.getTag()).mText1.setText((CharSequence)ex.toString());
            }
            return dropDownView2;
        }
    }
    
    public int getQueryRefinement() {
        return this.mQueryRefinement;
    }
    
    Cursor getSearchManagerSuggestions(final SearchableInfo searchableInfo, final String s, final int i) {
        final String[] array = null;
        if (searchableInfo == null) {
            return null;
        }
        final String suggestAuthority = searchableInfo.getSuggestAuthority();
        if (suggestAuthority == null) {
            return null;
        }
        final Uri$Builder fragment = new Uri$Builder().scheme("content").authority(suggestAuthority).query("").fragment("");
        final String suggestPath = searchableInfo.getSuggestPath();
        if (suggestPath != null) {
            fragment.appendEncodedPath(suggestPath);
        }
        fragment.appendPath("search_suggest_query");
        final String suggestSelection = searchableInfo.getSuggestSelection();
        String[] array2;
        if (suggestSelection != null) {
            array2 = new String[] { s };
        }
        else {
            fragment.appendPath(s);
            array2 = array;
        }
        if (i > 0) {
            fragment.appendQueryParameter("limit", String.valueOf(i));
        }
        return this.mContext.getContentResolver().query(fragment.build(), (String[])null, suggestSelection, array2, (String)null);
    }
    
    public View getView(final int n, View view, final ViewGroup viewGroup) {
        try {
            view = super.getView(n, view, viewGroup);
            return view;
        }
        catch (RuntimeException ex) {
            Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", (Throwable)ex);
            final View view2 = this.newView(this.mContext, this.mCursor, viewGroup);
            if (view2 != null) {
                ((ChildViewCache)view2.getTag()).mText1.setText((CharSequence)ex.toString());
            }
            return view2;
        }
    }
    
    public boolean hasStableIds() {
        return false;
    }
    
    @Override
    public View newView(final Context context, final Cursor cursor, final ViewGroup viewGroup) {
        final View view = super.newView(context, cursor, viewGroup);
        view.setTag((Object)new ChildViewCache(view));
        ((ImageView)view.findViewById(R.id.edit_query)).setImageResource(this.mCommitIconResId);
        return view;
    }
    
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        this.updateSpinnerState(this.getCursor());
    }
    
    public void notifyDataSetInvalidated() {
        super.notifyDataSetInvalidated();
        this.updateSpinnerState(this.getCursor());
    }
    
    public void onClick(final View view) {
        final Object tag = view.getTag();
        if (tag instanceof CharSequence) {
            this.mSearchView.onQueryRefine((CharSequence)tag);
        }
    }
    
    public Cursor runQueryOnBackgroundThread(final CharSequence charSequence) {
        String string;
        if (charSequence == null) {
            string = "";
        }
        else {
            string = charSequence.toString();
        }
        if (this.mSearchView.getVisibility() == 0) {
            if (this.mSearchView.getWindowVisibility() != 0) {
                return null;
            }
            try {
                final Cursor searchManagerSuggestions = this.getSearchManagerSuggestions(this.mSearchable, string, 50);
                if (searchManagerSuggestions != null) {
                    searchManagerSuggestions.getCount();
                    return searchManagerSuggestions;
                }
            }
            catch (RuntimeException ex) {
                Log.w("SuggestionsAdapter", "Search suggestions query threw an exception.", (Throwable)ex);
            }
        }
        return null;
    }
    
    public void setQueryRefinement(final int mQueryRefinement) {
        this.mQueryRefinement = mQueryRefinement;
    }
    
    private static final class ChildViewCache
    {
        public final ImageView mIcon1;
        public final ImageView mIcon2;
        public final ImageView mIconRefine;
        public final TextView mText1;
        public final TextView mText2;
        
        public ChildViewCache(final View view) {
            this.mText1 = (TextView)view.findViewById(16908308);
            this.mText2 = (TextView)view.findViewById(16908309);
            this.mIcon1 = (ImageView)view.findViewById(16908295);
            this.mIcon2 = (ImageView)view.findViewById(16908296);
            this.mIconRefine = (ImageView)view.findViewById(R.id.edit_query);
        }
    }
}
