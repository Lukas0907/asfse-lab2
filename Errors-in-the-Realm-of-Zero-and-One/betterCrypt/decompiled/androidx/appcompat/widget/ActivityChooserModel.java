// 
// Decompiled by Procyon v0.5.36
// 

package androidx.appcompat.widget;

import java.math.BigDecimal;
import android.content.ComponentName;
import java.util.Collections;
import java.util.Collection;
import android.os.AsyncTask;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashMap;
import android.content.Intent;
import android.content.Context;
import java.util.List;
import java.util.Map;
import android.database.DataSetObservable;

class ActivityChooserModel extends DataSetObservable
{
    static final String ATTRIBUTE_ACTIVITY = "activity";
    static final String ATTRIBUTE_TIME = "time";
    static final String ATTRIBUTE_WEIGHT = "weight";
    static final boolean DEBUG = false;
    private static final int DEFAULT_ACTIVITY_INFLATION = 5;
    private static final float DEFAULT_HISTORICAL_RECORD_WEIGHT = 1.0f;
    public static final String DEFAULT_HISTORY_FILE_NAME = "activity_choser_model_history.xml";
    public static final int DEFAULT_HISTORY_MAX_LENGTH = 50;
    private static final String HISTORY_FILE_EXTENSION = ".xml";
    private static final int INVALID_INDEX = -1;
    static final String LOG_TAG;
    static final String TAG_HISTORICAL_RECORD = "historical-record";
    static final String TAG_HISTORICAL_RECORDS = "historical-records";
    private static final Map<String, ActivityChooserModel> sDataModelRegistry;
    private static final Object sRegistryLock;
    private final List<ActivityResolveInfo> mActivities;
    private OnChooseActivityListener mActivityChoserModelPolicy;
    private ActivitySorter mActivitySorter;
    boolean mCanReadHistoricalData;
    final Context mContext;
    private final List<HistoricalRecord> mHistoricalRecords;
    private boolean mHistoricalRecordsChanged;
    final String mHistoryFileName;
    private int mHistoryMaxSize;
    private final Object mInstanceLock;
    private Intent mIntent;
    private boolean mReadShareHistoryCalled;
    private boolean mReloadActivities;
    
    static {
        LOG_TAG = ActivityChooserModel.class.getSimpleName();
        sRegistryLock = new Object();
        sDataModelRegistry = new HashMap<String, ActivityChooserModel>();
    }
    
    private ActivityChooserModel(final Context context, final String s) {
        this.mInstanceLock = new Object();
        this.mActivities = new ArrayList<ActivityResolveInfo>();
        this.mHistoricalRecords = new ArrayList<HistoricalRecord>();
        this.mActivitySorter = (ActivitySorter)new DefaultSorter();
        this.mHistoryMaxSize = 50;
        this.mCanReadHistoricalData = true;
        this.mReadShareHistoryCalled = false;
        this.mHistoricalRecordsChanged = true;
        this.mReloadActivities = false;
        this.mContext = context.getApplicationContext();
        if (!TextUtils.isEmpty((CharSequence)s) && !s.endsWith(".xml")) {
            final StringBuilder sb = new StringBuilder();
            sb.append(s);
            sb.append(".xml");
            this.mHistoryFileName = sb.toString();
            return;
        }
        this.mHistoryFileName = s;
    }
    
    private boolean addHistoricalRecord(final HistoricalRecord historicalRecord) {
        final boolean add = this.mHistoricalRecords.add(historicalRecord);
        if (add) {
            this.mHistoricalRecordsChanged = true;
            this.pruneExcessiveHistoricalRecordsIfNeeded();
            this.persistHistoricalDataIfNeeded();
            this.sortActivitiesIfNeeded();
            this.notifyChanged();
        }
        return add;
    }
    
    private void ensureConsistentState() {
        final boolean loadActivitiesIfNeeded = this.loadActivitiesIfNeeded();
        final boolean historicalDataIfNeeded = this.readHistoricalDataIfNeeded();
        this.pruneExcessiveHistoricalRecordsIfNeeded();
        if (loadActivitiesIfNeeded | historicalDataIfNeeded) {
            this.sortActivitiesIfNeeded();
            this.notifyChanged();
        }
    }
    
    public static ActivityChooserModel get(final Context context, final String s) {
        synchronized (ActivityChooserModel.sRegistryLock) {
            ActivityChooserModel activityChooserModel;
            if ((activityChooserModel = ActivityChooserModel.sDataModelRegistry.get(s)) == null) {
                activityChooserModel = new ActivityChooserModel(context, s);
                ActivityChooserModel.sDataModelRegistry.put(s, activityChooserModel);
            }
            return activityChooserModel;
        }
    }
    
    private boolean loadActivitiesIfNeeded() {
        final boolean mReloadActivities = this.mReloadActivities;
        int i = 0;
        if (mReloadActivities && this.mIntent != null) {
            this.mReloadActivities = false;
            this.mActivities.clear();
            for (List queryIntentActivities = this.mContext.getPackageManager().queryIntentActivities(this.mIntent, 0); i < queryIntentActivities.size(); ++i) {
                this.mActivities.add(new ActivityResolveInfo(queryIntentActivities.get(i)));
            }
            return true;
        }
        return false;
    }
    
    private void persistHistoricalDataIfNeeded() {
        if (!this.mReadShareHistoryCalled) {
            throw new IllegalStateException("No preceding call to #readHistoricalData");
        }
        if (!this.mHistoricalRecordsChanged) {
            return;
        }
        this.mHistoricalRecordsChanged = false;
        if (!TextUtils.isEmpty((CharSequence)this.mHistoryFileName)) {
            new PersistHistoryAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Object[] { new ArrayList(this.mHistoricalRecords), this.mHistoryFileName });
        }
    }
    
    private void pruneExcessiveHistoricalRecordsIfNeeded() {
        final int n = this.mHistoricalRecords.size() - this.mHistoryMaxSize;
        if (n <= 0) {
            return;
        }
        this.mHistoricalRecordsChanged = true;
        for (int i = 0; i < n; ++i) {
            final HistoricalRecord historicalRecord = this.mHistoricalRecords.remove(0);
        }
    }
    
    private boolean readHistoricalDataIfNeeded() {
        if (this.mCanReadHistoricalData && this.mHistoricalRecordsChanged && !TextUtils.isEmpty((CharSequence)this.mHistoryFileName)) {
            this.mCanReadHistoricalData = false;
            this.mReadShareHistoryCalled = true;
            this.readHistoricalDataImpl();
            return true;
        }
        return false;
    }
    
    private void readHistoricalDataImpl() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        androidx/appcompat/widget/ActivityChooserModel.mContext:Landroid/content/Context;
        //     4: aload_0        
        //     5: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
        //     8: invokevirtual   android/content/Context.openFileInput:(Ljava/lang/String;)Ljava/io/FileInputStream;
        //    11: astore_2       
        //    12: invokestatic    android/util/Xml.newPullParser:()Lorg/xmlpull/v1/XmlPullParser;
        //    15: astore_3       
        //    16: aload_3        
        //    17: aload_2        
        //    18: ldc_w           "UTF-8"
        //    21: invokeinterface org/xmlpull/v1/XmlPullParser.setInput:(Ljava/io/InputStream;Ljava/lang/String;)V
        //    26: iconst_0       
        //    27: istore_1       
        //    28: iload_1        
        //    29: iconst_1       
        //    30: if_icmpeq       48
        //    33: iload_1        
        //    34: iconst_2       
        //    35: if_icmpeq       48
        //    38: aload_3        
        //    39: invokeinterface org/xmlpull/v1/XmlPullParser.next:()I
        //    44: istore_1       
        //    45: goto            28
        //    48: ldc             "historical-records"
        //    50: aload_3        
        //    51: invokeinterface org/xmlpull/v1/XmlPullParser.getName:()Ljava/lang/String;
        //    56: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    59: ifeq            185
        //    62: aload_0        
        //    63: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoricalRecords:Ljava/util/List;
        //    66: astore          4
        //    68: aload           4
        //    70: invokeinterface java/util/List.clear:()V
        //    75: aload_3        
        //    76: invokeinterface org/xmlpull/v1/XmlPullParser.next:()I
        //    81: istore_1       
        //    82: iload_1        
        //    83: iconst_1       
        //    84: if_icmpne       96
        //    87: aload_2        
        //    88: ifnull          306
        //    91: aload_2        
        //    92: invokevirtual   java/io/FileInputStream.close:()V
        //    95: return         
        //    96: iload_1        
        //    97: iconst_3       
        //    98: if_icmpeq       75
        //   101: iload_1        
        //   102: iconst_4       
        //   103: if_icmpne       109
        //   106: goto            75
        //   109: ldc             "historical-record"
        //   111: aload_3        
        //   112: invokeinterface org/xmlpull/v1/XmlPullParser.getName:()Ljava/lang/String;
        //   117: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   120: ifeq            174
        //   123: aload           4
        //   125: new             Landroidx/appcompat/widget/ActivityChooserModel$HistoricalRecord;
        //   128: dup            
        //   129: aload_3        
        //   130: aconst_null    
        //   131: ldc             "activity"
        //   133: invokeinterface org/xmlpull/v1/XmlPullParser.getAttributeValue:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   138: aload_3        
        //   139: aconst_null    
        //   140: ldc             "time"
        //   142: invokeinterface org/xmlpull/v1/XmlPullParser.getAttributeValue:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   147: invokestatic    java/lang/Long.parseLong:(Ljava/lang/String;)J
        //   150: aload_3        
        //   151: aconst_null    
        //   152: ldc             "weight"
        //   154: invokeinterface org/xmlpull/v1/XmlPullParser.getAttributeValue:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   159: invokestatic    java/lang/Float.parseFloat:(Ljava/lang/String;)F
        //   162: invokespecial   androidx/appcompat/widget/ActivityChooserModel$HistoricalRecord.<init>:(Ljava/lang/String;JF)V
        //   165: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   170: pop            
        //   171: goto            75
        //   174: new             Lorg/xmlpull/v1/XmlPullParserException;
        //   177: dup            
        //   178: ldc_w           "Share records file not well-formed."
        //   181: invokespecial   org/xmlpull/v1/XmlPullParserException.<init>:(Ljava/lang/String;)V
        //   184: athrow         
        //   185: new             Lorg/xmlpull/v1/XmlPullParserException;
        //   188: dup            
        //   189: ldc_w           "Share records file does not start with historical-records tag."
        //   192: invokespecial   org/xmlpull/v1/XmlPullParserException.<init>:(Ljava/lang/String;)V
        //   195: athrow         
        //   196: astore_3       
        //   197: goto            307
        //   200: astore_3       
        //   201: getstatic       androidx/appcompat/widget/ActivityChooserModel.LOG_TAG:Ljava/lang/String;
        //   204: astore          4
        //   206: new             Ljava/lang/StringBuilder;
        //   209: dup            
        //   210: invokespecial   java/lang/StringBuilder.<init>:()V
        //   213: astore          5
        //   215: aload           5
        //   217: ldc_w           "Error reading historical recrod file: "
        //   220: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   223: pop            
        //   224: aload           5
        //   226: aload_0        
        //   227: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
        //   230: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   233: pop            
        //   234: aload           4
        //   236: aload           5
        //   238: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   241: aload_3        
        //   242: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   245: pop            
        //   246: aload_2        
        //   247: ifnull          306
        //   250: goto            91
        //   253: astore_3       
        //   254: getstatic       androidx/appcompat/widget/ActivityChooserModel.LOG_TAG:Ljava/lang/String;
        //   257: astore          4
        //   259: new             Ljava/lang/StringBuilder;
        //   262: dup            
        //   263: invokespecial   java/lang/StringBuilder.<init>:()V
        //   266: astore          5
        //   268: aload           5
        //   270: ldc_w           "Error reading historical recrod file: "
        //   273: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   276: pop            
        //   277: aload           5
        //   279: aload_0        
        //   280: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
        //   283: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   286: pop            
        //   287: aload           4
        //   289: aload           5
        //   291: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   294: aload_3        
        //   295: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
        //   298: pop            
        //   299: aload_2        
        //   300: ifnull          306
        //   303: goto            91
        //   306: return         
        //   307: aload_2        
        //   308: ifnull          315
        //   311: aload_2        
        //   312: invokevirtual   java/io/FileInputStream.close:()V
        //   315: aload_3        
        //   316: athrow         
        //   317: astore_2       
        //   318: return         
        //   319: astore_2       
        //   320: return         
        //   321: astore_2       
        //   322: goto            315
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                   
        //  -----  -----  -----  -----  ---------------------------------------
        //  0      12     317    319    Ljava/io/FileNotFoundException;
        //  12     26     253    306    Lorg/xmlpull/v1/XmlPullParserException;
        //  12     26     200    253    Ljava/io/IOException;
        //  12     26     196    317    Any
        //  38     45     253    306    Lorg/xmlpull/v1/XmlPullParserException;
        //  38     45     200    253    Ljava/io/IOException;
        //  38     45     196    317    Any
        //  48     75     253    306    Lorg/xmlpull/v1/XmlPullParserException;
        //  48     75     200    253    Ljava/io/IOException;
        //  48     75     196    317    Any
        //  75     82     253    306    Lorg/xmlpull/v1/XmlPullParserException;
        //  75     82     200    253    Ljava/io/IOException;
        //  75     82     196    317    Any
        //  91     95     319    321    Ljava/io/IOException;
        //  109    171    253    306    Lorg/xmlpull/v1/XmlPullParserException;
        //  109    171    200    253    Ljava/io/IOException;
        //  109    171    196    317    Any
        //  174    185    253    306    Lorg/xmlpull/v1/XmlPullParserException;
        //  174    185    200    253    Ljava/io/IOException;
        //  174    185    196    317    Any
        //  185    196    253    306    Lorg/xmlpull/v1/XmlPullParserException;
        //  185    196    200    253    Ljava/io/IOException;
        //  185    196    196    317    Any
        //  201    246    196    317    Any
        //  254    299    196    317    Any
        //  311    315    321    325    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 153 out of bounds for length 153
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:373)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:426)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private boolean sortActivitiesIfNeeded() {
        if (this.mActivitySorter != null && this.mIntent != null && !this.mActivities.isEmpty() && !this.mHistoricalRecords.isEmpty()) {
            this.mActivitySorter.sort(this.mIntent, this.mActivities, Collections.unmodifiableList((List<? extends HistoricalRecord>)this.mHistoricalRecords));
            return true;
        }
        return false;
    }
    
    public Intent chooseActivity(final int n) {
        synchronized (this.mInstanceLock) {
            if (this.mIntent == null) {
                return null;
            }
            this.ensureConsistentState();
            final ActivityResolveInfo activityResolveInfo = this.mActivities.get(n);
            final ComponentName component = new ComponentName(activityResolveInfo.resolveInfo.activityInfo.packageName, activityResolveInfo.resolveInfo.activityInfo.name);
            final Intent intent = new Intent(this.mIntent);
            intent.setComponent(component);
            if (this.mActivityChoserModelPolicy != null && this.mActivityChoserModelPolicy.onChooseActivity(this, new Intent(intent))) {
                return null;
            }
            this.addHistoricalRecord(new HistoricalRecord(component, System.currentTimeMillis(), 1.0f));
            return intent;
        }
    }
    
    public ResolveInfo getActivity(final int n) {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            return this.mActivities.get(n).resolveInfo;
        }
    }
    
    public int getActivityCount() {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            return this.mActivities.size();
        }
    }
    
    public int getActivityIndex(final ResolveInfo resolveInfo) {
        while (true) {
            while (true) {
                int n;
                synchronized (this.mInstanceLock) {
                    this.ensureConsistentState();
                    final List<ActivityResolveInfo> mActivities = this.mActivities;
                    final int size = mActivities.size();
                    n = 0;
                    if (n >= size) {
                        return -1;
                    }
                    if (mActivities.get(n).resolveInfo == resolveInfo) {
                        return n;
                    }
                }
                ++n;
                continue;
            }
        }
    }
    
    public ResolveInfo getDefaultActivity() {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            if (!this.mActivities.isEmpty()) {
                return this.mActivities.get(0).resolveInfo;
            }
            return null;
        }
    }
    
    public int getHistoryMaxSize() {
        synchronized (this.mInstanceLock) {
            return this.mHistoryMaxSize;
        }
    }
    
    public int getHistorySize() {
        synchronized (this.mInstanceLock) {
            this.ensureConsistentState();
            return this.mHistoricalRecords.size();
        }
    }
    
    public Intent getIntent() {
        synchronized (this.mInstanceLock) {
            return this.mIntent;
        }
    }
    
    public void setActivitySorter(final ActivitySorter mActivitySorter) {
        synchronized (this.mInstanceLock) {
            if (this.mActivitySorter == mActivitySorter) {
                return;
            }
            this.mActivitySorter = mActivitySorter;
            if (this.sortActivitiesIfNeeded()) {
                this.notifyChanged();
            }
        }
    }
    
    public void setDefaultActivity(final int n) {
        while (true) {
            while (true) {
                synchronized (this.mInstanceLock) {
                    this.ensureConsistentState();
                    final ActivityResolveInfo activityResolveInfo = this.mActivities.get(n);
                    final ActivityResolveInfo activityResolveInfo2 = this.mActivities.get(0);
                    if (activityResolveInfo2 != null) {
                        final float n2 = activityResolveInfo2.weight - activityResolveInfo.weight + 5.0f;
                        this.addHistoricalRecord(new HistoricalRecord(new ComponentName(activityResolveInfo.resolveInfo.activityInfo.packageName, activityResolveInfo.resolveInfo.activityInfo.name), System.currentTimeMillis(), n2));
                        return;
                    }
                }
                final float n2 = 1.0f;
                continue;
            }
        }
    }
    
    public void setHistoryMaxSize(final int mHistoryMaxSize) {
        synchronized (this.mInstanceLock) {
            if (this.mHistoryMaxSize == mHistoryMaxSize) {
                return;
            }
            this.mHistoryMaxSize = mHistoryMaxSize;
            this.pruneExcessiveHistoricalRecordsIfNeeded();
            if (this.sortActivitiesIfNeeded()) {
                this.notifyChanged();
            }
        }
    }
    
    public void setIntent(final Intent mIntent) {
        synchronized (this.mInstanceLock) {
            if (this.mIntent == mIntent) {
                return;
            }
            this.mIntent = mIntent;
            this.mReloadActivities = true;
            this.ensureConsistentState();
        }
    }
    
    public void setOnChooseActivityListener(final OnChooseActivityListener mActivityChoserModelPolicy) {
        synchronized (this.mInstanceLock) {
            this.mActivityChoserModelPolicy = mActivityChoserModelPolicy;
        }
    }
    
    public interface ActivityChooserModelClient
    {
        void setActivityChooserModel(final ActivityChooserModel p0);
    }
    
    public static final class ActivityResolveInfo implements Comparable<ActivityResolveInfo>
    {
        public final ResolveInfo resolveInfo;
        public float weight;
        
        public ActivityResolveInfo(final ResolveInfo resolveInfo) {
            this.resolveInfo = resolveInfo;
        }
        
        @Override
        public int compareTo(final ActivityResolveInfo activityResolveInfo) {
            return Float.floatToIntBits(activityResolveInfo.weight) - Float.floatToIntBits(this.weight);
        }
        
        @Override
        public boolean equals(final Object o) {
            return this == o || (o != null && this.getClass() == o.getClass() && Float.floatToIntBits(this.weight) == Float.floatToIntBits(((ActivityResolveInfo)o).weight));
        }
        
        @Override
        public int hashCode() {
            return Float.floatToIntBits(this.weight) + 31;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append("resolveInfo:");
            sb.append(this.resolveInfo.toString());
            sb.append("; weight:");
            sb.append(new BigDecimal(this.weight));
            sb.append("]");
            return sb.toString();
        }
    }
    
    public interface ActivitySorter
    {
        void sort(final Intent p0, final List<ActivityResolveInfo> p1, final List<HistoricalRecord> p2);
    }
    
    private static final class DefaultSorter implements ActivitySorter
    {
        private static final float WEIGHT_DECAY_COEFFICIENT = 0.95f;
        private final Map<ComponentName, ActivityResolveInfo> mPackageNameToActivityMap;
        
        DefaultSorter() {
            this.mPackageNameToActivityMap = new HashMap<ComponentName, ActivityResolveInfo>();
        }
        
        @Override
        public void sort(final Intent intent, final List<ActivityResolveInfo> list, final List<HistoricalRecord> list2) {
            final Map<ComponentName, ActivityResolveInfo> mPackageNameToActivityMap = this.mPackageNameToActivityMap;
            mPackageNameToActivityMap.clear();
            for (int size = list.size(), i = 0; i < size; ++i) {
                final ActivityResolveInfo activityResolveInfo = list.get(i);
                activityResolveInfo.weight = 0.0f;
                mPackageNameToActivityMap.put(new ComponentName(activityResolveInfo.resolveInfo.activityInfo.packageName, activityResolveInfo.resolveInfo.activityInfo.name), activityResolveInfo);
            }
            int j = list2.size() - 1;
            float n = 1.0f;
            while (j >= 0) {
                final HistoricalRecord historicalRecord = list2.get(j);
                final ActivityResolveInfo activityResolveInfo2 = mPackageNameToActivityMap.get(historicalRecord.activity);
                float n2 = n;
                if (activityResolveInfo2 != null) {
                    activityResolveInfo2.weight += historicalRecord.weight * n;
                    n2 = n * 0.95f;
                }
                --j;
                n = n2;
            }
            Collections.sort((List<Comparable>)list);
        }
    }
    
    public static final class HistoricalRecord
    {
        public final ComponentName activity;
        public final long time;
        public final float weight;
        
        public HistoricalRecord(final ComponentName activity, final long time, final float weight) {
            this.activity = activity;
            this.time = time;
            this.weight = weight;
        }
        
        public HistoricalRecord(final String s, final long n, final float n2) {
            this(ComponentName.unflattenFromString(s), n, n2);
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null) {
                return false;
            }
            if (this.getClass() != o.getClass()) {
                return false;
            }
            final HistoricalRecord historicalRecord = (HistoricalRecord)o;
            final ComponentName activity = this.activity;
            if (activity == null) {
                if (historicalRecord.activity != null) {
                    return false;
                }
            }
            else if (!activity.equals((Object)historicalRecord.activity)) {
                return false;
            }
            return this.time == historicalRecord.time && Float.floatToIntBits(this.weight) == Float.floatToIntBits(historicalRecord.weight);
        }
        
        @Override
        public int hashCode() {
            final ComponentName activity = this.activity;
            int hashCode;
            if (activity == null) {
                hashCode = 0;
            }
            else {
                hashCode = activity.hashCode();
            }
            final long time = this.time;
            return ((hashCode + 31) * 31 + (int)(time ^ time >>> 32)) * 31 + Float.floatToIntBits(this.weight);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append("; activity:");
            sb.append(this.activity);
            sb.append("; time:");
            sb.append(this.time);
            sb.append("; weight:");
            sb.append(new BigDecimal(this.weight));
            sb.append("]");
            return sb.toString();
        }
    }
    
    public interface OnChooseActivityListener
    {
        boolean onChooseActivity(final ActivityChooserModel p0, final Intent p1);
    }
    
    private final class PersistHistoryAsyncTask extends AsyncTask<Object, Void, Void>
    {
        PersistHistoryAsyncTask() {
        }
        
        public Void doInBackground(final Object... p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: iconst_0       
            //     2: aaload         
            //     3: checkcast       Ljava/util/List;
            //     6: astore          4
            //     8: aload_1        
            //     9: iconst_1       
            //    10: aaload         
            //    11: checkcast       Ljava/lang/String;
            //    14: astore          5
            //    16: aload_0        
            //    17: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //    20: getfield        androidx/appcompat/widget/ActivityChooserModel.mContext:Landroid/content/Context;
            //    23: aload           5
            //    25: iconst_0       
            //    26: invokevirtual   android/content/Context.openFileOutput:(Ljava/lang/String;I)Ljava/io/FileOutputStream;
            //    29: astore_1       
            //    30: invokestatic    android/util/Xml.newSerializer:()Lorg/xmlpull/v1/XmlSerializer;
            //    33: astore          5
            //    35: aload           5
            //    37: aload_1        
            //    38: aconst_null    
            //    39: invokeinterface org/xmlpull/v1/XmlSerializer.setOutput:(Ljava/io/OutputStream;Ljava/lang/String;)V
            //    44: aload           5
            //    46: ldc             "UTF-8"
            //    48: iconst_1       
            //    49: invokestatic    java/lang/Boolean.valueOf:(Z)Ljava/lang/Boolean;
            //    52: invokeinterface org/xmlpull/v1/XmlSerializer.startDocument:(Ljava/lang/String;Ljava/lang/Boolean;)V
            //    57: aload           5
            //    59: aconst_null    
            //    60: ldc             "historical-records"
            //    62: invokeinterface org/xmlpull/v1/XmlSerializer.startTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //    67: pop            
            //    68: aload           4
            //    70: invokeinterface java/util/List.size:()I
            //    75: istore_3       
            //    76: iconst_0       
            //    77: istore_2       
            //    78: iload_2        
            //    79: iload_3        
            //    80: if_icmpge       182
            //    83: aload           4
            //    85: iconst_0       
            //    86: invokeinterface java/util/List.remove:(I)Ljava/lang/Object;
            //    91: checkcast       Landroidx/appcompat/widget/ActivityChooserModel$HistoricalRecord;
            //    94: astore          6
            //    96: aload           5
            //    98: aconst_null    
            //    99: ldc             "historical-record"
            //   101: invokeinterface org/xmlpull/v1/XmlSerializer.startTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   106: pop            
            //   107: aload           5
            //   109: aconst_null    
            //   110: ldc             "activity"
            //   112: aload           6
            //   114: getfield        androidx/appcompat/widget/ActivityChooserModel$HistoricalRecord.activity:Landroid/content/ComponentName;
            //   117: invokevirtual   android/content/ComponentName.flattenToString:()Ljava/lang/String;
            //   120: invokeinterface org/xmlpull/v1/XmlSerializer.attribute:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   125: pop            
            //   126: aload           5
            //   128: aconst_null    
            //   129: ldc             "time"
            //   131: aload           6
            //   133: getfield        androidx/appcompat/widget/ActivityChooserModel$HistoricalRecord.time:J
            //   136: invokestatic    java/lang/String.valueOf:(J)Ljava/lang/String;
            //   139: invokeinterface org/xmlpull/v1/XmlSerializer.attribute:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   144: pop            
            //   145: aload           5
            //   147: aconst_null    
            //   148: ldc             "weight"
            //   150: aload           6
            //   152: getfield        androidx/appcompat/widget/ActivityChooserModel$HistoricalRecord.weight:F
            //   155: invokestatic    java/lang/String.valueOf:(F)Ljava/lang/String;
            //   158: invokeinterface org/xmlpull/v1/XmlSerializer.attribute:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   163: pop            
            //   164: aload           5
            //   166: aconst_null    
            //   167: ldc             "historical-record"
            //   169: invokeinterface org/xmlpull/v1/XmlSerializer.endTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   174: pop            
            //   175: iload_2        
            //   176: iconst_1       
            //   177: iadd           
            //   178: istore_2       
            //   179: goto            78
            //   182: aload           5
            //   184: aconst_null    
            //   185: ldc             "historical-records"
            //   187: invokeinterface org/xmlpull/v1/XmlSerializer.endTag:(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
            //   192: pop            
            //   193: aload           5
            //   195: invokeinterface org/xmlpull/v1/XmlSerializer.endDocument:()V
            //   200: aload_0        
            //   201: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   204: iconst_1       
            //   205: putfield        androidx/appcompat/widget/ActivityChooserModel.mCanReadHistoricalData:Z
            //   208: aload_1        
            //   209: ifnull          418
            //   212: aload_1        
            //   213: invokevirtual   java/io/FileOutputStream.close:()V
            //   216: aconst_null    
            //   217: areturn        
            //   218: astore          4
            //   220: goto            420
            //   223: astore          4
            //   225: getstatic       androidx/appcompat/widget/ActivityChooserModel.LOG_TAG:Ljava/lang/String;
            //   228: astore          5
            //   230: new             Ljava/lang/StringBuilder;
            //   233: dup            
            //   234: invokespecial   java/lang/StringBuilder.<init>:()V
            //   237: astore          6
            //   239: aload           6
            //   241: ldc             "Error writing historical record file: "
            //   243: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   246: pop            
            //   247: aload           6
            //   249: aload_0        
            //   250: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   253: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
            //   256: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   259: pop            
            //   260: aload           5
            //   262: aload           6
            //   264: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   267: aload           4
            //   269: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   272: pop            
            //   273: aload_0        
            //   274: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   277: iconst_1       
            //   278: putfield        androidx/appcompat/widget/ActivityChooserModel.mCanReadHistoricalData:Z
            //   281: aload_1        
            //   282: ifnull          418
            //   285: goto            212
            //   288: astore          4
            //   290: getstatic       androidx/appcompat/widget/ActivityChooserModel.LOG_TAG:Ljava/lang/String;
            //   293: astore          5
            //   295: new             Ljava/lang/StringBuilder;
            //   298: dup            
            //   299: invokespecial   java/lang/StringBuilder.<init>:()V
            //   302: astore          6
            //   304: aload           6
            //   306: ldc             "Error writing historical record file: "
            //   308: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   311: pop            
            //   312: aload           6
            //   314: aload_0        
            //   315: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   318: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
            //   321: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   324: pop            
            //   325: aload           5
            //   327: aload           6
            //   329: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   332: aload           4
            //   334: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   337: pop            
            //   338: aload_0        
            //   339: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   342: iconst_1       
            //   343: putfield        androidx/appcompat/widget/ActivityChooserModel.mCanReadHistoricalData:Z
            //   346: aload_1        
            //   347: ifnull          418
            //   350: goto            212
            //   353: astore          4
            //   355: getstatic       androidx/appcompat/widget/ActivityChooserModel.LOG_TAG:Ljava/lang/String;
            //   358: astore          5
            //   360: new             Ljava/lang/StringBuilder;
            //   363: dup            
            //   364: invokespecial   java/lang/StringBuilder.<init>:()V
            //   367: astore          6
            //   369: aload           6
            //   371: ldc             "Error writing historical record file: "
            //   373: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   376: pop            
            //   377: aload           6
            //   379: aload_0        
            //   380: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   383: getfield        androidx/appcompat/widget/ActivityChooserModel.mHistoryFileName:Ljava/lang/String;
            //   386: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   389: pop            
            //   390: aload           5
            //   392: aload           6
            //   394: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   397: aload           4
            //   399: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   402: pop            
            //   403: aload_0        
            //   404: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   407: iconst_1       
            //   408: putfield        androidx/appcompat/widget/ActivityChooserModel.mCanReadHistoricalData:Z
            //   411: aload_1        
            //   412: ifnull          418
            //   415: goto            212
            //   418: aconst_null    
            //   419: areturn        
            //   420: aload_0        
            //   421: getfield        androidx/appcompat/widget/ActivityChooserModel$PersistHistoryAsyncTask.this$0:Landroidx/appcompat/widget/ActivityChooserModel;
            //   424: iconst_1       
            //   425: putfield        androidx/appcompat/widget/ActivityChooserModel.mCanReadHistoricalData:Z
            //   428: aload_1        
            //   429: ifnull          436
            //   432: aload_1        
            //   433: invokevirtual   java/io/FileOutputStream.close:()V
            //   436: aload           4
            //   438: athrow         
            //   439: astore_1       
            //   440: getstatic       androidx/appcompat/widget/ActivityChooserModel.LOG_TAG:Ljava/lang/String;
            //   443: astore          4
            //   445: new             Ljava/lang/StringBuilder;
            //   448: dup            
            //   449: invokespecial   java/lang/StringBuilder.<init>:()V
            //   452: astore          6
            //   454: aload           6
            //   456: ldc             "Error writing historical record file: "
            //   458: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   461: pop            
            //   462: aload           6
            //   464: aload           5
            //   466: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   469: pop            
            //   470: aload           4
            //   472: aload           6
            //   474: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   477: aload_1        
            //   478: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
            //   481: pop            
            //   482: aconst_null    
            //   483: areturn        
            //   484: astore_1       
            //   485: aconst_null    
            //   486: areturn        
            //   487: astore_1       
            //   488: goto            436
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                                
            //  -----  -----  -----  -----  ------------------------------------
            //  16     30     439    484    Ljava/io/FileNotFoundException;
            //  35     76     353    418    Ljava/lang/IllegalArgumentException;
            //  35     76     288    353    Ljava/lang/IllegalStateException;
            //  35     76     223    288    Ljava/io/IOException;
            //  35     76     218    439    Any
            //  83     175    353    418    Ljava/lang/IllegalArgumentException;
            //  83     175    288    353    Ljava/lang/IllegalStateException;
            //  83     175    223    288    Ljava/io/IOException;
            //  83     175    218    439    Any
            //  182    200    353    418    Ljava/lang/IllegalArgumentException;
            //  182    200    288    353    Ljava/lang/IllegalStateException;
            //  182    200    223    288    Ljava/io/IOException;
            //  182    200    218    439    Any
            //  212    216    484    487    Ljava/io/IOException;
            //  225    273    218    439    Any
            //  290    338    218    439    Any
            //  355    403    218    439    Any
            //  432    436    487    491    Ljava/io/IOException;
            // 
            // The error that occurred was:
            // 
            // java.lang.IndexOutOfBoundsException: Index 234 out of bounds for length 234
            //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
            //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
            //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
            //     at java.base/java.util.Objects.checkIndex(Objects.java:373)
            //     at java.base/java.util.ArrayList.get(ArrayList.java:426)
            //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
            //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:576)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
    }
}
