// 
// Decompiled by Procyon v0.5.36
// 

package androidx.appcompat.widget;

import android.view.ViewDebug$ExportedProperty;
import android.view.ContextThemeWrapper;
import android.content.res.Configuration;
import android.view.MenuItem;
import androidx.appcompat.view.menu.MenuItemImpl;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.accessibility.AccessibilityEvent;
import android.view.ViewGroup$LayoutParams;
import androidx.appcompat.view.menu.ActionMenuItemView;
import android.view.View$MeasureSpec;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import androidx.appcompat.view.menu.MenuPresenter;
import androidx.appcompat.view.menu.MenuView;
import androidx.appcompat.view.menu.MenuBuilder;

public class ActionMenuView extends LinearLayoutCompat implements ItemInvoker, MenuView
{
    static final int GENERATED_ITEM_PADDING = 4;
    static final int MIN_CELL_SIZE = 56;
    private static final String TAG = "ActionMenuView";
    private MenuPresenter.Callback mActionMenuPresenterCallback;
    private boolean mFormatItems;
    private int mFormatItemsWidth;
    private int mGeneratedItemPadding;
    private MenuBuilder mMenu;
    Callback mMenuBuilderCallback;
    private int mMinCellSize;
    OnMenuItemClickListener mOnMenuItemClickListener;
    private Context mPopupContext;
    private int mPopupTheme;
    private ActionMenuPresenter mPresenter;
    private boolean mReserveOverflow;
    
    public ActionMenuView(final Context context) {
        this(context, null);
    }
    
    public ActionMenuView(final Context mPopupContext, final AttributeSet set) {
        super(mPopupContext, set);
        this.setBaselineAligned(false);
        final float density = mPopupContext.getResources().getDisplayMetrics().density;
        this.mMinCellSize = (int)(56.0f * density);
        this.mGeneratedItemPadding = (int)(density * 4.0f);
        this.mPopupContext = mPopupContext;
        this.mPopupTheme = 0;
    }
    
    static int measureChildForCells(final View view, final int n, int cellsUsed, int n2, int n3) {
        final LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(View$MeasureSpec.getSize(n2) - n3, View$MeasureSpec.getMode(n2));
        ActionMenuItemView actionMenuItemView;
        if (view instanceof ActionMenuItemView) {
            actionMenuItemView = (ActionMenuItemView)view;
        }
        else {
            actionMenuItemView = null;
        }
        boolean expandable = true;
        if (actionMenuItemView != null && actionMenuItemView.hasText()) {
            n2 = 1;
        }
        else {
            n2 = 0;
        }
        n3 = 2;
        if (cellsUsed > 0 && (n2 == 0 || cellsUsed >= 2)) {
            view.measure(View$MeasureSpec.makeMeasureSpec(cellsUsed * n, Integer.MIN_VALUE), measureSpec);
            final int measuredWidth = view.getMeasuredWidth();
            final int n4 = cellsUsed = measuredWidth / n;
            if (measuredWidth % n != 0) {
                cellsUsed = n4 + 1;
            }
            if (n2 != 0 && cellsUsed < 2) {
                cellsUsed = n3;
            }
        }
        else {
            cellsUsed = 0;
        }
        if (layoutParams.isOverflowButton || n2 == 0) {
            expandable = false;
        }
        layoutParams.expandable = expandable;
        layoutParams.cellsUsed = cellsUsed;
        view.measure(View$MeasureSpec.makeMeasureSpec(n * cellsUsed, 1073741824), measureSpec);
        return cellsUsed;
    }
    
    private void onMeasureExactFormat(int i, int max) {
        final int mode = View$MeasureSpec.getMode(max);
        i = View$MeasureSpec.getSize(i);
        final int size = View$MeasureSpec.getSize(max);
        final int paddingLeft = this.getPaddingLeft();
        final int paddingRight = this.getPaddingRight();
        final int n = this.getPaddingTop() + this.getPaddingBottom();
        final int childMeasureSpec = getChildMeasureSpec(max, n, -2);
        final int n2 = i - (paddingLeft + paddingRight);
        i = this.mMinCellSize;
        final int n3 = n2 / i;
        if (n3 == 0) {
            this.setMeasuredDimension(n2, 0);
            return;
        }
        final int n4 = i + n2 % i / n3;
        final int childCount = this.getChildCount();
        int j = max = 0;
        final int n6;
        int n5 = n6 = max;
        int n7;
        i = (n7 = n6);
        long k = 0L;
        int max2 = i;
        i = n3;
        int n8 = n6;
        final int n9 = size;
        while (j < childCount) {
            final View child = this.getChildAt(j);
            if (child.getVisibility() != 8) {
                final boolean b = child instanceof ActionMenuItemView;
                ++n8;
                if (b) {
                    final int mGeneratedItemPadding = this.mGeneratedItemPadding;
                    child.setPadding(mGeneratedItemPadding, 0, mGeneratedItemPadding, 0);
                }
                final LayoutParams layoutParams = (LayoutParams)child.getLayoutParams();
                layoutParams.expanded = false;
                layoutParams.extraPixels = 0;
                layoutParams.cellsUsed = 0;
                layoutParams.expandable = false;
                layoutParams.leftMargin = 0;
                layoutParams.rightMargin = 0;
                layoutParams.preventEdgeOffset = (b && ((ActionMenuItemView)child).hasText());
                int n10;
                if (layoutParams.isOverflowButton) {
                    n10 = 1;
                }
                else {
                    n10 = i;
                }
                final int measureChildForCells = measureChildForCells(child, n4, n10, childMeasureSpec, n);
                max2 = Math.max(max2, measureChildForCells);
                int n11 = n7;
                if (layoutParams.expandable) {
                    n11 = n7 + 1;
                }
                if (layoutParams.isOverflowButton) {
                    n5 = 1;
                }
                i -= measureChildForCells;
                max = Math.max(max, child.getMeasuredHeight());
                if (measureChildForCells == 1) {
                    k |= 1 << j;
                }
                n7 = n11;
            }
            ++j;
        }
        final boolean b2 = n5 != 0 && n8 == 2;
        final int n12 = 0;
        int n13 = i;
        i = n12;
        final int n14 = n2;
        while (n7 > 0 && n13 > 0) {
            int n15 = Integer.MAX_VALUE;
            int l = 0;
            int n16 = 0;
            long n17 = 0L;
            while (l < childCount) {
                final LayoutParams layoutParams2 = (LayoutParams)this.getChildAt(l).getLayoutParams();
                int cellsUsed;
                int n18;
                long n19;
                if (!layoutParams2.expandable) {
                    cellsUsed = n15;
                    n18 = n16;
                    n19 = n17;
                }
                else if (layoutParams2.cellsUsed < n15) {
                    cellsUsed = layoutParams2.cellsUsed;
                    n19 = 1L << l;
                    n18 = 1;
                }
                else {
                    cellsUsed = n15;
                    n18 = n16;
                    n19 = n17;
                    if (layoutParams2.cellsUsed == n15) {
                        n19 = (n17 | 1L << l);
                        n18 = n16 + 1;
                        cellsUsed = n15;
                    }
                }
                ++l;
                n15 = cellsUsed;
                n16 = n18;
                n17 = n19;
            }
            k |= n17;
            if (n16 > n13) {
                break;
            }
            i = n15 + 1;
            for (int n20 = 0; n20 < childCount; ++n20) {
                final View child2 = this.getChildAt(n20);
                final LayoutParams layoutParams3 = (LayoutParams)child2.getLayoutParams();
                final long n21 = 1 << n20;
                if ((n17 & n21) == 0x0L) {
                    long n22 = k;
                    if (layoutParams3.cellsUsed == i) {
                        n22 = (k | n21);
                    }
                    k = n22;
                }
                else {
                    if (b2 && layoutParams3.preventEdgeOffset && n13 == 1) {
                        final int mGeneratedItemPadding2 = this.mGeneratedItemPadding;
                        child2.setPadding(mGeneratedItemPadding2 + n4, 0, mGeneratedItemPadding2, 0);
                    }
                    ++layoutParams3.cellsUsed;
                    layoutParams3.expanded = true;
                    --n13;
                }
            }
            i = 1;
        }
        final boolean b3 = n5 == 0 && n8 == 1;
        int n28;
        if (n13 > 0 && k != 0L && (n13 < n8 - 1 || b3 || max2 > 1)) {
            float n23 = (float)Long.bitCount(k);
            if (!b3) {
                float n24 = n23;
                if ((k & 0x1L) != 0x0L) {
                    n24 = n23;
                    if (!((LayoutParams)this.getChildAt(0).getLayoutParams()).preventEdgeOffset) {
                        n24 = n23 - 0.5f;
                    }
                }
                final int n25 = childCount - 1;
                n23 = n24;
                if ((k & (long)(1 << n25)) != 0x0L) {
                    n23 = n24;
                    if (!((LayoutParams)this.getChildAt(n25).getLayoutParams()).preventEdgeOffset) {
                        n23 = n24 - 0.5f;
                    }
                }
            }
            int n26;
            if (n23 > 0.0f) {
                n26 = (int)(n13 * n4 / n23);
            }
            else {
                n26 = 0;
            }
            int n27 = 0;
            while (true) {
                n28 = i;
                if (n27 >= childCount) {
                    break;
                }
                int n29;
                if ((k & (long)(1 << n27)) == 0x0L) {
                    n29 = i;
                }
                else {
                    final View child3 = this.getChildAt(n27);
                    final LayoutParams layoutParams4 = (LayoutParams)child3.getLayoutParams();
                    if (child3 instanceof ActionMenuItemView) {
                        layoutParams4.extraPixels = n26;
                        layoutParams4.expanded = true;
                        if (n27 == 0 && !layoutParams4.preventEdgeOffset) {
                            layoutParams4.leftMargin = -n26 / 2;
                        }
                        n29 = 1;
                    }
                    else if (layoutParams4.isOverflowButton) {
                        layoutParams4.extraPixels = n26;
                        layoutParams4.expanded = true;
                        layoutParams4.rightMargin = -n26 / 2;
                        n29 = 1;
                    }
                    else {
                        if (n27 != 0) {
                            layoutParams4.leftMargin = n26 / 2;
                        }
                        n29 = i;
                        if (n27 != childCount - 1) {
                            layoutParams4.rightMargin = n26 / 2;
                            n29 = i;
                        }
                    }
                }
                ++n27;
                i = n29;
            }
        }
        else {
            n28 = i;
        }
        i = 0;
        if (n28 != 0) {
            while (i < childCount) {
                final View child4 = this.getChildAt(i);
                final LayoutParams layoutParams5 = (LayoutParams)child4.getLayoutParams();
                if (layoutParams5.expanded) {
                    child4.measure(View$MeasureSpec.makeMeasureSpec(layoutParams5.cellsUsed * n4 + layoutParams5.extraPixels, 1073741824), childMeasureSpec);
                }
                ++i;
            }
        }
        if (mode == 1073741824) {
            max = n9;
        }
        this.setMeasuredDimension(n14, max);
    }
    
    @Override
    protected boolean checkLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return viewGroup$LayoutParams != null && viewGroup$LayoutParams instanceof LayoutParams;
    }
    
    public void dismissPopupMenus() {
        final ActionMenuPresenter mPresenter = this.mPresenter;
        if (mPresenter != null) {
            mPresenter.dismissPopupMenus();
        }
    }
    
    public boolean dispatchPopulateAccessibilityEvent(final AccessibilityEvent accessibilityEvent) {
        return false;
    }
    
    protected LayoutParams generateDefaultLayoutParams() {
        final LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.gravity = 16;
        return layoutParams;
    }
    
    public LayoutParams generateLayoutParams(final AttributeSet set) {
        return new LayoutParams(this.getContext(), set);
    }
    
    protected LayoutParams generateLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        if (viewGroup$LayoutParams != null) {
            LayoutParams layoutParams;
            if (viewGroup$LayoutParams instanceof LayoutParams) {
                layoutParams = new LayoutParams((LayoutParams)viewGroup$LayoutParams);
            }
            else {
                layoutParams = new LayoutParams(viewGroup$LayoutParams);
            }
            if (layoutParams.gravity <= 0) {
                layoutParams.gravity = 16;
            }
            return layoutParams;
        }
        return this.generateDefaultLayoutParams();
    }
    
    public LayoutParams generateOverflowButtonLayoutParams() {
        final LayoutParams generateDefaultLayoutParams = this.generateDefaultLayoutParams();
        generateDefaultLayoutParams.isOverflowButton = true;
        return generateDefaultLayoutParams;
    }
    
    public Menu getMenu() {
        if (this.mMenu == null) {
            final Context context = this.getContext();
            (this.mMenu = new MenuBuilder(context)).setCallback((MenuBuilder.Callback)new MenuBuilderCallback());
            (this.mPresenter = new ActionMenuPresenter(context)).setReserveOverflow(true);
            final ActionMenuPresenter mPresenter = this.mPresenter;
            MenuPresenter.Callback mActionMenuPresenterCallback = this.mActionMenuPresenterCallback;
            if (mActionMenuPresenterCallback == null) {
                mActionMenuPresenterCallback = new ActionMenuPresenterCallback();
            }
            mPresenter.setCallback(mActionMenuPresenterCallback);
            this.mMenu.addMenuPresenter(this.mPresenter, this.mPopupContext);
            this.mPresenter.setMenuView(this);
        }
        return (Menu)this.mMenu;
    }
    
    public Drawable getOverflowIcon() {
        this.getMenu();
        return this.mPresenter.getOverflowIcon();
    }
    
    public int getPopupTheme() {
        return this.mPopupTheme;
    }
    
    @Override
    public int getWindowAnimations() {
        return 0;
    }
    
    protected boolean hasSupportDividerBeforeChildAt(final int n) {
        final boolean b = false;
        if (n == 0) {
            return false;
        }
        final View child = this.getChildAt(n - 1);
        final View child2 = this.getChildAt(n);
        boolean b2 = b;
        if (n < this.getChildCount()) {
            b2 = b;
            if (child instanceof ActionMenuChildView) {
                b2 = (false | ((ActionMenuChildView)child).needsDividerAfter());
            }
        }
        boolean b3 = b2;
        if (n > 0) {
            b3 = b2;
            if (child2 instanceof ActionMenuChildView) {
                b3 = (b2 | ((ActionMenuChildView)child2).needsDividerBefore());
            }
        }
        return b3;
    }
    
    public boolean hideOverflowMenu() {
        final ActionMenuPresenter mPresenter = this.mPresenter;
        return mPresenter != null && mPresenter.hideOverflowMenu();
    }
    
    @Override
    public void initialize(final MenuBuilder mMenu) {
        this.mMenu = mMenu;
    }
    
    @Override
    public boolean invokeItem(final MenuItemImpl menuItemImpl) {
        return this.mMenu.performItemAction((MenuItem)menuItemImpl, 0);
    }
    
    public boolean isOverflowMenuShowPending() {
        final ActionMenuPresenter mPresenter = this.mPresenter;
        return mPresenter != null && mPresenter.isOverflowMenuShowPending();
    }
    
    public boolean isOverflowMenuShowing() {
        final ActionMenuPresenter mPresenter = this.mPresenter;
        return mPresenter != null && mPresenter.isOverflowMenuShowing();
    }
    
    public boolean isOverflowReserved() {
        return this.mReserveOverflow;
    }
    
    public void onConfigurationChanged(final Configuration configuration) {
        super.onConfigurationChanged(configuration);
        final ActionMenuPresenter mPresenter = this.mPresenter;
        if (mPresenter != null) {
            mPresenter.updateMenuView(false);
            if (this.mPresenter.isOverflowMenuShowing()) {
                this.mPresenter.hideOverflowMenu();
                this.mPresenter.showOverflowMenu();
            }
        }
    }
    
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.dismissPopupMenus();
    }
    
    @Override
    protected void onLayout(final boolean b, int i, int j, int n, int n2) {
        if (!this.mFormatItems) {
            super.onLayout(b, i, j, n, n2);
            return;
        }
        final int childCount = this.getChildCount();
        final int n3 = (n2 - j) / 2;
        final int dividerWidth = this.getDividerWidth();
        final int n4 = n - i;
        i = this.getPaddingRight();
        j = this.getPaddingLeft();
        final boolean layoutRtl = ViewUtils.isLayoutRtl((View)this);
        i = n4 - i - j;
        j = 0;
        n2 = 0;
        n = 0;
        while (j < childCount) {
            final View child = this.getChildAt(j);
            if (child.getVisibility() != 8) {
                final LayoutParams layoutParams = (LayoutParams)child.getLayoutParams();
                if (layoutParams.isOverflowButton) {
                    final int n5 = n2 = child.getMeasuredWidth();
                    if (this.hasSupportDividerBeforeChildAt(j)) {
                        n2 = n5 + dividerWidth;
                    }
                    final int measuredHeight = child.getMeasuredHeight();
                    int n6;
                    int n7;
                    if (layoutRtl) {
                        n6 = this.getPaddingLeft() + layoutParams.leftMargin;
                        n7 = n6 + n2;
                    }
                    else {
                        n7 = this.getWidth() - this.getPaddingRight() - layoutParams.rightMargin;
                        n6 = n7 - n2;
                    }
                    final int n8 = n3 - measuredHeight / 2;
                    child.layout(n6, n8, n7, measuredHeight + n8);
                    i -= n2;
                    n2 = 1;
                }
                else {
                    i -= child.getMeasuredWidth() + layoutParams.leftMargin + layoutParams.rightMargin;
                    this.hasSupportDividerBeforeChildAt(j);
                    ++n;
                }
            }
            ++j;
        }
        if (childCount == 1 && n2 == 0) {
            final View child2 = this.getChildAt(0);
            i = child2.getMeasuredWidth();
            j = child2.getMeasuredHeight();
            n = n4 / 2 - i / 2;
            n2 = n3 - j / 2;
            child2.layout(n, n2, i + n, j + n2);
            return;
        }
        j = n - (n2 ^ 0x1);
        if (j > 0) {
            i /= j;
        }
        else {
            i = 0;
        }
        n2 = 0;
        n = 0;
        final int max = Math.max(0, i);
        if (layoutRtl) {
            j = this.getWidth() - this.getPaddingRight();
            View child3;
            LayoutParams layoutParams2;
            int n9;
            for (i = n; i < childCount; ++i, j = n) {
                child3 = this.getChildAt(i);
                layoutParams2 = (LayoutParams)child3.getLayoutParams();
                n = j;
                if (child3.getVisibility() != 8) {
                    if (layoutParams2.isOverflowButton) {
                        n = j;
                    }
                    else {
                        j -= layoutParams2.rightMargin;
                        n = child3.getMeasuredWidth();
                        n2 = child3.getMeasuredHeight();
                        n9 = n3 - n2 / 2;
                        child3.layout(j - n, n9, j, n2 + n9);
                        n = j - (n + layoutParams2.leftMargin + max);
                    }
                }
            }
        }
        else {
            j = this.getPaddingLeft();
            View child4;
            LayoutParams layoutParams3;
            int n10;
            for (i = n2; i < childCount; ++i, j = n) {
                child4 = this.getChildAt(i);
                layoutParams3 = (LayoutParams)child4.getLayoutParams();
                n = j;
                if (child4.getVisibility() != 8) {
                    if (layoutParams3.isOverflowButton) {
                        n = j;
                    }
                    else {
                        j += layoutParams3.leftMargin;
                        n = child4.getMeasuredWidth();
                        n2 = child4.getMeasuredHeight();
                        n10 = n3 - n2 / 2;
                        child4.layout(j, n10, j + n, n2 + n10);
                        n = j + (n + layoutParams3.rightMargin + max);
                    }
                }
            }
        }
    }
    
    @Override
    protected void onMeasure(final int n, final int n2) {
        final boolean mFormatItems = this.mFormatItems;
        this.mFormatItems = (View$MeasureSpec.getMode(n) == 1073741824);
        if (mFormatItems != this.mFormatItems) {
            this.mFormatItemsWidth = 0;
        }
        final int size = View$MeasureSpec.getSize(n);
        if (this.mFormatItems) {
            final MenuBuilder mMenu = this.mMenu;
            if (mMenu != null && size != this.mFormatItemsWidth) {
                this.mFormatItemsWidth = size;
                mMenu.onItemsChanged(true);
            }
        }
        final int childCount = this.getChildCount();
        if (this.mFormatItems && childCount > 0) {
            this.onMeasureExactFormat(n, n2);
            return;
        }
        for (int i = 0; i < childCount; ++i) {
            final LayoutParams layoutParams = (LayoutParams)this.getChildAt(i).getLayoutParams();
            layoutParams.rightMargin = 0;
            layoutParams.leftMargin = 0;
        }
        super.onMeasure(n, n2);
    }
    
    public MenuBuilder peekMenu() {
        return this.mMenu;
    }
    
    public void setExpandedActionViewsExclusive(final boolean expandedActionViewsExclusive) {
        this.mPresenter.setExpandedActionViewsExclusive(expandedActionViewsExclusive);
    }
    
    public void setMenuCallbacks(final MenuPresenter.Callback mActionMenuPresenterCallback, final Callback mMenuBuilderCallback) {
        this.mActionMenuPresenterCallback = mActionMenuPresenterCallback;
        this.mMenuBuilderCallback = mMenuBuilderCallback;
    }
    
    public void setOnMenuItemClickListener(final OnMenuItemClickListener mOnMenuItemClickListener) {
        this.mOnMenuItemClickListener = mOnMenuItemClickListener;
    }
    
    public void setOverflowIcon(final Drawable overflowIcon) {
        this.getMenu();
        this.mPresenter.setOverflowIcon(overflowIcon);
    }
    
    public void setOverflowReserved(final boolean mReserveOverflow) {
        this.mReserveOverflow = mReserveOverflow;
    }
    
    public void setPopupTheme(final int mPopupTheme) {
        if (this.mPopupTheme != mPopupTheme) {
            if ((this.mPopupTheme = mPopupTheme) == 0) {
                this.mPopupContext = this.getContext();
                return;
            }
            this.mPopupContext = (Context)new ContextThemeWrapper(this.getContext(), mPopupTheme);
        }
    }
    
    public void setPresenter(final ActionMenuPresenter mPresenter) {
        (this.mPresenter = mPresenter).setMenuView(this);
    }
    
    public boolean showOverflowMenu() {
        final ActionMenuPresenter mPresenter = this.mPresenter;
        return mPresenter != null && mPresenter.showOverflowMenu();
    }
    
    public interface ActionMenuChildView
    {
        boolean needsDividerAfter();
        
        boolean needsDividerBefore();
    }
    
    private static class ActionMenuPresenterCallback implements MenuPresenter.Callback
    {
        ActionMenuPresenterCallback() {
        }
        
        @Override
        public void onCloseMenu(final MenuBuilder menuBuilder, final boolean b) {
        }
        
        @Override
        public boolean onOpenSubMenu(final MenuBuilder menuBuilder) {
            return false;
        }
    }
    
    public static class LayoutParams extends LinearLayoutCompat.LayoutParams
    {
        @ViewDebug$ExportedProperty
        public int cellsUsed;
        @ViewDebug$ExportedProperty
        public boolean expandable;
        boolean expanded;
        @ViewDebug$ExportedProperty
        public int extraPixels;
        @ViewDebug$ExportedProperty
        public boolean isOverflowButton;
        @ViewDebug$ExportedProperty
        public boolean preventEdgeOffset;
        
        public LayoutParams(final int n, final int n2) {
            super(n, n2);
            this.isOverflowButton = false;
        }
        
        LayoutParams(final int n, final int n2, final boolean isOverflowButton) {
            super(n, n2);
            this.isOverflowButton = isOverflowButton;
        }
        
        public LayoutParams(final Context context, final AttributeSet set) {
            super(context, set);
        }
        
        public LayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
        }
        
        public LayoutParams(final LayoutParams layoutParams) {
            super((ViewGroup$LayoutParams)layoutParams);
            this.isOverflowButton = layoutParams.isOverflowButton;
        }
    }
    
    private class MenuBuilderCallback implements Callback
    {
        MenuBuilderCallback() {
        }
        
        @Override
        public boolean onMenuItemSelected(final MenuBuilder menuBuilder, final MenuItem menuItem) {
            return ActionMenuView.this.mOnMenuItemClickListener != null && ActionMenuView.this.mOnMenuItemClickListener.onMenuItemClick(menuItem);
        }
        
        @Override
        public void onMenuModeChange(final MenuBuilder menuBuilder) {
            if (ActionMenuView.this.mMenuBuilderCallback != null) {
                ActionMenuView.this.mMenuBuilderCallback.onMenuModeChange(menuBuilder);
            }
        }
    }
    
    public interface OnMenuItemClickListener
    {
        boolean onMenuItemClick(final MenuItem p0);
    }
}
