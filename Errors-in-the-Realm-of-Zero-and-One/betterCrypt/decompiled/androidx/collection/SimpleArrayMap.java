// 
// Decompiled by Procyon v0.5.36
// 

package androidx.collection;

import java.util.ConcurrentModificationException;

public class SimpleArrayMap<K, V>
{
    private static final int BASE_SIZE = 4;
    private static final int CACHE_SIZE = 10;
    private static final boolean CONCURRENT_MODIFICATION_EXCEPTIONS = true;
    private static final boolean DEBUG = false;
    private static final String TAG = "ArrayMap";
    static Object[] mBaseCache;
    static int mBaseCacheSize;
    static Object[] mTwiceBaseCache;
    static int mTwiceBaseCacheSize;
    Object[] mArray;
    int[] mHashes;
    int mSize;
    
    public SimpleArrayMap() {
        this.mHashes = ContainerHelpers.EMPTY_INTS;
        this.mArray = ContainerHelpers.EMPTY_OBJECTS;
        this.mSize = 0;
    }
    
    public SimpleArrayMap(final int n) {
        if (n == 0) {
            this.mHashes = ContainerHelpers.EMPTY_INTS;
            this.mArray = ContainerHelpers.EMPTY_OBJECTS;
        }
        else {
            this.allocArrays(n);
        }
        this.mSize = 0;
    }
    
    public SimpleArrayMap(final SimpleArrayMap<K, V> simpleArrayMap) {
        this();
        if (simpleArrayMap != null) {
            this.putAll((SimpleArrayMap<? extends K, ? extends V>)simpleArrayMap);
        }
    }
    
    private void allocArrays(final int n) {
        Label_0161: {
            if (n == 8) {
                synchronized (ArrayMap.class) {
                    if (SimpleArrayMap.mTwiceBaseCache != null) {
                        final Object[] mTwiceBaseCache = SimpleArrayMap.mTwiceBaseCache;
                        this.mArray = mTwiceBaseCache;
                        SimpleArrayMap.mTwiceBaseCache = (Object[])mTwiceBaseCache[0];
                        this.mHashes = (int[])mTwiceBaseCache[1];
                        mTwiceBaseCache[0] = (mTwiceBaseCache[1] = null);
                        --SimpleArrayMap.mTwiceBaseCacheSize;
                        return;
                    }
                    break Label_0161;
                }
            }
            if (n == 4) {
                synchronized (ArrayMap.class) {
                    if (SimpleArrayMap.mBaseCache != null) {
                        final Object[] mBaseCache = SimpleArrayMap.mBaseCache;
                        this.mArray = mBaseCache;
                        SimpleArrayMap.mBaseCache = (Object[])mBaseCache[0];
                        this.mHashes = (int[])mBaseCache[1];
                        mBaseCache[0] = (mBaseCache[1] = null);
                        --SimpleArrayMap.mBaseCacheSize;
                        return;
                    }
                }
            }
        }
        this.mHashes = new int[n];
        this.mArray = new Object[n << 1];
    }
    
    private static int binarySearchHashes(final int[] array, int binarySearch, final int n) {
        try {
            binarySearch = ContainerHelpers.binarySearch(array, binarySearch, n);
            return binarySearch;
        }
        catch (ArrayIndexOutOfBoundsException ex) {
            throw new ConcurrentModificationException();
        }
    }
    
    private static void freeArrays(final int[] array, final Object[] array2, int i) {
        if (array.length == 8) {
            while (true) {
                while (true) {
                    Label_0118: {
                        synchronized (ArrayMap.class) {
                            if (SimpleArrayMap.mTwiceBaseCacheSize < 10) {
                                array2[0] = SimpleArrayMap.mTwiceBaseCache;
                                array2[1] = array;
                                i = (i << 1) - 1;
                                break Label_0118;
                            }
                            return;
                            SimpleArrayMap.mTwiceBaseCache = array2;
                            ++SimpleArrayMap.mTwiceBaseCacheSize;
                            return;
                        }
                        break;
                    }
                    while (i >= 2) {
                        array2[i] = null;
                        --i;
                    }
                    continue;
                }
            }
        }
        if (array.length == 4) {
            while (true) {
                while (true) {
                    Label_0134: {
                        synchronized (ArrayMap.class) {
                            if (SimpleArrayMap.mBaseCacheSize < 10) {
                                array2[0] = SimpleArrayMap.mBaseCache;
                                array2[1] = array;
                                i = (i << 1) - 1;
                                break Label_0134;
                            }
                            return;
                            SimpleArrayMap.mBaseCache = array2;
                            ++SimpleArrayMap.mBaseCacheSize;
                            return;
                        }
                        break;
                    }
                    while (i >= 2) {
                        array2[i] = null;
                        --i;
                    }
                    continue;
                }
            }
        }
    }
    
    public void clear() {
        final int mSize = this.mSize;
        if (mSize > 0) {
            final int[] mHashes = this.mHashes;
            final Object[] mArray = this.mArray;
            this.mHashes = ContainerHelpers.EMPTY_INTS;
            this.mArray = ContainerHelpers.EMPTY_OBJECTS;
            this.mSize = 0;
            freeArrays(mHashes, mArray, mSize);
        }
        if (this.mSize <= 0) {
            return;
        }
        throw new ConcurrentModificationException();
    }
    
    public boolean containsKey(final Object o) {
        return this.indexOfKey(o) >= 0;
    }
    
    public boolean containsValue(final Object o) {
        return this.indexOfValue(o) >= 0;
    }
    
    public void ensureCapacity(final int n) {
        final int mSize = this.mSize;
        final int[] mHashes = this.mHashes;
        if (mHashes.length < n) {
            final Object[] mArray = this.mArray;
            this.allocArrays(n);
            if (this.mSize > 0) {
                System.arraycopy(mHashes, 0, this.mHashes, 0, mSize);
                System.arraycopy(mArray, 0, this.mArray, 0, mSize << 1);
            }
            freeArrays(mHashes, mArray, mSize);
        }
        if (this.mSize == mSize) {
            return;
        }
        throw new ConcurrentModificationException();
    }
    
    @Override
    public boolean equals(final Object p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: aload_1        
        //     2: if_acmpne       7
        //     5: iconst_1       
        //     6: ireturn        
        //     7: aload_1        
        //     8: instanceof      Landroidx/collection/SimpleArrayMap;
        //    11: ifeq            108
        //    14: aload_1        
        //    15: checkcast       Landroidx/collection/SimpleArrayMap;
        //    18: astore_1       
        //    19: aload_0        
        //    20: invokevirtual   androidx/collection/SimpleArrayMap.size:()I
        //    23: aload_1        
        //    24: invokevirtual   androidx/collection/SimpleArrayMap.size:()I
        //    27: if_icmpeq       32
        //    30: iconst_0       
        //    31: ireturn        
        //    32: iconst_0       
        //    33: istore_2       
        //    34: iload_2        
        //    35: aload_0        
        //    36: getfield        androidx/collection/SimpleArrayMap.mSize:I
        //    39: if_icmpge       106
        //    42: aload_0        
        //    43: iload_2        
        //    44: invokevirtual   androidx/collection/SimpleArrayMap.keyAt:(I)Ljava/lang/Object;
        //    47: astore          4
        //    49: aload_0        
        //    50: iload_2        
        //    51: invokevirtual   androidx/collection/SimpleArrayMap.valueAt:(I)Ljava/lang/Object;
        //    54: astore          5
        //    56: aload_1        
        //    57: aload           4
        //    59: invokevirtual   androidx/collection/SimpleArrayMap.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    62: astore          6
        //    64: aload           5
        //    66: ifnonnull       85
        //    69: aload           6
        //    71: ifnonnull       223
        //    74: aload_1        
        //    75: aload           4
        //    77: invokevirtual   androidx/collection/SimpleArrayMap.containsKey:(Ljava/lang/Object;)Z
        //    80: ifne            99
        //    83: iconst_0       
        //    84: ireturn        
        //    85: aload           5
        //    87: aload           6
        //    89: invokevirtual   java/lang/Object.equals:(Ljava/lang/Object;)Z
        //    92: istore_3       
        //    93: iload_3        
        //    94: ifne            99
        //    97: iconst_0       
        //    98: ireturn        
        //    99: iload_2        
        //   100: iconst_1       
        //   101: iadd           
        //   102: istore_2       
        //   103: goto            34
        //   106: iconst_1       
        //   107: ireturn        
        //   108: aload_1        
        //   109: instanceof      Ljava/util/Map;
        //   112: ifeq            215
        //   115: aload_1        
        //   116: checkcast       Ljava/util/Map;
        //   119: astore_1       
        //   120: aload_0        
        //   121: invokevirtual   androidx/collection/SimpleArrayMap.size:()I
        //   124: aload_1        
        //   125: invokeinterface java/util/Map.size:()I
        //   130: if_icmpeq       135
        //   133: iconst_0       
        //   134: ireturn        
        //   135: iconst_0       
        //   136: istore_2       
        //   137: iload_2        
        //   138: aload_0        
        //   139: getfield        androidx/collection/SimpleArrayMap.mSize:I
        //   142: if_icmpge       213
        //   145: aload_0        
        //   146: iload_2        
        //   147: invokevirtual   androidx/collection/SimpleArrayMap.keyAt:(I)Ljava/lang/Object;
        //   150: astore          4
        //   152: aload_0        
        //   153: iload_2        
        //   154: invokevirtual   androidx/collection/SimpleArrayMap.valueAt:(I)Ljava/lang/Object;
        //   157: astore          5
        //   159: aload_1        
        //   160: aload           4
        //   162: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   167: astore          6
        //   169: aload           5
        //   171: ifnonnull       192
        //   174: aload           6
        //   176: ifnonnull       225
        //   179: aload_1        
        //   180: aload           4
        //   182: invokeinterface java/util/Map.containsKey:(Ljava/lang/Object;)Z
        //   187: ifne            206
        //   190: iconst_0       
        //   191: ireturn        
        //   192: aload           5
        //   194: aload           6
        //   196: invokevirtual   java/lang/Object.equals:(Ljava/lang/Object;)Z
        //   199: istore_3       
        //   200: iload_3        
        //   201: ifne            206
        //   204: iconst_0       
        //   205: ireturn        
        //   206: iload_2        
        //   207: iconst_1       
        //   208: iadd           
        //   209: istore_2       
        //   210: goto            137
        //   213: iconst_1       
        //   214: ireturn        
        //   215: iconst_0       
        //   216: ireturn        
        //   217: astore_1       
        //   218: iconst_0       
        //   219: ireturn        
        //   220: astore_1       
        //   221: iconst_0       
        //   222: ireturn        
        //   223: iconst_0       
        //   224: ireturn        
        //   225: iconst_0       
        //   226: ireturn        
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                            
        //  -----  -----  -----  -----  --------------------------------
        //  34     64     217    220    Ljava/lang/NullPointerException;
        //  34     64     217    220    Ljava/lang/ClassCastException;
        //  74     83     217    220    Ljava/lang/NullPointerException;
        //  74     83     217    220    Ljava/lang/ClassCastException;
        //  85     93     217    220    Ljava/lang/NullPointerException;
        //  85     93     217    220    Ljava/lang/ClassCastException;
        //  137    169    220    223    Ljava/lang/NullPointerException;
        //  137    169    220    223    Ljava/lang/ClassCastException;
        //  179    190    220    223    Ljava/lang/NullPointerException;
        //  179    190    220    223    Ljava/lang/ClassCastException;
        //  192    200    220    223    Ljava/lang/NullPointerException;
        //  192    200    220    223    Ljava/lang/ClassCastException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0137:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public V get(final Object o) {
        final int indexOfKey = this.indexOfKey(o);
        if (indexOfKey >= 0) {
            return (V)this.mArray[(indexOfKey << 1) + 1];
        }
        return null;
    }
    
    @Override
    public int hashCode() {
        final int[] mHashes = this.mHashes;
        final Object[] mArray = this.mArray;
        int n2;
        for (int mSize = this.mSize, n = 1, i = n2 = 0; i < mSize; ++i, n += 2) {
            final Object o = mArray[n];
            final int n3 = mHashes[i];
            int hashCode;
            if (o == null) {
                hashCode = 0;
            }
            else {
                hashCode = o.hashCode();
            }
            n2 += (hashCode ^ n3);
        }
        return n2;
    }
    
    int indexOf(final Object o, final int n) {
        final int mSize = this.mSize;
        if (mSize == 0) {
            return -1;
        }
        final int binarySearchHashes = binarySearchHashes(this.mHashes, mSize, n);
        if (binarySearchHashes < 0) {
            return binarySearchHashes;
        }
        if (o.equals(this.mArray[binarySearchHashes << 1])) {
            return binarySearchHashes;
        }
        int n2;
        for (n2 = binarySearchHashes + 1; n2 < mSize && this.mHashes[n2] == n; ++n2) {
            if (o.equals(this.mArray[n2 << 1])) {
                return n2;
            }
        }
        for (int n3 = binarySearchHashes - 1; n3 >= 0 && this.mHashes[n3] == n; --n3) {
            if (o.equals(this.mArray[n3 << 1])) {
                return n3;
            }
        }
        return n2;
    }
    
    public int indexOfKey(final Object o) {
        if (o == null) {
            return this.indexOfNull();
        }
        return this.indexOf(o, o.hashCode());
    }
    
    int indexOfNull() {
        final int mSize = this.mSize;
        if (mSize == 0) {
            return -1;
        }
        final int binarySearchHashes = binarySearchHashes(this.mHashes, mSize, 0);
        if (binarySearchHashes < 0) {
            return binarySearchHashes;
        }
        if (this.mArray[binarySearchHashes << 1] == null) {
            return binarySearchHashes;
        }
        int n;
        for (n = binarySearchHashes + 1; n < mSize && this.mHashes[n] == 0; ++n) {
            if (this.mArray[n << 1] == null) {
                return n;
            }
        }
        for (int n2 = binarySearchHashes - 1; n2 >= 0 && this.mHashes[n2] == 0; --n2) {
            if (this.mArray[n2 << 1] == null) {
                return n2;
            }
        }
        return n;
    }
    
    int indexOfValue(final Object o) {
        final int n = this.mSize * 2;
        final Object[] mArray = this.mArray;
        if (o == null) {
            for (int i = 1; i < n; i += 2) {
                if (mArray[i] == null) {
                    return i >> 1;
                }
            }
        }
        else {
            for (int j = 1; j < n; j += 2) {
                if (o.equals(mArray[j])) {
                    return j >> 1;
                }
            }
        }
        return -1;
    }
    
    public boolean isEmpty() {
        return this.mSize <= 0;
    }
    
    public K keyAt(final int n) {
        return (K)this.mArray[n << 1];
    }
    
    public V put(final K k, final V v) {
        final int mSize = this.mSize;
        int n;
        int hashCode;
        if (k == null) {
            n = this.indexOfNull();
            hashCode = 0;
        }
        else {
            hashCode = k.hashCode();
            n = this.indexOf(k, hashCode);
        }
        if (n >= 0) {
            final int n2 = (n << 1) + 1;
            final Object[] mArray = this.mArray;
            final Object o = mArray[n2];
            mArray[n2] = v;
            return (V)o;
        }
        final int n3 = n;
        if (mSize >= this.mHashes.length) {
            int n4 = 4;
            if (mSize >= 8) {
                n4 = (mSize >> 1) + mSize;
            }
            else if (mSize >= 4) {
                n4 = 8;
            }
            final int[] mHashes = this.mHashes;
            final Object[] mArray2 = this.mArray;
            this.allocArrays(n4);
            if (mSize != this.mSize) {
                throw new ConcurrentModificationException();
            }
            final int[] mHashes2 = this.mHashes;
            if (mHashes2.length > 0) {
                System.arraycopy(mHashes, 0, mHashes2, 0, mHashes.length);
                System.arraycopy(mArray2, 0, this.mArray, 0, mArray2.length);
            }
            freeArrays(mHashes, mArray2, mSize);
        }
        if (n3 < mSize) {
            final int[] mHashes3 = this.mHashes;
            final int n5 = n3 + 1;
            System.arraycopy(mHashes3, n3, mHashes3, n5, mSize - n3);
            final Object[] mArray3 = this.mArray;
            System.arraycopy(mArray3, n3 << 1, mArray3, n5 << 1, this.mSize - n3 << 1);
        }
        final int mSize2 = this.mSize;
        if (mSize == mSize2) {
            final int[] mHashes4 = this.mHashes;
            if (n3 < mHashes4.length) {
                mHashes4[n3] = hashCode;
                final Object[] mArray4 = this.mArray;
                final int n6 = n3 << 1;
                mArray4[n6] = k;
                mArray4[n6 + 1] = v;
                this.mSize = mSize2 + 1;
                return null;
            }
        }
        throw new ConcurrentModificationException();
    }
    
    public void putAll(final SimpleArrayMap<? extends K, ? extends V> simpleArrayMap) {
        final int mSize = simpleArrayMap.mSize;
        this.ensureCapacity(this.mSize + mSize);
        final int mSize2 = this.mSize;
        int i = 0;
        if (mSize2 == 0) {
            if (mSize > 0) {
                System.arraycopy(simpleArrayMap.mHashes, 0, this.mHashes, 0, mSize);
                System.arraycopy(simpleArrayMap.mArray, 0, this.mArray, 0, mSize << 1);
                this.mSize = mSize;
            }
        }
        else {
            while (i < mSize) {
                this.put(simpleArrayMap.keyAt(i), simpleArrayMap.valueAt(i));
                ++i;
            }
        }
    }
    
    public V remove(final Object o) {
        final int indexOfKey = this.indexOfKey(o);
        if (indexOfKey >= 0) {
            return this.removeAt(indexOfKey);
        }
        return null;
    }
    
    public V removeAt(int n) {
        final Object[] mArray = this.mArray;
        final int n2 = n << 1;
        final Object o = mArray[n2 + 1];
        final int mSize = this.mSize;
        int mSize2;
        if (mSize <= 1) {
            freeArrays(this.mHashes, mArray, mSize);
            this.mHashes = ContainerHelpers.EMPTY_INTS;
            this.mArray = ContainerHelpers.EMPTY_OBJECTS;
            mSize2 = 0;
        }
        else {
            final int n3 = mSize - 1;
            final int[] mHashes = this.mHashes;
            final int length = mHashes.length;
            int n4 = 8;
            if (length > 8 && mSize < mHashes.length / 3) {
                if (mSize > 8) {
                    n4 = mSize + (mSize >> 1);
                }
                final int[] mHashes2 = this.mHashes;
                final Object[] mArray2 = this.mArray;
                this.allocArrays(n4);
                if (mSize != this.mSize) {
                    throw new ConcurrentModificationException();
                }
                if (n > 0) {
                    System.arraycopy(mHashes2, 0, this.mHashes, 0, n);
                    System.arraycopy(mArray2, 0, this.mArray, 0, n2);
                }
                if (n < (mSize2 = n3)) {
                    final int n5 = n + 1;
                    final int[] mHashes3 = this.mHashes;
                    final int n6 = n3 - n;
                    System.arraycopy(mHashes2, n5, mHashes3, n, n6);
                    System.arraycopy(mArray2, n5 << 1, this.mArray, n2, n6 << 1);
                    mSize2 = n3;
                }
            }
            else {
                if (n < n3) {
                    final int[] mHashes4 = this.mHashes;
                    final int n7 = n + 1;
                    final int n8 = n3 - n;
                    System.arraycopy(mHashes4, n7, mHashes4, n, n8);
                    final Object[] mArray3 = this.mArray;
                    System.arraycopy(mArray3, n7 << 1, mArray3, n2, n8 << 1);
                }
                final Object[] mArray4 = this.mArray;
                n = n3 << 1;
                mArray4[n + 1] = (mArray4[n] = null);
                mSize2 = n3;
            }
        }
        if (mSize == this.mSize) {
            this.mSize = mSize2;
            return (V)o;
        }
        throw new ConcurrentModificationException();
    }
    
    public V setValueAt(int n, final V v) {
        n = (n << 1) + 1;
        final Object[] mArray = this.mArray;
        final Object o = mArray[n];
        mArray[n] = v;
        return (V)o;
    }
    
    public int size() {
        return this.mSize;
    }
    
    @Override
    public String toString() {
        if (this.isEmpty()) {
            return "{}";
        }
        final StringBuilder sb = new StringBuilder(this.mSize * 28);
        sb.append('{');
        for (int i = 0; i < this.mSize; ++i) {
            if (i > 0) {
                sb.append(", ");
            }
            final K key = this.keyAt(i);
            if (key != this) {
                sb.append(key);
            }
            else {
                sb.append("(this Map)");
            }
            sb.append('=');
            final V value = this.valueAt(i);
            if (value != this) {
                sb.append(value);
            }
            else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }
    
    public V valueAt(final int n) {
        return (V)this.mArray[(n << 1) + 1];
    }
}
