// 
// Decompiled by Procyon v0.5.36
// 

package androidx.transition;

import java.lang.reflect.InvocationTargetException;
import android.util.Log;
import android.view.View;
import java.lang.reflect.Method;

class ViewUtilsApi19 extends ViewUtilsBase
{
    private static final String TAG = "ViewUtilsApi19";
    private static Method sGetTransitionAlphaMethod;
    private static boolean sGetTransitionAlphaMethodFetched;
    private static Method sSetTransitionAlphaMethod;
    private static boolean sSetTransitionAlphaMethodFetched;
    
    private void fetchGetTransitionAlphaMethod() {
        if (!ViewUtilsApi19.sGetTransitionAlphaMethodFetched) {
            try {
                (ViewUtilsApi19.sGetTransitionAlphaMethod = View.class.getDeclaredMethod("getTransitionAlpha", (Class<?>[])new Class[0])).setAccessible(true);
            }
            catch (NoSuchMethodException ex) {
                Log.i("ViewUtilsApi19", "Failed to retrieve getTransitionAlpha method", (Throwable)ex);
            }
            ViewUtilsApi19.sGetTransitionAlphaMethodFetched = true;
        }
    }
    
    private void fetchSetTransitionAlphaMethod() {
        if (!ViewUtilsApi19.sSetTransitionAlphaMethodFetched) {
            try {
                (ViewUtilsApi19.sSetTransitionAlphaMethod = View.class.getDeclaredMethod("setTransitionAlpha", Float.TYPE)).setAccessible(true);
            }
            catch (NoSuchMethodException ex) {
                Log.i("ViewUtilsApi19", "Failed to retrieve setTransitionAlpha method", (Throwable)ex);
            }
            ViewUtilsApi19.sSetTransitionAlphaMethodFetched = true;
        }
    }
    
    @Override
    public void clearNonTransitionAlpha(final View view) {
    }
    
    @Override
    public float getTransitionAlpha(final View obj) {
        this.fetchGetTransitionAlphaMethod();
        final Method sGetTransitionAlphaMethod = ViewUtilsApi19.sGetTransitionAlphaMethod;
        if (sGetTransitionAlphaMethod == null) {
            goto Label_0043;
        }
        try {
            return (float)sGetTransitionAlphaMethod.invoke(obj, new Object[0]);
        }
        catch (InvocationTargetException ex) {
            throw new RuntimeException(ex.getCause());
        }
        catch (IllegalAccessException ex2) {
            goto Label_0043;
        }
    }
    
    @Override
    public void saveNonTransitionAlpha(final View view) {
    }
    
    @Override
    public void setTransitionAlpha(final View obj, final float f) {
        this.fetchSetTransitionAlphaMethod();
        final Method sSetTransitionAlphaMethod = ViewUtilsApi19.sSetTransitionAlphaMethod;
        if (sSetTransitionAlphaMethod == null) {
            goto Label_0043;
        }
        try {
            sSetTransitionAlphaMethod.invoke(obj, f);
        }
        catch (InvocationTargetException ex) {
            throw new RuntimeException(ex.getCause());
        }
        catch (IllegalAccessException ex2) {}
    }
}
