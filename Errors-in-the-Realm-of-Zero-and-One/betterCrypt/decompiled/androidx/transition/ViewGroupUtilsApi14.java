// 
// Decompiled by Procyon v0.5.36
// 

package androidx.transition;

import android.view.ViewGroup;
import java.lang.reflect.Field;
import android.animation.LayoutTransition;
import java.lang.reflect.Method;

class ViewGroupUtilsApi14
{
    private static final int LAYOUT_TRANSITION_CHANGING = 4;
    private static final String TAG = "ViewGroupUtilsApi14";
    private static Method sCancelMethod;
    private static boolean sCancelMethodFetched;
    private static LayoutTransition sEmptyLayoutTransition;
    private static Field sLayoutSuppressedField;
    private static boolean sLayoutSuppressedFieldFetched;
    
    private ViewGroupUtilsApi14() {
    }
    
    private static void cancelLayoutTransition(final LayoutTransition p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: ifne            42
        //     6: ldc             Landroid/animation/LayoutTransition;.class
        //     8: ldc             "cancel"
        //    10: iconst_0       
        //    11: anewarray       Ljava/lang/Class;
        //    14: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //    17: putstatic       androidx/transition/ViewGroupUtilsApi14.sCancelMethod:Ljava/lang/reflect/Method;
        //    20: getstatic       androidx/transition/ViewGroupUtilsApi14.sCancelMethod:Ljava/lang/reflect/Method;
        //    23: iconst_1       
        //    24: invokevirtual   java/lang/reflect/Method.setAccessible:(Z)V
        //    27: goto            38
        //    30: ldc             "ViewGroupUtilsApi14"
        //    32: ldc             "Failed to access cancel method by reflection"
        //    34: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;)I
        //    37: pop            
        //    38: iconst_1       
        //    39: putstatic       androidx/transition/ViewGroupUtilsApi14.sCancelMethodFetched:Z
        //    42: getstatic       androidx/transition/ViewGroupUtilsApi14.sCancelMethod:Ljava/lang/reflect/Method;
        //    45: astore_1       
        //    46: aload_1        
        //    47: ifnull          78
        //    50: aload_1        
        //    51: aload_0        
        //    52: iconst_0       
        //    53: anewarray       Ljava/lang/Object;
        //    56: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //    59: pop            
        //    60: return         
        //    61: ldc             "ViewGroupUtilsApi14"
        //    63: ldc             "Failed to invoke cancel method by reflection"
        //    65: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;)I
        //    68: pop            
        //    69: return         
        //    70: ldc             "ViewGroupUtilsApi14"
        //    72: ldc             "Failed to access cancel method by reflection"
        //    74: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;)I
        //    77: pop            
        //    78: return         
        //    79: astore_1       
        //    80: goto            30
        //    83: astore_0       
        //    84: goto            70
        //    87: astore_0       
        //    88: goto            61
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                         
        //  -----  -----  -----  -----  ---------------------------------------------
        //  6      27     79     38     Ljava/lang/NoSuchMethodException;
        //  50     60     83     78     Ljava/lang/IllegalAccessException;
        //  50     60     87     70     Ljava/lang/reflect/InvocationTargetException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0061:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    static void suppressLayout(final ViewGroup p0, final boolean p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: astore          4
        //     5: iconst_0       
        //     6: istore_3       
        //     7: iconst_0       
        //     8: istore_2       
        //     9: aload           4
        //    11: ifnonnull       64
        //    14: new             Landroidx/transition/ViewGroupUtilsApi14$1;
        //    17: dup            
        //    18: invokespecial   androidx/transition/ViewGroupUtilsApi14$1.<init>:()V
        //    21: putstatic       androidx/transition/ViewGroupUtilsApi14.sEmptyLayoutTransition:Landroid/animation/LayoutTransition;
        //    24: getstatic       androidx/transition/ViewGroupUtilsApi14.sEmptyLayoutTransition:Landroid/animation/LayoutTransition;
        //    27: iconst_2       
        //    28: aconst_null    
        //    29: invokevirtual   android/animation/LayoutTransition.setAnimator:(ILandroid/animation/Animator;)V
        //    32: getstatic       androidx/transition/ViewGroupUtilsApi14.sEmptyLayoutTransition:Landroid/animation/LayoutTransition;
        //    35: iconst_0       
        //    36: aconst_null    
        //    37: invokevirtual   android/animation/LayoutTransition.setAnimator:(ILandroid/animation/Animator;)V
        //    40: getstatic       androidx/transition/ViewGroupUtilsApi14.sEmptyLayoutTransition:Landroid/animation/LayoutTransition;
        //    43: iconst_1       
        //    44: aconst_null    
        //    45: invokevirtual   android/animation/LayoutTransition.setAnimator:(ILandroid/animation/Animator;)V
        //    48: getstatic       androidx/transition/ViewGroupUtilsApi14.sEmptyLayoutTransition:Landroid/animation/LayoutTransition;
        //    51: iconst_3       
        //    52: aconst_null    
        //    53: invokevirtual   android/animation/LayoutTransition.setAnimator:(ILandroid/animation/Animator;)V
        //    56: getstatic       androidx/transition/ViewGroupUtilsApi14.sEmptyLayoutTransition:Landroid/animation/LayoutTransition;
        //    59: iconst_4       
        //    60: aconst_null    
        //    61: invokevirtual   android/animation/LayoutTransition.setAnimator:(ILandroid/animation/Animator;)V
        //    64: iload_1        
        //    65: ifeq            117
        //    68: aload_0        
        //    69: invokevirtual   android/view/ViewGroup.getLayoutTransition:()Landroid/animation/LayoutTransition;
        //    72: astore          4
        //    74: aload           4
        //    76: ifnull          109
        //    79: aload           4
        //    81: invokevirtual   android/animation/LayoutTransition.isRunning:()Z
        //    84: ifeq            92
        //    87: aload           4
        //    89: invokestatic    androidx/transition/ViewGroupUtilsApi14.cancelLayoutTransition:(Landroid/animation/LayoutTransition;)V
        //    92: aload           4
        //    94: getstatic       androidx/transition/ViewGroupUtilsApi14.sEmptyLayoutTransition:Landroid/animation/LayoutTransition;
        //    97: if_acmpeq       109
        //   100: aload_0        
        //   101: getstatic       androidx/transition/R$id.transition_layout_save:I
        //   104: aload           4
        //   106: invokevirtual   android/view/ViewGroup.setTag:(ILjava/lang/Object;)V
        //   109: aload_0        
        //   110: getstatic       androidx/transition/ViewGroupUtilsApi14.sEmptyLayoutTransition:Landroid/animation/LayoutTransition;
        //   113: invokevirtual   android/view/ViewGroup.setLayoutTransition:(Landroid/animation/LayoutTransition;)V
        //   116: return         
        //   117: aload_0        
        //   118: aconst_null    
        //   119: invokevirtual   android/view/ViewGroup.setLayoutTransition:(Landroid/animation/LayoutTransition;)V
        //   122: getstatic       androidx/transition/ViewGroupUtilsApi14.sLayoutSuppressedFieldFetched:Z
        //   125: ifne            160
        //   128: ldc             Landroid/view/ViewGroup;.class
        //   130: ldc             "mLayoutSuppressed"
        //   132: invokevirtual   java/lang/Class.getDeclaredField:(Ljava/lang/String;)Ljava/lang/reflect/Field;
        //   135: putstatic       androidx/transition/ViewGroupUtilsApi14.sLayoutSuppressedField:Ljava/lang/reflect/Field;
        //   138: getstatic       androidx/transition/ViewGroupUtilsApi14.sLayoutSuppressedField:Ljava/lang/reflect/Field;
        //   141: iconst_1       
        //   142: invokevirtual   java/lang/reflect/Field.setAccessible:(Z)V
        //   145: goto            156
        //   148: ldc             "ViewGroupUtilsApi14"
        //   150: ldc             "Failed to access mLayoutSuppressed field by reflection"
        //   152: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;)I
        //   155: pop            
        //   156: iconst_1       
        //   157: putstatic       androidx/transition/ViewGroupUtilsApi14.sLayoutSuppressedFieldFetched:Z
        //   160: getstatic       androidx/transition/ViewGroupUtilsApi14.sLayoutSuppressedField:Ljava/lang/reflect/Field;
        //   163: astore          4
        //   165: iload_3        
        //   166: istore_1       
        //   167: aload           4
        //   169: ifnull          208
        //   172: aload           4
        //   174: aload_0        
        //   175: invokevirtual   java/lang/reflect/Field.getBoolean:(Ljava/lang/Object;)Z
        //   178: istore_1       
        //   179: iload_1        
        //   180: ifeq            197
        //   183: getstatic       androidx/transition/ViewGroupUtilsApi14.sLayoutSuppressedField:Ljava/lang/reflect/Field;
        //   186: aload_0        
        //   187: iconst_0       
        //   188: invokevirtual   java/lang/reflect/Field.setBoolean:(Ljava/lang/Object;Z)V
        //   191: goto            197
        //   194: goto            200
        //   197: goto            208
        //   200: ldc             "ViewGroupUtilsApi14"
        //   202: ldc             "Failed to get mLayoutSuppressed field by reflection"
        //   204: invokestatic    android/util/Log.i:(Ljava/lang/String;Ljava/lang/String;)I
        //   207: pop            
        //   208: iload_1        
        //   209: ifeq            216
        //   212: aload_0        
        //   213: invokevirtual   android/view/ViewGroup.requestLayout:()V
        //   216: aload_0        
        //   217: getstatic       androidx/transition/R$id.transition_layout_save:I
        //   220: invokevirtual   android/view/ViewGroup.getTag:(I)Ljava/lang/Object;
        //   223: checkcast       Landroid/animation/LayoutTransition;
        //   226: astore          4
        //   228: aload           4
        //   230: ifnull          247
        //   233: aload_0        
        //   234: getstatic       androidx/transition/R$id.transition_layout_save:I
        //   237: aconst_null    
        //   238: invokevirtual   android/view/ViewGroup.setTag:(ILjava/lang/Object;)V
        //   241: aload_0        
        //   242: aload           4
        //   244: invokevirtual   android/view/ViewGroup.setLayoutTransition:(Landroid/animation/LayoutTransition;)V
        //   247: return         
        //   248: astore          4
        //   250: goto            148
        //   253: astore          4
        //   255: iload_2        
        //   256: istore_1       
        //   257: goto            200
        //   260: astore          4
        //   262: goto            194
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                              
        //  -----  -----  -----  -----  ----------------------------------
        //  128    145    248    156    Ljava/lang/NoSuchFieldException;
        //  172    179    253    260    Ljava/lang/IllegalAccessException;
        //  183    191    260    197    Ljava/lang/IllegalAccessException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 124 out of bounds for length 124
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:373)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:426)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
