// 
// Decompiled by Procyon v0.5.36
// 

package androidx.transition;

import android.os.Build$VERSION;
import android.graphics.Matrix;
import android.view.ViewGroup;
import android.view.View;

class GhostViewUtils
{
    private GhostViewUtils() {
    }
    
    static GhostViewImpl addGhost(final View view, final ViewGroup viewGroup, final Matrix matrix) {
        if (Build$VERSION.SDK_INT >= 21) {
            return GhostViewApi21.addGhost(view, viewGroup, matrix);
        }
        return GhostViewApi14.addGhost(view, viewGroup);
    }
    
    static void removeGhost(final View view) {
        if (Build$VERSION.SDK_INT >= 21) {
            GhostViewApi21.removeGhost(view);
            return;
        }
        GhostViewApi14.removeGhost(view);
    }
}
