// 
// Decompiled by Procyon v0.5.36
// 

package androidx.transition;

import android.view.View;

interface ViewGroupOverlayImpl extends ViewOverlayImpl
{
    void add(final View p0);
    
    void remove(final View p0);
}
