// 
// Decompiled by Procyon v0.5.36
// 

package androidx.transition;

import android.view.LayoutInflater;
import android.util.SparseArray;
import android.view.ViewGroup;
import android.view.View;
import android.content.Context;

public class Scene
{
    private Context mContext;
    private Runnable mEnterAction;
    private Runnable mExitAction;
    private View mLayout;
    private int mLayoutId;
    private ViewGroup mSceneRoot;
    
    public Scene(final ViewGroup mSceneRoot) {
        this.mLayoutId = -1;
        this.mSceneRoot = mSceneRoot;
    }
    
    private Scene(final ViewGroup mSceneRoot, final int mLayoutId, final Context mContext) {
        this.mLayoutId = -1;
        this.mContext = mContext;
        this.mSceneRoot = mSceneRoot;
        this.mLayoutId = mLayoutId;
    }
    
    public Scene(final ViewGroup mSceneRoot, final View mLayout) {
        this.mLayoutId = -1;
        this.mSceneRoot = mSceneRoot;
        this.mLayout = mLayout;
    }
    
    static Scene getCurrentScene(final View view) {
        return (Scene)view.getTag(R.id.transition_current_scene);
    }
    
    public static Scene getSceneForLayout(final ViewGroup viewGroup, final int n, final Context context) {
        SparseArray sparseArray;
        if ((sparseArray = (SparseArray)viewGroup.getTag(R.id.transition_scene_layoutid_cache)) == null) {
            sparseArray = new SparseArray();
            viewGroup.setTag(R.id.transition_scene_layoutid_cache, (Object)sparseArray);
        }
        final Scene scene = (Scene)sparseArray.get(n);
        if (scene != null) {
            return scene;
        }
        final Scene scene2 = new Scene(viewGroup, n, context);
        sparseArray.put(n, (Object)scene2);
        return scene2;
    }
    
    static void setCurrentScene(final View view, final Scene scene) {
        view.setTag(R.id.transition_current_scene, (Object)scene);
    }
    
    public void enter() {
        if (this.mLayoutId > 0 || this.mLayout != null) {
            this.getSceneRoot().removeAllViews();
            if (this.mLayoutId > 0) {
                LayoutInflater.from(this.mContext).inflate(this.mLayoutId, this.mSceneRoot);
            }
            else {
                this.mSceneRoot.addView(this.mLayout);
            }
        }
        final Runnable mEnterAction = this.mEnterAction;
        if (mEnterAction != null) {
            mEnterAction.run();
        }
        setCurrentScene((View)this.mSceneRoot, this);
    }
    
    public void exit() {
        if (getCurrentScene((View)this.mSceneRoot) == this) {
            final Runnable mExitAction = this.mExitAction;
            if (mExitAction != null) {
                mExitAction.run();
            }
        }
    }
    
    public ViewGroup getSceneRoot() {
        return this.mSceneRoot;
    }
    
    boolean isCreatedFromLayoutResource() {
        return this.mLayoutId > 0;
    }
    
    public void setEnterAction(final Runnable mEnterAction) {
        this.mEnterAction = mEnterAction;
    }
    
    public void setExitAction(final Runnable mExitAction) {
        this.mExitAction = mExitAction;
    }
}
