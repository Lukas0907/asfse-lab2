// 
// Decompiled by Procyon v0.5.36
// 

package androidx.transition;

import java.lang.reflect.InvocationTargetException;
import android.util.Log;
import android.view.View;
import java.lang.reflect.Method;

class ViewUtilsApi22 extends ViewUtilsApi21
{
    private static final String TAG = "ViewUtilsApi22";
    private static Method sSetLeftTopRightBottomMethod;
    private static boolean sSetLeftTopRightBottomMethodFetched;
    
    private void fetchSetLeftTopRightBottomMethod() {
        if (!ViewUtilsApi22.sSetLeftTopRightBottomMethodFetched) {
            try {
                (ViewUtilsApi22.sSetLeftTopRightBottomMethod = View.class.getDeclaredMethod("setLeftTopRightBottom", Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE)).setAccessible(true);
            }
            catch (NoSuchMethodException ex) {
                Log.i("ViewUtilsApi22", "Failed to retrieve setLeftTopRightBottom method", (Throwable)ex);
            }
            ViewUtilsApi22.sSetLeftTopRightBottomMethodFetched = true;
        }
    }
    
    @Override
    public void setLeftTopRightBottom(final View obj, final int i, final int j, final int k, final int l) {
        this.fetchSetLeftTopRightBottomMethod();
        final Method sSetLeftTopRightBottomMethod = ViewUtilsApi22.sSetLeftTopRightBottomMethod;
        if (sSetLeftTopRightBottomMethod == null) {
            goto Label_0069;
        }
        try {
            sSetLeftTopRightBottomMethod.invoke(obj, i, j, k, l);
        }
        catch (InvocationTargetException ex) {
            throw new RuntimeException(ex.getCause());
        }
        catch (IllegalAccessException ex2) {}
    }
}
