// 
// Decompiled by Procyon v0.5.36
// 

package androidx.transition;

import android.os.Build$VERSION;
import android.view.ViewGroup;

class ViewGroupUtils
{
    private ViewGroupUtils() {
    }
    
    static ViewGroupOverlayImpl getOverlay(final ViewGroup viewGroup) {
        if (Build$VERSION.SDK_INT >= 18) {
            return new ViewGroupOverlayApi18(viewGroup);
        }
        return ViewGroupOverlayApi14.createFrom(viewGroup);
    }
    
    static void suppressLayout(final ViewGroup viewGroup, final boolean b) {
        if (Build$VERSION.SDK_INT >= 18) {
            ViewGroupUtilsApi18.suppressLayout(viewGroup, b);
            return;
        }
        ViewGroupUtilsApi14.suppressLayout(viewGroup, b);
    }
}
