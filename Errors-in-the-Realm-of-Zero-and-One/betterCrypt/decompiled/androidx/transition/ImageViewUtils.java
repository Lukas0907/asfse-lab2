// 
// Decompiled by Procyon v0.5.36
// 

package androidx.transition;

import android.animation.Animator$AnimatorListener;
import android.widget.ImageView$ScaleType;
import android.animation.AnimatorListenerAdapter;
import android.animation.Animator;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import android.os.Build$VERSION;
import android.graphics.Matrix;
import android.widget.ImageView;
import java.lang.reflect.Method;

class ImageViewUtils
{
    private static final String TAG = "ImageViewUtils";
    private static Method sAnimateTransformMethod;
    private static boolean sAnimateTransformMethodFetched;
    
    private ImageViewUtils() {
    }
    
    static void animateTransform(final ImageView obj, final Matrix imageMatrix) {
        if (Build$VERSION.SDK_INT < 21) {
            obj.setImageMatrix(imageMatrix);
            return;
        }
        fetchAnimateTransformMethod();
        final Method sAnimateTransformMethod = ImageViewUtils.sAnimateTransformMethod;
        if (sAnimateTransformMethod == null) {
            goto Label_0053;
        }
        try {
            sAnimateTransformMethod.invoke(obj, imageMatrix);
        }
        catch (InvocationTargetException ex) {
            throw new RuntimeException(ex.getCause());
        }
        catch (IllegalAccessException ex2) {}
    }
    
    private static void fetchAnimateTransformMethod() {
        if (!ImageViewUtils.sAnimateTransformMethodFetched) {
            try {
                (ImageViewUtils.sAnimateTransformMethod = ImageView.class.getDeclaredMethod("animateTransform", Matrix.class)).setAccessible(true);
            }
            catch (NoSuchMethodException ex) {
                Log.i("ImageViewUtils", "Failed to retrieve animateTransform method", (Throwable)ex);
            }
            ImageViewUtils.sAnimateTransformMethodFetched = true;
        }
    }
    
    static void reserveEndAnimateTransform(final ImageView imageView, final Animator animator) {
        if (Build$VERSION.SDK_INT < 21) {
            animator.addListener((Animator$AnimatorListener)new AnimatorListenerAdapter() {
                public void onAnimationEnd(final Animator animator) {
                    final ImageView$ScaleType scaleType = (ImageView$ScaleType)imageView.getTag(R.id.save_scale_type);
                    imageView.setScaleType(scaleType);
                    imageView.setTag(R.id.save_scale_type, (Object)null);
                    if (scaleType == ImageView$ScaleType.MATRIX) {
                        final ImageView val$view = imageView;
                        val$view.setImageMatrix((Matrix)val$view.getTag(R.id.save_image_matrix));
                        imageView.setTag(R.id.save_image_matrix, (Object)null);
                    }
                    animator.removeListener((Animator$AnimatorListener)this);
                }
            });
        }
    }
    
    static void startAnimateTransform(final ImageView imageView) {
        if (Build$VERSION.SDK_INT < 21) {
            final ImageView$ScaleType scaleType = imageView.getScaleType();
            imageView.setTag(R.id.save_scale_type, (Object)scaleType);
            if (scaleType == ImageView$ScaleType.MATRIX) {
                imageView.setTag(R.id.save_image_matrix, (Object)imageView.getImageMatrix());
            }
            else {
                imageView.setScaleType(ImageView$ScaleType.MATRIX);
            }
            imageView.setImageMatrix(MatrixUtils.IDENTITY_MATRIX);
        }
    }
}
