// 
// Decompiled by Procyon v0.5.36
// 

package androidx.transition;

import android.graphics.drawable.Drawable;

interface ViewOverlayImpl
{
    void add(final Drawable p0);
    
    void clear();
    
    void remove(final Drawable p0);
}
