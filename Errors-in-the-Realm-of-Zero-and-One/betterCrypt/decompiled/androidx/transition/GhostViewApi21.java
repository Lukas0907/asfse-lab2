// 
// Decompiled by Procyon v0.5.36
// 

package androidx.transition;

import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import android.graphics.Matrix;
import android.view.ViewGroup;
import android.view.View;
import java.lang.reflect.Method;

class GhostViewApi21 implements GhostViewImpl
{
    private static final String TAG = "GhostViewApi21";
    private static Method sAddGhostMethod;
    private static boolean sAddGhostMethodFetched;
    private static Class<?> sGhostViewClass;
    private static boolean sGhostViewClassFetched;
    private static Method sRemoveGhostMethod;
    private static boolean sRemoveGhostMethodFetched;
    private final View mGhostView;
    
    private GhostViewApi21(final View mGhostView) {
        this.mGhostView = mGhostView;
    }
    
    static GhostViewImpl addGhost(final View view, final ViewGroup viewGroup, final Matrix matrix) {
        fetchAddGhostMethod();
        final Method sAddGhostMethod = GhostViewApi21.sAddGhostMethod;
        if (sAddGhostMethod == null) {
            goto Label_0058;
        }
        try {
            return new GhostViewApi21((View)sAddGhostMethod.invoke(null, view, viewGroup, matrix));
        }
        catch (InvocationTargetException ex) {
            throw new RuntimeException(ex.getCause());
        }
        catch (IllegalAccessException ex2) {
            return null;
        }
    }
    
    private static void fetchAddGhostMethod() {
        if (!GhostViewApi21.sAddGhostMethodFetched) {
            try {
                fetchGhostViewClass();
                (GhostViewApi21.sAddGhostMethod = GhostViewApi21.sGhostViewClass.getDeclaredMethod("addGhost", View.class, ViewGroup.class, Matrix.class)).setAccessible(true);
            }
            catch (NoSuchMethodException ex) {
                Log.i("GhostViewApi21", "Failed to retrieve addGhost method", (Throwable)ex);
            }
            GhostViewApi21.sAddGhostMethodFetched = true;
        }
    }
    
    private static void fetchGhostViewClass() {
        if (!GhostViewApi21.sGhostViewClassFetched) {
            try {
                GhostViewApi21.sGhostViewClass = Class.forName("android.view.GhostView");
            }
            catch (ClassNotFoundException ex) {
                Log.i("GhostViewApi21", "Failed to retrieve GhostView class", (Throwable)ex);
            }
            GhostViewApi21.sGhostViewClassFetched = true;
        }
    }
    
    private static void fetchRemoveGhostMethod() {
        if (!GhostViewApi21.sRemoveGhostMethodFetched) {
            try {
                fetchGhostViewClass();
                (GhostViewApi21.sRemoveGhostMethod = GhostViewApi21.sGhostViewClass.getDeclaredMethod("removeGhost", View.class)).setAccessible(true);
            }
            catch (NoSuchMethodException ex) {
                Log.i("GhostViewApi21", "Failed to retrieve removeGhost method", (Throwable)ex);
            }
            GhostViewApi21.sRemoveGhostMethodFetched = true;
        }
    }
    
    static void removeGhost(final View view) {
        fetchRemoveGhostMethod();
        final Method sRemoveGhostMethod = GhostViewApi21.sRemoveGhostMethod;
        if (sRemoveGhostMethod == null) {
            goto Label_0039;
        }
        try {
            sRemoveGhostMethod.invoke(null, view);
        }
        catch (InvocationTargetException ex) {
            throw new RuntimeException(ex.getCause());
        }
        catch (IllegalAccessException ex2) {}
    }
    
    @Override
    public void reserveEndViewTransition(final ViewGroup viewGroup, final View view) {
    }
    
    @Override
    public void setVisibility(final int visibility) {
        this.mGhostView.setVisibility(visibility);
    }
}
