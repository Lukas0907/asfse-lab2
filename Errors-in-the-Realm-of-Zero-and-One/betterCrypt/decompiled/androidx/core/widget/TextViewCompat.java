// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.widget;

import android.view.MenuItem;
import android.view.ActionMode;
import android.view.Menu;
import android.text.Editable;
import java.util.Iterator;
import android.app.Activity;
import java.util.ArrayList;
import java.util.List;
import android.content.pm.PackageManager;
import android.content.Context;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import java.lang.reflect.Method;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.lang.annotation.Annotation;
import android.graphics.Paint$FontMetricsInt;
import android.view.ActionMode$Callback;
import android.util.Log;
import android.graphics.Paint;
import android.text.TextPaint;
import androidx.core.text.PrecomputedTextCompat;
import android.icu.text.DecimalFormatSymbols;
import android.text.method.PasswordTransformationMethod;
import android.text.TextDirectionHeuristics;
import android.text.TextDirectionHeuristic;
import android.graphics.drawable.Drawable;
import android.graphics.PorterDuff$Mode;
import androidx.core.util.Preconditions;
import android.content.res.ColorStateList;
import android.os.Build$VERSION;
import android.widget.TextView;
import java.lang.reflect.Field;

public final class TextViewCompat
{
    public static final int AUTO_SIZE_TEXT_TYPE_NONE = 0;
    public static final int AUTO_SIZE_TEXT_TYPE_UNIFORM = 1;
    private static final int LINES = 1;
    private static final String LOG_TAG = "TextViewCompat";
    private static Field sMaxModeField;
    private static boolean sMaxModeFieldFetched;
    private static Field sMaximumField;
    private static boolean sMaximumFieldFetched;
    private static Field sMinModeField;
    private static boolean sMinModeFieldFetched;
    private static Field sMinimumField;
    private static boolean sMinimumFieldFetched;
    
    private TextViewCompat() {
    }
    
    public static int getAutoSizeMaxTextSize(final TextView textView) {
        if (Build$VERSION.SDK_INT >= 27) {
            return textView.getAutoSizeMaxTextSize();
        }
        if (textView instanceof AutoSizeableTextView) {
            return ((AutoSizeableTextView)textView).getAutoSizeMaxTextSize();
        }
        return -1;
    }
    
    public static int getAutoSizeMinTextSize(final TextView textView) {
        if (Build$VERSION.SDK_INT >= 27) {
            return textView.getAutoSizeMinTextSize();
        }
        if (textView instanceof AutoSizeableTextView) {
            return ((AutoSizeableTextView)textView).getAutoSizeMinTextSize();
        }
        return -1;
    }
    
    public static int getAutoSizeStepGranularity(final TextView textView) {
        if (Build$VERSION.SDK_INT >= 27) {
            return textView.getAutoSizeStepGranularity();
        }
        if (textView instanceof AutoSizeableTextView) {
            return ((AutoSizeableTextView)textView).getAutoSizeStepGranularity();
        }
        return -1;
    }
    
    public static int[] getAutoSizeTextAvailableSizes(final TextView textView) {
        if (Build$VERSION.SDK_INT >= 27) {
            return textView.getAutoSizeTextAvailableSizes();
        }
        if (textView instanceof AutoSizeableTextView) {
            return ((AutoSizeableTextView)textView).getAutoSizeTextAvailableSizes();
        }
        return new int[0];
    }
    
    public static int getAutoSizeTextType(final TextView textView) {
        if (Build$VERSION.SDK_INT >= 27) {
            return textView.getAutoSizeTextType();
        }
        if (textView instanceof AutoSizeableTextView) {
            return ((AutoSizeableTextView)textView).getAutoSizeTextType();
        }
        return 0;
    }
    
    public static ColorStateList getCompoundDrawableTintList(final TextView textView) {
        Preconditions.checkNotNull(textView);
        if (Build$VERSION.SDK_INT >= 23) {
            return textView.getCompoundDrawableTintList();
        }
        if (textView instanceof TintableCompoundDrawablesView) {
            return ((TintableCompoundDrawablesView)textView).getSupportCompoundDrawablesTintList();
        }
        return null;
    }
    
    public static PorterDuff$Mode getCompoundDrawableTintMode(final TextView textView) {
        Preconditions.checkNotNull(textView);
        if (Build$VERSION.SDK_INT >= 23) {
            return textView.getCompoundDrawableTintMode();
        }
        if (textView instanceof TintableCompoundDrawablesView) {
            return ((TintableCompoundDrawablesView)textView).getSupportCompoundDrawablesTintMode();
        }
        return null;
    }
    
    public static Drawable[] getCompoundDrawablesRelative(final TextView textView) {
        if (Build$VERSION.SDK_INT >= 18) {
            return textView.getCompoundDrawablesRelative();
        }
        if (Build$VERSION.SDK_INT >= 17) {
            final int layoutDirection = textView.getLayoutDirection();
            boolean b = true;
            if (layoutDirection != 1) {
                b = false;
            }
            final Drawable[] compoundDrawables = textView.getCompoundDrawables();
            if (b) {
                final Drawable drawable = compoundDrawables[2];
                final Drawable drawable2 = compoundDrawables[0];
                compoundDrawables[0] = drawable;
                compoundDrawables[2] = drawable2;
            }
            return compoundDrawables;
        }
        return textView.getCompoundDrawables();
    }
    
    public static int getFirstBaselineToTopHeight(final TextView textView) {
        return textView.getPaddingTop() - textView.getPaint().getFontMetricsInt().top;
    }
    
    public static int getLastBaselineToBottomHeight(final TextView textView) {
        return textView.getPaddingBottom() + textView.getPaint().getFontMetricsInt().bottom;
    }
    
    public static int getMaxLines(final TextView textView) {
        if (Build$VERSION.SDK_INT >= 16) {
            return textView.getMaxLines();
        }
        if (!TextViewCompat.sMaxModeFieldFetched) {
            TextViewCompat.sMaxModeField = retrieveField("mMaxMode");
            TextViewCompat.sMaxModeFieldFetched = true;
        }
        final Field sMaxModeField = TextViewCompat.sMaxModeField;
        if (sMaxModeField != null && retrieveIntFromField(sMaxModeField, textView) == 1) {
            if (!TextViewCompat.sMaximumFieldFetched) {
                TextViewCompat.sMaximumField = retrieveField("mMaximum");
                TextViewCompat.sMaximumFieldFetched = true;
            }
            final Field sMaximumField = TextViewCompat.sMaximumField;
            if (sMaximumField != null) {
                return retrieveIntFromField(sMaximumField, textView);
            }
        }
        return -1;
    }
    
    public static int getMinLines(final TextView textView) {
        if (Build$VERSION.SDK_INT >= 16) {
            return textView.getMinLines();
        }
        if (!TextViewCompat.sMinModeFieldFetched) {
            TextViewCompat.sMinModeField = retrieveField("mMinMode");
            TextViewCompat.sMinModeFieldFetched = true;
        }
        final Field sMinModeField = TextViewCompat.sMinModeField;
        if (sMinModeField != null && retrieveIntFromField(sMinModeField, textView) == 1) {
            if (!TextViewCompat.sMinimumFieldFetched) {
                TextViewCompat.sMinimumField = retrieveField("mMinimum");
                TextViewCompat.sMinimumFieldFetched = true;
            }
            final Field sMinimumField = TextViewCompat.sMinimumField;
            if (sMinimumField != null) {
                return retrieveIntFromField(sMinimumField, textView);
            }
        }
        return -1;
    }
    
    private static int getTextDirection(final TextDirectionHeuristic textDirectionHeuristic) {
        if (textDirectionHeuristic == TextDirectionHeuristics.FIRSTSTRONG_RTL) {
            return 1;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.FIRSTSTRONG_LTR) {
            return 1;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.ANYRTL_LTR) {
            return 2;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.LTR) {
            return 3;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.RTL) {
            return 4;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.LOCALE) {
            return 5;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.FIRSTSTRONG_LTR) {
            return 6;
        }
        if (textDirectionHeuristic == TextDirectionHeuristics.FIRSTSTRONG_RTL) {
            return 7;
        }
        return 1;
    }
    
    private static TextDirectionHeuristic getTextDirectionHeuristic(final TextView textView) {
        if (textView.getTransformationMethod() instanceof PasswordTransformationMethod) {
            return TextDirectionHeuristics.LTR;
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        boolean b = false;
        if (sdk_INT >= 28 && (textView.getInputType() & 0xF) == 0x3) {
            final byte directionality = Character.getDirectionality(DecimalFormatSymbols.getInstance(textView.getTextLocale()).getDigitStrings()[0].codePointAt(0));
            if (directionality != 1 && directionality != 2) {
                return TextDirectionHeuristics.LTR;
            }
            return TextDirectionHeuristics.RTL;
        }
        else {
            if (textView.getLayoutDirection() == 1) {
                b = true;
            }
            switch (textView.getTextDirection()) {
                default: {
                    if (b) {
                        return TextDirectionHeuristics.FIRSTSTRONG_RTL;
                    }
                    return TextDirectionHeuristics.FIRSTSTRONG_LTR;
                }
                case 7: {
                    return TextDirectionHeuristics.FIRSTSTRONG_RTL;
                }
                case 6: {
                    return TextDirectionHeuristics.FIRSTSTRONG_LTR;
                }
                case 5: {
                    return TextDirectionHeuristics.LOCALE;
                }
                case 4: {
                    return TextDirectionHeuristics.RTL;
                }
                case 3: {
                    return TextDirectionHeuristics.LTR;
                }
                case 2: {
                    return TextDirectionHeuristics.ANYRTL_LTR;
                }
            }
        }
    }
    
    public static PrecomputedTextCompat.Params getTextMetricsParams(final TextView textView) {
        if (Build$VERSION.SDK_INT >= 28) {
            return new PrecomputedTextCompat.Params(textView.getTextMetricsParams());
        }
        final PrecomputedTextCompat.Params.Builder builder = new PrecomputedTextCompat.Params.Builder(new TextPaint((Paint)textView.getPaint()));
        if (Build$VERSION.SDK_INT >= 23) {
            builder.setBreakStrategy(textView.getBreakStrategy());
            builder.setHyphenationFrequency(textView.getHyphenationFrequency());
        }
        if (Build$VERSION.SDK_INT >= 18) {
            builder.setTextDirection(getTextDirectionHeuristic(textView));
        }
        return builder.build();
    }
    
    private static Field retrieveField(final String s) {
        Field declaredField = null;
        while (true) {
            try {
                final Field field = declaredField = TextView.class.getDeclaredField(s);
                field.setAccessible(true);
                return field;
                final StringBuilder sb = new StringBuilder();
                sb.append("Could not retrieve ");
                sb.append(s);
                sb.append(" field.");
                Log.e("TextViewCompat", sb.toString());
                return declaredField;
            }
            catch (NoSuchFieldException ex) {
                continue;
            }
            break;
        }
    }
    
    private static int retrieveIntFromField(final Field field, final TextView obj) {
        while (true) {
            try {
                return field.getInt(obj);
                final StringBuilder sb = new StringBuilder();
                sb.append("Could not retrieve value of ");
                sb.append(field.getName());
                sb.append(" field.");
                Log.d("TextViewCompat", sb.toString());
                return -1;
            }
            catch (IllegalAccessException ex) {
                continue;
            }
            break;
        }
    }
    
    public static void setAutoSizeTextTypeUniformWithConfiguration(final TextView textView, final int n, final int n2, final int n3, final int n4) throws IllegalArgumentException {
        if (Build$VERSION.SDK_INT >= 27) {
            textView.setAutoSizeTextTypeUniformWithConfiguration(n, n2, n3, n4);
            return;
        }
        if (textView instanceof AutoSizeableTextView) {
            ((AutoSizeableTextView)textView).setAutoSizeTextTypeUniformWithConfiguration(n, n2, n3, n4);
        }
    }
    
    public static void setAutoSizeTextTypeUniformWithPresetSizes(final TextView textView, final int[] array, final int n) throws IllegalArgumentException {
        if (Build$VERSION.SDK_INT >= 27) {
            textView.setAutoSizeTextTypeUniformWithPresetSizes(array, n);
            return;
        }
        if (textView instanceof AutoSizeableTextView) {
            ((AutoSizeableTextView)textView).setAutoSizeTextTypeUniformWithPresetSizes(array, n);
        }
    }
    
    public static void setAutoSizeTextTypeWithDefaults(final TextView textView, final int n) {
        if (Build$VERSION.SDK_INT >= 27) {
            textView.setAutoSizeTextTypeWithDefaults(n);
            return;
        }
        if (textView instanceof AutoSizeableTextView) {
            ((AutoSizeableTextView)textView).setAutoSizeTextTypeWithDefaults(n);
        }
    }
    
    public static void setCompoundDrawableTintList(final TextView textView, final ColorStateList list) {
        Preconditions.checkNotNull(textView);
        if (Build$VERSION.SDK_INT >= 23) {
            textView.setCompoundDrawableTintList(list);
            return;
        }
        if (textView instanceof TintableCompoundDrawablesView) {
            ((TintableCompoundDrawablesView)textView).setSupportCompoundDrawablesTintList(list);
        }
    }
    
    public static void setCompoundDrawableTintMode(final TextView textView, final PorterDuff$Mode porterDuff$Mode) {
        Preconditions.checkNotNull(textView);
        if (Build$VERSION.SDK_INT >= 23) {
            textView.setCompoundDrawableTintMode(porterDuff$Mode);
            return;
        }
        if (textView instanceof TintableCompoundDrawablesView) {
            ((TintableCompoundDrawablesView)textView).setSupportCompoundDrawablesTintMode(porterDuff$Mode);
        }
    }
    
    public static void setCompoundDrawablesRelative(final TextView textView, Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
        if (Build$VERSION.SDK_INT >= 18) {
            textView.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
            return;
        }
        if (Build$VERSION.SDK_INT >= 17) {
            final int layoutDirection = textView.getLayoutDirection();
            boolean b = true;
            if (layoutDirection != 1) {
                b = false;
            }
            Drawable drawable5;
            if (b) {
                drawable5 = drawable3;
            }
            else {
                drawable5 = drawable;
            }
            if (!b) {
                drawable = drawable3;
            }
            textView.setCompoundDrawables(drawable5, drawable2, drawable, drawable4);
            return;
        }
        textView.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
    }
    
    public static void setCompoundDrawablesRelativeWithIntrinsicBounds(final TextView textView, int n, final int n2, final int n3, final int n4) {
        if (Build$VERSION.SDK_INT >= 18) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(n, n2, n3, n4);
            return;
        }
        if (Build$VERSION.SDK_INT >= 17) {
            final int layoutDirection = textView.getLayoutDirection();
            boolean b = true;
            if (layoutDirection != 1) {
                b = false;
            }
            int n5;
            if (b) {
                n5 = n3;
            }
            else {
                n5 = n;
            }
            if (!b) {
                n = n3;
            }
            textView.setCompoundDrawablesWithIntrinsicBounds(n5, n2, n, n4);
            return;
        }
        textView.setCompoundDrawablesWithIntrinsicBounds(n, n2, n3, n4);
    }
    
    public static void setCompoundDrawablesRelativeWithIntrinsicBounds(final TextView textView, Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
        if (Build$VERSION.SDK_INT >= 18) {
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
            return;
        }
        if (Build$VERSION.SDK_INT >= 17) {
            final int layoutDirection = textView.getLayoutDirection();
            boolean b = true;
            if (layoutDirection != 1) {
                b = false;
            }
            Drawable drawable5;
            if (b) {
                drawable5 = drawable3;
            }
            else {
                drawable5 = drawable;
            }
            if (!b) {
                drawable = drawable3;
            }
            textView.setCompoundDrawablesWithIntrinsicBounds(drawable5, drawable2, drawable, drawable4);
            return;
        }
        textView.setCompoundDrawablesWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
    }
    
    public static void setCustomSelectionActionModeCallback(final TextView textView, final ActionMode$Callback actionMode$Callback) {
        textView.setCustomSelectionActionModeCallback(wrapCustomSelectionActionModeCallback(textView, actionMode$Callback));
    }
    
    public static void setFirstBaselineToTopHeight(final TextView textView, final int firstBaselineToTopHeight) {
        Preconditions.checkArgumentNonnegative(firstBaselineToTopHeight);
        if (Build$VERSION.SDK_INT >= 28) {
            textView.setFirstBaselineToTopHeight(firstBaselineToTopHeight);
            return;
        }
        final Paint$FontMetricsInt fontMetricsInt = textView.getPaint().getFontMetricsInt();
        int a;
        if (Build$VERSION.SDK_INT >= 16 && !textView.getIncludeFontPadding()) {
            a = fontMetricsInt.ascent;
        }
        else {
            a = fontMetricsInt.top;
        }
        if (firstBaselineToTopHeight > Math.abs(a)) {
            textView.setPadding(textView.getPaddingLeft(), firstBaselineToTopHeight - -a, textView.getPaddingRight(), textView.getPaddingBottom());
        }
    }
    
    public static void setLastBaselineToBottomHeight(final TextView textView, final int n) {
        Preconditions.checkArgumentNonnegative(n);
        final Paint$FontMetricsInt fontMetricsInt = textView.getPaint().getFontMetricsInt();
        int a;
        if (Build$VERSION.SDK_INT >= 16 && !textView.getIncludeFontPadding()) {
            a = fontMetricsInt.descent;
        }
        else {
            a = fontMetricsInt.bottom;
        }
        if (n > Math.abs(a)) {
            textView.setPadding(textView.getPaddingLeft(), textView.getPaddingTop(), textView.getPaddingRight(), n - a);
        }
    }
    
    public static void setLineHeight(final TextView textView, final int n) {
        Preconditions.checkArgumentNonnegative(n);
        final int fontMetricsInt = textView.getPaint().getFontMetricsInt((Paint$FontMetricsInt)null);
        if (n != fontMetricsInt) {
            textView.setLineSpacing((float)(n - fontMetricsInt), 1.0f);
        }
    }
    
    public static void setPrecomputedText(final TextView textView, final PrecomputedTextCompat text) {
        if (Build$VERSION.SDK_INT >= 29) {
            textView.setText((CharSequence)text.getPrecomputedText());
            return;
        }
        if (getTextMetricsParams(textView).equalsWithoutTextDirection(text.getParams())) {
            textView.setText((CharSequence)text);
            return;
        }
        throw new IllegalArgumentException("Given text can not be applied to TextView.");
    }
    
    public static void setTextAppearance(final TextView textView, final int textAppearance) {
        if (Build$VERSION.SDK_INT >= 23) {
            textView.setTextAppearance(textAppearance);
            return;
        }
        textView.setTextAppearance(textView.getContext(), textAppearance);
    }
    
    public static void setTextMetricsParams(final TextView textView, final PrecomputedTextCompat.Params params) {
        if (Build$VERSION.SDK_INT >= 18) {
            textView.setTextDirection(getTextDirection(params.getTextDirection()));
        }
        if (Build$VERSION.SDK_INT < 23) {
            final float textScaleX = params.getTextPaint().getTextScaleX();
            textView.getPaint().set(params.getTextPaint());
            if (textScaleX == textView.getTextScaleX()) {
                textView.setTextScaleX(textScaleX / 2.0f + 1.0f);
            }
            textView.setTextScaleX(textScaleX);
            return;
        }
        textView.getPaint().set(params.getTextPaint());
        textView.setBreakStrategy(params.getBreakStrategy());
        textView.setHyphenationFrequency(params.getHyphenationFrequency());
    }
    
    public static ActionMode$Callback wrapCustomSelectionActionModeCallback(final TextView textView, final ActionMode$Callback actionMode$Callback) {
        if (Build$VERSION.SDK_INT < 26 || Build$VERSION.SDK_INT > 27) {
            return actionMode$Callback;
        }
        if (actionMode$Callback instanceof OreoCallback) {
            return actionMode$Callback;
        }
        return (ActionMode$Callback)new OreoCallback(actionMode$Callback, textView);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface AutoSizeTextType {
    }
    
    private static class OreoCallback implements ActionMode$Callback
    {
        private static final int MENU_ITEM_ORDER_PROCESS_TEXT_INTENT_ACTIONS_START = 100;
        private final ActionMode$Callback mCallback;
        private boolean mCanUseMenuBuilderReferences;
        private boolean mInitializedMenuBuilderReferences;
        private Class<?> mMenuBuilderClass;
        private Method mMenuBuilderRemoveItemAtMethod;
        private final TextView mTextView;
        
        OreoCallback(final ActionMode$Callback mCallback, final TextView mTextView) {
            this.mCallback = mCallback;
            this.mTextView = mTextView;
            this.mInitializedMenuBuilderReferences = false;
        }
        
        private Intent createProcessTextIntent() {
            return new Intent().setAction("android.intent.action.PROCESS_TEXT").setType("text/plain");
        }
        
        private Intent createProcessTextIntentForResolveInfo(final ResolveInfo resolveInfo, final TextView textView) {
            return this.createProcessTextIntent().putExtra("android.intent.extra.PROCESS_TEXT_READONLY", this.isEditable(textView) ^ true).setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
        }
        
        private List<ResolveInfo> getSupportedActivities(final Context context, final PackageManager packageManager) {
            final ArrayList<ResolveInfo> list = new ArrayList<ResolveInfo>();
            if (!(context instanceof Activity)) {
                return list;
            }
            for (final ResolveInfo resolveInfo : packageManager.queryIntentActivities(this.createProcessTextIntent(), 0)) {
                if (this.isSupportedActivity(resolveInfo, context)) {
                    list.add(resolveInfo);
                }
            }
            return list;
        }
        
        private boolean isEditable(final TextView textView) {
            return textView instanceof Editable && textView.onCheckIsTextEditor() && textView.isEnabled();
        }
        
        private boolean isSupportedActivity(final ResolveInfo resolveInfo, final Context context) {
            final boolean equals = context.getPackageName().equals(resolveInfo.activityInfo.packageName);
            boolean b = true;
            if (equals) {
                return true;
            }
            if (!resolveInfo.activityInfo.exported) {
                return false;
            }
            if (resolveInfo.activityInfo.permission != null) {
                if (context.checkSelfPermission(resolveInfo.activityInfo.permission) == 0) {
                    return true;
                }
                b = false;
            }
            return b;
        }
        
        private void recomputeProcessTextMenuItems(final Menu p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: getfield        androidx/core/widget/TextViewCompat$OreoCallback.mTextView:Landroid/widget/TextView;
            //     4: invokevirtual   android/widget/TextView.getContext:()Landroid/content/Context;
            //     7: astore          5
            //     9: aload           5
            //    11: invokevirtual   android/content/Context.getPackageManager:()Landroid/content/pm/PackageManager;
            //    14: astore          4
            //    16: aload_0        
            //    17: getfield        androidx/core/widget/TextViewCompat$OreoCallback.mInitializedMenuBuilderReferences:Z
            //    20: ifne            83
            //    23: aload_0        
            //    24: iconst_1       
            //    25: putfield        androidx/core/widget/TextViewCompat$OreoCallback.mInitializedMenuBuilderReferences:Z
            //    28: aload_0        
            //    29: ldc             "com.android.internal.view.menu.MenuBuilder"
            //    31: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
            //    34: putfield        androidx/core/widget/TextViewCompat$OreoCallback.mMenuBuilderClass:Ljava/lang/Class;
            //    37: aload_0        
            //    38: aload_0        
            //    39: getfield        androidx/core/widget/TextViewCompat$OreoCallback.mMenuBuilderClass:Ljava/lang/Class;
            //    42: ldc             "removeItemAt"
            //    44: iconst_1       
            //    45: anewarray       Ljava/lang/Class;
            //    48: dup            
            //    49: iconst_0       
            //    50: getstatic       java/lang/Integer.TYPE:Ljava/lang/Class;
            //    53: aastore        
            //    54: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
            //    57: putfield        androidx/core/widget/TextViewCompat$OreoCallback.mMenuBuilderRemoveItemAtMethod:Ljava/lang/reflect/Method;
            //    60: aload_0        
            //    61: iconst_1       
            //    62: putfield        androidx/core/widget/TextViewCompat$OreoCallback.mCanUseMenuBuilderReferences:Z
            //    65: goto            83
            //    68: aload_0        
            //    69: aconst_null    
            //    70: putfield        androidx/core/widget/TextViewCompat$OreoCallback.mMenuBuilderClass:Ljava/lang/Class;
            //    73: aload_0        
            //    74: aconst_null    
            //    75: putfield        androidx/core/widget/TextViewCompat$OreoCallback.mMenuBuilderRemoveItemAtMethod:Ljava/lang/reflect/Method;
            //    78: aload_0        
            //    79: iconst_0       
            //    80: putfield        androidx/core/widget/TextViewCompat$OreoCallback.mCanUseMenuBuilderReferences:Z
            //    83: aload_0        
            //    84: getfield        androidx/core/widget/TextViewCompat$OreoCallback.mCanUseMenuBuilderReferences:Z
            //    87: ifeq            109
            //    90: aload_0        
            //    91: getfield        androidx/core/widget/TextViewCompat$OreoCallback.mMenuBuilderClass:Ljava/lang/Class;
            //    94: aload_1        
            //    95: invokevirtual   java/lang/Class.isInstance:(Ljava/lang/Object;)Z
            //    98: ifeq            109
            //   101: aload_0        
            //   102: getfield        androidx/core/widget/TextViewCompat$OreoCallback.mMenuBuilderRemoveItemAtMethod:Ljava/lang/reflect/Method;
            //   105: astore_3       
            //   106: goto            129
            //   109: aload_1        
            //   110: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
            //   113: ldc             "removeItemAt"
            //   115: iconst_1       
            //   116: anewarray       Ljava/lang/Class;
            //   119: dup            
            //   120: iconst_0       
            //   121: getstatic       java/lang/Integer.TYPE:Ljava/lang/Class;
            //   124: aastore        
            //   125: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
            //   128: astore_3       
            //   129: aload_1        
            //   130: invokeinterface android/view/Menu.size:()I
            //   135: iconst_1       
            //   136: isub           
            //   137: istore_2       
            //   138: iload_2        
            //   139: iflt            203
            //   142: aload_1        
            //   143: iload_2        
            //   144: invokeinterface android/view/Menu.getItem:(I)Landroid/view/MenuItem;
            //   149: astore          6
            //   151: aload           6
            //   153: invokeinterface android/view/MenuItem.getIntent:()Landroid/content/Intent;
            //   158: ifnull          196
            //   161: ldc             "android.intent.action.PROCESS_TEXT"
            //   163: aload           6
            //   165: invokeinterface android/view/MenuItem.getIntent:()Landroid/content/Intent;
            //   170: invokevirtual   android/content/Intent.getAction:()Ljava/lang/String;
            //   173: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
            //   176: ifeq            196
            //   179: aload_3        
            //   180: aload_1        
            //   181: iconst_1       
            //   182: anewarray       Ljava/lang/Object;
            //   185: dup            
            //   186: iconst_0       
            //   187: iload_2        
            //   188: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
            //   191: aastore        
            //   192: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
            //   195: pop            
            //   196: iload_2        
            //   197: iconst_1       
            //   198: isub           
            //   199: istore_2       
            //   200: goto            138
            //   203: aload_0        
            //   204: aload           5
            //   206: aload           4
            //   208: invokespecial   androidx/core/widget/TextViewCompat$OreoCallback.getSupportedActivities:(Landroid/content/Context;Landroid/content/pm/PackageManager;)Ljava/util/List;
            //   211: astore_3       
            //   212: iconst_0       
            //   213: istore_2       
            //   214: iload_2        
            //   215: aload_3        
            //   216: invokeinterface java/util/List.size:()I
            //   221: if_icmpge       283
            //   224: aload_3        
            //   225: iload_2        
            //   226: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
            //   231: checkcast       Landroid/content/pm/ResolveInfo;
            //   234: astore          5
            //   236: aload_1        
            //   237: iconst_0       
            //   238: iconst_0       
            //   239: iload_2        
            //   240: bipush          100
            //   242: iadd           
            //   243: aload           5
            //   245: aload           4
            //   247: invokevirtual   android/content/pm/ResolveInfo.loadLabel:(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
            //   250: invokeinterface android/view/Menu.add:(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
            //   255: aload_0        
            //   256: aload           5
            //   258: aload_0        
            //   259: getfield        androidx/core/widget/TextViewCompat$OreoCallback.mTextView:Landroid/widget/TextView;
            //   262: invokespecial   androidx/core/widget/TextViewCompat$OreoCallback.createProcessTextIntentForResolveInfo:(Landroid/content/pm/ResolveInfo;Landroid/widget/TextView;)Landroid/content/Intent;
            //   265: invokeinterface android/view/MenuItem.setIntent:(Landroid/content/Intent;)Landroid/view/MenuItem;
            //   270: iconst_1       
            //   271: invokeinterface android/view/MenuItem.setShowAsAction:(I)V
            //   276: iload_2        
            //   277: iconst_1       
            //   278: iadd           
            //   279: istore_2       
            //   280: goto            214
            //   283: return         
            //   284: astore_3       
            //   285: goto            68
            //   288: astore_1       
            //   289: return         
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                                         
            //  -----  -----  -----  -----  ---------------------------------------------
            //  28     65     284    83     Ljava/lang/ClassNotFoundException;
            //  28     65     284    83     Ljava/lang/NoSuchMethodException;
            //  83     106    288    290    Ljava/lang/NoSuchMethodException;
            //  83     106    288    290    Ljava/lang/IllegalAccessException;
            //  83     106    288    290    Ljava/lang/reflect/InvocationTargetException;
            //  109    129    288    290    Ljava/lang/NoSuchMethodException;
            //  109    129    288    290    Ljava/lang/IllegalAccessException;
            //  109    129    288    290    Ljava/lang/reflect/InvocationTargetException;
            //  129    138    288    290    Ljava/lang/NoSuchMethodException;
            //  129    138    288    290    Ljava/lang/IllegalAccessException;
            //  129    138    288    290    Ljava/lang/reflect/InvocationTargetException;
            //  142    196    288    290    Ljava/lang/NoSuchMethodException;
            //  142    196    288    290    Ljava/lang/IllegalAccessException;
            //  142    196    288    290    Ljava/lang/reflect/InvocationTargetException;
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0083:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:576)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        public boolean onActionItemClicked(final ActionMode actionMode, final MenuItem menuItem) {
            return this.mCallback.onActionItemClicked(actionMode, menuItem);
        }
        
        public boolean onCreateActionMode(final ActionMode actionMode, final Menu menu) {
            return this.mCallback.onCreateActionMode(actionMode, menu);
        }
        
        public void onDestroyActionMode(final ActionMode actionMode) {
            this.mCallback.onDestroyActionMode(actionMode);
        }
        
        public boolean onPrepareActionMode(final ActionMode actionMode, final Menu menu) {
            this.recomputeProcessTextMenuItems(menu);
            return this.mCallback.onPrepareActionMode(actionMode, menu);
        }
    }
}
