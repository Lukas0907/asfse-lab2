// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.view;

import android.view.ViewGroup$MarginLayoutParams;
import java.util.Iterator;
import kotlin.sequences.Sequence;
import kotlin.jvm.functions.Function2;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import android.view.View;
import android.view.ViewGroup;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000L\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010)\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\u001a\u0015\u0010\n\u001a\u00020\u000b*\u00020\u00032\u0006\u0010\f\u001a\u00020\u0002H\u0086\n\u001a0\u0010\r\u001a\u00020\u000e*\u00020\u00032!\u0010\u000f\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\f\u0012\u0004\u0012\u00020\u000e0\u0010H\u0086\b\u001aE\u0010\u0013\u001a\u00020\u000e*\u00020\u000326\u0010\u000f\u001a2\u0012\u0013\u0012\u00110\u0007¢\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\u0015\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0011\u0012\b\b\u0012\u0012\u0004\b\b(\f\u0012\u0004\u0012\u00020\u000e0\u0014H\u0086\b\u001a\u0015\u0010\u0016\u001a\u00020\u0002*\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u0007H\u0086\u0002\u001a\r\u0010\u0017\u001a\u00020\u000b*\u00020\u0003H\u0086\b\u001a\r\u0010\u0018\u001a\u00020\u000b*\u00020\u0003H\u0086\b\u001a\u0013\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00020\u001a*\u00020\u0003H\u0086\u0002\u001a\u0015\u0010\u001b\u001a\u00020\u000e*\u00020\u00032\u0006\u0010\f\u001a\u00020\u0002H\u0086\n\u001a\u0015\u0010\u001c\u001a\u00020\u000e*\u00020\u00032\u0006\u0010\f\u001a\u00020\u0002H\u0086\n\u001a\u0017\u0010\u001d\u001a\u00020\u000e*\u00020\u001e2\b\b\u0001\u0010\u0006\u001a\u00020\u0007H\u0086\b\u001a5\u0010\u001f\u001a\u00020\u000e*\u00020\u001e2\b\b\u0003\u0010 \u001a\u00020\u00072\b\b\u0003\u0010!\u001a\u00020\u00072\b\b\u0003\u0010\"\u001a\u00020\u00072\b\b\u0003\u0010#\u001a\u00020\u0007H\u0086\b\u001a5\u0010$\u001a\u00020\u000e*\u00020\u001e2\b\b\u0003\u0010%\u001a\u00020\u00072\b\b\u0003\u0010!\u001a\u00020\u00072\b\b\u0003\u0010&\u001a\u00020\u00072\b\b\u0003\u0010#\u001a\u00020\u0007H\u0087\b\"\u001b\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005\"\u0016\u0010\u0006\u001a\u00020\u0007*\u00020\u00038\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\b\u0010\t¨\u0006'" }, d2 = { "children", "Lkotlin/sequences/Sequence;", "Landroid/view/View;", "Landroid/view/ViewGroup;", "getChildren", "(Landroid/view/ViewGroup;)Lkotlin/sequences/Sequence;", "size", "", "getSize", "(Landroid/view/ViewGroup;)I", "contains", "", "view", "forEach", "", "action", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "forEachIndexed", "Lkotlin/Function2;", "index", "get", "isEmpty", "isNotEmpty", "iterator", "", "minusAssign", "plusAssign", "setMargins", "Landroid/view/ViewGroup$MarginLayoutParams;", "updateMargins", "left", "top", "right", "bottom", "updateMarginsRelative", "start", "end", "core-ktx_release" }, k = 2, mv = { 1, 1, 15 })
public final class ViewGroupKt
{
    public static final boolean contains(final ViewGroup viewGroup, final View view) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "$this$contains");
        Intrinsics.checkParameterIsNotNull(view, "view");
        return viewGroup.indexOfChild(view) != -1;
    }
    
    public static final void forEach(final ViewGroup viewGroup, final Function1<? super View, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "$this$forEach");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        for (int childCount = viewGroup.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = viewGroup.getChildAt(i);
            Intrinsics.checkExpressionValueIsNotNull(child, "getChildAt(index)");
            function1.invoke(child);
        }
    }
    
    public static final void forEachIndexed(final ViewGroup viewGroup, final Function2<? super Integer, ? super View, Unit> function2) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "$this$forEachIndexed");
        Intrinsics.checkParameterIsNotNull(function2, "action");
        for (int childCount = viewGroup.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = viewGroup.getChildAt(i);
            Intrinsics.checkExpressionValueIsNotNull(child, "getChildAt(index)");
            function2.invoke(i, child);
        }
    }
    
    public static final View get(final ViewGroup viewGroup, final int i) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "$this$get");
        final View child = viewGroup.getChildAt(i);
        if (child != null) {
            return child;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Index: ");
        sb.append(i);
        sb.append(", Size: ");
        sb.append(viewGroup.getChildCount());
        throw new IndexOutOfBoundsException(sb.toString());
    }
    
    public static final Sequence<View> getChildren(final ViewGroup viewGroup) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "$this$children");
        return (Sequence<View>)new ViewGroupKt$children.ViewGroupKt$children$1(viewGroup);
    }
    
    public static final int getSize(final ViewGroup viewGroup) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "$this$size");
        return viewGroup.getChildCount();
    }
    
    public static final boolean isEmpty(final ViewGroup viewGroup) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "$this$isEmpty");
        return viewGroup.getChildCount() == 0;
    }
    
    public static final boolean isNotEmpty(final ViewGroup viewGroup) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "$this$isNotEmpty");
        return viewGroup.getChildCount() != 0;
    }
    
    public static final Iterator<View> iterator(final ViewGroup viewGroup) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "$this$iterator");
        return (Iterator<View>)new ViewGroupKt$iterator.ViewGroupKt$iterator$1(viewGroup);
    }
    
    public static final void minusAssign(final ViewGroup viewGroup, final View view) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "$this$minusAssign");
        Intrinsics.checkParameterIsNotNull(view, "view");
        viewGroup.removeView(view);
    }
    
    public static final void plusAssign(final ViewGroup viewGroup, final View view) {
        Intrinsics.checkParameterIsNotNull(viewGroup, "$this$plusAssign");
        Intrinsics.checkParameterIsNotNull(view, "view");
        viewGroup.addView(view);
    }
    
    public static final void setMargins(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int n) {
        Intrinsics.checkParameterIsNotNull(viewGroup$MarginLayoutParams, "$this$setMargins");
        viewGroup$MarginLayoutParams.setMargins(n, n, n, n);
    }
    
    public static final void updateMargins(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int n, final int n2, final int n3, final int n4) {
        Intrinsics.checkParameterIsNotNull(viewGroup$MarginLayoutParams, "$this$updateMargins");
        viewGroup$MarginLayoutParams.setMargins(n, n2, n3, n4);
    }
    
    public static final void updateMarginsRelative(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams, final int marginStart, final int topMargin, final int marginEnd, final int bottomMargin) {
        Intrinsics.checkParameterIsNotNull(viewGroup$MarginLayoutParams, "$this$updateMarginsRelative");
        viewGroup$MarginLayoutParams.setMarginStart(marginStart);
        viewGroup$MarginLayoutParams.topMargin = topMargin;
        viewGroup$MarginLayoutParams.setMarginEnd(marginEnd);
        viewGroup$MarginLayoutParams.bottomMargin = bottomMargin;
    }
}
