// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.view;

import kotlin.TypeCastException;
import kotlin.jvm.functions.Function0;
import android.view.ViewGroup$LayoutParams;
import android.view.ViewGroup$MarginLayoutParams;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.graphics.Bitmap$Config;
import android.view.View$OnLayoutChangeListener;
import android.view.View$OnAttachStateChangeListener;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import android.view.View;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\\\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\r\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\u001a2\u0010\u0019\u001a\u00020\u001a*\u00020\u00032#\b\u0004\u0010\u001b\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(\u001f\u0012\u0004\u0012\u00020\u001a0\u001cH\u0086\b\u001a2\u0010 \u001a\u00020\u001a*\u00020\u00032#\b\u0004\u0010\u001b\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(\u001f\u0012\u0004\u0012\u00020\u001a0\u001cH\u0086\b\u001a2\u0010!\u001a\u00020\u001a*\u00020\u00032#\b\u0004\u0010\u001b\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(\u001f\u0012\u0004\u0012\u00020\u001a0\u001cH\u0086\b\u001a2\u0010\"\u001a\u00020\u001a*\u00020\u00032#\b\u0004\u0010\u001b\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(\u001f\u0012\u0004\u0012\u00020\u001a0\u001cH\u0086\b\u001a2\u0010#\u001a\u00020$*\u00020\u00032#\b\u0004\u0010\u001b\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u001d\u0012\b\b\u001e\u0012\u0004\b\b(\u001f\u0012\u0004\u0012\u00020\u001a0\u001cH\u0086\b\u001a\u0014\u0010%\u001a\u00020&*\u00020\u00032\b\b\u0002\u0010'\u001a\u00020(\u001a%\u0010)\u001a\u00020**\u00020\u00032\u0006\u0010+\u001a\u00020,2\u000e\b\u0004\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001a0-H\u0086\b\u001a%\u0010.\u001a\u00020**\u00020\u00032\u0006\u0010+\u001a\u00020,2\u000e\b\u0004\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001a0-H\u0087\b\u001a\u0017\u0010/\u001a\u00020\u001a*\u00020\u00032\b\b\u0001\u00100\u001a\u00020\fH\u0086\b\u001a7\u00101\u001a\u00020\u001a\"\n\b\u0000\u00102\u0018\u0001*\u000203*\u00020\u00032\u0017\u00104\u001a\u0013\u0012\u0004\u0012\u0002H2\u0012\u0004\u0012\u00020\u001a0\u001c¢\u0006\u0002\b5H\u0087\b¢\u0006\u0002\b6\u001a&\u00101\u001a\u00020\u001a*\u00020\u00032\u0017\u00104\u001a\u0013\u0012\u0004\u0012\u000203\u0012\u0004\u0012\u00020\u001a0\u001c¢\u0006\u0002\b5H\u0086\b\u001a5\u00107\u001a\u00020\u001a*\u00020\u00032\b\b\u0003\u00108\u001a\u00020\f2\b\b\u0003\u00109\u001a\u00020\f2\b\b\u0003\u0010:\u001a\u00020\f2\b\b\u0003\u0010;\u001a\u00020\fH\u0086\b\u001a5\u0010<\u001a\u00020\u001a*\u00020\u00032\b\b\u0003\u0010=\u001a\u00020\f2\b\b\u0003\u00109\u001a\u00020\f2\b\b\u0003\u0010>\u001a\u00020\f2\b\b\u0003\u0010;\u001a\u00020\fH\u0087\b\"*\u0010\u0002\u001a\u00020\u0001*\u00020\u00032\u0006\u0010\u0000\u001a\u00020\u00018\u00c6\u0002@\u00c6\u0002X\u0086\u000e¢\u0006\f\u001a\u0004\b\u0002\u0010\u0004\"\u0004\b\u0005\u0010\u0006\"*\u0010\u0007\u001a\u00020\u0001*\u00020\u00032\u0006\u0010\u0000\u001a\u00020\u00018\u00c6\u0002@\u00c6\u0002X\u0086\u000e¢\u0006\f\u001a\u0004\b\u0007\u0010\u0004\"\u0004\b\b\u0010\u0006\"*\u0010\t\u001a\u00020\u0001*\u00020\u00032\u0006\u0010\u0000\u001a\u00020\u00018\u00c6\u0002@\u00c6\u0002X\u0086\u000e¢\u0006\f\u001a\u0004\b\t\u0010\u0004\"\u0004\b\n\u0010\u0006\"\u0016\u0010\u000b\u001a\u00020\f*\u00020\u00038\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\r\u0010\u000e\"\u0016\u0010\u000f\u001a\u00020\f*\u00020\u00038\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u000e\"\u0016\u0010\u0011\u001a\u00020\f*\u00020\u00038\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u000e\"\u0016\u0010\u0013\u001a\u00020\f*\u00020\u00038\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u0014\u0010\u000e\"\u0016\u0010\u0015\u001a\u00020\f*\u00020\u00038\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u000e\"\u0016\u0010\u0017\u001a\u00020\f*\u00020\u00038\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u000e¨\u0006?" }, d2 = { "value", "", "isGone", "Landroid/view/View;", "(Landroid/view/View;)Z", "setGone", "(Landroid/view/View;Z)V", "isInvisible", "setInvisible", "isVisible", "setVisible", "marginBottom", "", "getMarginBottom", "(Landroid/view/View;)I", "marginEnd", "getMarginEnd", "marginLeft", "getMarginLeft", "marginRight", "getMarginRight", "marginStart", "getMarginStart", "marginTop", "getMarginTop", "doOnAttach", "", "action", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "view", "doOnDetach", "doOnLayout", "doOnNextLayout", "doOnPreDraw", "Landroidx/core/view/OneShotPreDrawListener;", "drawToBitmap", "Landroid/graphics/Bitmap;", "config", "Landroid/graphics/Bitmap$Config;", "postDelayed", "Ljava/lang/Runnable;", "delayInMillis", "", "Lkotlin/Function0;", "postOnAnimationDelayed", "setPadding", "size", "updateLayoutParams", "T", "Landroid/view/ViewGroup$LayoutParams;", "block", "Lkotlin/ExtensionFunctionType;", "updateLayoutParamsTyped", "updatePadding", "left", "top", "right", "bottom", "updatePaddingRelative", "start", "end", "core-ktx_release" }, k = 2, mv = { 1, 1, 15 })
public final class ViewKt
{
    public static final void doOnAttach(final View view, final Function1<? super View, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(view, "$this$doOnAttach");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        if (ViewCompat.isAttachedToWindow(view)) {
            function1.invoke(view);
            return;
        }
        view.addOnAttachStateChangeListener((View$OnAttachStateChangeListener)new ViewKt$doOnAttach.ViewKt$doOnAttach$1(view, (Function1)function1));
    }
    
    public static final void doOnDetach(final View view, final Function1<? super View, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(view, "$this$doOnDetach");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        if (!ViewCompat.isAttachedToWindow(view)) {
            function1.invoke(view);
            return;
        }
        view.addOnAttachStateChangeListener((View$OnAttachStateChangeListener)new ViewKt$doOnDetach.ViewKt$doOnDetach$1(view, (Function1)function1));
    }
    
    public static final void doOnLayout(final View view, final Function1<? super View, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(view, "$this$doOnLayout");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        if (ViewCompat.isLaidOut(view) && !view.isLayoutRequested()) {
            function1.invoke(view);
            return;
        }
        view.addOnLayoutChangeListener((View$OnLayoutChangeListener)new ViewKt$doOnLayout$$inlined$doOnNextLayout.ViewKt$doOnLayout$$inlined$doOnNextLayout$1((Function1)function1));
    }
    
    public static final void doOnNextLayout(final View view, final Function1<? super View, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(view, "$this$doOnNextLayout");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        view.addOnLayoutChangeListener((View$OnLayoutChangeListener)new ViewKt$doOnNextLayout.ViewKt$doOnNextLayout$1((Function1)function1));
    }
    
    public static final OneShotPreDrawListener doOnPreDraw(final View view, final Function1<? super View, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(view, "$this$doOnPreDraw");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        final OneShotPreDrawListener add = OneShotPreDrawListener.add(view, (Runnable)new ViewKt$doOnPreDraw.ViewKt$doOnPreDraw$1(view, (Function1)function1));
        Intrinsics.checkExpressionValueIsNotNull(add, "OneShotPreDrawListener.add(this) { action(this) }");
        return add;
    }
    
    public static final Bitmap drawToBitmap(final View view, final Bitmap$Config bitmap$Config) {
        Intrinsics.checkParameterIsNotNull(view, "$this$drawToBitmap");
        Intrinsics.checkParameterIsNotNull(bitmap$Config, "config");
        if (ViewCompat.isLaidOut(view)) {
            final Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), bitmap$Config);
            Intrinsics.checkExpressionValueIsNotNull(bitmap, "Bitmap.createBitmap(width, height, config)");
            final Canvas canvas = new Canvas(bitmap);
            canvas.translate(-(float)view.getScrollX(), -(float)view.getScrollY());
            view.draw(canvas);
            return bitmap;
        }
        throw new IllegalStateException("View needs to be laid out before calling drawToBitmap()");
    }
    
    public static final int getMarginBottom(final View view) {
        Intrinsics.checkParameterIsNotNull(view, "$this$marginBottom");
        Object layoutParams;
        if (!((layoutParams = view.getLayoutParams()) instanceof ViewGroup$MarginLayoutParams)) {
            layoutParams = null;
        }
        final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)layoutParams;
        if (viewGroup$MarginLayoutParams != null) {
            return viewGroup$MarginLayoutParams.bottomMargin;
        }
        return 0;
    }
    
    public static final int getMarginEnd(final View view) {
        Intrinsics.checkParameterIsNotNull(view, "$this$marginEnd");
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams instanceof ViewGroup$MarginLayoutParams) {
            return MarginLayoutParamsCompat.getMarginEnd((ViewGroup$MarginLayoutParams)layoutParams);
        }
        return 0;
    }
    
    public static final int getMarginLeft(final View view) {
        Intrinsics.checkParameterIsNotNull(view, "$this$marginLeft");
        Object layoutParams;
        if (!((layoutParams = view.getLayoutParams()) instanceof ViewGroup$MarginLayoutParams)) {
            layoutParams = null;
        }
        final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)layoutParams;
        if (viewGroup$MarginLayoutParams != null) {
            return viewGroup$MarginLayoutParams.leftMargin;
        }
        return 0;
    }
    
    public static final int getMarginRight(final View view) {
        Intrinsics.checkParameterIsNotNull(view, "$this$marginRight");
        Object layoutParams;
        if (!((layoutParams = view.getLayoutParams()) instanceof ViewGroup$MarginLayoutParams)) {
            layoutParams = null;
        }
        final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)layoutParams;
        if (viewGroup$MarginLayoutParams != null) {
            return viewGroup$MarginLayoutParams.rightMargin;
        }
        return 0;
    }
    
    public static final int getMarginStart(final View view) {
        Intrinsics.checkParameterIsNotNull(view, "$this$marginStart");
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams instanceof ViewGroup$MarginLayoutParams) {
            return MarginLayoutParamsCompat.getMarginStart((ViewGroup$MarginLayoutParams)layoutParams);
        }
        return 0;
    }
    
    public static final int getMarginTop(final View view) {
        Intrinsics.checkParameterIsNotNull(view, "$this$marginTop");
        Object layoutParams;
        if (!((layoutParams = view.getLayoutParams()) instanceof ViewGroup$MarginLayoutParams)) {
            layoutParams = null;
        }
        final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)layoutParams;
        if (viewGroup$MarginLayoutParams != null) {
            return viewGroup$MarginLayoutParams.topMargin;
        }
        return 0;
    }
    
    public static final boolean isGone(final View view) {
        Intrinsics.checkParameterIsNotNull(view, "$this$isGone");
        return view.getVisibility() == 8;
    }
    
    public static final boolean isInvisible(final View view) {
        Intrinsics.checkParameterIsNotNull(view, "$this$isInvisible");
        return view.getVisibility() == 4;
    }
    
    public static final boolean isVisible(final View view) {
        Intrinsics.checkParameterIsNotNull(view, "$this$isVisible");
        return view.getVisibility() == 0;
    }
    
    public static final Runnable postDelayed(final View view, final long n, final Function0<Unit> function0) {
        Intrinsics.checkParameterIsNotNull(view, "$this$postDelayed");
        Intrinsics.checkParameterIsNotNull(function0, "action");
        final Runnable runnable = (Runnable)new ViewKt$postDelayed$runnable.ViewKt$postDelayed$runnable$1((Function0)function0);
        view.postDelayed(runnable, n);
        return runnable;
    }
    
    public static final Runnable postOnAnimationDelayed(final View view, final long n, final Function0<Unit> function0) {
        Intrinsics.checkParameterIsNotNull(view, "$this$postOnAnimationDelayed");
        Intrinsics.checkParameterIsNotNull(function0, "action");
        final Runnable runnable = (Runnable)new ViewKt$postOnAnimationDelayed$runnable.ViewKt$postOnAnimationDelayed$runnable$1((Function0)function0);
        view.postOnAnimationDelayed(runnable, n);
        return runnable;
    }
    
    public static final void setGone(final View view, final boolean b) {
        Intrinsics.checkParameterIsNotNull(view, "$this$isGone");
        int visibility;
        if (b) {
            visibility = 8;
        }
        else {
            visibility = 0;
        }
        view.setVisibility(visibility);
    }
    
    public static final void setInvisible(final View view, final boolean b) {
        Intrinsics.checkParameterIsNotNull(view, "$this$isInvisible");
        int visibility;
        if (b) {
            visibility = 4;
        }
        else {
            visibility = 0;
        }
        view.setVisibility(visibility);
    }
    
    public static final void setPadding(final View view, final int n) {
        Intrinsics.checkParameterIsNotNull(view, "$this$setPadding");
        view.setPadding(n, n, n, n);
    }
    
    public static final void setVisible(final View view, final boolean b) {
        Intrinsics.checkParameterIsNotNull(view, "$this$isVisible");
        int visibility;
        if (b) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        view.setVisibility(visibility);
    }
    
    public static final void updateLayoutParams(final View view, final Function1<? super ViewGroup$LayoutParams, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(view, "$this$updateLayoutParams");
        Intrinsics.checkParameterIsNotNull(function1, "block");
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams != null) {
            function1.invoke(layoutParams);
            view.setLayoutParams(layoutParams);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type android.view.ViewGroup.LayoutParams");
    }
    
    public static final void updatePadding(final View view, final int n, final int n2, final int n3, final int n4) {
        Intrinsics.checkParameterIsNotNull(view, "$this$updatePadding");
        view.setPadding(n, n2, n3, n4);
    }
    
    public static final void updatePaddingRelative(final View view, final int n, final int n2, final int n3, final int n4) {
        Intrinsics.checkParameterIsNotNull(view, "$this$updatePaddingRelative");
        view.setPaddingRelative(n, n2, n3, n4);
    }
}
