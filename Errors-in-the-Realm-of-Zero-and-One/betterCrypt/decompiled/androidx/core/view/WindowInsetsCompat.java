// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.view;

import android.graphics.Rect;
import androidx.core.graphics.Insets;
import androidx.core.util.ObjectsCompat;
import java.util.Objects;
import android.view.WindowInsets;
import android.os.Build$VERSION;

public class WindowInsetsCompat
{
    private final Object mInsets;
    
    public WindowInsetsCompat(final WindowInsetsCompat windowInsetsCompat) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        final Object o = null;
        if (sdk_INT >= 20) {
            Object mInsets;
            if (windowInsetsCompat == null) {
                mInsets = o;
            }
            else {
                mInsets = new WindowInsets((WindowInsets)windowInsetsCompat.mInsets);
            }
            this.mInsets = mInsets;
            return;
        }
        this.mInsets = null;
    }
    
    WindowInsetsCompat(final Object mInsets) {
        this.mInsets = mInsets;
    }
    
    public static WindowInsetsCompat toWindowInsetsCompat(final WindowInsets obj) {
        return new WindowInsetsCompat(Objects.requireNonNull(obj));
    }
    
    public WindowInsetsCompat consumeDisplayCutout() {
        if (Build$VERSION.SDK_INT >= 28) {
            return new WindowInsetsCompat(((WindowInsets)this.mInsets).consumeDisplayCutout());
        }
        return this;
    }
    
    public WindowInsetsCompat consumeStableInsets() {
        if (Build$VERSION.SDK_INT >= 21) {
            return new WindowInsetsCompat(((WindowInsets)this.mInsets).consumeStableInsets());
        }
        return null;
    }
    
    public WindowInsetsCompat consumeSystemWindowInsets() {
        if (Build$VERSION.SDK_INT >= 20) {
            return new WindowInsetsCompat(((WindowInsets)this.mInsets).consumeSystemWindowInsets());
        }
        return null;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o != null && this.getClass() == o.getClass() && ObjectsCompat.equals(this.mInsets, ((WindowInsetsCompat)o).mInsets));
    }
    
    public DisplayCutoutCompat getDisplayCutout() {
        if (Build$VERSION.SDK_INT >= 28) {
            return DisplayCutoutCompat.wrap(((WindowInsets)this.mInsets).getDisplayCutout());
        }
        return null;
    }
    
    public Insets getMandatorySystemGestureInsets() {
        if (Build$VERSION.SDK_INT >= 29) {
            return Insets.wrap(((WindowInsets)this.mInsets).getMandatorySystemGestureInsets());
        }
        return this.getSystemWindowInsets();
    }
    
    public int getStableInsetBottom() {
        if (Build$VERSION.SDK_INT >= 21) {
            return ((WindowInsets)this.mInsets).getStableInsetBottom();
        }
        return 0;
    }
    
    public int getStableInsetLeft() {
        if (Build$VERSION.SDK_INT >= 21) {
            return ((WindowInsets)this.mInsets).getStableInsetLeft();
        }
        return 0;
    }
    
    public int getStableInsetRight() {
        if (Build$VERSION.SDK_INT >= 21) {
            return ((WindowInsets)this.mInsets).getStableInsetRight();
        }
        return 0;
    }
    
    public int getStableInsetTop() {
        if (Build$VERSION.SDK_INT >= 21) {
            return ((WindowInsets)this.mInsets).getStableInsetTop();
        }
        return 0;
    }
    
    public Insets getStableInsets() {
        if (Build$VERSION.SDK_INT >= 29) {
            return Insets.wrap(((WindowInsets)this.mInsets).getStableInsets());
        }
        return Insets.of(this.getStableInsetLeft(), this.getStableInsetTop(), this.getStableInsetRight(), this.getStableInsetBottom());
    }
    
    public Insets getSystemGestureInsets() {
        if (Build$VERSION.SDK_INT >= 29) {
            return Insets.wrap(((WindowInsets)this.mInsets).getSystemGestureInsets());
        }
        return this.getSystemWindowInsets();
    }
    
    public int getSystemWindowInsetBottom() {
        if (Build$VERSION.SDK_INT >= 20) {
            return ((WindowInsets)this.mInsets).getSystemWindowInsetBottom();
        }
        return 0;
    }
    
    public int getSystemWindowInsetLeft() {
        if (Build$VERSION.SDK_INT >= 20) {
            return ((WindowInsets)this.mInsets).getSystemWindowInsetLeft();
        }
        return 0;
    }
    
    public int getSystemWindowInsetRight() {
        if (Build$VERSION.SDK_INT >= 20) {
            return ((WindowInsets)this.mInsets).getSystemWindowInsetRight();
        }
        return 0;
    }
    
    public int getSystemWindowInsetTop() {
        if (Build$VERSION.SDK_INT >= 20) {
            return ((WindowInsets)this.mInsets).getSystemWindowInsetTop();
        }
        return 0;
    }
    
    public Insets getSystemWindowInsets() {
        if (Build$VERSION.SDK_INT >= 29) {
            return Insets.wrap(((WindowInsets)this.mInsets).getSystemWindowInsets());
        }
        return Insets.of(this.getSystemWindowInsetLeft(), this.getSystemWindowInsetTop(), this.getSystemWindowInsetRight(), this.getSystemWindowInsetBottom());
    }
    
    public Insets getTappableElementInsets() {
        if (Build$VERSION.SDK_INT >= 29) {
            return Insets.wrap(((WindowInsets)this.mInsets).getTappableElementInsets());
        }
        return this.getSystemWindowInsets();
    }
    
    public boolean hasInsets() {
        return Build$VERSION.SDK_INT >= 20 && ((WindowInsets)this.mInsets).hasInsets();
    }
    
    public boolean hasStableInsets() {
        return Build$VERSION.SDK_INT >= 21 && ((WindowInsets)this.mInsets).hasStableInsets();
    }
    
    public boolean hasSystemWindowInsets() {
        return Build$VERSION.SDK_INT >= 20 && ((WindowInsets)this.mInsets).hasSystemWindowInsets();
    }
    
    @Override
    public int hashCode() {
        final Object mInsets = this.mInsets;
        if (mInsets == null) {
            return 0;
        }
        return mInsets.hashCode();
    }
    
    public boolean isConsumed() {
        return Build$VERSION.SDK_INT >= 21 && ((WindowInsets)this.mInsets).isConsumed();
    }
    
    public boolean isRound() {
        return Build$VERSION.SDK_INT >= 20 && ((WindowInsets)this.mInsets).isRound();
    }
    
    public WindowInsetsCompat replaceSystemWindowInsets(final int n, final int n2, final int n3, final int n4) {
        if (Build$VERSION.SDK_INT >= 20) {
            return new WindowInsetsCompat(((WindowInsets)this.mInsets).replaceSystemWindowInsets(n, n2, n3, n4));
        }
        return null;
    }
    
    public WindowInsetsCompat replaceSystemWindowInsets(final Rect rect) {
        if (Build$VERSION.SDK_INT >= 21) {
            return new WindowInsetsCompat(((WindowInsets)this.mInsets).replaceSystemWindowInsets(rect));
        }
        return null;
    }
    
    public WindowInsets toWindowInsets() {
        return (WindowInsets)this.mInsets;
    }
}
