// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.app;

import android.os.Build$VERSION;
import android.app.RemoteAction;
import androidx.core.util.Preconditions;
import androidx.core.graphics.drawable.IconCompat;
import android.app.PendingIntent;
import androidx.versionedparcelable.VersionedParcelable;

public final class RemoteActionCompat implements VersionedParcelable
{
    public PendingIntent mActionIntent;
    public CharSequence mContentDescription;
    public boolean mEnabled;
    public IconCompat mIcon;
    public boolean mShouldShowIcon;
    public CharSequence mTitle;
    
    public RemoteActionCompat() {
    }
    
    public RemoteActionCompat(final RemoteActionCompat remoteActionCompat) {
        Preconditions.checkNotNull(remoteActionCompat);
        this.mIcon = remoteActionCompat.mIcon;
        this.mTitle = remoteActionCompat.mTitle;
        this.mContentDescription = remoteActionCompat.mContentDescription;
        this.mActionIntent = remoteActionCompat.mActionIntent;
        this.mEnabled = remoteActionCompat.mEnabled;
        this.mShouldShowIcon = remoteActionCompat.mShouldShowIcon;
    }
    
    public RemoteActionCompat(final IconCompat iconCompat, final CharSequence charSequence, final CharSequence charSequence2, final PendingIntent pendingIntent) {
        this.mIcon = Preconditions.checkNotNull(iconCompat);
        this.mTitle = Preconditions.checkNotNull(charSequence);
        this.mContentDescription = Preconditions.checkNotNull(charSequence2);
        this.mActionIntent = Preconditions.checkNotNull(pendingIntent);
        this.mEnabled = true;
        this.mShouldShowIcon = true;
    }
    
    public static RemoteActionCompat createFromRemoteAction(final RemoteAction remoteAction) {
        Preconditions.checkNotNull(remoteAction);
        final RemoteActionCompat remoteActionCompat = new RemoteActionCompat(IconCompat.createFromIcon(remoteAction.getIcon()), remoteAction.getTitle(), remoteAction.getContentDescription(), remoteAction.getActionIntent());
        remoteActionCompat.setEnabled(remoteAction.isEnabled());
        if (Build$VERSION.SDK_INT >= 28) {
            remoteActionCompat.setShouldShowIcon(remoteAction.shouldShowIcon());
        }
        return remoteActionCompat;
    }
    
    public PendingIntent getActionIntent() {
        return this.mActionIntent;
    }
    
    public CharSequence getContentDescription() {
        return this.mContentDescription;
    }
    
    public IconCompat getIcon() {
        return this.mIcon;
    }
    
    public CharSequence getTitle() {
        return this.mTitle;
    }
    
    public boolean isEnabled() {
        return this.mEnabled;
    }
    
    public void setEnabled(final boolean mEnabled) {
        this.mEnabled = mEnabled;
    }
    
    public void setShouldShowIcon(final boolean mShouldShowIcon) {
        this.mShouldShowIcon = mShouldShowIcon;
    }
    
    public boolean shouldShowIcon() {
        return this.mShouldShowIcon;
    }
    
    public RemoteAction toRemoteAction() {
        final RemoteAction remoteAction = new RemoteAction(this.mIcon.toIcon(), this.mTitle, this.mContentDescription, this.mActionIntent);
        remoteAction.setEnabled(this.isEnabled());
        if (Build$VERSION.SDK_INT >= 28) {
            remoteAction.setShouldShowIcon(this.shouldShowIcon());
        }
        return remoteAction;
    }
}
