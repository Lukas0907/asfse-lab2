// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.util;

public interface Supplier<T>
{
    T get();
}
