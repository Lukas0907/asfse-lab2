// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.util;

import kotlin.jvm.internal.Intrinsics;
import android.util.Half;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0006\n\u0002\u0010\u0007\n\u0002\u0010\n\n\u0002\u0010\u000e\n\u0000\u001a\r\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0087\b\u001a\r\u0010\u0000\u001a\u00020\u0001*\u00020\u0003H\u0087\b\u001a\r\u0010\u0000\u001a\u00020\u0001*\u00020\u0004H\u0087\b\u001a\r\u0010\u0000\u001a\u00020\u0001*\u00020\u0005H\u0087\b¨\u0006\u0006" }, d2 = { "toHalf", "Landroid/util/Half;", "", "", "", "", "core-ktx_release" }, k = 2, mv = { 1, 1, 15 })
public final class HalfKt
{
    public static final Half toHalf(final double n) {
        final Half value = Half.valueOf((float)n);
        Intrinsics.checkExpressionValueIsNotNull(value, "Half.valueOf(this)");
        return value;
    }
    
    public static final Half toHalf(final float n) {
        final Half value = Half.valueOf(n);
        Intrinsics.checkExpressionValueIsNotNull(value, "Half.valueOf(this)");
        return value;
    }
    
    public static final Half toHalf(final String s) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toHalf");
        final Half value = Half.valueOf(s);
        Intrinsics.checkExpressionValueIsNotNull(value, "Half.valueOf(this)");
        return value;
    }
    
    public static final Half toHalf(final short n) {
        final Half value = Half.valueOf(n);
        Intrinsics.checkExpressionValueIsNotNull(value, "Half.valueOf(this)");
        return value;
    }
}
