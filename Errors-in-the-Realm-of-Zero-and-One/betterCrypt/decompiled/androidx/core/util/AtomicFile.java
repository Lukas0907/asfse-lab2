// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.util;

import java.io.FileNotFoundException;
import java.io.FileInputStream;
import android.util.Log;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.File;

public class AtomicFile
{
    private final File mBackupName;
    private final File mBaseName;
    
    public AtomicFile(final File mBaseName) {
        this.mBaseName = mBaseName;
        final StringBuilder sb = new StringBuilder();
        sb.append(mBaseName.getPath());
        sb.append(".bak");
        this.mBackupName = new File(sb.toString());
    }
    
    private static boolean sync(final FileOutputStream fileOutputStream) {
        try {
            fileOutputStream.getFD().sync();
            return true;
        }
        catch (IOException ex) {
            return false;
        }
    }
    
    public void delete() {
        this.mBaseName.delete();
        this.mBackupName.delete();
    }
    
    public void failWrite(final FileOutputStream fileOutputStream) {
        if (fileOutputStream != null) {
            sync(fileOutputStream);
            try {
                fileOutputStream.close();
                this.mBaseName.delete();
                this.mBackupName.renameTo(this.mBaseName);
            }
            catch (IOException ex) {
                Log.w("AtomicFile", "failWrite: Got exception:", (Throwable)ex);
            }
        }
    }
    
    public void finishWrite(final FileOutputStream fileOutputStream) {
        if (fileOutputStream != null) {
            sync(fileOutputStream);
            try {
                fileOutputStream.close();
                this.mBackupName.delete();
            }
            catch (IOException ex) {
                Log.w("AtomicFile", "finishWrite: Got exception:", (Throwable)ex);
            }
        }
    }
    
    public File getBaseFile() {
        return this.mBaseName;
    }
    
    public FileInputStream openRead() throws FileNotFoundException {
        if (this.mBackupName.exists()) {
            this.mBaseName.delete();
            this.mBackupName.renameTo(this.mBaseName);
        }
        return new FileInputStream(this.mBaseName);
    }
    
    public byte[] readFully() throws IOException {
        final FileInputStream openRead = this.openRead();
        try {
            byte[] b = new byte[openRead.available()];
            int off = 0;
            while (true) {
                final int read = openRead.read(b, off, b.length - off);
                if (read <= 0) {
                    break;
                }
                final int n = off + read;
                final int available = openRead.available();
                off = n;
                if (available <= b.length - n) {
                    continue;
                }
                final byte[] array = new byte[available + n];
                System.arraycopy(b, 0, array, 0, n);
                b = array;
                off = n;
            }
            return b;
        }
        finally {
            openRead.close();
        }
    }
    
    public FileOutputStream startWrite() throws IOException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        androidx/core/util/AtomicFile.mBaseName:Ljava/io/File;
        //     4: invokevirtual   java/io/File.exists:()Z
        //     7: ifeq            95
        //    10: aload_0        
        //    11: getfield        androidx/core/util/AtomicFile.mBackupName:Ljava/io/File;
        //    14: invokevirtual   java/io/File.exists:()Z
        //    17: ifne            87
        //    20: aload_0        
        //    21: getfield        androidx/core/util/AtomicFile.mBaseName:Ljava/io/File;
        //    24: aload_0        
        //    25: getfield        androidx/core/util/AtomicFile.mBackupName:Ljava/io/File;
        //    28: invokevirtual   java/io/File.renameTo:(Ljava/io/File;)Z
        //    31: ifne            95
        //    34: new             Ljava/lang/StringBuilder;
        //    37: dup            
        //    38: invokespecial   java/lang/StringBuilder.<init>:()V
        //    41: astore_1       
        //    42: aload_1        
        //    43: ldc             "Couldn't rename file "
        //    45: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    48: pop            
        //    49: aload_1        
        //    50: aload_0        
        //    51: getfield        androidx/core/util/AtomicFile.mBaseName:Ljava/io/File;
        //    54: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //    57: pop            
        //    58: aload_1        
        //    59: ldc             " to backup file "
        //    61: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    64: pop            
        //    65: aload_1        
        //    66: aload_0        
        //    67: getfield        androidx/core/util/AtomicFile.mBackupName:Ljava/io/File;
        //    70: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //    73: pop            
        //    74: ldc             "AtomicFile"
        //    76: aload_1        
        //    77: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    80: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
        //    83: pop            
        //    84: goto            95
        //    87: aload_0        
        //    88: getfield        androidx/core/util/AtomicFile.mBaseName:Ljava/io/File;
        //    91: invokevirtual   java/io/File.delete:()Z
        //    94: pop            
        //    95: new             Ljava/io/FileOutputStream;
        //    98: dup            
        //    99: aload_0        
        //   100: getfield        androidx/core/util/AtomicFile.mBaseName:Ljava/io/File;
        //   103: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;)V
        //   106: astore_1       
        //   107: aload_1        
        //   108: areturn        
        //   109: aload_0        
        //   110: getfield        androidx/core/util/AtomicFile.mBaseName:Ljava/io/File;
        //   113: invokevirtual   java/io/File.getParentFile:()Ljava/io/File;
        //   116: invokevirtual   java/io/File.mkdirs:()Z
        //   119: ifeq            172
        //   122: new             Ljava/io/FileOutputStream;
        //   125: dup            
        //   126: aload_0        
        //   127: getfield        androidx/core/util/AtomicFile.mBaseName:Ljava/io/File;
        //   130: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;)V
        //   133: astore_1       
        //   134: aload_1        
        //   135: areturn        
        //   136: new             Ljava/lang/StringBuilder;
        //   139: dup            
        //   140: invokespecial   java/lang/StringBuilder.<init>:()V
        //   143: astore_1       
        //   144: aload_1        
        //   145: ldc             "Couldn't create "
        //   147: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   150: pop            
        //   151: aload_1        
        //   152: aload_0        
        //   153: getfield        androidx/core/util/AtomicFile.mBaseName:Ljava/io/File;
        //   156: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   159: pop            
        //   160: new             Ljava/io/IOException;
        //   163: dup            
        //   164: aload_1        
        //   165: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   168: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   171: athrow         
        //   172: new             Ljava/lang/StringBuilder;
        //   175: dup            
        //   176: invokespecial   java/lang/StringBuilder.<init>:()V
        //   179: astore_1       
        //   180: aload_1        
        //   181: ldc             "Couldn't create directory "
        //   183: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   186: pop            
        //   187: aload_1        
        //   188: aload_0        
        //   189: getfield        androidx/core/util/AtomicFile.mBaseName:Ljava/io/File;
        //   192: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   195: pop            
        //   196: new             Ljava/io/IOException;
        //   199: dup            
        //   200: aload_1        
        //   201: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   204: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   207: athrow         
        //   208: astore_1       
        //   209: goto            109
        //   212: astore_1       
        //   213: goto            136
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                           
        //  -----  -----  -----  -----  -------------------------------
        //  95     107    208    216    Ljava/io/FileNotFoundException;
        //  122    134    212    172    Ljava/io/FileNotFoundException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0136:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
