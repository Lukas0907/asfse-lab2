// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.util;

import kotlin.collections.IntIterator;
import kotlin.jvm.functions.Function0;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import android.util.SparseIntArray;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u00008\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\u001a\u0015\u0010\u0005\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0001H\u0086\n\u001a\u0015\u0010\b\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0001H\u0086\b\u001a\u0015\u0010\t\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\n\u001a\u00020\u0001H\u0086\b\u001aE\u0010\u000b\u001a\u00020\f*\u00020\u000226\u0010\r\u001a2\u0012\u0013\u0012\u00110\u0001¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\u0007\u0012\u0013\u0012\u00110\u0001¢\u0006\f\b\u000f\u0012\b\b\u0010\u0012\u0004\b\b(\n\u0012\u0004\u0012\u00020\f0\u000eH\u0086\b\u001a\u001d\u0010\u0011\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u00012\u0006\u0010\u0012\u001a\u00020\u0001H\u0086\b\u001a#\u0010\u0013\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u00012\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00010\u0014H\u0086\b\u001a\r\u0010\u0015\u001a\u00020\u0006*\u00020\u0002H\u0086\b\u001a\r\u0010\u0016\u001a\u00020\u0006*\u00020\u0002H\u0086\b\u001a\n\u0010\u0017\u001a\u00020\u0018*\u00020\u0002\u001a\u0015\u0010\u0019\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u001a\u001a\u00020\u0002H\u0086\u0002\u001a\u0012\u0010\u001b\u001a\u00020\f*\u00020\u00022\u0006\u0010\u001a\u001a\u00020\u0002\u001a\u001a\u0010\u001c\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u00012\u0006\u0010\n\u001a\u00020\u0001\u001a\u001d\u0010\u001d\u001a\u00020\f*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u00012\u0006\u0010\n\u001a\u00020\u0001H\u0086\n\u001a\n\u0010\u001e\u001a\u00020\u0018*\u00020\u0002\"\u0016\u0010\u0000\u001a\u00020\u0001*\u00020\u00028\u00c6\u0002¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004¨\u0006\u001f" }, d2 = { "size", "", "Landroid/util/SparseIntArray;", "getSize", "(Landroid/util/SparseIntArray;)I", "contains", "", "key", "containsKey", "containsValue", "value", "forEach", "", "action", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "getOrDefault", "defaultValue", "getOrElse", "Lkotlin/Function0;", "isEmpty", "isNotEmpty", "keyIterator", "Lkotlin/collections/IntIterator;", "plus", "other", "putAll", "remove", "set", "valueIterator", "core-ktx_release" }, k = 2, mv = { 1, 1, 15 })
public final class SparseIntArrayKt
{
    public static final boolean contains(final SparseIntArray sparseIntArray, final int n) {
        Intrinsics.checkParameterIsNotNull(sparseIntArray, "$this$contains");
        return sparseIntArray.indexOfKey(n) >= 0;
    }
    
    public static final boolean containsKey(final SparseIntArray sparseIntArray, final int n) {
        Intrinsics.checkParameterIsNotNull(sparseIntArray, "$this$containsKey");
        return sparseIntArray.indexOfKey(n) >= 0;
    }
    
    public static final boolean containsValue(final SparseIntArray sparseIntArray, final int n) {
        Intrinsics.checkParameterIsNotNull(sparseIntArray, "$this$containsValue");
        return sparseIntArray.indexOfValue(n) != -1;
    }
    
    public static final void forEach(final SparseIntArray sparseIntArray, final Function2<? super Integer, ? super Integer, Unit> function2) {
        Intrinsics.checkParameterIsNotNull(sparseIntArray, "$this$forEach");
        Intrinsics.checkParameterIsNotNull(function2, "action");
        for (int size = sparseIntArray.size(), i = 0; i < size; ++i) {
            function2.invoke(sparseIntArray.keyAt(i), sparseIntArray.valueAt(i));
        }
    }
    
    public static final int getOrDefault(final SparseIntArray sparseIntArray, final int n, final int n2) {
        Intrinsics.checkParameterIsNotNull(sparseIntArray, "$this$getOrDefault");
        return sparseIntArray.get(n, n2);
    }
    
    public static final int getOrElse(final SparseIntArray sparseIntArray, int indexOfKey, final Function0<Integer> function0) {
        Intrinsics.checkParameterIsNotNull(sparseIntArray, "$this$getOrElse");
        Intrinsics.checkParameterIsNotNull(function0, "defaultValue");
        indexOfKey = sparseIntArray.indexOfKey(indexOfKey);
        if (indexOfKey != -1) {
            return sparseIntArray.valueAt(indexOfKey);
        }
        return function0.invoke().intValue();
    }
    
    public static final int getSize(final SparseIntArray sparseIntArray) {
        Intrinsics.checkParameterIsNotNull(sparseIntArray, "$this$size");
        return sparseIntArray.size();
    }
    
    public static final boolean isEmpty(final SparseIntArray sparseIntArray) {
        Intrinsics.checkParameterIsNotNull(sparseIntArray, "$this$isEmpty");
        return sparseIntArray.size() == 0;
    }
    
    public static final boolean isNotEmpty(final SparseIntArray sparseIntArray) {
        Intrinsics.checkParameterIsNotNull(sparseIntArray, "$this$isNotEmpty");
        return sparseIntArray.size() != 0;
    }
    
    public static final IntIterator keyIterator(final SparseIntArray sparseIntArray) {
        Intrinsics.checkParameterIsNotNull(sparseIntArray, "$this$keyIterator");
        return (IntIterator)new SparseIntArrayKt$keyIterator.SparseIntArrayKt$keyIterator$1(sparseIntArray);
    }
    
    public static final SparseIntArray plus(final SparseIntArray sparseIntArray, final SparseIntArray sparseIntArray2) {
        Intrinsics.checkParameterIsNotNull(sparseIntArray, "$this$plus");
        Intrinsics.checkParameterIsNotNull(sparseIntArray2, "other");
        final SparseIntArray sparseIntArray3 = new SparseIntArray(sparseIntArray.size() + sparseIntArray2.size());
        putAll(sparseIntArray3, sparseIntArray);
        putAll(sparseIntArray3, sparseIntArray2);
        return sparseIntArray3;
    }
    
    public static final void putAll(final SparseIntArray sparseIntArray, final SparseIntArray sparseIntArray2) {
        Intrinsics.checkParameterIsNotNull(sparseIntArray, "$this$putAll");
        Intrinsics.checkParameterIsNotNull(sparseIntArray2, "other");
        for (int size = sparseIntArray2.size(), i = 0; i < size; ++i) {
            sparseIntArray.put(sparseIntArray2.keyAt(i), sparseIntArray2.valueAt(i));
        }
    }
    
    public static final boolean remove(final SparseIntArray sparseIntArray, int indexOfKey, final int n) {
        Intrinsics.checkParameterIsNotNull(sparseIntArray, "$this$remove");
        indexOfKey = sparseIntArray.indexOfKey(indexOfKey);
        if (indexOfKey != -1 && n == sparseIntArray.valueAt(indexOfKey)) {
            sparseIntArray.removeAt(indexOfKey);
            return true;
        }
        return false;
    }
    
    public static final void set(final SparseIntArray sparseIntArray, final int n, final int n2) {
        Intrinsics.checkParameterIsNotNull(sparseIntArray, "$this$set");
        sparseIntArray.put(n, n2);
    }
    
    public static final IntIterator valueIterator(final SparseIntArray sparseIntArray) {
        Intrinsics.checkParameterIsNotNull(sparseIntArray, "$this$valueIterator");
        return (IntIterator)new SparseIntArrayKt$valueIterator.SparseIntArrayKt$valueIterator$1(sparseIntArray);
    }
}
