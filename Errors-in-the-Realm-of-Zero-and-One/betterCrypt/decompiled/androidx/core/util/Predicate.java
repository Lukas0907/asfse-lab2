// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.util;

public interface Predicate<T>
{
    boolean test(final T p0);
}
