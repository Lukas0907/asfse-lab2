// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.net;

import kotlin.jvm.internal.Intrinsics;
import java.io.File;
import android.net.Uri;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\r\u0010\u0003\u001a\u00020\u0002*\u00020\u0001H\u0086\b\u001a\r\u0010\u0003\u001a\u00020\u0002*\u00020\u0004H\u0086\b¨\u0006\u0005" }, d2 = { "toFile", "Ljava/io/File;", "Landroid/net/Uri;", "toUri", "", "core-ktx_release" }, k = 2, mv = { 1, 1, 15 })
public final class UriKt
{
    public static final File toFile(final Uri uri) {
        Intrinsics.checkParameterIsNotNull(uri, "$this$toFile");
        if (!Intrinsics.areEqual(uri.getScheme(), "file")) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Uri lacks 'file' scheme: ");
            sb.append(uri);
            throw new IllegalArgumentException(sb.toString().toString());
        }
        final String path = uri.getPath();
        if (path != null) {
            return new File(path);
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Uri path is null: ");
        sb2.append(uri);
        throw new IllegalArgumentException(sb2.toString().toString());
    }
    
    public static final Uri toUri(final File file) {
        Intrinsics.checkParameterIsNotNull(file, "$this$toUri");
        final Uri fromFile = Uri.fromFile(file);
        Intrinsics.checkExpressionValueIsNotNull(fromFile, "Uri.fromFile(this)");
        return fromFile;
    }
    
    public static final Uri toUri(final String s) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toUri");
        final Uri parse = Uri.parse(s);
        Intrinsics.checkExpressionValueIsNotNull(parse, "Uri.parse(this)");
        return parse;
    }
}
