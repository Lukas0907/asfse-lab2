// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.os;

import kotlin.TypeCastException;
import android.os.Build$VERSION;
import kotlin.jvm.internal.Intrinsics;
import android.os.PersistableBundle;
import kotlin.Pair;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0002\b\u0002\u001a=\u0010\u0000\u001a\u00020\u00012.\u0010\u0002\u001a\u0018\u0012\u0014\b\u0001\u0012\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00040\u0003\"\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u0004H\u0007¢\u0006\u0002\u0010\u0007¨\u0006\b" }, d2 = { "persistableBundleOf", "Landroid/os/PersistableBundle;", "pairs", "", "Lkotlin/Pair;", "", "", "([Lkotlin/Pair;)Landroid/os/PersistableBundle;", "core-ktx_release" }, k = 2, mv = { 1, 1, 15 })
public final class PersistableBundleKt
{
    public static final PersistableBundle persistableBundleOf(final Pair<String, ?>... array) {
        Intrinsics.checkParameterIsNotNull(array, "pairs");
        final PersistableBundle persistableBundle = new PersistableBundle(array.length);
        for (int length = array.length, i = 0; i < length; ++i) {
            final Pair<String, ?> pair = array[i];
            final String s = pair.component1();
            final Object component2 = pair.component2();
            if (component2 == null) {
                persistableBundle.putString(s, (String)null);
            }
            else if (component2 instanceof Boolean) {
                if (Build$VERSION.SDK_INT < 22) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Illegal value type boolean for key \"");
                    sb.append(s);
                    sb.append('\"');
                    throw new IllegalArgumentException(sb.toString());
                }
                persistableBundle.putBoolean(s, (boolean)component2);
            }
            else if (component2 instanceof Double) {
                persistableBundle.putDouble(s, ((Number)component2).doubleValue());
            }
            else if (component2 instanceof Integer) {
                persistableBundle.putInt(s, ((Number)component2).intValue());
            }
            else if (component2 instanceof Long) {
                persistableBundle.putLong(s, ((Number)component2).longValue());
            }
            else if (component2 instanceof String) {
                persistableBundle.putString(s, (String)component2);
            }
            else if (component2 instanceof boolean[]) {
                if (Build$VERSION.SDK_INT < 22) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Illegal value type boolean[] for key \"");
                    sb2.append(s);
                    sb2.append('\"');
                    throw new IllegalArgumentException(sb2.toString());
                }
                persistableBundle.putBooleanArray(s, (boolean[])component2);
            }
            else if (component2 instanceof double[]) {
                persistableBundle.putDoubleArray(s, (double[])component2);
            }
            else if (component2 instanceof int[]) {
                persistableBundle.putIntArray(s, (int[])component2);
            }
            else if (component2 instanceof long[]) {
                persistableBundle.putLongArray(s, (long[])component2);
            }
            else {
                if (!(component2 instanceof Object[])) {
                    final String canonicalName = ((String[])component2).getClass().getCanonicalName();
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Illegal value type ");
                    sb3.append(canonicalName);
                    sb3.append(" for key \"");
                    sb3.append(s);
                    sb3.append('\"');
                    throw new IllegalArgumentException(sb3.toString());
                }
                final Class<?> componentType = ((long[])component2).getClass().getComponentType();
                if (componentType == null) {
                    Intrinsics.throwNpe();
                }
                Intrinsics.checkExpressionValueIsNotNull(componentType, "value::class.java.componentType!!");
                if (!String.class.isAssignableFrom(componentType)) {
                    final String canonicalName2 = componentType.getCanonicalName();
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("Illegal value array type ");
                    sb4.append(canonicalName2);
                    sb4.append(" for key \"");
                    sb4.append(s);
                    sb4.append('\"');
                    throw new IllegalArgumentException(sb4.toString());
                }
                if (component2 == null) {
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<kotlin.String>");
                }
                persistableBundle.putStringArray(s, (String[])component2);
            }
        }
        return persistableBundle;
    }
}
