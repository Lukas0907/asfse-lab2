// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.os;

import android.os.Build$VERSION;
import android.os.Message;

public final class MessageCompat
{
    private static boolean sTryIsAsynchronous = true;
    private static boolean sTrySetAsynchronous = true;
    
    private MessageCompat() {
    }
    
    public static boolean isAsynchronous(final Message message) {
        if (Build$VERSION.SDK_INT >= 22) {
            return message.isAsynchronous();
        }
        if (!MessageCompat.sTryIsAsynchronous || Build$VERSION.SDK_INT < 16) {
            return false;
        }
        while (true) {
            while (true) {
                try {
                    return message.isAsynchronous();
                    MessageCompat.sTryIsAsynchronous = false;
                    return false;
                }
                catch (NoSuchMethodError noSuchMethodError) {}
                continue;
            }
        }
    }
    
    public static void setAsynchronous(final Message message, final boolean b) {
        if (Build$VERSION.SDK_INT >= 22) {
            message.setAsynchronous(b);
            return;
        }
        if (!MessageCompat.sTrySetAsynchronous || Build$VERSION.SDK_INT < 16) {
            return;
        }
        while (true) {
            while (true) {
                try {
                    message.setAsynchronous(b);
                    return;
                    MessageCompat.sTrySetAsynchronous = false;
                    return;
                }
                catch (NoSuchMethodError noSuchMethodError) {}
                continue;
            }
        }
    }
}
