// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.os;

import java.util.Arrays;
import android.os.Build$VERSION;
import java.util.Iterator;
import java.util.Collection;
import java.util.HashSet;
import java.util.Locale;

final class LocaleListCompatWrapper implements LocaleListInterface
{
    private static final Locale EN_LATN;
    private static final Locale LOCALE_AR_XB;
    private static final Locale LOCALE_EN_XA;
    private static final Locale[] sEmptyList;
    private final Locale[] mList;
    private final String mStringRepresentation;
    
    static {
        sEmptyList = new Locale[0];
        LOCALE_EN_XA = new Locale("en", "XA");
        LOCALE_AR_XB = new Locale("ar", "XB");
        EN_LATN = LocaleListCompat.forLanguageTagCompat("en-Latn");
    }
    
    LocaleListCompatWrapper(final Locale... array) {
        if (array.length == 0) {
            this.mList = LocaleListCompatWrapper.sEmptyList;
            this.mStringRepresentation = "";
            return;
        }
        final Locale[] mList = new Locale[array.length];
        final HashSet<Locale> set = new HashSet<Locale>();
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < array.length; ++i) {
            final Locale o = array[i];
            if (o == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("list[");
                sb2.append(i);
                sb2.append("] is null");
                throw new NullPointerException(sb2.toString());
            }
            if (set.contains(o)) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("list[");
                sb3.append(i);
                sb3.append("] is a repetition");
                throw new IllegalArgumentException(sb3.toString());
            }
            final Locale e = (Locale)o.clone();
            toLanguageTag(sb, mList[i] = e);
            if (i < array.length - 1) {
                sb.append(',');
            }
            set.add(e);
        }
        this.mList = mList;
        this.mStringRepresentation = sb.toString();
    }
    
    private Locale computeFirstMatch(final Collection<String> collection, final boolean b) {
        final int computeFirstMatchIndex = this.computeFirstMatchIndex(collection, b);
        if (computeFirstMatchIndex == -1) {
            return null;
        }
        return this.mList[computeFirstMatchIndex];
    }
    
    private int computeFirstMatchIndex(final Collection<String> collection, final boolean b) {
        final Locale[] mList = this.mList;
        if (mList.length == 1) {
            return 0;
        }
        if (mList.length == 0) {
            return -1;
        }
        int firstMatchIndex = 0;
        Label_0053: {
            if (b) {
                firstMatchIndex = this.findFirstMatchIndex(LocaleListCompatWrapper.EN_LATN);
                if (firstMatchIndex == 0) {
                    return 0;
                }
                if (firstMatchIndex < Integer.MAX_VALUE) {
                    break Label_0053;
                }
            }
            firstMatchIndex = Integer.MAX_VALUE;
        }
        final Iterator<String> iterator = collection.iterator();
        while (iterator.hasNext()) {
            final int firstMatchIndex2 = this.findFirstMatchIndex(LocaleListCompat.forLanguageTagCompat(iterator.next()));
            if (firstMatchIndex2 == 0) {
                return 0;
            }
            if (firstMatchIndex2 >= firstMatchIndex) {
                continue;
            }
            firstMatchIndex = firstMatchIndex2;
        }
        if (firstMatchIndex == Integer.MAX_VALUE) {
            return 0;
        }
        return firstMatchIndex;
    }
    
    private int findFirstMatchIndex(final Locale locale) {
        int n = 0;
        while (true) {
            final Locale[] mList = this.mList;
            if (n >= mList.length) {
                return Integer.MAX_VALUE;
            }
            if (matchScore(locale, mList[n]) > 0) {
                return n;
            }
            ++n;
        }
    }
    
    private static String getLikelyScript(final Locale locale) {
        if (Build$VERSION.SDK_INT >= 21) {
            final String script = locale.getScript();
            if (!script.isEmpty()) {
                return script;
            }
        }
        return "";
    }
    
    private static boolean isPseudoLocale(final Locale locale) {
        return LocaleListCompatWrapper.LOCALE_EN_XA.equals(locale) || LocaleListCompatWrapper.LOCALE_AR_XB.equals(locale);
    }
    
    private static int matchScore(final Locale locale, final Locale locale2) {
        throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:780)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.enexpr(TypeTransformer.java:659)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:719)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s1stmt(TypeTransformer.java:810)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:840)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
    }
    
    static void toLanguageTag(final StringBuilder sb, final Locale locale) {
        sb.append(locale.getLanguage());
        final String country = locale.getCountry();
        if (country != null && !country.isEmpty()) {
            sb.append('-');
            sb.append(locale.getCountry());
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof LocaleListCompatWrapper)) {
            return false;
        }
        final Locale[] mList = ((LocaleListCompatWrapper)o).mList;
        if (this.mList.length != mList.length) {
            return false;
        }
        int n = 0;
        while (true) {
            final Locale[] mList2 = this.mList;
            if (n >= mList2.length) {
                return true;
            }
            if (!mList2[n].equals(mList[n])) {
                return false;
            }
            ++n;
        }
    }
    
    @Override
    public Locale get(final int n) {
        if (n >= 0) {
            final Locale[] mList = this.mList;
            if (n < mList.length) {
                return mList[n];
            }
        }
        return null;
    }
    
    @Override
    public Locale getFirstMatch(final String[] a) {
        return this.computeFirstMatch(Arrays.asList(a), false);
    }
    
    @Override
    public Object getLocaleList() {
        return null;
    }
    
    @Override
    public int hashCode() {
        int n = 1;
        int n2 = 0;
        while (true) {
            final Locale[] mList = this.mList;
            if (n2 >= mList.length) {
                break;
            }
            n = n * 31 + mList[n2].hashCode();
            ++n2;
        }
        return n;
    }
    
    @Override
    public int indexOf(final Locale obj) {
        int n = 0;
        while (true) {
            final Locale[] mList = this.mList;
            if (n >= mList.length) {
                return -1;
            }
            if (mList[n].equals(obj)) {
                return n;
            }
            ++n;
        }
    }
    
    @Override
    public boolean isEmpty() {
        return this.mList.length == 0;
    }
    
    @Override
    public int size() {
        return this.mList.length;
    }
    
    @Override
    public String toLanguageTags() {
        return this.mStringRepresentation;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[");
        int n = 0;
        while (true) {
            final Locale[] mList = this.mList;
            if (n >= mList.length) {
                break;
            }
            sb.append(mList[n]);
            if (n < this.mList.length - 1) {
                sb.append(',');
            }
            ++n;
        }
        sb.append("]");
        return sb.toString();
    }
}
