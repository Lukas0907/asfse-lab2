// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.os;

import android.util.Log;
import android.os.Trace;
import android.os.Build$VERSION;
import java.lang.reflect.Method;

public final class TraceCompat
{
    private static final String TAG = "TraceCompat";
    private static Method sAsyncTraceBeginMethod;
    private static Method sAsyncTraceEndMethod;
    private static Method sIsTagEnabledMethod;
    private static Method sTraceCounterMethod;
    private static long sTraceTagApp;
    
    static {
        if (Build$VERSION.SDK_INT >= 18 && Build$VERSION.SDK_INT < 29) {
            try {
                TraceCompat.sTraceTagApp = Trace.class.getField("TRACE_TAG_APP").getLong(null);
                TraceCompat.sIsTagEnabledMethod = Trace.class.getMethod("isTagEnabled", Long.TYPE);
                TraceCompat.sAsyncTraceBeginMethod = Trace.class.getMethod("asyncTraceBegin", Long.TYPE, String.class, Integer.TYPE);
                TraceCompat.sAsyncTraceEndMethod = Trace.class.getMethod("asyncTraceEnd", Long.TYPE, String.class, Integer.TYPE);
                TraceCompat.sTraceCounterMethod = Trace.class.getMethod("traceCounter", Long.TYPE, String.class, Integer.TYPE);
            }
            catch (Exception ex) {
                Log.i("TraceCompat", "Unable to initialize via reflection.", (Throwable)ex);
            }
        }
    }
    
    private TraceCompat() {
    }
    
    public static void beginAsyncSection(final String s, final int i) {
        if (Build$VERSION.SDK_INT >= 29) {
            Trace.beginAsyncSection(s, i);
            return;
        }
        if (Build$VERSION.SDK_INT < 18) {
            return;
        }
        while (true) {
            while (true) {
                try {
                    TraceCompat.sAsyncTraceBeginMethod.invoke(null, TraceCompat.sTraceTagApp, s, i);
                    return;
                    Log.v("TraceCompat", "Unable to invoke asyncTraceBegin() via reflection.");
                    return;
                }
                catch (Exception ex) {}
                continue;
            }
        }
    }
    
    public static void beginSection(final String s) {
        if (Build$VERSION.SDK_INT >= 18) {
            Trace.beginSection(s);
        }
    }
    
    public static void endAsyncSection(final String s, final int i) {
        if (Build$VERSION.SDK_INT >= 29) {
            Trace.endAsyncSection(s, i);
            return;
        }
        if (Build$VERSION.SDK_INT < 18) {
            return;
        }
        while (true) {
            while (true) {
                try {
                    TraceCompat.sAsyncTraceEndMethod.invoke(null, TraceCompat.sTraceTagApp, s, i);
                    return;
                    Log.v("TraceCompat", "Unable to invoke endAsyncSection() via reflection.");
                    return;
                }
                catch (Exception ex) {}
                continue;
            }
        }
    }
    
    public static void endSection() {
        if (Build$VERSION.SDK_INT >= 18) {
            Trace.endSection();
        }
    }
    
    public static boolean isEnabled() {
        if (Build$VERSION.SDK_INT >= 29) {
            return Trace.isEnabled();
        }
        if (Build$VERSION.SDK_INT < 18) {
            return false;
        }
        while (true) {
            while (true) {
                try {
                    return (boolean)TraceCompat.sIsTagEnabledMethod.invoke(null, TraceCompat.sTraceTagApp);
                    Log.v("TraceCompat", "Unable to invoke isTagEnabled() via reflection.");
                    return false;
                }
                catch (Exception ex) {}
                continue;
            }
        }
    }
    
    public static void setCounter(final String s, final int i) {
        if (Build$VERSION.SDK_INT >= 29) {
            Trace.setCounter(s, (long)i);
            return;
        }
        if (Build$VERSION.SDK_INT < 18) {
            return;
        }
        while (true) {
            while (true) {
                try {
                    TraceCompat.sTraceCounterMethod.invoke(null, TraceCompat.sTraceTagApp, s, i);
                    return;
                    Log.v("TraceCompat", "Unable to invoke traceCounter() via reflection.");
                    return;
                }
                catch (Exception ex) {}
                continue;
            }
        }
    }
}
