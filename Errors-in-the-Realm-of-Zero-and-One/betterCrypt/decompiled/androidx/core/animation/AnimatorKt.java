// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.animation;

import android.animation.Animator$AnimatorPauseListener;
import kotlin.jvm.internal.Intrinsics;
import android.animation.Animator$AnimatorListener;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import android.animation.Animator;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000(\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\u001a¡\u0001\u0010\u0000\u001a\u00020\u0001*\u00020\u00022#\b\u0006\u0010\u0003\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u00042#\b\u0006\u0010\t\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u00042#\b\u0006\u0010\n\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u00042#\b\u0006\u0010\u000b\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0004H\u0086\b\u001aW\u0010\f\u001a\u00020\r*\u00020\u00022#\b\u0006\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u00042#\b\u0006\u0010\u000f\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0004H\u0087\b\u001a2\u0010\u0010\u001a\u00020\u0001*\u00020\u00022#\b\u0004\u0010\u0011\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0004H\u0086\b\u001a2\u0010\u0012\u001a\u00020\u0001*\u00020\u00022#\b\u0004\u0010\u0011\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0004H\u0086\b\u001a2\u0010\u0013\u001a\u00020\r*\u00020\u00022#\b\u0004\u0010\u0011\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0004H\u0087\b\u001a2\u0010\u0014\u001a\u00020\u0001*\u00020\u00022#\b\u0004\u0010\u0011\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0004H\u0086\b\u001a2\u0010\u0015\u001a\u00020\r*\u00020\u00022#\b\u0004\u0010\u0011\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0004H\u0087\b\u001a2\u0010\u0016\u001a\u00020\u0001*\u00020\u00022#\b\u0004\u0010\u0011\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0004H\u0086\b¨\u0006\u0017" }, d2 = { "addListener", "Landroid/animation/Animator$AnimatorListener;", "Landroid/animation/Animator;", "onEnd", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "animator", "", "onStart", "onCancel", "onRepeat", "addPauseListener", "Landroid/animation/Animator$AnimatorPauseListener;", "onResume", "onPause", "doOnCancel", "action", "doOnEnd", "doOnPause", "doOnRepeat", "doOnResume", "doOnStart", "core-ktx_release" }, k = 2, mv = { 1, 1, 15 })
public final class AnimatorKt
{
    public static final Animator$AnimatorListener addListener(final Animator animator, final Function1<? super Animator, Unit> function1, final Function1<? super Animator, Unit> function2, final Function1<? super Animator, Unit> function3, final Function1<? super Animator, Unit> function4) {
        Intrinsics.checkParameterIsNotNull(animator, "$this$addListener");
        Intrinsics.checkParameterIsNotNull(function1, "onEnd");
        Intrinsics.checkParameterIsNotNull(function2, "onStart");
        Intrinsics.checkParameterIsNotNull(function3, "onCancel");
        Intrinsics.checkParameterIsNotNull(function4, "onRepeat");
        final Animator$AnimatorListener animator$AnimatorListener = (Animator$AnimatorListener)new AnimatorKt$addListener$listener.AnimatorKt$addListener$listener$1((Function1)function4, (Function1)function1, (Function1)function3, (Function1)function2);
        animator.addListener(animator$AnimatorListener);
        return animator$AnimatorListener;
    }
    
    public static final Animator$AnimatorPauseListener addPauseListener(final Animator animator, final Function1<? super Animator, Unit> function1, final Function1<? super Animator, Unit> function2) {
        Intrinsics.checkParameterIsNotNull(animator, "$this$addPauseListener");
        Intrinsics.checkParameterIsNotNull(function1, "onResume");
        Intrinsics.checkParameterIsNotNull(function2, "onPause");
        final Animator$AnimatorPauseListener animator$AnimatorPauseListener = (Animator$AnimatorPauseListener)new AnimatorKt$addPauseListener$listener.AnimatorKt$addPauseListener$listener$1((Function1)function2, (Function1)function1);
        animator.addPauseListener(animator$AnimatorPauseListener);
        return animator$AnimatorPauseListener;
    }
    
    public static final Animator$AnimatorListener doOnCancel(final Animator animator, final Function1<? super Animator, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(animator, "$this$doOnCancel");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        final Animator$AnimatorListener animator$AnimatorListener = (Animator$AnimatorListener)new AnimatorKt$doOnCancel$$inlined$addListener.AnimatorKt$doOnCancel$$inlined$addListener$1((Function1)function1);
        animator.addListener(animator$AnimatorListener);
        return animator$AnimatorListener;
    }
    
    public static final Animator$AnimatorListener doOnEnd(final Animator animator, final Function1<? super Animator, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(animator, "$this$doOnEnd");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        final Animator$AnimatorListener animator$AnimatorListener = (Animator$AnimatorListener)new AnimatorKt$doOnEnd$$inlined$addListener.AnimatorKt$doOnEnd$$inlined$addListener$1((Function1)function1);
        animator.addListener(animator$AnimatorListener);
        return animator$AnimatorListener;
    }
    
    public static final Animator$AnimatorPauseListener doOnPause(final Animator animator, final Function1<? super Animator, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(animator, "$this$doOnPause");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        final Animator$AnimatorPauseListener animator$AnimatorPauseListener = (Animator$AnimatorPauseListener)new AnimatorKt$doOnPause$$inlined$addPauseListener.AnimatorKt$doOnPause$$inlined$addPauseListener$1((Function1)function1);
        animator.addPauseListener(animator$AnimatorPauseListener);
        return animator$AnimatorPauseListener;
    }
    
    public static final Animator$AnimatorListener doOnRepeat(final Animator animator, final Function1<? super Animator, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(animator, "$this$doOnRepeat");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        final Animator$AnimatorListener animator$AnimatorListener = (Animator$AnimatorListener)new AnimatorKt$doOnRepeat$$inlined$addListener.AnimatorKt$doOnRepeat$$inlined$addListener$1((Function1)function1);
        animator.addListener(animator$AnimatorListener);
        return animator$AnimatorListener;
    }
    
    public static final Animator$AnimatorPauseListener doOnResume(final Animator animator, final Function1<? super Animator, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(animator, "$this$doOnResume");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        final Animator$AnimatorPauseListener animator$AnimatorPauseListener = (Animator$AnimatorPauseListener)new AnimatorKt$doOnResume$$inlined$addPauseListener.AnimatorKt$doOnResume$$inlined$addPauseListener$1((Function1)function1);
        animator.addPauseListener(animator$AnimatorPauseListener);
        return animator$AnimatorPauseListener;
    }
    
    public static final Animator$AnimatorListener doOnStart(final Animator animator, final Function1<? super Animator, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(animator, "$this$doOnStart");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        final Animator$AnimatorListener animator$AnimatorListener = (Animator$AnimatorListener)new AnimatorKt$doOnStart$$inlined$addListener.AnimatorKt$doOnStart$$inlined$addListener$1((Function1)function1);
        animator.addListener(animator$AnimatorListener);
        return animator$AnimatorListener;
    }
}
