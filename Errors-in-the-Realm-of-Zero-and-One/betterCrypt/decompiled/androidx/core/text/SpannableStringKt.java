// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.text;

import android.text.SpannableString;
import kotlin.ranges.IntRange;
import android.text.Spanned;
import kotlin.jvm.internal.Intrinsics;
import android.text.Spannable;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000(\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\u001a\r\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0087\b\u001a%\u0010\u0003\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\bH\u0086\n\u001a\u001d\u0010\u0003\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0007\u001a\u00020\bH\u0086\n\u001a\r\u0010\u000b\u001a\u00020\u0002*\u00020\fH\u0086\b¨\u0006\r" }, d2 = { "clearSpans", "", "Landroid/text/Spannable;", "set", "start", "", "end", "span", "", "range", "Lkotlin/ranges/IntRange;", "toSpannable", "", "core-ktx_release" }, k = 2, mv = { 1, 1, 15 })
public final class SpannableStringKt
{
    public static final void clearSpans(final Spannable spannable) {
        Intrinsics.checkParameterIsNotNull(spannable, "$this$clearSpans");
        final Spanned spanned = (Spanned)spannable;
        final int length = spanned.length();
        int i = 0;
        final Object[] spans = spanned.getSpans(0, length, (Class)Object.class);
        Intrinsics.checkExpressionValueIsNotNull(spans, "getSpans(start, end, T::class.java)");
        while (i < spans.length) {
            spannable.removeSpan(spans[i]);
            ++i;
        }
    }
    
    public static final void set(final Spannable spannable, final int n, final int n2, final Object o) {
        Intrinsics.checkParameterIsNotNull(spannable, "$this$set");
        Intrinsics.checkParameterIsNotNull(o, "span");
        spannable.setSpan(o, n, n2, 17);
    }
    
    public static final void set(final Spannable spannable, final IntRange intRange, final Object o) {
        Intrinsics.checkParameterIsNotNull(spannable, "$this$set");
        Intrinsics.checkParameterIsNotNull(intRange, "range");
        Intrinsics.checkParameterIsNotNull(o, "span");
        spannable.setSpan(o, (int)intRange.getStart(), (int)intRange.getEndInclusive(), 17);
    }
    
    public static final Spannable toSpannable(final CharSequence charSequence) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$toSpannable");
        final SpannableString value = SpannableString.valueOf(charSequence);
        Intrinsics.checkExpressionValueIsNotNull(value, "SpannableString.valueOf(this)");
        return (Spannable)value;
    }
}
