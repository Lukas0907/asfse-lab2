// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.text;

import kotlin.jvm.internal.Intrinsics;
import android.text.Spanned;
import android.text.Html$TagHandler;
import android.text.Html$ImageGetter;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a/\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\b\b\u0002\u0010\u0003\u001a\u00020\u00042\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0086\b\u001a\u0017\u0010\t\u001a\u00020\u0002*\u00020\u00012\b\b\u0002\u0010\n\u001a\u00020\u0004H\u0086\b¨\u0006\u000b" }, d2 = { "parseAsHtml", "Landroid/text/Spanned;", "", "flags", "", "imageGetter", "Landroid/text/Html$ImageGetter;", "tagHandler", "Landroid/text/Html$TagHandler;", "toHtml", "option", "core-ktx_release" }, k = 2, mv = { 1, 1, 15 })
public final class HtmlKt
{
    public static final Spanned parseAsHtml(final String s, final int n, final Html$ImageGetter html$ImageGetter, final Html$TagHandler html$TagHandler) {
        Intrinsics.checkParameterIsNotNull(s, "$this$parseAsHtml");
        final Spanned fromHtml = HtmlCompat.fromHtml(s, n, html$ImageGetter, html$TagHandler);
        Intrinsics.checkExpressionValueIsNotNull(fromHtml, "HtmlCompat.fromHtml(this\u2026 imageGetter, tagHandler)");
        return fromHtml;
    }
    
    public static final String toHtml(final Spanned spanned, final int n) {
        Intrinsics.checkParameterIsNotNull(spanned, "$this$toHtml");
        final String html = HtmlCompat.toHtml(spanned, n);
        Intrinsics.checkExpressionValueIsNotNull(html, "HtmlCompat.toHtml(this, option)");
        return html;
    }
}
