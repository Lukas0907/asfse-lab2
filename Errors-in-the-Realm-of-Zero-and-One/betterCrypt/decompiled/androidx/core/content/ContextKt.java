// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.content;

import android.util.AttributeSet;
import kotlin.Unit;
import android.content.res.TypedArray;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import android.content.Context;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000:\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0015\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a \u0010\u0000\u001a\u0004\u0018\u0001H\u0001\"\n\b\u0000\u0010\u0001\u0018\u0001*\u00020\u0002*\u00020\u0003H\u0086\b¢\u0006\u0002\u0010\u0004\u001aN\u0010\u0005\u001a\u00020\u0006*\u00020\u00032\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0006\u0010\t\u001a\u00020\n2\b\b\u0003\u0010\u000b\u001a\u00020\f2\b\b\u0003\u0010\r\u001a\u00020\f2\u0017\u0010\u000e\u001a\u0013\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00060\u000f¢\u0006\u0002\b\u0011H\u0086\b\u001a8\u0010\u0005\u001a\u00020\u0006*\u00020\u00032\b\b\u0001\u0010\u0012\u001a\u00020\f2\u0006\u0010\t\u001a\u00020\n2\u0017\u0010\u000e\u001a\u0013\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00060\u000f¢\u0006\u0002\b\u0011H\u0086\b¨\u0006\u0013" }, d2 = { "getSystemService", "T", "", "Landroid/content/Context;", "(Landroid/content/Context;)Ljava/lang/Object;", "withStyledAttributes", "", "set", "Landroid/util/AttributeSet;", "attrs", "", "defStyleAttr", "", "defStyleRes", "block", "Lkotlin/Function1;", "Landroid/content/res/TypedArray;", "Lkotlin/ExtensionFunctionType;", "resourceId", "core-ktx_release" }, k = 2, mv = { 1, 1, 15 })
public final class ContextKt
{
    public static final void withStyledAttributes(final Context context, final int n, final int[] array, final Function1<? super TypedArray, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(context, "$this$withStyledAttributes");
        Intrinsics.checkParameterIsNotNull(array, "attrs");
        Intrinsics.checkParameterIsNotNull(function1, "block");
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(n, array);
        function1.invoke(obtainStyledAttributes);
        obtainStyledAttributes.recycle();
    }
    
    public static final void withStyledAttributes(final Context context, final AttributeSet set, final int[] array, final int n, final int n2, final Function1<? super TypedArray, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(context, "$this$withStyledAttributes");
        Intrinsics.checkParameterIsNotNull(array, "attrs");
        Intrinsics.checkParameterIsNotNull(function1, "block");
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, array, n, n2);
        function1.invoke(obtainStyledAttributes);
        obtainStyledAttributes.recycle();
    }
}
