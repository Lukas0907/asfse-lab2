// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.graphics;

import java.util.Iterator;
import android.graphics.RegionIterator;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import android.graphics.Point;
import android.graphics.Region$Op;
import kotlin.jvm.internal.Intrinsics;
import android.graphics.Rect;
import android.graphics.Region;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u00004\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010(\n\u0002\b\u0007\u001a\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0086\f\u001a\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0086\f\u001a\u0015\u0010\u0004\u001a\u00020\u0005*\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u0007H\u0086\n\u001a0\u0010\b\u001a\u00020\t*\u00020\u00012!\u0010\n\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\f\u0012\b\b\r\u0012\u0004\b\b(\u000e\u0012\u0004\u0012\u00020\t0\u000bH\u0086\b\u001a\u0013\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00030\u0010*\u00020\u0001H\u0086\u0002\u001a\u0015\u0010\u0011\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0086\n\u001a\u0015\u0010\u0011\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0086\n\u001a\r\u0010\u0012\u001a\u00020\u0001*\u00020\u0001H\u0086\n\u001a\u0015\u0010\u0013\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0086\f\u001a\u0015\u0010\u0013\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0086\f\u001a\u0015\u0010\u0014\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0086\n\u001a\u0015\u0010\u0014\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0086\n\u001a\r\u0010\u0015\u001a\u00020\u0001*\u00020\u0001H\u0086\n\u001a\u0015\u0010\u0016\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0086\f\u001a\u0015\u0010\u0016\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0086\f¨\u0006\u0017" }, d2 = { "and", "Landroid/graphics/Region;", "r", "Landroid/graphics/Rect;", "contains", "", "p", "Landroid/graphics/Point;", "forEach", "", "action", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "rect", "iterator", "", "minus", "not", "or", "plus", "unaryMinus", "xor", "core-ktx_release" }, k = 2, mv = { 1, 1, 15 })
public final class RegionKt
{
    public static final Region and(Region region, final Rect rect) {
        Intrinsics.checkParameterIsNotNull(region, "$this$and");
        Intrinsics.checkParameterIsNotNull(rect, "r");
        region = new Region(region);
        region.op(rect, Region$Op.INTERSECT);
        return region;
    }
    
    public static final Region and(Region region, final Region region2) {
        Intrinsics.checkParameterIsNotNull(region, "$this$and");
        Intrinsics.checkParameterIsNotNull(region2, "r");
        region = new Region(region);
        region.op(region2, Region$Op.INTERSECT);
        return region;
    }
    
    public static final boolean contains(final Region region, final Point point) {
        Intrinsics.checkParameterIsNotNull(region, "$this$contains");
        Intrinsics.checkParameterIsNotNull(point, "p");
        return region.contains(point.x, point.y);
    }
    
    public static final void forEach(final Region region, final Function1<? super Rect, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(region, "$this$forEach");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        final RegionIterator regionIterator = new RegionIterator(region);
        while (true) {
            final Rect rect = new Rect();
            if (!regionIterator.next(rect)) {
                break;
            }
            function1.invoke(rect);
        }
    }
    
    public static final Iterator<Rect> iterator(final Region region) {
        Intrinsics.checkParameterIsNotNull(region, "$this$iterator");
        return (Iterator<Rect>)new RegionKt$iterator.RegionKt$iterator$1(region);
    }
    
    public static final Region minus(Region region, final Rect rect) {
        Intrinsics.checkParameterIsNotNull(region, "$this$minus");
        Intrinsics.checkParameterIsNotNull(rect, "r");
        region = new Region(region);
        region.op(rect, Region$Op.DIFFERENCE);
        return region;
    }
    
    public static final Region minus(Region region, final Region region2) {
        Intrinsics.checkParameterIsNotNull(region, "$this$minus");
        Intrinsics.checkParameterIsNotNull(region2, "r");
        region = new Region(region);
        region.op(region2, Region$Op.DIFFERENCE);
        return region;
    }
    
    public static final Region not(final Region region) {
        Intrinsics.checkParameterIsNotNull(region, "$this$not");
        final Region region2 = new Region(region.getBounds());
        region2.op(region, Region$Op.DIFFERENCE);
        return region2;
    }
    
    public static final Region or(Region region, final Rect rect) {
        Intrinsics.checkParameterIsNotNull(region, "$this$or");
        Intrinsics.checkParameterIsNotNull(rect, "r");
        region = new Region(region);
        region.union(rect);
        return region;
    }
    
    public static final Region or(Region region, final Region region2) {
        Intrinsics.checkParameterIsNotNull(region, "$this$or");
        Intrinsics.checkParameterIsNotNull(region2, "r");
        region = new Region(region);
        region.op(region2, Region$Op.UNION);
        return region;
    }
    
    public static final Region plus(Region region, final Rect rect) {
        Intrinsics.checkParameterIsNotNull(region, "$this$plus");
        Intrinsics.checkParameterIsNotNull(rect, "r");
        region = new Region(region);
        region.union(rect);
        return region;
    }
    
    public static final Region plus(Region region, final Region region2) {
        Intrinsics.checkParameterIsNotNull(region, "$this$plus");
        Intrinsics.checkParameterIsNotNull(region2, "r");
        region = new Region(region);
        region.op(region2, Region$Op.UNION);
        return region;
    }
    
    public static final Region unaryMinus(final Region region) {
        Intrinsics.checkParameterIsNotNull(region, "$this$unaryMinus");
        final Region region2 = new Region(region.getBounds());
        region2.op(region, Region$Op.DIFFERENCE);
        return region2;
    }
    
    public static final Region xor(Region region, final Rect rect) {
        Intrinsics.checkParameterIsNotNull(region, "$this$xor");
        Intrinsics.checkParameterIsNotNull(rect, "r");
        region = new Region(region);
        region.op(rect, Region$Op.XOR);
        return region;
    }
    
    public static final Region xor(Region region, final Region region2) {
        Intrinsics.checkParameterIsNotNull(region, "$this$xor");
        Intrinsics.checkParameterIsNotNull(region2, "r");
        region = new Region(region);
        region.op(region2, Region$Op.XOR);
        return region;
    }
}
