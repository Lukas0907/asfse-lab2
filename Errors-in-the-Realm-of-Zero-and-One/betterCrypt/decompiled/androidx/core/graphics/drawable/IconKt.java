// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.graphics.drawable;

import android.net.Uri;
import kotlin.jvm.internal.Intrinsics;
import android.graphics.drawable.Icon;
import android.graphics.Bitmap;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0012\n\u0000\u001a\r\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0087\b\u001a\r\u0010\u0003\u001a\u00020\u0001*\u00020\u0002H\u0087\b\u001a\r\u0010\u0003\u001a\u00020\u0001*\u00020\u0004H\u0087\b\u001a\r\u0010\u0003\u001a\u00020\u0001*\u00020\u0005H\u0087\b¨\u0006\u0006" }, d2 = { "toAdaptiveIcon", "Landroid/graphics/drawable/Icon;", "Landroid/graphics/Bitmap;", "toIcon", "Landroid/net/Uri;", "", "core-ktx_release" }, k = 2, mv = { 1, 1, 15 })
public final class IconKt
{
    public static final Icon toAdaptiveIcon(final Bitmap bitmap) {
        Intrinsics.checkParameterIsNotNull(bitmap, "$this$toAdaptiveIcon");
        final Icon withAdaptiveBitmap = Icon.createWithAdaptiveBitmap(bitmap);
        Intrinsics.checkExpressionValueIsNotNull(withAdaptiveBitmap, "Icon.createWithAdaptiveBitmap(this)");
        return withAdaptiveBitmap;
    }
    
    public static final Icon toIcon(final Bitmap bitmap) {
        Intrinsics.checkParameterIsNotNull(bitmap, "$this$toIcon");
        final Icon withBitmap = Icon.createWithBitmap(bitmap);
        Intrinsics.checkExpressionValueIsNotNull(withBitmap, "Icon.createWithBitmap(this)");
        return withBitmap;
    }
    
    public static final Icon toIcon(final Uri uri) {
        Intrinsics.checkParameterIsNotNull(uri, "$this$toIcon");
        final Icon withContentUri = Icon.createWithContentUri(uri);
        Intrinsics.checkExpressionValueIsNotNull(withContentUri, "Icon.createWithContentUri(this)");
        return withContentUri;
    }
    
    public static final Icon toIcon(final byte[] array) {
        Intrinsics.checkParameterIsNotNull(array, "$this$toIcon");
        final Icon withData = Icon.createWithData(array, 0, array.length);
        Intrinsics.checkExpressionValueIsNotNull(withData, "Icon.createWithData(this, 0, size)");
        return withData;
    }
}
