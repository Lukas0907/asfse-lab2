// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.graphics.drawable;

import android.graphics.Rect;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import kotlin.jvm.internal.Intrinsics;
import android.graphics.Bitmap;
import android.graphics.Bitmap$Config;
import android.graphics.drawable.Drawable;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\u001a*\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\b\b\u0003\u0010\u0003\u001a\u00020\u00042\b\b\u0003\u0010\u0005\u001a\u00020\u00042\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u001a2\u0010\b\u001a\u00020\t*\u00020\u00022\b\b\u0003\u0010\n\u001a\u00020\u00042\b\b\u0003\u0010\u000b\u001a\u00020\u00042\b\b\u0003\u0010\f\u001a\u00020\u00042\b\b\u0003\u0010\r\u001a\u00020\u0004¨\u0006\u000e" }, d2 = { "toBitmap", "Landroid/graphics/Bitmap;", "Landroid/graphics/drawable/Drawable;", "width", "", "height", "config", "Landroid/graphics/Bitmap$Config;", "updateBounds", "", "left", "top", "right", "bottom", "core-ktx_release" }, k = 2, mv = { 1, 1, 15 })
public final class DrawableKt
{
    public static final Bitmap toBitmap(final Drawable drawable, final int n, final int n2, Bitmap$Config argb_8888) {
        Intrinsics.checkParameterIsNotNull(drawable, "$this$toBitmap");
        Label_0095: {
            if (drawable instanceof BitmapDrawable) {
                if (argb_8888 != null) {
                    final Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();
                    Intrinsics.checkExpressionValueIsNotNull(bitmap, "bitmap");
                    if (bitmap.getConfig() != argb_8888) {
                        break Label_0095;
                    }
                }
                final BitmapDrawable bitmapDrawable = (BitmapDrawable)drawable;
                if (n == bitmapDrawable.getIntrinsicWidth() && n2 == bitmapDrawable.getIntrinsicHeight()) {
                    final Bitmap bitmap2 = bitmapDrawable.getBitmap();
                    Intrinsics.checkExpressionValueIsNotNull(bitmap2, "bitmap");
                    return bitmap2;
                }
                final Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmapDrawable.getBitmap(), n, n2, true);
                Intrinsics.checkExpressionValueIsNotNull(scaledBitmap, "Bitmap.createScaledBitma\u2026map, width, height, true)");
                return scaledBitmap;
            }
        }
        final Rect bounds = drawable.getBounds();
        final int left = bounds.left;
        final int top = bounds.top;
        final int right = bounds.right;
        final int bottom = bounds.bottom;
        if (argb_8888 == null) {
            argb_8888 = Bitmap$Config.ARGB_8888;
        }
        final Bitmap bitmap3 = Bitmap.createBitmap(n, n2, argb_8888);
        drawable.setBounds(0, 0, n, n2);
        drawable.draw(new Canvas(bitmap3));
        drawable.setBounds(left, top, right, bottom);
        Intrinsics.checkExpressionValueIsNotNull(bitmap3, "bitmap");
        return bitmap3;
    }
    
    public static final void updateBounds(final Drawable drawable, final int n, final int n2, final int n3, final int n4) {
        Intrinsics.checkParameterIsNotNull(drawable, "$this$updateBounds");
        drawable.setBounds(n, n2, n3, n4);
    }
}
