// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.graphics;

import android.graphics.Point;
import kotlin.jvm.internal.Intrinsics;
import android.graphics.PointF;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0016\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\u0010\u0007\n\u0002\u0018\u0002\n\u0002\b\t\u001a\r\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0086\n\u001a\r\u0010\u0000\u001a\u00020\u0003*\u00020\u0004H\u0086\n\u001a\r\u0010\u0005\u001a\u00020\u0001*\u00020\u0002H\u0086\n\u001a\r\u0010\u0005\u001a\u00020\u0003*\u00020\u0004H\u0086\n\u001a\u0015\u0010\u0006\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0086\n\u001a\u0015\u0010\u0006\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\b\u001a\u00020\u0001H\u0086\n\u001a\u0015\u0010\u0006\u001a\u00020\u0004*\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0004H\u0086\n\u001a\u0015\u0010\u0006\u001a\u00020\u0004*\u00020\u00042\u0006\u0010\b\u001a\u00020\u0003H\u0086\n\u001a\u0015\u0010\t\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0002H\u0086\n\u001a\u0015\u0010\t\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\b\u001a\u00020\u0001H\u0086\n\u001a\u0015\u0010\t\u001a\u00020\u0004*\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u0004H\u0086\n\u001a\u0015\u0010\t\u001a\u00020\u0004*\u00020\u00042\u0006\u0010\b\u001a\u00020\u0003H\u0086\n\u001a\r\u0010\n\u001a\u00020\u0002*\u00020\u0004H\u0086\b\u001a\r\u0010\u000b\u001a\u00020\u0004*\u00020\u0002H\u0086\b\u001a\r\u0010\f\u001a\u00020\u0002*\u00020\u0002H\u0086\n\u001a\r\u0010\f\u001a\u00020\u0004*\u00020\u0004H\u0086\n¨\u0006\r" }, d2 = { "component1", "", "Landroid/graphics/Point;", "", "Landroid/graphics/PointF;", "component2", "minus", "p", "xy", "plus", "toPoint", "toPointF", "unaryMinus", "core-ktx_release" }, k = 2, mv = { 1, 1, 15 })
public final class PointKt
{
    public static final float component1(final PointF pointF) {
        Intrinsics.checkParameterIsNotNull(pointF, "$this$component1");
        return pointF.x;
    }
    
    public static final int component1(final Point point) {
        Intrinsics.checkParameterIsNotNull(point, "$this$component1");
        return point.x;
    }
    
    public static final float component2(final PointF pointF) {
        Intrinsics.checkParameterIsNotNull(pointF, "$this$component2");
        return pointF.y;
    }
    
    public static final int component2(final Point point) {
        Intrinsics.checkParameterIsNotNull(point, "$this$component2");
        return point.y;
    }
    
    public static final Point minus(Point point, int n) {
        Intrinsics.checkParameterIsNotNull(point, "$this$minus");
        point = new Point(point.x, point.y);
        n = -n;
        point.offset(n, n);
        return point;
    }
    
    public static final Point minus(Point point, final Point point2) {
        Intrinsics.checkParameterIsNotNull(point, "$this$minus");
        Intrinsics.checkParameterIsNotNull(point2, "p");
        point = new Point(point.x, point.y);
        point.offset(-point2.x, -point2.y);
        return point;
    }
    
    public static final PointF minus(PointF pointF, float n) {
        Intrinsics.checkParameterIsNotNull(pointF, "$this$minus");
        pointF = new PointF(pointF.x, pointF.y);
        n = -n;
        pointF.offset(n, n);
        return pointF;
    }
    
    public static final PointF minus(PointF pointF, final PointF pointF2) {
        Intrinsics.checkParameterIsNotNull(pointF, "$this$minus");
        Intrinsics.checkParameterIsNotNull(pointF2, "p");
        pointF = new PointF(pointF.x, pointF.y);
        pointF.offset(-pointF2.x, -pointF2.y);
        return pointF;
    }
    
    public static final Point plus(Point point, final int n) {
        Intrinsics.checkParameterIsNotNull(point, "$this$plus");
        point = new Point(point.x, point.y);
        point.offset(n, n);
        return point;
    }
    
    public static final Point plus(Point point, final Point point2) {
        Intrinsics.checkParameterIsNotNull(point, "$this$plus");
        Intrinsics.checkParameterIsNotNull(point2, "p");
        point = new Point(point.x, point.y);
        point.offset(point2.x, point2.y);
        return point;
    }
    
    public static final PointF plus(PointF pointF, final float n) {
        Intrinsics.checkParameterIsNotNull(pointF, "$this$plus");
        pointF = new PointF(pointF.x, pointF.y);
        pointF.offset(n, n);
        return pointF;
    }
    
    public static final PointF plus(PointF pointF, final PointF pointF2) {
        Intrinsics.checkParameterIsNotNull(pointF, "$this$plus");
        Intrinsics.checkParameterIsNotNull(pointF2, "p");
        pointF = new PointF(pointF.x, pointF.y);
        pointF.offset(pointF2.x, pointF2.y);
        return pointF;
    }
    
    public static final Point toPoint(final PointF pointF) {
        Intrinsics.checkParameterIsNotNull(pointF, "$this$toPoint");
        return new Point((int)pointF.x, (int)pointF.y);
    }
    
    public static final PointF toPointF(final Point point) {
        Intrinsics.checkParameterIsNotNull(point, "$this$toPointF");
        return new PointF(point);
    }
    
    public static final Point unaryMinus(final Point point) {
        Intrinsics.checkParameterIsNotNull(point, "$this$unaryMinus");
        return new Point(-point.x, -point.y);
    }
    
    public static final PointF unaryMinus(final PointF pointF) {
        Intrinsics.checkParameterIsNotNull(pointF, "$this$unaryMinus");
        return new PointF(-pointF.x, -pointF.y);
    }
}
