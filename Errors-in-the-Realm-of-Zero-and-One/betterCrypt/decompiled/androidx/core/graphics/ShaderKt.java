// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.graphics;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Unit;
import android.graphics.Matrix;
import kotlin.jvm.functions.Function1;
import android.graphics.Shader;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a&\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0017\u0010\u0003\u001a\u0013\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004¢\u0006\u0002\b\u0006H\u0086\b¨\u0006\u0007" }, d2 = { "transform", "", "Landroid/graphics/Shader;", "block", "Lkotlin/Function1;", "Landroid/graphics/Matrix;", "Lkotlin/ExtensionFunctionType;", "core-ktx_release" }, k = 2, mv = { 1, 1, 15 })
public final class ShaderKt
{
    public static final void transform(final Shader shader, final Function1<? super Matrix, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(shader, "$this$transform");
        Intrinsics.checkParameterIsNotNull(function1, "block");
        final Matrix localMatrix = new Matrix();
        shader.getLocalMatrix(localMatrix);
        function1.invoke(localMatrix);
        shader.setLocalMatrix(localMatrix);
    }
}
