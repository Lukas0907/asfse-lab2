// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.graphics;

import android.graphics.PorterDuff$Mode;
import android.graphics.BlendMode;
import android.graphics.PorterDuffColorFilter;
import android.graphics.BlendModeColorFilter;
import android.os.Build$VERSION;
import android.graphics.ColorFilter;

public class BlendModeColorFilterCompat
{
    private BlendModeColorFilterCompat() {
    }
    
    public static ColorFilter createBlendModeColorFilterCompat(final int n, final BlendModeCompat blendModeCompat) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        final ColorFilter colorFilter = null;
        final ColorFilter colorFilter2 = null;
        if (sdk_INT >= 29) {
            final BlendMode obtainBlendModeFromCompat = BlendModeUtils.obtainBlendModeFromCompat(blendModeCompat);
            Object o = colorFilter2;
            if (obtainBlendModeFromCompat != null) {
                o = new BlendModeColorFilter(n, obtainBlendModeFromCompat);
            }
            return (ColorFilter)o;
        }
        final PorterDuff$Mode obtainPorterDuffFromCompat = BlendModeUtils.obtainPorterDuffFromCompat(blendModeCompat);
        Object o2 = colorFilter;
        if (obtainPorterDuffFromCompat != null) {
            o2 = new PorterDuffColorFilter(n, obtainPorterDuffFromCompat);
        }
        return (ColorFilter)o2;
    }
}
