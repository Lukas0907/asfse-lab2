// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.graphics;

import android.graphics.fonts.FontFamily;
import java.io.InputStream;
import android.os.ParcelFileDescriptor;
import android.content.ContentResolver;
import androidx.core.provider.FontsContractCompat;
import android.os.CancellationSignal;
import android.graphics.fonts.Font;
import java.io.IOException;
import android.graphics.fonts.FontStyle;
import android.graphics.Typeface$CustomFallbackBuilder;
import android.graphics.fonts.FontFamily$Builder;
import android.graphics.fonts.Font$Builder;
import android.graphics.Typeface;
import android.content.res.Resources;
import androidx.core.content.res.FontResourcesParserCompat;
import android.content.Context;

public class TypefaceCompatApi29Impl extends TypefaceCompatBaseImpl
{
    @Override
    public Typeface createFromFontFamilyFilesResourceEntry(Context context, final FontResourcesParserCompat.FontFamilyFilesResourceEntry fontFamilyFilesResourceEntry, final Resources resources, final int n) {
        final FontResourcesParserCompat.FontFileResourceEntry[] entries = fontFamilyFilesResourceEntry.getEntries();
        final int length = entries.length;
        final int n2 = 0;
        context = null;
        int n3 = 0;
        while (true) {
            int slant = 1;
            Label_0124: {
                if (n3 >= length) {
                    break Label_0124;
                }
                while (true) {
                    final FontResourcesParserCompat.FontFileResourceEntry fontFileResourceEntry = entries[n3];
                    while (true) {
                    Label_0199:
                        while (true) {
                            try {
                                final Font$Builder setWeight = new Font$Builder(resources, fontFileResourceEntry.getResourceId()).setWeight(fontFileResourceEntry.getWeight());
                                if (fontFileResourceEntry.isItalic()) {
                                    final Font build = setWeight.setSlant(slant).setTtcIndex(fontFileResourceEntry.getTtcIndex()).setFontVariationSettings(fontFileResourceEntry.getVariationSettings()).build();
                                    if (context == null) {
                                        context = (Context)new FontFamily$Builder(build);
                                    }
                                    else {
                                        ((FontFamily$Builder)context).addFont(build);
                                    }
                                    ++n3;
                                    break;
                                }
                                break Label_0199;
                                // iftrue(Label_0145:, n & 0x1 == 0x0)
                                // iftrue(Label_0164:, n & 0x2 == 0x0)
                                while (true) {
                                    int n4 = 1;
                                    return new Typeface$CustomFallbackBuilder(((FontFamily$Builder)context).build()).setStyle(new FontStyle(n3, n4)).build();
                                    Label_0145: {
                                        n3 = 400;
                                    }
                                    Label_0150: {
                                        break Label_0150;
                                        while (true) {
                                            n3 = 700;
                                            break Label_0150;
                                            Label_0130:
                                            continue;
                                        }
                                        Label_0164:
                                        return new Typeface$CustomFallbackBuilder(((FontFamily$Builder)context).build()).setStyle(new FontStyle(n3, n4)).build();
                                    }
                                    n4 = n2;
                                    continue;
                                }
                                // iftrue(Label_0130:, context != null)
                                return null;
                            }
                            catch (IOException ex) {
                                continue;
                            }
                            break;
                        }
                        slant = 0;
                        continue;
                    }
                }
            }
        }
    }
    
    @Override
    public Typeface createFromFontInfo(Context context, CancellationSignal style, final FontsContractCompat.FontInfo[] array, final int n) {
        final ContentResolver contentResolver = context.getContentResolver();
        final int length = array.length;
        final int n2 = 0;
        context = null;
        int n3 = 0;
        while (true) {
            int slant = 1;
            Label_0218: {
                if (n3 >= length) {
                    break Label_0218;
                }
                while (true) {
                    final FontsContractCompat.FontInfo fontInfo = array[n3];
                    Object o = context;
                    while (true) {
                        Label_0293: {
                            while (true) {
                                try {
                                    final ParcelFileDescriptor openFileDescriptor = contentResolver.openFileDescriptor(fontInfo.getUri(), "r", style);
                                    Label_0065: {
                                        if (openFileDescriptor != null) {
                                            try {
                                                o = new Font$Builder(openFileDescriptor).setWeight(fontInfo.getWeight());
                                                if (!fontInfo.isItalic()) {
                                                    break Label_0293;
                                                }
                                                o = ((Font$Builder)o).setSlant(slant).setTtcIndex(fontInfo.getTtcIndex()).build();
                                                if (context == null) {
                                                    o = (context = (Context)new FontFamily$Builder((Font)o));
                                                }
                                                else {
                                                    ((FontFamily$Builder)context).addFont((Font)o);
                                                }
                                                o = context;
                                                if (openFileDescriptor != null) {
                                                    break Label_0065;
                                                }
                                            }
                                            finally {
                                                try {}
                                                finally {
                                                    if (openFileDescriptor != null) {
                                                        try {
                                                            openFileDescriptor.close();
                                                        }
                                                        finally {
                                                            o = context;
                                                            ((Throwable)fontInfo).addSuppressed((Throwable)openFileDescriptor);
                                                        }
                                                    }
                                                    o = context;
                                                }
                                            }
                                            break Label_0206;
                                        }
                                        o = context;
                                        if (openFileDescriptor == null) {
                                            break Label_0206;
                                        }
                                    }
                                    o = context;
                                    openFileDescriptor.close();
                                    o = context;
                                    ++n3;
                                    context = (Context)o;
                                    break;
                                    Label_0224: {
                                        n3 = 700;
                                    }
                                    // iftrue(Label_0239:, n & 0x1 == 0x0)
                                    // iftrue(Label_0258:, n & 0x2 == 0x0)
                                Label_0258_Outer:
                                    while (true) {
                                        break Label_0244;
                                        while (true) {
                                            int n4 = 0;
                                            style = (CancellationSignal)new FontStyle(n3, n4);
                                            return new Typeface$CustomFallbackBuilder(((FontFamily$Builder)context).build()).setStyle((FontStyle)style).build();
                                            n4 = n2;
                                            n4 = 1;
                                            continue;
                                        }
                                        Label_0239: {
                                            n3 = 400;
                                        }
                                        continue Label_0258_Outer;
                                    }
                                    // iftrue(Label_0224:, context != null)
                                    return null;
                                }
                                catch (IOException ex) {
                                    continue;
                                }
                                break;
                            }
                        }
                        slant = 0;
                        continue;
                    }
                }
            }
        }
    }
    
    @Override
    protected Typeface createFromInputStream(final Context context, final InputStream inputStream) {
        throw new RuntimeException("Do not use this function in API 29 or later.");
    }
    
    @Override
    public Typeface createFromResourcesFontFile(final Context context, final Resources resources, int n, final String s, int n2) {
        try {
            final FontFamily build = new FontFamily$Builder(new Font$Builder(resources, n).build()).build();
            if ((n2 & 0x1) != 0x0) {
                n = 700;
            }
            else {
                n = 400;
            }
            if ((n2 & 0x2) != 0x0) {
                n2 = 1;
            }
            else {
                n2 = 0;
            }
            return new Typeface$CustomFallbackBuilder(build).setStyle(new FontStyle(n, n2)).build();
        }
        catch (IOException ex) {
            return null;
        }
    }
    
    @Override
    protected FontsContractCompat.FontInfo findBestInfo(final FontsContractCompat.FontInfo[] array, final int n) {
        throw new RuntimeException("Do not use this function in API 29 or later.");
    }
}
