// 
// Decompiled by Procyon v0.5.36
// 

package androidx.core.transition;

import kotlin.jvm.internal.Intrinsics;
import android.transition.Transition$TransitionListener;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import android.transition.Transition;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u000b\u001a\u00c6\u0001\u0010\u0000\u001a\u00020\u0001*\u00020\u00022#\b\u0006\u0010\u0003\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u00042#\b\u0006\u0010\t\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u00042#\b\u0006\u0010\n\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u00042#\b\u0006\u0010\u000b\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u00042#\b\u0006\u0010\f\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0004H\u0087\b\u001a2\u0010\r\u001a\u00020\u0001*\u00020\u00022#\b\u0004\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0004H\u0087\b\u001a2\u0010\u000f\u001a\u00020\u0001*\u00020\u00022#\b\u0004\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0004H\u0087\b\u001a2\u0010\u0010\u001a\u00020\u0001*\u00020\u00022#\b\u0004\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0004H\u0087\b\u001a2\u0010\u0011\u001a\u00020\u0001*\u00020\u00022#\b\u0004\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0004H\u0087\b\u001a2\u0010\u0012\u001a\u00020\u0001*\u00020\u00022#\b\u0004\u0010\u000e\u001a\u001d\u0012\u0013\u0012\u00110\u0002¢\u0006\f\b\u0005\u0012\b\b\u0006\u0012\u0004\b\b(\u0007\u0012\u0004\u0012\u00020\b0\u0004H\u0087\b¨\u0006\u0013" }, d2 = { "addListener", "Landroid/transition/Transition$TransitionListener;", "Landroid/transition/Transition;", "onEnd", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "transition", "", "onStart", "onCancel", "onResume", "onPause", "doOnCancel", "action", "doOnEnd", "doOnPause", "doOnResume", "doOnStart", "core-ktx_release" }, k = 2, mv = { 1, 1, 15 })
public final class TransitionKt
{
    public static final Transition$TransitionListener addListener(final Transition transition, final Function1<? super Transition, Unit> function1, final Function1<? super Transition, Unit> function2, final Function1<? super Transition, Unit> function3, final Function1<? super Transition, Unit> function4, final Function1<? super Transition, Unit> function5) {
        Intrinsics.checkParameterIsNotNull(transition, "$this$addListener");
        Intrinsics.checkParameterIsNotNull(function1, "onEnd");
        Intrinsics.checkParameterIsNotNull(function2, "onStart");
        Intrinsics.checkParameterIsNotNull(function3, "onCancel");
        Intrinsics.checkParameterIsNotNull(function4, "onResume");
        Intrinsics.checkParameterIsNotNull(function5, "onPause");
        final Transition$TransitionListener transition$TransitionListener = (Transition$TransitionListener)new TransitionKt$addListener$listener.TransitionKt$addListener$listener$1((Function1)function1, (Function1)function4, (Function1)function5, (Function1)function3, (Function1)function2);
        transition.addListener(transition$TransitionListener);
        return transition$TransitionListener;
    }
    
    public static final Transition$TransitionListener doOnCancel(final Transition transition, final Function1<? super Transition, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(transition, "$this$doOnCancel");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        final Transition$TransitionListener transition$TransitionListener = (Transition$TransitionListener)new TransitionKt$doOnCancel$$inlined$addListener.TransitionKt$doOnCancel$$inlined$addListener$1((Function1)function1);
        transition.addListener(transition$TransitionListener);
        return transition$TransitionListener;
    }
    
    public static final Transition$TransitionListener doOnEnd(final Transition transition, final Function1<? super Transition, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(transition, "$this$doOnEnd");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        final Transition$TransitionListener transition$TransitionListener = (Transition$TransitionListener)new TransitionKt$doOnEnd$$inlined$addListener.TransitionKt$doOnEnd$$inlined$addListener$1((Function1)function1);
        transition.addListener(transition$TransitionListener);
        return transition$TransitionListener;
    }
    
    public static final Transition$TransitionListener doOnPause(final Transition transition, final Function1<? super Transition, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(transition, "$this$doOnPause");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        final Transition$TransitionListener transition$TransitionListener = (Transition$TransitionListener)new TransitionKt$doOnPause$$inlined$addListener.TransitionKt$doOnPause$$inlined$addListener$1((Function1)function1);
        transition.addListener(transition$TransitionListener);
        return transition$TransitionListener;
    }
    
    public static final Transition$TransitionListener doOnResume(final Transition transition, final Function1<? super Transition, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(transition, "$this$doOnResume");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        final Transition$TransitionListener transition$TransitionListener = (Transition$TransitionListener)new TransitionKt$doOnResume$$inlined$addListener.TransitionKt$doOnResume$$inlined$addListener$1((Function1)function1);
        transition.addListener(transition$TransitionListener);
        return transition$TransitionListener;
    }
    
    public static final Transition$TransitionListener doOnStart(final Transition transition, final Function1<? super Transition, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(transition, "$this$doOnStart");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        final Transition$TransitionListener transition$TransitionListener = (Transition$TransitionListener)new TransitionKt$doOnStart$$inlined$addListener.TransitionKt$doOnStart$$inlined$addListener$1((Function1)function1);
        transition.addListener(transition$TransitionListener);
        return transition$TransitionListener;
    }
}
