// 
// Decompiled by Procyon v0.5.36
// 

package androidx.vectordrawable;

public final class R
{
    private R() {
    }
    
    public static final class attr
    {
        public static final int alpha = 2130903079;
        public static final int coordinatorLayoutStyle = 2130903201;
        public static final int font = 2130903252;
        public static final int fontProviderAuthority = 2130903254;
        public static final int fontProviderCerts = 2130903255;
        public static final int fontProviderFetchStrategy = 2130903256;
        public static final int fontProviderFetchTimeout = 2130903257;
        public static final int fontProviderPackage = 2130903258;
        public static final int fontProviderQuery = 2130903259;
        public static final int fontStyle = 2130903260;
        public static final int fontVariationSettings = 2130903261;
        public static final int fontWeight = 2130903262;
        public static final int keylines = 2130903305;
        public static final int layout_anchor = 2130903310;
        public static final int layout_anchorGravity = 2130903311;
        public static final int layout_behavior = 2130903312;
        public static final int layout_dodgeInsetEdges = 2130903356;
        public static final int layout_insetEdge = 2130903368;
        public static final int layout_keyline = 2130903369;
        public static final int statusBarBackground = 2130903465;
        public static final int ttcIndex = 2130903563;
        
        private attr() {
        }
    }
    
    public static final class color
    {
        public static final int notification_action_color_filter = 2131034218;
        public static final int notification_icon_bg_color = 2131034219;
        public static final int ripple_material_light = 2131034233;
        public static final int secondary_text_default_material_light = 2131034236;
        
        private color() {
        }
    }
    
    public static final class dimen
    {
        public static final int compat_button_inset_horizontal_material = 2131099726;
        public static final int compat_button_inset_vertical_material = 2131099727;
        public static final int compat_button_padding_horizontal_material = 2131099728;
        public static final int compat_button_padding_vertical_material = 2131099729;
        public static final int compat_control_corner_material = 2131099730;
        public static final int compat_notification_large_icon_max_height = 2131099731;
        public static final int compat_notification_large_icon_max_width = 2131099732;
        public static final int notification_action_icon_size = 2131099841;
        public static final int notification_action_text_size = 2131099842;
        public static final int notification_big_circle_margin = 2131099843;
        public static final int notification_content_margin_start = 2131099844;
        public static final int notification_large_icon_height = 2131099845;
        public static final int notification_large_icon_width = 2131099846;
        public static final int notification_main_column_padding_top = 2131099847;
        public static final int notification_media_narrow_margin = 2131099848;
        public static final int notification_right_icon_size = 2131099849;
        public static final int notification_right_side_padding_top = 2131099850;
        public static final int notification_small_icon_background_padding = 2131099851;
        public static final int notification_small_icon_size_as_large = 2131099852;
        public static final int notification_subtext_size = 2131099853;
        public static final int notification_top_pad = 2131099854;
        public static final int notification_top_pad_large_text = 2131099855;
        
        private dimen() {
        }
    }
    
    public static final class drawable
    {
        public static final int notification_action_background = 2131165290;
        public static final int notification_bg = 2131165291;
        public static final int notification_bg_low = 2131165292;
        public static final int notification_bg_low_normal = 2131165293;
        public static final int notification_bg_low_pressed = 2131165294;
        public static final int notification_bg_normal = 2131165295;
        public static final int notification_bg_normal_pressed = 2131165296;
        public static final int notification_icon_background = 2131165297;
        public static final int notification_template_icon_bg = 2131165298;
        public static final int notification_template_icon_low_bg = 2131165299;
        public static final int notification_tile_bg = 2131165300;
        public static final int notify_panel_notification_icon_bg = 2131165301;
        
        private drawable() {
        }
    }
    
    public static final class id
    {
        public static final int action_container = 2131230766;
        public static final int action_divider = 2131230768;
        public static final int action_image = 2131230769;
        public static final int action_text = 2131230776;
        public static final int actions = 2131230777;
        public static final int async = 2131230783;
        public static final int blocking = 2131230787;
        public static final int bottom = 2131230788;
        public static final int chronometer = 2131230796;
        public static final int end = 2131230820;
        public static final int forever = 2131230836;
        public static final int icon = 2131230843;
        public static final int icon_group = 2131230844;
        public static final int info = 2131230847;
        public static final int italic = 2131230850;
        public static final int left = 2131230855;
        public static final int line1 = 2131230856;
        public static final int line3 = 2131230857;
        public static final int none = 2131230869;
        public static final int normal = 2131230870;
        public static final int notification_background = 2131230871;
        public static final int notification_main_column = 2131230872;
        public static final int notification_main_column_container = 2131230873;
        public static final int right = 2131230888;
        public static final int right_icon = 2131230889;
        public static final int right_side = 2131230890;
        public static final int start = 2131230929;
        public static final int tag_transition_group = 2131230940;
        public static final int tag_unhandled_key_event_manager = 2131230941;
        public static final int tag_unhandled_key_listeners = 2131230942;
        public static final int text = 2131230943;
        public static final int text2 = 2131230944;
        public static final int time = 2131230952;
        public static final int title = 2131230953;
        public static final int top = 2131230956;
        
        private id() {
        }
    }
    
    public static final class integer
    {
        public static final int status_bar_notification_info_maxnum = 2131296270;
        
        private integer() {
        }
    }
    
    public static final class layout
    {
        public static final int notification_action = 2131427378;
        public static final int notification_action_tombstone = 2131427379;
        public static final int notification_template_custom_big = 2131427380;
        public static final int notification_template_icon_group = 2131427381;
        public static final int notification_template_part_chronometer = 2131427382;
        public static final int notification_template_part_time = 2131427383;
        
        private layout() {
        }
    }
    
    public static final class string
    {
        public static final int status_bar_notification_info_overflow = 2131623997;
        
        private string() {
        }
    }
    
    public static final class style
    {
        public static final int TextAppearance_Compat_Notification = 2131689746;
        public static final int TextAppearance_Compat_Notification_Info = 2131689747;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131689748;
        public static final int TextAppearance_Compat_Notification_Time = 2131689749;
        public static final int TextAppearance_Compat_Notification_Title = 2131689750;
        public static final int Widget_Compat_NotificationActionContainer = 2131689916;
        public static final int Widget_Compat_NotificationActionText = 2131689917;
        public static final int Widget_Support_CoordinatorLayout = 2131689961;
        
        private style() {
        }
    }
    
    public static final class styleable
    {
        public static final int[] ColorStateListItem;
        public static final int ColorStateListItem_alpha = 2;
        public static final int ColorStateListItem_android_alpha = 1;
        public static final int ColorStateListItem_android_color = 0;
        public static final int[] CoordinatorLayout;
        public static final int[] CoordinatorLayout_Layout;
        public static final int CoordinatorLayout_Layout_android_layout_gravity = 0;
        public static final int CoordinatorLayout_Layout_layout_anchor = 1;
        public static final int CoordinatorLayout_Layout_layout_anchorGravity = 2;
        public static final int CoordinatorLayout_Layout_layout_behavior = 3;
        public static final int CoordinatorLayout_Layout_layout_dodgeInsetEdges = 4;
        public static final int CoordinatorLayout_Layout_layout_insetEdge = 5;
        public static final int CoordinatorLayout_Layout_layout_keyline = 6;
        public static final int CoordinatorLayout_keylines = 0;
        public static final int CoordinatorLayout_statusBarBackground = 1;
        public static final int[] FontFamily;
        public static final int[] FontFamilyFont;
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontVariationSettings = 4;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_android_ttcIndex = 3;
        public static final int FontFamilyFont_font = 5;
        public static final int FontFamilyFont_fontStyle = 6;
        public static final int FontFamilyFont_fontVariationSettings = 7;
        public static final int FontFamilyFont_fontWeight = 8;
        public static final int FontFamilyFont_ttcIndex = 9;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int[] GradientColor;
        public static final int[] GradientColorItem;
        public static final int GradientColorItem_android_color = 0;
        public static final int GradientColorItem_android_offset = 1;
        public static final int GradientColor_android_centerColor = 7;
        public static final int GradientColor_android_centerX = 3;
        public static final int GradientColor_android_centerY = 4;
        public static final int GradientColor_android_endColor = 1;
        public static final int GradientColor_android_endX = 10;
        public static final int GradientColor_android_endY = 11;
        public static final int GradientColor_android_gradientRadius = 5;
        public static final int GradientColor_android_startColor = 0;
        public static final int GradientColor_android_startX = 8;
        public static final int GradientColor_android_startY = 9;
        public static final int GradientColor_android_tileMode = 6;
        public static final int GradientColor_android_type = 2;
        
        static {
            ColorStateListItem = new int[] { 16843173, 16843551, 2130903079 };
            CoordinatorLayout = new int[] { 2130903305, 2130903465 };
            CoordinatorLayout_Layout = new int[] { 16842931, 2130903310, 2130903311, 2130903312, 2130903356, 2130903368, 2130903369 };
            FontFamily = new int[] { 2130903254, 2130903255, 2130903256, 2130903257, 2130903258, 2130903259 };
            FontFamilyFont = new int[] { 16844082, 16844083, 16844095, 16844143, 16844144, 2130903252, 2130903260, 2130903261, 2130903262, 2130903563 };
            GradientColor = new int[] { 16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051 };
            GradientColorItem = new int[] { 16843173, 16844052 };
        }
        
        private styleable() {
        }
    }
}
