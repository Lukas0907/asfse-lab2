// 
// Decompiled by Procyon v0.5.36
// 

package androidx.vectordrawable.graphics.drawable;

import android.content.res.XmlResourceParser;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator;
import android.content.res.Resources$NotFoundException;
import androidx.interpolator.view.animation.FastOutLinearInInterpolator;
import android.view.animation.AnimationUtils;
import android.os.Build$VERSION;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;
import android.util.AttributeSet;
import android.view.animation.BounceInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.CycleInterpolator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.util.Xml;
import android.view.animation.Interpolator;
import org.xmlpull.v1.XmlPullParser;
import android.content.res.Resources$Theme;
import android.content.res.Resources;
import android.content.Context;

public class AnimationUtilsCompat
{
    private AnimationUtilsCompat() {
    }
    
    private static Interpolator createInterpolatorFromXml(final Context context, final Resources resources, final Resources$Theme resources$Theme, final XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        final int depth = xmlPullParser.getDepth();
        Object o = null;
        while (true) {
            final int next = xmlPullParser.next();
            if ((next == 3 && xmlPullParser.getDepth() <= depth) || next == 1) {
                return (Interpolator)o;
            }
            if (next != 2) {
                continue;
            }
            final AttributeSet attributeSet = Xml.asAttributeSet(xmlPullParser);
            final String name = xmlPullParser.getName();
            if (name.equals("linearInterpolator")) {
                o = new LinearInterpolator();
            }
            else if (name.equals("accelerateInterpolator")) {
                o = new AccelerateInterpolator(context, attributeSet);
            }
            else if (name.equals("decelerateInterpolator")) {
                o = new DecelerateInterpolator(context, attributeSet);
            }
            else if (name.equals("accelerateDecelerateInterpolator")) {
                o = new AccelerateDecelerateInterpolator();
            }
            else if (name.equals("cycleInterpolator")) {
                o = new CycleInterpolator(context, attributeSet);
            }
            else if (name.equals("anticipateInterpolator")) {
                o = new AnticipateInterpolator(context, attributeSet);
            }
            else if (name.equals("overshootInterpolator")) {
                o = new OvershootInterpolator(context, attributeSet);
            }
            else if (name.equals("anticipateOvershootInterpolator")) {
                o = new AnticipateOvershootInterpolator(context, attributeSet);
            }
            else if (name.equals("bounceInterpolator")) {
                o = new BounceInterpolator();
            }
            else {
                if (!name.equals("pathInterpolator")) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unknown interpolator name: ");
                    sb.append(xmlPullParser.getName());
                    throw new RuntimeException(sb.toString());
                }
                o = new PathInterpolatorCompat(context, attributeSet, xmlPullParser);
            }
        }
    }
    
    public static Interpolator loadInterpolator(final Context context, final int i) throws Resources$NotFoundException {
        if (Build$VERSION.SDK_INT >= 21) {
            return AnimationUtils.loadInterpolator(context, i);
        }
        Object o = null;
        XmlResourceParser xmlResourceParser = null;
        XmlResourceParser animation = null;
        while (true) {
            if (i == 17563663) {
                while (true) {
                    try {
                        try {
                            return (Interpolator)new FastOutLinearInInterpolator();
                            animation = xmlResourceParser;
                            o = new StringBuilder();
                            animation = xmlResourceParser;
                            ((StringBuilder)o).append("Can't load animation resource ID #0x");
                            animation = xmlResourceParser;
                            ((StringBuilder)o).append(Integer.toHexString(i));
                            animation = xmlResourceParser;
                            o = new Resources$NotFoundException(((StringBuilder)o).toString());
                            animation = xmlResourceParser;
                            final Throwable t;
                            ((Resources$NotFoundException)o).initCause(t);
                            animation = xmlResourceParser;
                            throw o;
                            final Resources$NotFoundException ex;
                            Label_0063: {
                                ex = (Resources$NotFoundException)(xmlResourceParser = (XmlResourceParser)(o = (animation = context.getResources().getAnimation(i))));
                            }
                            final Interpolator interpolatorFromXml = createInterpolatorFromXml(context, context.getResources(), context.getTheme(), (XmlPullParser)ex);
                            // iftrue(Label_0110:, ex == null)
                            // iftrue(Label_0063:, i != 17563662)
                            // iftrue(Label_0247:, animation == null)
                            final Throwable t2;
                            while (true) {
                                Block_7: {
                                    break Block_7;
                                    animation.close();
                                    throw t2;
                                }
                                ((XmlResourceParser)ex).close();
                                return interpolatorFromXml;
                                Label_0049:
                                return (Interpolator)new LinearOutSlowInInterpolator();
                                continue;
                            }
                            Label_0247:
                            throw t2;
                            Label_0110:
                            return interpolatorFromXml;
                            // iftrue(Label_0049:, i != 17563661)
                            return (Interpolator)new FastOutSlowInInterpolator();
                        }
                        finally {}
                    }
                    catch (IOException ex3) {}
                    catch (XmlPullParserException ex2) {}
                    final XmlPullParserException ex2;
                    final Throwable t = (Throwable)ex2;
                    continue;
                }
            }
            continue;
        }
    }
}
