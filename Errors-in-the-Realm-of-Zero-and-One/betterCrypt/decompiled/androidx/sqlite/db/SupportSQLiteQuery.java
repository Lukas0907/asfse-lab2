// 
// Decompiled by Procyon v0.5.36
// 

package androidx.sqlite.db;

public interface SupportSQLiteQuery
{
    void bindTo(final SupportSQLiteProgram p0);
    
    int getArgCount();
    
    String getSql();
}
