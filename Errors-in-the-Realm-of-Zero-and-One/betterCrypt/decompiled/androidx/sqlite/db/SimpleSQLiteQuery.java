// 
// Decompiled by Procyon v0.5.36
// 

package androidx.sqlite.db;

public final class SimpleSQLiteQuery implements SupportSQLiteQuery
{
    private final Object[] mBindArgs;
    private final String mQuery;
    
    public SimpleSQLiteQuery(final String s) {
        this(s, null);
    }
    
    public SimpleSQLiteQuery(final String mQuery, final Object[] mBindArgs) {
        this.mQuery = mQuery;
        this.mBindArgs = mBindArgs;
    }
    
    private static void bind(final SupportSQLiteProgram supportSQLiteProgram, final int i, final Object obj) {
        if (obj == null) {
            supportSQLiteProgram.bindNull(i);
            return;
        }
        if (obj instanceof byte[]) {
            supportSQLiteProgram.bindBlob(i, (byte[])obj);
            return;
        }
        if (obj instanceof Float) {
            supportSQLiteProgram.bindDouble(i, (float)obj);
            return;
        }
        if (obj instanceof Double) {
            supportSQLiteProgram.bindDouble(i, (double)obj);
            return;
        }
        if (obj instanceof Long) {
            supportSQLiteProgram.bindLong(i, (long)obj);
            return;
        }
        if (obj instanceof Integer) {
            supportSQLiteProgram.bindLong(i, (int)obj);
            return;
        }
        if (obj instanceof Short) {
            supportSQLiteProgram.bindLong(i, (short)obj);
            return;
        }
        if (obj instanceof Byte) {
            supportSQLiteProgram.bindLong(i, (byte)obj);
            return;
        }
        if (obj instanceof String) {
            supportSQLiteProgram.bindString(i, (String)obj);
            return;
        }
        if (obj instanceof Boolean) {
            long n;
            if (obj) {
                n = 1L;
            }
            else {
                n = 0L;
            }
            supportSQLiteProgram.bindLong(i, n);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Cannot bind ");
        sb.append(obj);
        sb.append(" at index ");
        sb.append(i);
        sb.append(" Supported types: null, byte[], float, double, long, int, short, byte,");
        sb.append(" string");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static void bind(final SupportSQLiteProgram supportSQLiteProgram, final Object[] array) {
        if (array == null) {
            return;
        }
        final int length = array.length;
        int i = 0;
        while (i < length) {
            final Object o = array[i];
            ++i;
            bind(supportSQLiteProgram, i, o);
        }
    }
    
    @Override
    public void bindTo(final SupportSQLiteProgram supportSQLiteProgram) {
        bind(supportSQLiteProgram, this.mBindArgs);
    }
    
    @Override
    public int getArgCount() {
        final Object[] mBindArgs = this.mBindArgs;
        if (mBindArgs == null) {
            return 0;
        }
        return mBindArgs.length;
    }
    
    @Override
    public String getSql() {
        return this.mQuery;
    }
}
