// 
// Decompiled by Procyon v0.5.36
// 

package androidx.sqlite.db.framework;

import androidx.sqlite.db.SupportSQLiteOpenHelper;

public final class FrameworkSQLiteOpenHelperFactory implements Factory
{
    @Override
    public SupportSQLiteOpenHelper create(final Configuration configuration) {
        return new FrameworkSQLiteOpenHelper(configuration.context, configuration.name, configuration.callback);
    }
}
