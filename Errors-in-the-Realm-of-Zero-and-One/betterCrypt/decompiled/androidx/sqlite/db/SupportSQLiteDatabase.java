// 
// Decompiled by Procyon v0.5.36
// 

package androidx.sqlite.db;

import java.util.Locale;
import android.os.CancellationSignal;
import android.database.Cursor;
import android.content.ContentValues;
import android.util.Pair;
import java.util.List;
import android.database.SQLException;
import android.database.sqlite.SQLiteTransactionListener;
import java.io.Closeable;

public interface SupportSQLiteDatabase extends Closeable
{
    void beginTransaction();
    
    void beginTransactionNonExclusive();
    
    void beginTransactionWithListener(final SQLiteTransactionListener p0);
    
    void beginTransactionWithListenerNonExclusive(final SQLiteTransactionListener p0);
    
    SupportSQLiteStatement compileStatement(final String p0);
    
    int delete(final String p0, final String p1, final Object[] p2);
    
    void disableWriteAheadLogging();
    
    boolean enableWriteAheadLogging();
    
    void endTransaction();
    
    void execSQL(final String p0) throws SQLException;
    
    void execSQL(final String p0, final Object[] p1) throws SQLException;
    
    List<Pair<String, String>> getAttachedDbs();
    
    long getMaximumSize();
    
    long getPageSize();
    
    String getPath();
    
    int getVersion();
    
    boolean inTransaction();
    
    long insert(final String p0, final int p1, final ContentValues p2) throws SQLException;
    
    boolean isDatabaseIntegrityOk();
    
    boolean isDbLockedByCurrentThread();
    
    boolean isOpen();
    
    boolean isReadOnly();
    
    boolean isWriteAheadLoggingEnabled();
    
    boolean needUpgrade(final int p0);
    
    Cursor query(final SupportSQLiteQuery p0);
    
    Cursor query(final SupportSQLiteQuery p0, final CancellationSignal p1);
    
    Cursor query(final String p0);
    
    Cursor query(final String p0, final Object[] p1);
    
    void setForeignKeyConstraintsEnabled(final boolean p0);
    
    void setLocale(final Locale p0);
    
    void setMaxSqlCacheSize(final int p0);
    
    long setMaximumSize(final long p0);
    
    void setPageSize(final long p0);
    
    void setTransactionSuccessful();
    
    void setVersion(final int p0);
    
    int update(final String p0, final int p1, final ContentValues p2, final String p3, final Object[] p4);
    
    boolean yieldIfContendedSafely();
    
    boolean yieldIfContendedSafely(final long p0);
}
