// 
// Decompiled by Procyon v0.5.36
// 

package androidx.sqlite.db;

public interface SupportSQLiteStatement extends SupportSQLiteProgram
{
    void execute();
    
    long executeInsert();
    
    int executeUpdateDelete();
    
    long simpleQueryForLong();
    
    String simpleQueryForString();
}
