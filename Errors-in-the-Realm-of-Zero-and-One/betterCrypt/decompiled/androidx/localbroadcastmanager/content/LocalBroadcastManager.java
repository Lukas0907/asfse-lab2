// 
// Decompiled by Procyon v0.5.36
// 

package androidx.localbroadcastmanager.content;

import java.io.Serializable;
import java.util.Set;
import android.net.Uri;
import android.util.Log;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Message;
import android.os.Looper;
import android.content.BroadcastReceiver;
import android.os.Handler;
import android.content.Context;
import java.util.ArrayList;
import java.util.HashMap;

public final class LocalBroadcastManager
{
    private static final boolean DEBUG = false;
    static final int MSG_EXEC_PENDING_BROADCASTS = 1;
    private static final String TAG = "LocalBroadcastManager";
    private static LocalBroadcastManager mInstance;
    private static final Object mLock;
    private final HashMap<String, ArrayList<ReceiverRecord>> mActions;
    private final Context mAppContext;
    private final Handler mHandler;
    private final ArrayList<BroadcastRecord> mPendingBroadcasts;
    private final HashMap<BroadcastReceiver, ArrayList<ReceiverRecord>> mReceivers;
    
    static {
        mLock = new Object();
    }
    
    private LocalBroadcastManager(final Context mAppContext) {
        this.mReceivers = new HashMap<BroadcastReceiver, ArrayList<ReceiverRecord>>();
        this.mActions = new HashMap<String, ArrayList<ReceiverRecord>>();
        this.mPendingBroadcasts = new ArrayList<BroadcastRecord>();
        this.mAppContext = mAppContext;
        this.mHandler = new Handler(mAppContext.getMainLooper()) {
            public void handleMessage(final Message message) {
                if (message.what != 1) {
                    super.handleMessage(message);
                    return;
                }
                LocalBroadcastManager.this.executePendingBroadcasts();
            }
        };
    }
    
    public static LocalBroadcastManager getInstance(final Context context) {
        synchronized (LocalBroadcastManager.mLock) {
            if (LocalBroadcastManager.mInstance == null) {
                LocalBroadcastManager.mInstance = new LocalBroadcastManager(context.getApplicationContext());
            }
            return LocalBroadcastManager.mInstance;
        }
    }
    
    void executePendingBroadcasts() {
        while (true) {
            Object mReceivers = this.mReceivers;
            synchronized (mReceivers) {
                final int size = this.mPendingBroadcasts.size();
                if (size <= 0) {
                    return;
                }
                final BroadcastRecord[] a = new BroadcastRecord[size];
                this.mPendingBroadcasts.toArray(a);
                this.mPendingBroadcasts.clear();
                // monitorexit(mReceivers)
                for (int i = 0; i < a.length; ++i) {
                    mReceivers = a[i];
                    for (int size2 = ((BroadcastRecord)mReceivers).receivers.size(), j = 0; j < size2; ++j) {
                        final ReceiverRecord receiverRecord = ((BroadcastRecord)mReceivers).receivers.get(j);
                        if (!receiverRecord.dead) {
                            receiverRecord.receiver.onReceive(this.mAppContext, ((BroadcastRecord)mReceivers).intent);
                        }
                    }
                }
            }
        }
    }
    
    public void registerReceiver(final BroadcastReceiver broadcastReceiver, final IntentFilter intentFilter) {
        synchronized (this.mReceivers) {
            final ReceiverRecord receiverRecord = new ReceiverRecord(intentFilter, broadcastReceiver);
            ArrayList<ReceiverRecord> value;
            if ((value = this.mReceivers.get(broadcastReceiver)) == null) {
                value = new ArrayList<ReceiverRecord>(1);
                this.mReceivers.put(broadcastReceiver, value);
            }
            value.add(receiverRecord);
            for (int i = 0; i < intentFilter.countActions(); ++i) {
                final String action = intentFilter.getAction(i);
                ArrayList<ReceiverRecord> value2;
                if ((value2 = this.mActions.get(action)) == null) {
                    value2 = new ArrayList<ReceiverRecord>(1);
                    this.mActions.put(action, value2);
                }
                value2.add(receiverRecord);
            }
        }
    }
    
    public boolean sendBroadcast(final Intent obj) {
    Label_0433_Outer:
        while (true) {
            String action;
            String resolveTypeIfNeeded;
            Uri data;
            String scheme;
            Set categories;
            int index = 0;
            Serializable str;
            ArrayList<ReceiverRecord> obj2;
            Serializable s = null;
            int match = 0;
            int index2 = 0;
            Object e;
            IntentFilter filter;
            Label_0433:Label_0377_Outer:
            while (true) {
                Label_0196:Label_0394_Outer:
                while (true) {
                    Label_0605: {
                        while (true) {
                        Label_0377:
                            while (true) {
                                Label_0529: {
                                    Label_0521:Block_17_Outer:Label_0259_Outer:
                                    while (true) {
                                        Label_0516: {
                                            synchronized (this.mReceivers) {
                                                action = obj.getAction();
                                                resolveTypeIfNeeded = obj.resolveTypeIfNeeded(this.mAppContext.getContentResolver());
                                                data = obj.getData();
                                                scheme = obj.getScheme();
                                                categories = obj.getCategories();
                                                if ((obj.getFlags() & 0x8) == 0x0) {
                                                    break Label_0516;
                                                }
                                                index = 1;
                                                if (index != 0) {
                                                    str = new StringBuilder();
                                                    ((StringBuilder)str).append("Resolving type ");
                                                    ((StringBuilder)str).append(resolveTypeIfNeeded);
                                                    ((StringBuilder)str).append(" scheme ");
                                                    ((StringBuilder)str).append(scheme);
                                                    ((StringBuilder)str).append(" of intent ");
                                                    ((StringBuilder)str).append(obj);
                                                    Log.v("LocalBroadcastManager", ((StringBuilder)str).toString());
                                                }
                                                obj2 = this.mActions.get(obj.getAction());
                                                if (obj2 == null) {
                                                    return false;
                                                }
                                                if (index != 0) {
                                                    str = new StringBuilder();
                                                    ((StringBuilder)str).append("Action list: ");
                                                    ((StringBuilder)str).append(obj2);
                                                    Log.v("LocalBroadcastManager", ((StringBuilder)str).toString());
                                                }
                                                break Label_0521;
                                                // iftrue(Label_0462:, index >= s.size())
                                                // iftrue(Label_0532:, str != null)
                                                // iftrue(Label_0360:, index == 0)
                                                // iftrue(Label_0612:, index2 >= obj2.size())
                                                // iftrue(Label_0529:, index == 0)
                                                // iftrue(Label_0500:, this.mHandler.hasMessages(1))
                                                // iftrue(Label_0259:, index == 0)
                                                // iftrue(Label_0539:, match < 0)
                                                // iftrue(Label_0282:, !e.broadcasting)
                                                Block_16: {
                                                    Block_21: {
                                                        while (true) {
                                                            Block_14: {
                                                                Block_20: {
                                                                    break Block_20;
                                                                    while (true) {
                                                                        while (true) {
                                                                        Block_19:
                                                                            while (true) {
                                                                                break Block_19;
                                                                                s = new StringBuilder();
                                                                                ((StringBuilder)s).append("  Filter matched!  match=0x");
                                                                                ((StringBuilder)s).append(Integer.toHexString(match));
                                                                                Log.v("LocalBroadcastManager", ((StringBuilder)s).toString());
                                                                                continue Block_17_Outer;
                                                                            }
                                                                            s = new ArrayList();
                                                                            break Label_0377;
                                                                            Block_13: {
                                                                                break Block_13;
                                                                                break Block_16;
                                                                                Label_0462: {
                                                                                    this.mPendingBroadcasts.add(new BroadcastRecord(obj, (ArrayList<ReceiverRecord>)s));
                                                                                }
                                                                                break Block_21;
                                                                            }
                                                                            e = obj2.get(index2);
                                                                            break Block_14;
                                                                            Label_0282: {
                                                                                filter = ((ReceiverRecord)e).filter;
                                                                            }
                                                                            str = s;
                                                                            match = filter.match(action, resolveTypeIfNeeded, scheme, data, categories, "LocalBroadcastManager");
                                                                            continue Label_0377_Outer;
                                                                        }
                                                                        Label_0500: {
                                                                            return true;
                                                                        }
                                                                        continue Label_0259_Outer;
                                                                    }
                                                                    ((ArrayList<ReceiverRecord>)s).add((ReceiverRecord)e);
                                                                    ((ReceiverRecord)e).broadcasting = true;
                                                                    break Label_0605;
                                                                }
                                                                ((ReceiverRecord)((ArrayList<ReceiverRecord>)s).get(index)).broadcasting = false;
                                                                ++index;
                                                                continue Label_0433;
                                                                e = new StringBuilder();
                                                                ((StringBuilder)e).append("  Filter did not match: ");
                                                                ((StringBuilder)e).append((String)str);
                                                                Log.v("LocalBroadcastManager", ((StringBuilder)e).toString());
                                                                break Label_0605;
                                                            }
                                                            str = new StringBuilder();
                                                            ((StringBuilder)str).append("Matching against filter ");
                                                            ((StringBuilder)str).append(((ReceiverRecord)e).filter);
                                                            Log.v("LocalBroadcastManager", ((StringBuilder)str).toString());
                                                            continue Label_0394_Outer;
                                                        }
                                                    }
                                                    this.mHandler.sendEmptyMessage(1);
                                                    return true;
                                                }
                                                Log.v("LocalBroadcastManager", "  Filter's target already added");
                                                break Label_0529;
                                            }
                                        }
                                        index = 0;
                                        continue Label_0433_Outer;
                                    }
                                    s = null;
                                    index2 = 0;
                                    continue Label_0196;
                                }
                                break Label_0605;
                                Label_0532: {
                                    s = str;
                                }
                                continue Label_0377;
                            }
                            Label_0539: {
                                if (index != 0) {
                                    if (match == -4) {
                                        str = "category";
                                        continue;
                                    }
                                    if (match == -3) {
                                        str = "action";
                                        continue;
                                    }
                                    if (match == -2) {
                                        str = "data";
                                        continue;
                                    }
                                    if (match != -1) {
                                        str = "unknown reason";
                                        continue;
                                    }
                                    str = "type";
                                    continue;
                                }
                            }
                            break;
                        }
                    }
                    ++index2;
                    continue Label_0196;
                }
                Label_0612: {
                    if (s != null) {
                        index = 0;
                        continue Label_0433;
                    }
                }
                break;
            }
            return false;
        }
    }
    
    public void sendBroadcastSync(final Intent intent) {
        if (this.sendBroadcast(intent)) {
            this.executePendingBroadcasts();
        }
    }
    
    public void unregisterReceiver(final BroadcastReceiver key) {
        while (true) {
        Label_0062_Outer:
            while (true) {
                int index = 0;
            Label_0203:
                while (true) {
                    int n = 0;
                    Label_0196: {
                        while (true) {
                            int n2;
                            synchronized (this.mReceivers) {
                                final ArrayList<ReceiverRecord> list = this.mReceivers.remove(key);
                                if (list == null) {
                                    return;
                                }
                                index = list.size() - 1;
                                if (index < 0) {
                                    return;
                                }
                                final ReceiverRecord receiverRecord = list.get(index);
                                receiverRecord.dead = true;
                                n = 0;
                                if (n >= receiverRecord.filter.countActions()) {
                                    break Label_0203;
                                }
                                final String action = receiverRecord.filter.getAction(n);
                                final ArrayList<ReceiverRecord> list2 = this.mActions.get(action);
                                if (list2 == null) {
                                    break Label_0196;
                                }
                                n2 = list2.size() - 1;
                                if (n2 >= 0) {
                                    final ReceiverRecord receiverRecord2 = list2.get(n2);
                                    if (receiverRecord2.receiver == key) {
                                        receiverRecord2.dead = true;
                                        list2.remove(n2);
                                    }
                                }
                                else {
                                    if (list2.size() <= 0) {
                                        this.mActions.remove(action);
                                    }
                                    break Label_0196;
                                }
                            }
                            --n2;
                            continue;
                        }
                    }
                    ++n;
                    continue;
                }
                --index;
                continue Label_0062_Outer;
            }
        }
    }
    
    private static final class BroadcastRecord
    {
        final Intent intent;
        final ArrayList<ReceiverRecord> receivers;
        
        BroadcastRecord(final Intent intent, final ArrayList<ReceiverRecord> receivers) {
            this.intent = intent;
            this.receivers = receivers;
        }
    }
    
    private static final class ReceiverRecord
    {
        boolean broadcasting;
        boolean dead;
        final IntentFilter filter;
        final BroadcastReceiver receiver;
        
        ReceiverRecord(final IntentFilter filter, final BroadcastReceiver receiver) {
            this.filter = filter;
            this.receiver = receiver;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder(128);
            sb.append("Receiver{");
            sb.append(this.receiver);
            sb.append(" filter=");
            sb.append(this.filter);
            if (this.dead) {
                sb.append(" DEAD");
            }
            sb.append("}");
            return sb.toString();
        }
    }
}
