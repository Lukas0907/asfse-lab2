// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room;

import java.util.Iterator;
import java.util.List;
import androidx.room.migration.Migration;
import android.database.Cursor;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.sqlite.db.SimpleSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

public class RoomOpenHelper extends Callback
{
    private DatabaseConfiguration mConfiguration;
    private final Delegate mDelegate;
    private final String mIdentityHash;
    private final String mLegacyHash;
    
    public RoomOpenHelper(final DatabaseConfiguration databaseConfiguration, final Delegate delegate, final String s) {
        this(databaseConfiguration, delegate, "", s);
    }
    
    public RoomOpenHelper(final DatabaseConfiguration mConfiguration, final Delegate mDelegate, final String mIdentityHash, final String mLegacyHash) {
        super(mDelegate.version);
        this.mConfiguration = mConfiguration;
        this.mDelegate = mDelegate;
        this.mIdentityHash = mIdentityHash;
        this.mLegacyHash = mLegacyHash;
    }
    
    private void checkIdentity(final SupportSQLiteDatabase supportSQLiteDatabase) {
        if (hasRoomMasterTable(supportSQLiteDatabase)) {
            final Object o = null;
            final Cursor query = supportSQLiteDatabase.query(new SimpleSQLiteQuery("SELECT identity_hash FROM room_master_table WHERE id = 42 LIMIT 1"));
            Object string = o;
            try {
                if (query.moveToFirst()) {
                    string = query.getString(0);
                }
                query.close();
                if (this.mIdentityHash.equals(string)) {
                    return;
                }
                if (this.mLegacyHash.equals(string)) {
                    return;
                }
                throw new IllegalStateException("Room cannot verify the data integrity. Looks like you've changed schema but forgot to update the version number. You can simply fix this by increasing the version number.");
            }
            finally {
                query.close();
            }
        }
        final ValidationResult onValidateSchema = this.mDelegate.onValidateSchema(supportSQLiteDatabase);
        if (!onValidateSchema.isValid) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Pre-packaged database has an invalid schema: ");
            sb.append(onValidateSchema.expectedFoundMsg);
            throw new IllegalStateException(sb.toString());
        }
        this.mDelegate.onPostMigrate(supportSQLiteDatabase);
        this.updateIdentity(supportSQLiteDatabase);
    }
    
    private void createMasterTableIfNotExists(final SupportSQLiteDatabase supportSQLiteDatabase) {
        supportSQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    }
    
    private static boolean hasEmptySchema(SupportSQLiteDatabase query) {
        query = (SupportSQLiteDatabase)query.query("SELECT count(*) FROM sqlite_master WHERE name != 'android_metadata'");
        try {
            final boolean moveToFirst = ((Cursor)query).moveToFirst();
            boolean b = false;
            if (moveToFirst) {
                final int int1 = ((Cursor)query).getInt(0);
                b = b;
                if (int1 == 0) {
                    b = true;
                }
            }
            return b;
        }
        finally {
            ((Cursor)query).close();
        }
    }
    
    private static boolean hasRoomMasterTable(SupportSQLiteDatabase query) {
        query = (SupportSQLiteDatabase)query.query("SELECT 1 FROM sqlite_master WHERE type = 'table' AND name='room_master_table'");
        try {
            final boolean moveToFirst = ((Cursor)query).moveToFirst();
            boolean b = false;
            if (moveToFirst) {
                final int int1 = ((Cursor)query).getInt(0);
                b = b;
                if (int1 != 0) {
                    b = true;
                }
            }
            return b;
        }
        finally {
            ((Cursor)query).close();
        }
    }
    
    private void updateIdentity(final SupportSQLiteDatabase supportSQLiteDatabase) {
        this.createMasterTableIfNotExists(supportSQLiteDatabase);
        supportSQLiteDatabase.execSQL(RoomMasterTable.createInsertQuery(this.mIdentityHash));
    }
    
    @Override
    public void onConfigure(final SupportSQLiteDatabase supportSQLiteDatabase) {
        super.onConfigure(supportSQLiteDatabase);
    }
    
    @Override
    public void onCreate(final SupportSQLiteDatabase supportSQLiteDatabase) {
        final boolean hasEmptySchema = hasEmptySchema(supportSQLiteDatabase);
        this.mDelegate.createAllTables(supportSQLiteDatabase);
        if (!hasEmptySchema) {
            final ValidationResult onValidateSchema = this.mDelegate.onValidateSchema(supportSQLiteDatabase);
            if (!onValidateSchema.isValid) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Pre-packaged database has an invalid schema: ");
                sb.append(onValidateSchema.expectedFoundMsg);
                throw new IllegalStateException(sb.toString());
            }
        }
        this.updateIdentity(supportSQLiteDatabase);
        this.mDelegate.onCreate(supportSQLiteDatabase);
    }
    
    @Override
    public void onDowngrade(final SupportSQLiteDatabase supportSQLiteDatabase, final int n, final int n2) {
        this.onUpgrade(supportSQLiteDatabase, n, n2);
    }
    
    @Override
    public void onOpen(final SupportSQLiteDatabase supportSQLiteDatabase) {
        super.onOpen(supportSQLiteDatabase);
        this.checkIdentity(supportSQLiteDatabase);
        this.mDelegate.onOpen(supportSQLiteDatabase);
        this.mConfiguration = null;
    }
    
    @Override
    public void onUpgrade(final SupportSQLiteDatabase supportSQLiteDatabase, final int i, final int j) {
        final DatabaseConfiguration mConfiguration = this.mConfiguration;
        boolean b = false;
        Label_0149: {
            if (mConfiguration != null) {
                final List<Migration> migrationPath = mConfiguration.migrationContainer.findMigrationPath(i, j);
                if (migrationPath != null) {
                    this.mDelegate.onPreMigrate(supportSQLiteDatabase);
                    final Iterator<Migration> iterator = migrationPath.iterator();
                    while (iterator.hasNext()) {
                        iterator.next().migrate(supportSQLiteDatabase);
                    }
                    final ValidationResult onValidateSchema = this.mDelegate.onValidateSchema(supportSQLiteDatabase);
                    if (onValidateSchema.isValid) {
                        this.mDelegate.onPostMigrate(supportSQLiteDatabase);
                        this.updateIdentity(supportSQLiteDatabase);
                        b = true;
                        break Label_0149;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Migration didn't properly handle: ");
                    sb.append(onValidateSchema.expectedFoundMsg);
                    throw new IllegalStateException(sb.toString());
                }
            }
            b = false;
        }
        if (b) {
            return;
        }
        final DatabaseConfiguration mConfiguration2 = this.mConfiguration;
        if (mConfiguration2 != null && !mConfiguration2.isMigrationRequired(i, j)) {
            this.mDelegate.dropAllTables(supportSQLiteDatabase);
            this.mDelegate.createAllTables(supportSQLiteDatabase);
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("A migration from ");
        sb2.append(i);
        sb2.append(" to ");
        sb2.append(j);
        sb2.append(" was required but not found. Please provide the necessary Migration path via RoomDatabase.Builder.addMigration(Migration ...) or allow for destructive migrations via one of the RoomDatabase.Builder.fallbackToDestructiveMigration* methods.");
        throw new IllegalStateException(sb2.toString());
    }
    
    public abstract static class Delegate
    {
        public final int version;
        
        public Delegate(final int version) {
            this.version = version;
        }
        
        protected abstract void createAllTables(final SupportSQLiteDatabase p0);
        
        protected abstract void dropAllTables(final SupportSQLiteDatabase p0);
        
        protected abstract void onCreate(final SupportSQLiteDatabase p0);
        
        protected abstract void onOpen(final SupportSQLiteDatabase p0);
        
        protected void onPostMigrate(final SupportSQLiteDatabase supportSQLiteDatabase) {
        }
        
        protected void onPreMigrate(final SupportSQLiteDatabase supportSQLiteDatabase) {
        }
        
        protected ValidationResult onValidateSchema(final SupportSQLiteDatabase supportSQLiteDatabase) {
            this.validateMigration(supportSQLiteDatabase);
            return new ValidationResult(true, null);
        }
        
        @Deprecated
        protected void validateMigration(final SupportSQLiteDatabase supportSQLiteDatabase) {
            throw new UnsupportedOperationException("validateMigration is deprecated");
        }
    }
    
    public static class ValidationResult
    {
        public final String expectedFoundMsg;
        public final boolean isValid;
        
        public ValidationResult(final boolean isValid, final String expectedFoundMsg) {
            this.isValid = isValid;
            this.expectedFoundMsg = expectedFoundMsg;
        }
    }
}
