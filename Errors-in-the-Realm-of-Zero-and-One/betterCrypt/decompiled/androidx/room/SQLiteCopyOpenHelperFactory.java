// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room;

import java.io.File;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

class SQLiteCopyOpenHelperFactory implements Factory
{
    private final String mCopyFromAssetPath;
    private final File mCopyFromFile;
    private final Factory mDelegate;
    
    SQLiteCopyOpenHelperFactory(final String mCopyFromAssetPath, final File mCopyFromFile, final Factory mDelegate) {
        this.mCopyFromAssetPath = mCopyFromAssetPath;
        this.mCopyFromFile = mCopyFromFile;
        this.mDelegate = mDelegate;
    }
    
    @Override
    public SupportSQLiteOpenHelper create(final Configuration configuration) {
        return new SQLiteCopyOpenHelper(configuration.context, this.mCopyFromAssetPath, this.mCopyFromFile, configuration.callback.version, this.mDelegate.create(configuration));
    }
}
