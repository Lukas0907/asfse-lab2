// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room;

import android.os.Parcel;
import android.os.IBinder;
import android.os.Binder;
import android.os.RemoteException;
import android.os.IInterface;

public interface IMultiInstanceInvalidationCallback extends IInterface
{
    void onInvalidation(final String[] p0) throws RemoteException;
    
    public abstract static class Stub extends Binder implements IMultiInstanceInvalidationCallback
    {
        private static final String DESCRIPTOR = "androidx.room.IMultiInstanceInvalidationCallback";
        static final int TRANSACTION_onInvalidation = 1;
        
        public Stub() {
            this.attachInterface((IInterface)this, "androidx.room.IMultiInstanceInvalidationCallback");
        }
        
        public static IMultiInstanceInvalidationCallback asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("androidx.room.IMultiInstanceInvalidationCallback");
            if (queryLocalInterface != null && queryLocalInterface instanceof IMultiInstanceInvalidationCallback) {
                return (IMultiInstanceInvalidationCallback)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            if (n == 1) {
                parcel.enforceInterface("androidx.room.IMultiInstanceInvalidationCallback");
                this.onInvalidation(parcel.createStringArray());
                return true;
            }
            if (n != 1598968902) {
                return super.onTransact(n, parcel, parcel2, n2);
            }
            parcel2.writeString("androidx.room.IMultiInstanceInvalidationCallback");
            return true;
        }
        
        private static class Proxy implements IMultiInstanceInvalidationCallback
        {
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            public String getInterfaceDescriptor() {
                return "androidx.room.IMultiInstanceInvalidationCallback";
            }
            
            @Override
            public void onInvalidation(final String[] array) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("androidx.room.IMultiInstanceInvalidationCallback");
                    obtain.writeStringArray(array);
                    this.mRemote.transact(1, obtain, (Parcel)null, 1);
                }
                finally {
                    obtain.recycle();
                }
            }
        }
    }
}
