// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room.util;

import android.os.Build$VERSION;
import java.util.Locale;
import java.util.Collection;
import java.util.TreeMap;
import java.util.Iterator;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.List;
import android.database.Cursor;
import java.io.Serializable;
import java.util.HashMap;
import androidx.sqlite.db.SupportSQLiteDatabase;
import java.util.Collections;
import java.util.Set;
import java.util.Map;

public class TableInfo
{
    public static final int CREATED_FROM_DATABASE = 2;
    public static final int CREATED_FROM_ENTITY = 1;
    public static final int CREATED_FROM_UNKNOWN = 0;
    public final Map<String, Column> columns;
    public final Set<ForeignKey> foreignKeys;
    public final Set<Index> indices;
    public final String name;
    
    public TableInfo(final String s, final Map<String, Column> map, final Set<ForeignKey> set) {
        this(s, map, set, Collections.emptySet());
    }
    
    public TableInfo(final String name, final Map<String, Column> m, final Set<ForeignKey> s, final Set<Index> s2) {
        this.name = name;
        this.columns = Collections.unmodifiableMap((Map<? extends String, ? extends Column>)m);
        this.foreignKeys = Collections.unmodifiableSet((Set<? extends ForeignKey>)s);
        Set<Index> unmodifiableSet;
        if (s2 == null) {
            unmodifiableSet = null;
        }
        else {
            unmodifiableSet = Collections.unmodifiableSet((Set<? extends Index>)s2);
        }
        this.indices = unmodifiableSet;
    }
    
    public static TableInfo read(final SupportSQLiteDatabase supportSQLiteDatabase, final String s) {
        return new TableInfo(s, readColumns(supportSQLiteDatabase, s), readForeignKeys(supportSQLiteDatabase, s), readIndices(supportSQLiteDatabase, s));
    }
    
    private static Map<String, Column> readColumns(SupportSQLiteDatabase query, final String str) {
        while (true) {
            Serializable string = new StringBuilder();
            ((StringBuilder)string).append("PRAGMA table_info(`");
            ((StringBuilder)string).append(str);
            ((StringBuilder)string).append("`)");
            query = (SupportSQLiteDatabase)query.query(((StringBuilder)string).toString());
            final HashMap<StringBuilder, Column> hashMap = (HashMap<StringBuilder, Column>)new HashMap<String, Column>();
            while (true) {
                Label_0212: {
                    try {
                        if (((Cursor)query).getColumnCount() > 0) {
                            final int columnIndex = ((Cursor)query).getColumnIndex("name");
                            final int columnIndex2 = ((Cursor)query).getColumnIndex("type");
                            final int columnIndex3 = ((Cursor)query).getColumnIndex("notnull");
                            final int columnIndex4 = ((Cursor)query).getColumnIndex("pk");
                            final int columnIndex5 = ((Cursor)query).getColumnIndex("dflt_value");
                            while (((Cursor)query).moveToNext()) {
                                string = ((Cursor)query).getString(columnIndex);
                                final String string2 = ((Cursor)query).getString(columnIndex2);
                                if (((Cursor)query).getInt(columnIndex3) == 0) {
                                    break Label_0212;
                                }
                                final boolean b = true;
                                hashMap.put((String)string, new Column((String)string, string2, b, ((Cursor)query).getInt(columnIndex4), ((Cursor)query).getString(columnIndex5), 2));
                            }
                        }
                        return (Map<String, Column>)hashMap;
                    }
                    finally {
                        ((Cursor)query).close();
                    }
                }
                final boolean b = false;
                continue;
            }
        }
    }
    
    private static List<ForeignKeyWithSequence> readForeignKeyFieldMappings(final Cursor cursor) {
        final int columnIndex = cursor.getColumnIndex("id");
        final int columnIndex2 = cursor.getColumnIndex("seq");
        final int columnIndex3 = cursor.getColumnIndex("from");
        final int columnIndex4 = cursor.getColumnIndex("to");
        final int count = cursor.getCount();
        final ArrayList<Comparable> list = new ArrayList<Comparable>();
        for (int i = 0; i < count; ++i) {
            cursor.moveToPosition(i);
            list.add(new ForeignKeyWithSequence(cursor.getInt(columnIndex), cursor.getInt(columnIndex2), cursor.getString(columnIndex3), cursor.getString(columnIndex4)));
        }
        Collections.sort(list);
        return (List<ForeignKeyWithSequence>)list;
    }
    
    private static Set<ForeignKey> readForeignKeys(SupportSQLiteDatabase query, final String str) {
        final HashSet<ForeignKey> set = new HashSet<ForeignKey>();
        final StringBuilder sb = new StringBuilder();
        sb.append("PRAGMA foreign_key_list(`");
        sb.append(str);
        sb.append("`)");
        query = (SupportSQLiteDatabase)query.query(sb.toString());
        try {
            final int columnIndex = ((Cursor)query).getColumnIndex("id");
            final int columnIndex2 = ((Cursor)query).getColumnIndex("seq");
            final int columnIndex3 = ((Cursor)query).getColumnIndex("table");
            final int columnIndex4 = ((Cursor)query).getColumnIndex("on_delete");
            final int columnIndex5 = ((Cursor)query).getColumnIndex("on_update");
            final List<ForeignKeyWithSequence> foreignKeyFieldMappings = readForeignKeyFieldMappings((Cursor)query);
            for (int count = ((Cursor)query).getCount(), i = 0; i < count; ++i) {
                ((Cursor)query).moveToPosition(i);
                if (((Cursor)query).getInt(columnIndex2) == 0) {
                    final int int1 = ((Cursor)query).getInt(columnIndex);
                    final ArrayList<String> list = new ArrayList<String>();
                    final ArrayList<String> list2 = new ArrayList<String>();
                    for (final ForeignKeyWithSequence foreignKeyWithSequence : foreignKeyFieldMappings) {
                        if (foreignKeyWithSequence.mId == int1) {
                            list.add(foreignKeyWithSequence.mFrom);
                            list2.add(foreignKeyWithSequence.mTo);
                        }
                    }
                    set.add(new ForeignKey(((Cursor)query).getString(columnIndex3), ((Cursor)query).getString(columnIndex4), ((Cursor)query).getString(columnIndex5), list, list2));
                }
            }
            return set;
        }
        finally {
            ((Cursor)query).close();
        }
    }
    
    private static Index readIndex(SupportSQLiteDatabase query, final String str, final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("PRAGMA index_xinfo(`");
        sb.append(str);
        sb.append("`)");
        query = (SupportSQLiteDatabase)query.query(sb.toString());
        try {
            final int columnIndex = ((Cursor)query).getColumnIndex("seqno");
            final int columnIndex2 = ((Cursor)query).getColumnIndex("cid");
            final int columnIndex3 = ((Cursor)query).getColumnIndex("name");
            if (columnIndex != -1 && columnIndex2 != -1 && columnIndex3 != -1) {
                final TreeMap<Integer, String> treeMap = new TreeMap<Integer, String>();
                while (((Cursor)query).moveToNext()) {
                    if (((Cursor)query).getInt(columnIndex2) < 0) {
                        continue;
                    }
                    treeMap.put(((Cursor)query).getInt(columnIndex), ((Cursor)query).getString(columnIndex3));
                }
                final ArrayList list = new ArrayList<String>(treeMap.size());
                list.addAll((Collection<?>)treeMap.values());
                return new Index(str, b, (List<String>)list);
            }
            return null;
        }
        finally {
            ((Cursor)query).close();
        }
    }
    
    private static Set<Index> readIndices(final SupportSQLiteDatabase supportSQLiteDatabase, String query) {
    Label_0162_Outer:
        while (true) {
            Serializable s = new StringBuilder();
            ((StringBuilder)s).append("PRAGMA index_list(`");
            ((StringBuilder)s).append(query);
            ((StringBuilder)s).append("`)");
            query = (String)supportSQLiteDatabase.query(((StringBuilder)s).toString());
            while (true) {
                Label_0222: {
                    try {
                        final int columnIndex = ((Cursor)query).getColumnIndex("name");
                        final int columnIndex2 = ((Cursor)query).getColumnIndex("origin");
                        final int columnIndex3 = ((Cursor)query).getColumnIndex("unique");
                        if (columnIndex != -1 && columnIndex2 != -1 && columnIndex3 != -1) {
                            s = new HashSet<String>();
                            while (((Cursor)query).moveToNext()) {
                                if (!"c".equals(((Cursor)query).getString(columnIndex2))) {
                                    continue Label_0162_Outer;
                                }
                                Object e = ((Cursor)query).getString(columnIndex);
                                final int int1 = ((Cursor)query).getInt(columnIndex3);
                                final boolean b = true;
                                if (int1 != 1) {
                                    break Label_0222;
                                }
                                e = readIndex(supportSQLiteDatabase, (String)e, b);
                                if (e == null) {
                                    return null;
                                }
                                ((HashSet<String>)s).add((String)e);
                            }
                            return (Set<Index>)s;
                        }
                        return null;
                    }
                    finally {
                        ((Cursor)query).close();
                    }
                }
                final boolean b = false;
                continue;
            }
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (this.getClass() != o.getClass()) {
            return false;
        }
        final TableInfo tableInfo = (TableInfo)o;
        final String name = this.name;
        if (name != null) {
            if (!name.equals(tableInfo.name)) {
                return false;
            }
        }
        else if (tableInfo.name != null) {
            return false;
        }
        final Map<String, Column> columns = this.columns;
        if (columns != null) {
            if (!columns.equals(tableInfo.columns)) {
                return false;
            }
        }
        else if (tableInfo.columns != null) {
            return false;
        }
        final Set<ForeignKey> foreignKeys = this.foreignKeys;
        if (foreignKeys != null) {
            if (!foreignKeys.equals(tableInfo.foreignKeys)) {
                return false;
            }
        }
        else if (tableInfo.foreignKeys != null) {
            return false;
        }
        final Set<Index> indices = this.indices;
        if (indices != null) {
            final Set<Index> indices2 = tableInfo.indices;
            return indices2 == null || indices.equals(indices2);
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        final String name = this.name;
        int hashCode = 0;
        int hashCode2;
        if (name != null) {
            hashCode2 = name.hashCode();
        }
        else {
            hashCode2 = 0;
        }
        final Map<String, Column> columns = this.columns;
        int hashCode3;
        if (columns != null) {
            hashCode3 = columns.hashCode();
        }
        else {
            hashCode3 = 0;
        }
        final Set<ForeignKey> foreignKeys = this.foreignKeys;
        if (foreignKeys != null) {
            hashCode = foreignKeys.hashCode();
        }
        return (hashCode2 * 31 + hashCode3) * 31 + hashCode;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("TableInfo{name='");
        sb.append(this.name);
        sb.append('\'');
        sb.append(", columns=");
        sb.append(this.columns);
        sb.append(", foreignKeys=");
        sb.append(this.foreignKeys);
        sb.append(", indices=");
        sb.append(this.indices);
        sb.append('}');
        return sb.toString();
    }
    
    public static class Column
    {
        public final int affinity;
        public final String defaultValue;
        private final int mCreatedFrom;
        public final String name;
        public final boolean notNull;
        public final int primaryKeyPosition;
        public final String type;
        
        @Deprecated
        public Column(final String s, final String s2, final boolean b, final int n) {
            this(s, s2, b, n, null, 0);
        }
        
        public Column(final String name, final String type, final boolean notNull, final int primaryKeyPosition, final String defaultValue, final int mCreatedFrom) {
            this.name = name;
            this.type = type;
            this.notNull = notNull;
            this.primaryKeyPosition = primaryKeyPosition;
            this.affinity = findAffinity(type);
            this.defaultValue = defaultValue;
            this.mCreatedFrom = mCreatedFrom;
        }
        
        private static int findAffinity(String upperCase) {
            if (upperCase == null) {
                return 5;
            }
            upperCase = upperCase.toUpperCase(Locale.US);
            if (upperCase.contains("INT")) {
                return 3;
            }
            if (upperCase.contains("CHAR") || upperCase.contains("CLOB") || upperCase.contains("TEXT")) {
                return 2;
            }
            if (upperCase.contains("BLOB")) {
                return 5;
            }
            if (!upperCase.contains("REAL") && !upperCase.contains("FLOA") && !upperCase.contains("DOUB")) {
                return 1;
            }
            return 4;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null) {
                return false;
            }
            if (this.getClass() != o.getClass()) {
                return false;
            }
            final Column column = (Column)o;
            if (Build$VERSION.SDK_INT >= 20) {
                if (this.primaryKeyPosition != column.primaryKeyPosition) {
                    return false;
                }
            }
            else if (this.isPrimaryKey() != column.isPrimaryKey()) {
                return false;
            }
            if (!this.name.equals(column.name)) {
                return false;
            }
            if (this.notNull != column.notNull) {
                return false;
            }
            if (this.mCreatedFrom == 1 && column.mCreatedFrom == 2) {
                final String defaultValue = this.defaultValue;
                if (defaultValue != null && !defaultValue.equals(column.defaultValue)) {
                    return false;
                }
            }
            if (this.mCreatedFrom == 2 && column.mCreatedFrom == 1) {
                final String defaultValue2 = column.defaultValue;
                if (defaultValue2 != null && !defaultValue2.equals(this.defaultValue)) {
                    return false;
                }
            }
            final int mCreatedFrom = this.mCreatedFrom;
            if (mCreatedFrom != 0 && mCreatedFrom == column.mCreatedFrom) {
                final String defaultValue3 = this.defaultValue;
                if (defaultValue3 != null) {
                    if (!defaultValue3.equals(column.defaultValue)) {
                        return false;
                    }
                }
                else if (column.defaultValue != null) {
                    return false;
                }
            }
            return this.affinity == column.affinity;
        }
        
        @Override
        public int hashCode() {
            final int hashCode = this.name.hashCode();
            final int affinity = this.affinity;
            int n;
            if (this.notNull) {
                n = 1231;
            }
            else {
                n = 1237;
            }
            return ((hashCode * 31 + affinity) * 31 + n) * 31 + this.primaryKeyPosition;
        }
        
        public boolean isPrimaryKey() {
            return this.primaryKeyPosition > 0;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Column{name='");
            sb.append(this.name);
            sb.append('\'');
            sb.append(", type='");
            sb.append(this.type);
            sb.append('\'');
            sb.append(", affinity='");
            sb.append(this.affinity);
            sb.append('\'');
            sb.append(", notNull=");
            sb.append(this.notNull);
            sb.append(", primaryKeyPosition=");
            sb.append(this.primaryKeyPosition);
            sb.append(", defaultValue='");
            sb.append(this.defaultValue);
            sb.append('\'');
            sb.append('}');
            return sb.toString();
        }
    }
    
    public static class ForeignKey
    {
        public final List<String> columnNames;
        public final String onDelete;
        public final String onUpdate;
        public final List<String> referenceColumnNames;
        public final String referenceTable;
        
        public ForeignKey(final String referenceTable, final String onDelete, final String onUpdate, final List<String> list, final List<String> list2) {
            this.referenceTable = referenceTable;
            this.onDelete = onDelete;
            this.onUpdate = onUpdate;
            this.columnNames = Collections.unmodifiableList((List<? extends String>)list);
            this.referenceColumnNames = Collections.unmodifiableList((List<? extends String>)list2);
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null) {
                return false;
            }
            if (this.getClass() != o.getClass()) {
                return false;
            }
            final ForeignKey foreignKey = (ForeignKey)o;
            return this.referenceTable.equals(foreignKey.referenceTable) && this.onDelete.equals(foreignKey.onDelete) && this.onUpdate.equals(foreignKey.onUpdate) && this.columnNames.equals(foreignKey.columnNames) && this.referenceColumnNames.equals(foreignKey.referenceColumnNames);
        }
        
        @Override
        public int hashCode() {
            return (((this.referenceTable.hashCode() * 31 + this.onDelete.hashCode()) * 31 + this.onUpdate.hashCode()) * 31 + this.columnNames.hashCode()) * 31 + this.referenceColumnNames.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("ForeignKey{referenceTable='");
            sb.append(this.referenceTable);
            sb.append('\'');
            sb.append(", onDelete='");
            sb.append(this.onDelete);
            sb.append('\'');
            sb.append(", onUpdate='");
            sb.append(this.onUpdate);
            sb.append('\'');
            sb.append(", columnNames=");
            sb.append(this.columnNames);
            sb.append(", referenceColumnNames=");
            sb.append(this.referenceColumnNames);
            sb.append('}');
            return sb.toString();
        }
    }
    
    static class ForeignKeyWithSequence implements Comparable<ForeignKeyWithSequence>
    {
        final String mFrom;
        final int mId;
        final int mSequence;
        final String mTo;
        
        ForeignKeyWithSequence(final int mId, final int mSequence, final String mFrom, final String mTo) {
            this.mId = mId;
            this.mSequence = mSequence;
            this.mFrom = mFrom;
            this.mTo = mTo;
        }
        
        @Override
        public int compareTo(final ForeignKeyWithSequence foreignKeyWithSequence) {
            int n;
            if ((n = this.mId - foreignKeyWithSequence.mId) == 0) {
                n = this.mSequence - foreignKeyWithSequence.mSequence;
            }
            return n;
        }
    }
    
    public static class Index
    {
        public static final String DEFAULT_PREFIX = "index_";
        public final List<String> columns;
        public final String name;
        public final boolean unique;
        
        public Index(final String name, final boolean unique, final List<String> columns) {
            this.name = name;
            this.unique = unique;
            this.columns = columns;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null) {
                return false;
            }
            if (this.getClass() != o.getClass()) {
                return false;
            }
            final Index index = (Index)o;
            if (this.unique != index.unique) {
                return false;
            }
            if (!this.columns.equals(index.columns)) {
                return false;
            }
            if (this.name.startsWith("index_")) {
                return index.name.startsWith("index_");
            }
            return this.name.equals(index.name);
        }
        
        @Override
        public int hashCode() {
            throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:780)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e1expr(TypeTransformer.java:496)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:713)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e2expr(TypeTransformer.java:632)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:716)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e2expr(TypeTransformer.java:629)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:716)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e2expr(TypeTransformer.java:629)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:716)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s1stmt(TypeTransformer.java:810)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:840)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Index{name='");
            sb.append(this.name);
            sb.append('\'');
            sb.append(", unique=");
            sb.append(this.unique);
            sb.append(", columns=");
            sb.append(this.columns);
            sb.append('}');
            return sb.toString();
        }
    }
}
