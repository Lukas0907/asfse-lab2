// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room.util;

import java.util.Iterator;
import java.nio.channels.spi.AbstractInterruptibleChannel;
import java.nio.channels.FileChannel;
import java.io.IOException;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.io.File;
import android.database.AbstractWindowedCursor;
import android.database.Cursor;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.room.RoomDatabase;
import java.util.ArrayList;
import androidx.sqlite.db.SupportSQLiteDatabase;
import android.os.Build$VERSION;
import android.os.CancellationSignal;

public class DBUtil
{
    private DBUtil() {
    }
    
    public static CancellationSignal createCancellationSignal() {
        if (Build$VERSION.SDK_INT >= 16) {
            return new CancellationSignal();
        }
        return null;
    }
    
    public static void dropFtsSyncTriggers(final SupportSQLiteDatabase supportSQLiteDatabase) {
        final ArrayList<String> list = new ArrayList<String>();
        Object o = supportSQLiteDatabase.query("SELECT name FROM sqlite_master WHERE type = 'trigger'");
        try {
            while (((Cursor)o).moveToNext()) {
                list.add(((Cursor)o).getString(0));
            }
            ((Cursor)o).close();
            o = list.iterator();
            while (((Iterator)o).hasNext()) {
                final String str = ((Iterator<String>)o).next();
                if (str.startsWith("room_fts_content_sync_")) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("DROP TRIGGER IF EXISTS ");
                    sb.append(str);
                    supportSQLiteDatabase.execSQL(sb.toString());
                }
            }
        }
        finally {
            ((Cursor)o).close();
        }
    }
    
    @Deprecated
    public static Cursor query(final RoomDatabase roomDatabase, final SupportSQLiteQuery supportSQLiteQuery, final boolean b) {
        return query(roomDatabase, supportSQLiteQuery, b, null);
    }
    
    public static Cursor query(final RoomDatabase roomDatabase, final SupportSQLiteQuery supportSQLiteQuery, final boolean b, final CancellationSignal cancellationSignal) {
        Cursor cursor2;
        final Cursor cursor = cursor2 = roomDatabase.query(supportSQLiteQuery, cancellationSignal);
        if (b) {
            cursor2 = cursor;
            if (cursor instanceof AbstractWindowedCursor) {
                final AbstractWindowedCursor abstractWindowedCursor = (AbstractWindowedCursor)cursor;
                final int count = abstractWindowedCursor.getCount();
                int numRows;
                if (abstractWindowedCursor.hasWindow()) {
                    numRows = abstractWindowedCursor.getWindow().getNumRows();
                }
                else {
                    numRows = count;
                }
                if (Build$VERSION.SDK_INT >= 23) {
                    cursor2 = cursor;
                    if (numRows >= count) {
                        return cursor2;
                    }
                }
                cursor2 = CursorUtil.copyAndClose((Cursor)abstractWindowedCursor);
            }
        }
        return cursor2;
    }
    
    public static int readVersion(final File file) throws IOException {
        AbstractInterruptibleChannel abstractInterruptibleChannel;
        try {
            final ByteBuffer allocate = ByteBuffer.allocate(4);
            final FileChannel channel = new FileInputStream(file).getChannel();
            try {
                channel.tryLock(60L, 4L, true);
                channel.position(60L);
                if (channel.read(allocate) == 4) {
                    allocate.rewind();
                    final int int1 = allocate.getInt();
                    if (channel != null) {
                        channel.close();
                    }
                    return int1;
                }
                throw new IOException("Bad database header, unable to read 4 bytes at offset 60");
            }
            finally {}
        }
        finally {
            abstractInterruptibleChannel = null;
        }
        if (abstractInterruptibleChannel != null) {
            abstractInterruptibleChannel.close();
        }
    }
}
