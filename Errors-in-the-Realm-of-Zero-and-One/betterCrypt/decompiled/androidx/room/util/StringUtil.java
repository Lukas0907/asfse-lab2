// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room.util;

import android.util.Log;
import java.util.StringTokenizer;
import java.util.ArrayList;
import java.util.List;

public class StringUtil
{
    public static final String[] EMPTY_STRING_ARRAY;
    
    static {
        EMPTY_STRING_ARRAY = new String[0];
    }
    
    private StringUtil() {
    }
    
    public static void appendPlaceholders(final StringBuilder sb, final int n) {
        for (int i = 0; i < n; ++i) {
            sb.append("?");
            if (i < n - 1) {
                sb.append(",");
            }
        }
    }
    
    public static String joinIntoString(final List<Integer> list) {
        if (list == null) {
            return null;
        }
        final int size = list.size();
        if (size == 0) {
            return "";
        }
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; ++i) {
            sb.append(Integer.toString(list.get(i)));
            if (i < size - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }
    
    public static StringBuilder newStringBuilder() {
        return new StringBuilder();
    }
    
    public static List<Integer> splitToIntList(String str) {
        if (str == null) {
            return null;
        }
        final ArrayList<Integer> list = new ArrayList<Integer>();
        str = (String)new StringTokenizer(str, ",");
        while (((StringTokenizer)str).hasMoreElements()) {
            final String nextToken = ((StringTokenizer)str).nextToken();
            try {
                list.add(Integer.parseInt(nextToken));
            }
            catch (NumberFormatException ex) {
                Log.e("ROOM", "Malformed integer list", (Throwable)ex);
            }
        }
        return list;
    }
}
