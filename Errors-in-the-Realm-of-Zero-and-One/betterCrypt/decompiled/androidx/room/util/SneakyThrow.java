// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room.util;

public class SneakyThrow
{
    private SneakyThrow() {
    }
    
    public static void reThrow(final Exception ex) {
        sneakyThrow(ex);
    }
    
    private static <E extends Throwable> void sneakyThrow(final Throwable t) throws E, Throwable {
        throw t;
    }
}
