// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room.util;

import android.database.MatrixCursor;
import android.database.Cursor;

public class CursorUtil
{
    private CursorUtil() {
    }
    
    public static Cursor copyAndClose(final Cursor cursor) {
        while (true) {
            while (true) {
                int n = 0;
                Label_0185: {
                    Object[] array = null;
                    Label_0180: {
                        try {
                            final MatrixCursor matrixCursor = new MatrixCursor(cursor.getColumnNames(), cursor.getCount());
                            while (cursor.moveToNext()) {
                                array = new Object[cursor.getColumnCount()];
                                n = 0;
                                if (n < cursor.getColumnCount()) {
                                    final int type = cursor.getType(n);
                                    if (type == 0) {
                                        break Label_0180;
                                    }
                                    if (type == 1) {
                                        array[n] = cursor.getLong(n);
                                        break Label_0185;
                                    }
                                    if (type == 2) {
                                        array[n] = cursor.getDouble(n);
                                        break Label_0185;
                                    }
                                    if (type == 3) {
                                        array[n] = cursor.getString(n);
                                        break Label_0185;
                                    }
                                    if (type == 4) {
                                        array[n] = cursor.getBlob(n);
                                        break Label_0185;
                                    }
                                    throw new IllegalStateException();
                                }
                                else {
                                    matrixCursor.addRow(array);
                                }
                            }
                            return (Cursor)matrixCursor;
                        }
                        finally {
                            cursor.close();
                        }
                    }
                    array[n] = null;
                }
                ++n;
                continue;
            }
        }
    }
    
    public static int getColumnIndex(final Cursor cursor, final String str) {
        final int columnIndex = cursor.getColumnIndex(str);
        if (columnIndex >= 0) {
            return columnIndex;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("`");
        sb.append(str);
        sb.append("`");
        return cursor.getColumnIndex(sb.toString());
    }
    
    public static int getColumnIndexOrThrow(final Cursor cursor, final String str) {
        final int columnIndex = cursor.getColumnIndex(str);
        if (columnIndex >= 0) {
            return columnIndex;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("`");
        sb.append(str);
        sb.append("`");
        return cursor.getColumnIndexOrThrow(sb.toString());
    }
}
