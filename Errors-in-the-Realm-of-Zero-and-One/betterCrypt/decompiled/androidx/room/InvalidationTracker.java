// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import android.content.Context;
import androidx.lifecycle.LiveData;
import java.util.concurrent.Callable;
import java.util.Collection;
import java.util.Collections;
import java.util.Locale;
import java.util.Iterator;
import androidx.sqlite.db.SupportSQLiteDatabase;
import java.util.concurrent.locks.Lock;
import android.util.Log;
import android.database.sqlite.SQLiteException;
import android.database.Cursor;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.sqlite.db.SimpleSQLiteQuery;
import java.util.HashSet;
import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import androidx.arch.core.internal.SafeIterableMap;
import androidx.sqlite.db.SupportSQLiteStatement;

public class InvalidationTracker
{
    private static final String CREATE_TRACKING_TABLE_SQL = "CREATE TEMP TABLE room_table_modification_log(table_id INTEGER PRIMARY KEY, invalidated INTEGER NOT NULL DEFAULT 0)";
    private static final String INVALIDATED_COLUMN_NAME = "invalidated";
    static final String RESET_UPDATED_TABLES_SQL = "UPDATE room_table_modification_log SET invalidated = 0 WHERE invalidated = 1 ";
    static final String SELECT_UPDATED_TABLES_SQL = "SELECT * FROM room_table_modification_log WHERE invalidated = 1;";
    private static final String TABLE_ID_COLUMN_NAME = "table_id";
    private static final String[] TRIGGERS;
    private static final String UPDATE_TABLE_NAME = "room_table_modification_log";
    volatile SupportSQLiteStatement mCleanupStatement;
    final RoomDatabase mDatabase;
    private volatile boolean mInitialized;
    private final InvalidationLiveDataContainer mInvalidationLiveDataContainer;
    private MultiInstanceInvalidationClient mMultiInstanceInvalidationClient;
    private ObservedTableTracker mObservedTableTracker;
    final SafeIterableMap<Observer, ObserverWrapper> mObserverMap;
    AtomicBoolean mPendingRefresh;
    Runnable mRefreshRunnable;
    final HashMap<String, Integer> mTableIdLookup;
    final String[] mTableNames;
    private Map<String, Set<String>> mViewTables;
    
    static {
        TRIGGERS = new String[] { "UPDATE", "DELETE", "INSERT" };
    }
    
    public InvalidationTracker(final RoomDatabase mDatabase, final Map<String, String> map, final Map<String, Set<String>> mViewTables, final String... array) {
        int i = 0;
        this.mPendingRefresh = new AtomicBoolean(false);
        this.mInitialized = false;
        this.mObserverMap = new SafeIterableMap<Observer, ObserverWrapper>();
        this.mRefreshRunnable = new Runnable() {
            private Set<Integer> checkUpdatedTable() {
                final HashSet<Integer> set = new HashSet<Integer>();
                final Cursor query = InvalidationTracker.this.mDatabase.query(new SimpleSQLiteQuery("SELECT * FROM room_table_modification_log WHERE invalidated = 1;"));
                try {
                    while (query.moveToNext()) {
                        set.add(query.getInt(0));
                    }
                    query.close();
                    if (!set.isEmpty()) {
                        InvalidationTracker.this.mCleanupStatement.executeUpdateDelete();
                    }
                    return set;
                }
                finally {
                    query.close();
                }
            }
            
            @Override
            public void run() {
                final Lock closeLock = InvalidationTracker.this.mDatabase.getCloseLock();
                final Set<Integer> set = null;
                Set<Integer> checkUpdatedTable = null;
                Set<Integer> set2 = set;
                Set<Integer> set4 = null;
                Label_0277: {
                    try {
                        try {
                            closeLock.lock();
                            set2 = set;
                            if (!InvalidationTracker.this.ensureInitialization()) {
                                closeLock.unlock();
                                return;
                            }
                            set2 = set;
                            if (!InvalidationTracker.this.mPendingRefresh.compareAndSet(true, false)) {
                                closeLock.unlock();
                                return;
                            }
                            set2 = set;
                            if (InvalidationTracker.this.mDatabase.inTransaction()) {
                                closeLock.unlock();
                                return;
                            }
                            set2 = set;
                            if (InvalidationTracker.this.mDatabase.mWriteAheadLoggingEnabled) {
                                set2 = set;
                                final SupportSQLiteDatabase writableDatabase = InvalidationTracker.this.mDatabase.getOpenHelper().getWritableDatabase();
                                set2 = set;
                                writableDatabase.beginTransaction();
                                try {
                                    final Set<Integer> set3 = checkUpdatedTable = this.checkUpdatedTable();
                                    writableDatabase.setTransactionSuccessful();
                                    set2 = set3;
                                    writableDatabase.endTransaction();
                                    break Label_0277;
                                }
                                finally {
                                    set2 = checkUpdatedTable;
                                    writableDatabase.endTransaction();
                                    set2 = checkUpdatedTable;
                                }
                            }
                            set2 = set;
                            this.checkUpdatedTable();
                            break Label_0277;
                        }
                        finally {}
                    }
                    catch (SQLiteException ex) {
                        set4 = set2;
                        set2 = (Set<Integer>)ex;
                    }
                    catch (IllegalStateException ex2) {}
                    Log.e("ROOM", "Cannot run invalidation tracker. Is the db closed?", (Throwable)set2);
                }
                closeLock.unlock();
                if (set4 != null && !set4.isEmpty()) {
                    synchronized (InvalidationTracker.this.mObserverMap) {
                        final Iterator<Map.Entry<Observer, ObserverWrapper>> iterator = InvalidationTracker.this.mObserverMap.iterator();
                        while (iterator.hasNext()) {
                            ((Map.Entry<Observer, ObserverWrapper>)iterator.next()).getValue().notifyByTableInvalidStatus(set4);
                        }
                        return;
                    }
                }
                return;
                closeLock.unlock();
            }
        };
        this.mDatabase = mDatabase;
        this.mObservedTableTracker = new ObservedTableTracker(array.length);
        this.mTableIdLookup = new HashMap<String, Integer>();
        this.mViewTables = mViewTables;
        this.mInvalidationLiveDataContainer = new InvalidationLiveDataContainer(this.mDatabase);
        final int length = array.length;
        this.mTableNames = new String[length];
        while (i < length) {
            final String lowerCase = array[i].toLowerCase(Locale.US);
            this.mTableIdLookup.put(lowerCase, i);
            final String s = map.get(array[i]);
            if (s != null) {
                this.mTableNames[i] = s.toLowerCase(Locale.US);
            }
            else {
                this.mTableNames[i] = lowerCase;
            }
            ++i;
        }
        for (final Map.Entry<String, String> entry : map.entrySet()) {
            final String lowerCase2 = entry.getValue().toLowerCase(Locale.US);
            if (this.mTableIdLookup.containsKey(lowerCase2)) {
                final String lowerCase3 = entry.getKey().toLowerCase(Locale.US);
                final HashMap<String, Integer> mTableIdLookup = this.mTableIdLookup;
                mTableIdLookup.put(lowerCase3, mTableIdLookup.get(lowerCase2));
            }
        }
    }
    
    public InvalidationTracker(final RoomDatabase roomDatabase, final String... array) {
        this(roomDatabase, new HashMap<String, String>(), Collections.emptyMap(), array);
    }
    
    private static void appendTriggerName(final StringBuilder sb, final String str, final String str2) {
        sb.append("`");
        sb.append("room_table_modification_trigger_");
        sb.append(str);
        sb.append("_");
        sb.append(str2);
        sb.append("`");
    }
    
    private String[] resolveViews(final String[] array) {
        final HashSet<String> set = new HashSet<String>();
        for (int length = array.length, i = 0; i < length; ++i) {
            final String s = array[i];
            final String lowerCase = s.toLowerCase(Locale.US);
            if (this.mViewTables.containsKey(lowerCase)) {
                set.addAll((Collection<?>)this.mViewTables.get(lowerCase));
            }
            else {
                set.add(s);
            }
        }
        return set.toArray(new String[set.size()]);
    }
    
    private void startTrackingTable(final SupportSQLiteDatabase supportSQLiteDatabase, final int n) {
        final StringBuilder sb = new StringBuilder();
        sb.append("INSERT OR IGNORE INTO room_table_modification_log VALUES(");
        sb.append(n);
        sb.append(", 0)");
        supportSQLiteDatabase.execSQL(sb.toString());
        final String str = this.mTableNames[n];
        final StringBuilder sb2 = new StringBuilder();
        final String[] triggers = InvalidationTracker.TRIGGERS;
        for (int length = triggers.length, i = 0; i < length; ++i) {
            final String str2 = triggers[i];
            sb2.setLength(0);
            sb2.append("CREATE TEMP TRIGGER IF NOT EXISTS ");
            appendTriggerName(sb2, str, str2);
            sb2.append(" AFTER ");
            sb2.append(str2);
            sb2.append(" ON `");
            sb2.append(str);
            sb2.append("` BEGIN UPDATE ");
            sb2.append("room_table_modification_log");
            sb2.append(" SET ");
            sb2.append("invalidated");
            sb2.append(" = 1");
            sb2.append(" WHERE ");
            sb2.append("table_id");
            sb2.append(" = ");
            sb2.append(n);
            sb2.append(" AND ");
            sb2.append("invalidated");
            sb2.append(" = 0");
            sb2.append("; END");
            supportSQLiteDatabase.execSQL(sb2.toString());
        }
    }
    
    private void stopTrackingTable(final SupportSQLiteDatabase supportSQLiteDatabase, int i) {
        final String s = this.mTableNames[i];
        final StringBuilder sb = new StringBuilder();
        final String[] triggers = InvalidationTracker.TRIGGERS;
        int length;
        String s2;
        for (length = triggers.length, i = 0; i < length; ++i) {
            s2 = triggers[i];
            sb.setLength(0);
            sb.append("DROP TRIGGER IF EXISTS ");
            appendTriggerName(sb, s, s2);
            supportSQLiteDatabase.execSQL(sb.toString());
        }
    }
    
    private String[] validateAndResolveTableNames(final String[] array) {
        final String[] resolveViews = this.resolveViews(array);
        for (int length = resolveViews.length, i = 0; i < length; ++i) {
            final String str = resolveViews[i];
            if (!this.mTableIdLookup.containsKey(str.toLowerCase(Locale.US))) {
                final StringBuilder sb = new StringBuilder();
                sb.append("There is no table with name ");
                sb.append(str);
                throw new IllegalArgumentException(sb.toString());
            }
        }
        return resolveViews;
    }
    
    public void addObserver(final Observer observer) {
        final String[] resolveViews = this.resolveViews(observer.mTables);
        final int[] array = new int[resolveViews.length];
        for (int length = resolveViews.length, i = 0; i < length; ++i) {
            final Integer n = this.mTableIdLookup.get(resolveViews[i].toLowerCase(Locale.US));
            if (n == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("There is no table with name ");
                sb.append(resolveViews[i]);
                throw new IllegalArgumentException(sb.toString());
            }
            array[i] = n;
        }
        final ObserverWrapper observerWrapper = new ObserverWrapper(observer, array, resolveViews);
        synchronized (this.mObserverMap) {
            final ObserverWrapper observerWrapper2 = this.mObserverMap.putIfAbsent(observer, observerWrapper);
            // monitorexit(this.mObserverMap)
            if (observerWrapper2 == null && this.mObservedTableTracker.onAdded(array)) {
                this.syncTriggers();
            }
        }
    }
    
    public void addWeakObserver(final Observer observer) {
        this.addObserver((Observer)new WeakObserver(this, observer));
    }
    
    @Deprecated
    public <T> LiveData<T> createLiveData(final String[] array, final Callable<T> callable) {
        return this.createLiveData(array, false, callable);
    }
    
    public <T> LiveData<T> createLiveData(final String[] array, final boolean b, final Callable<T> callable) {
        return this.mInvalidationLiveDataContainer.create(this.validateAndResolveTableNames(array), b, callable);
    }
    
    boolean ensureInitialization() {
        if (!this.mDatabase.isOpen()) {
            return false;
        }
        if (!this.mInitialized) {
            this.mDatabase.getOpenHelper().getWritableDatabase();
        }
        if (!this.mInitialized) {
            Log.e("ROOM", "database is not initialized even though it is open");
            return false;
        }
        return true;
    }
    
    void internalInit(final SupportSQLiteDatabase supportSQLiteDatabase) {
        synchronized (this) {
            if (this.mInitialized) {
                Log.e("ROOM", "Invalidation tracker is initialized twice :/.");
                return;
            }
            supportSQLiteDatabase.execSQL("PRAGMA temp_store = MEMORY;");
            supportSQLiteDatabase.execSQL("PRAGMA recursive_triggers='ON';");
            supportSQLiteDatabase.execSQL("CREATE TEMP TABLE room_table_modification_log(table_id INTEGER PRIMARY KEY, invalidated INTEGER NOT NULL DEFAULT 0)");
            this.syncTriggers(supportSQLiteDatabase);
            this.mCleanupStatement = supportSQLiteDatabase.compileStatement("UPDATE room_table_modification_log SET invalidated = 0 WHERE invalidated = 1 ");
            this.mInitialized = true;
        }
    }
    
    public void notifyObserversByTableNames(final String... array) {
        synchronized (this.mObserverMap) {
            for (final Map.Entry<Observer, ObserverWrapper> entry : this.mObserverMap) {
                if (!entry.getKey().isRemote()) {
                    entry.getValue().notifyByTableNames(array);
                }
            }
        }
    }
    
    public void refreshVersionsAsync() {
        if (this.mPendingRefresh.compareAndSet(false, true)) {
            this.mDatabase.getQueryExecutor().execute(this.mRefreshRunnable);
        }
    }
    
    public void refreshVersionsSync() {
        this.syncTriggers();
        this.mRefreshRunnable.run();
    }
    
    public void removeObserver(final Observer observer) {
        synchronized (this.mObserverMap) {
            final ObserverWrapper observerWrapper = this.mObserverMap.remove(observer);
            // monitorexit(this.mObserverMap)
            if (observerWrapper != null && this.mObservedTableTracker.onRemoved(observerWrapper.mTableIds)) {
                this.syncTriggers();
            }
        }
    }
    
    void startMultiInstanceInvalidation(final Context context, final String s) {
        this.mMultiInstanceInvalidationClient = new MultiInstanceInvalidationClient(context, s, this, this.mDatabase.getQueryExecutor());
    }
    
    void stopMultiInstanceInvalidation() {
        final MultiInstanceInvalidationClient mMultiInstanceInvalidationClient = this.mMultiInstanceInvalidationClient;
        if (mMultiInstanceInvalidationClient != null) {
            mMultiInstanceInvalidationClient.stop();
            this.mMultiInstanceInvalidationClient = null;
        }
    }
    
    void syncTriggers() {
        if (!this.mDatabase.isOpen()) {
            return;
        }
        this.syncTriggers(this.mDatabase.getOpenHelper().getWritableDatabase());
    }
    
    void syncTriggers(final SupportSQLiteDatabase ex) {
        if (((SupportSQLiteDatabase)ex).inTransaction()) {
            return;
        }
        Label_0010: {
            break Label_0010;
        Label_0060_Outer:
            while (true) {
                while (true) {
                    int n = 0;
                    Label_0171: {
                        try {
                            while (true) {
                                final Lock closeLock = this.mDatabase.getCloseLock();
                                closeLock.lock();
                                try {
                                    final int[] tablesToSync = this.mObservedTableTracker.getTablesToSync();
                                    if (tablesToSync == null) {
                                        return;
                                    }
                                    final int length = tablesToSync.length;
                                    ((SupportSQLiteDatabase)ex).beginTransaction();
                                    n = 0;
                                    Label_0104: {
                                        if (n >= length) {
                                            break Label_0104;
                                        }
                                        final int n2 = tablesToSync[n];
                                        Label_0095: {
                                            if (n2 == 1) {
                                                break Label_0095;
                                            }
                                            if (n2 != 2) {
                                                break Label_0171;
                                            }
                                            try {
                                                this.stopTrackingTable((SupportSQLiteDatabase)ex, n);
                                                break Label_0171;
                                                ((SupportSQLiteDatabase)ex).setTransactionSuccessful();
                                                ((SupportSQLiteDatabase)ex).endTransaction();
                                                this.mObservedTableTracker.onSyncCompleted();
                                                continue Label_0060_Outer;
                                                this.startTrackingTable((SupportSQLiteDatabase)ex, n);
                                            }
                                            finally {
                                                ((SupportSQLiteDatabase)ex).endTransaction();
                                            }
                                        }
                                    }
                                }
                                finally {
                                    closeLock.unlock();
                                }
                            }
                        }
                        catch (SQLiteException ex) {}
                        catch (IllegalStateException ex2) {}
                        break;
                    }
                    ++n;
                    continue;
                }
            }
        }
        Log.e("ROOM", "Cannot run invalidation tracker. Is the db closed?", (Throwable)ex);
    }
    
    static class ObservedTableTracker
    {
        static final int ADD = 1;
        static final int NO_OP = 0;
        static final int REMOVE = 2;
        boolean mNeedsSync;
        boolean mPendingSync;
        final long[] mTableObservers;
        final int[] mTriggerStateChanges;
        final boolean[] mTriggerStates;
        
        ObservedTableTracker(final int n) {
            this.mTableObservers = new long[n];
            this.mTriggerStates = new boolean[n];
            this.mTriggerStateChanges = new int[n];
            Arrays.fill(this.mTableObservers, 0L);
            Arrays.fill(this.mTriggerStates, false);
        }
        
        int[] getTablesToSync() {
        Label_0083_Outer:
            while (true) {
                while (true) {
                    int n = 0;
                    int n2 = 0;
                    Label_0138: {
                    Label_0136:
                        while (true) {
                            synchronized (this) {
                                if (!this.mNeedsSync || this.mPendingSync) {
                                    return null;
                                }
                                final int length = this.mTableObservers.length;
                                n = 0;
                                while (true) {
                                    n2 = 1;
                                    if (n >= length) {
                                        this.mPendingSync = true;
                                        this.mNeedsSync = false;
                                        return this.mTriggerStateChanges;
                                    }
                                    if (this.mTableObservers[n] <= 0L) {
                                        break;
                                    }
                                    final boolean b = true;
                                    if (b != this.mTriggerStates[n]) {
                                        final int[] mTriggerStateChanges = this.mTriggerStateChanges;
                                        if (b) {
                                            break Label_0138;
                                        }
                                        break Label_0136;
                                    }
                                    else {
                                        this.mTriggerStateChanges[n] = 0;
                                        this.mTriggerStates[n] = b;
                                        ++n;
                                    }
                                }
                            }
                            final boolean b = false;
                            continue Label_0083_Outer;
                        }
                        n2 = 2;
                    }
                    final Throwable t;
                    t[n] = n2;
                    continue;
                }
            }
        }
        
        boolean onAdded(final int... array) {
            while (true) {
                while (true) {
                    int n;
                    synchronized (this) {
                        final int length = array.length;
                        n = 0;
                        boolean b = false;
                        if (n >= length) {
                            return b;
                        }
                        final int n2 = array[n];
                        final long n3 = this.mTableObservers[n2];
                        this.mTableObservers[n2] = 1L + n3;
                        if (n3 == 0L) {
                            this.mNeedsSync = true;
                            b = true;
                        }
                    }
                    ++n;
                    continue;
                }
            }
        }
        
        boolean onRemoved(final int... array) {
            while (true) {
                while (true) {
                    int n;
                    synchronized (this) {
                        final int length = array.length;
                        n = 0;
                        boolean b = false;
                        if (n >= length) {
                            return b;
                        }
                        final int n2 = array[n];
                        final long n3 = this.mTableObservers[n2];
                        this.mTableObservers[n2] = n3 - 1L;
                        if (n3 == 1L) {
                            this.mNeedsSync = true;
                            b = true;
                        }
                    }
                    ++n;
                    continue;
                }
            }
        }
        
        void onSyncCompleted() {
            synchronized (this) {
                this.mPendingSync = false;
            }
        }
    }
    
    public abstract static class Observer
    {
        final String[] mTables;
        
        protected Observer(final String s, final String... original) {
            (this.mTables = Arrays.copyOf(original, original.length + 1))[original.length] = s;
        }
        
        public Observer(final String[] original) {
            this.mTables = Arrays.copyOf(original, original.length);
        }
        
        boolean isRemote() {
            return false;
        }
        
        public abstract void onInvalidated(final Set<String> p0);
    }
    
    static class ObserverWrapper
    {
        final Observer mObserver;
        private final Set<String> mSingleTableSet;
        final int[] mTableIds;
        private final String[] mTableNames;
        
        ObserverWrapper(final Observer mObserver, final int[] mTableIds, final String[] mTableNames) {
            this.mObserver = mObserver;
            this.mTableIds = mTableIds;
            this.mTableNames = mTableNames;
            if (mTableIds.length == 1) {
                final HashSet<String> s = new HashSet<String>();
                s.add(this.mTableNames[0]);
                this.mSingleTableSet = (Set<String>)Collections.unmodifiableSet((Set<?>)s);
                return;
            }
            this.mSingleTableSet = null;
        }
        
        void notifyByTableInvalidStatus(final Set<Integer> set) {
            final int length = this.mTableIds.length;
            Set<String> set2 = null;
            Set<String> mSingleTableSet;
            for (int i = 0; i < length; ++i, set2 = mSingleTableSet) {
                mSingleTableSet = set2;
                if (set.contains(this.mTableIds[i])) {
                    if (length == 1) {
                        mSingleTableSet = this.mSingleTableSet;
                    }
                    else {
                        if ((mSingleTableSet = set2) == null) {
                            mSingleTableSet = new HashSet<String>(length);
                        }
                        mSingleTableSet.add(this.mTableNames[i]);
                    }
                }
            }
            if (set2 != null) {
                this.mObserver.onInvalidated(set2);
            }
        }
        
        void notifyByTableNames(final String[] array) {
            final int length = this.mTableNames.length;
            final Set<String> set = null;
            Set<String> mSingleTableSet;
            if (length == 1) {
                final int length2 = array.length;
                int n = 0;
                while (true) {
                    mSingleTableSet = set;
                    if (n >= length2) {
                        break;
                    }
                    if (array[n].equalsIgnoreCase(this.mTableNames[0])) {
                        mSingleTableSet = this.mSingleTableSet;
                        break;
                    }
                    ++n;
                }
            }
            else {
                final HashSet<String> set2 = new HashSet<String>();
                for (int length3 = array.length, i = 0; i < length3; ++i) {
                    final String anotherString = array[i];
                    final String[] mTableNames = this.mTableNames;
                    for (int length4 = mTableNames.length, j = 0; j < length4; ++j) {
                        final String e = mTableNames[j];
                        if (e.equalsIgnoreCase(anotherString)) {
                            set2.add(e);
                            break;
                        }
                    }
                }
                mSingleTableSet = set;
                if (set2.size() > 0) {
                    mSingleTableSet = set2;
                }
            }
            if (mSingleTableSet != null) {
                this.mObserver.onInvalidated(mSingleTableSet);
            }
        }
    }
    
    static class WeakObserver extends Observer
    {
        final WeakReference<Observer> mDelegateRef;
        final InvalidationTracker mTracker;
        
        WeakObserver(final InvalidationTracker mTracker, final Observer referent) {
            super(referent.mTables);
            this.mTracker = mTracker;
            this.mDelegateRef = new WeakReference<Observer>(referent);
        }
        
        @Override
        public void onInvalidated(final Set<String> set) {
            final Observer observer = this.mDelegateRef.get();
            if (observer == null) {
                this.mTracker.removeObserver((Observer)this);
                return;
            }
            observer.onInvalidated(set);
        }
    }
}
