// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room;

import java.util.ArrayDeque;
import java.util.concurrent.Executor;

class TransactionExecutor implements Executor
{
    private Runnable mActive;
    private final Executor mExecutor;
    private final ArrayDeque<Runnable> mTasks;
    
    TransactionExecutor(final Executor mExecutor) {
        this.mTasks = new ArrayDeque<Runnable>();
        this.mExecutor = mExecutor;
    }
    
    @Override
    public void execute(final Runnable runnable) {
        synchronized (this) {
            this.mTasks.offer(new Runnable() {
                @Override
                public void run() {
                    try {
                        runnable.run();
                    }
                    finally {
                        TransactionExecutor.this.scheduleNext();
                    }
                }
            });
            if (this.mActive == null) {
                this.scheduleNext();
            }
        }
    }
    
    void scheduleNext() {
        synchronized (this) {
            final Runnable mActive = this.mTasks.poll();
            this.mActive = mActive;
            if (mActive != null) {
                this.mExecutor.execute(this.mActive);
            }
        }
    }
}
