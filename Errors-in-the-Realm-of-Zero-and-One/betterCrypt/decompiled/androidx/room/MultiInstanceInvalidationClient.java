// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room;

import android.content.Intent;
import java.util.Set;
import android.os.RemoteException;
import android.util.Log;
import android.os.IBinder;
import android.content.ComponentName;
import java.util.concurrent.atomic.AtomicBoolean;
import android.content.ServiceConnection;
import java.util.concurrent.Executor;
import android.content.Context;

class MultiInstanceInvalidationClient
{
    final Context mAppContext;
    final IMultiInstanceInvalidationCallback mCallback;
    int mClientId;
    final Executor mExecutor;
    final InvalidationTracker mInvalidationTracker;
    final String mName;
    final InvalidationTracker.Observer mObserver;
    final Runnable mRemoveObserverRunnable;
    IMultiInstanceInvalidationService mService;
    final ServiceConnection mServiceConnection;
    final Runnable mSetUpRunnable;
    final AtomicBoolean mStopped;
    private final Runnable mTearDownRunnable;
    
    MultiInstanceInvalidationClient(final Context context, final String mName, final InvalidationTracker mInvalidationTracker, final Executor mExecutor) {
        this.mCallback = new IMultiInstanceInvalidationCallback.Stub() {
            public void onInvalidation(final String[] array) {
                MultiInstanceInvalidationClient.this.mExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        MultiInstanceInvalidationClient.this.mInvalidationTracker.notifyObserversByTableNames(array);
                    }
                });
            }
        };
        this.mStopped = new AtomicBoolean(false);
        this.mServiceConnection = (ServiceConnection)new ServiceConnection() {
            public void onServiceConnected(final ComponentName componentName, final IBinder binder) {
                MultiInstanceInvalidationClient.this.mService = IMultiInstanceInvalidationService.Stub.asInterface(binder);
                MultiInstanceInvalidationClient.this.mExecutor.execute(MultiInstanceInvalidationClient.this.mSetUpRunnable);
            }
            
            public void onServiceDisconnected(final ComponentName componentName) {
                MultiInstanceInvalidationClient.this.mExecutor.execute(MultiInstanceInvalidationClient.this.mRemoveObserverRunnable);
                MultiInstanceInvalidationClient.this.mService = null;
            }
        };
        this.mSetUpRunnable = new Runnable() {
            @Override
            public void run() {
                try {
                    final IMultiInstanceInvalidationService mService = MultiInstanceInvalidationClient.this.mService;
                    if (mService != null) {
                        MultiInstanceInvalidationClient.this.mClientId = mService.registerCallback(MultiInstanceInvalidationClient.this.mCallback, MultiInstanceInvalidationClient.this.mName);
                        MultiInstanceInvalidationClient.this.mInvalidationTracker.addObserver(MultiInstanceInvalidationClient.this.mObserver);
                    }
                }
                catch (RemoteException ex) {
                    Log.w("ROOM", "Cannot register multi-instance invalidation callback", (Throwable)ex);
                }
            }
        };
        this.mRemoveObserverRunnable = new Runnable() {
            @Override
            public void run() {
                MultiInstanceInvalidationClient.this.mInvalidationTracker.removeObserver(MultiInstanceInvalidationClient.this.mObserver);
            }
        };
        this.mTearDownRunnable = new Runnable() {
            @Override
            public void run() {
                MultiInstanceInvalidationClient.this.mInvalidationTracker.removeObserver(MultiInstanceInvalidationClient.this.mObserver);
                try {
                    final IMultiInstanceInvalidationService mService = MultiInstanceInvalidationClient.this.mService;
                    if (mService != null) {
                        mService.unregisterCallback(MultiInstanceInvalidationClient.this.mCallback, MultiInstanceInvalidationClient.this.mClientId);
                    }
                }
                catch (RemoteException ex) {
                    Log.w("ROOM", "Cannot unregister multi-instance invalidation callback", (Throwable)ex);
                }
                MultiInstanceInvalidationClient.this.mAppContext.unbindService(MultiInstanceInvalidationClient.this.mServiceConnection);
            }
        };
        this.mAppContext = context.getApplicationContext();
        this.mName = mName;
        this.mInvalidationTracker = mInvalidationTracker;
        this.mExecutor = mExecutor;
        this.mObserver = new InvalidationTracker.Observer(mInvalidationTracker.mTableNames) {
            @Override
            boolean isRemote() {
                return true;
            }
            
            @Override
            public void onInvalidated(final Set<String> set) {
                if (MultiInstanceInvalidationClient.this.mStopped.get()) {
                    return;
                }
                try {
                    final IMultiInstanceInvalidationService mService = MultiInstanceInvalidationClient.this.mService;
                    if (mService != null) {
                        mService.broadcastInvalidation(MultiInstanceInvalidationClient.this.mClientId, set.toArray(new String[0]));
                    }
                }
                catch (RemoteException ex) {
                    Log.w("ROOM", "Cannot broadcast invalidation", (Throwable)ex);
                }
            }
        };
        this.mAppContext.bindService(new Intent(this.mAppContext, (Class)MultiInstanceInvalidationService.class), this.mServiceConnection, 1);
    }
    
    void stop() {
        if (this.mStopped.compareAndSet(false, true)) {
            this.mExecutor.execute(this.mTearDownRunnable);
        }
    }
}
