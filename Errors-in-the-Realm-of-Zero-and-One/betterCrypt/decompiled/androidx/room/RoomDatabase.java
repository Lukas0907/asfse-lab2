// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room;

import java.util.Collections;
import android.util.Log;
import java.util.TreeMap;
import java.util.HashMap;
import android.app.ActivityManager;
import java.util.Iterator;
import androidx.sqlite.db.framework.FrameworkSQLiteOpenHelperFactory;
import androidx.arch.core.executor.ArchTaskExecutor;
import java.util.HashSet;
import androidx.room.migration.Migration;
import java.util.Set;
import java.io.File;
import android.content.Context;
import java.util.ArrayList;
import androidx.room.util.SneakyThrow;
import java.util.concurrent.Callable;
import androidx.sqlite.db.SimpleSQLiteQuery;
import android.os.CancellationSignal;
import android.database.Cursor;
import androidx.sqlite.db.SupportSQLiteQuery;
import android.os.Build$VERSION;
import java.util.concurrent.locks.Lock;
import androidx.sqlite.db.SupportSQLiteStatement;
import android.os.Looper;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteDatabase;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.List;
import java.util.Map;

public abstract class RoomDatabase
{
    private static final String DB_IMPL_SUFFIX = "_Impl";
    public static final int MAX_BIND_PARAMETER_CNT = 999;
    private boolean mAllowMainThreadQueries;
    private final Map<String, Object> mBackingFieldMap;
    @Deprecated
    protected List<Callback> mCallbacks;
    private final ReentrantReadWriteLock mCloseLock;
    @Deprecated
    protected volatile SupportSQLiteDatabase mDatabase;
    private final InvalidationTracker mInvalidationTracker;
    private SupportSQLiteOpenHelper mOpenHelper;
    private Executor mQueryExecutor;
    private final ThreadLocal<Integer> mSuspendingTransactionId;
    private Executor mTransactionExecutor;
    boolean mWriteAheadLoggingEnabled;
    
    public RoomDatabase() {
        this.mCloseLock = new ReentrantReadWriteLock();
        this.mSuspendingTransactionId = new ThreadLocal<Integer>();
        this.mBackingFieldMap = new ConcurrentHashMap<String, Object>();
        this.mInvalidationTracker = this.createInvalidationTracker();
    }
    
    private static boolean isMainThread() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }
    
    public void assertNotMainThread() {
        if (this.mAllowMainThreadQueries) {
            return;
        }
        if (!isMainThread()) {
            return;
        }
        throw new IllegalStateException("Cannot access database on the main thread since it may potentially lock the UI for a long period of time.");
    }
    
    public void assertNotSuspendingTransaction() {
        if (this.inTransaction()) {
            return;
        }
        if (this.mSuspendingTransactionId.get() == null) {
            return;
        }
        throw new IllegalStateException("Cannot access database on a different coroutine context inherited from a suspending transaction.");
    }
    
    @Deprecated
    public void beginTransaction() {
        this.assertNotMainThread();
        final SupportSQLiteDatabase writableDatabase = this.mOpenHelper.getWritableDatabase();
        this.mInvalidationTracker.syncTriggers(writableDatabase);
        writableDatabase.beginTransaction();
    }
    
    public abstract void clearAllTables();
    
    public void close() {
        if (this.isOpen()) {
            final ReentrantReadWriteLock.WriteLock writeLock = this.mCloseLock.writeLock();
            try {
                writeLock.lock();
                this.mInvalidationTracker.stopMultiInstanceInvalidation();
                this.mOpenHelper.close();
            }
            finally {
                writeLock.unlock();
            }
        }
    }
    
    public SupportSQLiteStatement compileStatement(final String s) {
        this.assertNotMainThread();
        this.assertNotSuspendingTransaction();
        return this.mOpenHelper.getWritableDatabase().compileStatement(s);
    }
    
    protected abstract InvalidationTracker createInvalidationTracker();
    
    protected abstract SupportSQLiteOpenHelper createOpenHelper(final DatabaseConfiguration p0);
    
    @Deprecated
    public void endTransaction() {
        this.mOpenHelper.getWritableDatabase().endTransaction();
        if (!this.inTransaction()) {
            this.mInvalidationTracker.refreshVersionsAsync();
        }
    }
    
    Map<String, Object> getBackingFieldMap() {
        return this.mBackingFieldMap;
    }
    
    Lock getCloseLock() {
        return this.mCloseLock.readLock();
    }
    
    public InvalidationTracker getInvalidationTracker() {
        return this.mInvalidationTracker;
    }
    
    public SupportSQLiteOpenHelper getOpenHelper() {
        return this.mOpenHelper;
    }
    
    public Executor getQueryExecutor() {
        return this.mQueryExecutor;
    }
    
    ThreadLocal<Integer> getSuspendingTransactionId() {
        return this.mSuspendingTransactionId;
    }
    
    public Executor getTransactionExecutor() {
        return this.mTransactionExecutor;
    }
    
    public boolean inTransaction() {
        return this.mOpenHelper.getWritableDatabase().inTransaction();
    }
    
    public void init(final DatabaseConfiguration databaseConfiguration) {
        this.mOpenHelper = this.createOpenHelper(databaseConfiguration);
        final SupportSQLiteOpenHelper mOpenHelper = this.mOpenHelper;
        if (mOpenHelper instanceof SQLiteCopyOpenHelper) {
            ((SQLiteCopyOpenHelper)mOpenHelper).setDatabaseConfiguration(databaseConfiguration);
        }
        final int sdk_INT = Build$VERSION.SDK_INT;
        boolean b = false;
        final boolean b2 = false;
        if (sdk_INT >= 16) {
            b = b2;
            if (databaseConfiguration.journalMode == JournalMode.WRITE_AHEAD_LOGGING) {
                b = true;
            }
            this.mOpenHelper.setWriteAheadLoggingEnabled(b);
        }
        this.mCallbacks = databaseConfiguration.callbacks;
        this.mQueryExecutor = databaseConfiguration.queryExecutor;
        this.mTransactionExecutor = new TransactionExecutor(databaseConfiguration.transactionExecutor);
        this.mAllowMainThreadQueries = databaseConfiguration.allowMainThreadQueries;
        this.mWriteAheadLoggingEnabled = b;
        if (databaseConfiguration.multiInstanceInvalidation) {
            this.mInvalidationTracker.startMultiInstanceInvalidation(databaseConfiguration.context, databaseConfiguration.name);
        }
    }
    
    protected void internalInitInvalidationTracker(final SupportSQLiteDatabase supportSQLiteDatabase) {
        this.mInvalidationTracker.internalInit(supportSQLiteDatabase);
    }
    
    public boolean isOpen() {
        final SupportSQLiteDatabase mDatabase = this.mDatabase;
        return mDatabase != null && mDatabase.isOpen();
    }
    
    public Cursor query(final SupportSQLiteQuery supportSQLiteQuery) {
        return this.query(supportSQLiteQuery, null);
    }
    
    public Cursor query(final SupportSQLiteQuery supportSQLiteQuery, final CancellationSignal cancellationSignal) {
        this.assertNotMainThread();
        this.assertNotSuspendingTransaction();
        if (cancellationSignal != null && Build$VERSION.SDK_INT >= 16) {
            return this.mOpenHelper.getWritableDatabase().query(supportSQLiteQuery, cancellationSignal);
        }
        return this.mOpenHelper.getWritableDatabase().query(supportSQLiteQuery);
    }
    
    public Cursor query(final String s, final Object[] array) {
        return this.mOpenHelper.getWritableDatabase().query(new SimpleSQLiteQuery(s, array));
    }
    
    public <V> V runInTransaction(final Callable<V> callable) {
        this.beginTransaction();
        try {
            try {
                final V call = callable.call();
                this.setTransactionSuccessful();
                this.endTransaction();
                return call;
            }
            finally {}
        }
        catch (Exception ex) {
            SneakyThrow.reThrow(ex);
            this.endTransaction();
            return null;
        }
        catch (RuntimeException ex2) {
            throw ex2;
        }
        this.endTransaction();
    }
    
    public void runInTransaction(final Runnable runnable) {
        this.beginTransaction();
        try {
            runnable.run();
            this.setTransactionSuccessful();
        }
        finally {
            this.endTransaction();
        }
    }
    
    @Deprecated
    public void setTransactionSuccessful() {
        this.mOpenHelper.getWritableDatabase().setTransactionSuccessful();
    }
    
    public static class Builder<T extends RoomDatabase>
    {
        private boolean mAllowDestructiveMigrationOnDowngrade;
        private boolean mAllowMainThreadQueries;
        private ArrayList<Callback> mCallbacks;
        private final Context mContext;
        private String mCopyFromAssetPath;
        private File mCopyFromFile;
        private final Class<T> mDatabaseClass;
        private SupportSQLiteOpenHelper.Factory mFactory;
        private JournalMode mJournalMode;
        private final MigrationContainer mMigrationContainer;
        private Set<Integer> mMigrationStartAndEndVersions;
        private Set<Integer> mMigrationsNotRequiredFrom;
        private boolean mMultiInstanceInvalidation;
        private final String mName;
        private Executor mQueryExecutor;
        private boolean mRequireMigration;
        private Executor mTransactionExecutor;
        
        Builder(final Context mContext, final Class<T> mDatabaseClass, final String mName) {
            this.mContext = mContext;
            this.mDatabaseClass = mDatabaseClass;
            this.mName = mName;
            this.mJournalMode = JournalMode.AUTOMATIC;
            this.mRequireMigration = true;
            this.mMigrationContainer = new MigrationContainer();
        }
        
        public Builder<T> addCallback(final Callback e) {
            if (this.mCallbacks == null) {
                this.mCallbacks = new ArrayList<Callback>();
            }
            this.mCallbacks.add(e);
            return this;
        }
        
        public Builder<T> addMigrations(final Migration... array) {
            if (this.mMigrationStartAndEndVersions == null) {
                this.mMigrationStartAndEndVersions = new HashSet<Integer>();
            }
            for (int length = array.length, i = 0; i < length; ++i) {
                final Migration migration = array[i];
                this.mMigrationStartAndEndVersions.add(migration.startVersion);
                this.mMigrationStartAndEndVersions.add(migration.endVersion);
            }
            this.mMigrationContainer.addMigrations(array);
            return this;
        }
        
        public Builder<T> allowMainThreadQueries() {
            this.mAllowMainThreadQueries = true;
            return this;
        }
        
        public T build() {
            if (this.mContext == null) {
                throw new IllegalArgumentException("Cannot provide null context for the database.");
            }
            if (this.mDatabaseClass != null) {
                if (this.mQueryExecutor == null && this.mTransactionExecutor == null) {
                    final Executor ioThreadExecutor = ArchTaskExecutor.getIOThreadExecutor();
                    this.mTransactionExecutor = ioThreadExecutor;
                    this.mQueryExecutor = ioThreadExecutor;
                }
                else {
                    final Executor mQueryExecutor = this.mQueryExecutor;
                    if (mQueryExecutor != null && this.mTransactionExecutor == null) {
                        this.mTransactionExecutor = mQueryExecutor;
                    }
                    else if (this.mQueryExecutor == null) {
                        final Executor mTransactionExecutor = this.mTransactionExecutor;
                        if (mTransactionExecutor != null) {
                            this.mQueryExecutor = mTransactionExecutor;
                        }
                    }
                }
                final Set<Integer> mMigrationStartAndEndVersions = this.mMigrationStartAndEndVersions;
                if (mMigrationStartAndEndVersions != null && this.mMigrationsNotRequiredFrom != null) {
                    for (final Integer obj : mMigrationStartAndEndVersions) {
                        if (!this.mMigrationsNotRequiredFrom.contains(obj)) {
                            continue;
                        }
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Inconsistency detected. A Migration was supplied to addMigration(Migration... migrations) that has a start or end version equal to a start version supplied to fallbackToDestructiveMigrationFrom(int... startVersions). Start version: ");
                        sb.append(obj);
                        throw new IllegalArgumentException(sb.toString());
                    }
                }
                if (this.mFactory == null) {
                    this.mFactory = new FrameworkSQLiteOpenHelperFactory();
                }
                if (this.mCopyFromAssetPath != null || this.mCopyFromFile != null) {
                    if (this.mName == null) {
                        throw new IllegalArgumentException("Cannot create from asset or file for an in-memory database.");
                    }
                    if (this.mCopyFromAssetPath != null && this.mCopyFromFile != null) {
                        throw new IllegalArgumentException("Both createFromAsset() and createFromFile() was called on this Builder but the database can only be created using one of the two configurations.");
                    }
                    this.mFactory = new SQLiteCopyOpenHelperFactory(this.mCopyFromAssetPath, this.mCopyFromFile, this.mFactory);
                }
                final Context mContext = this.mContext;
                final DatabaseConfiguration databaseConfiguration = new DatabaseConfiguration(mContext, this.mName, this.mFactory, this.mMigrationContainer, this.mCallbacks, this.mAllowMainThreadQueries, this.mJournalMode.resolve(mContext), this.mQueryExecutor, this.mTransactionExecutor, this.mMultiInstanceInvalidation, this.mRequireMigration, this.mAllowDestructiveMigrationOnDowngrade, this.mMigrationsNotRequiredFrom, this.mCopyFromAssetPath, this.mCopyFromFile);
                final RoomDatabase roomDatabase = Room.getGeneratedImplementation(this.mDatabaseClass, "_Impl");
                roomDatabase.init(databaseConfiguration);
                return (T)roomDatabase;
            }
            throw new IllegalArgumentException("Must provide an abstract class that extends RoomDatabase");
        }
        
        public Builder<T> createFromAsset(final String mCopyFromAssetPath) {
            this.mCopyFromAssetPath = mCopyFromAssetPath;
            return this;
        }
        
        public Builder<T> createFromFile(final File mCopyFromFile) {
            this.mCopyFromFile = mCopyFromFile;
            return this;
        }
        
        public Builder<T> enableMultiInstanceInvalidation() {
            this.mMultiInstanceInvalidation = (this.mName != null);
            return this;
        }
        
        public Builder<T> fallbackToDestructiveMigration() {
            this.mRequireMigration = false;
            this.mAllowDestructiveMigrationOnDowngrade = true;
            return this;
        }
        
        public Builder<T> fallbackToDestructiveMigrationFrom(final int... array) {
            if (this.mMigrationsNotRequiredFrom == null) {
                this.mMigrationsNotRequiredFrom = new HashSet<Integer>(array.length);
            }
            for (int length = array.length, i = 0; i < length; ++i) {
                this.mMigrationsNotRequiredFrom.add(array[i]);
            }
            return this;
        }
        
        public Builder<T> fallbackToDestructiveMigrationOnDowngrade() {
            this.mRequireMigration = true;
            this.mAllowDestructiveMigrationOnDowngrade = true;
            return this;
        }
        
        public Builder<T> openHelperFactory(final SupportSQLiteOpenHelper.Factory mFactory) {
            this.mFactory = mFactory;
            return this;
        }
        
        public Builder<T> setJournalMode(final JournalMode mJournalMode) {
            this.mJournalMode = mJournalMode;
            return this;
        }
        
        public Builder<T> setQueryExecutor(final Executor mQueryExecutor) {
            this.mQueryExecutor = mQueryExecutor;
            return this;
        }
        
        public Builder<T> setTransactionExecutor(final Executor mTransactionExecutor) {
            this.mTransactionExecutor = mTransactionExecutor;
            return this;
        }
    }
    
    public abstract static class Callback
    {
        public void onCreate(final SupportSQLiteDatabase supportSQLiteDatabase) {
        }
        
        public void onDestructiveMigration(final SupportSQLiteDatabase supportSQLiteDatabase) {
        }
        
        public void onOpen(final SupportSQLiteDatabase supportSQLiteDatabase) {
        }
    }
    
    public enum JournalMode
    {
        AUTOMATIC, 
        TRUNCATE, 
        WRITE_AHEAD_LOGGING;
        
        private static boolean isLowRamDevice(final ActivityManager activityManager) {
            return Build$VERSION.SDK_INT >= 19 && activityManager.isLowRamDevice();
        }
        
        JournalMode resolve(final Context context) {
            if (this != JournalMode.AUTOMATIC) {
                return this;
            }
            if (Build$VERSION.SDK_INT >= 16) {
                final ActivityManager activityManager = (ActivityManager)context.getSystemService("activity");
                if (activityManager != null && !isLowRamDevice(activityManager)) {
                    return JournalMode.WRITE_AHEAD_LOGGING;
                }
            }
            return JournalMode.TRUNCATE;
        }
    }
    
    public static class MigrationContainer
    {
        private HashMap<Integer, TreeMap<Integer, Migration>> mMigrations;
        
        public MigrationContainer() {
            this.mMigrations = new HashMap<Integer, TreeMap<Integer, Migration>>();
        }
        
        private void addMigration(final Migration migration) {
            final int startVersion = migration.startVersion;
            final int endVersion = migration.endVersion;
            TreeMap<Integer, Migration> value;
            if ((value = this.mMigrations.get(startVersion)) == null) {
                value = new TreeMap<Integer, Migration>();
                this.mMigrations.put(startVersion, value);
            }
            final Migration obj = value.get(endVersion);
            if (obj != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Overriding migration ");
                sb.append(obj);
                sb.append(" with ");
                sb.append(migration);
                Log.w("ROOM", sb.toString());
            }
            value.put(endVersion, migration);
        }
        
        private List<Migration> findUpMigrationPath(final List<Migration> list, final boolean b, int i, final int n) {
            int j;
        Label_0197:
            do {
                if (b) {
                    if (i >= n) {
                        return list;
                    }
                }
                else if (i <= n) {
                    return list;
                }
                final TreeMap<Integer, Migration> treeMap = this.mMigrations.get(i);
                if (treeMap == null) {
                    return null;
                }
                Set<Integer> set;
                if (b) {
                    set = treeMap.descendingKeySet();
                }
                else {
                    set = treeMap.keySet();
                }
                final Iterator<Integer> iterator = set.iterator();
                int k;
                int n2;
                int intValue;
                do {
                    final boolean hasNext = iterator.hasNext();
                    n2 = 1;
                    final int n3 = 0;
                    if (!hasNext) {
                        j = 0;
                        continue Label_0197;
                    }
                    intValue = iterator.next();
                    if (b) {
                        k = n3;
                        if (intValue > n) {
                            continue;
                        }
                        k = n3;
                        if (intValue <= i) {
                            continue;
                        }
                    }
                    else {
                        k = n3;
                        if (intValue < n) {
                            continue;
                        }
                        k = n3;
                        if (intValue >= i) {
                            continue;
                        }
                    }
                    k = 1;
                } while (k == 0);
                list.add(treeMap.get(intValue));
                i = intValue;
                j = n2;
            } while (j != 0);
            return null;
        }
        
        public void addMigrations(final Migration... array) {
            for (int length = array.length, i = 0; i < length; ++i) {
                this.addMigration(array[i]);
            }
        }
        
        public List<Migration> findMigrationPath(final int n, final int n2) {
            if (n == n2) {
                return Collections.emptyList();
            }
            return this.findUpMigrationPath(new ArrayList<Migration>(), n2 > n, n, n2);
        }
    }
}
