// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room;

import android.os.IBinder;
import android.content.Intent;
import android.os.RemoteException;
import android.util.Log;
import android.os.IInterface;
import java.util.HashMap;
import android.os.RemoteCallbackList;
import android.app.Service;

public class MultiInstanceInvalidationService extends Service
{
    private final IMultiInstanceInvalidationService.Stub mBinder;
    final RemoteCallbackList<IMultiInstanceInvalidationCallback> mCallbackList;
    final HashMap<Integer, String> mClientNames;
    int mMaxClientId;
    
    public MultiInstanceInvalidationService() {
        this.mMaxClientId = 0;
        this.mClientNames = new HashMap<Integer, String>();
        this.mCallbackList = new RemoteCallbackList<IMultiInstanceInvalidationCallback>() {
            public void onCallbackDied(final IMultiInstanceInvalidationCallback multiInstanceInvalidationCallback, final Object o) {
                MultiInstanceInvalidationService.this.mClientNames.remove((int)o);
            }
        };
        this.mBinder = new IMultiInstanceInvalidationService.Stub() {
            public void broadcastInvalidation(final int i, final String[] array) {
                synchronized (MultiInstanceInvalidationService.this.mCallbackList) {
                    final String s = MultiInstanceInvalidationService.this.mClientNames.get(i);
                    if (s == null) {
                        Log.w("ROOM", "Remote invalidation client ID not registered");
                        return;
                    }
                    final int beginBroadcast = MultiInstanceInvalidationService.this.mCallbackList.beginBroadcast();
                    int j = 0;
                    while (j < beginBroadcast) {
                        try {
                            final int intValue = (int)MultiInstanceInvalidationService.this.mCallbackList.getBroadcastCookie(j);
                            final String anObject = MultiInstanceInvalidationService.this.mClientNames.get(intValue);
                            if (i != intValue) {
                                if (s.equals(anObject)) {
                                    try {
                                        ((IMultiInstanceInvalidationCallback)MultiInstanceInvalidationService.this.mCallbackList.getBroadcastItem(j)).onInvalidation(array);
                                    }
                                    catch (RemoteException ex) {
                                        Log.w("ROOM", "Error invoking a remote callback", (Throwable)ex);
                                    }
                                }
                            }
                            ++j;
                            continue;
                        }
                        finally {
                            MultiInstanceInvalidationService.this.mCallbackList.finishBroadcast();
                        }
                        break;
                    }
                    MultiInstanceInvalidationService.this.mCallbackList.finishBroadcast();
                }
            }
            
            public int registerCallback(final IMultiInstanceInvalidationCallback multiInstanceInvalidationCallback, final String value) {
                if (value == null) {
                    return 0;
                }
                synchronized (MultiInstanceInvalidationService.this.mCallbackList) {
                    final MultiInstanceInvalidationService this$0 = MultiInstanceInvalidationService.this;
                    final int i = this$0.mMaxClientId + 1;
                    this$0.mMaxClientId = i;
                    if (MultiInstanceInvalidationService.this.mCallbackList.register((IInterface)multiInstanceInvalidationCallback, (Object)i)) {
                        MultiInstanceInvalidationService.this.mClientNames.put(i, value);
                        return i;
                    }
                    final MultiInstanceInvalidationService this$2 = MultiInstanceInvalidationService.this;
                    --this$2.mMaxClientId;
                    return 0;
                }
            }
            
            public void unregisterCallback(final IMultiInstanceInvalidationCallback multiInstanceInvalidationCallback, final int i) {
                synchronized (MultiInstanceInvalidationService.this.mCallbackList) {
                    MultiInstanceInvalidationService.this.mCallbackList.unregister((IInterface)multiInstanceInvalidationCallback);
                    MultiInstanceInvalidationService.this.mClientNames.remove(i);
                }
            }
        };
    }
    
    public IBinder onBind(final Intent intent) {
        return (IBinder)this.mBinder;
    }
}
