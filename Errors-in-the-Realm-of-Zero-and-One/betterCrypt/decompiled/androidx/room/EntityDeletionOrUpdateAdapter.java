// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room;

import java.util.Iterator;
import androidx.sqlite.db.SupportSQLiteStatement;

public abstract class EntityDeletionOrUpdateAdapter<T> extends SharedSQLiteStatement
{
    public EntityDeletionOrUpdateAdapter(final RoomDatabase roomDatabase) {
        super(roomDatabase);
    }
    
    protected abstract void bind(final SupportSQLiteStatement p0, final T p1);
    
    @Override
    protected abstract String createQuery();
    
    public final int handle(final T t) {
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            this.bind(acquire, t);
            return acquire.executeUpdateDelete();
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final int handleMultiple(final Iterable<? extends T> iterable) {
        final SupportSQLiteStatement acquire = this.acquire();
        int n = 0;
        try {
            final Iterator<? extends T> iterator = iterable.iterator();
            while (iterator.hasNext()) {
                this.bind(acquire, iterator.next());
                n += acquire.executeUpdateDelete();
            }
            return n;
        }
        finally {
            this.release(acquire);
        }
    }
    
    public final int handleMultiple(final T[] array) {
        final SupportSQLiteStatement acquire = this.acquire();
        try {
            final int length = array.length;
            int i = 0;
            int n = 0;
            while (i < length) {
                this.bind(acquire, array[i]);
                n += acquire.executeUpdateDelete();
                ++i;
            }
            return n;
        }
        finally {
            this.release(acquire);
        }
    }
}
