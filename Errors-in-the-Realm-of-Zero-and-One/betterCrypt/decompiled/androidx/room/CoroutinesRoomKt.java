// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room;

import java.util.concurrent.Executor;
import java.util.Map;
import kotlin.TypeCastException;
import kotlinx.coroutines.ExecutorsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CoroutineDispatcher;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028@X\u0080\u0004¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004\"\u0018\u0010\u0005\u001a\u00020\u0001*\u00020\u00028@X\u0080\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0004¨\u0006\u0007" }, d2 = { "queryDispatcher", "Lkotlinx/coroutines/CoroutineDispatcher;", "Landroidx/room/RoomDatabase;", "getQueryDispatcher", "(Landroidx/room/RoomDatabase;)Lkotlinx/coroutines/CoroutineDispatcher;", "transactionDispatcher", "getTransactionDispatcher", "room-ktx_release" }, k = 2, mv = { 1, 1, 15 })
public final class CoroutinesRoomKt
{
    public static final CoroutineDispatcher getQueryDispatcher(final RoomDatabase roomDatabase) {
        Intrinsics.checkParameterIsNotNull(roomDatabase, "$this$queryDispatcher");
        final Map<String, Object> backingFieldMap = roomDatabase.getBackingFieldMap();
        Intrinsics.checkExpressionValueIsNotNull(backingFieldMap, "backingFieldMap");
        Object o;
        if ((o = backingFieldMap.get("QueryDispatcher")) == null) {
            final Executor queryExecutor = roomDatabase.getQueryExecutor();
            Intrinsics.checkExpressionValueIsNotNull(queryExecutor, "queryExecutor");
            o = ExecutorsKt.from(queryExecutor);
            backingFieldMap.put("QueryDispatcher", o);
        }
        if (o != null) {
            return (CoroutineDispatcher)o;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.CoroutineDispatcher");
    }
    
    public static final CoroutineDispatcher getTransactionDispatcher(final RoomDatabase roomDatabase) {
        Intrinsics.checkParameterIsNotNull(roomDatabase, "$this$transactionDispatcher");
        final Map<String, Object> backingFieldMap = roomDatabase.getBackingFieldMap();
        Intrinsics.checkExpressionValueIsNotNull(backingFieldMap, "backingFieldMap");
        Object o;
        if ((o = backingFieldMap.get("TransactionDispatcher")) == null) {
            final Executor transactionExecutor = roomDatabase.getTransactionExecutor();
            Intrinsics.checkExpressionValueIsNotNull(transactionExecutor, "transactionExecutor");
            o = ExecutorsKt.from(transactionExecutor);
            backingFieldMap.put("TransactionDispatcher", o);
        }
        if (o != null) {
            return (CoroutineDispatcher)o;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlinx.coroutines.CoroutineDispatcher");
    }
}
