// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room;

import androidx.sqlite.db.SupportSQLiteDatabase;
import android.util.Log;
import androidx.room.util.DBUtil;
import androidx.room.util.CopyLock;
import java.nio.channels.ReadableByteChannel;
import java.io.IOException;
import androidx.room.util.FileUtil;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.nio.channels.Channels;
import java.io.File;
import android.content.Context;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

class SQLiteCopyOpenHelper implements SupportSQLiteOpenHelper
{
    private final Context mContext;
    private final String mCopyFromAssetPath;
    private final File mCopyFromFile;
    private DatabaseConfiguration mDatabaseConfiguration;
    private final int mDatabaseVersion;
    private final SupportSQLiteOpenHelper mDelegate;
    private boolean mVerified;
    
    SQLiteCopyOpenHelper(final Context mContext, final String mCopyFromAssetPath, final File mCopyFromFile, final int mDatabaseVersion, final SupportSQLiteOpenHelper mDelegate) {
        this.mContext = mContext;
        this.mCopyFromAssetPath = mCopyFromAssetPath;
        this.mCopyFromFile = mCopyFromFile;
        this.mDatabaseVersion = mDatabaseVersion;
        this.mDelegate = mDelegate;
    }
    
    private void copyDatabaseFile(final File dest) throws IOException {
        ReadableByteChannel readableByteChannel;
        if (this.mCopyFromAssetPath != null) {
            readableByteChannel = Channels.newChannel(this.mContext.getAssets().open(this.mCopyFromAssetPath));
        }
        else {
            final File mCopyFromFile = this.mCopyFromFile;
            if (mCopyFromFile == null) {
                throw new IllegalStateException("copyFromAssetPath and copyFromFile == null!");
            }
            readableByteChannel = new FileInputStream(mCopyFromFile).getChannel();
        }
        final File tempFile = File.createTempFile("room-copy-helper", ".tmp", this.mContext.getCacheDir());
        tempFile.deleteOnExit();
        FileUtil.copy(readableByteChannel, new FileOutputStream(tempFile).getChannel());
        final File parentFile = dest.getParentFile();
        if (parentFile != null && !parentFile.exists() && !parentFile.mkdirs()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to create directories for ");
            sb.append(dest.getAbsolutePath());
            throw new IOException(sb.toString());
        }
        if (tempFile.renameTo(dest)) {
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Failed to move intermediate file (");
        sb2.append(tempFile.getAbsolutePath());
        sb2.append(") to destination (");
        sb2.append(dest.getAbsolutePath());
        sb2.append(").");
        throw new IOException(sb2.toString());
    }
    
    private void verifyDatabaseFile() {
        final String databaseName = this.getDatabaseName();
        final File databasePath = this.mContext.getDatabasePath(databaseName);
        final DatabaseConfiguration mDatabaseConfiguration = this.mDatabaseConfiguration;
        final CopyLock copyLock = new CopyLock(databaseName, this.mContext.getFilesDir(), mDatabaseConfiguration == null || mDatabaseConfiguration.multiInstanceInvalidation);
        try {
            copyLock.lock();
            if (!databasePath.exists()) {
                try {
                    this.copyDatabaseFile(databasePath);
                    return;
                }
                catch (IOException cause) {
                    throw new RuntimeException("Unable to copy database file.", cause);
                }
            }
            if (this.mDatabaseConfiguration == null) {
                return;
            }
            try {
                final int version = DBUtil.readVersion(databasePath);
                if (version == this.mDatabaseVersion) {
                    return;
                }
                if (this.mDatabaseConfiguration.isMigrationRequired(version, this.mDatabaseVersion)) {
                    return;
                }
                if (this.mContext.deleteDatabase(databaseName)) {
                    try {
                        this.copyDatabaseFile(databasePath);
                    }
                    catch (IOException ex) {
                        Log.w("ROOM", "Unable to copy database file.", (Throwable)ex);
                    }
                }
                else {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to delete database file (");
                    sb.append(databaseName);
                    sb.append(") for a copy destructive migration.");
                    Log.w("ROOM", sb.toString());
                }
            }
            catch (IOException ex2) {
                Log.w("ROOM", "Unable to read database version.", (Throwable)ex2);
            }
        }
        finally {
            copyLock.unlock();
        }
    }
    
    @Override
    public void close() {
        synchronized (this) {
            this.mDelegate.close();
            this.mVerified = false;
        }
    }
    
    @Override
    public String getDatabaseName() {
        return this.mDelegate.getDatabaseName();
    }
    
    @Override
    public SupportSQLiteDatabase getReadableDatabase() {
        synchronized (this) {
            if (!this.mVerified) {
                this.verifyDatabaseFile();
                this.mVerified = true;
            }
            return this.mDelegate.getReadableDatabase();
        }
    }
    
    @Override
    public SupportSQLiteDatabase getWritableDatabase() {
        synchronized (this) {
            if (!this.mVerified) {
                this.verifyDatabaseFile();
                this.mVerified = true;
            }
            return this.mDelegate.getWritableDatabase();
        }
    }
    
    void setDatabaseConfiguration(final DatabaseConfiguration mDatabaseConfiguration) {
        this.mDatabaseConfiguration = mDatabaseConfiguration;
    }
    
    @Override
    public void setWriteAheadLoggingEnabled(final boolean writeAheadLoggingEnabled) {
        this.mDelegate.setWriteAheadLoggingEnabled(writeAheadLoggingEnabled);
    }
}
