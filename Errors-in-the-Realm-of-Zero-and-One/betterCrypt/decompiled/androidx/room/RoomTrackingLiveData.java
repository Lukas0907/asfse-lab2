// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room;

import java.util.concurrent.Executor;
import androidx.arch.core.executor.ArchTaskExecutor;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.Callable;
import androidx.lifecycle.LiveData;

class RoomTrackingLiveData<T> extends LiveData<T>
{
    final Callable<T> mComputeFunction;
    final AtomicBoolean mComputing;
    private final InvalidationLiveDataContainer mContainer;
    final RoomDatabase mDatabase;
    final boolean mInTransaction;
    final AtomicBoolean mInvalid;
    final Runnable mInvalidationRunnable;
    final InvalidationTracker.Observer mObserver;
    final Runnable mRefreshRunnable;
    final AtomicBoolean mRegisteredObserver;
    
    RoomTrackingLiveData(final RoomDatabase mDatabase, final InvalidationLiveDataContainer mContainer, final boolean mInTransaction, final Callable<T> mComputeFunction, final String[] array) {
        this.mInvalid = new AtomicBoolean(true);
        this.mComputing = new AtomicBoolean(false);
        this.mRegisteredObserver = new AtomicBoolean(false);
        this.mRefreshRunnable = new Runnable() {
            @Override
            public void run() {
                if (RoomTrackingLiveData.this.mRegisteredObserver.compareAndSet(false, true)) {
                    RoomTrackingLiveData.this.mDatabase.getInvalidationTracker().addWeakObserver(RoomTrackingLiveData.this.mObserver);
                }
                int n;
                do {
                    if (RoomTrackingLiveData.this.mComputing.compareAndSet(false, true)) {
                        Object call = null;
                        n = 0;
                        try {
                            while (RoomTrackingLiveData.this.mInvalid.compareAndSet(true, false)) {
                                try {
                                    call = RoomTrackingLiveData.this.mComputeFunction.call();
                                    n = 1;
                                    continue;
                                }
                                catch (Exception cause) {
                                    throw new RuntimeException("Exception while computing database live data.", cause);
                                }
                                break;
                            }
                            if (n == 0) {
                                continue;
                            }
                            LiveData.this.postValue(call);
                            continue;
                        }
                        finally {
                            RoomTrackingLiveData.this.mComputing.set(false);
                        }
                    }
                    n = 0;
                } while (n != 0 && RoomTrackingLiveData.this.mInvalid.get());
            }
        };
        this.mInvalidationRunnable = new Runnable() {
            @Override
            public void run() {
                final boolean hasActiveObservers = RoomTrackingLiveData.this.hasActiveObservers();
                if (RoomTrackingLiveData.this.mInvalid.compareAndSet(false, true) && hasActiveObservers) {
                    RoomTrackingLiveData.this.getQueryExecutor().execute(RoomTrackingLiveData.this.mRefreshRunnable);
                }
            }
        };
        this.mDatabase = mDatabase;
        this.mInTransaction = mInTransaction;
        this.mComputeFunction = mComputeFunction;
        this.mContainer = mContainer;
        this.mObserver = new InvalidationTracker.Observer(array) {
            @Override
            public void onInvalidated(final Set<String> set) {
                ArchTaskExecutor.getInstance().executeOnMainThread(RoomTrackingLiveData.this.mInvalidationRunnable);
            }
        };
    }
    
    Executor getQueryExecutor() {
        if (this.mInTransaction) {
            return this.mDatabase.getTransactionExecutor();
        }
        return this.mDatabase.getQueryExecutor();
    }
    
    @Override
    protected void onActive() {
        super.onActive();
        this.mContainer.onActive(this);
        this.getQueryExecutor().execute(this.mRefreshRunnable);
    }
    
    @Override
    protected void onInactive() {
        super.onInactive();
        this.mContainer.onInactive(this);
    }
}
