// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room;

import kotlinx.coroutines.CoroutineDispatcher;
import kotlinx.coroutines.BuildersKt;
import kotlinx.coroutines.CoroutineScope;
import kotlin.coroutines.ContinuationInterceptor;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.flow.FlowKt;
import kotlin.Unit;
import kotlinx.coroutines.flow.FlowCollector;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.Continuation;
import kotlin.jvm.JvmStatic;
import kotlinx.coroutines.flow.Flow;
import java.util.concurrent.Callable;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\b\u0007\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0007\b\u0002¢\u0006\u0002\u0010\u0002\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0004" }, d2 = { "Landroidx/room/CoroutinesRoom;", "", "()V", "Companion", "room-ktx_release" }, k = 1, mv = { 1, 1, 15 })
public final class CoroutinesRoom
{
    public static final Companion Companion;
    
    static {
        Companion = new Companion(null);
    }
    
    private CoroutinesRoom() {
    }
    
    @JvmStatic
    public static final <R> Flow<R> createFlow(final RoomDatabase roomDatabase, final boolean b, final String[] array, final Callable<R> callable) {
        return CoroutinesRoom.Companion.createFlow(roomDatabase, b, array, callable);
    }
    
    @JvmStatic
    public static final <R> Object execute(final RoomDatabase roomDatabase, final boolean b, final Callable<R> callable, final Continuation<? super R> continuation) {
        return CoroutinesRoom.Companion.execute(roomDatabase, b, callable, continuation);
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002JJ\u0010\u0003\u001a\r\u0012\t\u0012\u0007H\u0005¢\u0006\u0002\b\u00060\u0004\"\u0004\b\u0000\u0010\u00052\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u0002H\u00050\u000fH\u0007¢\u0006\u0002\u0010\u0010J5\u0010\u0011\u001a\u0002H\u0005\"\u0004\b\u0000\u0010\u00052\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u0002H\u00050\u000fH\u0087@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0012\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0013" }, d2 = { "Landroidx/room/CoroutinesRoom$Companion;", "", "()V", "createFlow", "Lkotlinx/coroutines/flow/Flow;", "R", "Lkotlin/jvm/JvmSuppressWildcards;", "db", "Landroidx/room/RoomDatabase;", "inTransaction", "", "tableNames", "", "", "callable", "Ljava/util/concurrent/Callable;", "(Landroidx/room/RoomDatabase;Z[Ljava/lang/String;Ljava/util/concurrent/Callable;)Lkotlinx/coroutines/flow/Flow;", "execute", "(Landroidx/room/RoomDatabase;ZLjava/util/concurrent/Callable;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "room-ktx_release" }, k = 1, mv = { 1, 1, 15 })
    public static final class Companion
    {
        private Companion() {
        }
        
        @JvmStatic
        public final <R> Flow<R> createFlow(final RoomDatabase roomDatabase, final boolean b, final String[] array, final Callable<R> callable) {
            Intrinsics.checkParameterIsNotNull(roomDatabase, "db");
            Intrinsics.checkParameterIsNotNull(array, "tableNames");
            Intrinsics.checkParameterIsNotNull(callable, "callable");
            return FlowKt.flow((Function2<? super FlowCollector<? super R>, ? super Continuation<? super Unit>, ?>)new CoroutinesRoom$Companion$createFlow.CoroutinesRoom$Companion$createFlow$1(array, b, roomDatabase, (Callable)callable, (Continuation)null));
        }
        
        @JvmStatic
        public final <R> Object execute(final RoomDatabase roomDatabase, final boolean b, final Callable<R> callable, final Continuation<? super R> continuation) {
            if (roomDatabase.isOpen() && roomDatabase.inTransaction()) {
                return callable.call();
            }
            final TransactionElement transactionElement = continuation.getContext().get((CoroutineContext.Key<TransactionElement>)TransactionElement.Key);
            if (transactionElement != null) {
                final ContinuationInterceptor transactionDispatcher$room_ktx_release = transactionElement.getTransactionDispatcher$room_ktx_release();
                if (transactionDispatcher$room_ktx_release != null) {
                    final ContinuationInterceptor continuationInterceptor = transactionDispatcher$room_ktx_release;
                    return BuildersKt.withContext((CoroutineContext)continuationInterceptor, (Function2<? super CoroutineScope, ? super Continuation<? super Object>, ?>)new CoroutinesRoom$Companion$execute.CoroutinesRoom$Companion$execute$2((Callable)callable, (Continuation)null), (Continuation<? super Object>)continuation);
                }
            }
            CoroutineDispatcher coroutineDispatcher;
            if (b) {
                coroutineDispatcher = CoroutinesRoomKt.getTransactionDispatcher(roomDatabase);
            }
            else {
                coroutineDispatcher = CoroutinesRoomKt.getQueryDispatcher(roomDatabase);
            }
            final ContinuationInterceptor continuationInterceptor = coroutineDispatcher;
            return BuildersKt.withContext((CoroutineContext)continuationInterceptor, (Function2<? super CoroutineScope, ? super Continuation<? super Object>, ?>)new CoroutinesRoom$Companion$execute.CoroutinesRoom$Companion$execute$2((Callable)callable, (Continuation)null), (Continuation<? super Object>)continuation);
        }
    }
}
