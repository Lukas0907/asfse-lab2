// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room;

import java.util.concurrent.Callable;
import java.util.Map;
import java.util.Collections;
import java.util.IdentityHashMap;
import androidx.lifecycle.LiveData;
import java.util.Set;

class InvalidationLiveDataContainer
{
    private final RoomDatabase mDatabase;
    final Set<LiveData> mLiveDataSet;
    
    InvalidationLiveDataContainer(final RoomDatabase mDatabase) {
        this.mLiveDataSet = (Set<LiveData>)Collections.newSetFromMap(new IdentityHashMap<LiveData, Boolean>());
        this.mDatabase = mDatabase;
    }
    
     <T> LiveData<T> create(final String[] array, final boolean b, final Callable<T> callable) {
        return new RoomTrackingLiveData<T>(this.mDatabase, this, b, callable, array);
    }
    
    void onActive(final LiveData liveData) {
        this.mLiveDataSet.add(liveData);
    }
    
    void onInactive(final LiveData liveData) {
        this.mLiveDataSet.remove(liveData);
    }
}
