// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room;

import kotlinx.coroutines.BuildersKt;
import kotlinx.coroutines.CoroutineScope;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.ThreadContextElementKt;
import kotlin.coroutines.jvm.internal.Boxing;
import kotlinx.coroutines.ThreadContextElement;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.JobKt;
import kotlin.ResultKt;
import kotlinx.coroutines.CompletableJob;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import java.util.concurrent.RejectedExecutionException;
import kotlinx.coroutines.CancellableContinuation;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlin.coroutines.ContinuationInterceptor;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.Job;
import java.util.concurrent.Executor;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u00000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\u001a\u001d\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0082@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0005\u001a\u0015\u0010\u0006\u001a\u00020\u0007*\u00020\bH\u0082@\u00f8\u0001\u0000¢\u0006\u0002\u0010\t\u001a9\u0010\n\u001a\u0002H\u000b\"\u0004\b\u0000\u0010\u000b*\u00020\b2\u001c\u0010\f\u001a\u0018\b\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u000b0\u000e\u0012\u0006\u0012\u0004\u0018\u00010\u000f0\rH\u0086@\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0010\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0011" }, d2 = { "acquireTransactionThread", "Lkotlin/coroutines/ContinuationInterceptor;", "Ljava/util/concurrent/Executor;", "controlJob", "Lkotlinx/coroutines/Job;", "(Ljava/util/concurrent/Executor;Lkotlinx/coroutines/Job;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "createTransactionContext", "Lkotlin/coroutines/CoroutineContext;", "Landroidx/room/RoomDatabase;", "(Landroidx/room/RoomDatabase;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "withTransaction", "R", "block", "Lkotlin/Function1;", "Lkotlin/coroutines/Continuation;", "", "(Landroidx/room/RoomDatabase;Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "room-ktx_release" }, k = 2, mv = { 1, 1, 15 })
public final class RoomDatabaseKt
{
    static final /* synthetic */ Object createTransactionContext(final RoomDatabase l$0, final Continuation<? super CoroutineContext> continuation) {
        Object o = null;
        Label_0047: {
            if (continuation instanceof RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1) {
                final RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1 roomDatabaseKt$createTransactionContext$1 = (RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1)continuation;
                if ((roomDatabaseKt$createTransactionContext$1.label & Integer.MIN_VALUE) != 0x0) {
                    roomDatabaseKt$createTransactionContext$1.label += Integer.MIN_VALUE;
                    o = roomDatabaseKt$createTransactionContext$1;
                    break Label_0047;
                }
            }
            o = new RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1((Continuation)continuation);
        }
        Object result = ((RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1)o).result;
        final Object coroutine_SUSPENDED = IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
        final int label = ((RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1)o).label;
        CompletableJob completableJob;
        RoomDatabase roomDatabase;
        if (label != 0) {
            if (label != 1) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            completableJob = (CompletableJob)((RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1)o).L$1;
            roomDatabase = (RoomDatabase)((RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1)o).L$0;
            ResultKt.throwOnFailure(result);
        }
        else {
            ResultKt.throwOnFailure(result);
            final CompletableJob job$default = JobKt.Job$default((Job)null, 1, (Object)null);
            final Executor transactionExecutor = l$0.getTransactionExecutor();
            Intrinsics.checkExpressionValueIsNotNull(transactionExecutor, "transactionExecutor");
            final CompletableJob completableJob2 = job$default;
            ((RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1)o).L$0 = l$0;
            ((RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1)o).L$1 = job$default;
            ((RoomDatabaseKt$createTransactionContext.RoomDatabaseKt$createTransactionContext$1)o).label = 1;
            final Object acquireTransactionThread = acquireTransactionThread(transactionExecutor, completableJob2, (Continuation<? super ContinuationInterceptor>)o);
            if (acquireTransactionThread == coroutine_SUSPENDED) {
                return coroutine_SUSPENDED;
            }
            roomDatabase = l$0;
            completableJob = job$default;
            result = acquireTransactionThread;
        }
        final ContinuationInterceptor continuationInterceptor = (ContinuationInterceptor)result;
        final TransactionElement transactionElement = new TransactionElement(completableJob, continuationInterceptor);
        final ThreadLocal<Integer> suspendingTransactionId = roomDatabase.getSuspendingTransactionId();
        Intrinsics.checkExpressionValueIsNotNull(suspendingTransactionId, "suspendingTransactionId");
        return continuationInterceptor.plus(transactionElement).plus(ThreadContextElementKt.asContextElement(suspendingTransactionId, Boxing.boxInt(System.identityHashCode(completableJob))));
    }
    
    public static final <R> Object withTransaction(RoomDatabase roomDatabase, Function1<? super Continuation<? super R>, ?> function1, final Continuation<? super R> continuation) {
        RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$1 roomDatabaseKt$withTransaction$2 = null;
        Label_0052: {
            if (continuation instanceof RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$1) {
                final RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$1 roomDatabaseKt$withTransaction$1 = (RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$1)continuation;
                if ((roomDatabaseKt$withTransaction$1.label & Integer.MIN_VALUE) != 0x0) {
                    roomDatabaseKt$withTransaction$1.label += Integer.MIN_VALUE;
                    roomDatabaseKt$withTransaction$2 = roomDatabaseKt$withTransaction$1;
                    break Label_0052;
                }
            }
            roomDatabaseKt$withTransaction$2 = new RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$1((Continuation)continuation);
        }
        Object o = roomDatabaseKt$withTransaction$2.result;
        final Object coroutine_SUSPENDED = IntrinsicsKt__IntrinsicsKt.getCOROUTINE_SUSPENDED();
        final int label = roomDatabaseKt$withTransaction$2.label;
        CoroutineContext l$2 = null;
        Label_0245: {
            if (label != 0) {
                if (label != 1) {
                    if (label == 2) {
                        final CoroutineContext coroutineContext = (CoroutineContext)roomDatabaseKt$withTransaction$2.L$2;
                        final Function1 function2 = (Function1)roomDatabaseKt$withTransaction$2.L$1;
                        roomDatabase = (RoomDatabase)roomDatabaseKt$withTransaction$2.L$0;
                        ResultKt.throwOnFailure(o);
                        return o;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                else {
                    function1 = (Function1)roomDatabaseKt$withTransaction$2.L$1;
                    roomDatabase = (RoomDatabase)roomDatabaseKt$withTransaction$2.L$0;
                    ResultKt.throwOnFailure(o);
                }
            }
            else {
                ResultKt.throwOnFailure(o);
                final TransactionElement transactionElement = ((Continuation)roomDatabaseKt$withTransaction$2).getContext().get((CoroutineContext.Key<TransactionElement>)TransactionElement.Key);
                if (transactionElement != null) {
                    final ContinuationInterceptor transactionDispatcher$room_ktx_release = transactionElement.getTransactionDispatcher$room_ktx_release();
                    if (transactionDispatcher$room_ktx_release != null) {
                        l$2 = transactionDispatcher$room_ktx_release;
                        break Label_0245;
                    }
                }
                roomDatabaseKt$withTransaction$2.L$0 = roomDatabase;
                roomDatabaseKt$withTransaction$2.L$1 = function1;
                roomDatabaseKt$withTransaction$2.label = 1;
                if ((o = createTransactionContext(roomDatabase, (Continuation<? super CoroutineContext>)roomDatabaseKt$withTransaction$2)) == coroutine_SUSPENDED) {
                    return coroutine_SUSPENDED;
                }
            }
            l$2 = (CoroutineContext)o;
        }
        final Function2<? super CoroutineScope, ? super Continuation<? super T>, ?> function3 = (Function2<? super CoroutineScope, ? super Continuation<? super T>, ?>)new RoomDatabaseKt$withTransaction.RoomDatabaseKt$withTransaction$2(roomDatabase, function1, (Continuation)null);
        roomDatabaseKt$withTransaction$2.L$0 = roomDatabase;
        roomDatabaseKt$withTransaction$2.L$1 = function1;
        roomDatabaseKt$withTransaction$2.L$2 = l$2;
        roomDatabaseKt$withTransaction$2.label = 2;
        final Object withContext = BuildersKt.withContext(l$2, (Function2<? super CoroutineScope, ? super Continuation<? super Object>, ?>)function3, (Continuation<? super Object>)roomDatabaseKt$withTransaction$2);
        if (withContext == coroutine_SUSPENDED) {
            return coroutine_SUSPENDED;
        }
        return withContext;
    }
}
