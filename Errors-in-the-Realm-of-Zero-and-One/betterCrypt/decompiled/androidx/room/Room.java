// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room;

import android.content.Context;

public class Room
{
    private static final String CURSOR_CONV_SUFFIX = "_CursorConverter";
    static final String LOG_TAG = "ROOM";
    public static final String MASTER_TABLE_NAME = "room_master_table";
    
    @Deprecated
    public Room() {
    }
    
    public static <T extends RoomDatabase> RoomDatabase.Builder<T> databaseBuilder(final Context context, final Class<T> clazz, final String s) {
        if (s != null && s.trim().length() != 0) {
            return (RoomDatabase.Builder<T>)new RoomDatabase.Builder(context, (Class<RoomDatabase>)clazz, s);
        }
        throw new IllegalArgumentException("Cannot build a database with null or empty name. If you are trying to create an in memory database, use Room.inMemoryDatabaseBuilder");
    }
    
    static <T, C> T getGeneratedImplementation(final Class<C> clazz, String string) {
        final String name = clazz.getPackage().getName();
        String s = clazz.getCanonicalName();
        if (!name.isEmpty()) {
            s = s.substring(name.length() + 1);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(s.replace('.', '_'));
        sb.append(string);
        final String string2 = sb.toString();
        while (true) {
            while (true) {
                try {
                    if (name.isEmpty()) {
                        string = string2;
                    }
                    else {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append(name);
                        sb2.append(".");
                        sb2.append(string2);
                        string = sb2.toString();
                    }
                    return (T)Class.forName(string).newInstance();
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Failed to create an instance of ");
                    sb3.append(clazz.getCanonicalName());
                    throw new RuntimeException(sb3.toString());
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("cannot find implementation for ");
                    sb4.append(clazz.getCanonicalName());
                    sb4.append(". ");
                    sb4.append(string2);
                    sb4.append(" does not exist");
                    throw new RuntimeException(sb4.toString());
                }
                catch (ClassNotFoundException ex) {
                    continue;
                }
                catch (IllegalAccessException ex2) {}
                catch (InstantiationException ex3) {}
                break;
            }
            continue;
        }
    }
    
    public static <T extends RoomDatabase> RoomDatabase.Builder<T> inMemoryDatabaseBuilder(final Context context, final Class<T> clazz) {
        return (RoomDatabase.Builder<T>)new RoomDatabase.Builder(context, (Class<RoomDatabase>)clazz, null);
    }
}
