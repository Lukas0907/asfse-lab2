// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room.paging;

import androidx.paging.PositionalDataSource$LoadRangeCallback;
import androidx.paging.PositionalDataSource$LoadRangeParams;
import java.util.Collections;
import androidx.paging.PositionalDataSource$LoadInitialCallback;
import androidx.paging.PositionalDataSource$LoadInitialParams;
import java.util.List;
import android.database.Cursor;
import androidx.sqlite.db.SupportSQLiteQuery;
import java.util.Set;
import androidx.room.RoomSQLiteQuery;
import androidx.room.InvalidationTracker;
import androidx.room.RoomDatabase;
import androidx.paging.PositionalDataSource;

public abstract class LimitOffsetDataSource<T> extends PositionalDataSource<T>
{
    private final String mCountQuery;
    private final RoomDatabase mDb;
    private final boolean mInTransaction;
    private final String mLimitOffsetQuery;
    private final InvalidationTracker.Observer mObserver;
    private final RoomSQLiteQuery mSourceQuery;
    
    protected LimitOffsetDataSource(final RoomDatabase mDb, final RoomSQLiteQuery mSourceQuery, final boolean mInTransaction, final String... array) {
        this.mDb = mDb;
        this.mSourceQuery = mSourceQuery;
        this.mInTransaction = mInTransaction;
        final StringBuilder sb = new StringBuilder();
        sb.append("SELECT COUNT(*) FROM ( ");
        sb.append(this.mSourceQuery.getSql());
        sb.append(" )");
        this.mCountQuery = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("SELECT * FROM ( ");
        sb2.append(this.mSourceQuery.getSql());
        sb2.append(" ) LIMIT ? OFFSET ?");
        this.mLimitOffsetQuery = sb2.toString();
        this.mObserver = new InvalidationTracker.Observer(array) {
            @Override
            public void onInvalidated(final Set<String> set) {
                LimitOffsetDataSource.this.invalidate();
            }
        };
        mDb.getInvalidationTracker().addWeakObserver(this.mObserver);
    }
    
    protected LimitOffsetDataSource(final RoomDatabase roomDatabase, final SupportSQLiteQuery supportSQLiteQuery, final boolean b, final String... array) {
        this(roomDatabase, RoomSQLiteQuery.copyFrom(supportSQLiteQuery), b, array);
    }
    
    private RoomSQLiteQuery getSQLiteQuery(final int n, final int n2) {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire(this.mLimitOffsetQuery, this.mSourceQuery.getArgCount() + 2);
        acquire.copyArgumentsFrom(this.mSourceQuery);
        acquire.bindLong(acquire.getArgCount() - 1, n2);
        acquire.bindLong(acquire.getArgCount(), n);
        return acquire;
    }
    
    protected abstract List<T> convertRows(final Cursor p0);
    
    public int countItems() {
        final RoomSQLiteQuery acquire = RoomSQLiteQuery.acquire(this.mCountQuery, this.mSourceQuery.getArgCount());
        acquire.copyArgumentsFrom(this.mSourceQuery);
        final Cursor query = this.mDb.query(acquire);
        try {
            if (query.moveToFirst()) {
                return query.getInt(0);
            }
            return 0;
        }
        finally {
            query.close();
            acquire.release();
        }
    }
    
    public boolean isInvalid() {
        this.mDb.getInvalidationTracker().refreshVersionsSync();
        return super.isInvalid();
    }
    
    public void loadInitial(PositionalDataSource$LoadInitialParams query, final PositionalDataSource$LoadInitialCallback<T> positionalDataSource$LoadInitialCallback) {
        final List<Object> emptyList = Collections.emptyList();
        this.mDb.beginTransaction();
        final PositionalDataSource$LoadInitialParams positionalDataSource$LoadInitialParams = null;
        Object o = null;
        final PositionalDataSource$LoadInitialParams positionalDataSource$LoadInitialParams2 = null;
        RoomSQLiteQuery roomSQLiteQuery = null;
        List list = null;
        Label_0158: {
            try {
                final int countItems = this.countItems();
                int computeInitialLoadPosition = 0;
                RoomSQLiteQuery sqLiteQuery = null;
                Label_0114: {
                    if (countItems != 0) {
                        computeInitialLoadPosition = computeInitialLoadPosition(query, countItems);
                        sqLiteQuery = this.getSQLiteQuery(computeInitialLoadPosition, computeInitialLoadSize(query, computeInitialLoadPosition, countItems));
                        query = positionalDataSource$LoadInitialParams2;
                        try {
                            o = (query = (PositionalDataSource$LoadInitialParams)this.mDb.query(sqLiteQuery));
                            final List<T> convertRows = this.convertRows((Cursor)o);
                            query = (PositionalDataSource$LoadInitialParams)o;
                            this.mDb.setTransactionSuccessful();
                            query = (PositionalDataSource$LoadInitialParams)o;
                            o = convertRows;
                            break Label_0114;
                        }
                        finally {
                            break Label_0158;
                        }
                    }
                    o = emptyList;
                    computeInitialLoadPosition = 0;
                    sqLiteQuery = null;
                    query = positionalDataSource$LoadInitialParams;
                }
                if (query != null) {
                    ((Cursor)query).close();
                }
                this.mDb.endTransaction();
                if (sqLiteQuery != null) {
                    sqLiteQuery.release();
                }
                positionalDataSource$LoadInitialCallback.onResult((List)o, computeInitialLoadPosition, countItems);
                return;
            }
            finally {
                roomSQLiteQuery = null;
                list = (List)o;
            }
        }
        if (list != null) {
            ((Cursor)list).close();
        }
        this.mDb.endTransaction();
        if (roomSQLiteQuery != null) {
            roomSQLiteQuery.release();
        }
    }
    
    public List<T> loadRange(final int n, final int n2) {
        final RoomSQLiteQuery sqLiteQuery = this.getSQLiteQuery(n, n2);
        if (this.mInTransaction) {
            this.mDb.beginTransaction();
            Cursor query = null;
            try {
                final Cursor cursor = query = this.mDb.query(sqLiteQuery);
                final List<T> convertRows = this.convertRows(cursor);
                query = cursor;
                this.mDb.setTransactionSuccessful();
                return convertRows;
            }
            finally {
                if (query != null) {
                    query.close();
                }
                this.mDb.endTransaction();
                sqLiteQuery.release();
            }
        }
        final Cursor query2 = this.mDb.query(sqLiteQuery);
        try {
            return this.convertRows(query2);
        }
        finally {
            query2.close();
            sqLiteQuery.release();
        }
    }
    
    public void loadRange(final PositionalDataSource$LoadRangeParams positionalDataSource$LoadRangeParams, final PositionalDataSource$LoadRangeCallback<T> positionalDataSource$LoadRangeCallback) {
        positionalDataSource$LoadRangeCallback.onResult((List)this.loadRange(positionalDataSource$LoadRangeParams.startPosition, positionalDataSource$LoadRangeParams.loadSize));
    }
}
