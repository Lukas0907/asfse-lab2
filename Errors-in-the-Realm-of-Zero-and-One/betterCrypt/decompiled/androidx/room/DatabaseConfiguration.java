// 
// Decompiled by Procyon v0.5.36
// 

package androidx.room;

import androidx.sqlite.db.SupportSQLiteOpenHelper;
import java.util.concurrent.Executor;
import java.util.Set;
import java.io.File;
import android.content.Context;
import java.util.List;

public class DatabaseConfiguration
{
    public final boolean allowDestructiveMigrationOnDowngrade;
    public final boolean allowMainThreadQueries;
    public final List<RoomDatabase.Callback> callbacks;
    public final Context context;
    public final String copyFromAssetPath;
    public final File copyFromFile;
    public final RoomDatabase.JournalMode journalMode;
    private final Set<Integer> mMigrationNotRequiredFrom;
    public final RoomDatabase.MigrationContainer migrationContainer;
    public final boolean multiInstanceInvalidation;
    public final String name;
    public final Executor queryExecutor;
    public final boolean requireMigration;
    public final SupportSQLiteOpenHelper.Factory sqliteOpenHelperFactory;
    public final Executor transactionExecutor;
    
    @Deprecated
    public DatabaseConfiguration(final Context context, final String s, final SupportSQLiteOpenHelper.Factory factory, final RoomDatabase.MigrationContainer migrationContainer, final List<RoomDatabase.Callback> list, final boolean b, final RoomDatabase.JournalMode journalMode, final Executor executor, final Executor executor2, final boolean b2, final boolean b3, final boolean b4, final Set<Integer> set) {
        this(context, s, factory, migrationContainer, list, b, journalMode, executor, executor2, b2, b3, b4, set, null, null);
    }
    
    public DatabaseConfiguration(final Context context, final String name, final SupportSQLiteOpenHelper.Factory sqliteOpenHelperFactory, final RoomDatabase.MigrationContainer migrationContainer, final List<RoomDatabase.Callback> callbacks, final boolean allowMainThreadQueries, final RoomDatabase.JournalMode journalMode, final Executor queryExecutor, final Executor transactionExecutor, final boolean multiInstanceInvalidation, final boolean requireMigration, final boolean allowDestructiveMigrationOnDowngrade, final Set<Integer> mMigrationNotRequiredFrom, final String copyFromAssetPath, final File copyFromFile) {
        this.sqliteOpenHelperFactory = sqliteOpenHelperFactory;
        this.context = context;
        this.name = name;
        this.migrationContainer = migrationContainer;
        this.callbacks = callbacks;
        this.allowMainThreadQueries = allowMainThreadQueries;
        this.journalMode = journalMode;
        this.queryExecutor = queryExecutor;
        this.transactionExecutor = transactionExecutor;
        this.multiInstanceInvalidation = multiInstanceInvalidation;
        this.requireMigration = requireMigration;
        this.allowDestructiveMigrationOnDowngrade = allowDestructiveMigrationOnDowngrade;
        this.mMigrationNotRequiredFrom = mMigrationNotRequiredFrom;
        this.copyFromAssetPath = copyFromAssetPath;
        this.copyFromFile = copyFromFile;
    }
    
    @Deprecated
    public DatabaseConfiguration(final Context context, final String s, final SupportSQLiteOpenHelper.Factory factory, final RoomDatabase.MigrationContainer migrationContainer, final List<RoomDatabase.Callback> list, final boolean b, final RoomDatabase.JournalMode journalMode, final Executor executor, final boolean b2, final Set<Integer> set) {
        this(context, s, factory, migrationContainer, list, b, journalMode, executor, executor, false, b2, false, set, null, null);
    }
    
    public boolean isMigrationRequired(final int i, int n) {
        boolean b = true;
        if (i > n) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (n != 0 && this.allowDestructiveMigrationOnDowngrade) {
            return false;
        }
        if (this.requireMigration) {
            final Set<Integer> mMigrationNotRequiredFrom = this.mMigrationNotRequiredFrom;
            if (mMigrationNotRequiredFrom == null) {
                return b;
            }
            if (!mMigrationNotRequiredFrom.contains(i)) {
                return true;
            }
        }
        b = false;
        return b;
    }
    
    @Deprecated
    public boolean isMigrationRequiredFrom(final int n) {
        return this.isMigrationRequired(n, n + 1);
    }
}
