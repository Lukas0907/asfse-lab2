// 
// Decompiled by Procyon v0.5.36
// 

package androidx.print;

import android.os.CancellationSignal$OnCancelListener;
import android.print.PrintDocumentInfo$Builder;
import android.os.Bundle;
import android.print.PrintDocumentAdapter$LayoutResultCallback;
import android.print.PageRange;
import android.os.AsyncTask;
import android.print.PrintAttributes$Margins;
import android.print.PrintDocumentAdapter$WriteResultCallback;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.print.PrintDocumentAdapter;
import android.print.PrintAttributes$MediaSize;
import android.print.PrintManager;
import java.io.FileNotFoundException;
import java.io.IOException;
import android.util.Log;
import android.graphics.Rect;
import java.io.InputStream;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.print.PrintAttributes$Builder;
import android.print.PrintAttributes;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.Paint;
import android.graphics.Canvas;
import android.graphics.Bitmap$Config;
import android.graphics.Bitmap;
import android.os.Build$VERSION;
import android.graphics.BitmapFactory$Options;
import android.content.Context;

public final class PrintHelper
{
    public static final int COLOR_MODE_COLOR = 2;
    public static final int COLOR_MODE_MONOCHROME = 1;
    static final boolean IS_MIN_MARGINS_HANDLING_CORRECT;
    private static final String LOG_TAG = "PrintHelper";
    private static final int MAX_PRINT_SIZE = 3500;
    public static final int ORIENTATION_LANDSCAPE = 1;
    public static final int ORIENTATION_PORTRAIT = 2;
    static final boolean PRINT_ACTIVITY_RESPECTS_ORIENTATION;
    public static final int SCALE_MODE_FILL = 2;
    public static final int SCALE_MODE_FIT = 1;
    int mColorMode;
    final Context mContext;
    BitmapFactory$Options mDecodeOptions;
    final Object mLock;
    int mOrientation;
    int mScaleMode;
    
    static {
        final int sdk_INT = Build$VERSION.SDK_INT;
        final boolean b = false;
        PRINT_ACTIVITY_RESPECTS_ORIENTATION = (sdk_INT < 20 || Build$VERSION.SDK_INT > 23);
        boolean is_MIN_MARGINS_HANDLING_CORRECT = b;
        if (Build$VERSION.SDK_INT != 23) {
            is_MIN_MARGINS_HANDLING_CORRECT = true;
        }
        IS_MIN_MARGINS_HANDLING_CORRECT = is_MIN_MARGINS_HANDLING_CORRECT;
    }
    
    public PrintHelper(final Context mContext) {
        this.mDecodeOptions = null;
        this.mLock = new Object();
        this.mScaleMode = 2;
        this.mColorMode = 2;
        this.mOrientation = 1;
        this.mContext = mContext;
    }
    
    static Bitmap convertBitmapForColorMode(final Bitmap bitmap, final int n) {
        if (n != 1) {
            return bitmap;
        }
        final Bitmap bitmap2 = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap$Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap2);
        final Paint paint = new Paint();
        final ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0.0f);
        paint.setColorFilter((ColorFilter)new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, paint);
        canvas.setBitmap((Bitmap)null);
        return bitmap2;
    }
    
    private static PrintAttributes$Builder copyAttributes(final PrintAttributes printAttributes) {
        final PrintAttributes$Builder setMinMargins = new PrintAttributes$Builder().setMediaSize(printAttributes.getMediaSize()).setResolution(printAttributes.getResolution()).setMinMargins(printAttributes.getMinMargins());
        if (printAttributes.getColorMode() != 0) {
            setMinMargins.setColorMode(printAttributes.getColorMode());
        }
        if (Build$VERSION.SDK_INT >= 23 && printAttributes.getDuplexMode() != 0) {
            setMinMargins.setDuplexMode(printAttributes.getDuplexMode());
        }
        return setMinMargins;
    }
    
    static Matrix getMatrix(final int n, final int n2, final RectF rectF, final int n3) {
        final Matrix matrix = new Matrix();
        final float width = rectF.width();
        final float n4 = (float)n;
        final float n5 = width / n4;
        float n6;
        if (n3 == 2) {
            n6 = Math.max(n5, rectF.height() / n2);
        }
        else {
            n6 = Math.min(n5, rectF.height() / n2);
        }
        matrix.postScale(n6, n6);
        matrix.postTranslate((rectF.width() - n4 * n6) / 2.0f, (rectF.height() - n2 * n6) / 2.0f);
        return matrix;
    }
    
    static boolean isPortrait(final Bitmap bitmap) {
        return bitmap.getWidth() <= bitmap.getHeight();
    }
    
    private Bitmap loadBitmap(Uri openInputStream, BitmapFactory$Options decodeStream) throws FileNotFoundException {
        if (openInputStream != null) {
            final Context mContext = this.mContext;
            if (mContext != null) {
                final InputStream inputStream = null;
                InputStream inputStream2;
                try {
                    openInputStream = (IOException)mContext.getContentResolver().openInputStream((Uri)openInputStream);
                    try {
                        decodeStream = (BitmapFactory$Options)BitmapFactory.decodeStream((InputStream)openInputStream, (Rect)null, decodeStream);
                        if (openInputStream != null) {
                            try {
                                ((InputStream)openInputStream).close();
                                return (Bitmap)decodeStream;
                            }
                            catch (IOException openInputStream) {
                                Log.w("PrintHelper", "close fail ", (Throwable)openInputStream);
                            }
                        }
                        return (Bitmap)decodeStream;
                    }
                    finally {}
                }
                finally {
                    inputStream2 = inputStream;
                }
                if (inputStream2 != null) {
                    try {
                        inputStream2.close();
                    }
                    catch (IOException ex) {
                        Log.w("PrintHelper", "close fail ", (Throwable)ex);
                    }
                }
            }
        }
        throw new IllegalArgumentException("bad argument to loadBitmap");
    }
    
    public static boolean systemSupportsPrint() {
        return Build$VERSION.SDK_INT >= 19;
    }
    
    public int getColorMode() {
        return this.mColorMode;
    }
    
    public int getOrientation() {
        if (Build$VERSION.SDK_INT >= 19 && this.mOrientation == 0) {
            return 1;
        }
        return this.mOrientation;
    }
    
    public int getScaleMode() {
        return this.mScaleMode;
    }
    
    Bitmap loadConstrainedBitmap(final Uri uri) throws FileNotFoundException {
        if (uri != null && this.mContext != null) {
            final BitmapFactory$Options bitmapFactory$Options = new BitmapFactory$Options();
            bitmapFactory$Options.inJustDecodeBounds = true;
            this.loadBitmap(uri, bitmapFactory$Options);
            final int outWidth = bitmapFactory$Options.outWidth;
            final int outHeight = bitmapFactory$Options.outHeight;
            if (outWidth > 0) {
                if (outHeight <= 0) {
                    return null;
                }
                int i;
                int inSampleSize;
                for (i = Math.max(outWidth, outHeight), inSampleSize = 1; i > 3500; i >>>= 1, inSampleSize <<= 1) {}
                if (inSampleSize > 0) {
                    if (Math.min(outWidth, outHeight) / inSampleSize <= 0) {
                        return null;
                    }
                    final Object mLock = this.mLock;
                    synchronized (mLock) {
                        this.mDecodeOptions = new BitmapFactory$Options();
                        this.mDecodeOptions.inMutable = true;
                        this.mDecodeOptions.inSampleSize = inSampleSize;
                        final BitmapFactory$Options mDecodeOptions = this.mDecodeOptions;
                        // monitorexit(mLock)
                        try {
                            final Bitmap loadBitmap = this.loadBitmap(uri, mDecodeOptions);
                            synchronized (this.mLock) {
                                this.mDecodeOptions = null;
                                return loadBitmap;
                            }
                        }
                        finally {
                            synchronized (this.mLock) {
                                this.mDecodeOptions = null;
                            }
                            // monitorexit(this.mLock)
                        }
                    }
                }
            }
            return null;
        }
        throw new IllegalArgumentException("bad argument to getScaledBitmap");
    }
    
    public void printBitmap(final String s, final Bitmap bitmap) {
        this.printBitmap(s, bitmap, null);
    }
    
    public void printBitmap(final String s, final Bitmap bitmap, final OnPrintFinishCallback onPrintFinishCallback) {
        if (Build$VERSION.SDK_INT >= 19) {
            if (bitmap == null) {
                return;
            }
            final PrintManager printManager = (PrintManager)this.mContext.getSystemService("print");
            PrintAttributes$MediaSize mediaSize;
            if (isPortrait(bitmap)) {
                mediaSize = PrintAttributes$MediaSize.UNKNOWN_PORTRAIT;
            }
            else {
                mediaSize = PrintAttributes$MediaSize.UNKNOWN_LANDSCAPE;
            }
            printManager.print(s, (PrintDocumentAdapter)new PrintBitmapAdapter(s, this.mScaleMode, bitmap, onPrintFinishCallback), new PrintAttributes$Builder().setMediaSize(mediaSize).setColorMode(this.mColorMode).build());
        }
    }
    
    public void printBitmap(final String s, final Uri uri) throws FileNotFoundException {
        this.printBitmap(s, uri, null);
    }
    
    public void printBitmap(final String s, final Uri uri, final OnPrintFinishCallback onPrintFinishCallback) throws FileNotFoundException {
        if (Build$VERSION.SDK_INT < 19) {
            return;
        }
        final PrintUriAdapter printUriAdapter = new PrintUriAdapter(s, uri, onPrintFinishCallback, this.mScaleMode);
        final PrintManager printManager = (PrintManager)this.mContext.getSystemService("print");
        final PrintAttributes$Builder printAttributes$Builder = new PrintAttributes$Builder();
        printAttributes$Builder.setColorMode(this.mColorMode);
        final int mOrientation = this.mOrientation;
        if (mOrientation != 1 && mOrientation != 0) {
            if (mOrientation == 2) {
                printAttributes$Builder.setMediaSize(PrintAttributes$MediaSize.UNKNOWN_PORTRAIT);
            }
        }
        else {
            printAttributes$Builder.setMediaSize(PrintAttributes$MediaSize.UNKNOWN_LANDSCAPE);
        }
        printManager.print(s, (PrintDocumentAdapter)printUriAdapter, printAttributes$Builder.build());
    }
    
    public void setColorMode(final int mColorMode) {
        this.mColorMode = mColorMode;
    }
    
    public void setOrientation(final int mOrientation) {
        this.mOrientation = mOrientation;
    }
    
    public void setScaleMode(final int mScaleMode) {
        this.mScaleMode = mScaleMode;
    }
    
    void writeBitmap(final PrintAttributes printAttributes, final int n, final Bitmap bitmap, final ParcelFileDescriptor parcelFileDescriptor, final CancellationSignal cancellationSignal, final PrintDocumentAdapter$WriteResultCallback printDocumentAdapter$WriteResultCallback) {
        PrintAttributes build;
        if (PrintHelper.IS_MIN_MARGINS_HANDLING_CORRECT) {
            build = printAttributes;
        }
        else {
            build = copyAttributes(printAttributes).setMinMargins(new PrintAttributes$Margins(0, 0, 0, 0)).build();
        }
        new AsyncTask<Void, Void, Throwable>() {
            protected Throwable doInBackground(final Void... p0) {
                // 
                // This method could not be decompiled.
                // 
                // Original Bytecode:
                // 
                //     1: getfield        androidx/print/PrintHelper$1.val$cancellationSignal:Landroid/os/CancellationSignal;
                //     4: invokevirtual   android/os/CancellationSignal.isCanceled:()Z
                //     7: ifeq            12
                //    10: aconst_null    
                //    11: areturn        
                //    12: new             Landroid/print/pdf/PrintedPdfDocument;
                //    15: dup            
                //    16: aload_0        
                //    17: getfield        androidx/print/PrintHelper$1.this$0:Landroidx/print/PrintHelper;
                //    20: getfield        androidx/print/PrintHelper.mContext:Landroid/content/Context;
                //    23: aload_0        
                //    24: getfield        androidx/print/PrintHelper$1.val$pdfAttributes:Landroid/print/PrintAttributes;
                //    27: invokespecial   android/print/pdf/PrintedPdfDocument.<init>:(Landroid/content/Context;Landroid/print/PrintAttributes;)V
                //    30: astore          4
                //    32: aload_0        
                //    33: getfield        androidx/print/PrintHelper$1.val$bitmap:Landroid/graphics/Bitmap;
                //    36: aload_0        
                //    37: getfield        androidx/print/PrintHelper$1.val$pdfAttributes:Landroid/print/PrintAttributes;
                //    40: invokevirtual   android/print/PrintAttributes.getColorMode:()I
                //    43: invokestatic    androidx/print/PrintHelper.convertBitmapForColorMode:(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
                //    46: astore_3       
                //    47: aload_0        
                //    48: getfield        androidx/print/PrintHelper$1.val$cancellationSignal:Landroid/os/CancellationSignal;
                //    51: invokevirtual   android/os/CancellationSignal.isCanceled:()Z
                //    54: istore_2       
                //    55: iload_2        
                //    56: ifeq            61
                //    59: aconst_null    
                //    60: areturn        
                //    61: aload           4
                //    63: iconst_1       
                //    64: invokevirtual   android/print/pdf/PrintedPdfDocument.startPage:(I)Landroid/graphics/pdf/PdfDocument$Page;
                //    67: astore          5
                //    69: getstatic       androidx/print/PrintHelper.IS_MIN_MARGINS_HANDLING_CORRECT:Z
                //    72: ifeq            94
                //    75: new             Landroid/graphics/RectF;
                //    78: dup            
                //    79: aload           5
                //    81: invokevirtual   android/graphics/pdf/PdfDocument$Page.getInfo:()Landroid/graphics/pdf/PdfDocument$PageInfo;
                //    84: invokevirtual   android/graphics/pdf/PdfDocument$PageInfo.getContentRect:()Landroid/graphics/Rect;
                //    87: invokespecial   android/graphics/RectF.<init>:(Landroid/graphics/Rect;)V
                //    90: astore_1       
                //    91: goto            150
                //    94: new             Landroid/print/pdf/PrintedPdfDocument;
                //    97: dup            
                //    98: aload_0        
                //    99: getfield        androidx/print/PrintHelper$1.this$0:Landroidx/print/PrintHelper;
                //   102: getfield        androidx/print/PrintHelper.mContext:Landroid/content/Context;
                //   105: aload_0        
                //   106: getfield        androidx/print/PrintHelper$1.val$attributes:Landroid/print/PrintAttributes;
                //   109: invokespecial   android/print/pdf/PrintedPdfDocument.<init>:(Landroid/content/Context;Landroid/print/PrintAttributes;)V
                //   112: astore          6
                //   114: aload           6
                //   116: iconst_1       
                //   117: invokevirtual   android/print/pdf/PrintedPdfDocument.startPage:(I)Landroid/graphics/pdf/PdfDocument$Page;
                //   120: astore          7
                //   122: new             Landroid/graphics/RectF;
                //   125: dup            
                //   126: aload           7
                //   128: invokevirtual   android/graphics/pdf/PdfDocument$Page.getInfo:()Landroid/graphics/pdf/PdfDocument$PageInfo;
                //   131: invokevirtual   android/graphics/pdf/PdfDocument$PageInfo.getContentRect:()Landroid/graphics/Rect;
                //   134: invokespecial   android/graphics/RectF.<init>:(Landroid/graphics/Rect;)V
                //   137: astore_1       
                //   138: aload           6
                //   140: aload           7
                //   142: invokevirtual   android/print/pdf/PrintedPdfDocument.finishPage:(Landroid/graphics/pdf/PdfDocument$Page;)V
                //   145: aload           6
                //   147: invokevirtual   android/print/pdf/PrintedPdfDocument.close:()V
                //   150: aload_3        
                //   151: invokevirtual   android/graphics/Bitmap.getWidth:()I
                //   154: aload_3        
                //   155: invokevirtual   android/graphics/Bitmap.getHeight:()I
                //   158: aload_1        
                //   159: aload_0        
                //   160: getfield        androidx/print/PrintHelper$1.val$fittingMode:I
                //   163: invokestatic    androidx/print/PrintHelper.getMatrix:(IILandroid/graphics/RectF;I)Landroid/graphics/Matrix;
                //   166: astore          6
                //   168: getstatic       androidx/print/PrintHelper.IS_MIN_MARGINS_HANDLING_CORRECT:Z
                //   171: ifeq            177
                //   174: goto            201
                //   177: aload           6
                //   179: aload_1        
                //   180: getfield        android/graphics/RectF.left:F
                //   183: aload_1        
                //   184: getfield        android/graphics/RectF.top:F
                //   187: invokevirtual   android/graphics/Matrix.postTranslate:(FF)Z
                //   190: pop            
                //   191: aload           5
                //   193: invokevirtual   android/graphics/pdf/PdfDocument$Page.getCanvas:()Landroid/graphics/Canvas;
                //   196: aload_1        
                //   197: invokevirtual   android/graphics/Canvas.clipRect:(Landroid/graphics/RectF;)Z
                //   200: pop            
                //   201: aload           5
                //   203: invokevirtual   android/graphics/pdf/PdfDocument$Page.getCanvas:()Landroid/graphics/Canvas;
                //   206: aload_3        
                //   207: aload           6
                //   209: aconst_null    
                //   210: invokevirtual   android/graphics/Canvas.drawBitmap:(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
                //   213: aload           4
                //   215: aload           5
                //   217: invokevirtual   android/print/pdf/PrintedPdfDocument.finishPage:(Landroid/graphics/pdf/PdfDocument$Page;)V
                //   220: aload_0        
                //   221: getfield        androidx/print/PrintHelper$1.val$cancellationSignal:Landroid/os/CancellationSignal;
                //   224: invokevirtual   android/os/CancellationSignal.isCanceled:()Z
                //   227: istore_2       
                //   228: iload_2        
                //   229: ifeq            267
                //   232: aload           4
                //   234: invokevirtual   android/print/pdf/PrintedPdfDocument.close:()V
                //   237: aload_0        
                //   238: getfield        androidx/print/PrintHelper$1.val$fileDescriptor:Landroid/os/ParcelFileDescriptor;
                //   241: astore_1       
                //   242: aload_1        
                //   243: ifnull          253
                //   246: aload_0        
                //   247: getfield        androidx/print/PrintHelper$1.val$fileDescriptor:Landroid/os/ParcelFileDescriptor;
                //   250: invokevirtual   android/os/ParcelFileDescriptor.close:()V
                //   253: aload_3        
                //   254: aload_0        
                //   255: getfield        androidx/print/PrintHelper$1.val$bitmap:Landroid/graphics/Bitmap;
                //   258: if_acmpeq       265
                //   261: aload_3        
                //   262: invokevirtual   android/graphics/Bitmap.recycle:()V
                //   265: aconst_null    
                //   266: areturn        
                //   267: aload           4
                //   269: new             Ljava/io/FileOutputStream;
                //   272: dup            
                //   273: aload_0        
                //   274: getfield        androidx/print/PrintHelper$1.val$fileDescriptor:Landroid/os/ParcelFileDescriptor;
                //   277: invokevirtual   android/os/ParcelFileDescriptor.getFileDescriptor:()Ljava/io/FileDescriptor;
                //   280: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/FileDescriptor;)V
                //   283: invokevirtual   android/print/pdf/PrintedPdfDocument.writeTo:(Ljava/io/OutputStream;)V
                //   286: aload           4
                //   288: invokevirtual   android/print/pdf/PrintedPdfDocument.close:()V
                //   291: aload_0        
                //   292: getfield        androidx/print/PrintHelper$1.val$fileDescriptor:Landroid/os/ParcelFileDescriptor;
                //   295: astore_1       
                //   296: aload_1        
                //   297: ifnull          307
                //   300: aload_0        
                //   301: getfield        androidx/print/PrintHelper$1.val$fileDescriptor:Landroid/os/ParcelFileDescriptor;
                //   304: invokevirtual   android/os/ParcelFileDescriptor.close:()V
                //   307: aload_3        
                //   308: aload_0        
                //   309: getfield        androidx/print/PrintHelper$1.val$bitmap:Landroid/graphics/Bitmap;
                //   312: if_acmpeq       375
                //   315: aload_3        
                //   316: invokevirtual   android/graphics/Bitmap.recycle:()V
                //   319: aconst_null    
                //   320: areturn        
                //   321: astore_1       
                //   322: aload           4
                //   324: invokevirtual   android/print/pdf/PrintedPdfDocument.close:()V
                //   327: aload_0        
                //   328: getfield        androidx/print/PrintHelper$1.val$fileDescriptor:Landroid/os/ParcelFileDescriptor;
                //   331: astore          4
                //   333: aload           4
                //   335: ifnull          345
                //   338: aload_0        
                //   339: getfield        androidx/print/PrintHelper$1.val$fileDescriptor:Landroid/os/ParcelFileDescriptor;
                //   342: invokevirtual   android/os/ParcelFileDescriptor.close:()V
                //   345: aload_3        
                //   346: aload_0        
                //   347: getfield        androidx/print/PrintHelper$1.val$bitmap:Landroid/graphics/Bitmap;
                //   350: if_acmpeq       357
                //   353: aload_3        
                //   354: invokevirtual   android/graphics/Bitmap.recycle:()V
                //   357: aload_1        
                //   358: athrow         
                //   359: astore_1       
                //   360: aload_1        
                //   361: areturn        
                //   362: astore_1       
                //   363: goto            253
                //   366: astore_1       
                //   367: goto            307
                //   370: astore          4
                //   372: goto            345
                //   375: aconst_null    
                //   376: areturn        
                //    Exceptions:
                //  Try           Handler
                //  Start  End    Start  End    Type                 
                //  -----  -----  -----  -----  ---------------------
                //  0      10     359    362    Any
                //  12     55     359    362    Any
                //  61     91     321    359    Any
                //  94     150    321    359    Any
                //  150    174    321    359    Any
                //  177    201    321    359    Any
                //  201    228    321    359    Any
                //  232    242    359    362    Any
                //  246    253    362    366    Ljava/io/IOException;
                //  246    253    359    362    Any
                //  253    265    359    362    Any
                //  267    286    321    359    Any
                //  286    296    359    362    Any
                //  300    307    366    370    Ljava/io/IOException;
                //  300    307    359    362    Any
                //  307    319    359    362    Any
                //  322    333    359    362    Any
                //  338    345    370    375    Ljava/io/IOException;
                //  338    345    359    362    Any
                //  345    357    359    362    Any
                //  357    359    359    362    Any
                // 
                // The error that occurred was:
                // 
                // java.lang.IndexOutOfBoundsException: Index 182 out of bounds for length 182
                //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
                //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
                //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
                //     at java.base/java.util.Objects.checkIndex(Objects.java:373)
                //     at java.base/java.util.ArrayList.get(ArrayList.java:426)
                //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
                //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformCall(AstMethodBodyBuilder.java:1164)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:1009)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:554)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:392)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:333)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:294)
                //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
                //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
                //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
                //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
                // 
                throw new IllegalStateException("An error occurred while decompiling this method.");
            }
            
            protected void onPostExecute(final Throwable t) {
                if (cancellationSignal.isCanceled()) {
                    printDocumentAdapter$WriteResultCallback.onWriteCancelled();
                    return;
                }
                if (t == null) {
                    printDocumentAdapter$WriteResultCallback.onWriteFinished(new PageRange[] { PageRange.ALL_PAGES });
                    return;
                }
                Log.e("PrintHelper", "Error writing printed content", t);
                printDocumentAdapter$WriteResultCallback.onWriteFailed((CharSequence)null);
            }
        }.execute((Object[])new Void[0]);
    }
    
    public interface OnPrintFinishCallback
    {
        void onFinish();
    }
    
    private class PrintBitmapAdapter extends PrintDocumentAdapter
    {
        private PrintAttributes mAttributes;
        private final Bitmap mBitmap;
        private final OnPrintFinishCallback mCallback;
        private final int mFittingMode;
        private final String mJobName;
        
        PrintBitmapAdapter(final String mJobName, final int mFittingMode, final Bitmap mBitmap, final OnPrintFinishCallback mCallback) {
            this.mJobName = mJobName;
            this.mFittingMode = mFittingMode;
            this.mBitmap = mBitmap;
            this.mCallback = mCallback;
        }
        
        public void onFinish() {
            final OnPrintFinishCallback mCallback = this.mCallback;
            if (mCallback != null) {
                mCallback.onFinish();
            }
        }
        
        public void onLayout(final PrintAttributes printAttributes, final PrintAttributes mAttributes, final CancellationSignal cancellationSignal, final PrintDocumentAdapter$LayoutResultCallback printDocumentAdapter$LayoutResultCallback, final Bundle bundle) {
            this.mAttributes = mAttributes;
            printDocumentAdapter$LayoutResultCallback.onLayoutFinished(new PrintDocumentInfo$Builder(this.mJobName).setContentType(1).setPageCount(1).build(), mAttributes.equals((Object)printAttributes) ^ true);
        }
        
        public void onWrite(final PageRange[] array, final ParcelFileDescriptor parcelFileDescriptor, final CancellationSignal cancellationSignal, final PrintDocumentAdapter$WriteResultCallback printDocumentAdapter$WriteResultCallback) {
            PrintHelper.this.writeBitmap(this.mAttributes, this.mFittingMode, this.mBitmap, parcelFileDescriptor, cancellationSignal, printDocumentAdapter$WriteResultCallback);
        }
    }
    
    private class PrintUriAdapter extends PrintDocumentAdapter
    {
        PrintAttributes mAttributes;
        Bitmap mBitmap;
        final OnPrintFinishCallback mCallback;
        final int mFittingMode;
        final Uri mImageFile;
        final String mJobName;
        AsyncTask<Uri, Boolean, Bitmap> mLoadBitmap;
        
        PrintUriAdapter(final String mJobName, final Uri mImageFile, final OnPrintFinishCallback mCallback, final int mFittingMode) {
            this.mJobName = mJobName;
            this.mImageFile = mImageFile;
            this.mCallback = mCallback;
            this.mFittingMode = mFittingMode;
            this.mBitmap = null;
        }
        
        void cancelLoad() {
            synchronized (PrintHelper.this.mLock) {
                if (PrintHelper.this.mDecodeOptions != null) {
                    if (Build$VERSION.SDK_INT < 24) {
                        PrintHelper.this.mDecodeOptions.requestCancelDecode();
                    }
                    PrintHelper.this.mDecodeOptions = null;
                }
            }
        }
        
        public void onFinish() {
            super.onFinish();
            this.cancelLoad();
            final AsyncTask<Uri, Boolean, Bitmap> mLoadBitmap = this.mLoadBitmap;
            if (mLoadBitmap != null) {
                mLoadBitmap.cancel(true);
            }
            final OnPrintFinishCallback mCallback = this.mCallback;
            if (mCallback != null) {
                mCallback.onFinish();
            }
            final Bitmap mBitmap = this.mBitmap;
            if (mBitmap != null) {
                mBitmap.recycle();
                this.mBitmap = null;
            }
        }
        
        public void onLayout(final PrintAttributes printAttributes, final PrintAttributes mAttributes, final CancellationSignal cancellationSignal, final PrintDocumentAdapter$LayoutResultCallback printDocumentAdapter$LayoutResultCallback, final Bundle bundle) {
            synchronized (this) {
                this.mAttributes = mAttributes;
                // monitorexit(this)
                if (cancellationSignal.isCanceled()) {
                    printDocumentAdapter$LayoutResultCallback.onLayoutCancelled();
                    return;
                }
                if (this.mBitmap != null) {
                    printDocumentAdapter$LayoutResultCallback.onLayoutFinished(new PrintDocumentInfo$Builder(this.mJobName).setContentType(1).setPageCount(1).build(), mAttributes.equals((Object)printAttributes) ^ true);
                    return;
                }
                this.mLoadBitmap = (AsyncTask<Uri, Boolean, Bitmap>)new AsyncTask<Uri, Boolean, Bitmap>() {
                    protected Bitmap doInBackground(final Uri... array) {
                        try {
                            return PrintHelper.this.loadConstrainedBitmap(PrintUriAdapter.this.mImageFile);
                        }
                        catch (FileNotFoundException ex) {
                            return null;
                        }
                    }
                    
                    protected void onCancelled(final Bitmap bitmap) {
                        printDocumentAdapter$LayoutResultCallback.onLayoutCancelled();
                        PrintUriAdapter.this.mLoadBitmap = null;
                    }
                    
                    protected void onPostExecute(final Bitmap bitmap) {
                        super.onPostExecute((Object)bitmap);
                        Object bitmap2 = bitmap;
                        Label_0109: {
                            if (bitmap != null) {
                                if (PrintHelper.PRINT_ACTIVITY_RESPECTS_ORIENTATION) {
                                    bitmap2 = bitmap;
                                    if (PrintHelper.this.mOrientation != 0) {
                                        break Label_0109;
                                    }
                                }
                                synchronized (this) {
                                    final PrintAttributes$MediaSize mediaSize = PrintUriAdapter.this.mAttributes.getMediaSize();
                                    // monitorexit(this)
                                    bitmap2 = bitmap;
                                    if (mediaSize != null) {
                                        bitmap2 = bitmap;
                                        if (mediaSize.isPortrait() != PrintHelper.isPortrait(bitmap)) {
                                            bitmap2 = new Matrix();
                                            ((Matrix)bitmap2).postRotate(90.0f);
                                            bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), (Matrix)bitmap2, true);
                                        }
                                    }
                                }
                            }
                        }
                        final PrintUriAdapter this$1 = PrintUriAdapter.this;
                        if ((this$1.mBitmap = (Bitmap)bitmap2) != null) {
                            printDocumentAdapter$LayoutResultCallback.onLayoutFinished(new PrintDocumentInfo$Builder(this$1.mJobName).setContentType(1).setPageCount(1).build(), true ^ mAttributes.equals((Object)printAttributes));
                        }
                        else {
                            printDocumentAdapter$LayoutResultCallback.onLayoutFailed((CharSequence)null);
                        }
                        PrintUriAdapter.this.mLoadBitmap = null;
                    }
                    
                    protected void onPreExecute() {
                        cancellationSignal.setOnCancelListener((CancellationSignal$OnCancelListener)new CancellationSignal$OnCancelListener() {
                            public void onCancel() {
                                PrintUriAdapter.this.cancelLoad();
                                AsyncTask.this.cancel(false);
                            }
                        });
                    }
                }.execute((Object[])new Uri[0]);
            }
        }
        
        public void onWrite(final PageRange[] array, final ParcelFileDescriptor parcelFileDescriptor, final CancellationSignal cancellationSignal, final PrintDocumentAdapter$WriteResultCallback printDocumentAdapter$WriteResultCallback) {
            PrintHelper.this.writeBitmap(this.mAttributes, this.mFittingMode, this.mBitmap, parcelFileDescriptor, cancellationSignal, printDocumentAdapter$WriteResultCallback);
        }
    }
}
