// 
// Decompiled by Procyon v0.5.36
// 

package androidx.constraintlayout.solver.widgets;

import androidx.constraintlayout.solver.ArrayRow;
import androidx.constraintlayout.solver.SolverVariable;
import java.util.ArrayList;
import androidx.constraintlayout.solver.LinearSystem;

class Chain
{
    private static final boolean DEBUG = false;
    
    static void applyChainConstraints(final ConstraintWidgetContainer constraintWidgetContainer, final LinearSystem linearSystem, final int n) {
        int i = 0;
        int n2;
        ChainHead[] array;
        int n3;
        if (n == 0) {
            n2 = constraintWidgetContainer.mHorizontalChainsSize;
            array = constraintWidgetContainer.mHorizontalChainsArray;
            n3 = 0;
        }
        else {
            n3 = 2;
            n2 = constraintWidgetContainer.mVerticalChainsSize;
            array = constraintWidgetContainer.mVerticalChainsArray;
        }
        while (i < n2) {
            final ChainHead chainHead = array[i];
            chainHead.define();
            if (constraintWidgetContainer.optimizeFor(4)) {
                if (!Optimizer.applyChainOptimized(constraintWidgetContainer, linearSystem, n, n3, chainHead)) {
                    applyChainConstraints(constraintWidgetContainer, linearSystem, n, n3, chainHead);
                }
            }
            else {
                applyChainConstraints(constraintWidgetContainer, linearSystem, n, n3, chainHead);
            }
            ++i;
        }
    }
    
    static void applyChainConstraints(final ConstraintWidgetContainer constraintWidgetContainer, final LinearSystem linearSystem, int n, int margin, final ChainHead chainHead) {
        final ConstraintWidget mFirst = chainHead.mFirst;
        final ConstraintWidget mLast = chainHead.mLast;
        final ConstraintWidget mFirstVisibleWidget = chainHead.mFirstVisibleWidget;
        final ConstraintWidget mLastVisibleWidget = chainHead.mLastVisibleWidget;
        final ConstraintWidget mHead = chainHead.mHead;
        final float mTotalWeight = chainHead.mTotalWeight;
        final ConstraintWidget mFirstMatchConstraintWidget = chainHead.mFirstMatchConstraintWidget;
        final ConstraintWidget mLastMatchConstraintWidget = chainHead.mLastMatchConstraintWidget;
        final boolean b = constraintWidgetContainer.mListDimensionBehaviors[n] == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        int n2 = 0;
        int n5 = 0;
        boolean b2 = false;
        Label_0205: {
            int n4 = 0;
            Label_0198: {
                int n3;
                if (n == 0) {
                    if (mHead.mHorizontalChainStyle == 0) {
                        n2 = 1;
                    }
                    else {
                        n2 = 0;
                    }
                    if (mHead.mHorizontalChainStyle == 1) {
                        n3 = 1;
                    }
                    else {
                        n3 = 0;
                    }
                    n4 = n2;
                    n5 = n3;
                    if (mHead.mHorizontalChainStyle != 2) {
                        break Label_0198;
                    }
                }
                else {
                    if (mHead.mVerticalChainStyle == 0) {
                        n2 = 1;
                    }
                    else {
                        n2 = 0;
                    }
                    if (mHead.mVerticalChainStyle == 1) {
                        n3 = 1;
                    }
                    else {
                        n3 = 0;
                    }
                    n4 = n2;
                    n5 = n3;
                    if (mHead.mVerticalChainStyle != 2) {
                        break Label_0198;
                    }
                }
                b2 = true;
                n5 = n3;
                break Label_0205;
            }
            b2 = false;
            n2 = n4;
        }
        ConstraintWidget constraintWidget = mFirst;
        final int n6 = 0;
        final int n7 = n2;
        int n8 = n6;
        while (true) {
            final ConstraintWidget constraintWidget2 = null;
            if (n8 != 0) {
                break;
            }
            final ConstraintAnchor constraintAnchor = constraintWidget.mListAnchors[margin];
            int n9;
            if (!b && !b2) {
                n9 = 4;
            }
            else {
                n9 = 1;
            }
            int margin2;
            final int n10 = margin2 = constraintAnchor.getMargin();
            if (constraintAnchor.mTarget != null) {
                margin2 = n10;
                if (constraintWidget != mFirst) {
                    margin2 = n10 + constraintAnchor.mTarget.getMargin();
                }
            }
            if (b2 && constraintWidget != mFirst && constraintWidget != mFirstVisibleWidget) {
                n9 = 6;
            }
            else if (n7 != 0 && b) {
                n9 = 4;
            }
            if (constraintAnchor.mTarget != null) {
                if (constraintWidget == mFirstVisibleWidget) {
                    linearSystem.addGreaterThan(constraintAnchor.mSolverVariable, constraintAnchor.mTarget.mSolverVariable, margin2, 5);
                }
                else {
                    linearSystem.addGreaterThan(constraintAnchor.mSolverVariable, constraintAnchor.mTarget.mSolverVariable, margin2, 6);
                }
                linearSystem.addEquality(constraintAnchor.mSolverVariable, constraintAnchor.mTarget.mSolverVariable, margin2, n9);
            }
            if (b) {
                if (constraintWidget.getVisibility() != 8 && constraintWidget.mListDimensionBehaviors[n] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    linearSystem.addGreaterThan(constraintWidget.mListAnchors[margin + 1].mSolverVariable, constraintWidget.mListAnchors[margin].mSolverVariable, 0, 5);
                }
                linearSystem.addGreaterThan(constraintWidget.mListAnchors[margin].mSolverVariable, constraintWidgetContainer.mListAnchors[margin].mSolverVariable, 0, 6);
            }
            final ConstraintAnchor mTarget = constraintWidget.mListAnchors[margin + 1].mTarget;
            ConstraintWidget constraintWidget3 = constraintWidget2;
            if (mTarget != null) {
                final ConstraintWidget mOwner = mTarget.mOwner;
                constraintWidget3 = constraintWidget2;
                if (mOwner.mListAnchors[margin].mTarget != null) {
                    if (mOwner.mListAnchors[margin].mTarget.mOwner != constraintWidget) {
                        constraintWidget3 = constraintWidget2;
                    }
                    else {
                        constraintWidget3 = mOwner;
                    }
                }
            }
            if (constraintWidget3 != null) {
                constraintWidget = constraintWidget3;
            }
            else {
                n8 = 1;
            }
        }
        if (mLastVisibleWidget != null) {
            final ConstraintAnchor[] mListAnchors = mLast.mListAnchors;
            final int n11 = margin + 1;
            if (mListAnchors[n11].mTarget != null) {
                final ConstraintAnchor constraintAnchor2 = mLastVisibleWidget.mListAnchors[n11];
                linearSystem.addLowerThan(constraintAnchor2.mSolverVariable, mLast.mListAnchors[n11].mTarget.mSolverVariable, -constraintAnchor2.getMargin(), 5);
            }
        }
        if (b) {
            final ConstraintAnchor[] mListAnchors2 = constraintWidgetContainer.mListAnchors;
            final int n12 = margin + 1;
            linearSystem.addGreaterThan(mListAnchors2[n12].mSolverVariable, mLast.mListAnchors[n12].mSolverVariable, mLast.mListAnchors[n12].getMargin(), 6);
        }
        final ArrayList<ConstraintWidget> mWeightedMatchConstraintsWidgets = chainHead.mWeightedMatchConstraintsWidgets;
        if (mWeightedMatchConstraintsWidgets != null) {
            final int size = mWeightedMatchConstraintsWidgets.size();
            if (size > 1) {
                float n13;
                if (chainHead.mHasUndefinedWeights && !chainHead.mHasComplexMatchWeights) {
                    n13 = (float)chainHead.mWidgetsMatchCount;
                }
                else {
                    n13 = mTotalWeight;
                }
                float n14 = 0.0f;
                ConstraintWidget constraintWidget4 = null;
                float n15;
                for (int i = 0; i < size; ++i, n14 = n15) {
                    final ConstraintWidget constraintWidget5 = mWeightedMatchConstraintsWidgets.get(i);
                    n15 = constraintWidget5.mWeight[n];
                    Label_0907: {
                        if (n15 < 0.0f) {
                            if (chainHead.mHasComplexMatchWeights) {
                                linearSystem.addEquality(constraintWidget5.mListAnchors[margin + 1].mSolverVariable, constraintWidget5.mListAnchors[margin].mSolverVariable, 0, 4);
                                break Label_0907;
                            }
                            n15 = 1.0f;
                        }
                        if (n15 != 0.0f) {
                            if (constraintWidget4 != null) {
                                final SolverVariable mSolverVariable = constraintWidget4.mListAnchors[margin].mSolverVariable;
                                final ConstraintAnchor[] mListAnchors3 = constraintWidget4.mListAnchors;
                                final int n16 = margin + 1;
                                final SolverVariable mSolverVariable2 = mListAnchors3[n16].mSolverVariable;
                                final SolverVariable mSolverVariable3 = constraintWidget5.mListAnchors[margin].mSolverVariable;
                                final SolverVariable mSolverVariable4 = constraintWidget5.mListAnchors[n16].mSolverVariable;
                                final ArrayRow row = linearSystem.createRow();
                                row.createRowEqualMatchDimensions(n14, n13, n15, mSolverVariable, mSolverVariable2, mSolverVariable3, mSolverVariable4);
                                linearSystem.addConstraint(row);
                            }
                            constraintWidget4 = constraintWidget5;
                            continue;
                        }
                        linearSystem.addEquality(constraintWidget5.mListAnchors[margin + 1].mSolverVariable, constraintWidget5.mListAnchors[margin].mSolverVariable, 0, 6);
                    }
                    n15 = n14;
                }
            }
        }
        if (mFirstVisibleWidget != null && (mFirstVisibleWidget == mLastVisibleWidget || b2)) {
            ConstraintAnchor constraintAnchor3 = mFirst.mListAnchors[margin];
            final ConstraintAnchor[] mListAnchors4 = mLast.mListAnchors;
            final int n17 = margin + 1;
            ConstraintAnchor constraintAnchor4 = mListAnchors4[n17];
            SolverVariable mSolverVariable5;
            if (mFirst.mListAnchors[margin].mTarget != null) {
                mSolverVariable5 = mFirst.mListAnchors[margin].mTarget.mSolverVariable;
            }
            else {
                mSolverVariable5 = null;
            }
            SolverVariable mSolverVariable6;
            if (mLast.mListAnchors[n17].mTarget != null) {
                mSolverVariable6 = mLast.mListAnchors[n17].mTarget.mSolverVariable;
            }
            else {
                mSolverVariable6 = null;
            }
            if (mFirstVisibleWidget == mLastVisibleWidget) {
                constraintAnchor3 = mFirstVisibleWidget.mListAnchors[margin];
                constraintAnchor4 = mFirstVisibleWidget.mListAnchors[n17];
            }
            if (mSolverVariable5 != null && mSolverVariable6 != null) {
                float n18;
                if (n == 0) {
                    n18 = mHead.mHorizontalBiasPercent;
                }
                else {
                    n18 = mHead.mVerticalBiasPercent;
                }
                n = constraintAnchor3.getMargin();
                linearSystem.addCentering(constraintAnchor3.mSolverVariable, mSolverVariable5, n, n18, mSolverVariable6, constraintAnchor4.mSolverVariable, constraintAnchor4.getMargin(), 5);
            }
        }
        else if (n7 != 0 && mFirstVisibleWidget != null) {
            final boolean b3 = chainHead.mWidgetsMatchCount > 0 && chainHead.mWidgetsCount == chainHead.mWidgetsMatchCount;
            ConstraintWidget constraintWidget7;
            ConstraintWidget constraintWidget8;
            for (ConstraintWidget constraintWidget6 = constraintWidget7 = mFirstVisibleWidget; constraintWidget6 != null; constraintWidget6 = constraintWidget8) {
                for (constraintWidget8 = constraintWidget6.mNextChainWidget[n]; constraintWidget8 != null && constraintWidget8.getVisibility() == 8; constraintWidget8 = constraintWidget8.mNextChainWidget[n]) {}
                if (constraintWidget8 != null || constraintWidget6 == mLastVisibleWidget) {
                    final ConstraintAnchor constraintAnchor5 = constraintWidget6.mListAnchors[margin];
                    final SolverVariable mSolverVariable7 = constraintAnchor5.mSolverVariable;
                    SolverVariable mSolverVariable8;
                    if (constraintAnchor5.mTarget != null) {
                        mSolverVariable8 = constraintAnchor5.mTarget.mSolverVariable;
                    }
                    else {
                        mSolverVariable8 = null;
                    }
                    SolverVariable solverVariable;
                    if (constraintWidget7 != constraintWidget6) {
                        solverVariable = constraintWidget7.mListAnchors[margin + 1].mSolverVariable;
                    }
                    else {
                        solverVariable = mSolverVariable8;
                        if (constraintWidget6 == mFirstVisibleWidget) {
                            solverVariable = mSolverVariable8;
                            if (constraintWidget7 == constraintWidget6) {
                                if (mFirst.mListAnchors[margin].mTarget != null) {
                                    solverVariable = mFirst.mListAnchors[margin].mTarget.mSolverVariable;
                                }
                                else {
                                    solverVariable = null;
                                }
                            }
                        }
                    }
                    final int margin3 = constraintAnchor5.getMargin();
                    final ConstraintAnchor[] mListAnchors5 = constraintWidget6.mListAnchors;
                    final int n19 = margin + 1;
                    final int margin4 = mListAnchors5[n19].getMargin();
                    ConstraintAnchor constraintAnchor6;
                    SolverVariable mSolverVariable9;
                    SolverVariable solverVariable2;
                    if (constraintWidget8 != null) {
                        constraintAnchor6 = constraintWidget8.mListAnchors[margin];
                        mSolverVariable9 = constraintAnchor6.mSolverVariable;
                        solverVariable2 = constraintWidget6.mListAnchors[n19].mSolverVariable;
                    }
                    else {
                        final ConstraintAnchor mTarget2 = mLast.mListAnchors[n19].mTarget;
                        SolverVariable mSolverVariable10;
                        if (mTarget2 != null) {
                            mSolverVariable10 = mTarget2.mSolverVariable;
                        }
                        else {
                            mSolverVariable10 = null;
                        }
                        solverVariable2 = constraintWidget6.mListAnchors[n19].mSolverVariable;
                        mSolverVariable9 = mSolverVariable10;
                        constraintAnchor6 = mTarget2;
                    }
                    int margin5 = margin4;
                    if (constraintAnchor6 != null) {
                        margin5 = margin4 + constraintAnchor6.getMargin();
                    }
                    int margin6 = margin3;
                    if (constraintWidget7 != null) {
                        margin6 = margin3 + constraintWidget7.mListAnchors[n19].getMargin();
                    }
                    if (mSolverVariable7 != null && solverVariable != null && mSolverVariable9 != null && solverVariable2 != null) {
                        if (constraintWidget6 == mFirstVisibleWidget) {
                            margin6 = mFirstVisibleWidget.mListAnchors[margin].getMargin();
                        }
                        if (constraintWidget6 == mLastVisibleWidget) {
                            margin5 = mLastVisibleWidget.mListAnchors[n19].getMargin();
                        }
                        int n20;
                        if (b3) {
                            n20 = 6;
                        }
                        else {
                            n20 = 4;
                        }
                        linearSystem.addCentering(mSolverVariable7, solverVariable, margin6, 0.5f, mSolverVariable9, solverVariable2, margin5, n20);
                    }
                }
                if (constraintWidget6.getVisibility() != 8) {
                    constraintWidget7 = constraintWidget6;
                }
            }
        }
        else {
            int n21 = 8;
            if (n5 != 0 && mFirstVisibleWidget != null) {
                final boolean b4 = chainHead.mWidgetsMatchCount > 0 && chainHead.mWidgetsCount == chainHead.mWidgetsMatchCount;
                ConstraintWidget constraintWidget10;
                ConstraintWidget constraintWidget11;
                for (ConstraintWidget constraintWidget9 = constraintWidget10 = mFirstVisibleWidget; constraintWidget9 != null; constraintWidget9 = constraintWidget11) {
                    for (constraintWidget11 = constraintWidget9.mNextChainWidget[n]; constraintWidget11 != null && constraintWidget11.getVisibility() == n21; constraintWidget11 = constraintWidget11.mNextChainWidget[n]) {}
                    if (constraintWidget9 != mFirstVisibleWidget && constraintWidget9 != mLastVisibleWidget && constraintWidget11 != null) {
                        if (constraintWidget11 == mLastVisibleWidget) {
                            constraintWidget11 = null;
                        }
                        final ConstraintAnchor constraintAnchor7 = constraintWidget9.mListAnchors[margin];
                        final SolverVariable mSolverVariable11 = constraintAnchor7.mSolverVariable;
                        if (constraintAnchor7.mTarget != null) {
                            final SolverVariable mSolverVariable12 = constraintAnchor7.mTarget.mSolverVariable;
                        }
                        final ConstraintAnchor[] mListAnchors6 = constraintWidget10.mListAnchors;
                        final int n22 = margin + 1;
                        final SolverVariable mSolverVariable13 = mListAnchors6[n22].mSolverVariable;
                        final int margin7 = constraintAnchor7.getMargin();
                        final int margin8 = constraintWidget9.mListAnchors[n22].getMargin();
                        ConstraintAnchor constraintAnchor8;
                        SolverVariable mSolverVariable14;
                        SolverVariable solverVariable3;
                        if (constraintWidget11 != null) {
                            constraintAnchor8 = constraintWidget11.mListAnchors[margin];
                            mSolverVariable14 = constraintAnchor8.mSolverVariable;
                            if (constraintAnchor8.mTarget != null) {
                                solverVariable3 = constraintAnchor8.mTarget.mSolverVariable;
                            }
                            else {
                                solverVariable3 = null;
                            }
                        }
                        else {
                            final ConstraintAnchor mTarget3 = constraintWidget9.mListAnchors[n22].mTarget;
                            SolverVariable mSolverVariable15;
                            if (mTarget3 != null) {
                                mSolverVariable15 = mTarget3.mSolverVariable;
                            }
                            else {
                                mSolverVariable15 = null;
                            }
                            solverVariable3 = constraintWidget9.mListAnchors[n22].mSolverVariable;
                            mSolverVariable14 = mSolverVariable15;
                            constraintAnchor8 = mTarget3;
                        }
                        int n23 = margin8;
                        if (constraintAnchor8 != null) {
                            n23 = margin8 + constraintAnchor8.getMargin();
                        }
                        int n24 = margin7;
                        if (constraintWidget10 != null) {
                            n24 = margin7 + constraintWidget10.mListAnchors[n22].getMargin();
                        }
                        int n25;
                        if (b4) {
                            n25 = 6;
                        }
                        else {
                            n25 = 4;
                        }
                        if (mSolverVariable11 != null && mSolverVariable13 != null && mSolverVariable14 != null && solverVariable3 != null) {
                            linearSystem.addCentering(mSolverVariable11, mSolverVariable13, n24, 0.5f, mSolverVariable14, solverVariable3, n23, n25);
                        }
                        n21 = 8;
                    }
                    if (constraintWidget9.getVisibility() == n21) {
                        constraintWidget9 = constraintWidget10;
                    }
                    constraintWidget10 = constraintWidget9;
                }
                final ConstraintAnchor constraintAnchor9 = mFirstVisibleWidget.mListAnchors[margin];
                final ConstraintAnchor mTarget4 = mFirst.mListAnchors[margin].mTarget;
                final ConstraintAnchor[] mListAnchors7 = mLastVisibleWidget.mListAnchors;
                n = margin + 1;
                final ConstraintAnchor constraintAnchor10 = mListAnchors7[n];
                final ConstraintAnchor mTarget5 = mLast.mListAnchors[n].mTarget;
                if (mTarget4 != null) {
                    if (mFirstVisibleWidget != mLastVisibleWidget) {
                        linearSystem.addEquality(constraintAnchor9.mSolverVariable, mTarget4.mSolverVariable, constraintAnchor9.getMargin(), 5);
                    }
                    else if (mTarget5 != null) {
                        linearSystem.addCentering(constraintAnchor9.mSolverVariable, mTarget4.mSolverVariable, constraintAnchor9.getMargin(), 0.5f, constraintAnchor10.mSolverVariable, mTarget5.mSolverVariable, constraintAnchor10.getMargin(), 5);
                    }
                }
                if (mTarget5 != null && mFirstVisibleWidget != mLastVisibleWidget) {
                    linearSystem.addEquality(constraintAnchor10.mSolverVariable, mTarget5.mSolverVariable, -constraintAnchor10.getMargin(), 5);
                }
            }
        }
        if ((n7 != 0 || n5 != 0) && mFirstVisibleWidget != null) {
            ConstraintAnchor constraintAnchor11 = mFirstVisibleWidget.mListAnchors[margin];
            final ConstraintAnchor[] mListAnchors8 = mLastVisibleWidget.mListAnchors;
            n = margin + 1;
            ConstraintAnchor constraintAnchor12 = mListAnchors8[n];
            SolverVariable mSolverVariable16;
            if (constraintAnchor11.mTarget != null) {
                mSolverVariable16 = constraintAnchor11.mTarget.mSolverVariable;
            }
            else {
                mSolverVariable16 = null;
            }
            SolverVariable solverVariable4;
            if (constraintAnchor12.mTarget != null) {
                solverVariable4 = constraintAnchor12.mTarget.mSolverVariable;
            }
            else {
                solverVariable4 = null;
            }
            if (mLast != mLastVisibleWidget) {
                final ConstraintAnchor constraintAnchor13 = mLast.mListAnchors[n];
                if (constraintAnchor13.mTarget != null) {
                    solverVariable4 = constraintAnchor13.mTarget.mSolverVariable;
                }
                else {
                    solverVariable4 = null;
                }
            }
            if (mFirstVisibleWidget == mLastVisibleWidget) {
                constraintAnchor11 = mFirstVisibleWidget.mListAnchors[margin];
                constraintAnchor12 = mFirstVisibleWidget.mListAnchors[n];
            }
            if (mSolverVariable16 != null && solverVariable4 != null) {
                margin = constraintAnchor11.getMargin();
                ConstraintWidget constraintWidget12;
                if (mLastVisibleWidget == null) {
                    constraintWidget12 = mLast;
                }
                else {
                    constraintWidget12 = mLastVisibleWidget;
                }
                n = constraintWidget12.mListAnchors[n].getMargin();
                linearSystem.addCentering(constraintAnchor11.mSolverVariable, mSolverVariable16, margin, 0.5f, solverVariable4, constraintAnchor12.mSolverVariable, n, 5);
            }
        }
    }
}
