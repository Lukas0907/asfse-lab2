// 
// Decompiled by Procyon v0.5.36
// 

package androidx.constraintlayout.solver.widgets;

import androidx.constraintlayout.solver.Metrics;
import androidx.constraintlayout.solver.LinearSystem;

public class Optimizer
{
    static final int FLAG_CHAIN_DANGLING = 1;
    static final int FLAG_RECOMPUTE_BOUNDS = 2;
    static final int FLAG_USE_OPTIMIZE = 0;
    public static final int OPTIMIZATION_BARRIER = 2;
    public static final int OPTIMIZATION_CHAIN = 4;
    public static final int OPTIMIZATION_DIMENSIONS = 8;
    public static final int OPTIMIZATION_DIRECT = 1;
    public static final int OPTIMIZATION_GROUPS = 32;
    public static final int OPTIMIZATION_NONE = 0;
    public static final int OPTIMIZATION_RATIO = 16;
    public static final int OPTIMIZATION_STANDARD = 7;
    static boolean[] flags;
    
    static {
        Optimizer.flags = new boolean[3];
    }
    
    static void analyze(int n, final ConstraintWidget constraintWidget) {
        constraintWidget.updateResolutionNodes();
        final ResolutionAnchor resolutionNode = constraintWidget.mLeft.getResolutionNode();
        final ResolutionAnchor resolutionNode2 = constraintWidget.mTop.getResolutionNode();
        final ResolutionAnchor resolutionNode3 = constraintWidget.mRight.getResolutionNode();
        final ResolutionAnchor resolutionNode4 = constraintWidget.mBottom.getResolutionNode();
        if ((n & 0x8) == 0x8) {
            n = 1;
        }
        else {
            n = 0;
        }
        final boolean b = constraintWidget.mListDimensionBehaviors[0] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && optimizableMatchConstraint(constraintWidget, 0);
        if (resolutionNode.type != 4 && resolutionNode3.type != 4) {
            if (constraintWidget.mListDimensionBehaviors[0] != ConstraintWidget.DimensionBehaviour.FIXED && (!b || constraintWidget.getVisibility() != 8)) {
                if (b) {
                    final int width = constraintWidget.getWidth();
                    resolutionNode.setType(1);
                    resolutionNode3.setType(1);
                    if (constraintWidget.mLeft.mTarget == null && constraintWidget.mRight.mTarget == null) {
                        if (n != 0) {
                            resolutionNode3.dependsOn(resolutionNode, 1, constraintWidget.getResolutionWidth());
                        }
                        else {
                            resolutionNode3.dependsOn(resolutionNode, width);
                        }
                    }
                    else if (constraintWidget.mLeft.mTarget != null && constraintWidget.mRight.mTarget == null) {
                        if (n != 0) {
                            resolutionNode3.dependsOn(resolutionNode, 1, constraintWidget.getResolutionWidth());
                        }
                        else {
                            resolutionNode3.dependsOn(resolutionNode, width);
                        }
                    }
                    else if (constraintWidget.mLeft.mTarget == null && constraintWidget.mRight.mTarget != null) {
                        if (n != 0) {
                            resolutionNode.dependsOn(resolutionNode3, -1, constraintWidget.getResolutionWidth());
                        }
                        else {
                            resolutionNode.dependsOn(resolutionNode3, -width);
                        }
                    }
                    else if (constraintWidget.mLeft.mTarget != null && constraintWidget.mRight.mTarget != null) {
                        if (n != 0) {
                            constraintWidget.getResolutionWidth().addDependent(resolutionNode);
                            constraintWidget.getResolutionWidth().addDependent(resolutionNode3);
                        }
                        if (constraintWidget.mDimensionRatio == 0.0f) {
                            resolutionNode.setType(3);
                            resolutionNode3.setType(3);
                            resolutionNode.setOpposite(resolutionNode3, 0.0f);
                            resolutionNode3.setOpposite(resolutionNode, 0.0f);
                        }
                        else {
                            resolutionNode.setType(2);
                            resolutionNode3.setType(2);
                            resolutionNode.setOpposite(resolutionNode3, (float)(-width));
                            resolutionNode3.setOpposite(resolutionNode, (float)width);
                            constraintWidget.setWidth(width);
                        }
                    }
                }
            }
            else if (constraintWidget.mLeft.mTarget == null && constraintWidget.mRight.mTarget == null) {
                resolutionNode.setType(1);
                resolutionNode3.setType(1);
                if (n != 0) {
                    resolutionNode3.dependsOn(resolutionNode, 1, constraintWidget.getResolutionWidth());
                }
                else {
                    resolutionNode3.dependsOn(resolutionNode, constraintWidget.getWidth());
                }
            }
            else if (constraintWidget.mLeft.mTarget != null && constraintWidget.mRight.mTarget == null) {
                resolutionNode.setType(1);
                resolutionNode3.setType(1);
                if (n != 0) {
                    resolutionNode3.dependsOn(resolutionNode, 1, constraintWidget.getResolutionWidth());
                }
                else {
                    resolutionNode3.dependsOn(resolutionNode, constraintWidget.getWidth());
                }
            }
            else if (constraintWidget.mLeft.mTarget == null && constraintWidget.mRight.mTarget != null) {
                resolutionNode.setType(1);
                resolutionNode3.setType(1);
                resolutionNode.dependsOn(resolutionNode3, -constraintWidget.getWidth());
                if (n != 0) {
                    resolutionNode.dependsOn(resolutionNode3, -1, constraintWidget.getResolutionWidth());
                }
                else {
                    resolutionNode.dependsOn(resolutionNode3, -constraintWidget.getWidth());
                }
            }
            else if (constraintWidget.mLeft.mTarget != null && constraintWidget.mRight.mTarget != null) {
                resolutionNode.setType(2);
                resolutionNode3.setType(2);
                if (n != 0) {
                    constraintWidget.getResolutionWidth().addDependent(resolutionNode);
                    constraintWidget.getResolutionWidth().addDependent(resolutionNode3);
                    resolutionNode.setOpposite(resolutionNode3, -1, constraintWidget.getResolutionWidth());
                    resolutionNode3.setOpposite(resolutionNode, 1, constraintWidget.getResolutionWidth());
                }
                else {
                    resolutionNode.setOpposite(resolutionNode3, (float)(-constraintWidget.getWidth()));
                    resolutionNode3.setOpposite(resolutionNode, (float)constraintWidget.getWidth());
                }
            }
        }
        final boolean b2 = constraintWidget.mListDimensionBehaviors[1] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && optimizableMatchConstraint(constraintWidget, 1);
        if (resolutionNode2.type != 4 && resolutionNode4.type != 4) {
            if (constraintWidget.mListDimensionBehaviors[1] != ConstraintWidget.DimensionBehaviour.FIXED && (!b2 || constraintWidget.getVisibility() != 8)) {
                if (b2) {
                    final int height = constraintWidget.getHeight();
                    resolutionNode2.setType(1);
                    resolutionNode4.setType(1);
                    if (constraintWidget.mTop.mTarget == null && constraintWidget.mBottom.mTarget == null) {
                        if (n != 0) {
                            resolutionNode4.dependsOn(resolutionNode2, 1, constraintWidget.getResolutionHeight());
                            return;
                        }
                        resolutionNode4.dependsOn(resolutionNode2, height);
                    }
                    else if (constraintWidget.mTop.mTarget != null && constraintWidget.mBottom.mTarget == null) {
                        if (n != 0) {
                            resolutionNode4.dependsOn(resolutionNode2, 1, constraintWidget.getResolutionHeight());
                            return;
                        }
                        resolutionNode4.dependsOn(resolutionNode2, height);
                    }
                    else if (constraintWidget.mTop.mTarget == null && constraintWidget.mBottom.mTarget != null) {
                        if (n != 0) {
                            resolutionNode2.dependsOn(resolutionNode4, -1, constraintWidget.getResolutionHeight());
                            return;
                        }
                        resolutionNode2.dependsOn(resolutionNode4, -height);
                    }
                    else if (constraintWidget.mTop.mTarget != null && constraintWidget.mBottom.mTarget != null) {
                        if (n != 0) {
                            constraintWidget.getResolutionHeight().addDependent(resolutionNode2);
                            constraintWidget.getResolutionWidth().addDependent(resolutionNode4);
                        }
                        if (constraintWidget.mDimensionRatio == 0.0f) {
                            resolutionNode2.setType(3);
                            resolutionNode4.setType(3);
                            resolutionNode2.setOpposite(resolutionNode4, 0.0f);
                            resolutionNode4.setOpposite(resolutionNode2, 0.0f);
                            return;
                        }
                        resolutionNode2.setType(2);
                        resolutionNode4.setType(2);
                        resolutionNode2.setOpposite(resolutionNode4, (float)(-height));
                        resolutionNode4.setOpposite(resolutionNode2, (float)height);
                        constraintWidget.setHeight(height);
                        if (constraintWidget.mBaselineDistance > 0) {
                            constraintWidget.mBaseline.getResolutionNode().dependsOn(1, resolutionNode2, constraintWidget.mBaselineDistance);
                        }
                    }
                }
            }
            else if (constraintWidget.mTop.mTarget == null && constraintWidget.mBottom.mTarget == null) {
                resolutionNode2.setType(1);
                resolutionNode4.setType(1);
                if (n != 0) {
                    resolutionNode4.dependsOn(resolutionNode2, 1, constraintWidget.getResolutionHeight());
                }
                else {
                    resolutionNode4.dependsOn(resolutionNode2, constraintWidget.getHeight());
                }
                if (constraintWidget.mBaseline.mTarget != null) {
                    constraintWidget.mBaseline.getResolutionNode().setType(1);
                    resolutionNode2.dependsOn(1, constraintWidget.mBaseline.getResolutionNode(), -constraintWidget.mBaselineDistance);
                }
            }
            else if (constraintWidget.mTop.mTarget != null && constraintWidget.mBottom.mTarget == null) {
                resolutionNode2.setType(1);
                resolutionNode4.setType(1);
                if (n != 0) {
                    resolutionNode4.dependsOn(resolutionNode2, 1, constraintWidget.getResolutionHeight());
                }
                else {
                    resolutionNode4.dependsOn(resolutionNode2, constraintWidget.getHeight());
                }
                if (constraintWidget.mBaselineDistance > 0) {
                    constraintWidget.mBaseline.getResolutionNode().dependsOn(1, resolutionNode2, constraintWidget.mBaselineDistance);
                }
            }
            else if (constraintWidget.mTop.mTarget == null && constraintWidget.mBottom.mTarget != null) {
                resolutionNode2.setType(1);
                resolutionNode4.setType(1);
                if (n != 0) {
                    resolutionNode2.dependsOn(resolutionNode4, -1, constraintWidget.getResolutionHeight());
                }
                else {
                    resolutionNode2.dependsOn(resolutionNode4, -constraintWidget.getHeight());
                }
                if (constraintWidget.mBaselineDistance > 0) {
                    constraintWidget.mBaseline.getResolutionNode().dependsOn(1, resolutionNode2, constraintWidget.mBaselineDistance);
                }
            }
            else if (constraintWidget.mTop.mTarget != null && constraintWidget.mBottom.mTarget != null) {
                resolutionNode2.setType(2);
                resolutionNode4.setType(2);
                if (n != 0) {
                    resolutionNode2.setOpposite(resolutionNode4, -1, constraintWidget.getResolutionHeight());
                    resolutionNode4.setOpposite(resolutionNode2, 1, constraintWidget.getResolutionHeight());
                    constraintWidget.getResolutionHeight().addDependent(resolutionNode2);
                    constraintWidget.getResolutionWidth().addDependent(resolutionNode4);
                }
                else {
                    resolutionNode2.setOpposite(resolutionNode4, (float)(-constraintWidget.getHeight()));
                    resolutionNode4.setOpposite(resolutionNode2, (float)constraintWidget.getHeight());
                }
                if (constraintWidget.mBaselineDistance > 0) {
                    constraintWidget.mBaseline.getResolutionNode().dependsOn(1, resolutionNode2, constraintWidget.mBaselineDistance);
                }
            }
        }
    }
    
    static boolean applyChainOptimized(final ConstraintWidgetContainer constraintWidgetContainer, final LinearSystem linearSystem, final int n, final int n2, final ChainHead chainHead) {
        final ConstraintWidget mFirst = chainHead.mFirst;
        final ConstraintWidget mLast = chainHead.mLast;
        final ConstraintWidget mFirstVisibleWidget = chainHead.mFirstVisibleWidget;
        final ConstraintWidget mLastVisibleWidget = chainHead.mLastVisibleWidget;
        final ConstraintWidget mHead = chainHead.mHead;
        final float mTotalWeight = chainHead.mTotalWeight;
        final ConstraintWidget mFirstMatchConstraintWidget = chainHead.mFirstMatchConstraintWidget;
        final ConstraintWidget mLastMatchConstraintWidget = chainHead.mLastMatchConstraintWidget;
        final ConstraintWidget.DimensionBehaviour dimensionBehaviour = constraintWidgetContainer.mListDimensionBehaviors[n];
        final ConstraintWidget.DimensionBehaviour wrap_CONTENT = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        int n3 = 0;
        int n4 = 0;
        int n5 = 0;
        Label_0214: {
            boolean b3 = false;
            Label_0141: {
                boolean b4;
                if (n == 0) {
                    final boolean b = mHead.mHorizontalChainStyle == 0;
                    final boolean b2 = mHead.mHorizontalChainStyle == 1;
                    b3 = b;
                    n3 = (b2 ? 1 : 0);
                    if (mHead.mHorizontalChainStyle != 2) {
                        break Label_0141;
                    }
                    n3 = (b2 ? 1 : 0);
                    b4 = b;
                }
                else {
                    final boolean b5 = mHead.mVerticalChainStyle == 0;
                    final boolean b6 = mHead.mVerticalChainStyle == 1;
                    b3 = b5;
                    n3 = (b6 ? 1 : 0);
                    if (mHead.mVerticalChainStyle != 2) {
                        break Label_0141;
                    }
                    b4 = b5;
                    n3 = (b6 ? 1 : 0);
                }
                n4 = 1;
                n5 = (b4 ? 1 : 0);
                break Label_0214;
            }
            n4 = 0;
            n5 = (b3 ? 1 : 0);
        }
        ConstraintWidget constraintWidget = mFirst;
        int n6 = 0;
        int i = 0;
        int n7 = 0;
        float n8 = 0.0f;
        float n9 = 0.0f;
        while (i == 0) {
            int n10 = n7;
            float n11 = n8;
            float n12 = n9;
            if (constraintWidget.getVisibility() != 8) {
                n10 = n7 + 1;
                int n13;
                if (n == 0) {
                    n13 = constraintWidget.getWidth();
                }
                else {
                    n13 = constraintWidget.getHeight();
                }
                float n15;
                final float n14 = n15 = n8 + n13;
                if (constraintWidget != mFirstVisibleWidget) {
                    n15 = n14 + constraintWidget.mListAnchors[n2].getMargin();
                }
                n11 = n15;
                if (constraintWidget != mLastVisibleWidget) {
                    n11 = n15 + constraintWidget.mListAnchors[n2 + 1].getMargin();
                }
                n12 = n9 + constraintWidget.mListAnchors[n2].getMargin() + constraintWidget.mListAnchors[n2 + 1].getMargin();
            }
            final ConstraintAnchor constraintAnchor = constraintWidget.mListAnchors[n2];
            int n16 = n6;
            Label_0498: {
                if (constraintWidget.getVisibility() != 8) {
                    n16 = n6;
                    if (constraintWidget.mListDimensionBehaviors[n] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                        n16 = n6 + 1;
                        if (n == 0) {
                            if (constraintWidget.mMatchConstraintDefaultWidth != 0) {
                                return false;
                            }
                            if (constraintWidget.mMatchConstraintMinWidth != 0 || constraintWidget.mMatchConstraintMaxWidth != 0) {
                                return false;
                            }
                        }
                        else {
                            if (constraintWidget.mMatchConstraintDefaultHeight != 0) {
                                return false;
                            }
                            if (constraintWidget.mMatchConstraintMinHeight != 0) {
                                return false;
                            }
                            if (constraintWidget.mMatchConstraintMaxHeight != 0) {
                                return false;
                            }
                        }
                        if (constraintWidget.mDimensionRatio == 0.0f) {
                            break Label_0498;
                        }
                        return false;
                    }
                }
            }
            final ConstraintAnchor mTarget = constraintWidget.mListAnchors[n2 + 1].mTarget;
            ConstraintWidget mOwner = null;
            Label_0557: {
                if (mTarget != null) {
                    mOwner = mTarget.mOwner;
                    if (mOwner.mListAnchors[n2].mTarget != null) {
                        if (mOwner.mListAnchors[n2].mTarget.mOwner == constraintWidget) {
                            break Label_0557;
                        }
                    }
                }
                mOwner = null;
            }
            if (mOwner != null) {
                n6 = n16;
                constraintWidget = mOwner;
                n7 = n10;
                n8 = n11;
                n9 = n12;
            }
            else {
                i = 1;
                n6 = n16;
                n7 = n10;
                n8 = n11;
                n9 = n12;
            }
        }
        final ResolutionAnchor resolutionNode = mFirst.mListAnchors[n2].getResolutionNode();
        final ConstraintAnchor[] mListAnchors = mLast.mListAnchors;
        final int n17 = n2 + 1;
        final ResolutionAnchor resolutionNode2 = mListAnchors[n17].getResolutionNode();
        if (resolutionNode.target == null || resolutionNode2.target == null) {
            return false;
        }
        if (resolutionNode.target.state != 1 || resolutionNode2.target.state != 1) {
            return false;
        }
        if (n6 > 0 && n6 != n7) {
            return false;
        }
        float n18;
        if (n4 == 0 && n5 == 0 && n3 == 0) {
            n18 = 0.0f;
        }
        else {
            float n19;
            if (mFirstVisibleWidget != null) {
                n19 = (float)mFirstVisibleWidget.mListAnchors[n2].getMargin();
            }
            else {
                n19 = 0.0f;
            }
            n18 = n19;
            if (mLastVisibleWidget != null) {
                n18 = n19 + mLastVisibleWidget.mListAnchors[n17].getMargin();
            }
        }
        float resolvedOffset = resolutionNode.target.resolvedOffset;
        final float resolvedOffset2 = resolutionNode2.target.resolvedOffset;
        float n20;
        if (resolvedOffset < resolvedOffset2) {
            n20 = resolvedOffset2 - resolvedOffset;
        }
        else {
            n20 = resolvedOffset - resolvedOffset2;
        }
        final float n21 = n20 - n8;
        if (n6 <= 0 || n6 != n7) {
            if (n21 < 0.0f) {
                n4 = 1;
                n5 = 0;
                n3 = 0;
            }
            if (n4 != 0) {
                ConstraintWidget constraintWidget2 = mFirst;
                float n22 = resolvedOffset + (n21 - n18) * constraintWidget2.getBiasPercent(n);
                while (constraintWidget2 != null) {
                    if (LinearSystem.sMetrics != null) {
                        final Metrics sMetrics = LinearSystem.sMetrics;
                        --sMetrics.nonresolvedWidgets;
                        final Metrics sMetrics2 = LinearSystem.sMetrics;
                        ++sMetrics2.resolvedWidgets;
                        final Metrics sMetrics3 = LinearSystem.sMetrics;
                        ++sMetrics3.chainConnectionResolved;
                    }
                    final ConstraintWidget constraintWidget3 = constraintWidget2.mNextChainWidget[n];
                    float n23 = 0.0f;
                    Label_1397: {
                        if (constraintWidget3 == null) {
                            n23 = n22;
                            if (constraintWidget2 != mLast) {
                                break Label_1397;
                            }
                        }
                        int n24;
                        if (n == 0) {
                            n24 = constraintWidget2.getWidth();
                        }
                        else {
                            n24 = constraintWidget2.getHeight();
                        }
                        final float n25 = (float)n24;
                        final float n26 = n22 + constraintWidget2.mListAnchors[n2].getMargin();
                        constraintWidget2.mListAnchors[n2].getResolutionNode().resolve(resolutionNode.resolvedTarget, n26);
                        final ResolutionAnchor resolutionNode3 = constraintWidget2.mListAnchors[n17].getResolutionNode();
                        final ResolutionAnchor resolvedTarget = resolutionNode.resolvedTarget;
                        final float n27 = n26 + n25;
                        resolutionNode3.resolve(resolvedTarget, n27);
                        constraintWidget2.mListAnchors[n2].getResolutionNode().addResolvedValue(linearSystem);
                        constraintWidget2.mListAnchors[n17].getResolutionNode().addResolvedValue(linearSystem);
                        n23 = n27 + constraintWidget2.mListAnchors[n17].getMargin();
                    }
                    constraintWidget2 = constraintWidget3;
                    n22 = n23;
                }
            }
            else if (n5 != 0 || n3 != 0) {
                float n28 = 0.0f;
                Label_1449: {
                    if (n5 == 0) {
                        n28 = n21;
                        if (n3 == 0) {
                            break Label_1449;
                        }
                    }
                    n28 = n21 - n18;
                }
                float n29 = n28 / (n7 + 1);
                if (n3 != 0) {
                    float n30;
                    if (n7 > 1) {
                        n30 = (float)(n7 - 1);
                    }
                    else {
                        n30 = 2.0f;
                    }
                    n29 = n28 / n30;
                }
                float n31;
                if (mFirst.getVisibility() != 8) {
                    n31 = resolvedOffset + n29;
                }
                else {
                    n31 = resolvedOffset;
                }
                float n32 = n31;
                if (n3 != 0) {
                    n32 = n31;
                    if (n7 > 1) {
                        n32 = mFirstVisibleWidget.mListAnchors[n2].getMargin() + resolvedOffset;
                    }
                }
                ConstraintWidget constraintWidget4 = mFirst;
                float n33 = n32;
                if (n5 != 0) {
                    constraintWidget4 = mFirst;
                    n33 = n32;
                    if (mFirstVisibleWidget != null) {
                        n33 = n32 + mFirstVisibleWidget.mListAnchors[n2].getMargin();
                        constraintWidget4 = mFirst;
                    }
                }
                while (constraintWidget4 != null) {
                    if (LinearSystem.sMetrics != null) {
                        final Metrics sMetrics4 = LinearSystem.sMetrics;
                        --sMetrics4.nonresolvedWidgets;
                        final Metrics sMetrics5 = LinearSystem.sMetrics;
                        ++sMetrics5.resolvedWidgets;
                        final Metrics sMetrics6 = LinearSystem.sMetrics;
                        ++sMetrics6.chainConnectionResolved;
                    }
                    final ConstraintWidget constraintWidget5 = constraintWidget4.mNextChainWidget[n];
                    Label_1853: {
                        float n38;
                        while (true) {
                            Label_1686: {
                                if (constraintWidget5 != null) {
                                    break Label_1686;
                                }
                                final float n34 = n33;
                                if (constraintWidget4 == mLast) {
                                    break Label_1686;
                                }
                                n33 = n34;
                                break Label_1853;
                            }
                            int n35;
                            if (n == 0) {
                                n35 = constraintWidget4.getWidth();
                            }
                            else {
                                n35 = constraintWidget4.getHeight();
                            }
                            final float n36 = (float)n35;
                            float n37 = n33;
                            if (constraintWidget4 != mFirstVisibleWidget) {
                                n37 = n33 + constraintWidget4.mListAnchors[n2].getMargin();
                            }
                            constraintWidget4.mListAnchors[n2].getResolutionNode().resolve(resolutionNode.resolvedTarget, n37);
                            constraintWidget4.mListAnchors[n17].getResolutionNode().resolve(resolutionNode.resolvedTarget, n37 + n36);
                            constraintWidget4.mListAnchors[n2].getResolutionNode().addResolvedValue(linearSystem);
                            constraintWidget4.mListAnchors[n17].getResolutionNode().addResolvedValue(linearSystem);
                            float n34;
                            n38 = (n34 = n37 + (n36 + constraintWidget4.mListAnchors[n17].getMargin()));
                            if (constraintWidget5 == null) {
                                continue;
                            }
                            break;
                        }
                        n33 = n38;
                        if (constraintWidget5.getVisibility() != 8) {
                            n33 = n38 + n29;
                        }
                    }
                    constraintWidget4 = constraintWidget5;
                }
            }
            return true;
        }
        if (constraintWidget.getParent() != null && constraintWidget.getParent().mListDimensionBehaviors[n] == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
            return false;
        }
        final float n39 = n21 + n8 - n9;
        ConstraintWidget constraintWidget7;
        float n40;
        for (ConstraintWidget constraintWidget6 = mFirst; constraintWidget6 != null; constraintWidget6 = constraintWidget7, resolvedOffset = n40) {
            if (LinearSystem.sMetrics != null) {
                final Metrics sMetrics7 = LinearSystem.sMetrics;
                --sMetrics7.nonresolvedWidgets;
                final Metrics sMetrics8 = LinearSystem.sMetrics;
                ++sMetrics8.resolvedWidgets;
                final Metrics sMetrics9 = LinearSystem.sMetrics;
                ++sMetrics9.chainConnectionResolved;
            }
            constraintWidget7 = constraintWidget6.mNextChainWidget[n];
            if (constraintWidget7 == null) {
                n40 = resolvedOffset;
                if (constraintWidget6 != mLast) {
                    continue;
                }
            }
            float n41 = n39 / n6;
            if (mTotalWeight > 0.0f) {
                if (constraintWidget6.mWeight[n] == -1.0f) {
                    n41 = 0.0f;
                }
                else {
                    n41 = constraintWidget6.mWeight[n] * n39 / mTotalWeight;
                }
            }
            if (constraintWidget6.getVisibility() == 8) {
                n41 = 0.0f;
            }
            final float n42 = resolvedOffset + constraintWidget6.mListAnchors[n2].getMargin();
            constraintWidget6.mListAnchors[n2].getResolutionNode().resolve(resolutionNode.resolvedTarget, n42);
            final ResolutionAnchor resolutionNode4 = constraintWidget6.mListAnchors[n17].getResolutionNode();
            final ResolutionAnchor resolvedTarget2 = resolutionNode.resolvedTarget;
            final float n43 = n42 + n41;
            resolutionNode4.resolve(resolvedTarget2, n43);
            constraintWidget6.mListAnchors[n2].getResolutionNode().addResolvedValue(linearSystem);
            constraintWidget6.mListAnchors[n17].getResolutionNode().addResolvedValue(linearSystem);
            n40 = n43 + constraintWidget6.mListAnchors[n17].getMargin();
        }
        return true;
    }
    
    static void checkMatchParent(final ConstraintWidgetContainer constraintWidgetContainer, final LinearSystem linearSystem, final ConstraintWidget constraintWidget) {
        if (constraintWidgetContainer.mListDimensionBehaviors[0] != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT && constraintWidget.mListDimensionBehaviors[0] == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
            final int mMargin = constraintWidget.mLeft.mMargin;
            final int n = constraintWidgetContainer.getWidth() - constraintWidget.mRight.mMargin;
            constraintWidget.mLeft.mSolverVariable = linearSystem.createObjectVariable(constraintWidget.mLeft);
            constraintWidget.mRight.mSolverVariable = linearSystem.createObjectVariable(constraintWidget.mRight);
            linearSystem.addEquality(constraintWidget.mLeft.mSolverVariable, mMargin);
            linearSystem.addEquality(constraintWidget.mRight.mSolverVariable, n);
            constraintWidget.mHorizontalResolution = 2;
            constraintWidget.setHorizontalDimension(mMargin, n);
        }
        if (constraintWidgetContainer.mListDimensionBehaviors[1] != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT && constraintWidget.mListDimensionBehaviors[1] == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
            final int mMargin2 = constraintWidget.mTop.mMargin;
            final int n2 = constraintWidgetContainer.getHeight() - constraintWidget.mBottom.mMargin;
            constraintWidget.mTop.mSolverVariable = linearSystem.createObjectVariable(constraintWidget.mTop);
            constraintWidget.mBottom.mSolverVariable = linearSystem.createObjectVariable(constraintWidget.mBottom);
            linearSystem.addEquality(constraintWidget.mTop.mSolverVariable, mMargin2);
            linearSystem.addEquality(constraintWidget.mBottom.mSolverVariable, n2);
            if (constraintWidget.mBaselineDistance > 0 || constraintWidget.getVisibility() == 8) {
                linearSystem.addEquality(constraintWidget.mBaseline.mSolverVariable = linearSystem.createObjectVariable(constraintWidget.mBaseline), constraintWidget.mBaselineDistance + mMargin2);
            }
            constraintWidget.mVerticalResolution = 2;
            constraintWidget.setVerticalDimension(mMargin2, n2);
        }
    }
    
    private static boolean optimizableMatchConstraint(final ConstraintWidget constraintWidget, int n) {
        if (constraintWidget.mListDimensionBehaviors[n] != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            return false;
        }
        final float mDimensionRatio = constraintWidget.mDimensionRatio;
        final int n2 = 1;
        if (mDimensionRatio != 0.0f) {
            final ConstraintWidget.DimensionBehaviour[] mListDimensionBehaviors = constraintWidget.mListDimensionBehaviors;
            if (n == 0) {
                n = n2;
            }
            else {
                n = 0;
            }
            if (mListDimensionBehaviors[n] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {}
            return false;
        }
        if (n == 0) {
            if (constraintWidget.mMatchConstraintDefaultWidth != 0) {
                return false;
            }
            if (constraintWidget.mMatchConstraintMinWidth != 0 || constraintWidget.mMatchConstraintMaxWidth != 0) {
                return false;
            }
        }
        else {
            if (constraintWidget.mMatchConstraintDefaultHeight != 0) {
                return false;
            }
            if (constraintWidget.mMatchConstraintMinHeight != 0) {
                return false;
            }
            if (constraintWidget.mMatchConstraintMaxHeight != 0) {
                return false;
            }
        }
        return true;
    }
    
    static void setOptimizedWidget(final ConstraintWidget constraintWidget, final int n, final int n2) {
        final int n3 = n * 2;
        final int n4 = n3 + 1;
        constraintWidget.mListAnchors[n3].getResolutionNode().resolvedTarget = constraintWidget.getParent().mLeft.getResolutionNode();
        constraintWidget.mListAnchors[n3].getResolutionNode().resolvedOffset = (float)n2;
        constraintWidget.mListAnchors[n3].getResolutionNode().state = 1;
        constraintWidget.mListAnchors[n4].getResolutionNode().resolvedTarget = constraintWidget.mListAnchors[n3].getResolutionNode();
        constraintWidget.mListAnchors[n4].getResolutionNode().resolvedOffset = (float)constraintWidget.getLength(n);
        constraintWidget.mListAnchors[n4].getResolutionNode().state = 1;
    }
}
