// 
// Decompiled by Procyon v0.5.36
// 

package androidx.constraintlayout.solver.widgets;

import androidx.constraintlayout.solver.Cache;
import androidx.constraintlayout.solver.Metrics;
import androidx.constraintlayout.solver.SolverVariable;
import androidx.constraintlayout.solver.LinearSystem;
import java.util.ArrayList;

public class ConstraintWidget
{
    protected static final int ANCHOR_BASELINE = 4;
    protected static final int ANCHOR_BOTTOM = 3;
    protected static final int ANCHOR_LEFT = 0;
    protected static final int ANCHOR_RIGHT = 1;
    protected static final int ANCHOR_TOP = 2;
    private static final boolean AUTOTAG_CENTER = false;
    public static final int CHAIN_PACKED = 2;
    public static final int CHAIN_SPREAD = 0;
    public static final int CHAIN_SPREAD_INSIDE = 1;
    public static float DEFAULT_BIAS = 0.5f;
    static final int DIMENSION_HORIZONTAL = 0;
    static final int DIMENSION_VERTICAL = 1;
    protected static final int DIRECT = 2;
    public static final int GONE = 8;
    public static final int HORIZONTAL = 0;
    public static final int INVISIBLE = 4;
    public static final int MATCH_CONSTRAINT_PERCENT = 2;
    public static final int MATCH_CONSTRAINT_RATIO = 3;
    public static final int MATCH_CONSTRAINT_RATIO_RESOLVED = 4;
    public static final int MATCH_CONSTRAINT_SPREAD = 0;
    public static final int MATCH_CONSTRAINT_WRAP = 1;
    protected static final int SOLVER = 1;
    public static final int UNKNOWN = -1;
    public static final int VERTICAL = 1;
    public static final int VISIBLE = 0;
    private static final int WRAP = -2;
    protected ArrayList<ConstraintAnchor> mAnchors;
    ConstraintAnchor mBaseline;
    int mBaselineDistance;
    ConstraintWidgetGroup mBelongingGroup;
    ConstraintAnchor mBottom;
    boolean mBottomHasCentered;
    ConstraintAnchor mCenter;
    ConstraintAnchor mCenterX;
    ConstraintAnchor mCenterY;
    private float mCircleConstraintAngle;
    private Object mCompanionWidget;
    private int mContainerItemSkip;
    private String mDebugName;
    protected float mDimensionRatio;
    protected int mDimensionRatioSide;
    int mDistToBottom;
    int mDistToLeft;
    int mDistToRight;
    int mDistToTop;
    private int mDrawHeight;
    private int mDrawWidth;
    private int mDrawX;
    private int mDrawY;
    boolean mGroupsToSolver;
    int mHeight;
    float mHorizontalBiasPercent;
    boolean mHorizontalChainFixedPosition;
    int mHorizontalChainStyle;
    ConstraintWidget mHorizontalNextWidget;
    public int mHorizontalResolution;
    boolean mHorizontalWrapVisited;
    boolean mIsHeightWrapContent;
    boolean mIsWidthWrapContent;
    ConstraintAnchor mLeft;
    boolean mLeftHasCentered;
    protected ConstraintAnchor[] mListAnchors;
    protected DimensionBehaviour[] mListDimensionBehaviors;
    protected ConstraintWidget[] mListNextMatchConstraintsWidget;
    int mMatchConstraintDefaultHeight;
    int mMatchConstraintDefaultWidth;
    int mMatchConstraintMaxHeight;
    int mMatchConstraintMaxWidth;
    int mMatchConstraintMinHeight;
    int mMatchConstraintMinWidth;
    float mMatchConstraintPercentHeight;
    float mMatchConstraintPercentWidth;
    private int[] mMaxDimension;
    protected int mMinHeight;
    protected int mMinWidth;
    protected ConstraintWidget[] mNextChainWidget;
    protected int mOffsetX;
    protected int mOffsetY;
    boolean mOptimizerMeasurable;
    boolean mOptimizerMeasured;
    ConstraintWidget mParent;
    int mRelX;
    int mRelY;
    ResolutionDimension mResolutionHeight;
    ResolutionDimension mResolutionWidth;
    float mResolvedDimensionRatio;
    int mResolvedDimensionRatioSide;
    int[] mResolvedMatchConstraintDefault;
    ConstraintAnchor mRight;
    boolean mRightHasCentered;
    ConstraintAnchor mTop;
    boolean mTopHasCentered;
    private String mType;
    float mVerticalBiasPercent;
    boolean mVerticalChainFixedPosition;
    int mVerticalChainStyle;
    ConstraintWidget mVerticalNextWidget;
    public int mVerticalResolution;
    boolean mVerticalWrapVisited;
    private int mVisibility;
    float[] mWeight;
    int mWidth;
    private int mWrapHeight;
    private int mWrapWidth;
    protected int mX;
    protected int mY;
    
    public ConstraintWidget() {
        this.mHorizontalResolution = -1;
        this.mVerticalResolution = -1;
        this.mMatchConstraintDefaultWidth = 0;
        this.mMatchConstraintDefaultHeight = 0;
        this.mResolvedMatchConstraintDefault = new int[2];
        this.mMatchConstraintMinWidth = 0;
        this.mMatchConstraintMaxWidth = 0;
        this.mMatchConstraintPercentWidth = 1.0f;
        this.mMatchConstraintMinHeight = 0;
        this.mMatchConstraintMaxHeight = 0;
        this.mMatchConstraintPercentHeight = 1.0f;
        this.mResolvedDimensionRatioSide = -1;
        this.mResolvedDimensionRatio = 1.0f;
        this.mBelongingGroup = null;
        this.mMaxDimension = new int[] { Integer.MAX_VALUE, Integer.MAX_VALUE };
        this.mCircleConstraintAngle = 0.0f;
        this.mLeft = new ConstraintAnchor(this, ConstraintAnchor.Type.LEFT);
        this.mTop = new ConstraintAnchor(this, ConstraintAnchor.Type.TOP);
        this.mRight = new ConstraintAnchor(this, ConstraintAnchor.Type.RIGHT);
        this.mBottom = new ConstraintAnchor(this, ConstraintAnchor.Type.BOTTOM);
        this.mBaseline = new ConstraintAnchor(this, ConstraintAnchor.Type.BASELINE);
        this.mCenterX = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER_X);
        this.mCenterY = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER_Y);
        this.mCenter = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER);
        this.mListAnchors = new ConstraintAnchor[] { this.mLeft, this.mRight, this.mTop, this.mBottom, this.mBaseline, this.mCenter };
        this.mAnchors = new ArrayList<ConstraintAnchor>();
        this.mListDimensionBehaviors = new DimensionBehaviour[] { DimensionBehaviour.FIXED, DimensionBehaviour.FIXED };
        this.mParent = null;
        this.mWidth = 0;
        this.mHeight = 0;
        this.mDimensionRatio = 0.0f;
        this.mDimensionRatioSide = -1;
        this.mX = 0;
        this.mY = 0;
        this.mRelX = 0;
        this.mRelY = 0;
        this.mDrawX = 0;
        this.mDrawY = 0;
        this.mDrawWidth = 0;
        this.mDrawHeight = 0;
        this.mOffsetX = 0;
        this.mOffsetY = 0;
        this.mBaselineDistance = 0;
        final float default_BIAS = ConstraintWidget.DEFAULT_BIAS;
        this.mHorizontalBiasPercent = default_BIAS;
        this.mVerticalBiasPercent = default_BIAS;
        this.mContainerItemSkip = 0;
        this.mVisibility = 0;
        this.mDebugName = null;
        this.mType = null;
        this.mOptimizerMeasurable = false;
        this.mOptimizerMeasured = false;
        this.mGroupsToSolver = false;
        this.mHorizontalChainStyle = 0;
        this.mVerticalChainStyle = 0;
        this.mWeight = new float[] { -1.0f, -1.0f };
        this.mListNextMatchConstraintsWidget = new ConstraintWidget[] { null, null };
        this.mNextChainWidget = new ConstraintWidget[] { null, null };
        this.mHorizontalNextWidget = null;
        this.mVerticalNextWidget = null;
        this.addAnchors();
    }
    
    public ConstraintWidget(final int n, final int n2) {
        this(0, 0, n, n2);
    }
    
    public ConstraintWidget(final int mx, final int my, final int mWidth, final int mHeight) {
        this.mHorizontalResolution = -1;
        this.mVerticalResolution = -1;
        this.mMatchConstraintDefaultWidth = 0;
        this.mMatchConstraintDefaultHeight = 0;
        this.mResolvedMatchConstraintDefault = new int[2];
        this.mMatchConstraintMinWidth = 0;
        this.mMatchConstraintMaxWidth = 0;
        this.mMatchConstraintPercentWidth = 1.0f;
        this.mMatchConstraintMinHeight = 0;
        this.mMatchConstraintMaxHeight = 0;
        this.mMatchConstraintPercentHeight = 1.0f;
        this.mResolvedDimensionRatioSide = -1;
        this.mResolvedDimensionRatio = 1.0f;
        this.mBelongingGroup = null;
        this.mMaxDimension = new int[] { Integer.MAX_VALUE, Integer.MAX_VALUE };
        this.mCircleConstraintAngle = 0.0f;
        this.mLeft = new ConstraintAnchor(this, ConstraintAnchor.Type.LEFT);
        this.mTop = new ConstraintAnchor(this, ConstraintAnchor.Type.TOP);
        this.mRight = new ConstraintAnchor(this, ConstraintAnchor.Type.RIGHT);
        this.mBottom = new ConstraintAnchor(this, ConstraintAnchor.Type.BOTTOM);
        this.mBaseline = new ConstraintAnchor(this, ConstraintAnchor.Type.BASELINE);
        this.mCenterX = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER_X);
        this.mCenterY = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER_Y);
        this.mCenter = new ConstraintAnchor(this, ConstraintAnchor.Type.CENTER);
        this.mListAnchors = new ConstraintAnchor[] { this.mLeft, this.mRight, this.mTop, this.mBottom, this.mBaseline, this.mCenter };
        this.mAnchors = new ArrayList<ConstraintAnchor>();
        this.mListDimensionBehaviors = new DimensionBehaviour[] { DimensionBehaviour.FIXED, DimensionBehaviour.FIXED };
        this.mParent = null;
        this.mWidth = 0;
        this.mHeight = 0;
        this.mDimensionRatio = 0.0f;
        this.mDimensionRatioSide = -1;
        this.mX = 0;
        this.mY = 0;
        this.mRelX = 0;
        this.mRelY = 0;
        this.mDrawX = 0;
        this.mDrawY = 0;
        this.mDrawWidth = 0;
        this.mDrawHeight = 0;
        this.mOffsetX = 0;
        this.mOffsetY = 0;
        this.mBaselineDistance = 0;
        final float default_BIAS = ConstraintWidget.DEFAULT_BIAS;
        this.mHorizontalBiasPercent = default_BIAS;
        this.mVerticalBiasPercent = default_BIAS;
        this.mContainerItemSkip = 0;
        this.mVisibility = 0;
        this.mDebugName = null;
        this.mType = null;
        this.mOptimizerMeasurable = false;
        this.mOptimizerMeasured = false;
        this.mGroupsToSolver = false;
        this.mHorizontalChainStyle = 0;
        this.mVerticalChainStyle = 0;
        this.mWeight = new float[] { -1.0f, -1.0f };
        this.mListNextMatchConstraintsWidget = new ConstraintWidget[] { null, null };
        this.mNextChainWidget = new ConstraintWidget[] { null, null };
        this.mHorizontalNextWidget = null;
        this.mVerticalNextWidget = null;
        this.mX = mx;
        this.mY = my;
        this.mWidth = mWidth;
        this.mHeight = mHeight;
        this.addAnchors();
        this.forceUpdateDrawPosition();
    }
    
    private void addAnchors() {
        this.mAnchors.add(this.mLeft);
        this.mAnchors.add(this.mTop);
        this.mAnchors.add(this.mRight);
        this.mAnchors.add(this.mBottom);
        this.mAnchors.add(this.mCenterX);
        this.mAnchors.add(this.mCenterY);
        this.mAnchors.add(this.mCenter);
        this.mAnchors.add(this.mBaseline);
    }
    
    private void applyConstraints(final LinearSystem linearSystem, final boolean b, SolverVariable solverVariable, final SolverVariable solverVariable2, final DimensionBehaviour dimensionBehaviour, final boolean b2, final ConstraintAnchor constraintAnchor, final ConstraintAnchor constraintAnchor2, int n, int a, int margin, int margin2, final float n2, final boolean b3, final boolean b4, int b5, int max, int min, final float n3, final boolean b6) {
        final SolverVariable objectVariable = linearSystem.createObjectVariable(constraintAnchor);
        final SolverVariable objectVariable2 = linearSystem.createObjectVariable(constraintAnchor2);
        final SolverVariable objectVariable3 = linearSystem.createObjectVariable(constraintAnchor.getTarget());
        final SolverVariable objectVariable4 = linearSystem.createObjectVariable(constraintAnchor2.getTarget());
        if (linearSystem.graphOptimizer && constraintAnchor.getResolutionNode().state == 1 && constraintAnchor2.getResolutionNode().state == 1) {
            if (LinearSystem.getMetrics() != null) {
                final Metrics metrics = LinearSystem.getMetrics();
                ++metrics.resolvedWidgets;
            }
            constraintAnchor.getResolutionNode().addResolvedValue(linearSystem);
            constraintAnchor2.getResolutionNode().addResolvedValue(linearSystem);
            if (!b4 && b) {
                linearSystem.addGreaterThan(solverVariable2, objectVariable2, 0, 6);
            }
            return;
        }
        if (LinearSystem.getMetrics() != null) {
            final Metrics metrics2 = LinearSystem.getMetrics();
            ++metrics2.nonresolvedWidgets;
        }
        final boolean connected = constraintAnchor.isConnected();
        final boolean connected2 = constraintAnchor2.isConnected();
        final boolean connected3 = this.mCenter.isConnected();
        int n4;
        if (connected) {
            n4 = 1;
        }
        else {
            n4 = 0;
        }
        int n5 = n4;
        if (connected2) {
            n5 = n4 + 1;
        }
        int n6 = n5;
        if (connected3) {
            n6 = n5 + 1;
        }
        int n7;
        if (b3) {
            n7 = 3;
        }
        else {
            n7 = b5;
        }
        b5 = ConstraintWidget$1.$SwitchMap$androidx$constraintlayout$solver$widgets$ConstraintWidget$DimensionBehaviour[dimensionBehaviour.ordinal()];
        if (b5 != 1 && b5 != 2 && b5 != 3 && b5 == 4 && n7 != 4) {
            b5 = 1;
        }
        else {
            b5 = 0;
        }
        if (this.mVisibility == 8) {
            a = 0;
            b5 = 0;
        }
        if (b6) {
            if (!connected && !connected2 && !connected3) {
                linearSystem.addEquality(objectVariable, n);
            }
            else if (connected && !connected2) {
                linearSystem.addEquality(objectVariable, objectVariable3, constraintAnchor.getMargin(), 6);
            }
        }
        if (b5 == 0) {
            if (b2) {
                linearSystem.addEquality(objectVariable2, objectVariable, 0, 3);
                if (margin > 0) {
                    linearSystem.addGreaterThan(objectVariable2, objectVariable, margin, 6);
                }
                if (margin2 < Integer.MAX_VALUE) {
                    linearSystem.addLowerThan(objectVariable2, objectVariable, margin2, 6);
                }
            }
            else {
                linearSystem.addEquality(objectVariable2, objectVariable, a, 6);
            }
            margin2 = min;
            a = max;
        }
        else {
            if (max == -2) {
                margin2 = a;
            }
            else {
                margin2 = max;
            }
            n = min;
            if (min == -2) {
                n = a;
            }
            max = a;
            if (margin2 > 0) {
                linearSystem.addGreaterThan(objectVariable2, objectVariable, margin2, 6);
                max = Math.max(a, margin2);
            }
            min = max;
            if (n > 0) {
                linearSystem.addLowerThan(objectVariable2, objectVariable, n, 6);
                min = Math.min(max, n);
            }
            Label_0739: {
                if (n7 == 1) {
                    if (b) {
                        linearSystem.addEquality(objectVariable2, objectVariable, min, 6);
                    }
                    else if (b4) {
                        linearSystem.addEquality(objectVariable2, objectVariable, min, 4);
                    }
                    else {
                        linearSystem.addEquality(objectVariable2, objectVariable, min, 1);
                    }
                }
                else if (n7 == 2) {
                    SolverVariable solverVariable3;
                    SolverVariable solverVariable4;
                    if (constraintAnchor.getType() != ConstraintAnchor.Type.TOP && constraintAnchor.getType() != ConstraintAnchor.Type.BOTTOM) {
                        solverVariable3 = linearSystem.createObjectVariable(this.mParent.getAnchor(ConstraintAnchor.Type.LEFT));
                        solverVariable4 = linearSystem.createObjectVariable(this.mParent.getAnchor(ConstraintAnchor.Type.RIGHT));
                    }
                    else {
                        solverVariable3 = linearSystem.createObjectVariable(this.mParent.getAnchor(ConstraintAnchor.Type.TOP));
                        solverVariable4 = linearSystem.createObjectVariable(this.mParent.getAnchor(ConstraintAnchor.Type.BOTTOM));
                    }
                    linearSystem.addConstraint(linearSystem.createRow().createRowDimensionRatio(objectVariable2, objectVariable, solverVariable4, solverVariable3, n3));
                    a = 0;
                    break Label_0739;
                }
                a = b5;
            }
            if (a != 0 && n6 != 2 && !b3) {
                b5 = (a = Math.max(margin2, min));
                if (n > 0) {
                    a = Math.min(n, b5);
                }
                linearSystem.addEquality(objectVariable2, objectVariable, a, 6);
                a = margin2;
                b5 = 0;
                margin2 = n;
            }
            else {
                max = margin2;
                margin2 = n;
                b5 = a;
                a = max;
            }
        }
        final SolverVariable solverVariable5 = objectVariable3;
        if (b6 && !b4) {
            Label_1448: {
                if (!connected && !connected2 && !connected3) {
                    if (b) {
                        linearSystem.addGreaterThan(solverVariable2, objectVariable2, 0, 5);
                    }
                }
                else if (connected && !connected2) {
                    if (b) {
                        linearSystem.addGreaterThan(solverVariable2, objectVariable2, 0, 5);
                    }
                }
                else if (!connected && connected2) {
                    linearSystem.addEquality(objectVariable2, objectVariable4, -constraintAnchor2.getMargin(), 6);
                    if (b) {
                        linearSystem.addGreaterThan(objectVariable, solverVariable, 0, 5);
                    }
                }
                else if (connected && connected2) {
                    if (b5 != 0) {
                        n = 0;
                        if (b && margin == 0) {
                            linearSystem.addGreaterThan(objectVariable2, objectVariable, n, 6);
                        }
                        if (n7 == 0) {
                            if (margin2 <= 0 && a <= 0) {
                                margin = n;
                                max = 6;
                            }
                            else {
                                max = 4;
                                margin = 1;
                            }
                            linearSystem.addEquality(objectVariable, solverVariable5, constraintAnchor.getMargin(), max);
                            linearSystem.addEquality(objectVariable2, objectVariable4, -constraintAnchor2.getMargin(), max);
                            if (margin2 > 0 || a > 0) {
                                n = 1;
                            }
                            a = 5;
                            margin2 = margin;
                            margin = n;
                            n = margin2;
                        }
                        else {
                            Label_1180: {
                                if (n7 == 1) {
                                    a = 6;
                                }
                                else {
                                    if (n7 != 3) {
                                        a = 5;
                                        break Label_1180;
                                    }
                                    if (!b3 && this.mResolvedDimensionRatioSide != -1 && margin2 <= 0) {
                                        n = 6;
                                    }
                                    else {
                                        n = 4;
                                    }
                                    linearSystem.addEquality(objectVariable, solverVariable5, constraintAnchor.getMargin(), n);
                                    linearSystem.addEquality(objectVariable2, objectVariable4, -constraintAnchor2.getMargin(), n);
                                    a = 5;
                                }
                                n = 1;
                            }
                            margin2 = n;
                            margin = n;
                            n = margin2;
                        }
                    }
                    else {
                        a = 5;
                        margin = 1;
                        n = 0;
                    }
                    final SolverVariable solverVariable6 = solverVariable5;
                    int n8 = 0;
                    int n9 = 0;
                    Label_1332: {
                        Label_1326: {
                            if (margin != 0) {
                                margin = constraintAnchor.getMargin();
                                margin2 = constraintAnchor2.getMargin();
                                n8 = 1;
                                linearSystem.addCentering(objectVariable, solverVariable6, margin, n2, objectVariable4, objectVariable2, margin2, a);
                                final boolean b7 = constraintAnchor.mTarget.mOwner instanceof Barrier;
                                final boolean b8 = constraintAnchor2.mTarget.mOwner instanceof Barrier;
                                if (b7 && !b8) {
                                    n9 = n8;
                                    margin = 5;
                                    a = 6;
                                    n8 = (b ? 1 : 0);
                                    break Label_1332;
                                }
                                if (!b7 && b8) {
                                    margin = 6;
                                    break Label_1326;
                                }
                            }
                            n8 = (b ? 1 : 0);
                            margin = 5;
                        }
                        n9 = (b ? 1 : 0);
                        a = 5;
                    }
                    final SolverVariable solverVariable7 = objectVariable2;
                    if (n != 0) {
                        margin = 6;
                        a = 6;
                    }
                    if ((b5 == 0 && n8) || n != 0) {
                        linearSystem.addGreaterThan(objectVariable, solverVariable6, constraintAnchor.getMargin(), margin);
                    }
                    if ((b5 == 0 && n9) || n != 0) {
                        linearSystem.addLowerThan(solverVariable7, objectVariable4, -constraintAnchor2.getMargin(), a);
                    }
                    if (b) {
                        n = 0;
                        linearSystem.addGreaterThan(objectVariable, solverVariable, 0, 6);
                        solverVariable = solverVariable7;
                        break Label_1448;
                    }
                    solverVariable = solverVariable7;
                    n = 0;
                    break Label_1448;
                }
                n = 0;
                solverVariable = objectVariable2;
            }
            if (b) {
                linearSystem.addGreaterThan(solverVariable2, solverVariable, n, 6);
            }
            return;
        }
        if (n6 < 2 && b) {
            linearSystem.addGreaterThan(objectVariable, solverVariable, 0, 6);
            linearSystem.addGreaterThan(solverVariable2, objectVariable2, 0, 6);
        }
    }
    
    private boolean isChainHead(int n) {
        n *= 2;
        if (this.mListAnchors[n].mTarget != null) {
            final ConstraintAnchor mTarget = this.mListAnchors[n].mTarget.mTarget;
            final ConstraintAnchor[] mListAnchors = this.mListAnchors;
            if (mTarget != mListAnchors[n]) {
                ++n;
                if (mListAnchors[n].mTarget != null && this.mListAnchors[n].mTarget.mTarget == this.mListAnchors[n]) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public void addToSolver(final LinearSystem linearSystem) {
        final SolverVariable objectVariable = linearSystem.createObjectVariable(this.mLeft);
        final SolverVariable objectVariable2 = linearSystem.createObjectVariable(this.mRight);
        final SolverVariable objectVariable3 = linearSystem.createObjectVariable(this.mTop);
        final SolverVariable objectVariable4 = linearSystem.createObjectVariable(this.mBottom);
        final SolverVariable objectVariable5 = linearSystem.createObjectVariable(this.mBaseline);
        final ConstraintWidget mParent = this.mParent;
        boolean b5;
        boolean b6;
        boolean b7;
        boolean b8;
        if (mParent != null) {
            final boolean b = mParent != null && mParent.mListDimensionBehaviors[0] == DimensionBehaviour.WRAP_CONTENT;
            final ConstraintWidget mParent2 = this.mParent;
            final boolean b2 = mParent2 != null && mParent2.mListDimensionBehaviors[1] == DimensionBehaviour.WRAP_CONTENT;
            boolean inHorizontalChain;
            if (this.isChainHead(0)) {
                ((ConstraintWidgetContainer)this.mParent).addChain(this, 0);
                inHorizontalChain = true;
            }
            else {
                inHorizontalChain = this.isInHorizontalChain();
            }
            boolean inVerticalChain;
            if (this.isChainHead(1)) {
                ((ConstraintWidgetContainer)this.mParent).addChain(this, 1);
                inVerticalChain = true;
            }
            else {
                inVerticalChain = this.isInVerticalChain();
            }
            if (b && this.mVisibility != 8 && this.mLeft.mTarget == null && this.mRight.mTarget == null) {
                linearSystem.addGreaterThan(linearSystem.createObjectVariable(this.mParent.mRight), objectVariable2, 0, 1);
            }
            if (b2 && this.mVisibility != 8 && this.mTop.mTarget == null && this.mBottom.mTarget == null && this.mBaseline == null) {
                linearSystem.addGreaterThan(linearSystem.createObjectVariable(this.mParent.mBottom), objectVariable4, 0, 1);
            }
            final boolean b3 = b2;
            final boolean b4 = inVerticalChain;
            b5 = b;
            b6 = b3;
            b7 = inHorizontalChain;
            b8 = b4;
        }
        else {
            final boolean b9 = b6 = false;
            b8 = (b7 = b6);
            b5 = b9;
        }
        final int mWidth = this.mWidth;
        final int mMinWidth = this.mMinWidth;
        int n = mWidth;
        if (mWidth < mMinWidth) {
            n = mMinWidth;
        }
        final int mHeight = this.mHeight;
        final int mMinHeight = this.mMinHeight;
        int n2;
        if ((n2 = mHeight) < mMinHeight) {
            n2 = mMinHeight;
        }
        final boolean b10 = this.mListDimensionBehaviors[0] != DimensionBehaviour.MATCH_CONSTRAINT;
        final boolean b11 = this.mListDimensionBehaviors[1] != DimensionBehaviour.MATCH_CONSTRAINT;
        this.mResolvedDimensionRatioSide = this.mDimensionRatioSide;
        final float mDimensionRatio = this.mDimensionRatio;
        this.mResolvedDimensionRatio = mDimensionRatio;
        final int mMatchConstraintDefaultWidth = this.mMatchConstraintDefaultWidth;
        final int mMatchConstraintDefaultHeight = this.mMatchConstraintDefaultHeight;
        int n4 = 0;
        int n5 = 0;
        int n10 = 0;
        int n11 = 0;
        int n12 = 0;
        Label_0831: {
            int n6 = 0;
            int n7 = 0;
            int n8 = 0;
            int n9 = 0;
            Label_0809: {
                if (mDimensionRatio > 0.0f && this.mVisibility != 8) {
                    int n3 = mMatchConstraintDefaultWidth;
                    if (this.mListDimensionBehaviors[0] == DimensionBehaviour.MATCH_CONSTRAINT && (n3 = mMatchConstraintDefaultWidth) == 0) {
                        n3 = 3;
                    }
                    n4 = mMatchConstraintDefaultHeight;
                    if (this.mListDimensionBehaviors[1] == DimensionBehaviour.MATCH_CONSTRAINT && (n4 = mMatchConstraintDefaultHeight) == 0) {
                        n4 = 3;
                    }
                    if (this.mListDimensionBehaviors[0] == DimensionBehaviour.MATCH_CONSTRAINT && this.mListDimensionBehaviors[1] == DimensionBehaviour.MATCH_CONSTRAINT && n3 == 3 && n4 == 3) {
                        this.setupDimensionRatio(b5, b6, b10, b11);
                    }
                    else if (this.mListDimensionBehaviors[0] == DimensionBehaviour.MATCH_CONSTRAINT && n3 == 3) {
                        this.mResolvedDimensionRatioSide = 0;
                        n5 = (int)(this.mResolvedDimensionRatio * this.mHeight);
                        if (this.mListDimensionBehaviors[1] != DimensionBehaviour.MATCH_CONSTRAINT) {
                            n6 = n2;
                            n7 = n4;
                            n8 = 4;
                            n9 = n5;
                            break Label_0809;
                        }
                        n10 = n3;
                        n11 = 1;
                        n12 = n2;
                        break Label_0831;
                    }
                    else if (this.mListDimensionBehaviors[1] == DimensionBehaviour.MATCH_CONSTRAINT && n4 == 3) {
                        this.mResolvedDimensionRatioSide = 1;
                        if (this.mDimensionRatioSide == -1) {
                            this.mResolvedDimensionRatio = 1.0f / this.mResolvedDimensionRatio;
                        }
                        final int n13 = (int)(this.mResolvedDimensionRatio * this.mWidth);
                        final DimensionBehaviour dimensionBehaviour = this.mListDimensionBehaviors[0];
                        final DimensionBehaviour match_CONSTRAINT = DimensionBehaviour.MATCH_CONSTRAINT;
                        final int n14 = n3;
                        final int n15 = n;
                        n2 = n13;
                        if (dimensionBehaviour != match_CONSTRAINT) {
                            n7 = 4;
                            n8 = n14;
                            n9 = n15;
                            n6 = n13;
                            break Label_0809;
                        }
                    }
                    n5 = n;
                    n10 = n3;
                    n11 = 1;
                    n12 = n2;
                    break Label_0831;
                }
                n6 = n2;
                n9 = n;
                n7 = mMatchConstraintDefaultHeight;
                n8 = mMatchConstraintDefaultWidth;
            }
            final int n16 = 0;
            n12 = n6;
            n5 = n9;
            n11 = n16;
            n4 = n7;
            n10 = n8;
        }
        final int[] mResolvedMatchConstraintDefault = this.mResolvedMatchConstraintDefault;
        mResolvedMatchConstraintDefault[0] = n10;
        mResolvedMatchConstraintDefault[1] = n4;
        boolean b12 = false;
        Label_0877: {
            if (n11 != 0) {
                final int mResolvedDimensionRatioSide = this.mResolvedDimensionRatioSide;
                if (mResolvedDimensionRatioSide == 0 || mResolvedDimensionRatioSide == -1) {
                    b12 = true;
                    break Label_0877;
                }
            }
            b12 = false;
        }
        final boolean b13 = this.mListDimensionBehaviors[0] == DimensionBehaviour.WRAP_CONTENT && this instanceof ConstraintWidgetContainer;
        final boolean b14 = this.mCenter.isConnected() ^ true;
        if (this.mHorizontalResolution != 2) {
            final ConstraintWidget mParent3 = this.mParent;
            SolverVariable objectVariable6;
            if (mParent3 != null) {
                objectVariable6 = linearSystem.createObjectVariable(mParent3.mRight);
            }
            else {
                objectVariable6 = null;
            }
            final ConstraintWidget mParent4 = this.mParent;
            SolverVariable objectVariable7;
            if (mParent4 != null) {
                objectVariable7 = linearSystem.createObjectVariable(mParent4.mLeft);
            }
            else {
                objectVariable7 = null;
            }
            this.applyConstraints(linearSystem, b5, objectVariable7, objectVariable6, this.mListDimensionBehaviors[0], b13, this.mLeft, this.mRight, this.mX, n5, this.mMinWidth, this.mMaxDimension[0], this.mHorizontalBiasPercent, b12, b7, n10, this.mMatchConstraintMinWidth, this.mMatchConstraintMaxWidth, this.mMatchConstraintPercentWidth, b14);
        }
        final SolverVariable solverVariable = objectVariable3;
        if (this.mVerticalResolution == 2) {
            return;
        }
        final boolean b15 = this.mListDimensionBehaviors[1] == DimensionBehaviour.WRAP_CONTENT && this instanceof ConstraintWidgetContainer;
        boolean b16 = false;
        Label_1120: {
            if (n11 != 0) {
                final int mResolvedDimensionRatioSide2 = this.mResolvedDimensionRatioSide;
                if (mResolvedDimensionRatioSide2 == 1 || mResolvedDimensionRatioSide2 == -1) {
                    b16 = true;
                    break Label_1120;
                }
            }
            b16 = false;
        }
        boolean b17 = false;
        Label_1217: {
            if (this.mBaselineDistance > 0) {
                if (this.mBaseline.getResolutionNode().state == 1) {
                    this.mBaseline.getResolutionNode().addResolvedValue(linearSystem);
                }
                else {
                    linearSystem.addEquality(objectVariable5, solverVariable, this.getBaselineDistance(), 6);
                    if (this.mBaseline.mTarget != null) {
                        linearSystem.addEquality(objectVariable5, linearSystem.createObjectVariable(this.mBaseline.mTarget), 0, 6);
                        b17 = false;
                        break Label_1217;
                    }
                }
            }
            b17 = b14;
        }
        final SolverVariable solverVariable2 = solverVariable;
        final ConstraintWidget mParent5 = this.mParent;
        SolverVariable objectVariable8;
        if (mParent5 != null) {
            objectVariable8 = linearSystem.createObjectVariable(mParent5.mBottom);
        }
        else {
            objectVariable8 = null;
        }
        final ConstraintWidget mParent6 = this.mParent;
        SolverVariable objectVariable9;
        if (mParent6 != null) {
            objectVariable9 = linearSystem.createObjectVariable(mParent6.mTop);
        }
        else {
            objectVariable9 = null;
        }
        this.applyConstraints(linearSystem, b6, objectVariable9, objectVariable8, this.mListDimensionBehaviors[1], b15, this.mTop, this.mBottom, this.mY, n12, this.mMinHeight, this.mMaxDimension[1], this.mVerticalBiasPercent, b16, b8, n4, this.mMatchConstraintMinHeight, this.mMatchConstraintMaxHeight, this.mMatchConstraintPercentHeight, b17);
        if (n11 != 0) {
            if (this.mResolvedDimensionRatioSide == 1) {
                linearSystem.addRatio(objectVariable4, solverVariable2, objectVariable2, objectVariable, this.mResolvedDimensionRatio, 6);
            }
            else {
                linearSystem.addRatio(objectVariable2, objectVariable, objectVariable4, solverVariable2, this.mResolvedDimensionRatio, 6);
            }
        }
        if (this.mCenter.isConnected()) {
            linearSystem.addCenterPoint(this, this.mCenter.getTarget().getOwner(), (float)Math.toRadians(this.mCircleConstraintAngle + 90.0f), this.mCenter.getMargin());
        }
    }
    
    public boolean allowedInBarrier() {
        return this.mVisibility != 8;
    }
    
    public void analyze(final int n) {
        Optimizer.analyze(n, this);
    }
    
    public void connect(final ConstraintAnchor.Type type, final ConstraintWidget constraintWidget, final ConstraintAnchor.Type type2) {
        this.connect(type, constraintWidget, type2, 0, ConstraintAnchor.Strength.STRONG);
    }
    
    public void connect(final ConstraintAnchor.Type type, final ConstraintWidget constraintWidget, final ConstraintAnchor.Type type2, final int n) {
        this.connect(type, constraintWidget, type2, n, ConstraintAnchor.Strength.STRONG);
    }
    
    public void connect(final ConstraintAnchor.Type type, final ConstraintWidget constraintWidget, final ConstraintAnchor.Type type2, final int n, final ConstraintAnchor.Strength strength) {
        this.connect(type, constraintWidget, type2, n, strength, 0);
    }
    
    public void connect(ConstraintAnchor.Type right, final ConstraintWidget constraintWidget, final ConstraintAnchor.Type type, int n, final ConstraintAnchor.Strength strength, final int n2) {
        final ConstraintAnchor.Type center = ConstraintAnchor.Type.CENTER;
        final int n3 = 0;
        Label_0400: {
            if (right != center) {
                break Label_0400;
            }
            if (type == ConstraintAnchor.Type.CENTER) {
                final ConstraintAnchor anchor = this.getAnchor(ConstraintAnchor.Type.LEFT);
                final ConstraintAnchor anchor2 = this.getAnchor(ConstraintAnchor.Type.RIGHT);
                final ConstraintAnchor anchor3 = this.getAnchor(ConstraintAnchor.Type.TOP);
                final ConstraintAnchor anchor4 = this.getAnchor(ConstraintAnchor.Type.BOTTOM);
                boolean b = true;
                if ((anchor != null && anchor.isConnected()) || (anchor2 != null && anchor2.isConnected())) {
                    n = 0;
                }
                else {
                    this.connect(ConstraintAnchor.Type.LEFT, constraintWidget, ConstraintAnchor.Type.LEFT, 0, strength, n2);
                    this.connect(ConstraintAnchor.Type.RIGHT, constraintWidget, ConstraintAnchor.Type.RIGHT, 0, strength, n2);
                    n = 1;
                }
                if ((anchor3 != null && anchor3.isConnected()) || (anchor4 != null && anchor4.isConnected())) {
                    b = false;
                }
                else {
                    this.connect(ConstraintAnchor.Type.TOP, constraintWidget, ConstraintAnchor.Type.TOP, 0, strength, n2);
                    this.connect(ConstraintAnchor.Type.BOTTOM, constraintWidget, ConstraintAnchor.Type.BOTTOM, 0, strength, n2);
                }
                if (n != 0 && b) {
                    this.getAnchor(ConstraintAnchor.Type.CENTER).connect(constraintWidget.getAnchor(ConstraintAnchor.Type.CENTER), 0, n2);
                    return;
                }
                if (n != 0) {
                    this.getAnchor(ConstraintAnchor.Type.CENTER_X).connect(constraintWidget.getAnchor(ConstraintAnchor.Type.CENTER_X), 0, n2);
                    return;
                }
                if (b) {
                    this.getAnchor(ConstraintAnchor.Type.CENTER_Y).connect(constraintWidget.getAnchor(ConstraintAnchor.Type.CENTER_Y), 0, n2);
                }
                return;
            }
            else if (type != ConstraintAnchor.Type.LEFT && type != ConstraintAnchor.Type.RIGHT) {
                if (type == ConstraintAnchor.Type.TOP || type == ConstraintAnchor.Type.BOTTOM) {
                    this.connect(ConstraintAnchor.Type.TOP, constraintWidget, type, 0, strength, n2);
                    this.connect(ConstraintAnchor.Type.BOTTOM, constraintWidget, type, 0, strength, n2);
                    this.getAnchor(ConstraintAnchor.Type.CENTER).connect(constraintWidget.getAnchor(type), 0, n2);
                }
                return;
            }
            this.connect(ConstraintAnchor.Type.LEFT, constraintWidget, type, 0, strength, n2);
            right = ConstraintAnchor.Type.RIGHT;
            try {
                this.connect(right, constraintWidget, type, 0, strength, n2);
                this.getAnchor(ConstraintAnchor.Type.CENTER).connect(constraintWidget.getAnchor(type), 0, n2);
                return;
                final ConstraintAnchor anchor5;
                Label_0860: {
                    anchor5 = this.getAnchor(ConstraintAnchor.Type.BASELINE);
                }
                // iftrue(Label_0876:, anchor5 == null)
                // iftrue(Label_0825:, anchor7.getTarget() == anchor8)
                // iftrue(Label_0860:, right == ConstraintAnchor.Type.TOP || right == ConstraintAnchor.Type.BOTTOM)
                // iftrue(Label_0896:, anchor10.getTarget() == anchor8)
                // iftrue(Label_0477:, right != ConstraintAnchor.Type.CENTER_X || type != ConstraintAnchor.Type.LEFT && type != ConstraintAnchor.Type.RIGHT)
                // iftrue(Label_0626:, right != ConstraintAnchor.Type.CENTER_X || type != ConstraintAnchor.Type.CENTER_X)
                // iftrue(Label_0928:, !anchor6.isConnected())
                // iftrue(Label_0702:, right != ConstraintAnchor.Type.CENTER_Y || type != ConstraintAnchor.Type.CENTER_Y)
                // iftrue(Label_0755:, anchor16 == null)
                // iftrue(Label_0928:, !anchor17.isConnected())
                // iftrue(Label_0956:, !anchor12.isValidConnection(anchor8))
                // iftrue(Label_0550:, right != ConstraintAnchor.Type.CENTER_Y || type != ConstraintAnchor.Type.TOP && type != ConstraintAnchor.Type.BOTTOM)
                // iftrue(Label_0771:, anchor11 == null)
                // iftrue(Label_0928:, right != ConstraintAnchor.Type.LEFT && right != ConstraintAnchor.Type.RIGHT)
                // iftrue(Label_0774:, right != ConstraintAnchor.Type.BASELINE)
            Block_25_Outer:
                while (true) {
                    ConstraintAnchor anchor7 = null;
                    Block_31: {
                        while (true) {
                            Block_24: {
                                while (true) {
                                    ConstraintAnchor anchor16 = null;
                                    Block_26: {
                                        ConstraintAnchor opposite;
                                        ConstraintAnchor anchor6;
                                        ConstraintAnchor anchor8;
                                        ConstraintAnchor anchor9;
                                        ConstraintAnchor anchor10;
                                        ConstraintAnchor anchor11;
                                        ConstraintAnchor anchor12;
                                        ConstraintAnchor anchor13;
                                        ConstraintAnchor anchor14;
                                        ConstraintAnchor anchor15;
                                        ConstraintAnchor opposite2;
                                        ConstraintAnchor anchor17;
                                        Label_0498_Outer:Block_27_Outer:
                                        while (true) {
                                            Label_0791: {
                                                while (true) {
                                                Block_21:
                                                    while (true) {
                                                    Label_0771:
                                                        while (true) {
                                                            Label_0928: {
                                                            Label_0896:
                                                                while (true) {
                                                                    Block_33: {
                                                                        break Block_33;
                                                                        Label_0421: {
                                                                            while (true) {
                                                                            Block_34:
                                                                                while (true) {
                                                                                    opposite.reset();
                                                                                    anchor6.reset();
                                                                                    break Label_0928;
                                                                                    anchor7 = this.getAnchor(ConstraintAnchor.Type.CENTER);
                                                                                    break Block_31;
                                                                                    Label_0774:
                                                                                    break Label_0791;
                                                                                    anchor9 = constraintWidget.getAnchor(type);
                                                                                    this.getAnchor(ConstraintAnchor.Type.TOP).connect(anchor9, 0, n2);
                                                                                    this.getAnchor(ConstraintAnchor.Type.BOTTOM).connect(anchor9, 0, n2);
                                                                                    this.getAnchor(ConstraintAnchor.Type.CENTER_Y).connect(anchor9, 0, n2);
                                                                                    return;
                                                                                    anchor10 = this.getAnchor(ConstraintAnchor.Type.CENTER);
                                                                                    break Block_34;
                                                                                    break Label_0421;
                                                                                    Label_0550:
                                                                                    break Block_21;
                                                                                    anchor11.reset();
                                                                                    n = n3;
                                                                                    break Label_0771;
                                                                                    this.getAnchor(ConstraintAnchor.Type.TOP).connect(constraintWidget.getAnchor(ConstraintAnchor.Type.TOP), 0, n2);
                                                                                    this.getAnchor(ConstraintAnchor.Type.BOTTOM).connect(constraintWidget.getAnchor(ConstraintAnchor.Type.BOTTOM), 0, n2);
                                                                                    this.getAnchor(ConstraintAnchor.Type.CENTER_Y).connect(constraintWidget.getAnchor(type), 0, n2);
                                                                                    return;
                                                                                    opposite = this.getAnchor(right).getOpposite();
                                                                                    anchor6 = this.getAnchor(ConstraintAnchor.Type.CENTER_X);
                                                                                    continue Label_0498_Outer;
                                                                                }
                                                                                anchor10.reset();
                                                                                break Label_0896;
                                                                                Label_0626:
                                                                                continue Block_25_Outer;
                                                                            }
                                                                            anchor12.connect(anchor8, n, strength, n2);
                                                                            anchor8.getOwner().connectedTo(anchor12.getOwner());
                                                                            return;
                                                                        }
                                                                        anchor13 = this.getAnchor(ConstraintAnchor.Type.LEFT);
                                                                        anchor14 = constraintWidget.getAnchor(type);
                                                                        anchor15 = this.getAnchor(ConstraintAnchor.Type.RIGHT);
                                                                        anchor13.connect(anchor14, 0, n2);
                                                                        anchor15.connect(anchor14, 0, n2);
                                                                        this.getAnchor(ConstraintAnchor.Type.CENTER_X).connect(anchor14, 0, n2);
                                                                        return;
                                                                        anchor16 = this.getAnchor(ConstraintAnchor.Type.TOP);
                                                                        anchor11 = this.getAnchor(ConstraintAnchor.Type.BOTTOM);
                                                                        break Block_26;
                                                                    }
                                                                    anchor5.reset();
                                                                    continue Block_27_Outer;
                                                                }
                                                                opposite2 = this.getAnchor(right).getOpposite();
                                                                anchor17 = this.getAnchor(ConstraintAnchor.Type.CENTER_Y);
                                                                Block_35: {
                                                                    break Block_35;
                                                                    Label_0702:
                                                                    anchor12 = this.getAnchor(right);
                                                                    anchor8 = constraintWidget.getAnchor(type);
                                                                    break Block_24;
                                                                }
                                                                opposite2.reset();
                                                                anchor17.reset();
                                                                break Label_0928;
                                                                Label_0956:
                                                                return;
                                                            }
                                                            continue Label_0771;
                                                        }
                                                        Label_0477:
                                                        continue Block_27_Outer;
                                                    }
                                                    this.getAnchor(ConstraintAnchor.Type.LEFT).connect(constraintWidget.getAnchor(ConstraintAnchor.Type.LEFT), 0, n2);
                                                    this.getAnchor(ConstraintAnchor.Type.RIGHT).connect(constraintWidget.getAnchor(ConstraintAnchor.Type.RIGHT), 0, n2);
                                                    this.getAnchor(ConstraintAnchor.Type.CENTER_X).connect(constraintWidget.getAnchor(type), 0, n2);
                                                    return;
                                                    n = n3;
                                                    continue Block_25_Outer;
                                                }
                                            }
                                            continue Block_25_Outer;
                                        }
                                    }
                                    anchor16.reset();
                                    continue;
                                }
                            }
                            continue;
                        }
                    }
                    anchor7.reset();
                    continue;
                }
            }
            finally {}
        }
    }
    
    public void connect(final ConstraintAnchor constraintAnchor, final ConstraintAnchor constraintAnchor2, final int n) {
        this.connect(constraintAnchor, constraintAnchor2, n, ConstraintAnchor.Strength.STRONG, 0);
    }
    
    public void connect(final ConstraintAnchor constraintAnchor, final ConstraintAnchor constraintAnchor2, final int n, final int n2) {
        this.connect(constraintAnchor, constraintAnchor2, n, ConstraintAnchor.Strength.STRONG, n2);
    }
    
    public void connect(final ConstraintAnchor constraintAnchor, final ConstraintAnchor constraintAnchor2, final int n, final ConstraintAnchor.Strength strength, final int n2) {
        if (constraintAnchor.getOwner() == this) {
            this.connect(constraintAnchor.getType(), constraintAnchor2.getOwner(), constraintAnchor2.getType(), n, strength, n2);
        }
    }
    
    public void connectCircularConstraint(final ConstraintWidget constraintWidget, final float mCircleConstraintAngle, final int n) {
        this.immediateConnect(ConstraintAnchor.Type.CENTER, constraintWidget, ConstraintAnchor.Type.CENTER, n, 0);
        this.mCircleConstraintAngle = mCircleConstraintAngle;
    }
    
    public void connectedTo(final ConstraintWidget constraintWidget) {
    }
    
    public void createObjectVariables(final LinearSystem linearSystem) {
        linearSystem.createObjectVariable(this.mLeft);
        linearSystem.createObjectVariable(this.mTop);
        linearSystem.createObjectVariable(this.mRight);
        linearSystem.createObjectVariable(this.mBottom);
        if (this.mBaselineDistance > 0) {
            linearSystem.createObjectVariable(this.mBaseline);
        }
    }
    
    public void disconnectUnlockedWidget(final ConstraintWidget constraintWidget) {
        final ArrayList<ConstraintAnchor> anchors = this.getAnchors();
        for (int size = anchors.size(), i = 0; i < size; ++i) {
            final ConstraintAnchor constraintAnchor = anchors.get(i);
            if (constraintAnchor.isConnected() && constraintAnchor.getTarget().getOwner() == constraintWidget && constraintAnchor.getConnectionCreator() == 2) {
                constraintAnchor.reset();
            }
        }
    }
    
    public void disconnectWidget(final ConstraintWidget constraintWidget) {
        final ArrayList<ConstraintAnchor> anchors = this.getAnchors();
        for (int size = anchors.size(), i = 0; i < size; ++i) {
            final ConstraintAnchor constraintAnchor = anchors.get(i);
            if (constraintAnchor.isConnected() && constraintAnchor.getTarget().getOwner() == constraintWidget) {
                constraintAnchor.reset();
            }
        }
    }
    
    public void forceUpdateDrawPosition() {
        final int mx = this.mX;
        final int my = this.mY;
        final int mWidth = this.mWidth;
        final int mHeight = this.mHeight;
        this.mDrawX = mx;
        this.mDrawY = my;
        this.mDrawWidth = mWidth + mx - mx;
        this.mDrawHeight = mHeight + my - my;
    }
    
    public ConstraintAnchor getAnchor(final ConstraintAnchor.Type type) {
        switch (ConstraintWidget$1.$SwitchMap$androidx$constraintlayout$solver$widgets$ConstraintAnchor$Type[type.ordinal()]) {
            default: {
                throw new AssertionError((Object)type.name());
            }
            case 9: {
                return null;
            }
            case 8: {
                return this.mCenterY;
            }
            case 7: {
                return this.mCenterX;
            }
            case 6: {
                return this.mCenter;
            }
            case 5: {
                return this.mBaseline;
            }
            case 4: {
                return this.mBottom;
            }
            case 3: {
                return this.mRight;
            }
            case 2: {
                return this.mTop;
            }
            case 1: {
                return this.mLeft;
            }
        }
    }
    
    public ArrayList<ConstraintAnchor> getAnchors() {
        return this.mAnchors;
    }
    
    public int getBaselineDistance() {
        return this.mBaselineDistance;
    }
    
    public float getBiasPercent(final int n) {
        if (n == 0) {
            return this.mHorizontalBiasPercent;
        }
        if (n == 1) {
            return this.mVerticalBiasPercent;
        }
        return -1.0f;
    }
    
    public int getBottom() {
        return this.getY() + this.mHeight;
    }
    
    public Object getCompanionWidget() {
        return this.mCompanionWidget;
    }
    
    public int getContainerItemSkip() {
        return this.mContainerItemSkip;
    }
    
    public String getDebugName() {
        return this.mDebugName;
    }
    
    public DimensionBehaviour getDimensionBehaviour(final int n) {
        if (n == 0) {
            return this.getHorizontalDimensionBehaviour();
        }
        if (n == 1) {
            return this.getVerticalDimensionBehaviour();
        }
        return null;
    }
    
    public float getDimensionRatio() {
        return this.mDimensionRatio;
    }
    
    public int getDimensionRatioSide() {
        return this.mDimensionRatioSide;
    }
    
    public int getDrawBottom() {
        return this.getDrawY() + this.mDrawHeight;
    }
    
    public int getDrawHeight() {
        return this.mDrawHeight;
    }
    
    public int getDrawRight() {
        return this.getDrawX() + this.mDrawWidth;
    }
    
    public int getDrawWidth() {
        return this.mDrawWidth;
    }
    
    public int getDrawX() {
        return this.mDrawX + this.mOffsetX;
    }
    
    public int getDrawY() {
        return this.mDrawY + this.mOffsetY;
    }
    
    public int getHeight() {
        if (this.mVisibility == 8) {
            return 0;
        }
        return this.mHeight;
    }
    
    public float getHorizontalBiasPercent() {
        return this.mHorizontalBiasPercent;
    }
    
    public ConstraintWidget getHorizontalChainControlWidget() {
        ConstraintWidget constraintWidget3;
        if (this.isInHorizontalChain()) {
            ConstraintWidget constraintWidget = this;
            ConstraintWidget constraintWidget2 = null;
            while (true) {
                constraintWidget3 = constraintWidget2;
                if (constraintWidget2 != null) {
                    break;
                }
                constraintWidget3 = constraintWidget2;
                if (constraintWidget == null) {
                    break;
                }
                final ConstraintAnchor anchor = constraintWidget.getAnchor(ConstraintAnchor.Type.LEFT);
                ConstraintAnchor target;
                if (anchor == null) {
                    target = null;
                }
                else {
                    target = anchor.getTarget();
                }
                ConstraintWidget owner;
                if (target == null) {
                    owner = null;
                }
                else {
                    owner = target.getOwner();
                }
                if (owner == this.getParent()) {
                    return constraintWidget;
                }
                ConstraintAnchor target2;
                if (owner == null) {
                    target2 = null;
                }
                else {
                    target2 = owner.getAnchor(ConstraintAnchor.Type.RIGHT).getTarget();
                }
                if (target2 != null && target2.getOwner() != constraintWidget) {
                    constraintWidget2 = constraintWidget;
                }
                else {
                    constraintWidget = owner;
                }
            }
        }
        else {
            constraintWidget3 = null;
        }
        return constraintWidget3;
    }
    
    public int getHorizontalChainStyle() {
        return this.mHorizontalChainStyle;
    }
    
    public DimensionBehaviour getHorizontalDimensionBehaviour() {
        return this.mListDimensionBehaviors[0];
    }
    
    public int getInternalDrawBottom() {
        return this.mDrawY + this.mDrawHeight;
    }
    
    public int getInternalDrawRight() {
        return this.mDrawX + this.mDrawWidth;
    }
    
    int getInternalDrawX() {
        return this.mDrawX;
    }
    
    int getInternalDrawY() {
        return this.mDrawY;
    }
    
    public int getLeft() {
        return this.getX();
    }
    
    public int getLength(final int n) {
        if (n == 0) {
            return this.getWidth();
        }
        if (n == 1) {
            return this.getHeight();
        }
        return 0;
    }
    
    public int getMaxHeight() {
        return this.mMaxDimension[1];
    }
    
    public int getMaxWidth() {
        return this.mMaxDimension[0];
    }
    
    public int getMinHeight() {
        return this.mMinHeight;
    }
    
    public int getMinWidth() {
        return this.mMinWidth;
    }
    
    public int getOptimizerWrapHeight() {
        int mHeight = this.mHeight;
        if (this.mListDimensionBehaviors[1] == DimensionBehaviour.MATCH_CONSTRAINT) {
            int mHeight2;
            if (this.mMatchConstraintDefaultHeight == 1) {
                mHeight2 = Math.max(this.mMatchConstraintMinHeight, mHeight);
            }
            else {
                mHeight2 = this.mMatchConstraintMinHeight;
                if (mHeight2 > 0) {
                    this.mHeight = mHeight2;
                }
                else {
                    mHeight2 = 0;
                }
            }
            final int mMatchConstraintMaxHeight = this.mMatchConstraintMaxHeight;
            mHeight = mHeight2;
            if (mMatchConstraintMaxHeight > 0 && mMatchConstraintMaxHeight < (mHeight = mHeight2)) {
                mHeight = mMatchConstraintMaxHeight;
            }
        }
        return mHeight;
    }
    
    public int getOptimizerWrapWidth() {
        int mWidth = this.mWidth;
        if (this.mListDimensionBehaviors[0] == DimensionBehaviour.MATCH_CONSTRAINT) {
            int mWidth2;
            if (this.mMatchConstraintDefaultWidth == 1) {
                mWidth2 = Math.max(this.mMatchConstraintMinWidth, mWidth);
            }
            else {
                mWidth2 = this.mMatchConstraintMinWidth;
                if (mWidth2 > 0) {
                    this.mWidth = mWidth2;
                }
                else {
                    mWidth2 = 0;
                }
            }
            final int mMatchConstraintMaxWidth = this.mMatchConstraintMaxWidth;
            mWidth = mWidth2;
            if (mMatchConstraintMaxWidth > 0 && mMatchConstraintMaxWidth < (mWidth = mWidth2)) {
                mWidth = mMatchConstraintMaxWidth;
            }
        }
        return mWidth;
    }
    
    public ConstraintWidget getParent() {
        return this.mParent;
    }
    
    int getRelativePositioning(final int n) {
        if (n == 0) {
            return this.mRelX;
        }
        if (n == 1) {
            return this.mRelY;
        }
        return 0;
    }
    
    public ResolutionDimension getResolutionHeight() {
        if (this.mResolutionHeight == null) {
            this.mResolutionHeight = new ResolutionDimension();
        }
        return this.mResolutionHeight;
    }
    
    public ResolutionDimension getResolutionWidth() {
        if (this.mResolutionWidth == null) {
            this.mResolutionWidth = new ResolutionDimension();
        }
        return this.mResolutionWidth;
    }
    
    public int getRight() {
        return this.getX() + this.mWidth;
    }
    
    public WidgetContainer getRootWidgetContainer() {
        ConstraintWidget parent;
        for (parent = this; parent.getParent() != null; parent = parent.getParent()) {}
        if (parent instanceof WidgetContainer) {
            return (WidgetContainer)parent;
        }
        return null;
    }
    
    protected int getRootX() {
        return this.mX + this.mOffsetX;
    }
    
    protected int getRootY() {
        return this.mY + this.mOffsetY;
    }
    
    public int getTop() {
        return this.getY();
    }
    
    public String getType() {
        return this.mType;
    }
    
    public float getVerticalBiasPercent() {
        return this.mVerticalBiasPercent;
    }
    
    public ConstraintWidget getVerticalChainControlWidget() {
        ConstraintWidget constraintWidget3;
        if (this.isInVerticalChain()) {
            ConstraintWidget constraintWidget = this;
            ConstraintWidget constraintWidget2 = null;
            while (true) {
                constraintWidget3 = constraintWidget2;
                if (constraintWidget2 != null) {
                    break;
                }
                constraintWidget3 = constraintWidget2;
                if (constraintWidget == null) {
                    break;
                }
                final ConstraintAnchor anchor = constraintWidget.getAnchor(ConstraintAnchor.Type.TOP);
                ConstraintAnchor target;
                if (anchor == null) {
                    target = null;
                }
                else {
                    target = anchor.getTarget();
                }
                ConstraintWidget owner;
                if (target == null) {
                    owner = null;
                }
                else {
                    owner = target.getOwner();
                }
                if (owner == this.getParent()) {
                    return constraintWidget;
                }
                ConstraintAnchor target2;
                if (owner == null) {
                    target2 = null;
                }
                else {
                    target2 = owner.getAnchor(ConstraintAnchor.Type.BOTTOM).getTarget();
                }
                if (target2 != null && target2.getOwner() != constraintWidget) {
                    constraintWidget2 = constraintWidget;
                }
                else {
                    constraintWidget = owner;
                }
            }
        }
        else {
            constraintWidget3 = null;
        }
        return constraintWidget3;
    }
    
    public int getVerticalChainStyle() {
        return this.mVerticalChainStyle;
    }
    
    public DimensionBehaviour getVerticalDimensionBehaviour() {
        return this.mListDimensionBehaviors[1];
    }
    
    public int getVisibility() {
        return this.mVisibility;
    }
    
    public int getWidth() {
        if (this.mVisibility == 8) {
            return 0;
        }
        return this.mWidth;
    }
    
    public int getWrapHeight() {
        return this.mWrapHeight;
    }
    
    public int getWrapWidth() {
        return this.mWrapWidth;
    }
    
    public int getX() {
        return this.mX;
    }
    
    public int getY() {
        return this.mY;
    }
    
    public boolean hasAncestor(final ConstraintWidget constraintWidget) {
        final ConstraintWidget parent = this.getParent();
        if (parent == constraintWidget) {
            return true;
        }
        ConstraintWidget parent2;
        if ((parent2 = parent) == constraintWidget.getParent()) {
            return false;
        }
        while (parent2 != null) {
            if (parent2 == constraintWidget) {
                return true;
            }
            if (parent2 == constraintWidget.getParent()) {
                return true;
            }
            parent2 = parent2.getParent();
        }
        return false;
    }
    
    public boolean hasBaseline() {
        return this.mBaselineDistance > 0;
    }
    
    public void immediateConnect(final ConstraintAnchor.Type type, final ConstraintWidget constraintWidget, final ConstraintAnchor.Type type2, final int n, final int n2) {
        this.getAnchor(type).connect(constraintWidget.getAnchor(type2), n, n2, ConstraintAnchor.Strength.STRONG, 0, true);
    }
    
    public boolean isFullyResolved() {
        return this.mLeft.getResolutionNode().state == 1 && this.mRight.getResolutionNode().state == 1 && this.mTop.getResolutionNode().state == 1 && this.mBottom.getResolutionNode().state == 1;
    }
    
    public boolean isHeightWrapContent() {
        return this.mIsHeightWrapContent;
    }
    
    public boolean isInHorizontalChain() {
        return (this.mLeft.mTarget != null && this.mLeft.mTarget.mTarget == this.mLeft) || (this.mRight.mTarget != null && this.mRight.mTarget.mTarget == this.mRight);
    }
    
    public boolean isInVerticalChain() {
        return (this.mTop.mTarget != null && this.mTop.mTarget.mTarget == this.mTop) || (this.mBottom.mTarget != null && this.mBottom.mTarget.mTarget == this.mBottom);
    }
    
    public boolean isInsideConstraintLayout() {
        ConstraintWidget constraintWidget;
        if ((constraintWidget = this.getParent()) == null) {
            return false;
        }
        while (constraintWidget != null) {
            if (constraintWidget instanceof ConstraintWidgetContainer) {
                return true;
            }
            constraintWidget = constraintWidget.getParent();
        }
        return false;
    }
    
    public boolean isRoot() {
        return this.mParent == null;
    }
    
    public boolean isRootContainer() {
        if (this instanceof ConstraintWidgetContainer) {
            final ConstraintWidget mParent = this.mParent;
            if (mParent == null || !(mParent instanceof ConstraintWidgetContainer)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isSpreadHeight() {
        return this.mMatchConstraintDefaultHeight == 0 && this.mDimensionRatio == 0.0f && this.mMatchConstraintMinHeight == 0 && this.mMatchConstraintMaxHeight == 0 && this.mListDimensionBehaviors[1] == DimensionBehaviour.MATCH_CONSTRAINT;
    }
    
    public boolean isSpreadWidth() {
        final int mMatchConstraintDefaultWidth = this.mMatchConstraintDefaultWidth;
        boolean b2;
        final boolean b = b2 = false;
        if (mMatchConstraintDefaultWidth == 0) {
            b2 = b;
            if (this.mDimensionRatio == 0.0f) {
                b2 = b;
                if (this.mMatchConstraintMinWidth == 0) {
                    b2 = b;
                    if (this.mMatchConstraintMaxWidth == 0) {
                        b2 = b;
                        if (this.mListDimensionBehaviors[0] == DimensionBehaviour.MATCH_CONSTRAINT) {
                            b2 = true;
                        }
                    }
                }
            }
        }
        return b2;
    }
    
    public boolean isWidthWrapContent() {
        return this.mIsWidthWrapContent;
    }
    
    public void reset() {
        this.mLeft.reset();
        this.mTop.reset();
        this.mRight.reset();
        this.mBottom.reset();
        this.mBaseline.reset();
        this.mCenterX.reset();
        this.mCenterY.reset();
        this.mCenter.reset();
        this.mParent = null;
        this.mCircleConstraintAngle = 0.0f;
        this.mWidth = 0;
        this.mHeight = 0;
        this.mDimensionRatio = 0.0f;
        this.mDimensionRatioSide = -1;
        this.mX = 0;
        this.mY = 0;
        this.mDrawX = 0;
        this.mDrawY = 0;
        this.mDrawWidth = 0;
        this.mDrawHeight = 0;
        this.mOffsetX = 0;
        this.mOffsetY = 0;
        this.mBaselineDistance = 0;
        this.mMinWidth = 0;
        this.mMinHeight = 0;
        this.mWrapWidth = 0;
        this.mWrapHeight = 0;
        final float default_BIAS = ConstraintWidget.DEFAULT_BIAS;
        this.mHorizontalBiasPercent = default_BIAS;
        this.mVerticalBiasPercent = default_BIAS;
        this.mListDimensionBehaviors[0] = DimensionBehaviour.FIXED;
        this.mListDimensionBehaviors[1] = DimensionBehaviour.FIXED;
        this.mCompanionWidget = null;
        this.mContainerItemSkip = 0;
        this.mVisibility = 0;
        this.mType = null;
        this.mHorizontalWrapVisited = false;
        this.mVerticalWrapVisited = false;
        this.mHorizontalChainStyle = 0;
        this.mVerticalChainStyle = 0;
        this.mHorizontalChainFixedPosition = false;
        this.mVerticalChainFixedPosition = false;
        final float[] mWeight = this.mWeight;
        mWeight[1] = (mWeight[0] = -1.0f);
        this.mHorizontalResolution = -1;
        this.mVerticalResolution = -1;
        final int[] mMaxDimension = this.mMaxDimension;
        mMaxDimension[1] = (mMaxDimension[0] = Integer.MAX_VALUE);
        this.mMatchConstraintDefaultWidth = 0;
        this.mMatchConstraintDefaultHeight = 0;
        this.mMatchConstraintPercentWidth = 1.0f;
        this.mMatchConstraintPercentHeight = 1.0f;
        this.mMatchConstraintMaxWidth = Integer.MAX_VALUE;
        this.mMatchConstraintMaxHeight = Integer.MAX_VALUE;
        this.mMatchConstraintMinWidth = 0;
        this.mMatchConstraintMinHeight = 0;
        this.mResolvedDimensionRatioSide = -1;
        this.mResolvedDimensionRatio = 1.0f;
        final ResolutionDimension mResolutionWidth = this.mResolutionWidth;
        if (mResolutionWidth != null) {
            mResolutionWidth.reset();
        }
        final ResolutionDimension mResolutionHeight = this.mResolutionHeight;
        if (mResolutionHeight != null) {
            mResolutionHeight.reset();
        }
        this.mBelongingGroup = null;
        this.mOptimizerMeasurable = false;
        this.mOptimizerMeasured = false;
        this.mGroupsToSolver = false;
    }
    
    public void resetAllConstraints() {
        this.resetAnchors();
        this.setVerticalBiasPercent(ConstraintWidget.DEFAULT_BIAS);
        this.setHorizontalBiasPercent(ConstraintWidget.DEFAULT_BIAS);
        if (this instanceof ConstraintWidgetContainer) {
            return;
        }
        if (this.getHorizontalDimensionBehaviour() == DimensionBehaviour.MATCH_CONSTRAINT) {
            if (this.getWidth() == this.getWrapWidth()) {
                this.setHorizontalDimensionBehaviour(DimensionBehaviour.WRAP_CONTENT);
            }
            else if (this.getWidth() > this.getMinWidth()) {
                this.setHorizontalDimensionBehaviour(DimensionBehaviour.FIXED);
            }
        }
        if (this.getVerticalDimensionBehaviour() == DimensionBehaviour.MATCH_CONSTRAINT) {
            if (this.getHeight() == this.getWrapHeight()) {
                this.setVerticalDimensionBehaviour(DimensionBehaviour.WRAP_CONTENT);
                return;
            }
            if (this.getHeight() > this.getMinHeight()) {
                this.setVerticalDimensionBehaviour(DimensionBehaviour.FIXED);
            }
        }
    }
    
    public void resetAnchor(final ConstraintAnchor constraintAnchor) {
        if (this.getParent() != null && this.getParent() instanceof ConstraintWidgetContainer && ((ConstraintWidgetContainer)this.getParent()).handlesInternalConstraints()) {
            return;
        }
        final ConstraintAnchor anchor = this.getAnchor(ConstraintAnchor.Type.LEFT);
        final ConstraintAnchor anchor2 = this.getAnchor(ConstraintAnchor.Type.RIGHT);
        final ConstraintAnchor anchor3 = this.getAnchor(ConstraintAnchor.Type.TOP);
        final ConstraintAnchor anchor4 = this.getAnchor(ConstraintAnchor.Type.BOTTOM);
        final ConstraintAnchor anchor5 = this.getAnchor(ConstraintAnchor.Type.CENTER);
        final ConstraintAnchor anchor6 = this.getAnchor(ConstraintAnchor.Type.CENTER_X);
        final ConstraintAnchor anchor7 = this.getAnchor(ConstraintAnchor.Type.CENTER_Y);
        if (constraintAnchor == anchor5) {
            if (anchor.isConnected() && anchor2.isConnected() && anchor.getTarget() == anchor2.getTarget()) {
                anchor.reset();
                anchor2.reset();
            }
            if (anchor3.isConnected() && anchor4.isConnected() && anchor3.getTarget() == anchor4.getTarget()) {
                anchor3.reset();
                anchor4.reset();
            }
            this.mHorizontalBiasPercent = 0.5f;
            this.mVerticalBiasPercent = 0.5f;
        }
        else if (constraintAnchor == anchor6) {
            if (anchor.isConnected() && anchor2.isConnected() && anchor.getTarget().getOwner() == anchor2.getTarget().getOwner()) {
                anchor.reset();
                anchor2.reset();
            }
            this.mHorizontalBiasPercent = 0.5f;
        }
        else if (constraintAnchor == anchor7) {
            if (anchor3.isConnected() && anchor4.isConnected() && anchor3.getTarget().getOwner() == anchor4.getTarget().getOwner()) {
                anchor3.reset();
                anchor4.reset();
            }
            this.mVerticalBiasPercent = 0.5f;
        }
        else if (constraintAnchor != anchor && constraintAnchor != anchor2) {
            if ((constraintAnchor == anchor3 || constraintAnchor == anchor4) && anchor3.isConnected() && anchor3.getTarget() == anchor4.getTarget()) {
                anchor5.reset();
            }
        }
        else if (anchor.isConnected() && anchor.getTarget() == anchor2.getTarget()) {
            anchor5.reset();
        }
        constraintAnchor.reset();
    }
    
    public void resetAnchors() {
        final ConstraintWidget parent = this.getParent();
        if (parent != null && parent instanceof ConstraintWidgetContainer && ((ConstraintWidgetContainer)this.getParent()).handlesInternalConstraints()) {
            return;
        }
        for (int i = 0; i < this.mAnchors.size(); ++i) {
            this.mAnchors.get(i).reset();
        }
    }
    
    public void resetAnchors(final int n) {
        final ConstraintWidget parent = this.getParent();
        if (parent != null && parent instanceof ConstraintWidgetContainer && ((ConstraintWidgetContainer)this.getParent()).handlesInternalConstraints()) {
            return;
        }
        for (int i = 0; i < this.mAnchors.size(); ++i) {
            final ConstraintAnchor constraintAnchor = this.mAnchors.get(i);
            if (n == constraintAnchor.getConnectionCreator()) {
                if (constraintAnchor.isVerticalAnchor()) {
                    this.setVerticalBiasPercent(ConstraintWidget.DEFAULT_BIAS);
                }
                else {
                    this.setHorizontalBiasPercent(ConstraintWidget.DEFAULT_BIAS);
                }
                constraintAnchor.reset();
            }
        }
    }
    
    public void resetResolutionNodes() {
        for (int i = 0; i < 6; ++i) {
            this.mListAnchors[i].getResolutionNode().reset();
        }
    }
    
    public void resetSolverVariables(final Cache cache) {
        this.mLeft.resetSolverVariable(cache);
        this.mTop.resetSolverVariable(cache);
        this.mRight.resetSolverVariable(cache);
        this.mBottom.resetSolverVariable(cache);
        this.mBaseline.resetSolverVariable(cache);
        this.mCenter.resetSolverVariable(cache);
        this.mCenterX.resetSolverVariable(cache);
        this.mCenterY.resetSolverVariable(cache);
    }
    
    public void resolve() {
    }
    
    public void setBaselineDistance(final int mBaselineDistance) {
        this.mBaselineDistance = mBaselineDistance;
    }
    
    public void setCompanionWidget(final Object mCompanionWidget) {
        this.mCompanionWidget = mCompanionWidget;
    }
    
    public void setContainerItemSkip(final int mContainerItemSkip) {
        if (mContainerItemSkip >= 0) {
            this.mContainerItemSkip = mContainerItemSkip;
            return;
        }
        this.mContainerItemSkip = 0;
    }
    
    public void setDebugName(final String mDebugName) {
        this.mDebugName = mDebugName;
    }
    
    public void setDebugSolverName(final LinearSystem linearSystem, final String s) {
        this.mDebugName = s;
        final SolverVariable objectVariable = linearSystem.createObjectVariable(this.mLeft);
        final SolverVariable objectVariable2 = linearSystem.createObjectVariable(this.mTop);
        final SolverVariable objectVariable3 = linearSystem.createObjectVariable(this.mRight);
        final SolverVariable objectVariable4 = linearSystem.createObjectVariable(this.mBottom);
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append(".left");
        objectVariable.setName(sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(s);
        sb2.append(".top");
        objectVariable2.setName(sb2.toString());
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(s);
        sb3.append(".right");
        objectVariable3.setName(sb3.toString());
        final StringBuilder sb4 = new StringBuilder();
        sb4.append(s);
        sb4.append(".bottom");
        objectVariable4.setName(sb4.toString());
        if (this.mBaselineDistance > 0) {
            final SolverVariable objectVariable5 = linearSystem.createObjectVariable(this.mBaseline);
            final StringBuilder sb5 = new StringBuilder();
            sb5.append(s);
            sb5.append(".baseline");
            objectVariable5.setName(sb5.toString());
        }
    }
    
    public void setDimension(int mWidth, int mMinHeight) {
        this.mWidth = mWidth;
        mWidth = this.mWidth;
        final int mMinWidth = this.mMinWidth;
        if (mWidth < mMinWidth) {
            this.mWidth = mMinWidth;
        }
        this.mHeight = mMinHeight;
        mWidth = this.mHeight;
        mMinHeight = this.mMinHeight;
        if (mWidth < mMinHeight) {
            this.mHeight = mMinHeight;
        }
    }
    
    public void setDimensionRatio(final float mDimensionRatio, final int mDimensionRatioSide) {
        this.mDimensionRatio = mDimensionRatio;
        this.mDimensionRatioSide = mDimensionRatioSide;
    }
    
    public void setDimensionRatio(String s) {
        Label_0261: {
            if (s == null || s.length() == 0) {
                break Label_0261;
            }
            final int n = -1;
            final int length = s.length();
            final int index = s.indexOf(44);
            final int n2 = 0;
            int mDimensionRatioSide = n;
            int n3 = n2;
            if (index > 0) {
                mDimensionRatioSide = n;
                n3 = n2;
                if (index < length - 1) {
                    final String substring = s.substring(0, index);
                    if (substring.equalsIgnoreCase("W")) {
                        mDimensionRatioSide = 0;
                    }
                    else {
                        mDimensionRatioSide = n;
                        if (substring.equalsIgnoreCase("H")) {
                            mDimensionRatioSide = 1;
                        }
                    }
                    n3 = index + 1;
                }
            }
            final int index2 = s.indexOf(58);
            Label_0219: {
                if (index2 < 0 || index2 >= length - 1) {
                    break Label_0219;
                }
                final String substring2 = s.substring(n3, index2);
                s = s.substring(index2 + 1);
            Block_15_Outer:
                while (true) {
                    if (substring2.length() <= 0 || s.length() <= 0) {
                        break Label_0241;
                    }
                    try {
                        final float float1 = Float.parseFloat(substring2);
                        final float float2 = Float.parseFloat(s);
                        while (true) {
                            while (true) {
                                if (float1 > 0.0f && float2 > 0.0f) {
                                    if (mDimensionRatioSide == 1) {
                                        final float mDimensionRatio = Math.abs(float2 / float1);
                                        break Label_0243;
                                    }
                                    final float mDimensionRatio = Math.abs(float1 / float2);
                                    break Label_0243;
                                }
                                float mDimensionRatio = 0.0f;
                                if (mDimensionRatio > 0.0f) {
                                    this.mDimensionRatio = mDimensionRatio;
                                    this.mDimensionRatioSide = mDimensionRatioSide;
                                }
                                return;
                                mDimensionRatio = Float.parseFloat(s);
                                continue Block_15_Outer;
                            }
                            this.mDimensionRatio = 0.0f;
                            return;
                            s = s.substring(n3);
                            continue;
                        }
                    }
                    // iftrue(Label_0241:, s.length() <= 0)
                    catch (NumberFormatException ex) {
                        continue;
                    }
                    break;
                }
            }
        }
    }
    
    public void setDrawHeight(final int mDrawHeight) {
        this.mDrawHeight = mDrawHeight;
    }
    
    public void setDrawOrigin(final int n, final int n2) {
        this.mDrawX = n - this.mOffsetX;
        this.mDrawY = n2 - this.mOffsetY;
        this.mX = this.mDrawX;
        this.mY = this.mDrawY;
    }
    
    public void setDrawWidth(final int mDrawWidth) {
        this.mDrawWidth = mDrawWidth;
    }
    
    public void setDrawX(final int n) {
        this.mDrawX = n - this.mOffsetX;
        this.mX = this.mDrawX;
    }
    
    public void setDrawY(final int n) {
        this.mDrawY = n - this.mOffsetY;
        this.mY = this.mDrawY;
    }
    
    public void setFrame(final int n, final int n2, final int n3) {
        if (n3 == 0) {
            this.setHorizontalDimension(n, n2);
        }
        else if (n3 == 1) {
            this.setVerticalDimension(n, n2);
        }
        this.mOptimizerMeasured = true;
    }
    
    public void setFrame(int n, int n2, int n3, final int n4) {
        final int n5 = n3 - n;
        n3 = n4 - n2;
        this.mX = n;
        this.mY = n2;
        if (this.mVisibility == 8) {
            this.mWidth = 0;
            this.mHeight = 0;
            return;
        }
        Label_0069: {
            if (this.mListDimensionBehaviors[0] == DimensionBehaviour.FIXED) {
                n = this.mWidth;
                if (n5 < n) {
                    break Label_0069;
                }
            }
            n = n5;
        }
        Label_0096: {
            if (this.mListDimensionBehaviors[1] == DimensionBehaviour.FIXED) {
                n2 = this.mHeight;
                if (n3 < n2) {
                    break Label_0096;
                }
            }
            n2 = n3;
        }
        this.mWidth = n;
        this.mHeight = n2;
        n = this.mHeight;
        n2 = this.mMinHeight;
        if (n < n2) {
            this.mHeight = n2;
        }
        n = this.mWidth;
        n2 = this.mMinWidth;
        if (n < n2) {
            this.mWidth = n2;
        }
        this.mOptimizerMeasured = true;
    }
    
    public void setGoneMargin(final ConstraintAnchor.Type type, final int n) {
        final int n2 = ConstraintWidget$1.$SwitchMap$androidx$constraintlayout$solver$widgets$ConstraintAnchor$Type[type.ordinal()];
        if (n2 == 1) {
            this.mLeft.mGoneMargin = n;
            return;
        }
        if (n2 == 2) {
            this.mTop.mGoneMargin = n;
            return;
        }
        if (n2 == 3) {
            this.mRight.mGoneMargin = n;
            return;
        }
        if (n2 != 4) {
            return;
        }
        this.mBottom.mGoneMargin = n;
    }
    
    public void setHeight(int mHeight) {
        this.mHeight = mHeight;
        mHeight = this.mHeight;
        final int mMinHeight = this.mMinHeight;
        if (mHeight < mMinHeight) {
            this.mHeight = mMinHeight;
        }
    }
    
    public void setHeightWrapContent(final boolean mIsHeightWrapContent) {
        this.mIsHeightWrapContent = mIsHeightWrapContent;
    }
    
    public void setHorizontalBiasPercent(final float mHorizontalBiasPercent) {
        this.mHorizontalBiasPercent = mHorizontalBiasPercent;
    }
    
    public void setHorizontalChainStyle(final int mHorizontalChainStyle) {
        this.mHorizontalChainStyle = mHorizontalChainStyle;
    }
    
    public void setHorizontalDimension(int mWidth, int mMinWidth) {
        this.mX = mWidth;
        this.mWidth = mMinWidth - mWidth;
        mWidth = this.mWidth;
        mMinWidth = this.mMinWidth;
        if (mWidth < mMinWidth) {
            this.mWidth = mMinWidth;
        }
    }
    
    public void setHorizontalDimensionBehaviour(final DimensionBehaviour dimensionBehaviour) {
        this.mListDimensionBehaviors[0] = dimensionBehaviour;
        if (dimensionBehaviour == DimensionBehaviour.WRAP_CONTENT) {
            this.setWidth(this.mWrapWidth);
        }
    }
    
    public void setHorizontalMatchStyle(final int mMatchConstraintDefaultWidth, final int mMatchConstraintMinWidth, final int mMatchConstraintMaxWidth, final float mMatchConstraintPercentWidth) {
        this.mMatchConstraintDefaultWidth = mMatchConstraintDefaultWidth;
        this.mMatchConstraintMinWidth = mMatchConstraintMinWidth;
        this.mMatchConstraintMaxWidth = mMatchConstraintMaxWidth;
        this.mMatchConstraintPercentWidth = mMatchConstraintPercentWidth;
        if (mMatchConstraintPercentWidth < 1.0f && this.mMatchConstraintDefaultWidth == 0) {
            this.mMatchConstraintDefaultWidth = 2;
        }
    }
    
    public void setHorizontalWeight(final float n) {
        this.mWeight[0] = n;
    }
    
    public void setLength(final int n, final int n2) {
        if (n2 == 0) {
            this.setWidth(n);
            return;
        }
        if (n2 == 1) {
            this.setHeight(n);
        }
    }
    
    public void setMaxHeight(final int n) {
        this.mMaxDimension[1] = n;
    }
    
    public void setMaxWidth(final int n) {
        this.mMaxDimension[0] = n;
    }
    
    public void setMinHeight(final int mMinHeight) {
        if (mMinHeight < 0) {
            this.mMinHeight = 0;
            return;
        }
        this.mMinHeight = mMinHeight;
    }
    
    public void setMinWidth(final int mMinWidth) {
        if (mMinWidth < 0) {
            this.mMinWidth = 0;
            return;
        }
        this.mMinWidth = mMinWidth;
    }
    
    public void setOffset(final int mOffsetX, final int mOffsetY) {
        this.mOffsetX = mOffsetX;
        this.mOffsetY = mOffsetY;
    }
    
    public void setOrigin(final int mx, final int my) {
        this.mX = mx;
        this.mY = my;
    }
    
    public void setParent(final ConstraintWidget mParent) {
        this.mParent = mParent;
    }
    
    void setRelativePositioning(final int n, final int n2) {
        if (n2 == 0) {
            this.mRelX = n;
            return;
        }
        if (n2 == 1) {
            this.mRelY = n;
        }
    }
    
    public void setType(final String mType) {
        this.mType = mType;
    }
    
    public void setVerticalBiasPercent(final float mVerticalBiasPercent) {
        this.mVerticalBiasPercent = mVerticalBiasPercent;
    }
    
    public void setVerticalChainStyle(final int mVerticalChainStyle) {
        this.mVerticalChainStyle = mVerticalChainStyle;
    }
    
    public void setVerticalDimension(int mHeight, int mMinHeight) {
        this.mY = mHeight;
        this.mHeight = mMinHeight - mHeight;
        mHeight = this.mHeight;
        mMinHeight = this.mMinHeight;
        if (mHeight < mMinHeight) {
            this.mHeight = mMinHeight;
        }
    }
    
    public void setVerticalDimensionBehaviour(final DimensionBehaviour dimensionBehaviour) {
        this.mListDimensionBehaviors[1] = dimensionBehaviour;
        if (dimensionBehaviour == DimensionBehaviour.WRAP_CONTENT) {
            this.setHeight(this.mWrapHeight);
        }
    }
    
    public void setVerticalMatchStyle(final int mMatchConstraintDefaultHeight, final int mMatchConstraintMinHeight, final int mMatchConstraintMaxHeight, final float mMatchConstraintPercentHeight) {
        this.mMatchConstraintDefaultHeight = mMatchConstraintDefaultHeight;
        this.mMatchConstraintMinHeight = mMatchConstraintMinHeight;
        this.mMatchConstraintMaxHeight = mMatchConstraintMaxHeight;
        this.mMatchConstraintPercentHeight = mMatchConstraintPercentHeight;
        if (mMatchConstraintPercentHeight < 1.0f && this.mMatchConstraintDefaultHeight == 0) {
            this.mMatchConstraintDefaultHeight = 2;
        }
    }
    
    public void setVerticalWeight(final float n) {
        this.mWeight[1] = n;
    }
    
    public void setVisibility(final int mVisibility) {
        this.mVisibility = mVisibility;
    }
    
    public void setWidth(int mWidth) {
        this.mWidth = mWidth;
        mWidth = this.mWidth;
        final int mMinWidth = this.mMinWidth;
        if (mWidth < mMinWidth) {
            this.mWidth = mMinWidth;
        }
    }
    
    public void setWidthWrapContent(final boolean mIsWidthWrapContent) {
        this.mIsWidthWrapContent = mIsWidthWrapContent;
    }
    
    public void setWrapHeight(final int mWrapHeight) {
        this.mWrapHeight = mWrapHeight;
    }
    
    public void setWrapWidth(final int mWrapWidth) {
        this.mWrapWidth = mWrapWidth;
    }
    
    public void setX(final int mx) {
        this.mX = mx;
    }
    
    public void setY(final int my) {
        this.mY = my;
    }
    
    public void setupDimensionRatio(final boolean b, final boolean b2, final boolean b3, final boolean b4) {
        if (this.mResolvedDimensionRatioSide == -1) {
            if (b3 && !b4) {
                this.mResolvedDimensionRatioSide = 0;
            }
            else if (!b3 && b4) {
                this.mResolvedDimensionRatioSide = 1;
                if (this.mDimensionRatioSide == -1) {
                    this.mResolvedDimensionRatio = 1.0f / this.mResolvedDimensionRatio;
                }
            }
        }
        if (this.mResolvedDimensionRatioSide == 0 && (!this.mTop.isConnected() || !this.mBottom.isConnected())) {
            this.mResolvedDimensionRatioSide = 1;
        }
        else if (this.mResolvedDimensionRatioSide == 1 && (!this.mLeft.isConnected() || !this.mRight.isConnected())) {
            this.mResolvedDimensionRatioSide = 0;
        }
        if (this.mResolvedDimensionRatioSide == -1 && (!this.mTop.isConnected() || !this.mBottom.isConnected() || !this.mLeft.isConnected() || !this.mRight.isConnected())) {
            if (this.mTop.isConnected() && this.mBottom.isConnected()) {
                this.mResolvedDimensionRatioSide = 0;
            }
            else if (this.mLeft.isConnected() && this.mRight.isConnected()) {
                this.mResolvedDimensionRatio = 1.0f / this.mResolvedDimensionRatio;
                this.mResolvedDimensionRatioSide = 1;
            }
        }
        if (this.mResolvedDimensionRatioSide == -1) {
            if (b && !b2) {
                this.mResolvedDimensionRatioSide = 0;
            }
            else if (!b && b2) {
                this.mResolvedDimensionRatio = 1.0f / this.mResolvedDimensionRatio;
                this.mResolvedDimensionRatioSide = 1;
            }
        }
        if (this.mResolvedDimensionRatioSide == -1) {
            if (this.mMatchConstraintMinWidth > 0 && this.mMatchConstraintMinHeight == 0) {
                this.mResolvedDimensionRatioSide = 0;
            }
            else if (this.mMatchConstraintMinWidth == 0 && this.mMatchConstraintMinHeight > 0) {
                this.mResolvedDimensionRatio = 1.0f / this.mResolvedDimensionRatio;
                this.mResolvedDimensionRatioSide = 1;
            }
        }
        if (this.mResolvedDimensionRatioSide == -1 && b && b2) {
            this.mResolvedDimensionRatio = 1.0f / this.mResolvedDimensionRatio;
            this.mResolvedDimensionRatioSide = 1;
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        final String mType = this.mType;
        final String s = "";
        String string;
        if (mType != null) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("type: ");
            sb2.append(this.mType);
            sb2.append(" ");
            string = sb2.toString();
        }
        else {
            string = "";
        }
        sb.append(string);
        String string2 = s;
        if (this.mDebugName != null) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("id: ");
            sb3.append(this.mDebugName);
            sb3.append(" ");
            string2 = sb3.toString();
        }
        sb.append(string2);
        sb.append("(");
        sb.append(this.mX);
        sb.append(", ");
        sb.append(this.mY);
        sb.append(") - (");
        sb.append(this.mWidth);
        sb.append(" x ");
        sb.append(this.mHeight);
        sb.append(") wrap: (");
        sb.append(this.mWrapWidth);
        sb.append(" x ");
        sb.append(this.mWrapHeight);
        sb.append(")");
        return sb.toString();
    }
    
    public void updateDrawPosition() {
        final int mx = this.mX;
        final int my = this.mY;
        final int mWidth = this.mWidth;
        final int mHeight = this.mHeight;
        this.mDrawX = mx;
        this.mDrawY = my;
        this.mDrawWidth = mWidth + mx - mx;
        this.mDrawHeight = mHeight + my - my;
    }
    
    public void updateFromSolver(final LinearSystem linearSystem) {
        int objectVariableValue = linearSystem.getObjectVariableValue(this.mLeft);
        int objectVariableValue2 = linearSystem.getObjectVariableValue(this.mTop);
        int objectVariableValue3 = linearSystem.getObjectVariableValue(this.mRight);
        final int objectVariableValue4 = linearSystem.getObjectVariableValue(this.mBottom);
        int n;
        if (objectVariableValue3 - objectVariableValue < 0 || objectVariableValue4 - objectVariableValue2 < 0 || objectVariableValue == Integer.MIN_VALUE || objectVariableValue == Integer.MAX_VALUE || objectVariableValue2 == Integer.MIN_VALUE || objectVariableValue2 == Integer.MAX_VALUE || objectVariableValue3 == Integer.MIN_VALUE || objectVariableValue3 == Integer.MAX_VALUE || objectVariableValue4 == Integer.MIN_VALUE || (n = objectVariableValue4) == Integer.MAX_VALUE) {
            n = (objectVariableValue = 0);
            objectVariableValue2 = (objectVariableValue3 = objectVariableValue);
        }
        this.setFrame(objectVariableValue, objectVariableValue2, objectVariableValue3, n);
    }
    
    public void updateResolutionNodes() {
        for (int i = 0; i < 6; ++i) {
            this.mListAnchors[i].getResolutionNode().update();
        }
    }
    
    public enum ContentAlignment
    {
        BEGIN, 
        BOTTOM, 
        END, 
        LEFT, 
        MIDDLE, 
        RIGHT, 
        TOP, 
        VERTICAL_MIDDLE;
    }
    
    public enum DimensionBehaviour
    {
        FIXED, 
        MATCH_CONSTRAINT, 
        MATCH_PARENT, 
        WRAP_CONTENT;
    }
}
