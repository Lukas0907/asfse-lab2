// 
// Decompiled by Procyon v0.5.36
// 

package androidx.constraintlayout.solver;

import java.io.PrintStream;
import androidx.constraintlayout.solver.widgets.ConstraintAnchor;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import java.util.Arrays;
import java.util.HashMap;

public class LinearSystem
{
    private static final boolean DEBUG = false;
    public static final boolean FULL_DEBUG = false;
    private static int POOL_SIZE = 1000;
    public static Metrics sMetrics;
    private int TABLE_SIZE;
    public boolean graphOptimizer;
    private boolean[] mAlreadyTestedCandidates;
    final Cache mCache;
    private Row mGoal;
    private int mMaxColumns;
    private int mMaxRows;
    int mNumColumns;
    int mNumRows;
    private SolverVariable[] mPoolVariables;
    private int mPoolVariablesCount;
    ArrayRow[] mRows;
    private final Row mTempGoal;
    private HashMap<String, SolverVariable> mVariables;
    int mVariablesID;
    private ArrayRow[] tempClientsCopy;
    
    public LinearSystem() {
        this.mVariablesID = 0;
        this.mVariables = null;
        this.TABLE_SIZE = 32;
        final int table_SIZE = this.TABLE_SIZE;
        this.mMaxColumns = table_SIZE;
        this.mRows = null;
        this.graphOptimizer = false;
        this.mAlreadyTestedCandidates = new boolean[table_SIZE];
        this.mNumColumns = 1;
        this.mNumRows = 0;
        this.mMaxRows = table_SIZE;
        this.mPoolVariables = new SolverVariable[LinearSystem.POOL_SIZE];
        this.mPoolVariablesCount = 0;
        this.tempClientsCopy = new ArrayRow[table_SIZE];
        this.mRows = new ArrayRow[table_SIZE];
        this.releaseRows();
        this.mCache = new Cache();
        this.mGoal = (Row)new GoalRow(this.mCache);
        this.mTempGoal = (Row)new ArrayRow(this.mCache);
    }
    
    private SolverVariable acquireSolverVariable(final SolverVariable.Type type, final String s) {
        final SolverVariable solverVariable = this.mCache.solverVariablePool.acquire();
        SolverVariable solverVariable3;
        if (solverVariable == null) {
            final SolverVariable solverVariable2 = new SolverVariable(type, s);
            solverVariable2.setType(type, s);
            solverVariable3 = solverVariable2;
        }
        else {
            solverVariable.reset();
            solverVariable.setType(type, s);
            solverVariable3 = solverVariable;
        }
        final int mPoolVariablesCount = this.mPoolVariablesCount;
        final int pool_SIZE = LinearSystem.POOL_SIZE;
        if (mPoolVariablesCount >= pool_SIZE) {
            LinearSystem.POOL_SIZE = pool_SIZE * 2;
            this.mPoolVariables = Arrays.copyOf(this.mPoolVariables, LinearSystem.POOL_SIZE);
        }
        return this.mPoolVariables[this.mPoolVariablesCount++] = solverVariable3;
    }
    
    private void addError(final ArrayRow arrayRow) {
        arrayRow.addError(this, 0);
    }
    
    private final void addRow(final ArrayRow arrayRow) {
        if (this.mRows[this.mNumRows] != null) {
            this.mCache.arrayRowPool.release(this.mRows[this.mNumRows]);
        }
        this.mRows[this.mNumRows] = arrayRow;
        final SolverVariable variable = arrayRow.variable;
        final int mNumRows = this.mNumRows;
        variable.definitionId = mNumRows;
        this.mNumRows = mNumRows + 1;
        arrayRow.variable.updateReferencesWithNewDefinition(arrayRow);
    }
    
    private void addSingleError(final ArrayRow arrayRow, final int n) {
        this.addSingleError(arrayRow, n, 0);
    }
    
    private void computeValues() {
        for (int i = 0; i < this.mNumRows; ++i) {
            final ArrayRow arrayRow = this.mRows[i];
            arrayRow.variable.computedValue = arrayRow.constantValue;
        }
    }
    
    public static ArrayRow createRowCentering(final LinearSystem linearSystem, final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final float n2, final SolverVariable solverVariable3, final SolverVariable solverVariable4, final int n3, final boolean b) {
        final ArrayRow row = linearSystem.createRow();
        row.createRowCentering(solverVariable, solverVariable2, n, n2, solverVariable3, solverVariable4, n3);
        if (b) {
            row.addError(linearSystem, 4);
        }
        return row;
    }
    
    public static ArrayRow createRowDimensionPercent(final LinearSystem linearSystem, final SolverVariable solverVariable, final SolverVariable solverVariable2, final SolverVariable solverVariable3, final float n, final boolean b) {
        final ArrayRow row = linearSystem.createRow();
        if (b) {
            linearSystem.addError(row);
        }
        return row.createRowDimensionPercent(solverVariable, solverVariable2, solverVariable3, n);
    }
    
    public static ArrayRow createRowEquals(final LinearSystem linearSystem, final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final boolean b) {
        final ArrayRow row = linearSystem.createRow();
        row.createRowEquals(solverVariable, solverVariable2, n);
        if (b) {
            linearSystem.addSingleError(row, 1);
        }
        return row;
    }
    
    public static ArrayRow createRowGreaterThan(final LinearSystem linearSystem, final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final boolean b) {
        final SolverVariable slackVariable = linearSystem.createSlackVariable();
        final ArrayRow row = linearSystem.createRow();
        row.createRowGreaterThan(solverVariable, solverVariable2, slackVariable, n);
        if (b) {
            linearSystem.addSingleError(row, (int)(row.variables.get(slackVariable) * -1.0f));
        }
        return row;
    }
    
    public static ArrayRow createRowLowerThan(final LinearSystem linearSystem, final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final boolean b) {
        final SolverVariable slackVariable = linearSystem.createSlackVariable();
        final ArrayRow row = linearSystem.createRow();
        row.createRowLowerThan(solverVariable, solverVariable2, slackVariable, n);
        if (b) {
            linearSystem.addSingleError(row, (int)(row.variables.get(slackVariable) * -1.0f));
        }
        return row;
    }
    
    private SolverVariable createVariable(final String s, final SolverVariable.Type type) {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.variables;
        }
        if (this.mNumColumns + 1 >= this.mMaxColumns) {
            this.increaseTableSize();
        }
        final SolverVariable acquireSolverVariable = this.acquireSolverVariable(type, null);
        acquireSolverVariable.setName(s);
        ++this.mVariablesID;
        ++this.mNumColumns;
        acquireSolverVariable.id = this.mVariablesID;
        if (this.mVariables == null) {
            this.mVariables = new HashMap<String, SolverVariable>();
        }
        this.mVariables.put(s, acquireSolverVariable);
        return this.mCache.mIndexedVariables[this.mVariablesID] = acquireSolverVariable;
    }
    
    private void displayRows() {
        this.displaySolverVariables();
        String string = "";
        for (int i = 0; i < this.mNumRows; ++i) {
            final StringBuilder sb = new StringBuilder();
            sb.append(string);
            sb.append(this.mRows[i]);
            final String string2 = sb.toString();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(string2);
            sb2.append("\n");
            string = sb2.toString();
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(string);
        sb3.append(this.mGoal);
        sb3.append("\n");
        System.out.println(sb3.toString());
    }
    
    private void displaySolverVariables() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Display Rows (");
        sb.append(this.mNumRows);
        sb.append("x");
        sb.append(this.mNumColumns);
        sb.append(")\n");
        System.out.println(sb.toString());
    }
    
    private int enforceBFS(final Row row) throws Exception {
        while (true) {
            for (int i = 0; i < this.mNumRows; ++i) {
                if (this.mRows[i].variable.mType != SolverVariable.Type.UNRESTRICTED) {
                    if (this.mRows[i].constantValue < 0.0f) {
                        final boolean b = true;
                        int n3;
                        if (b) {
                            int n = 0;
                            int n2 = 0;
                            while (true) {
                                n3 = n2;
                                if (n != 0) {
                                    break;
                                }
                                final Metrics sMetrics = LinearSystem.sMetrics;
                                if (sMetrics != null) {
                                    ++sMetrics.bfs;
                                }
                                final int n4 = n2 + 1;
                                float n5 = Float.MAX_VALUE;
                                int n6;
                                int definitionId = n6 = -1;
                                int j = 0;
                                int n7 = 0;
                                while (j < this.mNumRows) {
                                    final ArrayRow arrayRow = this.mRows[j];
                                    int n8;
                                    int n9;
                                    float n10;
                                    int n11;
                                    if (arrayRow.variable.mType == SolverVariable.Type.UNRESTRICTED) {
                                        n8 = definitionId;
                                        n9 = n6;
                                        n10 = n5;
                                        n11 = n7;
                                    }
                                    else if (arrayRow.isSimpleDefinition) {
                                        n8 = definitionId;
                                        n9 = n6;
                                        n10 = n5;
                                        n11 = n7;
                                    }
                                    else {
                                        n8 = definitionId;
                                        n9 = n6;
                                        n10 = n5;
                                        n11 = n7;
                                        if (arrayRow.constantValue < 0.0f) {
                                            int n12 = 1;
                                            while (true) {
                                                n8 = definitionId;
                                                n9 = n6;
                                                n10 = n5;
                                                n11 = n7;
                                                if (n12 >= this.mNumColumns) {
                                                    break;
                                                }
                                                final SolverVariable solverVariable = this.mCache.mIndexedVariables[n12];
                                                final float value = arrayRow.variables.get(solverVariable);
                                                if (value > 0.0f) {
                                                    final int n13 = n7;
                                                    final int n14 = 0;
                                                    int n15 = n6;
                                                    int n16 = definitionId;
                                                    int k = n14;
                                                    int n17 = n13;
                                                    while (k < 7) {
                                                        final float n18 = solverVariable.strengthVector[k] / value;
                                                        int n19;
                                                        if ((n18 < n5 && k == n17) || k > (n19 = n17)) {
                                                            n15 = n12;
                                                            n16 = j;
                                                            n5 = n18;
                                                            n19 = k;
                                                        }
                                                        ++k;
                                                        n17 = n19;
                                                    }
                                                    final int n20 = n15;
                                                    n7 = n17;
                                                    n6 = n20;
                                                    definitionId = n16;
                                                }
                                                ++n12;
                                            }
                                        }
                                    }
                                    ++j;
                                    definitionId = n8;
                                    n6 = n9;
                                    n5 = n10;
                                    n7 = n11;
                                }
                                if (definitionId != -1) {
                                    final ArrayRow arrayRow2 = this.mRows[definitionId];
                                    arrayRow2.variable.definitionId = -1;
                                    final Metrics sMetrics2 = LinearSystem.sMetrics;
                                    if (sMetrics2 != null) {
                                        ++sMetrics2.pivots;
                                    }
                                    arrayRow2.pivot(this.mCache.mIndexedVariables[n6]);
                                    arrayRow2.variable.definitionId = definitionId;
                                    arrayRow2.variable.updateReferencesWithNewDefinition(arrayRow2);
                                }
                                else {
                                    n = 1;
                                }
                                if (n4 > this.mNumColumns / 2) {
                                    n = 1;
                                }
                                n2 = n4;
                            }
                        }
                        else {
                            n3 = 0;
                        }
                        return n3;
                    }
                }
            }
            final boolean b = false;
            continue;
        }
    }
    
    private String getDisplaySize(int i) {
        i *= 4;
        final int j = i / 1024;
        final int k = j / 1024;
        if (k > 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(k);
            sb.append(" Mb");
            return sb.toString();
        }
        if (j > 0) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("");
            sb2.append(j);
            sb2.append(" Kb");
            return sb2.toString();
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("");
        sb3.append(i);
        sb3.append(" bytes");
        return sb3.toString();
    }
    
    private String getDisplayStrength(final int n) {
        if (n == 1) {
            return "LOW";
        }
        if (n == 2) {
            return "MEDIUM";
        }
        if (n == 3) {
            return "HIGH";
        }
        if (n == 4) {
            return "HIGHEST";
        }
        if (n == 5) {
            return "EQUALITY";
        }
        if (n == 6) {
            return "FIXED";
        }
        return "NONE";
    }
    
    public static Metrics getMetrics() {
        return LinearSystem.sMetrics;
    }
    
    private void increaseTableSize() {
        this.TABLE_SIZE *= 2;
        this.mRows = Arrays.copyOf(this.mRows, this.TABLE_SIZE);
        final Cache mCache = this.mCache;
        mCache.mIndexedVariables = Arrays.copyOf(mCache.mIndexedVariables, this.TABLE_SIZE);
        final int table_SIZE = this.TABLE_SIZE;
        this.mAlreadyTestedCandidates = new boolean[table_SIZE];
        this.mMaxColumns = table_SIZE;
        this.mMaxRows = table_SIZE;
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.tableSizeIncrease;
            final Metrics sMetrics2 = LinearSystem.sMetrics;
            sMetrics2.maxTableSize = Math.max(sMetrics2.maxTableSize, this.TABLE_SIZE);
            final Metrics sMetrics3 = LinearSystem.sMetrics;
            sMetrics3.lastTableSize = sMetrics3.maxTableSize;
        }
    }
    
    private final int optimize(final Row row, final boolean b) {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.optimize;
        }
        for (int i = 0; i < this.mNumColumns; ++i) {
            this.mAlreadyTestedCandidates[i] = false;
        }
        int n;
        int j = n = 0;
        while (j == 0) {
            final Metrics sMetrics2 = LinearSystem.sMetrics;
            if (sMetrics2 != null) {
                ++sMetrics2.iterations;
            }
            final int n2 = n + 1;
            if (n2 >= this.mNumColumns * 2) {
                return n2;
            }
            if (row.getKey() != null) {
                this.mAlreadyTestedCandidates[row.getKey().id] = true;
            }
            final SolverVariable pivotCandidate = row.getPivotCandidate(this, this.mAlreadyTestedCandidates);
            if (pivotCandidate != null) {
                if (this.mAlreadyTestedCandidates[pivotCandidate.id]) {
                    return n2;
                }
                this.mAlreadyTestedCandidates[pivotCandidate.id] = true;
            }
            if (pivotCandidate != null) {
                float n3 = Float.MAX_VALUE;
                int definitionId = -1;
                int n4;
                float n5;
                for (int k = 0; k < this.mNumRows; ++k, definitionId = n4, n3 = n5) {
                    final ArrayRow arrayRow = this.mRows[k];
                    if (arrayRow.variable.mType == SolverVariable.Type.UNRESTRICTED) {
                        n4 = definitionId;
                        n5 = n3;
                    }
                    else if (arrayRow.isSimpleDefinition) {
                        n4 = definitionId;
                        n5 = n3;
                    }
                    else {
                        n4 = definitionId;
                        n5 = n3;
                        if (arrayRow.hasVariable(pivotCandidate)) {
                            final float value = arrayRow.variables.get(pivotCandidate);
                            n4 = definitionId;
                            n5 = n3;
                            if (value < 0.0f) {
                                final float n6 = -arrayRow.constantValue / value;
                                n4 = definitionId;
                                n5 = n3;
                                if (n6 < n3) {
                                    n4 = k;
                                    n5 = n6;
                                }
                            }
                        }
                    }
                }
                if (definitionId > -1) {
                    final ArrayRow arrayRow2 = this.mRows[definitionId];
                    arrayRow2.variable.definitionId = -1;
                    final Metrics sMetrics3 = LinearSystem.sMetrics;
                    if (sMetrics3 != null) {
                        ++sMetrics3.pivots;
                    }
                    arrayRow2.pivot(pivotCandidate);
                    arrayRow2.variable.definitionId = definitionId;
                    arrayRow2.variable.updateReferencesWithNewDefinition(arrayRow2);
                    n = n2;
                    continue;
                }
            }
            j = 1;
            n = n2;
        }
        return n;
    }
    
    private void releaseRows() {
        int n = 0;
        while (true) {
            final ArrayRow[] mRows = this.mRows;
            if (n >= mRows.length) {
                break;
            }
            final ArrayRow arrayRow = mRows[n];
            if (arrayRow != null) {
                this.mCache.arrayRowPool.release(arrayRow);
            }
            this.mRows[n] = null;
            ++n;
        }
    }
    
    private final void updateRowFromVariables(final ArrayRow arrayRow) {
        if (this.mNumRows > 0) {
            arrayRow.variables.updateFromSystem(arrayRow, this.mRows);
            if (arrayRow.variables.currentSize == 0) {
                arrayRow.isSimpleDefinition = true;
            }
        }
    }
    
    public void addCenterPoint(final ConstraintWidget constraintWidget, final ConstraintWidget constraintWidget2, final float n, final int n2) {
        final SolverVariable objectVariable = this.createObjectVariable(constraintWidget.getAnchor(ConstraintAnchor.Type.LEFT));
        final SolverVariable objectVariable2 = this.createObjectVariable(constraintWidget.getAnchor(ConstraintAnchor.Type.TOP));
        final SolverVariable objectVariable3 = this.createObjectVariable(constraintWidget.getAnchor(ConstraintAnchor.Type.RIGHT));
        final SolverVariable objectVariable4 = this.createObjectVariable(constraintWidget.getAnchor(ConstraintAnchor.Type.BOTTOM));
        final SolverVariable objectVariable5 = this.createObjectVariable(constraintWidget2.getAnchor(ConstraintAnchor.Type.LEFT));
        final SolverVariable objectVariable6 = this.createObjectVariable(constraintWidget2.getAnchor(ConstraintAnchor.Type.TOP));
        final SolverVariable objectVariable7 = this.createObjectVariable(constraintWidget2.getAnchor(ConstraintAnchor.Type.RIGHT));
        final SolverVariable objectVariable8 = this.createObjectVariable(constraintWidget2.getAnchor(ConstraintAnchor.Type.BOTTOM));
        final ArrayRow row = this.createRow();
        final double n3 = n;
        final double sin = Math.sin(n3);
        final double n4 = n2;
        row.createRowWithAngle(objectVariable2, objectVariable4, objectVariable6, objectVariable8, (float)(sin * n4));
        this.addConstraint(row);
        final ArrayRow row2 = this.createRow();
        row2.createRowWithAngle(objectVariable, objectVariable3, objectVariable5, objectVariable7, (float)(Math.cos(n3) * n4));
        this.addConstraint(row2);
    }
    
    public void addCentering(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final float n2, final SolverVariable solverVariable3, final SolverVariable solverVariable4, final int n3, final int n4) {
        final ArrayRow row = this.createRow();
        row.createRowCentering(solverVariable, solverVariable2, n, n2, solverVariable3, solverVariable4, n3);
        if (n4 != 6) {
            row.addError(this, n4);
        }
        this.addConstraint(row);
    }
    
    public void addConstraint(final ArrayRow arrayRow) {
        if (arrayRow == null) {
            return;
        }
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.constraints;
            if (arrayRow.isSimpleDefinition) {
                final Metrics sMetrics2 = LinearSystem.sMetrics;
                ++sMetrics2.simpleconstraints;
            }
        }
        if (this.mNumRows + 1 >= this.mMaxRows || this.mNumColumns + 1 >= this.mMaxColumns) {
            this.increaseTableSize();
        }
        int n = 0;
        final int n2 = 0;
        if (!arrayRow.isSimpleDefinition) {
            this.updateRowFromVariables(arrayRow);
            if (arrayRow.isEmpty()) {
                return;
            }
            arrayRow.ensurePositiveConstant();
            n = n2;
            if (arrayRow.chooseSubject(this)) {
                final SolverVariable extraVariable = this.createExtraVariable();
                arrayRow.variable = extraVariable;
                this.addRow(arrayRow);
                this.mTempGoal.initFromRow((Row)arrayRow);
                this.optimize(this.mTempGoal, true);
                if (extraVariable.definitionId == -1) {
                    if (arrayRow.variable == extraVariable) {
                        final SolverVariable pickPivot = arrayRow.pickPivot(extraVariable);
                        if (pickPivot != null) {
                            final Metrics sMetrics3 = LinearSystem.sMetrics;
                            if (sMetrics3 != null) {
                                ++sMetrics3.pivots;
                            }
                            arrayRow.pivot(pickPivot);
                        }
                    }
                    if (!arrayRow.isSimpleDefinition) {
                        arrayRow.variable.updateReferencesWithNewDefinition(arrayRow);
                    }
                    --this.mNumRows;
                }
                n = 1;
            }
            if (!arrayRow.hasKeyVariable()) {
                return;
            }
        }
        if (n == 0) {
            this.addRow(arrayRow);
        }
    }
    
    public ArrayRow addEquality(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final int n2) {
        final ArrayRow row = this.createRow();
        row.createRowEquals(solverVariable, solverVariable2, n);
        if (n2 != 6) {
            row.addError(this, n2);
        }
        this.addConstraint(row);
        return row;
    }
    
    public void addEquality(final SolverVariable solverVariable, final int n) {
        final int definitionId = solverVariable.definitionId;
        if (solverVariable.definitionId == -1) {
            final ArrayRow row = this.createRow();
            row.createRowDefinition(solverVariable, n);
            this.addConstraint(row);
            return;
        }
        final ArrayRow arrayRow = this.mRows[definitionId];
        if (arrayRow.isSimpleDefinition) {
            arrayRow.constantValue = (float)n;
            return;
        }
        if (arrayRow.variables.currentSize == 0) {
            arrayRow.isSimpleDefinition = true;
            arrayRow.constantValue = (float)n;
            return;
        }
        final ArrayRow row2 = this.createRow();
        row2.createRowEquals(solverVariable, n);
        this.addConstraint(row2);
    }
    
    public void addEquality(final SolverVariable solverVariable, final int n, final int n2) {
        final int definitionId = solverVariable.definitionId;
        if (solverVariable.definitionId == -1) {
            final ArrayRow row = this.createRow();
            row.createRowDefinition(solverVariable, n);
            row.addError(this, n2);
            this.addConstraint(row);
            return;
        }
        final ArrayRow arrayRow = this.mRows[definitionId];
        if (arrayRow.isSimpleDefinition) {
            arrayRow.constantValue = (float)n;
            return;
        }
        final ArrayRow row2 = this.createRow();
        row2.createRowEquals(solverVariable, n);
        row2.addError(this, n2);
        this.addConstraint(row2);
    }
    
    public void addGreaterBarrier(final SolverVariable solverVariable, final SolverVariable solverVariable2, final boolean b) {
        final ArrayRow row = this.createRow();
        final SolverVariable slackVariable = this.createSlackVariable();
        row.createRowGreaterThan(solverVariable, solverVariable2, slackVariable, slackVariable.strength = 0);
        if (b) {
            this.addSingleError(row, (int)(row.variables.get(slackVariable) * -1.0f), 1);
        }
        this.addConstraint(row);
    }
    
    public void addGreaterThan(final SolverVariable solverVariable, final int n) {
        final ArrayRow row = this.createRow();
        final SolverVariable slackVariable = this.createSlackVariable();
        slackVariable.strength = 0;
        row.createRowGreaterThan(solverVariable, n, slackVariable);
        this.addConstraint(row);
    }
    
    public void addGreaterThan(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final int n2) {
        final ArrayRow row = this.createRow();
        final SolverVariable slackVariable = this.createSlackVariable();
        slackVariable.strength = 0;
        row.createRowGreaterThan(solverVariable, solverVariable2, slackVariable, n);
        if (n2 != 6) {
            this.addSingleError(row, (int)(row.variables.get(slackVariable) * -1.0f), n2);
        }
        this.addConstraint(row);
    }
    
    public void addLowerBarrier(final SolverVariable solverVariable, final SolverVariable solverVariable2, final boolean b) {
        final ArrayRow row = this.createRow();
        final SolverVariable slackVariable = this.createSlackVariable();
        row.createRowLowerThan(solverVariable, solverVariable2, slackVariable, slackVariable.strength = 0);
        if (b) {
            this.addSingleError(row, (int)(row.variables.get(slackVariable) * -1.0f), 1);
        }
        this.addConstraint(row);
    }
    
    public void addLowerThan(final SolverVariable solverVariable, final SolverVariable solverVariable2, final int n, final int n2) {
        final ArrayRow row = this.createRow();
        final SolverVariable slackVariable = this.createSlackVariable();
        slackVariable.strength = 0;
        row.createRowLowerThan(solverVariable, solverVariable2, slackVariable, n);
        if (n2 != 6) {
            this.addSingleError(row, (int)(row.variables.get(slackVariable) * -1.0f), n2);
        }
        this.addConstraint(row);
    }
    
    public void addRatio(final SolverVariable solverVariable, final SolverVariable solverVariable2, final SolverVariable solverVariable3, final SolverVariable solverVariable4, final float n, final int n2) {
        final ArrayRow row = this.createRow();
        row.createRowDimensionRatio(solverVariable, solverVariable2, solverVariable3, solverVariable4, n);
        if (n2 != 6) {
            row.addError(this, n2);
        }
        this.addConstraint(row);
    }
    
    void addSingleError(final ArrayRow arrayRow, final int n, final int n2) {
        arrayRow.addSingleError(this.createErrorVariable(n2, null), n);
    }
    
    public SolverVariable createErrorVariable(final int strength, final String s) {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.errors;
        }
        if (this.mNumColumns + 1 >= this.mMaxColumns) {
            this.increaseTableSize();
        }
        final SolverVariable acquireSolverVariable = this.acquireSolverVariable(SolverVariable.Type.ERROR, s);
        ++this.mVariablesID;
        ++this.mNumColumns;
        acquireSolverVariable.id = this.mVariablesID;
        acquireSolverVariable.strength = strength;
        this.mCache.mIndexedVariables[this.mVariablesID] = acquireSolverVariable;
        this.mGoal.addError(acquireSolverVariable);
        return acquireSolverVariable;
    }
    
    public SolverVariable createExtraVariable() {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.extravariables;
        }
        if (this.mNumColumns + 1 >= this.mMaxColumns) {
            this.increaseTableSize();
        }
        final SolverVariable acquireSolverVariable = this.acquireSolverVariable(SolverVariable.Type.SLACK, null);
        ++this.mVariablesID;
        ++this.mNumColumns;
        acquireSolverVariable.id = this.mVariablesID;
        return this.mCache.mIndexedVariables[this.mVariablesID] = acquireSolverVariable;
    }
    
    public SolverVariable createObjectVariable(final Object o) {
        SolverVariable solverVariable = null;
        if (o == null) {
            return null;
        }
        if (this.mNumColumns + 1 >= this.mMaxColumns) {
            this.increaseTableSize();
        }
        if (o instanceof ConstraintAnchor) {
            final ConstraintAnchor constraintAnchor = (ConstraintAnchor)o;
            SolverVariable solverVariable2;
            if ((solverVariable2 = constraintAnchor.getSolverVariable()) == null) {
                constraintAnchor.resetSolverVariable(this.mCache);
                solverVariable2 = constraintAnchor.getSolverVariable();
            }
            if (solverVariable2.id != -1 && solverVariable2.id <= this.mVariablesID) {
                solverVariable = solverVariable2;
                if (this.mCache.mIndexedVariables[solverVariable2.id] != null) {
                    return solverVariable;
                }
            }
            if (solverVariable2.id != -1) {
                solverVariable2.reset();
            }
            ++this.mVariablesID;
            ++this.mNumColumns;
            solverVariable2.id = this.mVariablesID;
            solverVariable2.mType = SolverVariable.Type.UNRESTRICTED;
            this.mCache.mIndexedVariables[this.mVariablesID] = solverVariable2;
            solverVariable = solverVariable2;
        }
        return solverVariable;
    }
    
    public ArrayRow createRow() {
        ArrayRow arrayRow = this.mCache.arrayRowPool.acquire();
        if (arrayRow == null) {
            arrayRow = new ArrayRow(this.mCache);
        }
        else {
            arrayRow.reset();
        }
        SolverVariable.increaseErrorId();
        return arrayRow;
    }
    
    public SolverVariable createSlackVariable() {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.slackvariables;
        }
        if (this.mNumColumns + 1 >= this.mMaxColumns) {
            this.increaseTableSize();
        }
        final SolverVariable acquireSolverVariable = this.acquireSolverVariable(SolverVariable.Type.SLACK, null);
        ++this.mVariablesID;
        ++this.mNumColumns;
        acquireSolverVariable.id = this.mVariablesID;
        return this.mCache.mIndexedVariables[this.mVariablesID] = acquireSolverVariable;
    }
    
    void displayReadableRows() {
        this.displaySolverVariables();
        String string = " #  ";
        for (int i = 0; i < this.mNumRows; ++i) {
            final StringBuilder sb = new StringBuilder();
            sb.append(string);
            sb.append(this.mRows[i].toReadableString());
            final String string2 = sb.toString();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(string2);
            sb2.append("\n #  ");
            string = sb2.toString();
        }
        String string3 = string;
        if (this.mGoal != null) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(string);
            sb3.append(this.mGoal);
            sb3.append("\n");
            string3 = sb3.toString();
        }
        System.out.println(string3);
    }
    
    void displaySystemInformations() {
        int n;
        int n2;
        for (int i = n = 0; i < this.TABLE_SIZE; ++i, n = n2) {
            final ArrayRow[] mRows = this.mRows;
            n2 = n;
            if (mRows[i] != null) {
                n2 = n + mRows[i].sizeInBytes();
            }
        }
        int n3;
        int n4;
        for (int j = n3 = 0; j < this.mNumRows; ++j, n3 = n4) {
            final ArrayRow[] mRows2 = this.mRows;
            n4 = n3;
            if (mRows2[j] != null) {
                n4 = n3 + mRows2[j].sizeInBytes();
            }
        }
        final PrintStream out = System.out;
        final StringBuilder sb = new StringBuilder();
        sb.append("Linear System -> Table size: ");
        sb.append(this.TABLE_SIZE);
        sb.append(" (");
        final int table_SIZE = this.TABLE_SIZE;
        sb.append(this.getDisplaySize(table_SIZE * table_SIZE));
        sb.append(") -- row sizes: ");
        sb.append(this.getDisplaySize(n));
        sb.append(", actual size: ");
        sb.append(this.getDisplaySize(n3));
        sb.append(" rows: ");
        sb.append(this.mNumRows);
        sb.append("/");
        sb.append(this.mMaxRows);
        sb.append(" cols: ");
        sb.append(this.mNumColumns);
        sb.append("/");
        sb.append(this.mMaxColumns);
        sb.append(" ");
        sb.append(0);
        sb.append(" occupied cells, ");
        sb.append(this.getDisplaySize(0));
        out.println(sb.toString());
    }
    
    public void displayVariablesReadableRows() {
        this.displaySolverVariables();
        String s = "";
        String string;
        for (int i = 0; i < this.mNumRows; ++i, s = string) {
            string = s;
            if (this.mRows[i].variable.mType == SolverVariable.Type.UNRESTRICTED) {
                final StringBuilder sb = new StringBuilder();
                sb.append(s);
                sb.append(this.mRows[i].toReadableString());
                final String string2 = sb.toString();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string2);
                sb2.append("\n");
                string = sb2.toString();
            }
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(s);
        sb3.append(this.mGoal);
        sb3.append("\n");
        System.out.println(sb3.toString());
    }
    
    public void fillMetrics(final Metrics sMetrics) {
        LinearSystem.sMetrics = sMetrics;
    }
    
    public Cache getCache() {
        return this.mCache;
    }
    
    Row getGoal() {
        return this.mGoal;
    }
    
    public int getMemoryUsed() {
        int i = 0;
        int n = 0;
        while (i < this.mNumRows) {
            final ArrayRow[] mRows = this.mRows;
            int n2 = n;
            if (mRows[i] != null) {
                n2 = n + mRows[i].sizeInBytes();
            }
            ++i;
            n = n2;
        }
        return n;
    }
    
    public int getNumEquations() {
        return this.mNumRows;
    }
    
    public int getNumVariables() {
        return this.mVariablesID;
    }
    
    public int getObjectVariableValue(final Object o) {
        final SolverVariable solverVariable = ((ConstraintAnchor)o).getSolverVariable();
        if (solverVariable != null) {
            return (int)(solverVariable.computedValue + 0.5f);
        }
        return 0;
    }
    
    ArrayRow getRow(final int n) {
        return this.mRows[n];
    }
    
    float getValueFor(final String s) {
        final SolverVariable variable = this.getVariable(s, SolverVariable.Type.UNRESTRICTED);
        if (variable == null) {
            return 0.0f;
        }
        return variable.computedValue;
    }
    
    SolverVariable getVariable(final String key, final SolverVariable.Type type) {
        if (this.mVariables == null) {
            this.mVariables = new HashMap<String, SolverVariable>();
        }
        SolverVariable variable;
        if ((variable = this.mVariables.get(key)) == null) {
            variable = this.createVariable(key, type);
        }
        return variable;
    }
    
    public void minimize() throws Exception {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.minimize;
        }
        if (this.graphOptimizer) {
            final Metrics sMetrics2 = LinearSystem.sMetrics;
            if (sMetrics2 != null) {
                ++sMetrics2.graphOptimizer;
            }
            final int n = 0;
            int i = 0;
            while (true) {
                while (i < this.mNumRows) {
                    if (!this.mRows[i].isSimpleDefinition) {
                        final int n2 = n;
                        if (n2 == 0) {
                            this.minimizeGoal(this.mGoal);
                            return;
                        }
                        final Metrics sMetrics3 = LinearSystem.sMetrics;
                        if (sMetrics3 != null) {
                            ++sMetrics3.fullySolved;
                        }
                        this.computeValues();
                        return;
                    }
                    else {
                        ++i;
                    }
                }
                final int n2 = 1;
                continue;
            }
        }
        this.minimizeGoal(this.mGoal);
    }
    
    void minimizeGoal(final Row row) throws Exception {
        final Metrics sMetrics = LinearSystem.sMetrics;
        if (sMetrics != null) {
            ++sMetrics.minimizeGoal;
            final Metrics sMetrics2 = LinearSystem.sMetrics;
            sMetrics2.maxVariables = Math.max(sMetrics2.maxVariables, this.mNumColumns);
            final Metrics sMetrics3 = LinearSystem.sMetrics;
            sMetrics3.maxRows = Math.max(sMetrics3.maxRows, this.mNumRows);
        }
        this.updateRowFromVariables((ArrayRow)row);
        this.enforceBFS(row);
        this.optimize(row, false);
        this.computeValues();
    }
    
    public void reset() {
        for (int i = 0; i < this.mCache.mIndexedVariables.length; ++i) {
            final SolverVariable solverVariable = this.mCache.mIndexedVariables[i];
            if (solverVariable != null) {
                solverVariable.reset();
            }
        }
        this.mCache.solverVariablePool.releaseAll(this.mPoolVariables, this.mPoolVariablesCount);
        this.mPoolVariablesCount = 0;
        Arrays.fill(this.mCache.mIndexedVariables, null);
        final HashMap<String, SolverVariable> mVariables = this.mVariables;
        if (mVariables != null) {
            mVariables.clear();
        }
        this.mVariablesID = 0;
        this.mGoal.clear();
        this.mNumColumns = 1;
        for (int j = 0; j < this.mNumRows; ++j) {
            this.mRows[j].used = false;
        }
        this.releaseRows();
        this.mNumRows = 0;
    }
    
    interface Row
    {
        void addError(final SolverVariable p0);
        
        void clear();
        
        SolverVariable getKey();
        
        SolverVariable getPivotCandidate(final LinearSystem p0, final boolean[] p1);
        
        void initFromRow(final Row p0);
        
        boolean isEmpty();
    }
}
