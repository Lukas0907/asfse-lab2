// 
// Decompiled by Procyon v0.5.36
// 

package androidx.constraintlayout.widget;

public final class R
{
    private R() {
    }
    
    public static final class attr
    {
        public static final int barrierAllowsGoneWidgets = 2130903095;
        public static final int barrierDirection = 2130903096;
        public static final int chainUseRtl = 2130903137;
        public static final int constraintSet = 2130903184;
        public static final int constraint_referenced_ids = 2130903185;
        public static final int content = 2130903186;
        public static final int emptyVisibility = 2130903225;
        public static final int layout_constrainedHeight = 2130903315;
        public static final int layout_constrainedWidth = 2130903316;
        public static final int layout_constraintBaseline_creator = 2130903317;
        public static final int layout_constraintBaseline_toBaselineOf = 2130903318;
        public static final int layout_constraintBottom_creator = 2130903319;
        public static final int layout_constraintBottom_toBottomOf = 2130903320;
        public static final int layout_constraintBottom_toTopOf = 2130903321;
        public static final int layout_constraintCircle = 2130903322;
        public static final int layout_constraintCircleAngle = 2130903323;
        public static final int layout_constraintCircleRadius = 2130903324;
        public static final int layout_constraintDimensionRatio = 2130903325;
        public static final int layout_constraintEnd_toEndOf = 2130903326;
        public static final int layout_constraintEnd_toStartOf = 2130903327;
        public static final int layout_constraintGuide_begin = 2130903328;
        public static final int layout_constraintGuide_end = 2130903329;
        public static final int layout_constraintGuide_percent = 2130903330;
        public static final int layout_constraintHeight_default = 2130903331;
        public static final int layout_constraintHeight_max = 2130903332;
        public static final int layout_constraintHeight_min = 2130903333;
        public static final int layout_constraintHeight_percent = 2130903334;
        public static final int layout_constraintHorizontal_bias = 2130903335;
        public static final int layout_constraintHorizontal_chainStyle = 2130903336;
        public static final int layout_constraintHorizontal_weight = 2130903337;
        public static final int layout_constraintLeft_creator = 2130903338;
        public static final int layout_constraintLeft_toLeftOf = 2130903339;
        public static final int layout_constraintLeft_toRightOf = 2130903340;
        public static final int layout_constraintRight_creator = 2130903341;
        public static final int layout_constraintRight_toLeftOf = 2130903342;
        public static final int layout_constraintRight_toRightOf = 2130903343;
        public static final int layout_constraintStart_toEndOf = 2130903344;
        public static final int layout_constraintStart_toStartOf = 2130903345;
        public static final int layout_constraintTop_creator = 2130903346;
        public static final int layout_constraintTop_toBottomOf = 2130903347;
        public static final int layout_constraintTop_toTopOf = 2130903348;
        public static final int layout_constraintVertical_bias = 2130903349;
        public static final int layout_constraintVertical_chainStyle = 2130903350;
        public static final int layout_constraintVertical_weight = 2130903351;
        public static final int layout_constraintWidth_default = 2130903352;
        public static final int layout_constraintWidth_max = 2130903353;
        public static final int layout_constraintWidth_min = 2130903354;
        public static final int layout_constraintWidth_percent = 2130903355;
        public static final int layout_editor_absoluteX = 2130903357;
        public static final int layout_editor_absoluteY = 2130903358;
        public static final int layout_goneMarginBottom = 2130903362;
        public static final int layout_goneMarginEnd = 2130903363;
        public static final int layout_goneMarginLeft = 2130903364;
        public static final int layout_goneMarginRight = 2130903365;
        public static final int layout_goneMarginStart = 2130903366;
        public static final int layout_goneMarginTop = 2130903367;
        public static final int layout_optimizationLevel = 2130903374;
        
        private attr() {
        }
    }
    
    public static final class id
    {
        public static final int bottom = 2131230788;
        public static final int end = 2131230820;
        public static final int gone = 2131230838;
        public static final int invisible = 2131230849;
        public static final int left = 2131230855;
        public static final int packed = 2131230875;
        public static final int parent = 2131230877;
        public static final int percent = 2131230881;
        public static final int right = 2131230888;
        public static final int spread = 2131230923;
        public static final int spread_inside = 2131230924;
        public static final int start = 2131230929;
        public static final int top = 2131230956;
        public static final int wrap = 2131230973;
        
        private id() {
        }
    }
    
    public static final class styleable
    {
        public static final int[] ConstraintLayout_Layout;
        public static final int ConstraintLayout_Layout_android_maxHeight = 2;
        public static final int ConstraintLayout_Layout_android_maxWidth = 1;
        public static final int ConstraintLayout_Layout_android_minHeight = 4;
        public static final int ConstraintLayout_Layout_android_minWidth = 3;
        public static final int ConstraintLayout_Layout_android_orientation = 0;
        public static final int ConstraintLayout_Layout_barrierAllowsGoneWidgets = 5;
        public static final int ConstraintLayout_Layout_barrierDirection = 6;
        public static final int ConstraintLayout_Layout_chainUseRtl = 7;
        public static final int ConstraintLayout_Layout_constraintSet = 8;
        public static final int ConstraintLayout_Layout_constraint_referenced_ids = 9;
        public static final int ConstraintLayout_Layout_layout_constrainedHeight = 10;
        public static final int ConstraintLayout_Layout_layout_constrainedWidth = 11;
        public static final int ConstraintLayout_Layout_layout_constraintBaseline_creator = 12;
        public static final int ConstraintLayout_Layout_layout_constraintBaseline_toBaselineOf = 13;
        public static final int ConstraintLayout_Layout_layout_constraintBottom_creator = 14;
        public static final int ConstraintLayout_Layout_layout_constraintBottom_toBottomOf = 15;
        public static final int ConstraintLayout_Layout_layout_constraintBottom_toTopOf = 16;
        public static final int ConstraintLayout_Layout_layout_constraintCircle = 17;
        public static final int ConstraintLayout_Layout_layout_constraintCircleAngle = 18;
        public static final int ConstraintLayout_Layout_layout_constraintCircleRadius = 19;
        public static final int ConstraintLayout_Layout_layout_constraintDimensionRatio = 20;
        public static final int ConstraintLayout_Layout_layout_constraintEnd_toEndOf = 21;
        public static final int ConstraintLayout_Layout_layout_constraintEnd_toStartOf = 22;
        public static final int ConstraintLayout_Layout_layout_constraintGuide_begin = 23;
        public static final int ConstraintLayout_Layout_layout_constraintGuide_end = 24;
        public static final int ConstraintLayout_Layout_layout_constraintGuide_percent = 25;
        public static final int ConstraintLayout_Layout_layout_constraintHeight_default = 26;
        public static final int ConstraintLayout_Layout_layout_constraintHeight_max = 27;
        public static final int ConstraintLayout_Layout_layout_constraintHeight_min = 28;
        public static final int ConstraintLayout_Layout_layout_constraintHeight_percent = 29;
        public static final int ConstraintLayout_Layout_layout_constraintHorizontal_bias = 30;
        public static final int ConstraintLayout_Layout_layout_constraintHorizontal_chainStyle = 31;
        public static final int ConstraintLayout_Layout_layout_constraintHorizontal_weight = 32;
        public static final int ConstraintLayout_Layout_layout_constraintLeft_creator = 33;
        public static final int ConstraintLayout_Layout_layout_constraintLeft_toLeftOf = 34;
        public static final int ConstraintLayout_Layout_layout_constraintLeft_toRightOf = 35;
        public static final int ConstraintLayout_Layout_layout_constraintRight_creator = 36;
        public static final int ConstraintLayout_Layout_layout_constraintRight_toLeftOf = 37;
        public static final int ConstraintLayout_Layout_layout_constraintRight_toRightOf = 38;
        public static final int ConstraintLayout_Layout_layout_constraintStart_toEndOf = 39;
        public static final int ConstraintLayout_Layout_layout_constraintStart_toStartOf = 40;
        public static final int ConstraintLayout_Layout_layout_constraintTop_creator = 41;
        public static final int ConstraintLayout_Layout_layout_constraintTop_toBottomOf = 42;
        public static final int ConstraintLayout_Layout_layout_constraintTop_toTopOf = 43;
        public static final int ConstraintLayout_Layout_layout_constraintVertical_bias = 44;
        public static final int ConstraintLayout_Layout_layout_constraintVertical_chainStyle = 45;
        public static final int ConstraintLayout_Layout_layout_constraintVertical_weight = 46;
        public static final int ConstraintLayout_Layout_layout_constraintWidth_default = 47;
        public static final int ConstraintLayout_Layout_layout_constraintWidth_max = 48;
        public static final int ConstraintLayout_Layout_layout_constraintWidth_min = 49;
        public static final int ConstraintLayout_Layout_layout_constraintWidth_percent = 50;
        public static final int ConstraintLayout_Layout_layout_editor_absoluteX = 51;
        public static final int ConstraintLayout_Layout_layout_editor_absoluteY = 52;
        public static final int ConstraintLayout_Layout_layout_goneMarginBottom = 53;
        public static final int ConstraintLayout_Layout_layout_goneMarginEnd = 54;
        public static final int ConstraintLayout_Layout_layout_goneMarginLeft = 55;
        public static final int ConstraintLayout_Layout_layout_goneMarginRight = 56;
        public static final int ConstraintLayout_Layout_layout_goneMarginStart = 57;
        public static final int ConstraintLayout_Layout_layout_goneMarginTop = 58;
        public static final int ConstraintLayout_Layout_layout_optimizationLevel = 59;
        public static final int[] ConstraintLayout_placeholder;
        public static final int ConstraintLayout_placeholder_content = 0;
        public static final int ConstraintLayout_placeholder_emptyVisibility = 1;
        public static final int[] ConstraintSet;
        public static final int ConstraintSet_android_alpha = 13;
        public static final int ConstraintSet_android_elevation = 26;
        public static final int ConstraintSet_android_id = 1;
        public static final int ConstraintSet_android_layout_height = 4;
        public static final int ConstraintSet_android_layout_marginBottom = 8;
        public static final int ConstraintSet_android_layout_marginEnd = 24;
        public static final int ConstraintSet_android_layout_marginLeft = 5;
        public static final int ConstraintSet_android_layout_marginRight = 7;
        public static final int ConstraintSet_android_layout_marginStart = 23;
        public static final int ConstraintSet_android_layout_marginTop = 6;
        public static final int ConstraintSet_android_layout_width = 3;
        public static final int ConstraintSet_android_maxHeight = 10;
        public static final int ConstraintSet_android_maxWidth = 9;
        public static final int ConstraintSet_android_minHeight = 12;
        public static final int ConstraintSet_android_minWidth = 11;
        public static final int ConstraintSet_android_orientation = 0;
        public static final int ConstraintSet_android_rotation = 20;
        public static final int ConstraintSet_android_rotationX = 21;
        public static final int ConstraintSet_android_rotationY = 22;
        public static final int ConstraintSet_android_scaleX = 18;
        public static final int ConstraintSet_android_scaleY = 19;
        public static final int ConstraintSet_android_transformPivotX = 14;
        public static final int ConstraintSet_android_transformPivotY = 15;
        public static final int ConstraintSet_android_translationX = 16;
        public static final int ConstraintSet_android_translationY = 17;
        public static final int ConstraintSet_android_translationZ = 25;
        public static final int ConstraintSet_android_visibility = 2;
        public static final int ConstraintSet_barrierAllowsGoneWidgets = 27;
        public static final int ConstraintSet_barrierDirection = 28;
        public static final int ConstraintSet_chainUseRtl = 29;
        public static final int ConstraintSet_constraint_referenced_ids = 30;
        public static final int ConstraintSet_layout_constrainedHeight = 31;
        public static final int ConstraintSet_layout_constrainedWidth = 32;
        public static final int ConstraintSet_layout_constraintBaseline_creator = 33;
        public static final int ConstraintSet_layout_constraintBaseline_toBaselineOf = 34;
        public static final int ConstraintSet_layout_constraintBottom_creator = 35;
        public static final int ConstraintSet_layout_constraintBottom_toBottomOf = 36;
        public static final int ConstraintSet_layout_constraintBottom_toTopOf = 37;
        public static final int ConstraintSet_layout_constraintCircle = 38;
        public static final int ConstraintSet_layout_constraintCircleAngle = 39;
        public static final int ConstraintSet_layout_constraintCircleRadius = 40;
        public static final int ConstraintSet_layout_constraintDimensionRatio = 41;
        public static final int ConstraintSet_layout_constraintEnd_toEndOf = 42;
        public static final int ConstraintSet_layout_constraintEnd_toStartOf = 43;
        public static final int ConstraintSet_layout_constraintGuide_begin = 44;
        public static final int ConstraintSet_layout_constraintGuide_end = 45;
        public static final int ConstraintSet_layout_constraintGuide_percent = 46;
        public static final int ConstraintSet_layout_constraintHeight_default = 47;
        public static final int ConstraintSet_layout_constraintHeight_max = 48;
        public static final int ConstraintSet_layout_constraintHeight_min = 49;
        public static final int ConstraintSet_layout_constraintHeight_percent = 50;
        public static final int ConstraintSet_layout_constraintHorizontal_bias = 51;
        public static final int ConstraintSet_layout_constraintHorizontal_chainStyle = 52;
        public static final int ConstraintSet_layout_constraintHorizontal_weight = 53;
        public static final int ConstraintSet_layout_constraintLeft_creator = 54;
        public static final int ConstraintSet_layout_constraintLeft_toLeftOf = 55;
        public static final int ConstraintSet_layout_constraintLeft_toRightOf = 56;
        public static final int ConstraintSet_layout_constraintRight_creator = 57;
        public static final int ConstraintSet_layout_constraintRight_toLeftOf = 58;
        public static final int ConstraintSet_layout_constraintRight_toRightOf = 59;
        public static final int ConstraintSet_layout_constraintStart_toEndOf = 60;
        public static final int ConstraintSet_layout_constraintStart_toStartOf = 61;
        public static final int ConstraintSet_layout_constraintTop_creator = 62;
        public static final int ConstraintSet_layout_constraintTop_toBottomOf = 63;
        public static final int ConstraintSet_layout_constraintTop_toTopOf = 64;
        public static final int ConstraintSet_layout_constraintVertical_bias = 65;
        public static final int ConstraintSet_layout_constraintVertical_chainStyle = 66;
        public static final int ConstraintSet_layout_constraintVertical_weight = 67;
        public static final int ConstraintSet_layout_constraintWidth_default = 68;
        public static final int ConstraintSet_layout_constraintWidth_max = 69;
        public static final int ConstraintSet_layout_constraintWidth_min = 70;
        public static final int ConstraintSet_layout_constraintWidth_percent = 71;
        public static final int ConstraintSet_layout_editor_absoluteX = 72;
        public static final int ConstraintSet_layout_editor_absoluteY = 73;
        public static final int ConstraintSet_layout_goneMarginBottom = 74;
        public static final int ConstraintSet_layout_goneMarginEnd = 75;
        public static final int ConstraintSet_layout_goneMarginLeft = 76;
        public static final int ConstraintSet_layout_goneMarginRight = 77;
        public static final int ConstraintSet_layout_goneMarginStart = 78;
        public static final int ConstraintSet_layout_goneMarginTop = 79;
        public static final int[] LinearConstraintLayout;
        public static final int LinearConstraintLayout_android_orientation = 0;
        
        static {
            ConstraintLayout_Layout = new int[] { 16842948, 16843039, 16843040, 16843071, 16843072, 2130903095, 2130903096, 2130903137, 2130903184, 2130903185, 2130903315, 2130903316, 2130903317, 2130903318, 2130903319, 2130903320, 2130903321, 2130903322, 2130903323, 2130903324, 2130903325, 2130903326, 2130903327, 2130903328, 2130903329, 2130903330, 2130903331, 2130903332, 2130903333, 2130903334, 2130903335, 2130903336, 2130903337, 2130903338, 2130903339, 2130903340, 2130903341, 2130903342, 2130903343, 2130903344, 2130903345, 2130903346, 2130903347, 2130903348, 2130903349, 2130903350, 2130903351, 2130903352, 2130903353, 2130903354, 2130903355, 2130903357, 2130903358, 2130903362, 2130903363, 2130903364, 2130903365, 2130903366, 2130903367, 2130903374 };
            ConstraintLayout_placeholder = new int[] { 2130903186, 2130903225 };
            ConstraintSet = new int[] { 16842948, 16842960, 16842972, 16842996, 16842997, 16842999, 16843000, 16843001, 16843002, 16843039, 16843040, 16843071, 16843072, 16843551, 16843552, 16843553, 16843554, 16843555, 16843556, 16843557, 16843558, 16843559, 16843560, 16843701, 16843702, 16843770, 16843840, 2130903095, 2130903096, 2130903137, 2130903185, 2130903315, 2130903316, 2130903317, 2130903318, 2130903319, 2130903320, 2130903321, 2130903322, 2130903323, 2130903324, 2130903325, 2130903326, 2130903327, 2130903328, 2130903329, 2130903330, 2130903331, 2130903332, 2130903333, 2130903334, 2130903335, 2130903336, 2130903337, 2130903338, 2130903339, 2130903340, 2130903341, 2130903342, 2130903343, 2130903344, 2130903345, 2130903346, 2130903347, 2130903348, 2130903349, 2130903350, 2130903351, 2130903352, 2130903353, 2130903354, 2130903355, 2130903357, 2130903358, 2130903362, 2130903363, 2130903364, 2130903365, 2130903366, 2130903367 };
            LinearConstraintLayout = new int[] { 16842948 };
        }
        
        private styleable() {
        }
    }
}
