// 
// Decompiled by Procyon v0.5.36
// 

package androidx.constraintlayout.widget;

import android.content.res.TypedArray;
import android.util.SparseIntArray;
import android.view.ViewGroup$MarginLayoutParams;
import androidx.constraintlayout.solver.widgets.Analyzer;
import android.graphics.Paint;
import android.graphics.Canvas;
import android.os.Build$VERSION;
import android.view.ViewGroup$LayoutParams;
import android.view.View$MeasureSpec;
import androidx.constraintlayout.solver.widgets.ResolutionAnchor;
import androidx.constraintlayout.solver.widgets.ConstraintAnchor;
import android.content.res.Resources$NotFoundException;
import android.util.AttributeSet;
import android.content.Context;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import androidx.constraintlayout.solver.Metrics;
import androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer;
import java.util.HashMap;
import java.util.ArrayList;
import android.view.View;
import android.util.SparseArray;
import android.view.ViewGroup;

public class ConstraintLayout extends ViewGroup
{
    static final boolean ALLOWS_EMBEDDED = false;
    private static final boolean CACHE_MEASURED_DIMENSION = false;
    private static final boolean DEBUG = false;
    public static final int DESIGN_INFO_ID = 0;
    private static final String TAG = "ConstraintLayout";
    private static final boolean USE_CONSTRAINTS_HELPER = true;
    public static final String VERSION = "ConstraintLayout-1.1.3";
    SparseArray<View> mChildrenByIds;
    private ArrayList<ConstraintHelper> mConstraintHelpers;
    private ConstraintSet mConstraintSet;
    private int mConstraintSetId;
    private HashMap<String, Integer> mDesignIds;
    private boolean mDirtyHierarchy;
    private int mLastMeasureHeight;
    int mLastMeasureHeightMode;
    int mLastMeasureHeightSize;
    private int mLastMeasureWidth;
    int mLastMeasureWidthMode;
    int mLastMeasureWidthSize;
    ConstraintWidgetContainer mLayoutWidget;
    private int mMaxHeight;
    private int mMaxWidth;
    private Metrics mMetrics;
    private int mMinHeight;
    private int mMinWidth;
    private int mOptimizationLevel;
    private final ArrayList<ConstraintWidget> mVariableDimensionsWidgets;
    
    public ConstraintLayout(final Context context) {
        super(context);
        this.mChildrenByIds = (SparseArray<View>)new SparseArray();
        this.mConstraintHelpers = new ArrayList<ConstraintHelper>(4);
        this.mVariableDimensionsWidgets = new ArrayList<ConstraintWidget>(100);
        this.mLayoutWidget = new ConstraintWidgetContainer();
        this.mMinWidth = 0;
        this.mMinHeight = 0;
        this.mMaxWidth = Integer.MAX_VALUE;
        this.mMaxHeight = Integer.MAX_VALUE;
        this.mDirtyHierarchy = true;
        this.mOptimizationLevel = 7;
        this.mConstraintSet = null;
        this.mConstraintSetId = -1;
        this.mDesignIds = new HashMap<String, Integer>();
        this.mLastMeasureWidth = -1;
        this.mLastMeasureHeight = -1;
        this.mLastMeasureWidthSize = -1;
        this.mLastMeasureHeightSize = -1;
        this.mLastMeasureWidthMode = 0;
        this.mLastMeasureHeightMode = 0;
        this.init(null);
    }
    
    public ConstraintLayout(final Context context, final AttributeSet set) {
        super(context, set);
        this.mChildrenByIds = (SparseArray<View>)new SparseArray();
        this.mConstraintHelpers = new ArrayList<ConstraintHelper>(4);
        this.mVariableDimensionsWidgets = new ArrayList<ConstraintWidget>(100);
        this.mLayoutWidget = new ConstraintWidgetContainer();
        this.mMinWidth = 0;
        this.mMinHeight = 0;
        this.mMaxWidth = Integer.MAX_VALUE;
        this.mMaxHeight = Integer.MAX_VALUE;
        this.mDirtyHierarchy = true;
        this.mOptimizationLevel = 7;
        this.mConstraintSet = null;
        this.mConstraintSetId = -1;
        this.mDesignIds = new HashMap<String, Integer>();
        this.mLastMeasureWidth = -1;
        this.mLastMeasureHeight = -1;
        this.mLastMeasureWidthSize = -1;
        this.mLastMeasureHeightSize = -1;
        this.mLastMeasureWidthMode = 0;
        this.mLastMeasureHeightMode = 0;
        this.init(set);
    }
    
    public ConstraintLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mChildrenByIds = (SparseArray<View>)new SparseArray();
        this.mConstraintHelpers = new ArrayList<ConstraintHelper>(4);
        this.mVariableDimensionsWidgets = new ArrayList<ConstraintWidget>(100);
        this.mLayoutWidget = new ConstraintWidgetContainer();
        this.mMinWidth = 0;
        this.mMinHeight = 0;
        this.mMaxWidth = Integer.MAX_VALUE;
        this.mMaxHeight = Integer.MAX_VALUE;
        this.mDirtyHierarchy = true;
        this.mOptimizationLevel = 7;
        this.mConstraintSet = null;
        this.mConstraintSetId = -1;
        this.mDesignIds = new HashMap<String, Integer>();
        this.mLastMeasureWidth = -1;
        this.mLastMeasureHeight = -1;
        this.mLastMeasureWidthSize = -1;
        this.mLastMeasureHeightSize = -1;
        this.mLastMeasureWidthMode = 0;
        this.mLastMeasureHeightMode = 0;
        this.init(set);
    }
    
    private final ConstraintWidget getTargetWidget(final int n) {
        if (n == 0) {
            return this.mLayoutWidget;
        }
        View view;
        if ((view = (View)this.mChildrenByIds.get(n)) == null) {
            final View viewById = this.findViewById(n);
            if ((view = viewById) != null && (view = viewById) != this) {
                view = viewById;
                if (viewById.getParent() == this) {
                    this.onViewAdded(viewById);
                    view = viewById;
                }
            }
        }
        if (view == this) {
            return this.mLayoutWidget;
        }
        if (view == null) {
            return null;
        }
        return ((LayoutParams)view.getLayoutParams()).widget;
    }
    
    private void init(AttributeSet obtainStyledAttributes) {
        this.mLayoutWidget.setCompanionWidget(this);
        this.mChildrenByIds.put(this.getId(), (Object)this);
        this.mConstraintSet = null;
        while (true) {
            Label_0251: {
                if (obtainStyledAttributes == null) {
                    break Label_0251;
                }
                obtainStyledAttributes = (AttributeSet)this.getContext().obtainStyledAttributes(obtainStyledAttributes, R.styleable.ConstraintLayout_Layout);
                final int indexCount = ((TypedArray)obtainStyledAttributes).getIndexCount();
                int n = 0;
            Label_0234_Outer:
                while (true) {
                    Label_0247: {
                        if (n >= indexCount) {
                            break Label_0247;
                        }
                        int mConstraintSetId = ((TypedArray)obtainStyledAttributes).getIndex(n);
                        Label_0240: {
                            if (mConstraintSetId == R.styleable.ConstraintLayout_Layout_android_minWidth) {
                                this.mMinWidth = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(mConstraintSetId, this.mMinWidth);
                                break Label_0240;
                            }
                            if (mConstraintSetId == R.styleable.ConstraintLayout_Layout_android_minHeight) {
                                this.mMinHeight = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(mConstraintSetId, this.mMinHeight);
                                break Label_0240;
                            }
                            if (mConstraintSetId == R.styleable.ConstraintLayout_Layout_android_maxWidth) {
                                this.mMaxWidth = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(mConstraintSetId, this.mMaxWidth);
                                break Label_0240;
                            }
                            if (mConstraintSetId == R.styleable.ConstraintLayout_Layout_android_maxHeight) {
                                this.mMaxHeight = ((TypedArray)obtainStyledAttributes).getDimensionPixelOffset(mConstraintSetId, this.mMaxHeight);
                                break Label_0240;
                            }
                            if (mConstraintSetId == R.styleable.ConstraintLayout_Layout_layout_optimizationLevel) {
                                this.mOptimizationLevel = ((TypedArray)obtainStyledAttributes).getInt(mConstraintSetId, this.mOptimizationLevel);
                                break Label_0240;
                            }
                            if (mConstraintSetId != R.styleable.ConstraintLayout_Layout_constraintSet) {
                                break Label_0240;
                            }
                            mConstraintSetId = ((TypedArray)obtainStyledAttributes).getResourceId(mConstraintSetId, 0);
                            try {
                                (this.mConstraintSet = new ConstraintSet()).load(this.getContext(), mConstraintSetId);
                                while (true) {
                                    this.mConstraintSetId = mConstraintSetId;
                                    ++n;
                                    continue Label_0234_Outer;
                                    ((TypedArray)obtainStyledAttributes).recycle();
                                    this.mLayoutWidget.setOptimizationLevel(this.mOptimizationLevel);
                                    return;
                                    this.mConstraintSet = null;
                                    continue;
                                }
                            }
                            catch (Resources$NotFoundException ex) {}
                        }
                    }
                    break;
                }
            }
            continue;
        }
    }
    
    private void internalMeasureChildren(final int n, final int n2) {
        final int n3 = this.getPaddingTop() + this.getPaddingBottom();
        final int n4 = this.getPaddingLeft() + this.getPaddingRight();
        for (int childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            if (child.getVisibility() != 8) {
                final LayoutParams layoutParams = (LayoutParams)child.getLayoutParams();
                final ConstraintWidget widget = layoutParams.widget;
                if (!layoutParams.isGuideline) {
                    if (!layoutParams.isHelper) {
                        widget.setVisibility(child.getVisibility());
                        final int width = layoutParams.width;
                        final int height = layoutParams.height;
                        boolean b = false;
                        Label_0197: {
                            Label_0195: {
                                if (!layoutParams.horizontalDimensionFixed && !layoutParams.verticalDimensionFixed && (layoutParams.horizontalDimensionFixed || layoutParams.matchConstraintDefaultWidth != 1) && layoutParams.width != -1) {
                                    if (!layoutParams.verticalDimensionFixed) {
                                        if (layoutParams.matchConstraintDefaultHeight == 1) {
                                            break Label_0195;
                                        }
                                        if (layoutParams.height == -1) {
                                            break Label_0195;
                                        }
                                    }
                                    b = false;
                                    break Label_0197;
                                }
                            }
                            b = true;
                        }
                        boolean b2;
                        boolean b3;
                        int measuredWidth;
                        int measuredHeight;
                        if (b) {
                            int n5;
                            if (width == 0) {
                                n5 = getChildMeasureSpec(n, n4, -2);
                                b2 = true;
                            }
                            else if (width == -1) {
                                n5 = getChildMeasureSpec(n, n4, -1);
                                b2 = false;
                            }
                            else {
                                b2 = (width == -2);
                                n5 = getChildMeasureSpec(n, n4, width);
                            }
                            int n6;
                            if (height == 0) {
                                n6 = getChildMeasureSpec(n2, n3, -2);
                                b3 = true;
                            }
                            else if (height == -1) {
                                n6 = getChildMeasureSpec(n2, n3, -1);
                                b3 = false;
                            }
                            else {
                                b3 = (height == -2);
                                n6 = getChildMeasureSpec(n2, n3, height);
                            }
                            child.measure(n5, n6);
                            final Metrics mMetrics = this.mMetrics;
                            if (mMetrics != null) {
                                ++mMetrics.measures;
                            }
                            widget.setWidthWrapContent(width == -2);
                            widget.setHeightWrapContent(height == -2);
                            measuredWidth = child.getMeasuredWidth();
                            measuredHeight = child.getMeasuredHeight();
                        }
                        else {
                            b2 = false;
                            b3 = false;
                            measuredHeight = height;
                            measuredWidth = width;
                        }
                        widget.setWidth(measuredWidth);
                        widget.setHeight(measuredHeight);
                        if (b2) {
                            widget.setWrapWidth(measuredWidth);
                        }
                        if (b3) {
                            widget.setWrapHeight(measuredHeight);
                        }
                        if (layoutParams.needsBaseline) {
                            final int baseline = child.getBaseline();
                            if (baseline != -1) {
                                widget.setBaselineDistance(baseline);
                            }
                        }
                    }
                }
            }
        }
    }
    
    private void internalMeasureDimensions(final int n, final int n2) {
        ConstraintLayout constraintLayout = this;
        final int n3 = this.getPaddingTop() + this.getPaddingBottom();
        final int n4 = this.getPaddingLeft() + this.getPaddingRight();
        final int childCount = this.getChildCount();
        for (int i = 0; i < childCount; ++i) {
            final View child = constraintLayout.getChildAt(i);
            if (child.getVisibility() != 8) {
                final LayoutParams layoutParams = (LayoutParams)child.getLayoutParams();
                final ConstraintWidget widget = layoutParams.widget;
                if (!layoutParams.isGuideline) {
                    if (!layoutParams.isHelper) {
                        widget.setVisibility(child.getVisibility());
                        final int width = layoutParams.width;
                        final int height = layoutParams.height;
                        if (width != 0 && height != 0) {
                            final boolean b = width == -2;
                            final int childMeasureSpec = getChildMeasureSpec(n, n4, width);
                            final boolean b2 = height == -2;
                            child.measure(childMeasureSpec, getChildMeasureSpec(n2, n3, height));
                            final Metrics mMetrics = constraintLayout.mMetrics;
                            if (mMetrics != null) {
                                ++mMetrics.measures;
                            }
                            widget.setWidthWrapContent(width == -2);
                            widget.setHeightWrapContent(height == -2);
                            final int measuredWidth = child.getMeasuredWidth();
                            final int measuredHeight = child.getMeasuredHeight();
                            widget.setWidth(measuredWidth);
                            widget.setHeight(measuredHeight);
                            if (b) {
                                widget.setWrapWidth(measuredWidth);
                            }
                            if (b2) {
                                widget.setWrapHeight(measuredHeight);
                            }
                            if (layoutParams.needsBaseline) {
                                final int baseline = child.getBaseline();
                                if (baseline != -1) {
                                    widget.setBaselineDistance(baseline);
                                }
                            }
                            if (layoutParams.horizontalDimensionFixed && layoutParams.verticalDimensionFixed) {
                                widget.getResolutionWidth().resolve(measuredWidth);
                                widget.getResolutionHeight().resolve(measuredHeight);
                            }
                        }
                        else {
                            widget.getResolutionWidth().invalidate();
                            widget.getResolutionHeight().invalidate();
                        }
                    }
                }
            }
        }
        constraintLayout.mLayoutWidget.solveGraph();
        ConstraintLayout constraintLayout2;
        for (int j = 0; j < childCount; ++j, constraintLayout = constraintLayout2) {
            final View child2 = constraintLayout.getChildAt(j);
            if (child2.getVisibility() != 8) {
                final LayoutParams layoutParams2 = (LayoutParams)child2.getLayoutParams();
                final ConstraintWidget widget2 = layoutParams2.widget;
                if (!layoutParams2.isGuideline) {
                    if (!layoutParams2.isHelper) {
                        widget2.setVisibility(child2.getVisibility());
                        int width2 = layoutParams2.width;
                        int height2 = layoutParams2.height;
                        if (width2 == 0 || height2 == 0) {
                            final ResolutionAnchor resolutionNode = widget2.getAnchor(ConstraintAnchor.Type.LEFT).getResolutionNode();
                            final ResolutionAnchor resolutionNode2 = widget2.getAnchor(ConstraintAnchor.Type.RIGHT).getResolutionNode();
                            final boolean b3 = widget2.getAnchor(ConstraintAnchor.Type.LEFT).getTarget() != null && widget2.getAnchor(ConstraintAnchor.Type.RIGHT).getTarget() != null;
                            final ResolutionAnchor resolutionNode3 = widget2.getAnchor(ConstraintAnchor.Type.TOP).getResolutionNode();
                            final ResolutionAnchor resolutionNode4 = widget2.getAnchor(ConstraintAnchor.Type.BOTTOM).getResolutionNode();
                            final boolean b4 = widget2.getAnchor(ConstraintAnchor.Type.TOP).getTarget() != null && widget2.getAnchor(ConstraintAnchor.Type.BOTTOM).getTarget() != null;
                            if (width2 == 0 && height2 == 0 && b3 && b4) {
                                constraintLayout2 = constraintLayout;
                                continue;
                            }
                            final boolean b5 = constraintLayout.mLayoutWidget.getHorizontalDimensionBehaviour() != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                            final boolean b6 = constraintLayout.mLayoutWidget.getVerticalDimensionBehaviour() != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                            if (!b5) {
                                widget2.getResolutionWidth().invalidate();
                            }
                            if (!b6) {
                                widget2.getResolutionHeight().invalidate();
                            }
                            int n5 = 0;
                            boolean b7 = false;
                            boolean b8 = false;
                            int n6 = 0;
                            Label_0908: {
                                if (width2 == 0) {
                                    if (!b5 || !widget2.isSpreadWidth() || !b3 || !resolutionNode.isResolved() || !resolutionNode2.isResolved()) {
                                        n5 = getChildMeasureSpec(n, n4, -2);
                                        b7 = true;
                                        b8 = false;
                                        n6 = width2;
                                        break Label_0908;
                                    }
                                    width2 = (int)(resolutionNode2.getResolvedValue() - resolutionNode.getResolvedValue());
                                    widget2.getResolutionWidth().resolve(width2);
                                    n5 = getChildMeasureSpec(n, n4, width2);
                                }
                                else {
                                    if (width2 != -1) {
                                        b7 = (width2 == -2);
                                        n5 = getChildMeasureSpec(n, n4, width2);
                                        n6 = width2;
                                        b8 = b5;
                                        break Label_0908;
                                    }
                                    n5 = getChildMeasureSpec(n, n4, -1);
                                }
                                b7 = false;
                                b8 = b5;
                                n6 = width2;
                            }
                            int n7 = 0;
                            int n8 = 0;
                            boolean b9 = false;
                            Label_1060: {
                                if (height2 == 0) {
                                    if (!b6 || !widget2.isSpreadHeight() || !b4 || !resolutionNode3.isResolved() || !resolutionNode4.isResolved()) {
                                        n7 = getChildMeasureSpec(n2, n3, -2);
                                        n8 = 1;
                                        b9 = false;
                                        break Label_1060;
                                    }
                                    height2 = (int)(resolutionNode4.getResolvedValue() - resolutionNode3.getResolvedValue());
                                    widget2.getResolutionHeight().resolve(height2);
                                    n7 = getChildMeasureSpec(n2, n3, height2);
                                }
                                else {
                                    if (height2 != -1) {
                                        final boolean b10 = height2 == -2;
                                        final int childMeasureSpec2 = getChildMeasureSpec(n2, n3, height2);
                                        b9 = b6;
                                        n8 = (b10 ? 1 : 0);
                                        n7 = childMeasureSpec2;
                                        break Label_1060;
                                    }
                                    n7 = getChildMeasureSpec(n2, n3, -1);
                                }
                                b9 = b6;
                                n8 = 0;
                            }
                            child2.measure(n5, n7);
                            final Metrics mMetrics2 = this.mMetrics;
                            if (mMetrics2 != null) {
                                ++mMetrics2.measures;
                            }
                            widget2.setWidthWrapContent(n6 == -2);
                            widget2.setHeightWrapContent(height2 == -2);
                            final int measuredWidth2 = child2.getMeasuredWidth();
                            final int measuredHeight2 = child2.getMeasuredHeight();
                            widget2.setWidth(measuredWidth2);
                            widget2.setHeight(measuredHeight2);
                            if (b7) {
                                widget2.setWrapWidth(measuredWidth2);
                            }
                            if (n8 != 0) {
                                widget2.setWrapHeight(measuredHeight2);
                            }
                            if (b8) {
                                widget2.getResolutionWidth().resolve(measuredWidth2);
                            }
                            else {
                                widget2.getResolutionWidth().remove();
                            }
                            if (b9) {
                                widget2.getResolutionHeight().resolve(measuredHeight2);
                            }
                            else {
                                widget2.getResolutionHeight().remove();
                            }
                            if (!layoutParams2.needsBaseline) {
                                constraintLayout2 = this;
                                continue;
                            }
                            final int baseline2 = child2.getBaseline();
                            constraintLayout2 = this;
                            if (baseline2 != -1) {
                                widget2.setBaselineDistance(baseline2);
                                constraintLayout2 = this;
                            }
                            continue;
                        }
                    }
                }
            }
            constraintLayout2 = constraintLayout;
        }
    }
    
    private void setChildrenConstraints() {
        throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge Z and I\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
    }
    
    private void setSelfDimensionBehaviour(int size, int size2) {
        final int mode = View$MeasureSpec.getMode(size);
        size = View$MeasureSpec.getSize(size);
        final int mode2 = View$MeasureSpec.getMode(size2);
        size2 = View$MeasureSpec.getSize(size2);
        final int paddingTop = this.getPaddingTop();
        final int paddingBottom = this.getPaddingBottom();
        final int paddingLeft = this.getPaddingLeft();
        final int paddingRight = this.getPaddingRight();
        ConstraintWidget.DimensionBehaviour horizontalDimensionBehaviour = ConstraintWidget.DimensionBehaviour.FIXED;
        ConstraintWidget.DimensionBehaviour verticalDimensionBehaviour = ConstraintWidget.DimensionBehaviour.FIXED;
        this.getLayoutParams();
        Label_0117: {
            if (mode != Integer.MIN_VALUE) {
                if (mode != 0) {
                    if (mode == 1073741824) {
                        size = Math.min(this.mMaxWidth, size) - (paddingLeft + paddingRight);
                        break Label_0117;
                    }
                }
                else {
                    horizontalDimensionBehaviour = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                }
                size = 0;
            }
            else {
                horizontalDimensionBehaviour = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            }
        }
        Label_0171: {
            if (mode2 != Integer.MIN_VALUE) {
                if (mode2 != 0) {
                    if (mode2 == 1073741824) {
                        size2 = Math.min(this.mMaxHeight, size2) - (paddingTop + paddingBottom);
                        break Label_0171;
                    }
                }
                else {
                    verticalDimensionBehaviour = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                }
                size2 = 0;
            }
            else {
                verticalDimensionBehaviour = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            }
        }
        this.mLayoutWidget.setMinWidth(0);
        this.mLayoutWidget.setMinHeight(0);
        this.mLayoutWidget.setHorizontalDimensionBehaviour(horizontalDimensionBehaviour);
        this.mLayoutWidget.setWidth(size);
        this.mLayoutWidget.setVerticalDimensionBehaviour(verticalDimensionBehaviour);
        this.mLayoutWidget.setHeight(size2);
        this.mLayoutWidget.setMinWidth(this.mMinWidth - this.getPaddingLeft() - this.getPaddingRight());
        this.mLayoutWidget.setMinHeight(this.mMinHeight - this.getPaddingTop() - this.getPaddingBottom());
    }
    
    private void updateHierarchy() {
        final int childCount = this.getChildCount();
        final int n = 0;
        int n2 = 0;
        int n3;
        while (true) {
            n3 = n;
            if (n2 >= childCount) {
                break;
            }
            if (this.getChildAt(n2).isLayoutRequested()) {
                n3 = 1;
                break;
            }
            ++n2;
        }
        if (n3 != 0) {
            this.mVariableDimensionsWidgets.clear();
            this.setChildrenConstraints();
        }
    }
    
    private void updatePostMeasures() {
        final int childCount = this.getChildCount();
        final int n = 0;
        for (int i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            if (child instanceof Placeholder) {
                ((Placeholder)child).updatePostMeasure(this);
            }
        }
        final int size = this.mConstraintHelpers.size();
        if (size > 0) {
            for (int j = n; j < size; ++j) {
                this.mConstraintHelpers.get(j).updatePostMeasure(this);
            }
        }
    }
    
    public void addView(final View view, final int n, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        super.addView(view, n, viewGroup$LayoutParams);
        if (Build$VERSION.SDK_INT < 14) {
            this.onViewAdded(view);
        }
    }
    
    protected boolean checkLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return viewGroup$LayoutParams instanceof LayoutParams;
    }
    
    public void dispatchDraw(final Canvas canvas) {
        super.dispatchDraw(canvas);
        if (this.isInEditMode()) {
            final int childCount = this.getChildCount();
            final float n = (float)this.getWidth();
            final float n2 = (float)this.getHeight();
            for (int i = 0; i < childCount; ++i) {
                final View child = this.getChildAt(i);
                if (child.getVisibility() != 8) {
                    final Object tag = child.getTag();
                    if (tag != null && tag instanceof String) {
                        final String[] split = ((String)tag).split(",");
                        if (split.length == 4) {
                            final int int1 = Integer.parseInt(split[0]);
                            final int int2 = Integer.parseInt(split[1]);
                            final int int3 = Integer.parseInt(split[2]);
                            final int int4 = Integer.parseInt(split[3]);
                            final int n3 = (int)(int1 / 1080.0f * n);
                            final int n4 = (int)(int2 / 1920.0f * n2);
                            final int n5 = (int)(int3 / 1080.0f * n);
                            final int n6 = (int)(int4 / 1920.0f * n2);
                            final Paint paint = new Paint();
                            paint.setColor(-65536);
                            final float n7 = (float)n3;
                            final float n8 = (float)n4;
                            final float n9 = (float)(n3 + n5);
                            canvas.drawLine(n7, n8, n9, n8, paint);
                            final float n10 = (float)(n4 + n6);
                            canvas.drawLine(n9, n8, n9, n10, paint);
                            canvas.drawLine(n9, n10, n7, n10, paint);
                            canvas.drawLine(n7, n10, n7, n8, paint);
                            paint.setColor(-16711936);
                            canvas.drawLine(n7, n8, n9, n10, paint);
                            canvas.drawLine(n7, n10, n9, n8, paint);
                        }
                    }
                }
            }
        }
    }
    
    public void fillMetrics(final Metrics mMetrics) {
        this.mMetrics = mMetrics;
        this.mLayoutWidget.fillMetrics(mMetrics);
    }
    
    protected LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-2, -2);
    }
    
    protected ViewGroup$LayoutParams generateLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return (ViewGroup$LayoutParams)new LayoutParams(viewGroup$LayoutParams);
    }
    
    public LayoutParams generateLayoutParams(final AttributeSet set) {
        return new LayoutParams(this.getContext(), set);
    }
    
    public Object getDesignInformation(final int n, final Object o) {
        if (n == 0 && o instanceof String) {
            final String s = (String)o;
            final HashMap<String, Integer> mDesignIds = this.mDesignIds;
            if (mDesignIds != null && mDesignIds.containsKey(s)) {
                return this.mDesignIds.get(s);
            }
        }
        return null;
    }
    
    public int getMaxHeight() {
        return this.mMaxHeight;
    }
    
    public int getMaxWidth() {
        return this.mMaxWidth;
    }
    
    public int getMinHeight() {
        return this.mMinHeight;
    }
    
    public int getMinWidth() {
        return this.mMinWidth;
    }
    
    public int getOptimizationLevel() {
        return this.mLayoutWidget.getOptimizationLevel();
    }
    
    public View getViewById(final int n) {
        return (View)this.mChildrenByIds.get(n);
    }
    
    public final ConstraintWidget getViewWidget(final View view) {
        if (view == this) {
            return this.mLayoutWidget;
        }
        if (view == null) {
            return null;
        }
        return ((LayoutParams)view.getLayoutParams()).widget;
    }
    
    protected void onLayout(final boolean b, int i, int n, int n2, int drawX) {
        n2 = this.getChildCount();
        final boolean inEditMode = this.isInEditMode();
        n = 0;
        View child;
        LayoutParams layoutParams;
        ConstraintWidget widget;
        int drawY;
        int n3;
        int n4;
        View content;
        for (i = 0; i < n2; ++i) {
            child = this.getChildAt(i);
            layoutParams = (LayoutParams)child.getLayoutParams();
            widget = layoutParams.widget;
            if (child.getVisibility() != 8 || layoutParams.isGuideline || layoutParams.isHelper || inEditMode) {
                if (!layoutParams.isInPlaceholder) {
                    drawX = widget.getDrawX();
                    drawY = widget.getDrawY();
                    n3 = widget.getWidth() + drawX;
                    n4 = widget.getHeight() + drawY;
                    child.layout(drawX, drawY, n3, n4);
                    if (child instanceof Placeholder) {
                        content = ((Placeholder)child).getContent();
                        if (content != null) {
                            content.setVisibility(0);
                            content.layout(drawX, drawY, n3, n4);
                        }
                    }
                }
            }
        }
        n2 = this.mConstraintHelpers.size();
        if (n2 > 0) {
            for (i = n; i < n2; ++i) {
                this.mConstraintHelpers.get(i).updatePostLayout(this);
            }
        }
    }
    
    protected void onMeasure(int resolveSizeAndState, int min) {
        System.currentTimeMillis();
        final int mode = View$MeasureSpec.getMode(resolveSizeAndState);
        final int size = View$MeasureSpec.getSize(resolveSizeAndState);
        final int mode2 = View$MeasureSpec.getMode(min);
        final int size2 = View$MeasureSpec.getSize(min);
        final int paddingLeft = this.getPaddingLeft();
        final int paddingTop = this.getPaddingTop();
        this.mLayoutWidget.setX(paddingLeft);
        this.mLayoutWidget.setY(paddingTop);
        this.mLayoutWidget.setMaxWidth(this.mMaxWidth);
        this.mLayoutWidget.setMaxHeight(this.mMaxHeight);
        if (Build$VERSION.SDK_INT >= 17) {
            this.mLayoutWidget.setRtl(this.getLayoutDirection() == 1);
        }
        this.setSelfDimensionBehaviour(resolveSizeAndState, min);
        final int width = this.mLayoutWidget.getWidth();
        final int height = this.mLayoutWidget.getHeight();
        boolean b;
        if (this.mDirtyHierarchy) {
            this.mDirtyHierarchy = false;
            this.updateHierarchy();
            b = true;
        }
        else {
            b = false;
        }
        final boolean b2 = (this.mOptimizationLevel & 0x8) == 0x8;
        if (b2) {
            this.mLayoutWidget.preOptimize();
            this.mLayoutWidget.optimizeForDimensions(width, height);
            this.internalMeasureDimensions(resolveSizeAndState, min);
        }
        else {
            this.internalMeasureChildren(resolveSizeAndState, min);
        }
        this.updatePostMeasures();
        if (this.getChildCount() > 0 && b) {
            Analyzer.determineGroups(this.mLayoutWidget);
        }
        if (this.mLayoutWidget.mGroupsWrapOptimized) {
            if (this.mLayoutWidget.mHorizontalWrapOptimized && mode == Integer.MIN_VALUE) {
                if (this.mLayoutWidget.mWrapFixedWidth < size) {
                    final ConstraintWidgetContainer mLayoutWidget = this.mLayoutWidget;
                    mLayoutWidget.setWidth(mLayoutWidget.mWrapFixedWidth);
                }
                this.mLayoutWidget.setHorizontalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.FIXED);
            }
            if (this.mLayoutWidget.mVerticalWrapOptimized && mode2 == Integer.MIN_VALUE) {
                if (this.mLayoutWidget.mWrapFixedHeight < size2) {
                    final ConstraintWidgetContainer mLayoutWidget2 = this.mLayoutWidget;
                    mLayoutWidget2.setHeight(mLayoutWidget2.mWrapFixedHeight);
                }
                this.mLayoutWidget.setVerticalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.FIXED);
            }
        }
        if ((this.mOptimizationLevel & 0x20) == 0x20) {
            final int width2 = this.mLayoutWidget.getWidth();
            final int height2 = this.mLayoutWidget.getHeight();
            if (this.mLastMeasureWidth != width2 && mode == 1073741824) {
                Analyzer.setPosition(this.mLayoutWidget.mWidgetGroups, 0, width2);
            }
            if (this.mLastMeasureHeight != height2 && mode2 == 1073741824) {
                Analyzer.setPosition(this.mLayoutWidget.mWidgetGroups, 1, height2);
            }
            if (this.mLayoutWidget.mHorizontalWrapOptimized && this.mLayoutWidget.mWrapFixedWidth > size) {
                Analyzer.setPosition(this.mLayoutWidget.mWidgetGroups, 0, size);
            }
            if (this.mLayoutWidget.mVerticalWrapOptimized && this.mLayoutWidget.mWrapFixedHeight > size2) {
                Analyzer.setPosition(this.mLayoutWidget.mWidgetGroups, 1, size2);
            }
        }
        if (this.getChildCount() > 0) {
            this.solveLinearSystem("First pass");
        }
        final int size3 = this.mVariableDimensionsWidgets.size();
        final int n = paddingTop + this.getPaddingBottom();
        final int n2 = paddingLeft + this.getPaddingRight();
        int n10;
        if (size3 > 0) {
            final boolean b3 = this.mLayoutWidget.getHorizontalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            final boolean b4 = this.mLayoutWidget.getVerticalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            int max = Math.max(this.mLayoutWidget.getWidth(), this.mMinWidth);
            int max2 = Math.max(this.mLayoutWidget.getHeight(), this.mMinHeight);
            int i = 0;
            int n3 = 0;
            int combineMeasuredStates = 0;
            while (i < size3) {
                final ConstraintWidget constraintWidget = this.mVariableDimensionsWidgets.get(i);
                final View view = (View)constraintWidget.getCompanionWidget();
                int n8 = 0;
                Label_1197: {
                    if (view != null) {
                        final LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
                        if (!layoutParams.isHelper) {
                            if (!layoutParams.isGuideline) {
                                final int visibility = view.getVisibility();
                                int n4 = n3;
                                if (visibility != 8 && (!b2 || !constraintWidget.getResolutionWidth().isResolved() || !constraintWidget.getResolutionHeight().isResolved())) {
                                    int n5;
                                    if (layoutParams.width == -2 && layoutParams.horizontalDimensionFixed) {
                                        n5 = getChildMeasureSpec(resolveSizeAndState, n2, layoutParams.width);
                                    }
                                    else {
                                        n5 = View$MeasureSpec.makeMeasureSpec(constraintWidget.getWidth(), 1073741824);
                                    }
                                    int n6;
                                    if (layoutParams.height == -2 && layoutParams.verticalDimensionFixed) {
                                        n6 = getChildMeasureSpec(min, n, layoutParams.height);
                                    }
                                    else {
                                        n6 = View$MeasureSpec.makeMeasureSpec(constraintWidget.getHeight(), 1073741824);
                                    }
                                    view.measure(n5, n6);
                                    final Metrics mMetrics = this.mMetrics;
                                    if (mMetrics != null) {
                                        ++mMetrics.additionalMeasures;
                                    }
                                    final int measuredWidth = view.getMeasuredWidth();
                                    final int measuredHeight = view.getMeasuredHeight();
                                    int max3 = max;
                                    if (measuredWidth != constraintWidget.getWidth()) {
                                        constraintWidget.setWidth(measuredWidth);
                                        if (b2) {
                                            constraintWidget.getResolutionWidth().resolve(measuredWidth);
                                        }
                                        max3 = max;
                                        if (b3 && constraintWidget.getRight() > (max3 = max)) {
                                            max3 = Math.max(max, constraintWidget.getRight() + constraintWidget.getAnchor(ConstraintAnchor.Type.RIGHT).getMargin());
                                        }
                                        n4 = 1;
                                    }
                                    int max4 = max2;
                                    if (measuredHeight != constraintWidget.getHeight()) {
                                        constraintWidget.setHeight(measuredHeight);
                                        if (b2) {
                                            constraintWidget.getResolutionHeight().resolve(measuredHeight);
                                        }
                                        max4 = max2;
                                        if (b4 && constraintWidget.getBottom() > (max4 = max2)) {
                                            max4 = Math.max(max2, constraintWidget.getBottom() + constraintWidget.getAnchor(ConstraintAnchor.Type.BOTTOM).getMargin());
                                        }
                                        n4 = 1;
                                    }
                                    int n7 = n4;
                                    if (layoutParams.needsBaseline) {
                                        final int baseline = view.getBaseline();
                                        n7 = n4;
                                        if (baseline != -1) {
                                            n7 = n4;
                                            if (baseline != constraintWidget.getBaselineDistance()) {
                                                constraintWidget.setBaselineDistance(baseline);
                                                n7 = 1;
                                            }
                                        }
                                    }
                                    if (Build$VERSION.SDK_INT >= 11) {
                                        combineMeasuredStates = combineMeasuredStates(combineMeasuredStates, view.getMeasuredState());
                                        n8 = max4;
                                        max = max3;
                                        n3 = n7;
                                        break Label_1197;
                                    }
                                    n8 = max4;
                                    max = max3;
                                    n3 = n7;
                                    break Label_1197;
                                }
                            }
                        }
                    }
                    n8 = max2;
                }
                ++i;
                max2 = n8;
            }
            final int n9 = combineMeasuredStates;
            if (n3 != 0) {
                this.mLayoutWidget.setWidth(width);
                this.mLayoutWidget.setHeight(height);
                if (b2) {
                    this.mLayoutWidget.solveGraph();
                }
                this.solveLinearSystem("2nd pass");
                boolean b5;
                if (this.mLayoutWidget.getWidth() < max) {
                    this.mLayoutWidget.setWidth(max);
                    b5 = true;
                }
                else {
                    b5 = false;
                }
                if (this.mLayoutWidget.getHeight() < max2) {
                    this.mLayoutWidget.setHeight(max2);
                    b5 = true;
                }
                if (b5) {
                    this.solveLinearSystem("3rd pass");
                }
            }
            int index = 0;
            while (true) {
                n10 = n9;
                if (index >= size3) {
                    break;
                }
                final ConstraintWidget constraintWidget2 = this.mVariableDimensionsWidgets.get(index);
                final View view2 = (View)constraintWidget2.getCompanionWidget();
                if (view2 != null && (view2.getMeasuredWidth() != constraintWidget2.getWidth() || view2.getMeasuredHeight() != constraintWidget2.getHeight()) && constraintWidget2.getVisibility() != 8) {
                    view2.measure(View$MeasureSpec.makeMeasureSpec(constraintWidget2.getWidth(), 1073741824), View$MeasureSpec.makeMeasureSpec(constraintWidget2.getHeight(), 1073741824));
                    final Metrics mMetrics2 = this.mMetrics;
                    if (mMetrics2 != null) {
                        ++mMetrics2.additionalMeasures;
                    }
                }
                ++index;
            }
        }
        else {
            n10 = 0;
        }
        final int mLastMeasureWidth = this.mLayoutWidget.getWidth() + n2;
        final int mLastMeasureHeight = this.mLayoutWidget.getHeight() + n;
        if (Build$VERSION.SDK_INT >= 11) {
            resolveSizeAndState = resolveSizeAndState(mLastMeasureWidth, resolveSizeAndState, n10);
            final int resolveSizeAndState2 = resolveSizeAndState(mLastMeasureHeight, min, n10 << 16);
            min = Math.min(this.mMaxWidth, resolveSizeAndState & 0xFFFFFF);
            final int min2 = Math.min(this.mMaxHeight, resolveSizeAndState2 & 0xFFFFFF);
            resolveSizeAndState = min;
            if (this.mLayoutWidget.isWidthMeasuredTooSmall()) {
                resolveSizeAndState = (min | 0x1000000);
            }
            min = min2;
            if (this.mLayoutWidget.isHeightMeasuredTooSmall()) {
                min = (min2 | 0x1000000);
            }
            this.setMeasuredDimension(resolveSizeAndState, min);
            this.mLastMeasureWidth = resolveSizeAndState;
            this.mLastMeasureHeight = min;
            return;
        }
        this.setMeasuredDimension(mLastMeasureWidth, mLastMeasureHeight);
        this.mLastMeasureWidth = mLastMeasureWidth;
        this.mLastMeasureHeight = mLastMeasureHeight;
    }
    
    public void onViewAdded(final View view) {
        if (Build$VERSION.SDK_INT >= 14) {
            super.onViewAdded(view);
        }
        final ConstraintWidget viewWidget = this.getViewWidget(view);
        if (view instanceof Guideline && !(viewWidget instanceof androidx.constraintlayout.solver.widgets.Guideline)) {
            final LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
            layoutParams.widget = new androidx.constraintlayout.solver.widgets.Guideline();
            layoutParams.isGuideline = true;
            ((androidx.constraintlayout.solver.widgets.Guideline)layoutParams.widget).setOrientation(layoutParams.orientation);
        }
        if (view instanceof ConstraintHelper) {
            final ConstraintHelper constraintHelper = (ConstraintHelper)view;
            constraintHelper.validateParams();
            ((LayoutParams)view.getLayoutParams()).isHelper = true;
            if (!this.mConstraintHelpers.contains(constraintHelper)) {
                this.mConstraintHelpers.add(constraintHelper);
            }
        }
        this.mChildrenByIds.put(view.getId(), (Object)view);
        this.mDirtyHierarchy = true;
    }
    
    public void onViewRemoved(final View o) {
        if (Build$VERSION.SDK_INT >= 14) {
            super.onViewRemoved(o);
        }
        this.mChildrenByIds.remove(o.getId());
        final ConstraintWidget viewWidget = this.getViewWidget(o);
        this.mLayoutWidget.remove(viewWidget);
        this.mConstraintHelpers.remove(o);
        this.mVariableDimensionsWidgets.remove(viewWidget);
        this.mDirtyHierarchy = true;
    }
    
    public void removeView(final View view) {
        super.removeView(view);
        if (Build$VERSION.SDK_INT < 14) {
            this.onViewRemoved(view);
        }
    }
    
    public void requestLayout() {
        super.requestLayout();
        this.mDirtyHierarchy = true;
        this.mLastMeasureWidth = -1;
        this.mLastMeasureHeight = -1;
        this.mLastMeasureWidthSize = -1;
        this.mLastMeasureHeightSize = -1;
        this.mLastMeasureWidthMode = 0;
        this.mLastMeasureHeightMode = 0;
    }
    
    public void setConstraintSet(final ConstraintSet mConstraintSet) {
        this.mConstraintSet = mConstraintSet;
    }
    
    public void setDesignInformation(int i, final Object o, final Object o2) {
        if (i == 0 && o instanceof String && o2 instanceof Integer) {
            if (this.mDesignIds == null) {
                this.mDesignIds = new HashMap<String, Integer>();
            }
            final String s = (String)o;
            i = s.indexOf("/");
            String substring = s;
            if (i != -1) {
                substring = s.substring(i + 1);
            }
            i = (int)o2;
            this.mDesignIds.put(substring, i);
        }
    }
    
    public void setId(final int id) {
        this.mChildrenByIds.remove(this.getId());
        super.setId(id);
        this.mChildrenByIds.put(this.getId(), (Object)this);
    }
    
    public void setMaxHeight(final int mMaxHeight) {
        if (mMaxHeight == this.mMaxHeight) {
            return;
        }
        this.mMaxHeight = mMaxHeight;
        this.requestLayout();
    }
    
    public void setMaxWidth(final int mMaxWidth) {
        if (mMaxWidth == this.mMaxWidth) {
            return;
        }
        this.mMaxWidth = mMaxWidth;
        this.requestLayout();
    }
    
    public void setMinHeight(final int mMinHeight) {
        if (mMinHeight == this.mMinHeight) {
            return;
        }
        this.mMinHeight = mMinHeight;
        this.requestLayout();
    }
    
    public void setMinWidth(final int mMinWidth) {
        if (mMinWidth == this.mMinWidth) {
            return;
        }
        this.mMinWidth = mMinWidth;
        this.requestLayout();
    }
    
    public void setOptimizationLevel(final int optimizationLevel) {
        this.mLayoutWidget.setOptimizationLevel(optimizationLevel);
    }
    
    public boolean shouldDelayChildPressedState() {
        return false;
    }
    
    protected void solveLinearSystem(final String s) {
        this.mLayoutWidget.layout();
        final Metrics mMetrics = this.mMetrics;
        if (mMetrics != null) {
            ++mMetrics.resolutions;
        }
    }
    
    public static class LayoutParams extends ViewGroup$MarginLayoutParams
    {
        public static final int BASELINE = 5;
        public static final int BOTTOM = 4;
        public static final int CHAIN_PACKED = 2;
        public static final int CHAIN_SPREAD = 0;
        public static final int CHAIN_SPREAD_INSIDE = 1;
        public static final int END = 7;
        public static final int HORIZONTAL = 0;
        public static final int LEFT = 1;
        public static final int MATCH_CONSTRAINT = 0;
        public static final int MATCH_CONSTRAINT_PERCENT = 2;
        public static final int MATCH_CONSTRAINT_SPREAD = 0;
        public static final int MATCH_CONSTRAINT_WRAP = 1;
        public static final int PARENT_ID = 0;
        public static final int RIGHT = 2;
        public static final int START = 6;
        public static final int TOP = 3;
        public static final int UNSET = -1;
        public static final int VERTICAL = 1;
        public int baselineToBaseline;
        public int bottomToBottom;
        public int bottomToTop;
        public float circleAngle;
        public int circleConstraint;
        public int circleRadius;
        public boolean constrainedHeight;
        public boolean constrainedWidth;
        public String dimensionRatio;
        int dimensionRatioSide;
        float dimensionRatioValue;
        public int editorAbsoluteX;
        public int editorAbsoluteY;
        public int endToEnd;
        public int endToStart;
        public int goneBottomMargin;
        public int goneEndMargin;
        public int goneLeftMargin;
        public int goneRightMargin;
        public int goneStartMargin;
        public int goneTopMargin;
        public int guideBegin;
        public int guideEnd;
        public float guidePercent;
        public boolean helped;
        public float horizontalBias;
        public int horizontalChainStyle;
        boolean horizontalDimensionFixed;
        public float horizontalWeight;
        boolean isGuideline;
        boolean isHelper;
        boolean isInPlaceholder;
        public int leftToLeft;
        public int leftToRight;
        public int matchConstraintDefaultHeight;
        public int matchConstraintDefaultWidth;
        public int matchConstraintMaxHeight;
        public int matchConstraintMaxWidth;
        public int matchConstraintMinHeight;
        public int matchConstraintMinWidth;
        public float matchConstraintPercentHeight;
        public float matchConstraintPercentWidth;
        boolean needsBaseline;
        public int orientation;
        int resolveGoneLeftMargin;
        int resolveGoneRightMargin;
        int resolvedGuideBegin;
        int resolvedGuideEnd;
        float resolvedGuidePercent;
        float resolvedHorizontalBias;
        int resolvedLeftToLeft;
        int resolvedLeftToRight;
        int resolvedRightToLeft;
        int resolvedRightToRight;
        public int rightToLeft;
        public int rightToRight;
        public int startToEnd;
        public int startToStart;
        public int topToBottom;
        public int topToTop;
        public float verticalBias;
        public int verticalChainStyle;
        boolean verticalDimensionFixed;
        public float verticalWeight;
        ConstraintWidget widget;
        
        public LayoutParams(final int n, final int n2) {
            super(n, n2);
            this.guideBegin = -1;
            this.guideEnd = -1;
            this.guidePercent = -1.0f;
            this.leftToLeft = -1;
            this.leftToRight = -1;
            this.rightToLeft = -1;
            this.rightToRight = -1;
            this.topToTop = -1;
            this.topToBottom = -1;
            this.bottomToTop = -1;
            this.bottomToBottom = -1;
            this.baselineToBaseline = -1;
            this.circleConstraint = -1;
            this.circleRadius = 0;
            this.circleAngle = 0.0f;
            this.startToEnd = -1;
            this.startToStart = -1;
            this.endToStart = -1;
            this.endToEnd = -1;
            this.goneLeftMargin = -1;
            this.goneTopMargin = -1;
            this.goneRightMargin = -1;
            this.goneBottomMargin = -1;
            this.goneStartMargin = -1;
            this.goneEndMargin = -1;
            this.horizontalBias = 0.5f;
            this.verticalBias = 0.5f;
            this.dimensionRatio = null;
            this.dimensionRatioValue = 0.0f;
            this.dimensionRatioSide = 1;
            this.horizontalWeight = -1.0f;
            this.verticalWeight = -1.0f;
            this.horizontalChainStyle = 0;
            this.verticalChainStyle = 0;
            this.matchConstraintDefaultWidth = 0;
            this.matchConstraintDefaultHeight = 0;
            this.matchConstraintMinWidth = 0;
            this.matchConstraintMinHeight = 0;
            this.matchConstraintMaxWidth = 0;
            this.matchConstraintMaxHeight = 0;
            this.matchConstraintPercentWidth = 1.0f;
            this.matchConstraintPercentHeight = 1.0f;
            this.editorAbsoluteX = -1;
            this.editorAbsoluteY = -1;
            this.orientation = -1;
            this.constrainedWidth = false;
            this.constrainedHeight = false;
            this.horizontalDimensionFixed = true;
            this.verticalDimensionFixed = true;
            this.needsBaseline = false;
            this.isGuideline = false;
            this.isHelper = false;
            this.isInPlaceholder = false;
            this.resolvedLeftToLeft = -1;
            this.resolvedLeftToRight = -1;
            this.resolvedRightToLeft = -1;
            this.resolvedRightToRight = -1;
            this.resolveGoneLeftMargin = -1;
            this.resolveGoneRightMargin = -1;
            this.resolvedHorizontalBias = 0.5f;
            this.widget = new ConstraintWidget();
            this.helped = false;
        }
        
        public LayoutParams(final Context p0, final AttributeSet p1) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: aload_1        
            //     2: aload_2        
            //     3: invokespecial   android/view/ViewGroup$MarginLayoutParams.<init>:(Landroid/content/Context;Landroid/util/AttributeSet;)V
            //     6: aload_0        
            //     7: iconst_m1      
            //     8: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.guideBegin:I
            //    11: aload_0        
            //    12: iconst_m1      
            //    13: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.guideEnd:I
            //    16: aload_0        
            //    17: ldc             -1.0
            //    19: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.guidePercent:F
            //    22: aload_0        
            //    23: iconst_m1      
            //    24: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.leftToLeft:I
            //    27: aload_0        
            //    28: iconst_m1      
            //    29: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.leftToRight:I
            //    32: aload_0        
            //    33: iconst_m1      
            //    34: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.rightToLeft:I
            //    37: aload_0        
            //    38: iconst_m1      
            //    39: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.rightToRight:I
            //    42: aload_0        
            //    43: iconst_m1      
            //    44: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.topToTop:I
            //    47: aload_0        
            //    48: iconst_m1      
            //    49: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.topToBottom:I
            //    52: aload_0        
            //    53: iconst_m1      
            //    54: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.bottomToTop:I
            //    57: aload_0        
            //    58: iconst_m1      
            //    59: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.bottomToBottom:I
            //    62: aload_0        
            //    63: iconst_m1      
            //    64: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.baselineToBaseline:I
            //    67: aload_0        
            //    68: iconst_m1      
            //    69: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.circleConstraint:I
            //    72: aload_0        
            //    73: iconst_0       
            //    74: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.circleRadius:I
            //    77: aload_0        
            //    78: fconst_0       
            //    79: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.circleAngle:F
            //    82: aload_0        
            //    83: iconst_m1      
            //    84: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.startToEnd:I
            //    87: aload_0        
            //    88: iconst_m1      
            //    89: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.startToStart:I
            //    92: aload_0        
            //    93: iconst_m1      
            //    94: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.endToStart:I
            //    97: aload_0        
            //    98: iconst_m1      
            //    99: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.endToEnd:I
            //   102: aload_0        
            //   103: iconst_m1      
            //   104: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.goneLeftMargin:I
            //   107: aload_0        
            //   108: iconst_m1      
            //   109: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.goneTopMargin:I
            //   112: aload_0        
            //   113: iconst_m1      
            //   114: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.goneRightMargin:I
            //   117: aload_0        
            //   118: iconst_m1      
            //   119: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.goneBottomMargin:I
            //   122: aload_0        
            //   123: iconst_m1      
            //   124: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.goneStartMargin:I
            //   127: aload_0        
            //   128: iconst_m1      
            //   129: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.goneEndMargin:I
            //   132: aload_0        
            //   133: ldc             0.5
            //   135: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.horizontalBias:F
            //   138: aload_0        
            //   139: ldc             0.5
            //   141: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.verticalBias:F
            //   144: aload_0        
            //   145: aconst_null    
            //   146: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.dimensionRatio:Ljava/lang/String;
            //   149: aload_0        
            //   150: fconst_0       
            //   151: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.dimensionRatioValue:F
            //   154: aload_0        
            //   155: iconst_1       
            //   156: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.dimensionRatioSide:I
            //   159: aload_0        
            //   160: ldc             -1.0
            //   162: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.horizontalWeight:F
            //   165: aload_0        
            //   166: ldc             -1.0
            //   168: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.verticalWeight:F
            //   171: aload_0        
            //   172: iconst_0       
            //   173: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.horizontalChainStyle:I
            //   176: aload_0        
            //   177: iconst_0       
            //   178: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.verticalChainStyle:I
            //   181: aload_0        
            //   182: iconst_0       
            //   183: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintDefaultWidth:I
            //   186: aload_0        
            //   187: iconst_0       
            //   188: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintDefaultHeight:I
            //   191: aload_0        
            //   192: iconst_0       
            //   193: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintMinWidth:I
            //   196: aload_0        
            //   197: iconst_0       
            //   198: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintMinHeight:I
            //   201: aload_0        
            //   202: iconst_0       
            //   203: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintMaxWidth:I
            //   206: aload_0        
            //   207: iconst_0       
            //   208: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintMaxHeight:I
            //   211: aload_0        
            //   212: fconst_1       
            //   213: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintPercentWidth:F
            //   216: aload_0        
            //   217: fconst_1       
            //   218: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintPercentHeight:F
            //   221: aload_0        
            //   222: iconst_m1      
            //   223: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.editorAbsoluteX:I
            //   226: aload_0        
            //   227: iconst_m1      
            //   228: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.editorAbsoluteY:I
            //   231: aload_0        
            //   232: iconst_m1      
            //   233: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.orientation:I
            //   236: aload_0        
            //   237: iconst_0       
            //   238: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.constrainedWidth:Z
            //   241: aload_0        
            //   242: iconst_0       
            //   243: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.constrainedHeight:Z
            //   246: aload_0        
            //   247: iconst_1       
            //   248: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.horizontalDimensionFixed:Z
            //   251: aload_0        
            //   252: iconst_1       
            //   253: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.verticalDimensionFixed:Z
            //   256: aload_0        
            //   257: iconst_0       
            //   258: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.needsBaseline:Z
            //   261: aload_0        
            //   262: iconst_0       
            //   263: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.isGuideline:Z
            //   266: aload_0        
            //   267: iconst_0       
            //   268: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.isHelper:Z
            //   271: aload_0        
            //   272: iconst_0       
            //   273: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.isInPlaceholder:Z
            //   276: aload_0        
            //   277: iconst_m1      
            //   278: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.resolvedLeftToLeft:I
            //   281: aload_0        
            //   282: iconst_m1      
            //   283: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.resolvedLeftToRight:I
            //   286: aload_0        
            //   287: iconst_m1      
            //   288: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.resolvedRightToLeft:I
            //   291: aload_0        
            //   292: iconst_m1      
            //   293: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.resolvedRightToRight:I
            //   296: aload_0        
            //   297: iconst_m1      
            //   298: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.resolveGoneLeftMargin:I
            //   301: aload_0        
            //   302: iconst_m1      
            //   303: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.resolveGoneRightMargin:I
            //   306: aload_0        
            //   307: ldc             0.5
            //   309: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.resolvedHorizontalBias:F
            //   312: aload_0        
            //   313: new             Landroidx/constraintlayout/solver/widgets/ConstraintWidget;
            //   316: dup            
            //   317: invokespecial   androidx/constraintlayout/solver/widgets/ConstraintWidget.<init>:()V
            //   320: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.widget:Landroidx/constraintlayout/solver/widgets/ConstraintWidget;
            //   323: aload_0        
            //   324: iconst_0       
            //   325: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.helped:Z
            //   328: aload_1        
            //   329: aload_2        
            //   330: getstatic       androidx/constraintlayout/widget/R$styleable.ConstraintLayout_Layout:[I
            //   333: invokevirtual   android/content/Context.obtainStyledAttributes:(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
            //   336: astore_1       
            //   337: aload_1        
            //   338: invokevirtual   android/content/res/TypedArray.getIndexCount:()I
            //   341: istore          7
            //   343: iconst_0       
            //   344: istore          5
            //   346: iload           5
            //   348: iload           7
            //   350: if_icmpge       2039
            //   353: aload_1        
            //   354: iload           5
            //   356: invokevirtual   android/content/res/TypedArray.getIndex:(I)I
            //   359: istore          6
            //   361: getstatic       androidx/constraintlayout/widget/ConstraintLayout$LayoutParams$Table.map:Landroid/util/SparseIntArray;
            //   364: iload           6
            //   366: invokevirtual   android/util/SparseIntArray.get:(I)I
            //   369: tableswitch {
            //                0: 2030
            //                1: 2016
            //                2: 1980
            //                3: 1963
            //                4: 1918
            //                5: 1901
            //                6: 1884
            //                7: 1867
            //                8: 1831
            //                9: 1795
            //               10: 1759
            //               11: 1723
            //               12: 1687
            //               13: 1651
            //               14: 1615
            //               15: 1579
            //               16: 1543
            //               17: 1507
            //               18: 1471
            //               19: 1435
            //               20: 1399
            //               21: 1382
            //               22: 1365
            //               23: 1348
            //               24: 1331
            //               25: 1314
            //               26: 1297
            //               27: 1280
            //               28: 1263
            //               29: 1246
            //               30: 1229
            //               31: 1197
            //               32: 1165
            //               33: 1124
            //               34: 1083
            //               35: 1062
            //               36: 1021
            //               37: 980
            //               38: 959
            //               39: 2030
            //               40: 2030
            //               41: 2030
            //               42: 2030
            //               43: 588
            //               44: 687
            //               45: 670
            //               46: 653
            //               47: 639
            //               48: 625
            //               49: 608
            //               50: 591
            //          default: 588
            //        }
            //   588: goto            2030
            //   591: aload_0        
            //   592: aload_1        
            //   593: iload           6
            //   595: aload_0        
            //   596: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.editorAbsoluteY:I
            //   599: invokevirtual   android/content/res/TypedArray.getDimensionPixelOffset:(II)I
            //   602: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.editorAbsoluteY:I
            //   605: goto            2030
            //   608: aload_0        
            //   609: aload_1        
            //   610: iload           6
            //   612: aload_0        
            //   613: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.editorAbsoluteX:I
            //   616: invokevirtual   android/content/res/TypedArray.getDimensionPixelOffset:(II)I
            //   619: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.editorAbsoluteX:I
            //   622: goto            2030
            //   625: aload_0        
            //   626: aload_1        
            //   627: iload           6
            //   629: iconst_0       
            //   630: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //   633: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.verticalChainStyle:I
            //   636: goto            2030
            //   639: aload_0        
            //   640: aload_1        
            //   641: iload           6
            //   643: iconst_0       
            //   644: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //   647: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.horizontalChainStyle:I
            //   650: goto            2030
            //   653: aload_0        
            //   654: aload_1        
            //   655: iload           6
            //   657: aload_0        
            //   658: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.verticalWeight:F
            //   661: invokevirtual   android/content/res/TypedArray.getFloat:(IF)F
            //   664: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.verticalWeight:F
            //   667: goto            2030
            //   670: aload_0        
            //   671: aload_1        
            //   672: iload           6
            //   674: aload_0        
            //   675: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.horizontalWeight:F
            //   678: invokevirtual   android/content/res/TypedArray.getFloat:(IF)F
            //   681: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.horizontalWeight:F
            //   684: goto            2030
            //   687: aload_0        
            //   688: aload_1        
            //   689: iload           6
            //   691: invokevirtual   android/content/res/TypedArray.getString:(I)Ljava/lang/String;
            //   694: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.dimensionRatio:Ljava/lang/String;
            //   697: aload_0        
            //   698: ldc_w           NaN
            //   701: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.dimensionRatioValue:F
            //   704: aload_0        
            //   705: iconst_m1      
            //   706: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.dimensionRatioSide:I
            //   709: aload_0        
            //   710: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.dimensionRatio:Ljava/lang/String;
            //   713: astore_2       
            //   714: aload_2        
            //   715: ifnull          2030
            //   718: aload_2        
            //   719: invokevirtual   java/lang/String.length:()I
            //   722: istore          8
            //   724: aload_0        
            //   725: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.dimensionRatio:Ljava/lang/String;
            //   728: bipush          44
            //   730: invokevirtual   java/lang/String.indexOf:(I)I
            //   733: istore          6
            //   735: iload           6
            //   737: ifle            802
            //   740: iload           6
            //   742: iload           8
            //   744: iconst_1       
            //   745: isub           
            //   746: if_icmpge       802
            //   749: aload_0        
            //   750: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.dimensionRatio:Ljava/lang/String;
            //   753: iconst_0       
            //   754: iload           6
            //   756: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
            //   759: astore_2       
            //   760: aload_2        
            //   761: ldc_w           "W"
            //   764: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
            //   767: ifeq            778
            //   770: aload_0        
            //   771: iconst_0       
            //   772: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.dimensionRatioSide:I
            //   775: goto            793
            //   778: aload_2        
            //   779: ldc_w           "H"
            //   782: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
            //   785: ifeq            793
            //   788: aload_0        
            //   789: iconst_1       
            //   790: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.dimensionRatioSide:I
            //   793: iload           6
            //   795: iconst_1       
            //   796: iadd           
            //   797: istore          6
            //   799: goto            805
            //   802: iconst_0       
            //   803: istore          6
            //   805: aload_0        
            //   806: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.dimensionRatio:Ljava/lang/String;
            //   809: bipush          58
            //   811: invokevirtual   java/lang/String.indexOf:(I)I
            //   814: istore          9
            //   816: iload           9
            //   818: iflt            931
            //   821: iload           9
            //   823: iload           8
            //   825: iconst_1       
            //   826: isub           
            //   827: if_icmpge       931
            //   830: aload_0        
            //   831: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.dimensionRatio:Ljava/lang/String;
            //   834: iload           6
            //   836: iload           9
            //   838: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
            //   841: astore_2       
            //   842: aload_0        
            //   843: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.dimensionRatio:Ljava/lang/String;
            //   846: iload           9
            //   848: iconst_1       
            //   849: iadd           
            //   850: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
            //   853: astore          10
            //   855: aload_2        
            //   856: invokevirtual   java/lang/String.length:()I
            //   859: ifle            2030
            //   862: aload           10
            //   864: invokevirtual   java/lang/String.length:()I
            //   867: ifle            2030
            //   870: aload_2        
            //   871: invokestatic    java/lang/Float.parseFloat:(Ljava/lang/String;)F
            //   874: fstore_3       
            //   875: aload           10
            //   877: invokestatic    java/lang/Float.parseFloat:(Ljava/lang/String;)F
            //   880: fstore          4
            //   882: fload_3        
            //   883: fconst_0       
            //   884: fcmpl          
            //   885: ifle            2030
            //   888: fload           4
            //   890: fconst_0       
            //   891: fcmpl          
            //   892: ifle            2030
            //   895: aload_0        
            //   896: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.dimensionRatioSide:I
            //   899: iconst_1       
            //   900: if_icmpne       917
            //   903: aload_0        
            //   904: fload           4
            //   906: fload_3        
            //   907: fdiv           
            //   908: invokestatic    java/lang/Math.abs:(F)F
            //   911: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.dimensionRatioValue:F
            //   914: goto            2030
            //   917: aload_0        
            //   918: fload_3        
            //   919: fload           4
            //   921: fdiv           
            //   922: invokestatic    java/lang/Math.abs:(F)F
            //   925: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.dimensionRatioValue:F
            //   928: goto            2030
            //   931: aload_0        
            //   932: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.dimensionRatio:Ljava/lang/String;
            //   935: iload           6
            //   937: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
            //   940: astore_2       
            //   941: aload_2        
            //   942: invokevirtual   java/lang/String.length:()I
            //   945: ifle            2030
            //   948: aload_0        
            //   949: aload_2        
            //   950: invokestatic    java/lang/Float.parseFloat:(Ljava/lang/String;)F
            //   953: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.dimensionRatioValue:F
            //   956: goto            2030
            //   959: aload_0        
            //   960: fconst_0       
            //   961: aload_1        
            //   962: iload           6
            //   964: aload_0        
            //   965: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintPercentHeight:F
            //   968: invokevirtual   android/content/res/TypedArray.getFloat:(IF)F
            //   971: invokestatic    java/lang/Math.max:(FF)F
            //   974: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintPercentHeight:F
            //   977: goto            2030
            //   980: aload_0        
            //   981: aload_1        
            //   982: iload           6
            //   984: aload_0        
            //   985: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintMaxHeight:I
            //   988: invokevirtual   android/content/res/TypedArray.getDimensionPixelSize:(II)I
            //   991: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintMaxHeight:I
            //   994: goto            2030
            //   997: aload_1        
            //   998: iload           6
            //  1000: aload_0        
            //  1001: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintMaxHeight:I
            //  1004: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  1007: bipush          -2
            //  1009: if_icmpne       2030
            //  1012: aload_0        
            //  1013: bipush          -2
            //  1015: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintMaxHeight:I
            //  1018: goto            2030
            //  1021: aload_0        
            //  1022: aload_1        
            //  1023: iload           6
            //  1025: aload_0        
            //  1026: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintMinHeight:I
            //  1029: invokevirtual   android/content/res/TypedArray.getDimensionPixelSize:(II)I
            //  1032: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintMinHeight:I
            //  1035: goto            2030
            //  1038: aload_1        
            //  1039: iload           6
            //  1041: aload_0        
            //  1042: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintMinHeight:I
            //  1045: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  1048: bipush          -2
            //  1050: if_icmpne       2030
            //  1053: aload_0        
            //  1054: bipush          -2
            //  1056: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintMinHeight:I
            //  1059: goto            2030
            //  1062: aload_0        
            //  1063: fconst_0       
            //  1064: aload_1        
            //  1065: iload           6
            //  1067: aload_0        
            //  1068: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintPercentWidth:F
            //  1071: invokevirtual   android/content/res/TypedArray.getFloat:(IF)F
            //  1074: invokestatic    java/lang/Math.max:(FF)F
            //  1077: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintPercentWidth:F
            //  1080: goto            2030
            //  1083: aload_0        
            //  1084: aload_1        
            //  1085: iload           6
            //  1087: aload_0        
            //  1088: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintMaxWidth:I
            //  1091: invokevirtual   android/content/res/TypedArray.getDimensionPixelSize:(II)I
            //  1094: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintMaxWidth:I
            //  1097: goto            2030
            //  1100: aload_1        
            //  1101: iload           6
            //  1103: aload_0        
            //  1104: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintMaxWidth:I
            //  1107: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  1110: bipush          -2
            //  1112: if_icmpne       2030
            //  1115: aload_0        
            //  1116: bipush          -2
            //  1118: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintMaxWidth:I
            //  1121: goto            2030
            //  1124: aload_0        
            //  1125: aload_1        
            //  1126: iload           6
            //  1128: aload_0        
            //  1129: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintMinWidth:I
            //  1132: invokevirtual   android/content/res/TypedArray.getDimensionPixelSize:(II)I
            //  1135: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintMinWidth:I
            //  1138: goto            2030
            //  1141: aload_1        
            //  1142: iload           6
            //  1144: aload_0        
            //  1145: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintMinWidth:I
            //  1148: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  1151: bipush          -2
            //  1153: if_icmpne       2030
            //  1156: aload_0        
            //  1157: bipush          -2
            //  1159: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintMinWidth:I
            //  1162: goto            2030
            //  1165: aload_0        
            //  1166: aload_1        
            //  1167: iload           6
            //  1169: iconst_0       
            //  1170: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  1173: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintDefaultHeight:I
            //  1176: aload_0        
            //  1177: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintDefaultHeight:I
            //  1180: iconst_1       
            //  1181: if_icmpne       2030
            //  1184: ldc_w           "ConstraintLayout"
            //  1187: ldc_w           "layout_constraintHeight_default=\"wrap\" is deprecated.\nUse layout_height=\"WRAP_CONTENT\" and layout_constrainedHeight=\"true\" instead."
            //  1190: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
            //  1193: pop            
            //  1194: goto            2030
            //  1197: aload_0        
            //  1198: aload_1        
            //  1199: iload           6
            //  1201: iconst_0       
            //  1202: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  1205: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintDefaultWidth:I
            //  1208: aload_0        
            //  1209: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.matchConstraintDefaultWidth:I
            //  1212: iconst_1       
            //  1213: if_icmpne       2030
            //  1216: ldc_w           "ConstraintLayout"
            //  1219: ldc_w           "layout_constraintWidth_default=\"wrap\" is deprecated.\nUse layout_width=\"WRAP_CONTENT\" and layout_constrainedWidth=\"true\" instead."
            //  1222: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
            //  1225: pop            
            //  1226: goto            2030
            //  1229: aload_0        
            //  1230: aload_1        
            //  1231: iload           6
            //  1233: aload_0        
            //  1234: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.verticalBias:F
            //  1237: invokevirtual   android/content/res/TypedArray.getFloat:(IF)F
            //  1240: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.verticalBias:F
            //  1243: goto            2030
            //  1246: aload_0        
            //  1247: aload_1        
            //  1248: iload           6
            //  1250: aload_0        
            //  1251: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.horizontalBias:F
            //  1254: invokevirtual   android/content/res/TypedArray.getFloat:(IF)F
            //  1257: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.horizontalBias:F
            //  1260: goto            2030
            //  1263: aload_0        
            //  1264: aload_1        
            //  1265: iload           6
            //  1267: aload_0        
            //  1268: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.constrainedHeight:Z
            //  1271: invokevirtual   android/content/res/TypedArray.getBoolean:(IZ)Z
            //  1274: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.constrainedHeight:Z
            //  1277: goto            2030
            //  1280: aload_0        
            //  1281: aload_1        
            //  1282: iload           6
            //  1284: aload_0        
            //  1285: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.constrainedWidth:Z
            //  1288: invokevirtual   android/content/res/TypedArray.getBoolean:(IZ)Z
            //  1291: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.constrainedWidth:Z
            //  1294: goto            2030
            //  1297: aload_0        
            //  1298: aload_1        
            //  1299: iload           6
            //  1301: aload_0        
            //  1302: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.goneEndMargin:I
            //  1305: invokevirtual   android/content/res/TypedArray.getDimensionPixelSize:(II)I
            //  1308: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.goneEndMargin:I
            //  1311: goto            2030
            //  1314: aload_0        
            //  1315: aload_1        
            //  1316: iload           6
            //  1318: aload_0        
            //  1319: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.goneStartMargin:I
            //  1322: invokevirtual   android/content/res/TypedArray.getDimensionPixelSize:(II)I
            //  1325: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.goneStartMargin:I
            //  1328: goto            2030
            //  1331: aload_0        
            //  1332: aload_1        
            //  1333: iload           6
            //  1335: aload_0        
            //  1336: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.goneBottomMargin:I
            //  1339: invokevirtual   android/content/res/TypedArray.getDimensionPixelSize:(II)I
            //  1342: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.goneBottomMargin:I
            //  1345: goto            2030
            //  1348: aload_0        
            //  1349: aload_1        
            //  1350: iload           6
            //  1352: aload_0        
            //  1353: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.goneRightMargin:I
            //  1356: invokevirtual   android/content/res/TypedArray.getDimensionPixelSize:(II)I
            //  1359: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.goneRightMargin:I
            //  1362: goto            2030
            //  1365: aload_0        
            //  1366: aload_1        
            //  1367: iload           6
            //  1369: aload_0        
            //  1370: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.goneTopMargin:I
            //  1373: invokevirtual   android/content/res/TypedArray.getDimensionPixelSize:(II)I
            //  1376: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.goneTopMargin:I
            //  1379: goto            2030
            //  1382: aload_0        
            //  1383: aload_1        
            //  1384: iload           6
            //  1386: aload_0        
            //  1387: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.goneLeftMargin:I
            //  1390: invokevirtual   android/content/res/TypedArray.getDimensionPixelSize:(II)I
            //  1393: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.goneLeftMargin:I
            //  1396: goto            2030
            //  1399: aload_0        
            //  1400: aload_1        
            //  1401: iload           6
            //  1403: aload_0        
            //  1404: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.endToEnd:I
            //  1407: invokevirtual   android/content/res/TypedArray.getResourceId:(II)I
            //  1410: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.endToEnd:I
            //  1413: aload_0        
            //  1414: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.endToEnd:I
            //  1417: iconst_m1      
            //  1418: if_icmpne       2030
            //  1421: aload_0        
            //  1422: aload_1        
            //  1423: iload           6
            //  1425: iconst_m1      
            //  1426: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  1429: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.endToEnd:I
            //  1432: goto            2030
            //  1435: aload_0        
            //  1436: aload_1        
            //  1437: iload           6
            //  1439: aload_0        
            //  1440: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.endToStart:I
            //  1443: invokevirtual   android/content/res/TypedArray.getResourceId:(II)I
            //  1446: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.endToStart:I
            //  1449: aload_0        
            //  1450: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.endToStart:I
            //  1453: iconst_m1      
            //  1454: if_icmpne       2030
            //  1457: aload_0        
            //  1458: aload_1        
            //  1459: iload           6
            //  1461: iconst_m1      
            //  1462: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  1465: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.endToStart:I
            //  1468: goto            2030
            //  1471: aload_0        
            //  1472: aload_1        
            //  1473: iload           6
            //  1475: aload_0        
            //  1476: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.startToStart:I
            //  1479: invokevirtual   android/content/res/TypedArray.getResourceId:(II)I
            //  1482: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.startToStart:I
            //  1485: aload_0        
            //  1486: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.startToStart:I
            //  1489: iconst_m1      
            //  1490: if_icmpne       2030
            //  1493: aload_0        
            //  1494: aload_1        
            //  1495: iload           6
            //  1497: iconst_m1      
            //  1498: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  1501: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.startToStart:I
            //  1504: goto            2030
            //  1507: aload_0        
            //  1508: aload_1        
            //  1509: iload           6
            //  1511: aload_0        
            //  1512: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.startToEnd:I
            //  1515: invokevirtual   android/content/res/TypedArray.getResourceId:(II)I
            //  1518: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.startToEnd:I
            //  1521: aload_0        
            //  1522: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.startToEnd:I
            //  1525: iconst_m1      
            //  1526: if_icmpne       2030
            //  1529: aload_0        
            //  1530: aload_1        
            //  1531: iload           6
            //  1533: iconst_m1      
            //  1534: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  1537: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.startToEnd:I
            //  1540: goto            2030
            //  1543: aload_0        
            //  1544: aload_1        
            //  1545: iload           6
            //  1547: aload_0        
            //  1548: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.baselineToBaseline:I
            //  1551: invokevirtual   android/content/res/TypedArray.getResourceId:(II)I
            //  1554: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.baselineToBaseline:I
            //  1557: aload_0        
            //  1558: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.baselineToBaseline:I
            //  1561: iconst_m1      
            //  1562: if_icmpne       2030
            //  1565: aload_0        
            //  1566: aload_1        
            //  1567: iload           6
            //  1569: iconst_m1      
            //  1570: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  1573: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.baselineToBaseline:I
            //  1576: goto            2030
            //  1579: aload_0        
            //  1580: aload_1        
            //  1581: iload           6
            //  1583: aload_0        
            //  1584: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.bottomToBottom:I
            //  1587: invokevirtual   android/content/res/TypedArray.getResourceId:(II)I
            //  1590: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.bottomToBottom:I
            //  1593: aload_0        
            //  1594: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.bottomToBottom:I
            //  1597: iconst_m1      
            //  1598: if_icmpne       2030
            //  1601: aload_0        
            //  1602: aload_1        
            //  1603: iload           6
            //  1605: iconst_m1      
            //  1606: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  1609: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.bottomToBottom:I
            //  1612: goto            2030
            //  1615: aload_0        
            //  1616: aload_1        
            //  1617: iload           6
            //  1619: aload_0        
            //  1620: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.bottomToTop:I
            //  1623: invokevirtual   android/content/res/TypedArray.getResourceId:(II)I
            //  1626: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.bottomToTop:I
            //  1629: aload_0        
            //  1630: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.bottomToTop:I
            //  1633: iconst_m1      
            //  1634: if_icmpne       2030
            //  1637: aload_0        
            //  1638: aload_1        
            //  1639: iload           6
            //  1641: iconst_m1      
            //  1642: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  1645: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.bottomToTop:I
            //  1648: goto            2030
            //  1651: aload_0        
            //  1652: aload_1        
            //  1653: iload           6
            //  1655: aload_0        
            //  1656: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.topToBottom:I
            //  1659: invokevirtual   android/content/res/TypedArray.getResourceId:(II)I
            //  1662: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.topToBottom:I
            //  1665: aload_0        
            //  1666: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.topToBottom:I
            //  1669: iconst_m1      
            //  1670: if_icmpne       2030
            //  1673: aload_0        
            //  1674: aload_1        
            //  1675: iload           6
            //  1677: iconst_m1      
            //  1678: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  1681: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.topToBottom:I
            //  1684: goto            2030
            //  1687: aload_0        
            //  1688: aload_1        
            //  1689: iload           6
            //  1691: aload_0        
            //  1692: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.topToTop:I
            //  1695: invokevirtual   android/content/res/TypedArray.getResourceId:(II)I
            //  1698: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.topToTop:I
            //  1701: aload_0        
            //  1702: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.topToTop:I
            //  1705: iconst_m1      
            //  1706: if_icmpne       2030
            //  1709: aload_0        
            //  1710: aload_1        
            //  1711: iload           6
            //  1713: iconst_m1      
            //  1714: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  1717: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.topToTop:I
            //  1720: goto            2030
            //  1723: aload_0        
            //  1724: aload_1        
            //  1725: iload           6
            //  1727: aload_0        
            //  1728: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.rightToRight:I
            //  1731: invokevirtual   android/content/res/TypedArray.getResourceId:(II)I
            //  1734: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.rightToRight:I
            //  1737: aload_0        
            //  1738: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.rightToRight:I
            //  1741: iconst_m1      
            //  1742: if_icmpne       2030
            //  1745: aload_0        
            //  1746: aload_1        
            //  1747: iload           6
            //  1749: iconst_m1      
            //  1750: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  1753: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.rightToRight:I
            //  1756: goto            2030
            //  1759: aload_0        
            //  1760: aload_1        
            //  1761: iload           6
            //  1763: aload_0        
            //  1764: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.rightToLeft:I
            //  1767: invokevirtual   android/content/res/TypedArray.getResourceId:(II)I
            //  1770: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.rightToLeft:I
            //  1773: aload_0        
            //  1774: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.rightToLeft:I
            //  1777: iconst_m1      
            //  1778: if_icmpne       2030
            //  1781: aload_0        
            //  1782: aload_1        
            //  1783: iload           6
            //  1785: iconst_m1      
            //  1786: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  1789: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.rightToLeft:I
            //  1792: goto            2030
            //  1795: aload_0        
            //  1796: aload_1        
            //  1797: iload           6
            //  1799: aload_0        
            //  1800: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.leftToRight:I
            //  1803: invokevirtual   android/content/res/TypedArray.getResourceId:(II)I
            //  1806: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.leftToRight:I
            //  1809: aload_0        
            //  1810: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.leftToRight:I
            //  1813: iconst_m1      
            //  1814: if_icmpne       2030
            //  1817: aload_0        
            //  1818: aload_1        
            //  1819: iload           6
            //  1821: iconst_m1      
            //  1822: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  1825: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.leftToRight:I
            //  1828: goto            2030
            //  1831: aload_0        
            //  1832: aload_1        
            //  1833: iload           6
            //  1835: aload_0        
            //  1836: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.leftToLeft:I
            //  1839: invokevirtual   android/content/res/TypedArray.getResourceId:(II)I
            //  1842: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.leftToLeft:I
            //  1845: aload_0        
            //  1846: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.leftToLeft:I
            //  1849: iconst_m1      
            //  1850: if_icmpne       2030
            //  1853: aload_0        
            //  1854: aload_1        
            //  1855: iload           6
            //  1857: iconst_m1      
            //  1858: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  1861: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.leftToLeft:I
            //  1864: goto            2030
            //  1867: aload_0        
            //  1868: aload_1        
            //  1869: iload           6
            //  1871: aload_0        
            //  1872: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.guidePercent:F
            //  1875: invokevirtual   android/content/res/TypedArray.getFloat:(IF)F
            //  1878: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.guidePercent:F
            //  1881: goto            2030
            //  1884: aload_0        
            //  1885: aload_1        
            //  1886: iload           6
            //  1888: aload_0        
            //  1889: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.guideEnd:I
            //  1892: invokevirtual   android/content/res/TypedArray.getDimensionPixelOffset:(II)I
            //  1895: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.guideEnd:I
            //  1898: goto            2030
            //  1901: aload_0        
            //  1902: aload_1        
            //  1903: iload           6
            //  1905: aload_0        
            //  1906: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.guideBegin:I
            //  1909: invokevirtual   android/content/res/TypedArray.getDimensionPixelOffset:(II)I
            //  1912: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.guideBegin:I
            //  1915: goto            2030
            //  1918: aload_0        
            //  1919: aload_1        
            //  1920: iload           6
            //  1922: aload_0        
            //  1923: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.circleAngle:F
            //  1926: invokevirtual   android/content/res/TypedArray.getFloat:(IF)F
            //  1929: ldc_w           360.0
            //  1932: frem           
            //  1933: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.circleAngle:F
            //  1936: aload_0        
            //  1937: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.circleAngle:F
            //  1940: fstore_3       
            //  1941: fload_3        
            //  1942: fconst_0       
            //  1943: fcmpg          
            //  1944: ifge            2030
            //  1947: aload_0        
            //  1948: ldc_w           360.0
            //  1951: fload_3        
            //  1952: fsub           
            //  1953: ldc_w           360.0
            //  1956: frem           
            //  1957: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.circleAngle:F
            //  1960: goto            2030
            //  1963: aload_0        
            //  1964: aload_1        
            //  1965: iload           6
            //  1967: aload_0        
            //  1968: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.circleRadius:I
            //  1971: invokevirtual   android/content/res/TypedArray.getDimensionPixelSize:(II)I
            //  1974: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.circleRadius:I
            //  1977: goto            2030
            //  1980: aload_0        
            //  1981: aload_1        
            //  1982: iload           6
            //  1984: aload_0        
            //  1985: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.circleConstraint:I
            //  1988: invokevirtual   android/content/res/TypedArray.getResourceId:(II)I
            //  1991: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.circleConstraint:I
            //  1994: aload_0        
            //  1995: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.circleConstraint:I
            //  1998: iconst_m1      
            //  1999: if_icmpne       2030
            //  2002: aload_0        
            //  2003: aload_1        
            //  2004: iload           6
            //  2006: iconst_m1      
            //  2007: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  2010: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.circleConstraint:I
            //  2013: goto            2030
            //  2016: aload_0        
            //  2017: aload_1        
            //  2018: iload           6
            //  2020: aload_0        
            //  2021: getfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.orientation:I
            //  2024: invokevirtual   android/content/res/TypedArray.getInt:(II)I
            //  2027: putfield        androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.orientation:I
            //  2030: iload           5
            //  2032: iconst_1       
            //  2033: iadd           
            //  2034: istore          5
            //  2036: goto            346
            //  2039: aload_1        
            //  2040: invokevirtual   android/content/res/TypedArray.recycle:()V
            //  2043: aload_0        
            //  2044: invokevirtual   androidx/constraintlayout/widget/ConstraintLayout$LayoutParams.validate:()V
            //  2047: return         
            //  2048: astore_2       
            //  2049: goto            2030
            //  2052: astore_2       
            //  2053: goto            997
            //  2056: astore_2       
            //  2057: goto            1038
            //  2060: astore_2       
            //  2061: goto            1100
            //  2064: astore_2       
            //  2065: goto            1141
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                             
            //  -----  -----  -----  -----  ---------------------------------
            //  870    882    2048   2052   Ljava/lang/NumberFormatException;
            //  895    914    2048   2052   Ljava/lang/NumberFormatException;
            //  917    928    2048   2052   Ljava/lang/NumberFormatException;
            //  948    956    2048   2052   Ljava/lang/NumberFormatException;
            //  980    994    2052   1021   Ljava/lang/Exception;
            //  1021   1035   2056   1062   Ljava/lang/Exception;
            //  1083   1097   2060   1124   Ljava/lang/Exception;
            //  1124   1138   2064   1165   Ljava/lang/Exception;
            // 
            // The error that occurred was:
            // 
            // java.lang.IndexOutOfBoundsException: Index 936 out of bounds for length 936
            //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
            //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
            //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
            //     at java.base/java.util.Objects.checkIndex(Objects.java:373)
            //     at java.base/java.util.ArrayList.get(ArrayList.java:426)
            //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
            //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:713)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:549)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:576)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        public LayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
            this.guideBegin = -1;
            this.guideEnd = -1;
            this.guidePercent = -1.0f;
            this.leftToLeft = -1;
            this.leftToRight = -1;
            this.rightToLeft = -1;
            this.rightToRight = -1;
            this.topToTop = -1;
            this.topToBottom = -1;
            this.bottomToTop = -1;
            this.bottomToBottom = -1;
            this.baselineToBaseline = -1;
            this.circleConstraint = -1;
            this.circleRadius = 0;
            this.circleAngle = 0.0f;
            this.startToEnd = -1;
            this.startToStart = -1;
            this.endToStart = -1;
            this.endToEnd = -1;
            this.goneLeftMargin = -1;
            this.goneTopMargin = -1;
            this.goneRightMargin = -1;
            this.goneBottomMargin = -1;
            this.goneStartMargin = -1;
            this.goneEndMargin = -1;
            this.horizontalBias = 0.5f;
            this.verticalBias = 0.5f;
            this.dimensionRatio = null;
            this.dimensionRatioValue = 0.0f;
            this.dimensionRatioSide = 1;
            this.horizontalWeight = -1.0f;
            this.verticalWeight = -1.0f;
            this.horizontalChainStyle = 0;
            this.verticalChainStyle = 0;
            this.matchConstraintDefaultWidth = 0;
            this.matchConstraintDefaultHeight = 0;
            this.matchConstraintMinWidth = 0;
            this.matchConstraintMinHeight = 0;
            this.matchConstraintMaxWidth = 0;
            this.matchConstraintMaxHeight = 0;
            this.matchConstraintPercentWidth = 1.0f;
            this.matchConstraintPercentHeight = 1.0f;
            this.editorAbsoluteX = -1;
            this.editorAbsoluteY = -1;
            this.orientation = -1;
            this.constrainedWidth = false;
            this.constrainedHeight = false;
            this.horizontalDimensionFixed = true;
            this.verticalDimensionFixed = true;
            this.needsBaseline = false;
            this.isGuideline = false;
            this.isHelper = false;
            this.isInPlaceholder = false;
            this.resolvedLeftToLeft = -1;
            this.resolvedLeftToRight = -1;
            this.resolvedRightToLeft = -1;
            this.resolvedRightToRight = -1;
            this.resolveGoneLeftMargin = -1;
            this.resolveGoneRightMargin = -1;
            this.resolvedHorizontalBias = 0.5f;
            this.widget = new ConstraintWidget();
            this.helped = false;
        }
        
        public LayoutParams(final LayoutParams layoutParams) {
            super((ViewGroup$MarginLayoutParams)layoutParams);
            this.guideBegin = -1;
            this.guideEnd = -1;
            this.guidePercent = -1.0f;
            this.leftToLeft = -1;
            this.leftToRight = -1;
            this.rightToLeft = -1;
            this.rightToRight = -1;
            this.topToTop = -1;
            this.topToBottom = -1;
            this.bottomToTop = -1;
            this.bottomToBottom = -1;
            this.baselineToBaseline = -1;
            this.circleConstraint = -1;
            this.circleRadius = 0;
            this.circleAngle = 0.0f;
            this.startToEnd = -1;
            this.startToStart = -1;
            this.endToStart = -1;
            this.endToEnd = -1;
            this.goneLeftMargin = -1;
            this.goneTopMargin = -1;
            this.goneRightMargin = -1;
            this.goneBottomMargin = -1;
            this.goneStartMargin = -1;
            this.goneEndMargin = -1;
            this.horizontalBias = 0.5f;
            this.verticalBias = 0.5f;
            this.dimensionRatio = null;
            this.dimensionRatioValue = 0.0f;
            this.dimensionRatioSide = 1;
            this.horizontalWeight = -1.0f;
            this.verticalWeight = -1.0f;
            this.horizontalChainStyle = 0;
            this.verticalChainStyle = 0;
            this.matchConstraintDefaultWidth = 0;
            this.matchConstraintDefaultHeight = 0;
            this.matchConstraintMinWidth = 0;
            this.matchConstraintMinHeight = 0;
            this.matchConstraintMaxWidth = 0;
            this.matchConstraintMaxHeight = 0;
            this.matchConstraintPercentWidth = 1.0f;
            this.matchConstraintPercentHeight = 1.0f;
            this.editorAbsoluteX = -1;
            this.editorAbsoluteY = -1;
            this.orientation = -1;
            this.constrainedWidth = false;
            this.constrainedHeight = false;
            this.horizontalDimensionFixed = true;
            this.verticalDimensionFixed = true;
            this.needsBaseline = false;
            this.isGuideline = false;
            this.isHelper = false;
            this.isInPlaceholder = false;
            this.resolvedLeftToLeft = -1;
            this.resolvedLeftToRight = -1;
            this.resolvedRightToLeft = -1;
            this.resolvedRightToRight = -1;
            this.resolveGoneLeftMargin = -1;
            this.resolveGoneRightMargin = -1;
            this.resolvedHorizontalBias = 0.5f;
            this.widget = new ConstraintWidget();
            this.helped = false;
            this.guideBegin = layoutParams.guideBegin;
            this.guideEnd = layoutParams.guideEnd;
            this.guidePercent = layoutParams.guidePercent;
            this.leftToLeft = layoutParams.leftToLeft;
            this.leftToRight = layoutParams.leftToRight;
            this.rightToLeft = layoutParams.rightToLeft;
            this.rightToRight = layoutParams.rightToRight;
            this.topToTop = layoutParams.topToTop;
            this.topToBottom = layoutParams.topToBottom;
            this.bottomToTop = layoutParams.bottomToTop;
            this.bottomToBottom = layoutParams.bottomToBottom;
            this.baselineToBaseline = layoutParams.baselineToBaseline;
            this.circleConstraint = layoutParams.circleConstraint;
            this.circleRadius = layoutParams.circleRadius;
            this.circleAngle = layoutParams.circleAngle;
            this.startToEnd = layoutParams.startToEnd;
            this.startToStart = layoutParams.startToStart;
            this.endToStart = layoutParams.endToStart;
            this.endToEnd = layoutParams.endToEnd;
            this.goneLeftMargin = layoutParams.goneLeftMargin;
            this.goneTopMargin = layoutParams.goneTopMargin;
            this.goneRightMargin = layoutParams.goneRightMargin;
            this.goneBottomMargin = layoutParams.goneBottomMargin;
            this.goneStartMargin = layoutParams.goneStartMargin;
            this.goneEndMargin = layoutParams.goneEndMargin;
            this.horizontalBias = layoutParams.horizontalBias;
            this.verticalBias = layoutParams.verticalBias;
            this.dimensionRatio = layoutParams.dimensionRatio;
            this.dimensionRatioValue = layoutParams.dimensionRatioValue;
            this.dimensionRatioSide = layoutParams.dimensionRatioSide;
            this.horizontalWeight = layoutParams.horizontalWeight;
            this.verticalWeight = layoutParams.verticalWeight;
            this.horizontalChainStyle = layoutParams.horizontalChainStyle;
            this.verticalChainStyle = layoutParams.verticalChainStyle;
            this.constrainedWidth = layoutParams.constrainedWidth;
            this.constrainedHeight = layoutParams.constrainedHeight;
            this.matchConstraintDefaultWidth = layoutParams.matchConstraintDefaultWidth;
            this.matchConstraintDefaultHeight = layoutParams.matchConstraintDefaultHeight;
            this.matchConstraintMinWidth = layoutParams.matchConstraintMinWidth;
            this.matchConstraintMaxWidth = layoutParams.matchConstraintMaxWidth;
            this.matchConstraintMinHeight = layoutParams.matchConstraintMinHeight;
            this.matchConstraintMaxHeight = layoutParams.matchConstraintMaxHeight;
            this.matchConstraintPercentWidth = layoutParams.matchConstraintPercentWidth;
            this.matchConstraintPercentHeight = layoutParams.matchConstraintPercentHeight;
            this.editorAbsoluteX = layoutParams.editorAbsoluteX;
            this.editorAbsoluteY = layoutParams.editorAbsoluteY;
            this.orientation = layoutParams.orientation;
            this.horizontalDimensionFixed = layoutParams.horizontalDimensionFixed;
            this.verticalDimensionFixed = layoutParams.verticalDimensionFixed;
            this.needsBaseline = layoutParams.needsBaseline;
            this.isGuideline = layoutParams.isGuideline;
            this.resolvedLeftToLeft = layoutParams.resolvedLeftToLeft;
            this.resolvedLeftToRight = layoutParams.resolvedLeftToRight;
            this.resolvedRightToLeft = layoutParams.resolvedRightToLeft;
            this.resolvedRightToRight = layoutParams.resolvedRightToRight;
            this.resolveGoneLeftMargin = layoutParams.resolveGoneLeftMargin;
            this.resolveGoneRightMargin = layoutParams.resolveGoneRightMargin;
            this.resolvedHorizontalBias = layoutParams.resolvedHorizontalBias;
            this.widget = layoutParams.widget;
        }
        
        public void reset() {
            final ConstraintWidget widget = this.widget;
            if (widget != null) {
                widget.reset();
            }
        }
        
        public void resolveLayoutDirection(int resolvedLeftToRight) {
            final int leftMargin = this.leftMargin;
            final int rightMargin = this.rightMargin;
            super.resolveLayoutDirection(resolvedLeftToRight);
            this.resolvedRightToLeft = -1;
            this.resolvedRightToRight = -1;
            this.resolvedLeftToLeft = -1;
            this.resolvedLeftToRight = -1;
            this.resolveGoneLeftMargin = -1;
            this.resolveGoneRightMargin = -1;
            this.resolveGoneLeftMargin = this.goneLeftMargin;
            this.resolveGoneRightMargin = this.goneRightMargin;
            this.resolvedHorizontalBias = this.horizontalBias;
            this.resolvedGuideBegin = this.guideBegin;
            this.resolvedGuideEnd = this.guideEnd;
            this.resolvedGuidePercent = this.guidePercent;
            resolvedLeftToRight = this.getLayoutDirection();
            final int n = 0;
            if (1 == resolvedLeftToRight) {
                resolvedLeftToRight = 1;
            }
            else {
                resolvedLeftToRight = 0;
            }
            if (resolvedLeftToRight != 0) {
                resolvedLeftToRight = this.startToEnd;
                Label_0161: {
                    if (resolvedLeftToRight != -1) {
                        this.resolvedRightToLeft = resolvedLeftToRight;
                    }
                    else {
                        final int startToStart = this.startToStart;
                        resolvedLeftToRight = n;
                        if (startToStart == -1) {
                            break Label_0161;
                        }
                        this.resolvedRightToRight = startToStart;
                    }
                    resolvedLeftToRight = 1;
                }
                final int endToStart = this.endToStart;
                if (endToStart != -1) {
                    this.resolvedLeftToRight = endToStart;
                    resolvedLeftToRight = 1;
                }
                final int endToEnd = this.endToEnd;
                if (endToEnd != -1) {
                    this.resolvedLeftToLeft = endToEnd;
                    resolvedLeftToRight = 1;
                }
                final int goneStartMargin = this.goneStartMargin;
                if (goneStartMargin != -1) {
                    this.resolveGoneRightMargin = goneStartMargin;
                }
                final int goneEndMargin = this.goneEndMargin;
                if (goneEndMargin != -1) {
                    this.resolveGoneLeftMargin = goneEndMargin;
                }
                if (resolvedLeftToRight != 0) {
                    this.resolvedHorizontalBias = 1.0f - this.horizontalBias;
                }
                if (this.isGuideline && this.orientation == 1) {
                    final float guidePercent = this.guidePercent;
                    if (guidePercent != -1.0f) {
                        this.resolvedGuidePercent = 1.0f - guidePercent;
                        this.resolvedGuideBegin = -1;
                        this.resolvedGuideEnd = -1;
                    }
                    else {
                        resolvedLeftToRight = this.guideBegin;
                        if (resolvedLeftToRight != -1) {
                            this.resolvedGuideEnd = resolvedLeftToRight;
                            this.resolvedGuideBegin = -1;
                            this.resolvedGuidePercent = -1.0f;
                        }
                        else {
                            resolvedLeftToRight = this.guideEnd;
                            if (resolvedLeftToRight != -1) {
                                this.resolvedGuideBegin = resolvedLeftToRight;
                                this.resolvedGuideEnd = -1;
                                this.resolvedGuidePercent = -1.0f;
                            }
                        }
                    }
                }
            }
            else {
                resolvedLeftToRight = this.startToEnd;
                if (resolvedLeftToRight != -1) {
                    this.resolvedLeftToRight = resolvedLeftToRight;
                }
                resolvedLeftToRight = this.startToStart;
                if (resolvedLeftToRight != -1) {
                    this.resolvedLeftToLeft = resolvedLeftToRight;
                }
                resolvedLeftToRight = this.endToStart;
                if (resolvedLeftToRight != -1) {
                    this.resolvedRightToLeft = resolvedLeftToRight;
                }
                resolvedLeftToRight = this.endToEnd;
                if (resolvedLeftToRight != -1) {
                    this.resolvedRightToRight = resolvedLeftToRight;
                }
                resolvedLeftToRight = this.goneStartMargin;
                if (resolvedLeftToRight != -1) {
                    this.resolveGoneLeftMargin = resolvedLeftToRight;
                }
                resolvedLeftToRight = this.goneEndMargin;
                if (resolvedLeftToRight != -1) {
                    this.resolveGoneRightMargin = resolvedLeftToRight;
                }
            }
            if (this.endToStart == -1 && this.endToEnd == -1 && this.startToStart == -1 && this.startToEnd == -1) {
                resolvedLeftToRight = this.rightToLeft;
                if (resolvedLeftToRight != -1) {
                    this.resolvedRightToLeft = resolvedLeftToRight;
                    if (this.rightMargin <= 0 && rightMargin > 0) {
                        this.rightMargin = rightMargin;
                    }
                }
                else {
                    resolvedLeftToRight = this.rightToRight;
                    if (resolvedLeftToRight != -1) {
                        this.resolvedRightToRight = resolvedLeftToRight;
                        if (this.rightMargin <= 0 && rightMargin > 0) {
                            this.rightMargin = rightMargin;
                        }
                    }
                }
                resolvedLeftToRight = this.leftToLeft;
                if (resolvedLeftToRight != -1) {
                    this.resolvedLeftToLeft = resolvedLeftToRight;
                    if (this.leftMargin <= 0 && leftMargin > 0) {
                        this.leftMargin = leftMargin;
                    }
                }
                else {
                    resolvedLeftToRight = this.leftToRight;
                    if (resolvedLeftToRight != -1) {
                        this.resolvedLeftToRight = resolvedLeftToRight;
                        if (this.leftMargin <= 0 && leftMargin > 0) {
                            this.leftMargin = leftMargin;
                        }
                    }
                }
            }
        }
        
        public void validate() {
            this.isGuideline = false;
            this.horizontalDimensionFixed = true;
            this.verticalDimensionFixed = true;
            if (this.width == -2 && this.constrainedWidth) {
                this.horizontalDimensionFixed = false;
                this.matchConstraintDefaultWidth = 1;
            }
            if (this.height == -2 && this.constrainedHeight) {
                this.verticalDimensionFixed = false;
                this.matchConstraintDefaultHeight = 1;
            }
            if (this.width == 0 || this.width == -1) {
                this.horizontalDimensionFixed = false;
                if (this.width == 0 && this.matchConstraintDefaultWidth == 1) {
                    this.width = -2;
                    this.constrainedWidth = true;
                }
            }
            if (this.height == 0 || this.height == -1) {
                this.verticalDimensionFixed = false;
                if (this.height == 0 && this.matchConstraintDefaultHeight == 1) {
                    this.height = -2;
                    this.constrainedHeight = true;
                }
            }
            if (this.guidePercent != -1.0f || this.guideBegin != -1 || this.guideEnd != -1) {
                this.isGuideline = true;
                this.horizontalDimensionFixed = true;
                this.verticalDimensionFixed = true;
                if (!(this.widget instanceof androidx.constraintlayout.solver.widgets.Guideline)) {
                    this.widget = new androidx.constraintlayout.solver.widgets.Guideline();
                }
                ((androidx.constraintlayout.solver.widgets.Guideline)this.widget).setOrientation(this.orientation);
            }
        }
        
        private static class Table
        {
            public static final int ANDROID_ORIENTATION = 1;
            public static final int LAYOUT_CONSTRAINED_HEIGHT = 28;
            public static final int LAYOUT_CONSTRAINED_WIDTH = 27;
            public static final int LAYOUT_CONSTRAINT_BASELINE_CREATOR = 43;
            public static final int LAYOUT_CONSTRAINT_BASELINE_TO_BASELINE_OF = 16;
            public static final int LAYOUT_CONSTRAINT_BOTTOM_CREATOR = 42;
            public static final int LAYOUT_CONSTRAINT_BOTTOM_TO_BOTTOM_OF = 15;
            public static final int LAYOUT_CONSTRAINT_BOTTOM_TO_TOP_OF = 14;
            public static final int LAYOUT_CONSTRAINT_CIRCLE = 2;
            public static final int LAYOUT_CONSTRAINT_CIRCLE_ANGLE = 4;
            public static final int LAYOUT_CONSTRAINT_CIRCLE_RADIUS = 3;
            public static final int LAYOUT_CONSTRAINT_DIMENSION_RATIO = 44;
            public static final int LAYOUT_CONSTRAINT_END_TO_END_OF = 20;
            public static final int LAYOUT_CONSTRAINT_END_TO_START_OF = 19;
            public static final int LAYOUT_CONSTRAINT_GUIDE_BEGIN = 5;
            public static final int LAYOUT_CONSTRAINT_GUIDE_END = 6;
            public static final int LAYOUT_CONSTRAINT_GUIDE_PERCENT = 7;
            public static final int LAYOUT_CONSTRAINT_HEIGHT_DEFAULT = 32;
            public static final int LAYOUT_CONSTRAINT_HEIGHT_MAX = 37;
            public static final int LAYOUT_CONSTRAINT_HEIGHT_MIN = 36;
            public static final int LAYOUT_CONSTRAINT_HEIGHT_PERCENT = 38;
            public static final int LAYOUT_CONSTRAINT_HORIZONTAL_BIAS = 29;
            public static final int LAYOUT_CONSTRAINT_HORIZONTAL_CHAINSTYLE = 47;
            public static final int LAYOUT_CONSTRAINT_HORIZONTAL_WEIGHT = 45;
            public static final int LAYOUT_CONSTRAINT_LEFT_CREATOR = 39;
            public static final int LAYOUT_CONSTRAINT_LEFT_TO_LEFT_OF = 8;
            public static final int LAYOUT_CONSTRAINT_LEFT_TO_RIGHT_OF = 9;
            public static final int LAYOUT_CONSTRAINT_RIGHT_CREATOR = 41;
            public static final int LAYOUT_CONSTRAINT_RIGHT_TO_LEFT_OF = 10;
            public static final int LAYOUT_CONSTRAINT_RIGHT_TO_RIGHT_OF = 11;
            public static final int LAYOUT_CONSTRAINT_START_TO_END_OF = 17;
            public static final int LAYOUT_CONSTRAINT_START_TO_START_OF = 18;
            public static final int LAYOUT_CONSTRAINT_TOP_CREATOR = 40;
            public static final int LAYOUT_CONSTRAINT_TOP_TO_BOTTOM_OF = 13;
            public static final int LAYOUT_CONSTRAINT_TOP_TO_TOP_OF = 12;
            public static final int LAYOUT_CONSTRAINT_VERTICAL_BIAS = 30;
            public static final int LAYOUT_CONSTRAINT_VERTICAL_CHAINSTYLE = 48;
            public static final int LAYOUT_CONSTRAINT_VERTICAL_WEIGHT = 46;
            public static final int LAYOUT_CONSTRAINT_WIDTH_DEFAULT = 31;
            public static final int LAYOUT_CONSTRAINT_WIDTH_MAX = 34;
            public static final int LAYOUT_CONSTRAINT_WIDTH_MIN = 33;
            public static final int LAYOUT_CONSTRAINT_WIDTH_PERCENT = 35;
            public static final int LAYOUT_EDITOR_ABSOLUTEX = 49;
            public static final int LAYOUT_EDITOR_ABSOLUTEY = 50;
            public static final int LAYOUT_GONE_MARGIN_BOTTOM = 24;
            public static final int LAYOUT_GONE_MARGIN_END = 26;
            public static final int LAYOUT_GONE_MARGIN_LEFT = 21;
            public static final int LAYOUT_GONE_MARGIN_RIGHT = 23;
            public static final int LAYOUT_GONE_MARGIN_START = 25;
            public static final int LAYOUT_GONE_MARGIN_TOP = 22;
            public static final int UNUSED = 0;
            public static final SparseIntArray map;
            
            static {
                (map = new SparseIntArray()).append(R.styleable.ConstraintLayout_Layout_layout_constraintLeft_toLeftOf, 8);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintLeft_toRightOf, 9);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintRight_toLeftOf, 10);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintRight_toRightOf, 11);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintTop_toTopOf, 12);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintTop_toBottomOf, 13);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintBottom_toTopOf, 14);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintBottom_toBottomOf, 15);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintBaseline_toBaselineOf, 16);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintCircle, 2);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintCircleRadius, 3);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintCircleAngle, 4);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_editor_absoluteX, 49);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_editor_absoluteY, 50);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintGuide_begin, 5);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintGuide_end, 6);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintGuide_percent, 7);
                Table.map.append(R.styleable.ConstraintLayout_Layout_android_orientation, 1);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintStart_toEndOf, 17);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintStart_toStartOf, 18);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintEnd_toStartOf, 19);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintEnd_toEndOf, 20);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_goneMarginLeft, 21);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_goneMarginTop, 22);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_goneMarginRight, 23);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_goneMarginBottom, 24);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_goneMarginStart, 25);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_goneMarginEnd, 26);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintHorizontal_bias, 29);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintVertical_bias, 30);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintDimensionRatio, 44);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintHorizontal_weight, 45);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintVertical_weight, 46);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintHorizontal_chainStyle, 47);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintVertical_chainStyle, 48);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constrainedWidth, 27);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constrainedHeight, 28);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintWidth_default, 31);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintHeight_default, 32);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintWidth_min, 33);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintWidth_max, 34);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintWidth_percent, 35);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintHeight_min, 36);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintHeight_max, 37);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintHeight_percent, 38);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintLeft_creator, 39);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintTop_creator, 40);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintRight_creator, 41);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintBottom_creator, 42);
                Table.map.append(R.styleable.ConstraintLayout_Layout_layout_constraintBaseline_creator, 43);
            }
        }
    }
}
