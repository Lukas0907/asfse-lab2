// 
// Decompiled by Procyon v0.5.36
// 

package androidx.lifecycle;

import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

class ClassesInfoCache
{
    private static final int CALL_TYPE_NO_ARG = 0;
    private static final int CALL_TYPE_PROVIDER = 1;
    private static final int CALL_TYPE_PROVIDER_WITH_EVENT = 2;
    static ClassesInfoCache sInstance;
    private final Map<Class, CallbackInfo> mCallbackMap;
    private final Map<Class, Boolean> mHasLifecycleMethods;
    
    static {
        ClassesInfoCache.sInstance = new ClassesInfoCache();
    }
    
    ClassesInfoCache() {
        this.mCallbackMap = new HashMap<Class, CallbackInfo>();
        this.mHasLifecycleMethods = new HashMap<Class, Boolean>();
    }
    
    private CallbackInfo createInfo(final Class clazz, final Method[] array) {
        throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge Z and I\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
    }
    
    private Method[] getDeclaredMethods(final Class clazz) {
        try {
            return clazz.getDeclaredMethods();
        }
        catch (NoClassDefFoundError cause) {
            throw new IllegalArgumentException("The observer class has some methods that use newer APIs which are not available in the current OS version. Lifecycles cannot access even other methods so you should make sure that your observer classes only access framework classes that are available in your min API level OR use lifecycle:compiler annotation processor.", cause);
        }
    }
    
    private void verifyAndPutHandler(final Map<MethodReference, Lifecycle.Event> map, final MethodReference methodReference, final Lifecycle.Event obj, final Class clazz) {
        final Lifecycle.Event obj2 = map.get(methodReference);
        if (obj2 != null && obj != obj2) {
            final Method mMethod = methodReference.mMethod;
            final StringBuilder sb = new StringBuilder();
            sb.append("Method ");
            sb.append(mMethod.getName());
            sb.append(" in ");
            sb.append(clazz.getName());
            sb.append(" already declared with different @OnLifecycleEvent value: previous");
            sb.append(" value ");
            sb.append(obj2);
            sb.append(", new value ");
            sb.append(obj);
            throw new IllegalArgumentException(sb.toString());
        }
        if (obj2 == null) {
            map.put(methodReference, obj);
        }
    }
    
    CallbackInfo getInfo(final Class clazz) {
        final CallbackInfo callbackInfo = this.mCallbackMap.get(clazz);
        if (callbackInfo != null) {
            return callbackInfo;
        }
        return this.createInfo(clazz, null);
    }
    
    boolean hasLifecycleMethods(final Class clazz) {
        if (this.mHasLifecycleMethods.containsKey(clazz)) {
            return this.mHasLifecycleMethods.get(clazz);
        }
        final Method[] declaredMethods = this.getDeclaredMethods(clazz);
        for (int length = declaredMethods.length, i = 0; i < length; ++i) {
            if (declaredMethods[i].getAnnotation(OnLifecycleEvent.class) != null) {
                this.createInfo(clazz, declaredMethods);
                return true;
            }
        }
        this.mHasLifecycleMethods.put(clazz, false);
        return false;
    }
    
    static class CallbackInfo
    {
        final Map<Lifecycle.Event, List<MethodReference>> mEventToHandlers;
        final Map<MethodReference, Lifecycle.Event> mHandlerToEvent;
        
        CallbackInfo(final Map<MethodReference, Lifecycle.Event> mHandlerToEvent) {
            this.mHandlerToEvent = mHandlerToEvent;
            this.mEventToHandlers = new HashMap<Lifecycle.Event, List<MethodReference>>();
            for (final Map.Entry<MethodReference, Lifecycle.Event> entry : mHandlerToEvent.entrySet()) {
                final Lifecycle.Event event = entry.getValue();
                List<MethodReference> list;
                if ((list = this.mEventToHandlers.get(event)) == null) {
                    list = new ArrayList<MethodReference>();
                    this.mEventToHandlers.put(event, list);
                }
                list.add(entry.getKey());
            }
        }
        
        private static void invokeMethodsForEvent(final List<MethodReference> list, final LifecycleOwner lifecycleOwner, final Lifecycle.Event event, final Object o) {
            if (list != null) {
                for (int i = list.size() - 1; i >= 0; --i) {
                    list.get(i).invokeCallback(lifecycleOwner, event, o);
                }
            }
        }
        
        void invokeCallbacks(final LifecycleOwner lifecycleOwner, final Lifecycle.Event event, final Object o) {
            invokeMethodsForEvent(this.mEventToHandlers.get(event), lifecycleOwner, event, o);
            invokeMethodsForEvent(this.mEventToHandlers.get(Lifecycle.Event.ON_ANY), lifecycleOwner, event, o);
        }
    }
    
    static class MethodReference
    {
        final int mCallType;
        final Method mMethod;
        
        MethodReference(final int mCallType, final Method mMethod) {
            this.mCallType = mCallType;
            (this.mMethod = mMethod).setAccessible(true);
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null) {
                return false;
            }
            if (this.getClass() != o.getClass()) {
                return false;
            }
            final MethodReference methodReference = (MethodReference)o;
            return this.mCallType == methodReference.mCallType && this.mMethod.getName().equals(methodReference.mMethod.getName());
        }
        
        @Override
        public int hashCode() {
            return this.mCallType * 31 + this.mMethod.getName().hashCode();
        }
        
        void invokeCallback(final LifecycleOwner lifecycleOwner, final Lifecycle.Event event, final Object obj) {
            try {
                final int mCallType = this.mCallType;
                if (mCallType == 0) {
                    this.mMethod.invoke(obj, new Object[0]);
                    return;
                }
                if (mCallType == 1) {
                    this.mMethod.invoke(obj, lifecycleOwner);
                    return;
                }
                if (mCallType != 2) {
                    return;
                }
                this.mMethod.invoke(obj, lifecycleOwner, event);
            }
            catch (IllegalAccessException cause) {
                throw new RuntimeException(cause);
            }
            catch (InvocationTargetException ex) {
                throw new RuntimeException("Failed to call observer method", ex.getCause());
            }
        }
    }
}
