// 
// Decompiled by Procyon v0.5.36
// 

package androidx.recyclerview.widget;

import java.util.List;

public abstract class ListAdapter<T, VH extends ViewHolder> extends Adapter<VH>
{
    private final AsyncListDiffer<T> mHelper;
    
    protected ListAdapter(final AsyncDifferConfig<T> asyncDifferConfig) {
        this.mHelper = new AsyncListDiffer<T>(new AdapterListUpdateCallback(this), asyncDifferConfig);
    }
    
    protected ListAdapter(final DiffUtil.ItemCallback<T> itemCallback) {
        this.mHelper = new AsyncListDiffer<T>(new AdapterListUpdateCallback(this), new AsyncDifferConfig.Builder<T>(itemCallback).build());
    }
    
    protected T getItem(final int n) {
        return this.mHelper.getCurrentList().get(n);
    }
    
    @Override
    public int getItemCount() {
        return this.mHelper.getCurrentList().size();
    }
    
    public void submitList(final List<T> list) {
        this.mHelper.submitList(list);
    }
}
