// 
// Decompiled by Procyon v0.5.36
// 

package androidx.versionedparcelable;

import java.io.ByteArrayOutputStream;
import android.os.IInterface;
import java.util.Iterator;
import java.util.Set;
import android.os.IBinder;
import android.os.Parcelable;
import android.os.Bundle;
import java.io.IOException;
import java.io.FilterInputStream;
import java.lang.reflect.Method;
import androidx.collection.ArrayMap;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.nio.charset.Charset;

class VersionedParcelStream extends VersionedParcel
{
    private static final int TYPE_BOOLEAN = 5;
    private static final int TYPE_BOOLEAN_ARRAY = 6;
    private static final int TYPE_DOUBLE = 7;
    private static final int TYPE_DOUBLE_ARRAY = 8;
    private static final int TYPE_FLOAT = 13;
    private static final int TYPE_FLOAT_ARRAY = 14;
    private static final int TYPE_INT = 9;
    private static final int TYPE_INT_ARRAY = 10;
    private static final int TYPE_LONG = 11;
    private static final int TYPE_LONG_ARRAY = 12;
    private static final int TYPE_NULL = 0;
    private static final int TYPE_STRING = 3;
    private static final int TYPE_STRING_ARRAY = 4;
    private static final int TYPE_SUB_BUNDLE = 1;
    private static final int TYPE_SUB_PERSISTABLE_BUNDLE = 2;
    private static final Charset UTF_16;
    int mCount;
    private DataInputStream mCurrentInput;
    private DataOutputStream mCurrentOutput;
    private FieldBuffer mFieldBuffer;
    private int mFieldId;
    int mFieldSize;
    private boolean mIgnoreParcelables;
    private final DataInputStream mMasterInput;
    private final DataOutputStream mMasterOutput;
    
    static {
        UTF_16 = Charset.forName("UTF-16");
    }
    
    public VersionedParcelStream(final InputStream inputStream, final OutputStream outputStream) {
        this(inputStream, outputStream, new ArrayMap<String, Method>(), new ArrayMap<String, Method>(), new ArrayMap<String, Class>());
    }
    
    private VersionedParcelStream(final InputStream inputStream, final OutputStream out, final ArrayMap<String, Method> arrayMap, final ArrayMap<String, Method> arrayMap2, final ArrayMap<String, Class> arrayMap3) {
        super(arrayMap, arrayMap2, arrayMap3);
        this.mCount = 0;
        this.mFieldId = -1;
        this.mFieldSize = -1;
        final DataOutputStream dataOutputStream = null;
        DataInputStream mMasterInput;
        if (inputStream != null) {
            mMasterInput = new DataInputStream(new FilterInputStream(inputStream) {
                @Override
                public int read() throws IOException {
                    if (VersionedParcelStream.this.mFieldSize != -1 && VersionedParcelStream.this.mCount >= VersionedParcelStream.this.mFieldSize) {
                        throw new IOException();
                    }
                    final int read = super.read();
                    final VersionedParcelStream this$0 = VersionedParcelStream.this;
                    ++this$0.mCount;
                    return read;
                }
                
                @Override
                public int read(final byte[] b, int read, final int len) throws IOException {
                    if (VersionedParcelStream.this.mFieldSize != -1 && VersionedParcelStream.this.mCount >= VersionedParcelStream.this.mFieldSize) {
                        throw new IOException();
                    }
                    read = super.read(b, read, len);
                    if (read > 0) {
                        final VersionedParcelStream this$0 = VersionedParcelStream.this;
                        this$0.mCount += read;
                    }
                    return read;
                }
                
                @Override
                public long skip(long skip) throws IOException {
                    if (VersionedParcelStream.this.mFieldSize != -1 && VersionedParcelStream.this.mCount >= VersionedParcelStream.this.mFieldSize) {
                        throw new IOException();
                    }
                    skip = super.skip(skip);
                    if (skip > 0L) {
                        final VersionedParcelStream this$0 = VersionedParcelStream.this;
                        this$0.mCount += (int)skip;
                    }
                    return skip;
                }
            });
        }
        else {
            mMasterInput = null;
        }
        this.mMasterInput = mMasterInput;
        DataOutputStream mMasterOutput = dataOutputStream;
        if (out != null) {
            mMasterOutput = new DataOutputStream(out);
        }
        this.mMasterOutput = mMasterOutput;
        this.mCurrentInput = this.mMasterInput;
        this.mCurrentOutput = this.mMasterOutput;
    }
    
    private void readObject(final int i, final String s, final Bundle bundle) {
        switch (i) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown type ");
                sb.append(i);
                throw new RuntimeException(sb.toString());
            }
            case 14: {
                bundle.putFloatArray(s, this.readFloatArray());
            }
            case 13: {
                bundle.putFloat(s, this.readFloat());
            }
            case 12: {
                bundle.putLongArray(s, this.readLongArray());
            }
            case 11: {
                bundle.putLong(s, this.readLong());
            }
            case 10: {
                bundle.putIntArray(s, this.readIntArray());
            }
            case 9: {
                bundle.putInt(s, this.readInt());
            }
            case 8: {
                bundle.putDoubleArray(s, this.readDoubleArray());
            }
            case 7: {
                bundle.putDouble(s, this.readDouble());
            }
            case 6: {
                bundle.putBooleanArray(s, this.readBooleanArray());
            }
            case 5: {
                bundle.putBoolean(s, this.readBoolean());
            }
            case 4: {
                bundle.putStringArray(s, (String[])this.readArray(new String[0]));
            }
            case 3: {
                bundle.putString(s, this.readString());
            }
            case 2: {
                bundle.putBundle(s, this.readBundle());
            }
            case 1: {
                bundle.putBundle(s, this.readBundle());
            }
            case 0: {
                bundle.putParcelable(s, (Parcelable)null);
            }
        }
    }
    
    private void writeObject(final Object o) {
        if (o == null) {
            this.writeInt(0);
            return;
        }
        if (o instanceof Bundle) {
            this.writeInt(1);
            this.writeBundle((Bundle)o);
            return;
        }
        if (o instanceof String) {
            this.writeInt(3);
            this.writeString((String)o);
            return;
        }
        if (o instanceof String[]) {
            this.writeInt(4);
            this.writeArray((String[])o);
            return;
        }
        if (o instanceof Boolean) {
            this.writeInt(5);
            this.writeBoolean((boolean)o);
            return;
        }
        if (o instanceof boolean[]) {
            this.writeInt(6);
            this.writeBooleanArray((boolean[])o);
            return;
        }
        if (o instanceof Double) {
            this.writeInt(7);
            this.writeDouble((double)o);
            return;
        }
        if (o instanceof double[]) {
            this.writeInt(8);
            this.writeDoubleArray((double[])o);
            return;
        }
        if (o instanceof Integer) {
            this.writeInt(9);
            this.writeInt((int)o);
            return;
        }
        if (o instanceof int[]) {
            this.writeInt(10);
            this.writeIntArray((int[])o);
            return;
        }
        if (o instanceof Long) {
            this.writeInt(11);
            this.writeLong((long)o);
            return;
        }
        if (o instanceof long[]) {
            this.writeInt(12);
            this.writeLongArray((long[])o);
            return;
        }
        if (o instanceof Float) {
            this.writeInt(13);
            this.writeFloat((float)o);
            return;
        }
        if (o instanceof float[]) {
            this.writeInt(14);
            this.writeFloatArray((float[])o);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unsupported type ");
        sb.append(o.getClass());
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void closeField() {
        final FieldBuffer mFieldBuffer = this.mFieldBuffer;
        if (mFieldBuffer != null) {
            try {
                if (mFieldBuffer.mOutput.size() != 0) {
                    this.mFieldBuffer.flushField();
                }
                this.mFieldBuffer = null;
            }
            catch (IOException ex) {
                throw new ParcelException(ex);
            }
        }
    }
    
    @Override
    protected VersionedParcel createSubParcel() {
        return new VersionedParcelStream(this.mCurrentInput, this.mCurrentOutput, this.mReadCache, this.mWriteCache, this.mParcelizerCache);
    }
    
    @Override
    public boolean isStream() {
        return true;
    }
    
    public boolean readBoolean() {
        try {
            return this.mCurrentInput.readBoolean();
        }
        catch (IOException ex) {
            throw new ParcelException(ex);
        }
    }
    
    public Bundle readBundle() {
        final int int1 = this.readInt();
        if (int1 < 0) {
            return null;
        }
        final Bundle bundle = new Bundle();
        for (int i = 0; i < int1; ++i) {
            this.readObject(this.readInt(), this.readString(), bundle);
        }
        return bundle;
    }
    
    public byte[] readByteArray() {
        try {
            final int int1 = this.mCurrentInput.readInt();
            if (int1 > 0) {
                final byte[] b = new byte[int1];
                this.mCurrentInput.readFully(b);
                return b;
            }
            return null;
        }
        catch (IOException ex) {
            throw new ParcelException(ex);
        }
    }
    
    @Override
    protected CharSequence readCharSequence() {
        return null;
    }
    
    public double readDouble() {
        try {
            return this.mCurrentInput.readDouble();
        }
        catch (IOException ex) {
            throw new ParcelException(ex);
        }
    }
    
    public boolean readField(final int i) {
        try {
            while (this.mFieldId != i) {
                if (String.valueOf(this.mFieldId).compareTo(String.valueOf(i)) > 0) {
                    return false;
                }
                if (this.mCount < this.mFieldSize) {
                    this.mMasterInput.skip(this.mFieldSize - this.mCount);
                }
                this.mFieldSize = -1;
                final int int1 = this.mMasterInput.readInt();
                this.mCount = 0;
                int int2;
                if ((int2 = (int1 & 0xFFFF)) == 65535) {
                    int2 = this.mMasterInput.readInt();
                }
                this.mFieldId = (int1 >> 16 & 0xFFFF);
                this.mFieldSize = int2;
            }
            return true;
        }
        catch (IOException ex) {
            return false;
        }
    }
    
    public float readFloat() {
        try {
            return this.mCurrentInput.readFloat();
        }
        catch (IOException ex) {
            throw new ParcelException(ex);
        }
    }
    
    public int readInt() {
        try {
            return this.mCurrentInput.readInt();
        }
        catch (IOException ex) {
            throw new ParcelException(ex);
        }
    }
    
    public long readLong() {
        try {
            return this.mCurrentInput.readLong();
        }
        catch (IOException ex) {
            throw new ParcelException(ex);
        }
    }
    
    public <T extends Parcelable> T readParcelable() {
        return null;
    }
    
    public String readString() {
        try {
            final int int1 = this.mCurrentInput.readInt();
            if (int1 > 0) {
                final byte[] array = new byte[int1];
                this.mCurrentInput.readFully(array);
                return new String(array, VersionedParcelStream.UTF_16);
            }
            return null;
        }
        catch (IOException ex) {
            throw new ParcelException(ex);
        }
    }
    
    public IBinder readStrongBinder() {
        return null;
    }
    
    public void setOutputField(final int n) {
        this.closeField();
        this.mFieldBuffer = new FieldBuffer(n, this.mMasterOutput);
        this.mCurrentOutput = this.mFieldBuffer.mDataStream;
    }
    
    @Override
    public void setSerializationFlags(final boolean b, final boolean mIgnoreParcelables) {
        if (b) {
            this.mIgnoreParcelables = mIgnoreParcelables;
            return;
        }
        throw new RuntimeException("Serialization of this object is not allowed");
    }
    
    public void writeBoolean(final boolean v) {
        try {
            this.mCurrentOutput.writeBoolean(v);
        }
        catch (IOException ex) {
            throw new ParcelException(ex);
        }
    }
    
    public void writeBundle(final Bundle bundle) {
        Label_0065: {
            if (bundle == null) {
                break Label_0065;
            }
            try {
                final Set keySet = bundle.keySet();
                this.mCurrentOutput.writeInt(keySet.size());
                for (final String s : keySet) {
                    this.writeString(s);
                    this.writeObject(bundle.get(s));
                }
                return;
                this.mCurrentOutput.writeInt(-1);
            }
            catch (IOException ex) {
                throw new ParcelException(ex);
            }
        }
    }
    
    public void writeByteArray(final byte[] b) {
        Label_0022: {
            if (b == null) {
                break Label_0022;
            }
            try {
                this.mCurrentOutput.writeInt(b.length);
                this.mCurrentOutput.write(b);
                return;
                this.mCurrentOutput.writeInt(-1);
            }
            catch (IOException ex) {
                throw new ParcelException(ex);
            }
        }
    }
    
    public void writeByteArray(final byte[] b, final int off, final int n) {
        Label_0023: {
            if (b == null) {
                break Label_0023;
            }
            try {
                this.mCurrentOutput.writeInt(n);
                this.mCurrentOutput.write(b, off, n);
                return;
                this.mCurrentOutput.writeInt(-1);
            }
            catch (IOException ex) {
                throw new ParcelException(ex);
            }
        }
    }
    
    @Override
    protected void writeCharSequence(final CharSequence charSequence) {
        if (this.mIgnoreParcelables) {
            return;
        }
        throw new RuntimeException("CharSequence cannot be written to an OutputStream");
    }
    
    public void writeDouble(final double v) {
        try {
            this.mCurrentOutput.writeDouble(v);
        }
        catch (IOException ex) {
            throw new ParcelException(ex);
        }
    }
    
    public void writeFloat(final float v) {
        try {
            this.mCurrentOutput.writeFloat(v);
        }
        catch (IOException ex) {
            throw new ParcelException(ex);
        }
    }
    
    public void writeInt(final int v) {
        try {
            this.mCurrentOutput.writeInt(v);
        }
        catch (IOException ex) {
            throw new ParcelException(ex);
        }
    }
    
    public void writeLong(final long v) {
        try {
            this.mCurrentOutput.writeLong(v);
        }
        catch (IOException ex) {
            throw new ParcelException(ex);
        }
    }
    
    public void writeParcelable(final Parcelable parcelable) {
        if (this.mIgnoreParcelables) {
            return;
        }
        throw new RuntimeException("Parcelables cannot be written to an OutputStream");
    }
    
    public void writeString(final String s) {
        Label_0030: {
            if (s == null) {
                break Label_0030;
            }
            try {
                final byte[] bytes = s.getBytes(VersionedParcelStream.UTF_16);
                this.mCurrentOutput.writeInt(bytes.length);
                this.mCurrentOutput.write(bytes);
                return;
                this.mCurrentOutput.writeInt(-1);
            }
            catch (IOException ex) {
                throw new ParcelException(ex);
            }
        }
    }
    
    public void writeStrongBinder(final IBinder binder) {
        if (this.mIgnoreParcelables) {
            return;
        }
        throw new RuntimeException("Binders cannot be written to an OutputStream");
    }
    
    public void writeStrongInterface(final IInterface interface1) {
        if (this.mIgnoreParcelables) {
            return;
        }
        throw new RuntimeException("Binders cannot be written to an OutputStream");
    }
    
    private static class FieldBuffer
    {
        final DataOutputStream mDataStream;
        private final int mFieldId;
        final ByteArrayOutputStream mOutput;
        private final DataOutputStream mTarget;
        
        FieldBuffer(final int mFieldId, final DataOutputStream mTarget) {
            this.mOutput = new ByteArrayOutputStream();
            this.mDataStream = new DataOutputStream(this.mOutput);
            this.mFieldId = mFieldId;
            this.mTarget = mTarget;
        }
        
        void flushField() throws IOException {
            this.mDataStream.flush();
            final int size = this.mOutput.size();
            final int mFieldId = this.mFieldId;
            int n;
            if (size >= 65535) {
                n = 65535;
            }
            else {
                n = size;
            }
            this.mTarget.writeInt(mFieldId << 16 | n);
            if (size >= 65535) {
                this.mTarget.writeInt(size);
            }
            this.mOutput.writeTo(this.mTarget);
        }
    }
}
