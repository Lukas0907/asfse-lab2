// 
// Decompiled by Procyon v0.5.36
// 

package androidx.cardview;

public final class R
{
    private R() {
    }
    
    public static final class attr
    {
        public static final int cardBackgroundColor = 2130903130;
        public static final int cardCornerRadius = 2130903131;
        public static final int cardElevation = 2130903132;
        public static final int cardMaxElevation = 2130903133;
        public static final int cardPreventCornerOverlap = 2130903134;
        public static final int cardUseCompatPadding = 2130903135;
        public static final int cardViewStyle = 2130903136;
        public static final int contentPadding = 2130903194;
        public static final int contentPaddingBottom = 2130903195;
        public static final int contentPaddingLeft = 2130903196;
        public static final int contentPaddingRight = 2130903197;
        public static final int contentPaddingTop = 2130903198;
        
        private attr() {
        }
    }
    
    public static final class color
    {
        public static final int cardview_dark_background = 2131034151;
        public static final int cardview_light_background = 2131034152;
        public static final int cardview_shadow_end_color = 2131034153;
        public static final int cardview_shadow_start_color = 2131034154;
        
        private color() {
        }
    }
    
    public static final class dimen
    {
        public static final int cardview_compat_inset_shadow = 2131099723;
        public static final int cardview_default_elevation = 2131099724;
        public static final int cardview_default_radius = 2131099725;
        
        private dimen() {
        }
    }
    
    public static final class style
    {
        public static final int Base_CardView = 2131689487;
        public static final int CardView = 2131689664;
        public static final int CardView_Dark = 2131689665;
        public static final int CardView_Light = 2131689666;
        
        private style() {
        }
    }
    
    public static final class styleable
    {
        public static final int[] CardView;
        public static final int CardView_android_minHeight = 1;
        public static final int CardView_android_minWidth = 0;
        public static final int CardView_cardBackgroundColor = 2;
        public static final int CardView_cardCornerRadius = 3;
        public static final int CardView_cardElevation = 4;
        public static final int CardView_cardMaxElevation = 5;
        public static final int CardView_cardPreventCornerOverlap = 6;
        public static final int CardView_cardUseCompatPadding = 7;
        public static final int CardView_contentPadding = 8;
        public static final int CardView_contentPaddingBottom = 9;
        public static final int CardView_contentPaddingLeft = 10;
        public static final int CardView_contentPaddingRight = 11;
        public static final int CardView_contentPaddingTop = 12;
        
        static {
            CardView = new int[] { 16843071, 16843072, 2130903130, 2130903131, 2130903132, 2130903133, 2130903134, 2130903135, 2130903194, 2130903195, 2130903196, 2130903197, 2130903198 };
        }
        
        private styleable() {
        }
    }
}
