// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.i18n.filter;

public class SQLFilter implements Filter
{
    @Override
    public String doFilter(String s) {
        final StringBuffer sb = new StringBuffer(s);
        int end;
        for (int i = 0; i < sb.length(); i = end + 1) {
            final char char1 = sb.charAt(i);
            if (char1 != '\n') {
                if (char1 != '\r') {
                    if (char1 != '\"') {
                        if (char1 != '\'') {
                            if (char1 != '-') {
                                if (char1 != '/') {
                                    if (char1 != ';') {
                                        if (char1 != '=') {
                                            if (char1 != '\\') {
                                                end = i;
                                                continue;
                                            }
                                            end = i + 1;
                                            s = "\\\\";
                                        }
                                        else {
                                            end = i + 1;
                                            s = "\\=";
                                        }
                                    }
                                    else {
                                        end = i + 1;
                                        s = "\\;";
                                    }
                                }
                                else {
                                    end = i + 1;
                                    s = "\\/";
                                }
                            }
                            else {
                                end = i + 1;
                                s = "\\-";
                            }
                        }
                        else {
                            end = i + 1;
                            s = "\\'";
                        }
                    }
                    else {
                        end = i + 1;
                        s = "\\\"";
                    }
                }
                else {
                    end = i + 1;
                    s = "\\r";
                }
            }
            else {
                end = i + 1;
                s = "\\n";
            }
            sb.replace(i, end, s);
        }
        return sb.toString();
    }
    
    @Override
    public String doFilterUrl(final String s) {
        return this.doFilter(s);
    }
}
