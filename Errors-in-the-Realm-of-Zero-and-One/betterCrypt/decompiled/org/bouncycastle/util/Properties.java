// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util;

import java.util.HashMap;
import java.security.AccessControlException;
import java.security.AccessController;
import java.util.Map;
import java.security.PrivilegedAction;
import java.util.Collections;
import java.util.StringTokenizer;
import java.util.HashSet;
import java.util.Set;
import java.math.BigInteger;

public class Properties
{
    private static final ThreadLocal threadProperties;
    
    static {
        threadProperties = new ThreadLocal();
    }
    
    private Properties() {
    }
    
    public static BigInteger asBigInteger(String propertyValue) {
        propertyValue = getPropertyValue(propertyValue);
        if (propertyValue != null) {
            return new BigInteger(propertyValue);
        }
        return null;
    }
    
    public static Set<String> asKeySet(String propertyValue) {
        final HashSet<String> s = new HashSet<String>();
        propertyValue = getPropertyValue(propertyValue);
        if (propertyValue != null) {
            final StringTokenizer stringTokenizer = new StringTokenizer(propertyValue, ",");
            while (stringTokenizer.hasMoreElements()) {
                s.add(Strings.toLowerCase(stringTokenizer.nextToken()).trim());
            }
        }
        return (Set<String>)Collections.unmodifiableSet((Set<?>)s);
    }
    
    public static String getPropertyValue(final String s) {
        return AccessController.doPrivileged((PrivilegedAction<String>)new PrivilegedAction() {
            @Override
            public Object run() {
                final Map<K, Object> map = Properties.threadProperties.get();
                if (map != null) {
                    return map.get(s);
                }
                return System.getProperty(s);
            }
        });
    }
    
    public static boolean isOverrideSet(String propertyValue) {
        try {
            propertyValue = getPropertyValue(propertyValue);
            return propertyValue != null && "true".equals(Strings.toLowerCase(propertyValue));
        }
        catch (AccessControlException ex) {
            return false;
        }
    }
    
    public static boolean removeThreadOverride(final String s) {
        final boolean overrideSet = isOverrideSet(s);
        final Map value = Properties.threadProperties.get();
        if (value == null) {
            return false;
        }
        value.remove(s);
        if (value.isEmpty()) {
            Properties.threadProperties.remove();
            return overrideSet;
        }
        Properties.threadProperties.set(value);
        return overrideSet;
    }
    
    public static boolean setThreadOverride(final String s, final boolean b) {
        final boolean overrideSet = isOverrideSet(s);
        Map<String, String> value;
        if ((value = Properties.threadProperties.get()) == null) {
            value = new HashMap<String, String>();
        }
        String s2;
        if (b) {
            s2 = "true";
        }
        else {
            s2 = "false";
        }
        value.put(s, s2);
        Properties.threadProperties.set(value);
        return overrideSet;
    }
}
