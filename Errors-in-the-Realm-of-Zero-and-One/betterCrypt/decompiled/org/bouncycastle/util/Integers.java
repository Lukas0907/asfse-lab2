// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util;

public class Integers
{
    public static int numberOfLeadingZeros(final int i) {
        return Integer.numberOfLeadingZeros(i);
    }
    
    public static int rotateLeft(final int i, final int distance) {
        return Integer.rotateLeft(i, distance);
    }
    
    public static int rotateRight(final int i, final int distance) {
        return Integer.rotateRight(i, distance);
    }
    
    public static Integer valueOf(final int i) {
        return i;
    }
}
