// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util;

import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Vector;
import org.bouncycastle.util.encoders.UTF8;

public final class Strings
{
    private static String LINE_SEPARATOR;
    
    static {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   org/bouncycastle/util/Strings$1.<init>:()V
        //     7: invokestatic    java/security/AccessController.doPrivileged:(Ljava/security/PrivilegedAction;)Ljava/lang/Object;
        //    10: checkcast       Ljava/lang/String;
        //    13: putstatic       org/bouncycastle/util/Strings.LINE_SEPARATOR:Ljava/lang/String;
        //    16: return         
        //    17: ldc             "%n"
        //    19: iconst_0       
        //    20: anewarray       Ljava/lang/Object;
        //    23: invokestatic    java/lang/String.format:(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //    26: putstatic       org/bouncycastle/util/Strings.LINE_SEPARATOR:Ljava/lang/String;
        //    29: return         
        //    30: ldc             "\n"
        //    32: putstatic       org/bouncycastle/util/Strings.LINE_SEPARATOR:Ljava/lang/String;
        //    35: return         
        //    36: astore_0       
        //    37: goto            17
        //    40: astore_0       
        //    41: goto            30
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  0      16     36     44     Ljava/lang/Exception;
        //  17     29     40     36     Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0017:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static char[] asCharArray(final byte[] array) {
        final char[] array2 = new char[array.length];
        for (int i = 0; i != array2.length; ++i) {
            array2[i] = (char)(array[i] & 0xFF);
        }
        return array2;
    }
    
    public static String fromByteArray(final byte[] array) {
        return new String(asCharArray(array));
    }
    
    public static String fromUTF8ByteArray(final byte[] array) {
        final char[] value = new char[array.length];
        final int transcodeToUTF16 = UTF8.transcodeToUTF16(array, value);
        if (transcodeToUTF16 >= 0) {
            return new String(value, 0, transcodeToUTF16);
        }
        throw new IllegalArgumentException("Invalid UTF-8 input");
    }
    
    public static String lineSeparator() {
        return Strings.LINE_SEPARATOR;
    }
    
    public static StringList newList() {
        return new StringListImpl();
    }
    
    public static String[] split(String substring, final char ch) {
        final Vector<String> vector = new Vector<String>();
        int n = 1;
        int n2;
        while (true) {
            n2 = 0;
            if (n == 0) {
                break;
            }
            final int index = substring.indexOf(ch);
            if (index > 0) {
                vector.addElement(substring.substring(0, index));
                substring = substring.substring(index + 1);
            }
            else {
                vector.addElement(substring);
                n = 0;
            }
        }
        final String[] array = new String[vector.size()];
        for (int i = n2; i != array.length; ++i) {
            array[i] = vector.elementAt(i);
        }
        return array;
    }
    
    public static int toByteArray(final String s, final byte[] array, final int n) {
        final int length = s.length();
        for (int i = 0; i < length; ++i) {
            array[n + i] = (byte)s.charAt(i);
        }
        return length;
    }
    
    public static byte[] toByteArray(final String s) {
        final byte[] array = new byte[s.length()];
        for (int i = 0; i != array.length; ++i) {
            array[i] = (byte)s.charAt(i);
        }
        return array;
    }
    
    public static byte[] toByteArray(final char[] array) {
        final byte[] array2 = new byte[array.length];
        for (int i = 0; i != array2.length; ++i) {
            array2[i] = (byte)array[i];
        }
        return array2;
    }
    
    public static String toLowerCase(String s) {
        final char[] charArray = s.toCharArray();
        int i = 0;
        int n = 0;
        while (i != charArray.length) {
            final char c = charArray[i];
            int n2 = n;
            if ('A' <= c) {
                n2 = n;
                if ('Z' >= c) {
                    charArray[i] = (char)(c - 'A' + 97);
                    n2 = 1;
                }
            }
            ++i;
            n = n2;
        }
        if (n != 0) {
            s = new String(charArray);
        }
        return s;
    }
    
    public static void toUTF8ByteArray(final char[] array, final OutputStream outputStream) throws IOException {
        for (int i = 0; i < array.length; ++i) {
            int n = array[i];
            if (n >= 128) {
                Label_0142: {
                    int n2;
                    if (n < 2048) {
                        n2 = (n >> 6 | 0xC0);
                    }
                    else if (n >= 55296 && n <= 57343) {
                        ++i;
                        if (i >= array.length) {
                            throw new IllegalStateException("invalid UTF-16 codepoint");
                        }
                        final char c = array[i];
                        if (n <= 56319) {
                            n = ((n & 0x3FF) << 10 | (c & '\u03ff')) + 65536;
                            outputStream.write(n >> 18 | 0xF0);
                            outputStream.write((n >> 12 & 0x3F) | 0x80);
                            outputStream.write((n >> 6 & 0x3F) | 0x80);
                            break Label_0142;
                        }
                        throw new IllegalStateException("invalid UTF-16 codepoint");
                    }
                    else {
                        outputStream.write(n >> 12 | 0xE0);
                        n2 = ((n >> 6 & 0x3F) | 0x80);
                    }
                    outputStream.write(n2);
                }
                n = ((n & 0x3F) | 0x80);
            }
            outputStream.write(n);
        }
    }
    
    public static byte[] toUTF8ByteArray(final String s) {
        return toUTF8ByteArray(s.toCharArray());
    }
    
    public static byte[] toUTF8ByteArray(final char[] array) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            toUTF8ByteArray(array, byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        }
        catch (IOException ex) {
            throw new IllegalStateException("cannot encode string to byte array!");
        }
    }
    
    public static String toUpperCase(String s) {
        final char[] charArray = s.toCharArray();
        int i = 0;
        int n = 0;
        while (i != charArray.length) {
            final char c = charArray[i];
            int n2 = n;
            if ('a' <= c) {
                n2 = n;
                if ('z' >= c) {
                    charArray[i] = (char)(c - 'a' + 65);
                    n2 = 1;
                }
            }
            ++i;
            n = n2;
        }
        if (n != 0) {
            s = new String(charArray);
        }
        return s;
    }
    
    private static class StringListImpl extends ArrayList<String> implements StringList
    {
        @Override
        public void add(final int index, final String element) {
            super.add(index, element);
        }
        
        @Override
        public boolean add(final String e) {
            return super.add(e);
        }
        
        @Override
        public String set(final int index, final String element) {
            return super.set(index, element);
        }
        
        @Override
        public String[] toStringArray() {
            final String[] array = new String[this.size()];
            for (int i = 0; i != array.length; ++i) {
                array[i] = this.get(i);
            }
            return array;
        }
        
        @Override
        public String[] toStringArray(final int n, final int n2) {
            final String[] array = new String[n2 - n];
            for (int index = n; index != this.size() && index != n2; ++index) {
                array[index - n] = this.get(index);
            }
            return array;
        }
    }
}
