// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util;

import org.bouncycastle.crypto.digests.SHA512tDigest;
import org.bouncycastle.crypto.digests.SHAKEDigest;

public class Fingerprint
{
    private static char[] encodingTable;
    private final byte[] fingerprint;
    
    static {
        Fingerprint.encodingTable = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    }
    
    public Fingerprint(final byte[] array) {
        this(array, 160);
    }
    
    public Fingerprint(final byte[] array, final int n) {
        this.fingerprint = calculateFingerprint(array, n);
    }
    
    public Fingerprint(final byte[] array, final boolean b) {
        if (b) {
            this.fingerprint = calculateFingerprintSHA512_160(array);
            return;
        }
        this.fingerprint = calculateFingerprint(array);
    }
    
    public static byte[] calculateFingerprint(final byte[] array) {
        return calculateFingerprint(array, 160);
    }
    
    public static byte[] calculateFingerprint(byte[] array, int n) {
        if (n % 8 == 0) {
            final SHAKEDigest shakeDigest = new SHAKEDigest(256);
            shakeDigest.update(array, 0, array.length);
            n /= 8;
            array = new byte[n];
            shakeDigest.doFinal(array, 0, n);
            return array;
        }
        throw new IllegalArgumentException("bitLength must be a multiple of 8");
    }
    
    public static byte[] calculateFingerprintSHA512_160(byte[] array) {
        final SHA512tDigest sha512tDigest = new SHA512tDigest(160);
        sha512tDigest.update(array, 0, array.length);
        array = new byte[sha512tDigest.getDigestSize()];
        sha512tDigest.doFinal(array, 0);
        return array;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof Fingerprint && Arrays.areEqual(((Fingerprint)o).fingerprint, this.fingerprint));
    }
    
    public byte[] getFingerprint() {
        return Arrays.clone(this.fingerprint);
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.fingerprint);
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        for (int i = 0; i != this.fingerprint.length; ++i) {
            if (i > 0) {
                sb.append(":");
            }
            sb.append(Fingerprint.encodingTable[this.fingerprint[i] >>> 4 & 0xF]);
            sb.append(Fingerprint.encodingTable[this.fingerprint[i] & 0xF]);
        }
        return sb.toString();
    }
}
