// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util;

public class IPAddress
{
    private static boolean isMaskValue(final String s, final int n) {
        final boolean b = false;
        try {
            final int int1 = Integer.parseInt(s);
            boolean b2 = b;
            if (int1 >= 0) {
                b2 = b;
                if (int1 <= n) {
                    b2 = true;
                }
            }
            return b2;
        }
        catch (NumberFormatException ex) {
            return false;
        }
    }
    
    public static boolean isValid(final String s) {
        return isValidIPv4(s) || isValidIPv6(s);
    }
    
    public static boolean isValidIPv4(String string) {
        final int length = string.length();
        boolean b = false;
        if (length == 0) {
            return false;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(string);
        sb.append(".");
        string = sb.toString();
        int n2;
        int n = n2 = 0;
        while (true) {
            Label_0112: {
                if (n >= string.length()) {
                    break Label_0112;
                }
                final int index = string.indexOf(46, n);
                if (index <= n) {
                    break Label_0112;
                }
                if (n2 == 4) {
                    break;
                }
                try {
                    final int int1 = Integer.parseInt(string.substring(n, index));
                    if (int1 < 0) {
                        return false;
                    }
                    if (int1 > 255) {
                        return false;
                    }
                    n = index + 1;
                    ++n2;
                    continue;
                    b = true;
                    Label_0120: {
                        return b;
                    }
                    // iftrue(Label_0120:, n2 != 4)
                    return true;
                }
                catch (NumberFormatException ex) {
                    return false;
                }
            }
        }
        return false;
    }
    
    public static boolean isValidIPv4WithNetmask(final String s) {
        final int index = s.indexOf("/");
        final String substring = s.substring(index + 1);
        boolean b2;
        final boolean b = b2 = false;
        if (index > 0) {
            b2 = b;
            if (isValidIPv4(s.substring(0, index))) {
                if (!isValidIPv4(substring)) {
                    b2 = b;
                    if (!isMaskValue(substring, 32)) {
                        return b2;
                    }
                }
                b2 = true;
            }
        }
        return b2;
    }
    
    public static boolean isValidIPv6(String string) {
        final int length = string.length();
        boolean b = false;
        if (length == 0) {
            return false;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(string);
        sb.append(":");
        string = sb.toString();
        int beginIndex = 0;
        int n2;
        int n = n2 = beginIndex;
    Label_0184_Outer:
        while (true) {
            Label_0196: {
                if (beginIndex >= string.length()) {
                    break Label_0196;
                }
                final int index = string.indexOf(58, beginIndex);
                if (index < beginIndex) {
                    break Label_0196;
                }
                if (n == 8) {
                    return false;
                }
                Label_0159: {
                    if (beginIndex == index) {
                        break Label_0159;
                    }
                    final String substring = string.substring(beginIndex, index);
                    while (true) {
                        if (index == string.length() - 1 && substring.indexOf(46) > 0) {
                            if (!isValidIPv4(substring)) {
                                return false;
                            }
                            ++n;
                            break Label_0184;
                        }
                        try {
                            final int int1 = Integer.parseInt(string.substring(beginIndex, index), 16);
                            if (int1 < 0 || int1 > 65535) {
                                return false;
                            }
                            beginIndex = index + 1;
                            ++n;
                            continue Label_0184_Outer;
                            // iftrue(Label_0209:, n != 8 && n2 == 0)
                            return true;
                            Label_0209: {
                                return b;
                            }
                            Label_0182:
                            n2 = 1;
                            continue;
                            // iftrue(Label_0182:, index == 1 || index == string.length() - 1 || n2 == 0)
                            return false;
                            b = true;
                            return b;
                        }
                        catch (NumberFormatException ex) {
                            return false;
                        }
                        break;
                    }
                }
            }
        }
    }
    
    public static boolean isValidIPv6WithNetmask(final String s) {
        final int index = s.indexOf("/");
        final String substring = s.substring(index + 1);
        boolean b2;
        final boolean b = b2 = false;
        if (index > 0) {
            b2 = b;
            if (isValidIPv6(s.substring(0, index))) {
                if (!isValidIPv6(substring)) {
                    b2 = b;
                    if (!isMaskValue(substring, 128)) {
                        return b2;
                    }
                }
                b2 = true;
            }
        }
        return b2;
    }
    
    public static boolean isValidWithNetMask(final String s) {
        return isValidIPv4WithNetmask(s) || isValidIPv6WithNetmask(s);
    }
}
