// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.Collection;

public class CollectionStore<T> implements Store<T>, Iterable<T>
{
    private Collection<T> _local;
    
    public CollectionStore(final Collection<T> c) {
        this._local = new ArrayList<T>((Collection<? extends T>)c);
    }
    
    @Override
    public Collection<T> getMatches(final Selector<T> selector) {
        if (selector == null) {
            return new ArrayList<T>((Collection<? extends T>)this._local);
        }
        final ArrayList<T> list = new ArrayList<T>();
        for (final T next : this._local) {
            if (selector.match(next)) {
                list.add(next);
            }
        }
        return list;
    }
    
    @Override
    public Iterator<T> iterator() {
        return this.getMatches(null).iterator();
    }
}
