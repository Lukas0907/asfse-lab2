// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util;

import java.security.SecureRandom;
import java.math.BigInteger;

public final class BigIntegers
{
    private static final int MAX_ITERATIONS = 1000;
    private static final int MAX_SMALL;
    public static final BigInteger ONE;
    private static final BigInteger SMALL_PRIMES_PRODUCT;
    private static final BigInteger THREE;
    private static final BigInteger TWO;
    public static final BigInteger ZERO;
    
    static {
        ZERO = BigInteger.valueOf(0L);
        ONE = BigInteger.valueOf(1L);
        TWO = BigInteger.valueOf(2L);
        THREE = BigInteger.valueOf(3L);
        SMALL_PRIMES_PRODUCT = new BigInteger("8138e8a0fcf3a4e84a771d40fd305d7f4aa59306d7251de54d98af8fe95729a1f73d893fa424cd2edc8636a6c3285e022b0e3866a565ae8108eed8591cd4fe8d2ce86165a978d719ebf647f362d33fca29cd179fb42401cbaf3df0c614056f9c8f3cfd51e474afb6bc6974f78db8aba8e9e517fded658591ab7502bd41849462f", 16);
        MAX_SMALL = BigInteger.valueOf(743L).bitLength();
    }
    
    public static byte[] asUnsignedByteArray(final int n, final BigInteger bigInteger) {
        final byte[] byteArray = bigInteger.toByteArray();
        if (byteArray.length == n) {
            return byteArray;
        }
        int n2 = 0;
        if (byteArray[0] == 0) {
            n2 = 1;
        }
        final int n3 = byteArray.length - n2;
        if (n3 <= n) {
            final byte[] array = new byte[n];
            System.arraycopy(byteArray, n2, array, array.length - n3, n3);
            return array;
        }
        throw new IllegalArgumentException("standard length exceeded for value");
    }
    
    public static byte[] asUnsignedByteArray(final BigInteger bigInteger) {
        final byte[] byteArray = bigInteger.toByteArray();
        if (byteArray[0] == 0) {
            final byte[] array = new byte[byteArray.length - 1];
            System.arraycopy(byteArray, 1, array, 0, array.length);
            return array;
        }
        return byteArray;
    }
    
    private static byte[] createRandom(final int n, final SecureRandom secureRandom) throws IllegalArgumentException {
        if (n >= 1) {
            final int n2 = (n + 7) / 8;
            final byte[] bytes = new byte[n2];
            secureRandom.nextBytes(bytes);
            bytes[0] &= (byte)(255 >>> n2 * 8 - n);
            return bytes;
        }
        throw new IllegalArgumentException("bitLength must be at least 1");
    }
    
    public static BigInteger createRandomBigInteger(final int n, final SecureRandom secureRandom) {
        return new BigInteger(1, createRandom(n, secureRandom));
    }
    
    public static BigInteger createRandomInRange(final BigInteger bigInteger, BigInteger bigInteger2, final SecureRandom secureRandom) {
        final int compareTo = bigInteger.compareTo(bigInteger2);
        if (compareTo < 0) {
            if (bigInteger.bitLength() > bigInteger2.bitLength() / 2) {
                bigInteger2 = createRandomInRange(BigIntegers.ZERO, bigInteger2.subtract(bigInteger), secureRandom);
            }
            else {
                for (int i = 0; i < 1000; ++i) {
                    final BigInteger randomBigInteger = createRandomBigInteger(bigInteger2.bitLength(), secureRandom);
                    if (randomBigInteger.compareTo(bigInteger) >= 0 && randomBigInteger.compareTo(bigInteger2) <= 0) {
                        return randomBigInteger;
                    }
                }
                bigInteger2 = createRandomBigInteger(bigInteger2.subtract(bigInteger).bitLength() - 1, secureRandom);
            }
            return bigInteger2.add(bigInteger);
        }
        if (compareTo <= 0) {
            return bigInteger;
        }
        throw new IllegalArgumentException("'min' may not be greater than 'max'");
    }
    
    public static BigInteger createRandomPrime(final int n, final int certainty, final SecureRandom secureRandom) {
        if (n < 2) {
            throw new IllegalArgumentException("bitLength < 2");
        }
        if (n != 2) {
            BigInteger bigInteger;
            do {
                final byte[] random = createRandom(n, secureRandom);
                random[0] |= (byte)(1 << 7 - (random.length * 8 - n));
                final int n2 = random.length - 1;
                random[n2] |= 0x1;
                BigInteger add = bigInteger = new BigInteger(1, random);
                if (n > BigIntegers.MAX_SMALL) {
                    while (true) {
                        bigInteger = add;
                        if (add.gcd(BigIntegers.SMALL_PRIMES_PRODUCT).equals(BigIntegers.ONE)) {
                            break;
                        }
                        add = add.add(BigIntegers.TWO);
                    }
                }
            } while (!bigInteger.isProbablePrime(certainty));
            return bigInteger;
        }
        if (secureRandom.nextInt() < 0) {
            return BigIntegers.TWO;
        }
        return BigIntegers.THREE;
    }
    
    public static BigInteger fromUnsignedByteArray(final byte[] magnitude) {
        return new BigInteger(1, magnitude);
    }
    
    public static BigInteger fromUnsignedByteArray(final byte[] array, final int n, final int n2) {
        if (n == 0) {
            final byte[] array2 = array;
            if (n2 == array.length) {
                return new BigInteger(1, array2);
            }
        }
        final byte[] array2 = new byte[n2];
        System.arraycopy(array, n, array2, 0, n2);
        return new BigInteger(1, array2);
    }
    
    public static int getUnsignedByteLength(final BigInteger bigInteger) {
        return (bigInteger.bitLength() + 7) / 8;
    }
    
    public static int intValueExact(final BigInteger bigInteger) {
        if (bigInteger.bitLength() <= 31) {
            return bigInteger.intValue();
        }
        throw new ArithmeticException("BigInteger out of int range");
    }
    
    public static long longValueExact(final BigInteger bigInteger) {
        if (bigInteger.bitLength() <= 63) {
            return bigInteger.longValue();
        }
        throw new ArithmeticException("BigInteger out of long range");
    }
}
