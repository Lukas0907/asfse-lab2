// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util;

public interface Selector<T> extends Cloneable
{
    Object clone();
    
    boolean match(final T p0);
}
