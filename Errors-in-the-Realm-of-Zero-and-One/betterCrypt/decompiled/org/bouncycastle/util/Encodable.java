// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util;

import java.io.IOException;

public interface Encodable
{
    byte[] getEncoded() throws IOException;
}
