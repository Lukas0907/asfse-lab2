// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util.test;

public interface TestResult
{
    Throwable getException();
    
    boolean isSuccessful();
    
    String toString();
}
