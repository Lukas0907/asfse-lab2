// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util.test;

import org.bouncycastle.util.Strings;

public class SimpleTestResult implements TestResult
{
    private static final String SEPARATOR;
    private Throwable exception;
    private String message;
    private boolean success;
    
    static {
        SEPARATOR = Strings.lineSeparator();
    }
    
    public SimpleTestResult(final boolean success, final String message) {
        this.success = success;
        this.message = message;
    }
    
    public SimpleTestResult(final boolean success, final String message, final Throwable exception) {
        this.success = success;
        this.message = message;
        this.exception = exception;
    }
    
    public static TestResult failed(final Test test, final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append(test.getName());
        sb.append(": ");
        sb.append(str);
        return new SimpleTestResult(false, sb.toString());
    }
    
    public static TestResult failed(final Test test, final String str, final Object obj, final Object obj2) {
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(SimpleTestResult.SEPARATOR);
        sb.append("Expected: ");
        sb.append(obj);
        sb.append(SimpleTestResult.SEPARATOR);
        sb.append("Found   : ");
        sb.append(obj2);
        return failed(test, sb.toString());
    }
    
    public static TestResult failed(final Test test, final String str, final Throwable t) {
        final StringBuilder sb = new StringBuilder();
        sb.append(test.getName());
        sb.append(": ");
        sb.append(str);
        return new SimpleTestResult(false, sb.toString(), t);
    }
    
    public static String failedMessage(final String str, final String str2, final String str3, final String str4) {
        final StringBuffer sb = new StringBuffer(str);
        sb.append(" failing ");
        sb.append(str2);
        sb.append(SimpleTestResult.SEPARATOR);
        sb.append("    expected: ");
        sb.append(str3);
        sb.append(SimpleTestResult.SEPARATOR);
        sb.append("    got     : ");
        sb.append(str4);
        return sb.toString();
    }
    
    public static TestResult successful(final Test test, final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append(test.getName());
        sb.append(": ");
        sb.append(str);
        return new SimpleTestResult(true, sb.toString());
    }
    
    @Override
    public Throwable getException() {
        return this.exception;
    }
    
    @Override
    public boolean isSuccessful() {
        return this.success;
    }
    
    @Override
    public String toString() {
        return this.message;
    }
}
