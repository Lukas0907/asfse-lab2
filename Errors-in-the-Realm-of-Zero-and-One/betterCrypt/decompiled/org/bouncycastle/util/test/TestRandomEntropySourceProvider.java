// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util.test;

import org.bouncycastle.crypto.prng.EntropySource;
import java.security.SecureRandom;
import org.bouncycastle.crypto.prng.EntropySourceProvider;

public class TestRandomEntropySourceProvider implements EntropySourceProvider
{
    private final boolean _predictionResistant;
    private final SecureRandom _sr;
    
    public TestRandomEntropySourceProvider(final boolean predictionResistant) {
        this._sr = new SecureRandom();
        this._predictionResistant = predictionResistant;
    }
    
    @Override
    public EntropySource get(final int n) {
        return new EntropySource() {
            @Override
            public int entropySize() {
                return n;
            }
            
            @Override
            public byte[] getEntropy() {
                final byte[] bytes = new byte[(n + 7) / 8];
                TestRandomEntropySourceProvider.this._sr.nextBytes(bytes);
                return bytes;
            }
            
            @Override
            public boolean isPredictionResistant() {
                return TestRandomEntropySourceProvider.this._predictionResistant;
            }
        };
    }
}
