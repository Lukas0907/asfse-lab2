// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util.test;

import org.bouncycastle.util.Arrays;
import java.util.Enumeration;
import java.util.Vector;
import java.io.PrintStream;

public abstract class SimpleTest implements Test
{
    public static void runTest(final Test test) {
        runTest(test, System.out);
    }
    
    public static void runTest(final Test test, final PrintStream s) {
        final TestResult perform = test.perform();
        if (perform.getException() != null) {
            perform.getException().printStackTrace(s);
        }
        s.println(perform);
    }
    
    public static void runTests(final Test[] array) {
        runTests(array, System.out);
    }
    
    public static void runTests(final Test[] array, PrintStream out) {
        final Vector<TestResult> vector = new Vector<TestResult>();
        for (int i = 0; i != array.length; ++i) {
            final TestResult perform = array[i].perform();
            if (!perform.isSuccessful()) {
                vector.addElement(perform);
            }
            if (perform.getException() != null) {
                perform.getException().printStackTrace(out);
            }
            out.println(perform);
        }
        out.println("-----");
        if (vector.isEmpty()) {
            out.println("All tests successful.");
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Completed with ");
        sb.append(vector.size());
        sb.append(" FAILURES:");
        out.println(sb.toString());
        final Enumeration<TestResult> elements = vector.elements();
        while (elements.hasMoreElements()) {
            out = System.out;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("=>  ");
            sb2.append(elements.nextElement());
            out.println(sb2.toString());
        }
    }
    
    private TestResult success() {
        return SimpleTestResult.successful(this, "Okay");
    }
    
    protected boolean areEqual(final byte[] array, final byte[] array2) {
        return Arrays.areEqual(array, array2);
    }
    
    protected boolean areEqual(final byte[][] array, final byte[][] array2) {
        if (array == null && array2 == null) {
            return true;
        }
        if (array == null) {
            return false;
        }
        if (array2 == null) {
            return false;
        }
        if (array.length != array2.length) {
            return false;
        }
        for (int i = 0; i < array.length; ++i) {
            if (!this.areEqual(array[i], array2[i])) {
                return false;
            }
        }
        return true;
    }
    
    protected void fail(final String s) {
        throw new TestFailedException(SimpleTestResult.failed(this, s));
    }
    
    protected void fail(final String s, final Object o, final Object o2) {
        throw new TestFailedException(SimpleTestResult.failed(this, s, o, o2));
    }
    
    protected void fail(final String s, final Throwable t) {
        throw new TestFailedException(SimpleTestResult.failed(this, s, t));
    }
    
    @Override
    public abstract String getName();
    
    protected void isEquals(final int n, final int n2) {
        if (n == n2) {
            return;
        }
        throw new TestFailedException(SimpleTestResult.failed(this, "no message"));
    }
    
    protected void isEquals(final long n, final long n2) {
        if (n == n2) {
            return;
        }
        throw new TestFailedException(SimpleTestResult.failed(this, "no message"));
    }
    
    protected void isEquals(final Object o, final Object obj) {
        if (o.equals(obj)) {
            return;
        }
        throw new TestFailedException(SimpleTestResult.failed(this, "no message"));
    }
    
    protected void isEquals(final String s, final long n, final long n2) {
        if (n == n2) {
            return;
        }
        throw new TestFailedException(SimpleTestResult.failed(this, s));
    }
    
    protected void isEquals(final String s, final Object o, final Object obj) {
        if (o == null && obj == null) {
            return;
        }
        if (o == null) {
            throw new TestFailedException(SimpleTestResult.failed(this, s));
        }
        if (obj == null) {
            throw new TestFailedException(SimpleTestResult.failed(this, s));
        }
        if (o.equals(obj)) {
            return;
        }
        throw new TestFailedException(SimpleTestResult.failed(this, s));
    }
    
    protected void isEquals(final String s, final boolean b, final boolean b2) {
        if (b == b2) {
            return;
        }
        throw new TestFailedException(SimpleTestResult.failed(this, s));
    }
    
    protected void isTrue(final String s, final boolean b) {
        if (b) {
            return;
        }
        throw new TestFailedException(SimpleTestResult.failed(this, s));
    }
    
    protected void isTrue(final boolean b) {
        if (b) {
            return;
        }
        throw new TestFailedException(SimpleTestResult.failed(this, "no message"));
    }
    
    @Override
    public TestResult perform() {
        try {
            this.performTest();
            return this.success();
        }
        catch (Exception obj) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Exception: ");
            sb.append(obj);
            return SimpleTestResult.failed(this, sb.toString(), obj);
        }
        catch (TestFailedException ex) {
            return ex.getResult();
        }
    }
    
    public abstract void performTest() throws Exception;
}
