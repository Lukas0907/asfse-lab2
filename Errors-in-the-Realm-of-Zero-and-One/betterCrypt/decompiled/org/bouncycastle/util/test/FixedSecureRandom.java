// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util.test;

import java.security.SecureRandomSpi;
import java.security.Provider;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.util.Pack;
import java.util.Random;
import java.math.BigInteger;
import java.security.SecureRandom;

public class FixedSecureRandom extends SecureRandom
{
    private static java.math.BigInteger ANDROID;
    private static java.math.BigInteger CLASSPATH;
    private static java.math.BigInteger REGULAR;
    private static final boolean isAndroidStyle;
    private static final boolean isClasspathStyle;
    private static final boolean isRegularStyle;
    private byte[] _data;
    private int _index;
    
    static {
        FixedSecureRandom.REGULAR = new java.math.BigInteger("01020304ffffffff0506070811111111", 16);
        FixedSecureRandom.ANDROID = new java.math.BigInteger("1111111105060708ffffffff01020304", 16);
        FixedSecureRandom.CLASSPATH = new java.math.BigInteger("3020104ffffffff05060708111111", 16);
        final java.math.BigInteger bigInteger = new java.math.BigInteger(128, new RandomChecker());
        final java.math.BigInteger bigInteger2 = new java.math.BigInteger(120, new RandomChecker());
        isAndroidStyle = bigInteger.equals(FixedSecureRandom.ANDROID);
        isRegularStyle = bigInteger.equals(FixedSecureRandom.REGULAR);
        isClasspathStyle = bigInteger2.equals(FixedSecureRandom.CLASSPATH);
    }
    
    public FixedSecureRandom(final byte[] array) {
        this(new Source[] { (Source)new Data(array) });
    }
    
    public FixedSecureRandom(final Source[] p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: aconst_null    
        //     2: new             Lorg/bouncycastle/util/test/FixedSecureRandom$DummyProvider;
        //     5: dup            
        //     6: invokespecial   org/bouncycastle/util/test/FixedSecureRandom$DummyProvider.<init>:()V
        //     9: invokespecial   java/security/SecureRandom.<init>:(Ljava/security/SecureRandomSpi;Ljava/security/Provider;)V
        //    12: new             Ljava/io/ByteArrayOutputStream;
        //    15: dup            
        //    16: invokespecial   java/io/ByteArrayOutputStream.<init>:()V
        //    19: astore          7
        //    21: getstatic       org/bouncycastle/util/test/FixedSecureRandom.isRegularStyle:Z
        //    24: istore          6
        //    26: iconst_0       
        //    27: istore_2       
        //    28: iconst_0       
        //    29: istore_3       
        //    30: iload           6
        //    32: ifeq            198
        //    35: getstatic       org/bouncycastle/util/test/FixedSecureRandom.isClasspathStyle:Z
        //    38: ifeq            164
        //    41: iload_3        
        //    42: istore_2       
        //    43: iload_2        
        //    44: aload_1        
        //    45: arraylength    
        //    46: if_icmpeq       365
        //    49: aload_1        
        //    50: iload_2        
        //    51: aaload         
        //    52: instanceof      Lorg/bouncycastle/util/test/FixedSecureRandom$BigInteger;
        //    55: ifeq            136
        //    58: aload_1        
        //    59: iload_2        
        //    60: aaload         
        //    61: getfield        org/bouncycastle/util/test/FixedSecureRandom$Source.data:[B
        //    64: astore          8
        //    66: aload           8
        //    68: arraylength    
        //    69: aload           8
        //    71: arraylength    
        //    72: iconst_4       
        //    73: irem           
        //    74: isub           
        //    75: istore          4
        //    77: aload           8
        //    79: arraylength    
        //    80: iload           4
        //    82: isub           
        //    83: iconst_1       
        //    84: isub           
        //    85: istore_3       
        //    86: iload_3        
        //    87: iflt            106
        //    90: aload           7
        //    92: aload           8
        //    94: iload_3        
        //    95: baload         
        //    96: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //    99: iload_3        
        //   100: iconst_1       
        //   101: isub           
        //   102: istore_3       
        //   103: goto            86
        //   106: aload           8
        //   108: arraylength    
        //   109: iload           4
        //   111: isub           
        //   112: istore_3       
        //   113: iload_3        
        //   114: aload           8
        //   116: arraylength    
        //   117: if_icmpge       147
        //   120: aload           7
        //   122: aload           8
        //   124: iload_3        
        //   125: iconst_4       
        //   126: invokevirtual   java/io/ByteArrayOutputStream.write:([BII)V
        //   129: iload_3        
        //   130: iconst_4       
        //   131: iadd           
        //   132: istore_3       
        //   133: goto            113
        //   136: aload           7
        //   138: aload_1        
        //   139: iload_2        
        //   140: aaload         
        //   141: getfield        org/bouncycastle/util/test/FixedSecureRandom$Source.data:[B
        //   144: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //   147: iload_2        
        //   148: iconst_1       
        //   149: iadd           
        //   150: istore_2       
        //   151: goto            43
        //   154: new             Ljava/lang/IllegalArgumentException;
        //   157: dup            
        //   158: ldc             "can't save value source."
        //   160: invokespecial   java/lang/IllegalArgumentException.<init>:(Ljava/lang/String;)V
        //   163: athrow         
        //   164: iload_2        
        //   165: aload_1        
        //   166: arraylength    
        //   167: if_icmpeq       365
        //   170: aload           7
        //   172: aload_1        
        //   173: iload_2        
        //   174: aaload         
        //   175: getfield        org/bouncycastle/util/test/FixedSecureRandom$Source.data:[B
        //   178: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //   181: iload_2        
        //   182: iconst_1       
        //   183: iadd           
        //   184: istore_2       
        //   185: goto            164
        //   188: new             Ljava/lang/IllegalArgumentException;
        //   191: dup            
        //   192: ldc             "can't save value source."
        //   194: invokespecial   java/lang/IllegalArgumentException.<init>:(Ljava/lang/String;)V
        //   197: athrow         
        //   198: getstatic       org/bouncycastle/util/test/FixedSecureRandom.isAndroidStyle:Z
        //   201: ifeq            375
        //   204: iconst_0       
        //   205: istore_2       
        //   206: iload_2        
        //   207: aload_1        
        //   208: arraylength    
        //   209: if_icmpeq       365
        //   212: aload_1        
        //   213: iload_2        
        //   214: aaload         
        //   215: instanceof      Lorg/bouncycastle/util/test/FixedSecureRandom$BigInteger;
        //   218: ifeq            337
        //   221: aload_1        
        //   222: iload_2        
        //   223: aaload         
        //   224: getfield        org/bouncycastle/util/test/FixedSecureRandom$Source.data:[B
        //   227: astore          8
        //   229: aload           8
        //   231: arraylength    
        //   232: aload           8
        //   234: arraylength    
        //   235: iconst_4       
        //   236: irem           
        //   237: isub           
        //   238: istore          4
        //   240: iconst_0       
        //   241: istore_3       
        //   242: iload_3        
        //   243: iload           4
        //   245: if_icmpge       272
        //   248: aload           8
        //   250: arraylength    
        //   251: istore          5
        //   253: iload_3        
        //   254: iconst_4       
        //   255: iadd           
        //   256: istore_3       
        //   257: aload           7
        //   259: aload           8
        //   261: iload           5
        //   263: iload_3        
        //   264: isub           
        //   265: iconst_4       
        //   266: invokevirtual   java/io/ByteArrayOutputStream.write:([BII)V
        //   269: goto            242
        //   272: aload           8
        //   274: arraylength    
        //   275: iload           4
        //   277: isub           
        //   278: ifeq            397
        //   281: iconst_0       
        //   282: istore_3       
        //   283: iload_3        
        //   284: iconst_4       
        //   285: aload           8
        //   287: arraylength    
        //   288: iload           4
        //   290: isub           
        //   291: isub           
        //   292: if_icmpeq       397
        //   295: aload           7
        //   297: iconst_0       
        //   298: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //   301: iload_3        
        //   302: iconst_1       
        //   303: iadd           
        //   304: istore_3       
        //   305: goto            283
        //   308: iload_3        
        //   309: aload           8
        //   311: arraylength    
        //   312: iload           4
        //   314: isub           
        //   315: if_icmpeq       348
        //   318: aload           7
        //   320: aload           8
        //   322: iload           4
        //   324: iload_3        
        //   325: iadd           
        //   326: baload         
        //   327: invokevirtual   java/io/ByteArrayOutputStream.write:(I)V
        //   330: iload_3        
        //   331: iconst_1       
        //   332: iadd           
        //   333: istore_3       
        //   334: goto            308
        //   337: aload           7
        //   339: aload_1        
        //   340: iload_2        
        //   341: aaload         
        //   342: getfield        org/bouncycastle/util/test/FixedSecureRandom$Source.data:[B
        //   345: invokevirtual   java/io/ByteArrayOutputStream.write:([B)V
        //   348: iload_2        
        //   349: iconst_1       
        //   350: iadd           
        //   351: istore_2       
        //   352: goto            206
        //   355: new             Ljava/lang/IllegalArgumentException;
        //   358: dup            
        //   359: ldc             "can't save value source."
        //   361: invokespecial   java/lang/IllegalArgumentException.<init>:(Ljava/lang/String;)V
        //   364: athrow         
        //   365: aload_0        
        //   366: aload           7
        //   368: invokevirtual   java/io/ByteArrayOutputStream.toByteArray:()[B
        //   371: putfield        org/bouncycastle/util/test/FixedSecureRandom._data:[B
        //   374: return         
        //   375: new             Ljava/lang/IllegalStateException;
        //   378: dup            
        //   379: ldc             "Unrecognized BigInteger implementation"
        //   381: invokespecial   java/lang/IllegalStateException.<init>:(Ljava/lang/String;)V
        //   384: athrow         
        //   385: astore_1       
        //   386: goto            154
        //   389: astore_1       
        //   390: goto            188
        //   393: astore_1       
        //   394: goto            355
        //   397: iconst_0       
        //   398: istore_3       
        //   399: goto            308
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  49     86     385    164    Ljava/io/IOException;
        //  90     99     385    164    Ljava/io/IOException;
        //  106    113    385    164    Ljava/io/IOException;
        //  113    129    385    164    Ljava/io/IOException;
        //  136    147    385    164    Ljava/io/IOException;
        //  170    181    389    198    Ljava/io/IOException;
        //  212    240    393    365    Ljava/io/IOException;
        //  248    253    393    365    Ljava/io/IOException;
        //  257    269    393    365    Ljava/io/IOException;
        //  272    281    393    365    Ljava/io/IOException;
        //  283    301    393    365    Ljava/io/IOException;
        //  308    330    393    365    Ljava/io/IOException;
        //  337    348    393    365    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 240 out of bounds for length 240
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:373)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:426)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:713)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:549)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public FixedSecureRandom(final byte[][] array) {
        this((Source[])buildDataArray(array));
    }
    
    private static Data[] buildDataArray(final byte[][] array) {
        final Data[] array2 = new Data[array.length];
        for (int i = 0; i != array.length; ++i) {
            array2[i] = new Data(array[i]);
        }
        return array2;
    }
    
    private static byte[] expandToBitLength(int n, final byte[] array) {
        final int n2 = (n + 7) / 8;
        if (n2 > array.length) {
            final byte[] array2 = new byte[n2];
            System.arraycopy(array, 0, array2, array2.length - array.length, array.length);
            if (FixedSecureRandom.isAndroidStyle) {
                n %= 8;
                if (n != 0) {
                    Pack.intToBigEndian(Pack.bigEndianToInt(array2, 0) << 8 - n, array2, 0);
                }
            }
            return array2;
        }
        if (FixedSecureRandom.isAndroidStyle && n < array.length * 8) {
            n %= 8;
            if (n != 0) {
                Pack.intToBigEndian(Pack.bigEndianToInt(array, 0) << 8 - n, array, 0);
            }
        }
        return array;
    }
    
    private int nextValue() {
        return this._data[this._index++] & 0xFF;
    }
    
    @Override
    public byte[] generateSeed(final int n) {
        final byte[] array = new byte[n];
        this.nextBytes(array);
        return array;
    }
    
    public boolean isExhausted() {
        return this._index == this._data.length;
    }
    
    @Override
    public void nextBytes(final byte[] array) {
        System.arraycopy(this._data, this._index, array, 0, array.length);
        this._index += array.length;
    }
    
    @Override
    public int nextInt() {
        return this.nextValue() << 24 | 0x0 | this.nextValue() << 16 | this.nextValue() << 8 | this.nextValue();
    }
    
    @Override
    public long nextLong() {
        return (long)this.nextValue() << 56 | 0x0L | (long)this.nextValue() << 48 | (long)this.nextValue() << 40 | (long)this.nextValue() << 32 | (long)this.nextValue() << 24 | (long)this.nextValue() << 16 | (long)this.nextValue() << 8 | (long)this.nextValue();
    }
    
    public static class BigInteger extends Source
    {
        public BigInteger(final int n, final String s) {
            super(expandToBitLength(n, Hex.decode(s)));
        }
        
        public BigInteger(final int n, final byte[] array) {
            super(expandToBitLength(n, array));
        }
        
        public BigInteger(final String s) {
            this(Hex.decode(s));
        }
        
        public BigInteger(final byte[] array) {
            super(array);
        }
    }
    
    public static class Data extends Source
    {
        public Data(final byte[] array) {
            super(array);
        }
    }
    
    private static class DummyProvider extends Provider
    {
        DummyProvider() {
            super("BCFIPS_FIXED_RNG", 1.0, "BCFIPS Fixed Secure Random Provider");
        }
    }
    
    private static class RandomChecker extends SecureRandom
    {
        byte[] data;
        int index;
        
        RandomChecker() {
            super(null, new DummyProvider());
            this.data = Hex.decode("01020304ffffffff0506070811111111");
            this.index = 0;
        }
        
        @Override
        public void nextBytes(final byte[] array) {
            System.arraycopy(this.data, this.index, array, 0, array.length);
            this.index += array.length;
        }
    }
    
    public static class Source
    {
        byte[] data;
        
        Source(final byte[] data) {
            this.data = data;
        }
    }
}
