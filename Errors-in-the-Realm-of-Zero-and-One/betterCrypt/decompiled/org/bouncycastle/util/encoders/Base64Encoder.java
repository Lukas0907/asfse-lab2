// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util.encoders;

import java.io.IOException;
import java.io.OutputStream;

public class Base64Encoder implements Encoder
{
    protected final byte[] decodingTable;
    protected final byte[] encodingTable;
    protected byte padding;
    
    public Base64Encoder() {
        this.encodingTable = new byte[] { 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47 };
        this.padding = 61;
        this.decodingTable = new byte[128];
        this.initialiseDecodingTable();
    }
    
    private int decodeLastBlock(final OutputStream outputStream, final char c, final char c2, final char c3, final char c4) throws IOException {
        final byte padding = this.padding;
        if (c3 == padding) {
            if (c4 != padding) {
                throw new IOException("invalid characters encountered at end of base64 data");
            }
            final byte[] decodingTable = this.decodingTable;
            final byte b = decodingTable[c];
            final byte b2 = decodingTable[c2];
            if ((b | b2) >= 0) {
                outputStream.write(b << 2 | b2 >> 4);
                return 1;
            }
            throw new IOException("invalid characters encountered at end of base64 data");
        }
        else if (c4 == padding) {
            final byte[] decodingTable2 = this.decodingTable;
            final byte b3 = decodingTable2[c];
            final byte b4 = decodingTable2[c2];
            final byte b5 = decodingTable2[c3];
            if ((b3 | b4 | b5) >= 0) {
                outputStream.write(b3 << 2 | b4 >> 4);
                outputStream.write(b4 << 4 | b5 >> 2);
                return 2;
            }
            throw new IOException("invalid characters encountered at end of base64 data");
        }
        else {
            final byte[] decodingTable3 = this.decodingTable;
            final byte b6 = decodingTable3[c];
            final byte b7 = decodingTable3[c2];
            final byte b8 = decodingTable3[c3];
            final byte b9 = decodingTable3[c4];
            if ((b6 | b7 | b8 | b9) >= 0) {
                outputStream.write(b6 << 2 | b7 >> 4);
                outputStream.write(b7 << 4 | b8 >> 2);
                outputStream.write(b8 << 6 | b9);
                return 3;
            }
            throw new IOException("invalid characters encountered at end of base64 data");
        }
    }
    
    private boolean ignore(final char c) {
        return c == '\n' || c == '\r' || c == '\t' || c == ' ';
    }
    
    private int nextI(final String s, int index, final int n) {
        while (index < n && this.ignore(s.charAt(index))) {
            ++index;
        }
        return index;
    }
    
    private int nextI(final byte[] array, int n, final int n2) {
        while (n < n2 && this.ignore((char)array[n])) {
            ++n;
        }
        return n;
    }
    
    @Override
    public int decode(final String s, final OutputStream outputStream) throws IOException {
        int length;
        for (length = s.length(); length > 0 && this.ignore(s.charAt(length - 1)); --length) {}
        final int n = 0;
        if (length == 0) {
            return 0;
        }
        int n2 = length;
        int n4;
        for (int n3 = 0; n2 > 0 && n3 != 4; --n2, n3 = n4) {
            n4 = n3;
            if (!this.ignore(s.charAt(n2 - 1))) {
                n4 = n3 + 1;
            }
        }
        int i = this.nextI(s, 0, n2);
        int n5 = n;
        while (i < n2) {
            final byte b = this.decodingTable[s.charAt(i)];
            final int nextI = this.nextI(s, i + 1, n2);
            final byte b2 = this.decodingTable[s.charAt(nextI)];
            final int nextI2 = this.nextI(s, nextI + 1, n2);
            final byte b3 = this.decodingTable[s.charAt(nextI2)];
            final int nextI3 = this.nextI(s, nextI2 + 1, n2);
            final byte b4 = this.decodingTable[s.charAt(nextI3)];
            if ((b | b2 | b3 | b4) < 0) {
                throw new IOException("invalid characters encountered in base64 data");
            }
            outputStream.write(b << 2 | b2 >> 4);
            outputStream.write(b2 << 4 | b3 >> 2);
            outputStream.write(b3 << 6 | b4);
            n5 += 3;
            i = this.nextI(s, nextI3 + 1, n2);
        }
        final int nextI4 = this.nextI(s, i, length);
        final int nextI5 = this.nextI(s, nextI4 + 1, length);
        final int nextI6 = this.nextI(s, nextI5 + 1, length);
        return n5 + this.decodeLastBlock(outputStream, s.charAt(nextI4), s.charAt(nextI5), s.charAt(nextI6), s.charAt(this.nextI(s, nextI6 + 1, length)));
    }
    
    @Override
    public int decode(final byte[] array, int n, int nextI, final OutputStream outputStream) throws IOException {
        for (nextI += n; nextI > n && this.ignore((char)array[nextI - 1]); --nextI) {}
        final int n2 = 0;
        if (nextI == 0) {
            return 0;
        }
        int n3 = nextI;
        int n5;
        for (int n4 = 0; n3 > n && n4 != 4; --n3, n4 = n5) {
            n5 = n4;
            if (!this.ignore((char)array[n3 - 1])) {
                n5 = n4 + 1;
            }
        }
        int i = this.nextI(array, n, n3);
        n = n2;
        while (i < n3) {
            final byte b = this.decodingTable[array[i]];
            final int nextI2 = this.nextI(array, i + 1, n3);
            final byte b2 = this.decodingTable[array[nextI2]];
            final int nextI3 = this.nextI(array, nextI2 + 1, n3);
            final byte b3 = this.decodingTable[array[nextI3]];
            final int nextI4 = this.nextI(array, nextI3 + 1, n3);
            final byte b4 = this.decodingTable[array[nextI4]];
            if ((b | b2 | b3 | b4) < 0) {
                throw new IOException("invalid characters encountered in base64 data");
            }
            outputStream.write(b << 2 | b2 >> 4);
            outputStream.write(b2 << 4 | b3 >> 2);
            outputStream.write(b3 << 6 | b4);
            n += 3;
            i = this.nextI(array, nextI4 + 1, n3);
        }
        final int nextI5 = this.nextI(array, i, nextI);
        final int nextI6 = this.nextI(array, nextI5 + 1, nextI);
        final int nextI7 = this.nextI(array, nextI6 + 1, nextI);
        nextI = this.nextI(array, nextI7 + 1, nextI);
        return n + this.decodeLastBlock(outputStream, (char)array[nextI5], (char)array[nextI6], (char)array[nextI7], (char)array[nextI]);
    }
    
    @Override
    public int encode(final byte[] array, int padding, int n, final OutputStream outputStream) throws IOException {
        final int n2 = n % 3;
        final int n3 = n - n2;
        n = padding;
        int n4;
        int n5;
        while (true) {
            n4 = padding + n3;
            n5 = 4;
            if (n >= n4) {
                break;
            }
            final int n6 = array[n] & 0xFF;
            final int n7 = array[n + 1] & 0xFF;
            final int n8 = array[n + 2] & 0xFF;
            outputStream.write(this.encodingTable[n6 >>> 2 & 0x3F]);
            outputStream.write(this.encodingTable[(n6 << 4 | n7 >>> 4) & 0x3F]);
            outputStream.write(this.encodingTable[(n7 << 2 | n8 >>> 6) & 0x3F]);
            outputStream.write(this.encodingTable[n8 & 0x3F]);
            n += 3;
        }
        Label_0295: {
            if (n2 != 0) {
                if (n2 != 1) {
                    if (n2 != 2) {
                        break Label_0295;
                    }
                    padding = (array[n4] & 0xFF);
                    n = (array[n4 + 1] & 0xFF);
                    outputStream.write(this.encodingTable[padding >>> 2 & 0x3F]);
                    outputStream.write(this.encodingTable[(padding << 4 | n >>> 4) & 0x3F]);
                    padding = this.encodingTable[n << 2 & 0x3F];
                }
                else {
                    padding = (array[n4] & 0xFF);
                    outputStream.write(this.encodingTable[padding >>> 2 & 0x3F]);
                    outputStream.write(this.encodingTable[padding << 4 & 0x3F]);
                    padding = this.padding;
                }
                outputStream.write(padding);
                outputStream.write(this.padding);
            }
        }
        n = n3 / 3;
        padding = n5;
        if (n2 == 0) {
            padding = 0;
        }
        return n * 4 + padding;
    }
    
    protected void initialiseDecodingTable() {
        final int n = 0;
        int n2 = 0;
        int n3;
        while (true) {
            final byte[] decodingTable = this.decodingTable;
            n3 = n;
            if (n2 >= decodingTable.length) {
                break;
            }
            decodingTable[n2] = -1;
            ++n2;
        }
        while (true) {
            final byte[] encodingTable = this.encodingTable;
            if (n3 >= encodingTable.length) {
                break;
            }
            this.decodingTable[encodingTable[n3]] = (byte)n3;
            ++n3;
        }
    }
}
