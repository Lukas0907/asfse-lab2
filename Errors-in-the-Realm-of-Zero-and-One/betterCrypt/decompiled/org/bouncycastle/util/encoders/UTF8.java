// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util.encoders;

public class UTF8
{
    private static final byte C_CR1 = 1;
    private static final byte C_CR2 = 2;
    private static final byte C_CR3 = 3;
    private static final byte C_ILL = 0;
    private static final byte C_L2A = 4;
    private static final byte C_L3A = 5;
    private static final byte C_L3B = 6;
    private static final byte C_L3C = 7;
    private static final byte C_L4A = 8;
    private static final byte C_L4B = 9;
    private static final byte C_L4C = 10;
    private static final byte S_CS1 = 0;
    private static final byte S_CS2 = 16;
    private static final byte S_CS3 = 32;
    private static final byte S_END = -1;
    private static final byte S_ERR = -2;
    private static final byte S_P3A = 48;
    private static final byte S_P3B = 64;
    private static final byte S_P4A = 80;
    private static final byte S_P4B = 96;
    private static final short[] firstUnitTable;
    private static final byte[] transitionTable;
    
    static {
        firstUnitTable = new short[128];
        transitionTable = new byte[112];
        final byte[] array = new byte[128];
        int i = 0;
        fill(array, 0, 15, (byte)1);
        fill(array, 16, 31, (byte)2);
        fill(array, 32, 63, (byte)3);
        fill(array, 64, 65, (byte)0);
        fill(array, 66, 95, (byte)4);
        fill(array, 96, 96, (byte)5);
        fill(array, 97, 108, (byte)6);
        fill(array, 109, 109, (byte)7);
        fill(array, 110, 111, (byte)6);
        fill(array, 112, 112, (byte)8);
        fill(array, 113, 115, (byte)9);
        fill(array, 116, 116, (byte)10);
        fill(array, 117, 127, (byte)0);
        final byte[] transitionTable2 = UTF8.transitionTable;
        fill(transitionTable2, 0, transitionTable2.length - 1, (byte)(-2));
        fill(UTF8.transitionTable, 8, 11, (byte)(-1));
        fill(UTF8.transitionTable, 24, 27, (byte)0);
        fill(UTF8.transitionTable, 40, 43, (byte)16);
        fill(UTF8.transitionTable, 58, 59, (byte)0);
        fill(UTF8.transitionTable, 72, 73, (byte)0);
        fill(UTF8.transitionTable, 89, 91, (byte)16);
        fill(UTF8.transitionTable, 104, 104, (byte)16);
        while (i < 128) {
            final byte b = array[i];
            UTF8.firstUnitTable[i] = (short)((new byte[] { -2, -2, -2, -2, 0, 48, 16, 64, 80, 32, 96 })[b] | ((new byte[] { 0, 0, 0, 0, 31, 15, 15, 15, 7, 7, 7 })[b] & i) << 8);
            ++i;
        }
    }
    
    private static void fill(final byte[] array, int i, final int n, final byte b) {
        while (i <= n) {
            array[i] = b;
            ++i;
        }
    }
    
    public static int transcodeToUTF16(final byte[] array, final char[] array2) {
        int i = 0;
        int n = 0;
        while (i < array.length) {
            final int n2 = i + 1;
            final byte b = array[i];
            if (b >= 0) {
                if (n >= array2.length) {
                    return -1;
                }
                array2[n] = (char)b;
                i = n2;
                ++n;
            }
            else {
                final short n3 = UTF8.firstUnitTable[b & 0x7F];
                int n4 = n3 >>> 8;
                final byte b2 = (byte)n3;
                byte b3;
                byte b4;
                for (i = n2, b3 = b2; b3 >= 0; b3 = UTF8.transitionTable[b3 + ((b4 & 0xFF) >>> 4)], ++i) {
                    if (i >= array.length) {
                        return -1;
                    }
                    b4 = array[i];
                    n4 = (n4 << 6 | (b4 & 0x3F));
                }
                if (b3 == -2) {
                    return -1;
                }
                if (n4 <= 65535) {
                    if (n >= array2.length) {
                        return -1;
                    }
                    array2[n] = (char)n4;
                    ++n;
                }
                else {
                    if (n >= array2.length - 1) {
                        return -1;
                    }
                    final int n5 = n + 1;
                    array2[n] = (char)((n4 >>> 10) + 55232);
                    n = n5 + 1;
                    array2[n5] = (char)(0xDC00 | (n4 & 0x3FF));
                }
            }
        }
        return n;
    }
}
