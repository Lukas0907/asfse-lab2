// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util.encoders;

import java.io.IOException;
import java.io.OutputStream;

public class HexEncoder implements Encoder
{
    protected final byte[] decodingTable;
    protected final byte[] encodingTable;
    
    public HexEncoder() {
        this.encodingTable = new byte[] { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102 };
        this.decodingTable = new byte[128];
        this.initialiseDecodingTable();
    }
    
    private static boolean ignore(final char c) {
        return c == '\n' || c == '\r' || c == '\t' || c == ' ';
    }
    
    @Override
    public int decode(final String s, final OutputStream outputStream) throws IOException {
        int length;
        for (length = s.length(); length > 0 && ignore(s.charAt(length - 1)); --length) {}
        int i = 0;
        int n = 0;
        while (i < length) {
            while (i < length && ignore(s.charAt(i))) {
                ++i;
            }
            final byte[] decodingTable = this.decodingTable;
            final int n2 = i + 1;
            final byte b = decodingTable[s.charAt(i)];
            int n3;
            for (n3 = n2; n3 < length && ignore(s.charAt(n3)); ++n3) {}
            final byte b2 = this.decodingTable[s.charAt(n3)];
            if ((b | b2) < 0) {
                throw new IOException("invalid characters encountered in Hex string");
            }
            outputStream.write(b << 4 | b2);
            ++n;
            i = n3 + 1;
        }
        return n;
    }
    
    @Override
    public int decode(final byte[] array, int i, int n, final OutputStream outputStream) throws IOException {
        for (n += i; n > i && ignore((char)array[n - 1]); --n) {}
        int n2 = 0;
        while (i < n) {
            while (i < n && ignore((char)array[i])) {
                ++i;
            }
            final byte[] decodingTable = this.decodingTable;
            final int n3 = i + 1;
            final byte b = decodingTable[array[i]];
            for (i = n3; i < n && ignore((char)array[i]); ++i) {}
            final byte b2 = this.decodingTable[array[i]];
            if ((b | b2) < 0) {
                throw new IOException("invalid characters encountered in Hex data");
            }
            outputStream.write(b << 4 | b2);
            ++n2;
            ++i;
        }
        return n2;
    }
    
    byte[] decodeStrict(final String s, int i, int index) throws IOException {
        if (s == null) {
            throw new NullPointerException("'str' cannot be null");
        }
        if (i < 0 || index < 0 || i > s.length() - index) {
            throw new IndexOutOfBoundsException("invalid offset and/or length specified");
        }
        if ((index & 0x1) == 0x0) {
            final int n = index >>> 1;
            final byte[] array = new byte[n];
            final int n2 = 0;
            index = i;
            byte[] decodingTable;
            int index2;
            for (i = n2; i < n; ++i, index = index2 + 1) {
                decodingTable = this.decodingTable;
                index2 = index + 1;
                index = (decodingTable[s.charAt(index)] << 4 | this.decodingTable[s.charAt(index2)]);
                if (index < 0) {
                    throw new IOException("invalid characters encountered in Hex string");
                }
                array[i] = (byte)index;
            }
            return array;
        }
        throw new IOException("a hexadecimal encoding must have an even number of characters");
    }
    
    @Override
    public int encode(final byte[] array, final int n, final int n2, final OutputStream outputStream) throws IOException {
        for (int i = n; i < n + n2; ++i) {
            final int n3 = array[i] & 0xFF;
            outputStream.write(this.encodingTable[n3 >>> 4]);
            outputStream.write(this.encodingTable[n3 & 0xF]);
        }
        return n2 * 2;
    }
    
    protected void initialiseDecodingTable() {
        final int n = 0;
        int n2 = 0;
        int n3;
        while (true) {
            final byte[] decodingTable = this.decodingTable;
            n3 = n;
            if (n2 >= decodingTable.length) {
                break;
            }
            decodingTable[n2] = -1;
            ++n2;
        }
        while (true) {
            final byte[] encodingTable = this.encodingTable;
            if (n3 >= encodingTable.length) {
                break;
            }
            this.decodingTable[encodingTable[n3]] = (byte)n3;
            ++n3;
        }
        final byte[] decodingTable2 = this.decodingTable;
        decodingTable2[65] = decodingTable2[97];
        decodingTable2[66] = decodingTable2[98];
        decodingTable2[67] = decodingTable2[99];
        decodingTable2[68] = decodingTable2[100];
        decodingTable2[69] = decodingTable2[101];
        decodingTable2[70] = decodingTable2[102];
    }
}
