// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util;

public class MemoableResetException extends ClassCastException
{
    public MemoableResetException(final String s) {
        super(s);
    }
}
