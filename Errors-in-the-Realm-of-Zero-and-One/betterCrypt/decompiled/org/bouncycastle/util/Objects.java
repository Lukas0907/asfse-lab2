// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util;

public class Objects
{
    public static boolean areEqual(final Object o, final Object obj) {
        return o == obj || (o != null && obj != null && o.equals(obj));
    }
    
    public static int hashCode(final Object o) {
        if (o == null) {
            return 0;
        }
        return o.hashCode();
    }
}
