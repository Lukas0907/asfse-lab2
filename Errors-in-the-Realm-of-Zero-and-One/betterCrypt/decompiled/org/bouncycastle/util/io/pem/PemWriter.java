// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util.io.pem;

import java.util.Iterator;
import java.io.IOException;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.Strings;
import java.io.Writer;
import java.io.BufferedWriter;

public class PemWriter extends BufferedWriter
{
    private static final int LINE_LENGTH = 64;
    private char[] buf;
    private final int nlLength;
    
    public PemWriter(final Writer out) {
        super(out);
        this.buf = new char[64];
        final String lineSeparator = Strings.lineSeparator();
        int length;
        if (lineSeparator != null) {
            length = lineSeparator.length();
        }
        else {
            length = 2;
        }
        this.nlLength = length;
    }
    
    private void writeEncoded(byte[] encode) throws IOException {
        encode = Base64.encode(encode);
        for (int i = 0; i < encode.length; i += this.buf.length) {
            int len = 0;
            while (true) {
                final char[] buf = this.buf;
                if (len == buf.length) {
                    break;
                }
                final int n = i + len;
                if (n >= encode.length) {
                    break;
                }
                buf[len] = (char)encode[n];
                ++len;
            }
            this.write(this.buf, 0, len);
            this.newLine();
        }
    }
    
    private void writePostEncapsulationBoundary(final String str) throws IOException {
        final StringBuilder sb = new StringBuilder();
        sb.append("-----END ");
        sb.append(str);
        sb.append("-----");
        this.write(sb.toString());
        this.newLine();
    }
    
    private void writePreEncapsulationBoundary(final String str) throws IOException {
        final StringBuilder sb = new StringBuilder();
        sb.append("-----BEGIN ");
        sb.append(str);
        sb.append("-----");
        this.write(sb.toString());
        this.newLine();
    }
    
    public int getOutputSize(final PemObject pemObject) {
        int n2;
        int n = n2 = (pemObject.getType().length() + 10 + this.nlLength) * 2 + 6 + 4;
        if (!pemObject.getHeaders().isEmpty()) {
            for (final PemHeader pemHeader : pemObject.getHeaders()) {
                n += pemHeader.getName().length() + 2 + pemHeader.getValue().length() + this.nlLength;
            }
            n2 = n + this.nlLength;
        }
        final int n3 = (pemObject.getContent().length + 2) / 3 * 4;
        return n2 + (n3 + (n3 + 64 - 1) / 64 * this.nlLength);
    }
    
    public void writeObject(final PemObjectGenerator pemObjectGenerator) throws IOException {
        final PemObject generate = pemObjectGenerator.generate();
        this.writePreEncapsulationBoundary(generate.getType());
        if (!generate.getHeaders().isEmpty()) {
            for (final PemHeader pemHeader : generate.getHeaders()) {
                this.write(pemHeader.getName());
                this.write(": ");
                this.write(pemHeader.getValue());
                this.newLine();
            }
            this.newLine();
        }
        this.writeEncoded(generate.getContent());
        this.writePostEncapsulationBoundary(generate.getType());
    }
}
