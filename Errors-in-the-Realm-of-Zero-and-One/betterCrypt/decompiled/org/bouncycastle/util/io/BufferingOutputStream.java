// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util.io;

import org.bouncycastle.util.Arrays;
import java.io.IOException;
import java.io.OutputStream;

public class BufferingOutputStream extends OutputStream
{
    private final byte[] buf;
    private int bufOff;
    private final OutputStream other;
    
    public BufferingOutputStream(final OutputStream other) {
        this.other = other;
        this.buf = new byte[4096];
    }
    
    public BufferingOutputStream(final OutputStream other, final int n) {
        this.other = other;
        this.buf = new byte[n];
    }
    
    @Override
    public void close() throws IOException {
        this.flush();
        this.other.close();
    }
    
    @Override
    public void flush() throws IOException {
        this.other.write(this.buf, 0, this.bufOff);
        this.bufOff = 0;
        Arrays.fill(this.buf, (byte)0);
    }
    
    @Override
    public void write(final int n) throws IOException {
        final byte[] buf = this.buf;
        buf[this.bufOff++] = (byte)n;
        if (this.bufOff == buf.length) {
            this.flush();
        }
    }
    
    @Override
    public void write(final byte[] b, int n, int off) throws IOException {
        final byte[] buf = this.buf;
        final int length = buf.length;
        final int bufOff = this.bufOff;
        if (off < length - bufOff) {
            System.arraycopy(b, n, buf, bufOff, off);
            n = off;
        }
        else {
            final int n2 = buf.length - bufOff;
            System.arraycopy(b, n, buf, bufOff, n2);
            this.bufOff += n2;
            this.flush();
            final int n3 = n + n2;
            n = off - n2;
            off = n3;
            byte[] buf2;
            while (true) {
                buf2 = this.buf;
                if (n < buf2.length) {
                    break;
                }
                this.other.write(b, off, buf2.length);
                final byte[] buf3 = this.buf;
                off += buf3.length;
                n -= buf3.length;
            }
            if (n <= 0) {
                return;
            }
            System.arraycopy(b, off, buf2, this.bufOff, n);
        }
        this.bufOff += n;
    }
}
