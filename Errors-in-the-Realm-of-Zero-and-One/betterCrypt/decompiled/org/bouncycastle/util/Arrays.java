// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.util;

import java.util.NoSuchElementException;
import java.util.Iterator;
import java.math.BigInteger;

public final class Arrays
{
    private Arrays() {
    }
    
    public static byte[] append(final byte[] array, final byte b) {
        if (array == null) {
            return new byte[] { b };
        }
        final int length = array.length;
        final byte[] array2 = new byte[length + 1];
        System.arraycopy(array, 0, array2, 0, length);
        array2[length] = b;
        return array2;
    }
    
    public static int[] append(final int[] array, final int n) {
        if (array == null) {
            return new int[] { n };
        }
        final int length = array.length;
        final int[] array2 = new int[length + 1];
        System.arraycopy(array, 0, array2, 0, length);
        array2[length] = n;
        return array2;
    }
    
    public static String[] append(final String[] array, final String s) {
        if (array == null) {
            return new String[] { s };
        }
        final int length = array.length;
        final String[] array2 = new String[length + 1];
        System.arraycopy(array, 0, array2, 0, length);
        array2[length] = s;
        return array2;
    }
    
    public static short[] append(final short[] array, final short n) {
        if (array == null) {
            return new short[] { n };
        }
        final int length = array.length;
        final short[] array2 = new short[length + 1];
        System.arraycopy(array, 0, array2, 0, length);
        array2[length] = n;
        return array2;
    }
    
    public static boolean areAllZeroes(final byte[] array, final int n, final int n2) {
        boolean b = false;
        int n3;
        for (int i = n3 = 0; i < n2; ++i) {
            n3 |= array[n + i];
        }
        if (n3 == 0) {
            b = true;
        }
        return b;
    }
    
    public static boolean areEqual(final byte[] a, final byte[] a2) {
        return java.util.Arrays.equals(a, a2);
    }
    
    public static boolean areEqual(final char[] a, final char[] a2) {
        return java.util.Arrays.equals(a, a2);
    }
    
    public static boolean areEqual(final int[] a, final int[] a2) {
        return java.util.Arrays.equals(a, a2);
    }
    
    public static boolean areEqual(final long[] a, final long[] a2) {
        return java.util.Arrays.equals(a, a2);
    }
    
    public static boolean areEqual(final Object[] a, final Object[] a2) {
        return java.util.Arrays.equals(a, a2);
    }
    
    public static boolean areEqual(final short[] a, final short[] a2) {
        return java.util.Arrays.equals(a, a2);
    }
    
    public static boolean areEqual(final boolean[] a, final boolean[] a2) {
        return java.util.Arrays.equals(a, a2);
    }
    
    public static void clear(final byte[] a) {
        if (a != null) {
            java.util.Arrays.fill(a, (byte)0);
        }
    }
    
    public static void clear(final int[] a) {
        if (a != null) {
            java.util.Arrays.fill(a, 0);
        }
    }
    
    public static byte[] clone(final byte[] array) {
        if (array == null) {
            return null;
        }
        return array.clone();
    }
    
    public static byte[] clone(final byte[] array, final byte[] array2) {
        if (array == null) {
            return null;
        }
        if (array2 != null && array2.length == array.length) {
            System.arraycopy(array, 0, array2, 0, array2.length);
            return array2;
        }
        return clone(array);
    }
    
    public static char[] clone(final char[] array) {
        if (array == null) {
            return null;
        }
        return array.clone();
    }
    
    public static int[] clone(final int[] array) {
        if (array == null) {
            return null;
        }
        return array.clone();
    }
    
    public static long[] clone(final long[] array) {
        if (array == null) {
            return null;
        }
        return array.clone();
    }
    
    public static long[] clone(final long[] array, final long[] array2) {
        if (array == null) {
            return null;
        }
        if (array2 != null && array2.length == array.length) {
            System.arraycopy(array, 0, array2, 0, array2.length);
            return array2;
        }
        return clone(array);
    }
    
    public static BigInteger[] clone(final BigInteger[] array) {
        if (array == null) {
            return null;
        }
        return array.clone();
    }
    
    public static short[] clone(final short[] array) {
        if (array == null) {
            return null;
        }
        return array.clone();
    }
    
    public static boolean[] clone(final boolean[] array) {
        if (array == null) {
            return null;
        }
        return array.clone();
    }
    
    public static byte[][] clone(final byte[][] array) {
        if (array == null) {
            return null;
        }
        final byte[][] array2 = new byte[array.length][];
        for (int i = 0; i != array2.length; ++i) {
            array2[i] = clone(array[i]);
        }
        return array2;
    }
    
    public static byte[][][] clone(final byte[][][] array) {
        if (array == null) {
            return null;
        }
        final byte[][][] array2 = new byte[array.length][][];
        for (int i = 0; i != array2.length; ++i) {
            array2[i] = clone(array[i]);
        }
        return array2;
    }
    
    public static int compareUnsigned(final byte[] array, final byte[] array2) {
        if (array == array2) {
            return 0;
        }
        if (array == null) {
            return -1;
        }
        if (array2 == null) {
            return 1;
        }
        for (int min = Math.min(array.length, array2.length), i = 0; i < min; ++i) {
            final int n = array[i] & 0xFF;
            final int n2 = array2[i] & 0xFF;
            if (n < n2) {
                return -1;
            }
            if (n > n2) {
                return 1;
            }
        }
        if (array.length < array2.length) {
            return -1;
        }
        if (array.length > array2.length) {
            return 1;
        }
        return 0;
    }
    
    public static byte[] concatenate(final byte[] array, final byte[] array2) {
        byte[] array3;
        if (array == null) {
            array3 = array2.clone();
        }
        else {
            if (array2 != null) {
                final byte[] array4 = new byte[array.length + array2.length];
                System.arraycopy(array, 0, array4, 0, array.length);
                System.arraycopy(array2, 0, array4, array.length, array2.length);
                return array4;
            }
            array3 = array.clone();
        }
        return array3;
    }
    
    public static byte[] concatenate(final byte[] array, final byte[] array2, final byte[] array3) {
        if (array == null) {
            return concatenate(array2, array3);
        }
        if (array2 == null) {
            return concatenate(array, array3);
        }
        if (array3 == null) {
            return concatenate(array, array2);
        }
        final byte[] array4 = new byte[array.length + array2.length + array3.length];
        System.arraycopy(array, 0, array4, 0, array.length);
        final int n = array.length + 0;
        System.arraycopy(array2, 0, array4, n, array2.length);
        System.arraycopy(array3, 0, array4, n + array2.length, array3.length);
        return array4;
    }
    
    public static byte[] concatenate(final byte[] array, final byte[] array2, final byte[] array3, final byte[] array4) {
        if (array == null) {
            return concatenate(array2, array3, array4);
        }
        if (array2 == null) {
            return concatenate(array, array3, array4);
        }
        if (array3 == null) {
            return concatenate(array, array2, array4);
        }
        if (array4 == null) {
            return concatenate(array, array2, array3);
        }
        final byte[] array5 = new byte[array.length + array2.length + array3.length + array4.length];
        System.arraycopy(array, 0, array5, 0, array.length);
        final int n = array.length + 0;
        System.arraycopy(array2, 0, array5, n, array2.length);
        final int n2 = n + array2.length;
        System.arraycopy(array3, 0, array5, n2, array3.length);
        System.arraycopy(array4, 0, array5, n2 + array3.length, array4.length);
        return array5;
    }
    
    public static byte[] concatenate(final byte[][] array) {
        int n;
        for (int i = n = 0; i != array.length; ++i) {
            n += array[i].length;
        }
        final byte[] array2 = new byte[n];
        int n2;
        for (int j = n2 = 0; j != array.length; ++j) {
            System.arraycopy(array[j], 0, array2, n2, array[j].length);
            n2 += array[j].length;
        }
        return array2;
    }
    
    public static int[] concatenate(final int[] array, final int[] array2) {
        int[] array3;
        if (array == null) {
            array3 = array2.clone();
        }
        else {
            if (array2 != null) {
                final int[] array4 = new int[array.length + array2.length];
                System.arraycopy(array, 0, array4, 0, array.length);
                System.arraycopy(array2, 0, array4, array.length, array2.length);
                return array4;
            }
            array3 = array.clone();
        }
        return array3;
    }
    
    public static boolean constantTimeAreEqual(final int n, final byte[] array, final int n2, final byte[] array2, final int n3) {
        if (array == null) {
            throw new NullPointerException("'a' cannot be null");
        }
        if (array2 == null) {
            throw new NullPointerException("'b' cannot be null");
        }
        if (n < 0) {
            throw new IllegalArgumentException("'len' cannot be negative");
        }
        if (n2 > array.length - n) {
            throw new IndexOutOfBoundsException("'aOff' value invalid for specified length");
        }
        if (n3 <= array2.length - n) {
            boolean b = false;
            int n4;
            for (int i = n4 = 0; i < n; ++i) {
                n4 |= (array[n2 + i] ^ array2[n3 + i]);
            }
            if (n4 == 0) {
                b = true;
            }
            return b;
        }
        throw new IndexOutOfBoundsException("'bOff' value invalid for specified length");
    }
    
    public static boolean constantTimeAreEqual(final byte[] array, final byte[] array2) {
        boolean b = false;
        if (array != null) {
            if (array2 == null) {
                return false;
            }
            if (array == array2) {
                return true;
            }
            int n;
            if (array.length < array2.length) {
                n = array.length;
            }
            else {
                n = array2.length;
            }
            int n2 = array.length ^ array2.length;
            int n3 = 0;
            int i;
            int n4;
            while (true) {
                i = n;
                n4 = n2;
                if (n3 == n) {
                    break;
                }
                n2 |= (array[n3] ^ array2[n3]);
                ++n3;
            }
            while (i < array2.length) {
                n4 |= (array2[i] ^ array2[i]);
                ++i;
            }
            b = b;
            if (n4 == 0) {
                b = true;
            }
        }
        return b;
    }
    
    public static boolean contains(final byte[] array, final byte b) {
        for (int i = 0; i < array.length; ++i) {
            if (array[i] == b) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean contains(final char[] array, final char c) {
        for (int i = 0; i < array.length; ++i) {
            if (array[i] == c) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean contains(final int[] array, final int n) {
        for (int i = 0; i < array.length; ++i) {
            if (array[i] == n) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean contains(final long[] array, final long n) {
        for (int i = 0; i < array.length; ++i) {
            if (array[i] == n) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean contains(final short[] array, final short n) {
        for (int i = 0; i < array.length; ++i) {
            if (array[i] == n) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean contains(final boolean[] array, final boolean b) {
        for (int i = 0; i < array.length; ++i) {
            if (array[i] == b) {
                return true;
            }
        }
        return false;
    }
    
    public static byte[] copyOf(final byte[] array, final int b) {
        final byte[] array2 = new byte[b];
        System.arraycopy(array, 0, array2, 0, Math.min(array.length, b));
        return array2;
    }
    
    public static char[] copyOf(final char[] array, final int b) {
        final char[] array2 = new char[b];
        System.arraycopy(array, 0, array2, 0, Math.min(array.length, b));
        return array2;
    }
    
    public static int[] copyOf(final int[] array, final int b) {
        final int[] array2 = new int[b];
        System.arraycopy(array, 0, array2, 0, Math.min(array.length, b));
        return array2;
    }
    
    public static long[] copyOf(final long[] array, final int b) {
        final long[] array2 = new long[b];
        System.arraycopy(array, 0, array2, 0, Math.min(array.length, b));
        return array2;
    }
    
    public static BigInteger[] copyOf(final BigInteger[] array, final int b) {
        final BigInteger[] array2 = new BigInteger[b];
        System.arraycopy(array, 0, array2, 0, Math.min(array.length, b));
        return array2;
    }
    
    public static short[] copyOf(final short[] array, final int b) {
        final short[] array2 = new short[b];
        System.arraycopy(array, 0, array2, 0, Math.min(array.length, b));
        return array2;
    }
    
    public static boolean[] copyOf(final boolean[] array, final int b) {
        final boolean[] array2 = new boolean[b];
        System.arraycopy(array, 0, array2, 0, Math.min(array.length, b));
        return array2;
    }
    
    public static byte[] copyOfRange(final byte[] array, final int n, int length) {
        length = getLength(n, length);
        final byte[] array2 = new byte[length];
        System.arraycopy(array, n, array2, 0, Math.min(array.length - n, length));
        return array2;
    }
    
    public static char[] copyOfRange(final char[] array, final int n, int length) {
        length = getLength(n, length);
        final char[] array2 = new char[length];
        System.arraycopy(array, n, array2, 0, Math.min(array.length - n, length));
        return array2;
    }
    
    public static int[] copyOfRange(final int[] array, final int n, int length) {
        length = getLength(n, length);
        final int[] array2 = new int[length];
        System.arraycopy(array, n, array2, 0, Math.min(array.length - n, length));
        return array2;
    }
    
    public static long[] copyOfRange(final long[] array, final int n, int length) {
        length = getLength(n, length);
        final long[] array2 = new long[length];
        System.arraycopy(array, n, array2, 0, Math.min(array.length - n, length));
        return array2;
    }
    
    public static BigInteger[] copyOfRange(final BigInteger[] array, final int n, int length) {
        length = getLength(n, length);
        final BigInteger[] array2 = new BigInteger[length];
        System.arraycopy(array, n, array2, 0, Math.min(array.length - n, length));
        return array2;
    }
    
    public static short[] copyOfRange(final short[] array, final int n, int length) {
        length = getLength(n, length);
        final short[] array2 = new short[length];
        System.arraycopy(array, n, array2, 0, Math.min(array.length - n, length));
        return array2;
    }
    
    public static boolean[] copyOfRange(final boolean[] array, final int n, int length) {
        length = getLength(n, length);
        final boolean[] array2 = new boolean[length];
        System.arraycopy(array, n, array2, 0, Math.min(array.length - n, length));
        return array2;
    }
    
    public static void fill(final byte[] a, final byte val) {
        java.util.Arrays.fill(a, val);
    }
    
    public static void fill(final byte[] array, final int n, final byte b) {
        fill(array, n, array.length, b);
    }
    
    public static void fill(final byte[] a, final int fromIndex, final int toIndex, final byte val) {
        java.util.Arrays.fill(a, fromIndex, toIndex, val);
    }
    
    public static void fill(final char[] a, final char val) {
        java.util.Arrays.fill(a, val);
    }
    
    public static void fill(final char[] a, final int fromIndex, final int toIndex, final char val) {
        java.util.Arrays.fill(a, fromIndex, toIndex, val);
    }
    
    public static void fill(final int[] a, final int val) {
        java.util.Arrays.fill(a, val);
    }
    
    public static void fill(final int[] a, final int fromIndex, final int val) {
        java.util.Arrays.fill(a, fromIndex, a.length, val);
    }
    
    public static void fill(final int[] a, final int fromIndex, final int toIndex, final int val) {
        java.util.Arrays.fill(a, fromIndex, toIndex, val);
    }
    
    public static void fill(final long[] a, final int fromIndex, final int toIndex, final long val) {
        java.util.Arrays.fill(a, fromIndex, toIndex, val);
    }
    
    public static void fill(final long[] a, final int fromIndex, final long val) {
        java.util.Arrays.fill(a, fromIndex, a.length, val);
    }
    
    public static void fill(final long[] a, final long val) {
        java.util.Arrays.fill(a, val);
    }
    
    public static void fill(final Object[] a, final int fromIndex, final int toIndex, final Object val) {
        java.util.Arrays.fill(a, fromIndex, toIndex, val);
    }
    
    public static void fill(final Object[] a, final Object val) {
        java.util.Arrays.fill(a, val);
    }
    
    public static void fill(final short[] a, final int fromIndex, final int toIndex, final short val) {
        java.util.Arrays.fill(a, fromIndex, toIndex, val);
    }
    
    public static void fill(final short[] a, final int fromIndex, final short val) {
        java.util.Arrays.fill(a, fromIndex, a.length, val);
    }
    
    public static void fill(final short[] a, final short val) {
        java.util.Arrays.fill(a, val);
    }
    
    public static void fill(final boolean[] a, final int fromIndex, final int toIndex, final boolean val) {
        java.util.Arrays.fill(a, fromIndex, toIndex, val);
    }
    
    public static void fill(final boolean[] a, final boolean val) {
        java.util.Arrays.fill(a, val);
    }
    
    private static int getLength(final int capacity, final int i) {
        final int n = i - capacity;
        if (n >= 0) {
            return n;
        }
        final StringBuffer sb = new StringBuffer(capacity);
        sb.append(" > ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static int hashCode(final byte[] array) {
        if (array == null) {
            return 0;
        }
        int length = array.length;
        int n = length + 1;
        while (true) {
            --length;
            if (length < 0) {
                break;
            }
            n = (n * 257 ^ array[length]);
        }
        return n;
    }
    
    public static int hashCode(final byte[] array, final int n, int n2) {
        if (array == null) {
            return 0;
        }
        final int n3 = n2 + 1;
        int n4 = n2;
        n2 = n3;
        while (true) {
            --n4;
            if (n4 < 0) {
                break;
            }
            n2 = (n2 * 257 ^ array[n + n4]);
        }
        return n2;
    }
    
    public static int hashCode(final char[] array) {
        if (array == null) {
            return 0;
        }
        int length = array.length;
        int n = length + 1;
        while (true) {
            --length;
            if (length < 0) {
                break;
            }
            n = (n * 257 ^ array[length]);
        }
        return n;
    }
    
    public static int hashCode(final int[] array) {
        if (array == null) {
            return 0;
        }
        int length = array.length;
        int n = length + 1;
        while (true) {
            --length;
            if (length < 0) {
                break;
            }
            n = (n * 257 ^ array[length]);
        }
        return n;
    }
    
    public static int hashCode(final int[] array, final int n, int n2) {
        if (array == null) {
            return 0;
        }
        final int n3 = n2 + 1;
        int n4 = n2;
        n2 = n3;
        while (true) {
            --n4;
            if (n4 < 0) {
                break;
            }
            n2 = (n2 * 257 ^ array[n + n4]);
        }
        return n2;
    }
    
    public static int hashCode(final long[] array) {
        if (array == null) {
            return 0;
        }
        int length = array.length;
        int n = length + 1;
        while (true) {
            --length;
            if (length < 0) {
                break;
            }
            final long n2 = array[length];
            n = ((n * 257 ^ (int)n2) * 257 ^ (int)(n2 >>> 32));
        }
        return n;
    }
    
    public static int hashCode(final long[] array, final int n, int n2) {
        if (array == null) {
            return 0;
        }
        final int n3 = n2 + 1;
        int n4 = n2;
        n2 = n3;
        while (true) {
            --n4;
            if (n4 < 0) {
                break;
            }
            final long n5 = array[n + n4];
            n2 = ((n2 * 257 ^ (int)n5) * 257 ^ (int)(n5 >>> 32));
        }
        return n2;
    }
    
    public static int hashCode(final Object[] array) {
        if (array == null) {
            return 0;
        }
        int length = array.length;
        int n = length + 1;
        while (true) {
            --length;
            if (length < 0) {
                break;
            }
            n = (n * 257 ^ Objects.hashCode(array[length]));
        }
        return n;
    }
    
    public static int hashCode(final short[] array) {
        if (array == null) {
            return 0;
        }
        int length = array.length;
        int n = length + 1;
        while (true) {
            --length;
            if (length < 0) {
                break;
            }
            n = (n * 257 ^ (array[length] & 0xFF));
        }
        return n;
    }
    
    public static int hashCode(final int[][] array) {
        int i = 0;
        int n = 0;
        while (i != array.length) {
            n = n * 257 + hashCode(array[i]);
            ++i;
        }
        return n;
    }
    
    public static int hashCode(final short[][] array) {
        int i = 0;
        int n = 0;
        while (i != array.length) {
            n = n * 257 + hashCode(array[i]);
            ++i;
        }
        return n;
    }
    
    public static int hashCode(final short[][][] array) {
        int i = 0;
        int n = 0;
        while (i != array.length) {
            n = n * 257 + hashCode(array[i]);
            ++i;
        }
        return n;
    }
    
    public static boolean isNullOrContainsNull(final Object[] array) {
        if (array == null) {
            return true;
        }
        for (int length = array.length, i = 0; i < length; ++i) {
            if (array[i] == null) {
                return true;
            }
        }
        return false;
    }
    
    public static byte[] prepend(final byte[] array, final byte b) {
        if (array == null) {
            return new byte[] { b };
        }
        final int length = array.length;
        final byte[] array2 = new byte[length + 1];
        System.arraycopy(array, 0, array2, 1, length);
        array2[0] = b;
        return array2;
    }
    
    public static int[] prepend(final int[] array, final int n) {
        if (array == null) {
            return new int[] { n };
        }
        final int length = array.length;
        final int[] array2 = new int[length + 1];
        System.arraycopy(array, 0, array2, 1, length);
        array2[0] = n;
        return array2;
    }
    
    public static short[] prepend(final short[] array, final short n) {
        if (array == null) {
            return new short[] { n };
        }
        final int length = array.length;
        final short[] array2 = new short[length + 1];
        System.arraycopy(array, 0, array2, 1, length);
        array2[0] = n;
        return array2;
    }
    
    public static byte[] reverse(final byte[] array) {
        if (array == null) {
            return null;
        }
        int n = 0;
        int length = array.length;
        final byte[] array2 = new byte[length];
        while (true) {
            --length;
            if (length < 0) {
                break;
            }
            array2[length] = array[n];
            ++n;
        }
        return array2;
    }
    
    public static int[] reverse(final int[] array) {
        if (array == null) {
            return null;
        }
        int n = 0;
        int length = array.length;
        final int[] array2 = new int[length];
        while (true) {
            --length;
            if (length < 0) {
                break;
            }
            array2[length] = array[n];
            ++n;
        }
        return array2;
    }
    
    public static class Iterator<T> implements java.util.Iterator<T>
    {
        private final T[] dataArray;
        private int position;
        
        public Iterator(final T[] dataArray) {
            this.position = 0;
            this.dataArray = dataArray;
        }
        
        @Override
        public boolean hasNext() {
            return this.position < this.dataArray.length;
        }
        
        @Override
        public T next() {
            final int position = this.position;
            final T[] dataArray = this.dataArray;
            if (position != dataArray.length) {
                this.position = position + 1;
                return dataArray[position];
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Out of elements: ");
            sb.append(this.position);
            throw new NoSuchElementException(sb.toString());
        }
        
        @Override
        public void remove() {
            throw new UnsupportedOperationException("Cannot remove element from an Array.");
        }
    }
}
