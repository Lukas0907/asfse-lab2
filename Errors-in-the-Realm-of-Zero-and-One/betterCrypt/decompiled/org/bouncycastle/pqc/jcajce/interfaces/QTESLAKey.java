// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.interfaces;

import org.bouncycastle.pqc.jcajce.spec.QTESLAParameterSpec;

public interface QTESLAKey
{
    QTESLAParameterSpec getParams();
}
