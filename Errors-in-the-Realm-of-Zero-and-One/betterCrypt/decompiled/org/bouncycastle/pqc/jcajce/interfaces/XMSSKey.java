// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.interfaces;

public interface XMSSKey
{
    int getHeight();
    
    String getTreeDigest();
}
