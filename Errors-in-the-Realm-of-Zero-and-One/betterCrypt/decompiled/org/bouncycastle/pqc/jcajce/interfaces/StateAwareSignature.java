// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.interfaces;

import java.nio.ByteBuffer;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.InvalidKeyException;
import java.security.PrivateKey;

public interface StateAwareSignature
{
    String getAlgorithm();
    
    PrivateKey getUpdatedPrivateKey();
    
    void initSign(final PrivateKey p0) throws InvalidKeyException;
    
    void initSign(final PrivateKey p0, final SecureRandom p1) throws InvalidKeyException;
    
    void initVerify(final PublicKey p0) throws InvalidKeyException;
    
    void initVerify(final Certificate p0) throws InvalidKeyException;
    
    boolean isSigningCapable();
    
    int sign(final byte[] p0, final int p1, final int p2) throws SignatureException;
    
    byte[] sign() throws SignatureException;
    
    void update(final byte p0) throws SignatureException;
    
    void update(final ByteBuffer p0) throws SignatureException;
    
    void update(final byte[] p0) throws SignatureException;
    
    void update(final byte[] p0, final int p1, final int p2) throws SignatureException;
    
    boolean verify(final byte[] p0) throws SignatureException;
    
    boolean verify(final byte[] p0, final int p1, final int p2) throws SignatureException;
}
