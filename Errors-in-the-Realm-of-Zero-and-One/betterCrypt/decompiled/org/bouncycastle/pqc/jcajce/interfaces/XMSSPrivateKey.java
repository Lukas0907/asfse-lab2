// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.interfaces;

import java.security.PrivateKey;

public interface XMSSPrivateKey extends XMSSKey, PrivateKey
{
    XMSSPrivateKey extractKeyShard(final int p0);
    
    long getUsagesRemaining();
}
