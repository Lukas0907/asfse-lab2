// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.mceliece;

import java.security.spec.PKCS8EncodedKeySpec;
import org.bouncycastle.pqc.crypto.mceliece.McElieceCCA2PrivateKeyParameters;
import org.bouncycastle.pqc.asn1.McElieceCCA2PrivateKey;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import java.security.InvalidKeyException;
import java.security.Key;
import java.io.IOException;
import org.bouncycastle.pqc.crypto.mceliece.McElieceCCA2PublicKeyParameters;
import org.bouncycastle.pqc.asn1.McElieceCCA2PublicKey;
import org.bouncycastle.pqc.asn1.PQCObjectIdentifiers;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.ASN1Primitive;
import java.security.spec.X509EncodedKeySpec;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.PrivateKey;
import java.security.spec.KeySpec;
import org.bouncycastle.jcajce.provider.util.AsymmetricKeyInfoConverter;
import java.security.KeyFactorySpi;

public class McElieceCCA2KeyFactorySpi extends KeyFactorySpi implements AsymmetricKeyInfoConverter
{
    public static final String OID = "1.3.6.1.4.1.8301.3.1.3.4.2";
    
    @Override
    protected PrivateKey engineGeneratePrivate(final KeySpec p0) throws InvalidKeySpecException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: instanceof      Ljava/security/spec/PKCS8EncodedKeySpec;
        //     4: ifeq            148
        //     7: aload_1        
        //     8: checkcast       Ljava/security/spec/PKCS8EncodedKeySpec;
        //    11: invokevirtual   java/security/spec/PKCS8EncodedKeySpec.getEncoded:()[B
        //    14: astore_1       
        //    15: aload_1        
        //    16: invokestatic    org/bouncycastle/asn1/ASN1Primitive.fromByteArray:([B)Lorg/bouncycastle/asn1/ASN1Primitive;
        //    19: invokestatic    org/bouncycastle/asn1/pkcs/PrivateKeyInfo.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/pkcs/PrivateKeyInfo;
        //    22: astore_1       
        //    23: getstatic       org/bouncycastle/pqc/asn1/PQCObjectIdentifiers.mcElieceCca2:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //    26: aload_1        
        //    27: invokevirtual   org/bouncycastle/asn1/pkcs/PrivateKeyInfo.getPrivateKeyAlgorithm:()Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //    30: invokevirtual   org/bouncycastle/asn1/x509/AlgorithmIdentifier.getAlgorithm:()Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //    33: invokevirtual   org/bouncycastle/asn1/ASN1ObjectIdentifier.equals:(Lorg/bouncycastle/asn1/ASN1Primitive;)Z
        //    36: ifeq            94
        //    39: aload_1        
        //    40: invokevirtual   org/bouncycastle/asn1/pkcs/PrivateKeyInfo.parsePrivateKey:()Lorg/bouncycastle/asn1/ASN1Encodable;
        //    43: invokestatic    org/bouncycastle/pqc/asn1/McElieceCCA2PrivateKey.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/pqc/asn1/McElieceCCA2PrivateKey;
        //    46: astore_1       
        //    47: new             Lorg/bouncycastle/pqc/jcajce/provider/mceliece/BCMcElieceCCA2PrivateKey;
        //    50: dup            
        //    51: new             Lorg/bouncycastle/pqc/crypto/mceliece/McElieceCCA2PrivateKeyParameters;
        //    54: dup            
        //    55: aload_1        
        //    56: invokevirtual   org/bouncycastle/pqc/asn1/McElieceCCA2PrivateKey.getN:()I
        //    59: aload_1        
        //    60: invokevirtual   org/bouncycastle/pqc/asn1/McElieceCCA2PrivateKey.getK:()I
        //    63: aload_1        
        //    64: invokevirtual   org/bouncycastle/pqc/asn1/McElieceCCA2PrivateKey.getField:()Lorg/bouncycastle/pqc/math/linearalgebra/GF2mField;
        //    67: aload_1        
        //    68: invokevirtual   org/bouncycastle/pqc/asn1/McElieceCCA2PrivateKey.getGoppaPoly:()Lorg/bouncycastle/pqc/math/linearalgebra/PolynomialGF2mSmallM;
        //    71: aload_1        
        //    72: invokevirtual   org/bouncycastle/pqc/asn1/McElieceCCA2PrivateKey.getP:()Lorg/bouncycastle/pqc/math/linearalgebra/Permutation;
        //    75: aload_1        
        //    76: invokevirtual   org/bouncycastle/pqc/asn1/McElieceCCA2PrivateKey.getDigest:()Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //    79: invokestatic    org/bouncycastle/pqc/jcajce/provider/mceliece/Utils.getDigest:(Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;)Lorg/bouncycastle/crypto/Digest;
        //    82: invokeinterface org/bouncycastle/crypto/Digest.getAlgorithmName:()Ljava/lang/String;
        //    87: invokespecial   org/bouncycastle/pqc/crypto/mceliece/McElieceCCA2PrivateKeyParameters.<init>:(IILorg/bouncycastle/pqc/math/linearalgebra/GF2mField;Lorg/bouncycastle/pqc/math/linearalgebra/PolynomialGF2mSmallM;Lorg/bouncycastle/pqc/math/linearalgebra/Permutation;Ljava/lang/String;)V
        //    90: invokespecial   org/bouncycastle/pqc/jcajce/provider/mceliece/BCMcElieceCCA2PrivateKey.<init>:(Lorg/bouncycastle/pqc/crypto/mceliece/McElieceCCA2PrivateKeyParameters;)V
        //    93: areturn        
        //    94: new             Ljava/security/spec/InvalidKeySpecException;
        //    97: dup            
        //    98: ldc             "Unable to recognise OID in McEliece public key"
        //   100: invokespecial   java/security/spec/InvalidKeySpecException.<init>:(Ljava/lang/String;)V
        //   103: athrow         
        //   104: new             Ljava/security/spec/InvalidKeySpecException;
        //   107: dup            
        //   108: ldc             "Unable to decode PKCS8EncodedKeySpec."
        //   110: invokespecial   java/security/spec/InvalidKeySpecException.<init>:(Ljava/lang/String;)V
        //   113: athrow         
        //   114: astore_1       
        //   115: new             Ljava/lang/StringBuilder;
        //   118: dup            
        //   119: invokespecial   java/lang/StringBuilder.<init>:()V
        //   122: astore_2       
        //   123: aload_2        
        //   124: ldc             "Unable to decode PKCS8EncodedKeySpec: "
        //   126: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   129: pop            
        //   130: aload_2        
        //   131: aload_1        
        //   132: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   135: pop            
        //   136: new             Ljava/security/spec/InvalidKeySpecException;
        //   139: dup            
        //   140: aload_2        
        //   141: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   144: invokespecial   java/security/spec/InvalidKeySpecException.<init>:(Ljava/lang/String;)V
        //   147: athrow         
        //   148: new             Ljava/lang/StringBuilder;
        //   151: dup            
        //   152: invokespecial   java/lang/StringBuilder.<init>:()V
        //   155: astore_2       
        //   156: aload_2        
        //   157: ldc             "Unsupported key specification: "
        //   159: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   162: pop            
        //   163: aload_2        
        //   164: aload_1        
        //   165: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //   168: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   171: pop            
        //   172: aload_2        
        //   173: ldc             "."
        //   175: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   178: pop            
        //   179: new             Ljava/security/spec/InvalidKeySpecException;
        //   182: dup            
        //   183: aload_2        
        //   184: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   187: invokespecial   java/security/spec/InvalidKeySpecException.<init>:(Ljava/lang/String;)V
        //   190: athrow         
        //   191: astore_1       
        //   192: goto            104
        //    Exceptions:
        //  throws java.security.spec.InvalidKeySpecException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  15     23     114    148    Ljava/io/IOException;
        //  23     94     191    114    Ljava/io/IOException;
        //  94     104    191    114    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0094:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    protected PublicKey engineGeneratePublic(final KeySpec keySpec) throws InvalidKeySpecException {
        if (keySpec instanceof X509EncodedKeySpec) {
            final byte[] encoded = ((X509EncodedKeySpec)keySpec).getEncoded();
            try {
                final SubjectPublicKeyInfo instance = SubjectPublicKeyInfo.getInstance(ASN1Primitive.fromByteArray(encoded));
                try {
                    if (PQCObjectIdentifiers.mcElieceCca2.equals(instance.getAlgorithm().getAlgorithm())) {
                        final McElieceCCA2PublicKey instance2 = McElieceCCA2PublicKey.getInstance(instance.parsePublicKey());
                        return new BCMcElieceCCA2PublicKey(new McElieceCCA2PublicKeyParameters(instance2.getN(), instance2.getT(), instance2.getG(), Utils.getDigest(instance2.getDigest()).getAlgorithmName()));
                    }
                    throw new InvalidKeySpecException("Unable to recognise OID in McEliece private key");
                }
                catch (IOException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unable to decode X509EncodedKeySpec: ");
                    sb.append(ex.getMessage());
                    throw new InvalidKeySpecException(sb.toString());
                }
            }
            catch (IOException ex2) {
                throw new InvalidKeySpecException(ex2.toString());
            }
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Unsupported key specification: ");
        sb2.append(keySpec.getClass());
        sb2.append(".");
        throw new InvalidKeySpecException(sb2.toString());
    }
    
    @Override
    protected KeySpec engineGetKeySpec(final Key key, final Class clazz) throws InvalidKeySpecException {
        return null;
    }
    
    @Override
    protected Key engineTranslateKey(final Key key) throws InvalidKeyException {
        return null;
    }
    
    @Override
    public PrivateKey generatePrivate(final PrivateKeyInfo privateKeyInfo) throws IOException {
        final McElieceCCA2PrivateKey instance = McElieceCCA2PrivateKey.getInstance(privateKeyInfo.parsePrivateKey().toASN1Primitive());
        return new BCMcElieceCCA2PrivateKey(new McElieceCCA2PrivateKeyParameters(instance.getN(), instance.getK(), instance.getField(), instance.getGoppaPoly(), instance.getP(), null));
    }
    
    @Override
    public PublicKey generatePublic(final SubjectPublicKeyInfo subjectPublicKeyInfo) throws IOException {
        final McElieceCCA2PublicKey instance = McElieceCCA2PublicKey.getInstance(subjectPublicKeyInfo.parsePublicKey());
        return new BCMcElieceCCA2PublicKey(new McElieceCCA2PublicKeyParameters(instance.getN(), instance.getT(), instance.getG(), Utils.getDigest(instance.getDigest()).getAlgorithmName()));
    }
    
    public KeySpec getKeySpec(final Key key, final Class obj) throws InvalidKeySpecException {
        if (key instanceof BCMcElieceCCA2PrivateKey) {
            if (PKCS8EncodedKeySpec.class.isAssignableFrom(obj)) {
                return new PKCS8EncodedKeySpec(key.getEncoded());
            }
        }
        else {
            if (!(key instanceof BCMcElieceCCA2PublicKey)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unsupported key type: ");
                sb.append(key.getClass());
                sb.append(".");
                throw new InvalidKeySpecException(sb.toString());
            }
            if (X509EncodedKeySpec.class.isAssignableFrom(obj)) {
                return new X509EncodedKeySpec(key.getEncoded());
            }
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Unknown key specification: ");
        sb2.append(obj);
        sb2.append(".");
        throw new InvalidKeySpecException(sb2.toString());
    }
    
    public Key translateKey(final Key key) throws InvalidKeyException {
        if (key instanceof BCMcElieceCCA2PrivateKey) {
            return key;
        }
        if (key instanceof BCMcElieceCCA2PublicKey) {
            return key;
        }
        throw new InvalidKeyException("Unsupported key type.");
    }
}
