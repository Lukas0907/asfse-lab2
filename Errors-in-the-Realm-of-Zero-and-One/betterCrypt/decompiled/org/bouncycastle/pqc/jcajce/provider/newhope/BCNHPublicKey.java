// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.newhope;

import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.pqc.crypto.util.SubjectPublicKeyInfoFactory;
import org.bouncycastle.util.Arrays;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import org.bouncycastle.pqc.crypto.util.PublicKeyFactory;
import java.io.IOException;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.pqc.crypto.newhope.NHPublicKeyParameters;
import org.bouncycastle.pqc.jcajce.interfaces.NHPublicKey;

public class BCNHPublicKey implements NHPublicKey
{
    private static final long serialVersionUID = 1L;
    private transient NHPublicKeyParameters params;
    
    public BCNHPublicKey(final SubjectPublicKeyInfo subjectPublicKeyInfo) throws IOException {
        this.init(subjectPublicKeyInfo);
    }
    
    public BCNHPublicKey(final NHPublicKeyParameters params) {
        this.params = params;
    }
    
    private void init(final SubjectPublicKeyInfo subjectPublicKeyInfo) throws IOException {
        this.params = (NHPublicKeyParameters)PublicKeyFactory.createKey(subjectPublicKeyInfo);
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.init(SubjectPublicKeyInfo.getInstance(objectInputStream.readObject()));
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.getEncoded());
    }
    
    @Override
    public boolean equals(final Object o) {
        return o != null && o instanceof BCNHPublicKey && Arrays.areEqual(this.params.getPubData(), ((BCNHPublicKey)o).params.getPubData());
    }
    
    @Override
    public final String getAlgorithm() {
        return "NH";
    }
    
    @Override
    public byte[] getEncoded() {
        try {
            return SubjectPublicKeyInfoFactory.createSubjectPublicKeyInfo(this.params).getEncoded();
        }
        catch (IOException ex) {
            return null;
        }
    }
    
    @Override
    public String getFormat() {
        return "X.509";
    }
    
    CipherParameters getKeyParams() {
        return this.params;
    }
    
    @Override
    public byte[] getPublicData() {
        return this.params.getPubData();
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.params.getPubData());
    }
}
