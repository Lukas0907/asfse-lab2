// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.gmss;

import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.pqc.jcajce.provider.util.KeyUtil;
import org.bouncycastle.pqc.asn1.GMSSPublicKey;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.pqc.asn1.ParSet;
import org.bouncycastle.pqc.asn1.PQCObjectIdentifiers;
import org.bouncycastle.pqc.crypto.gmss.GMSSPublicKeyParameters;
import org.bouncycastle.pqc.crypto.gmss.GMSSParameters;
import java.security.PublicKey;
import org.bouncycastle.crypto.CipherParameters;

public class BCGMSSPublicKey implements CipherParameters, PublicKey
{
    private static final long serialVersionUID = 1L;
    private GMSSParameters gmssParameterSet;
    private GMSSParameters gmssParams;
    private byte[] publicKeyBytes;
    
    public BCGMSSPublicKey(final GMSSPublicKeyParameters gmssPublicKeyParameters) {
        this(gmssPublicKeyParameters.getPublicKey(), gmssPublicKeyParameters.getParameters());
    }
    
    public BCGMSSPublicKey(final byte[] publicKeyBytes, final GMSSParameters gmssParameterSet) {
        this.gmssParameterSet = gmssParameterSet;
        this.publicKeyBytes = publicKeyBytes;
    }
    
    @Override
    public String getAlgorithm() {
        return "GMSS";
    }
    
    @Override
    public byte[] getEncoded() {
        return KeyUtil.getEncodedSubjectPublicKeyInfo(new AlgorithmIdentifier(PQCObjectIdentifiers.gmss, new ParSet(this.gmssParameterSet.getNumOfLayers(), this.gmssParameterSet.getHeightOfTrees(), this.gmssParameterSet.getWinternitzParameter(), this.gmssParameterSet.getK()).toASN1Primitive()), new GMSSPublicKey(this.publicKeyBytes));
    }
    
    @Override
    public String getFormat() {
        return "X.509";
    }
    
    public GMSSParameters getParameterSet() {
        return this.gmssParameterSet;
    }
    
    public byte[] getPublicKeyBytes() {
        return this.publicKeyBytes;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("GMSS public key : ");
        sb.append(new String(Hex.encode(this.publicKeyBytes)));
        sb.append("\nHeight of Trees: \n");
        String str = sb.toString();
        for (int i = 0; i < this.gmssParameterSet.getHeightOfTrees().length; ++i) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append("Layer ");
            sb2.append(i);
            sb2.append(" : ");
            sb2.append(this.gmssParameterSet.getHeightOfTrees()[i]);
            sb2.append(" WinternitzParameter: ");
            sb2.append(this.gmssParameterSet.getWinternitzParameter()[i]);
            sb2.append(" K: ");
            sb2.append(this.gmssParameterSet.getK()[i]);
            sb2.append("\n");
            str = sb2.toString();
        }
        return str;
    }
}
