// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.sphincs;

import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.pqc.asn1.PQCObjectIdentifiers;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.pqc.crypto.util.SubjectPublicKeyInfoFactory;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.asn1.ASN1Primitive;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import org.bouncycastle.pqc.crypto.util.PublicKeyFactory;
import org.bouncycastle.pqc.asn1.SPHINCS256KeyParams;
import java.io.IOException;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.pqc.crypto.sphincs.SPHINCSPublicKeyParameters;
import org.bouncycastle.pqc.jcajce.interfaces.SPHINCSKey;
import java.security.PublicKey;

public class BCSphincs256PublicKey implements PublicKey, SPHINCSKey
{
    private static final long serialVersionUID = 1L;
    private transient SPHINCSPublicKeyParameters params;
    private transient ASN1ObjectIdentifier treeDigest;
    
    public BCSphincs256PublicKey(final ASN1ObjectIdentifier treeDigest, final SPHINCSPublicKeyParameters params) {
        this.treeDigest = treeDigest;
        this.params = params;
    }
    
    public BCSphincs256PublicKey(final SubjectPublicKeyInfo subjectPublicKeyInfo) throws IOException {
        this.init(subjectPublicKeyInfo);
    }
    
    private void init(final SubjectPublicKeyInfo subjectPublicKeyInfo) throws IOException {
        this.treeDigest = SPHINCS256KeyParams.getInstance(subjectPublicKeyInfo.getAlgorithm().getParameters()).getTreeDigest().getAlgorithm();
        this.params = (SPHINCSPublicKeyParameters)PublicKeyFactory.createKey(subjectPublicKeyInfo);
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.init(SubjectPublicKeyInfo.getInstance(objectInputStream.readObject()));
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.getEncoded());
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof BCSphincs256PublicKey) {
            final BCSphincs256PublicKey bcSphincs256PublicKey = (BCSphincs256PublicKey)o;
            return this.treeDigest.equals(bcSphincs256PublicKey.treeDigest) && Arrays.areEqual(this.params.getKeyData(), bcSphincs256PublicKey.params.getKeyData());
        }
        return false;
    }
    
    @Override
    public final String getAlgorithm() {
        return "SPHINCS-256";
    }
    
    @Override
    public byte[] getEncoded() {
        try {
            SubjectPublicKeyInfo subjectPublicKeyInfo;
            if (this.params.getTreeDigest() != null) {
                subjectPublicKeyInfo = SubjectPublicKeyInfoFactory.createSubjectPublicKeyInfo(this.params);
            }
            else {
                subjectPublicKeyInfo = new SubjectPublicKeyInfo(new AlgorithmIdentifier(PQCObjectIdentifiers.sphincs256, new SPHINCS256KeyParams(new AlgorithmIdentifier(this.treeDigest))), this.params.getKeyData());
            }
            return subjectPublicKeyInfo.getEncoded();
        }
        catch (IOException ex) {
            return null;
        }
    }
    
    @Override
    public String getFormat() {
        return "X.509";
    }
    
    @Override
    public byte[] getKeyData() {
        return this.params.getKeyData();
    }
    
    CipherParameters getKeyParams() {
        return this.params;
    }
    
    ASN1ObjectIdentifier getTreeDigest() {
        return this.treeDigest;
    }
    
    @Override
    public int hashCode() {
        return this.treeDigest.hashCode() + Arrays.hashCode(this.params.getKeyData()) * 37;
    }
}
