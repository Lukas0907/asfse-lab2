// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.mceliece;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import java.security.SecureRandom;
import java.security.InvalidAlgorithmParameterException;
import org.bouncycastle.crypto.CipherParameters;
import java.security.spec.AlgorithmParameterSpec;
import java.security.InvalidKeyException;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.pqc.crypto.mceliece.McElieceKeyParameters;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Key;
import org.bouncycastle.pqc.crypto.mceliece.McElieceCipher;
import org.bouncycastle.asn1.x509.X509ObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.pqc.jcajce.provider.util.AsymmetricBlockCipher;

public class McEliecePKCSCipherSpi extends AsymmetricBlockCipher implements PKCSObjectIdentifiers, X509ObjectIdentifiers
{
    private McElieceCipher cipher;
    
    public McEliecePKCSCipherSpi(final McElieceCipher cipher) {
        this.cipher = cipher;
    }
    
    @Override
    public int getKeySize(final Key key) throws InvalidKeyException {
        AsymmetricKeyParameter asymmetricKeyParameter;
        if (key instanceof PublicKey) {
            asymmetricKeyParameter = McElieceKeysToParams.generatePublicKeyParameter((PublicKey)key);
        }
        else {
            asymmetricKeyParameter = McElieceKeysToParams.generatePrivateKeyParameter((PrivateKey)key);
        }
        return this.cipher.getKeySize((McElieceKeyParameters)asymmetricKeyParameter);
    }
    
    @Override
    public String getName() {
        return "McEliecePKCS";
    }
    
    @Override
    protected void initCipherDecrypt(final Key key, final AlgorithmParameterSpec algorithmParameterSpec) throws InvalidKeyException, InvalidAlgorithmParameterException {
        this.cipher.init(false, McElieceKeysToParams.generatePrivateKeyParameter((PrivateKey)key));
        this.maxPlainTextSize = this.cipher.maxPlainTextSize;
        this.cipherTextSize = this.cipher.cipherTextSize;
    }
    
    @Override
    protected void initCipherEncrypt(final Key key, final AlgorithmParameterSpec algorithmParameterSpec, final SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        this.cipher.init(true, new ParametersWithRandom(McElieceKeysToParams.generatePublicKeyParameter((PublicKey)key), secureRandom));
        this.maxPlainTextSize = this.cipher.maxPlainTextSize;
        this.cipherTextSize = this.cipher.cipherTextSize;
    }
    
    @Override
    protected byte[] messageDecrypt(byte[] messageDecrypt) throws IllegalBlockSizeException, BadPaddingException {
        try {
            messageDecrypt = this.cipher.messageDecrypt(messageDecrypt);
            return messageDecrypt;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    @Override
    protected byte[] messageEncrypt(byte[] messageEncrypt) throws IllegalBlockSizeException, BadPaddingException {
        try {
            messageEncrypt = this.cipher.messageEncrypt(messageEncrypt);
            return messageEncrypt;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public static class McEliecePKCS extends McEliecePKCSCipherSpi
    {
        public McEliecePKCS() {
            super(new McElieceCipher());
        }
    }
}
