// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.xmss;

import org.bouncycastle.crypto.digests.SHAKEDigest;
import org.bouncycastle.crypto.digests.SHA512Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.digests.NullDigest;
import org.bouncycastle.pqc.crypto.xmss.XMSSMTPrivateKeyParameters;
import java.security.SignatureException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.PublicKey;
import org.bouncycastle.crypto.CipherParameters;
import java.security.InvalidKeyException;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import java.security.PrivateKey;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.pqc.crypto.xmss.XMSSMTSigner;
import java.security.SecureRandom;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.pqc.jcajce.interfaces.StateAwareSignature;
import java.security.Signature;

public class XMSSMTSignatureSpi extends Signature implements StateAwareSignature
{
    private Digest digest;
    private SecureRandom random;
    private XMSSMTSigner signer;
    private ASN1ObjectIdentifier treeDigest;
    
    protected XMSSMTSignatureSpi(final String algorithm) {
        super(algorithm);
    }
    
    protected XMSSMTSignatureSpi(final String algorithm, final Digest digest, final XMSSMTSigner signer) {
        super(algorithm);
        this.digest = digest;
        this.signer = signer;
    }
    
    @Override
    protected Object engineGetParameter(final String s) {
        throw new UnsupportedOperationException("engineSetParameter unsupported");
    }
    
    @Override
    protected void engineInitSign(final PrivateKey privateKey) throws InvalidKeyException {
        if (privateKey instanceof BCXMSSMTPrivateKey) {
            final BCXMSSMTPrivateKey bcxmssmtPrivateKey = (BCXMSSMTPrivateKey)privateKey;
            final CipherParameters keyParams = bcxmssmtPrivateKey.getKeyParams();
            this.treeDigest = bcxmssmtPrivateKey.getTreeDigestOID();
            final SecureRandom random = this.random;
            CipherParameters cipherParameters = keyParams;
            if (random != null) {
                cipherParameters = new ParametersWithRandom(keyParams, random);
            }
            this.digest.reset();
            this.signer.init(true, cipherParameters);
            return;
        }
        throw new InvalidKeyException("unknown private key passed to XMSSMT");
    }
    
    @Override
    protected void engineInitSign(final PrivateKey privateKey, final SecureRandom random) throws InvalidKeyException {
        this.random = random;
        this.engineInitSign(privateKey);
    }
    
    @Override
    protected void engineInitVerify(final PublicKey publicKey) throws InvalidKeyException {
        if (publicKey instanceof BCXMSSMTPublicKey) {
            final CipherParameters keyParams = ((BCXMSSMTPublicKey)publicKey).getKeyParams();
            this.treeDigest = null;
            this.digest.reset();
            this.signer.init(false, keyParams);
            return;
        }
        throw new InvalidKeyException("unknown public key passed to XMSSMT");
    }
    
    @Override
    protected void engineSetParameter(final String s, final Object o) {
        throw new UnsupportedOperationException("engineSetParameter unsupported");
    }
    
    @Override
    protected void engineSetParameter(final AlgorithmParameterSpec algorithmParameterSpec) {
        throw new UnsupportedOperationException("engineSetParameter unsupported");
    }
    
    @Override
    protected byte[] engineSign() throws SignatureException {
        final byte[] digestResult = DigestUtil.getDigestResult(this.digest);
        try {
            return this.signer.generateSignature(digestResult);
        }
        catch (Exception cause) {
            if (cause instanceof IllegalStateException) {
                throw new SignatureException(cause.getMessage(), cause);
            }
            throw new SignatureException(cause.toString());
        }
    }
    
    @Override
    protected void engineUpdate(final byte b) throws SignatureException {
        this.digest.update(b);
    }
    
    @Override
    protected void engineUpdate(final byte[] array, final int n, final int n2) throws SignatureException {
        this.digest.update(array, n, n2);
    }
    
    @Override
    protected boolean engineVerify(final byte[] array) throws SignatureException {
        return this.signer.verifySignature(DigestUtil.getDigestResult(this.digest), array);
    }
    
    @Override
    public PrivateKey getUpdatedPrivateKey() {
        final ASN1ObjectIdentifier treeDigest = this.treeDigest;
        if (treeDigest != null) {
            final BCXMSSMTPrivateKey bcxmssmtPrivateKey = new BCXMSSMTPrivateKey(treeDigest, (XMSSMTPrivateKeyParameters)this.signer.getUpdatedPrivateKey());
            this.treeDigest = null;
            return bcxmssmtPrivateKey;
        }
        throw new IllegalStateException("signature object not in a signing state");
    }
    
    @Override
    public boolean isSigningCapable() {
        return this.treeDigest != null && this.signer.getUsagesRemaining() != 0L;
    }
    
    public static class withSha256 extends XMSSMTSignatureSpi
    {
        public withSha256() {
            super("XMSSMT-SHA256", new NullDigest(), new XMSSMTSigner());
        }
    }
    
    public static class withSha256andPrehash extends XMSSMTSignatureSpi
    {
        public withSha256andPrehash() {
            super("SHA256withXMSSMT-SHA256", new SHA256Digest(), new XMSSMTSigner());
        }
    }
    
    public static class withSha512 extends XMSSMTSignatureSpi
    {
        public withSha512() {
            super("XMSSMT-SHA512", new NullDigest(), new XMSSMTSigner());
        }
    }
    
    public static class withSha512andPrehash extends XMSSMTSignatureSpi
    {
        public withSha512andPrehash() {
            super("SHA512withXMSSMT-SHA512", new SHA512Digest(), new XMSSMTSigner());
        }
    }
    
    public static class withShake128 extends XMSSMTSignatureSpi
    {
        public withShake128() {
            super("XMSSMT-SHAKE128", new NullDigest(), new XMSSMTSigner());
        }
    }
    
    public static class withShake128andPrehash extends XMSSMTSignatureSpi
    {
        public withShake128andPrehash() {
            super("SHAKE128withXMSSMT-SHAKE128", new SHAKEDigest(128), new XMSSMTSigner());
        }
    }
    
    public static class withShake256 extends XMSSMTSignatureSpi
    {
        public withShake256() {
            super("XMSSMT-SHAKE256", new NullDigest(), new XMSSMTSigner());
        }
    }
    
    public static class withShake256andPrehash extends XMSSMTSignatureSpi
    {
        public withShake256andPrehash() {
            super("SHAKE256withXMSSMT-SHAKE256", new SHAKEDigest(256), new XMSSMTSigner());
        }
    }
}
