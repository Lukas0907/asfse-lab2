// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.qtesla;

import org.bouncycastle.pqc.jcajce.spec.QTESLAParameterSpec;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.pqc.crypto.util.PrivateKeyInfoFactory;
import org.bouncycastle.pqc.crypto.qtesla.QTESLASecurityCategory;
import org.bouncycastle.util.Arrays;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import org.bouncycastle.pqc.crypto.util.PrivateKeyFactory;
import java.io.IOException;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.pqc.crypto.qtesla.QTESLAPrivateKeyParameters;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.pqc.jcajce.interfaces.QTESLAKey;
import java.security.PrivateKey;

public class BCqTESLAPrivateKey implements PrivateKey, QTESLAKey
{
    private static final long serialVersionUID = 1L;
    private transient ASN1Set attributes;
    private transient QTESLAPrivateKeyParameters keyParams;
    
    public BCqTESLAPrivateKey(final PrivateKeyInfo privateKeyInfo) throws IOException {
        this.init(privateKeyInfo);
    }
    
    public BCqTESLAPrivateKey(final QTESLAPrivateKeyParameters keyParams) {
        this.keyParams = keyParams;
    }
    
    private void init(final PrivateKeyInfo privateKeyInfo) throws IOException {
        this.attributes = privateKeyInfo.getAttributes();
        this.keyParams = (QTESLAPrivateKeyParameters)PrivateKeyFactory.createKey(privateKeyInfo);
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.init(PrivateKeyInfo.getInstance(objectInputStream.readObject()));
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.getEncoded());
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof BCqTESLAPrivateKey) {
            final BCqTESLAPrivateKey bCqTESLAPrivateKey = (BCqTESLAPrivateKey)o;
            return this.keyParams.getSecurityCategory() == bCqTESLAPrivateKey.keyParams.getSecurityCategory() && Arrays.areEqual(this.keyParams.getSecret(), bCqTESLAPrivateKey.keyParams.getSecret());
        }
        return false;
    }
    
    @Override
    public final String getAlgorithm() {
        return QTESLASecurityCategory.getName(this.keyParams.getSecurityCategory());
    }
    
    @Override
    public byte[] getEncoded() {
        try {
            return PrivateKeyInfoFactory.createPrivateKeyInfo(this.keyParams, this.attributes).getEncoded();
        }
        catch (IOException ex) {
            return null;
        }
    }
    
    @Override
    public String getFormat() {
        return "PKCS#8";
    }
    
    CipherParameters getKeyParams() {
        return this.keyParams;
    }
    
    @Override
    public QTESLAParameterSpec getParams() {
        return new QTESLAParameterSpec(this.getAlgorithm());
    }
    
    @Override
    public int hashCode() {
        return this.keyParams.getSecurityCategory() + Arrays.hashCode(this.keyParams.getSecret()) * 37;
    }
}
