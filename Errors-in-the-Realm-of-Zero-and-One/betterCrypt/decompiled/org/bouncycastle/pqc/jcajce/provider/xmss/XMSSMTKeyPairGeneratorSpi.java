// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.xmss;

import java.security.InvalidAlgorithmParameterException;
import org.bouncycastle.crypto.digests.SHAKEDigest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.pqc.jcajce.spec.XMSSMTParameterSpec;
import java.security.spec.AlgorithmParameterSpec;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import org.bouncycastle.pqc.crypto.xmss.XMSSMTPrivateKeyParameters;
import org.bouncycastle.pqc.crypto.xmss.XMSSMTPublicKeyParameters;
import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.pqc.crypto.xmss.XMSSMTParameters;
import org.bouncycastle.crypto.digests.SHA512Digest;
import java.security.KeyPair;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.security.SecureRandom;
import org.bouncycastle.pqc.crypto.xmss.XMSSMTKeyGenerationParameters;
import org.bouncycastle.pqc.crypto.xmss.XMSSMTKeyPairGenerator;
import java.security.KeyPairGenerator;

public class XMSSMTKeyPairGeneratorSpi extends KeyPairGenerator
{
    private XMSSMTKeyPairGenerator engine;
    private boolean initialised;
    private XMSSMTKeyGenerationParameters param;
    private SecureRandom random;
    private ASN1ObjectIdentifier treeDigest;
    
    public XMSSMTKeyPairGeneratorSpi() {
        super("XMSSMT");
        this.engine = new XMSSMTKeyPairGenerator();
        this.random = CryptoServicesRegistrar.getSecureRandom();
        this.initialised = false;
    }
    
    @Override
    public KeyPair generateKeyPair() {
        if (!this.initialised) {
            this.param = new XMSSMTKeyGenerationParameters(new XMSSMTParameters(10, 20, new SHA512Digest()), this.random);
            this.engine.init(this.param);
            this.initialised = true;
        }
        final AsymmetricCipherKeyPair generateKeyPair = this.engine.generateKeyPair();
        return new KeyPair(new BCXMSSMTPublicKey(this.treeDigest, (XMSSMTPublicKeyParameters)generateKeyPair.getPublic()), new BCXMSSMTPrivateKey(this.treeDigest, (XMSSMTPrivateKeyParameters)generateKeyPair.getPrivate()));
    }
    
    @Override
    public void initialize(final int n, final SecureRandom secureRandom) {
        throw new IllegalArgumentException("use AlgorithmParameterSpec");
    }
    
    @Override
    public void initialize(final AlgorithmParameterSpec algorithmParameterSpec, final SecureRandom secureRandom) throws InvalidAlgorithmParameterException {
        if (algorithmParameterSpec instanceof XMSSMTParameterSpec) {
            final XMSSMTParameterSpec xmssmtParameterSpec = (XMSSMTParameterSpec)algorithmParameterSpec;
            Label_0235: {
                XMSSMTKeyGenerationParameters param;
                if (xmssmtParameterSpec.getTreeDigest().equals("SHA256")) {
                    this.treeDigest = NISTObjectIdentifiers.id_sha256;
                    param = new XMSSMTKeyGenerationParameters(new XMSSMTParameters(xmssmtParameterSpec.getHeight(), xmssmtParameterSpec.getLayers(), new SHA256Digest()), secureRandom);
                }
                else if (xmssmtParameterSpec.getTreeDigest().equals("SHA512")) {
                    this.treeDigest = NISTObjectIdentifiers.id_sha512;
                    param = new XMSSMTKeyGenerationParameters(new XMSSMTParameters(xmssmtParameterSpec.getHeight(), xmssmtParameterSpec.getLayers(), new SHA512Digest()), secureRandom);
                }
                else if (xmssmtParameterSpec.getTreeDigest().equals("SHAKE128")) {
                    this.treeDigest = NISTObjectIdentifiers.id_shake128;
                    param = new XMSSMTKeyGenerationParameters(new XMSSMTParameters(xmssmtParameterSpec.getHeight(), xmssmtParameterSpec.getLayers(), new SHAKEDigest(128)), secureRandom);
                }
                else {
                    if (!xmssmtParameterSpec.getTreeDigest().equals("SHAKE256")) {
                        break Label_0235;
                    }
                    this.treeDigest = NISTObjectIdentifiers.id_shake256;
                    param = new XMSSMTKeyGenerationParameters(new XMSSMTParameters(xmssmtParameterSpec.getHeight(), xmssmtParameterSpec.getLayers(), new SHAKEDigest(256)), secureRandom);
                }
                this.param = param;
            }
            this.engine.init(this.param);
            this.initialised = true;
            return;
        }
        throw new InvalidAlgorithmParameterException("parameter object not a XMSSMTParameterSpec");
    }
}
