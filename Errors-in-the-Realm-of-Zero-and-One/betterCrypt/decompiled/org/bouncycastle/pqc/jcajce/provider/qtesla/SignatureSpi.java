// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.qtesla;

import org.bouncycastle.crypto.digests.NullDigest;
import org.bouncycastle.pqc.crypto.qtesla.QTESLASecurityCategory;
import java.security.SignatureException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.PublicKey;
import org.bouncycastle.crypto.CipherParameters;
import java.security.InvalidKeyException;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import java.security.PrivateKey;
import org.bouncycastle.pqc.crypto.qtesla.QTESLASigner;
import java.security.SecureRandom;
import org.bouncycastle.crypto.Digest;
import java.security.Signature;

public class SignatureSpi extends Signature
{
    private Digest digest;
    private SecureRandom random;
    private QTESLASigner signer;
    
    protected SignatureSpi(final String algorithm) {
        super(algorithm);
    }
    
    protected SignatureSpi(final String algorithm, final Digest digest, final QTESLASigner signer) {
        super(algorithm);
        this.digest = digest;
        this.signer = signer;
    }
    
    @Override
    protected Object engineGetParameter(final String s) {
        throw new UnsupportedOperationException("engineSetParameter unsupported");
    }
    
    @Override
    protected void engineInitSign(final PrivateKey privateKey) throws InvalidKeyException {
        if (privateKey instanceof BCqTESLAPrivateKey) {
            final CipherParameters keyParams = ((BCqTESLAPrivateKey)privateKey).getKeyParams();
            final SecureRandom random = this.random;
            CipherParameters cipherParameters = keyParams;
            if (random != null) {
                cipherParameters = new ParametersWithRandom(keyParams, random);
            }
            this.signer.init(true, cipherParameters);
            return;
        }
        throw new InvalidKeyException("unknown private key passed to qTESLA");
    }
    
    @Override
    protected void engineInitSign(final PrivateKey privateKey, final SecureRandom random) throws InvalidKeyException {
        this.random = random;
        this.engineInitSign(privateKey);
    }
    
    @Override
    protected void engineInitVerify(final PublicKey publicKey) throws InvalidKeyException {
        if (publicKey instanceof BCqTESLAPublicKey) {
            final CipherParameters keyParams = ((BCqTESLAPublicKey)publicKey).getKeyParams();
            this.digest.reset();
            this.signer.init(false, keyParams);
            return;
        }
        throw new InvalidKeyException("unknown public key passed to qTESLA");
    }
    
    @Override
    protected void engineSetParameter(final String s, final Object o) {
        throw new UnsupportedOperationException("engineSetParameter unsupported");
    }
    
    @Override
    protected void engineSetParameter(final AlgorithmParameterSpec algorithmParameterSpec) {
        throw new UnsupportedOperationException("engineSetParameter unsupported");
    }
    
    @Override
    protected byte[] engineSign() throws SignatureException {
        try {
            return this.signer.generateSignature(DigestUtil.getDigestResult(this.digest));
        }
        catch (Exception ex) {
            if (ex instanceof IllegalStateException) {
                throw new SignatureException(ex.getMessage());
            }
            throw new SignatureException(ex.toString());
        }
    }
    
    @Override
    protected void engineUpdate(final byte b) throws SignatureException {
        this.digest.update(b);
    }
    
    @Override
    protected void engineUpdate(final byte[] array, final int n, final int n2) throws SignatureException {
        this.digest.update(array, n, n2);
    }
    
    @Override
    protected boolean engineVerify(final byte[] array) throws SignatureException {
        return this.signer.verifySignature(DigestUtil.getDigestResult(this.digest), array);
    }
    
    public static class PI extends SignatureSpi
    {
        public PI() {
            super(QTESLASecurityCategory.getName(5), new NullDigest(), new QTESLASigner());
        }
    }
    
    public static class PIII extends SignatureSpi
    {
        public PIII() {
            super(QTESLASecurityCategory.getName(6), new NullDigest(), new QTESLASigner());
        }
    }
    
    public static class qTESLA extends SignatureSpi
    {
        public qTESLA() {
            super("qTESLA", new NullDigest(), new QTESLASigner());
        }
    }
}
