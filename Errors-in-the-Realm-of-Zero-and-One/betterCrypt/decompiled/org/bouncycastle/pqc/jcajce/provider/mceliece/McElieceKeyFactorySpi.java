// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.mceliece;

import java.security.spec.PKCS8EncodedKeySpec;
import org.bouncycastle.pqc.crypto.mceliece.McEliecePrivateKeyParameters;
import org.bouncycastle.pqc.asn1.McEliecePrivateKey;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import java.security.InvalidKeyException;
import java.security.Key;
import java.io.IOException;
import org.bouncycastle.pqc.crypto.mceliece.McEliecePublicKeyParameters;
import org.bouncycastle.pqc.asn1.McEliecePublicKey;
import org.bouncycastle.pqc.asn1.PQCObjectIdentifiers;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.ASN1Primitive;
import java.security.spec.X509EncodedKeySpec;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.PrivateKey;
import java.security.spec.KeySpec;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.jcajce.provider.util.AsymmetricKeyInfoConverter;
import java.security.KeyFactorySpi;

public class McElieceKeyFactorySpi extends KeyFactorySpi implements AsymmetricKeyInfoConverter
{
    public static final String OID = "1.3.6.1.4.1.8301.3.1.3.4.1";
    
    private static Digest getDigest(final AlgorithmIdentifier algorithmIdentifier) {
        return new SHA256Digest();
    }
    
    @Override
    protected PrivateKey engineGeneratePrivate(final KeySpec p0) throws InvalidKeySpecException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: instanceof      Ljava/security/spec/PKCS8EncodedKeySpec;
        //     4: ifeq            144
        //     7: aload_1        
        //     8: checkcast       Ljava/security/spec/PKCS8EncodedKeySpec;
        //    11: invokevirtual   java/security/spec/PKCS8EncodedKeySpec.getEncoded:()[B
        //    14: astore_1       
        //    15: aload_1        
        //    16: invokestatic    org/bouncycastle/asn1/ASN1Primitive.fromByteArray:([B)Lorg/bouncycastle/asn1/ASN1Primitive;
        //    19: invokestatic    org/bouncycastle/asn1/pkcs/PrivateKeyInfo.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/pkcs/PrivateKeyInfo;
        //    22: astore_1       
        //    23: getstatic       org/bouncycastle/pqc/asn1/PQCObjectIdentifiers.mcEliece:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //    26: aload_1        
        //    27: invokevirtual   org/bouncycastle/asn1/pkcs/PrivateKeyInfo.getPrivateKeyAlgorithm:()Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //    30: invokevirtual   org/bouncycastle/asn1/x509/AlgorithmIdentifier.getAlgorithm:()Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //    33: invokevirtual   org/bouncycastle/asn1/ASN1ObjectIdentifier.equals:(Lorg/bouncycastle/asn1/ASN1Primitive;)Z
        //    36: ifeq            90
        //    39: aload_1        
        //    40: invokevirtual   org/bouncycastle/asn1/pkcs/PrivateKeyInfo.parsePrivateKey:()Lorg/bouncycastle/asn1/ASN1Encodable;
        //    43: invokestatic    org/bouncycastle/pqc/asn1/McEliecePrivateKey.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/pqc/asn1/McEliecePrivateKey;
        //    46: astore_1       
        //    47: new             Lorg/bouncycastle/pqc/jcajce/provider/mceliece/BCMcEliecePrivateKey;
        //    50: dup            
        //    51: new             Lorg/bouncycastle/pqc/crypto/mceliece/McEliecePrivateKeyParameters;
        //    54: dup            
        //    55: aload_1        
        //    56: invokevirtual   org/bouncycastle/pqc/asn1/McEliecePrivateKey.getN:()I
        //    59: aload_1        
        //    60: invokevirtual   org/bouncycastle/pqc/asn1/McEliecePrivateKey.getK:()I
        //    63: aload_1        
        //    64: invokevirtual   org/bouncycastle/pqc/asn1/McEliecePrivateKey.getField:()Lorg/bouncycastle/pqc/math/linearalgebra/GF2mField;
        //    67: aload_1        
        //    68: invokevirtual   org/bouncycastle/pqc/asn1/McEliecePrivateKey.getGoppaPoly:()Lorg/bouncycastle/pqc/math/linearalgebra/PolynomialGF2mSmallM;
        //    71: aload_1        
        //    72: invokevirtual   org/bouncycastle/pqc/asn1/McEliecePrivateKey.getP1:()Lorg/bouncycastle/pqc/math/linearalgebra/Permutation;
        //    75: aload_1        
        //    76: invokevirtual   org/bouncycastle/pqc/asn1/McEliecePrivateKey.getP2:()Lorg/bouncycastle/pqc/math/linearalgebra/Permutation;
        //    79: aload_1        
        //    80: invokevirtual   org/bouncycastle/pqc/asn1/McEliecePrivateKey.getSInv:()Lorg/bouncycastle/pqc/math/linearalgebra/GF2Matrix;
        //    83: invokespecial   org/bouncycastle/pqc/crypto/mceliece/McEliecePrivateKeyParameters.<init>:(IILorg/bouncycastle/pqc/math/linearalgebra/GF2mField;Lorg/bouncycastle/pqc/math/linearalgebra/PolynomialGF2mSmallM;Lorg/bouncycastle/pqc/math/linearalgebra/Permutation;Lorg/bouncycastle/pqc/math/linearalgebra/Permutation;Lorg/bouncycastle/pqc/math/linearalgebra/GF2Matrix;)V
        //    86: invokespecial   org/bouncycastle/pqc/jcajce/provider/mceliece/BCMcEliecePrivateKey.<init>:(Lorg/bouncycastle/pqc/crypto/mceliece/McEliecePrivateKeyParameters;)V
        //    89: areturn        
        //    90: new             Ljava/security/spec/InvalidKeySpecException;
        //    93: dup            
        //    94: ldc             "Unable to recognise OID in McEliece private key"
        //    96: invokespecial   java/security/spec/InvalidKeySpecException.<init>:(Ljava/lang/String;)V
        //    99: athrow         
        //   100: new             Ljava/security/spec/InvalidKeySpecException;
        //   103: dup            
        //   104: ldc             "Unable to decode PKCS8EncodedKeySpec."
        //   106: invokespecial   java/security/spec/InvalidKeySpecException.<init>:(Ljava/lang/String;)V
        //   109: athrow         
        //   110: astore_1       
        //   111: new             Ljava/lang/StringBuilder;
        //   114: dup            
        //   115: invokespecial   java/lang/StringBuilder.<init>:()V
        //   118: astore_2       
        //   119: aload_2        
        //   120: ldc             "Unable to decode PKCS8EncodedKeySpec: "
        //   122: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   125: pop            
        //   126: aload_2        
        //   127: aload_1        
        //   128: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   131: pop            
        //   132: new             Ljava/security/spec/InvalidKeySpecException;
        //   135: dup            
        //   136: aload_2        
        //   137: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   140: invokespecial   java/security/spec/InvalidKeySpecException.<init>:(Ljava/lang/String;)V
        //   143: athrow         
        //   144: new             Ljava/lang/StringBuilder;
        //   147: dup            
        //   148: invokespecial   java/lang/StringBuilder.<init>:()V
        //   151: astore_2       
        //   152: aload_2        
        //   153: ldc             "Unsupported key specification: "
        //   155: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   158: pop            
        //   159: aload_2        
        //   160: aload_1        
        //   161: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //   164: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   167: pop            
        //   168: aload_2        
        //   169: ldc             "."
        //   171: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   174: pop            
        //   175: new             Ljava/security/spec/InvalidKeySpecException;
        //   178: dup            
        //   179: aload_2        
        //   180: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   183: invokespecial   java/security/spec/InvalidKeySpecException.<init>:(Ljava/lang/String;)V
        //   186: athrow         
        //   187: astore_1       
        //   188: goto            100
        //    Exceptions:
        //  throws java.security.spec.InvalidKeySpecException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  15     23     110    144    Ljava/io/IOException;
        //  23     90     187    110    Ljava/io/IOException;
        //  90     100    187    110    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0090:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    protected PublicKey engineGeneratePublic(final KeySpec keySpec) throws InvalidKeySpecException {
        if (keySpec instanceof X509EncodedKeySpec) {
            final byte[] encoded = ((X509EncodedKeySpec)keySpec).getEncoded();
            try {
                final SubjectPublicKeyInfo instance = SubjectPublicKeyInfo.getInstance(ASN1Primitive.fromByteArray(encoded));
                try {
                    if (PQCObjectIdentifiers.mcEliece.equals(instance.getAlgorithm().getAlgorithm())) {
                        final McEliecePublicKey instance2 = McEliecePublicKey.getInstance(instance.parsePublicKey());
                        return new BCMcEliecePublicKey(new McEliecePublicKeyParameters(instance2.getN(), instance2.getT(), instance2.getG()));
                    }
                    throw new InvalidKeySpecException("Unable to recognise OID in McEliece public key");
                }
                catch (IOException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unable to decode X509EncodedKeySpec: ");
                    sb.append(ex.getMessage());
                    throw new InvalidKeySpecException(sb.toString());
                }
            }
            catch (IOException ex2) {
                throw new InvalidKeySpecException(ex2.toString());
            }
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Unsupported key specification: ");
        sb2.append(keySpec.getClass());
        sb2.append(".");
        throw new InvalidKeySpecException(sb2.toString());
    }
    
    @Override
    protected KeySpec engineGetKeySpec(final Key key, final Class clazz) throws InvalidKeySpecException {
        return null;
    }
    
    @Override
    protected Key engineTranslateKey(final Key key) throws InvalidKeyException {
        return null;
    }
    
    @Override
    public PrivateKey generatePrivate(final PrivateKeyInfo privateKeyInfo) throws IOException {
        final McEliecePrivateKey instance = McEliecePrivateKey.getInstance(privateKeyInfo.parsePrivateKey().toASN1Primitive());
        return new BCMcEliecePrivateKey(new McEliecePrivateKeyParameters(instance.getN(), instance.getK(), instance.getField(), instance.getGoppaPoly(), instance.getP1(), instance.getP2(), instance.getSInv()));
    }
    
    @Override
    public PublicKey generatePublic(final SubjectPublicKeyInfo subjectPublicKeyInfo) throws IOException {
        final McEliecePublicKey instance = McEliecePublicKey.getInstance(subjectPublicKeyInfo.parsePublicKey());
        return new BCMcEliecePublicKey(new McEliecePublicKeyParameters(instance.getN(), instance.getT(), instance.getG()));
    }
    
    public KeySpec getKeySpec(final Key key, final Class obj) throws InvalidKeySpecException {
        if (key instanceof BCMcEliecePrivateKey) {
            if (PKCS8EncodedKeySpec.class.isAssignableFrom(obj)) {
                return new PKCS8EncodedKeySpec(key.getEncoded());
            }
        }
        else {
            if (!(key instanceof BCMcEliecePublicKey)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unsupported key type: ");
                sb.append(key.getClass());
                sb.append(".");
                throw new InvalidKeySpecException(sb.toString());
            }
            if (X509EncodedKeySpec.class.isAssignableFrom(obj)) {
                return new X509EncodedKeySpec(key.getEncoded());
            }
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Unknown key specification: ");
        sb2.append(obj);
        sb2.append(".");
        throw new InvalidKeySpecException(sb2.toString());
    }
    
    public Key translateKey(final Key key) throws InvalidKeyException {
        if (key instanceof BCMcEliecePrivateKey) {
            return key;
        }
        if (key instanceof BCMcEliecePublicKey) {
            return key;
        }
        throw new InvalidKeyException("Unsupported key type.");
    }
}
