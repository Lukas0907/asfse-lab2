// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.newhope;

import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.pqc.crypto.util.PrivateKeyInfoFactory;
import org.bouncycastle.util.Arrays;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import org.bouncycastle.pqc.crypto.util.PrivateKeyFactory;
import java.io.IOException;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.pqc.crypto.newhope.NHPrivateKeyParameters;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.pqc.jcajce.interfaces.NHPrivateKey;

public class BCNHPrivateKey implements NHPrivateKey
{
    private static final long serialVersionUID = 1L;
    private transient ASN1Set attributes;
    private transient NHPrivateKeyParameters params;
    
    public BCNHPrivateKey(final PrivateKeyInfo privateKeyInfo) throws IOException {
        this.init(privateKeyInfo);
    }
    
    public BCNHPrivateKey(final NHPrivateKeyParameters params) {
        this.params = params;
    }
    
    private void init(final PrivateKeyInfo privateKeyInfo) throws IOException {
        this.attributes = privateKeyInfo.getAttributes();
        this.params = (NHPrivateKeyParameters)PrivateKeyFactory.createKey(privateKeyInfo);
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.init(PrivateKeyInfo.getInstance(objectInputStream.readObject()));
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.getEncoded());
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof BCNHPrivateKey && Arrays.areEqual(this.params.getSecData(), ((BCNHPrivateKey)o).params.getSecData());
    }
    
    @Override
    public final String getAlgorithm() {
        return "NH";
    }
    
    @Override
    public byte[] getEncoded() {
        try {
            return PrivateKeyInfoFactory.createPrivateKeyInfo(this.params, this.attributes).getEncoded();
        }
        catch (IOException ex) {
            return null;
        }
    }
    
    @Override
    public String getFormat() {
        return "PKCS#8";
    }
    
    CipherParameters getKeyParams() {
        return this.params;
    }
    
    @Override
    public short[] getSecretData() {
        return this.params.getSecData();
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.params.getSecData());
    }
}
