// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.xmss;

import org.bouncycastle.crypto.digests.SHAKEDigest;
import org.bouncycastle.crypto.digests.SHA512Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.digests.NullDigest;
import org.bouncycastle.pqc.crypto.xmss.XMSSPrivateKeyParameters;
import java.security.SignatureException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.PublicKey;
import org.bouncycastle.crypto.CipherParameters;
import java.security.InvalidKeyException;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import java.security.PrivateKey;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.pqc.crypto.xmss.XMSSSigner;
import java.security.SecureRandom;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.pqc.jcajce.interfaces.StateAwareSignature;
import java.security.Signature;

public class XMSSSignatureSpi extends Signature implements StateAwareSignature
{
    private Digest digest;
    private SecureRandom random;
    private XMSSSigner signer;
    private ASN1ObjectIdentifier treeDigest;
    
    protected XMSSSignatureSpi(final String algorithm) {
        super(algorithm);
    }
    
    protected XMSSSignatureSpi(final String algorithm, final Digest digest, final XMSSSigner signer) {
        super(algorithm);
        this.digest = digest;
        this.signer = signer;
    }
    
    @Override
    protected Object engineGetParameter(final String s) {
        throw new UnsupportedOperationException("engineSetParameter unsupported");
    }
    
    @Override
    protected void engineInitSign(final PrivateKey privateKey) throws InvalidKeyException {
        if (privateKey instanceof BCXMSSPrivateKey) {
            final BCXMSSPrivateKey bcxmssPrivateKey = (BCXMSSPrivateKey)privateKey;
            final CipherParameters keyParams = bcxmssPrivateKey.getKeyParams();
            this.treeDigest = bcxmssPrivateKey.getTreeDigestOID();
            final SecureRandom random = this.random;
            CipherParameters cipherParameters = keyParams;
            if (random != null) {
                cipherParameters = new ParametersWithRandom(keyParams, random);
            }
            this.digest.reset();
            this.signer.init(true, cipherParameters);
            return;
        }
        throw new InvalidKeyException("unknown private key passed to XMSS");
    }
    
    @Override
    protected void engineInitSign(final PrivateKey privateKey, final SecureRandom random) throws InvalidKeyException {
        this.random = random;
        this.engineInitSign(privateKey);
    }
    
    @Override
    protected void engineInitVerify(final PublicKey publicKey) throws InvalidKeyException {
        if (publicKey instanceof BCXMSSPublicKey) {
            final CipherParameters keyParams = ((BCXMSSPublicKey)publicKey).getKeyParams();
            this.treeDigest = null;
            this.digest.reset();
            this.signer.init(false, keyParams);
            return;
        }
        throw new InvalidKeyException("unknown public key passed to XMSS");
    }
    
    @Override
    protected void engineSetParameter(final String s, final Object o) {
        throw new UnsupportedOperationException("engineSetParameter unsupported");
    }
    
    @Override
    protected void engineSetParameter(final AlgorithmParameterSpec algorithmParameterSpec) {
        throw new UnsupportedOperationException("engineSetParameter unsupported");
    }
    
    @Override
    protected byte[] engineSign() throws SignatureException {
        final byte[] digestResult = DigestUtil.getDigestResult(this.digest);
        try {
            return this.signer.generateSignature(digestResult);
        }
        catch (Exception ex) {
            if (ex instanceof IllegalStateException) {
                throw new SignatureException(ex.getMessage(), ex);
            }
            throw new SignatureException(ex.toString(), ex);
        }
    }
    
    @Override
    protected void engineUpdate(final byte b) throws SignatureException {
        this.digest.update(b);
    }
    
    @Override
    protected void engineUpdate(final byte[] array, final int n, final int n2) throws SignatureException {
        this.digest.update(array, n, n2);
    }
    
    @Override
    protected boolean engineVerify(final byte[] array) throws SignatureException {
        return this.signer.verifySignature(DigestUtil.getDigestResult(this.digest), array);
    }
    
    @Override
    public PrivateKey getUpdatedPrivateKey() {
        final ASN1ObjectIdentifier treeDigest = this.treeDigest;
        if (treeDigest != null) {
            final BCXMSSPrivateKey bcxmssPrivateKey = new BCXMSSPrivateKey(treeDigest, (XMSSPrivateKeyParameters)this.signer.getUpdatedPrivateKey());
            this.treeDigest = null;
            return bcxmssPrivateKey;
        }
        throw new IllegalStateException("signature object not in a signing state");
    }
    
    @Override
    public boolean isSigningCapable() {
        return this.treeDigest != null && this.signer.getUsagesRemaining() != 0L;
    }
    
    public static class withSha256 extends XMSSSignatureSpi
    {
        public withSha256() {
            super("XMSS-SHA256", new NullDigest(), new XMSSSigner());
        }
    }
    
    public static class withSha256andPrehash extends XMSSSignatureSpi
    {
        public withSha256andPrehash() {
            super("SHA256withXMSS-SHA256", new SHA256Digest(), new XMSSSigner());
        }
    }
    
    public static class withSha512 extends XMSSSignatureSpi
    {
        public withSha512() {
            super("XMSS-SHA512", new NullDigest(), new XMSSSigner());
        }
    }
    
    public static class withSha512andPrehash extends XMSSSignatureSpi
    {
        public withSha512andPrehash() {
            super("SHA512withXMSS-SHA512", new SHA512Digest(), new XMSSSigner());
        }
    }
    
    public static class withShake128 extends XMSSSignatureSpi
    {
        public withShake128() {
            super("XMSS-SHAKE128", new NullDigest(), new XMSSSigner());
        }
    }
    
    public static class withShake128andPrehash extends XMSSSignatureSpi
    {
        public withShake128andPrehash() {
            super("SHAKE128withXMSSMT-SHAKE128", new SHAKEDigest(128), new XMSSSigner());
        }
    }
    
    public static class withShake256 extends XMSSSignatureSpi
    {
        public withShake256() {
            super("XMSS-SHAKE256", new NullDigest(), new XMSSSigner());
        }
    }
    
    public static class withShake256andPrehash extends XMSSSignatureSpi
    {
        public withShake256andPrehash() {
            super("SHAKE256withXMSS-SHAKE256", new SHAKEDigest(256), new XMSSSigner());
        }
    }
}
