// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.qtesla;

import org.bouncycastle.pqc.jcajce.spec.QTESLAParameterSpec;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.pqc.crypto.util.SubjectPublicKeyInfoFactory;
import org.bouncycastle.pqc.crypto.qtesla.QTESLASecurityCategory;
import org.bouncycastle.util.Arrays;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import org.bouncycastle.pqc.crypto.util.PublicKeyFactory;
import java.io.IOException;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.pqc.crypto.qtesla.QTESLAPublicKeyParameters;
import org.bouncycastle.pqc.jcajce.interfaces.QTESLAKey;
import java.security.PublicKey;

public class BCqTESLAPublicKey implements PublicKey, QTESLAKey
{
    private static final long serialVersionUID = 1L;
    private transient QTESLAPublicKeyParameters keyParams;
    
    public BCqTESLAPublicKey(final SubjectPublicKeyInfo subjectPublicKeyInfo) throws IOException {
        this.init(subjectPublicKeyInfo);
    }
    
    public BCqTESLAPublicKey(final QTESLAPublicKeyParameters keyParams) {
        this.keyParams = keyParams;
    }
    
    private void init(final SubjectPublicKeyInfo subjectPublicKeyInfo) throws IOException {
        this.keyParams = (QTESLAPublicKeyParameters)PublicKeyFactory.createKey(subjectPublicKeyInfo);
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.init(SubjectPublicKeyInfo.getInstance(objectInputStream.readObject()));
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.getEncoded());
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof BCqTESLAPublicKey) {
            final BCqTESLAPublicKey bCqTESLAPublicKey = (BCqTESLAPublicKey)o;
            return this.keyParams.getSecurityCategory() == bCqTESLAPublicKey.keyParams.getSecurityCategory() && Arrays.areEqual(this.keyParams.getPublicData(), bCqTESLAPublicKey.keyParams.getPublicData());
        }
        return false;
    }
    
    @Override
    public final String getAlgorithm() {
        return QTESLASecurityCategory.getName(this.keyParams.getSecurityCategory());
    }
    
    @Override
    public byte[] getEncoded() {
        try {
            return SubjectPublicKeyInfoFactory.createSubjectPublicKeyInfo(this.keyParams).getEncoded();
        }
        catch (IOException ex) {
            return null;
        }
    }
    
    @Override
    public String getFormat() {
        return "X.509";
    }
    
    CipherParameters getKeyParams() {
        return this.keyParams;
    }
    
    @Override
    public QTESLAParameterSpec getParams() {
        return new QTESLAParameterSpec(this.getAlgorithm());
    }
    
    @Override
    public int hashCode() {
        return this.keyParams.getSecurityCategory() + Arrays.hashCode(this.keyParams.getPublicData()) * 37;
    }
}
