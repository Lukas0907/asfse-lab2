// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.qtesla;

import java.security.InvalidAlgorithmParameterException;
import org.bouncycastle.pqc.jcajce.spec.QTESLAParameterSpec;
import java.security.spec.AlgorithmParameterSpec;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import org.bouncycastle.pqc.crypto.qtesla.QTESLAPrivateKeyParameters;
import org.bouncycastle.pqc.crypto.qtesla.QTESLAPublicKeyParameters;
import org.bouncycastle.crypto.KeyGenerationParameters;
import java.security.KeyPair;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.util.Integers;
import org.bouncycastle.pqc.crypto.qtesla.QTESLASecurityCategory;
import java.util.HashMap;
import java.security.SecureRandom;
import org.bouncycastle.pqc.crypto.qtesla.QTESLAKeyGenerationParameters;
import org.bouncycastle.pqc.crypto.qtesla.QTESLAKeyPairGenerator;
import java.util.Map;
import java.security.KeyPairGenerator;

public class KeyPairGeneratorSpi extends KeyPairGenerator
{
    private static final Map catLookup;
    private QTESLAKeyPairGenerator engine;
    private boolean initialised;
    private QTESLAKeyGenerationParameters param;
    private SecureRandom random;
    
    static {
        (catLookup = new HashMap()).put(QTESLASecurityCategory.getName(5), Integers.valueOf(5));
        KeyPairGeneratorSpi.catLookup.put(QTESLASecurityCategory.getName(6), Integers.valueOf(6));
    }
    
    public KeyPairGeneratorSpi() {
        super("qTESLA");
        this.engine = new QTESLAKeyPairGenerator();
        this.random = CryptoServicesRegistrar.getSecureRandom();
        this.initialised = false;
    }
    
    @Override
    public KeyPair generateKeyPair() {
        if (!this.initialised) {
            this.param = new QTESLAKeyGenerationParameters(6, this.random);
            this.engine.init(this.param);
            this.initialised = true;
        }
        final AsymmetricCipherKeyPair generateKeyPair = this.engine.generateKeyPair();
        return new KeyPair(new BCqTESLAPublicKey((QTESLAPublicKeyParameters)generateKeyPair.getPublic()), new BCqTESLAPrivateKey((QTESLAPrivateKeyParameters)generateKeyPair.getPrivate()));
    }
    
    @Override
    public void initialize(final int n, final SecureRandom secureRandom) {
        throw new IllegalArgumentException("use AlgorithmParameterSpec");
    }
    
    @Override
    public void initialize(final AlgorithmParameterSpec algorithmParameterSpec, final SecureRandom secureRandom) throws InvalidAlgorithmParameterException {
        if (algorithmParameterSpec instanceof QTESLAParameterSpec) {
            this.param = new QTESLAKeyGenerationParameters(KeyPairGeneratorSpi.catLookup.get(((QTESLAParameterSpec)algorithmParameterSpec).getSecurityCategory()), secureRandom);
            this.engine.init(this.param);
            this.initialised = true;
            return;
        }
        throw new InvalidAlgorithmParameterException("parameter object not a QTESLAParameterSpec");
    }
}
