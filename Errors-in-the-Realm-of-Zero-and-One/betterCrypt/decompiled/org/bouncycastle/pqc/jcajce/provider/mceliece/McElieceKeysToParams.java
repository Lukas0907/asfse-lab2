// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.mceliece;

import java.security.PublicKey;
import java.security.InvalidKeyException;
import org.bouncycastle.pqc.crypto.mceliece.McEliecePrivateKeyParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import java.security.PrivateKey;

public class McElieceKeysToParams
{
    public static AsymmetricKeyParameter generatePrivateKeyParameter(final PrivateKey privateKey) throws InvalidKeyException {
        if (privateKey instanceof BCMcEliecePrivateKey) {
            final BCMcEliecePrivateKey bcMcEliecePrivateKey = (BCMcEliecePrivateKey)privateKey;
            return new McEliecePrivateKeyParameters(bcMcEliecePrivateKey.getN(), bcMcEliecePrivateKey.getK(), bcMcEliecePrivateKey.getField(), bcMcEliecePrivateKey.getGoppaPoly(), bcMcEliecePrivateKey.getP1(), bcMcEliecePrivateKey.getP2(), bcMcEliecePrivateKey.getSInv());
        }
        throw new InvalidKeyException("can't identify McEliece private key.");
    }
    
    public static AsymmetricKeyParameter generatePublicKeyParameter(final PublicKey publicKey) throws InvalidKeyException {
        if (publicKey instanceof BCMcEliecePublicKey) {
            return ((BCMcEliecePublicKey)publicKey).getKeyParams();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("can't identify McEliece public key: ");
        sb.append(publicKey.getClass().getName());
        throw new InvalidKeyException(sb.toString());
    }
}
