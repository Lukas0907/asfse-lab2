// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.rainbow;

import org.bouncycastle.pqc.jcajce.provider.util.KeyUtil;
import org.bouncycastle.pqc.asn1.RainbowPublicKey;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.pqc.asn1.PQCObjectIdentifiers;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.pqc.crypto.rainbow.util.RainbowUtil;
import org.bouncycastle.pqc.jcajce.spec.RainbowPublicKeySpec;
import org.bouncycastle.pqc.crypto.rainbow.RainbowPublicKeyParameters;
import org.bouncycastle.pqc.crypto.rainbow.RainbowParameters;
import java.security.PublicKey;

public class BCRainbowPublicKey implements PublicKey
{
    private static final long serialVersionUID = 1L;
    private short[][] coeffquadratic;
    private short[] coeffscalar;
    private short[][] coeffsingular;
    private int docLength;
    private RainbowParameters rainbowParams;
    
    public BCRainbowPublicKey(final int docLength, final short[][] coeffquadratic, final short[][] coeffsingular, final short[] coeffscalar) {
        this.docLength = docLength;
        this.coeffquadratic = coeffquadratic;
        this.coeffsingular = coeffsingular;
        this.coeffscalar = coeffscalar;
    }
    
    public BCRainbowPublicKey(final RainbowPublicKeyParameters rainbowPublicKeyParameters) {
        this(rainbowPublicKeyParameters.getDocLength(), rainbowPublicKeyParameters.getCoeffQuadratic(), rainbowPublicKeyParameters.getCoeffSingular(), rainbowPublicKeyParameters.getCoeffScalar());
    }
    
    public BCRainbowPublicKey(final RainbowPublicKeySpec rainbowPublicKeySpec) {
        this(rainbowPublicKeySpec.getDocLength(), rainbowPublicKeySpec.getCoeffQuadratic(), rainbowPublicKeySpec.getCoeffSingular(), rainbowPublicKeySpec.getCoeffScalar());
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b2;
        final boolean b = b2 = false;
        if (o != null) {
            if (!(o instanceof BCRainbowPublicKey)) {
                return false;
            }
            final BCRainbowPublicKey bcRainbowPublicKey = (BCRainbowPublicKey)o;
            b2 = b;
            if (this.docLength == bcRainbowPublicKey.getDocLength()) {
                b2 = b;
                if (RainbowUtil.equals(this.coeffquadratic, bcRainbowPublicKey.getCoeffQuadratic())) {
                    b2 = b;
                    if (RainbowUtil.equals(this.coeffsingular, bcRainbowPublicKey.getCoeffSingular())) {
                        b2 = b;
                        if (RainbowUtil.equals(this.coeffscalar, bcRainbowPublicKey.getCoeffScalar())) {
                            b2 = true;
                        }
                    }
                }
            }
        }
        return b2;
    }
    
    @Override
    public final String getAlgorithm() {
        return "Rainbow";
    }
    
    public short[][] getCoeffQuadratic() {
        return this.coeffquadratic;
    }
    
    public short[] getCoeffScalar() {
        return Arrays.clone(this.coeffscalar);
    }
    
    public short[][] getCoeffSingular() {
        final short[][] array = new short[this.coeffsingular.length][];
        int n = 0;
        while (true) {
            final short[][] coeffsingular = this.coeffsingular;
            if (n == coeffsingular.length) {
                break;
            }
            array[n] = Arrays.clone(coeffsingular[n]);
            ++n;
        }
        return array;
    }
    
    public int getDocLength() {
        return this.docLength;
    }
    
    @Override
    public byte[] getEncoded() {
        return KeyUtil.getEncodedSubjectPublicKeyInfo(new AlgorithmIdentifier(PQCObjectIdentifiers.rainbow, DERNull.INSTANCE), new RainbowPublicKey(this.docLength, this.coeffquadratic, this.coeffsingular, this.coeffscalar));
    }
    
    @Override
    public String getFormat() {
        return "X.509";
    }
    
    @Override
    public int hashCode() {
        return ((this.docLength * 37 + Arrays.hashCode(this.coeffquadratic)) * 37 + Arrays.hashCode(this.coeffsingular)) * 37 + Arrays.hashCode(this.coeffscalar);
    }
}
