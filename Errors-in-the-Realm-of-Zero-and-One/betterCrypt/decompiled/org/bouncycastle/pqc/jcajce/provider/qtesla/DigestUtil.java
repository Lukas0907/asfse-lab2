// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.provider.qtesla;

import org.bouncycastle.crypto.Xof;
import org.bouncycastle.crypto.digests.SHAKEDigest;
import org.bouncycastle.crypto.digests.SHA512Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;

class DigestUtil
{
    static Digest getDigest(final ASN1ObjectIdentifier obj) {
        if (obj.equals(NISTObjectIdentifiers.id_sha256)) {
            return new SHA256Digest();
        }
        if (obj.equals(NISTObjectIdentifiers.id_sha512)) {
            return new SHA512Digest();
        }
        if (obj.equals(NISTObjectIdentifiers.id_shake128)) {
            return new SHAKEDigest(128);
        }
        if (obj.equals(NISTObjectIdentifiers.id_shake256)) {
            return new SHAKEDigest(256);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unrecognized digest OID: ");
        sb.append(obj);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static byte[] getDigestResult(final Digest digest) {
        final byte[] array = new byte[getDigestSize(digest)];
        if (digest instanceof Xof) {
            ((Xof)digest).doFinal(array, 0, array.length);
            return array;
        }
        digest.doFinal(array, 0);
        return array;
    }
    
    public static int getDigestSize(final Digest digest) {
        final boolean b = digest instanceof Xof;
        int digestSize = digest.getDigestSize();
        if (b) {
            digestSize *= 2;
        }
        return digestSize;
    }
}
