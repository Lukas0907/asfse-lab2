// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.jcajce.spec;

import org.bouncycastle.pqc.crypto.qtesla.QTESLASecurityCategory;
import java.security.spec.AlgorithmParameterSpec;

public class QTESLAParameterSpec implements AlgorithmParameterSpec
{
    public static final String PROVABLY_SECURE_I;
    public static final String PROVABLY_SECURE_III;
    private String securityCategory;
    
    static {
        PROVABLY_SECURE_I = QTESLASecurityCategory.getName(5);
        PROVABLY_SECURE_III = QTESLASecurityCategory.getName(6);
    }
    
    public QTESLAParameterSpec(final String securityCategory) {
        this.securityCategory = securityCategory;
    }
    
    public String getSecurityCategory() {
        return this.securityCategory;
    }
}
