// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.math.linearalgebra;

import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import java.io.PrintStream;
import java.security.SecureRandom;
import java.math.BigInteger;

public final class IntegerFunctions
{
    private static final BigInteger FOUR;
    private static final BigInteger ONE;
    private static final int[] SMALL_PRIMES;
    private static final long SMALL_PRIME_PRODUCT = 152125131763605L;
    private static final BigInteger TWO;
    private static final BigInteger ZERO;
    private static final int[] jacobiTable;
    private static SecureRandom sr;
    
    static {
        ZERO = BigInteger.valueOf(0L);
        ONE = BigInteger.valueOf(1L);
        TWO = BigInteger.valueOf(2L);
        FOUR = BigInteger.valueOf(4L);
        SMALL_PRIMES = new int[] { 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41 };
        IntegerFunctions.sr = null;
        jacobiTable = new int[] { 0, 1, 0, -1, 0, -1, 0, 1 };
    }
    
    private IntegerFunctions() {
    }
    
    public static BigInteger binomial(final int n, int i) {
        BigInteger bigInteger = IntegerFunctions.ONE;
        if (n != 0) {
            int n2;
            if ((n2 = i) > n >>> 1) {
                n2 = n - i;
            }
            for (i = 1; i <= n2; ++i) {
                bigInteger = bigInteger.multiply(BigInteger.valueOf(n - (i - 1))).divide(BigInteger.valueOf(i));
            }
            return bigInteger;
        }
        if (i == 0) {
            return bigInteger;
        }
        return IntegerFunctions.ZERO;
    }
    
    public static int bitCount(int i) {
        int n = 0;
        while (i != 0) {
            n += (i & 0x1);
            i >>>= 1;
        }
        return n;
    }
    
    public static int ceilLog(final int n) {
        int i;
        int n2;
        for (i = 1, n2 = 0; i < n; i <<= 1, ++n2) {}
        return n2;
    }
    
    public static int ceilLog(final BigInteger val) {
        BigInteger bigInteger = IntegerFunctions.ONE;
        int n = 0;
        while (bigInteger.compareTo(val) < 0) {
            ++n;
            bigInteger = bigInteger.shiftLeft(1);
        }
        return n;
    }
    
    public static int ceilLog256(int n) {
        if (n == 0) {
            return 1;
        }
        int i;
        if ((i = n) < 0) {
            i = -n;
        }
        n = 0;
        while (i > 0) {
            ++n;
            i >>>= 8;
        }
        return n;
    }
    
    public static int ceilLog256(final long n) {
        final long n2 = lcmp(n, 0L);
        if (n2 == 0) {
            return 1;
        }
        long n3 = n;
        if (n2 < 0) {
            n3 = -n;
        }
        int n4 = 0;
        while (n3 > 0L) {
            ++n4;
            n3 >>>= 8;
        }
        return n4;
    }
    
    public static BigInteger divideAndRound(final BigInteger bigInteger, final BigInteger val) {
        if (bigInteger.signum() < 0) {
            return divideAndRound(bigInteger.negate(), val).negate();
        }
        if (val.signum() < 0) {
            return divideAndRound(bigInteger, val.negate()).negate();
        }
        return bigInteger.shiftLeft(1).add(val).divide(val.shiftLeft(1));
    }
    
    public static BigInteger[] divideAndRound(final BigInteger[] array, final BigInteger bigInteger) {
        final BigInteger[] array2 = new BigInteger[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = divideAndRound(array[i], bigInteger);
        }
        return array2;
    }
    
    public static int[] extGCD(final int n, final int n2) {
        final BigInteger[] extgcd = extgcd(BigInteger.valueOf(n), BigInteger.valueOf(n2));
        return new int[] { extgcd[0].intValue(), extgcd[1].intValue(), extgcd[2].intValue() };
    }
    
    public static BigInteger[] extgcd(BigInteger bigInteger, final BigInteger val) {
        final BigInteger one = IntegerFunctions.ONE;
        BigInteger bigInteger2 = IntegerFunctions.ZERO;
        BigInteger bigInteger3 = one;
        BigInteger bigInteger4 = bigInteger;
        if (val.signum() != 0) {
            BigInteger zero = IntegerFunctions.ZERO;
            BigInteger val2 = one;
            BigInteger bigInteger5 = bigInteger;
            BigInteger bigInteger7;
            BigInteger subtract;
            BigInteger bigInteger8;
            for (BigInteger val3 = val; val3.signum() != 0; val3 = bigInteger7, bigInteger8 = subtract, val2 = zero, zero = bigInteger8) {
                final BigInteger[] divideAndRemainder = bigInteger5.divideAndRemainder(val3);
                final BigInteger bigInteger6 = divideAndRemainder[0];
                bigInteger7 = divideAndRemainder[1];
                subtract = val2.subtract(bigInteger6.multiply(zero));
                bigInteger5 = val3;
            }
            bigInteger2 = bigInteger5.subtract(bigInteger.multiply(val2)).divide(val);
            bigInteger = val2;
            bigInteger4 = bigInteger5;
            bigInteger3 = bigInteger;
        }
        return new BigInteger[] { bigInteger4, bigInteger3, bigInteger2 };
    }
    
    public static float floatPow(final float n, int i) {
        float n2 = 1.0f;
        while (i > 0) {
            n2 *= n;
            --i;
        }
        return n2;
    }
    
    public static int floorLog(int i) {
        if (i <= 0) {
            return -1;
        }
        i >>>= 1;
        int n = 0;
        while (i > 0) {
            ++n;
            i >>>= 1;
        }
        return n;
    }
    
    public static int floorLog(final BigInteger val) {
        BigInteger bigInteger = IntegerFunctions.ONE;
        int n = -1;
        while (bigInteger.compareTo(val) <= 0) {
            ++n;
            bigInteger = bigInteger.shiftLeft(1);
        }
        return n;
    }
    
    public static int gcd(final int n, final int n2) {
        return BigInteger.valueOf(n).gcd(BigInteger.valueOf(n2)).intValue();
    }
    
    public static float intRoot(final int n, final int n2) {
        float n3 = (float)(n / n2);
        float n5;
        for (float n4 = 0.0f; Math.abs(n4 - n3) > 1.0E-4; n4 = n3, n3 -= n5) {
            float floatPow;
            while (true) {
                floatPow = floatPow(n3, n2);
                if (!Float.isInfinite(floatPow)) {
                    break;
                }
                n3 = (n3 + n4) / 2.0f;
            }
            n5 = (floatPow - n) / (n2 * floatPow(n3, n2 - 1));
        }
        return n3;
    }
    
    public static byte[] integerToOctets(final BigInteger bigInteger) {
        final byte[] byteArray = bigInteger.abs().toByteArray();
        if ((bigInteger.bitLength() & 0x7) != 0x0) {
            return byteArray;
        }
        final byte[] array = new byte[bigInteger.bitLength() >> 3];
        System.arraycopy(byteArray, 1, array, 0, array.length);
        return array;
    }
    
    public static boolean isIncreasing(final int[] array) {
        for (int i = 1; i < array.length; ++i) {
            final int j = i - 1;
            if (array[j] >= array[i]) {
                final PrintStream out = System.out;
                final StringBuilder sb = new StringBuilder();
                sb.append("a[");
                sb.append(j);
                sb.append("] = ");
                sb.append(array[j]);
                sb.append(" >= ");
                sb.append(array[i]);
                sb.append(" = a[");
                sb.append(i);
                sb.append("]");
                out.println(sb.toString());
                return false;
            }
        }
        return true;
    }
    
    public static int isPower(int n, final int n2) {
        if (n <= 0) {
            return -1;
        }
        final int n3 = 0;
        int i;
        for (i = n, n = n3; i > 1; i /= n2, ++n) {
            if (i % n2 != 0) {
                return -1;
            }
        }
        return n;
    }
    
    public static boolean isPrime(final int n) {
        if (n < 2) {
            return false;
        }
        if (n == 2) {
            return true;
        }
        if ((n & 0x1) == 0x0) {
            return false;
        }
        if (n < 42) {
            int n2 = 0;
            while (true) {
                final int[] small_PRIMES = IntegerFunctions.SMALL_PRIMES;
                if (n2 >= small_PRIMES.length) {
                    break;
                }
                if (n == small_PRIMES[n2]) {
                    return true;
                }
                ++n2;
            }
        }
        return n % 3 != 0 && n % 5 != 0 && n % 7 != 0 && n % 11 != 0 && n % 13 != 0 && n % 17 != 0 && n % 19 != 0 && n % 23 != 0 && n % 29 != 0 && n % 31 != 0 && n % 37 != 0 && n % 41 != 0 && BigInteger.valueOf(n).isProbablePrime(20);
    }
    
    public static int jacobi(final BigInteger bigInteger, final BigInteger bigInteger2) {
        throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:780)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.enexpr(TypeTransformer.java:659)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:719)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s1stmt(TypeTransformer.java:810)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:840)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
    }
    
    public static BigInteger leastCommonMultiple(final BigInteger[] array) {
        final int length = array.length;
        BigInteger divide = array[0];
        for (int i = 1; i < length; ++i) {
            divide = divide.multiply(array[i]).divide(divide.gcd(array[i]));
        }
        return divide;
    }
    
    public static int leastDiv(int i) {
        int n = i;
        if (i < 0) {
            n = -i;
        }
        if (n == 0) {
            return 1;
        }
        if ((n & 0x1) == 0x0) {
            return 2;
        }
        for (i = 3; i <= n / i; i += 2) {
            if (n % i == 0) {
                return i;
            }
        }
        return n;
    }
    
    public static double log(double logBKM) {
        if (logBKM > 0.0 && logBKM < 1.0) {
            return -log(1.0 / logBKM);
        }
        int n = 0;
        double n2 = 1.0;
        for (double n3 = logBKM; n3 > 2.0; n3 /= 2.0, ++n, n2 *= 2.0) {}
        logBKM = logBKM(logBKM / n2);
        return n + logBKM;
    }
    
    public static double log(final long val) {
        final int floorLog = floorLog(BigInteger.valueOf(val));
        return floorLog + logBKM(val / (double)(1 << floorLog));
    }
    
    private static double logBKM(final double n) {
        double n2 = 1.0;
        double n3 = 0.0;
        int i = 0;
        double n4 = 1.0;
        while (i < 53) {
            final double n5 = n2 * n4 + n2;
            double n6 = n3;
            if (n5 <= n) {
                n6 = n3 + (new double[] { 1.0, 0.5849625007211562, 0.32192809488736235, 0.16992500144231237, 0.0874628412503394, 0.044394119358453436, 0.02236781302845451, 0.01122725542325412, 0.005624549193878107, 0.0028150156070540383, 0.0014081943928083889, 7.042690112466433E-4, 3.5217748030102726E-4, 1.7609948644250602E-4, 8.80524301221769E-5, 4.4026886827316716E-5, 2.2013611360340496E-5, 1.1006847667481442E-5, 5.503434330648604E-6, 2.751719789561283E-6, 1.375860550841138E-6, 6.879304394358497E-7, 3.4396526072176454E-7, 1.7198264061184464E-7, 8.599132286866321E-8, 4.299566207501687E-8, 2.1497831197679756E-8, 1.0748915638882709E-8, 5.374457829452062E-9, 2.687228917228708E-9, 1.3436144592400231E-9, 6.718072297764289E-10, 3.3590361492731876E-10, 1.6795180747343547E-10, 8.397590373916176E-11, 4.1987951870191886E-11, 2.0993975935248694E-11, 1.0496987967662534E-11, 5.2484939838408146E-12, 2.624246991922794E-12, 1.3121234959619935E-12, 6.56061747981146E-13, 3.2803087399061026E-13, 1.6401543699531447E-13, 8.200771849765956E-14, 4.1003859248830365E-14, 2.0501929624415328E-14, 1.02509648122077E-14, 5.1254824061038595E-15, 2.5627412030519317E-15, 1.2813706015259665E-15, 6.406853007629834E-16, 3.203426503814917E-16, 1.6017132519074588E-16, 8.008566259537294E-17, 4.004283129768647E-17, 2.0021415648843235E-17, 1.0010707824421618E-17, 5.005353912210809E-18, 2.5026769561054044E-18, 1.2513384780527022E-18, 6.256692390263511E-19, 3.1283461951317555E-19, 1.5641730975658778E-19, 7.820865487829389E-20, 3.9104327439146944E-20, 1.9552163719573472E-20, 9.776081859786736E-21, 4.888040929893368E-21, 2.444020464946684E-21, 1.222010232473342E-21, 6.11005116236671E-22, 3.055025581183355E-22, 1.5275127905916775E-22, 7.637563952958387E-23, 3.818781976479194E-23, 1.909390988239597E-23, 9.546954941197984E-24, 4.773477470598992E-24, 2.386738735299496E-24, 1.193369367649748E-24, 5.96684683824874E-25, 2.98342341912437E-25, 1.491711709562185E-25, 7.458558547810925E-26, 3.7292792739054626E-26, 1.8646396369527313E-26, 9.323198184763657E-27, 4.661599092381828E-27, 2.330799546190914E-27, 1.165399773095457E-27, 5.826998865477285E-28, 2.9134994327386427E-28, 1.4567497163693213E-28, 7.283748581846607E-29, 3.6418742909233034E-29, 1.8209371454616517E-29, 9.104685727308258E-30, 4.552342863654129E-30, 2.2761714318270646E-30 })[i];
                n2 = n5;
            }
            n4 *= 0.5;
            ++i;
            n3 = n6;
        }
        return n3;
    }
    
    public static int maxPower(final int n) {
        int n2 = 0;
        int n3 = 0;
        if (n != 0) {
            int n4 = 1;
            while (true) {
                n2 = n3;
                if ((n & n4) != 0x0) {
                    break;
                }
                ++n3;
                n4 <<= 1;
            }
        }
        return n2;
    }
    
    public static long mod(long n, final long n2) {
        final long n3 = n %= n2;
        if (n3 < 0L) {
            n = n3 + n2;
        }
        return n;
    }
    
    public static int modInverse(final int n, final int n2) {
        return BigInteger.valueOf(n).modInverse(BigInteger.valueOf(n2)).intValue();
    }
    
    public static long modInverse(final long val, final long val2) {
        return BigInteger.valueOf(val).modInverse(BigInteger.valueOf(val2)).longValue();
    }
    
    public static int modPow(int n, int i, final int n2) {
        if (n2 > 0 && n2 * n2 <= Integer.MAX_VALUE && i >= 0) {
            n = (n % n2 + n2) % n2;
            int n3 = 1;
            while (i > 0) {
                int n4 = n3;
                if ((i & 0x1) == 0x1) {
                    n4 = n3 * n % n2;
                }
                n = n * n % n2;
                i >>>= 1;
                n3 = n4;
            }
            return n3;
        }
        return 0;
    }
    
    public static BigInteger nextPrime(final long n) {
        if (n <= 1L) {
            return BigInteger.valueOf(2L);
        }
        if (n == 2L) {
            return BigInteger.valueOf(3L);
        }
        long n2 = n + 1L + (n & 0x1L);
        int n3 = 0;
        long val = 0L;
        while (n2 <= n << 1 && n3 == 0) {
            for (long n4 = 3L; n4 <= n2 >> 1 && n3 == 0; n4 += 2L) {
                if (n2 % n4 == 0L) {
                    n3 = 1;
                }
            }
            if (n3 == 0) {
                val = n2;
            }
            n3 ^= 0x1;
            n2 += 2L;
        }
        return BigInteger.valueOf(val);
    }
    
    public static BigInteger nextProbablePrime(final BigInteger bigInteger) {
        return nextProbablePrime(bigInteger, 20);
    }
    
    public static BigInteger nextProbablePrime(BigInteger bigInteger, final int certainty) {
        if (bigInteger.signum() < 0 || bigInteger.signum() == 0 || bigInteger.equals(IntegerFunctions.ONE)) {
            return IntegerFunctions.TWO;
        }
        final BigInteger bigInteger2 = bigInteger = bigInteger.add(IntegerFunctions.ONE);
        while (true) {
        Label_0218:
            while (true) {
                Label_0067: {
                    if (bigInteger2.testBit(0)) {
                        break Label_0067;
                    }
                    final BigInteger one = IntegerFunctions.ONE;
                    bigInteger = bigInteger2;
                    final BigInteger two = one;
                    bigInteger = bigInteger.add(two);
                }
                if (bigInteger.bitLength() <= 6) {
                    break Label_0218;
                }
                final long longValue = bigInteger.remainder(BigInteger.valueOf(152125131763605L)).longValue();
                if (longValue % 3L != 0L && longValue % 5L != 0L && longValue % 7L != 0L && longValue % 11L != 0L && longValue % 13L != 0L && longValue % 17L != 0L && longValue % 19L != 0L && longValue % 23L != 0L && longValue % 29L != 0L && longValue % 31L != 0L && longValue % 37L != 0L && longValue % 41L != 0L) {
                    break Label_0218;
                }
                final BigInteger two = IntegerFunctions.TWO;
                continue;
            }
            if (bigInteger.bitLength() < 4) {
                return bigInteger;
            }
            if (bigInteger.isProbablePrime(certainty)) {
                return bigInteger;
            }
            continue;
        }
    }
    
    public static int nextSmallerPrime(int n) {
        if (n <= 2) {
            return 1;
        }
        if (n == 3) {
            return 2;
        }
        int n2 = n;
        while (true) {
            Label_0033: {
                if ((n & 0x1) == 0x0) {
                    --n;
                    break Label_0033;
                }
                n = n2 - 2;
            }
            if (n > 3 && !isPrime(n)) {
                n2 = n;
                continue;
            }
            break;
        }
        return n;
    }
    
    public static BigInteger octetsToInteger(final byte[] array) {
        return octetsToInteger(array, 0, array.length);
    }
    
    public static BigInteger octetsToInteger(final byte[] array, final int n, final int n2) {
        final byte[] val = new byte[n2 + 1];
        val[0] = 0;
        System.arraycopy(array, n, val, 1, n2);
        return new BigInteger(val);
    }
    
    public static int order(final int i, final int j) {
        int k = i % j;
        if (k != 0) {
            int n = 1;
            while (k != 1) {
                final int n2 = k * i % j;
                if ((k = n2) < 0) {
                    k = n2 + j;
                }
                ++n;
            }
            return n;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(i);
        sb.append(" is not an element of Z/(");
        sb.append(j);
        sb.append("Z)^*; it is not meaningful to compute its order.");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static boolean passesSmallPrimeTest(final BigInteger bigInteger) {
        final int[] array2;
        final int[] array = array2 = new int[239];
        array2[0] = 2;
        array2[1] = 3;
        array2[2] = 5;
        array2[3] = 7;
        array2[4] = 11;
        array2[5] = 13;
        array2[6] = 17;
        array2[7] = 19;
        array2[8] = 23;
        array2[9] = 29;
        array2[10] = 31;
        array2[11] = 37;
        array2[12] = 41;
        array2[13] = 43;
        array2[14] = 47;
        array2[15] = 53;
        array2[16] = 59;
        array2[17] = 61;
        array2[18] = 67;
        array2[19] = 71;
        array2[20] = 73;
        array2[21] = 79;
        array2[22] = 83;
        array2[23] = 89;
        array2[24] = 97;
        array2[25] = 101;
        array2[26] = 103;
        array2[27] = 107;
        array2[28] = 109;
        array2[29] = 113;
        array2[30] = 127;
        array2[31] = 131;
        array2[32] = 137;
        array2[33] = 139;
        array2[34] = 149;
        array2[35] = 151;
        array2[36] = 157;
        array2[37] = 163;
        array2[38] = 167;
        array2[39] = 173;
        array2[40] = 179;
        array2[41] = 181;
        array2[42] = 191;
        array2[43] = 193;
        array2[44] = 197;
        array2[45] = 199;
        array2[46] = 211;
        array2[47] = 223;
        array2[48] = 227;
        array2[49] = 229;
        array2[50] = 233;
        array2[51] = 239;
        array2[52] = 241;
        array2[53] = 251;
        array2[54] = 257;
        array2[55] = 263;
        array2[56] = 269;
        array2[57] = 271;
        array2[58] = 277;
        array2[59] = 281;
        array2[60] = 283;
        array2[61] = 293;
        array2[62] = 307;
        array2[63] = 311;
        array2[64] = 313;
        array2[65] = 317;
        array2[66] = 331;
        array2[67] = 337;
        array2[68] = 347;
        array2[69] = 349;
        array2[70] = 353;
        array2[71] = 359;
        array2[72] = 367;
        array2[73] = 373;
        array2[74] = 379;
        array2[75] = 383;
        array2[76] = 389;
        array2[77] = 397;
        array2[78] = 401;
        array2[79] = 409;
        array2[80] = 419;
        array2[81] = 421;
        array2[82] = 431;
        array2[83] = 433;
        array2[84] = 439;
        array2[85] = 443;
        array2[86] = 449;
        array2[87] = 457;
        array2[88] = 461;
        array2[89] = 463;
        array2[90] = 467;
        array2[91] = 479;
        array2[92] = 487;
        array2[93] = 491;
        array2[94] = 499;
        array2[95] = 503;
        array2[96] = 509;
        array2[97] = 521;
        array2[98] = 523;
        array2[99] = 541;
        array2[100] = 547;
        array2[101] = 557;
        array2[102] = 563;
        array2[103] = 569;
        array2[104] = 571;
        array2[105] = 577;
        array2[106] = 587;
        array2[107] = 593;
        array2[108] = 599;
        array2[109] = 601;
        array2[110] = 607;
        array2[111] = 613;
        array2[112] = 617;
        array2[113] = 619;
        array2[114] = 631;
        array2[115] = 641;
        array2[116] = 643;
        array2[117] = 647;
        array2[118] = 653;
        array2[119] = 659;
        array2[120] = 661;
        array2[121] = 673;
        array2[122] = 677;
        array2[123] = 683;
        array2[124] = 691;
        array2[125] = 701;
        array2[126] = 709;
        array2[127] = 719;
        array2[128] = 727;
        array2[129] = 733;
        array2[130] = 739;
        array2[131] = 743;
        array2[132] = 751;
        array2[133] = 757;
        array2[134] = 761;
        array2[135] = 769;
        array2[136] = 773;
        array2[137] = 787;
        array2[138] = 797;
        array2[139] = 809;
        array2[140] = 811;
        array2[141] = 821;
        array2[142] = 823;
        array2[143] = 827;
        array2[144] = 829;
        array2[145] = 839;
        array2[146] = 853;
        array2[147] = 857;
        array2[148] = 859;
        array2[149] = 863;
        array2[150] = 877;
        array2[151] = 881;
        array2[152] = 883;
        array2[153] = 887;
        array2[154] = 907;
        array2[155] = 911;
        array2[156] = 919;
        array2[157] = 929;
        array2[158] = 937;
        array2[159] = 941;
        array2[160] = 947;
        array2[161] = 953;
        array2[162] = 967;
        array2[163] = 971;
        array2[164] = 977;
        array2[165] = 983;
        array2[166] = 991;
        array2[167] = 997;
        array2[168] = 1009;
        array2[169] = 1013;
        array2[170] = 1019;
        array2[171] = 1021;
        array2[172] = 1031;
        array2[173] = 1033;
        array2[174] = 1039;
        array2[175] = 1049;
        array2[176] = 1051;
        array2[177] = 1061;
        array2[178] = 1063;
        array2[179] = 1069;
        array2[180] = 1087;
        array2[181] = 1091;
        array2[182] = 1093;
        array2[183] = 1097;
        array2[184] = 1103;
        array2[185] = 1109;
        array2[186] = 1117;
        array2[187] = 1123;
        array2[188] = 1129;
        array2[189] = 1151;
        array2[190] = 1153;
        array2[191] = 1163;
        array2[192] = 1171;
        array2[193] = 1181;
        array2[194] = 1187;
        array2[195] = 1193;
        array2[196] = 1201;
        array2[197] = 1213;
        array2[198] = 1217;
        array2[199] = 1223;
        array2[200] = 1229;
        array2[201] = 1231;
        array2[202] = 1237;
        array2[203] = 1249;
        array2[204] = 1259;
        array2[205] = 1277;
        array2[206] = 1279;
        array2[207] = 1283;
        array2[208] = 1289;
        array2[209] = 1291;
        array2[210] = 1297;
        array2[211] = 1301;
        array2[212] = 1303;
        array2[213] = 1307;
        array2[214] = 1319;
        array2[215] = 1321;
        array2[216] = 1327;
        array2[217] = 1361;
        array2[218] = 1367;
        array2[219] = 1373;
        array2[220] = 1381;
        array2[221] = 1399;
        array2[222] = 1409;
        array2[223] = 1423;
        array2[224] = 1427;
        array2[225] = 1429;
        array2[226] = 1433;
        array2[227] = 1439;
        array2[228] = 1447;
        array2[229] = 1451;
        array2[230] = 1453;
        array2[231] = 1459;
        array2[232] = 1471;
        array2[233] = 1481;
        array2[234] = 1483;
        array2[235] = 1487;
        array2[236] = 1489;
        array2[237] = 1493;
        array2[238] = 1499;
        for (int i = 0; i < array.length; ++i) {
            if (bigInteger.mod(BigInteger.valueOf(array[i])).equals(IntegerFunctions.ZERO)) {
                return false;
            }
        }
        return true;
    }
    
    public static int pow(int n, int i) {
        int n2 = 1;
        while (i > 0) {
            int n3 = n2;
            if ((i & 0x1) == 0x1) {
                n3 = n2 * n;
            }
            n *= n;
            i >>>= 1;
            n2 = n3;
        }
        return n2;
    }
    
    public static long pow(long n, int i) {
        long n2 = 1L;
        while (i > 0) {
            long n3 = n2;
            if ((i & 0x1) == 0x1) {
                n3 = n2 * n;
            }
            n *= n;
            i >>>= 1;
            n2 = n3;
        }
        return n2;
    }
    
    public static BigInteger randomize(final BigInteger bigInteger) {
        if (IntegerFunctions.sr == null) {
            IntegerFunctions.sr = CryptoServicesRegistrar.getSecureRandom();
        }
        return randomize(bigInteger, IntegerFunctions.sr);
    }
    
    public static BigInteger randomize(final BigInteger bigInteger, final SecureRandom secureRandom) {
        final int bitLength = bigInteger.bitLength();
        final BigInteger value = BigInteger.valueOf(0L);
        SecureRandom secureRandom2 = secureRandom;
        if (secureRandom == null) {
            secureRandom2 = IntegerFunctions.sr;
            if (secureRandom2 == null) {
                secureRandom2 = CryptoServicesRegistrar.getSecureRandom();
            }
        }
        int i = 0;
        BigInteger randomBigInteger = value;
        while (i < 20) {
            randomBigInteger = BigIntegers.createRandomBigInteger(bitLength, secureRandom2);
            if (randomBigInteger.compareTo(bigInteger) < 0) {
                return randomBigInteger;
            }
            ++i;
        }
        return randomBigInteger.mod(bigInteger);
    }
    
    public static BigInteger reduceInto(final BigInteger bigInteger, final BigInteger val, final BigInteger bigInteger2) {
        return bigInteger.subtract(val).mod(bigInteger2.subtract(val)).add(val);
    }
    
    public static BigInteger ressol(BigInteger val, final BigInteger bigInteger) throws IllegalArgumentException {
        BigInteger add;
        if (val.compareTo(IntegerFunctions.ZERO) < 0) {
            add = val.add(bigInteger);
        }
        else {
            add = val;
        }
        if (add.equals(IntegerFunctions.ZERO)) {
            return IntegerFunctions.ZERO;
        }
        if (bigInteger.equals(IntegerFunctions.TWO)) {
            return add;
        }
        if (bigInteger.testBit(0) && bigInteger.testBit(1)) {
            if (jacobi(add, bigInteger) == 1) {
                return add.modPow(bigInteger.add(IntegerFunctions.ONE).shiftRight(2), bigInteger);
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("No quadratic residue: ");
            sb.append(add);
            sb.append(", ");
            sb.append(bigInteger);
            throw new IllegalArgumentException(sb.toString());
        }
        else {
            val = bigInteger.subtract(IntegerFunctions.ONE);
            long n = 0L;
            while (!val.testBit(0)) {
                ++n;
                val = val.shiftRight(1);
            }
            final BigInteger shiftRight = val.subtract(IntegerFunctions.ONE).shiftRight(1);
            val = add.modPow(shiftRight, bigInteger);
            final BigInteger remainder = val.multiply(val).remainder(bigInteger).multiply(add).remainder(bigInteger);
            final BigInteger remainder2 = val.multiply(add).remainder(bigInteger);
            if (remainder.equals(IntegerFunctions.ONE)) {
                return remainder2;
            }
            for (val = IntegerFunctions.TWO; jacobi(val, bigInteger) == 1; val = val.add(IntegerFunctions.ONE)) {}
            final BigInteger modPow = val.modPow(shiftRight.multiply(IntegerFunctions.TWO).add(IntegerFunctions.ONE), bigInteger);
            val = remainder;
            long n2 = n;
            BigInteger remainder3 = remainder2;
            long n3;
            for (BigInteger remainder4 = modPow; val.compareTo(IntegerFunctions.ONE) == 1; val = val.multiply(remainder4).mod(bigInteger), n2 = n3) {
                n3 = 0L;
                for (BigInteger mod = val; !mod.equals(IntegerFunctions.ONE); mod = mod.multiply(mod).mod(bigInteger), ++n3) {}
                final long n4 = n2 - n3;
                if (n4 == 0L) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("No quadratic residue: ");
                    sb2.append(add);
                    sb2.append(", ");
                    sb2.append(bigInteger);
                    throw new IllegalArgumentException(sb2.toString());
                }
                BigInteger exponent = IntegerFunctions.ONE;
                for (long n5 = 0L; n5 < n4 - 1L; ++n5) {
                    exponent = exponent.shiftLeft(1);
                }
                final BigInteger modPow2 = remainder4.modPow(exponent, bigInteger);
                remainder3 = remainder3.multiply(modPow2).remainder(bigInteger);
                remainder4 = modPow2.multiply(modPow2).remainder(bigInteger);
            }
            return remainder3;
        }
    }
    
    public static BigInteger squareRoot(final BigInteger bigInteger) {
        throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:780)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.enexpr(TypeTransformer.java:659)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:719)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e2expr(TypeTransformer.java:632)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:716)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e1expr(TypeTransformer.java:539)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:713)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.enexpr(TypeTransformer.java:698)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:719)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.enexpr(TypeTransformer.java:698)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:719)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s2stmt(TypeTransformer.java:820)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:843)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
    }
}
