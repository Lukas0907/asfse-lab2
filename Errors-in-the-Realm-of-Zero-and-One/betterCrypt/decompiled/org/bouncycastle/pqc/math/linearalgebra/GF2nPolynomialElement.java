// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.math.linearalgebra;

import java.math.BigInteger;
import java.util.Random;

public class GF2nPolynomialElement extends GF2nElement
{
    private static final int[] bitMask;
    private GF2Polynomial polynomial;
    
    static {
        bitMask = new int[] { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608, 16777216, 33554432, 67108864, 134217728, 268435456, 536870912, 1073741824, Integer.MIN_VALUE, 0 };
    }
    
    public GF2nPolynomialElement(final GF2nPolynomialElement gf2nPolynomialElement) {
        this.mField = gf2nPolynomialElement.mField;
        this.mDegree = gf2nPolynomialElement.mDegree;
        this.polynomial = new GF2Polynomial(gf2nPolynomialElement.polynomial);
    }
    
    public GF2nPolynomialElement(final GF2nPolynomialField mField, final Random random) {
        this.mField = mField;
        this.mDegree = this.mField.getDegree();
        this.polynomial = new GF2Polynomial(this.mDegree);
        this.randomize(random);
    }
    
    public GF2nPolynomialElement(final GF2nPolynomialField mField, final GF2Polynomial gf2Polynomial) {
        this.mField = mField;
        this.mDegree = this.mField.getDegree();
        (this.polynomial = new GF2Polynomial(gf2Polynomial)).expandN(this.mDegree);
    }
    
    public GF2nPolynomialElement(final GF2nPolynomialField mField, final byte[] array) {
        this.mField = mField;
        this.mDegree = this.mField.getDegree();
        (this.polynomial = new GF2Polynomial(this.mDegree, array)).expandN(this.mDegree);
    }
    
    public GF2nPolynomialElement(final GF2nPolynomialField mField, final int[] array) {
        this.mField = mField;
        this.mDegree = this.mField.getDegree();
        (this.polynomial = new GF2Polynomial(this.mDegree, array)).expandN(mField.mDegree);
    }
    
    public static GF2nPolynomialElement ONE(final GF2nPolynomialField gf2nPolynomialField) {
        return new GF2nPolynomialElement(gf2nPolynomialField, new GF2Polynomial(gf2nPolynomialField.getDegree(), new int[] { 1 }));
    }
    
    public static GF2nPolynomialElement ZERO(final GF2nPolynomialField gf2nPolynomialField) {
        return new GF2nPolynomialElement(gf2nPolynomialField, new GF2Polynomial(gf2nPolynomialField.getDegree()));
    }
    
    private GF2Polynomial getGF2Polynomial() {
        return new GF2Polynomial(this.polynomial);
    }
    
    private GF2nPolynomialElement halfTrace() throws RuntimeException {
        if ((this.mDegree & 0x1) != 0x0) {
            final GF2nPolynomialElement gf2nPolynomialElement = new GF2nPolynomialElement(this);
            for (int i = 1; i <= this.mDegree - 1 >> 1; ++i) {
                gf2nPolynomialElement.squareThis();
                gf2nPolynomialElement.squareThis();
                gf2nPolynomialElement.addToThis(this);
            }
            return gf2nPolynomialElement;
        }
        throw new RuntimeException();
    }
    
    private void randomize(final Random random) {
        this.polynomial.expandN(this.mDegree);
        this.polynomial.randomize(random);
    }
    
    private void reducePentanomialBitwise(final int[] array) {
        final int mDegree = this.mDegree;
        final int n = array[2];
        final int mDegree2 = this.mDegree;
        final int n2 = array[1];
        final int mDegree3 = this.mDegree;
        final int n3 = array[0];
        for (int i = this.polynomial.getLength() - 1; i >= this.mDegree; --i) {
            if (this.polynomial.testBit(i)) {
                this.polynomial.xorBit(i);
                this.polynomial.xorBit(i - (mDegree - n));
                this.polynomial.xorBit(i - (mDegree2 - n2));
                this.polynomial.xorBit(i - (mDegree3 - n3));
                this.polynomial.xorBit(i - this.mDegree);
            }
        }
        this.polynomial.reduceN();
        this.polynomial.expandN(this.mDegree);
    }
    
    private void reduceThis() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.polynomial:Lorg/bouncycastle/pqc/math/linearalgebra/GF2Polynomial;
        //     4: invokevirtual   org/bouncycastle/pqc/math/linearalgebra/GF2Polynomial.getLength:()I
        //     7: aload_0        
        //     8: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.mDegree:I
        //    11: if_icmple       212
        //    14: aload_0        
        //    15: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.mField:Lorg/bouncycastle/pqc/math/linearalgebra/GF2nField;
        //    18: checkcast       Lorg/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialField;
        //    21: invokevirtual   org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialField.isTrinomial:()Z
        //    24: ifeq            97
        //    27: aload_0        
        //    28: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.mField:Lorg/bouncycastle/pqc/math/linearalgebra/GF2nField;
        //    31: checkcast       Lorg/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialField;
        //    34: invokevirtual   org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialField.getTc:()I
        //    37: istore_1       
        //    38: aload_0        
        //    39: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.mDegree:I
        //    42: iload_1        
        //    43: isub           
        //    44: bipush          32
        //    46: if_icmple       81
        //    49: aload_0        
        //    50: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.polynomial:Lorg/bouncycastle/pqc/math/linearalgebra/GF2Polynomial;
        //    53: invokevirtual   org/bouncycastle/pqc/math/linearalgebra/GF2Polynomial.getLength:()I
        //    56: aload_0        
        //    57: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.mDegree:I
        //    60: iconst_1       
        //    61: ishl           
        //    62: if_icmple       68
        //    65: goto            81
        //    68: aload_0        
        //    69: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.polynomial:Lorg/bouncycastle/pqc/math/linearalgebra/GF2Polynomial;
        //    72: aload_0        
        //    73: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.mDegree:I
        //    76: iload_1        
        //    77: invokevirtual   org/bouncycastle/pqc/math/linearalgebra/GF2Polynomial.reduceTrinomial:(II)V
        //    80: return         
        //    81: aload_0        
        //    82: iload_1        
        //    83: invokespecial   org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.reduceTrinomialBitwise:(I)V
        //    86: return         
        //    87: new             Ljava/lang/RuntimeException;
        //    90: dup            
        //    91: ldc             "GF2nPolynomialElement.reduce: the field polynomial is not a trinomial"
        //    93: invokespecial   java/lang/RuntimeException.<init>:(Ljava/lang/String;)V
        //    96: athrow         
        //    97: aload_0        
        //    98: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.mField:Lorg/bouncycastle/pqc/math/linearalgebra/GF2nField;
        //   101: checkcast       Lorg/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialField;
        //   104: invokevirtual   org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialField.isPentanomial:()Z
        //   107: ifeq            182
        //   110: aload_0        
        //   111: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.mField:Lorg/bouncycastle/pqc/math/linearalgebra/GF2nField;
        //   114: checkcast       Lorg/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialField;
        //   117: invokevirtual   org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialField.getPc:()[I
        //   120: astore_2       
        //   121: aload_0        
        //   122: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.mDegree:I
        //   125: aload_2        
        //   126: iconst_2       
        //   127: iaload         
        //   128: isub           
        //   129: bipush          32
        //   131: if_icmple       166
        //   134: aload_0        
        //   135: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.polynomial:Lorg/bouncycastle/pqc/math/linearalgebra/GF2Polynomial;
        //   138: invokevirtual   org/bouncycastle/pqc/math/linearalgebra/GF2Polynomial.getLength:()I
        //   141: aload_0        
        //   142: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.mDegree:I
        //   145: iconst_1       
        //   146: ishl           
        //   147: if_icmple       153
        //   150: goto            166
        //   153: aload_0        
        //   154: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.polynomial:Lorg/bouncycastle/pqc/math/linearalgebra/GF2Polynomial;
        //   157: aload_0        
        //   158: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.mDegree:I
        //   161: aload_2        
        //   162: invokevirtual   org/bouncycastle/pqc/math/linearalgebra/GF2Polynomial.reducePentanomial:(I[I)V
        //   165: return         
        //   166: aload_0        
        //   167: aload_2        
        //   168: invokespecial   org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.reducePentanomialBitwise:([I)V
        //   171: return         
        //   172: new             Ljava/lang/RuntimeException;
        //   175: dup            
        //   176: ldc             "GF2nPolynomialElement.reduce: the field polynomial is not a pentanomial"
        //   178: invokespecial   java/lang/RuntimeException.<init>:(Ljava/lang/String;)V
        //   181: athrow         
        //   182: aload_0        
        //   183: aload_0        
        //   184: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.polynomial:Lorg/bouncycastle/pqc/math/linearalgebra/GF2Polynomial;
        //   187: aload_0        
        //   188: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.mField:Lorg/bouncycastle/pqc/math/linearalgebra/GF2nField;
        //   191: invokevirtual   org/bouncycastle/pqc/math/linearalgebra/GF2nField.getFieldPolynomial:()Lorg/bouncycastle/pqc/math/linearalgebra/GF2Polynomial;
        //   194: invokevirtual   org/bouncycastle/pqc/math/linearalgebra/GF2Polynomial.remainder:(Lorg/bouncycastle/pqc/math/linearalgebra/GF2Polynomial;)Lorg/bouncycastle/pqc/math/linearalgebra/GF2Polynomial;
        //   197: putfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.polynomial:Lorg/bouncycastle/pqc/math/linearalgebra/GF2Polynomial;
        //   200: aload_0        
        //   201: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.polynomial:Lorg/bouncycastle/pqc/math/linearalgebra/GF2Polynomial;
        //   204: aload_0        
        //   205: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.mDegree:I
        //   208: invokevirtual   org/bouncycastle/pqc/math/linearalgebra/GF2Polynomial.expandN:(I)V
        //   211: return         
        //   212: aload_0        
        //   213: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.polynomial:Lorg/bouncycastle/pqc/math/linearalgebra/GF2Polynomial;
        //   216: invokevirtual   org/bouncycastle/pqc/math/linearalgebra/GF2Polynomial.getLength:()I
        //   219: aload_0        
        //   220: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.mDegree:I
        //   223: if_icmpge       237
        //   226: aload_0        
        //   227: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.polynomial:Lorg/bouncycastle/pqc/math/linearalgebra/GF2Polynomial;
        //   230: aload_0        
        //   231: getfield        org/bouncycastle/pqc/math/linearalgebra/GF2nPolynomialElement.mDegree:I
        //   234: invokevirtual   org/bouncycastle/pqc/math/linearalgebra/GF2Polynomial.expandN:(I)V
        //   237: return         
        //   238: astore_2       
        //   239: goto            87
        //   242: astore_2       
        //   243: goto            172
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                        
        //  -----  -----  -----  -----  ----------------------------
        //  27     38     238    97     Ljava/lang/RuntimeException;
        //  110    121    242    182    Ljava/lang/RuntimeException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0153:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void reduceTrinomialBitwise(final int n) {
        final int mDegree = this.mDegree;
        for (int i = this.polynomial.getLength() - 1; i >= this.mDegree; --i) {
            if (this.polynomial.testBit(i)) {
                this.polynomial.xorBit(i);
                this.polynomial.xorBit(i - (mDegree - n));
                this.polynomial.xorBit(i - this.mDegree);
            }
        }
        this.polynomial.reduceN();
        this.polynomial.expandN(this.mDegree);
    }
    
    @Override
    public GFElement add(final GFElement gfElement) throws RuntimeException {
        final GF2nPolynomialElement gf2nPolynomialElement = new GF2nPolynomialElement(this);
        gf2nPolynomialElement.addToThis(gfElement);
        return gf2nPolynomialElement;
    }
    
    @Override
    public void addToThis(final GFElement gfElement) throws RuntimeException {
        if (!(gfElement instanceof GF2nPolynomialElement)) {
            throw new RuntimeException();
        }
        final GF2nField mField = this.mField;
        final GF2nPolynomialElement gf2nPolynomialElement = (GF2nPolynomialElement)gfElement;
        if (mField.equals(gf2nPolynomialElement.mField)) {
            this.polynomial.addToThis(gf2nPolynomialElement.polynomial);
            return;
        }
        throw new RuntimeException();
    }
    
    @Override
    void assignOne() {
        this.polynomial.assignOne();
    }
    
    @Override
    void assignZero() {
        this.polynomial.assignZero();
    }
    
    @Override
    public Object clone() {
        return new GF2nPolynomialElement(this);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof GF2nPolynomialElement)) {
            return false;
        }
        final GF2nPolynomialElement gf2nPolynomialElement = (GF2nPolynomialElement)o;
        return (this.mField == gf2nPolynomialElement.mField || this.mField.getFieldPolynomial().equals(gf2nPolynomialElement.mField.getFieldPolynomial())) && this.polynomial.equals(gf2nPolynomialElement.polynomial);
    }
    
    @Override
    public int hashCode() {
        return this.mField.hashCode() + this.polynomial.hashCode();
    }
    
    @Override
    public GF2nElement increase() {
        final GF2nPolynomialElement gf2nPolynomialElement = new GF2nPolynomialElement(this);
        gf2nPolynomialElement.increaseThis();
        return gf2nPolynomialElement;
    }
    
    @Override
    public void increaseThis() {
        this.polynomial.increaseThis();
    }
    
    @Override
    public GFElement invert() throws ArithmeticException {
        return this.invertMAIA();
    }
    
    public GF2nPolynomialElement invertEEA() throws ArithmeticException {
        if (!this.isZero()) {
            GF2Polynomial gf2Polynomial = new GF2Polynomial(this.mDegree + 32, "ONE");
            gf2Polynomial.reduceN();
            GF2Polynomial gf2Polynomial2 = new GF2Polynomial(this.mDegree + 32);
            gf2Polynomial2.reduceN();
            GF2Polynomial gf2Polynomial3 = this.getGF2Polynomial();
            GF2Polynomial fieldPolynomial = this.mField.getFieldPolynomial();
            gf2Polynomial3.reduceN();
            while (!gf2Polynomial3.isOne()) {
                gf2Polynomial3.reduceN();
                fieldPolynomial.reduceN();
                final int n = gf2Polynomial3.getLength() - fieldPolynomial.getLength();
                GF2Polynomial gf2Polynomial4 = gf2Polynomial;
                GF2Polynomial gf2Polynomial5 = gf2Polynomial2;
                GF2Polynomial gf2Polynomial6 = gf2Polynomial3;
                GF2Polynomial gf2Polynomial7 = fieldPolynomial;
                int n2;
                if ((n2 = n) < 0) {
                    n2 = -n;
                    gf2Polynomial.reduceN();
                    gf2Polynomial7 = gf2Polynomial3;
                    gf2Polynomial6 = fieldPolynomial;
                    gf2Polynomial5 = gf2Polynomial;
                    gf2Polynomial4 = gf2Polynomial2;
                }
                gf2Polynomial6.shiftLeftAddThis(gf2Polynomial7, n2);
                gf2Polynomial4.shiftLeftAddThis(gf2Polynomial5, n2);
                gf2Polynomial = gf2Polynomial4;
                gf2Polynomial2 = gf2Polynomial5;
                gf2Polynomial3 = gf2Polynomial6;
                fieldPolynomial = gf2Polynomial7;
            }
            gf2Polynomial.reduceN();
            return new GF2nPolynomialElement((GF2nPolynomialField)this.mField, gf2Polynomial);
        }
        throw new ArithmeticException();
    }
    
    public GF2nPolynomialElement invertMAIA() throws ArithmeticException {
        if (!this.isZero()) {
            GF2Polynomial gf2Polynomial = new GF2Polynomial(this.mDegree, "ONE");
            GF2Polynomial gf2Polynomial2 = new GF2Polynomial(this.mDegree);
            GF2Polynomial gf2Polynomial3 = this.getGF2Polynomial();
            GF2Polynomial fieldPolynomial = this.mField.getFieldPolynomial();
            while (true) {
                if (!gf2Polynomial3.testBit(0)) {
                    gf2Polynomial3.shiftRightThis();
                    if (gf2Polynomial.testBit(0)) {
                        gf2Polynomial.addToThis(this.mField.getFieldPolynomial());
                    }
                    gf2Polynomial.shiftRightThis();
                }
                else {
                    if (gf2Polynomial3.isOne()) {
                        break;
                    }
                    gf2Polynomial3.reduceN();
                    fieldPolynomial.reduceN();
                    GF2Polynomial gf2Polynomial4 = gf2Polynomial;
                    GF2Polynomial gf2Polynomial5 = gf2Polynomial2;
                    GF2Polynomial gf2Polynomial6 = gf2Polynomial3;
                    GF2Polynomial gf2Polynomial7 = fieldPolynomial;
                    if (gf2Polynomial3.getLength() < fieldPolynomial.getLength()) {
                        gf2Polynomial7 = gf2Polynomial3;
                        gf2Polynomial6 = fieldPolynomial;
                        gf2Polynomial5 = gf2Polynomial;
                        gf2Polynomial4 = gf2Polynomial2;
                    }
                    gf2Polynomial6.addToThis(gf2Polynomial7);
                    gf2Polynomial4.addToThis(gf2Polynomial5);
                    gf2Polynomial = gf2Polynomial4;
                    gf2Polynomial2 = gf2Polynomial5;
                    gf2Polynomial3 = gf2Polynomial6;
                    fieldPolynomial = gf2Polynomial7;
                }
            }
            return new GF2nPolynomialElement((GF2nPolynomialField)this.mField, gf2Polynomial);
        }
        throw new ArithmeticException();
    }
    
    public GF2nPolynomialElement invertSquare() throws ArithmeticException {
        if (!this.isZero()) {
            final int n = this.mField.getDegree() - 1;
            final GF2nPolynomialElement gf2nPolynomialElement = new GF2nPolynomialElement(this);
            gf2nPolynomialElement.polynomial.expandN((this.mDegree << 1) + 32);
            gf2nPolynomialElement.polynomial.reduceN();
            int i = IntegerFunctions.floorLog(n) - 1;
            int n2 = 1;
            while (i >= 0) {
                final GF2nPolynomialElement gf2nPolynomialElement2 = new GF2nPolynomialElement(gf2nPolynomialElement);
                for (int j = 1; j <= n2; ++j) {
                    gf2nPolynomialElement2.squareThisPreCalc();
                }
                gf2nPolynomialElement.multiplyThisBy(gf2nPolynomialElement2);
                final int n3 = n2 <<= 1;
                if ((GF2nPolynomialElement.bitMask[i] & n) != 0x0) {
                    gf2nPolynomialElement.squareThisPreCalc();
                    gf2nPolynomialElement.multiplyThisBy(this);
                    n2 = n3 + 1;
                }
                --i;
            }
            gf2nPolynomialElement.squareThisPreCalc();
            return gf2nPolynomialElement;
        }
        throw new ArithmeticException();
    }
    
    @Override
    public boolean isOne() {
        return this.polynomial.isOne();
    }
    
    @Override
    public boolean isZero() {
        return this.polynomial.isZero();
    }
    
    @Override
    public GFElement multiply(final GFElement gfElement) throws RuntimeException {
        final GF2nPolynomialElement gf2nPolynomialElement = new GF2nPolynomialElement(this);
        gf2nPolynomialElement.multiplyThisBy(gfElement);
        return gf2nPolynomialElement;
    }
    
    @Override
    public void multiplyThisBy(final GFElement gfElement) throws RuntimeException {
        if (!(gfElement instanceof GF2nPolynomialElement)) {
            throw new RuntimeException();
        }
        final GF2nField mField = this.mField;
        final GF2nPolynomialElement gf2nPolynomialElement = (GF2nPolynomialElement)gfElement;
        if (!mField.equals(gf2nPolynomialElement.mField)) {
            throw new RuntimeException();
        }
        if (this.equals(gfElement)) {
            this.squareThis();
            return;
        }
        this.polynomial = this.polynomial.multiply(gf2nPolynomialElement.polynomial);
        this.reduceThis();
    }
    
    public GF2nPolynomialElement power(final int n) {
        if (n == 1) {
            return new GF2nPolynomialElement(this);
        }
        final GF2nPolynomialElement one = ONE((GF2nPolynomialField)this.mField);
        if (n == 0) {
            return one;
        }
        final GF2nPolynomialElement gf2nPolynomialElement = new GF2nPolynomialElement(this);
        gf2nPolynomialElement.polynomial.expandN((gf2nPolynomialElement.mDegree << 1) + 32);
        gf2nPolynomialElement.polynomial.reduceN();
        for (int i = 0; i < this.mDegree; ++i) {
            if ((1 << i & n) != 0x0) {
                one.multiplyThisBy(gf2nPolynomialElement);
            }
            gf2nPolynomialElement.square();
        }
        return one;
    }
    
    @Override
    public GF2nElement solveQuadraticEquation() throws RuntimeException {
        if (this.isZero()) {
            return ZERO((GF2nPolynomialField)this.mField);
        }
        if ((this.mDegree & 0x1) == 0x1) {
            return this.halfTrace();
        }
        GF2nPolynomialElement gf2nPolynomialElement;
        GF2nPolynomialElement zero;
        do {
            final GF2nPolynomialElement gf2nPolynomialElement2 = new GF2nPolynomialElement((GF2nPolynomialField)this.mField, new Random());
            zero = ZERO((GF2nPolynomialField)this.mField);
            gf2nPolynomialElement = (GF2nPolynomialElement)gf2nPolynomialElement2.clone();
            for (int i = 1; i < this.mDegree; ++i) {
                zero.squareThis();
                gf2nPolynomialElement.squareThis();
                zero.addToThis(gf2nPolynomialElement.multiply(this));
                gf2nPolynomialElement.addToThis(gf2nPolynomialElement2);
            }
        } while (gf2nPolynomialElement.isZero());
        if (this.equals(zero.square().add(zero))) {
            return zero;
        }
        throw new RuntimeException();
    }
    
    @Override
    public GF2nElement square() {
        return this.squarePreCalc();
    }
    
    public GF2nPolynomialElement squareBitwise() {
        final GF2nPolynomialElement gf2nPolynomialElement = new GF2nPolynomialElement(this);
        gf2nPolynomialElement.squareThisBitwise();
        gf2nPolynomialElement.reduceThis();
        return gf2nPolynomialElement;
    }
    
    public GF2nPolynomialElement squareMatrix() {
        final GF2nPolynomialElement gf2nPolynomialElement = new GF2nPolynomialElement(this);
        gf2nPolynomialElement.squareThisMatrix();
        gf2nPolynomialElement.reduceThis();
        return gf2nPolynomialElement;
    }
    
    public GF2nPolynomialElement squarePreCalc() {
        final GF2nPolynomialElement gf2nPolynomialElement = new GF2nPolynomialElement(this);
        gf2nPolynomialElement.squareThisPreCalc();
        gf2nPolynomialElement.reduceThis();
        return gf2nPolynomialElement;
    }
    
    @Override
    public GF2nElement squareRoot() {
        final GF2nPolynomialElement gf2nPolynomialElement = new GF2nPolynomialElement(this);
        gf2nPolynomialElement.squareRootThis();
        return gf2nPolynomialElement;
    }
    
    @Override
    public void squareRootThis() {
        this.polynomial.expandN((this.mDegree << 1) + 32);
        this.polynomial.reduceN();
        for (int i = 0; i < this.mField.getDegree() - 1; ++i) {
            this.squareThis();
        }
    }
    
    @Override
    public void squareThis() {
        this.squareThisPreCalc();
    }
    
    public void squareThisBitwise() {
        this.polynomial.squareThisBitwise();
        this.reduceThis();
    }
    
    public void squareThisMatrix() {
        final GF2Polynomial polynomial = new GF2Polynomial(this.mDegree);
        for (int i = 0; i < this.mDegree; ++i) {
            if (this.polynomial.vectorMult(((GF2nPolynomialField)this.mField).squaringMatrix[this.mDegree - i - 1])) {
                polynomial.setBit(i);
            }
        }
        this.polynomial = polynomial;
    }
    
    public void squareThisPreCalc() {
        this.polynomial.squareThisPreCalc();
        this.reduceThis();
    }
    
    @Override
    boolean testBit(final int n) {
        return this.polynomial.testBit(n);
    }
    
    @Override
    public boolean testRightmostBit() {
        return this.polynomial.testBit(0);
    }
    
    @Override
    public byte[] toByteArray() {
        return this.polynomial.toByteArray();
    }
    
    @Override
    public BigInteger toFlexiBigInt() {
        return this.polynomial.toFlexiBigInt();
    }
    
    @Override
    public String toString() {
        return this.polynomial.toString(16);
    }
    
    @Override
    public String toString(final int n) {
        return this.polynomial.toString(n);
    }
    
    @Override
    public int trace() {
        final GF2nPolynomialElement gf2nPolynomialElement = new GF2nPolynomialElement(this);
        for (int i = 1; i < this.mDegree; ++i) {
            gf2nPolynomialElement.squareThis();
            gf2nPolynomialElement.addToThis(this);
        }
        if (gf2nPolynomialElement.isOne()) {
            return 1;
        }
        return 0;
    }
}
