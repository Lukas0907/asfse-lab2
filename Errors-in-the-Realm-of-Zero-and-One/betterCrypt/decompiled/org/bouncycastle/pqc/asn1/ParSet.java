// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.asn1;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Sequence;
import java.math.BigInteger;
import org.bouncycastle.asn1.ASN1Object;

public class ParSet extends ASN1Object
{
    private static final BigInteger ZERO;
    private int[] h;
    private int[] k;
    private int t;
    private int[] w;
    
    static {
        ZERO = BigInteger.valueOf(0L);
    }
    
    public ParSet(final int t, final int[] h, final int[] w, final int[] k) {
        this.t = t;
        this.h = h;
        this.w = w;
        this.k = k;
    }
    
    private ParSet(ASN1Sequence asn1Sequence) {
        if (asn1Sequence.size() != 4) {
            final StringBuilder sb = new StringBuilder();
            sb.append("sie of seqOfParams = ");
            sb.append(asn1Sequence.size());
            throw new IllegalArgumentException(sb.toString());
        }
        int i = 0;
        this.t = checkBigIntegerInIntRangeAndPositive(asn1Sequence.getObjectAt(0));
        final ASN1Sequence asn1Sequence2 = (ASN1Sequence)asn1Sequence.getObjectAt(1);
        final ASN1Sequence asn1Sequence3 = (ASN1Sequence)asn1Sequence.getObjectAt(2);
        asn1Sequence = (ASN1Sequence)asn1Sequence.getObjectAt(3);
        if (asn1Sequence2.size() == this.t && asn1Sequence3.size() == this.t && asn1Sequence.size() == this.t) {
            this.h = new int[asn1Sequence2.size()];
            this.w = new int[asn1Sequence3.size()];
            this.k = new int[asn1Sequence.size()];
            while (i < this.t) {
                this.h[i] = checkBigIntegerInIntRangeAndPositive(asn1Sequence2.getObjectAt(i));
                this.w[i] = checkBigIntegerInIntRangeAndPositive(asn1Sequence3.getObjectAt(i));
                this.k[i] = checkBigIntegerInIntRangeAndPositive(asn1Sequence.getObjectAt(i));
                ++i;
            }
            return;
        }
        throw new IllegalArgumentException("invalid size of sequences");
    }
    
    private static int checkBigIntegerInIntRangeAndPositive(final ASN1Encodable asn1Encodable) {
        final int intValueExact = ((ASN1Integer)asn1Encodable).intValueExact();
        if (intValueExact > 0) {
            return intValueExact;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("BigInteger not in Range: ");
        sb.append(intValueExact);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static ParSet getInstance(final Object o) {
        if (o instanceof ParSet) {
            return (ParSet)o;
        }
        if (o != null) {
            return new ParSet(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public int[] getH() {
        return Arrays.clone(this.h);
    }
    
    public int[] getK() {
        return Arrays.clone(this.k);
    }
    
    public int getT() {
        return this.t;
    }
    
    public int[] getW() {
        return Arrays.clone(this.w);
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        final ASN1EncodableVector asn1EncodableVector2 = new ASN1EncodableVector();
        final ASN1EncodableVector asn1EncodableVector3 = new ASN1EncodableVector();
        int n = 0;
        while (true) {
            final int[] h = this.h;
            if (n >= h.length) {
                break;
            }
            asn1EncodableVector.add(new ASN1Integer(h[n]));
            asn1EncodableVector2.add(new ASN1Integer(this.w[n]));
            asn1EncodableVector3.add(new ASN1Integer(this.k[n]));
            ++n;
        }
        final ASN1EncodableVector asn1EncodableVector4 = new ASN1EncodableVector();
        asn1EncodableVector4.add(new ASN1Integer(this.t));
        asn1EncodableVector4.add(new DERSequence(asn1EncodableVector));
        asn1EncodableVector4.add(new DERSequence(asn1EncodableVector2));
        asn1EncodableVector4.add(new DERSequence(asn1EncodableVector3));
        return new DERSequence(asn1EncodableVector4);
    }
}
