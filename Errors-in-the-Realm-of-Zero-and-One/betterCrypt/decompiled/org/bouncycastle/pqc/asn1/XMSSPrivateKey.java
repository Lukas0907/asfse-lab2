// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.asn1;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.asn1.ASN1Object;

public class XMSSPrivateKey extends ASN1Object
{
    private final byte[] bdsState;
    private final int index;
    private final int maxIndex;
    private final byte[] publicSeed;
    private final byte[] root;
    private final byte[] secretKeyPRF;
    private final byte[] secretKeySeed;
    private final int version;
    
    public XMSSPrivateKey(final int index, final byte[] array, final byte[] array2, final byte[] array3, final byte[] array4, final byte[] array5) {
        this.version = 0;
        this.index = index;
        this.secretKeySeed = Arrays.clone(array);
        this.secretKeyPRF = Arrays.clone(array2);
        this.publicSeed = Arrays.clone(array3);
        this.root = Arrays.clone(array4);
        this.bdsState = Arrays.clone(array5);
        this.maxIndex = -1;
    }
    
    public XMSSPrivateKey(final int index, final byte[] array, final byte[] array2, final byte[] array3, final byte[] array4, final byte[] array5, final int maxIndex) {
        this.version = 1;
        this.index = index;
        this.secretKeySeed = Arrays.clone(array);
        this.secretKeyPRF = Arrays.clone(array2);
        this.publicSeed = Arrays.clone(array3);
        this.root = Arrays.clone(array4);
        this.bdsState = Arrays.clone(array5);
        this.maxIndex = maxIndex;
    }
    
    private XMSSPrivateKey(final ASN1Sequence asn1Sequence) {
        final ASN1Integer instance = ASN1Integer.getInstance(asn1Sequence.getObjectAt(0));
        if (!instance.hasValue(BigIntegers.ZERO) && !instance.hasValue(BigIntegers.ONE)) {
            throw new IllegalArgumentException("unknown version of sequence");
        }
        this.version = instance.intValueExact();
        if (asn1Sequence.size() != 2 && asn1Sequence.size() != 3) {
            throw new IllegalArgumentException("key sequence wrong size");
        }
        final ASN1Sequence instance2 = ASN1Sequence.getInstance(asn1Sequence.getObjectAt(1));
        this.index = ASN1Integer.getInstance(instance2.getObjectAt(0)).intValueExact();
        this.secretKeySeed = Arrays.clone(ASN1OctetString.getInstance(instance2.getObjectAt(1)).getOctets());
        this.secretKeyPRF = Arrays.clone(ASN1OctetString.getInstance(instance2.getObjectAt(2)).getOctets());
        this.publicSeed = Arrays.clone(ASN1OctetString.getInstance(instance2.getObjectAt(3)).getOctets());
        this.root = Arrays.clone(ASN1OctetString.getInstance(instance2.getObjectAt(4)).getOctets());
        int intValueExact;
        if (instance2.size() == 6) {
            final ASN1TaggedObject instance3 = ASN1TaggedObject.getInstance(instance2.getObjectAt(5));
            if (instance3.getTagNo() != 0) {
                throw new IllegalArgumentException("unknown tag in XMSSPrivateKey");
            }
            intValueExact = ASN1Integer.getInstance(instance3, false).intValueExact();
        }
        else {
            if (instance2.size() != 5) {
                throw new IllegalArgumentException("keySeq should be 5 or 6 in length");
            }
            intValueExact = -1;
        }
        this.maxIndex = intValueExact;
        if (asn1Sequence.size() == 3) {
            this.bdsState = Arrays.clone(ASN1OctetString.getInstance(ASN1TaggedObject.getInstance(asn1Sequence.getObjectAt(2)), true).getOctets());
            return;
        }
        this.bdsState = null;
    }
    
    public static XMSSPrivateKey getInstance(final Object o) {
        if (o instanceof XMSSPrivateKey) {
            return (XMSSPrivateKey)o;
        }
        if (o != null) {
            return new XMSSPrivateKey(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public byte[] getBdsState() {
        return Arrays.clone(this.bdsState);
    }
    
    public int getIndex() {
        return this.index;
    }
    
    public int getMaxIndex() {
        return this.maxIndex;
    }
    
    public byte[] getPublicSeed() {
        return Arrays.clone(this.publicSeed);
    }
    
    public byte[] getRoot() {
        return Arrays.clone(this.root);
    }
    
    public byte[] getSecretKeyPRF() {
        return Arrays.clone(this.secretKeyPRF);
    }
    
    public byte[] getSecretKeySeed() {
        return Arrays.clone(this.secretKeySeed);
    }
    
    public int getVersion() {
        return this.version;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        ASN1Integer asn1Integer;
        if (this.maxIndex >= 0) {
            asn1Integer = new ASN1Integer(1L);
        }
        else {
            asn1Integer = new ASN1Integer(0L);
        }
        asn1EncodableVector.add(asn1Integer);
        final ASN1EncodableVector asn1EncodableVector2 = new ASN1EncodableVector();
        asn1EncodableVector2.add(new ASN1Integer(this.index));
        asn1EncodableVector2.add(new DEROctetString(this.secretKeySeed));
        asn1EncodableVector2.add(new DEROctetString(this.secretKeyPRF));
        asn1EncodableVector2.add(new DEROctetString(this.publicSeed));
        asn1EncodableVector2.add(new DEROctetString(this.root));
        final int maxIndex = this.maxIndex;
        if (maxIndex >= 0) {
            asn1EncodableVector2.add(new DERTaggedObject(false, 0, new ASN1Integer(maxIndex)));
        }
        asn1EncodableVector.add(new DERSequence(asn1EncodableVector2));
        asn1EncodableVector.add(new DERTaggedObject(true, 0, new DEROctetString(this.bdsState)));
        return new DERSequence(asn1EncodableVector);
    }
}
