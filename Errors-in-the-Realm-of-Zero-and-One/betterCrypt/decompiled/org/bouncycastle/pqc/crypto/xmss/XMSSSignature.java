// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.xmss;

import org.bouncycastle.util.Pack;
import java.io.IOException;
import org.bouncycastle.util.Encodable;

public final class XMSSSignature extends XMSSReducedSignature implements XMSSStoreableObjectInterface, Encodable
{
    private final int index;
    private final byte[] random;
    
    private XMSSSignature(final Builder builder) {
        super((XMSSReducedSignature.Builder)builder);
        this.index = builder.index;
        final int treeDigestSize = this.getParams().getTreeDigestSize();
        final byte[] access$100 = builder.random;
        if (access$100 == null) {
            this.random = new byte[treeDigestSize];
            return;
        }
        if (access$100.length == treeDigestSize) {
            this.random = access$100;
            return;
        }
        throw new IllegalArgumentException("size of random needs to be equal to size of digest");
    }
    
    @Override
    public byte[] getEncoded() throws IOException {
        return this.toByteArray();
    }
    
    public int getIndex() {
        return this.index;
    }
    
    public byte[] getRandom() {
        return XMSSUtil.cloneArray(this.random);
    }
    
    @Override
    public byte[] toByteArray() {
        final int treeDigestSize = this.getParams().getTreeDigestSize();
        final byte[] array = new byte[treeDigestSize + 4 + this.getParams().getWOTSPlus().getParams().getLen() * treeDigestSize + this.getParams().getHeight() * treeDigestSize];
        final int index = this.index;
        final int n = 0;
        Pack.intToBigEndian(index, array, 0);
        XMSSUtil.copyBytesAtOffset(array, this.random, 4);
        final byte[][] byteArray = this.getWOTSPlusSignature().toByteArray();
        int n2 = 4 + treeDigestSize;
        int n3 = 0;
        int i;
        int n4;
        while (true) {
            i = n;
            n4 = n2;
            if (n3 >= byteArray.length) {
                break;
            }
            XMSSUtil.copyBytesAtOffset(array, byteArray[n3], n2);
            n2 += treeDigestSize;
            ++n3;
        }
        while (i < this.getAuthPath().size()) {
            XMSSUtil.copyBytesAtOffset(array, this.getAuthPath().get(i).getValue(), n4);
            n4 += treeDigestSize;
            ++i;
        }
        return array;
    }
    
    public static class Builder extends XMSSReducedSignature.Builder
    {
        private int index;
        private final XMSSParameters params;
        private byte[] random;
        
        public Builder(final XMSSParameters params) {
            super(params);
            this.index = 0;
            this.random = null;
            this.params = params;
        }
        
        public XMSSSignature build() {
            return new XMSSSignature(this, null);
        }
        
        public Builder withIndex(final int index) {
            this.index = index;
            return this;
        }
        
        public Builder withRandom(final byte[] array) {
            this.random = XMSSUtil.cloneArray(array);
            return this;
        }
        
        public Builder withSignature(final byte[] array) {
            if (array != null) {
                final int treeDigestSize = this.params.getTreeDigestSize();
                final int len = this.params.getWOTSPlus().getParams().getLen();
                final int height = this.params.getHeight();
                this.index = Pack.bigEndianToInt(array, 0);
                this.random = XMSSUtil.extractBytesAtOffset(array, 4, treeDigestSize);
                ((XMSSReducedSignature.Builder)this).withReducedSignature(XMSSUtil.extractBytesAtOffset(array, 4 + treeDigestSize, len * treeDigestSize + height * treeDigestSize));
                return this;
            }
            throw new NullPointerException("signature == null");
        }
    }
}
