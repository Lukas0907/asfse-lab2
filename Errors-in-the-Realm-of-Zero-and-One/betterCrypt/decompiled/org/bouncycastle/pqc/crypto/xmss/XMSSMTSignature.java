// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.xmss;

import org.bouncycastle.util.Arrays;
import java.util.Iterator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.bouncycastle.util.Encodable;

public final class XMSSMTSignature implements XMSSStoreableObjectInterface, Encodable
{
    private final long index;
    private final XMSSMTParameters params;
    private final byte[] random;
    private final List<XMSSReducedSignature> reducedSignatures;
    
    private XMSSMTSignature(final Builder builder) {
        this.params = builder.params;
        final XMSSMTParameters params = this.params;
        if (params != null) {
            final int treeDigestSize = params.getTreeDigestSize();
            final byte[] access$100 = builder.signature;
            if (access$100 != null) {
                final int len = this.params.getWOTSPlus().getParams().getLen();
                final int n = (int)Math.ceil(this.params.getHeight() / 8.0);
                final int n2 = (this.params.getHeight() / this.params.getLayers() + len) * treeDigestSize;
                if (access$100.length != n + treeDigestSize + this.params.getLayers() * n2) {
                    throw new IllegalArgumentException("signature has wrong size");
                }
                this.index = XMSSUtil.bytesToXBigEndian(access$100, 0, n);
                if (!XMSSUtil.isIndexValid(this.params.getHeight(), this.index)) {
                    throw new IllegalArgumentException("index out of bounds");
                }
                final int n3 = n + 0;
                this.random = XMSSUtil.extractBytesAtOffset(access$100, n3, treeDigestSize);
                int i = n3 + treeDigestSize;
                this.reducedSignatures = new ArrayList<XMSSReducedSignature>();
                while (i < access$100.length) {
                    this.reducedSignatures.add(new XMSSReducedSignature.Builder(this.params.getXMSSParameters()).withReducedSignature(XMSSUtil.extractBytesAtOffset(access$100, i, n2)).build());
                    i += n2;
                }
            }
            else {
                this.index = builder.index;
                final byte[] access$101 = builder.random;
                if (access$101 != null) {
                    if (access$101.length != treeDigestSize) {
                        throw new IllegalArgumentException("size of random needs to be equal to size of digest");
                    }
                    this.random = access$101;
                }
                else {
                    this.random = new byte[treeDigestSize];
                }
                List<XMSSReducedSignature> access$102 = builder.reducedSignatures;
                if (access$102 == null) {
                    access$102 = new ArrayList<XMSSReducedSignature>();
                }
                this.reducedSignatures = access$102;
            }
            return;
        }
        throw new NullPointerException("params == null");
    }
    
    @Override
    public byte[] getEncoded() throws IOException {
        return this.toByteArray();
    }
    
    public long getIndex() {
        return this.index;
    }
    
    public byte[] getRandom() {
        return XMSSUtil.cloneArray(this.random);
    }
    
    public List<XMSSReducedSignature> getReducedSignatures() {
        return this.reducedSignatures;
    }
    
    @Override
    public byte[] toByteArray() {
        final int treeDigestSize = this.params.getTreeDigestSize();
        final int len = this.params.getWOTSPlus().getParams().getLen();
        final int n = (int)Math.ceil(this.params.getHeight() / 8.0);
        final int n2 = (this.params.getHeight() / this.params.getLayers() + len) * treeDigestSize;
        final byte[] array = new byte[n + treeDigestSize + this.params.getLayers() * n2];
        XMSSUtil.copyBytesAtOffset(array, XMSSUtil.toBytesBigEndian(this.index, n), 0);
        final int n3 = n + 0;
        XMSSUtil.copyBytesAtOffset(array, this.random, n3);
        int n4 = n3 + treeDigestSize;
        final Iterator<XMSSReducedSignature> iterator = this.reducedSignatures.iterator();
        while (iterator.hasNext()) {
            XMSSUtil.copyBytesAtOffset(array, iterator.next().toByteArray(), n4);
            n4 += n2;
        }
        return array;
    }
    
    public static class Builder
    {
        private long index;
        private final XMSSMTParameters params;
        private byte[] random;
        private List<XMSSReducedSignature> reducedSignatures;
        private byte[] signature;
        
        public Builder(final XMSSMTParameters params) {
            this.index = 0L;
            this.random = null;
            this.reducedSignatures = null;
            this.signature = null;
            this.params = params;
        }
        
        public XMSSMTSignature build() {
            return new XMSSMTSignature(this, null);
        }
        
        public Builder withIndex(final long index) {
            this.index = index;
            return this;
        }
        
        public Builder withRandom(final byte[] array) {
            this.random = XMSSUtil.cloneArray(array);
            return this;
        }
        
        public Builder withReducedSignatures(final List<XMSSReducedSignature> reducedSignatures) {
            this.reducedSignatures = reducedSignatures;
            return this;
        }
        
        public Builder withSignature(final byte[] array) {
            this.signature = Arrays.clone(array);
            return this;
        }
    }
}
