// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.xmss;

import java.io.IOException;
import org.bouncycastle.util.Pack;
import org.bouncycastle.util.Encodable;

public final class XMSSMTPublicKeyParameters extends XMSSMTKeyParameters implements XMSSStoreableObjectInterface, Encodable
{
    private final int oid;
    private final XMSSMTParameters params;
    private final byte[] publicSeed;
    private final byte[] root;
    
    private XMSSMTPublicKeyParameters(final Builder builder) {
        final String treeDigest = builder.params.getTreeDigest();
        int oid = 0;
        super(false, treeDigest);
        this.params = builder.params;
        final XMSSMTParameters params = this.params;
        if (params == null) {
            throw new NullPointerException("params == null");
        }
        final int treeDigestSize = params.getTreeDigestSize();
        final byte[] access$100 = builder.publicKey;
        if (access$100 != null) {
            if (access$100.length == treeDigestSize + treeDigestSize) {
                this.oid = 0;
                this.root = XMSSUtil.extractBytesAtOffset(access$100, 0, treeDigestSize);
                this.publicSeed = XMSSUtil.extractBytesAtOffset(access$100, treeDigestSize + 0, treeDigestSize);
                return;
            }
            if (access$100.length == treeDigestSize + 4 + treeDigestSize) {
                this.oid = Pack.bigEndianToInt(access$100, 0);
                this.root = XMSSUtil.extractBytesAtOffset(access$100, 4, treeDigestSize);
                this.publicSeed = XMSSUtil.extractBytesAtOffset(access$100, 4 + treeDigestSize, treeDigestSize);
                return;
            }
            throw new IllegalArgumentException("public key has wrong size");
        }
        else {
            if (this.params.getOid() != null) {
                oid = this.params.getOid().getOid();
            }
            this.oid = oid;
            final byte[] access$101 = builder.root;
            if (access$101 != null) {
                if (access$101.length != treeDigestSize) {
                    throw new IllegalArgumentException("length of root must be equal to length of digest");
                }
                this.root = access$101;
            }
            else {
                this.root = new byte[treeDigestSize];
            }
            final byte[] access$102 = builder.publicSeed;
            if (access$102 == null) {
                this.publicSeed = new byte[treeDigestSize];
                return;
            }
            if (access$102.length == treeDigestSize) {
                this.publicSeed = access$102;
                return;
            }
            throw new IllegalArgumentException("length of publicSeed must be equal to length of digest");
        }
    }
    
    @Override
    public byte[] getEncoded() throws IOException {
        return this.toByteArray();
    }
    
    public XMSSMTParameters getParameters() {
        return this.params;
    }
    
    public byte[] getPublicSeed() {
        return XMSSUtil.cloneArray(this.publicSeed);
    }
    
    public byte[] getRoot() {
        return XMSSUtil.cloneArray(this.root);
    }
    
    @Override
    public byte[] toByteArray() {
        final int treeDigestSize = this.params.getTreeDigestSize();
        final int oid = this.oid;
        int n = 0;
        byte[] array;
        if (oid != 0) {
            array = new byte[treeDigestSize + 4 + treeDigestSize];
            Pack.intToBigEndian(oid, array, 0);
            n = 4;
        }
        else {
            array = new byte[treeDigestSize + treeDigestSize];
        }
        XMSSUtil.copyBytesAtOffset(array, this.root, n);
        XMSSUtil.copyBytesAtOffset(array, this.publicSeed, n + treeDigestSize);
        return array;
    }
    
    public static class Builder
    {
        private final XMSSMTParameters params;
        private byte[] publicKey;
        private byte[] publicSeed;
        private byte[] root;
        
        public Builder(final XMSSMTParameters params) {
            this.root = null;
            this.publicSeed = null;
            this.publicKey = null;
            this.params = params;
        }
        
        public XMSSMTPublicKeyParameters build() {
            return new XMSSMTPublicKeyParameters(this, null);
        }
        
        public Builder withPublicKey(final byte[] array) {
            this.publicKey = XMSSUtil.cloneArray(array);
            return this;
        }
        
        public Builder withPublicSeed(final byte[] array) {
            this.publicSeed = XMSSUtil.cloneArray(array);
            return this;
        }
        
        public Builder withRoot(final byte[] array) {
            this.root = XMSSUtil.cloneArray(array);
            return this;
        }
    }
}
