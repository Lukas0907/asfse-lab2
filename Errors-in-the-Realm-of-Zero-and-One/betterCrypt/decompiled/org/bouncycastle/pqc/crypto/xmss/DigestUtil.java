// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.xmss;

import org.bouncycastle.crypto.Xof;
import org.bouncycastle.crypto.digests.SHAKEDigest;
import org.bouncycastle.crypto.digests.SHA512Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import java.util.HashMap;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.util.Map;

class DigestUtil
{
    private static Map<String, ASN1ObjectIdentifier> nameToOid;
    
    static {
        (DigestUtil.nameToOid = new HashMap<String, ASN1ObjectIdentifier>()).put("SHA-256", NISTObjectIdentifiers.id_sha256);
        DigestUtil.nameToOid.put("SHA-512", NISTObjectIdentifiers.id_sha512);
        DigestUtil.nameToOid.put("SHAKE128", NISTObjectIdentifiers.id_shake128);
        DigestUtil.nameToOid.put("SHAKE256", NISTObjectIdentifiers.id_shake256);
    }
    
    static Digest getDigest(final ASN1ObjectIdentifier obj) {
        if (obj.equals(NISTObjectIdentifiers.id_sha256)) {
            return new SHA256Digest();
        }
        if (obj.equals(NISTObjectIdentifiers.id_sha512)) {
            return new SHA512Digest();
        }
        if (obj.equals(NISTObjectIdentifiers.id_shake128)) {
            return new SHAKEDigest(128);
        }
        if (obj.equals(NISTObjectIdentifiers.id_shake256)) {
            return new SHAKEDigest(256);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unrecognized digest OID: ");
        sb.append(obj);
        throw new IllegalArgumentException(sb.toString());
    }
    
    static ASN1ObjectIdentifier getDigestOID(final String str) {
        final ASN1ObjectIdentifier asn1ObjectIdentifier = DigestUtil.nameToOid.get(str);
        if (asn1ObjectIdentifier != null) {
            return asn1ObjectIdentifier;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unrecognized digest name: ");
        sb.append(str);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static int getDigestSize(final Digest digest) {
        final boolean b = digest instanceof Xof;
        int digestSize = digest.getDigestSize();
        if (b) {
            digestSize *= 2;
        }
        return digestSize;
    }
}
