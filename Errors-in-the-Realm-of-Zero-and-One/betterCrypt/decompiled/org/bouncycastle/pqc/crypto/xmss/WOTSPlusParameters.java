// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.xmss;

import org.bouncycastle.crypto.Digest;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;

final class WOTSPlusParameters
{
    private final int digestSize;
    private final int len;
    private final int len1;
    private final int len2;
    private final XMSSOid oid;
    private final ASN1ObjectIdentifier treeDigest;
    private final int winternitzParameter;
    
    protected WOTSPlusParameters(final ASN1ObjectIdentifier treeDigest) {
        if (treeDigest == null) {
            throw new NullPointerException("treeDigest == null");
        }
        this.treeDigest = treeDigest;
        final Digest digest = DigestUtil.getDigest(treeDigest);
        this.digestSize = XMSSUtil.getDigestSize(digest);
        this.winternitzParameter = 16;
        this.len1 = (int)Math.ceil(this.digestSize * 8 / (double)XMSSUtil.log2(this.winternitzParameter));
        this.len2 = (int)Math.floor(XMSSUtil.log2(this.len1 * (this.winternitzParameter - 1)) / XMSSUtil.log2(this.winternitzParameter)) + 1;
        this.len = this.len1 + this.len2;
        this.oid = WOTSPlusOid.lookup(digest.getAlgorithmName(), this.digestSize, this.winternitzParameter, this.len);
        if (this.oid != null) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("cannot find OID for digest algorithm: ");
        sb.append(digest.getAlgorithmName());
        throw new IllegalArgumentException(sb.toString());
    }
    
    protected int getLen() {
        return this.len;
    }
    
    protected int getLen1() {
        return this.len1;
    }
    
    protected int getLen2() {
        return this.len2;
    }
    
    protected XMSSOid getOid() {
        return this.oid;
    }
    
    public ASN1ObjectIdentifier getTreeDigest() {
        return this.treeDigest;
    }
    
    protected int getTreeDigestSize() {
        return this.digestSize;
    }
    
    protected int getWinternitzParameter() {
        return this.winternitzParameter;
    }
}
