// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.xmss;

import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import java.security.SecureRandom;

public final class XMSSKeyPairGenerator
{
    private XMSSParameters params;
    private SecureRandom prng;
    
    private XMSSPrivateKeyParameters generatePrivateKey(final XMSSParameters xmssParameters, final SecureRandom secureRandom) {
        final int treeDigestSize = xmssParameters.getTreeDigestSize();
        final byte[] bytes = new byte[treeDigestSize];
        secureRandom.nextBytes(bytes);
        final byte[] bytes2 = new byte[treeDigestSize];
        secureRandom.nextBytes(bytes2);
        final byte[] bytes3 = new byte[treeDigestSize];
        secureRandom.nextBytes(bytes3);
        return new XMSSPrivateKeyParameters.Builder(xmssParameters).withSecretKeySeed(bytes).withSecretKeyPRF(bytes2).withPublicSeed(bytes3).withBDSState(new BDS(xmssParameters, bytes3, bytes, (OTSHashAddress)new OTSHashAddress.Builder().build())).build();
    }
    
    public AsymmetricCipherKeyPair generateKeyPair() {
        final XMSSPrivateKeyParameters generatePrivateKey = this.generatePrivateKey(this.params, this.prng);
        final XMSSNode root = generatePrivateKey.getBDSState().getRoot();
        final XMSSPrivateKeyParameters build = new XMSSPrivateKeyParameters.Builder(this.params).withSecretKeySeed(generatePrivateKey.getSecretKeySeed()).withSecretKeyPRF(generatePrivateKey.getSecretKeyPRF()).withPublicSeed(generatePrivateKey.getPublicSeed()).withRoot(root.getValue()).withBDSState(generatePrivateKey.getBDSState()).build();
        return new AsymmetricCipherKeyPair(new XMSSPublicKeyParameters.Builder(this.params).withRoot(root.getValue()).withPublicSeed(build.getPublicSeed()).build(), build);
    }
    
    public void init(final KeyGenerationParameters keyGenerationParameters) {
        final XMSSKeyGenerationParameters xmssKeyGenerationParameters = (XMSSKeyGenerationParameters)keyGenerationParameters;
        this.prng = xmssKeyGenerationParameters.getRandom();
        this.params = xmssKeyGenerationParameters.getParameters();
    }
}
