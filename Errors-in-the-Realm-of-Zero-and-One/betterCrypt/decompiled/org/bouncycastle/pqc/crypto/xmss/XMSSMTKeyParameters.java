// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.xmss;

import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public class XMSSMTKeyParameters extends AsymmetricKeyParameter
{
    private final String treeDigest;
    
    public XMSSMTKeyParameters(final boolean b, final String treeDigest) {
        super(b);
        this.treeDigest = treeDigest;
    }
    
    public String getTreeDigest() {
        return this.treeDigest;
    }
}
