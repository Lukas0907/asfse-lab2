// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.xmss;

public interface XMSSStoreableObjectInterface
{
    byte[] toByteArray();
}
