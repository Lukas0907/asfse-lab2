// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.xmss;

import org.bouncycastle.util.Arrays;
import java.io.IOException;
import org.bouncycastle.util.Encodable;

public final class XMSSMTPrivateKeyParameters extends XMSSMTKeyParameters implements XMSSStoreableObjectInterface, Encodable
{
    private volatile BDSStateMap bdsState;
    private volatile long index;
    private final XMSSMTParameters params;
    private final byte[] publicSeed;
    private final byte[] root;
    private final byte[] secretKeyPRF;
    private final byte[] secretKeySeed;
    private volatile boolean used;
    
    private XMSSMTPrivateKeyParameters(final Builder builder) {
        super(true, builder.params.getTreeDigest());
        this.params = builder.params;
        final XMSSMTParameters params = this.params;
        if (params == null) {
            throw new NullPointerException("params == null");
        }
        final int treeDigestSize = params.getTreeDigestSize();
        final byte[] access$100 = builder.privateKey;
        if (access$100 != null) {
            if (builder.xmss != null) {
                final int height = this.params.getHeight();
                final int n = (height + 7) / 8;
                this.index = XMSSUtil.bytesToXBigEndian(access$100, 0, n);
                if (XMSSUtil.isIndexValid(height, this.index)) {
                    final int n2 = n + 0;
                    this.secretKeySeed = XMSSUtil.extractBytesAtOffset(access$100, n2, treeDigestSize);
                    final int n3 = n2 + treeDigestSize;
                    this.secretKeyPRF = XMSSUtil.extractBytesAtOffset(access$100, n3, treeDigestSize);
                    final int n4 = n3 + treeDigestSize;
                    this.publicSeed = XMSSUtil.extractBytesAtOffset(access$100, n4, treeDigestSize);
                    final int n5 = n4 + treeDigestSize;
                    this.root = XMSSUtil.extractBytesAtOffset(access$100, n5, treeDigestSize);
                    final int n6 = n5 + treeDigestSize;
                    final byte[] bytesAtOffset = XMSSUtil.extractBytesAtOffset(access$100, n6, access$100.length - n6);
                    try {
                        this.bdsState = ((BDSStateMap)XMSSUtil.deserialize(bytesAtOffset, BDSStateMap.class)).withWOTSDigest(builder.xmss.getTreeDigestOID());
                        return;
                    }
                    catch (ClassNotFoundException cause) {
                        throw new IllegalArgumentException(cause.getMessage(), cause);
                    }
                    catch (IOException cause2) {
                        throw new IllegalArgumentException(cause2.getMessage(), cause2);
                    }
                }
                throw new IllegalArgumentException("index out of bounds");
            }
            throw new NullPointerException("xmss == null");
        }
        else {
            this.index = builder.index;
            final byte[] access$101 = builder.secretKeySeed;
            if (access$101 != null) {
                if (access$101.length != treeDigestSize) {
                    throw new IllegalArgumentException("size of secretKeySeed needs to be equal size of digest");
                }
                this.secretKeySeed = access$101;
            }
            else {
                this.secretKeySeed = new byte[treeDigestSize];
            }
            final byte[] access$102 = builder.secretKeyPRF;
            if (access$102 != null) {
                if (access$102.length != treeDigestSize) {
                    throw new IllegalArgumentException("size of secretKeyPRF needs to be equal size of digest");
                }
                this.secretKeyPRF = access$102;
            }
            else {
                this.secretKeyPRF = new byte[treeDigestSize];
            }
            final byte[] access$103 = builder.publicSeed;
            if (access$103 != null) {
                if (access$103.length != treeDigestSize) {
                    throw new IllegalArgumentException("size of publicSeed needs to be equal size of digest");
                }
                this.publicSeed = access$103;
            }
            else {
                this.publicSeed = new byte[treeDigestSize];
            }
            final byte[] access$104 = builder.root;
            if (access$104 != null) {
                if (access$104.length != treeDigestSize) {
                    throw new IllegalArgumentException("size of root needs to be equal size of digest");
                }
                this.root = access$104;
            }
            else {
                this.root = new byte[treeDigestSize];
            }
            BDSStateMap access$105 = builder.bdsState;
            if (access$105 == null) {
                if (XMSSUtil.isIndexValid(this.params.getHeight(), builder.index) && access$103 != null && access$101 != null) {
                    access$105 = new BDSStateMap(this.params, builder.index, access$103, access$101);
                }
                else {
                    access$105 = new BDSStateMap(builder.maxIndex + 1L);
                }
            }
            this.bdsState = access$105;
            if (builder.maxIndex < 0L) {
                return;
            }
            if (builder.maxIndex == this.bdsState.getMaxIndex()) {
                return;
            }
            throw new IllegalArgumentException("maxIndex set but not reflected in state");
        }
    }
    
    public XMSSMTPrivateKeyParameters extractKeyShard(final int n) {
        if (n >= 1) {
            // monitorenter(this)
            final long n2 = n;
            try {
                if (n2 <= this.getUsagesRemaining()) {
                    final XMSSMTPrivateKeyParameters build = new Builder(this.params).withSecretKeySeed(this.secretKeySeed).withSecretKeyPRF(this.secretKeyPRF).withPublicSeed(this.publicSeed).withRoot(this.root).withIndex(this.getIndex()).withBDSState(new BDSStateMap(this.bdsState, this.getIndex() + n2 - 1L)).build();
                    for (int i = 0; i != n; ++i) {
                        this.rollKey();
                    }
                    return build;
                }
                throw new IllegalArgumentException("usageCount exceeds usages remaining");
            }
            finally {
            }
            // monitorexit(this)
        }
        throw new IllegalArgumentException("cannot ask for a shard with 0 keys");
    }
    
    BDSStateMap getBDSState() {
        return this.bdsState;
    }
    
    @Override
    public byte[] getEncoded() throws IOException {
        synchronized (this) {
            return this.toByteArray();
        }
    }
    
    public long getIndex() {
        return this.index;
    }
    
    public XMSSMTPrivateKeyParameters getNextKey() {
        synchronized (this) {
            return this.extractKeyShard(1);
        }
    }
    
    public XMSSMTParameters getParameters() {
        return this.params;
    }
    
    public byte[] getPublicSeed() {
        return XMSSUtil.cloneArray(this.publicSeed);
    }
    
    public byte[] getRoot() {
        return XMSSUtil.cloneArray(this.root);
    }
    
    public byte[] getSecretKeyPRF() {
        return XMSSUtil.cloneArray(this.secretKeyPRF);
    }
    
    public byte[] getSecretKeySeed() {
        return XMSSUtil.cloneArray(this.secretKeySeed);
    }
    
    public long getUsagesRemaining() {
        synchronized (this) {
            return this.bdsState.getMaxIndex() - this.getIndex() + 1L;
        }
    }
    
    XMSSMTPrivateKeyParameters rollKey() {
        synchronized (this) {
            if (this.getIndex() < this.bdsState.getMaxIndex()) {
                this.bdsState.updateState(this.params, this.index, this.publicSeed, this.secretKeySeed);
                ++this.index;
            }
            else {
                this.index = this.bdsState.getMaxIndex() + 1L;
                this.bdsState = new BDSStateMap(this.bdsState.getMaxIndex());
            }
            this.used = false;
            return this;
        }
    }
    
    @Override
    public byte[] toByteArray() {
        synchronized (this) {
            final int treeDigestSize = this.params.getTreeDigestSize();
            final int n = (this.params.getHeight() + 7) / 8;
            final byte[] array = new byte[n + treeDigestSize + treeDigestSize + treeDigestSize + treeDigestSize];
            XMSSUtil.copyBytesAtOffset(array, XMSSUtil.toBytesBigEndian(this.index, n), 0);
            final int n2 = n + 0;
            XMSSUtil.copyBytesAtOffset(array, this.secretKeySeed, n2);
            final int n3 = n2 + treeDigestSize;
            XMSSUtil.copyBytesAtOffset(array, this.secretKeyPRF, n3);
            final int n4 = n3 + treeDigestSize;
            XMSSUtil.copyBytesAtOffset(array, this.publicSeed, n4);
            XMSSUtil.copyBytesAtOffset(array, this.root, n4 + treeDigestSize);
            try {
                return Arrays.concatenate(array, XMSSUtil.serialize(this.bdsState));
            }
            catch (IOException cause) {
                final StringBuilder sb = new StringBuilder();
                sb.append("error serializing bds state: ");
                sb.append(cause.getMessage());
                throw new IllegalStateException(sb.toString(), cause);
            }
        }
    }
    
    public static class Builder
    {
        private BDSStateMap bdsState;
        private long index;
        private long maxIndex;
        private final XMSSMTParameters params;
        private byte[] privateKey;
        private byte[] publicSeed;
        private byte[] root;
        private byte[] secretKeyPRF;
        private byte[] secretKeySeed;
        private XMSSParameters xmss;
        
        public Builder(final XMSSMTParameters params) {
            this.index = 0L;
            this.maxIndex = -1L;
            this.secretKeySeed = null;
            this.secretKeyPRF = null;
            this.publicSeed = null;
            this.root = null;
            this.bdsState = null;
            this.privateKey = null;
            this.xmss = null;
            this.params = params;
        }
        
        public XMSSMTPrivateKeyParameters build() {
            return new XMSSMTPrivateKeyParameters(this, null);
        }
        
        public Builder withBDSState(final BDSStateMap bdsState) {
            if (bdsState.getMaxIndex() == 0L) {
                this.bdsState = new BDSStateMap(bdsState, (1L << this.params.getHeight()) - 1L);
                return this;
            }
            this.bdsState = bdsState;
            return this;
        }
        
        public Builder withIndex(final long index) {
            this.index = index;
            return this;
        }
        
        public Builder withMaxIndex(final long maxIndex) {
            this.maxIndex = maxIndex;
            return this;
        }
        
        public Builder withPrivateKey(final byte[] array) {
            this.privateKey = XMSSUtil.cloneArray(array);
            this.xmss = this.params.getXMSSParameters();
            return this;
        }
        
        public Builder withPublicSeed(final byte[] array) {
            this.publicSeed = XMSSUtil.cloneArray(array);
            return this;
        }
        
        public Builder withRoot(final byte[] array) {
            this.root = XMSSUtil.cloneArray(array);
            return this;
        }
        
        public Builder withSecretKeyPRF(final byte[] array) {
            this.secretKeyPRF = XMSSUtil.cloneArray(array);
            return this;
        }
        
        public Builder withSecretKeySeed(final byte[] array) {
            this.secretKeySeed = XMSSUtil.cloneArray(array);
            return this;
        }
    }
}
