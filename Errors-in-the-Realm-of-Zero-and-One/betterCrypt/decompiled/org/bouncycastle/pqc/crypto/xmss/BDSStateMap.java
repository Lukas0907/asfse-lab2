// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.xmss;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.util.Integers;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.Map;
import java.io.Serializable;

public class BDSStateMap implements Serializable
{
    private static final long serialVersionUID = -3464451825208522308L;
    private final Map<Integer, BDS> bdsState;
    private transient long maxIndex;
    
    BDSStateMap(final long maxIndex) {
        this.bdsState = new TreeMap<Integer, BDS>();
        this.maxIndex = maxIndex;
    }
    
    BDSStateMap(final BDSStateMap bdsStateMap, final long maxIndex) {
        this.bdsState = new TreeMap<Integer, BDS>();
        for (final Integer n : bdsStateMap.bdsState.keySet()) {
            this.bdsState.put(n, new BDS(bdsStateMap.bdsState.get(n)));
        }
        this.maxIndex = maxIndex;
    }
    
    BDSStateMap(final XMSSMTParameters xmssmtParameters, final long n, final byte[] array, final byte[] array2) {
        this.bdsState = new TreeMap<Integer, BDS>();
        this.maxIndex = (1L << xmssmtParameters.getHeight()) - 1L;
        for (long n2 = 0L; n2 < n; ++n2) {
            this.updateState(xmssmtParameters, n2, array, array2);
        }
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        long long1;
        if (objectInputStream.available() != 0) {
            long1 = objectInputStream.readLong();
        }
        else {
            long1 = 0L;
        }
        this.maxIndex = long1;
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeLong(this.maxIndex);
    }
    
    BDS get(final int n) {
        return this.bdsState.get(Integers.valueOf(n));
    }
    
    public long getMaxIndex() {
        return this.maxIndex;
    }
    
    public boolean isEmpty() {
        return this.bdsState.isEmpty();
    }
    
    void put(final int n, final BDS bds) {
        this.bdsState.put(Integers.valueOf(n), bds);
    }
    
    BDS update(final int n, final byte[] array, final byte[] array2, final OTSHashAddress otsHashAddress) {
        return this.bdsState.put(Integers.valueOf(n), this.bdsState.get(Integers.valueOf(n)).getNextState(array, array2, otsHashAddress));
    }
    
    void updateState(final XMSSMTParameters xmssmtParameters, final long n, final byte[] array, final byte[] array2) {
        final XMSSParameters xmssParameters = xmssmtParameters.getXMSSParameters();
        final int height = xmssParameters.getHeight();
        final long treeIndex = XMSSUtil.getTreeIndex(n, height);
        final int leafIndex = XMSSUtil.getLeafIndex(n, height);
        final OTSHashAddress otsHashAddress = (OTSHashAddress)((OTSHashAddress.Builder)((XMSSAddress.Builder<OTSHashAddress.Builder>)new OTSHashAddress.Builder()).withTreeAddress(treeIndex)).withOTSAddress(leafIndex).build();
        final int n2 = 1;
        final int n3 = (1 << height) - 1;
        long treeIndex2 = treeIndex;
        int i = n2;
        if (leafIndex < n3) {
            if (this.get(0) == null || leafIndex == 0) {
                this.put(0, new BDS(xmssParameters, array, array2, otsHashAddress));
            }
            this.update(0, array, array2, otsHashAddress);
            i = n2;
            treeIndex2 = treeIndex;
        }
        while (i < xmssmtParameters.getLayers()) {
            final int leafIndex2 = XMSSUtil.getLeafIndex(treeIndex2, height);
            treeIndex2 = XMSSUtil.getTreeIndex(treeIndex2, height);
            final OTSHashAddress otsHashAddress2 = (OTSHashAddress)((OTSHashAddress.Builder)((XMSSAddress.Builder<OTSHashAddress.Builder>)((XMSSAddress.Builder<OTSHashAddress.Builder>)new OTSHashAddress.Builder()).withLayerAddress(i)).withTreeAddress(treeIndex2)).withOTSAddress(leafIndex2).build();
            if (this.bdsState.get(i) == null || XMSSUtil.isNewBDSInitNeeded(n, height, i)) {
                this.bdsState.put(i, new BDS(xmssParameters, array, array2, otsHashAddress2));
            }
            if (leafIndex2 < n3 && XMSSUtil.isNewAuthenticationPathNeeded(n, height, i)) {
                this.update(i, array, array2, otsHashAddress2);
            }
            ++i;
        }
    }
    
    public BDSStateMap withWOTSDigest(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        final BDSStateMap bdsStateMap = new BDSStateMap(this.maxIndex);
        for (final Integer n : this.bdsState.keySet()) {
            bdsStateMap.bdsState.put(n, this.bdsState.get(n).withWOTSDigest(asn1ObjectIdentifier));
        }
        return bdsStateMap;
    }
}
