// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.xmss;

import org.bouncycastle.crypto.Digest;

public final class XMSSMTParameters
{
    private final int height;
    private final int layers;
    private final XMSSOid oid;
    private final XMSSParameters xmssParams;
    
    public XMSSMTParameters(final int height, final int layers, final Digest digest) {
        this.height = height;
        this.layers = layers;
        this.xmssParams = new XMSSParameters(xmssTreeHeight(height, layers), digest);
        this.oid = DefaultXMSSMTOid.lookup(this.getTreeDigest(), this.getTreeDigestSize(), this.getWinternitzParameter(), this.getLen(), this.getHeight(), layers);
    }
    
    private static int xmssTreeHeight(int n, final int n2) throws IllegalArgumentException {
        if (n < 2) {
            throw new IllegalArgumentException("totalHeight must be > 1");
        }
        if (n % n2 != 0) {
            throw new IllegalArgumentException("layers must divide totalHeight without remainder");
        }
        n /= n2;
        if (n != 1) {
            return n;
        }
        throw new IllegalArgumentException("height / layers must be greater than 1");
    }
    
    public int getHeight() {
        return this.height;
    }
    
    public int getLayers() {
        return this.layers;
    }
    
    protected int getLen() {
        return this.xmssParams.getLen();
    }
    
    protected XMSSOid getOid() {
        return this.oid;
    }
    
    protected String getTreeDigest() {
        return this.xmssParams.getTreeDigest();
    }
    
    public int getTreeDigestSize() {
        return this.xmssParams.getTreeDigestSize();
    }
    
    protected WOTSPlus getWOTSPlus() {
        return this.xmssParams.getWOTSPlus();
    }
    
    int getWinternitzParameter() {
        return this.xmssParams.getWinternitzParameter();
    }
    
    protected XMSSParameters getXMSSParameters() {
        return this.xmssParams;
    }
}
