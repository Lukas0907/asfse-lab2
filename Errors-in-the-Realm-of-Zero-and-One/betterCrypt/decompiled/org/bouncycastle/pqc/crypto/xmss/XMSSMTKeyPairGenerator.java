// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.xmss;

import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import java.security.SecureRandom;

public final class XMSSMTKeyPairGenerator
{
    private XMSSMTParameters params;
    private SecureRandom prng;
    private XMSSParameters xmssParams;
    
    private XMSSMTPrivateKeyParameters generatePrivateKey(final BDSStateMap bdsStateMap) {
        final int treeDigestSize = this.params.getTreeDigestSize();
        final byte[] bytes = new byte[treeDigestSize];
        this.prng.nextBytes(bytes);
        final byte[] bytes2 = new byte[treeDigestSize];
        this.prng.nextBytes(bytes2);
        final byte[] bytes3 = new byte[treeDigestSize];
        this.prng.nextBytes(bytes3);
        return new XMSSMTPrivateKeyParameters.Builder(this.params).withSecretKeySeed(bytes).withSecretKeyPRF(bytes2).withPublicSeed(bytes3).withBDSState(bdsStateMap).build();
    }
    
    public AsymmetricCipherKeyPair generateKeyPair() {
        final XMSSMTPrivateKeyParameters generatePrivateKey = this.generatePrivateKey(new XMSSMTPrivateKeyParameters.Builder(this.params).build().getBDSState());
        this.xmssParams.getWOTSPlus().importKeys(new byte[this.params.getTreeDigestSize()], generatePrivateKey.getPublicSeed());
        final int n = this.params.getLayers() - 1;
        final BDS bds = new BDS(this.xmssParams, generatePrivateKey.getPublicSeed(), generatePrivateKey.getSecretKeySeed(), (OTSHashAddress)((OTSHashAddress.Builder)((XMSSAddress.Builder<OTSHashAddress.Builder>)new OTSHashAddress.Builder()).withLayerAddress(n)).build());
        final XMSSNode root = bds.getRoot();
        generatePrivateKey.getBDSState().put(n, bds);
        final XMSSMTPrivateKeyParameters build = new XMSSMTPrivateKeyParameters.Builder(this.params).withSecretKeySeed(generatePrivateKey.getSecretKeySeed()).withSecretKeyPRF(generatePrivateKey.getSecretKeyPRF()).withPublicSeed(generatePrivateKey.getPublicSeed()).withRoot(root.getValue()).withBDSState(generatePrivateKey.getBDSState()).build();
        return new AsymmetricCipherKeyPair(new XMSSMTPublicKeyParameters.Builder(this.params).withRoot(root.getValue()).withPublicSeed(build.getPublicSeed()).build(), build);
    }
    
    public void init(final KeyGenerationParameters keyGenerationParameters) {
        final XMSSMTKeyGenerationParameters xmssmtKeyGenerationParameters = (XMSSMTKeyGenerationParameters)keyGenerationParameters;
        this.prng = xmssmtKeyGenerationParameters.getRandom();
        this.params = xmssmtKeyGenerationParameters.getParameters();
        this.xmssParams = this.params.getXMSSParameters();
    }
}
