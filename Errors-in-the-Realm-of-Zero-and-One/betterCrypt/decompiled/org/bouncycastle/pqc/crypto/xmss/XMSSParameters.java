// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.xmss;

import org.bouncycastle.crypto.Digest;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;

public final class XMSSParameters
{
    private final int height;
    private final int k;
    private final XMSSOid oid;
    private final String treeDigest;
    private final ASN1ObjectIdentifier treeDigestOID;
    private final int treeDigestSize;
    private final int winternitzParameter;
    private final WOTSPlusParameters wotsPlusParams;
    
    public XMSSParameters(final int height, final Digest digest) {
        if (height < 2) {
            throw new IllegalArgumentException("height must be >= 2");
        }
        if (digest != null) {
            this.height = height;
            this.k = this.determineMinK();
            this.treeDigest = digest.getAlgorithmName();
            this.treeDigestOID = DigestUtil.getDigestOID(digest.getAlgorithmName());
            this.wotsPlusParams = new WOTSPlusParameters(this.treeDigestOID);
            this.treeDigestSize = this.wotsPlusParams.getTreeDigestSize();
            this.winternitzParameter = this.wotsPlusParams.getWinternitzParameter();
            this.oid = DefaultXMSSOid.lookup(this.treeDigest, this.treeDigestSize, this.winternitzParameter, this.wotsPlusParams.getLen(), height);
            return;
        }
        throw new NullPointerException("digest == null");
    }
    
    private int determineMinK() {
        int n = 2;
        while (true) {
            final int height = this.height;
            if (n > height) {
                throw new IllegalStateException("should never happen...");
            }
            if ((height - n) % 2 == 0) {
                return n;
            }
            ++n;
        }
    }
    
    public int getHeight() {
        return this.height;
    }
    
    int getK() {
        return this.k;
    }
    
    int getLen() {
        return this.wotsPlusParams.getLen();
    }
    
    XMSSOid getOid() {
        return this.oid;
    }
    
    String getTreeDigest() {
        return this.treeDigest;
    }
    
    ASN1ObjectIdentifier getTreeDigestOID() {
        return this.treeDigestOID;
    }
    
    public int getTreeDigestSize() {
        return this.treeDigestSize;
    }
    
    WOTSPlus getWOTSPlus() {
        return new WOTSPlus(this.wotsPlusParams);
    }
    
    int getWinternitzParameter() {
        return this.winternitzParameter;
    }
}
