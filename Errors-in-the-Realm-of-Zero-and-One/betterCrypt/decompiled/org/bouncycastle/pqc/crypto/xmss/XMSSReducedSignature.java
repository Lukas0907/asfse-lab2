// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.xmss;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class XMSSReducedSignature implements XMSSStoreableObjectInterface
{
    private final List<XMSSNode> authPath;
    private final XMSSParameters params;
    private final WOTSPlusSignature wotsPlusSignature;
    
    protected XMSSReducedSignature(final Builder builder) {
        this.params = builder.params;
        final XMSSParameters params = this.params;
        if (params != null) {
            final int treeDigestSize = params.getTreeDigestSize();
            final int len = this.params.getWOTSPlus().getParams().getLen();
            final int height = this.params.getHeight();
            final byte[] access$100 = builder.reducedSignature;
            List<XMSSNode> access$101;
            if (access$100 != null) {
                if (access$100.length != len * treeDigestSize + height * treeDigestSize) {
                    throw new IllegalArgumentException("signature has wrong size");
                }
                final byte[][] array = new byte[len][];
                final int n = 0;
                int n2;
                for (int i = n2 = 0; i < array.length; ++i) {
                    array[i] = XMSSUtil.extractBytesAtOffset(access$100, n2, treeDigestSize);
                    n2 += treeDigestSize;
                }
                this.wotsPlusSignature = new WOTSPlusSignature(this.params.getWOTSPlus().getParams(), array);
                final ArrayList<XMSSNode> list = new ArrayList<XMSSNode>();
                int n3 = n2;
                int n4 = n;
                while (true) {
                    access$101 = list;
                    if (n4 >= height) {
                        break;
                    }
                    list.add(new XMSSNode(n4, XMSSUtil.extractBytesAtOffset(access$100, n3, treeDigestSize)));
                    n3 += treeDigestSize;
                    ++n4;
                }
            }
            else {
                WOTSPlusSignature access$102 = builder.wotsPlusSignature;
                if (access$102 == null) {
                    access$102 = new WOTSPlusSignature(this.params.getWOTSPlus().getParams(), (byte[][])Array.newInstance(Byte.TYPE, len, treeDigestSize));
                }
                this.wotsPlusSignature = access$102;
                access$101 = builder.authPath;
                if (access$101 != null) {
                    if (access$101.size() != height) {
                        throw new IllegalArgumentException("size of authPath needs to be equal to height of tree");
                    }
                }
                else {
                    access$101 = new ArrayList<XMSSNode>();
                }
            }
            this.authPath = access$101;
            return;
        }
        throw new NullPointerException("params == null");
    }
    
    public List<XMSSNode> getAuthPath() {
        return this.authPath;
    }
    
    public XMSSParameters getParams() {
        return this.params;
    }
    
    public WOTSPlusSignature getWOTSPlusSignature() {
        return this.wotsPlusSignature;
    }
    
    @Override
    public byte[] toByteArray() {
        final int treeDigestSize = this.params.getTreeDigestSize();
        final byte[] array = new byte[this.params.getWOTSPlus().getParams().getLen() * treeDigestSize + this.params.getHeight() * treeDigestSize];
        final byte[][] byteArray = this.wotsPlusSignature.toByteArray();
        final int n = 0;
        int n3;
        int n2 = n3 = 0;
        int i;
        int n4;
        while (true) {
            i = n;
            n4 = n3;
            if (n2 >= byteArray.length) {
                break;
            }
            XMSSUtil.copyBytesAtOffset(array, byteArray[n2], n3);
            n3 += treeDigestSize;
            ++n2;
        }
        while (i < this.authPath.size()) {
            XMSSUtil.copyBytesAtOffset(array, this.authPath.get(i).getValue(), n4);
            n4 += treeDigestSize;
            ++i;
        }
        return array;
    }
    
    public static class Builder
    {
        private List<XMSSNode> authPath;
        private final XMSSParameters params;
        private byte[] reducedSignature;
        private WOTSPlusSignature wotsPlusSignature;
        
        public Builder(final XMSSParameters params) {
            this.wotsPlusSignature = null;
            this.authPath = null;
            this.reducedSignature = null;
            this.params = params;
        }
        
        public XMSSReducedSignature build() {
            return new XMSSReducedSignature(this);
        }
        
        public Builder withAuthPath(final List<XMSSNode> authPath) {
            this.authPath = authPath;
            return this;
        }
        
        public Builder withReducedSignature(final byte[] array) {
            this.reducedSignature = XMSSUtil.cloneArray(array);
            return this;
        }
        
        public Builder withWOTSPlusSignature(final WOTSPlusSignature wotsPlusSignature) {
            this.wotsPlusSignature = wotsPlusSignature;
            return this;
        }
    }
}
