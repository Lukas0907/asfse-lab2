// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.xmss;

import org.bouncycastle.util.Arrays;
import java.io.IOException;
import org.bouncycastle.util.Pack;
import org.bouncycastle.util.Encodable;

public final class XMSSPrivateKeyParameters extends XMSSKeyParameters implements XMSSStoreableObjectInterface, Encodable
{
    private volatile BDS bdsState;
    private final XMSSParameters params;
    private final byte[] publicSeed;
    private final byte[] root;
    private final byte[] secretKeyPRF;
    private final byte[] secretKeySeed;
    
    private XMSSPrivateKeyParameters(final Builder builder) {
        super(true, builder.params.getTreeDigest());
        this.params = builder.params;
        final XMSSParameters params = this.params;
        if (params == null) {
            throw new NullPointerException("params == null");
        }
        final int treeDigestSize = params.getTreeDigestSize();
        final byte[] access$100 = builder.privateKey;
        if (access$100 != null) {
            final int height = this.params.getHeight();
            final int bigEndianToInt = Pack.bigEndianToInt(access$100, 0);
            if (XMSSUtil.isIndexValid(height, bigEndianToInt)) {
                this.secretKeySeed = XMSSUtil.extractBytesAtOffset(access$100, 4, treeDigestSize);
                final int n = 4 + treeDigestSize;
                this.secretKeyPRF = XMSSUtil.extractBytesAtOffset(access$100, n, treeDigestSize);
                final int n2 = n + treeDigestSize;
                this.publicSeed = XMSSUtil.extractBytesAtOffset(access$100, n2, treeDigestSize);
                final int n3 = n2 + treeDigestSize;
                this.root = XMSSUtil.extractBytesAtOffset(access$100, n3, treeDigestSize);
                final int n4 = n3 + treeDigestSize;
                final byte[] bytesAtOffset = XMSSUtil.extractBytesAtOffset(access$100, n4, access$100.length - n4);
                try {
                    final BDS bds = (BDS)XMSSUtil.deserialize(bytesAtOffset, BDS.class);
                    if (bds.getIndex() == bigEndianToInt) {
                        this.bdsState = bds.withWOTSDigest(builder.params.getTreeDigestOID());
                        return;
                    }
                    throw new IllegalStateException("serialized BDS has wrong index");
                }
                catch (ClassNotFoundException cause) {
                    throw new IllegalArgumentException(cause.getMessage(), cause);
                }
                catch (IOException cause2) {
                    throw new IllegalArgumentException(cause2.getMessage(), cause2);
                }
            }
            throw new IllegalArgumentException("index out of bounds");
        }
        final byte[] access$101 = builder.secretKeySeed;
        if (access$101 != null) {
            if (access$101.length != treeDigestSize) {
                throw new IllegalArgumentException("size of secretKeySeed needs to be equal size of digest");
            }
            this.secretKeySeed = access$101;
        }
        else {
            this.secretKeySeed = new byte[treeDigestSize];
        }
        final byte[] access$102 = builder.secretKeyPRF;
        if (access$102 != null) {
            if (access$102.length != treeDigestSize) {
                throw new IllegalArgumentException("size of secretKeyPRF needs to be equal size of digest");
            }
            this.secretKeyPRF = access$102;
        }
        else {
            this.secretKeyPRF = new byte[treeDigestSize];
        }
        final byte[] access$103 = builder.publicSeed;
        if (access$103 != null) {
            if (access$103.length != treeDigestSize) {
                throw new IllegalArgumentException("size of publicSeed needs to be equal size of digest");
            }
            this.publicSeed = access$103;
        }
        else {
            this.publicSeed = new byte[treeDigestSize];
        }
        final byte[] access$104 = builder.root;
        if (access$104 != null) {
            if (access$104.length != treeDigestSize) {
                throw new IllegalArgumentException("size of root needs to be equal size of digest");
            }
            this.root = access$104;
        }
        else {
            this.root = new byte[treeDigestSize];
        }
        BDS access$105 = builder.bdsState;
        if (access$105 == null) {
            if (builder.index < (1 << this.params.getHeight()) - 2 && access$103 != null && access$101 != null) {
                access$105 = new BDS(this.params, access$103, access$101, (OTSHashAddress)new OTSHashAddress.Builder().build(), builder.index);
            }
            else {
                final XMSSParameters params2 = this.params;
                access$105 = new BDS(params2, (1 << params2.getHeight()) - 1, builder.index);
            }
        }
        this.bdsState = access$105;
        if (builder.maxIndex < 0) {
            return;
        }
        if (builder.maxIndex == this.bdsState.getMaxIndex()) {
            return;
        }
        throw new IllegalArgumentException("maxIndex set but not reflected in state");
    }
    
    public XMSSPrivateKeyParameters extractKeyShard(final int n) {
        if (n >= 1) {
            // monitorenter(this)
            final long n2 = n;
            try {
                if (n2 <= this.getUsagesRemaining()) {
                    final XMSSPrivateKeyParameters build = new Builder(this.params).withSecretKeySeed(this.secretKeySeed).withSecretKeyPRF(this.secretKeyPRF).withPublicSeed(this.publicSeed).withRoot(this.root).withIndex(this.getIndex()).withBDSState(this.bdsState.withMaxIndex(this.bdsState.getIndex() + n - 1, this.params.getTreeDigestOID())).build();
                    if (n2 == this.getUsagesRemaining()) {
                        this.bdsState = new BDS(this.params, this.bdsState.getMaxIndex(), this.getIndex() + n);
                    }
                    else {
                        final OTSHashAddress otsHashAddress = (OTSHashAddress)new OTSHashAddress.Builder().build();
                        for (int i = 0; i != n; ++i) {
                            this.bdsState = this.bdsState.getNextState(this.publicSeed, this.secretKeySeed, otsHashAddress);
                        }
                    }
                    return build;
                }
                throw new IllegalArgumentException("usageCount exceeds usages remaining");
            }
            finally {
            }
            // monitorexit(this)
        }
        throw new IllegalArgumentException("cannot ask for a shard with 0 keys");
    }
    
    BDS getBDSState() {
        return this.bdsState;
    }
    
    @Override
    public byte[] getEncoded() throws IOException {
        synchronized (this) {
            return this.toByteArray();
        }
    }
    
    public int getIndex() {
        return this.bdsState.getIndex();
    }
    
    public XMSSPrivateKeyParameters getNextKey() {
        synchronized (this) {
            return this.extractKeyShard(1);
        }
    }
    
    public XMSSParameters getParameters() {
        return this.params;
    }
    
    public byte[] getPublicSeed() {
        return XMSSUtil.cloneArray(this.publicSeed);
    }
    
    public byte[] getRoot() {
        return XMSSUtil.cloneArray(this.root);
    }
    
    public byte[] getSecretKeyPRF() {
        return XMSSUtil.cloneArray(this.secretKeyPRF);
    }
    
    public byte[] getSecretKeySeed() {
        return XMSSUtil.cloneArray(this.secretKeySeed);
    }
    
    public long getUsagesRemaining() {
        synchronized (this) {
            return this.bdsState.getMaxIndex() - this.getIndex() + 1;
        }
    }
    
    XMSSPrivateKeyParameters rollKey() {
        synchronized (this) {
            BDS nextState;
            if (this.bdsState.getIndex() < this.bdsState.getMaxIndex()) {
                nextState = this.bdsState.getNextState(this.publicSeed, this.secretKeySeed, (OTSHashAddress)new OTSHashAddress.Builder().build());
            }
            else {
                nextState = new BDS(this.params, this.bdsState.getMaxIndex(), this.bdsState.getMaxIndex() + 1);
            }
            this.bdsState = nextState;
            return this;
        }
    }
    
    @Override
    public byte[] toByteArray() {
        synchronized (this) {
            final int treeDigestSize = this.params.getTreeDigestSize();
            final byte[] array = new byte[treeDigestSize + 4 + treeDigestSize + treeDigestSize + treeDigestSize];
            Pack.intToBigEndian(this.bdsState.getIndex(), array, 0);
            XMSSUtil.copyBytesAtOffset(array, this.secretKeySeed, 4);
            final int n = 4 + treeDigestSize;
            XMSSUtil.copyBytesAtOffset(array, this.secretKeyPRF, n);
            final int n2 = n + treeDigestSize;
            XMSSUtil.copyBytesAtOffset(array, this.publicSeed, n2);
            XMSSUtil.copyBytesAtOffset(array, this.root, n2 + treeDigestSize);
            try {
                return Arrays.concatenate(array, XMSSUtil.serialize(this.bdsState));
            }
            catch (IOException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("error serializing bds state: ");
                sb.append(ex.getMessage());
                throw new RuntimeException(sb.toString());
            }
        }
    }
    
    public static class Builder
    {
        private BDS bdsState;
        private int index;
        private int maxIndex;
        private final XMSSParameters params;
        private byte[] privateKey;
        private byte[] publicSeed;
        private byte[] root;
        private byte[] secretKeyPRF;
        private byte[] secretKeySeed;
        
        public Builder(final XMSSParameters params) {
            this.index = 0;
            this.maxIndex = -1;
            this.secretKeySeed = null;
            this.secretKeyPRF = null;
            this.publicSeed = null;
            this.root = null;
            this.bdsState = null;
            this.privateKey = null;
            this.params = params;
        }
        
        public XMSSPrivateKeyParameters build() {
            return new XMSSPrivateKeyParameters(this, null);
        }
        
        public Builder withBDSState(final BDS bdsState) {
            this.bdsState = bdsState;
            return this;
        }
        
        public Builder withIndex(final int index) {
            this.index = index;
            return this;
        }
        
        public Builder withMaxIndex(final int maxIndex) {
            this.maxIndex = maxIndex;
            return this;
        }
        
        public Builder withPrivateKey(final byte[] array) {
            this.privateKey = XMSSUtil.cloneArray(array);
            return this;
        }
        
        public Builder withPublicSeed(final byte[] array) {
            this.publicSeed = XMSSUtil.cloneArray(array);
            return this;
        }
        
        public Builder withRoot(final byte[] array) {
            this.root = XMSSUtil.cloneArray(array);
            return this;
        }
        
        public Builder withSecretKeyPRF(final byte[] array) {
            this.secretKeyPRF = XMSSUtil.cloneArray(array);
            return this;
        }
        
        public Builder withSecretKeySeed(final byte[] array) {
            this.secretKeySeed = XMSSUtil.cloneArray(array);
            return this;
        }
    }
}
