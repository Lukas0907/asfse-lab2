// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.xmss;

import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.pqc.crypto.StateAwareMessageSigner;

public class XMSSSigner implements StateAwareMessageSigner
{
    private boolean hasGenerated;
    private boolean initSign;
    private KeyedHashFunctions khf;
    private XMSSParameters params;
    private XMSSPrivateKeyParameters privateKey;
    private XMSSPublicKeyParameters publicKey;
    private WOTSPlus wotsPlus;
    
    private WOTSPlusSignature wotsSign(final byte[] array, final OTSHashAddress otsHashAddress) {
        if (array.length != this.params.getTreeDigestSize()) {
            throw new IllegalArgumentException("size of messageDigest needs to be equal to size of digest");
        }
        if (otsHashAddress != null) {
            final WOTSPlus wotsPlus = this.wotsPlus;
            wotsPlus.importKeys(wotsPlus.getWOTSPlusSecretKey(this.privateKey.getSecretKeySeed(), otsHashAddress), this.privateKey.getPublicSeed());
            return this.wotsPlus.sign(array, otsHashAddress);
        }
        throw new NullPointerException("otsHashAddress == null");
    }
    
    @Override
    public byte[] generateSignature(byte[] byteArray) {
        if (byteArray == null) {
            throw new NullPointerException("message == null");
        }
        if (this.initSign) {
            final XMSSPrivateKeyParameters privateKey = this.privateKey;
            if (privateKey != null) {
                synchronized (privateKey) {
                    if (this.privateKey.getUsagesRemaining() > 0L) {
                        if (!this.privateKey.getBDSState().getAuthenticationPath().isEmpty()) {
                            try {
                                final int index = this.privateKey.getIndex();
                                this.hasGenerated = true;
                                final KeyedHashFunctions khf = this.khf;
                                final byte[] secretKeyPRF = this.privateKey.getSecretKeyPRF();
                                final long n = index;
                                final byte[] prf = khf.PRF(secretKeyPRF, XMSSUtil.toBytesBigEndian(n, 32));
                                byteArray = ((XMSSReducedSignature.Builder)new XMSSSignature.Builder(this.params).withIndex(index).withRandom(prf)).withWOTSPlusSignature(this.wotsSign(this.khf.HMsg(Arrays.concatenate(prf, this.privateKey.getRoot(), XMSSUtil.toBytesBigEndian(n, this.params.getTreeDigestSize())), byteArray), (OTSHashAddress)new OTSHashAddress.Builder().withOTSAddress(index).build())).withAuthPath(this.privateKey.getBDSState().getAuthenticationPath()).build().toByteArray();
                                return byteArray;
                            }
                            finally {
                                this.privateKey.getBDSState().markUsed();
                                this.privateKey.rollKey();
                            }
                        }
                        throw new IllegalStateException("not initialized");
                    }
                    throw new IllegalStateException("no usages of private key remaining");
                }
            }
            throw new IllegalStateException("signing key no longer usable");
        }
        throw new IllegalStateException("signer not initialized for signature generation");
    }
    
    @Override
    public AsymmetricKeyParameter getUpdatedPrivateKey() {
        synchronized (this.privateKey) {
            if (this.hasGenerated) {
                final XMSSPrivateKeyParameters privateKey = this.privateKey;
                this.privateKey = null;
                return privateKey;
            }
            final XMSSPrivateKeyParameters privateKey2 = this.privateKey;
            if (privateKey2 != null) {
                this.privateKey = this.privateKey.getNextKey();
            }
            return privateKey2;
        }
    }
    
    public long getUsagesRemaining() {
        return this.privateKey.getUsagesRemaining();
    }
    
    @Override
    public void init(final boolean b, final CipherParameters cipherParameters) {
        XMSSParameters params;
        if (b) {
            this.initSign = true;
            this.hasGenerated = false;
            this.privateKey = (XMSSPrivateKeyParameters)cipherParameters;
            params = this.privateKey.getParameters();
        }
        else {
            this.initSign = false;
            this.publicKey = (XMSSPublicKeyParameters)cipherParameters;
            params = this.publicKey.getParameters();
        }
        this.params = params;
        this.wotsPlus = this.params.getWOTSPlus();
        this.khf = this.wotsPlus.getKhf();
    }
    
    @Override
    public boolean verifySignature(byte[] hMsg, final byte[] array) {
        final XMSSSignature build = new XMSSSignature.Builder(this.params).withSignature(array).build();
        final int index = build.getIndex();
        this.wotsPlus.importKeys(new byte[this.params.getTreeDigestSize()], this.publicKey.getPublicSeed());
        final byte[] random = build.getRandom();
        final byte[] root = this.publicKey.getRoot();
        final long n = index;
        hMsg = this.khf.HMsg(Arrays.concatenate(random, root, XMSSUtil.toBytesBigEndian(n, this.params.getTreeDigestSize())), hMsg);
        final int height = this.params.getHeight();
        return Arrays.constantTimeAreEqual(XMSSVerifierUtil.getRootNodeFromSignature(this.wotsPlus, height, hMsg, build, (OTSHashAddress)new OTSHashAddress.Builder().withOTSAddress(index).build(), XMSSUtil.getLeafIndex(n, height)).getValue(), this.publicKey.getRoot());
    }
}
