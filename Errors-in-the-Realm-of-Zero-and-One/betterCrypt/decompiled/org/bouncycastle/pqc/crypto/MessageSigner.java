// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto;

import org.bouncycastle.crypto.CipherParameters;

public interface MessageSigner
{
    byte[] generateSignature(final byte[] p0);
    
    void init(final boolean p0, final CipherParameters p1);
    
    boolean verifySignature(final byte[] p0, final byte[] p1);
}
