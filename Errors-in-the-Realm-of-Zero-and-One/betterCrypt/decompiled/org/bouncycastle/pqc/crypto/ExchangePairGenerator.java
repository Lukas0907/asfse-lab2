// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto;

import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public interface ExchangePairGenerator
{
    ExchangePair GenerateExchange(final AsymmetricKeyParameter p0);
    
    ExchangePair generateExchange(final AsymmetricKeyParameter p0);
}
