// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.qteslarnd1;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public final class QTESLAPublicKeyParameters extends AsymmetricKeyParameter
{
    private byte[] publicKey;
    private int securityCategory;
    
    public QTESLAPublicKeyParameters(final int securityCategory, final byte[] array) {
        super(false);
        if (array.length == QTESLASecurityCategory.getPublicSize(securityCategory)) {
            this.securityCategory = securityCategory;
            this.publicKey = Arrays.clone(array);
            return;
        }
        throw new IllegalArgumentException("invalid key size for security category");
    }
    
    public byte[] getPublicData() {
        return Arrays.clone(this.publicKey);
    }
    
    public int getSecurityCategory() {
        return this.securityCategory;
    }
}
