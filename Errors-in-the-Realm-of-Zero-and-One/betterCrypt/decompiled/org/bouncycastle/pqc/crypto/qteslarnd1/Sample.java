// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.qteslarnd1;

import org.bouncycastle.util.Arrays;

class Sample
{
    static final long[][] CUMULATIVE_DISTRIBUTION_TABLE_I;
    static final long[][] CUMULATIVE_DISTRIBUTION_TABLE_III;
    static final double[][] EXPONENTIAL_DISTRIBUTION_I;
    static final double[][] EXPONENTIAL_DISTRIBUTION_III_SIZE;
    static final double[][] EXPONENTIAL_DISTRIBUTION_III_SPEED;
    static final double[][] EXPONENTIAL_DISTRIBUTION_P;
    
    static {
        EXPONENTIAL_DISTRIBUTION_I = new double[][] { { 1.0, 0.9990496327075997, 0.99810016861319, 0.9971516068584009, 0.9962039465856783, 0.9952571869382832, 0.9943113270602909, 0.9933663660965897, 0.992422303192881, 0.9914791374956781, 0.9905368681523049, 0.9895954943108964, 0.9886550151203967, 0.9877154297305588, 0.9867767372919439, 0.9858389369559202, 0.9849020278746626, 0.983966009201152, 0.9830308800891736, 0.9820966396933174, 0.9811632871689767, 0.9802308216723474, 0.9792992423604274, 0.9783685483910157, 0.9774387389227118, 0.9765098131149148, 0.9755817701278225, 0.9746546091224311, 0.973728329260534, 0.9728029297047213, 0.9718784096183788, 0.9709547681656876 }, { 1.0, 0.9700320045116229, 0.940962089776837, 0.9127633421156709, 0.8854096543971924, 0.8588757018688518, 0.8331369187101693, 0.8081694752890625, 0.7839502560997557, 0.7604568383618461, 0.7376674712607126, 0.715561055810049, 0.6941171253178751, 0.6733158264379437, 0.6531379007889985, 0.6335646671248656, 0.6145780040388725, 0.5961603331865797, 0.5782946030112949, 0.5609642729572996, 0.5441532981561744, 0.5278461145720446, 0.5120276245919921, 0.49668318304829484, 0.48179858365955075, 0.46736004587813484, 0.45335420213181116, 0.4397680854476882, 0.42658911744705963, 0.4138050967000153, 0.40140418742904177, 0.3893749085511526 }, { 1.0, 0.37770612304840434, 0.14266191538825637, 0.05388427896795781, 0.02035242210224602, 0.007687234446884, 0.0029035155198967005, 0.0010966755902310549, 4.142210854279923E-4, 1.5645384026190888E-4, 5.9093573441359953E-5, 2.232000452161222E-5, 8.430402374281007E-6, 3.184214596527742E-6, 1.2026973502086328E-6, 4.5426615334789165E-7, 1.715791076131441E-7, 6.480647953266561E-8, 2.4477804132698898E-8, 9.24541649969991E-9, 3.4920504220694024E-9, 1.318968826409378E-9, 4.9818260184479E-10, 1.881666191129625E-10, 7.107168419228285E-11, 2.6844210294787717E-11, 1.0139222596740334E-11, 3.829646457739566E-12, 1.4464809161988664E-12, 5.463446989209777E-13, 2.0635773807749023E-13, 7.794258121028692E-14 } };
        EXPONENTIAL_DISTRIBUTION_III_SIZE = new double[][] { { 1.0, 0.9914791374956781, 0.9830308800891736, 0.9746546091224311, 0.9663497112088952, 0.9581155781885929, 0.9499516070835989, 0.94185720005388, 0.9338317643535151, 0.9258747122872905, 0.9179854611676618, 0.9101634332720855, 0.9024080558007124, 0.894718760834442, 0.8870949852933344, 0.8795361708953764, 0.872041764115599, 0.8646112161455436, 0.8572439828530728, 0.8499395247425244, 0.8426973069152046, 0.8355167990302177, 0.82839747526563, 0.8213388142799641, 0.8143402991740217, 0.8074014174530314, 0.8005216609891195, 0.7937005259840998, 0.7869375129325812, 0.7802321265853895, 0.7735838759133007, 0.766992274071083 }, { 1.0, 0.7604568383618461, 0.5782946030112949, 0.4397680854476882, 0.3344246478719911, 0.254315510391008, 0.19339596897832517, 0.14706928712118283, 0.11183984510430525, 0.08504937501089856, 0.06467637882543892, 0.049183594558286324, 0.037402000817065316, 0.02844260728975267, 0.021629375214332912, 0.016448206291233683, 0.012508150952954992, 0.009511908927436865, 0.007233396189744457, 0.005500685597071693, 0.004183033977971684, 0.0031810167936485224, 0.002419025973673892, 0.0018395648438552343, 0.0013989096651197545, 0.0010638104210907973, 8.089819094390918E-4, 6.15195825143981E-4, 4.678298721623989E-4, 3.557644254758445E-4, 2.7054349019897927E-4, 2.0573664719609488E-4 }, { 1.0, 1.5645384026190888E-4, 2.4477804132698898E-8, 3.829646457739566E-12, 5.991628951587712E-16, 9.374133589003324E-20, 1.4666191991277205E-23, 2.294582059053771E-27, 3.5899617493504067E-31, 5.616633020792314E-35, 8.787438054448035E-39, 1.3748284296820321E-42, 2.1509718752500368E-46, 3.365278101782278E-50, 5.265106825731444E-54, 8.237461822748735E-58, 1.2887825361799032E-61, 2.016349770478284E-65, 3.15465664902546E-69, 4.93558147447798E-73, 7.721906756076147E-77, 1.2081219661324923E-80, 1.8901532110619624E-84, 2.957217285540224E-88, 4.626680008116659E-92, 7.238618549328511E-96, 1.1325096702335332E-99, 1.7718548704178432E-103, 2.7721349886363846E-107, 4.337111646965655E-111, 6.78557772812429E-115, 1.0616296939607244E-118 } };
        EXPONENTIAL_DISTRIBUTION_III_SPEED = new double[][] { { 1.0, 0.9951980443443538, 0.9904191474668262, 0.9856631986401876, 0.980930087668915, 0.9762197048866396, 0.9715319411536059, 0.9668666878541423, 0.9622238368941451, 0.9576032806985737, 0.9530049122089577, 0.9484286248809173, 0.9438743126816935, 0.9393418700876924, 0.9348311920820395, 0.9303421741521466, 0.9258747122872905, 0.9214287029762026, 0.9170040432046712, 0.9126006304531541, 0.9082183626944031, 0.903857138391101, 0.8995168564935077, 0.8951974164371195, 0.8908987181403393, 0.8866206620021573, 0.8823631488998432, 0.8781260801866497, 0.8739093576895269, 0.8697128837068475, 0.8655365610061431, 0.8613802928218509 }, { 1.0, 0.8572439828530728, 0.7348672461377994, 0.6299605249474366, 0.540029869446153, 0.46293735614364523, 0.3968502629920499, 0.3401975000435942, 0.29163225989402913, 0.25, 0.2143109957132682, 0.18371681153444985, 0.15749013123685915, 0.13500746736153826, 0.11573433903591131, 0.09921256574801247, 0.08504937501089856, 0.07290806497350728, 0.0625, 0.05357774892831705, 0.04592920288361246, 0.03937253280921479, 0.033751866840384566, 0.028933584758977827, 0.02480314143700312, 0.02126234375272464, 0.01822701624337682, 0.015625, 0.013394437232079262, 0.011482300720903116, 0.009843133202303697, 0.008437966710096141 }, { 1.0, 0.007233396189744457, 5.232202043780962E-5, 3.784659032745837E-7, 2.7375938226945676E-9, 1.9802100726146846E-11, 1.4323643994144654E-13, 1.03608591890502E-15, 7.494419938055456E-18, 5.421010862427522E-20, 3.921231931684655E-22, 2.8363824113752076E-24, 2.0516677727099623E-26, 1.4840525849741735E-28, 1.0734740313532598E-30, 7.764862968180291E-33, 5.616633020792314E-35, 4.062733189179202E-37, 2.9387358770557188E-39, 2.125704089576017E-41, 1.537605986206337E-43, 1.1122113281953186E-45, 8.045065183558638E-48, 5.819314384499884E-50, 4.209340649576657E-52, 3.0447828615984243E-54, 2.2024120749685265E-56, 1.5930919111324523E-58, 1.1523464959898195E-60, 8.335378753358135E-63, 6.029309691461764E-65, 4.3612385749008845E-67 } };
        EXPONENTIAL_DISTRIBUTION_P = new double[][] { { 1.0, 0.9930924954370359, 0.9862327044933592, 0.9794202975869268, 0.9726549474122855, 0.9659363289248456, 0.9592641193252643, 0.9526379980439373, 0.9460576467255959, 0.9395227492140118, 0.9330329915368074, 0.9265880618903709, 0.9201876506248751, 0.9138314502294005, 0.9075191553171609, 0.9012504626108302, 0.8950250709279725, 0.8888426811665702, 0.8827029962906549, 0.8766057213160351, 0.8705505632961241, 0.8645372313078652, 0.8585654364377537, 0.8526348917679567, 0.8467453123625271, 0.8408964152537145, 0.8350879194283694, 0.8293195458144417, 0.8235910172675731, 0.8179020585577811, 0.8122523963562355, 0.8066417592221263 }, { 1.0, 0.8010698775896221, 0.6417129487814521, 0.5140569133280333, 0.41179550863378656, 0.32987697769322355, 0.26425451014034507, 0.2116863280906318, 0.16957554093095897, 0.13584185781575725, 0.10881882041201552, 0.08717147914690034, 0.06983044612951375, 0.05593906693299828, 0.0448111015004946, 0.03589682359365735, 0.028755864082027346, 0.023035456520173456, 0.01845301033483641, 0.014782150730087436, 0.011841535675862484, 0.009485897534336304, 0.007598866776658481, 0.0060872232785976555, 0.004876291206646921, 0.00390625, 0.0031291792093344614, 0.0025066912061775474, 0.00200803481768763, 0.0016085762056007287, 0.0012885819441141545, 0.001032244180235723 }, { 1.0, 8.268997191040304E-4, 6.837631454543244E-7, 5.654035529098692E-10, 4.675320390815916E-13, 3.866021117887027E-16, 3.196811776431032E-19, 2.643442759959277E-22, 2.185862075677909E-25, 1.807488736378216E-28, 1.4946119283948456E-31, 1.2358941837592312E-34, 1.0219605533928131E-37, 8.450588945359167E-41, 6.987789625181121E-44, 5.778201278220326E-47, 4.777993013886938E-50, 3.9509210810641284E-53, 3.26701553213412E-56, 2.701494225830208E-59, 2.2338648165001596E-62, 1.8471821892803583E-65, 1.5274344334498962E-68, 1.263035103969543E-71, 1.044403372690945E-74, 8.636168555094445E-78, 7.1412453523426565E-81, 5.905093775905105E-84, 4.882920384578891E-87, 4.037685494415629E-90, 3.3387610011627014E-93, 2.760820534016929E-96 } };
        CUMULATIVE_DISTRIBUTION_TABLE_I = new long[][] { { 144115188075855872L, 0L }, { 216172782113783808L, 0L }, { 225179981368524800L, 0L }, { 225461456345235456L, 0L }, { 225463655368491008L, 0L }, { 225463659663458304L, 0L }, { 225463659665555456L, 0L }, { 225463659665555712L, 0L }, { 225463659665555712L, 144115188075855872L }, { 225463659665555712L, 144116287587483648L }, { 225463659665555712L, 144116287589580800L }, { 225463659665555712L, 144116287589580801L } };
        CUMULATIVE_DISTRIBUTION_TABLE_III = new long[][] { { 2199023255552L, 0L, 0L }, { 3298534883328L, 0L, 0L }, { 3435973836800L, 0L, 0L }, { 3440268804096L, 0L, 0L }, { 3440302358528L, 0L, 0L }, { 3440302424064L, 0L, 0L }, { 3440302424096L, 0L, 0L }, { 3440302424096L, 72057594037927936L, 0L }, { 3440302424096L, 72059793061183488L, 0L }, { 3440302424096L, 72059793077960704L, 0L }, { 3440302424096L, 72059793077960736L, 0L }, { 3440302424096L, 72059793077960736L, 281474976710656L }, { 3440302424096L, 72059793077960736L, 281475010265088L }, { 3440302424096L, 72059793077960736L, 281475010265089L } };
    }
    
    private static int bernoulli(final long n, long n2, final double[][] array) {
        double n3 = 4.6116860184273879E18;
        for (long n4 = 0L; n4 < 3L; ++n4, n2 >>= 5) {
            n3 *= array[(int)n4][(int)(0x1FL & n2)];
        }
        return (int)((n & 0x3FFFFFFFFFFFFFFFL) - round(n3) >>> 63);
    }
    
    public static void encodeC(final int[] array, final short[] array2, final byte[] array3, final int n, final int n2, final int n3) {
        final short[] array4 = new short[n2];
        final byte[] array5 = new byte[168];
        short n4 = 1;
        HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array5, 0, 168, (short)0, array3, n, 32);
        Arrays.fill(array4, (short)0);
        int i;
        int n9;
        for (int n5 = i = 0; i < n3; i = n9) {
            int n6 = n5;
            short n7 = n4;
            if (n5 > 165) {
                n7 = (short)(n4 + 1);
                HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array5, 0, 168, n4, array3, n, 32);
                n6 = 0;
            }
            final int n8 = (array5[n6] << 8 | (array5[n6 + 1] & 0xFF)) & n2 - 1;
            n9 = i;
            if (array4[n8] == 0) {
                if ((array5[n6 + 2] & 0x1) == 0x1) {
                    array4[n8] = -1;
                }
                else {
                    array4[n8] = 1;
                }
                array[i] = n8;
                array2[i] = array4[n8];
                n9 = i + 1;
            }
            n5 = n6 + 3;
            n4 = n7;
        }
    }
    
    private static long modulus7(long n) {
        for (int i = 0; i < 2; ++i) {
            n = (n >> 3) + (0x7L & n);
        }
        return n & n - 7L >> 3;
    }
    
    public static void polynomialGaussSamplerI(final int[] array, final int n, final byte[] array2, final int n2, int i) {
        final byte[] array3 = new byte[1024];
        final short n3 = (short)(i << 8);
        short n4 = (short)(n3 + 1);
        HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array3, 0, 1024, n3, array2, n2, 32);
        i = 0;
        short n6;
        for (int j = 0; j < 512; ++j, n4 = n6) {
            int n5 = i;
            n6 = n4;
            if (i + 46 > 512) {
                n6 = (short)(n4 + 1);
                HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array3, 0, 1024, n4, array2, n2, 32);
                n5 = 0;
            }
            long n22;
            long n24;
            long n25;
            long n26;
            while (true) {
                i = n5 + 1;
                long load64 = CommonFunction.load64(array3, n5 * 64 / 32);
                long n7 = 64L;
                long n20;
                int n23;
                while (true) {
                    final int n8 = i + 1;
                    final long load65 = CommonFunction.load64(array3, i * 64 / 32);
                    int n9 = n8 + 1;
                    final long load66 = CommonFunction.load64(array3, n8 * 64 / 32);
                    long n10 = n7;
                    long load67 = load64;
                    if (n7 <= 58L) {
                        load67 = (load64 << 6 ^ (load65 >>> 58 & 0x3FL));
                        n10 = n7 + 6L;
                    }
                    final long n11 = load65 & 0x3FFFFFFFFFFFFFFL;
                    if (n11 <= 225463659665555712L) {
                        long n12 = 0L;
                        long[][] cumulative_DISTRIBUTION_TABLE_I;
                        long n13;
                        for (i = 0; i < 12; ++i) {
                            cumulative_DISTRIBUTION_TABLE_I = Sample.CUMULATIVE_DISTRIBUTION_TABLE_I;
                            n13 = load66 - cumulative_DISTRIBUTION_TABLE_I[i][1];
                            n12 += (n11 - (cumulative_DISTRIBUTION_TABLE_I[i][0] + ((n13 & cumulative_DISTRIBUTION_TABLE_I[i][1] & 0x1L) + (cumulative_DISTRIBUTION_TABLE_I[i][1] >> 1) + (n13 >>> 1) >>> 63)) >>> 63 & 0x1L);
                        }
                        long n19;
                        double n21;
                        while (true) {
                            long n14 = n10;
                            int n15 = n9;
                            if (n10 < 6L) {
                                load67 = CommonFunction.load64(array3, n9 * 64 / 32);
                                n15 = n9 + 1;
                                n14 = 64L;
                            }
                            final long n16 = load67 & 0x3FL;
                            long load68 = load67 >> 6;
                            final long n17 = n10 = n14 - 6L;
                            n9 = n15;
                            load67 = load68;
                            if (n16 != 63L) {
                                long n18 = n17;
                                i = n15;
                                if (n17 < 2L) {
                                    load68 = CommonFunction.load64(array3, n15 * 64 / 32);
                                    i = n15 + 1;
                                    n18 = 64L;
                                }
                                n19 = (modulus7(n16) << 2) + (load68 & 0x3L);
                                n20 = load68 >> 2;
                                n7 = n18 - 2L;
                                n21 = (double)n19;
                                n10 = n7;
                                n9 = i;
                                load67 = n20;
                                if (n21 < 27.0) {
                                    break;
                                }
                                continue;
                            }
                        }
                        n22 = (long)(n12 * 27.0 + n21);
                        n23 = i + 1;
                        final long load69 = CommonFunction.load64(array3, i * 64 / 32);
                        n24 = n22 << 1;
                        if (bernoulli(load69, n19 * (n24 - n19), Sample.EXPONENTIAL_DISTRIBUTION_I) != 0) {
                            break;
                        }
                        i = n23;
                        load64 = n20;
                    }
                    else {
                        i = n9;
                        n7 = n10;
                        load64 = load67;
                    }
                }
                n25 = 64L;
                long load70 = n20 << (int)(64L - n7);
                if (n7 == 0L) {
                    load70 = CommonFunction.load64(array3, n23 * 64 / 32);
                    i = n23 + 1;
                }
                else {
                    n25 = n7;
                    i = n23;
                }
                n26 = load70 << 1;
                if (((load70 >> 63 & 0x1L) | n22) != 0x0L) {
                    break;
                }
                n5 = i;
            }
            long load71;
            if (n25 - 1L == 0L) {
                final int n27 = i + 1;
                load71 = CommonFunction.load64(array3, i * 64 / 32);
                i = n27;
            }
            else {
                load71 = n26;
            }
            array[n + j] = (int)((n24 & load71 >> 63) - n22 << 48 >> 48);
        }
    }
    
    public static void polynomialGaussSamplerIII(final int[] array, final int n, final byte[] array2, final int n2, int n3, final int n4, final double n5, final double[][] array3) {
        final int n6 = n4 * 64 / 32;
        final byte[] array4 = new byte[n6];
        final short n7 = (short)(n3 << 8);
        short n8 = (short)(n7 + 1);
        HashUtils.customizableSecureHashAlgorithmKECCAK256Simple(array4, 0, n6, n7, array2, n2, 32);
        int n9 = 0;
        int i = 0;
        n3 = n6;
        while (i < n4) {
            int n10 = n3;
            int n11 = n9;
            short n12 = n8;
            if (n9 + 46 > n4) {
                n12 = (short)(n8 + 1);
                HashUtils.customizableSecureHashAlgorithmKECCAK256Simple(array4, 0, n3, n8, array2, n2, 32);
                n11 = 0;
                n10 = n3;
            }
            long n32;
            long n33;
            long n34;
            int n35;
            long n36;
            while (true) {
                int n13 = n11 + 1;
                long load64 = CommonFunction.load64(array4, n11 * 64 / 32);
                long n14 = 64L;
                n3 = n10;
                long n30;
                while (true) {
                    final int n15 = n13 + 1;
                    final long load65 = CommonFunction.load64(array4, n13 * 64 / 32);
                    final int n16 = n15 + 1;
                    final long load66 = CommonFunction.load64(array4, n15 * 64 / 32);
                    n13 = n16 + 1;
                    final long load67 = CommonFunction.load64(array4, n16 * 64 / 32);
                    long n17 = n14;
                    long load68 = load64;
                    if (n14 <= 58L) {
                        load68 = (load64 << 6 ^ (load65 >>> 58 & 0x3FL));
                        n17 = n14 + 6L;
                    }
                    final long n18 = load65 & 0x3FFFFFFFFFFL;
                    if (n18 <= 3440302424096L) {
                        long n19 = 0L;
                        for (int j = 0; j < 14; ++j) {
                            final long[][] cumulative_DISTRIBUTION_TABLE_III = Sample.CUMULATIVE_DISTRIBUTION_TABLE_III;
                            final long n20 = load67 - cumulative_DISTRIBUTION_TABLE_III[j][2];
                            final long n21 = (n20 & cumulative_DISTRIBUTION_TABLE_III[j][2] & 0x1L) + (cumulative_DISTRIBUTION_TABLE_III[j][2] >> 1) + (n20 >>> 1) >> 63;
                            final long n22 = load66 - (cumulative_DISTRIBUTION_TABLE_III[j][1] + n21);
                            n19 += (n18 - (cumulative_DISTRIBUTION_TABLE_III[j][0] + ((n22 & n21 & 0x1L) + (cumulative_DISTRIBUTION_TABLE_III[j][1] >> 1) + (n22 >>> 1) >> 63)) >>> 63 & 0x1L);
                        }
                        int n27;
                        long n29;
                        double n31;
                        while (true) {
                            int n23 = n13;
                            long n24 = n17;
                            if (n17 < 6L) {
                                load68 = CommonFunction.load64(array4, n13 * 64 / 32);
                                n23 = n13 + 1;
                                n24 = 64L;
                            }
                            final long n25 = load68 & 0x3FL;
                            long load69 = load68 >> 6;
                            final long n26 = n24 - 6L;
                            n13 = n23;
                            n17 = n26;
                            load68 = load69;
                            if (n25 != 63L) {
                                n27 = n23;
                                long n28 = n26;
                                if (n26 < 2L) {
                                    load69 = CommonFunction.load64(array4, n23 * 64 / 32);
                                    n27 = n23 + 1;
                                    n28 = 64L;
                                }
                                n29 = (modulus7(n25) << 2) + (load69 & 0x3L);
                                n30 = load69 >> 2;
                                n14 = n28 - 2L;
                                n31 = (double)n29;
                                n13 = n27;
                                n17 = n14;
                                load68 = n30;
                                if (n31 < n5) {
                                    break;
                                }
                                continue;
                            }
                        }
                        n32 = (long)(n19 * n5 + n31);
                        n13 = n27 + 1;
                        final long load70 = CommonFunction.load64(array4, n27 * 64 / 32);
                        n33 = n32 << 1;
                        if (bernoulli(load70, n29 * (n33 - n29), array3) != 0) {
                            break;
                        }
                        load64 = n30;
                    }
                    else {
                        n14 = n17;
                        load64 = load68;
                    }
                }
                n34 = 64L;
                long load71 = n30 << (int)(64L - n14);
                if (n14 == 0L) {
                    load71 = CommonFunction.load64(array4, n13 * 64 / 32);
                    n35 = n13 + 1;
                }
                else {
                    n34 = n14;
                    n35 = n13;
                }
                n36 = load71 << 1;
                if (((load71 >> 63 & 0x1L) | n32) != 0x0L) {
                    break;
                }
                n11 = n35;
                n10 = n3;
            }
            n9 = n35;
            long load72 = n36;
            if (n34 - 1L == 0L) {
                load72 = CommonFunction.load64(array4, n35 * 64 / 32);
                n9 = n35 + 1;
            }
            array[n + i] = (int)((n33 & load72 >> 63) - n32 << 48 >> 48);
            ++i;
            n8 = n12;
        }
    }
    
    public static void polynomialGaussSamplerIIIP(final long[] array, final int n, final byte[] array2, final int n2, int n3) {
        final byte[] array3 = new byte[4096];
        final short n4 = (short)(n3 << 8);
        short n5 = (short)(n4 + 1);
        HashUtils.customizableSecureHashAlgorithmKECCAK256Simple(array3, 0, 4096, n4, array2, n2, 32);
        n3 = 0;
        short n7;
        int n31;
        for (int i = 0; i < 2048; ++i, n3 = n31, n5 = n7) {
            int n6 = n3;
            n7 = n5;
            if (n3 + 46 > 2048) {
                n7 = (short)(n5 + 1);
                HashUtils.customizableSecureHashAlgorithmKECCAK256Simple(array3, 0, 4096, n5, array2, n2, 32);
                n6 = 0;
            }
            long n26;
            long n28;
            long n29;
            long n30;
            while (true) {
                n3 = n6 + 1;
                long load64 = CommonFunction.load64(array3, n6 * 64 / 32);
                long n8 = 64L;
                long n24;
                int n27;
                while (true) {
                    final int n9 = n3 + 1;
                    final long load65 = CommonFunction.load64(array3, n3 * 64 / 32);
                    final int n10 = n9 + 1;
                    final long load66 = CommonFunction.load64(array3, n9 * 64 / 32);
                    n3 = n10 + 1;
                    final long load67 = CommonFunction.load64(array3, n10 * 64 / 32);
                    long n11 = n8;
                    long load68 = load64;
                    if (n8 <= 58L) {
                        load68 = (load64 << 6 ^ (load65 >>> 58 & 0x3FL));
                        n11 = n8 + 6L;
                    }
                    final long n12 = load65 & 0x3FFFFFFFFFFL;
                    if (n12 <= 3440302424096L) {
                        long n13 = 0L;
                        for (int j = 0; j < 14; ++j) {
                            final long[][] cumulative_DISTRIBUTION_TABLE_III = Sample.CUMULATIVE_DISTRIBUTION_TABLE_III;
                            final long n14 = load67 - cumulative_DISTRIBUTION_TABLE_III[j][2];
                            final long n15 = (n14 & cumulative_DISTRIBUTION_TABLE_III[j][2] & 0x1L) + (cumulative_DISTRIBUTION_TABLE_III[j][2] >> 1) + (n14 >>> 1) >> 63;
                            final long n16 = load66 - (cumulative_DISTRIBUTION_TABLE_III[j][1] + n15);
                            n13 += (n12 - (cumulative_DISTRIBUTION_TABLE_III[j][0] + ((n16 & n15 & 0x1L) + (cumulative_DISTRIBUTION_TABLE_III[j][1] >> 1) + (n16 >>> 1) >> 63)) >>> 63 & 0x1L);
                        }
                        int n17 = n3;
                        long n23;
                        double n25;
                        while (true) {
                            int n18 = n17;
                            long n19 = n11;
                            if (n11 < 6L) {
                                load68 = CommonFunction.load64(array3, n17 * 64 / 32);
                                n18 = n17 + 1;
                                n19 = 64L;
                            }
                            final long n20 = load68 & 0x3FL;
                            long load69 = load68 >> 6;
                            final long n21 = n19 - 6L;
                            n17 = n18;
                            n11 = n21;
                            load68 = load69;
                            if (n20 != 63L) {
                                n3 = n18;
                                long n22 = n21;
                                if (n21 < 2L) {
                                    load69 = CommonFunction.load64(array3, n18 * 64 / 32);
                                    n3 = n18 + 1;
                                    n22 = 64L;
                                }
                                n23 = (modulus7(n20) << 2) + (load69 & 0x3L);
                                n24 = load69 >> 2;
                                n8 = n22 - 2L;
                                n25 = (double)n23;
                                n17 = n3;
                                n11 = n8;
                                load68 = n24;
                                if (n25 < 10.0) {
                                    break;
                                }
                                continue;
                            }
                        }
                        n26 = (long)(n13 * 10.0 + n25);
                        n27 = n3 + 1;
                        final long load70 = CommonFunction.load64(array3, n3 * 64 / 32);
                        n28 = n26 << 1;
                        if (bernoulli(load70, n23 * (n28 - n23), Sample.EXPONENTIAL_DISTRIBUTION_P) != 0) {
                            break;
                        }
                        n3 = n27;
                        load64 = n24;
                    }
                    else {
                        n8 = n11;
                        load64 = load68;
                    }
                }
                n29 = 64L;
                long load71 = n24 << (int)(64L - n8);
                if (n8 == 0L) {
                    n3 = n27 + 1;
                    load71 = CommonFunction.load64(array3, n27 * 64 / 32);
                }
                else {
                    n29 = n8;
                    n3 = n27;
                }
                n30 = load71 << 1;
                n6 = n3;
                if (((load71 >> 63 & 0x1L) | n26) != 0x0L) {
                    break;
                }
            }
            n31 = n3;
            long load72 = n30;
            if (n29 - 1L == 0L) {
                load72 = CommonFunction.load64(array3, n3 * 64 / 32);
                n31 = n3 + 1;
            }
            array[n + i] = (n28 & load72 >> 63) - n26 << 48 >> 48;
        }
    }
    
    public static void polynomialGaussSamplerIP(final long[] array, final int n, final byte[] array2, final int n2, int i) {
        final byte[] array3 = new byte[2048];
        final short n3 = (short)(i << 8);
        short n4 = (short)(n3 + 1);
        HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array3, 0, 2048, n3, array2, n2, 32);
        i = 0;
        short n6;
        for (int j = 0; j < 1024; ++j, n4 = n6) {
            int n5 = i;
            n6 = n4;
            if (i + 46 > 1024) {
                n6 = (short)(n4 + 1);
                HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array3, 0, 2048, n4, array2, n2, 32);
                n5 = 0;
            }
            long n22;
            long n24;
            long n25;
            long n26;
            while (true) {
                i = n5 + 1;
                long load64 = CommonFunction.load64(array3, n5 * 64 / 32);
                long n7 = 64L;
                long n20;
                int n23;
                while (true) {
                    final int n8 = i + 1;
                    final long load65 = CommonFunction.load64(array3, i * 64 / 32);
                    int n9 = n8 + 1;
                    final long load66 = CommonFunction.load64(array3, n8 * 64 / 32);
                    long n10 = n7;
                    long load67 = load64;
                    if (n7 <= 58L) {
                        load67 = (load64 << 6 ^ (load65 >>> 58 & 0x3FL));
                        n10 = n7 + 6L;
                    }
                    final long n11 = load65 & 0x3FFFFFFFFFFFFFFL;
                    if (n11 <= 225463659665555712L) {
                        long n12 = 0L;
                        long[][] cumulative_DISTRIBUTION_TABLE_I;
                        long n13;
                        for (i = 0; i < 12; ++i) {
                            cumulative_DISTRIBUTION_TABLE_I = Sample.CUMULATIVE_DISTRIBUTION_TABLE_I;
                            n13 = load66 - cumulative_DISTRIBUTION_TABLE_I[i][1];
                            n12 += (n11 - (cumulative_DISTRIBUTION_TABLE_I[i][0] + ((n13 & cumulative_DISTRIBUTION_TABLE_I[i][1] & 0x1L) + (cumulative_DISTRIBUTION_TABLE_I[i][1] >> 1) + (n13 >>> 1) >>> 63)) >>> 63 & 0x1L);
                        }
                        long n19;
                        double n21;
                        while (true) {
                            long n14 = n10;
                            int n15 = n9;
                            if (n10 < 6L) {
                                load67 = CommonFunction.load64(array3, n9 * 64 / 32);
                                n15 = n9 + 1;
                                n14 = 64L;
                            }
                            final long n16 = load67 & 0x3FL;
                            long load68 = load67 >> 6;
                            final long n17 = n10 = n14 - 6L;
                            n9 = n15;
                            load67 = load68;
                            if (n16 != 63L) {
                                long n18 = n17;
                                i = n15;
                                if (n17 < 2L) {
                                    load68 = CommonFunction.load64(array3, n15 * 64 / 32);
                                    i = n15 + 1;
                                    n18 = 64L;
                                }
                                n19 = (modulus7(n16) << 2) + (load68 & 0x3L);
                                n20 = load68 >> 2;
                                n7 = n18 - 2L;
                                n21 = (double)n19;
                                n10 = n7;
                                n9 = i;
                                load67 = n20;
                                if (n21 < 10.0) {
                                    break;
                                }
                                continue;
                            }
                        }
                        n22 = (long)(n12 * 10.0 + n21);
                        n23 = i + 1;
                        final long load69 = CommonFunction.load64(array3, i * 64 / 32);
                        n24 = n22 << 1;
                        if (bernoulli(load69, n19 * (n24 - n19), Sample.EXPONENTIAL_DISTRIBUTION_P) != 0) {
                            break;
                        }
                        i = n23;
                        load64 = n20;
                    }
                    else {
                        i = n9;
                        n7 = n10;
                        load64 = load67;
                    }
                }
                n25 = 64L;
                long load70 = n20 << (int)(64L - n7);
                if (n7 == 0L) {
                    load70 = CommonFunction.load64(array3, n23 * 64 / 32);
                    i = n23 + 1;
                }
                else {
                    n25 = n7;
                    i = n23;
                }
                n26 = load70 << 1;
                if (((load70 >> 63 & 0x1L) | n22) != 0x0L) {
                    break;
                }
                n5 = i;
            }
            long load71;
            if (n25 - 1L == 0L) {
                final int n27 = i + 1;
                load71 = CommonFunction.load64(array3, i * 64 / 32);
                i = n27;
            }
            else {
                load71 = n26;
            }
            array[n + j] = (n24 & load71 >> 63) - n22 << 48 >> 48;
        }
    }
    
    private static long round(double n) {
        if (n < 0.0) {
            n -= 0.5;
        }
        else {
            n += 0.5;
        }
        return (long)n;
    }
    
    public static void sampleY(final int[] array, final byte[] array2, final int n, int i, final int n2, final int n3, final int n4, final int n5) {
        final int n6 = n5 + 1;
        final int n7 = (n6 + 7) / 8;
        final int n8 = n2 * n7;
        final byte[] array3 = new byte[n8];
        final int[] array4 = new int[4];
        short n10;
        final short n9 = n10 = (short)(i << 8);
        if (n3 == 4205569) {
            n10 = (short)(n9 + 1);
            HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array3, 0, n8, n9, array2, n, 32);
        }
        if (n3 == 4206593 || n3 == 8404993) {
            final short n11 = (short)(n10 + 1);
            HashUtils.customizableSecureHashAlgorithmKECCAK256Simple(array3, 0, n8, n10, array2, n, 32);
            n10 = n11;
        }
        int n12 = n2;
        int n13 = i = 0;
        while (i < n2) {
            int n17;
            if (n13 > n12 * n7 * 4) {
                int n14;
                if (n3 == 4205569) {
                    n14 = 168 / n7;
                    final short n15 = (short)(n10 + 1);
                    HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array3, 0, 168, n10, array2, n, 32);
                    n10 = n15;
                }
                else {
                    n14 = n12;
                }
                if (n3 == 4206593 || n3 == 8404993) {
                    n14 = 136 / n7;
                    final short n16 = (short)(n10 + 1);
                    HashUtils.customizableSecureHashAlgorithmKECCAK256Simple(array3, 0, 136, n10, array2, n, 32);
                    n10 = n16;
                }
                n17 = 0;
                n12 = n14;
            }
            else {
                n17 = n13;
            }
            final int load32 = CommonFunction.load32(array3, n17);
            final int n18 = (1 << n6) - 1;
            array4[0] = (load32 & n18) - n4;
            array4[1] = (CommonFunction.load32(array3, n17 + n7) & n18) - n4;
            array4[2] = (CommonFunction.load32(array3, n7 * 2 + n17) & n18) - n4;
            array4[3] = (CommonFunction.load32(array3, n7 * 3 + n17) & n18) - n4;
            int n19 = i;
            if (i < n2) {
                n19 = i;
                if (array4[0] != 1 << n5) {
                    array[i] = array4[0];
                    n19 = i + 1;
                }
            }
            if ((i = n19) < n2) {
                i = n19;
                if (array4[1] != 1 << n5) {
                    array[n19] = array4[1];
                    i = n19 + 1;
                }
            }
            int n20;
            if ((n20 = i) < n2) {
                n20 = i;
                if (array4[2] != 1 << n5) {
                    array[i] = array4[2];
                    n20 = i + 1;
                }
            }
            if ((i = n20) < n2) {
                i = n20;
                if (array4[3] != 1 << n5) {
                    array[n20] = array4[3];
                    i = n20 + 1;
                }
            }
            n13 = n17 + n7 * 4;
        }
    }
    
    public static void sampleY(final long[] array, final byte[] array2, final int n, int n2, final int n3, final int n4, final int n5, final int n6) {
        final int n7 = n6 + 1;
        final int n8 = (n7 + 7) / 8;
        final int n9 = n3 * n8;
        final byte[] array3 = new byte[n9];
        short n11;
        final short n10 = n11 = (short)(n2 << 8);
        if (n4 == 485978113) {
            n11 = (short)(n10 + 1);
            HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array3, 0, n9, n10, array2, n, 32);
        }
        if (n4 == 1129725953) {
            final short n12 = (short)(n11 + 1);
            HashUtils.customizableSecureHashAlgorithmKECCAK256Simple(array3, 0, n9, n11, array2, n, 32);
            n11 = n12;
        }
        n2 = n3;
        int i;
        int n18;
        for (int n13 = i = 0; i < n3; i = n18) {
            int n14 = n13;
            int n15 = n2;
            short n16 = n11;
            if (n13 > n2 * n8) {
                if (n4 == 485978113) {
                    n2 = 168 / n8;
                    final short n17 = (short)(n11 + 1);
                    HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array3, 0, 168, n11, array2, n, 32);
                    n11 = n17;
                }
                n16 = n11;
                if (n4 == 1129725953) {
                    n2 = 136 / n8;
                    n16 = (short)(n11 + 1);
                    HashUtils.customizableSecureHashAlgorithmKECCAK256Simple(array3, 0, 136, n11, array2, n, 32);
                }
                n14 = 0;
                n15 = n2;
            }
            array[i] = (CommonFunction.load32(array3, n14) & (1 << n7) - 1) - n5;
            n18 = i;
            if (array[i] != 1 << n6) {
                n18 = i + 1;
            }
            n13 = n14 + n8;
            n2 = n15;
            n11 = n16;
        }
    }
}
