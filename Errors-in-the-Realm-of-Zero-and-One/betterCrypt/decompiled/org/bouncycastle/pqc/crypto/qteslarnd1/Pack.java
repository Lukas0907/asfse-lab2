// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.qteslarnd1;

class Pack
{
    public static void decodePrivateKeyI(final byte[] array, final short[] array2, final short[] array3, final byte[] array4) {
        int n;
        for (int i = n = 0; i < 512; i += 4) {
            final byte b = array4[n + 0];
            final int n2 = i + 0;
            array2[n2] = (short)(b & 0xFF);
            final int n3 = n + 1;
            array2[n2] |= (short)((array4[n3] & 0xFF) << 30 >> 22);
            final byte b2 = array4[n3];
            final int n4 = i + 1;
            array2[n4] = (short)((b2 & 0xFF) >> 2);
            final int n5 = n + 2;
            array2[n4] |= (short)((array4[n5] & 0xFF) << 28 >> 22);
            final byte b3 = array4[n5];
            final int n6 = i + 2;
            array2[n6] = (short)((b3 & 0xFF) >> 4);
            final int n7 = n + 3;
            array2[n6] |= (short)((array4[n7] & 0xFF) << 26 >> 22);
            final byte b4 = array4[n7];
            final int n8 = i + 3;
            array2[n8] = (short)((b4 & 0xFF) >> 6);
            array2[n8] |= (short)(array4[n + 4] << 2);
            n += 5;
        }
        for (int j = 0; j < 512; j += 4) {
            final byte b5 = array4[n + 0];
            final int n9 = j + 0;
            array3[n9] = (short)(b5 & 0xFF);
            final int n10 = n + 1;
            array3[n9] |= (short)((array4[n10] & 0xFF) << 30 >> 22);
            final byte b6 = array4[n10];
            final int n11 = j + 1;
            array3[n11] = (short)((b6 & 0xFF) >> 2);
            final int n12 = n + 2;
            array3[n11] |= (short)((array4[n12] & 0xFF) << 28 >> 22);
            final byte b7 = array4[n12];
            final int n13 = j + 2;
            array3[n13] = (short)((b7 & 0xFF) >> 4);
            final int n14 = n + 3;
            array3[n13] |= (short)((array4[n14] & 0xFF) << 26 >> 22);
            final byte b8 = array4[n14];
            final int n15 = j + 3;
            array3[n15] = (short)((b8 & 0xFF) >> 6);
            array3[n15] |= (short)(array4[n + 4] << 2);
            n += 5;
        }
        System.arraycopy(array4, 1280, array, 0, 64);
    }
    
    public static void decodePrivateKeyIIISize(final byte[] array, final short[] array2, final short[] array3, final byte[] array4) {
        for (int i = 0; i < 1024; ++i) {
            array2[i] = array4[i];
        }
        for (int j = 0; j < 1024; ++j) {
            array3[j] = array4[j + 1024];
        }
        System.arraycopy(array4, 2048, array, 0, 64);
    }
    
    public static void decodePrivateKeyIIISpeed(final byte[] array, final short[] array2, final short[] array3, final byte[] array4) {
        int n;
        for (int i = n = 0; i < 1024; i += 8) {
            final byte b = array4[n + 0];
            final int n2 = i + 0;
            array2[n2] = (short)(b & 0xFF);
            final int n3 = n + 1;
            array2[n2] |= (short)((array4[n3] & 0xFF) << 31 >> 23);
            final byte b2 = array4[n3];
            final int n4 = i + 1;
            array2[n4] = (short)((b2 & 0xFF) >> 1);
            final int n5 = n + 2;
            array2[n4] |= (short)((array4[n5] & 0xFF) << 30 >> 23);
            final byte b3 = array4[n5];
            final int n6 = i + 2;
            array2[n6] = (short)((b3 & 0xFF) >> 2);
            final int n7 = n + 3;
            array2[n6] |= (short)((array4[n7] & 0xFF) << 29 >> 23);
            final byte b4 = array4[n7];
            final int n8 = i + 3;
            array2[n8] = (short)((b4 & 0xFF) >> 3);
            final int n9 = n + 4;
            array2[n8] |= (short)((array4[n9] & 0xFF) << 28 >> 23);
            final byte b5 = array4[n9];
            final int n10 = i + 4;
            array2[n10] = (short)((b5 & 0xFF) >> 4);
            final int n11 = n + 5;
            array2[n10] |= (short)((array4[n11] & 0xFF) << 27 >> 23);
            final byte b6 = array4[n11];
            final int n12 = i + 5;
            array2[n12] = (short)((b6 & 0xFF) >> 5);
            final int n13 = n + 6;
            array2[n12] |= (short)((array4[n13] & 0xFF) << 26 >> 23);
            final byte b7 = array4[n13];
            final int n14 = i + 6;
            array2[n14] = (short)((b7 & 0xFF) >> 6);
            final int n15 = n + 7;
            array2[n14] |= (short)((array4[n15] & 0xFF) << 25 >> 23);
            final byte b8 = array4[n15];
            final int n16 = i + 7;
            array2[n16] = (short)((b8 & 0xFF) >> 7);
            array2[n16] |= (short)(array4[n + 8] << 1);
            n += 9;
        }
        for (int j = 0; j < 1024; j += 8) {
            final byte b9 = array4[n + 0];
            final int n17 = j + 0;
            array3[n17] = (short)(b9 & 0xFF);
            final int n18 = n + 1;
            array3[n17] |= (short)((array4[n18] & 0xFF) << 31 >> 23);
            final byte b10 = array4[n18];
            final int n19 = j + 1;
            array3[n19] = (short)((b10 & 0xFF) >> 1);
            final int n20 = n + 2;
            array3[n19] |= (short)((array4[n20] & 0xFF) << 30 >> 23);
            final byte b11 = array4[n20];
            final int n21 = j + 2;
            array3[n21] = (short)((b11 & 0xFF) >> 2);
            final int n22 = n + 3;
            array3[n21] |= (short)((array4[n22] & 0xFF) << 29 >> 23);
            final byte b12 = array4[n22];
            final int n23 = j + 3;
            array3[n23] = (short)((b12 & 0xFF) >> 3);
            final int n24 = n + 4;
            array3[n23] |= (short)((array4[n24] & 0xFF) << 28 >> 23);
            final byte b13 = array4[n24];
            final int n25 = j + 4;
            array3[n25] = (short)((b13 & 0xFF) >> 4);
            final int n26 = n + 5;
            array3[n25] |= (short)((array4[n26] & 0xFF) << 27 >> 23);
            final byte b14 = array4[n26];
            final int n27 = j + 5;
            array3[n27] = (short)((b14 & 0xFF) >> 5);
            final int n28 = n + 6;
            array3[n27] |= (short)((array4[n28] & 0xFF) << 26 >> 23);
            final byte b15 = array4[n28];
            final int n29 = j + 6;
            array3[n29] = (short)((b15 & 0xFF) >> 6);
            final int n30 = n + 7;
            array3[n29] |= (short)((array4[n30] & 0xFF) << 25 >> 23);
            final byte b16 = array4[n30];
            final int n31 = j + 7;
            array3[n31] = (short)((b16 & 0xFF) >> 7);
            array3[n31] |= (short)(array4[n + 8] << 1);
            n += 9;
        }
        System.arraycopy(array4, 2304, array, 0, 64);
    }
    
    public static void decodePublicKey(final int[] array, final byte[] array2, final int n, final byte[] array3, final int n2, final int n3) {
        final int n4 = (1 << n3) - 1;
        int i = 0;
        int n5 = 0;
        while (i < n2) {
            final int n6 = (n5 + 0) * 4;
            array[i + 0] = (CommonFunction.load32(array3, n6) & n4);
            final int load32 = CommonFunction.load32(array3, n6);
            final int n7 = (n5 + 1) * 4;
            array[i + 1] = ((load32 >>> 23 | CommonFunction.load32(array3, n7) << 9) & n4);
            final int load33 = CommonFunction.load32(array3, n7);
            final int n8 = (n5 + 2) * 4;
            array[i + 2] = ((load33 >>> 14 | CommonFunction.load32(array3, n8) << 18) & n4);
            array[i + 3] = (CommonFunction.load32(array3, n8) >>> 5 & n4);
            final int load34 = CommonFunction.load32(array3, n8);
            final int n9 = (n5 + 3) * 4;
            array[i + 4] = ((load34 >>> 28 | CommonFunction.load32(array3, n9) << 4) & n4);
            final int load35 = CommonFunction.load32(array3, n9);
            final int n10 = (n5 + 4) * 4;
            array[i + 5] = ((load35 >>> 19 | CommonFunction.load32(array3, n10) << 13) & n4);
            final int load36 = CommonFunction.load32(array3, n10);
            final int n11 = (n5 + 5) * 4;
            array[i + 6] = ((load36 >>> 10 | CommonFunction.load32(array3, n11) << 22) & n4);
            array[i + 7] = (CommonFunction.load32(array3, n11) >>> 1 & n4);
            final int load37 = CommonFunction.load32(array3, n11);
            final int n12 = (n5 + 6) * 4;
            array[i + 8] = ((load37 >>> 24 | CommonFunction.load32(array3, n12) << 8) & n4);
            final int load38 = CommonFunction.load32(array3, n12);
            final int n13 = (n5 + 7) * 4;
            array[i + 9] = ((load38 >>> 15 | CommonFunction.load32(array3, n13) << 17) & n4);
            array[i + 10] = (CommonFunction.load32(array3, n13) >>> 6 & n4);
            final int load39 = CommonFunction.load32(array3, n13);
            final int n14 = (n5 + 8) * 4;
            array[i + 11] = ((load39 >>> 29 | CommonFunction.load32(array3, n14) << 3) & n4);
            final int load40 = CommonFunction.load32(array3, n14);
            final int n15 = (n5 + 9) * 4;
            array[i + 12] = ((load40 >>> 20 | CommonFunction.load32(array3, n15) << 12) & n4);
            final int load41 = CommonFunction.load32(array3, n15);
            final int n16 = (n5 + 10) * 4;
            array[i + 13] = ((load41 >>> 11 | CommonFunction.load32(array3, n16) << 21) & n4);
            array[i + 14] = (CommonFunction.load32(array3, n16) >>> 2 & n4);
            final int load42 = CommonFunction.load32(array3, n16);
            final int n17 = (n5 + 11) * 4;
            array[i + 15] = ((load42 >>> 25 | CommonFunction.load32(array3, n17) << 7) & n4);
            final int load43 = CommonFunction.load32(array3, n17);
            final int n18 = (n5 + 12) * 4;
            array[i + 16] = ((load43 >>> 16 | CommonFunction.load32(array3, n18) << 16) & n4);
            array[i + 17] = (CommonFunction.load32(array3, n18) >>> 7 & n4);
            final int load44 = CommonFunction.load32(array3, n18);
            final int n19 = (n5 + 13) * 4;
            array[i + 18] = ((load44 >>> 30 | CommonFunction.load32(array3, n19) << 2) & n4);
            final int load45 = CommonFunction.load32(array3, n19);
            final int n20 = (n5 + 14) * 4;
            array[i + 19] = ((load45 >>> 21 | CommonFunction.load32(array3, n20) << 11) & n4);
            final int load46 = CommonFunction.load32(array3, n20);
            final int n21 = (n5 + 15) * 4;
            array[i + 20] = ((load46 >>> 12 | CommonFunction.load32(array3, n21) << 20) & n4);
            array[i + 21] = (CommonFunction.load32(array3, n21) >>> 3 & n4);
            final int load47 = CommonFunction.load32(array3, n21);
            final int n22 = (n5 + 16) * 4;
            array[i + 22] = ((load47 >>> 26 | CommonFunction.load32(array3, n22) << 6) & n4);
            final int load48 = CommonFunction.load32(array3, n22);
            final int n23 = (n5 + 17) * 4;
            array[i + 23] = ((load48 >>> 17 | CommonFunction.load32(array3, n23) << 15) & n4);
            array[i + 24] = (CommonFunction.load32(array3, n23) >>> 8 & n4);
            final int load49 = CommonFunction.load32(array3, n23);
            final int n24 = (n5 + 18) * 4;
            array[i + 25] = ((load49 >>> 31 | CommonFunction.load32(array3, n24) << 1) & n4);
            final int load50 = CommonFunction.load32(array3, n24);
            final int n25 = (n5 + 19) * 4;
            array[i + 26] = ((load50 >>> 22 | CommonFunction.load32(array3, n25) << 10) & n4);
            final int load51 = CommonFunction.load32(array3, n25);
            final int n26 = (n5 + 20) * 4;
            array[i + 27] = ((load51 >>> 13 | CommonFunction.load32(array3, n26) << 19) & n4);
            array[i + 28] = (CommonFunction.load32(array3, n26) >>> 4 & n4);
            final int load52 = CommonFunction.load32(array3, n26);
            final int n27 = (n5 + 21) * 4;
            array[i + 29] = ((load52 >>> 27 | CommonFunction.load32(array3, n27) << 5) & n4);
            final int load53 = CommonFunction.load32(array3, n27);
            final int n28 = (n5 + 22) * 4;
            array[i + 30] = ((load53 >>> 18 | CommonFunction.load32(array3, n28) << 14) & n4);
            array[i + 31] = CommonFunction.load32(array3, n28) >>> 9;
            n5 += n3;
            i += 32;
        }
        System.arraycopy(array3, n2 * n3 / 8, array2, n, 32);
    }
    
    public static void decodePublicKeyIIIP(final int[] array, final byte[] array2, final int n, final byte[] array3) {
        int i = 0;
        int n2 = 0;
        while (i < 10240) {
            array[i] = (CommonFunction.load32(array3, n2 * 4) & Integer.MAX_VALUE);
            for (int j = 1; j < 31; ++j) {
                final int n3 = n2 + j;
                array[i + j] = ((CommonFunction.load32(array3, n3 * 4) << j | CommonFunction.load32(array3, (n3 - 1) * 4) >>> 32 - j) & Integer.MAX_VALUE);
            }
            n2 += 31;
            array[i + 31] = CommonFunction.load32(array3, (n2 - 1) * 4) >>> 1;
            i += 32;
        }
        System.arraycopy(array3, 39680, array2, n, 32);
    }
    
    public static void decodePublicKeyIIISpeed(final int[] array, final byte[] array2, final int n, final byte[] array3) {
        int i = 0;
        int n2 = 0;
        while (i < 1024) {
            final int n3 = (n2 + 0) * 4;
            array[i + 0] = (CommonFunction.load32(array3, n3) & 0xFFFFFF);
            final int load32 = CommonFunction.load32(array3, n3);
            final int n4 = (n2 + 1) * 4;
            array[i + 1] = ((load32 >>> 24 | CommonFunction.load32(array3, n4) << 8) & 0xFFFFFF);
            final int load33 = CommonFunction.load32(array3, n4);
            final int n5 = (n2 + 2) * 4;
            array[i + 2] = ((load33 >>> 16 | CommonFunction.load32(array3, n5) << 16) & 0xFFFFFF);
            array[i + 3] = CommonFunction.load32(array3, n5) >>> 8;
            n2 += 3;
            i += 4;
        }
        System.arraycopy(array3, 3072, array2, n, 32);
    }
    
    public static void decodePublicKeyIP(final int[] array, final byte[] array2, final int n, final byte[] array3) {
        int i = 0;
        int n2 = 0;
        while (i < 4096) {
            final int n3 = (n2 + 0) * 4;
            array[i + 0] = (CommonFunction.load32(array3, n3) & 0x1FFFFFFF);
            final int load32 = CommonFunction.load32(array3, n3);
            final int n4 = (n2 + 1) * 4;
            array[i + 1] = ((load32 >>> 29 | CommonFunction.load32(array3, n4) << 3) & 0x1FFFFFFF);
            final int load33 = CommonFunction.load32(array3, n4);
            final int n5 = (n2 + 2) * 4;
            array[i + 2] = ((load33 >>> 26 | CommonFunction.load32(array3, n5) << 6) & 0x1FFFFFFF);
            final int load34 = CommonFunction.load32(array3, n5);
            final int n6 = (n2 + 3) * 4;
            array[i + 3] = ((load34 >>> 23 | CommonFunction.load32(array3, n6) << 9) & 0x1FFFFFFF);
            final int load35 = CommonFunction.load32(array3, n6);
            final int n7 = (n2 + 4) * 4;
            array[i + 4] = ((load35 >>> 20 | CommonFunction.load32(array3, n7) << 12) & 0x1FFFFFFF);
            final int load36 = CommonFunction.load32(array3, n7);
            final int n8 = (n2 + 5) * 4;
            array[i + 5] = ((load36 >>> 17 | CommonFunction.load32(array3, n8) << 15) & 0x1FFFFFFF);
            final int load37 = CommonFunction.load32(array3, n8);
            final int n9 = (n2 + 6) * 4;
            array[i + 6] = ((load37 >>> 14 | CommonFunction.load32(array3, n9) << 18) & 0x1FFFFFFF);
            final int load38 = CommonFunction.load32(array3, n9);
            final int n10 = (n2 + 7) * 4;
            array[i + 7] = ((load38 >>> 11 | CommonFunction.load32(array3, n10) << 21) & 0x1FFFFFFF);
            final int load39 = CommonFunction.load32(array3, n10);
            final int n11 = (n2 + 8) * 4;
            array[i + 8] = ((load39 >>> 8 | CommonFunction.load32(array3, n11) << 24) & 0x1FFFFFFF);
            final int load40 = CommonFunction.load32(array3, n11);
            final int n12 = (n2 + 9) * 4;
            array[i + 9] = ((load40 >>> 5 | CommonFunction.load32(array3, n12) << 27) & 0x1FFFFFFF);
            array[i + 10] = (CommonFunction.load32(array3, n12) >>> 2 & 0x1FFFFFFF);
            final int load41 = CommonFunction.load32(array3, n12);
            final int n13 = (n2 + 10) * 4;
            array[i + 11] = ((load41 >>> 31 | CommonFunction.load32(array3, n13) << 1) & 0x1FFFFFFF);
            final int load42 = CommonFunction.load32(array3, n13);
            final int n14 = (n2 + 11) * 4;
            array[i + 12] = ((load42 >>> 28 | CommonFunction.load32(array3, n14) << 4) & 0x1FFFFFFF);
            final int load43 = CommonFunction.load32(array3, n14);
            final int n15 = (n2 + 12) * 4;
            array[i + 13] = ((load43 >>> 25 | CommonFunction.load32(array3, n15) << 7) & 0x1FFFFFFF);
            final int load44 = CommonFunction.load32(array3, n15);
            final int n16 = (n2 + 13) * 4;
            array[i + 14] = ((load44 >>> 22 | CommonFunction.load32(array3, n16) << 10) & 0x1FFFFFFF);
            final int load45 = CommonFunction.load32(array3, n16);
            final int n17 = (n2 + 14) * 4;
            array[i + 15] = ((load45 >>> 19 | CommonFunction.load32(array3, n17) << 13) & 0x1FFFFFFF);
            final int load46 = CommonFunction.load32(array3, n17);
            final int n18 = (n2 + 15) * 4;
            array[i + 16] = ((load46 >>> 16 | CommonFunction.load32(array3, n18) << 16) & 0x1FFFFFFF);
            final int load47 = CommonFunction.load32(array3, n18);
            final int n19 = (n2 + 16) * 4;
            array[i + 17] = ((load47 >>> 13 | CommonFunction.load32(array3, n19) << 19) & 0x1FFFFFFF);
            final int load48 = CommonFunction.load32(array3, n19);
            final int n20 = (n2 + 17) * 4;
            array[i + 18] = ((load48 >>> 10 | CommonFunction.load32(array3, n20) << 22) & 0x1FFFFFFF);
            final int load49 = CommonFunction.load32(array3, n20);
            final int n21 = (n2 + 18) * 4;
            array[i + 19] = ((load49 >>> 7 | CommonFunction.load32(array3, n21) << 25) & 0x1FFFFFFF);
            final int load50 = CommonFunction.load32(array3, n21);
            final int n22 = (n2 + 19) * 4;
            array[i + 20] = ((load50 >>> 4 | CommonFunction.load32(array3, n22) << 28) & 0x1FFFFFFF);
            array[i + 21] = (CommonFunction.load32(array3, n22) >>> 1 & 0x1FFFFFFF);
            final int load51 = CommonFunction.load32(array3, n22);
            final int n23 = (n2 + 20) * 4;
            array[i + 22] = ((load51 >>> 30 | CommonFunction.load32(array3, n23) << 2) & 0x1FFFFFFF);
            final int load52 = CommonFunction.load32(array3, n23);
            final int n24 = (n2 + 21) * 4;
            array[i + 23] = ((load52 >>> 27 | CommonFunction.load32(array3, n24) << 5) & 0x1FFFFFFF);
            final int load53 = CommonFunction.load32(array3, n24);
            final int n25 = (n2 + 22) * 4;
            array[i + 24] = ((load53 >>> 24 | CommonFunction.load32(array3, n25) << 8) & 0x1FFFFFFF);
            final int load54 = CommonFunction.load32(array3, n25);
            final int n26 = (n2 + 23) * 4;
            array[i + 25] = ((load54 >>> 21 | CommonFunction.load32(array3, n26) << 11) & 0x1FFFFFFF);
            final int load55 = CommonFunction.load32(array3, n26);
            final int n27 = (n2 + 24) * 4;
            array[i + 26] = ((load55 >>> 18 | CommonFunction.load32(array3, n27) << 14) & 0x1FFFFFFF);
            final int load56 = CommonFunction.load32(array3, n27);
            final int n28 = (n2 + 25) * 4;
            array[i + 27] = ((load56 >>> 15 | CommonFunction.load32(array3, n28) << 17) & 0x1FFFFFFF);
            final int load57 = CommonFunction.load32(array3, n28);
            final int n29 = (n2 + 26) * 4;
            array[i + 28] = ((load57 >>> 12 | CommonFunction.load32(array3, n29) << 20) & 0x1FFFFFFF);
            final int load58 = CommonFunction.load32(array3, n29);
            final int n30 = (n2 + 27) * 4;
            array[i + 29] = ((load58 >>> 9 | CommonFunction.load32(array3, n30) << 23) & 0x1FFFFFFF);
            final int load59 = CommonFunction.load32(array3, n30);
            final int n31 = (n2 + 28) * 4;
            array[i + 30] = ((load59 >>> 6 | CommonFunction.load32(array3, n31) << 26) & 0x1FFFFFFF);
            array[i + 31] = CommonFunction.load32(array3, n31) >>> 3;
            n2 += 29;
            i += 32;
        }
        System.arraycopy(array3, 14848, array2, n, 32);
    }
    
    public static void decodeSignature(final byte[] array, final int[] array2, final byte[] array3, final int n, final int n2, final int n3) {
        int n4;
        for (int i = n4 = 0; i < n2; i += 32) {
            final int n5 = (n4 + 0) * 4 + n;
            array2[i + 0] = CommonFunction.load32(array3, n5) << 11 >> 11;
            final int load32 = CommonFunction.load32(array3, n5);
            final int n6 = (n4 + 1) * 4 + n;
            array2[i + 1] = (load32 >>> 21 | CommonFunction.load32(array3, n6) << 22 >> 11);
            array2[i + 2] = CommonFunction.load32(array3, n6) << 1 >> 11;
            final int load33 = CommonFunction.load32(array3, n6);
            final int n7 = (n4 + 2) * 4 + n;
            array2[i + 3] = (load33 >>> 31 | CommonFunction.load32(array3, n7) << 12 >> 11);
            final int load34 = CommonFunction.load32(array3, n7);
            final int n8 = (n4 + 3) * 4 + n;
            array2[i + 4] = (load34 >>> 20 | CommonFunction.load32(array3, n8) << 23 >> 11);
            array2[i + 5] = CommonFunction.load32(array3, n8) << 2 >> 11;
            final int load35 = CommonFunction.load32(array3, n8);
            final int n9 = (n4 + 4) * 4 + n;
            array2[i + 6] = (load35 >>> 30 | CommonFunction.load32(array3, n9) << 13 >> 11);
            final int load36 = CommonFunction.load32(array3, n9);
            final int n10 = (n4 + 5) * 4 + n;
            array2[i + 7] = (load36 >>> 19 | CommonFunction.load32(array3, n10) << 24 >> 11);
            array2[i + 8] = CommonFunction.load32(array3, n10) << 3 >> 11;
            final int load37 = CommonFunction.load32(array3, n10);
            final int n11 = (n4 + 6) * 4 + n;
            array2[i + 9] = (load37 >>> 29 | CommonFunction.load32(array3, n11) << 14 >> 11);
            final int load38 = CommonFunction.load32(array3, n11);
            final int n12 = (n4 + 7) * 4 + n;
            array2[i + 10] = (load38 >>> 18 | CommonFunction.load32(array3, n12) << 25 >> 11);
            array2[i + 11] = CommonFunction.load32(array3, n12) << 4 >> 11;
            final int load39 = CommonFunction.load32(array3, n12);
            final int n13 = (n4 + 8) * 4 + n;
            array2[i + 12] = (load39 >>> 28 | CommonFunction.load32(array3, n13) << 15 >> 11);
            final int load40 = CommonFunction.load32(array3, n13);
            final int n14 = (n4 + 9) * 4 + n;
            array2[i + 13] = (load40 >>> 17 | CommonFunction.load32(array3, n14) << 26 >> 11);
            array2[i + 14] = CommonFunction.load32(array3, n14) << 5 >> 11;
            final int load41 = CommonFunction.load32(array3, n14);
            final int n15 = (n4 + 10) * 4 + n;
            array2[i + 15] = (load41 >>> 27 | CommonFunction.load32(array3, n15) << 16 >> 11);
            final int load42 = CommonFunction.load32(array3, n15);
            final int n16 = (n4 + 11) * 4 + n;
            array2[i + 16] = (load42 >>> 16 | CommonFunction.load32(array3, n16) << 27 >> 11);
            array2[i + 17] = CommonFunction.load32(array3, n16) << 6 >> 11;
            final int load43 = CommonFunction.load32(array3, n16);
            final int n17 = (n4 + 12) * 4 + n;
            array2[i + 18] = (load43 >>> 26 | CommonFunction.load32(array3, n17) << 17 >> 11);
            final int load44 = CommonFunction.load32(array3, n17);
            final int n18 = (n4 + 13) * 4 + n;
            array2[i + 19] = (load44 >>> 15 | CommonFunction.load32(array3, n18) << 28 >> 11);
            array2[i + 20] = CommonFunction.load32(array3, n18) << 7 >> 11;
            final int load45 = CommonFunction.load32(array3, n18);
            final int n19 = (n4 + 14) * 4 + n;
            array2[i + 21] = (load45 >>> 25 | CommonFunction.load32(array3, n19) << 18 >> 11);
            final int load46 = CommonFunction.load32(array3, n19);
            final int n20 = (n4 + 15) * 4 + n;
            array2[i + 22] = (load46 >>> 14 | CommonFunction.load32(array3, n20) << 29 >> 11);
            array2[i + 23] = CommonFunction.load32(array3, n20) << 8 >> 11;
            final int load47 = CommonFunction.load32(array3, n20);
            final int n21 = (n4 + 16) * 4 + n;
            array2[i + 24] = (load47 >>> 24 | CommonFunction.load32(array3, n21) << 19 >> 11);
            final int load48 = CommonFunction.load32(array3, n21);
            final int n22 = (n4 + 17) * 4 + n;
            array2[i + 25] = (load48 >>> 13 | CommonFunction.load32(array3, n22) << 30 >> 11);
            array2[i + 26] = CommonFunction.load32(array3, n22) << 9 >> 11;
            final int load49 = CommonFunction.load32(array3, n22);
            final int n23 = (n4 + 18) * 4 + n;
            array2[i + 27] = (load49 >>> 23 | CommonFunction.load32(array3, n23) << 20 >> 11);
            final int load50 = CommonFunction.load32(array3, n23);
            final int n24 = (n4 + 19) * 4 + n;
            array2[i + 28] = (load50 >>> 12 | CommonFunction.load32(array3, n24) << 31 >> 11);
            array2[i + 29] = CommonFunction.load32(array3, n24) << 10 >> 11;
            final int load51 = CommonFunction.load32(array3, n24);
            final int n25 = (n4 + 20) * 4 + n;
            array2[i + 30] = (load51 >>> 22 | CommonFunction.load32(array3, n25) << 21 >> 11);
            array2[i + 31] = CommonFunction.load32(array3, n25) >> 11;
            n4 += n3;
        }
        System.arraycopy(array3, n + n2 * n3 / 8, array, 0, 32);
    }
    
    public static void decodeSignatureIIIP(final byte[] array, final long[] array2, final byte[] array3, final int n) {
        int n2;
        for (int i = n2 = 0; i < 2048; i += 4) {
            final int n3 = (n2 + 0) * 4 + n;
            array2[i + 0] = CommonFunction.load32(array3, n3) << 8 >> 8;
            final int load32 = CommonFunction.load32(array3, n3);
            final int n4 = (n2 + 1) * 4 + n;
            array2[i + 1] = ((load32 >>> 24 & 0xFF) | CommonFunction.load32(array3, n4) << 16 >> 8);
            final int load33 = CommonFunction.load32(array3, n4);
            final int n5 = (n2 + 2) * 4 + n;
            array2[i + 2] = ((load33 >>> 16 & 0xFFFF) | CommonFunction.load32(array3, n5) << 24 >> 8);
            array2[i + 3] = CommonFunction.load32(array3, n5) >> 8;
            n2 += 3;
        }
        System.arraycopy(array3, n + 6144, array, 0, 32);
    }
    
    public static void decodeSignatureIIISpeed(final byte[] array, final int[] array2, final byte[] array3, final int n) {
        int n2;
        for (int i = n2 = 0; i < 1024; i += 16) {
            final int n3 = (n2 + 0) * 4 + n;
            array2[i + 0] = CommonFunction.load32(array3, n3) << 10 >> 10;
            final int load32 = CommonFunction.load32(array3, n3);
            final int n4 = (n2 + 1) * 4 + n;
            array2[i + 1] = (load32 >>> 22 | CommonFunction.load32(array3, n4) << 20 >> 10);
            final int load33 = CommonFunction.load32(array3, n4);
            final int n5 = (n2 + 2) * 4 + n;
            array2[i + 2] = (load33 >>> 12 | CommonFunction.load32(array3, n5) << 30 >> 10);
            array2[i + 3] = CommonFunction.load32(array3, n5) << 8 >> 10;
            final int load34 = CommonFunction.load32(array3, n5);
            final int n6 = (n2 + 3) * 4 + n;
            array2[i + 4] = (load34 >>> 24 | CommonFunction.load32(array3, n6) << 18 >> 10);
            final int load35 = CommonFunction.load32(array3, n6);
            final int n7 = (n2 + 4) * 4 + n;
            array2[i + 5] = (load35 >>> 14 | CommonFunction.load32(array3, n7) << 28 >> 10);
            array2[i + 6] = CommonFunction.load32(array3, n7) << 6 >> 10;
            final int load36 = CommonFunction.load32(array3, n7);
            final int n8 = (n2 + 5) * 4 + n;
            array2[i + 7] = (load36 >>> 26 | CommonFunction.load32(array3, n8) << 16 >> 10);
            final int load37 = CommonFunction.load32(array3, n8);
            final int n9 = (n2 + 6) * 4 + n;
            array2[i + 8] = (load37 >>> 16 | CommonFunction.load32(array3, n9) << 26 >> 10);
            array2[i + 9] = CommonFunction.load32(array3, n9) << 4 >> 10;
            final int load38 = CommonFunction.load32(array3, n9);
            final int n10 = (n2 + 7) * 4 + n;
            array2[i + 10] = (load38 >>> 28 | CommonFunction.load32(array3, n10) << 14 >> 10);
            final int load39 = CommonFunction.load32(array3, n10);
            final int n11 = (n2 + 8) * 4 + n;
            array2[i + 11] = (load39 >>> 18 | CommonFunction.load32(array3, n11) << 24 >> 10);
            array2[i + 12] = CommonFunction.load32(array3, n11) << 2 >> 10;
            final int load40 = CommonFunction.load32(array3, n11);
            final int n12 = (n2 + 9) * 4 + n;
            array2[i + 13] = (load40 >>> 30 | CommonFunction.load32(array3, n12) << 12 >> 10);
            final int load41 = CommonFunction.load32(array3, n12);
            final int n13 = (n2 + 10) * 4 + n;
            array2[i + 14] = (load41 >>> 20 | CommonFunction.load32(array3, n13) << 22 >> 10);
            array2[i + 15] = CommonFunction.load32(array3, n13) >> 10;
            n2 += 11;
        }
        System.arraycopy(array3, n + 2816, array, 0, 32);
    }
    
    public static void decodeSignatureIP(final byte[] array, final long[] array2, final byte[] array3, final int n) {
        int n2;
        for (int i = n2 = 0; i < 1024; i += 16) {
            final int n3 = (n2 + 0) * 4 + n;
            array2[i + 0] = CommonFunction.load32(array3, n3) << 10 >> 10;
            final int load32 = CommonFunction.load32(array3, n3);
            final int n4 = (n2 + 1) * 4 + n;
            array2[i + 1] = (load32 >>> 22 | CommonFunction.load32(array3, n4) << 20 >> 10);
            final int load33 = CommonFunction.load32(array3, n4);
            final int n5 = (n2 + 2) * 4 + n;
            array2[i + 2] = (load33 >>> 12 | CommonFunction.load32(array3, n5) << 30 >> 10);
            array2[i + 3] = CommonFunction.load32(array3, n5) << 8 >> 10;
            final int load34 = CommonFunction.load32(array3, n5);
            final int n6 = (n2 + 3) * 4 + n;
            array2[i + 4] = (load34 >>> 24 | CommonFunction.load32(array3, n6) << 18 >> 10);
            final int load35 = CommonFunction.load32(array3, n6);
            final int n7 = (n2 + 4) * 4 + n;
            array2[i + 5] = (load35 >>> 14 | CommonFunction.load32(array3, n7) << 28 >> 10);
            array2[i + 6] = CommonFunction.load32(array3, n7) << 6 >> 10;
            final int load36 = CommonFunction.load32(array3, n7);
            final int n8 = (n2 + 5) * 4 + n;
            array2[i + 7] = (load36 >>> 26 | CommonFunction.load32(array3, n8) << 16 >> 10);
            final int load37 = CommonFunction.load32(array3, n8);
            final int n9 = (n2 + 6) * 4 + n;
            array2[i + 8] = (load37 >>> 16 | CommonFunction.load32(array3, n9) << 26 >> 10);
            array2[i + 9] = CommonFunction.load32(array3, n9) << 4 >> 10;
            final int load38 = CommonFunction.load32(array3, n9);
            final int n10 = (n2 + 7) * 4 + n;
            array2[i + 10] = (load38 >>> 28 | CommonFunction.load32(array3, n10) << 14 >> 10);
            final int load39 = CommonFunction.load32(array3, n10);
            final int n11 = (n2 + 8) * 4 + n;
            array2[i + 11] = (load39 >>> 18 | CommonFunction.load32(array3, n11) << 24 >> 10);
            array2[i + 12] = CommonFunction.load32(array3, n11) << 2 >> 10;
            final int load40 = CommonFunction.load32(array3, n11);
            final int n12 = (n2 + 9) * 4 + n;
            array2[i + 13] = (load40 >>> 30 | CommonFunction.load32(array3, n12) << 12 >> 10);
            final int load41 = CommonFunction.load32(array3, n12);
            final int n13 = (n2 + 10) * 4 + n;
            array2[i + 14] = (load41 >>> 20 | CommonFunction.load32(array3, n13) << 22 >> 10);
            array2[i + 15] = CommonFunction.load32(array3, n13) >> 10;
            n2 += 11;
        }
        System.arraycopy(array3, n + 2816, array, 0, 32);
    }
    
    public static void encodePrivateKeyI(final byte[] array, final int[] array2, final int[] array3, final byte[] array4, final int n) {
        final int n2 = 0;
        int n4;
        int n3 = n4 = 0;
        int i;
        int n5;
        while (true) {
            i = n2;
            n5 = n4;
            if (n3 >= 512) {
                break;
            }
            final int n6 = n3 + 0;
            array[n4 + 0] = (byte)array2[n6];
            final int n7 = array2[n6];
            final int n8 = n3 + 1;
            array[n4 + 1] = (byte)((n7 >> 8 & 0x3) | array2[n8] << 2);
            final int n9 = array2[n8];
            final int n10 = n3 + 2;
            array[n4 + 2] = (byte)((n9 >> 6 & 0xF) | array2[n10] << 4);
            final int n11 = array2[n10];
            final int n12 = n3 + 3;
            array[n4 + 3] = (byte)((n11 >> 4 & 0x3F) | array2[n12] << 6);
            array[n4 + 4] = (byte)(array2[n12] >> 2);
            n4 += 5;
            n3 += 4;
        }
        while (i < 512) {
            final int n13 = i + 0;
            array[n5 + 0] = (byte)array3[n13];
            final int n14 = array3[n13];
            final int n15 = i + 1;
            array[n5 + 1] = (byte)((n14 >> 8 & 0x3) | array3[n15] << 2);
            final int n16 = array3[n15];
            final int n17 = i + 2;
            array[n5 + 2] = (byte)((n16 >> 6 & 0xF) | array3[n17] << 4);
            final int n18 = array3[n17];
            final int n19 = i + 3;
            array[n5 + 3] = (byte)((n18 >> 4 & 0x3F) | array3[n19] << 6);
            array[n5 + 4] = (byte)(array3[n19] >> 2);
            n5 += 5;
            i += 4;
        }
        System.arraycopy(array4, n, array, 1280, 64);
    }
    
    public static void encodePrivateKeyIIISize(final byte[] array, final int[] array2, final int[] array3, final byte[] array4, final int n) {
        final int n2 = 0;
        int n3 = 0;
        int i;
        while (true) {
            i = n2;
            if (n3 >= 1024) {
                break;
            }
            array[n3] = (byte)array2[n3];
            ++n3;
        }
        while (i < 1024) {
            array[i + 1024] = (byte)array3[i];
            ++i;
        }
        System.arraycopy(array4, n, array, 2048, 64);
    }
    
    public static void encodePrivateKeyIIISpeed(final byte[] array, final int[] array2, final int[] array3, final byte[] array4, final int n) {
        final int n2 = 0;
        int n4;
        int n3 = n4 = 0;
        int i;
        int n5;
        while (true) {
            i = n2;
            n5 = n4;
            if (n3 >= 1024) {
                break;
            }
            final int n6 = n3 + 0;
            array[n4 + 0] = (byte)array2[n6];
            final int n7 = array2[n6];
            final int n8 = n3 + 1;
            array[n4 + 1] = (byte)((n7 >> 8 & 0x1) | array2[n8] << 1);
            final int n9 = array2[n8];
            final int n10 = n3 + 2;
            array[n4 + 2] = (byte)((n9 >> 7 & 0x3) | array2[n10] << 2);
            final int n11 = array2[n10];
            final int n12 = n3 + 3;
            array[n4 + 3] = (byte)((n11 >> 6 & 0x7) | array2[n12] << 3);
            final int n13 = array2[n12];
            final int n14 = n3 + 4;
            array[n4 + 4] = (byte)((n13 >> 5 & 0xF) | array2[n14] << 4);
            final int n15 = array2[n14];
            final int n16 = n3 + 5;
            array[n4 + 5] = (byte)((n15 >> 4 & 0x1F) | array2[n16] << 5);
            final int n17 = array2[n16];
            final int n18 = n3 + 6;
            array[n4 + 6] = (byte)((n17 >> 3 & 0x3F) | array2[n18] << 6);
            final int n19 = array2[n18];
            final int n20 = n3 + 7;
            array[n4 + 7] = (byte)((n19 >> 2 & 0x7F) | array2[n20] << 7);
            array[n4 + 8] = (byte)(array2[n20] >> 1);
            n4 += 9;
            n3 += 8;
        }
        while (i < 1024) {
            final int n21 = i + 0;
            array[n5 + 0] = (byte)array3[n21];
            final int n22 = array3[n21];
            final int n23 = i + 1;
            array[n5 + 1] = (byte)((n22 >> 8 & 0x1) | array3[n23] << 1);
            final int n24 = array3[n23];
            final int n25 = i + 2;
            array[n5 + 2] = (byte)((n24 >> 7 & 0x3) | array3[n25] << 2);
            final int n26 = array3[n25];
            final int n27 = i + 3;
            array[n5 + 3] = (byte)((n26 >> 6 & 0x7) | array3[n27] << 3);
            final int n28 = array3[n27];
            final int n29 = i + 4;
            array[n5 + 4] = (byte)((n28 >> 5 & 0xF) | array3[n29] << 4);
            final int n30 = array3[n29];
            final int n31 = i + 5;
            array[n5 + 5] = (byte)((n30 >> 4 & 0x1F) | array3[n31] << 5);
            final int n32 = array3[n31];
            final int n33 = i + 6;
            array[n5 + 6] = (byte)((n32 >> 3 & 0x3F) | array3[n33] << 6);
            final int n34 = array3[n33];
            final int n35 = i + 7;
            array[n5 + 7] = (byte)((n34 >> 2 & 0x7F) | array3[n35] << 7);
            array[n5 + 8] = (byte)(array3[n35] >> 1);
            n5 += 9;
            i += 8;
        }
        System.arraycopy(array4, n, array, 2304, 64);
    }
    
    public static void encodePublicKey(final byte[] array, final int[] array2, final byte[] array3, final int n, final int n2, final int n3) {
        int n4 = 0;
        int n5 = 0;
        int n6;
        while (true) {
            n6 = n2 * n3;
            if (n4 >= n6 / 32) {
                break;
            }
            final int n7 = array2[n5 + 0];
            final int n8 = n5 + 1;
            CommonFunction.store32(array, (n4 + 0) * 4, n7 | array2[n8] << 23);
            final int n9 = array2[n8];
            final int n10 = n5 + 2;
            CommonFunction.store32(array, (n4 + 1) * 4, n9 >> 9 | array2[n10] << 14);
            final int n11 = array2[n10];
            final int n12 = array2[n5 + 3];
            final int n13 = n5 + 4;
            CommonFunction.store32(array, (n4 + 2) * 4, n11 >> 18 | n12 << 5 | array2[n13] << 28);
            final int n14 = array2[n13];
            final int n15 = n5 + 5;
            CommonFunction.store32(array, (n4 + 3) * 4, n14 >> 4 | array2[n15] << 19);
            final int n16 = array2[n15];
            final int n17 = n5 + 6;
            CommonFunction.store32(array, (n4 + 4) * 4, n16 >> 13 | array2[n17] << 10);
            final int n18 = array2[n17];
            final int n19 = array2[n5 + 7];
            final int n20 = n5 + 8;
            CommonFunction.store32(array, (n4 + 5) * 4, n18 >> 22 | n19 << 1 | array2[n20] << 24);
            final int n21 = array2[n20];
            final int n22 = n5 + 9;
            CommonFunction.store32(array, (n4 + 6) * 4, n21 >> 8 | array2[n22] << 15);
            final int n23 = array2[n22];
            final int n24 = array2[n5 + 10];
            final int n25 = n5 + 11;
            CommonFunction.store32(array, (n4 + 7) * 4, n23 >> 17 | n24 << 6 | array2[n25] << 29);
            final int n26 = array2[n25];
            final int n27 = n5 + 12;
            CommonFunction.store32(array, (n4 + 8) * 4, n26 >> 3 | array2[n27] << 20);
            final int n28 = array2[n27];
            final int n29 = n5 + 13;
            CommonFunction.store32(array, (n4 + 9) * 4, n28 >> 12 | array2[n29] << 11);
            final int n30 = array2[n29];
            final int n31 = array2[n5 + 14];
            final int n32 = n5 + 15;
            CommonFunction.store32(array, (n4 + 10) * 4, n30 >> 21 | n31 << 2 | array2[n32] << 25);
            final int n33 = array2[n32];
            final int n34 = n5 + 16;
            CommonFunction.store32(array, (n4 + 11) * 4, n33 >> 7 | array2[n34] << 16);
            final int n35 = array2[n34];
            final int n36 = array2[n5 + 17];
            final int n37 = n5 + 18;
            CommonFunction.store32(array, (n4 + 12) * 4, n35 >> 16 | n36 << 7 | array2[n37] << 30);
            final int n38 = array2[n37];
            final int n39 = n5 + 19;
            CommonFunction.store32(array, (n4 + 13) * 4, n38 >> 2 | array2[n39] << 21);
            final int n40 = array2[n39];
            final int n41 = n5 + 20;
            CommonFunction.store32(array, (n4 + 14) * 4, n40 >> 11 | array2[n41] << 12);
            final int n42 = array2[n41];
            final int n43 = array2[n5 + 21];
            final int n44 = n5 + 22;
            CommonFunction.store32(array, (n4 + 15) * 4, n42 >> 20 | n43 << 3 | array2[n44] << 26);
            final int n45 = array2[n44];
            final int n46 = n5 + 23;
            CommonFunction.store32(array, (n4 + 16) * 4, n45 >> 6 | array2[n46] << 17);
            final int n47 = array2[n46];
            final int n48 = array2[n5 + 24];
            final int n49 = n5 + 25;
            CommonFunction.store32(array, (n4 + 17) * 4, n47 >> 15 | n48 << 8 | array2[n49] << 31);
            final int n50 = array2[n49];
            final int n51 = n5 + 26;
            CommonFunction.store32(array, (n4 + 18) * 4, n50 >> 1 | array2[n51] << 22);
            final int n52 = array2[n51];
            final int n53 = n5 + 27;
            CommonFunction.store32(array, (n4 + 19) * 4, n52 >> 10 | array2[n53] << 13);
            final int n54 = array2[n53];
            final int n55 = array2[n5 + 28];
            final int n56 = n5 + 29;
            CommonFunction.store32(array, (n4 + 20) * 4, n54 >> 19 | n55 << 4 | array2[n56] << 27);
            final int n57 = array2[n56];
            final int n58 = n5 + 30;
            CommonFunction.store32(array, (n4 + 21) * 4, n57 >> 5 | array2[n58] << 18);
            CommonFunction.store32(array, (n4 + 22) * 4, array2[n58] >> 14 | array2[n5 + 31] << 9);
            n5 += 32;
            n4 += n3;
        }
        System.arraycopy(array3, n, array, n6 / 8, 32);
    }
    
    public static void encodePublicKeyIIIP(final byte[] array, final long[] array2, final byte[] array3, final int n) {
        int n2;
        for (int i = n2 = 0; i < 9920; i += 31) {
            for (int j = 0; j < 31; ++j) {
                final int n3 = n2 + j;
                CommonFunction.store32(array, (i + j) * 4, (int)(array2[n3] >> j | array2[n3 + 1] << 31 - j));
            }
            n2 += 32;
        }
        System.arraycopy(array3, n, array, 39680, 32);
    }
    
    public static void encodePublicKeyIIISpeed(final byte[] array, final int[] array2, final byte[] array3, final int n) {
        int i = 0;
        int n2 = 0;
        while (i < 768) {
            final int n3 = array2[n2 + 0];
            final int n4 = n2 + 1;
            CommonFunction.store32(array, (i + 0) * 4, n3 | array2[n4] << 24);
            final int n5 = array2[n4];
            final int n6 = n2 + 2;
            CommonFunction.store32(array, (i + 1) * 4, n5 >> 8 | array2[n6] << 16);
            CommonFunction.store32(array, (i + 2) * 4, array2[n6] >> 16 | array2[n2 + 3] << 8);
            n2 += 4;
            i += 3;
        }
        System.arraycopy(array3, n, array, 3072, 32);
    }
    
    public static void encodePublicKeyIP(final byte[] array, final long[] array2, final byte[] array3, final int n) {
        int i = 0;
        int n2 = 0;
        while (i < 3712) {
            final long n3 = array2[n2 + 0];
            final int n4 = n2 + 1;
            CommonFunction.store32(array, (i + 0) * 4, (int)(n3 | array2[n4] << 29));
            final long n5 = array2[n4];
            final int n6 = n2 + 2;
            CommonFunction.store32(array, (i + 1) * 4, (int)(n5 >> 3 | array2[n6] << 26));
            final long n7 = array2[n6];
            final int n8 = n2 + 3;
            CommonFunction.store32(array, (i + 2) * 4, (int)(n7 >> 6 | array2[n8] << 23));
            final long n9 = array2[n8];
            final int n10 = n2 + 4;
            CommonFunction.store32(array, (i + 3) * 4, (int)(n9 >> 9 | array2[n10] << 20));
            final long n11 = array2[n10];
            final int n12 = n2 + 5;
            CommonFunction.store32(array, (i + 4) * 4, (int)(n11 >> 12 | array2[n12] << 17));
            final long n13 = array2[n12];
            final int n14 = n2 + 6;
            CommonFunction.store32(array, (i + 5) * 4, (int)(n13 >> 15 | array2[n14] << 14));
            final long n15 = array2[n14];
            final int n16 = n2 + 7;
            CommonFunction.store32(array, (i + 6) * 4, (int)(n15 >> 18 | array2[n16] << 11));
            final long n17 = array2[n16];
            final int n18 = n2 + 8;
            CommonFunction.store32(array, (i + 7) * 4, (int)(n17 >> 21 | array2[n18] << 8));
            final long n19 = array2[n18];
            final int n20 = n2 + 9;
            CommonFunction.store32(array, (i + 8) * 4, (int)(n19 >> 24 | array2[n20] << 5));
            final long n21 = array2[n20];
            final long n22 = array2[n2 + 10];
            final int n23 = n2 + 11;
            CommonFunction.store32(array, (i + 9) * 4, (int)(n21 >> 27 | n22 << 2 | array2[n23] << 31));
            final long n24 = array2[n23];
            final int n25 = n2 + 12;
            CommonFunction.store32(array, (i + 10) * 4, (int)(n24 >> 1 | array2[n25] << 28));
            final long n26 = array2[n25];
            final int n27 = n2 + 13;
            CommonFunction.store32(array, (i + 11) * 4, (int)(n26 >> 4 | array2[n27] << 25));
            final long n28 = array2[n27];
            final int n29 = n2 + 14;
            CommonFunction.store32(array, (i + 12) * 4, (int)(n28 >> 7 | array2[n29] << 22));
            final long n30 = array2[n29];
            final int n31 = n2 + 15;
            CommonFunction.store32(array, (i + 13) * 4, (int)(n30 >> 10 | array2[n31] << 19));
            final long n32 = array2[n31];
            final int n33 = n2 + 16;
            CommonFunction.store32(array, (i + 14) * 4, (int)(n32 >> 13 | array2[n33] << 16));
            final long n34 = array2[n33];
            final int n35 = n2 + 17;
            CommonFunction.store32(array, (i + 15) * 4, (int)(n34 >> 16 | array2[n35] << 13));
            final long n36 = array2[n35];
            final int n37 = n2 + 18;
            CommonFunction.store32(array, (i + 16) * 4, (int)(n36 >> 19 | array2[n37] << 10));
            final long n38 = array2[n37];
            final int n39 = n2 + 19;
            CommonFunction.store32(array, (i + 17) * 4, (int)(n38 >> 22 | array2[n39] << 7));
            final long n40 = array2[n39];
            final int n41 = n2 + 20;
            CommonFunction.store32(array, (i + 18) * 4, (int)(n40 >> 25 | array2[n41] << 4));
            final long n42 = array2[n41];
            final long n43 = array2[n2 + 21];
            final int n44 = n2 + 22;
            CommonFunction.store32(array, (i + 19) * 4, (int)(n42 >> 28 | n43 << 1 | array2[n44] << 30));
            final long n45 = array2[n44];
            final int n46 = n2 + 23;
            CommonFunction.store32(array, (i + 20) * 4, (int)(n45 >> 2 | array2[n46] << 27));
            final long n47 = array2[n46];
            final int n48 = n2 + 24;
            CommonFunction.store32(array, (i + 21) * 4, (int)(n47 >> 5 | array2[n48] << 24));
            final long n49 = array2[n48];
            final int n50 = n2 + 25;
            CommonFunction.store32(array, (i + 22) * 4, (int)(n49 >> 8 | array2[n50] << 21));
            final long n51 = array2[n50];
            final int n52 = n2 + 26;
            CommonFunction.store32(array, (i + 23) * 4, (int)(n51 >> 11 | array2[n52] << 18));
            final long n53 = array2[n52];
            final int n54 = n2 + 27;
            CommonFunction.store32(array, (i + 24) * 4, (int)(n53 >> 14 | array2[n54] << 15));
            final long n55 = array2[n54];
            final int n56 = n2 + 28;
            CommonFunction.store32(array, (i + 25) * 4, (int)(n55 >> 17 | array2[n56] << 12));
            final long n57 = array2[n56];
            final int n58 = n2 + 29;
            CommonFunction.store32(array, (i + 26) * 4, (int)(n57 >> 20 | array2[n58] << 9));
            final long n59 = array2[n58];
            final int n60 = n2 + 30;
            CommonFunction.store32(array, (i + 27) * 4, (int)(n59 >> 23 | array2[n60] << 6));
            CommonFunction.store32(array, (i + 28) * 4, (int)(array2[n60] >> 26 | array2[n2 + 31] << 3));
            n2 += 32;
            i += 29;
        }
        System.arraycopy(array3, n, array, 14848, 32);
    }
    
    public static void encodeSignature(final byte[] array, final int n, final byte[] array2, final int n2, final int[] array3, final int n3, final int n4) {
        int n5 = 0;
        int n6 = 0;
        int n7;
        while (true) {
            n7 = n3 * n4;
            if (n5 >= n7 / 32) {
                break;
            }
            final int n8 = array3[n6 + 0];
            final int n9 = n6 + 1;
            CommonFunction.store32(array, (n5 + 0) * 4 + n, (n8 & 0x1FFFFF) | array3[n9] << 21);
            final int n10 = array3[n9];
            final int n11 = array3[n6 + 2];
            final int n12 = n6 + 3;
            CommonFunction.store32(array, (n5 + 1) * 4 + n, (n10 >>> 11 & 0x3FF) | (n11 & 0x1FFFFF) << 10 | array3[n12] << 31);
            final int n13 = array3[n12];
            final int n14 = n6 + 4;
            CommonFunction.store32(array, (n5 + 2) * 4 + n, (n13 >>> 1 & 0xFFFFF) | array3[n14] << 20);
            final int n15 = array3[n14];
            final int n16 = array3[n6 + 5];
            final int n17 = n6 + 6;
            CommonFunction.store32(array, (n5 + 3) * 4 + n, (n15 >>> 12 & 0x1FF) | (n16 & 0x1FFFFF) << 9 | array3[n17] << 30);
            final int n18 = array3[n17];
            final int n19 = n6 + 7;
            CommonFunction.store32(array, (n5 + 4) * 4 + n, (n18 >>> 2 & 0x7FFFF) | array3[n19] << 19);
            final int n20 = array3[n19];
            final int n21 = array3[n6 + 8];
            final int n22 = n6 + 9;
            CommonFunction.store32(array, (n5 + 5) * 4 + n, (n20 >>> 13 & 0xFF) | (n21 & 0x1FFFFF) << 8 | array3[n22] << 29);
            final int n23 = array3[n22];
            final int n24 = n6 + 10;
            CommonFunction.store32(array, (n5 + 6) * 4 + n, (n23 >>> 3 & 0x3FFFF) | array3[n24] << 18);
            final int n25 = array3[n24];
            final int n26 = array3[n6 + 11];
            final int n27 = n6 + 12;
            CommonFunction.store32(array, (n5 + 7) * 4 + n, (n25 >>> 14 & 0x7F) | (n26 & 0x1FFFFF) << 7 | array3[n27] << 28);
            final int n28 = array3[n27];
            final int n29 = n6 + 13;
            CommonFunction.store32(array, (n5 + 8) * 4 + n, (n28 >>> 4 & 0x1FFFF) | array3[n29] << 17);
            final int n30 = array3[n29];
            final int n31 = array3[n6 + 14];
            final int n32 = n6 + 15;
            CommonFunction.store32(array, (n5 + 9) * 4 + n, (n30 >>> 15 & 0x3F) | (n31 & 0x1FFFFF) << 6 | array3[n32] << 27);
            final int n33 = array3[n32];
            final int n34 = n6 + 16;
            CommonFunction.store32(array, (n5 + 10) * 4 + n, (n33 >>> 5 & 0xFFFF) | array3[n34] << 16);
            final int n35 = array3[n34];
            final int n36 = array3[n6 + 17];
            final int n37 = n6 + 18;
            CommonFunction.store32(array, (n5 + 11) * 4 + n, (n35 >>> 16 & 0x1F) | (n36 & 0x1FFFFF) << 5 | array3[n37] << 26);
            final int n38 = array3[n37];
            final int n39 = n6 + 19;
            CommonFunction.store32(array, (n5 + 12) * 4 + n, (n38 >>> 6 & 0x7FFF) | array3[n39] << 15);
            final int n40 = array3[n39];
            final int n41 = array3[n6 + 20];
            final int n42 = n6 + 21;
            CommonFunction.store32(array, (n5 + 13) * 4 + n, (n40 >>> 17 & 0xF) | (n41 & 0x1FFFFF) << 4 | array3[n42] << 25);
            final int n43 = array3[n42];
            final int n44 = n6 + 22;
            CommonFunction.store32(array, (n5 + 14) * 4 + n, (n43 >>> 7 & 0x3FFF) | array3[n44] << 14);
            final int n45 = array3[n44];
            final int n46 = array3[n6 + 23];
            final int n47 = n6 + 24;
            CommonFunction.store32(array, (n5 + 15) * 4 + n, (n45 >>> 18 & 0x7) | (n46 & 0x1FFFFF) << 3 | array3[n47] << 24);
            final int n48 = array3[n47];
            final int n49 = n6 + 25;
            CommonFunction.store32(array, (n5 + 16) * 4 + n, (n48 >>> 8 & 0x1FFF) | array3[n49] << 13);
            final int n50 = array3[n49];
            final int n51 = array3[n6 + 26];
            final int n52 = n6 + 27;
            CommonFunction.store32(array, (n5 + 17) * 4 + n, (n50 >>> 19 & 0x3) | (n51 & 0x1FFFFF) << 2 | array3[n52] << 23);
            final int n53 = array3[n52];
            final int n54 = n6 + 28;
            CommonFunction.store32(array, (n5 + 18) * 4 + n, (n53 >>> 9 & 0xFFF) | array3[n54] << 12);
            final int n55 = array3[n54];
            final int n56 = array3[n6 + 29];
            final int n57 = n6 + 30;
            CommonFunction.store32(array, (n5 + 19) * 4 + n, (n55 >>> 20 & 0x1) | (0x1FFFFF & n56) << 1 | array3[n57] << 22);
            CommonFunction.store32(array, (n5 + 20) * 4 + n, (array3[n57] >>> 10 & 0x7FF) | array3[n6 + 31] << 11);
            n6 += 32;
            n5 += n4;
        }
        System.arraycopy(array2, n2, array, n + n7 / 8, 32);
    }
    
    public static void encodeSignatureIIIP(final byte[] array, final int n, final byte[] array2, final int n2, final long[] array3) {
        int i = 0;
        int n3 = 0;
        while (i < 1536) {
            final long n4 = array3[n3 + 0];
            final int n5 = n3 + 1;
            CommonFunction.store32(array, (i + 0) * 4 + n, (int)((n4 & 0xFFFFFFL) | array3[n5] << 24));
            final long n6 = array3[n5];
            final int n7 = n3 + 2;
            CommonFunction.store32(array, (i + 1) * 4 + n, (int)((n6 >>> 8 & 0xFFFFL) | array3[n7] << 16));
            CommonFunction.store32(array, (i + 2) * 4 + n, (int)((array3[n7] >>> 16 & 0xFFL) | array3[n3 + 3] << 8));
            n3 += 4;
            i += 3;
        }
        System.arraycopy(array2, n2, array, n + 6144, 32);
    }
    
    public static void encodeSignatureIIISpeed(final byte[] array, final int n, final byte[] array2, final int n2, final int[] array3) {
        int i = 0;
        int n3 = 0;
        while (i < 704) {
            final int n4 = array3[n3 + 0];
            final int n5 = n3 + 1;
            CommonFunction.store32(array, (i + 0) * 4 + n, (n4 & 0x3FFFFF) | array3[n5] << 22);
            final int n6 = array3[n5];
            final int n7 = n3 + 2;
            CommonFunction.store32(array, (i + 1) * 4 + n, (n6 >>> 10 & 0xFFF) | array3[n7] << 12);
            final int n8 = array3[n7];
            final int n9 = array3[n3 + 3];
            final int n10 = n3 + 4;
            CommonFunction.store32(array, (i + 2) * 4 + n, (n8 >>> 20 & 0x3) | (n9 & 0x3FFFFF) << 2 | array3[n10] << 24);
            final int n11 = array3[n10];
            final int n12 = n3 + 5;
            CommonFunction.store32(array, (i + 3) * 4 + n, (n11 >>> 8 & 0x3FFF) | array3[n12] << 14);
            final int n13 = array3[n12];
            final int n14 = array3[n3 + 6];
            final int n15 = n3 + 7;
            CommonFunction.store32(array, (i + 4) * 4 + n, (n13 >>> 18 & 0xF) | (n14 & 0x3FFFFF) << 4 | array3[n15] << 26);
            final int n16 = array3[n15];
            final int n17 = n3 + 8;
            CommonFunction.store32(array, (i + 5) * 4 + n, (n16 >>> 6 & 0xFFFF) | array3[n17] << 16);
            final int n18 = array3[n17];
            final int n19 = array3[n3 + 9];
            final int n20 = n3 + 10;
            CommonFunction.store32(array, (i + 6) * 4 + n, (n18 >>> 16 & 0x3F) | (n19 & 0x3FFFFF) << 6 | array3[n20] << 28);
            final int n21 = array3[n20];
            final int n22 = n3 + 11;
            CommonFunction.store32(array, (i + 7) * 4 + n, (n21 >>> 4 & 0x3FFFF) | array3[n22] << 18);
            final int n23 = array3[n22];
            final int n24 = array3[n3 + 12];
            final int n25 = n3 + 13;
            CommonFunction.store32(array, (i + 8) * 4 + n, (n23 >>> 14 & 0xFF) | (0x3FFFFF & n24) << 8 | array3[n25] << 30);
            final int n26 = array3[n25];
            final int n27 = n3 + 14;
            CommonFunction.store32(array, (i + 9) * 4 + n, (n26 >>> 2 & 0xFFFFF) | array3[n27] << 20);
            CommonFunction.store32(array, (i + 10) * 4 + n, (array3[n27] >>> 12 & 0x3FF) | array3[n3 + 15] << 10);
            n3 += 16;
            i += 11;
        }
        System.arraycopy(array2, n2, array, n + 2816, 32);
    }
    
    public static void encodeSignatureIP(final byte[] array, final int n, final byte[] array2, final int n2, final long[] array3) {
        int i = 0;
        int n3 = 0;
        while (i < 704) {
            final long n4 = array3[n3 + 0];
            final int n5 = n3 + 1;
            CommonFunction.store32(array, (i + 0) * 4 + n, (int)((n4 & 0x3FFFFFL) | array3[n5] << 22));
            final long n6 = array3[n5];
            final int n7 = n3 + 2;
            CommonFunction.store32(array, (i + 1) * 4 + n, (int)((n6 >>> 10 & 0xFFFL) | array3[n7] << 12));
            final long n8 = array3[n7];
            final long n9 = array3[n3 + 3];
            final int n10 = n3 + 4;
            CommonFunction.store32(array, (i + 2) * 4 + n, (int)((n8 >>> 20 & 0x3L) | (n9 & 0x3FFFFFL) << 2 | array3[n10] << 24));
            final long n11 = array3[n10];
            final int n12 = n3 + 5;
            CommonFunction.store32(array, (i + 3) * 4 + n, (int)((n11 >>> 8 & 0x3FFFL) | array3[n12] << 14));
            final long n13 = array3[n12];
            final long n14 = array3[n3 + 6];
            final int n15 = n3 + 7;
            CommonFunction.store32(array, (i + 4) * 4 + n, (int)((n13 >>> 18 & 0xFL) | (n14 & 0x3FFFFFL) << 4 | array3[n15] << 26));
            final long n16 = array3[n15];
            final int n17 = n3 + 8;
            CommonFunction.store32(array, (i + 5) * 4 + n, (int)((n16 >>> 6 & 0xFFFFL) | array3[n17] << 16));
            final long n18 = array3[n17];
            final long n19 = array3[n3 + 9];
            final int n20 = n3 + 10;
            CommonFunction.store32(array, (i + 6) * 4 + n, (int)((n18 >>> 16 & 0x3FL) | (n19 & 0x3FFFFFL) << 6 | array3[n20] << 28));
            final long n21 = array3[n20];
            final int n22 = n3 + 11;
            CommonFunction.store32(array, (i + 7) * 4 + n, (int)((n21 >>> 4 & 0x3FFFFL) | array3[n22] << 18));
            final long n23 = array3[n22];
            final long n24 = array3[n3 + 12];
            final int n25 = n3 + 13;
            CommonFunction.store32(array, (i + 8) * 4 + n, (int)((n23 >>> 14 & 0xFFL) | (n24 & 0x3FFFFFL) << 8 | array3[n25] << 30));
            final long n26 = array3[n25];
            final int n27 = n3 + 14;
            CommonFunction.store32(array, (i + 9) * 4 + n, (int)((n26 >>> 2 & 0xFFFFFL) | array3[n27] << 20));
            CommonFunction.store32(array, (i + 10) * 4 + n, (int)((array3[n27] >>> 12 & 0x3FFL) | array3[n3 + 15] << 10));
            n3 += 16;
            i += 11;
        }
        System.arraycopy(array2, n2, array, n + 2816, 32);
    }
    
    public static void packPrivateKey(final byte[] array, final long[] array2, final long[] array3, final byte[] array4, final int n, final int n2, final int n3) {
        for (int i = 0; i < n2; ++i) {
            array[i] = (byte)array2[i];
        }
        for (int j = 0; j < n3; ++j) {
            for (int k = 0; k < n2; ++k) {
                final int n4 = j * n2;
                array[n2 + n4 + k] = (byte)array3[n4 + k];
            }
        }
        System.arraycopy(array4, n, array, n2 + n3 * n2, 64);
    }
}
