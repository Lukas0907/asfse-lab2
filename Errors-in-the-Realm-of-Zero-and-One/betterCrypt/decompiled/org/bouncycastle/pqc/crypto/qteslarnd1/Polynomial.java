// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.qteslarnd1;

import org.bouncycastle.util.Arrays;

class Polynomial
{
    public static final int HASH = 32;
    public static final int MESSAGE = 64;
    public static final int PRIVATE_KEY_I = 1344;
    public static final int PRIVATE_KEY_III_P = 12352;
    public static final int PRIVATE_KEY_III_SIZE = 2112;
    public static final int PRIVATE_KEY_III_SPEED = 2368;
    public static final int PRIVATE_KEY_I_P = 5184;
    public static final int PUBLIC_KEY_I = 1504;
    public static final int PUBLIC_KEY_III_P = 39712;
    public static final int PUBLIC_KEY_III_SIZE = 2976;
    public static final int PUBLIC_KEY_III_SPEED = 3104;
    public static final int PUBLIC_KEY_I_P = 14880;
    public static final int RANDOM = 32;
    public static final int SEED = 32;
    public static final int SIGNATURE_I = 1376;
    public static final int SIGNATURE_III_P = 6176;
    public static final int SIGNATURE_III_SIZE = 2720;
    public static final int SIGNATURE_III_SPEED = 2848;
    public static final int SIGNATURE_I_P = 2848;
    
    public static int barrett(final int n, final int n2, final int n3, final int n4) {
        return n - (int)(n * (long)n3 >> n4) * n2;
    }
    
    public static long barrett(final long n, final int n2, final int n3, final int n4) {
        return n - (n3 * n >> n4) * n2;
    }
    
    private static void componentWisePolynomialMultiplication(final int[] array, final int[] array2, final int[] array3, final int n, final int n2, final long n3) {
        for (int i = 0; i < n; ++i) {
            array[i] = montgomery(array2[i] * (long)array3[i], n2, n3);
        }
    }
    
    private static void componentWisePolynomialMultiplication(final long[] array, final int n, final long[] array2, final int n2, final long[] array3, final int n3, final int n4, final int n5, final long n6) {
        for (int i = 0; i < n4; ++i) {
            array[n + i] = montgomeryP(array2[n2 + i] * array3[n3 + i], n5, n6);
        }
    }
    
    private static void inverseNumberTheoreticTransform(final int[] array, final int[] array2, final int n, final int n2, final long n3, final int n4, int i, final int n5) {
        int j = 1;
        int n6 = 0;
        while (j < n) {
            int l;
            for (int k = 0; k < n; k = l + j, ++n6) {
                final long n7 = array2[n6];
                for (l = k; l < k + j; ++l) {
                    final int n8 = array[l];
                    if (j == 16) {
                        array[l] = barrett(array[l + j] + n8, n2, i, n5);
                    }
                    else {
                        array[l] = array[l + j] + n8;
                    }
                    final int n9 = l + j;
                    array[n9] = montgomery((n8 - array[n9]) * n7, n2, n3);
                }
            }
            j *= 2;
        }
        for (i = 0; i < n / 2; ++i) {
            array[i] = montgomery(n4 * (long)array[i], n2, n3);
        }
    }
    
    private static void inverseNumberTheoreticTransformI(final int[] array, final int[] array2) {
        final int n = 0;
        int n2 = 1;
        int n3 = 0;
        int i;
        while (true) {
            i = n;
            if (n2 >= 512) {
                break;
            }
            int k;
            for (int j = 0; j < 512; j = k + n2, ++n3) {
                final long n4 = array2[n3];
                for (k = j; k < j + n2; ++k) {
                    final int n5 = array[k];
                    final int n6 = k + n2;
                    array[k] = array[n6] + n5;
                    array[n6] = montgomery((n5 - array[n6]) * n4, 4205569, 3098553343L);
                }
            }
            n2 *= 2;
        }
        while (i < 256) {
            array[i] = montgomery(array[i] * 1081347L, 4205569, 3098553343L);
            ++i;
        }
    }
    
    private static void inverseNumberTheoreticTransformIIIP(final long[] array, final int n, final long[] array2, final int n2) {
        int i = 1;
        int n3 = 0;
        while (i < 2048) {
            int k;
            for (int j = 0; j < 2048; j = k + i, ++n3) {
                final long n4 = array2[n2 + n3];
                for (k = j; k < j + i; ++k) {
                    final int n5 = n + k;
                    final long n6 = array[n5];
                    final int n7 = n5 + i;
                    array[n5] = barrett(array[n7] + n6, 1129725953, 15, 34);
                    array[n7] = barrett(montgomeryP((n6 + (2259451906L - array[n7])) * n4, 1129725953, 861290495L), 1129725953, 15, 34);
                }
            }
            i *= 2;
        }
    }
    
    private static void inverseNumberTheoreticTransformIP(final long[] array, final int n, final long[] array2, final int n2) {
        int i = 1;
        int n3 = 0;
        while (i < 1024) {
            int k;
            for (int j = 0; j < 1024; j = k + i, ++n3) {
                final long n4 = array2[n2 + n3];
                for (k = j; k < j + i; ++k) {
                    final int n5 = n + k;
                    final long n6 = array[n5];
                    final int n7 = n5 + i;
                    array[n5] = n6 + array[n7];
                    array[n7] = montgomeryP(n4 * (n6 + (971956226L - array[n7])), 485978113, 3421990911L);
                }
            }
            final int n8 = i * 2;
            int n10;
            for (int l = 0; l < 1024; l = n10 + n8, ++n3) {
                final long n9 = array2[n2 + n3];
                for (n10 = l; n10 < l + n8; ++n10) {
                    final int n11 = n + n10;
                    final long n12 = array[n11];
                    final int n13 = n11 + n8;
                    array[n11] = barrett(n12 + array[n13], 485978113, 1, 29);
                    array[n13] = montgomeryP(n9 * (n12 + (971956226L - array[n13])), 485978113, 3421990911L);
                }
            }
            i = n8 * 2;
        }
    }
    
    private static int montgomery(final long n, final int n2, final long n3) {
        return (int)(n + (n3 * n & 0xFFFFFFFFL) * n2 >> 32);
    }
    
    private static long montgomeryP(final long n, final int n2, final long n3) {
        return n + (n3 * n & 0xFFFFFFFFL) * n2 >> 32;
    }
    
    private static void numberTheoreticTransform(final int[] array, final int[] array2, final int n, final int n2, final long n3) {
        int i = n >> 1;
        int n4 = 0;
        while (i > 0) {
            int k;
            for (int j = 0; j < n; j = k + i, ++n4) {
                final long n5 = array2[n4];
                for (k = j; k < j + i; ++k) {
                    final int n6 = k + i;
                    final int montgomery = montgomery(array[n6] * n5, n2, n3);
                    array[n6] = array[k] - montgomery;
                    array[k] += montgomery;
                }
            }
            i >>= 1;
        }
    }
    
    private static void numberTheoreticTransformIIIP(final long[] array, final long[] array2) {
        int i = 1024;
        int n = 0;
        while (i > 0) {
            int k;
            for (int j = 0; j < 2048; j = k + i, ++n) {
                final int n2 = (int)array2[n];
                for (k = j; k < j + i; ++k) {
                    final long n3 = n2;
                    final int n4 = k + i;
                    final long barrett = barrett(montgomeryP(n3 * array[n4], 1129725953, 861290495L), 1129725953, 15, 34);
                    array[n4] = barrett(array[k] + (2259451906L - barrett), 1129725953, 15, 34);
                    array[k] = barrett(array[k] + barrett, 1129725953, 15, 34);
                }
            }
            i >>= 1;
        }
    }
    
    private static void numberTheoreticTransformIP(final long[] array, final long[] array2) {
        int i = 512;
        int n = 0;
        while (i > 0) {
            int k;
            for (int j = 0; j < 1024; j = k + i, ++n) {
                final long n2 = array2[n];
                for (k = j; k < j + i; ++k) {
                    final int n3 = k + i;
                    final long montgomeryP = montgomeryP(array[n3] * n2, 485978113, 3421990911L);
                    array[n3] = array[k] + (485978113L - montgomeryP);
                    array[k] += montgomeryP;
                }
            }
            i >>= 1;
        }
    }
    
    public static void polynomialAddition(final int[] array, final int[] array2, final int[] array3, final int n) {
        for (int i = 0; i < n; ++i) {
            array[i] = array2[i] + array3[i];
        }
    }
    
    public static void polynomialAddition(final long[] array, final int n, final long[] array2, final int n2, final long[] array3, final int n3, final int n4) {
        for (int i = 0; i < n4; ++i) {
            array[n + i] = array2[n2 + i] + array3[n3 + i];
        }
    }
    
    public static void polynomialAdditionCorrection(final int[] array, final int[] array2, final int[] array3, final int n, final int n2) {
        for (int i = 0; i < n; ++i) {
            array[i] = array2[i] + array3[i];
            array[i] += (array[i] >> 31 & n2);
            array[i] -= n2;
            array[i] += (array[i] >> 31 & n2);
        }
    }
    
    public static void polynomialMultiplication(final int[] array, final int[] array2, final int[] array3, final int n, final int n2, final long n3, final int[] array4) {
        final int[] array5 = new int[n];
        for (int i = 0; i < n; ++i) {
            array5[i] = array3[i];
        }
        numberTheoreticTransform(array5, array4, n, n2, n3);
        componentWisePolynomialMultiplication(array, array2, array5, n, n2, n3);
        if (n2 == 4205569) {
            inverseNumberTheoreticTransformI(array, PolynomialHeuristic.ZETA_INVERSE_I);
        }
        if (n2 == 4206593) {
            inverseNumberTheoreticTransform(array, PolynomialHeuristic.ZETA_INVERSE_III_SIZE, 1024, 4206593, 4148178943L, 35843, 1021, 32);
        }
        if (n2 == 8404993) {
            inverseNumberTheoreticTransform(array, PolynomialHeuristic.ZETA_INVERSE_III_SPEED, 1024, 8404993, 4034936831L, 15873, 511, 32);
        }
    }
    
    public static void polynomialMultiplication(final long[] array, final int n, final long[] array2, final int n2, final long[] array3, final int n3, final int n4, final int n5, final long n6) {
        componentWisePolynomialMultiplication(array, n, array2, n2, array3, n3, n4, n5, n6);
        if (n5 == 485978113) {
            inverseNumberTheoreticTransformIP(array, n, PolynomialProvablySecure.ZETA_INVERSE_I_P, 0);
        }
        if (n5 == 1129725953) {
            inverseNumberTheoreticTransformIIIP(array, n, PolynomialProvablySecure.ZETA_INVERSE_III_P, 0);
        }
    }
    
    public static void polynomialNumberTheoreticTransform(final long[] array, final long[] array2, final int n) {
        for (int i = 0; i < n; ++i) {
            array[i] = array2[i];
        }
        if (n == 1024) {
            numberTheoreticTransformIP(array, PolynomialProvablySecure.ZETA_I_P);
        }
        if (n == 2048) {
            numberTheoreticTransformIIIP(array, PolynomialProvablySecure.ZETA_III_P);
        }
    }
    
    public static void polynomialSubtraction(final long[] array, final int n, final long[] array2, final int n2, final long[] array3, final int n3, final int n4, final int n5, final int n6, final int n7) {
        for (int i = 0; i < n4; ++i) {
            array[n + i] = barrett(array2[n2 + i] - array3[n3 + i], n5, n6, n7);
        }
    }
    
    public static void polynomialSubtractionCorrection(final int[] array, final int[] array2, final int[] array3, final int n, final int n2) {
        for (int i = 0; i < n; ++i) {
            array[i] = array2[i] - array3[i];
            array[i] += (array[i] >> 31 & n2);
        }
    }
    
    public static void polynomialSubtractionMontgomery(final int[] array, final int[] array2, final int[] array3, final int n, final int n2, final long n3, final int n4) {
        for (int i = 0; i < n; ++i) {
            array[i] = montgomery(n4 * (long)(array2[i] - array3[i]), n2, n3);
        }
    }
    
    public static void polynomialUniform(final int[] array, final byte[] array2, final int n, final int n2, final int n3, final long n4, int n5, int i, final int n6) {
        final int n7 = (n5 + 7) / 8;
        final int n8 = (1 << n5) - 1;
        n5 = i * 168;
        final byte[] array3 = new byte[n5];
        short n9 = 1;
        final byte[] array4 = array3;
        HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array3, 0, n5, (short)0, array2, n, 32);
        final int n10 = 0;
        n5 = 0;
        int n11 = i;
        i = n10;
        while (i < n2) {
            int n13;
            if (n5 > n11 * 168 - n7 * 4) {
                final short n12 = (short)(n9 + 1);
                HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array4, 0, 168, n9, array2, n, 32);
                n5 = 1;
                n9 = n12;
                n13 = 0;
            }
            else {
                final int n14 = n5;
                n5 = n11;
                n13 = n14;
            }
            final int n15 = CommonFunction.load32(array4, n13) & n8;
            final int n16 = n13 + n7;
            final int n17 = CommonFunction.load32(array4, n16) & n8;
            final int n18 = n16 + n7;
            final int n19 = CommonFunction.load32(array4, n18) & n8;
            final int n20 = n18 + n7;
            final int n21 = CommonFunction.load32(array4, n20) & n8;
            final int n22 = n20 + n7;
            if (n15 < n3 && i < n2) {
                array[i] = montgomery(n15 * (long)n6, n3, n4);
                ++i;
            }
            int n23 = i;
            if (n17 < n3 && (n23 = i) < n2) {
                array[i] = montgomery(n17 * (long)n6, n3, n4);
                n23 = i + 1;
            }
            i = n23;
            if (n19 < n3 && (i = n23) < n2) {
                array[n23] = montgomery(n19 * (long)n6, n3, n4);
                i = n23 + 1;
            }
            if (n21 < n3 && i < n2) {
                array[i] = montgomery(n21 * (long)n6, n3, n4);
                ++i;
            }
            n11 = n5;
            n5 = n22;
        }
    }
    
    public static void polynomialUniform(final long[] array, final byte[] array2, final int n, final int n2, final int n3, final int n4, final long n5, int n6, int n7, final int n8) {
        final int n9 = (n6 + 7) / 8;
        final int n10 = (1 << n6) - 1;
        n6 = n7 * 168;
        final byte[] array3 = new byte[n6];
        short n11 = 1;
        HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array3, 0, n6, (short)0, array2, n, 32);
        n6 = 0;
        int n12 = 0;
        while (true) {
            final int n13 = n2 * n3;
            if (n6 >= n13) {
                break;
            }
            if (n12 > n7 * 168 - n9 * 4) {
                final short n14 = (short)(n11 + 1);
                HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array3, 0, 168, n11, array2, n, 32);
                n11 = n14;
                n12 = 0;
                n7 = 1;
            }
            final int n15 = CommonFunction.load32(array3, n12) & n10;
            final int n16 = n12 + n9;
            final int n17 = CommonFunction.load32(array3, n16) & n10;
            final int n18 = n16 + n9;
            final int n19 = CommonFunction.load32(array3, n18) & n10;
            final int n20 = n18 + n9;
            final int n21 = CommonFunction.load32(array3, n20) & n10;
            final int n22 = n20 + n9;
            if (n15 < n4 && n6 < n13) {
                array[n6] = montgomeryP(n15 * (long)n8, n4, n5);
                ++n6;
            }
            final int n23 = n7;
            n7 = n6;
            if (n17 < n4 && (n7 = n6) < n13) {
                array[n6] = montgomeryP(n17 * (long)n8, n4, n5);
                n7 = n6 + 1;
            }
            n6 = n7;
            if (n19 < n4 && (n6 = n7) < n13) {
                array[n7] = montgomeryP(n19 * (long)n8, n4, n5);
                n6 = n7 + 1;
            }
            if (n21 < n4 && n6 < n13) {
                array[n6] = montgomeryP(n21 * (long)n8, n4, n5);
                ++n6;
            }
            n7 = n23;
            n12 = n22;
        }
    }
    
    public static void sparsePolynomialMultiplication16(final int[] array, final short[] array2, final int[] array3, final short[] array4, final int n, final int n2) {
        Arrays.fill(array, 0);
        for (int i = 0; i < n2; ++i) {
            final int n3 = array3[i];
            for (int j = 0; j < n3; ++j) {
                array[j] -= array4[i] * array2[n + j - n3];
            }
            for (int k = n3; k < n; ++k) {
                array[k] += array4[i] * array2[k - n3];
            }
        }
    }
    
    public static void sparsePolynomialMultiplication32(final int[] array, final int[] array2, final int[] array3, final short[] array4, final int n, final int n2) {
        Arrays.fill(array, 0);
        for (int i = 0; i < n2; ++i) {
            final int n3 = array3[i];
            for (int j = 0; j < n3; ++j) {
                array[j] -= array4[i] * array2[n + j - n3];
            }
            for (int k = n3; k < n; ++k) {
                array[k] += array4[i] * array2[k - n3];
            }
        }
    }
    
    public static void sparsePolynomialMultiplication32(final long[] array, final int n, final int[] array2, int n2, final int[] array3, final short[] array4, final int n3, final int n4, final int n5, final int n6, final int n7) {
        Arrays.fill(array, 0L);
        final int n8 = 0;
        int n9 = 0;
        int i;
        while (true) {
            i = n8;
            if (n9 >= n4) {
                break;
            }
            final int n10 = array3[n9];
            for (int j = 0; j < n10; ++j) {
                final int n11 = n + j;
                array[n11] -= array4[n9] * array2[n2 + n3 + j - n10];
            }
            for (int k = n10; k < n3; ++k) {
                final int n12 = n + k;
                array[n12] += array4[n9] * array2[n2 + k - n10];
            }
            ++n9;
        }
        while (i < n3) {
            n2 = n + i;
            array[n2] = barrett(array[n2], n5, n6, n7);
            ++i;
        }
    }
    
    public static void sparsePolynomialMultiplication8(final long[] array, final int n, final byte[] array2, final int n2, final int[] array3, final short[] array4, final int n3, final int n4) {
        Arrays.fill(array, 0L);
        for (int i = 0; i < n4; ++i) {
            final int n5 = array3[i];
            for (int j = 0; j < n5; ++j) {
                final int n6 = n + j;
                array[n6] -= array4[i] * array2[n2 + n3 + j - n5];
            }
            for (int k = n5; k < n3; ++k) {
                final int n7 = n + k;
                array[n7] += array4[i] * array2[n2 + k - n5];
            }
        }
    }
}
