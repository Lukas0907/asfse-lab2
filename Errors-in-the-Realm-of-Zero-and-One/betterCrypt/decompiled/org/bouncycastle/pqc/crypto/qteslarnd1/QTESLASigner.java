// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.qteslarnd1;

import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import java.security.SecureRandom;
import org.bouncycastle.pqc.crypto.MessageSigner;

public class QTESLASigner implements MessageSigner
{
    private QTESLAPrivateKeyParameters privateKey;
    private QTESLAPublicKeyParameters publicKey;
    private SecureRandom secureRandom;
    
    @Override
    public byte[] generateSignature(final byte[] array) {
        final byte[] array2 = new byte[QTESLASecurityCategory.getSignatureSize(this.privateKey.getSecurityCategory())];
        final int securityCategory = this.privateKey.getSecurityCategory();
        if (securityCategory == 0) {
            QTESLA.signingI(array2, array, 0, array.length, this.privateKey.getSecret(), this.secureRandom);
            return array2;
        }
        if (securityCategory == 1) {
            QTESLA.signingIIISize(array2, array, 0, array.length, this.privateKey.getSecret(), this.secureRandom);
            return array2;
        }
        if (securityCategory == 2) {
            QTESLA.signingIIISpeed(array2, array, 0, array.length, this.privateKey.getSecret(), this.secureRandom);
            return array2;
        }
        if (securityCategory == 3) {
            QTESLA.signingIP(array2, array, 0, array.length, this.privateKey.getSecret(), this.secureRandom);
            return array2;
        }
        if (securityCategory == 4) {
            QTESLA.signingIIIP(array2, array, 0, array.length, this.privateKey.getSecret(), this.secureRandom);
            return array2;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown security category: ");
        sb.append(this.privateKey.getSecurityCategory());
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public void init(final boolean b, final CipherParameters cipherParameters) {
        int n;
        if (b) {
            if (cipherParameters instanceof ParametersWithRandom) {
                final ParametersWithRandom parametersWithRandom = (ParametersWithRandom)cipherParameters;
                this.secureRandom = parametersWithRandom.getRandom();
                this.privateKey = (QTESLAPrivateKeyParameters)parametersWithRandom.getParameters();
            }
            else {
                this.secureRandom = CryptoServicesRegistrar.getSecureRandom();
                this.privateKey = (QTESLAPrivateKeyParameters)cipherParameters;
            }
            this.publicKey = null;
            n = this.privateKey.getSecurityCategory();
        }
        else {
            this.privateKey = null;
            this.publicKey = (QTESLAPublicKeyParameters)cipherParameters;
            n = this.publicKey.getSecurityCategory();
        }
        QTESLASecurityCategory.validate(n);
    }
    
    @Override
    public boolean verifySignature(final byte[] array, final byte[] array2) {
        final int securityCategory = this.publicKey.getSecurityCategory();
        int n;
        if (securityCategory != 0) {
            if (securityCategory != 1) {
                if (securityCategory != 2) {
                    if (securityCategory != 3) {
                        if (securityCategory != 4) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("unknown security category: ");
                            sb.append(this.publicKey.getSecurityCategory());
                            throw new IllegalArgumentException(sb.toString());
                        }
                        n = QTESLA.verifyingPIII(array, array2, 0, array2.length, this.publicKey.getPublicData());
                    }
                    else {
                        n = QTESLA.verifyingPI(array, array2, 0, array2.length, this.publicKey.getPublicData());
                    }
                }
                else {
                    n = QTESLA.verifyingIIISpeed(array, array2, 0, array2.length, this.publicKey.getPublicData());
                }
            }
            else {
                n = QTESLA.verifyingIIISize(array, array2, 0, array2.length, this.publicKey.getPublicData());
            }
        }
        else {
            n = QTESLA.verifyingI(array, array2, 0, array2.length, this.publicKey.getPublicData());
        }
        return n == 0;
    }
}
