// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.qteslarnd1;

class CommonFunction
{
    public static short load16(final byte[] array, final int n) {
        final int length = array.length;
        final int n2 = 0;
        int n3 = 0;
        short n5;
        if (length - n >= 2) {
            short n4 = 0;
            while (true) {
                n5 = n4;
                if (n3 >= 2) {
                    break;
                }
                n4 ^= (short)((short)(array[n + n3] & 0xFF) << n3 * 8);
                ++n3;
            }
        }
        else {
            short n6 = 0;
            int n7 = n2;
            while (true) {
                n5 = n6;
                if (n7 >= array.length - n) {
                    break;
                }
                n6 ^= (short)((short)(array[n + n7] & 0xFF) << n7 * 8);
                ++n7;
            }
        }
        return n5;
    }
    
    public static int load32(final byte[] array, final int n) {
        final int length = array.length;
        final int n2 = 0;
        int n3 = 0;
        int n5;
        if (length - n >= 4) {
            int n4 = 0;
            while (true) {
                n5 = n4;
                if (n3 >= 4) {
                    break;
                }
                n4 ^= (array[n + n3] & 0xFF) << n3 * 8;
                ++n3;
            }
        }
        else {
            final int n6 = 0;
            int n7 = n2;
            int n8 = n6;
            while (true) {
                n5 = n8;
                if (n7 >= array.length - n) {
                    break;
                }
                n8 ^= (array[n + n7] & 0xFF) << n7 * 8;
                ++n7;
            }
        }
        return n5;
    }
    
    public static long load64(final byte[] array, final int n) {
        final int length = array.length;
        int n2 = 0;
        final int n3 = 0;
        long n5;
        final long n4 = n5 = 0L;
        long n8;
        if (length - n >= 8) {
            long n6 = n4;
            int n7 = n3;
            while (true) {
                n8 = n6;
                if (n7 >= 8) {
                    break;
                }
                n6 ^= (long)(array[n + n7] & 0xFF) << n7 * 8;
                ++n7;
            }
        }
        else {
            while (true) {
                n8 = n5;
                if (n2 >= array.length - n) {
                    break;
                }
                n5 ^= (long)(array[n + n2] & 0xFF) << n2 * 8;
                ++n2;
            }
        }
        return n8;
    }
    
    public static boolean memoryEqual(final byte[] array, final int n, final byte[] array2, final int n2, final int n3) {
        if (n + n3 <= array.length && n2 + n3 <= array2.length) {
            for (int i = 0; i < n3; ++i) {
                if (array[n + i] != array2[n2 + i]) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    public static void store16(final byte[] array, final int n, final short n2) {
        final int length = array.length;
        int i = 0;
        final int n3 = 0;
        if (length - n >= 2) {
            for (int j = n3; j < 2; ++j) {
                array[n + j] = (byte)(n2 >> j * 8 & 0xFF);
            }
        }
        else {
            while (i < array.length - n) {
                array[n + i] = (byte)(n2 >> i * 8 & 0xFF);
                ++i;
            }
        }
    }
    
    public static void store32(final byte[] array, final int n, final int n2) {
        final int length = array.length;
        int i = 0;
        final int n3 = 0;
        if (length - n >= 4) {
            for (int j = n3; j < 4; ++j) {
                array[n + j] = (byte)(n2 >> j * 8 & 0xFF);
            }
        }
        else {
            while (i < array.length - n) {
                array[n + i] = (byte)(n2 >> i * 8 & 0xFF);
                ++i;
            }
        }
    }
    
    public static void store64(final byte[] array, final int n, final long n2) {
        final int length = array.length;
        int i = 0;
        final int n3 = 0;
        if (length - n >= 8) {
            for (int j = n3; j < 8; ++j) {
                array[n + j] = (byte)(n2 >> j * 8 & 0xFFL);
            }
        }
        else {
            while (i < array.length - n) {
                array[n + i] = (byte)(n2 >> i * 8 & 0xFFL);
                ++i;
            }
        }
    }
}
