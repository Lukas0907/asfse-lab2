// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.qteslarnd1;

import java.security.SecureRandom;

public class QTESLA
{
    private static int absolute(final int n) {
        final int n2 = n >> 31;
        return (n ^ n2) - n2;
    }
    
    private static long absolute(final long n) {
        final long n2 = n >> 63;
        return (n ^ n2) - n2;
    }
    
    private static boolean checkPolynomial(final int[] array, final int n, int i, final int n2) {
        final int[] array2 = new int[i];
        for (int j = 0; j < i; ++j) {
            array2[j] = absolute(array[j]);
        }
        int n3 = i;
        int n4;
        int n5;
        int n6;
        int n7;
        int n8;
        int n9;
        int n10;
        int n11;
        for (i = (n4 = 0); i < n2; ++i) {
            n5 = 0;
            while (true) {
                n6 = n3 - 1;
                if (n5 >= n6) {
                    break;
                }
                n7 = n5 + 1;
                n8 = array2[n7] - array2[n5] >> 31;
                n9 = array2[n7];
                n10 = array2[n5];
                n11 = n8;
                array2[n7] = ((n8 & array2[n5]) | (array2[n7] & n11));
                array2[n5] = ((n9 & n8) | (n10 & n11));
                n5 = n7;
            }
            n4 += array2[n6];
            --n3;
        }
        return n4 > n;
    }
    
    private static boolean checkPolynomial(final long[] array, int i, final int n, int n2, final int n3) {
        final short[] array2 = new short[n2];
        for (int j = 0; j < n2; ++j) {
            array2[j] = (short)absolute(array[i + j]);
        }
        int n4;
        int n5;
        int n6;
        int n7;
        short n8;
        short n9;
        short n10;
        short n11;
        short n12;
        for (i = (n4 = 0); i < n3; ++i) {
            n5 = 0;
            while (true) {
                n6 = n2 - 1;
                if (n5 >= n6) {
                    break;
                }
                n7 = n5 + 1;
                n8 = (short)(array2[n7] - array2[n5] >> 15);
                n9 = array2[n7];
                n10 = array2[n5];
                n11 = n8;
                n12 = (short)((n9 & n8) | (n10 & n11));
                array2[n7] = (short)((n8 & array2[n5]) | (array2[n7] & n11));
                array2[n5] = n12;
                n5 = n7;
            }
            n4 += array2[n6];
            --n2;
        }
        return n4 > n;
    }
    
    private static int generateKeyPair(final byte[] array, final byte[] array2, final SecureRandom secureRandom, final int n, final int n2, int i, final int n3, final long n4, int j, int n5, int n6, final double n7, final long[] array3, int n8, int n9) {
        final byte[] bytes = new byte[32];
        final int n10 = (n2 + 3) * 32;
        final byte[] array4 = new byte[n10];
        final long[] array5 = new long[n];
        final long[] array6 = new long[n];
        final int n11 = n * n2;
        final long[] array7 = new long[n11];
        final long[] array8 = new long[n11];
        final long[] array9 = new long[n11];
        secureRandom.nextBytes(bytes);
        if (n3 == 485978113) {
            HashUtils.secureHashAlgorithmKECCAK128(array4, 0, n10, bytes, 0, 32);
        }
        final long[] array10 = array7;
        final int n12 = 485978113;
        if (n3 == 1129725953) {
            HashUtils.secureHashAlgorithmKECCAK256(array4, 0, n10, bytes, 0, 32);
        }
        final int n13 = 0;
        int n14;
        for (int k = n14 = 0; k < n2; ++k) {
            do {
                int n15;
                if (n3 == 485978113) {
                    n15 = n14 + 1;
                    Sample.polynomialGaussSamplerIP(array10, n * k, array4, k * 32, n15);
                }
                else {
                    n15 = n14;
                }
                n14 = n15;
                if (n3 == 1129725953) {
                    n14 = n15 + 1;
                    Sample.polynomialGaussSamplerIIIP(array10, n * k, array4, k * 32, n14);
                }
            } while (checkPolynomial(array10, n * k, n8, n, i));
        }
        n8 = n13;
        final int n16 = n12;
        do {
            int n17 = n14;
            if (n3 == n16) {
                n17 = n14 + 1;
                Sample.polynomialGaussSamplerIP(array5, n8, array4, n2 * 32, n17);
            }
            n14 = n17;
            if (n3 == 1129725953) {
                n14 = n17 + 1;
                Sample.polynomialGaussSamplerIIIP(array5, n8, array4, n2 * 32, n14);
            }
        } while (checkPolynomial(array5, n8, n9, n, i));
        n9 = (n2 + 1) * 32;
        Polynomial.polynomialUniform(array8, array4, n9, n, n2, n3, n4, j, n5, n6);
        Polynomial.polynomialNumberTheoreticTransform(array6, array5, n);
        long n18;
        for (i = n8; i < n2; ++i) {
            n5 = n * i;
            Polynomial.polynomialMultiplication(array9, n5, array8, n5, array6, 0, n, n3, n4);
            Polynomial.polynomialAddition(array9, n5, array9, n5, array10, n5, n);
            for (j = n8; j < n; ++j) {
                n18 = n3;
                n6 = n5 + j;
                array9[n6] -= (n18 & n18 - array9[n6] >> 63);
            }
        }
        Pack.packPrivateKey(array2, array5, array10, array4, n9, n, n2);
        if (n3 == 485978113) {
            Pack.encodePublicKeyIP(array, array9, array4, n9);
        }
        if (n3 == 1129725953) {
            Pack.encodePublicKeyIIIP(array, array9, array4, n9);
        }
        return n8;
    }
    
    private static int generateKeyPair(final byte[] array, final byte[] array2, final SecureRandom secureRandom, final int n, final int n2, final int n3, final long n4, final int n5, final int n6, final int n7, final double n8, final int[] array3, int n9, final int n10) {
        final byte[] bytes = new byte[32];
        final byte[] array4 = new byte[128];
        final int[] array5 = new int[n];
        final int[] array6 = new int[n];
        final int[] array7 = new int[n];
        final int[] array8 = new int[n];
        secureRandom.nextBytes(bytes);
        if (n3 == 4205569) {
            HashUtils.secureHashAlgorithmKECCAK128(array4, 0, 128, bytes, 0, 32);
        }
        if (n3 == 4206593 || n3 == 8404993) {
            HashUtils.secureHashAlgorithmKECCAK256(array4, 0, 128, bytes, 0, 32);
        }
        int n11 = 0;
        final int[] array9 = array7;
        boolean checkPolynomial;
        boolean b;
        do {
            int n12 = n11;
            if (n3 == 4205569) {
                n12 = n11 + 1;
                Sample.polynomialGaussSamplerI(array6, 0, array4, 0, n12);
            }
            if (n3 == 4206593) {
                ++n12;
                Sample.polynomialGaussSamplerIII(array6, 0, array4, 0, n12, n, n8, Sample.EXPONENTIAL_DISTRIBUTION_III_SIZE);
            }
            n11 = n12;
            if (n3 == 8404993) {
                n11 = n12 + 1;
                Sample.polynomialGaussSamplerIII(array6, 0, array4, 0, n11, n, n8, Sample.EXPONENTIAL_DISTRIBUTION_III_SPEED);
            }
            checkPolynomial = checkPolynomial(array6, n9, n, n2);
            b = true;
        } while (checkPolynomial);
        do {
            if (n3 == 4205569) {
                n9 = n11 + 1;
                Sample.polynomialGaussSamplerI(array5, 0, array4, 32, n9);
            }
            else {
                n9 = n11;
            }
            if (n3 == 4206593) {
                ++n9;
                Sample.polynomialGaussSamplerIII(array5, 0, array4, 32, n9, n, n8, Sample.EXPONENTIAL_DISTRIBUTION_III_SIZE);
            }
            n11 = n9;
            if (n3 == 8404993) {
                n11 = n9 + 1;
                Sample.polynomialGaussSamplerIII(array5, 0, array4, 32, n11, n, n8, Sample.EXPONENTIAL_DISTRIBUTION_III_SPEED);
            }
        } while (checkPolynomial(array5, n10, n, n2) == b);
        Polynomial.polynomialUniform(array9, array4, 64, n, n3, n4, n5, n6, n7);
        Polynomial.polynomialMultiplication(array8, array9, array5, n, n3, n4, array3);
        Polynomial.polynomialAdditionCorrection(array8, array8, array6, n, n3);
        if (n3 == 4205569) {
            Pack.encodePrivateKeyI(array2, array5, array6, array4, 64);
            Pack.encodePublicKey(array, array8, array4, 64, 512, 23);
        }
        if (n3 == 4206593) {
            Pack.encodePrivateKeyIIISize(array2, array5, array6, array4, 64);
            Pack.encodePublicKey(array, array8, array4, 64, 1024, 23);
        }
        if (n3 == 8404993) {
            Pack.encodePrivateKeyIIISpeed(array2, array5, array6, array4, 64);
            Pack.encodePublicKeyIIISpeed(array, array8, array4, 64);
        }
        return 0;
    }
    
    public static int generateKeyPairI(final byte[] array, final byte[] array2, final SecureRandom secureRandom) {
        return generateKeyPair(array, array2, secureRandom, 512, 30, 4205569, 3098553343L, 23, 19, 113307, 27.0, PolynomialHeuristic.ZETA_I, 1586, 1586);
    }
    
    public static int generateKeyPairIIIP(final byte[] array, final byte[] array2, final SecureRandom secureRandom) {
        return generateKeyPair(array, array2, secureRandom, 2048, 5, 40, 1129725953, 861290495L, 31, 180, 851423148, 10.0, PolynomialProvablySecure.ZETA_III_P, 901, 901);
    }
    
    public static int generateKeyPairIIISize(final byte[] array, final byte[] array2, final SecureRandom secureRandom) {
        return generateKeyPair(array, array2, secureRandom, 1024, 48, 4206593, 4148178943L, 23, 38, 1217638, 9.0, PolynomialHeuristic.ZETA_III_SIZE, 910, 910);
    }
    
    public static int generateKeyPairIIISpeed(final byte[] array, final byte[] array2, final SecureRandom secureRandom) {
        return generateKeyPair(array, array2, secureRandom, 1024, 48, 8404993, 4034936831L, 24, 38, 237839, 12.0, PolynomialHeuristic.ZETA_III_SPEED, 1147, 1233);
    }
    
    public static int generateKeyPairIP(final byte[] array, final byte[] array2, final SecureRandom secureRandom) {
        return generateKeyPair(array, array2, secureRandom, 1024, 4, 25, 485978113, 3421990911L, 29, 108, 472064468, 10.0, PolynomialProvablySecure.ZETA_I_P, 554, 554);
    }
    
    private static void hashFunction(final byte[] array, final int n, final int[] array2, final byte[] array3, final int n2, final int n3, final int n4, final int n5) {
        final int n6 = n3 + 64;
        final byte[] array4 = new byte[n6];
        for (int i = 0; i < n3; ++i) {
            final int n7 = n5 / 2 - array2[i] >> 31;
            array2[i] = ((n7 & array2[i]) | (array2[i] - n5 & n7));
            final int n8 = array2[i];
            final int n9 = 1 << n4;
            final int n10 = n8 & n9 - 1;
            final int n11 = (1 << n4 - 1) - n10 >> 31;
            array4[i] = (byte)(array2[i] - ((n10 & n11) | (n10 - n9 & n11)) >> n4);
        }
        System.arraycopy(array3, n2, array4, n3, 64);
        if (n5 == 4205569) {
            HashUtils.secureHashAlgorithmKECCAK128(array, n, 32, array4, 0, n6);
        }
        if (n5 == 4206593 || n5 == 8404993) {
            HashUtils.secureHashAlgorithmKECCAK256(array, n, 32, array4, 0, n6);
        }
    }
    
    private static void hashFunction(final byte[] array, final int n, final long[] array2, final byte[] array3, final int n2, final int n3, final int n4, final int n5, final int n6) {
        final int n7 = n3 * n4;
        final int n8 = n7 + 64;
        final byte[] array4 = new byte[n8];
        for (int i = 0; i < n4; ++i) {
            for (int n9 = n3 * i, j = 0; j < n3; ++j, ++n9) {
                final long n10 = array2[n9];
                final long n11 = n6 / 2 - n10 >> 63;
                final long n12 = (n10 - n6 & n11) | (n10 & n11);
                final int n13 = 1 << n5;
                final long n14 = (long)(n13 - 1) & n12;
                final long n15 = (1 << n5 - 1) - n14 >> 63;
                array4[n9] = (byte)(n12 - ((n15 & n14) | (n14 - n13 & n15)) >> n5);
            }
        }
        System.arraycopy(array3, n2, array4, n7, 64);
        if (n6 == 485978113) {
            HashUtils.secureHashAlgorithmKECCAK128(array, n, 32, array4, 0, n8);
        }
        if (n6 == 1129725953) {
            HashUtils.secureHashAlgorithmKECCAK256(array, n, 32, array4, 0, n8);
        }
    }
    
    private static int signing(final byte[] array, final byte[] array2, int i, int n, final byte[] array3, final SecureRandom secureRandom, final int n2, final int n3, final int n4, final int n5, final long n6, int n7, final int n8, final int n9, final int n10, final int n11, final int n12, final int n13, final int n14, final int n15, final int n16, final int n17) {
        final byte[] array4 = new byte[32];
        final byte[] array5 = new byte[32];
        final byte[] array6 = new byte[128];
        final byte[] bytes = new byte[32];
        final int[] array7 = new int[n4];
        final short[] array8 = new short[n4];
        i = n2 * n3;
        final long[] array9 = new long[i];
        final long[] array10 = new long[i];
        final long[] array11 = new long[n2];
        final long[] array12 = new long[n2];
        final long[] array13 = new long[n2];
        final long[] array14 = new long[n2];
        final long[] array15 = new long[i];
        secureRandom.nextBytes(bytes);
        System.arraycopy(bytes, 0, array6, 32, 32);
        System.arraycopy(array3, n15 - 32, array6, 0, 32);
        if (n5 == 485978113) {
            HashUtils.secureHashAlgorithmKECCAK128(array6, 64, 64, array2, 0, n);
            HashUtils.secureHashAlgorithmKECCAK128(array5, 0, 32, array6, 0, 128);
        }
        final long[] array16 = array11;
        if (n5 == 1129725953) {
            HashUtils.secureHashAlgorithmKECCAK256(array6, 64, 64, array2, 0, n);
            HashUtils.secureHashAlgorithmKECCAK256(array5, 0, 32, array6, 0, 128);
        }
        Polynomial.polynomialUniform(array9, array3, n15 - 64, n2, n3, n5, n6, n7, n13, n14);
        i = 0;
        boolean testV = false;
        final long[] array17 = array12;
        final long[] array18 = array13;
        while (true) {
            final boolean b = true;
            n = i + 1;
            Sample.sampleY(array16, array5, 0, n, n2, n5, n8, n9);
            Polynomial.polynomialNumberTheoreticTransform(array17, array16, n2);
            for (i = 0; i < n3; ++i) {
                n7 = n2 * i;
                Polynomial.polynomialMultiplication(array10, n7, array9, n7, array17, 0, n2, n5, n6);
            }
            hashFunction(array4, 0, array10, array6, 64, n2, n3, n10, n5);
            Sample.encodeC(array7, array8, array4, 0, n2, n4);
            Polynomial.sparsePolynomialMultiplication8(array14, 0, array3, 0, array7, array8, n2, n4);
            Polynomial.polynomialAddition(array18, 0, array16, 0, array14, 0, n2);
            if (testRejection(array18, n2, n8, n11) != b) {
                i = 0;
                while (i < n3) {
                    n7 = n2 * i;
                    ++i;
                    Polynomial.sparsePolynomialMultiplication8(array15, n7, array3, n2 * i, array7, array8, n2, n4);
                    Polynomial.polynomialSubtraction(array10, n7, array10, n7, array15, n7, n2, n5, n16, n17);
                    testV = testV(array10, n7, n2, n10, n5, n12);
                    if (testV == b) {
                        break;
                    }
                }
                if (testV != b) {
                    break;
                }
            }
            i = n;
        }
        if (n5 == 485978113) {
            Pack.encodeSignatureIP(array, 0, array4, 0, array18);
        }
        if (n5 == 1129725953) {
            Pack.encodeSignatureIIIP(array, 0, array4, 0, array18);
        }
        return 0;
    }
    
    private static int signing(final byte[] array, final byte[] array2, int n, final int n2, final byte[] array3, final SecureRandom secureRandom, final int n3, final int n4, final int n5, final long n6, final int n7, final int n8, final int n9, final int n10, final int n11, final int n12, final int n13, final int n14, final int n15, final int n16, final int[] array4) {
        final byte[] array5 = new byte[32];
        final byte[] array6 = new byte[32];
        final byte[] array7 = new byte[128];
        final byte[] array8 = new byte[64];
        final byte[] bytes = new byte[32];
        final int[] array9 = new int[n4];
        final short[] array10 = new short[n4];
        final short[] array11 = new short[n3];
        final short[] array12 = new short[n3];
        final int[] array13 = new int[n3];
        final int[] array14 = new int[n3];
        final int[] array15 = new int[n3];
        final int[] array16 = new int[n3];
        final int[] array17 = new int[n3];
        final int[] array18 = new int[n3];
        if (n5 == 4205569) {
            Pack.decodePrivateKeyI(array8, array11, array12, array3);
        }
        if (n5 == 4206593) {
            Pack.decodePrivateKeyIIISize(array8, array11, array12, array3);
        }
        if (n5 == 8404993) {
            Pack.decodePrivateKeyIIISpeed(array8, array11, array12, array3);
        }
        secureRandom.nextBytes(bytes);
        System.arraycopy(bytes, 0, array7, 32, 32);
        System.arraycopy(array8, 32, array7, 0, 32);
        if (n5 == 4205569) {
            HashUtils.secureHashAlgorithmKECCAK128(array7, 64, 64, array2, 0, n2);
            HashUtils.secureHashAlgorithmKECCAK128(array6, 0, 32, array7, 0, 128);
        }
        final int[] array19 = array14;
        final int[] array20 = array17;
        if (n5 == 4206593 || n5 == 8404993) {
            HashUtils.secureHashAlgorithmKECCAK256(array7, 64, 64, array2, 0, n2);
            HashUtils.secureHashAlgorithmKECCAK256(array6, 0, 32, array7, 0, 128);
        }
        final int[] array21 = array16;
        final int[] array22 = array15;
        Polynomial.polynomialUniform(array13, array8, 0, n3, n5, n6, n7, n13, n14);
        n = 0;
        final int[] array23 = array19;
        final int[] array24 = array18;
        while (true) {
            ++n;
            Sample.sampleY(array22, array6, 0, n, n3, n5, n8, n9);
            Polynomial.polynomialMultiplication(array23, array13, array22, n3, n5, n6, array4);
            hashFunction(array5, 0, array23, array7, 64, n3, n10, n5);
            Sample.encodeC(array9, array10, array5, 0, n3, n4);
            Polynomial.sparsePolynomialMultiplication16(array20, array11, array9, array10, n3, n4);
            Polynomial.polynomialAddition(array21, array22, array20, n3);
            if (testRejection(array21, n3, n8, n11)) {
                continue;
            }
            Polynomial.sparsePolynomialMultiplication16(array24, array12, array9, array10, n3, n4);
            final int[] array25 = array23;
            Polynomial.polynomialSubtractionCorrection(array25, array25, array24, n3, n5);
            if (testV(array25, n3, n10, n5, n12)) {
                continue;
            }
            break;
        }
        if (n5 == 4205569) {
            Pack.encodeSignature(array, 0, array5, 0, array21, n3, n10);
        }
        if (n5 == 4206593) {
            Pack.encodeSignature(array, 0, array5, 0, array21, n3, n10);
        }
        if (n5 == 8404993) {
            Pack.encodeSignatureIIISpeed(array, 0, array5, 0, array21);
        }
        return 0;
    }
    
    static int signingI(final byte[] array, final byte[] array2, final int n, final int n2, final byte[] array3, final SecureRandom secureRandom) {
        return signing(array, array2, n, n2, array3, secureRandom, 512, 30, 4205569, 3098553343L, 23, 1048575, 20, 21, 1586, 1586, 19, 113307, 1021, 32, PolynomialHeuristic.ZETA_I);
    }
    
    public static int signingIIIP(final byte[] array, final byte[] array2, final int n, final int n2, final byte[] array3, final SecureRandom secureRandom) {
        return signing(array, array2, n, n2, array3, secureRandom, 2048, 5, 40, 1129725953, 861290495L, 31, 8388607, 23, 24, 901, 901, 180, 851423148, 12352, 15, 34);
    }
    
    static int signingIIISize(final byte[] array, final byte[] array2, final int n, final int n2, final byte[] array3, final SecureRandom secureRandom) {
        return signing(array, array2, n, n2, array3, secureRandom, 1024, 48, 4206593, 4148178943L, 23, 1048575, 20, 21, 910, 910, 38, 1217638, 1021, 32, PolynomialHeuristic.ZETA_III_SIZE);
    }
    
    static int signingIIISpeed(final byte[] array, final byte[] array2, final int n, final int n2, final byte[] array3, final SecureRandom secureRandom) {
        return signing(array, array2, n, n2, array3, secureRandom, 1024, 48, 8404993, 4034936831L, 24, 2097151, 21, 22, 1233, 1147, 38, 237839, 511, 32, PolynomialHeuristic.ZETA_III_SPEED);
    }
    
    public static int signingIP(final byte[] array, final byte[] array2, final int n, final int n2, final byte[] array3, final SecureRandom secureRandom) {
        return signing(array, array2, n, n2, array3, secureRandom, 1024, 4, 25, 485978113, 3421990911L, 29, 2097151, 21, 22, 554, 554, 108, 472064468, 5184, 1, 29);
    }
    
    private static boolean testRejection(final int[] array, final int n, final int n2, final int n3) {
        for (int i = 0; i < n; ++i) {
            if (absolute(array[i]) > n2 - n3) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean testRejection(final long[] array, final int n, final int n2, final int n3) {
        for (int i = 0; i < n; ++i) {
            if (absolute(array[i]) > n2 - n3) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean testV(final int[] array, final int n, final int n2, final int n3, final int n4) {
        for (int i = 0; i < n; ++i) {
            final int n5 = n3 / 2;
            final int n6 = n5 - array[i] >> 31;
            final int n7 = (n6 & array[i]) | (array[i] - n3 & n6);
            final int absolute = absolute(n7);
            final int n8 = 1 << n2 - 1;
            if ((absolute - (n5 - n4) >>> 31 | absolute(n7 - (n7 + n8 - 1 >> n2 << n2)) - (n8 - n4) >>> 31) == 0x1) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean testV(final long[] array, final int n, final int n2, final int n3, final int n4, final int n5) {
        for (int i = 0; i < n2; ++i) {
            final int n6 = n4 / 2;
            final long n7 = n6;
            final int n8 = n + i;
            final long n9 = n7 - array[n8] >> 63;
            final long n10 = (n9 & array[n8]) | (array[n8] - n4 & n9);
            final long absolute = absolute(n10);
            final long n11 = n6 - n5;
            final int n12 = 1 << n3 - 1;
            if ((absolute(n10 - ((long)(int)(n12 + n10 - 1L >> n3) << n3)) - (n12 - n5) >>> 63 | absolute - n11 >>> 63) == 0x1L) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean testZ(final int[] array, final int n, final int n2, final int n3) {
        for (int i = 0; i < n; ++i) {
            final int n4 = array[i];
            final int n5 = n2 - n3;
            if (n4 < -n5 || array[i] > n5) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean testZ(final long[] array, final int n, final int n2, final int n3) {
        for (int i = 0; i < n; ++i) {
            final long n4 = array[i];
            final int n5 = n2 - n3;
            if (n4 < -n5 || array[i] > n5) {
                return true;
            }
        }
        return false;
    }
    
    private static int verifying(final byte[] array, final byte[] array2, int i, int n, final byte[] array3, final int n2, final int n3, final int n4, final int n5, final long n6, final int n7, final int n8, final int n9, final int n10, final int n11, final int n12, final int n13, final int n14, final int n15, final long[] array4) {
        final byte[] array5 = new byte[32];
        final byte[] array6 = new byte[32];
        final byte[] array7 = new byte[32];
        final byte[] array8 = new byte[64];
        final int n16 = n2 * n3;
        final int[] array9 = new int[n16];
        final int[] array10 = new int[n4];
        final short[] array11 = new short[n4];
        final long[] array12 = new long[n16];
        final long[] array13 = new long[n2];
        final long[] array14 = new long[n2];
        final long[] array15 = new long[n16];
        final long[] array16 = new long[n16];
        if (n < n11) {
            return -1;
        }
        if (n5 == 485978113) {
            Pack.decodeSignatureIP(array5, array13, array2, i);
        }
        if (n5 == 1129725953) {
            Pack.decodeSignatureIIIP(array5, array13, array2, i);
        }
        if (testZ(array13, n2, n8, n10)) {
            return -2;
        }
        if (n5 == 485978113) {
            Pack.decodePublicKeyIP(array9, array7, 0, array3);
        }
        if (n5 == 1129725953) {
            Pack.decodePublicKeyIIIP(array9, array7, 0, array3);
        }
        Polynomial.polynomialUniform(array16, array7, 0, n2, n3, n5, n6, n7, n12, n13);
        Sample.encodeC(array10, array11, array5, 0, n2, n4);
        Polynomial.polynomialNumberTheoreticTransform(array14, array13, n2);
        for (i = 0; i < n3; ++i) {
            n = n2 * i;
            Polynomial.polynomialMultiplication(array12, n, array16, n, array14, 0, n2, n5, n6);
            Polynomial.sparsePolynomialMultiplication32(array15, n, array9, n, array10, array11, n2, n4, n5, n14, n15);
            Polynomial.polynomialSubtraction(array12, n, array12, n, array15, n, n2, n5, n14, n15);
        }
        if (n5 == 485978113) {
            HashUtils.secureHashAlgorithmKECCAK128(array8, 0, 64, array, 0, array.length);
        }
        if (n5 == 1129725953) {
            HashUtils.secureHashAlgorithmKECCAK256(array8, 0, 64, array, 0, array.length);
        }
        hashFunction(array6, 0, array12, array8, 0, n2, n3, n9, n5);
        if (!CommonFunction.memoryEqual(array5, 0, array6, 0, 32)) {
            return -3;
        }
        return 0;
    }
    
    private static int verifying(final byte[] array, final byte[] array2, final int n, final int n2, final byte[] array3, final int n3, final int n4, final int n5, final long n6, final int n7, final int n8, final int n9, final int n10, final int n11, final int n12, final int n13, final int n14, final int n15, final int n16, final int[] array4) {
        final byte[] array5 = new byte[32];
        final byte[] array6 = new byte[32];
        final byte[] array7 = new byte[32];
        final byte[] array8 = new byte[64];
        final int[] array9 = new int[n3];
        final int[] array10 = new int[n4];
        final short[] array11 = new short[n4];
        final int[] array12 = new int[n3];
        final int[] array13 = new int[n3];
        final int[] array14 = new int[n3];
        final int[] array15 = new int[n3];
        if (n2 < n12) {
            return -1;
        }
        if (n5 == 4205569 || n5 == 4206593) {
            Pack.decodeSignature(array5, array13, array2, n, n3, n9);
        }
        if (n5 == 8404993) {
            Pack.decodeSignatureIIISpeed(array5, array13, array2, n);
        }
        if (testZ(array13, n3, n8, n10)) {
            return -2;
        }
        if (n5 == 4205569 || n5 == 4206593) {
            Pack.decodePublicKey(array9, array7, 0, array3, n3, n7);
        }
        if (n5 == 8404993) {
            Pack.decodePublicKeyIIISpeed(array9, array7, 0, array3);
        }
        Polynomial.polynomialUniform(array15, array7, 0, n3, n5, n6, n7, n13, n14);
        Sample.encodeC(array10, array11, array5, 0, n3, n4);
        Polynomial.sparsePolynomialMultiplication32(array14, array9, array10, array11, n3, n4);
        Polynomial.polynomialMultiplication(array12, array15, array13, n3, n5, n6, array4);
        Polynomial.polynomialSubtractionMontgomery(array12, array12, array14, n3, n5, n6, n11);
        if (n5 == 4205569) {
            HashUtils.secureHashAlgorithmKECCAK128(array8, 0, 64, array, 0, array.length);
        }
        if (n5 == 4206593 || n5 == 8404993) {
            HashUtils.secureHashAlgorithmKECCAK256(array8, 0, 64, array, 0, array.length);
        }
        hashFunction(array6, 0, array12, array8, 0, n3, n9, n5);
        if (!CommonFunction.memoryEqual(array5, 0, array6, 0, 32)) {
            return -3;
        }
        return 0;
    }
    
    static int verifyingI(final byte[] array, final byte[] array2, final int n, final int n2, final byte[] array3) {
        return verifying(array, array2, n, n2, array3, 512, 30, 4205569, 3098553343L, 23, 1048575, 21, 1586, 1081347, 1376, 19, 113307, 1021, 32, PolynomialHeuristic.ZETA_I);
    }
    
    static int verifyingIIISize(final byte[] array, final byte[] array2, final int n, final int n2, final byte[] array3) {
        return verifying(array, array2, n, n2, array3, 1024, 48, 4206593, 4148178943L, 23, 1048575, 21, 910, 35843, 2720, 38, 1217638, 1021, 32, PolynomialHeuristic.ZETA_III_SIZE);
    }
    
    static int verifyingIIISpeed(final byte[] array, final byte[] array2, final int n, final int n2, final byte[] array3) {
        return verifying(array, array2, n, n2, array3, 1024, 48, 8404993, 4034936831L, 24, 2097151, 22, 1233, 15873, 2848, 38, 237839, 511, 32, PolynomialHeuristic.ZETA_III_SPEED);
    }
    
    static int verifyingPI(final byte[] array, final byte[] array2, final int n, final int n2, final byte[] array3) {
        return verifying(array, array2, n, n2, array3, 1024, 4, 25, 485978113, 3421990911L, 29, 2097151, 22, 554, 2848, 108, 472064468, 1, 29, PolynomialProvablySecure.ZETA_I_P);
    }
    
    static int verifyingPIII(final byte[] array, final byte[] array2, final int n, final int n2, final byte[] array3) {
        return verifying(array, array2, n, n2, array3, 2048, 5, 40, 1129725953, 861290495L, 31, 8388607, 24, 901, 6176, 180, 851423148, 15, 34, PolynomialProvablySecure.ZETA_III_P);
    }
}
