// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.qteslarnd1;

import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import java.security.SecureRandom;
import org.bouncycastle.crypto.AsymmetricCipherKeyPairGenerator;

public final class QTESLAKeyPairGenerator implements AsymmetricCipherKeyPairGenerator
{
    private SecureRandom secureRandom;
    private int securityCategory;
    
    private byte[] allocatePrivate(final int n) {
        return new byte[QTESLASecurityCategory.getPrivateSize(n)];
    }
    
    private byte[] allocatePublic(final int n) {
        return new byte[QTESLASecurityCategory.getPublicSize(n)];
    }
    
    @Override
    public AsymmetricCipherKeyPair generateKeyPair() {
        final byte[] allocatePrivate = this.allocatePrivate(this.securityCategory);
        final byte[] allocatePublic = this.allocatePublic(this.securityCategory);
        final int securityCategory = this.securityCategory;
        if (securityCategory != 0) {
            if (securityCategory != 1) {
                if (securityCategory != 2) {
                    if (securityCategory != 3) {
                        if (securityCategory != 4) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("unknown security category: ");
                            sb.append(this.securityCategory);
                            throw new IllegalArgumentException(sb.toString());
                        }
                        QTESLA.generateKeyPairIIIP(allocatePublic, allocatePrivate, this.secureRandom);
                    }
                    else {
                        QTESLA.generateKeyPairIP(allocatePublic, allocatePrivate, this.secureRandom);
                    }
                }
                else {
                    QTESLA.generateKeyPairIIISpeed(allocatePublic, allocatePrivate, this.secureRandom);
                }
            }
            else {
                QTESLA.generateKeyPairIIISize(allocatePublic, allocatePrivate, this.secureRandom);
            }
        }
        else {
            QTESLA.generateKeyPairI(allocatePublic, allocatePrivate, this.secureRandom);
        }
        return new AsymmetricCipherKeyPair(new QTESLAPublicKeyParameters(this.securityCategory, allocatePublic), new QTESLAPrivateKeyParameters(this.securityCategory, allocatePrivate));
    }
    
    @Override
    public void init(final KeyGenerationParameters keyGenerationParameters) {
        final QTESLAKeyGenerationParameters qteslaKeyGenerationParameters = (QTESLAKeyGenerationParameters)keyGenerationParameters;
        this.secureRandom = qteslaKeyGenerationParameters.getRandom();
        this.securityCategory = qteslaKeyGenerationParameters.getSecurityCategory();
    }
}
