// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.qteslarnd1;

public class QTESLASecurityCategory
{
    public static final int HEURISTIC_I = 0;
    public static final int HEURISTIC_III_SIZE = 1;
    public static final int HEURISTIC_III_SPEED = 2;
    public static final int PROVABLY_SECURE_I = 3;
    public static final int PROVABLY_SECURE_III = 4;
    
    private QTESLASecurityCategory() {
    }
    
    public static String getName(final int i) {
        if (i == 0) {
            return "qTESLA-I";
        }
        if (i == 1) {
            return "qTESLA-III-size";
        }
        if (i == 2) {
            return "qTESLA-III-speed";
        }
        if (i == 3) {
            return "qTESLA-p-I";
        }
        if (i == 4) {
            return "qTESLA-p-III";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown security category: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    static int getPrivateSize(final int i) {
        if (i == 0) {
            return 1344;
        }
        if (i == 1) {
            return 2112;
        }
        if (i == 2) {
            return 2368;
        }
        if (i == 3) {
            return 5184;
        }
        if (i == 4) {
            return 12352;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown security category: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    static int getPublicSize(final int i) {
        if (i == 0) {
            return 1504;
        }
        if (i == 1) {
            return 2976;
        }
        if (i == 2) {
            return 3104;
        }
        if (i == 3) {
            return 14880;
        }
        if (i == 4) {
            return 39712;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown security category: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    static int getSignatureSize(final int i) {
        if (i == 0) {
            return 1376;
        }
        if (i == 1) {
            return 2720;
        }
        if (i == 2 || i == 3) {
            return 2848;
        }
        if (i == 4) {
            return 6176;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown security category: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    static void validate(final int i) {
        if (i == 0 || i == 1 || i == 2 || i == 3) {
            return;
        }
        if (i == 4) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown security category: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
}
