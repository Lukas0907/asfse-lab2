// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.util;

import java.io.IOException;
import org.bouncycastle.pqc.asn1.XMSSMTPublicKey;
import org.bouncycastle.pqc.asn1.XMSSMTKeyParams;
import org.bouncycastle.pqc.crypto.xmss.XMSSMTPublicKeyParameters;
import org.bouncycastle.pqc.asn1.XMSSPublicKey;
import org.bouncycastle.pqc.asn1.XMSSKeyParams;
import org.bouncycastle.pqc.crypto.xmss.XMSSPublicKeyParameters;
import org.bouncycastle.pqc.crypto.newhope.NHPublicKeyParameters;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.pqc.asn1.SPHINCS256KeyParams;
import org.bouncycastle.pqc.asn1.PQCObjectIdentifiers;
import org.bouncycastle.pqc.crypto.sphincs.SPHINCSPublicKeyParameters;
import org.bouncycastle.pqc.crypto.qtesla.QTESLAPublicKeyParameters;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public class SubjectPublicKeyInfoFactory
{
    private SubjectPublicKeyInfoFactory() {
    }
    
    public static SubjectPublicKeyInfo createSubjectPublicKeyInfo(final AsymmetricKeyParameter asymmetricKeyParameter) throws IOException {
        if (asymmetricKeyParameter instanceof QTESLAPublicKeyParameters) {
            final QTESLAPublicKeyParameters qteslaPublicKeyParameters = (QTESLAPublicKeyParameters)asymmetricKeyParameter;
            return new SubjectPublicKeyInfo(Utils.qTeslaLookupAlgID(qteslaPublicKeyParameters.getSecurityCategory()), qteslaPublicKeyParameters.getPublicData());
        }
        if (asymmetricKeyParameter instanceof SPHINCSPublicKeyParameters) {
            final SPHINCSPublicKeyParameters sphincsPublicKeyParameters = (SPHINCSPublicKeyParameters)asymmetricKeyParameter;
            return new SubjectPublicKeyInfo(new AlgorithmIdentifier(PQCObjectIdentifiers.sphincs256, new SPHINCS256KeyParams(Utils.sphincs256LookupTreeAlgID(sphincsPublicKeyParameters.getTreeDigest()))), sphincsPublicKeyParameters.getKeyData());
        }
        if (asymmetricKeyParameter instanceof NHPublicKeyParameters) {
            return new SubjectPublicKeyInfo(new AlgorithmIdentifier(PQCObjectIdentifiers.newHope), ((NHPublicKeyParameters)asymmetricKeyParameter).getPubData());
        }
        if (asymmetricKeyParameter instanceof XMSSPublicKeyParameters) {
            final XMSSPublicKeyParameters xmssPublicKeyParameters = (XMSSPublicKeyParameters)asymmetricKeyParameter;
            return new SubjectPublicKeyInfo(new AlgorithmIdentifier(PQCObjectIdentifiers.xmss, new XMSSKeyParams(xmssPublicKeyParameters.getParameters().getHeight(), Utils.xmssLookupTreeAlgID(xmssPublicKeyParameters.getTreeDigest()))), new XMSSPublicKey(xmssPublicKeyParameters.getPublicSeed(), xmssPublicKeyParameters.getRoot()));
        }
        if (asymmetricKeyParameter instanceof XMSSMTPublicKeyParameters) {
            final XMSSMTPublicKeyParameters xmssmtPublicKeyParameters = (XMSSMTPublicKeyParameters)asymmetricKeyParameter;
            return new SubjectPublicKeyInfo(new AlgorithmIdentifier(PQCObjectIdentifiers.xmss_mt, new XMSSMTKeyParams(xmssmtPublicKeyParameters.getParameters().getHeight(), xmssmtPublicKeyParameters.getParameters().getLayers(), Utils.xmssLookupTreeAlgID(xmssmtPublicKeyParameters.getTreeDigest()))), new XMSSMTPublicKey(xmssmtPublicKeyParameters.getPublicSeed(), xmssmtPublicKeyParameters.getRoot()));
        }
        throw new IOException("key parameters not recognized");
    }
}
