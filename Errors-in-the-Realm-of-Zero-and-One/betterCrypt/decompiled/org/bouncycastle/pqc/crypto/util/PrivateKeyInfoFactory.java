// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.util;

import org.bouncycastle.pqc.crypto.xmss.BDSStateMap;
import org.bouncycastle.pqc.asn1.XMSSMTPrivateKey;
import org.bouncycastle.pqc.crypto.xmss.BDS;
import org.bouncycastle.pqc.crypto.xmss.XMSSUtil;
import org.bouncycastle.pqc.asn1.XMSSPrivateKey;
import org.bouncycastle.pqc.asn1.XMSSMTKeyParams;
import org.bouncycastle.pqc.crypto.xmss.XMSSMTPrivateKeyParameters;
import org.bouncycastle.pqc.asn1.XMSSKeyParams;
import org.bouncycastle.pqc.crypto.xmss.XMSSPrivateKeyParameters;
import org.bouncycastle.util.Pack;
import org.bouncycastle.pqc.crypto.newhope.NHPrivateKeyParameters;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.pqc.asn1.SPHINCS256KeyParams;
import org.bouncycastle.pqc.asn1.PQCObjectIdentifiers;
import org.bouncycastle.pqc.crypto.sphincs.SPHINCSPrivateKeyParameters;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.pqc.crypto.qtesla.QTESLAPrivateKeyParameters;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public class PrivateKeyInfoFactory
{
    private PrivateKeyInfoFactory() {
    }
    
    public static PrivateKeyInfo createPrivateKeyInfo(final AsymmetricKeyParameter asymmetricKeyParameter) throws IOException {
        return createPrivateKeyInfo(asymmetricKeyParameter, null);
    }
    
    public static PrivateKeyInfo createPrivateKeyInfo(final AsymmetricKeyParameter asymmetricKeyParameter, final ASN1Set set) throws IOException {
        if (asymmetricKeyParameter instanceof QTESLAPrivateKeyParameters) {
            final QTESLAPrivateKeyParameters qteslaPrivateKeyParameters = (QTESLAPrivateKeyParameters)asymmetricKeyParameter;
            return new PrivateKeyInfo(Utils.qTeslaLookupAlgID(qteslaPrivateKeyParameters.getSecurityCategory()), new DEROctetString(qteslaPrivateKeyParameters.getSecret()), set);
        }
        if (asymmetricKeyParameter instanceof SPHINCSPrivateKeyParameters) {
            final SPHINCSPrivateKeyParameters sphincsPrivateKeyParameters = (SPHINCSPrivateKeyParameters)asymmetricKeyParameter;
            return new PrivateKeyInfo(new AlgorithmIdentifier(PQCObjectIdentifiers.sphincs256, new SPHINCS256KeyParams(Utils.sphincs256LookupTreeAlgID(sphincsPrivateKeyParameters.getTreeDigest()))), new DEROctetString(sphincsPrivateKeyParameters.getKeyData()));
        }
        if (asymmetricKeyParameter instanceof NHPrivateKeyParameters) {
            final NHPrivateKeyParameters nhPrivateKeyParameters = (NHPrivateKeyParameters)asymmetricKeyParameter;
            final AlgorithmIdentifier algorithmIdentifier = new AlgorithmIdentifier(PQCObjectIdentifiers.newHope);
            final short[] secData = nhPrivateKeyParameters.getSecData();
            final byte[] array = new byte[secData.length * 2];
            for (int i = 0; i != secData.length; ++i) {
                Pack.shortToLittleEndian(secData[i], array, i * 2);
            }
            return new PrivateKeyInfo(algorithmIdentifier, new DEROctetString(array));
        }
        if (asymmetricKeyParameter instanceof XMSSPrivateKeyParameters) {
            final XMSSPrivateKeyParameters xmssPrivateKeyParameters = (XMSSPrivateKeyParameters)asymmetricKeyParameter;
            return new PrivateKeyInfo(new AlgorithmIdentifier(PQCObjectIdentifiers.xmss, new XMSSKeyParams(xmssPrivateKeyParameters.getParameters().getHeight(), Utils.xmssLookupTreeAlgID(xmssPrivateKeyParameters.getTreeDigest()))), xmssCreateKeyStructure(xmssPrivateKeyParameters));
        }
        if (asymmetricKeyParameter instanceof XMSSMTPrivateKeyParameters) {
            final XMSSMTPrivateKeyParameters xmssmtPrivateKeyParameters = (XMSSMTPrivateKeyParameters)asymmetricKeyParameter;
            return new PrivateKeyInfo(new AlgorithmIdentifier(PQCObjectIdentifiers.xmss_mt, new XMSSMTKeyParams(xmssmtPrivateKeyParameters.getParameters().getHeight(), xmssmtPrivateKeyParameters.getParameters().getLayers(), Utils.xmssLookupTreeAlgID(xmssmtPrivateKeyParameters.getTreeDigest()))), xmssmtCreateKeyStructure(xmssmtPrivateKeyParameters));
        }
        throw new IOException("key parameters not recognized");
    }
    
    private static XMSSPrivateKey xmssCreateKeyStructure(final XMSSPrivateKeyParameters xmssPrivateKeyParameters) throws IOException {
        final byte[] encoded = xmssPrivateKeyParameters.getEncoded();
        final int treeDigestSize = xmssPrivateKeyParameters.getParameters().getTreeDigestSize();
        final int height = xmssPrivateKeyParameters.getParameters().getHeight();
        final int n = (int)XMSSUtil.bytesToXBigEndian(encoded, 0, 4);
        if (XMSSUtil.isIndexValid(height, n)) {
            final byte[] bytesAtOffset = XMSSUtil.extractBytesAtOffset(encoded, 4, treeDigestSize);
            final int n2 = 4 + treeDigestSize;
            final byte[] bytesAtOffset2 = XMSSUtil.extractBytesAtOffset(encoded, n2, treeDigestSize);
            final int n3 = n2 + treeDigestSize;
            final byte[] bytesAtOffset3 = XMSSUtil.extractBytesAtOffset(encoded, n3, treeDigestSize);
            final int n4 = n3 + treeDigestSize;
            final byte[] bytesAtOffset4 = XMSSUtil.extractBytesAtOffset(encoded, n4, treeDigestSize);
            final int n5 = n4 + treeDigestSize;
            final byte[] bytesAtOffset5 = XMSSUtil.extractBytesAtOffset(encoded, n5, encoded.length - n5);
            try {
                final BDS bds = (BDS)XMSSUtil.deserialize(bytesAtOffset5, BDS.class);
                if (bds.getMaxIndex() != (1 << height) - 1) {
                    return new XMSSPrivateKey(n, bytesAtOffset, bytesAtOffset2, bytesAtOffset3, bytesAtOffset4, bytesAtOffset5, bds.getMaxIndex());
                }
                return new XMSSPrivateKey(n, bytesAtOffset, bytesAtOffset2, bytesAtOffset3, bytesAtOffset4, bytesAtOffset5);
            }
            catch (ClassNotFoundException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("cannot parse BDS: ");
                sb.append(ex.getMessage());
                throw new IOException(sb.toString());
            }
        }
        throw new IllegalArgumentException("index out of bounds");
    }
    
    private static XMSSMTPrivateKey xmssmtCreateKeyStructure(final XMSSMTPrivateKeyParameters xmssmtPrivateKeyParameters) throws IOException {
        final byte[] encoded = xmssmtPrivateKeyParameters.getEncoded();
        final int treeDigestSize = xmssmtPrivateKeyParameters.getParameters().getTreeDigestSize();
        final int height = xmssmtPrivateKeyParameters.getParameters().getHeight();
        final int n = (height + 7) / 8;
        final long n2 = (int)XMSSUtil.bytesToXBigEndian(encoded, 0, n);
        if (XMSSUtil.isIndexValid(height, n2)) {
            final int n3 = n + 0;
            final byte[] bytesAtOffset = XMSSUtil.extractBytesAtOffset(encoded, n3, treeDigestSize);
            final int n4 = n3 + treeDigestSize;
            final byte[] bytesAtOffset2 = XMSSUtil.extractBytesAtOffset(encoded, n4, treeDigestSize);
            final int n5 = n4 + treeDigestSize;
            final byte[] bytesAtOffset3 = XMSSUtil.extractBytesAtOffset(encoded, n5, treeDigestSize);
            final int n6 = n5 + treeDigestSize;
            final byte[] bytesAtOffset4 = XMSSUtil.extractBytesAtOffset(encoded, n6, treeDigestSize);
            final int n7 = n6 + treeDigestSize;
            final byte[] bytesAtOffset5 = XMSSUtil.extractBytesAtOffset(encoded, n7, encoded.length - n7);
            try {
                final BDSStateMap bdsStateMap = (BDSStateMap)XMSSUtil.deserialize(bytesAtOffset5, BDSStateMap.class);
                if (bdsStateMap.getMaxIndex() != (1L << height) - 1L) {
                    return new XMSSMTPrivateKey(n2, bytesAtOffset, bytesAtOffset2, bytesAtOffset3, bytesAtOffset4, bytesAtOffset5, bdsStateMap.getMaxIndex());
                }
                return new XMSSMTPrivateKey(n2, bytesAtOffset, bytesAtOffset2, bytesAtOffset3, bytesAtOffset4, bytesAtOffset5);
            }
            catch (ClassNotFoundException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("cannot parse BDSStateMap: ");
                sb.append(ex.getMessage());
                throw new IOException(sb.toString());
            }
        }
        throw new IllegalArgumentException("index out of bounds");
    }
}
