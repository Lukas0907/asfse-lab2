// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.util;

import org.bouncycastle.pqc.asn1.SPHINCS256KeyParams;
import org.bouncycastle.crypto.digests.SHAKEDigest;
import org.bouncycastle.crypto.digests.SHA512Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.util.Integers;
import java.util.HashMap;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.pqc.asn1.PQCObjectIdentifiers;
import java.util.Map;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;

class Utils
{
    static final AlgorithmIdentifier AlgID_qTESLA_p_I;
    static final AlgorithmIdentifier AlgID_qTESLA_p_III;
    static final AlgorithmIdentifier SPHINCS_SHA3_256;
    static final AlgorithmIdentifier SPHINCS_SHA512_256;
    static final AlgorithmIdentifier XMSS_SHA256;
    static final AlgorithmIdentifier XMSS_SHA512;
    static final AlgorithmIdentifier XMSS_SHAKE128;
    static final AlgorithmIdentifier XMSS_SHAKE256;
    static final Map categories;
    
    static {
        AlgID_qTESLA_p_I = new AlgorithmIdentifier(PQCObjectIdentifiers.qTESLA_p_I);
        AlgID_qTESLA_p_III = new AlgorithmIdentifier(PQCObjectIdentifiers.qTESLA_p_III);
        SPHINCS_SHA3_256 = new AlgorithmIdentifier(NISTObjectIdentifiers.id_sha3_256);
        SPHINCS_SHA512_256 = new AlgorithmIdentifier(NISTObjectIdentifiers.id_sha512_256);
        XMSS_SHA256 = new AlgorithmIdentifier(NISTObjectIdentifiers.id_sha256);
        XMSS_SHA512 = new AlgorithmIdentifier(NISTObjectIdentifiers.id_sha512);
        XMSS_SHAKE128 = new AlgorithmIdentifier(NISTObjectIdentifiers.id_shake128);
        XMSS_SHAKE256 = new AlgorithmIdentifier(NISTObjectIdentifiers.id_shake256);
        (categories = new HashMap()).put(PQCObjectIdentifiers.qTESLA_p_I, Integers.valueOf(5));
        Utils.categories.put(PQCObjectIdentifiers.qTESLA_p_III, Integers.valueOf(6));
    }
    
    static Digest getDigest(final ASN1ObjectIdentifier obj) {
        if (obj.equals(NISTObjectIdentifiers.id_sha256)) {
            return new SHA256Digest();
        }
        if (obj.equals(NISTObjectIdentifiers.id_sha512)) {
            return new SHA512Digest();
        }
        if (obj.equals(NISTObjectIdentifiers.id_shake128)) {
            return new SHAKEDigest(128);
        }
        if (obj.equals(NISTObjectIdentifiers.id_shake256)) {
            return new SHAKEDigest(256);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unrecognized digest OID: ");
        sb.append(obj);
        throw new IllegalArgumentException(sb.toString());
    }
    
    static AlgorithmIdentifier qTeslaLookupAlgID(final int i) {
        if (i == 5) {
            return Utils.AlgID_qTESLA_p_I;
        }
        if (i == 6) {
            return Utils.AlgID_qTESLA_p_III;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown security category: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    static int qTeslaLookupSecurityCategory(final AlgorithmIdentifier algorithmIdentifier) {
        return Utils.categories.get(algorithmIdentifier.getAlgorithm());
    }
    
    static AlgorithmIdentifier sphincs256LookupTreeAlgID(final String str) {
        if (str.equals("SHA3-256")) {
            return Utils.SPHINCS_SHA3_256;
        }
        if (str.equals("SHA-512/256")) {
            return Utils.SPHINCS_SHA512_256;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown tree digest: ");
        sb.append(str);
        throw new IllegalArgumentException(sb.toString());
    }
    
    static String sphincs256LookupTreeAlgName(final SPHINCS256KeyParams sphincs256KeyParams) {
        final AlgorithmIdentifier treeDigest = sphincs256KeyParams.getTreeDigest();
        if (treeDigest.getAlgorithm().equals(Utils.SPHINCS_SHA3_256.getAlgorithm())) {
            return "SHA3-256";
        }
        if (treeDigest.getAlgorithm().equals(Utils.SPHINCS_SHA512_256.getAlgorithm())) {
            return "SHA-512/256";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown tree digest: ");
        sb.append(treeDigest.getAlgorithm());
        throw new IllegalArgumentException(sb.toString());
    }
    
    static AlgorithmIdentifier xmssLookupTreeAlgID(final String str) {
        if (str.equals("SHA-256")) {
            return Utils.XMSS_SHA256;
        }
        if (str.equals("SHA-512")) {
            return Utils.XMSS_SHA512;
        }
        if (str.equals("SHAKE128")) {
            return Utils.XMSS_SHAKE128;
        }
        if (str.equals("SHAKE256")) {
            return Utils.XMSS_SHAKE256;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown tree digest: ");
        sb.append(str);
        throw new IllegalArgumentException(sb.toString());
    }
}
