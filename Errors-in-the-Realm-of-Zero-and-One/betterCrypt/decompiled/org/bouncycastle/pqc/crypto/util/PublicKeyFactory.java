// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.util;

import org.bouncycastle.pqc.crypto.xmss.XMSSMTPublicKeyParameters;
import org.bouncycastle.pqc.crypto.xmss.XMSSMTParameters;
import org.bouncycastle.pqc.asn1.XMSSMTKeyParams;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.pqc.crypto.xmss.XMSSPublicKeyParameters;
import org.bouncycastle.pqc.crypto.xmss.XMSSParameters;
import org.bouncycastle.pqc.asn1.XMSSPublicKey;
import org.bouncycastle.pqc.asn1.XMSSKeyParams;
import org.bouncycastle.pqc.crypto.sphincs.SPHINCSPublicKeyParameters;
import org.bouncycastle.pqc.asn1.SPHINCS256KeyParams;
import org.bouncycastle.pqc.crypto.qtesla.QTESLAPublicKeyParameters;
import org.bouncycastle.pqc.crypto.newhope.NHPublicKeyParameters;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import java.io.IOException;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import java.io.InputStream;
import org.bouncycastle.pqc.asn1.PQCObjectIdentifiers;
import java.util.HashMap;
import java.util.Map;

public class PublicKeyFactory
{
    private static Map converters;
    
    static {
        (PublicKeyFactory.converters = new HashMap()).put(PQCObjectIdentifiers.qTESLA_p_I, new QTeslaConverter());
        PublicKeyFactory.converters.put(PQCObjectIdentifiers.qTESLA_p_III, new QTeslaConverter());
        PublicKeyFactory.converters.put(PQCObjectIdentifiers.sphincs256, new SPHINCSConverter());
        PublicKeyFactory.converters.put(PQCObjectIdentifiers.newHope, new NHConverter());
        PublicKeyFactory.converters.put(PQCObjectIdentifiers.xmss, new XMSSConverter());
        PublicKeyFactory.converters.put(PQCObjectIdentifiers.xmss_mt, new XMSSMTConverter());
    }
    
    public static AsymmetricKeyParameter createKey(final InputStream inputStream) throws IOException {
        return createKey(SubjectPublicKeyInfo.getInstance(new ASN1InputStream(inputStream).readObject()));
    }
    
    public static AsymmetricKeyParameter createKey(final SubjectPublicKeyInfo subjectPublicKeyInfo) throws IOException {
        return createKey(subjectPublicKeyInfo, null);
    }
    
    public static AsymmetricKeyParameter createKey(final SubjectPublicKeyInfo subjectPublicKeyInfo, final Object o) throws IOException {
        final AlgorithmIdentifier algorithm = subjectPublicKeyInfo.getAlgorithm();
        final SubjectPublicKeyInfoConverter subjectPublicKeyInfoConverter = PublicKeyFactory.converters.get(algorithm.getAlgorithm());
        if (subjectPublicKeyInfoConverter != null) {
            return subjectPublicKeyInfoConverter.getPublicKeyParameters(subjectPublicKeyInfo, o);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("algorithm identifier in public key not recognised: ");
        sb.append(algorithm.getAlgorithm());
        throw new IOException(sb.toString());
    }
    
    public static AsymmetricKeyParameter createKey(final byte[] array) throws IOException {
        return createKey(SubjectPublicKeyInfo.getInstance(ASN1Primitive.fromByteArray(array)));
    }
    
    private static class NHConverter extends SubjectPublicKeyInfoConverter
    {
        @Override
        AsymmetricKeyParameter getPublicKeyParameters(final SubjectPublicKeyInfo subjectPublicKeyInfo, final Object o) throws IOException {
            return new NHPublicKeyParameters(subjectPublicKeyInfo.getPublicKeyData().getBytes());
        }
    }
    
    private static class QTeslaConverter extends SubjectPublicKeyInfoConverter
    {
        @Override
        AsymmetricKeyParameter getPublicKeyParameters(final SubjectPublicKeyInfo subjectPublicKeyInfo, final Object o) throws IOException {
            return new QTESLAPublicKeyParameters(Utils.qTeslaLookupSecurityCategory(subjectPublicKeyInfo.getAlgorithm()), subjectPublicKeyInfo.getPublicKeyData().getOctets());
        }
    }
    
    private static class SPHINCSConverter extends SubjectPublicKeyInfoConverter
    {
        @Override
        AsymmetricKeyParameter getPublicKeyParameters(final SubjectPublicKeyInfo subjectPublicKeyInfo, final Object o) throws IOException {
            return new SPHINCSPublicKeyParameters(subjectPublicKeyInfo.getPublicKeyData().getBytes(), Utils.sphincs256LookupTreeAlgName(SPHINCS256KeyParams.getInstance(subjectPublicKeyInfo.getAlgorithm().getParameters())));
        }
    }
    
    private abstract static class SubjectPublicKeyInfoConverter
    {
        abstract AsymmetricKeyParameter getPublicKeyParameters(final SubjectPublicKeyInfo p0, final Object p1) throws IOException;
    }
    
    private static class XMSSConverter extends SubjectPublicKeyInfoConverter
    {
        @Override
        AsymmetricKeyParameter getPublicKeyParameters(final SubjectPublicKeyInfo subjectPublicKeyInfo, final Object o) throws IOException {
            final XMSSKeyParams instance = XMSSKeyParams.getInstance(subjectPublicKeyInfo.getAlgorithm().getParameters());
            final ASN1ObjectIdentifier algorithm = instance.getTreeDigest().getAlgorithm();
            final XMSSPublicKey instance2 = XMSSPublicKey.getInstance(subjectPublicKeyInfo.parsePublicKey());
            return new XMSSPublicKeyParameters.Builder(new XMSSParameters(instance.getHeight(), Utils.getDigest(algorithm))).withPublicSeed(instance2.getPublicSeed()).withRoot(instance2.getRoot()).build();
        }
    }
    
    private static class XMSSMTConverter extends SubjectPublicKeyInfoConverter
    {
        @Override
        AsymmetricKeyParameter getPublicKeyParameters(final SubjectPublicKeyInfo subjectPublicKeyInfo, final Object o) throws IOException {
            final XMSSMTKeyParams instance = XMSSMTKeyParams.getInstance(subjectPublicKeyInfo.getAlgorithm().getParameters());
            final ASN1ObjectIdentifier algorithm = instance.getTreeDigest().getAlgorithm();
            final XMSSPublicKey instance2 = XMSSPublicKey.getInstance(subjectPublicKeyInfo.parsePublicKey());
            return new XMSSMTPublicKeyParameters.Builder(new XMSSMTParameters(instance.getHeight(), instance.getLayers(), Utils.getDigest(algorithm))).withPublicSeed(instance2.getPublicSeed()).withRoot(instance2.getRoot()).build();
        }
    }
}
