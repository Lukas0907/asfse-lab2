// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.util;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.pqc.crypto.xmss.BDSStateMap;
import org.bouncycastle.pqc.crypto.xmss.XMSSMTPrivateKeyParameters;
import org.bouncycastle.pqc.crypto.xmss.XMSSMTParameters;
import org.bouncycastle.pqc.asn1.XMSSMTPrivateKey;
import org.bouncycastle.pqc.asn1.XMSSMTKeyParams;
import org.bouncycastle.pqc.asn1.PQCObjectIdentifiers;
import org.bouncycastle.pqc.crypto.xmss.XMSSUtil;
import org.bouncycastle.pqc.crypto.xmss.BDS;
import org.bouncycastle.pqc.crypto.xmss.XMSSPrivateKeyParameters;
import org.bouncycastle.pqc.crypto.xmss.XMSSParameters;
import org.bouncycastle.pqc.asn1.XMSSPrivateKey;
import org.bouncycastle.pqc.asn1.XMSSKeyParams;
import org.bouncycastle.pqc.crypto.newhope.NHPrivateKeyParameters;
import org.bouncycastle.pqc.crypto.sphincs.SPHINCSPrivateKeyParameters;
import org.bouncycastle.pqc.asn1.SPHINCS256KeyParams;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.pqc.crypto.qtesla.QTESLAPrivateKeyParameters;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.bc.BCObjectIdentifiers;
import java.io.IOException;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import java.io.InputStream;
import org.bouncycastle.util.Pack;

public class PrivateKeyFactory
{
    private static short[] convert(final byte[] array) {
        final short[] array2 = new short[array.length / 2];
        for (int i = 0; i != array2.length; ++i) {
            array2[i] = Pack.littleEndianToShort(array, i * 2);
        }
        return array2;
    }
    
    public static AsymmetricKeyParameter createKey(final InputStream inputStream) throws IOException {
        return createKey(PrivateKeyInfo.getInstance(new ASN1InputStream(inputStream).readObject()));
    }
    
    public static AsymmetricKeyParameter createKey(final PrivateKeyInfo privateKeyInfo) throws IOException {
        final ASN1ObjectIdentifier algorithm = privateKeyInfo.getPrivateKeyAlgorithm().getAlgorithm();
        if (algorithm.on(BCObjectIdentifiers.qTESLA)) {
            return new QTESLAPrivateKeyParameters(Utils.qTeslaLookupSecurityCategory(privateKeyInfo.getPrivateKeyAlgorithm()), ASN1OctetString.getInstance(privateKeyInfo.parsePrivateKey()).getOctets());
        }
        if (algorithm.equals(BCObjectIdentifiers.sphincs256)) {
            return new SPHINCSPrivateKeyParameters(ASN1OctetString.getInstance(privateKeyInfo.parsePrivateKey()).getOctets(), Utils.sphincs256LookupTreeAlgName(SPHINCS256KeyParams.getInstance(privateKeyInfo.getPrivateKeyAlgorithm().getParameters())));
        }
        if (algorithm.equals(BCObjectIdentifiers.newHope)) {
            return new NHPrivateKeyParameters(convert(ASN1OctetString.getInstance(privateKeyInfo.parsePrivateKey()).getOctets()));
        }
        if (algorithm.equals(BCObjectIdentifiers.xmss)) {
            final XMSSKeyParams instance = XMSSKeyParams.getInstance(privateKeyInfo.getPrivateKeyAlgorithm().getParameters());
            final ASN1ObjectIdentifier algorithm2 = instance.getTreeDigest().getAlgorithm();
            final XMSSPrivateKey instance2 = XMSSPrivateKey.getInstance(privateKeyInfo.parsePrivateKey());
            try {
                final XMSSPrivateKeyParameters.Builder withRoot = new XMSSPrivateKeyParameters.Builder(new XMSSParameters(instance.getHeight(), Utils.getDigest(algorithm2))).withIndex(instance2.getIndex()).withSecretKeySeed(instance2.getSecretKeySeed()).withSecretKeyPRF(instance2.getSecretKeyPRF()).withPublicSeed(instance2.getPublicSeed()).withRoot(instance2.getRoot());
                if (instance2.getVersion() != 0) {
                    withRoot.withMaxIndex(instance2.getMaxIndex());
                }
                if (instance2.getBdsState() != null) {
                    withRoot.withBDSState(((BDS)XMSSUtil.deserialize(instance2.getBdsState(), BDS.class)).withWOTSDigest(algorithm2));
                }
                return withRoot.build();
            }
            catch (ClassNotFoundException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("ClassNotFoundException processing BDS state: ");
                sb.append(ex.getMessage());
                throw new IOException(sb.toString());
            }
        }
        if (algorithm.equals(PQCObjectIdentifiers.xmss_mt)) {
            final XMSSMTKeyParams instance3 = XMSSMTKeyParams.getInstance(privateKeyInfo.getPrivateKeyAlgorithm().getParameters());
            final ASN1ObjectIdentifier algorithm3 = instance3.getTreeDigest().getAlgorithm();
            try {
                final XMSSMTPrivateKey instance4 = XMSSMTPrivateKey.getInstance(privateKeyInfo.parsePrivateKey());
                final XMSSMTPrivateKeyParameters.Builder withRoot2 = new XMSSMTPrivateKeyParameters.Builder(new XMSSMTParameters(instance3.getHeight(), instance3.getLayers(), Utils.getDigest(algorithm3))).withIndex(instance4.getIndex()).withSecretKeySeed(instance4.getSecretKeySeed()).withSecretKeyPRF(instance4.getSecretKeyPRF()).withPublicSeed(instance4.getPublicSeed()).withRoot(instance4.getRoot());
                if (instance4.getVersion() != 0) {
                    withRoot2.withMaxIndex(instance4.getMaxIndex());
                }
                if (instance4.getBdsState() != null) {
                    withRoot2.withBDSState(((BDSStateMap)XMSSUtil.deserialize(instance4.getBdsState(), BDSStateMap.class)).withWOTSDigest(algorithm3));
                }
                return withRoot2.build();
            }
            catch (ClassNotFoundException ex2) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("ClassNotFoundException processing BDS state: ");
                sb2.append(ex2.getMessage());
                throw new IOException(sb2.toString());
            }
        }
        throw new RuntimeException("algorithm identifier in private key not recognised");
    }
    
    public static AsymmetricKeyParameter createKey(final byte[] array) throws IOException {
        return createKey(PrivateKeyInfo.getInstance(ASN1Primitive.fromByteArray(array)));
    }
}
