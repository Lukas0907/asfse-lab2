// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto;

import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public interface StateAwareMessageSigner extends MessageSigner
{
    AsymmetricKeyParameter getUpdatedPrivateKey();
}
