// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.rainbow;

import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.pqc.crypto.rainbow.util.GF2Field;
import java.security.SecureRandom;
import org.bouncycastle.pqc.crypto.rainbow.util.ComputeInField;
import org.bouncycastle.pqc.crypto.MessageSigner;

public class RainbowSigner implements MessageSigner
{
    private static final int MAXITS = 65536;
    private ComputeInField cf;
    RainbowKeyParameters key;
    private SecureRandom random;
    int signableDocumentLength;
    private short[] x;
    
    public RainbowSigner() {
        this.cf = new ComputeInField();
    }
    
    private short[] initSign(final Layer[] array, short[] array2) {
        final short[] array3 = new short[array2.length];
        array2 = this.cf.addVect(((RainbowPrivateKeyParameters)this.key).getB1(), array2);
        array2 = this.cf.multiplyMatrix(((RainbowPrivateKeyParameters)this.key).getInvA1(), array2);
        for (int i = 0; i < array[0].getVi(); ++i) {
            this.x[i] = (short)this.random.nextInt();
            final short[] x = this.x;
            x[i] &= 0xFF;
        }
        return array2;
    }
    
    private short[] makeMessageRepresentative(final byte[] array) {
        final short[] array2 = new short[this.signableDocumentLength];
        int i = 0;
        int n = 0;
        while (i < array.length) {
            array2[i] = array[n];
            array2[i] &= 0xFF;
            ++n;
            if (++i >= array2.length) {
                return array2;
            }
        }
        return array2;
    }
    
    private short[] verifySignatureIntern(final short[] array) {
        final short[][] coeffQuadratic = ((RainbowPublicKeyParameters)this.key).getCoeffQuadratic();
        final short[][] coeffSingular = ((RainbowPublicKeyParameters)this.key).getCoeffSingular();
        final short[] coeffScalar = ((RainbowPublicKeyParameters)this.key).getCoeffScalar();
        final short[] array2 = new short[coeffQuadratic.length];
        final int length = coeffSingular[0].length;
        for (int i = 0; i < coeffQuadratic.length; ++i) {
            int n;
            for (int j = n = 0; j < length; ++j) {
                for (int k = j; k < length; ++k) {
                    array2[i] = GF2Field.addElem(array2[i], GF2Field.multElem(coeffQuadratic[i][n], GF2Field.multElem(array[j], array[k])));
                    ++n;
                }
                array2[i] = GF2Field.addElem(array2[i], GF2Field.multElem(coeffSingular[i][j], array[j]));
            }
            array2[i] = GF2Field.addElem(array2[i], coeffScalar[i]);
        }
        return array2;
    }
    
    @Override
    public byte[] generateSignature(byte[] messageRepresentative) {
        final Layer[] layers = ((RainbowPrivateKeyParameters)this.key).getLayers();
        final int length = layers.length;
        this.x = new short[((RainbowPrivateKeyParameters)this.key).getInvA2().length];
        final byte[] array = new byte[layers[length - 1].getViNext()];
        messageRepresentative = (byte[])this.makeMessageRepresentative(messageRepresentative);
        int n = 0;
        while (true) {
        Label_0293_Outer:
            while (true) {
                int n3 = 0;
                Label_0342: {
                    while (true) {
                        try {
                            while (true) {
                                int n2;
                                do {
                                    final short[] initSign = this.initSign(layers, (short[])messageRepresentative);
                                    int n4;
                                    n3 = (n4 = 0);
                                    if (n3 < length) {
                                        final short[] array2 = new short[layers[n3].getOi()];
                                        final short[] array3 = new short[layers[n3].getOi()];
                                        for (int i = 0; i < layers[n3].getOi(); ++i) {
                                            array2[i] = initSign[n4];
                                            ++n4;
                                        }
                                        final short[] solveEquation = this.cf.solveEquation(layers[n3].plugInVinegars(this.x), array2);
                                        if (solveEquation != null) {
                                            for (int j = 0; j < solveEquation.length; ++j) {
                                                this.x[layers[n3].getVi() + j] = solveEquation[j];
                                            }
                                            break Label_0342;
                                        }
                                        throw new Exception("LES is not solveable!");
                                    }
                                    else {
                                        final short[] multiplyMatrix = this.cf.multiplyMatrix(((RainbowPrivateKeyParameters)this.key).getInvA2(), this.cf.addVect(((RainbowPrivateKeyParameters)this.key).getB2(), this.x));
                                        for (int k = 0; k < array.length; ++k) {
                                            array[k] = (byte)multiplyMatrix[k];
                                        }
                                        final boolean b = true;
                                        n2 = n;
                                        if (b) {
                                            break;
                                        }
                                        n2 = n + 1;
                                    }
                                } while ((n = n2) < 65536);
                                if (n2 != 65536) {
                                    return array;
                                }
                                throw new IllegalStateException("unable to generate signature - LES not solvable");
                                final boolean b = false;
                                continue Label_0293_Outer;
                            }
                        }
                        catch (Exception ex) {}
                        continue;
                    }
                }
                ++n3;
                continue;
            }
        }
    }
    
    @Override
    public void init(final boolean b, final CipherParameters cipherParameters) {
        Label_0063: {
            RainbowKeyParameters key;
            if (b) {
                if (cipherParameters instanceof ParametersWithRandom) {
                    final ParametersWithRandom parametersWithRandom = (ParametersWithRandom)cipherParameters;
                    this.random = parametersWithRandom.getRandom();
                    this.key = (RainbowPrivateKeyParameters)parametersWithRandom.getParameters();
                    break Label_0063;
                }
                this.random = CryptoServicesRegistrar.getSecureRandom();
                key = (RainbowPrivateKeyParameters)cipherParameters;
            }
            else {
                key = (RainbowPublicKeyParameters)cipherParameters;
            }
            this.key = key;
        }
        this.signableDocumentLength = this.key.getDocLength();
    }
    
    @Override
    public boolean verifySignature(final byte[] array, final byte[] array2) {
        final short[] array3 = new short[array2.length];
        for (int i = 0; i < array2.length; ++i) {
            array3[i] = (short)(array2[i] & 0xFF);
        }
        final short[] messageRepresentative = this.makeMessageRepresentative(array);
        final short[] verifySignatureIntern = this.verifySignatureIntern(array3);
        if (messageRepresentative.length != verifySignatureIntern.length) {
            return false;
        }
        boolean b = true;
        for (int j = 0; j < messageRepresentative.length; ++j) {
            b = (b && messageRepresentative[j] == verifySignatureIntern[j]);
        }
        return b;
    }
}
