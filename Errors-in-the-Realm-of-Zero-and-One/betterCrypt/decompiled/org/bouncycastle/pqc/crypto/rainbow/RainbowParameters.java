// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.rainbow;

import org.bouncycastle.crypto.CipherParameters;

public class RainbowParameters implements CipherParameters
{
    private final int[] DEFAULT_VI;
    private int[] vi;
    
    public RainbowParameters() {
        this.DEFAULT_VI = new int[] { 6, 12, 17, 22, 33 };
        this.vi = this.DEFAULT_VI;
    }
    
    public RainbowParameters(final int[] vi) {
        this.DEFAULT_VI = new int[] { 6, 12, 17, 22, 33 };
        this.vi = vi;
        this.checkParams();
    }
    
    private void checkParams() {
        final int[] vi = this.vi;
        if (vi == null) {
            throw new IllegalArgumentException("no layers defined.");
        }
        if (vi.length <= 1) {
            throw new IllegalArgumentException("Rainbow needs at least 1 layer, such that v1 < v2.");
        }
        int n = 0;
        while (true) {
            final int[] vi2 = this.vi;
            if (n >= vi2.length - 1) {
                return;
            }
            final int n2 = vi2[n];
            ++n;
            if (n2 < vi2[n]) {
                continue;
            }
            throw new IllegalArgumentException("v[i] has to be smaller than v[i+1]");
        }
    }
    
    public int getDocLength() {
        final int[] vi = this.vi;
        return vi[vi.length - 1] - vi[0];
    }
    
    public int getNumOfLayers() {
        return this.vi.length - 1;
    }
    
    public int[] getVi() {
        return this.vi;
    }
}
