// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.mceliece;

import org.bouncycastle.pqc.math.linearalgebra.BigIntUtils;
import org.bouncycastle.pqc.math.linearalgebra.IntegerFunctions;
import org.bouncycastle.pqc.math.linearalgebra.GF2Vector;
import java.math.BigInteger;

final class Conversions
{
    private static final BigInteger ONE;
    private static final BigInteger ZERO;
    
    static {
        ZERO = BigInteger.valueOf(0L);
        ONE = BigInteger.valueOf(1L);
    }
    
    private Conversions() {
    }
    
    public static byte[] decode(final int n, int i, final GF2Vector gf2Vector) {
        if (gf2Vector.getLength() == n && gf2Vector.getHammingWeight() == i) {
            final int[] vecArray = gf2Vector.getVecArray();
            BigInteger bigInteger = IntegerFunctions.binomial(n, i);
            BigInteger zero = Conversions.ZERO;
            final int n2 = 0;
            int n3 = n;
            int n4 = i;
            BigInteger divide;
            int n5;
            BigInteger add;
            int n6;
            for (i = n2; i < n; ++i, zero = add, n4 = n6, n3 = n5) {
                divide = bigInteger.multiply(BigInteger.valueOf(n3 - n4)).divide(BigInteger.valueOf(n3));
                n5 = n3 - 1;
                bigInteger = divide;
                add = zero;
                n6 = n4;
                if ((vecArray[i >> 5] & 1 << (i & 0x1F)) != 0x0) {
                    add = zero.add(divide);
                    n6 = n4 - 1;
                    if (n5 == n6) {
                        bigInteger = Conversions.ONE;
                    }
                    else {
                        bigInteger = divide.multiply(BigInteger.valueOf(n6 + 1)).divide(BigInteger.valueOf(n5 - n6));
                    }
                }
            }
            return BigIntUtils.toMinimalByteArray(zero);
        }
        throw new IllegalArgumentException("vector has wrong length or hamming weight");
    }
    
    public static GF2Vector encode(final int n, int i, final byte[] magnitude) {
        if (n < i) {
            throw new IllegalArgumentException("n < t");
        }
        final BigInteger binomial = IntegerFunctions.binomial(n, i);
        BigInteger val = new BigInteger(1, magnitude);
        if (val.compareTo(binomial) < 0) {
            final GF2Vector gf2Vector = new GF2Vector(n);
            final int n2 = 0;
            int n3 = i;
            int n4 = n;
            i = n2;
            BigInteger bigInteger = binomial;
            while (i < n) {
                final BigInteger divide = bigInteger.multiply(BigInteger.valueOf(n4 - n3)).divide(BigInteger.valueOf(n4));
                final int n5 = n4 - 1;
                bigInteger = divide;
                int n6 = n3;
                BigInteger subtract = val;
                if (divide.compareTo(val) <= 0) {
                    gf2Vector.setBit(i);
                    subtract = val.subtract(divide);
                    n6 = n3 - 1;
                    if (n5 == n6) {
                        bigInteger = Conversions.ONE;
                    }
                    else {
                        bigInteger = divide.multiply(BigInteger.valueOf(n6 + 1)).divide(BigInteger.valueOf(n5 - n6));
                    }
                }
                ++i;
                n3 = n6;
                val = subtract;
                n4 = n5;
            }
            return gf2Vector;
        }
        throw new IllegalArgumentException("Encoded number too large.");
    }
    
    public static byte[] signConversion(int i, int j, byte[] array) {
        if (i >= j) {
            final BigInteger binomial = IntegerFunctions.binomial(i, j);
            final int n = binomial.bitLength() - 1;
            final int n2 = n >> 3;
            final int n3 = n & 0x7;
            int n4 = 8;
            int n5 = n3;
            int n6 = n2;
            if (n3 == 0) {
                n6 = n2 - 1;
                n5 = 8;
            }
            int n7 = i >> 3;
            final int n8 = i & 0x7;
            if (n8 == 0) {
                --n7;
            }
            else {
                n4 = n8;
            }
            final byte[] array2 = new byte[n7 + 1];
            if (array.length < array2.length) {
                System.arraycopy(array, 0, array2, 0, array.length);
                for (int k = array.length; k < array2.length; ++k) {
                    array2[k] = 0;
                }
            }
            else {
                System.arraycopy(array, 0, array2, 0, n7);
                array2[n7] = (byte)(array[n7] & (1 << n4) - 1);
            }
            BigInteger zero = Conversions.ZERO;
            final int n9 = i;
            int n10 = j;
            j = 0;
            int l = n9;
            BigInteger bigInteger = binomial;
            while (j < i) {
                final BigInteger divide = bigInteger.multiply(new BigInteger(Integer.toString(l - n10))).divide(new BigInteger(Integer.toString(l)));
                --l;
                bigInteger = divide;
                int n11 = n10;
                BigInteger add = zero;
                if ((byte)(array2[j >>> 3] & 1 << (j & 0x7)) != 0) {
                    add = zero.add(divide);
                    n11 = n10 - 1;
                    if (l == n11) {
                        bigInteger = Conversions.ONE;
                    }
                    else {
                        bigInteger = divide.multiply(new BigInteger(Integer.toString(n11 + 1))).divide(new BigInteger(Integer.toString(l - n11)));
                    }
                }
                ++j;
                n10 = n11;
                zero = add;
            }
            array = new byte[n6 + 1];
            final byte[] byteArray = zero.toByteArray();
            if (byteArray.length < array.length) {
                System.arraycopy(byteArray, 0, array, 0, byteArray.length);
                for (i = byteArray.length; i < array.length; ++i) {
                    array[i] = 0;
                }
            }
            else {
                System.arraycopy(byteArray, 0, array, 0, n6);
                array[n6] = (byte)(byteArray[n6] & (1 << n5) - 1);
            }
            return array;
        }
        throw new IllegalArgumentException("n < t");
    }
}
