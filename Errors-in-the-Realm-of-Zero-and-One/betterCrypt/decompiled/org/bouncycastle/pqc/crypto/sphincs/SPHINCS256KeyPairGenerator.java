// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.sphincs;

import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.Digest;
import java.security.SecureRandom;
import org.bouncycastle.crypto.AsymmetricCipherKeyPairGenerator;

public class SPHINCS256KeyPairGenerator implements AsymmetricCipherKeyPairGenerator
{
    private SecureRandom random;
    private Digest treeDigest;
    
    @Override
    public AsymmetricCipherKeyPair generateKeyPair() {
        final Tree.leafaddr leafaddr = new Tree.leafaddr();
        final byte[] bytes = new byte[1088];
        this.random.nextBytes(bytes);
        final byte[] array = new byte[1056];
        System.arraycopy(bytes, 32, array, 0, 1024);
        leafaddr.level = 11;
        leafaddr.subtree = 0L;
        leafaddr.subleaf = 0L;
        Tree.treehash(new HashFunctions(this.treeDigest), array, 1024, 5, bytes, leafaddr, array, 0);
        return new AsymmetricCipherKeyPair(new SPHINCSPublicKeyParameters(array, this.treeDigest.getAlgorithmName()), new SPHINCSPrivateKeyParameters(bytes, this.treeDigest.getAlgorithmName()));
    }
    
    @Override
    public void init(final KeyGenerationParameters keyGenerationParameters) {
        this.random = keyGenerationParameters.getRandom();
        this.treeDigest = ((SPHINCS256KeyGenerationParameters)keyGenerationParameters).getTreeDigest();
    }
}
