// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.sphincs;

import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public class SPHINCSKeyParameters extends AsymmetricKeyParameter
{
    public static final String SHA3_256 = "SHA3-256";
    public static final String SHA512_256 = "SHA-512/256";
    private final String treeDigest;
    
    protected SPHINCSKeyParameters(final boolean b, final String treeDigest) {
        super(b);
        this.treeDigest = treeDigest;
    }
    
    public String getTreeDigest() {
        return this.treeDigest;
    }
}
