// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.sphincs;

import org.bouncycastle.util.Arrays;

public class SPHINCSPublicKeyParameters extends SPHINCSKeyParameters
{
    private final byte[] keyData;
    
    public SPHINCSPublicKeyParameters(final byte[] array) {
        super(false, null);
        this.keyData = Arrays.clone(array);
    }
    
    public SPHINCSPublicKeyParameters(final byte[] array, final String s) {
        super(false, s);
        this.keyData = Arrays.clone(array);
    }
    
    public byte[] getKeyData() {
        return Arrays.clone(this.keyData);
    }
}
