// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.sphincs;

import org.bouncycastle.util.Arrays;

public class SPHINCSPrivateKeyParameters extends SPHINCSKeyParameters
{
    private final byte[] keyData;
    
    public SPHINCSPrivateKeyParameters(final byte[] array) {
        super(true, null);
        this.keyData = Arrays.clone(array);
    }
    
    public SPHINCSPrivateKeyParameters(final byte[] array, final String s) {
        super(true, s);
        this.keyData = Arrays.clone(array);
    }
    
    public byte[] getKeyData() {
        return Arrays.clone(this.keyData);
    }
}
