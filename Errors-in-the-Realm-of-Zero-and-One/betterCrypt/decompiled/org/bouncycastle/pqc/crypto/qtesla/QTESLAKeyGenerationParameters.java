// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.qtesla;

import java.security.SecureRandom;
import org.bouncycastle.crypto.KeyGenerationParameters;

public class QTESLAKeyGenerationParameters extends KeyGenerationParameters
{
    private final int securityCategory;
    
    public QTESLAKeyGenerationParameters(final int securityCategory, final SecureRandom secureRandom) {
        super(secureRandom, -1);
        QTESLASecurityCategory.getPrivateSize(securityCategory);
        this.securityCategory = securityCategory;
    }
    
    public int getSecurityCategory() {
        return this.securityCategory;
    }
}
