// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.qtesla;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public final class QTESLAPrivateKeyParameters extends AsymmetricKeyParameter
{
    private byte[] privateKey;
    private int securityCategory;
    
    public QTESLAPrivateKeyParameters(final int securityCategory, final byte[] array) {
        super(true);
        if (array.length == QTESLASecurityCategory.getPrivateSize(securityCategory)) {
            this.securityCategory = securityCategory;
            this.privateKey = Arrays.clone(array);
            return;
        }
        throw new IllegalArgumentException("invalid key size for security category");
    }
    
    public byte[] getSecret() {
        return Arrays.clone(this.privateKey);
    }
    
    public int getSecurityCategory() {
        return this.securityCategory;
    }
}
