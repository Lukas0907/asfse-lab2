// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.qtesla;

public class QTESLASecurityCategory
{
    public static final int PROVABLY_SECURE_I = 5;
    public static final int PROVABLY_SECURE_III = 6;
    
    private QTESLASecurityCategory() {
    }
    
    public static String getName(final int i) {
        if (i == 5) {
            return "qTESLA-p-I";
        }
        if (i == 6) {
            return "qTESLA-p-III";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown security category: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    static int getPrivateSize(final int i) {
        if (i == 5) {
            return 5184;
        }
        if (i == 6) {
            return 12352;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown security category: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    static int getPublicSize(final int i) {
        if (i == 5) {
            return 14880;
        }
        if (i == 6) {
            return 38432;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown security category: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    static int getSignatureSize(final int i) {
        if (i == 5) {
            return 2592;
        }
        if (i == 6) {
            return 5664;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown security category: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    static void validate(final int i) {
        if (i == 5) {
            return;
        }
        if (i == 6) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown security category: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
}
