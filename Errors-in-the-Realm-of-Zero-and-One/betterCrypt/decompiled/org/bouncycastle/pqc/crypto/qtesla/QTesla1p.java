// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.qtesla;

import java.security.SecureRandom;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.util.Pack;

class QTesla1p
{
    private static int BPLUS1BYTES = 3;
    static final int CRYPTO_BYTES = 2592;
    private static final int CRYPTO_C_BYTES = 32;
    static final int CRYPTO_PUBLICKEYBYTES = 14880;
    private static final int CRYPTO_RANDOMBYTES = 32;
    static final int CRYPTO_SECRETKEYBYTES = 5184;
    private static final int CRYPTO_SEEDBYTES = 32;
    private static final int HM_BYTES = 64;
    private static int NBLOCKS_SHAKE = 56;
    private static final int PARAM_B = 524287;
    private static final int PARAM_BARR_DIV = 30;
    private static final int PARAM_BARR_MULT = 3;
    private static final int PARAM_B_BITS = 19;
    private static final int PARAM_D = 22;
    private static final int PARAM_E = 554;
    private static final int PARAM_GEN_A = 108;
    private static final int PARAM_H = 25;
    private static final int PARAM_K = 4;
    private static final int PARAM_KEYGEN_BOUND_E = 554;
    private static final int PARAM_KEYGEN_BOUND_S = 554;
    private static final int PARAM_N = 1024;
    private static final int PARAM_N_LOG = 10;
    private static final int PARAM_Q = 343576577;
    private static final long PARAM_QINV = 2205847551L;
    private static final int PARAM_Q_LOG = 29;
    private static final int PARAM_R = 172048372;
    private static final int PARAM_R2_INVN = 13632409;
    private static final int PARAM_S = 554;
    private static final double PARAM_SIGMA = 8.5;
    private static final double PARAM_SIGMA_E = 8.5;
    private static final int PARAM_S_BITS = 8;
    private static final int RADIX = 32;
    private static final int RADIX32 = 32;
    private static final int maskb1 = 1048575;
    
    private static int absolute(final int n) {
        final int n2 = n >> 31;
        return (n ^ n2) - n2;
    }
    
    private static long absolute(final long n) {
        final long n2 = n >> 63;
        return (n ^ n2) - n2;
    }
    
    private static int at(final byte[] array, int n, int n2) {
        n2 = n * 4 + n2 * 4;
        n = array[n2];
        final int n3 = n2 + 1;
        n2 = array[n3];
        final int n4 = n3 + 1;
        return array[n4 + 1] << 24 | ((n & 0xFF) | (n2 & 0xFF) << 8 | (array[n4] & 0xFF) << 16);
    }
    
    private static void at(final byte[] array, final int n, final int n2, final int n3) {
        Pack.intToLittleEndian(n3, array, n * 4 + n2 * 4);
    }
    
    private static boolean checkPolynomial(final long[] array, int i, final int n) {
        final int n2 = 1024;
        final long[] array2 = new long[1024];
        boolean b = false;
        for (int j = 0; j < 1024; ++j) {
            array2[j] = absolute(array[i + j]);
        }
        int n3;
        i = (n3 = 0);
        int n4 = n2;
        while (i < 25) {
            int n5 = 0;
            int n6;
            while (true) {
                n6 = n4 - 1;
                if (n5 >= n6) {
                    break;
                }
                final int n7 = n5 + 1;
                final long n8 = array2[n7] - array2[n5] >> 31;
                final long n9 = array2[n7];
                final long n10 = array2[n5];
                final long n11 = n8;
                array2[n7] = ((n8 & array2[n5]) | (array2[n7] & n11));
                array2[n5] = ((n9 & n8) | (n10 & n11));
                n5 = n7;
            }
            n3 += (int)array2[n6];
            --n4;
            ++i;
        }
        if (n3 > n) {
            b = true;
        }
        return b;
    }
    
    static void decodePublicKey(final int[] array, final byte[] array2, final int n, final byte[] array3) {
        int n2 = 0;
        for (int i = 0; i < 4096; i += 32) {
            array[i] = (at(array3, n2, 0) & 0x1FFFFFFF);
            array[i + 1] = ((at(array3, n2, 0) >>> 29 | at(array3, n2, 1) << 3) & 0x1FFFFFFF);
            array[i + 2] = ((at(array3, n2, 1) >>> 26 | at(array3, n2, 2) << 6) & 0x1FFFFFFF);
            array[i + 3] = ((at(array3, n2, 2) >>> 23 | at(array3, n2, 3) << 9) & 0x1FFFFFFF);
            array[i + 4] = ((at(array3, n2, 3) >>> 20 | at(array3, n2, 4) << 12) & 0x1FFFFFFF);
            array[i + 5] = ((at(array3, n2, 4) >>> 17 | at(array3, n2, 5) << 15) & 0x1FFFFFFF);
            array[i + 6] = ((at(array3, n2, 5) >>> 14 | at(array3, n2, 6) << 18) & 0x1FFFFFFF);
            array[i + 7] = ((at(array3, n2, 6) >>> 11 | at(array3, n2, 7) << 21) & 0x1FFFFFFF);
            array[i + 8] = ((at(array3, n2, 7) >>> 8 | at(array3, n2, 8) << 24) & 0x1FFFFFFF);
            array[i + 9] = ((at(array3, n2, 8) >>> 5 | at(array3, n2, 9) << 27) & 0x1FFFFFFF);
            array[i + 10] = (at(array3, n2, 9) >>> 2 & 0x1FFFFFFF);
            array[i + 11] = ((at(array3, n2, 9) >>> 31 | at(array3, n2, 10) << 1) & 0x1FFFFFFF);
            array[i + 12] = ((at(array3, n2, 10) >>> 28 | at(array3, n2, 11) << 4) & 0x1FFFFFFF);
            array[i + 13] = ((at(array3, n2, 11) >>> 25 | at(array3, n2, 12) << 7) & 0x1FFFFFFF);
            array[i + 14] = ((at(array3, n2, 12) >>> 22 | at(array3, n2, 13) << 10) & 0x1FFFFFFF);
            array[i + 15] = ((at(array3, n2, 13) >>> 19 | at(array3, n2, 14) << 13) & 0x1FFFFFFF);
            array[i + 16] = ((at(array3, n2, 14) >>> 16 | at(array3, n2, 15) << 16) & 0x1FFFFFFF);
            array[i + 17] = ((at(array3, n2, 15) >>> 13 | at(array3, n2, 16) << 19) & 0x1FFFFFFF);
            array[i + 18] = ((at(array3, n2, 16) >>> 10 | at(array3, n2, 17) << 22) & 0x1FFFFFFF);
            array[i + 19] = ((at(array3, n2, 17) >>> 7 | at(array3, n2, 18) << 25) & 0x1FFFFFFF);
            array[i + 20] = ((at(array3, n2, 18) >>> 4 | at(array3, n2, 19) << 28) & 0x1FFFFFFF);
            array[i + 21] = (at(array3, n2, 19) >>> 1 & 0x1FFFFFFF);
            array[i + 22] = ((at(array3, n2, 19) >>> 30 | at(array3, n2, 20) << 2) & 0x1FFFFFFF);
            array[i + 23] = ((at(array3, n2, 20) >>> 27 | at(array3, n2, 21) << 5) & 0x1FFFFFFF);
            array[i + 24] = ((at(array3, n2, 21) >>> 24 | at(array3, n2, 22) << 8) & 0x1FFFFFFF);
            array[i + 25] = ((at(array3, n2, 22) >>> 21 | at(array3, n2, 23) << 11) & 0x1FFFFFFF);
            array[i + 26] = ((at(array3, n2, 23) >>> 18 | at(array3, n2, 24) << 14) & 0x1FFFFFFF);
            array[i + 27] = ((at(array3, n2, 24) >>> 15 | at(array3, n2, 25) << 17) & 0x1FFFFFFF);
            array[i + 28] = ((at(array3, n2, 25) >>> 12 | at(array3, n2, 26) << 20) & 0x1FFFFFFF);
            array[i + 29] = ((at(array3, n2, 26) >>> 9 | at(array3, n2, 27) << 23) & 0x1FFFFFFF);
            array[i + 30] = (0x1FFFFFFF & (at(array3, n2, 27) >>> 6 | at(array3, n2, 28) << 26));
            array[i + 31] = at(array3, n2, 28) >>> 3;
            n2 += 29;
        }
        System.arraycopy(array3, 14848, array2, n, 32);
    }
    
    static void decodeSignature(final byte[] array, final long[] array2, final byte[] array3, final int n) {
        int n2;
        for (int i = n2 = 0; i < 1024; i += 16) {
            array2[i] = at(array3, n2, 0) << 12 >> 12;
            array2[i + 1] = (at(array3, n2, 0) >>> 20 | at(array3, n2, 1) << 24 >> 12);
            array2[i + 2] = at(array3, n2, 1) << 4 >> 12;
            array2[i + 3] = (at(array3, n2, 1) >>> 28 | at(array3, n2, 2) << 16 >> 12);
            array2[i + 4] = (at(array3, n2, 2) >>> 16 | at(array3, n2, 3) << 28 >> 12);
            array2[i + 5] = at(array3, n2, 3) << 8 >> 12;
            array2[i + 6] = (at(array3, n2, 3) >>> 24 | at(array3, n2, 4) << 20 >> 12);
            array2[i + 7] = at(array3, n2, 4) >> 12;
            array2[i + 8] = at(array3, n2, 5) << 12 >> 12;
            array2[i + 9] = (at(array3, n2, 5) >>> 20 | at(array3, n2, 6) << 24 >> 12);
            array2[i + 10] = at(array3, n2, 6) << 4 >> 12;
            array2[i + 11] = (at(array3, n2, 6) >>> 28 | at(array3, n2, 7) << 16 >> 12);
            array2[i + 12] = (at(array3, n2, 7) >>> 16 | at(array3, n2, 8) << 28 >> 12);
            array2[i + 13] = at(array3, n2, 8) << 8 >> 12;
            array2[i + 14] = (at(array3, n2, 8) >>> 24 | at(array3, n2, 9) << 20 >> 12);
            array2[i + 15] = at(array3, n2, 9) >> 12;
            n2 += 10;
        }
        System.arraycopy(array3, n + 2560, array, 0, 32);
    }
    
    static void encodeC(final int[] array, final short[] array2, final byte[] array3, final int n) {
        final short[] array4 = new short[1024];
        final byte[] array5 = new byte[168];
        short n2 = 1;
        HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array5, 0, 168, (short)0, array3, n, 32);
        Arrays.fill(array4, (short)0);
        int i;
        int n7;
        for (int n3 = i = 0; i < 25; i = n7) {
            int n4 = n3;
            short n5 = n2;
            if (n3 > 165) {
                n5 = (short)(n2 + 1);
                HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array5, 0, 168, n2, array3, n, 32);
                n4 = 0;
            }
            final int n6 = (array5[n4] << 8 | (array5[n4 + 1] & 0xFF)) & 0x3FF;
            n7 = i;
            if (array4[n6] == 0) {
                if ((array5[n4 + 2] & 0x1) == 0x1) {
                    array4[n6] = -1;
                }
                else {
                    array4[n6] = 1;
                }
                array[i] = n6;
                array2[i] = array4[n6];
                n7 = i + 1;
            }
            n3 = n4 + 3;
            n2 = n5;
        }
    }
    
    static void encodePrivateKey(final byte[] array, final long[] array2, final long[] array3, final byte[] array4, final int n) {
        for (int i = 0; i < 1024; ++i) {
            array[0 + i] = (byte)array2[i];
        }
        for (int j = 0; j < 4; ++j) {
            for (int k = 0; k < 1024; ++k) {
                final int n2 = j * 1024 + k;
                array[1024 + n2] = (byte)array3[n2];
            }
        }
        System.arraycopy(array4, n, array, 5120, 64);
    }
    
    static void encodePublicKey(final byte[] array, final long[] array2, final byte[] array3, final int n) {
        int n2 = 0;
        for (int i = 0; i < 3712; i += 29) {
            final long n3 = array2[n2];
            final int n4 = n2 + 1;
            at(array, i, 0, (int)(n3 | array2[n4] << 29));
            final long n5 = array2[n4];
            final int n6 = n2 + 2;
            at(array, i, 1, (int)(n5 >> 3 | array2[n6] << 26));
            final long n7 = array2[n6];
            final int n8 = n2 + 3;
            at(array, i, 2, (int)(n7 >> 6 | array2[n8] << 23));
            final long n9 = array2[n8];
            final int n10 = n2 + 4;
            at(array, i, 3, (int)(n9 >> 9 | array2[n10] << 20));
            final long n11 = array2[n10];
            final int n12 = n2 + 5;
            at(array, i, 4, (int)(n11 >> 12 | array2[n12] << 17));
            final long n13 = array2[n12];
            final int n14 = n2 + 6;
            at(array, i, 5, (int)(n13 >> 15 | array2[n14] << 14));
            final long n15 = array2[n14];
            final int n16 = n2 + 7;
            at(array, i, 6, (int)(n15 >> 18 | array2[n16] << 11));
            final long n17 = array2[n16];
            final int n18 = n2 + 8;
            at(array, i, 7, (int)(n17 >> 21 | array2[n18] << 8));
            final long n19 = array2[n18];
            final int n20 = n2 + 9;
            at(array, i, 8, (int)(n19 >> 24 | array2[n20] << 5));
            final long n21 = array2[n20];
            final long n22 = array2[n2 + 10];
            final int n23 = n2 + 11;
            at(array, i, 9, (int)(n21 >> 27 | n22 << 2 | array2[n23] << 31));
            final long n24 = array2[n23];
            final int n25 = n2 + 12;
            at(array, i, 10, (int)(n24 >> 1 | array2[n25] << 28));
            final long n26 = array2[n25];
            final int n27 = n2 + 13;
            at(array, i, 11, (int)(n26 >> 4 | array2[n27] << 25));
            final long n28 = array2[n27];
            final int n29 = n2 + 14;
            at(array, i, 12, (int)(n28 >> 7 | array2[n29] << 22));
            final long n30 = array2[n29];
            final int n31 = n2 + 15;
            at(array, i, 13, (int)(n30 >> 10 | array2[n31] << 19));
            final long n32 = array2[n31];
            final int n33 = n2 + 16;
            at(array, i, 14, (int)(n32 >> 13 | array2[n33] << 16));
            final long n34 = array2[n33];
            final int n35 = n2 + 17;
            at(array, i, 15, (int)(n34 >> 16 | array2[n35] << 13));
            final long n36 = array2[n35];
            final int n37 = n2 + 18;
            at(array, i, 16, (int)(n36 >> 19 | array2[n37] << 10));
            final long n38 = array2[n37];
            final int n39 = n2 + 19;
            at(array, i, 17, (int)(n38 >> 22 | array2[n39] << 7));
            final long n40 = array2[n39];
            final int n41 = n2 + 20;
            at(array, i, 18, (int)(n40 >> 25 | array2[n41] << 4));
            final long n42 = array2[n41];
            final long n43 = array2[n2 + 21];
            final int n44 = n2 + 22;
            at(array, i, 19, (int)(n42 >> 28 | n43 << 1 | array2[n44] << 30));
            final long n45 = array2[n44];
            final int n46 = n2 + 23;
            at(array, i, 20, (int)(n45 >> 2 | array2[n46] << 27));
            final long n47 = array2[n46];
            final int n48 = n2 + 24;
            at(array, i, 21, (int)(n47 >> 5 | array2[n48] << 24));
            final long n49 = array2[n48];
            final int n50 = n2 + 25;
            at(array, i, 22, (int)(n49 >> 8 | array2[n50] << 21));
            final long n51 = array2[n50];
            final int n52 = n2 + 26;
            at(array, i, 23, (int)(n51 >> 11 | array2[n52] << 18));
            final long n53 = array2[n52];
            final int n54 = n2 + 27;
            at(array, i, 24, (int)(n53 >> 14 | array2[n54] << 15));
            final long n55 = array2[n54];
            final int n56 = n2 + 28;
            at(array, i, 25, (int)(array2[n56] << 12 | n55 >> 17));
            final long n57 = array2[n56];
            final int n58 = n2 + 29;
            at(array, i, 26, (int)(n57 >> 20 | array2[n58] << 9));
            final long n59 = array2[n58];
            final int n60 = n2 + 30;
            at(array, i, 27, (int)(n59 >> 23 | array2[n60] << 6));
            at(array, i, 28, (int)(array2[n60] >> 26 | array2[n2 + 31] << 3));
            n2 += 32;
        }
        System.arraycopy(array3, n, array, 14848, 32);
    }
    
    static void encodeSignature(final byte[] array, final int n, final byte[] array2, final int n2, final long[] array3) {
        int n3;
        for (int i = n3 = 0; i < 640; i += 10) {
            final long n4 = array3[n3];
            final int n5 = n3 + 1;
            at(array, i, 0, (int)((n4 & 0xFFFFFL) | array3[n5] << 20));
            final long n6 = array3[n5];
            final long n7 = array3[n3 + 2];
            final int n8 = n3 + 3;
            at(array, i, 1, (int)((n6 >>> 12 & 0xFFL) | (n7 & 0xFFFFFL) << 8 | array3[n8] << 28));
            final long n9 = array3[n8];
            final int n10 = n3 + 4;
            at(array, i, 2, (int)((n9 >>> 4 & 0xFFFFL) | array3[n10] << 16));
            final long n11 = array3[n10];
            final long n12 = array3[n3 + 5];
            final int n13 = n3 + 6;
            at(array, i, 3, (int)((n11 >>> 16 & 0xFL) | (n12 & 0xFFFFFL) << 4 | array3[n13] << 24));
            at(array, i, 4, (int)((array3[n13] >>> 8 & 0xFFFL) | array3[n3 + 7] << 12));
            final long n14 = array3[n3 + 8];
            final int n15 = n3 + 9;
            at(array, i, 5, (int)((n14 & 0xFFFFFL) | array3[n15] << 20));
            final long n16 = array3[n15];
            final long n17 = array3[n3 + 10];
            final int n18 = n3 + 11;
            at(array, i, 6, (int)((n16 >>> 12 & 0xFFL) | (n17 & 0xFFFFFL) << 8 | array3[n18] << 28));
            final long n19 = array3[n18];
            final int n20 = n3 + 12;
            at(array, i, 7, (int)((n19 >>> 4 & 0xFFFFL) | array3[n20] << 16));
            final long n21 = array3[n20];
            final long n22 = array3[n3 + 13];
            final int n23 = n3 + 14;
            at(array, i, 8, (int)((n21 >>> 16 & 0xFL) | (0xFFFFFL & n22) << 4 | array3[n23] << 24));
            at(array, i, 9, (int)((array3[n23] >>> 8 & 0xFFFL) | array3[n3 + 15] << 12));
            n3 += 16;
        }
        System.arraycopy(array2, n2, array, n + 2560, 32);
    }
    
    static int generateKeyPair(final byte[] array, final byte[] array2, final SecureRandom secureRandom) {
        final byte[] bytes = new byte[32];
        final byte[] array3 = new byte[224];
        final long[] array4 = new long[1024];
        final long[] array5 = new long[4096];
        final long[] array6 = new long[4096];
        final long[] array7 = new long[4096];
        final long[] array8 = new long[1024];
        secureRandom.nextBytes(bytes);
        HashUtils.secureHashAlgorithmKECCAK128(array3, 0, 224, bytes, 0, 32);
        int n2;
        int n = n2 = 0;
        int n3;
        while (true) {
            n3 = n2;
            if (n >= 4) {
                break;
            }
            int n4 = n2;
            int n5;
            do {
                n2 = n4 + 1;
                n5 = n * 1024;
                Gaussian.sample_gauss_polly(n2, array3, n * 32, array5, n5);
                n4 = n2;
            } while (checkPolynomial(array5, n5, 554));
            ++n;
        }
        do {
            ++n3;
            Gaussian.sample_gauss_polly(n3, array3, 128, array4, 0);
        } while (checkPolynomial(array4, 0, 554));
        QTesla1PPolynomial.poly_uniform(array6, array3, 160);
        QTesla1PPolynomial.poly_ntt(array8, array4);
        int i = 0;
        final long[] array9 = array8;
        while (i < 4) {
            final int n6 = i * 1024;
            QTesla1PPolynomial.poly_mul(array7, n6, array6, n6, array9);
            QTesla1PPolynomial.poly_add_correct(array7, n6, array7, n6, array5, n6);
            ++i;
        }
        encodePublicKey(array, array7, array3, 160);
        encodePrivateKey(array2, array4, array5, array3, 160);
        return 0;
    }
    
    static int generateSignature(final byte[] array, final byte[] array2, int i, int n, final byte[] array3, final SecureRandom secureRandom) {
        byte[] array4 = array3;
        final byte[] array5 = new byte[32];
        final byte[] array6 = new byte[32];
        final byte[] array7 = new byte[128];
        final int[] array8 = new int[25];
        final short[] array9 = new short[25];
        final long[] array10 = new long[1024];
        final long[] array11 = new long[1024];
        final long[] array12 = new long[1024];
        final long[] array13 = new long[1024];
        final long[] array14 = new long[4096];
        final long[] array15 = new long[4096];
        final long[] array16 = new long[4096];
        final byte[] bytes = new byte[32];
        secureRandom.nextBytes(bytes);
        System.arraycopy(bytes, 0, array7, 32, 32);
        System.arraycopy(array4, 5152, array7, 0, 32);
        HashUtils.secureHashAlgorithmKECCAK128(array7, 64, 64, array2, 0, n);
        HashUtils.secureHashAlgorithmKECCAK128(array6, 0, 32, array7, 0, 128);
        QTesla1PPolynomial.poly_uniform(array16, array4, 5120);
        i = 0;
        boolean test_correctness = false;
        final int[] array17 = array8;
        short[] array18 = array9;
        while (true) {
            n = i + 1;
            sample_y(array10, array6, 0, n);
            QTesla1PPolynomial.poly_ntt(array11, array10);
            int n2;
            for (i = 0; i < 4; ++i) {
                n2 = i * 1024;
                QTesla1PPolynomial.poly_mul(array14, n2, array16, n2, array11);
            }
            hashFunction(array5, 0, array14, array7, 64);
            encodeC(array17, array18, array5, 0);
            QTesla1PPolynomial.sparse_mul8(array12, array4, array17, array18);
            QTesla1PPolynomial.poly_add(array13, array10, array12);
            if (!testRejection(array13)) {
                i = 0;
                while (i < 4) {
                    final int n3 = i * 1024;
                    ++i;
                    final short[] array19 = array18;
                    QTesla1PPolynomial.sparse_mul8(array15, n3, array3, i * 1024, array17, array19);
                    QTesla1PPolynomial.poly_sub(array14, n3, array14, n3, array15, n3);
                    test_correctness = test_correctness(array14, n3);
                    if (test_correctness) {
                        break;
                    }
                    array18 = array19;
                }
                if (!test_correctness) {
                    break;
                }
                array4 = array3;
            }
            i = n;
        }
        encodeSignature(array, 0, array5, 0, array13);
        return 0;
    }
    
    private static void hashFunction(final byte[] array, final int n, final long[] array2, final byte[] array3, final int n2) {
        final byte[] array4 = new byte[4160];
        for (int i = 0; i < 4; ++i) {
            for (int n3 = i * 1024, j = 0; j < 1024; ++j, ++n3) {
                final int n4 = (int)array2[n3];
                final int n5 = 171788288 - n4 >> 31;
                final int n6 = (n4 & n5) | (n4 - 343576577 & n5);
                final int n7 = 0x3FFFFF & n6;
                final int n8 = 2097152 - n7 >> 31;
                array4[n3] = (byte)(n6 - ((n7 & n8) | (n7 - 4194304 & n8)) >> 22);
            }
        }
        System.arraycopy(array3, n2, array4, 4096, 64);
        HashUtils.secureHashAlgorithmKECCAK128(array, n, 32, array4, 0, 4160);
    }
    
    static int lE24BitToInt(final byte[] array, int n) {
        final byte b = array[n];
        ++n;
        return (array[n + 1] & 0xFF) << 16 | ((b & 0xFF) | (array[n] & 0xFF) << 8);
    }
    
    static boolean memoryEqual(final byte[] array, final int n, final byte[] array2, final int n2, final int n3) {
        if (n + n3 <= array.length && n2 + n3 <= array2.length) {
            for (int i = 0; i < n3; ++i) {
                if (array[n + i] != array2[n2 + i]) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    static void sample_y(final long[] array, final byte[] array2, final int n, int n2) {
        final int bplus1BYTES = QTesla1p.BPLUS1BYTES;
        final byte[] array3 = new byte[bplus1BYTES * 1024 + 1];
        final short n3 = (short)(n2 << 8);
        final int n4 = bplus1BYTES * 1024;
        short n5 = (short)(n3 + 1);
        HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array3, 0, n4, n3, array2, n, 32);
        int i;
        n2 = (i = 0);
        int n6 = 1024;
        while (i < 1024) {
            int n7 = n2;
            int nblocks_SHAKE = n6;
            short n8 = n5;
            if (n2 >= n6 * bplus1BYTES) {
                nblocks_SHAKE = QTesla1p.NBLOCKS_SHAKE;
                n8 = (short)(n5 + 1);
                HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array3, 0, n4, n5, array2, n, 32);
                n7 = 0;
            }
            array[i] = (lE24BitToInt(array3, n7) & 0xFFFFF);
            array[i] -= 524287L;
            int n9 = i;
            if (array[i] != 524288L) {
                n9 = i + 1;
            }
            n2 = n7 + bplus1BYTES;
            n6 = nblocks_SHAKE;
            n5 = n8;
            i = n9;
        }
    }
    
    private static boolean testRejection(final long[] array) {
        boolean b = false;
        int n;
        for (int i = n = 0; i < 1024; ++i) {
            n = (int)((long)n | 523733L - absolute(array[i]));
        }
        if (n >>> 31 > 0) {
            b = true;
        }
        return b;
    }
    
    private static boolean testZ(final long[] array) {
        for (int i = 0; i < 1024; ++i) {
            if (array[i] < -523733L || array[i] > 523733L) {
                return true;
            }
        }
        return false;
    }
    
    static boolean test_correctness(final long[] array, final int n) {
        for (int i = 0; i < 1024; ++i) {
            final int n2 = n + i;
            final int n3 = (int)(171788288L - array[n2]) >> 31;
            final int n4 = (int)((array[n2] & (long)n3) | (array[n2] - 343576577L & (long)n3));
            if ((absolute(n4 - (2097152 + n4 - 1 >> 22 << 22)) - 2096598 >>> 31 | absolute(n4) - 171787734 >>> 31) == 0x1) {
                return true;
            }
        }
        return false;
    }
    
    static int verifying(final byte[] array, final byte[] array2, int i, int n, final byte[] array3) {
        final byte[] array4 = new byte[32];
        final byte[] array5 = new byte[32];
        final byte[] array6 = new byte[32];
        final byte[] array7 = new byte[64];
        final int[] array8 = new int[25];
        final short[] array9 = new short[25];
        final int[] array10 = new int[4096];
        final long[] array11 = new long[4096];
        final long[] array12 = new long[4096];
        final long[] array13 = new long[4096];
        final long[] array14 = new long[1024];
        final long[] array15 = new long[1024];
        if (n < 2592) {
            return -1;
        }
        decodeSignature(array4, array14, array2, i);
        if (testZ(array14)) {
            return -2;
        }
        decodePublicKey(array10, array6, 0, array3);
        QTesla1PPolynomial.poly_uniform(array12, array6, 0);
        encodeC(array8, array9, array4, 0);
        QTesla1PPolynomial.poly_ntt(array15, array14);
        i = 0;
        final long[] array16 = array11;
        final long[] array17 = array13;
        while (i < 4) {
            n = i * 1024;
            QTesla1PPolynomial.sparse_mul32(array17, n, array10, n, array8, array9);
            QTesla1PPolynomial.poly_mul(array16, n, array12, n, array15);
            QTesla1PPolynomial.poly_sub(array16, n, array16, n, array17, n);
            ++i;
        }
        HashUtils.secureHashAlgorithmKECCAK128(array7, 0, 64, array, 0, array.length);
        i = 0;
        hashFunction(array5, 0, array16, array7, 0);
        if (!memoryEqual(array4, 0, array5, 0, 32)) {
            i = -3;
        }
        return i;
    }
    
    static class Gaussian
    {
        private static final int CDT_COLS = 2;
        private static final int CDT_ROWS = 78;
        private static final int CHUNK_SIZE = 512;
        private static final long[] cdt_v;
        
        static {
            cdt_v = new long[] { 0L, 0L, 100790826L, 671507412L, 300982266L, 372236861L, 497060329L, 1131554536L, 686469725L, 80027618L, 866922278L, 352172656L, 1036478428L, 1164298592L, 1193606242L, 860014474L, 1337215220L, 1378472045L, 1466664345L, 1948467327L, 1581745882L, 839957239L, 1682648210L, 1125857607L, 1769902286L, 2009293508L, 1844317078L, 664324558L, 1906909508L, 1466301668L, 1958834133L, 506071440L, 2001317010L, 234057451L, 2035597220L, 671584905L, 2062878330L, 786178128L, 2084290940L, 306011771L, 2100866422L, 714310105L, 2113521119L, 243698855L, 2123049658L, 417712145L, 2130125692L, 9470578L, 2135308229L, 1840927014L, 2139051783L, 1246948843L, 2141718732L, 589890969L, 2143592579L, 1774056149L, 2144891082L, 1109874008L, 2145778525L, 1056451611L, 2146376698L, 1812177762L, 2146774350L, 829172876L, 2147035066L, 313414831L, 2147203651L, 1956430050L, 2147311165L, 1160031633L, 2147378788L, 1398244789L, 2147420737L, 187242113L, 2147446401L, 321666415L, 2147461886L, 1304194029L, 2147471101L, 2048797972L, 2147476510L, 1282326805L, 2147479641L, 831849416L, 2147481428L, 1574767936L, 2147482435L, 194943011L, 2147482993L, 1991776993L, 2147483299L, 2120655340L, 2147483465L, 653713809L, 2147483553L, 799217300L, 2147483599L, 1380433609L, 2147483623L, 1329670087L, 2147483635L, 1873439229L, 2147483642L, 103862387L, 2147483645L, 254367675L, 2147483646L, 1339200562L, 2147483647L, 754636301L, 2147483647L, 1499965744L, 2147483647L, 1850514943L, 2147483647L, 2013121736L, 2147483647L, 2087512222L, 2147483647L, 2121077103L, 2147483647L, 2136013361L, 2147483647L, 2142568585L, 2147483647L, 2145405997L, 2147483647L, 2146617281L, 2147483647L, 2147127267L, 2147483647L, 2147339035L, 2147483647L, 2147425762L, 2147483647L, 2147460791L, 2147483647L, 2147474745L, 2147483647L, 2147480227L, 2147483647L, 2147482351L, 2147483647L, 2147483163L, 2147483647L, 2147483469L, 2147483647L, 2147483583L, 2147483647L, 2147483625L, 2147483647L, 2147483640L, 2147483647L, 2147483645L, 2147483647L, 2147483647L };
        }
        
        static void sample_gauss_polly(int n, final byte[] array, final int n2, final long[] array2, final int n3) {
            final byte[] array3 = new byte[4096];
            final int[] array4 = new int[2];
            n <<= 8;
            for (int i = 0; i < 1024; i += 512, ++n) {
                HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array3, 0, 4096, (short)n, array, n2, 32);
                for (int j = 0; j < 512; ++j) {
                    final int n4 = n3 + i + j;
                    array2[n4] = 0L;
                    for (int k = 1; k < 78; ++k) {
                        int l = 1;
                        int n5 = 0;
                        while (l >= 0) {
                            array4[l] = (int)((at(array3, 0, j * 2 + l) & Integer.MAX_VALUE) - (Gaussian.cdt_v[k * 2 + l] + n5));
                            n5 = array4[l] >> 31;
                            --l;
                        }
                        array2[n4] += (n5 & 0x1);
                    }
                    final int n6 = at(array3, 0, j * 2) >> 31;
                    array2[n4] = (((long)n6 & -array2[n4]) | ((long)n6 & array2[n4]));
                }
            }
        }
    }
    
    static class QTesla1PPolynomial
    {
        private static final long[] zeta;
        private static final long[] zetainv;
        
        static {
            zeta = new long[] { 184007114L, 341297933L, 172127038L, 306069179L, 260374244L, 269720605L, 20436325L, 2157599L, 36206659L, 61987110L, 112759694L, 92762708L, 278504038L, 139026960L, 183642748L, 298230187L, 37043356L, 230730845L, 107820937L, 97015745L, 156688276L, 38891102L, 170244636L, 259345227L, 170077366L, 141586883L, 100118513L, 328793523L, 289946488L, 263574185L, 132014089L, 14516260L, 87424978L, 192691578L, 190961717L, 262687761L, 333967048L, 12957952L, 326574509L, 273585413L, 151922543L, 195893203L, 261889302L, 120488377L, 169571794L, 44896463L, 128576039L, 68257019L, 20594664L, 44164717L, 36060712L, 256009818L, 172063915L, 211967562L, 135533785L, 104908181L, 203788155L, 52968398L, 123297488L, 44711423L, 329131026L, 245797804L, 220629853L, 200431766L, 92905498L, 215466666L, 227373088L, 120513729L, 274875394L, 236766448L, 84216704L, 97363940L, 224003799L, 167341181L, 333540791L, 225846253L, 290150331L, 137934911L, 101127339L, 95054535L, 7072757L, 58600117L, 264117725L, 207480694L, 268253444L, 292044590L, 166300682L, 256585624L, 133577520L, 119707476L, 58169614L, 188489502L, 184778640L, 156039906L, 286669262L, 112658784L, 89254003L, 266568758L, 290599527L, 80715937L, 180664712L, 225980378L, 103512701L, 304604206L, 327443646L, 92082345L, 296093912L, 144843084L, 309484036L, 329737605L, 141656867L, 264967053L, 227847682L, 328674715L, 208663554L, 309005608L, 315790590L, 182996330L, 333212133L, 203436199L, 13052895L, 23858345L, 173478900L, 97132319L, 57066271L, 70747422L, 202106993L, 309870606L, 56390934L, 336126437L, 189147643L, 219236223L, 293351741L, 305570320L, 18378834L, 336914091L, 59506067L, 277923611L, 217306643L, 129369847L, 308113789L, 56954705L, 190254906L, 199465001L, 119331054L, 143640880L, 17590914L, 309468163L, 172483421L, 153376031L, 58864560L, 70957183L, 237697179L, 116097341L, 62196815L, 80692520L, 310642530L, 328595292L, 12121494L, 71200620L, 200016287L, 235006678L, 21821056L, 102505389L, 183332133L, 59734849L, 283127491L, 313646880L, 30359439L, 163176989L, 50717815L, 100183661L, 322975554L, 92821217L, 283119421L, 34453836L, 303758926L, 89460722L, 147514506L, 175603941L, 76494101L, 220775631L, 304963431L, 38821441L, 217317485L, 301302769L, 328727631L, 101476595L, 270750726L, 253708871L, 176201368L, 324059659L, 114780906L, 304156831L, 273708648L, 144095014L, 263545324L, 179240984L, 187811389L, 244886526L, 202581571L, 209325648L, 117231636L, 182195945L, 217965216L, 252295904L, 332003328L, 46153749L, 334740528L, 62618402L, 301165510L, 283016648L, 212224416L, 234984074L, 107363471L, 125430881L, 172821269L, 270409387L, 156316970L, 311644197L, 50537885L, 248376507L, 154072039L, 331539029L, 48454192L, 267029920L, 225963915L, 16753350L, 76840946L, 226444843L, 108106635L, 154887261L, 326283837L, 101291223L, 204194230L, 54014060L, 104099734L, 104245071L, 260949411L, 333985274L, 291682234L, 328313139L, 29607387L, 106291750L, 162553334L, 275058303L, 64179189L, 263147140L, 15599810L, 325103190L, 137254480L, 66787068L, 4755224L, 308520011L, 181897417L, 325162685L, 221099032L, 131741505L, 147534370L, 131533267L, 144073688L, 166398146L, 155829711L, 252509898L, 251605008L, 323547097L, 216038649L, 232629333L, 95137254L, 287931575L, 235583527L, 32386598L, 76722491L, 60825791L, 138354268L, 400761L, 51907675L, 197369064L, 319840588L, 98618414L, 84343982L, 108113946L, 314679670L, 134518178L, 64988900L, 4333172L, 295712261L, 200707216L, 147647414L, 318013383L, 77682006L, 92518996L, 42154619L, 87464521L, 285037574L, 332936592L, 62635246L, 5534097L, 308862707L, 91097989L, 269726589L, 273280832L, 251670430L, 95492698L, 21676891L, 182964692L, 177187742L, 294825274L, 85128609L, 273594538L, 93115857L, 116308166L, 312212122L, 18665807L, 32192823L, 313249299L, 98777368L, 273984239L, 312125377L, 205655336L, 264861277L, 178920022L, 341054719L, 232663249L, 173564046L, 176591124L, 157537342L, 305058098L, 277279130L, 170028356L, 228573747L, 31628995L, 175280663L, 37304323L, 122111670L, 210658936L, 175704183L, 314649282L, 325535066L, 266783938L, 301319742L, 327923297L, 279787306L, 304633001L, 304153402L, 292839078L, 147442886L, 94150133L, 40461238L, 221384781L, 269671052L, 265445273L, 208370149L, 160863546L, 287765159L, 339146643L, 129600429L, 96192870L, 113146118L, 95879915L, 216708053L, 285201955L, 67756451L, 79028039L, 309141895L, 138447809L, 212246614L, 12641916L, 243544995L, 33459809L, 76979779L, 71155723L, 152521243L, 200750888L, 36425947L, 339074467L, 319204591L, 188312744L, 266105966L, 280016981L, 183723313L, 238915015L, 23277613L, 160934729L, 200611286L, 163282810L, 297928823L, 226921588L, 86839172L, 145317111L, 202226936L, 51887320L, 318474782L, 282270658L, 221219795L, 207597867L, 132089009L, 334627662L, 163952597L, 67529059L, 173759630L, 234865017L, 255217646L, 277806158L, 61964704L, 216678166L, 96126463L, 39218331L, 70028373L, 4899005L, 238135514L, 242700690L, 284680271L, 81041980L, 332906491L, 463527L, 299280916L, 204600651L, 149654879L, 222229829L, 26825157L, 81825189L, 127990873L, 200962599L, 16149163L, 108812393L, 217708971L, 152638110L, 28735779L, 5272794L, 19720409L, 231726324L, 49854178L, 118319174L, 185669526L, 223407181L, 243138094L, 259020958L, 308825615L, 164156486L, 341391280L, 192526841L, 97036052L, 279986894L, 20263748L, 32228956L, 43816679L, 343421811L, 124320208L, 3484106L, 31711063L, 147679160L, 195369505L, 54243678L, 279088595L, 149119313L, 301997352L, 244557309L, 19700779L, 138872683L, 230523717L, 113507709L, 135291486L, 313025300L, 254384479L, 219815764L, 253574481L, 220646316L, 124744817L, 123915741L, 325760383L, 123516396L, 138140410L, 154060994L, 314730104L, 57286356L, 222353426L, 76630003L, 145380041L, 52039855L, 229881219L, 332902036L, 152308429L, 95071889L, 124799350L, 270141530L, 47897266L, 119620601L, 133269057L, 138561303L, 341820265L, 66049665L, 273409631L, 304306012L, 212490958L, 210388603L, 277413768L, 280793261L, 223131872L, 162407285L, 44911970L, 316685837L, 298709373L, 252812339L, 230786851L, 230319350L, 56863422L, 341141914L, 177295413L, 248222411L, 215148650L, 97970603L, 291678055L, 161911155L, 339645428L, 206445182L, 31895080L, 279676698L, 78257775L, 268845232L, 92545841L, 336725589L, 47384597L, 62216335L, 82290365L, 89893410L, 266117967L, 791867L, 28042243L, 110563426L, 183316855L, 281174508L, 166338432L, 86326996L, 261473803L, 164647535L, 84749290L, 157518777L, 214336587L, 72257047L, 13358702L, 229010735L, 204196474L, 179927635L, 21786785L, 330554989L, 164559635L, 144505300L, 280425045L, 324057501L, 268227440L, 323362437L, 26891539L, 228523003L, 166709094L, 61174973L, 13532911L, 42168701L, 133044957L, 158219357L, 220115616L, 15174468L, 281706353L, 283813987L, 263212325L, 289818392L, 247170937L, 276072317L, 197581495L, 33713097L, 181695825L, 96829354L, 32991226L, 228583784L, 4040287L, 65188717L, 258204083L, 96366799L, 176298395L, 341574369L, 306098123L, 218746932L, 29191888L, 311810435L, 305844323L, 31614267L, 28130094L, 72716426L, 38568041L, 197579396L, 14876445L, 228525674L, 294569685L, 2451649L, 165929882L, 112195415L, 204786047L, 138216235L, 3438132L, 126150615L, 59754608L, 158965324L, 268160978L, 266231264L, 244422459L, 306155336L, 218178824L, 301806695L, 208837335L, 212153467L, 209725081L, 269355286L, 295716530L, 13980580L, 264284060L, 301901789L, 275319045L, 107139083L, 4006959L, 143908623L, 139848274L, 25357089L, 21607040L, 340818603L, 91260932L, 198869267L, 45119941L, 224113252L, 269556513L, 42857483L, 268925602L, 188501450L, 235382337L, 324688793L, 113056679L, 177232352L, 98280013L, 117743899L, 87369665L, 330110286L, 310895756L, 268425063L, 27568325L, 266303142L, 181405304L, 65876631L, 246283438L, 127636847L, 16153922L, 210256884L, 9257227L, 147272724L, 235571791L, 340876897L, 31558760L, 224463520L, 229909008L, 40943950L, 263351999L, 14865952L, 27279162L, 51980445L, 99553161L, 108121152L, 145230283L, 217402431L, 84060866L, 190168688L, 46894008L, 205718237L, 296935065L, 331646198L, 59709076L, 265829428L, 214503586L, 310273189L, 86051634L, 247210969L, 275872780L, 55395653L, 302717617L, 155583500L, 207999042L, 293597246L, 305796948L, 139332832L, 198434142L, 104197059L, 320317582L, 101819543L, 70813687L, 43594385L, 241913829L, 210308279L, 298735610L, 151599086L, 92093482L, 24654121L, 52528801L, 134711941L, 324580593L, 293101038L, 121757877L, 323940193L, 276114751L, 33522997L, 218880483L, 46953248L, 33126382L, 294367143L, 161595040L, 208968904L, 129221110L, 323693686L, 234366848L, 50155901L, 123936119L, 72127416L, 34243899L, 171824126L, 26019236L, 93997235L, 28452989L, 24219933L, 188331672L, 181161011L, 146526219L, 186502916L, 258266311L, 207146754L, 206589869L, 189836867L, 107762500L, 129011227L, 222324073L, 331319091L, 36618753L, 141615400L, 273319528L, 246222615L, 156139193L, 290104141L, 154851520L, 310226922L, 60187406L, 73704819L, 225899604L, 87931539L, 142487643L, 152682959L, 45891249L, 212048348L, 148547910L, 207745063L, 4405848L, 179269204L, 216233362L, 230307487L, 303352796L, 41616117L, 47140231L, 13452075L, 94626849L, 48892822L, 78453712L, 214721933L, 300785835L, 1512599L, 173577933L, 163255132L, 239883248L, 205714288L, 306118903L, 106953300L, 150085654L, 77068348L, 246390345L, 199698311L, 280165539L, 256497526L, 194381508L, 78125966L, 168327358L, 180735395L, 145983352L, 243342736L, 198463602L, 83165996L, 286431792L, 22885329L, 271516106L, 66137359L, 243561376L, 324886778L, 149497212L, 24531379L, 32857894L, 62778029L, 56960216L, 224996784L, 129315394L, 81068505L, 277744916L, 215817366L, 117205172L, 195090165L, 287841567L, 57750901L, 162987791L, 259309908L, 135370005L, 194853269L, 236792732L, 219249166L, 42349628L, 27805769L, 186263338L, 310699018L, 6491000L, 228545163L, 315890485L, 22219119L, 144392189L, 15505150L, 87848372L, 155973124L, 20446561L, 177725890L, 226669021L, 205315635L, 269580641L, 133696452L, 189388357L, 314652032L, 317225560L, 304194584L, 157633737L, 298144493L, 185785271L, 337434647L, 559796L, 4438732L, 249110619L, 184824722L, 221490126L, 205632858L, 172362641L, 176702767L, 276712118L, 296075254L, 111221225L, 259809961L, 15438443L, 198021462L, 134378223L, 162261445L, 170746654L, 256890644L, 125206341L, 307078324L, 279553989L, 170124925L, 296845387L, 188226544L, 295437875L, 315053523L, 172025817L, 279046062L, 189967278L, 158662482L, 192989875L, 326540363L, 135446089L, 98631439L, 257379933L, 325004289L, 26554274L, 62190249L, 228828648L, 274361329L, 18518762L, 184854759L, 210189061L, 186836398L, 230859454L, 206912014L, 201250021L, 276332768L, 119984643L, 91358832L, 325377399L, 69085488L, 307352479L, 308876137L, 208756649L, 32865966L, 152976045L, 207821125L, 66426662L, 67585526L, 118828370L, 3107192L, 322037257L, 146029104L, 106553806L, 266958791L, 89567376L, 153815988L, 90786397L, 271042585L, 203781777L, 169087756L, 315867500L, 306916544L, 7528726L, 327732739L, 227901532L, 2263402L, 14357894L, 269740764L, 322090105L, 59838559L, 298337502L, 292797139L, 337635349L, 66476915L, 75612762L, 328089387L, 155232910L, 87069405L, 36163560L, 273715413L, 321325749L, 218096743L, 308178877L, 21861281L, 180676741L, 135208372L, 119891712L, 122406065L, 267537516L, 341350322L, 87789083L, 196340943L, 217070591L, 83564209L, 159382818L, 253921239L, 184673854L, 213569600L, 194031064L, 35973794L, 18071215L, 250854127L, 115090766L, 147707843L, 330337973L, 266187164L, 27853295L, 296801215L, 254949704L, 43331190L, 73930201L, 35703461L, 119780800L, 216998106L, 12687572L, 250863345L, 243908221L, 330555990L, 296216993L, 202100577L, 111307303L, 151049872L, 103451600L, 237710099L, 78658022L, 121490075L, 134292528L, 88277916L, 177315676L, 186629690L, 77848818L, 211822377L, 145696683L, 289190386L, 274721999L, 328391282L, 218772820L, 91324151L, 321725584L, 277577004L, 65732866L, 275538085L, 144429136L, 204062923L, 177280727L, 214204692L, 264758257L, 169151951L, 335535576L, 334002493L, 281131703L, 305997258L, 310527888L, 136973519L, 216764406L, 235954329L, 254049694L, 285174861L, 264316834L, 11792643L, 149333889L, 214699018L, 261331547L, 317320791L, 24527858L, 118790777L, 264146824L, 174296812L, 332779737L, 94199786L, 288227027L, 172048372L };
            zetainv = new long[] { 55349550L, 249376791L, 10796840L, 169279765L, 79429753L, 224785800L, 319048719L, 26255786L, 82245030L, 128877559L, 194242688L, 331783934L, 79259743L, 58401716L, 89526883L, 107622248L, 126812171L, 206603058L, 33048689L, 37579319L, 62444874L, 9574084L, 8041001L, 174424626L, 78818320L, 129371885L, 166295850L, 139513654L, 199147441L, 68038492L, 277843711L, 65999573L, 21850993L, 252252426L, 124803757L, 15185295L, 68854578L, 54386191L, 197879894L, 131754200L, 265727759L, 156946887L, 166260901L, 255298661L, 209284049L, 222086502L, 264918555L, 105866478L, 240124977L, 192526705L, 232269274L, 141476000L, 47359584L, 13020587L, 99668356L, 92713232L, 330889005L, 126578471L, 223795777L, 307873116L, 269646376L, 300245387L, 88626873L, 46775362L, 315723282L, 77389413L, 13238604L, 195868734L, 228485811L, 92722450L, 325505362L, 307602783L, 149545513L, 130006977L, 158902723L, 89655338L, 184193759L, 260012368L, 126505986L, 147235634L, 255787494L, 2226255L, 76039061L, 221170512L, 223684865L, 208368205L, 162899836L, 321715296L, 35397700L, 125479834L, 22250828L, 69861164L, 307413017L, 256507172L, 188343667L, 15487190L, 267963815L, 277099662L, 5941228L, 50779438L, 45239075L, 283738018L, 21486472L, 73835813L, 329218683L, 341313175L, 115675045L, 15843838L, 336047851L, 36660033L, 27709077L, 174488821L, 139794800L, 72533992L, 252790180L, 189760589L, 254009201L, 76617786L, 237022771L, 197547473L, 21539320L, 340469385L, 224748207L, 275991051L, 277149915L, 135755452L, 190600532L, 310710611L, 134819928L, 34700440L, 36224098L, 274491089L, 18199178L, 252217745L, 223591934L, 67243809L, 142326556L, 136664563L, 112717123L, 156740179L, 133387516L, 158721818L, 325057815L, 69215248L, 114747929L, 281386328L, 317022303L, 18572288L, 86196644L, 244945138L, 208130488L, 17036214L, 150586702L, 184914095L, 153609299L, 64530515L, 171550760L, 28523054L, 48138702L, 155350033L, 46731190L, 173451652L, 64022588L, 36498253L, 218370236L, 86685933L, 172829923L, 181315132L, 209198354L, 145555115L, 328138134L, 83766616L, 232355352L, 47501323L, 66864459L, 166873810L, 171213936L, 137943719L, 122086451L, 158751855L, 94465958L, 339137845L, 343016781L, 6141930L, 157791306L, 45432084L, 185942840L, 39381993L, 26351017L, 28924545L, 154188220L, 209880125L, 73995936L, 138260942L, 116907556L, 165850687L, 323130016L, 187603453L, 255728205L, 328071427L, 199184388L, 321357458L, 27686092L, 115031414L, 337085577L, 32877559L, 157313239L, 315770808L, 301226949L, 124327411L, 106783845L, 148723308L, 208206572L, 84266669L, 180588786L, 285825676L, 55735010L, 148486412L, 226371405L, 127759211L, 65831661L, 262508072L, 214261183L, 118579793L, 286616361L, 280798548L, 310718683L, 319045198L, 194079365L, 18689799L, 100015201L, 277439218L, 72060471L, 320691248L, 57144785L, 260410581L, 145112975L, 100233841L, 197593225L, 162841182L, 175249219L, 265450611L, 149195069L, 87079051L, 63411038L, 143878266L, 97186232L, 266508229L, 193490923L, 236623277L, 37457674L, 137862289L, 103693329L, 180321445L, 169998644L, 342063978L, 42790742L, 128854644L, 265122865L, 294683755L, 248949728L, 330124502L, 296436346L, 301960460L, 40223781L, 113269090L, 127343215L, 164307373L, 339170729L, 135831514L, 195028667L, 131528229L, 297685328L, 190893618L, 201088934L, 255645038L, 117676973L, 269871758L, 283389171L, 33349655L, 188725057L, 53472436L, 187437384L, 97353962L, 70257049L, 201961177L, 306957824L, 12257486L, 121252504L, 214565350L, 235814077L, 153739710L, 136986708L, 136429823L, 85310266L, 157073661L, 197050358L, 162415566L, 155244905L, 319356644L, 315123588L, 249579342L, 317557341L, 171752451L, 309332678L, 271449161L, 219640458L, 293420676L, 109209729L, 19882891L, 214355467L, 134607673L, 181981537L, 49209434L, 310450195L, 296623329L, 124696094L, 310053580L, 67461826L, 19636384L, 221818700L, 50475539L, 18995984L, 208864636L, 291047776L, 318922456L, 251483095L, 191977491L, 44840967L, 133268298L, 101662748L, 299982192L, 272762890L, 241757034L, 23258995L, 239379518L, 145142435L, 204243745L, 37779629L, 49979331L, 135577535L, 187993077L, 40858960L, 288180924L, 67703797L, 96365608L, 257524943L, 33303388L, 129072991L, 77747149L, 283867501L, 11930379L, 46641512L, 137858340L, 296682569L, 153407889L, 259515711L, 126174146L, 198346294L, 235455425L, 244023416L, 291596132L, 316297415L, 328710625L, 80224578L, 302632627L, 113667569L, 119113057L, 312017817L, 2699680L, 108004786L, 196303853L, 334319350L, 133319693L, 327422655L, 215939730L, 97293139L, 277699946L, 162171273L, 77273435L, 316008252L, 75151514L, 32680821L, 13466291L, 256206912L, 225832678L, 245296564L, 166344225L, 230519898L, 18887784L, 108194240L, 155075127L, 74650975L, 300719094L, 74020064L, 119463325L, 298456636L, 144707310L, 252315645L, 2757974L, 321969537L, 318219488L, 203728303L, 199667954L, 339569618L, 236437494L, 68257532L, 41674788L, 79292517L, 329595997L, 47860047L, 74221291L, 133851496L, 131423110L, 134739242L, 41769882L, 125397753L, 37421241L, 99154118L, 77345313L, 75415599L, 184611253L, 283821969L, 217425962L, 340138445L, 205360342L, 138790530L, 231381162L, 177646695L, 341124928L, 49006892L, 115050903L, 328700132L, 145997181L, 305008536L, 270860151L, 315446483L, 311962310L, 37732254L, 31766142L, 314384689L, 124829645L, 37478454L, 2002208L, 167278182L, 247209778L, 85372494L, 278387860L, 339536290L, 114992793L, 310585351L, 246747223L, 161880752L, 309863480L, 145995082L, 67504260L, 96405640L, 53758185L, 80364252L, 59762590L, 61870224L, 328402109L, 123460961L, 185357220L, 210531620L, 301407876L, 330043666L, 282401604L, 176867483L, 115053574L, 316685038L, 20214140L, 75349137L, 19519076L, 63151532L, 199071277L, 179016942L, 13021588L, 321789792L, 163648942L, 139380103L, 114565842L, 330217875L, 271319530L, 129239990L, 186057800L, 258827287L, 178929042L, 82102774L, 257249581L, 177238145L, 62402069L, 160259722L, 233013151L, 315534334L, 342784710L, 77458610L, 253683167L, 261286212L, 281360242L, 296191980L, 6850988L, 251030736L, 74731345L, 265318802L, 63899879L, 311681497L, 137131395L, 3931149L, 181665422L, 51898522L, 245605974L, 128427927L, 95354166L, 166281164L, 2434663L, 286713155L, 113257227L, 112789726L, 90764238L, 44867204L, 26890740L, 298664607L, 181169292L, 120444705L, 62783316L, 66162809L, 133187974L, 131085619L, 39270565L, 70166946L, 277526912L, 1756312L, 205015274L, 210307520L, 223955976L, 295679311L, 73435047L, 218777227L, 248504688L, 191268148L, 10674541L, 113695358L, 291536722L, 198196536L, 266946574L, 121223151L, 286290221L, 28846473L, 189515583L, 205436167L, 220060181L, 17816194L, 219660836L, 218831760L, 122930261L, 90002096L, 123760813L, 89192098L, 30551277L, 208285091L, 230068868L, 113052860L, 204703894L, 323875798L, 99019268L, 41579225L, 194457264L, 64487982L, 289332899L, 148207072L, 195897417L, 311865514L, 340092471L, 219256369L, 154766L, 299759898L, 311347621L, 323312829L, 63589683L, 246540525L, 151049736L, 2185297L, 179420091L, 34750962L, 84555619L, 100438483L, 120169396L, 157907051L, 225257403L, 293722399L, 111850253L, 323856168L, 338303783L, 314840798L, 190938467L, 125867606L, 234764184L, 327427414L, 142613978L, 215585704L, 261751388L, 316751420L, 121346748L, 193921698L, 138975926L, 44295661L, 343113050L, 10670086L, 262534597L, 58896306L, 100875887L, 105441063L, 338677572L, 273548204L, 304358246L, 247450114L, 126898411L, 281611873L, 65770419L, 88358931L, 108711560L, 169816947L, 276047518L, 179623980L, 8948915L, 211487568L, 135978710L, 122356782L, 61305919L, 25101795L, 291689257L, 141349641L, 198259466L, 256737405L, 116654989L, 45647754L, 180293767L, 142965291L, 182641848L, 320298964L, 104661562L, 159853264L, 63559596L, 77470611L, 155263833L, 24371986L, 4502110L, 307150630L, 142825689L, 191055334L, 272420854L, 266596798L, 310116768L, 100031582L, 330934661L, 131329963L, 205128768L, 34434682L, 264548538L, 275820126L, 58374622L, 126868524L, 247696662L, 230430459L, 247383707L, 213976148L, 4429934L, 55811418L, 182713031L, 135206428L, 78131304L, 73905525L, 122191796L, 303115339L, 249426444L, 196133691L, 50737499L, 39423175L, 38943576L, 63789271L, 15653280L, 42256835L, 76792639L, 18041511L, 28927295L, 167872394L, 132917641L, 221464907L, 306272254L, 168295914L, 311947582L, 115002830L, 173548221L, 66297447L, 38518479L, 186039235L, 166985453L, 170012531L, 110913328L, 2521858L, 164656555L, 78715300L, 137921241L, 31451200L, 69592338L, 244799209L, 30327278L, 311383754L, 324910770L, 31364455L, 227268411L, 250460720L, 69982039L, 258447968L, 48751303L, 166388835L, 160611885L, 321899686L, 248083879L, 91906147L, 70295745L, 73849988L, 252478588L, 34713870L, 338042480L, 280941331L, 10639985L, 58539003L, 256112056L, 301421958L, 251057581L, 265894571L, 25563194L, 195929163L, 142869361L, 47864316L, 339243405L, 278587677L, 209058399L, 28896907L, 235462631L, 259232595L, 244958163L, 23735989L, 146207513L, 291668902L, 343175816L, 205222309L, 282750786L, 266854086L, 311189979L, 107993050L, 55645002L, 248439323L, 110947244L, 127537928L, 20029480L, 91971569L, 91066679L, 187746866L, 177178431L, 199502889L, 212043310L, 196042207L, 211835072L, 122477545L, 18413892L, 161679160L, 35056566L, 338821353L, 276789509L, 206322097L, 18473387L, 327976767L, 80429437L, 279397388L, 68518274L, 181023243L, 237284827L, 313969190L, 15263438L, 51894343L, 9591303L, 82627166L, 239331506L, 239476843L, 289562517L, 139382347L, 242285354L, 17292740L, 188689316L, 235469942L, 117131734L, 266735631L, 326823227L, 117612662L, 76546657L, 295122385L, 12037548L, 189504538L, 95200070L, 293038692L, 31932380L, 187259607L, 73167190L, 170755308L, 218145696L, 236213106L, 108592503L, 131352161L, 60559929L, 42411067L, 280958175L, 8836049L, 297422828L, 11573249L, 91280673L, 125611361L, 161380632L, 226344941L, 134250929L, 140995006L, 98690051L, 155765188L, 164335593L, 80031253L, 199481563L, 69867929L, 39419746L, 228795671L, 19516918L, 167375209L, 89867706L, 72825851L, 242099982L, 14848946L, 42273808L, 126259092L, 304755136L, 38613146L, 122800946L, 267082476L, 167972636L, 196062071L, 254115855L, 39817651L, 309122741L, 60457156L, 250755360L, 20601023L, 243392916L, 292858762L, 180399588L, 313217138L, 29929697L, 60449086L, 283841728L, 160244444L, 241071188L, 321755521L, 108569899L, 143560290L, 272375957L, 331455083L, 14981285L, 32934047L, 262884057L, 281379762L, 227479236L, 105879398L, 272619394L, 284712017L, 190200546L, 171093156L, 34108414L, 325985663L, 199935697L, 224245523L, 144111576L, 153321671L, 286621872L, 35462788L, 214206730L, 126269934L, 65652966L, 284070510L, 6662486L, 325197743L, 38006257L, 50224836L, 124340354L, 154428934L, 7450140L, 287185643L, 33705971L, 141469584L, 272829155L, 286510306L, 246444258L, 170097677L, 319718232L, 330523682L, 140140378L, 10364444L, 160580247L, 27785987L, 34570969L, 134913023L, 14901862L, 115728895L, 78609524L, 201919710L, 13838972L, 34092541L, 198733493L, 47482665L, 251494232L, 16132931L, 38972371L, 240063876L, 117596199L, 162911865L, 262860640L, 52977050L, 77007819L, 254322574L, 230917793L, 56907315L, 187536671L, 158797937L, 155087075L, 285406963L, 223869101L, 209999057L, 86990953L, 177275895L, 51531987L, 75323133L, 136095883L, 79458852L, 284976460L, 336503820L, 248522042L, 242449238L, 205641666L, 53426246L, 117730324L, 10035786L, 176235396L, 119572778L, 246212637L, 259359873L, 106810129L, 68701183L, 223062848L, 116203489L, 128109911L, 250671079L, 143144811L, 122946724L, 97778773L, 14445551L, 298865154L, 220279089L, 290608179L, 139788422L, 238668396L, 208042792L, 131609015L, 171512662L, 87566759L, 307515865L, 299411860L, 322981913L, 275319558L, 215000538L, 298680114L, 174004783L, 223088200L, 81687275L, 147683374L, 191654034L, 69991164L, 17002068L, 330618625L, 9609529L, 80888816L, 152614860L, 150884999L, 256151599L, 329060317L, 211562488L, 80002392L, 53630089L, 14783054L, 243458064L, 201989694L, 173499211L, 84231350L, 173331941L, 304685475L, 186888301L, 246560832L, 235755640L, 112845732L, 306533221L, 45346390L, 159933829L, 204549617L, 65072539L, 250813869L, 230816883L, 281589467L, 307369918L, 341418978L, 323140252L, 73855972L, 83202333L, 37507398L, 171449539L, 2278644L, 159569463L, 171528205L };
        }
        
        static long barr_reduce(final long n) {
            return n - (3L * n >> 30) * 343576577L;
        }
        
        static void ntt(final long[] array, final long[] array2) {
            int i = 512;
            int n = 0;
            while (i > 0) {
                int k;
                for (int j = 0; j < 1024; j = k + i, ++n) {
                    final long n2 = (int)array2[n];
                    for (k = j; k < j + i; ++k) {
                        final int n3 = k + i;
                        final long reduce = reduce(array[n3] * n2);
                        array[n3] = array[k] + (343576577L - reduce);
                        array[k] += reduce;
                    }
                }
                i >>= 1;
            }
        }
        
        static void nttinv(final long[] array, final int n, final long[] array2) {
            int i = 1;
            int n2 = 0;
            while (i < 1024) {
                int k;
                for (int j = 0; j < 1024; j = k + i, ++n2) {
                    final int n3 = (int)array2[n2];
                    for (k = j; k < j + i; ++k) {
                        final int n4 = n + k;
                        final long n5 = array[n4];
                        final int n6 = n4 + i;
                        array[n4] = array[n6] + n5;
                        array[n6] = reduce(n3 * (n5 + (687153154L - array[n6])));
                    }
                }
                final int n7 = i * 2;
                int n9;
                for (int l = 0; l < 1024; l = n9 + n7, ++n2) {
                    final int n8 = (int)array2[n2];
                    for (n9 = l; n9 < l + n7; ++n9) {
                        final int n10 = n + n9;
                        final long n11 = array[n10];
                        final int n12 = n10 + n7;
                        array[n10] = barr_reduce(array[n12] + n11);
                        array[n12] = reduce(n8 * (n11 + (687153154L - array[n12])));
                    }
                }
                i = n7 * 2;
            }
        }
        
        static void nttinv(final long[] array, final long[] array2) {
            final int n = 0;
            int n2 = 1;
            int n3 = 0;
            int i;
            while (true) {
                i = n;
                if (n2 >= 1024) {
                    break;
                }
                int k;
                for (int j = 0; j < 1024; j = k + n2, ++n3) {
                    final int n4 = (int)array2[n3];
                    for (k = j; k < j + n2; ++k) {
                        final long n5 = array[k];
                        if (n2 == 16) {
                            array[k] = barr_reduce(array[k + n2] + n5);
                        }
                        else {
                            array[k] = array[k + n2] + n5;
                        }
                        final int n6 = k + n2;
                        array[n6] = reduce(n4 * (n5 - array[n6]));
                    }
                }
                n2 *= 2;
            }
            while (i < 512) {
                array[i] = reduce(array[i] * 172048372L);
                ++i;
            }
        }
        
        static void poly_add(final long[] array, final long[] array2, final long[] array3) {
            for (int i = 0; i < 1024; ++i) {
                array[i] = array2[i] + array3[i];
            }
        }
        
        static void poly_add_correct(final long[] array, final int n, final long[] array2, final int n2, final long[] array3, final int n3) {
            for (int i = 0; i < 1024; ++i) {
                final int n4 = n + i;
                array[n4] = array2[n2 + i] + array3[n3 + i];
                array[n4] -= 343576577L;
                array[n4] += (0x147A9001L & array[n4] >> 31);
            }
        }
        
        static void poly_mul(final long[] array, final int n, final long[] array2, final int n2, final long[] array3) {
            poly_pointwise(array, n, array2, n2, array3);
            nttinv(array, n, QTesla1PPolynomial.zetainv);
        }
        
        static void poly_mul(final long[] array, final long[] array2, final long[] array3) {
            poly_pointwise(array, array2, array3);
            nttinv(array, QTesla1PPolynomial.zetainv);
        }
        
        static void poly_ntt(final long[] array, final long[] array2) {
            for (int i = 0; i < 1024; ++i) {
                array[i] = array2[i];
            }
            ntt(array, QTesla1PPolynomial.zeta);
        }
        
        static void poly_pointwise(final long[] array, final int n, final long[] array2, final int n2, final long[] array3) {
            for (int i = 0; i < 1024; ++i) {
                array[i + n] = reduce(array2[i + n2] * array3[i]);
            }
        }
        
        static void poly_pointwise(final long[] array, final long[] array2, final long[] array3) {
            for (int i = 0; i < 1024; ++i) {
                array[i] = reduce(array2[i] * array3[i]);
            }
        }
        
        static void poly_sub(final long[] array, final int n, final long[] array2, final int n2, final long[] array3, final int n3) {
            for (int i = 0; i < 1024; ++i) {
                array[n + i] = barr_reduce(array2[n2 + i] - array3[n3 + i]);
            }
        }
        
        static void poly_sub_correct(final int[] array, final int[] array2, final int[] array3) {
            for (int i = 0; i < 1024; ++i) {
                array[i] = array2[i] - array3[i];
                array[i] += (array[i] >> 31 & 0x147A9001);
            }
        }
        
        static void poly_uniform(final long[] array, final byte[] array2, final int n) {
            final byte[] array3 = new byte[18144];
            short n2 = 1;
            HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array3, 0, 18144, (short)0, array2, n, 32);
            int n3 = 108;
            int n4 = 0;
            int i = 0;
            while (i < 4096) {
                int n5 = n4;
                int n6 = n3;
                short n7 = n2;
                if (n4 > n3 * 168 - 16) {
                    n7 = (short)(n2 + 1);
                    HashUtils.customizableSecureHashAlgorithmKECCAK128Simple(array3, 0, 18144, n2, array2, n, 32);
                    n6 = 1;
                    n5 = 0;
                }
                final int n8 = Pack.littleEndianToInt(array3, n5) & 0x1FFFFFFF;
                final int n9 = n5 + 4;
                final int n10 = Pack.littleEndianToInt(array3, n9) & 0x1FFFFFFF;
                final int n11 = n9 + 4;
                final int n12 = Pack.littleEndianToInt(array3, n11) & 0x1FFFFFFF;
                final int n13 = n11 + 4;
                final int n14 = 0x1FFFFFFF & Pack.littleEndianToInt(array3, n13);
                final int n15 = n13 + 4;
                if (n8 < 343576577 && i < 4096) {
                    final int n16 = i + 1;
                    array[i] = reduce(n8 * 13632409L);
                    i = n16;
                }
                int n17 = i;
                if (n10 < 343576577 && (n17 = i) < 4096) {
                    array[i] = reduce(n10 * 13632409L);
                    n17 = i + 1;
                }
                int n18;
                if (n12 < 343576577 && n17 < 4096) {
                    array[n17] = reduce(n12 * 13632409L);
                    n18 = n17 + 1;
                }
                else {
                    n18 = n17;
                }
                n4 = n15;
                n3 = n6;
                n2 = n7;
                i = n18;
                if (n14 < 343576577) {
                    n4 = n15;
                    n3 = n6;
                    n2 = n7;
                    if ((i = n18) >= 4096) {
                        continue;
                    }
                    array[n18] = reduce(n14 * 13632409L);
                    i = n18 + 1;
                    n4 = n15;
                    n3 = n6;
                    n2 = n7;
                }
            }
        }
        
        static long reduce(final long n) {
            return n + (2205847551L * n & 0xFFFFFFFFL) * 343576577L >> 32;
        }
        
        static void sparse_mul16(final int[] array, final int[] array2, final int[] array3, final short[] array4) {
            for (int i = 0; i < 1024; ++i) {
                array[i] = 0;
            }
            for (int j = 0; j < 25; ++j) {
                final int n = array3[j];
                for (int k = 0; k < n; ++k) {
                    array[k] -= array4[j] * array2[k + 1024 - n];
                }
                for (int l = n; l < 1024; ++l) {
                    array[l] += array4[j] * array2[l - n];
                }
            }
        }
        
        static void sparse_mul32(final int[] array, final int[] array2, final int[] array3, final short[] array4) {
            for (int i = 0; i < 1024; ++i) {
                array[i] = 0;
            }
            for (int j = 0; j < 25; ++j) {
                final int n = array3[j];
                for (int k = 0; k < n; ++k) {
                    array[k] -= array4[j] * array2[k + 1024 - n];
                }
                for (int l = n; l < 1024; ++l) {
                    array[l] += array4[j] * array2[l - n];
                }
            }
        }
        
        static void sparse_mul32(final long[] array, final int n, final int[] array2, final int n2, final int[] array3, final short[] array4) {
            for (int i = 0; i < 1024; ++i) {
                array[n + i] = 0L;
            }
            for (int j = 0; j < 25; ++j) {
                final int n3 = array3[j];
                for (int k = 0; k < n3; ++k) {
                    final int n4 = n + k;
                    array[n4] -= array4[j] * array2[n2 + k + 1024 - n3];
                }
                for (int l = n3; l < 1024; ++l) {
                    final int n5 = n + l;
                    array[n5] += array4[j] * array2[n2 + l - n3];
                }
            }
        }
        
        static void sparse_mul8(final long[] array, final int n, final byte[] array2, final int n2, final int[] array3, final short[] array4) {
            for (int i = 0; i < 1024; ++i) {
                array[n + i] = 0L;
            }
            for (int j = 0; j < 25; ++j) {
                final int n3 = array3[j];
                for (int k = 0; k < n3; ++k) {
                    final int n4 = n + k;
                    array[n4] -= array4[j] * array2[n2 + k + 1024 - n3];
                }
                for (int l = n3; l < 1024; ++l) {
                    final int n5 = n + l;
                    array[n5] += array4[j] * array2[n2 + l - n3];
                }
            }
        }
        
        static void sparse_mul8(final long[] array, final byte[] array2, final int[] array3, final short[] array4) {
            for (int i = 0; i < 1024; ++i) {
                array[i] = 0L;
            }
            for (int j = 0; j < 25; ++j) {
                final int n = array3[j];
                for (int k = 0; k < n; ++k) {
                    array[k] -= array4[j] * array2[k + 1024 - n];
                }
                for (int l = n; l < 1024; ++l) {
                    array[l] += array4[j] * array2[l - n];
                }
            }
        }
    }
}
