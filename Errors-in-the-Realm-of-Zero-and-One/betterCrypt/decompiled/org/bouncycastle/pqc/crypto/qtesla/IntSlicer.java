// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.qtesla;

final class IntSlicer
{
    private int base;
    private final int[] values;
    
    IntSlicer(final int[] values, final int base) {
        this.values = values;
        this.base = base;
    }
    
    final int at(final int n) {
        return this.values[this.base + n];
    }
    
    final int at(final int n, final int n2) {
        return this.values[this.base + n] = n2;
    }
    
    final int at(final int n, final long n2) {
        return this.values[this.base + n] = (int)n2;
    }
    
    final IntSlicer copy() {
        return new IntSlicer(this.values, this.base);
    }
    
    final IntSlicer from(final int n) {
        return new IntSlicer(this.values, this.base + n);
    }
    
    final void incBase(final int n) {
        this.base += n;
    }
}
