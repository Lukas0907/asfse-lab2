// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.qtesla;

import org.bouncycastle.crypto.digests.SHAKEDigest;
import org.bouncycastle.crypto.digests.CSHAKEDigest;

class HashUtils
{
    static final int SECURE_HASH_ALGORITHM_KECCAK_128_RATE = 168;
    static final int SECURE_HASH_ALGORITHM_KECCAK_256_RATE = 136;
    
    static void customizableSecureHashAlgorithmKECCAK128Simple(final byte[] array, final int n, final int n2, final short n3, final byte[] array2, final int n4, final int n5) {
        final CSHAKEDigest cshakeDigest = new CSHAKEDigest(128, null, new byte[] { (byte)n3, (byte)(n3 >> 8) });
        cshakeDigest.update(array2, n4, n5);
        cshakeDigest.doFinal(array, n, n2);
    }
    
    static void customizableSecureHashAlgorithmKECCAK256Simple(final byte[] array, final int n, final int n2, final short n3, final byte[] array2, final int n4, final int n5) {
        final CSHAKEDigest cshakeDigest = new CSHAKEDigest(256, null, new byte[] { (byte)n3, (byte)(n3 >> 8) });
        cshakeDigest.update(array2, n4, n5);
        cshakeDigest.doFinal(array, n, n2);
    }
    
    static void secureHashAlgorithmKECCAK128(final byte[] array, final int n, final int n2, final byte[] array2, final int n3, final int n4) {
        final SHAKEDigest shakeDigest = new SHAKEDigest(128);
        shakeDigest.update(array2, n3, n4);
        shakeDigest.doFinal(array, n, n2);
    }
    
    static void secureHashAlgorithmKECCAK256(final byte[] array, final int n, final int n2, final byte[] array2, final int n3, final int n4) {
        final SHAKEDigest shakeDigest = new SHAKEDigest(256);
        shakeDigest.update(array2, n3, n4);
        shakeDigest.doFinal(array, n, n2);
    }
}
