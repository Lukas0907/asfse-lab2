// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.gmss.util;

import java.lang.reflect.Array;
import org.bouncycastle.crypto.Digest;

public class WinternitzOTSignature
{
    private int checksumsize;
    private GMSSRandom gmssRandom;
    private int keysize;
    private int mdsize;
    private Digest messDigestOTS;
    private int messagesize;
    private byte[][] privateKeyOTS;
    private int w;
    
    public WinternitzOTSignature(final byte[] array, final Digest messDigestOTS, int i) {
        this.w = i;
        this.messDigestOTS = messDigestOTS;
        this.gmssRandom = new GMSSRandom(this.messDigestOTS);
        this.mdsize = this.messDigestOTS.getDigestSize();
        final double n = this.mdsize << 3;
        final double n2 = i;
        this.messagesize = (int)Math.ceil(n / n2);
        this.checksumsize = this.getLog((this.messagesize << i) + 1);
        this.keysize = this.messagesize + (int)Math.ceil(this.checksumsize / n2);
        this.privateKeyOTS = (byte[][])Array.newInstance(Byte.TYPE, this.keysize, this.mdsize);
        final byte[] array2 = new byte[this.mdsize];
        final int length = array2.length;
        i = 0;
        System.arraycopy(array, 0, array2, 0, length);
        while (i < this.keysize) {
            this.privateKeyOTS[i] = this.gmssRandom.nextSeed(array2);
            ++i;
        }
    }
    
    public int getLog(final int n) {
        int n2 = 1;
        for (int i = 2; i < n; i <<= 1, ++n2) {}
        return n2;
    }
    
    public byte[][] getPrivateKey() {
        return this.privateKeyOTS;
    }
    
    public byte[] getPublicKey() {
        final int keysize = this.keysize;
        final int mdsize = this.mdsize;
        final byte[] array = new byte[keysize * mdsize];
        final byte[] array2 = new byte[mdsize];
        final int w = this.w;
        for (int i = 0; i < this.keysize; ++i) {
            final Digest messDigestOTS = this.messDigestOTS;
            final byte[][] privateKeyOTS = this.privateKeyOTS;
            messDigestOTS.update(privateKeyOTS[i], 0, privateKeyOTS[i].length);
            byte[] array3 = new byte[this.messDigestOTS.getDigestSize()];
            this.messDigestOTS.doFinal(array3, 0);
            for (int j = 2; j < 1 << w; ++j) {
                this.messDigestOTS.update(array3, 0, array3.length);
                array3 = new byte[this.messDigestOTS.getDigestSize()];
                this.messDigestOTS.doFinal(array3, 0);
            }
            final int mdsize2 = this.mdsize;
            System.arraycopy(array3, 0, array, mdsize2 * i, mdsize2);
        }
        this.messDigestOTS.update(array, 0, array.length);
        final byte[] array4 = new byte[this.messDigestOTS.getDigestSize()];
        this.messDigestOTS.doFinal(array4, 0);
        return array4;
    }
    
    public byte[] getSignature(byte[] array) {
        final int keysize = this.keysize;
        final int mdsize = this.mdsize;
        final byte[] array2 = new byte[keysize * mdsize];
        final byte[] array3 = new byte[mdsize];
        this.messDigestOTS.update(array, 0, array.length);
        final byte[] array4 = new byte[this.messDigestOTS.getDigestSize()];
        this.messDigestOTS.doFinal(array4, 0);
        final int w = this.w;
        if (8 % w == 0) {
            final int n = 8 / w;
            final int n2 = (1 << w) - 1;
            array = new byte[this.mdsize];
            int n3 = 0;
            int i;
            int n4;
            for (n4 = (i = n3); i < array4.length; ++i) {
                int n6;
                for (int j = 0; j < n; ++j, n3 = n6) {
                    final int n5 = array4[i] & n2;
                    n6 = n3 + n5;
                    System.arraycopy(this.privateKeyOTS[n4], 0, array, 0, this.mdsize);
                    for (int k = n5; k > 0; --k) {
                        this.messDigestOTS.update(array, 0, array.length);
                        array = new byte[this.messDigestOTS.getDigestSize()];
                        this.messDigestOTS.doFinal(array, 0);
                    }
                    final int mdsize2 = this.mdsize;
                    System.arraycopy(array, 0, array2, n4 * mdsize2, mdsize2);
                    array4[i] >>>= (byte)this.w;
                    ++n4;
                }
            }
            int n7 = (this.messagesize << this.w) - n3;
            int w2;
            for (int l = 0; l < this.checksumsize; l += w2) {
                int n8 = n7 & n2;
                System.arraycopy(this.privateKeyOTS[n4], 0, array, 0, this.mdsize);
                while (n8 > 0) {
                    this.messDigestOTS.update(array, 0, array.length);
                    array = new byte[this.messDigestOTS.getDigestSize()];
                    this.messDigestOTS.doFinal(array, 0);
                    --n8;
                }
                final int mdsize3 = this.mdsize;
                System.arraycopy(array, 0, array2, n4 * mdsize3, mdsize3);
                w2 = this.w;
                n7 >>>= w2;
                ++n4;
            }
        }
        else if (w < 8) {
            final int mdsize4 = this.mdsize;
            final int n9 = mdsize4 / w;
            final int n10 = (1 << w) - 1;
            array = new byte[mdsize4];
            int n12;
            int n11 = n12 = 0;
            int n14;
            int n13;
            int n18;
            for (n13 = (n14 = n12); n14 < n9; ++n14, n13 = n18) {
                long n15 = 0L;
                for (int n16 = 0; n16 < this.w; ++n16) {
                    n15 ^= (array4[n11] & 0xFF) << (n16 << 3);
                    ++n11;
                }
                final int n17 = 0;
                n18 = n13;
                int n21;
                for (int n19 = n17; n19 < 8; ++n19, n18 = n21) {
                    final int n20 = (int)((long)n10 & n15);
                    n21 = n18 + n20;
                    System.arraycopy(this.privateKeyOTS[n12], 0, array, 0, this.mdsize);
                    for (int n22 = n20; n22 > 0; --n22) {
                        this.messDigestOTS.update(array, 0, array.length);
                        array = new byte[this.messDigestOTS.getDigestSize()];
                        this.messDigestOTS.doFinal(array, 0);
                    }
                    final int mdsize5 = this.mdsize;
                    System.arraycopy(array, 0, array2, n12 * mdsize5, mdsize5);
                    n15 >>>= this.w;
                    ++n12;
                }
            }
            final int n23 = this.mdsize % this.w;
            final int n24 = 0;
            long n25 = 0L;
            int n26 = n11;
            for (int n27 = n24; n27 < n23; ++n27) {
                n25 ^= (array4[n26] & 0xFF) << (n27 << 3);
                ++n26;
            }
            int n30;
            int w3;
            for (int n28 = 0; n28 < n23 << 3; n28 += w3, n13 = n30) {
                final int n29 = (int)(n25 & (long)n10);
                n30 = n13 + n29;
                System.arraycopy(this.privateKeyOTS[n12], 0, array, 0, this.mdsize);
                for (int n31 = n29; n31 > 0; --n31) {
                    this.messDigestOTS.update(array, 0, array.length);
                    array = new byte[this.messDigestOTS.getDigestSize()];
                    this.messDigestOTS.doFinal(array, 0);
                }
                final int mdsize6 = this.mdsize;
                System.arraycopy(array, 0, array2, n12 * mdsize6, mdsize6);
                w3 = this.w;
                n25 >>>= w3;
                ++n12;
            }
            int n32 = (this.messagesize << this.w) - n13;
            int w4;
            for (int n33 = 0; n33 < this.checksumsize; n33 += w4) {
                int n34 = n32 & n10;
                System.arraycopy(this.privateKeyOTS[n12], 0, array, 0, this.mdsize);
                while (n34 > 0) {
                    this.messDigestOTS.update(array, 0, array.length);
                    array = new byte[this.messDigestOTS.getDigestSize()];
                    this.messDigestOTS.doFinal(array, 0);
                    --n34;
                }
                final int mdsize7 = this.mdsize;
                System.arraycopy(array, 0, array2, n12 * mdsize7, mdsize7);
                w4 = this.w;
                n32 >>>= w4;
                ++n12;
            }
        }
        else if (w < 57) {
            final int mdsize8 = this.mdsize;
            final int n35 = (1 << w) - 1;
            array = new byte[mdsize8];
            int n36 = 0;
            int n38;
            int n37 = n38 = n36;
            int n39;
            while (true) {
                n39 = n38;
                if (n39 > (mdsize8 << 3) - w) {
                    break;
                }
                int n40 = n39 >>> 3;
                final int n41 = n39 + this.w;
                int n42 = 0;
                long n43 = 0L;
                while (n40 < n41 + 7 >>> 3) {
                    n43 ^= (array4[n40] & 0xFF) << (n42 << 3);
                    ++n42;
                    ++n40;
                }
                long n44 = n43 >>> n39 % 8 & (long)n35;
                n37 += (int)n44;
                System.arraycopy(this.privateKeyOTS[n36], 0, array, 0, this.mdsize);
                while (n44 > 0L) {
                    this.messDigestOTS.update(array, 0, array.length);
                    array = new byte[this.messDigestOTS.getDigestSize()];
                    this.messDigestOTS.doFinal(array, 0);
                    --n44;
                }
                final int mdsize9 = this.mdsize;
                System.arraycopy(array, 0, array2, n36 * mdsize9, mdsize9);
                ++n36;
                n38 = n41;
            }
            final int n45 = n39 >>> 3;
            int n46 = n36;
            int n47 = n37;
            byte[] array5 = array;
            if (n45 < this.mdsize) {
                int n48 = 0;
                long n49 = 0L;
                int n50 = n45;
                int mdsize10;
                while (true) {
                    mdsize10 = this.mdsize;
                    if (n50 >= mdsize10) {
                        break;
                    }
                    n49 ^= (array4[n50] & 0xFF) << (n48 << 3);
                    ++n48;
                    ++n50;
                }
                long n51 = n49 >>> n39 % 8 & (long)n35;
                n47 = (int)(n37 + n51);
                System.arraycopy(this.privateKeyOTS[n36], 0, array, 0, mdsize10);
                while (n51 > 0L) {
                    this.messDigestOTS.update(array, 0, array.length);
                    array = new byte[this.messDigestOTS.getDigestSize()];
                    this.messDigestOTS.doFinal(array, 0);
                    --n51;
                }
                final int mdsize11 = this.mdsize;
                System.arraycopy(array, 0, array2, n36 * mdsize11, mdsize11);
                n46 = n36 + 1;
                array5 = array;
            }
            int n52 = (this.messagesize << this.w) - n47;
            int w5;
            for (int n53 = 0; n53 < this.checksumsize; n53 += w5) {
                long n54 = n52 & n35;
                System.arraycopy(this.privateKeyOTS[n46], 0, array5, 0, this.mdsize);
                while (n54 > 0L) {
                    this.messDigestOTS.update(array5, 0, array5.length);
                    array5 = new byte[this.messDigestOTS.getDigestSize()];
                    this.messDigestOTS.doFinal(array5, 0);
                    --n54;
                }
                final int mdsize12 = this.mdsize;
                System.arraycopy(array5, 0, array2, n46 * mdsize12, mdsize12);
                w5 = this.w;
                n52 >>>= w5;
                ++n46;
            }
        }
        return array2;
    }
}
