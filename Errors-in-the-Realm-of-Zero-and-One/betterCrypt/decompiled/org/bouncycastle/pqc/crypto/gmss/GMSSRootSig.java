// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.gmss;

import org.bouncycastle.util.encoders.Hex;
import java.lang.reflect.Array;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.pqc.crypto.gmss.util.GMSSRandom;

public class GMSSRootSig
{
    private long big8;
    private int checksum;
    private int counter;
    private GMSSRandom gmssRandom;
    private byte[] hash;
    private int height;
    private int ii;
    private int k;
    private int keysize;
    private int mdsize;
    private Digest messDigestOTS;
    private int messagesize;
    private byte[] privateKeyOTS;
    private int r;
    private byte[] seed;
    private byte[] sign;
    private int steps;
    private int test;
    private long test8;
    private int w;
    
    public GMSSRootSig(final Digest messDigestOTS, final int w, final int height) {
        this.messDigestOTS = messDigestOTS;
        this.gmssRandom = new GMSSRandom(this.messDigestOTS);
        this.mdsize = this.messDigestOTS.getDigestSize();
        this.w = w;
        this.height = height;
        this.k = (1 << w) - 1;
        this.messagesize = (int)Math.ceil((this.mdsize << 3) / (double)w);
    }
    
    public GMSSRootSig(final Digest messDigestOTS, final byte[][] array, final int[] array2) {
        this.messDigestOTS = messDigestOTS;
        this.gmssRandom = new GMSSRandom(this.messDigestOTS);
        this.counter = array2[0];
        this.test = array2[1];
        this.ii = array2[2];
        this.r = array2[3];
        this.steps = array2[4];
        this.keysize = array2[5];
        this.height = array2[6];
        this.w = array2[7];
        this.checksum = array2[8];
        this.mdsize = this.messDigestOTS.getDigestSize();
        final int w = this.w;
        this.k = (1 << w) - 1;
        this.messagesize = (int)Math.ceil((this.mdsize << 3) / (double)w);
        this.privateKeyOTS = array[0];
        this.seed = array[1];
        this.hash = array[2];
        this.sign = array[3];
        this.test8 = ((long)(array[4][2] & 0xFF) << 16 | ((long)(array[4][0] & 0xFF) | (long)(array[4][1] & 0xFF) << 8) | (long)(array[4][3] & 0xFF) << 24 | (long)(array[4][4] & 0xFF) << 32 | (long)(array[4][5] & 0xFF) << 40 | (long)(array[4][6] & 0xFF) << 48 | (long)(array[4][7] & 0xFF) << 56);
        this.big8 = ((long)(array[4][15] & 0xFF) << 56 | ((long)(array[4][8] & 0xFF) | (long)(array[4][9] & 0xFF) << 8 | (long)(array[4][10] & 0xFF) << 16 | (long)(array[4][11] & 0xFF) << 24 | (long)(array[4][12] & 0xFF) << 32 | (long)(array[4][13] & 0xFF) << 40 | (long)(array[4][14] & 0xFF) << 48));
    }
    
    private void oneStep() {
        final int w = this.w;
        if (8 % w != 0) {
            if (w < 8) {
                final int test = this.test;
                if (test == 0) {
                    final int counter = this.counter;
                    if (counter % 8 == 0) {
                        final int ii = this.ii;
                        final int mdsize = this.mdsize;
                        if (ii < mdsize) {
                            this.big8 = 0L;
                            if (counter < mdsize / w << 3) {
                                for (int i = 0; i < this.w; ++i) {
                                    final long big8 = this.big8;
                                    final byte[] hash = this.hash;
                                    final int ii2 = this.ii;
                                    this.big8 = (big8 ^ (long)((hash[ii2] & 0xFF) << (i << 3)));
                                    this.ii = ii2 + 1;
                                }
                            }
                            else {
                                for (int j = 0; j < this.mdsize % this.w; ++j) {
                                    final long big9 = this.big8;
                                    final byte[] hash2 = this.hash;
                                    final int ii3 = this.ii;
                                    this.big8 = (big9 ^ (long)((hash2[ii3] & 0xFF) << (j << 3)));
                                    this.ii = ii3 + 1;
                                }
                            }
                        }
                    }
                    if (this.counter == this.messagesize) {
                        this.big8 = this.checksum;
                    }
                    this.test = (int)(this.big8 & (long)this.k);
                    this.privateKeyOTS = this.gmssRandom.nextSeed(this.seed);
                }
                else if (test > 0) {
                    final Digest messDigestOTS = this.messDigestOTS;
                    final byte[] privateKeyOTS = this.privateKeyOTS;
                    messDigestOTS.update(privateKeyOTS, 0, privateKeyOTS.length);
                    this.privateKeyOTS = new byte[this.messDigestOTS.getDigestSize()];
                    this.messDigestOTS.doFinal(this.privateKeyOTS, 0);
                    --this.test;
                }
                if (this.test != 0) {
                    return;
                }
                final byte[] privateKeyOTS2 = this.privateKeyOTS;
                final byte[] sign = this.sign;
                final int counter2 = this.counter;
                final int mdsize2 = this.mdsize;
                System.arraycopy(privateKeyOTS2, 0, sign, counter2 * mdsize2, mdsize2);
                this.big8 >>>= this.w;
            }
            else {
                if (w >= 57) {
                    return;
                }
                final long test2 = this.test8;
                if (test2 == 0L) {
                    this.big8 = 0L;
                    this.ii = 0;
                    final int r = this.r;
                    int n = r >>> 3;
                    int mdsize3 = this.mdsize;
                    if (n < mdsize3) {
                        if (r <= (mdsize3 << 3) - w) {
                            this.r = r + w;
                            mdsize3 = this.r + 7 >>> 3;
                        }
                        else {
                            this.r = r + w;
                        }
                        long big10;
                        while (true) {
                            big10 = this.big8;
                            if (n >= mdsize3) {
                                break;
                            }
                            final byte b = this.hash[n];
                            final int ii4 = this.ii;
                            this.big8 = (big10 ^ (long)((b & 0xFF) << (ii4 << 3)));
                            this.ii = ii4 + 1;
                            ++n;
                        }
                        this.big8 = big10 >>> r % 8;
                        this.test8 = (this.big8 & (long)this.k);
                    }
                    else {
                        final int checksum = this.checksum;
                        this.test8 = (this.k & checksum);
                        this.checksum = checksum >>> w;
                    }
                    this.privateKeyOTS = this.gmssRandom.nextSeed(this.seed);
                }
                else if (test2 > 0L) {
                    final Digest messDigestOTS2 = this.messDigestOTS;
                    final byte[] privateKeyOTS3 = this.privateKeyOTS;
                    messDigestOTS2.update(privateKeyOTS3, 0, privateKeyOTS3.length);
                    this.privateKeyOTS = new byte[this.messDigestOTS.getDigestSize()];
                    this.messDigestOTS.doFinal(this.privateKeyOTS, 0);
                    --this.test8;
                }
                if (this.test8 != 0L) {
                    return;
                }
                final byte[] privateKeyOTS4 = this.privateKeyOTS;
                final byte[] sign2 = this.sign;
                final int counter3 = this.counter;
                final int mdsize4 = this.mdsize;
                System.arraycopy(privateKeyOTS4, 0, sign2, counter3 * mdsize4, mdsize4);
            }
            ++this.counter;
            return;
        }
        final int test3 = this.test;
        if (test3 == 0) {
            this.privateKeyOTS = this.gmssRandom.nextSeed(this.seed);
            final int ii5 = this.ii;
            if (ii5 < this.mdsize) {
                final byte[] hash3 = this.hash;
                this.test = (hash3[ii5] & this.k);
                hash3[ii5] >>>= (byte)this.w;
            }
            else {
                final int checksum2 = this.checksum;
                this.test = (this.k & checksum2);
                this.checksum = checksum2 >>> this.w;
            }
        }
        else if (test3 > 0) {
            final Digest messDigestOTS3 = this.messDigestOTS;
            final byte[] privateKeyOTS5 = this.privateKeyOTS;
            messDigestOTS3.update(privateKeyOTS5, 0, privateKeyOTS5.length);
            this.privateKeyOTS = new byte[this.messDigestOTS.getDigestSize()];
            this.messDigestOTS.doFinal(this.privateKeyOTS, 0);
            --this.test;
        }
        if (this.test == 0) {
            final byte[] privateKeyOTS6 = this.privateKeyOTS;
            final byte[] sign3 = this.sign;
            final int counter4 = this.counter;
            final int mdsize5 = this.mdsize;
            System.arraycopy(privateKeyOTS6, 0, sign3, counter4 * mdsize5, mdsize5);
            ++this.counter;
            if (this.counter % (8 / this.w) == 0) {
                ++this.ii;
            }
        }
    }
    
    public int getLog(final int n) {
        int n2 = 1;
        for (int i = 2; i < n; i <<= 1, ++n2) {}
        return n2;
    }
    
    public byte[] getSig() {
        return this.sign;
    }
    
    public byte[][] getStatByte() {
        final byte[][] array = (byte[][])Array.newInstance(Byte.TYPE, 5, this.mdsize);
        array[0] = this.privateKeyOTS;
        array[1] = this.seed;
        array[2] = this.hash;
        array[3] = this.sign;
        array[4] = this.getStatLong();
        return array;
    }
    
    public int[] getStatInt() {
        return new int[] { this.counter, this.test, this.ii, this.r, this.steps, this.keysize, this.height, this.w, this.checksum };
    }
    
    public byte[] getStatLong() {
        final long test8 = this.test8;
        final byte b = (byte)(test8 & 0xFFL);
        final byte b2 = (byte)(test8 >> 8 & 0xFFL);
        final byte b3 = (byte)(test8 >> 16 & 0xFFL);
        final byte b4 = (byte)(test8 >> 24 & 0xFFL);
        final byte b5 = (byte)(test8 >> 32 & 0xFFL);
        final byte b6 = (byte)(test8 >> 40 & 0xFFL);
        final byte b7 = (byte)(test8 >> 48 & 0xFFL);
        final byte b8 = (byte)(test8 >> 56 & 0xFFL);
        final long big8 = this.big8;
        return new byte[] { b, b2, b3, b4, b5, b6, b7, b8, (byte)(big8 & 0xFFL), (byte)(big8 >> 8 & 0xFFL), (byte)(big8 >> 16 & 0xFFL), (byte)(big8 >> 24 & 0xFFL), (byte)(big8 >> 32 & 0xFFL), (byte)(big8 >> 40 & 0xFFL), (byte)(big8 >> 48 & 0xFFL), (byte)(big8 >> 56 & 0xFFL) };
    }
    
    public void initSign(final byte[] array, byte[] array2) {
        this.hash = new byte[this.mdsize];
        this.messDigestOTS.update(array2, 0, array2.length);
        this.hash = new byte[this.messDigestOTS.getDigestSize()];
        this.messDigestOTS.doFinal(this.hash, 0);
        final int mdsize = this.mdsize;
        array2 = new byte[mdsize];
        System.arraycopy(this.hash, 0, array2, 0, mdsize);
        final int log = this.getLog((this.messagesize << this.w) + 1);
        final int w = this.w;
        int n5;
        if (8 % w == 0) {
            final int n = 8 / w;
            int n2;
            for (int i = n2 = 0; i < this.mdsize; ++i) {
                for (int j = 0; j < n; ++j) {
                    n2 += (array2[i] & this.k);
                    array2[i] >>>= (byte)this.w;
                }
            }
            this.checksum = (this.messagesize << this.w) - n2;
            int checksum = this.checksum;
            int n3 = 0;
            int n4 = n2;
            while (true) {
                n5 = n4;
                if (n3 >= log) {
                    break;
                }
                n4 += (this.k & checksum);
                final int w2 = this.w;
                checksum >>>= w2;
                n3 += w2;
            }
        }
        else if (w < 8) {
            final int n6 = this.mdsize / w;
            int k = 0;
            int n8;
            int n7 = n8 = k;
            while (k < n6) {
                long n9 = 0L;
                for (int l = 0; l < this.w; ++l) {
                    n9 ^= (array2[n7] & 0xFF) << (l << 3);
                    ++n7;
                }
                for (int n10 = 0; n10 < 8; ++n10) {
                    n8 += (int)((long)this.k & n9);
                    n9 >>>= this.w;
                }
                ++k;
            }
            final int n11 = this.mdsize % this.w;
            final int n12 = 0;
            long n13 = 0L;
            int n14 = n7;
            for (int n15 = n12; n15 < n11; ++n15) {
                n13 ^= (array2[n14] & 0xFF) << (n15 << 3);
                ++n14;
            }
            int w3;
            for (int n16 = 0; n16 < n11 << 3; n16 += w3) {
                n8 += (int)((long)this.k & n13);
                w3 = this.w;
                n13 >>>= w3;
            }
            this.checksum = (this.messagesize << this.w) - n8;
            int checksum2 = this.checksum;
            int w4;
            for (int n17 = 0; n17 < log; n17 += w4) {
                n8 += (this.k & checksum2);
                w4 = this.w;
                checksum2 >>>= w4;
            }
            n5 = n8;
        }
        else if (w < 57) {
            int n19;
            int n18 = n19 = 0;
            int n20;
            int mdsize2;
            while (true) {
                n20 = n18;
                mdsize2 = this.mdsize;
                final int w5 = this.w;
                if (n20 > (mdsize2 << 3) - w5) {
                    break;
                }
                int n21 = n20 >>> 3;
                final int n22 = n20 + w5;
                int n23 = 0;
                long n24 = 0L;
                while (n21 < n22 + 7 >>> 3) {
                    n24 ^= (array2[n21] & 0xFF) << (n23 << 3);
                    ++n23;
                    ++n21;
                }
                n19 += (int)(n24 >>> n20 % 8 & (long)this.k);
                n18 = n22;
            }
            final int n25 = n20 >>> 3;
            int n26 = n19;
            if (n25 < mdsize2) {
                final int n27 = 0;
                long n28 = 0L;
                int n29 = n25;
                int n30 = n27;
                while (n29 < this.mdsize) {
                    n28 ^= (array2[n29] & 0xFF) << (n30 << 3);
                    ++n30;
                    ++n29;
                }
                n26 = (int)(n19 + (n28 >>> n20 % 8 & (long)this.k));
            }
            this.checksum = (this.messagesize << this.w) - n26;
            int checksum3 = this.checksum;
            int n31 = 0;
            while (true) {
                n5 = n26;
                if (n31 >= log) {
                    break;
                }
                n26 += (this.k & checksum3);
                final int w6 = this.w;
                checksum3 >>>= w6;
                n31 += w6;
            }
        }
        else {
            n5 = 0;
        }
        this.keysize = this.messagesize + (int)Math.ceil(log / (double)this.w);
        this.steps = (int)Math.ceil((this.keysize + n5) / (double)(1 << this.height));
        final int keysize = this.keysize;
        final int mdsize3 = this.mdsize;
        this.sign = new byte[keysize * mdsize3];
        this.counter = 0;
        this.test = 0;
        this.ii = 0;
        this.test8 = 0L;
        this.r = 0;
        this.privateKeyOTS = new byte[mdsize3];
        System.arraycopy(array, 0, this.seed = new byte[mdsize3], 0, mdsize3);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(this.big8);
        sb.append("  ");
        String str = sb.toString();
        final int[] statInt = this.getStatInt();
        final byte[][] array = (byte[][])Array.newInstance(Byte.TYPE, 5, this.mdsize);
        final byte[][] statByte = this.getStatByte();
        final int n = 0;
        int n2 = 0;
        int i;
        String string;
        while (true) {
            i = n;
            string = str;
            if (n2 >= 9) {
                break;
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append(statInt[n2]);
            sb2.append(" ");
            str = sb2.toString();
            ++n2;
        }
        while (i < 5) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(string);
            sb3.append(new String(Hex.encode(statByte[i])));
            sb3.append(" ");
            string = sb3.toString();
            ++i;
        }
        return string;
    }
    
    public boolean updateSign() {
        for (int i = 0; i < this.steps; ++i) {
            if (this.counter < this.keysize) {
                this.oneStep();
            }
            if (this.counter == this.keysize) {
                return true;
            }
        }
        return false;
    }
}
