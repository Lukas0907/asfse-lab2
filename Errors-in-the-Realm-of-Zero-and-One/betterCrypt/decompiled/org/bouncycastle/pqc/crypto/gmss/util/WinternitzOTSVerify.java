// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.gmss.util;

import org.bouncycastle.crypto.Digest;

public class WinternitzOTSVerify
{
    private Digest messDigestOTS;
    private int w;
    
    public WinternitzOTSVerify(final Digest messDigestOTS, final int w) {
        this.w = w;
        this.messDigestOTS = messDigestOTS;
    }
    
    public byte[] Verify(byte[] array, byte[] array2) {
        final byte[] array3 = array2;
        final int digestSize = this.messDigestOTS.getDigestSize();
        final byte[] array4 = new byte[digestSize];
        this.messDigestOTS.update(array, 0, array.length);
        final byte[] array5 = new byte[this.messDigestOTS.getDigestSize()];
        this.messDigestOTS.doFinal(array5, 0);
        final int n = digestSize << 3;
        final int w = this.w;
        final int n2 = (w - 1 + n) / w;
        final int log = this.getLog((n2 << w) + 1);
        final int w2 = this.w;
        final int n3 = ((log + w2 - 1) / w2 + n2) * digestSize;
        if (n3 != array3.length) {
            return null;
        }
        final byte[] array6 = new byte[n3];
        byte[] array7;
        if (8 % w2 == 0) {
            final int n4 = 8 / w2;
            final int n5 = (1 << w2) - 1;
            array = new byte[digestSize];
            int n6 = 0;
            int i;
            int n7;
            for (n7 = (i = n6); i < array5.length; ++i) {
                for (int j = 0; j < n4; ++j) {
                    int k = array5[i] & n5;
                    n6 += k;
                    final int n8 = n7 * digestSize;
                    System.arraycopy(array2, n8, array, 0, digestSize);
                    while (k < n5) {
                        this.messDigestOTS.update(array, 0, array.length);
                        array = new byte[this.messDigestOTS.getDigestSize()];
                        this.messDigestOTS.doFinal(array, 0);
                        ++k;
                    }
                    System.arraycopy(array, 0, array6, n8, digestSize);
                    array5[i] >>>= (byte)this.w;
                    ++n7;
                }
            }
            int n9 = (n2 << this.w) - n6;
            int n10 = 0;
            while (true) {
                array7 = array6;
                if (n10 >= log) {
                    break;
                }
                int l = n9 & n5;
                final int n11 = n7 * digestSize;
                System.arraycopy(array2, n11, array, 0, digestSize);
                while (l < n5) {
                    this.messDigestOTS.update(array, 0, array.length);
                    array = new byte[this.messDigestOTS.getDigestSize()];
                    this.messDigestOTS.doFinal(array, 0);
                    ++l;
                }
                System.arraycopy(array, 0, array6, n11, digestSize);
                final int w3 = this.w;
                n9 >>>= w3;
                ++n7;
                n10 += w3;
            }
        }
        else if (w2 < 8) {
            final int n12 = digestSize / w2;
            final int n13 = (1 << w2) - 1;
            array = new byte[digestSize];
            int n15;
            int n14 = n15 = 0;
            int n17;
            int n16;
            int n21;
            for (n16 = (n17 = n15); n17 < n12; ++n17, n15 = n21) {
                long n18 = 0L;
                for (int n19 = 0; n19 < this.w; ++n19) {
                    n18 ^= (array5[n14] & 0xFF) << (n19 << 3);
                    ++n14;
                }
                final int n20 = 0;
                n21 = n15;
                int n24;
                for (int n22 = n20; n22 < 8; ++n22, n21 = n24) {
                    final int n23 = (int)(n18 & (long)n13);
                    n24 = n21 + n23;
                    final int n25 = n16 * digestSize;
                    System.arraycopy(array3, n25, array, 0, digestSize);
                    for (int n26 = n23; n26 < n13; ++n26) {
                        this.messDigestOTS.update(array, 0, array.length);
                        array = new byte[this.messDigestOTS.getDigestSize()];
                        this.messDigestOTS.doFinal(array, 0);
                    }
                    System.arraycopy(array, 0, array6, n25, digestSize);
                    n18 >>>= this.w;
                    ++n16;
                }
            }
            final int n27 = digestSize % this.w;
            final int n28 = 0;
            long n29 = 0L;
            int n30 = n14;
            for (int n31 = n28; n31 < n27; ++n31) {
                n29 ^= (array5[n30] & 0xFF) << (n31 << 3);
                ++n30;
            }
            int n34;
            int w4;
            for (int n32 = 0; n32 < n27 << 3; n32 += w4, n15 = n34) {
                final int n33 = (int)(n29 & (long)n13);
                n34 = n15 + n33;
                final int n35 = n16 * digestSize;
                System.arraycopy(array3, n35, array, 0, digestSize);
                for (int n36 = n33; n36 < n13; ++n36) {
                    this.messDigestOTS.update(array, 0, array.length);
                    array = new byte[this.messDigestOTS.getDigestSize()];
                    this.messDigestOTS.doFinal(array, 0);
                }
                System.arraycopy(array, 0, array6, n35, digestSize);
                w4 = this.w;
                n29 >>>= w4;
                ++n16;
            }
            int n37 = (n2 << this.w) - n15;
            int n38 = 0;
            while (true) {
                array7 = array6;
                if (n38 >= log) {
                    break;
                }
                int n39 = n37 & n13;
                final int n40 = n16 * digestSize;
                System.arraycopy(array3, n40, array, 0, digestSize);
                while (n39 < n13) {
                    this.messDigestOTS.update(array, 0, array.length);
                    array = new byte[this.messDigestOTS.getDigestSize()];
                    this.messDigestOTS.doFinal(array, 0);
                    ++n39;
                }
                System.arraycopy(array, 0, array6, n40, digestSize);
                final int w5 = this.w;
                n37 >>>= w5;
                ++n16;
                n38 += w5;
            }
        }
        else {
            array7 = array6;
            if (w2 < 57) {
                int n41 = n - w2;
                final int n42 = (1 << w2) - 1;
                array = new byte[digestSize];
                int n43 = 0;
                int n45;
                int n44 = n45 = n43;
                int n46 = log;
                while (n43 <= n41) {
                    int n47 = n43 >>> 3;
                    final int n48 = n43 + this.w;
                    int n49 = 0;
                    long n50 = 0L;
                    final int n51 = n46;
                    final int n52 = n41;
                    while (n47 < n48 + 7 >>> 3) {
                        n50 ^= (array5[n47] & 0xFF) << (n49 << 3);
                        ++n49;
                        ++n47;
                    }
                    final long n53 = n42;
                    final long n54 = n50 >>> n43 % 8 & n53;
                    n44 += (int)n54;
                    final int n55 = n45 * digestSize;
                    System.arraycopy(array3, n55, array, 0, digestSize);
                    for (long n56 = n53, n57 = n54; n57 < n56; ++n57) {
                        this.messDigestOTS.update(array, 0, array.length);
                        array = new byte[this.messDigestOTS.getDigestSize()];
                        this.messDigestOTS.doFinal(array, 0);
                    }
                    System.arraycopy(array, 0, array6, n55, digestSize);
                    ++n45;
                    n43 = n48;
                    n41 = n52;
                    n46 = n51;
                }
                int n58 = n43 >>> 3;
                if (n58 < digestSize) {
                    int n59 = 0;
                    long n60 = 0L;
                    while (n58 < digestSize) {
                        n60 ^= (array5[n58] & 0xFF) << (n59 << 3);
                        ++n59;
                        ++n58;
                    }
                    final long n61 = n42;
                    final long n62 = n60 >>> n43 % 8 & n61;
                    n44 += (int)n62;
                    final int n63 = n45 * digestSize;
                    System.arraycopy(array3, n63, array, 0, digestSize);
                    for (long n64 = n61, n65 = n62; n65 < n64; ++n65) {
                        this.messDigestOTS.update(array, 0, array.length);
                        array = new byte[this.messDigestOTS.getDigestSize()];
                        this.messDigestOTS.doFinal(array, 0);
                    }
                    System.arraycopy(array, 0, array6, n63, digestSize);
                    ++n45;
                }
                final int n66 = n42;
                int n67 = (n2 << this.w) - n44;
                int n68 = 0;
                array2 = array;
                array = array6;
                while (true) {
                    array7 = array;
                    if (n68 >= n46) {
                        break;
                    }
                    long n69 = n67 & n66;
                    final int n70 = n45 * digestSize;
                    System.arraycopy(array3, n70, array2, 0, digestSize);
                    while (n69 < n66) {
                        this.messDigestOTS.update(array2, 0, array2.length);
                        array2 = new byte[this.messDigestOTS.getDigestSize()];
                        this.messDigestOTS.doFinal(array2, 0);
                        ++n69;
                    }
                    System.arraycopy(array2, 0, array, n70, digestSize);
                    final int w6 = this.w;
                    n67 >>>= w6;
                    ++n45;
                    n68 += w6;
                }
            }
        }
        array = new byte[digestSize];
        this.messDigestOTS.update(array7, 0, array7.length);
        array = new byte[this.messDigestOTS.getDigestSize()];
        this.messDigestOTS.doFinal(array, 0);
        return array;
    }
    
    public int getLog(final int n) {
        int n2 = 1;
        for (int i = 2; i < n; i <<= 1, ++n2) {}
        return n2;
    }
    
    public int getSignatureLength() {
        final int digestSize = this.messDigestOTS.getDigestSize();
        final int w = this.w;
        final int n = ((digestSize << 3) + (w - 1)) / w;
        final int log = this.getLog((n << w) + 1);
        final int w2 = this.w;
        return digestSize * (n + (log + w2 - 1) / w2);
    }
}
