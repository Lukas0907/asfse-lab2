// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.gmss;

import org.bouncycastle.util.Integers;
import org.bouncycastle.util.encoders.Hex;
import java.util.Enumeration;
import org.bouncycastle.util.Arrays;
import java.lang.reflect.Array;
import org.bouncycastle.crypto.Digest;
import java.util.Vector;

public class GMSSRootCalc
{
    private byte[][] AuthPath;
    private int K;
    private GMSSDigestProvider digestProvider;
    private int heightOfNextSeed;
    private Vector heightOfNodes;
    private int heightOfTree;
    private int[] index;
    private int indexForNextSeed;
    private boolean isFinished;
    private boolean isInitialized;
    private int mdLength;
    private Digest messDigestTree;
    private Vector[] retain;
    private byte[] root;
    private Vector tailStack;
    private Treehash[] treehash;
    
    public GMSSRootCalc(int i, final int k, final GMSSDigestProvider digestProvider) {
        this.heightOfTree = i;
        this.digestProvider = digestProvider;
        this.messDigestTree = digestProvider.get();
        this.mdLength = this.messDigestTree.getDigestSize();
        this.K = k;
        this.index = new int[i];
        this.AuthPath = (byte[][])Array.newInstance(Byte.TYPE, i, this.mdLength);
        this.root = new byte[this.mdLength];
        this.retain = new Vector[this.K - 1];
        for (i = 0; i < k - 1; ++i) {
            this.retain[i] = new Vector();
        }
    }
    
    public byte[][] getAuthPath() {
        return GMSSUtils.clone(this.AuthPath);
    }
    
    public Vector[] getRetain() {
        return GMSSUtils.clone(this.retain);
    }
    
    public byte[] getRoot() {
        return Arrays.clone(this.root);
    }
    
    public Vector getStack() {
        final Vector<Object> vector = new Vector<Object>();
        final Enumeration<Object> elements = this.tailStack.elements();
        while (elements.hasMoreElements()) {
            vector.addElement(elements.nextElement());
        }
        return vector;
    }
    
    public byte[][] getStatByte() {
        final Vector tailStack = this.tailStack;
        final int n = 0;
        int size;
        if (tailStack == null) {
            size = 0;
        }
        else {
            size = tailStack.size();
        }
        final byte[][] array = (byte[][])Array.newInstance(Byte.TYPE, this.heightOfTree + 1 + size, 64);
        array[0] = this.root;
        int n2 = 0;
        int i;
        while (true) {
            i = n;
            if (n2 >= this.heightOfTree) {
                break;
            }
            final int n3 = n2 + 1;
            array[n3] = this.AuthPath[n2];
            n2 = n3;
        }
        while (i < size) {
            array[this.heightOfTree + 1 + i] = (byte[])this.tailStack.elementAt(i);
            ++i;
        }
        return array;
    }
    
    public int[] getStatInt() {
        final Vector tailStack = this.tailStack;
        final int n = 0;
        int size;
        if (tailStack == null) {
            size = 0;
        }
        else {
            size = tailStack.size();
        }
        final int heightOfTree = this.heightOfTree;
        final int[] array = new int[heightOfTree + 8 + size];
        array[0] = heightOfTree;
        array[1] = this.mdLength;
        array[2] = this.K;
        array[3] = this.indexForNextSeed;
        array[4] = this.heightOfNextSeed;
        if (this.isFinished) {
            array[5] = 1;
        }
        else {
            array[5] = 0;
        }
        if (this.isInitialized) {
            array[6] = 1;
        }
        else {
            array[6] = 0;
        }
        array[7] = size;
        int n2 = 0;
        int i;
        while (true) {
            i = n;
            if (n2 >= this.heightOfTree) {
                break;
            }
            array[n2 + 8] = this.index[n2];
            ++n2;
        }
        while (i < size) {
            array[this.heightOfTree + 8 + i] = (int)this.heightOfNodes.elementAt(i);
            ++i;
        }
        return array;
    }
    
    public Treehash[] getTreehash() {
        return GMSSUtils.clone(this.treehash);
    }
    
    public void initialize(final Vector vector) {
        this.treehash = new Treehash[this.heightOfTree - this.K];
        int n = 0;
        int heightOfTree;
        while (true) {
            heightOfTree = this.heightOfTree;
            if (n >= heightOfTree - this.K) {
                break;
            }
            this.treehash[n] = new Treehash(vector, n, this.digestProvider.get());
            ++n;
        }
        this.index = new int[heightOfTree];
        this.AuthPath = (byte[][])Array.newInstance(Byte.TYPE, heightOfTree, this.mdLength);
        this.root = new byte[this.mdLength];
        this.tailStack = new Vector();
        this.heightOfNodes = new Vector();
        this.isInitialized = true;
        this.isFinished = false;
        for (int i = 0; i < this.heightOfTree; ++i) {
            this.index[i] = -1;
        }
        this.retain = new Vector[this.K - 1];
        for (int j = 0; j < this.K - 1; ++j) {
            this.retain[j] = new Vector();
        }
        this.indexForNextSeed = 3;
        this.heightOfNextSeed = 0;
    }
    
    public void initializeTreehashSeed(final byte[] array, final int n) {
        this.treehash[n].initializeSeed(array);
    }
    
    @Override
    public String toString() {
        final Vector tailStack = this.tailStack;
        final int n = 0;
        int size;
        if (tailStack == null) {
            size = 0;
        }
        else {
            size = tailStack.size();
        }
        String string = "";
        int n2 = 0;
        int i;
        String string2;
        while (true) {
            i = n;
            string2 = string;
            if (n2 >= this.heightOfTree + 8 + size) {
                break;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(string);
            sb.append(this.getStatInt()[n2]);
            sb.append(" ");
            string = sb.toString();
            ++n2;
        }
        while (i < this.heightOfTree + 1 + size) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(string2);
            sb2.append(new String(Hex.encode(this.getStatByte()[i])));
            sb2.append(" ");
            string2 = sb2.toString();
            ++i;
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(string2);
        sb3.append("  ");
        sb3.append(this.digestProvider.get().getDigestSize());
        return sb3.toString();
    }
    
    public void update(byte[] array) {
        if (this.isFinished) {
            System.out.print("Too much updates for Tree!!");
            return;
        }
        if (!this.isInitialized) {
            System.err.println("GMSSRootCalc not initialized!");
            return;
        }
        final int[] index = this.index;
        ++index[0];
        if (index[0] == 1) {
            System.arraycopy(array, 0, this.AuthPath[0], 0, this.mdLength);
        }
        else if (index[0] == 3 && this.heightOfTree > this.K) {
            this.treehash[0].setFirstNode(array);
        }
        final int[] index2 = this.index;
        if ((index2[0] - 3) % 2 == 0 && index2[0] >= 3 && this.heightOfTree == this.K) {
            this.retain[0].insertElementAt(array, 0);
        }
        if (this.index[0] == 0) {
            this.tailStack.addElement(array);
            this.heightOfNodes.addElement(Integers.valueOf(0));
            return;
        }
        final int mdLength = this.mdLength;
        final byte[] array2 = new byte[mdLength];
        final byte[] array3 = new byte[mdLength << 1];
        System.arraycopy(array, 0, array2, 0, mdLength);
        int n = 0;
        array = array2;
        while (this.tailStack.size() > 0 && n == this.heightOfNodes.lastElement()) {
            System.arraycopy(this.tailStack.lastElement(), 0, array3, 0, this.mdLength);
            final Vector tailStack = this.tailStack;
            tailStack.removeElementAt(tailStack.size() - 1);
            final Vector heightOfNodes = this.heightOfNodes;
            heightOfNodes.removeElementAt(heightOfNodes.size() - 1);
            final int mdLength2 = this.mdLength;
            System.arraycopy(array, 0, array3, mdLength2, mdLength2);
            this.messDigestTree.update(array3, 0, array3.length);
            final byte[] array4 = new byte[this.messDigestTree.getDigestSize()];
            this.messDigestTree.doFinal(array4, 0);
            final int n2 = n + 1;
            array = array4;
            if ((n = n2) < this.heightOfTree) {
                final int[] index3 = this.index;
                ++index3[n2];
                if (index3[n2] == 1) {
                    System.arraycopy(array4, 0, this.AuthPath[n2], 0, this.mdLength);
                }
                if (n2 >= this.heightOfTree - this.K) {
                    if (n2 == 0) {
                        System.out.println("M\ufffd\ufffd\ufffdP");
                    }
                    final int[] index4 = this.index;
                    array = array4;
                    n = n2;
                    if ((index4[n2] - 3) % 2 != 0) {
                        continue;
                    }
                    array = array4;
                    n = n2;
                    if (index4[n2] < 3) {
                        continue;
                    }
                    this.retain[n2 - (this.heightOfTree - this.K)].insertElementAt(array4, 0);
                    array = array4;
                    n = n2;
                }
                else {
                    array = array4;
                    n = n2;
                    if (this.index[n2] != 3) {
                        continue;
                    }
                    this.treehash[n2].setFirstNode(array4);
                    array = array4;
                    n = n2;
                }
            }
        }
        this.tailStack.addElement(array);
        this.heightOfNodes.addElement(Integers.valueOf(n));
        if (n == this.heightOfTree) {
            this.isFinished = true;
            this.isInitialized = false;
            this.root = this.tailStack.lastElement();
        }
    }
    
    public void update(final byte[] array, final byte[] array2) {
        final int heightOfNextSeed = this.heightOfNextSeed;
        if (heightOfNextSeed < this.heightOfTree - this.K && this.indexForNextSeed - 2 == this.index[0]) {
            this.initializeTreehashSeed(array, heightOfNextSeed);
            ++this.heightOfNextSeed;
            this.indexForNextSeed *= 2;
        }
        this.update(array2);
    }
    
    public boolean wasFinished() {
        return this.isFinished;
    }
    
    public boolean wasInitialized() {
        return this.isInitialized;
    }
}
