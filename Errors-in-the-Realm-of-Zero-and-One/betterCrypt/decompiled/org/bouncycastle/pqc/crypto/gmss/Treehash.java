// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.gmss;

import org.bouncycastle.pqc.crypto.gmss.util.GMSSRandom;
import org.bouncycastle.util.encoders.Hex;
import java.io.PrintStream;
import java.lang.reflect.Array;
import org.bouncycastle.util.Integers;
import org.bouncycastle.crypto.Digest;
import java.util.Vector;

public class Treehash
{
    private byte[] firstNode;
    private int firstNodeHeight;
    private Vector heightOfNodes;
    private boolean isFinished;
    private boolean isInitialized;
    private int maxHeight;
    private Digest messDigestTree;
    private byte[] seedActive;
    private boolean seedInitialized;
    private byte[] seedNext;
    private int tailLength;
    private Vector tailStack;
    
    public Treehash(final Vector tailStack, final int maxHeight, final Digest messDigestTree) {
        this.tailStack = tailStack;
        this.maxHeight = maxHeight;
        this.firstNode = null;
        this.isInitialized = false;
        this.isFinished = false;
        this.seedInitialized = false;
        this.messDigestTree = messDigestTree;
        this.seedNext = new byte[this.messDigestTree.getDigestSize()];
        this.seedActive = new byte[this.messDigestTree.getDigestSize()];
    }
    
    public Treehash(final Digest messDigestTree, final byte[][] array, final int[] array2) {
        this.messDigestTree = messDigestTree;
        final int n = 0;
        this.maxHeight = array2[0];
        this.tailLength = array2[1];
        this.firstNodeHeight = array2[2];
        if (array2[3] == 1) {
            this.isFinished = true;
        }
        else {
            this.isFinished = false;
        }
        if (array2[4] == 1) {
            this.isInitialized = true;
        }
        else {
            this.isInitialized = false;
        }
        if (array2[5] == 1) {
            this.seedInitialized = true;
        }
        else {
            this.seedInitialized = false;
        }
        this.heightOfNodes = new Vector();
        for (int i = 0; i < this.tailLength; ++i) {
            this.heightOfNodes.addElement(Integers.valueOf(array2[i + 6]));
        }
        this.firstNode = array[0];
        this.seedActive = array[1];
        this.seedNext = array[2];
        this.tailStack = new Vector();
        for (int j = n; j < this.tailLength; ++j) {
            this.tailStack.addElement(array[j + 3]);
        }
    }
    
    public void destroy() {
        this.isInitialized = false;
        this.isFinished = false;
        this.firstNode = null;
        this.tailLength = 0;
        this.firstNodeHeight = -1;
    }
    
    public byte[] getFirstNode() {
        return this.firstNode;
    }
    
    public int getFirstNodeHeight() {
        if (this.firstNode == null) {
            return this.maxHeight;
        }
        return this.firstNodeHeight;
    }
    
    public int getLowestNodeHeight() {
        if (this.firstNode == null) {
            return this.maxHeight;
        }
        if (this.tailLength == 0) {
            return this.firstNodeHeight;
        }
        return Math.min(this.firstNodeHeight, this.heightOfNodes.lastElement());
    }
    
    public byte[] getSeedActive() {
        return this.seedActive;
    }
    
    public byte[][] getStatByte() {
        final byte[][] array = (byte[][])Array.newInstance(Byte.TYPE, this.tailLength + 3, this.messDigestTree.getDigestSize());
        final byte[] firstNode = this.firstNode;
        int i = 0;
        array[0] = firstNode;
        array[1] = this.seedActive;
        array[2] = this.seedNext;
        while (i < this.tailLength) {
            array[i + 3] = (byte[])this.tailStack.elementAt(i);
            ++i;
        }
        return array;
    }
    
    public int[] getStatInt() {
        final int tailLength = this.tailLength;
        final int[] array = new int[tailLength + 6];
        final int maxHeight = this.maxHeight;
        int i = 0;
        array[0] = maxHeight;
        array[1] = tailLength;
        array[2] = this.firstNodeHeight;
        if (this.isFinished) {
            array[3] = 1;
        }
        else {
            array[3] = 0;
        }
        if (this.isInitialized) {
            array[4] = 1;
        }
        else {
            array[4] = 0;
        }
        if (this.seedInitialized) {
            array[5] = 1;
        }
        else {
            array[5] = 0;
        }
        while (i < this.tailLength) {
            array[i + 6] = (int)this.heightOfNodes.elementAt(i);
            ++i;
        }
        return array;
    }
    
    public Vector getTailStack() {
        return this.tailStack;
    }
    
    public void initialize() {
        if (!this.seedInitialized) {
            final PrintStream err = System.err;
            final StringBuilder sb = new StringBuilder();
            sb.append("Seed ");
            sb.append(this.maxHeight);
            sb.append(" not initialized");
            err.println(sb.toString());
            return;
        }
        this.heightOfNodes = new Vector();
        this.tailLength = 0;
        this.firstNode = null;
        this.firstNodeHeight = -1;
        this.isInitialized = true;
        System.arraycopy(this.seedNext, 0, this.seedActive, 0, this.messDigestTree.getDigestSize());
    }
    
    public void initializeSeed(final byte[] array) {
        System.arraycopy(array, 0, this.seedNext, 0, this.messDigestTree.getDigestSize());
        this.seedInitialized = true;
    }
    
    public void setFirstNode(final byte[] firstNode) {
        if (!this.isInitialized) {
            this.initialize();
        }
        this.firstNode = firstNode;
        this.firstNodeHeight = this.maxHeight;
        this.isFinished = true;
    }
    
    @Override
    public String toString() {
        final int n = 0;
        String string = "Treehash    : ";
        int n2 = 0;
        int i;
        String string2;
        while (true) {
            i = n;
            string2 = string;
            if (n2 >= this.tailLength + 6) {
                break;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(string);
            sb.append(this.getStatInt()[n2]);
            sb.append(" ");
            string = sb.toString();
            ++n2;
        }
        while (i < this.tailLength + 3) {
            StringBuilder sb2;
            if (this.getStatByte()[i] != null) {
                sb2 = new StringBuilder();
                sb2.append(string2);
                sb2.append(new String(Hex.encode(this.getStatByte()[i])));
                sb2.append(" ");
            }
            else {
                sb2 = new StringBuilder();
                sb2.append(string2);
                sb2.append("null ");
            }
            string2 = sb2.toString();
            ++i;
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(string2);
        sb3.append("  ");
        sb3.append(this.messDigestTree.getDigestSize());
        return sb3.toString();
    }
    
    public void update(final GMSSRandom gmssRandom, byte[] array) {
        PrintStream printStream;
        String x;
        if (this.isFinished) {
            printStream = System.err;
            x = "No more update possible for treehash instance!";
        }
        else {
            if (this.isInitialized) {
                final byte[] array2 = new byte[this.messDigestTree.getDigestSize()];
                gmssRandom.nextSeed(this.seedActive);
                if (this.firstNode == null) {
                    this.firstNode = array;
                    this.firstNodeHeight = 0;
                }
                else {
                    int n;
                    for (n = 0; this.tailLength > 0 && n == this.heightOfNodes.lastElement(); ++n, --this.tailLength) {
                        final byte[] array3 = new byte[this.messDigestTree.getDigestSize() << 1];
                        System.arraycopy(this.tailStack.lastElement(), 0, array3, 0, this.messDigestTree.getDigestSize());
                        final Vector tailStack = this.tailStack;
                        tailStack.removeElementAt(tailStack.size() - 1);
                        final Vector heightOfNodes = this.heightOfNodes;
                        heightOfNodes.removeElementAt(heightOfNodes.size() - 1);
                        System.arraycopy(array, 0, array3, this.messDigestTree.getDigestSize(), this.messDigestTree.getDigestSize());
                        this.messDigestTree.update(array3, 0, array3.length);
                        array = new byte[this.messDigestTree.getDigestSize()];
                        this.messDigestTree.doFinal(array, 0);
                    }
                    this.tailStack.addElement(array);
                    this.heightOfNodes.addElement(Integers.valueOf(n));
                    ++this.tailLength;
                    if (this.heightOfNodes.lastElement() == this.firstNodeHeight) {
                        final byte[] array4 = new byte[this.messDigestTree.getDigestSize() << 1];
                        System.arraycopy(this.firstNode, 0, array4, 0, this.messDigestTree.getDigestSize());
                        System.arraycopy(this.tailStack.lastElement(), 0, array4, this.messDigestTree.getDigestSize(), this.messDigestTree.getDigestSize());
                        final Vector tailStack2 = this.tailStack;
                        tailStack2.removeElementAt(tailStack2.size() - 1);
                        final Vector heightOfNodes2 = this.heightOfNodes;
                        heightOfNodes2.removeElementAt(heightOfNodes2.size() - 1);
                        this.messDigestTree.update(array4, 0, array4.length);
                        this.firstNode = new byte[this.messDigestTree.getDigestSize()];
                        this.messDigestTree.doFinal(this.firstNode, 0);
                        ++this.firstNodeHeight;
                        this.tailLength = 0;
                    }
                }
                if (this.firstNodeHeight == this.maxHeight) {
                    this.isFinished = true;
                }
                return;
            }
            printStream = System.err;
            x = "Treehash instance not initialized before update";
        }
        printStream.println(x);
    }
    
    public void updateNextSeed(final GMSSRandom gmssRandom) {
        gmssRandom.nextSeed(this.seedNext);
    }
    
    public boolean wasFinished() {
        return this.isFinished;
    }
    
    public boolean wasInitialized() {
        return this.isInitialized;
    }
}
