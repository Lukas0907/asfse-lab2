// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.pqc.crypto.gmss;

import java.security.SecureRandom;
import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.pqc.crypto.gmss.util.WinternitzOTSVerify;
import org.bouncycastle.pqc.crypto.gmss.util.WinternitzOTSignature;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import java.lang.reflect.Array;
import java.util.Vector;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.pqc.crypto.gmss.util.GMSSRandom;
import org.bouncycastle.crypto.AsymmetricCipherKeyPairGenerator;

public class GMSSKeyPairGenerator implements AsymmetricCipherKeyPairGenerator
{
    public static final String OID = "1.3.6.1.4.1.8301.3.1.3.3";
    private int[] K;
    private byte[][] currentRootSigs;
    private byte[][] currentSeeds;
    private GMSSDigestProvider digestProvider;
    private GMSSParameters gmssPS;
    private GMSSKeyGenerationParameters gmssParams;
    private GMSSRandom gmssRandom;
    private int[] heightOfTrees;
    private boolean initialized;
    private int mdLength;
    private Digest messDigestTree;
    private byte[][] nextNextSeeds;
    private int numLayer;
    private int[] otsIndex;
    
    public GMSSKeyPairGenerator(final GMSSDigestProvider digestProvider) {
        this.initialized = false;
        this.digestProvider = digestProvider;
        this.messDigestTree = digestProvider.get();
        this.mdLength = this.messDigestTree.getDigestSize();
        this.gmssRandom = new GMSSRandom(this.messDigestTree);
    }
    
    private AsymmetricCipherKeyPair genKeyPair() {
        if (!this.initialized) {
            this.initializeDefault();
        }
        final int numLayer = this.numLayer;
        final byte[][][] array = new byte[numLayer][][];
        final byte[][][] array2 = new byte[numLayer - 1][][];
        final Treehash[][] array3 = new Treehash[numLayer][];
        final Treehash[][] array4 = new Treehash[numLayer - 1][];
        final Vector[] array5 = new Vector[numLayer];
        final Vector[] array6 = new Vector[numLayer - 1];
        final Vector[][] array7 = new Vector[numLayer][];
        final Vector[][] array8 = new Vector[numLayer - 1][];
        int n = 0;
        int numLayer2;
        while (true) {
            numLayer2 = this.numLayer;
            if (n >= numLayer2) {
                break;
            }
            array[n] = (byte[][])Array.newInstance(Byte.TYPE, this.heightOfTrees[n], this.mdLength);
            final int[] heightOfTrees = this.heightOfTrees;
            array3[n] = new Treehash[heightOfTrees[n] - this.K[n]];
            if (n > 0) {
                final int n2 = n - 1;
                array2[n2] = (byte[][])Array.newInstance(Byte.TYPE, heightOfTrees[n], this.mdLength);
                array4[n2] = new Treehash[this.heightOfTrees[n] - this.K[n]];
            }
            array5[n] = new Vector();
            if (n > 0) {
                array6[n - 1] = new Vector();
            }
            ++n;
        }
        final byte[][] array9 = (byte[][])Array.newInstance(Byte.TYPE, numLayer2, this.mdLength);
        final byte[][] array10 = (byte[][])Array.newInstance(Byte.TYPE, this.numLayer - 1, this.mdLength);
        final byte[][] array11 = (byte[][])Array.newInstance(Byte.TYPE, this.numLayer, this.mdLength);
        int n3 = 0;
        int numLayer3;
        while (true) {
            numLayer3 = this.numLayer;
            if (n3 >= numLayer3) {
                break;
            }
            System.arraycopy(this.currentSeeds[n3], 0, array11[n3], 0, this.mdLength);
            ++n3;
        }
        this.currentRootSigs = (byte[][])Array.newInstance(Byte.TYPE, numLayer3 - 1, this.mdLength);
        for (int i = this.numLayer - 1; i >= 0; --i) {
            GMSSRootCalc gmssRootCalc;
            if (i == this.numLayer - 1) {
                gmssRootCalc = this.generateCurrentAuthpathAndRoot(null, array5[i], array11[i], i);
            }
            else {
                gmssRootCalc = this.generateCurrentAuthpathAndRoot(array9[i + 1], array5[i], array11[i], i);
            }
            for (int j = 0; j < this.heightOfTrees[i]; ++j) {
                System.arraycopy(gmssRootCalc.getAuthPath()[j], 0, array[i][j], 0, this.mdLength);
            }
            array7[i] = gmssRootCalc.getRetain();
            array3[i] = gmssRootCalc.getTreehash();
            System.arraycopy(gmssRootCalc.getRoot(), 0, array9[i], 0, this.mdLength);
        }
        int k = this.numLayer - 2;
        final Vector[][] array12 = array7;
        while (k >= 0) {
            final Vector vector = array6[k];
            final int n4 = k + 1;
            final GMSSRootCalc generateNextAuthpathAndRoot = this.generateNextAuthpathAndRoot(vector, array11[n4], n4);
            for (int l = 0; l < this.heightOfTrees[n4]; ++l) {
                System.arraycopy(generateNextAuthpathAndRoot.getAuthPath()[l], 0, array2[k][l], 0, this.mdLength);
            }
            array8[k] = generateNextAuthpathAndRoot.getRetain();
            array4[k] = generateNextAuthpathAndRoot.getTreehash();
            System.arraycopy(generateNextAuthpathAndRoot.getRoot(), 0, array10[k], 0, this.mdLength);
            System.arraycopy(array11[n4], 0, this.nextNextSeeds[k], 0, this.mdLength);
            --k;
        }
        return new AsymmetricCipherKeyPair(new GMSSPublicKeyParameters(array9[0], this.gmssPS), new GMSSPrivateKeyParameters(this.currentSeeds, this.nextNextSeeds, array, array2, array3, array4, array5, array6, array12, array8, array10, this.currentRootSigs, this.gmssPS, this.digestProvider));
    }
    
    private GMSSRootCalc generateCurrentAuthpathAndRoot(byte[] array, final Vector vector, final byte[] array2, final int n) {
        final int mdLength = this.mdLength;
        final byte[] array3 = new byte[mdLength];
        final byte[] array4 = new byte[mdLength];
        final byte[] nextSeed = this.gmssRandom.nextSeed(array2);
        final GMSSRootCalc gmssRootCalc = new GMSSRootCalc(this.heightOfTrees[n], this.K[n], this.digestProvider);
        gmssRootCalc.initialize(vector);
        if (n == this.numLayer - 1) {
            array = new WinternitzOTSignature(nextSeed, this.digestProvider.get(), this.otsIndex[n]).getPublicKey();
        }
        else {
            this.currentRootSigs[n] = new WinternitzOTSignature(nextSeed, this.digestProvider.get(), this.otsIndex[n]).getSignature(array);
            array = new WinternitzOTSVerify(this.digestProvider.get(), this.otsIndex[n]).Verify(array, this.currentRootSigs[n]);
        }
        gmssRootCalc.update(array);
        int n2 = 0;
        int n3 = 3;
        int n4 = 1;
        while (true) {
            final int[] heightOfTrees = this.heightOfTrees;
            if (n4 >= 1 << heightOfTrees[n]) {
                break;
            }
            int n5 = n2;
            int n6;
            if (n4 == (n6 = n3)) {
                n5 = n2;
                n6 = n3;
                if (n2 < heightOfTrees[n] - this.K[n]) {
                    gmssRootCalc.initializeTreehashSeed(array2, n2);
                    n6 = n3 * 2;
                    n5 = n2 + 1;
                }
            }
            gmssRootCalc.update(new WinternitzOTSignature(this.gmssRandom.nextSeed(array2), this.digestProvider.get(), this.otsIndex[n]).getPublicKey());
            ++n4;
            n2 = n5;
            n3 = n6;
        }
        if (gmssRootCalc.wasFinished()) {
            return gmssRootCalc;
        }
        System.err.println("Baum noch nicht fertig konstruiert!!!");
        return null;
    }
    
    private GMSSRootCalc generateNextAuthpathAndRoot(final Vector vector, final byte[] array, final int n) {
        final byte[] array2 = new byte[this.numLayer];
        final GMSSRootCalc gmssRootCalc = new GMSSRootCalc(this.heightOfTrees[n], this.K[n], this.digestProvider);
        gmssRootCalc.initialize(vector);
        int n2 = 0;
        int n3 = 3;
        int n4 = 0;
        while (true) {
            final int[] heightOfTrees = this.heightOfTrees;
            if (n2 >= 1 << heightOfTrees[n]) {
                break;
            }
            int n5 = n3;
            int n6 = n4;
            if (n2 == n3) {
                n5 = n3;
                if ((n6 = n4) < heightOfTrees[n] - this.K[n]) {
                    gmssRootCalc.initializeTreehashSeed(array, n4);
                    n5 = n3 * 2;
                    n6 = n4 + 1;
                }
            }
            gmssRootCalc.update(new WinternitzOTSignature(this.gmssRandom.nextSeed(array), this.digestProvider.get(), this.otsIndex[n]).getPublicKey());
            ++n2;
            n3 = n5;
            n4 = n6;
        }
        if (gmssRootCalc.wasFinished()) {
            return gmssRootCalc;
        }
        System.err.println("N\ufffdchster Baum noch nicht fertig konstruiert!!!");
        return null;
    }
    
    private void initializeDefault() {
        final int[] array2;
        final int[] array = array2 = new int[4];
        array2[1] = (array2[0] = 10);
        array2[3] = (array2[2] = 10);
        this.initialize(new GMSSKeyGenerationParameters(CryptoServicesRegistrar.getSecureRandom(), new GMSSParameters(array.length, array, new int[] { 3, 3, 3, 3 }, new int[] { 2, 2, 2, 2 })));
    }
    
    @Override
    public AsymmetricCipherKeyPair generateKeyPair() {
        return this.genKeyPair();
    }
    
    @Override
    public void init(final KeyGenerationParameters keyGenerationParameters) {
        this.initialize(keyGenerationParameters);
    }
    
    public void initialize(final int n, final SecureRandom secureRandom) {
        GMSSKeyGenerationParameters gmssKeyGenerationParameters;
        if (n <= 10) {
            final int[] array = { 10 };
            gmssKeyGenerationParameters = new GMSSKeyGenerationParameters(secureRandom, new GMSSParameters(array.length, array, new int[] { 3 }, new int[] { 2 }));
        }
        else if (n <= 20) {
            final int[] array3;
            final int[] array2 = array3 = new int[2];
            array3[1] = (array3[0] = 10);
            gmssKeyGenerationParameters = new GMSSKeyGenerationParameters(secureRandom, new GMSSParameters(array2.length, array2, new int[] { 5, 4 }, new int[] { 2, 2 }));
        }
        else {
            final int[] array5;
            final int[] array4 = array5 = new int[4];
            array5[1] = (array5[0] = 10);
            array5[3] = (array5[2] = 10);
            gmssKeyGenerationParameters = new GMSSKeyGenerationParameters(secureRandom, new GMSSParameters(array4.length, array4, new int[] { 9, 9, 9, 3 }, new int[] { 2, 2, 2, 2 }));
        }
        this.initialize(gmssKeyGenerationParameters);
    }
    
    public void initialize(final KeyGenerationParameters keyGenerationParameters) {
        this.gmssParams = (GMSSKeyGenerationParameters)keyGenerationParameters;
        this.gmssPS = new GMSSParameters(this.gmssParams.getParameters().getNumOfLayers(), this.gmssParams.getParameters().getHeightOfTrees(), this.gmssParams.getParameters().getWinternitzParameter(), this.gmssParams.getParameters().getK());
        this.numLayer = this.gmssPS.getNumOfLayers();
        this.heightOfTrees = this.gmssPS.getHeightOfTrees();
        this.otsIndex = this.gmssPS.getWinternitzParameter();
        this.K = this.gmssPS.getK();
        this.currentSeeds = (byte[][])Array.newInstance(Byte.TYPE, this.numLayer, this.mdLength);
        this.nextNextSeeds = (byte[][])Array.newInstance(Byte.TYPE, this.numLayer - 1, this.mdLength);
        final SecureRandom secureRandom = CryptoServicesRegistrar.getSecureRandom();
        for (int i = 0; i < this.numLayer; ++i) {
            secureRandom.nextBytes(this.currentSeeds[i]);
            this.gmssRandom.nextSeed(this.currentSeeds[i]);
        }
        this.initialized = true;
    }
}
