// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;

public class DERSet extends ASN1Set
{
    private int bodyLength;
    
    public DERSet() {
        this.bodyLength = -1;
    }
    
    public DERSet(final ASN1Encodable asn1Encodable) {
        super(asn1Encodable);
        this.bodyLength = -1;
    }
    
    public DERSet(final ASN1EncodableVector asn1EncodableVector) {
        super(asn1EncodableVector, true);
        this.bodyLength = -1;
    }
    
    DERSet(final boolean b, final ASN1Encodable[] array) {
        super(checkSorted(b), array);
        this.bodyLength = -1;
    }
    
    public DERSet(final ASN1Encodable[] array) {
        super(array, true);
        this.bodyLength = -1;
    }
    
    private static boolean checkSorted(final boolean b) {
        if (b) {
            return b;
        }
        throw new IllegalStateException("DERSet elements should always be in sorted order");
    }
    
    public static DERSet convert(final ASN1Set set) {
        return (DERSet)set.toDERObject();
    }
    
    private int getBodyLength() throws IOException {
        if (this.bodyLength < 0) {
            final int length = this.elements.length;
            int i = 0;
            int bodyLength = 0;
            while (i < length) {
                bodyLength += this.elements[i].toASN1Primitive().toDERObject().encodedLength();
                ++i;
            }
            this.bodyLength = bodyLength;
        }
        return this.bodyLength;
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream, final boolean b) throws IOException {
        if (b) {
            asn1OutputStream.write(49);
        }
        final DEROutputStream derSubStream = asn1OutputStream.getDERSubStream();
        final int length = this.elements.length;
        final int bodyLength = this.bodyLength;
        int i = 0;
        final int n = 0;
        if (bodyLength < 0 && length <= 16) {
            final ASN1Primitive[] array = new ASN1Primitive[length];
            int bodyLength2;
            for (int j = bodyLength2 = 0; j < length; ++j) {
                final ASN1Primitive derObject = this.elements[j].toASN1Primitive().toDERObject();
                array[j] = derObject;
                bodyLength2 += derObject.encodedLength();
            }
            asn1OutputStream.writeLength(this.bodyLength = bodyLength2);
            for (int k = n; k < length; ++k) {
                array[k].encode(derSubStream, true);
            }
        }
        else {
            asn1OutputStream.writeLength(this.getBodyLength());
            while (i < length) {
                this.elements[i].toASN1Primitive().toDERObject().encode(derSubStream, true);
                ++i;
            }
        }
    }
    
    @Override
    int encodedLength() throws IOException {
        final int bodyLength = this.getBodyLength();
        return StreamUtil.calculateBodyLength(bodyLength) + 1 + bodyLength;
    }
    
    @Override
    ASN1Primitive toDERObject() {
        if (this.isSorted) {
            return this;
        }
        return super.toDERObject();
    }
    
    @Override
    ASN1Primitive toDLObject() {
        return this;
    }
}
