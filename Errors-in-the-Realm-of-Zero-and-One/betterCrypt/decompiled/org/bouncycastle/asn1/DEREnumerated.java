// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.math.BigInteger;

public class DEREnumerated extends ASN1Enumerated
{
    public DEREnumerated(final int n) {
        super(n);
    }
    
    public DEREnumerated(final BigInteger bigInteger) {
        super(bigInteger);
    }
    
    DEREnumerated(final byte[] array) {
        super(array);
    }
}
