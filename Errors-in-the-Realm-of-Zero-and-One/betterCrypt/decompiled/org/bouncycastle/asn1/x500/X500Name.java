// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x500;

import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1TaggedObject;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Choice;
import org.bouncycastle.asn1.ASN1Object;

public class X500Name extends ASN1Object implements ASN1Choice
{
    private static X500NameStyle defaultStyle;
    private int hashCodeValue;
    private boolean isHashCodeCalculated;
    private DERSequence rdnSeq;
    private RDN[] rdns;
    private X500NameStyle style;
    
    static {
        X500Name.defaultStyle = BCStyle.INSTANCE;
    }
    
    public X500Name(final String s) {
        this(X500Name.defaultStyle, s);
    }
    
    private X500Name(final ASN1Sequence asn1Sequence) {
        this(X500Name.defaultStyle, asn1Sequence);
    }
    
    public X500Name(final X500NameStyle style, final String s) {
        this(style.fromString(s));
        this.style = style;
    }
    
    private X500Name(final X500NameStyle style, final ASN1Sequence asn1Sequence) {
        this.style = style;
        this.rdns = new RDN[asn1Sequence.size()];
        final Enumeration objects = asn1Sequence.getObjects();
        int n = 0;
        boolean b = true;
        while (objects.hasMoreElements()) {
            final RDN nextElement = objects.nextElement();
            final RDN instance = RDN.getInstance(nextElement);
            b &= (instance == nextElement);
            this.rdns[n] = instance;
            ++n;
        }
        DERSequence convert;
        if (b) {
            convert = DERSequence.convert(asn1Sequence);
        }
        else {
            convert = new DERSequence(this.rdns);
        }
        this.rdnSeq = convert;
    }
    
    public X500Name(final X500NameStyle style, final X500Name x500Name) {
        this.style = style;
        this.rdns = x500Name.rdns;
        this.rdnSeq = x500Name.rdnSeq;
    }
    
    public X500Name(final X500NameStyle style, final RDN[] array) {
        this.style = style;
        this.rdns = array.clone();
        this.rdnSeq = new DERSequence(this.rdns);
    }
    
    public X500Name(final RDN[] array) {
        this(X500Name.defaultStyle, array);
    }
    
    public static X500NameStyle getDefaultStyle() {
        return X500Name.defaultStyle;
    }
    
    public static X500Name getInstance(final Object o) {
        if (o instanceof X500Name) {
            return (X500Name)o;
        }
        if (o != null) {
            return new X500Name(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public static X500Name getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        return getInstance(ASN1Sequence.getInstance(asn1TaggedObject, true));
    }
    
    public static X500Name getInstance(final X500NameStyle x500NameStyle, final Object o) {
        if (o instanceof X500Name) {
            return new X500Name(x500NameStyle, (X500Name)o);
        }
        if (o != null) {
            return new X500Name(x500NameStyle, ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public static void setDefaultStyle(final X500NameStyle defaultStyle) {
        if (defaultStyle != null) {
            X500Name.defaultStyle = defaultStyle;
            return;
        }
        throw new NullPointerException("cannot set style to null");
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof X500Name) && !(o instanceof ASN1Sequence)) {
            return false;
        }
        if (this.toASN1Primitive().equals(((ASN1Encodable)o).toASN1Primitive())) {
            return true;
        }
        try {
            return this.style.areEqual(this, new X500Name(ASN1Sequence.getInstance(((ASN1Encodable)o).toASN1Primitive())));
        }
        catch (Exception ex) {
            return false;
        }
    }
    
    public ASN1ObjectIdentifier[] getAttributeTypes() {
        final int length = this.rdns.length;
        final int n = 0;
        int n2;
        for (int i = n2 = 0; i < length; ++i) {
            n2 += this.rdns[i].size();
        }
        final ASN1ObjectIdentifier[] array = new ASN1ObjectIdentifier[n2];
        int n3 = 0;
        for (int j = n; j < length; ++j) {
            n3 += this.rdns[j].collectAttributeTypes(array, n3);
        }
        return array;
    }
    
    public RDN[] getRDNs() {
        return this.rdns.clone();
    }
    
    public RDN[] getRDNs(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        final RDN[] array = new RDN[this.rdns.length];
        int n2;
        int n = n2 = 0;
        while (true) {
            final RDN[] rdns = this.rdns;
            if (n == rdns.length) {
                break;
            }
            final RDN rdn = rdns[n];
            int n3 = n2;
            if (rdn.containsAttributeType(asn1ObjectIdentifier)) {
                array[n2] = rdn;
                n3 = n2 + 1;
            }
            ++n;
            n2 = n3;
        }
        if (n2 < array.length) {
            final RDN[] array2 = new RDN[n2];
            System.arraycopy(array, 0, array2, 0, array2.length);
            return array2;
        }
        return array;
    }
    
    @Override
    public int hashCode() {
        if (this.isHashCodeCalculated) {
            return this.hashCodeValue;
        }
        this.isHashCodeCalculated = true;
        return this.hashCodeValue = this.style.calculateHashCode(this);
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        return this.rdnSeq;
    }
    
    @Override
    public String toString() {
        return this.style.toString(this);
    }
}
