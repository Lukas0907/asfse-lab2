// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x500.style;

public class X500NameTokenizer
{
    private StringBuffer buf;
    private int index;
    private char separator;
    private String value;
    
    public X500NameTokenizer(final String s) {
        this(s, ',');
    }
    
    public X500NameTokenizer(final String value, final char separator) {
        this.buf = new StringBuffer();
        this.value = value;
        this.index = -1;
        this.separator = separator;
    }
    
    public boolean hasMoreTokens() {
        return this.index != this.value.length();
    }
    
    public String nextToken() {
        if (this.index == this.value.length()) {
            return null;
        }
        int i = this.index + 1;
        this.buf.setLength(0);
        int n2;
        int n = n2 = 0;
        while (i != this.value.length()) {
            final char char1 = this.value.charAt(i);
            int n4 = 0;
            Label_0159: {
                int n3;
                if (char1 == '\"') {
                    n3 = n2;
                    if (n == 0) {
                        n3 = (n2 ^ 0x1);
                    }
                }
                else {
                    n3 = n2;
                    if (n == 0) {
                        if (n2 != 0) {
                            n3 = n2;
                        }
                        else {
                            if (char1 == '\\') {
                                this.buf.append(char1);
                                n4 = 1;
                                break Label_0159;
                            }
                            if (char1 == this.separator) {
                                break;
                            }
                            this.buf.append(char1);
                            n4 = n;
                            break Label_0159;
                        }
                    }
                }
                this.buf.append(char1);
                final int n5 = 0;
                n2 = n3;
                n4 = n5;
            }
            ++i;
            n = n4;
        }
        this.index = i;
        return this.buf.toString();
    }
}
