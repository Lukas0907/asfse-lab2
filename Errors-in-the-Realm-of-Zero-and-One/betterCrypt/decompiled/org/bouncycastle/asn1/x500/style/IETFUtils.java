// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x500.style;

import org.bouncycastle.asn1.DERUniversalString;
import java.util.Vector;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.X500NameStyle;
import java.util.Enumeration;
import java.io.IOException;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.util.Strings;
import org.bouncycastle.asn1.ASN1String;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.x500.AttributeTypeAndValue;
import java.util.Hashtable;
import org.bouncycastle.asn1.x500.RDN;

public class IETFUtils
{
    public static void appendRDN(final StringBuffer sb, final RDN rdn, final Hashtable hashtable) {
        if (rdn.isMultiValued()) {
            final AttributeTypeAndValue[] typesAndValues = rdn.getTypesAndValues();
            int n = 1;
            for (int i = 0; i != typesAndValues.length; ++i) {
                if (n != 0) {
                    n = 0;
                }
                else {
                    sb.append('+');
                }
                appendTypeAndValue(sb, typesAndValues[i], hashtable);
            }
        }
        else if (rdn.getFirst() != null) {
            appendTypeAndValue(sb, rdn.getFirst(), hashtable);
        }
    }
    
    public static void appendTypeAndValue(final StringBuffer sb, final AttributeTypeAndValue attributeTypeAndValue, final Hashtable hashtable) {
        String id = hashtable.get(attributeTypeAndValue.getType());
        if (id == null) {
            id = attributeTypeAndValue.getType().getId();
        }
        sb.append(id);
        sb.append('=');
        sb.append(valueToString(attributeTypeAndValue.getValue()));
    }
    
    private static boolean atvAreEqual(final AttributeTypeAndValue attributeTypeAndValue, final AttributeTypeAndValue attributeTypeAndValue2) {
        return attributeTypeAndValue == attributeTypeAndValue2 || (attributeTypeAndValue != null && attributeTypeAndValue2 != null && attributeTypeAndValue.getType().equals(attributeTypeAndValue2.getType()) && canonicalString(attributeTypeAndValue.getValue()).equals(canonicalString(attributeTypeAndValue2.getValue())));
    }
    
    public static String canonicalString(final ASN1Encodable asn1Encodable) {
        return canonicalize(valueToString(asn1Encodable));
    }
    
    public static String canonicalize(String substring) {
        final int length = substring.length();
        int n = 0;
        String string = substring;
        if (length > 0) {
            string = substring;
            if (substring.charAt(0) == '#') {
                final ASN1Primitive decodeObject = decodeObject(substring);
                string = substring;
                if (decodeObject instanceof ASN1String) {
                    string = ((ASN1String)decodeObject).getString();
                }
            }
        }
        final String lowerCase = Strings.toLowerCase(string);
        final int length2 = lowerCase.length();
        if (length2 < 2) {
            return lowerCase;
        }
        int n2;
        for (n2 = length2 - 1; n < n2 && lowerCase.charAt(n) == '\\' && lowerCase.charAt(n + 1) == ' '; n += 2) {}
        int index;
        for (index = n2; index > n + 1 && lowerCase.charAt(index - 1) == '\\' && lowerCase.charAt(index) == ' '; index -= 2) {}
        if (n <= 0) {
            substring = lowerCase;
            if (index >= n2) {
                return stripInternalSpaces(substring);
            }
        }
        substring = lowerCase.substring(n, index + 1);
        return stripInternalSpaces(substring);
    }
    
    private static int convertHex(final char c) {
        if ('0' <= c && c <= '9') {
            return c - '0';
        }
        int n;
        if ('a' <= c && c <= 'f') {
            n = c - 'a';
        }
        else {
            n = c - 'A';
        }
        return n + 10;
    }
    
    public static ASN1ObjectIdentifier decodeAttrName(final String str, final Hashtable hashtable) {
        if (Strings.toUpperCase(str).startsWith("OID.")) {
            return new ASN1ObjectIdentifier(str.substring(4));
        }
        if (str.charAt(0) >= '0' && str.charAt(0) <= '9') {
            return new ASN1ObjectIdentifier(str);
        }
        final ASN1ObjectIdentifier asn1ObjectIdentifier = hashtable.get(Strings.toLowerCase(str));
        if (asn1ObjectIdentifier != null) {
            return asn1ObjectIdentifier;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown object id - ");
        sb.append(str);
        sb.append(" - passed to distinguished name");
        throw new IllegalArgumentException(sb.toString());
    }
    
    private static ASN1Primitive decodeObject(final String s) {
        try {
            return ASN1Primitive.fromByteArray(Hex.decodeStrict(s, 1, s.length() - 1));
        }
        catch (IOException obj) {
            final StringBuilder sb = new StringBuilder();
            sb.append("unknown encoding in name: ");
            sb.append(obj);
            throw new IllegalStateException(sb.toString());
        }
    }
    
    public static String[] findAttrNamesForOID(final ASN1ObjectIdentifier asn1ObjectIdentifier, final Hashtable hashtable) {
        final Enumeration<Object> elements = hashtable.elements();
        final int n = 0;
        int n2 = 0;
        while (elements.hasMoreElements()) {
            if (asn1ObjectIdentifier.equals(elements.nextElement())) {
                ++n2;
            }
        }
        final String[] array = new String[n2];
        final Enumeration<K> keys = hashtable.keys();
        int n3 = n;
        while (keys.hasMoreElements()) {
            final String key = (String)keys.nextElement();
            if (asn1ObjectIdentifier.equals(hashtable.get(key))) {
                array[n3] = key;
                ++n3;
            }
        }
        return array;
    }
    
    private static boolean isHexDigit(final char c) {
        return ('0' <= c && c <= '9') || ('a' <= c && c <= 'f') || ('A' <= c && c <= 'F');
    }
    
    public static boolean rDNAreEqual(final RDN rdn, final RDN rdn2) {
        if (rdn.size() != rdn2.size()) {
            return false;
        }
        final AttributeTypeAndValue[] typesAndValues = rdn.getTypesAndValues();
        final AttributeTypeAndValue[] typesAndValues2 = rdn2.getTypesAndValues();
        if (typesAndValues.length != typesAndValues2.length) {
            return false;
        }
        for (int i = 0; i != typesAndValues.length; ++i) {
            if (!atvAreEqual(typesAndValues[i], typesAndValues2[i])) {
                return false;
            }
        }
        return true;
    }
    
    public static RDN[] rDNsFromString(String s, final X500NameStyle x500NameStyle) {
        final X500NameTokenizer x500NameTokenizer = new X500NameTokenizer(s);
        final X500NameBuilder x500NameBuilder = new X500NameBuilder(x500NameStyle);
        while (x500NameTokenizer.hasMoreTokens()) {
            s = x500NameTokenizer.nextToken();
            if (s.indexOf(43) > 0) {
                final X500NameTokenizer x500NameTokenizer2 = new X500NameTokenizer(s, '+');
                final X500NameTokenizer x500NameTokenizer3 = new X500NameTokenizer(x500NameTokenizer2.nextToken(), '=');
                final String nextToken = x500NameTokenizer3.nextToken();
                if (!x500NameTokenizer3.hasMoreTokens()) {
                    throw new IllegalArgumentException("badly formatted directory string");
                }
                s = x500NameTokenizer3.nextToken();
                ASN1ObjectIdentifier obj = x500NameStyle.attrNameToOID(nextToken.trim());
                if (x500NameTokenizer2.hasMoreTokens()) {
                    final Vector<ASN1ObjectIdentifier> vector = new Vector<ASN1ObjectIdentifier>();
                    final Vector<String> vector2 = new Vector<String>();
                    while (true) {
                        vector.addElement(obj);
                        vector2.addElement(unescape(s));
                        if (!x500NameTokenizer2.hasMoreTokens()) {
                            x500NameBuilder.addMultiValuedRDN(toOIDArray(vector), toValueArray(vector2));
                            break;
                        }
                        final X500NameTokenizer x500NameTokenizer4 = new X500NameTokenizer(x500NameTokenizer2.nextToken(), '=');
                        final String nextToken2 = x500NameTokenizer4.nextToken();
                        if (!x500NameTokenizer4.hasMoreTokens()) {
                            throw new IllegalArgumentException("badly formatted directory string");
                        }
                        s = x500NameTokenizer4.nextToken();
                        obj = x500NameStyle.attrNameToOID(nextToken2.trim());
                    }
                }
                else {
                    x500NameBuilder.addRDN(obj, unescape(s));
                }
            }
            else {
                final X500NameTokenizer x500NameTokenizer5 = new X500NameTokenizer(s, '=');
                s = x500NameTokenizer5.nextToken();
                if (!x500NameTokenizer5.hasMoreTokens()) {
                    throw new IllegalArgumentException("badly formatted directory string");
                }
                x500NameBuilder.addRDN(x500NameStyle.attrNameToOID(s.trim()), unescape(x500NameTokenizer5.nextToken()));
            }
        }
        return x500NameBuilder.build().getRDNs();
    }
    
    public static String stripInternalSpaces(final String s) {
        if (s.indexOf("  ") < 0) {
            return s;
        }
        final StringBuffer sb = new StringBuffer();
        final char char1 = s.charAt(0);
        sb.append(char1);
        int i = 1;
        char c = char1;
        while (i < s.length()) {
            final char char2 = s.charAt(i);
            if (c != ' ' || char2 != ' ') {
                sb.append(char2);
                c = char2;
            }
            ++i;
        }
        return sb.toString();
    }
    
    private static ASN1ObjectIdentifier[] toOIDArray(final Vector vector) {
        final ASN1ObjectIdentifier[] array = new ASN1ObjectIdentifier[vector.size()];
        for (int i = 0; i != array.length; ++i) {
            array[i] = vector.elementAt(i);
        }
        return array;
    }
    
    private static String[] toValueArray(final Vector vector) {
        final String[] array = new String[vector.size()];
        for (int i = 0; i != array.length; ++i) {
            array[i] = vector.elementAt(i);
        }
        return array;
    }
    
    private static String unescape(final String s) {
        if (s.length() != 0 && (s.indexOf(92) >= 0 || s.indexOf(34) >= 0)) {
            final char[] charArray = s.toCharArray();
            final StringBuffer sb = new StringBuffer(s.length());
            int n;
            if (charArray[0] == '\\' && charArray[1] == '#') {
                n = 2;
                sb.append("\\#");
            }
            else {
                n = 0;
            }
            final int n2 = 0;
            final int n4;
            final int n3 = n4 = n2;
            int n6;
            int n5 = n6 = n4;
            int n7 = n4;
            int length = n3;
            int n8 = n2;
            int c;
            int n9;
        Label_0151_Outer:
            for (int i = n; i != charArray.length; ++i, n8 = n9, n5 = c) {
                c = charArray[i];
                if (c != 32) {
                    n6 = 1;
                }
                while (true) {
                    Label_0270: {
                        if (c == 34) {
                            if (n8 != 0) {
                                break Label_0270;
                            }
                            n7 ^= 0x1;
                        }
                        else {
                            if (c == 92 && n8 == 0 && n7 == 0) {
                                length = sb.length();
                                n9 = 1;
                                c = n5;
                                continue Label_0151_Outer;
                            }
                            if (c == 32 && n8 == 0 && n6 == 0) {
                                n9 = n8;
                                c = n5;
                                continue Label_0151_Outer;
                            }
                            if (n8 == 0 || !isHexDigit((char)c)) {
                                break Label_0270;
                            }
                            if (n5 != 0) {
                                sb.append((char)(convertHex((char)n5) * 16 + convertHex((char)c)));
                                c = (n9 = 0);
                                continue Label_0151_Outer;
                            }
                            n9 = n8;
                            continue Label_0151_Outer;
                        }
                        n9 = 0;
                        c = n5;
                        continue Label_0151_Outer;
                    }
                    sb.append((char)c);
                    continue;
                }
            }
            if (sb.length() > 0) {
                while (sb.charAt(sb.length() - 1) == ' ' && length != sb.length() - 1) {
                    sb.setLength(sb.length() - 1);
                }
            }
            return sb.toString();
        }
        return s.trim();
    }
    
    public static ASN1Encodable valueFromHexString(final String s, final int n) throws IOException {
        final byte[] array = new byte[(s.length() - n) / 2];
        for (int i = 0; i != array.length; ++i) {
            final int index = i * 2 + n;
            array[i] = (byte)(convertHex(s.charAt(index + 1)) | convertHex(s.charAt(index)) << 4);
        }
        return ASN1Primitive.fromByteArray(array);
    }
    
    public static String valueToString(final ASN1Encodable asn1Encodable) {
        final StringBuffer sb = new StringBuffer();
        final boolean b = asn1Encodable instanceof ASN1String;
        final int n = 0;
        Label_0103: {
            if (b && !(asn1Encodable instanceof DERUniversalString)) {
                final String string = ((ASN1String)asn1Encodable).getString();
                if (string.length() > 0 && string.charAt(0) == '#') {
                    sb.append('\\');
                }
                sb.append(string);
                break Label_0103;
            }
            try {
                sb.append('#');
                sb.append(Hex.toHexString(asn1Encodable.toASN1Primitive().getEncoded("DER")));
                int length = sb.length();
                final int length2 = sb.length();
                int i = 2;
                if (length2 < 2 || sb.charAt(0) != '\\' || sb.charAt(1) != '#') {
                    i = 0;
                }
                while (i != length) {
                    final char char1 = sb.charAt(i);
                    if (char1 != '\"' && char1 != '\\' && char1 != '+' && char1 != ',') {
                        switch (char1) {
                            default: {
                                ++i;
                                continue;
                            }
                            case 59:
                            case 60:
                            case 61:
                            case 62: {
                                break;
                            }
                        }
                    }
                    sb.insert(i, "\\");
                    i += 2;
                    ++length;
                }
                if (sb.length() > 0) {
                    for (int n2 = n; sb.length() > n2 && sb.charAt(n2) == ' '; n2 += 2) {
                        sb.insert(n2, "\\");
                    }
                }
                for (int n3 = sb.length() - 1; n3 >= 0 && sb.charAt(n3) == ' '; --n3) {
                    sb.insert(n3, '\\');
                }
                return sb.toString();
            }
            catch (IOException ex) {
                throw new IllegalArgumentException("Other value has no encoded form");
            }
        }
    }
}
