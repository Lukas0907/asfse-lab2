// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x500.style;

import java.io.IOException;
import org.bouncycastle.asn1.ASN1ParsingException;
import org.bouncycastle.asn1.DERUTF8String;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x500.AttributeTypeAndValue;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.RDN;
import java.util.Enumeration;
import java.util.Hashtable;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x500.X500NameStyle;

public abstract class AbstractX500NameStyle implements X500NameStyle
{
    private int calcHashCode(final ASN1Encodable asn1Encodable) {
        return IETFUtils.canonicalString(asn1Encodable).hashCode();
    }
    
    public static Hashtable copyHashTable(final Hashtable hashtable) {
        final Hashtable<Object, Object> hashtable2 = new Hashtable<Object, Object>();
        final Enumeration<Object> keys = hashtable.keys();
        while (keys.hasMoreElements()) {
            final Object nextElement = keys.nextElement();
            hashtable2.put(nextElement, hashtable.get(nextElement));
        }
        return hashtable2;
    }
    
    private boolean foundMatch(final boolean b, final RDN rdn, final RDN[] array) {
        if (b) {
            for (int i = array.length - 1; i >= 0; --i) {
                if (array[i] != null && this.rdnAreEqual(rdn, array[i])) {
                    array[i] = null;
                    return true;
                }
            }
        }
        else {
            for (int j = 0; j != array.length; ++j) {
                if (array[j] != null && this.rdnAreEqual(rdn, array[j])) {
                    array[j] = null;
                    return true;
                }
            }
        }
        return false;
    }
    
    @Override
    public boolean areEqual(final X500Name x500Name, final X500Name x500Name2) {
        final RDN[] rdNs = x500Name.getRDNs();
        final RDN[] rdNs2 = x500Name2.getRDNs();
        if (rdNs.length != rdNs2.length) {
            return false;
        }
        final boolean b = rdNs[0].getFirst() != null && rdNs2[0].getFirst() != null && (rdNs[0].getFirst().getType().equals(rdNs2[0].getFirst().getType()) ^ true);
        for (int i = 0; i != rdNs.length; ++i) {
            if (!this.foundMatch(b, rdNs[i], rdNs2)) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public int calculateHashCode(final X500Name x500Name) {
        final RDN[] rdNs = x500Name.getRDNs();
        int n;
        for (int i = n = 0; i != rdNs.length; ++i) {
            if (rdNs[i].isMultiValued()) {
                final AttributeTypeAndValue[] typesAndValues = rdNs[i].getTypesAndValues();
                for (int j = 0; j != typesAndValues.length; ++j) {
                    n = (n ^ typesAndValues[j].getType().hashCode() ^ this.calcHashCode(typesAndValues[j].getValue()));
                }
            }
            else {
                n = (n ^ rdNs[i].getFirst().getType().hashCode() ^ this.calcHashCode(rdNs[i].getFirst().getValue()));
            }
        }
        return n;
    }
    
    protected ASN1Encodable encodeStringValue(final ASN1ObjectIdentifier asn1ObjectIdentifier, final String s) {
        return new DERUTF8String(s);
    }
    
    protected boolean rdnAreEqual(final RDN rdn, final RDN rdn2) {
        return IETFUtils.rDNAreEqual(rdn, rdn2);
    }
    
    @Override
    public ASN1Encodable stringToValue(final ASN1ObjectIdentifier asn1ObjectIdentifier, final String s) {
        Label_0061: {
            if (s.length() == 0 || s.charAt(0) != '#') {
                break Label_0061;
            }
            while (true) {
                while (true) {
                    try {
                        return IETFUtils.valueFromHexString(s, 1);
                        Label_0088: {
                            final String substring;
                            return this.encodeStringValue(asn1ObjectIdentifier, substring);
                        }
                        String substring = s;
                        // iftrue(Label_0088:, s.length() == 0)
                        substring = s;
                        // iftrue(Label_0088:, s.charAt(0) != '\\')
                        substring = s.substring(1);
                        return this.encodeStringValue(asn1ObjectIdentifier, substring);
                        final StringBuilder sb = new StringBuilder();
                        sb.append("can't recode value for oid ");
                        sb.append(asn1ObjectIdentifier.getId());
                        throw new ASN1ParsingException(sb.toString());
                    }
                    catch (IOException ex) {}
                    continue;
                }
            }
        }
    }
}
