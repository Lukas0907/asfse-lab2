// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;

public class DERBitString extends ASN1BitString
{
    protected DERBitString(final byte b, final int n) {
        super(b, n);
    }
    
    public DERBitString(final int n) {
        super(ASN1BitString.getBytes(n), ASN1BitString.getPadBits(n));
    }
    
    public DERBitString(final ASN1Encodable asn1Encodable) throws IOException {
        super(asn1Encodable.toASN1Primitive().getEncoded("DER"), 0);
    }
    
    public DERBitString(final byte[] array) {
        this(array, 0);
    }
    
    public DERBitString(final byte[] array, final int n) {
        super(array, n);
    }
    
    static DERBitString fromOctetString(final byte[] array) {
        if (array.length >= 1) {
            final byte b = array[0];
            final byte[] array2 = new byte[array.length - 1];
            if (array2.length != 0) {
                System.arraycopy(array, 1, array2, 0, array.length - 1);
            }
            return new DERBitString(array2, b);
        }
        throw new IllegalArgumentException("truncated BIT STRING detected");
    }
    
    public static DERBitString getInstance(final Object o) {
        if (o == null || o instanceof DERBitString) {
            return (DERBitString)o;
        }
        if (o instanceof DLBitString) {
            final DLBitString dlBitString = (DLBitString)o;
            return new DERBitString(dlBitString.data, dlBitString.padBits);
        }
        if (o instanceof byte[]) {
            try {
                return (DERBitString)ASN1Primitive.fromByteArray((byte[])o);
            }
            catch (Exception ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("encoding error in getInstance: ");
                sb.append(ex.toString());
                throw new IllegalArgumentException(sb.toString());
            }
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("illegal object in getInstance: ");
        sb2.append(o.getClass().getName());
        throw new IllegalArgumentException(sb2.toString());
    }
    
    public static DERBitString getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        final ASN1Primitive object = asn1TaggedObject.getObject();
        if (!b && !(object instanceof DERBitString)) {
            return fromOctetString(ASN1OctetString.getInstance(object).getOctets());
        }
        return getInstance(object);
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream, final boolean b) throws IOException {
        final int length = this.data.length;
        if (length != 0 && this.padBits != 0) {
            final byte[] data = this.data;
            final int n = length - 1;
            if (data[n] != (byte)(this.data[n] & 255 << this.padBits)) {
                asn1OutputStream.writeEncoded(b, 3, (byte)this.padBits, this.data, 0, n, (byte)(this.data[n] & 255 << this.padBits));
                return;
            }
        }
        asn1OutputStream.writeEncoded(b, 3, (byte)this.padBits, this.data);
    }
    
    @Override
    int encodedLength() {
        return StreamUtil.calculateBodyLength(this.data.length + 1) + 1 + this.data.length + 1;
    }
    
    @Override
    boolean isConstructed() {
        return false;
    }
    
    @Override
    ASN1Primitive toDERObject() {
        return this;
    }
    
    @Override
    ASN1Primitive toDLObject() {
        return this;
    }
}
