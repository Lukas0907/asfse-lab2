// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

public class ASN1EncodableVector
{
    private static final int DEFAULT_CAPACITY = 10;
    static final ASN1Encodable[] EMPTY_ELEMENTS;
    private boolean copyOnWrite;
    private int elementCount;
    private ASN1Encodable[] elements;
    
    static {
        EMPTY_ELEMENTS = new ASN1Encodable[0];
    }
    
    public ASN1EncodableVector() {
        this(10);
    }
    
    public ASN1EncodableVector(final int n) {
        if (n >= 0) {
            ASN1Encodable[] empty_ELEMENTS;
            if (n == 0) {
                empty_ELEMENTS = ASN1EncodableVector.EMPTY_ELEMENTS;
            }
            else {
                empty_ELEMENTS = new ASN1Encodable[n];
            }
            this.elements = empty_ELEMENTS;
            this.elementCount = 0;
            this.copyOnWrite = false;
            return;
        }
        throw new IllegalArgumentException("'initialCapacity' must not be negative");
    }
    
    static ASN1Encodable[] cloneElements(final ASN1Encodable[] array) {
        if (array.length < 1) {
            return ASN1EncodableVector.EMPTY_ELEMENTS;
        }
        return array.clone();
    }
    
    private void reallocate(final int n) {
        final ASN1Encodable[] elements = new ASN1Encodable[Math.max(this.elements.length, n + (n >> 1))];
        System.arraycopy(this.elements, 0, elements, 0, this.elementCount);
        this.elements = elements;
        this.copyOnWrite = false;
    }
    
    public void add(final ASN1Encodable asn1Encodable) {
        if (asn1Encodable != null) {
            final int length = this.elements.length;
            final int elementCount = this.elementCount;
            boolean b = true;
            final int elementCount2 = elementCount + 1;
            if (elementCount2 <= length) {
                b = false;
            }
            if (this.copyOnWrite | b) {
                this.reallocate(elementCount2);
            }
            this.elements[this.elementCount] = asn1Encodable;
            this.elementCount = elementCount2;
            return;
        }
        throw new NullPointerException("'element' cannot be null");
    }
    
    public void addAll(final ASN1EncodableVector asn1EncodableVector) {
        if (asn1EncodableVector == null) {
            throw new NullPointerException("'other' cannot be null");
        }
        final int size = asn1EncodableVector.size();
        boolean b = true;
        if (size < 1) {
            return;
        }
        final int length = this.elements.length;
        final int elementCount = this.elementCount + size;
        final int n = 0;
        if (elementCount <= length) {
            b = false;
        }
        int n2 = n;
        if (b | this.copyOnWrite) {
            this.reallocate(elementCount);
            n2 = n;
        }
        do {
            final ASN1Encodable value = asn1EncodableVector.get(n2);
            if (value == null) {
                throw new NullPointerException("'other' elements cannot be null");
            }
            this.elements[this.elementCount + n2] = value;
        } while (++n2 < size);
        this.elementCount = elementCount;
    }
    
    ASN1Encodable[] copyElements() {
        final int elementCount = this.elementCount;
        if (elementCount == 0) {
            return ASN1EncodableVector.EMPTY_ELEMENTS;
        }
        final ASN1Encodable[] array = new ASN1Encodable[elementCount];
        System.arraycopy(this.elements, 0, array, 0, elementCount);
        return array;
    }
    
    public ASN1Encodable get(final int i) {
        if (i < this.elementCount) {
            return this.elements[i];
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(i);
        sb.append(" >= ");
        sb.append(this.elementCount);
        throw new ArrayIndexOutOfBoundsException(sb.toString());
    }
    
    public int size() {
        return this.elementCount;
    }
    
    ASN1Encodable[] takeElements() {
        final int elementCount = this.elementCount;
        if (elementCount == 0) {
            return ASN1EncodableVector.EMPTY_ELEMENTS;
        }
        final ASN1Encodable[] elements = this.elements;
        if (elements.length == elementCount) {
            this.copyOnWrite = true;
            return elements;
        }
        final ASN1Encodable[] array = new ASN1Encodable[elementCount];
        System.arraycopy(elements, 0, array, 0, elementCount);
        return array;
    }
}
