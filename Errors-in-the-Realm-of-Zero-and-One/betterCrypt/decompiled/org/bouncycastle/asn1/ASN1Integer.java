// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;
import org.bouncycastle.util.Properties;
import org.bouncycastle.util.Arrays;
import java.math.BigInteger;

public class ASN1Integer extends ASN1Primitive
{
    static final int SIGN_EXT_SIGNED = -1;
    static final int SIGN_EXT_UNSIGNED = 255;
    private final byte[] bytes;
    private final int start;
    
    public ASN1Integer(final long val) {
        this.bytes = BigInteger.valueOf(val).toByteArray();
        this.start = 0;
    }
    
    public ASN1Integer(final BigInteger bigInteger) {
        this.bytes = bigInteger.toByteArray();
        this.start = 0;
    }
    
    public ASN1Integer(final byte[] array) {
        this(array, true);
    }
    
    ASN1Integer(final byte[] array, final boolean b) {
        if (!isMalformed(array)) {
            byte[] clone;
            if (b) {
                clone = Arrays.clone(array);
            }
            else {
                clone = array;
            }
            this.bytes = clone;
            this.start = signBytesToSkip(array);
            return;
        }
        throw new IllegalArgumentException("malformed integer");
    }
    
    public static ASN1Integer getInstance(final Object o) {
        if (o != null && !(o instanceof ASN1Integer)) {
            if (o instanceof byte[]) {
                try {
                    return (ASN1Integer)ASN1Primitive.fromByteArray((byte[])o);
                }
                catch (Exception ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("encoding error in getInstance: ");
                    sb.append(ex.toString());
                    throw new IllegalArgumentException(sb.toString());
                }
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("illegal object in getInstance: ");
            sb2.append(o.getClass().getName());
            throw new IllegalArgumentException(sb2.toString());
        }
        return (ASN1Integer)o;
    }
    
    public static ASN1Integer getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        final ASN1Primitive object = asn1TaggedObject.getObject();
        if (!b && !(object instanceof ASN1Integer)) {
            return new ASN1Integer(ASN1OctetString.getInstance(object).getOctets());
        }
        return getInstance(object);
    }
    
    static int intValue(final byte[] array, int a, int n) {
        final int length = array.length;
        final int max = Math.max(a, length - 4);
        a = (n & array[max]);
        n = max;
        while (true) {
            ++n;
            if (n >= length) {
                break;
            }
            a = (a << 8 | (array[n] & 0xFF));
        }
        return a;
    }
    
    static boolean isMalformed(final byte[] array) {
        final int length = array.length;
        return length == 0 || (length != 1 && (array[0] == array[1] >> 7 && !Properties.isOverrideSet("org.bouncycastle.asn1.allow_unsafe_integer")));
    }
    
    static long longValue(final byte[] array, int max, final int n) {
        final int length = array.length;
        max = Math.max(max, length - 8);
        long n2 = n & array[max];
        while (true) {
            ++max;
            if (max >= length) {
                break;
            }
            n2 = (n2 << 8 | (long)(array[max] & 0xFF));
        }
        return n2;
    }
    
    static int signBytesToSkip(final byte[] array) {
        int length;
        int i;
        int n;
        for (length = array.length, i = 0; i < length - 1; i = n) {
            final byte b = array[i];
            n = i + 1;
            if (b != array[n] >> 7) {
                break;
            }
        }
        return i;
    }
    
    @Override
    boolean asn1Equals(final ASN1Primitive asn1Primitive) {
        return asn1Primitive instanceof ASN1Integer && Arrays.areEqual(this.bytes, ((ASN1Integer)asn1Primitive).bytes);
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream, final boolean b) throws IOException {
        asn1OutputStream.writeEncoded(b, 2, this.bytes);
    }
    
    @Override
    int encodedLength() {
        return StreamUtil.calculateBodyLength(this.bytes.length) + 1 + this.bytes.length;
    }
    
    public BigInteger getPositiveValue() {
        return new BigInteger(1, this.bytes);
    }
    
    public BigInteger getValue() {
        return new BigInteger(this.bytes);
    }
    
    public boolean hasValue(final BigInteger x) {
        return x != null && intValue(this.bytes, this.start, -1) == x.intValue() && this.getValue().equals(x);
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.bytes);
    }
    
    public int intPositiveValueExact() {
        final byte[] bytes = this.bytes;
        final int length = bytes.length;
        final int start = this.start;
        final int n = length - start;
        if (n <= 4 && (n != 4 || (bytes[start] & 0x80) == 0x0)) {
            return intValue(this.bytes, this.start, 255);
        }
        throw new ArithmeticException("ASN.1 Integer out of positive int range");
    }
    
    public int intValueExact() {
        final byte[] bytes = this.bytes;
        final int length = bytes.length;
        final int start = this.start;
        if (length - start <= 4) {
            return intValue(bytes, start, -1);
        }
        throw new ArithmeticException("ASN.1 Integer out of int range");
    }
    
    @Override
    boolean isConstructed() {
        return false;
    }
    
    public long longValueExact() {
        final byte[] bytes = this.bytes;
        final int length = bytes.length;
        final int start = this.start;
        if (length - start <= 8) {
            return longValue(bytes, start, -1);
        }
        throw new ArithmeticException("ASN.1 Integer out of long range");
    }
    
    @Override
    public String toString() {
        return this.getValue().toString();
    }
}
