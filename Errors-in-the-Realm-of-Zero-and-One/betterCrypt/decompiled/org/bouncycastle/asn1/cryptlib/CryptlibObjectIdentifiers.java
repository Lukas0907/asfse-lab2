// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.cryptlib;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;

public class CryptlibObjectIdentifiers
{
    public static final ASN1ObjectIdentifier cryptlib;
    public static final ASN1ObjectIdentifier curvey25519;
    public static final ASN1ObjectIdentifier ecc;
    
    static {
        cryptlib = new ASN1ObjectIdentifier("1.3.6.1.4.1.3029");
        ecc = CryptlibObjectIdentifiers.cryptlib.branch("1").branch("5");
        curvey25519 = CryptlibObjectIdentifiers.ecc.branch("1");
    }
}
