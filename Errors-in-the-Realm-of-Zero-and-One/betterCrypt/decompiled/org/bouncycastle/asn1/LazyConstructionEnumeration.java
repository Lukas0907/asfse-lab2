// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;
import java.util.Enumeration;

class LazyConstructionEnumeration implements Enumeration
{
    private ASN1InputStream aIn;
    private Object nextObj;
    
    public LazyConstructionEnumeration(final byte[] array) {
        this.aIn = new ASN1InputStream(array, true);
        this.nextObj = this.readObject();
    }
    
    private Object readObject() {
        try {
            return this.aIn.readObject();
        }
        catch (IOException obj) {
            final StringBuilder sb = new StringBuilder();
            sb.append("malformed DER construction: ");
            sb.append(obj);
            throw new ASN1ParsingException(sb.toString(), obj);
        }
    }
    
    @Override
    public boolean hasMoreElements() {
        return this.nextObj != null;
    }
    
    @Override
    public Object nextElement() {
        final Object nextObj = this.nextObj;
        this.nextObj = this.readObject();
        return nextObj;
    }
}
