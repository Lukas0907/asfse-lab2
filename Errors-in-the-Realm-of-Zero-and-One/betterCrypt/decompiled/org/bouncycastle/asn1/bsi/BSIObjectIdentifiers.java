// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.bsi;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;

public interface BSIObjectIdentifiers
{
    public static final ASN1ObjectIdentifier algorithm = BSIObjectIdentifiers.bsi_de.branch("1");
    public static final ASN1ObjectIdentifier bsi_de = new ASN1ObjectIdentifier("0.4.0.127.0.7");
    public static final ASN1ObjectIdentifier ecdsa_plain_RIPEMD160 = BSIObjectIdentifiers.ecdsa_plain_signatures.branch("6");
    public static final ASN1ObjectIdentifier ecdsa_plain_SHA1 = BSIObjectIdentifiers.ecdsa_plain_signatures.branch("1");
    public static final ASN1ObjectIdentifier ecdsa_plain_SHA224 = BSIObjectIdentifiers.ecdsa_plain_signatures.branch("2");
    public static final ASN1ObjectIdentifier ecdsa_plain_SHA256 = BSIObjectIdentifiers.ecdsa_plain_signatures.branch("3");
    public static final ASN1ObjectIdentifier ecdsa_plain_SHA384 = BSIObjectIdentifiers.ecdsa_plain_signatures.branch("4");
    public static final ASN1ObjectIdentifier ecdsa_plain_SHA512 = BSIObjectIdentifiers.ecdsa_plain_signatures.branch("5");
    public static final ASN1ObjectIdentifier ecdsa_plain_signatures = BSIObjectIdentifiers.id_ecc.branch("4.1");
    public static final ASN1ObjectIdentifier ecka_eg = BSIObjectIdentifiers.id_ecc.branch("5.1");
    public static final ASN1ObjectIdentifier ecka_eg_SessionKDF = BSIObjectIdentifiers.ecka_eg.branch("2");
    public static final ASN1ObjectIdentifier ecka_eg_SessionKDF_3DES = BSIObjectIdentifiers.ecka_eg_SessionKDF.branch("1");
    public static final ASN1ObjectIdentifier ecka_eg_SessionKDF_AES128 = BSIObjectIdentifiers.ecka_eg_SessionKDF.branch("2");
    public static final ASN1ObjectIdentifier ecka_eg_SessionKDF_AES192 = BSIObjectIdentifiers.ecka_eg_SessionKDF.branch("3");
    public static final ASN1ObjectIdentifier ecka_eg_SessionKDF_AES256 = BSIObjectIdentifiers.ecka_eg_SessionKDF.branch("4");
    public static final ASN1ObjectIdentifier ecka_eg_X963kdf = BSIObjectIdentifiers.ecka_eg.branch("1");
    public static final ASN1ObjectIdentifier ecka_eg_X963kdf_RIPEMD160 = BSIObjectIdentifiers.ecka_eg_X963kdf.branch("6");
    public static final ASN1ObjectIdentifier ecka_eg_X963kdf_SHA1 = BSIObjectIdentifiers.ecka_eg_X963kdf.branch("1");
    public static final ASN1ObjectIdentifier ecka_eg_X963kdf_SHA224 = BSIObjectIdentifiers.ecka_eg_X963kdf.branch("2");
    public static final ASN1ObjectIdentifier ecka_eg_X963kdf_SHA256 = BSIObjectIdentifiers.ecka_eg_X963kdf.branch("3");
    public static final ASN1ObjectIdentifier ecka_eg_X963kdf_SHA384 = BSIObjectIdentifiers.ecka_eg_X963kdf.branch("4");
    public static final ASN1ObjectIdentifier ecka_eg_X963kdf_SHA512 = BSIObjectIdentifiers.ecka_eg_X963kdf.branch("5");
    public static final ASN1ObjectIdentifier id_ecc = BSIObjectIdentifiers.bsi_de.branch("1.1");
}
