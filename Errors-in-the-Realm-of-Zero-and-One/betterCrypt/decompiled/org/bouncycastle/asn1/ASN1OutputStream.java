// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.util.Enumeration;
import java.io.IOException;
import java.io.OutputStream;

public class ASN1OutputStream
{
    private OutputStream os;
    
    public ASN1OutputStream(final OutputStream os) {
        this.os = os;
    }
    
    public static ASN1OutputStream create(final OutputStream outputStream) {
        return new ASN1OutputStream(outputStream);
    }
    
    public static ASN1OutputStream create(final OutputStream outputStream, final String s) {
        if (s.equals("DER")) {
            return new DEROutputStream(outputStream);
        }
        if (s.equals("DL")) {
            return new DLOutputStream(outputStream);
        }
        return new ASN1OutputStream(outputStream);
    }
    
    public void close() throws IOException {
        this.os.close();
    }
    
    public void flush() throws IOException {
        this.os.flush();
    }
    
    void flushInternal() throws IOException {
    }
    
    DEROutputStream getDERSubStream() {
        return new DEROutputStream(this.os);
    }
    
    ASN1OutputStream getDLSubStream() {
        return new DLOutputStream(this.os);
    }
    
    final void write(final int n) throws IOException {
        this.os.write(n);
    }
    
    final void write(final byte[] b, final int off, final int len) throws IOException {
        this.os.write(b, off, len);
    }
    
    final void writeElements(final Enumeration enumeration) throws IOException {
        while (enumeration.hasMoreElements()) {
            this.writePrimitive(enumeration.nextElement().toASN1Primitive(), true);
        }
    }
    
    final void writeElements(final ASN1Encodable[] array) throws IOException {
        for (int length = array.length, i = 0; i < length; ++i) {
            this.writePrimitive(array[i].toASN1Primitive(), true);
        }
    }
    
    final void writeEncoded(final boolean b, final int n, final byte b2) throws IOException {
        if (b) {
            this.write(n);
        }
        this.writeLength(1);
        this.write(b2);
    }
    
    final void writeEncoded(final boolean b, final int n, final byte b2, final byte[] array) throws IOException {
        if (b) {
            this.write(n);
        }
        this.writeLength(array.length + 1);
        this.write(b2);
        this.write(array, 0, array.length);
    }
    
    final void writeEncoded(final boolean b, final int n, final byte b2, final byte[] array, final int n2, final int n3, final byte b3) throws IOException {
        if (b) {
            this.write(n);
        }
        this.writeLength(n3 + 2);
        this.write(b2);
        this.write(array, n2, n3);
        this.write(b3);
    }
    
    final void writeEncoded(final boolean b, final int n, final int n2, final byte[] array) throws IOException {
        this.writeTag(b, n, n2);
        this.writeLength(array.length);
        this.write(array, 0, array.length);
    }
    
    final void writeEncoded(final boolean b, final int n, final byte[] array) throws IOException {
        if (b) {
            this.write(n);
        }
        this.writeLength(array.length);
        this.write(array, 0, array.length);
    }
    
    final void writeEncoded(final boolean b, final int n, final byte[] array, final int n2, final int n3) throws IOException {
        if (b) {
            this.write(n);
        }
        this.writeLength(n3);
        this.write(array, n2, n3);
    }
    
    final void writeEncodedIndef(final boolean b, final int n, final int n2, final byte[] array) throws IOException {
        this.writeTag(b, n, n2);
        this.write(128);
        this.write(array, 0, array.length);
        this.write(0);
        this.write(0);
    }
    
    final void writeEncodedIndef(final boolean b, final int n, final Enumeration enumeration) throws IOException {
        if (b) {
            this.write(n);
        }
        this.write(128);
        this.writeElements(enumeration);
        this.write(0);
        this.write(0);
    }
    
    final void writeEncodedIndef(final boolean b, final int n, final ASN1Encodable[] array) throws IOException {
        if (b) {
            this.write(n);
        }
        this.write(128);
        this.writeElements(array);
        this.write(0);
        this.write(0);
    }
    
    final void writeLength(final int n) throws IOException {
        if (n > 127) {
            int n2 = n;
            int n3 = 1;
            while (true) {
                n2 >>>= 8;
                if (n2 == 0) {
                    break;
                }
                ++n3;
            }
            this.write((byte)(n3 | 0x80));
            for (int i = (n3 - 1) * 8; i >= 0; i -= 8) {
                this.write((byte)(n >> i));
            }
        }
        else {
            this.write((byte)n);
        }
    }
    
    protected void writeNull() throws IOException {
        this.write(5);
        this.write(0);
    }
    
    public void writeObject(final ASN1Encodable asn1Encodable) throws IOException {
        if (asn1Encodable != null) {
            this.writePrimitive(asn1Encodable.toASN1Primitive(), true);
            this.flushInternal();
            return;
        }
        throw new IOException("null object detected");
    }
    
    public void writeObject(final ASN1Primitive asn1Primitive) throws IOException {
        if (asn1Primitive != null) {
            this.writePrimitive(asn1Primitive, true);
            this.flushInternal();
            return;
        }
        throw new IOException("null object detected");
    }
    
    void writePrimitive(final ASN1Primitive asn1Primitive, final boolean b) throws IOException {
        asn1Primitive.encode(this, b);
    }
    
    final void writeTag(final boolean b, int n, int n2) throws IOException {
        if (!b) {
            return;
        }
        if (n2 < 31) {
            this.write(n | n2);
            return;
        }
        this.write(0x1F | n);
        if (n2 < 128) {
            this.write(n2);
            return;
        }
        final byte[] array = new byte[5];
        n = array.length - 1;
        array[n] = (byte)(n2 & 0x7F);
        int n3;
        int n4;
        do {
            n3 = n2 >> 7;
            n4 = n - 1;
            array[n4] = (byte)((n3 & 0x7F) | 0x80);
            n = n4;
        } while ((n2 = n3) > 127);
        this.write(array, n4, array.length - n4);
    }
}
