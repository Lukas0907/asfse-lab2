// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;

public abstract class ASN1Null extends ASN1Primitive
{
    ASN1Null() {
    }
    
    public static ASN1Null getInstance(final Object o) {
        if (o instanceof ASN1Null) {
            return (ASN1Null)o;
        }
        if (o == null) {
            goto Label_0108;
        }
        while (true) {
            while (true) {
                try {
                    return getInstance(ASN1Primitive.fromByteArray((byte[])o));
                    final StringBuilder sb = new StringBuilder();
                    sb.append("unknown object in getInstance(): ");
                    sb.append(o.getClass().getName());
                    throw new IllegalArgumentException(sb.toString());
                }
                catch (IOException ex) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("failed to construct NULL from byte[]: ");
                    sb2.append(ex.getMessage());
                    throw new IllegalArgumentException(sb2.toString());
                }
                catch (ClassCastException ex2) {}
                continue;
            }
        }
    }
    
    @Override
    boolean asn1Equals(final ASN1Primitive asn1Primitive) {
        return asn1Primitive instanceof ASN1Null;
    }
    
    @Override
    abstract void encode(final ASN1OutputStream p0, final boolean p1) throws IOException;
    
    @Override
    public int hashCode() {
        return -1;
    }
    
    @Override
    public String toString() {
        return "NULL";
    }
}
