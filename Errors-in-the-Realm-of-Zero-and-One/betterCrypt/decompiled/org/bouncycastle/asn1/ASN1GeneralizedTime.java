// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;
import org.bouncycastle.util.Arrays;
import java.util.Locale;
import java.util.TimeZone;
import java.util.SimpleTimeZone;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;
import org.bouncycastle.util.Strings;

public class ASN1GeneralizedTime extends ASN1Primitive
{
    protected byte[] time;
    
    public ASN1GeneralizedTime(final String s) {
        this.time = Strings.toByteArray(s);
        try {
            this.getDate();
        }
        catch (ParseException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("invalid date string: ");
            sb.append(ex.getMessage());
            throw new IllegalArgumentException(sb.toString());
        }
    }
    
    public ASN1GeneralizedTime(final Date date) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss'Z'", DateUtil.EN_Locale);
        simpleDateFormat.setTimeZone(new SimpleTimeZone(0, "Z"));
        this.time = Strings.toByteArray(simpleDateFormat.format(date));
    }
    
    public ASN1GeneralizedTime(final Date date, final Locale locale) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss'Z'", locale);
        simpleDateFormat.setTimeZone(new SimpleTimeZone(0, "Z"));
        this.time = Strings.toByteArray(simpleDateFormat.format(date));
    }
    
    ASN1GeneralizedTime(final byte[] time) {
        if (time.length < 4) {
            throw new IllegalArgumentException("GeneralizedTime string too short");
        }
        this.time = time;
        if (this.isDigit(0) && this.isDigit(1) && this.isDigit(2) && this.isDigit(3)) {
            return;
        }
        throw new IllegalArgumentException("illegal characters in GeneralizedTime string");
    }
    
    private SimpleDateFormat calculateGMTDateFormat() {
        SimpleDateFormat simpleDateFormat;
        if (this.hasFractionalSeconds()) {
            simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss.SSSz");
        }
        else if (this.hasSeconds()) {
            simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssz");
        }
        else if (this.hasMinutes()) {
            simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmz");
        }
        else {
            simpleDateFormat = new SimpleDateFormat("yyyyMMddHHz");
        }
        simpleDateFormat.setTimeZone(new SimpleTimeZone(0, "Z"));
        return simpleDateFormat;
    }
    
    private String calculateGMTOffset(final String s) {
        final TimeZone default1 = TimeZone.getDefault();
        int rawOffset = default1.getRawOffset();
        String s2;
        if (rawOffset < 0) {
            rawOffset = -rawOffset;
            s2 = "-";
        }
        else {
            s2 = "+";
        }
        final int n = rawOffset / 3600000;
        final int n2 = (rawOffset - n * 60 * 60 * 1000) / 60000;
        int n3 = n;
        try {
            if (default1.useDaylightTime()) {
                String pruneFractionalSeconds = s;
                if (this.hasFractionalSeconds()) {
                    pruneFractionalSeconds = this.pruneFractionalSeconds(s);
                }
                final SimpleDateFormat calculateGMTDateFormat = this.calculateGMTDateFormat();
                final StringBuilder sb = new StringBuilder();
                sb.append(pruneFractionalSeconds);
                sb.append("GMT");
                sb.append(s2);
                sb.append(this.convert(n));
                sb.append(":");
                sb.append(this.convert(n2));
                n3 = n;
                if (default1.inDaylightTime(calculateGMTDateFormat.parse(sb.toString()))) {
                    int n4;
                    if (s2.equals("+")) {
                        n4 = 1;
                    }
                    else {
                        n4 = -1;
                    }
                    n3 = n + n4;
                }
            }
        }
        catch (ParseException ex) {
            ex.printStackTrace();
            n3 = n;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("GMT");
        sb2.append(s2);
        sb2.append(this.convert(n3));
        sb2.append(":");
        sb2.append(this.convert(n2));
        return sb2.toString();
    }
    
    private String convert(final int n) {
        if (n < 10) {
            final StringBuilder sb = new StringBuilder();
            sb.append("0");
            sb.append(n);
            return sb.toString();
        }
        return Integer.toString(n);
    }
    
    public static ASN1GeneralizedTime getInstance(final Object o) {
        if (o != null && !(o instanceof ASN1GeneralizedTime)) {
            if (o instanceof byte[]) {
                try {
                    return (ASN1GeneralizedTime)ASN1Primitive.fromByteArray((byte[])o);
                }
                catch (Exception ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("encoding error in getInstance: ");
                    sb.append(ex.toString());
                    throw new IllegalArgumentException(sb.toString());
                }
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("illegal object in getInstance: ");
            sb2.append(o.getClass().getName());
            throw new IllegalArgumentException(sb2.toString());
        }
        return (ASN1GeneralizedTime)o;
    }
    
    public static ASN1GeneralizedTime getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        final ASN1Primitive object = asn1TaggedObject.getObject();
        if (!b && !(object instanceof ASN1GeneralizedTime)) {
            return new ASN1GeneralizedTime(ASN1OctetString.getInstance(object).getOctets());
        }
        return getInstance(object);
    }
    
    private boolean isDigit(final int n) {
        final byte[] time = this.time;
        return time.length > n && time[n] >= 48 && time[n] <= 57;
    }
    
    private String pruneFractionalSeconds(final String s) {
        String substring;
        int i;
        char char1;
        for (substring = s.substring(14), i = 1; i < substring.length(); ++i) {
            char1 = substring.charAt(i);
            if ('0' > char1) {
                break;
            }
            if (char1 > '9') {
                break;
            }
        }
        final int n = i - 1;
        String str;
        StringBuilder sb2;
        if (n > 3) {
            final StringBuilder sb = new StringBuilder();
            sb.append(substring.substring(0, 4));
            sb.append(substring.substring(i));
            str = sb.toString();
            sb2 = new StringBuilder();
        }
        else if (n == 1) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(substring.substring(0, i));
            sb3.append("00");
            sb3.append(substring.substring(i));
            str = sb3.toString();
            sb2 = new StringBuilder();
        }
        else {
            if (n != 2) {
                return s;
            }
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(substring.substring(0, i));
            sb4.append("0");
            sb4.append(substring.substring(i));
            str = sb4.toString();
            sb2 = new StringBuilder();
        }
        sb2.append(s.substring(0, 14));
        sb2.append(str);
        return sb2.toString();
    }
    
    @Override
    boolean asn1Equals(final ASN1Primitive asn1Primitive) {
        return asn1Primitive instanceof ASN1GeneralizedTime && Arrays.areEqual(this.time, ((ASN1GeneralizedTime)asn1Primitive).time);
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream, final boolean b) throws IOException {
        asn1OutputStream.writeEncoded(b, 24, this.time);
    }
    
    @Override
    int encodedLength() {
        final int length = this.time.length;
        return StreamUtil.calculateBodyLength(length) + 1 + length;
    }
    
    public Date getDate() throws ParseException {
        String s = Strings.fromByteArray(this.time);
        SimpleDateFormat calculateGMTDateFormat;
        if (s.endsWith("Z")) {
            if (this.hasFractionalSeconds()) {
                calculateGMTDateFormat = new SimpleDateFormat("yyyyMMddHHmmss.SSS'Z'");
            }
            else if (this.hasSeconds()) {
                calculateGMTDateFormat = new SimpleDateFormat("yyyyMMddHHmmss'Z'");
            }
            else if (this.hasMinutes()) {
                calculateGMTDateFormat = new SimpleDateFormat("yyyyMMddHHmm'Z'");
            }
            else {
                calculateGMTDateFormat = new SimpleDateFormat("yyyyMMddHH'Z'");
            }
            calculateGMTDateFormat.setTimeZone(new SimpleTimeZone(0, "Z"));
        }
        else if (s.indexOf(45) <= 0 && s.indexOf(43) <= 0) {
            if (this.hasFractionalSeconds()) {
                calculateGMTDateFormat = new SimpleDateFormat("yyyyMMddHHmmss.SSS");
            }
            else if (this.hasSeconds()) {
                calculateGMTDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            }
            else if (this.hasMinutes()) {
                calculateGMTDateFormat = new SimpleDateFormat("yyyyMMddHHmm");
            }
            else {
                calculateGMTDateFormat = new SimpleDateFormat("yyyyMMddHH");
            }
            calculateGMTDateFormat.setTimeZone(new SimpleTimeZone(0, TimeZone.getDefault().getID()));
        }
        else {
            s = this.getTime();
            calculateGMTDateFormat = this.calculateGMTDateFormat();
        }
        String pruneFractionalSeconds = s;
        if (this.hasFractionalSeconds()) {
            pruneFractionalSeconds = this.pruneFractionalSeconds(s);
        }
        return DateUtil.epochAdjust(calculateGMTDateFormat.parse(pruneFractionalSeconds));
    }
    
    public String getTime() {
        final String fromByteArray = Strings.fromByteArray(this.time);
        if (fromByteArray.charAt(fromByteArray.length() - 1) == 'Z') {
            final StringBuilder sb = new StringBuilder();
            sb.append(fromByteArray.substring(0, fromByteArray.length() - 1));
            sb.append("GMT+00:00");
            return sb.toString();
        }
        final int index = fromByteArray.length() - 6;
        final char char1 = fromByteArray.charAt(index);
        if ((char1 == '-' || char1 == '+') && fromByteArray.indexOf("GMT") == index - 3) {
            return fromByteArray;
        }
        final int beginIndex = fromByteArray.length() - 5;
        final char char2 = fromByteArray.charAt(beginIndex);
        if (char2 == '-' || char2 == '+') {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(fromByteArray.substring(0, beginIndex));
            sb2.append("GMT");
            final int n = beginIndex + 3;
            sb2.append(fromByteArray.substring(beginIndex, n));
            sb2.append(":");
            sb2.append(fromByteArray.substring(n));
            return sb2.toString();
        }
        final int beginIndex2 = fromByteArray.length() - 3;
        final char char3 = fromByteArray.charAt(beginIndex2);
        if (char3 != '-' && char3 != '+') {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(fromByteArray);
            sb3.append(this.calculateGMTOffset(fromByteArray));
            return sb3.toString();
        }
        final StringBuilder sb4 = new StringBuilder();
        sb4.append(fromByteArray.substring(0, beginIndex2));
        sb4.append("GMT");
        sb4.append(fromByteArray.substring(beginIndex2));
        sb4.append(":00");
        return sb4.toString();
    }
    
    public String getTimeString() {
        return Strings.fromByteArray(this.time);
    }
    
    protected boolean hasFractionalSeconds() {
        int n = 0;
        while (true) {
            final byte[] time = this.time;
            if (n == time.length) {
                return false;
            }
            if (time[n] == 46 && n == 14) {
                return true;
            }
            ++n;
        }
    }
    
    protected boolean hasMinutes() {
        return this.isDigit(10) && this.isDigit(11);
    }
    
    protected boolean hasSeconds() {
        return this.isDigit(12) && this.isDigit(13);
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.time);
    }
    
    @Override
    boolean isConstructed() {
        return false;
    }
    
    @Override
    ASN1Primitive toDERObject() {
        return new DERGeneralizedTime(this.time);
    }
    
    @Override
    ASN1Primitive toDLObject() {
        return new DERGeneralizedTime(this.time);
    }
}
