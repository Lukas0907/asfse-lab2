// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

class DLFactory
{
    static final ASN1Sequence EMPTY_SEQUENCE;
    static final ASN1Set EMPTY_SET;
    
    static {
        EMPTY_SEQUENCE = new DLSequence();
        EMPTY_SET = new DLSet();
    }
    
    static ASN1Sequence createSequence(final ASN1EncodableVector asn1EncodableVector) {
        if (asn1EncodableVector.size() < 1) {
            return DLFactory.EMPTY_SEQUENCE;
        }
        return new DLSequence(asn1EncodableVector);
    }
    
    static ASN1Set createSet(final ASN1EncodableVector asn1EncodableVector) {
        if (asn1EncodableVector.size() < 1) {
            return DLFactory.EMPTY_SET;
        }
        return new DLSet(asn1EncodableVector);
    }
}
