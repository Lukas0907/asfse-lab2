// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Enumeration;
import java.io.IOException;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.util.Iterable;

public abstract class ASN1Set extends ASN1Primitive implements Iterable<ASN1Encodable>
{
    protected final ASN1Encodable[] elements;
    protected final boolean isSorted;
    
    protected ASN1Set() {
        this.elements = ASN1EncodableVector.EMPTY_ELEMENTS;
        this.isSorted = true;
    }
    
    protected ASN1Set(final ASN1Encodable asn1Encodable) {
        if (asn1Encodable != null) {
            this.elements = new ASN1Encodable[] { asn1Encodable };
            this.isSorted = true;
            return;
        }
        throw new NullPointerException("'element' cannot be null");
    }
    
    protected ASN1Set(final ASN1EncodableVector asn1EncodableVector, final boolean b) {
        if (asn1EncodableVector != null) {
            ASN1Encodable[] elements;
            if (b && asn1EncodableVector.size() >= 2) {
                elements = asn1EncodableVector.copyElements();
                sort(elements);
            }
            else {
                elements = asn1EncodableVector.takeElements();
            }
            this.elements = elements;
            this.isSorted = (b || elements.length < 2);
            return;
        }
        throw new NullPointerException("'elementVector' cannot be null");
    }
    
    ASN1Set(final boolean b, final ASN1Encodable[] elements) {
        this.elements = elements;
        this.isSorted = (b || elements.length < 2);
    }
    
    protected ASN1Set(ASN1Encodable[] cloneElements, final boolean b) {
        if (!Arrays.isNullOrContainsNull(cloneElements)) {
            cloneElements = ASN1EncodableVector.cloneElements(cloneElements);
            if (b && cloneElements.length >= 2) {
                sort(cloneElements);
            }
            this.elements = cloneElements;
            this.isSorted = (b || cloneElements.length < 2);
            return;
        }
        throw new NullPointerException("'elements' cannot be null, or contain null");
    }
    
    private static byte[] getDEREncoded(final ASN1Encodable asn1Encodable) {
        try {
            return asn1Encodable.toASN1Primitive().getEncoded("DER");
        }
        catch (IOException ex) {
            throw new IllegalArgumentException("cannot encode object added to SET");
        }
    }
    
    public static ASN1Set getInstance(final Object o) {
        if (o == null || o instanceof ASN1Set) {
            return (ASN1Set)o;
        }
        if (o instanceof ASN1SetParser) {
            return getInstance(((ASN1SetParser)o).toASN1Primitive());
        }
        if (o instanceof byte[]) {
            try {
                return getInstance(ASN1Primitive.fromByteArray((byte[])o));
            }
            catch (IOException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("failed to construct set from byte[]: ");
                sb.append(ex.getMessage());
                throw new IllegalArgumentException(sb.toString());
            }
        }
        if (o instanceof ASN1Encodable) {
            final ASN1Primitive asn1Primitive = ((ASN1Encodable)o).toASN1Primitive();
            if (asn1Primitive instanceof ASN1Set) {
                return (ASN1Set)asn1Primitive;
            }
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("unknown object in getInstance: ");
        sb2.append(o.getClass().getName());
        throw new IllegalArgumentException(sb2.toString());
    }
    
    public static ASN1Set getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        if (b) {
            if (asn1TaggedObject.isExplicit()) {
                return getInstance(asn1TaggedObject.getObject());
            }
            throw new IllegalArgumentException("object implicit - explicit expected.");
        }
        else {
            final ASN1Primitive object = asn1TaggedObject.getObject();
            if (asn1TaggedObject.isExplicit()) {
                if (asn1TaggedObject instanceof BERTaggedObject) {
                    return new BERSet(object);
                }
                return new DLSet(object);
            }
            else if (object instanceof ASN1Set) {
                final ASN1Set set = (ASN1Set)object;
                if (asn1TaggedObject instanceof BERTaggedObject) {
                    return set;
                }
                return (ASN1Set)set.toDLObject();
            }
            else {
                if (!(object instanceof ASN1Sequence)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("unknown object in getInstance: ");
                    sb.append(asn1TaggedObject.getClass().getName());
                    throw new IllegalArgumentException(sb.toString());
                }
                final ASN1Encodable[] arrayInternal = ((ASN1Sequence)object).toArrayInternal();
                if (asn1TaggedObject instanceof BERTaggedObject) {
                    return new BERSet(false, arrayInternal);
                }
                return new DLSet(false, arrayInternal);
            }
        }
    }
    
    private static boolean lessThanOrEqual(final byte[] array, final byte[] array2) {
        final boolean b = false;
        final boolean b2 = false;
        boolean b3 = false;
        final int n = array[0] & 0xFFFFFFDF;
        final int n2 = array2[0] & 0xFFFFFFDF;
        if (n != n2) {
            if (n < n2) {
                b3 = true;
            }
            return b3;
        }
        final int n3 = Math.min(array.length, array2.length) - 1;
        for (int i = 1; i < n3; ++i) {
            if (array[i] != array2[i]) {
                boolean b4 = b;
                if ((array[i] & 0xFF) < (array2[i] & 0xFF)) {
                    b4 = true;
                }
                return b4;
            }
        }
        boolean b5 = b2;
        if ((array[n3] & 0xFF) <= (array2[n3] & 0xFF)) {
            b5 = true;
        }
        return b5;
    }
    
    private static void sort(final ASN1Encodable[] array) {
        final int length = array.length;
        final int n = 2;
        if (length < 2) {
            return;
        }
        final ASN1Encodable asn1Encodable = array[0];
        final ASN1Encodable asn1Encodable2 = array[1];
        final byte[] derEncoded = getDEREncoded(asn1Encodable);
        final byte[] derEncoded2 = getDEREncoded(asn1Encodable2);
        int i = n;
        ASN1Encodable asn1Encodable3 = asn1Encodable;
        ASN1Encodable asn1Encodable4 = asn1Encodable2;
        byte[] array2 = derEncoded;
        byte[] array3 = derEncoded2;
        if (lessThanOrEqual(derEncoded2, derEncoded)) {
            asn1Encodable4 = asn1Encodable;
            asn1Encodable3 = asn1Encodable2;
            array3 = derEncoded;
            array2 = derEncoded2;
            i = n;
        }
        while (i < length) {
            final ASN1Encodable asn1Encodable5 = array[i];
            final byte[] derEncoded3 = getDEREncoded(asn1Encodable5);
            if (lessThanOrEqual(array3, derEncoded3)) {
                array[i - 2] = asn1Encodable3;
                asn1Encodable3 = asn1Encodable4;
                asn1Encodable4 = asn1Encodable5;
                array2 = array3;
                array3 = derEncoded3;
            }
            else if (lessThanOrEqual(array2, derEncoded3)) {
                array[i - 2] = asn1Encodable3;
                asn1Encodable3 = asn1Encodable5;
                array2 = derEncoded3;
            }
            else {
                int n2 = i - 1;
                while (true) {
                    --n2;
                    if (n2 <= 0) {
                        break;
                    }
                    final ASN1Encodable asn1Encodable6 = array[n2 - 1];
                    if (lessThanOrEqual(getDEREncoded(asn1Encodable6), derEncoded3)) {
                        break;
                    }
                    array[n2] = asn1Encodable6;
                }
                array[n2] = asn1Encodable5;
            }
            ++i;
        }
        array[length - 2] = asn1Encodable3;
        array[length - 1] = asn1Encodable4;
    }
    
    @Override
    boolean asn1Equals(final ASN1Primitive asn1Primitive) {
        if (!(asn1Primitive instanceof ASN1Set)) {
            return false;
        }
        final ASN1Set set = (ASN1Set)asn1Primitive;
        final int size = this.size();
        if (set.size() != size) {
            return false;
        }
        final DERSet set2 = (DERSet)this.toDERObject();
        final DERSet set3 = (DERSet)set.toDERObject();
        for (int i = 0; i < size; ++i) {
            final ASN1Primitive asn1Primitive2 = set2.elements[i].toASN1Primitive();
            final ASN1Primitive asn1Primitive3 = set3.elements[i].toASN1Primitive();
            if (asn1Primitive2 != asn1Primitive3 && !asn1Primitive2.asn1Equals(asn1Primitive3)) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    abstract void encode(final ASN1OutputStream p0, final boolean p1) throws IOException;
    
    public ASN1Encodable getObjectAt(final int n) {
        return this.elements[n];
    }
    
    public Enumeration getObjects() {
        return new Enumeration() {
            private int pos = 0;
            
            @Override
            public boolean hasMoreElements() {
                return this.pos < ASN1Set.this.elements.length;
            }
            
            @Override
            public Object nextElement() {
                if (this.pos < ASN1Set.this.elements.length) {
                    return ASN1Set.this.elements[this.pos++];
                }
                throw new NoSuchElementException("ASN1Set Enumeration");
            }
        };
    }
    
    @Override
    public int hashCode() {
        int length = this.elements.length;
        int n = length + 1;
        while (true) {
            --length;
            if (length < 0) {
                break;
            }
            n += this.elements[length].toASN1Primitive().hashCode();
        }
        return n;
    }
    
    @Override
    boolean isConstructed() {
        return true;
    }
    
    @Override
    public Iterator<ASN1Encodable> iterator() {
        return new Arrays.Iterator<ASN1Encodable>(this.toArray());
    }
    
    public ASN1SetParser parser() {
        return new ASN1SetParser() {
            private int pos = 0;
            final /* synthetic */ int val$count = ASN1Set.this.size();
            
            @Override
            public ASN1Primitive getLoadedObject() {
                return ASN1Set.this;
            }
            
            @Override
            public ASN1Encodable readObject() throws IOException {
                if (this.val$count == this.pos) {
                    return null;
                }
                final ASN1Encodable asn1Encodable = ASN1Set.this.elements[this.pos++];
                if (asn1Encodable instanceof ASN1Sequence) {
                    return ((ASN1Sequence)asn1Encodable).parser();
                }
                Object parser = asn1Encodable;
                if (asn1Encodable instanceof ASN1Set) {
                    parser = ((ASN1Set)asn1Encodable).parser();
                }
                return (ASN1Encodable)parser;
            }
            
            @Override
            public ASN1Primitive toASN1Primitive() {
                return ASN1Set.this;
            }
        };
    }
    
    public int size() {
        return this.elements.length;
    }
    
    public ASN1Encodable[] toArray() {
        return ASN1EncodableVector.cloneElements(this.elements);
    }
    
    @Override
    ASN1Primitive toDERObject() {
        ASN1Encodable[] elements;
        if (this.isSorted) {
            elements = this.elements;
        }
        else {
            elements = this.elements.clone();
            sort(elements);
        }
        return new DERSet(true, elements);
    }
    
    @Override
    ASN1Primitive toDLObject() {
        return new DLSet(this.isSorted, this.elements);
    }
    
    @Override
    public String toString() {
        final int size = this.size();
        if (size == 0) {
            return "[]";
        }
        final StringBuffer sb = new StringBuffer();
        sb.append('[');
        int n = 0;
        while (true) {
            sb.append(this.elements[n]);
            ++n;
            if (n >= size) {
                break;
            }
            sb.append(", ");
        }
        sb.append(']');
        return sb.toString();
    }
}
