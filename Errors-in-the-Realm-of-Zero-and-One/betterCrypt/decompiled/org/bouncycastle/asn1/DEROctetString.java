// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;

public class DEROctetString extends ASN1OctetString
{
    public DEROctetString(final ASN1Encodable asn1Encodable) throws IOException {
        super(asn1Encodable.toASN1Primitive().getEncoded("DER"));
    }
    
    public DEROctetString(final byte[] array) {
        super(array);
    }
    
    static void encode(final ASN1OutputStream asn1OutputStream, final boolean b, final byte[] array, final int n, final int n2) throws IOException {
        asn1OutputStream.writeEncoded(b, 4, array, n, n2);
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream, final boolean b) throws IOException {
        asn1OutputStream.writeEncoded(b, 4, this.string);
    }
    
    @Override
    int encodedLength() {
        return StreamUtil.calculateBodyLength(this.string.length) + 1 + this.string.length;
    }
    
    @Override
    boolean isConstructed() {
        return false;
    }
    
    @Override
    ASN1Primitive toDERObject() {
        return this;
    }
    
    @Override
    ASN1Primitive toDLObject() {
        return this;
    }
}
