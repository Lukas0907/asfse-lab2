// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import org.bouncycastle.util.encoders.Hex;
import java.io.IOException;
import org.bouncycastle.util.Arrays;

public abstract class ASN1ApplicationSpecific extends ASN1Primitive
{
    protected final boolean isConstructed;
    protected final byte[] octets;
    protected final int tag;
    
    ASN1ApplicationSpecific(final boolean isConstructed, final int tag, final byte[] array) {
        this.isConstructed = isConstructed;
        this.tag = tag;
        this.octets = Arrays.clone(array);
    }
    
    public static ASN1ApplicationSpecific getInstance(final Object o) {
        if (o != null && !(o instanceof ASN1ApplicationSpecific)) {
            if (o instanceof byte[]) {
                try {
                    return getInstance(ASN1Primitive.fromByteArray((byte[])o));
                }
                catch (IOException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to construct object from byte[]: ");
                    sb.append(ex.getMessage());
                    throw new IllegalArgumentException(sb.toString());
                }
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("unknown object in getInstance: ");
            sb2.append(o.getClass().getName());
            throw new IllegalArgumentException(sb2.toString());
        }
        return (ASN1ApplicationSpecific)o;
    }
    
    protected static int getLengthOfHeader(final byte[] array) {
        final int n = array[1] & 0xFF;
        if (n == 128) {
            return 2;
        }
        if (n <= 127) {
            return 2;
        }
        final int i = n & 0x7F;
        if (i <= 4) {
            return i + 2;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("DER length more than 4 bytes: ");
        sb.append(i);
        throw new IllegalStateException(sb.toString());
    }
    
    private byte[] replaceTagNumber(final int n, final byte[] array) throws IOException {
        int n4;
        if ((array[0] & 0x1F) == 0x1F) {
            int n2 = 2;
            int n3 = array[1] & 0xFF;
            if ((n3 & 0x7F) == 0x0) {
                throw new IOException("corrupted stream - invalid high tag number found");
            }
            while (true) {
                n4 = n2;
                if ((n3 & 0x80) == 0x0) {
                    break;
                }
                n3 = (array[n2] & 0xFF);
                ++n2;
            }
        }
        else {
            n4 = 1;
        }
        final byte[] array2 = new byte[array.length - n4 + 1];
        System.arraycopy(array, n4, array2, 1, array2.length - 1);
        array2[0] = (byte)n;
        return array2;
    }
    
    @Override
    boolean asn1Equals(final ASN1Primitive asn1Primitive) {
        final boolean b = asn1Primitive instanceof ASN1ApplicationSpecific;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final ASN1ApplicationSpecific asn1ApplicationSpecific = (ASN1ApplicationSpecific)asn1Primitive;
        boolean b3 = b2;
        if (this.isConstructed == asn1ApplicationSpecific.isConstructed) {
            b3 = b2;
            if (this.tag == asn1ApplicationSpecific.tag) {
                b3 = b2;
                if (Arrays.areEqual(this.octets, asn1ApplicationSpecific.octets)) {
                    b3 = true;
                }
            }
        }
        return b3;
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream, final boolean b) throws IOException {
        int n;
        if (this.isConstructed) {
            n = 96;
        }
        else {
            n = 64;
        }
        asn1OutputStream.writeEncoded(b, n, this.tag, this.octets);
    }
    
    @Override
    int encodedLength() throws IOException {
        return StreamUtil.calculateTagLength(this.tag) + StreamUtil.calculateBodyLength(this.octets.length) + this.octets.length;
    }
    
    public int getApplicationTag() {
        return this.tag;
    }
    
    public byte[] getContents() {
        return Arrays.clone(this.octets);
    }
    
    public ASN1Primitive getObject() throws IOException {
        return ASN1Primitive.fromByteArray(this.getContents());
    }
    
    public ASN1Primitive getObject(final int n) throws IOException {
        if (n < 31) {
            final byte[] encoded = this.getEncoded();
            final byte[] replaceTagNumber = this.replaceTagNumber(n, encoded);
            if ((encoded[0] & 0x20) != 0x0) {
                replaceTagNumber[0] |= 0x20;
            }
            return ASN1Primitive.fromByteArray(replaceTagNumber);
        }
        throw new IOException("unsupported tag number");
    }
    
    @Override
    public int hashCode() {
        return (this.isConstructed ? 1 : 0) ^ this.tag ^ Arrays.hashCode(this.octets);
    }
    
    public boolean isConstructed() {
        return this.isConstructed;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("[");
        if (this.isConstructed()) {
            sb.append("CONSTRUCTED ");
        }
        sb.append("APPLICATION ");
        sb.append(Integer.toString(this.getApplicationTag()));
        sb.append("]");
        String hexString;
        if (this.octets != null) {
            sb.append(" #");
            hexString = Hex.toHexString(this.octets);
        }
        else {
            hexString = " #null";
        }
        sb.append(hexString);
        sb.append(" ");
        return sb.toString();
    }
}
