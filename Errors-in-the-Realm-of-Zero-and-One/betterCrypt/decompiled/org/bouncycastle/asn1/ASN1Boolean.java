// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;

public class ASN1Boolean extends ASN1Primitive
{
    public static final ASN1Boolean FALSE;
    private static final byte FALSE_VALUE = 0;
    public static final ASN1Boolean TRUE;
    private static final byte TRUE_VALUE = -1;
    private final byte value;
    
    static {
        FALSE = new ASN1Boolean((byte)0);
        TRUE = new ASN1Boolean((byte)(-1));
    }
    
    private ASN1Boolean(final byte value) {
        this.value = value;
    }
    
    static ASN1Boolean fromOctetString(final byte[] array) {
        if (array.length != 1) {
            throw new IllegalArgumentException("BOOLEAN value should have 1 byte in it");
        }
        final byte b = array[0];
        if (b == -1) {
            return ASN1Boolean.TRUE;
        }
        if (b != 0) {
            return new ASN1Boolean(b);
        }
        return ASN1Boolean.FALSE;
    }
    
    public static ASN1Boolean getInstance(final int n) {
        if (n != 0) {
            return ASN1Boolean.TRUE;
        }
        return ASN1Boolean.FALSE;
    }
    
    public static ASN1Boolean getInstance(final Object o) {
        if (o != null && !(o instanceof ASN1Boolean)) {
            if (o instanceof byte[]) {
                final byte[] array = (byte[])o;
                try {
                    return (ASN1Boolean)ASN1Primitive.fromByteArray(array);
                }
                catch (IOException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("failed to construct boolean from byte[]: ");
                    sb.append(ex.getMessage());
                    throw new IllegalArgumentException(sb.toString());
                }
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("illegal object in getInstance: ");
            sb2.append(o.getClass().getName());
            throw new IllegalArgumentException(sb2.toString());
        }
        return (ASN1Boolean)o;
    }
    
    public static ASN1Boolean getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        final ASN1Primitive object = asn1TaggedObject.getObject();
        if (!b && !(object instanceof ASN1Boolean)) {
            return fromOctetString(ASN1OctetString.getInstance(object).getOctets());
        }
        return getInstance(object);
    }
    
    public static ASN1Boolean getInstance(final boolean b) {
        if (b) {
            return ASN1Boolean.TRUE;
        }
        return ASN1Boolean.FALSE;
    }
    
    @Override
    boolean asn1Equals(final ASN1Primitive asn1Primitive) {
        final boolean b = asn1Primitive instanceof ASN1Boolean;
        boolean b2 = false;
        if (!b) {
            return false;
        }
        if (this.isTrue() == ((ASN1Boolean)asn1Primitive).isTrue()) {
            b2 = true;
        }
        return b2;
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream, final boolean b) throws IOException {
        asn1OutputStream.writeEncoded(b, 1, this.value);
    }
    
    @Override
    int encodedLength() {
        return 3;
    }
    
    @Override
    public int hashCode() {
        throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:780)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.enexpr(TypeTransformer.java:659)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:719)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s1stmt(TypeTransformer.java:810)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:840)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
    }
    
    @Override
    boolean isConstructed() {
        return false;
    }
    
    public boolean isTrue() {
        return this.value != 0;
    }
    
    @Override
    ASN1Primitive toDERObject() {
        if (this.isTrue()) {
            return ASN1Boolean.TRUE;
        }
        return ASN1Boolean.FALSE;
    }
    
    @Override
    public String toString() {
        if (this.isTrue()) {
            return "TRUE";
        }
        return "FALSE";
    }
}
