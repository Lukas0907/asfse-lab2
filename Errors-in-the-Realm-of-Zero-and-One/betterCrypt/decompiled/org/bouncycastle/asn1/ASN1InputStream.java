// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.EOFException;
import org.bouncycastle.util.io.Streams;
import java.io.IOException;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.FilterInputStream;

public class ASN1InputStream extends FilterInputStream implements BERTags
{
    private final boolean lazyEvaluate;
    private final int limit;
    private final byte[][] tmpBuffers;
    
    public ASN1InputStream(final InputStream inputStream) {
        this(inputStream, StreamUtil.findLimit(inputStream));
    }
    
    public ASN1InputStream(final InputStream inputStream, final int n) {
        this(inputStream, n, false);
    }
    
    public ASN1InputStream(final InputStream in, final int limit, final boolean lazyEvaluate) {
        super(in);
        this.limit = limit;
        this.lazyEvaluate = lazyEvaluate;
        this.tmpBuffers = new byte[11][];
    }
    
    public ASN1InputStream(final InputStream inputStream, final boolean b) {
        this(inputStream, StreamUtil.findLimit(inputStream), b);
    }
    
    public ASN1InputStream(final byte[] buf) {
        this(new ByteArrayInputStream(buf), buf.length);
    }
    
    public ASN1InputStream(final byte[] buf, final boolean b) {
        this(new ByteArrayInputStream(buf), buf.length, b);
    }
    
    static ASN1Primitive createPrimitiveDERObject(final int i, final DefiniteLengthInputStream definiteLengthInputStream, final byte[][] array) throws IOException {
        if (i == 10) {
            return ASN1Enumerated.fromOctetString(getBuffer(definiteLengthInputStream, array));
        }
        if (i == 12) {
            return new DERUTF8String(definiteLengthInputStream.toByteArray());
        }
        if (i == 30) {
            return new DERBMPString(getBMPCharBuffer(definiteLengthInputStream));
        }
        switch (i) {
            default: {
                switch (i) {
                    default: {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("unknown tag ");
                        sb.append(i);
                        sb.append(" encountered");
                        throw new IOException(sb.toString());
                    }
                    case 28: {
                        return new DERUniversalString(definiteLengthInputStream.toByteArray());
                    }
                    case 27: {
                        return new DERGeneralString(definiteLengthInputStream.toByteArray());
                    }
                    case 26: {
                        return new DERVisibleString(definiteLengthInputStream.toByteArray());
                    }
                    case 25: {
                        return new DERGraphicString(definiteLengthInputStream.toByteArray());
                    }
                    case 24: {
                        return new ASN1GeneralizedTime(definiteLengthInputStream.toByteArray());
                    }
                    case 23: {
                        return new ASN1UTCTime(definiteLengthInputStream.toByteArray());
                    }
                    case 22: {
                        return new DERIA5String(definiteLengthInputStream.toByteArray());
                    }
                    case 21: {
                        return new DERVideotexString(definiteLengthInputStream.toByteArray());
                    }
                    case 20: {
                        return new DERT61String(definiteLengthInputStream.toByteArray());
                    }
                    case 19: {
                        return new DERPrintableString(definiteLengthInputStream.toByteArray());
                    }
                    case 18: {
                        return new DERNumericString(definiteLengthInputStream.toByteArray());
                    }
                }
                break;
            }
            case 6: {
                return ASN1ObjectIdentifier.fromOctetString(getBuffer(definiteLengthInputStream, array));
            }
            case 5: {
                return DERNull.INSTANCE;
            }
            case 4: {
                return new DEROctetString(definiteLengthInputStream.toByteArray());
            }
            case 3: {
                return ASN1BitString.fromInputStream(definiteLengthInputStream.getRemaining(), definiteLengthInputStream);
            }
            case 2: {
                return new ASN1Integer(definiteLengthInputStream.toByteArray(), false);
            }
            case 1: {
                return ASN1Boolean.fromOctetString(getBuffer(definiteLengthInputStream, array));
            }
        }
    }
    
    private static char[] getBMPCharBuffer(final DefiniteLengthInputStream definiteLengthInputStream) throws IOException {
        int i = definiteLengthInputStream.getRemaining();
        if ((i & 0x1) != 0x0) {
            throw new IOException("malformed BMPString encoding encountered");
        }
        final char[] array = new char[i / 2];
        final byte[] array2 = new byte[8];
        final int n = 0;
        int n2 = 0;
        while (i >= 8) {
            if (Streams.readFully(definiteLengthInputStream, array2, 0, 8) != 8) {
                throw new EOFException("EOF encountered in middle of BMPString");
            }
            array[n2] = (char)(array2[0] << 8 | (array2[1] & 0xFF));
            array[n2 + 1] = (char)(array2[2] << 8 | (array2[3] & 0xFF));
            array[n2 + 2] = (char)(array2[4] << 8 | (array2[5] & 0xFF));
            array[n2 + 3] = (char)(array2[6] << 8 | (array2[7] & 0xFF));
            n2 += 4;
            i -= 8;
        }
        int n3 = n2;
        if (i > 0) {
            if (Streams.readFully(definiteLengthInputStream, array2, 0, i) != i) {
                throw new EOFException("EOF encountered in middle of BMPString");
            }
            int n4 = n;
            while (true) {
                final int n5 = n4 + 1;
                final byte b = array2[n4];
                final int n6 = n5 + 1;
                final byte b2 = array2[n5];
                n3 = n2 + 1;
                array[n2] = (char)(b << 8 | (b2 & 0xFF));
                if (n6 >= i) {
                    break;
                }
                final int n7 = n6;
                final int n8 = n3;
                n4 = n7;
                n2 = n8;
            }
        }
        if (definiteLengthInputStream.getRemaining() == 0 && array.length == n3) {
            return array;
        }
        throw new IllegalStateException();
    }
    
    private static byte[] getBuffer(final DefiniteLengthInputStream definiteLengthInputStream, final byte[][] array) throws IOException {
        final int remaining = definiteLengthInputStream.getRemaining();
        if (definiteLengthInputStream.getRemaining() < array.length) {
            byte[] array2;
            if ((array2 = array[remaining]) == null) {
                array2 = new byte[remaining];
                array[remaining] = array2;
            }
            Streams.readFully(definiteLengthInputStream, array2);
            return array2;
        }
        return definiteLengthInputStream.toByteArray();
    }
    
    static int readLength(final InputStream inputStream, final int i, final boolean b) throws IOException {
        final int read = inputStream.read();
        if (read < 0) {
            throw new EOFException("EOF found when length expected");
        }
        if (read == 128) {
            return -1;
        }
        if (read <= 127) {
            return read;
        }
        final int j = read & 0x7F;
        if (j > 4) {
            final StringBuilder sb = new StringBuilder();
            sb.append("DER length more than 4 bytes: ");
            sb.append(j);
            throw new IOException(sb.toString());
        }
        int k = 0;
        int l = 0;
        while (k < j) {
            final int read2 = inputStream.read();
            if (read2 < 0) {
                throw new EOFException("EOF found reading length");
            }
            l = (l << 8) + read2;
            ++k;
        }
        if (l < 0) {
            throw new IOException("corrupted stream - negative length found");
        }
        if (l >= i && !b) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("corrupted stream - out of bounds length found: ");
            sb2.append(l);
            sb2.append(" >= ");
            sb2.append(i);
            throw new IOException(sb2.toString());
        }
        return l;
    }
    
    static int readTagNumber(final InputStream inputStream, int n) throws IOException {
        n &= 0x1F;
        if (n != 31) {
            return n;
        }
        int n2 = 0;
        n = inputStream.read();
        if ((n & 0x7F) == 0x0) {
            throw new IOException("corrupted stream - invalid high tag number found");
        }
        while (n >= 0 && (n & 0x80) != 0x0) {
            n2 = (n2 | (n & 0x7F)) << 7;
            n = inputStream.read();
        }
        if (n >= 0) {
            return n2 | (n & 0x7F);
        }
        throw new EOFException("EOF found inside tag value.");
    }
    
    protected ASN1Primitive buildObject(int i, final int j, final int n) throws IOException {
        final int n2 = 0;
        final boolean b = (i & 0x20) != 0x0;
        final DefiniteLengthInputStream definiteLengthInputStream = new DefiniteLengthInputStream(this, n, this.limit);
        if ((i & 0x40) != 0x0) {
            return new DLApplicationSpecific(b, j, definiteLengthInputStream.toByteArray());
        }
        if ((i & 0x80) != 0x0) {
            return new ASN1StreamParser(definiteLengthInputStream).readTaggedObject(b, j);
        }
        if (!b) {
            return createPrimitiveDERObject(j, definiteLengthInputStream, this.tmpBuffers);
        }
        if (j == 4) {
            final ASN1EncodableVector vector = this.readVector(definiteLengthInputStream);
            ASN1OctetString[] array;
            ASN1Encodable value;
            StringBuilder sb;
            for (array = new ASN1OctetString[vector.size()], i = n2; i != array.length; ++i) {
                value = vector.get(i);
                if (!(value instanceof ASN1OctetString)) {
                    sb = new StringBuilder();
                    sb.append("unknown object encountered in constructed OCTET STRING: ");
                    sb.append(((ASN1OctetString)value).getClass());
                    throw new ASN1Exception(sb.toString());
                }
                array[i] = (ASN1OctetString)value;
            }
            return new BEROctetString(array);
        }
        if (j == 8) {
            return new DLExternal(this.readVector(definiteLengthInputStream));
        }
        if (j != 16) {
            if (j == 17) {
                return DLFactory.createSet(this.readVector(definiteLengthInputStream));
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("unknown tag ");
            sb2.append(j);
            sb2.append(" encountered");
            throw new IOException(sb2.toString());
        }
        else {
            if (this.lazyEvaluate) {
                return new LazyEncodedSequence(definiteLengthInputStream.toByteArray());
            }
            return DLFactory.createSequence(this.readVector(definiteLengthInputStream));
        }
    }
    
    int getLimit() {
        return this.limit;
    }
    
    protected void readFully(final byte[] array) throws IOException {
        if (Streams.readFully(this, array) == array.length) {
            return;
        }
        throw new EOFException("EOF encountered in middle of object");
    }
    
    protected int readLength() throws IOException {
        return readLength(this, this.limit, false);
    }
    
    public ASN1Primitive readObject() throws IOException {
        final int read = this.read();
        if (read <= 0) {
            if (read != 0) {
                return null;
            }
            throw new IOException("unexpected end-of-contents marker");
        }
        else {
            final int tagNumber = readTagNumber(this, read);
            final boolean b = (read & 0x20) != 0x0;
            final int length = this.readLength();
            if (length < 0) {
                if (!b) {
                    throw new IOException("indefinite-length primitive encoding encountered");
                }
                final ASN1StreamParser asn1StreamParser = new ASN1StreamParser(new IndefiniteLengthInputStream(this, this.limit), this.limit);
                if ((read & 0x40) != 0x0) {
                    return new BERApplicationSpecificParser(tagNumber, asn1StreamParser).getLoadedObject();
                }
                if ((read & 0x80) != 0x0) {
                    return new BERTaggedObjectParser(true, tagNumber, asn1StreamParser).getLoadedObject();
                }
                if (tagNumber == 4) {
                    return new BEROctetStringParser(asn1StreamParser).getLoadedObject();
                }
                if (tagNumber == 8) {
                    return new DERExternalParser(asn1StreamParser).getLoadedObject();
                }
                if (tagNumber == 16) {
                    return new BERSequenceParser(asn1StreamParser).getLoadedObject();
                }
                if (tagNumber == 17) {
                    return new BERSetParser(asn1StreamParser).getLoadedObject();
                }
                throw new IOException("unknown BER object encountered");
            }
            else {
                try {
                    return this.buildObject(read, tagNumber, length);
                }
                catch (IllegalArgumentException ex) {
                    throw new ASN1Exception("corrupted stream detected", ex);
                }
            }
        }
    }
    
    ASN1EncodableVector readVector(final DefiniteLengthInputStream definiteLengthInputStream) throws IOException {
        if (definiteLengthInputStream.getRemaining() < 1) {
            return new ASN1EncodableVector(0);
        }
        final ASN1InputStream asn1InputStream = new ASN1InputStream(definiteLengthInputStream);
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        while (true) {
            final ASN1Primitive object = asn1InputStream.readObject();
            if (object == null) {
                break;
            }
            asn1EncodableVector.add(object);
        }
        return asn1EncodableVector;
    }
}
