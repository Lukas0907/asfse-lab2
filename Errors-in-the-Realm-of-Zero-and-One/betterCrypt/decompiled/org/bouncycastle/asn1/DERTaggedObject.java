// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;

public class DERTaggedObject extends ASN1TaggedObject
{
    public DERTaggedObject(final int n, final ASN1Encodable asn1Encodable) {
        super(true, n, asn1Encodable);
    }
    
    public DERTaggedObject(final boolean b, final int n, final ASN1Encodable asn1Encodable) {
        super(b, n, asn1Encodable);
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream, final boolean b) throws IOException {
        final ASN1Primitive derObject = this.obj.toASN1Primitive().toDERObject();
        int n;
        if (!this.explicit && !derObject.isConstructed()) {
            n = 128;
        }
        else {
            n = 160;
        }
        asn1OutputStream.writeTag(b, n, this.tagNo);
        if (this.explicit) {
            asn1OutputStream.writeLength(derObject.encodedLength());
        }
        derObject.encode(asn1OutputStream.getDERSubStream(), this.explicit);
    }
    
    @Override
    int encodedLength() throws IOException {
        int encodedLength = this.obj.toASN1Primitive().toDERObject().encodedLength();
        int calculateTagLength;
        if (this.explicit) {
            calculateTagLength = StreamUtil.calculateTagLength(this.tagNo) + StreamUtil.calculateBodyLength(encodedLength);
        }
        else {
            --encodedLength;
            calculateTagLength = StreamUtil.calculateTagLength(this.tagNo);
        }
        return calculateTagLength + encodedLength;
    }
    
    @Override
    boolean isConstructed() {
        return this.explicit || this.obj.toASN1Primitive().toDERObject().isConstructed();
    }
    
    @Override
    ASN1Primitive toDERObject() {
        return this;
    }
    
    @Override
    ASN1Primitive toDLObject() {
        return this;
    }
}
