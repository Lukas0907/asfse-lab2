// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class DERApplicationSpecific extends ASN1ApplicationSpecific
{
    public DERApplicationSpecific(final int n, final ASN1Encodable asn1Encodable) throws IOException {
        this(true, n, asn1Encodable);
    }
    
    public DERApplicationSpecific(final int n, final ASN1EncodableVector asn1EncodableVector) {
        super(true, n, getEncodedVector(asn1EncodableVector));
    }
    
    public DERApplicationSpecific(final int n, final byte[] array) {
        this(false, n, array);
    }
    
    public DERApplicationSpecific(final boolean b, final int n, final ASN1Encodable asn1Encodable) throws IOException {
        super(b || asn1Encodable.toASN1Primitive().isConstructed(), n, getEncoding(b, asn1Encodable));
    }
    
    DERApplicationSpecific(final boolean b, final int n, final byte[] array) {
        super(b, n, array);
    }
    
    private static byte[] getEncodedVector(final ASN1EncodableVector asn1EncodableVector) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 0;
        while (i != asn1EncodableVector.size()) {
            try {
                byteArrayOutputStream.write(((ASN1Object)asn1EncodableVector.get(i)).getEncoded("DER"));
                ++i;
                continue;
            }
            catch (IOException obj) {
                final StringBuilder sb = new StringBuilder();
                sb.append("malformed object: ");
                sb.append(obj);
                throw new ASN1ParsingException(sb.toString(), obj);
            }
            break;
        }
        return byteArrayOutputStream.toByteArray();
    }
    
    private static byte[] getEncoding(final boolean b, final ASN1Encodable asn1Encodable) throws IOException {
        final byte[] encoded = asn1Encodable.toASN1Primitive().getEncoded("DER");
        if (b) {
            return encoded;
        }
        final int lengthOfHeader = ASN1ApplicationSpecific.getLengthOfHeader(encoded);
        final byte[] array = new byte[encoded.length - lengthOfHeader];
        System.arraycopy(encoded, lengthOfHeader, array, 0, array.length);
        return array;
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream, final boolean b) throws IOException {
        int n;
        if (this.isConstructed) {
            n = 96;
        }
        else {
            n = 64;
        }
        asn1OutputStream.writeEncoded(b, n, this.tag, this.octets);
    }
}
