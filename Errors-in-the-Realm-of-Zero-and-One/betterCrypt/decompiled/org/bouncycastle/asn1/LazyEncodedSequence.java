// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.util.Iterator;
import java.util.Enumeration;
import java.io.IOException;

class LazyEncodedSequence extends ASN1Sequence
{
    private byte[] encoded;
    
    LazyEncodedSequence(final byte[] encoded) throws IOException {
        this.encoded = encoded;
    }
    
    private void force() {
        if (this.encoded != null) {
            final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
            final LazyConstructionEnumeration lazyConstructionEnumeration = new LazyConstructionEnumeration(this.encoded);
            while (lazyConstructionEnumeration.hasMoreElements()) {
                asn1EncodableVector.add(lazyConstructionEnumeration.nextElement());
            }
            this.elements = asn1EncodableVector.takeElements();
            this.encoded = null;
        }
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream, final boolean b) throws IOException {
        synchronized (this) {
            if (this.encoded != null) {
                asn1OutputStream.writeEncoded(b, 48, this.encoded);
            }
            else {
                super.toDLObject().encode(asn1OutputStream, b);
            }
        }
    }
    
    @Override
    int encodedLength() throws IOException {
        synchronized (this) {
            if (this.encoded != null) {
                final int calculateBodyLength = StreamUtil.calculateBodyLength(this.encoded.length);
                final int length = this.encoded.length;
                // monitorexit(this)
                return calculateBodyLength + 1 + length;
            }
            return super.toDLObject().encodedLength();
        }
    }
    
    @Override
    public ASN1Encodable getObjectAt(final int n) {
        synchronized (this) {
            this.force();
            return super.getObjectAt(n);
        }
    }
    
    @Override
    public Enumeration getObjects() {
        synchronized (this) {
            if (this.encoded != null) {
                return new LazyConstructionEnumeration(this.encoded);
            }
            return super.getObjects();
        }
    }
    
    @Override
    public int hashCode() {
        synchronized (this) {
            this.force();
            return super.hashCode();
        }
    }
    
    @Override
    public Iterator<ASN1Encodable> iterator() {
        synchronized (this) {
            this.force();
            return super.iterator();
        }
    }
    
    @Override
    public int size() {
        synchronized (this) {
            this.force();
            return super.size();
        }
    }
    
    @Override
    public ASN1Encodable[] toArray() {
        synchronized (this) {
            this.force();
            return super.toArray();
        }
    }
    
    @Override
    ASN1Encodable[] toArrayInternal() {
        this.force();
        return super.toArrayInternal();
    }
    
    @Override
    ASN1Primitive toDERObject() {
        synchronized (this) {
            this.force();
            return super.toDERObject();
        }
    }
    
    @Override
    ASN1Primitive toDLObject() {
        synchronized (this) {
            this.force();
            return super.toDLObject();
        }
    }
}
