// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.esf;

import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERSequence;
import java.util.Enumeration;
import org.bouncycastle.asn1.x509.CertificateList;
import org.bouncycastle.asn1.ocsp.BasicOCSPResponse;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Object;

public class RevocationValues extends ASN1Object
{
    private ASN1Sequence crlVals;
    private ASN1Sequence ocspVals;
    private OtherRevVals otherRevVals;
    
    private RevocationValues(final ASN1Sequence asn1Sequence) {
        if (asn1Sequence.size() <= 3) {
            final Enumeration objects = asn1Sequence.getObjects();
            while (objects.hasMoreElements()) {
                final ASN1TaggedObject asn1TaggedObject = objects.nextElement();
                final int tagNo = asn1TaggedObject.getTagNo();
                if (tagNo != 0) {
                    if (tagNo != 1) {
                        if (tagNo != 2) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("invalid tag: ");
                            sb.append(asn1TaggedObject.getTagNo());
                            throw new IllegalArgumentException(sb.toString());
                        }
                        this.otherRevVals = OtherRevVals.getInstance(asn1TaggedObject.getObject());
                    }
                    else {
                        final ASN1Sequence ocspVals = (ASN1Sequence)asn1TaggedObject.getObject();
                        final Enumeration objects2 = ocspVals.getObjects();
                        while (objects2.hasMoreElements()) {
                            BasicOCSPResponse.getInstance(objects2.nextElement());
                        }
                        this.ocspVals = ocspVals;
                    }
                }
                else {
                    final ASN1Sequence crlVals = (ASN1Sequence)asn1TaggedObject.getObject();
                    final Enumeration objects3 = crlVals.getObjects();
                    while (objects3.hasMoreElements()) {
                        CertificateList.getInstance(objects3.nextElement());
                    }
                    this.crlVals = crlVals;
                }
            }
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Bad sequence size: ");
        sb2.append(asn1Sequence.size());
        throw new IllegalArgumentException(sb2.toString());
    }
    
    public RevocationValues(final CertificateList[] array, final BasicOCSPResponse[] array2, final OtherRevVals otherRevVals) {
        if (array != null) {
            this.crlVals = new DERSequence(array);
        }
        if (array2 != null) {
            this.ocspVals = new DERSequence(array2);
        }
        this.otherRevVals = otherRevVals;
    }
    
    public static RevocationValues getInstance(final Object o) {
        if (o instanceof RevocationValues) {
            return (RevocationValues)o;
        }
        if (o != null) {
            return new RevocationValues(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public CertificateList[] getCrlVals() {
        final ASN1Sequence crlVals = this.crlVals;
        int i = 0;
        if (crlVals == null) {
            return new CertificateList[0];
        }
        CertificateList[] array;
        for (array = new CertificateList[crlVals.size()]; i < array.length; ++i) {
            array[i] = CertificateList.getInstance(this.crlVals.getObjectAt(i));
        }
        return array;
    }
    
    public BasicOCSPResponse[] getOcspVals() {
        final ASN1Sequence ocspVals = this.ocspVals;
        int i = 0;
        if (ocspVals == null) {
            return new BasicOCSPResponse[0];
        }
        BasicOCSPResponse[] array;
        for (array = new BasicOCSPResponse[ocspVals.size()]; i < array.length; ++i) {
            array[i] = BasicOCSPResponse.getInstance(this.ocspVals.getObjectAt(i));
        }
        return array;
    }
    
    public OtherRevVals getOtherRevVals() {
        return this.otherRevVals;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(3);
        final ASN1Sequence crlVals = this.crlVals;
        if (crlVals != null) {
            asn1EncodableVector.add(new DERTaggedObject(true, 0, crlVals));
        }
        final ASN1Sequence ocspVals = this.ocspVals;
        if (ocspVals != null) {
            asn1EncodableVector.add(new DERTaggedObject(true, 1, ocspVals));
        }
        final OtherRevVals otherRevVals = this.otherRevVals;
        if (otherRevVals != null) {
            asn1EncodableVector.add(new DERTaggedObject(true, 2, otherRevVals.toASN1Primitive()));
        }
        return new DERSequence(asn1EncodableVector);
    }
}
