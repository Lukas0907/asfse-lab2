// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.esf;

import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Object;

public class SigPolicyQualifiers extends ASN1Object
{
    ASN1Sequence qualifiers;
    
    private SigPolicyQualifiers(final ASN1Sequence qualifiers) {
        this.qualifiers = qualifiers;
    }
    
    public SigPolicyQualifiers(final SigPolicyQualifierInfo[] array) {
        this.qualifiers = new DERSequence(array);
    }
    
    public static SigPolicyQualifiers getInstance(final Object o) {
        if (o instanceof SigPolicyQualifiers) {
            return (SigPolicyQualifiers)o;
        }
        if (o instanceof ASN1Sequence) {
            return new SigPolicyQualifiers(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public SigPolicyQualifierInfo getInfoAt(final int n) {
        return SigPolicyQualifierInfo.getInstance(this.qualifiers.getObjectAt(n));
    }
    
    public int size() {
        return this.qualifiers.size();
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        return this.qualifiers;
    }
}
