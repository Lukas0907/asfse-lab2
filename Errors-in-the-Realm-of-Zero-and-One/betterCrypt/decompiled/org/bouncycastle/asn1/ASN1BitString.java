// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;
import java.io.EOFException;
import org.bouncycastle.util.io.Streams;
import java.io.InputStream;
import org.bouncycastle.util.Arrays;

public abstract class ASN1BitString extends ASN1Primitive implements ASN1String
{
    private static final char[] table;
    protected final byte[] data;
    protected final int padBits;
    
    static {
        table = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
    }
    
    protected ASN1BitString(final byte b, final int padBits) {
        if (padBits <= 7 && padBits >= 0) {
            this.data = new byte[] { b };
            this.padBits = padBits;
            return;
        }
        throw new IllegalArgumentException("pad bits cannot be greater than 7 or less than 0");
    }
    
    public ASN1BitString(final byte[] array, final int padBits) {
        if (array == null) {
            throw new NullPointerException("'data' cannot be null");
        }
        if (array.length == 0 && padBits != 0) {
            throw new IllegalArgumentException("zero length data with non-zero pad bits");
        }
        if (padBits <= 7 && padBits >= 0) {
            this.data = Arrays.clone(array);
            this.padBits = padBits;
            return;
        }
        throw new IllegalArgumentException("pad bits cannot be greater than 7 or less than 0");
    }
    
    protected static byte[] derForm(final byte[] array, final int n) {
        if (array.length == 0) {
            return array;
        }
        final byte[] clone = Arrays.clone(array);
        final int n2 = array.length - 1;
        clone[n2] &= (byte)(255 << n);
        return clone;
    }
    
    static ASN1BitString fromInputStream(final int n, final InputStream inputStream) throws IOException {
        if (n >= 1) {
            final int read = inputStream.read();
            final byte[] array = new byte[n - 1];
            if (array.length != 0) {
                if (Streams.readFully(inputStream, array) != array.length) {
                    throw new EOFException("EOF encountered in middle of BIT STRING");
                }
                if (read > 0 && read < 8 && array[array.length - 1] != (byte)(array[array.length - 1] & 255 << read)) {
                    return new DLBitString(array, read);
                }
            }
            return new DERBitString(array, read);
        }
        throw new IllegalArgumentException("truncated BIT STRING detected");
    }
    
    protected static byte[] getBytes(final int n) {
        final int n2 = 0;
        if (n == 0) {
            return new byte[0];
        }
        int n3 = 4;
        for (int n4 = 3; n4 >= 1 && (255 << n4 * 8 & n) == 0x0; --n4) {
            --n3;
        }
        final byte[] array = new byte[n3];
        for (int i = n2; i < n3; ++i) {
            array[i] = (byte)(n >> i * 8 & 0xFF);
        }
        return array;
    }
    
    protected static int getPadBits(int n) {
        int i = 3;
    Label_0052:
        while (true) {
            while (i >= 0) {
                if (i != 0) {
                    final int n2 = n >> i * 8;
                    if (n2 != 0) {
                        n = (n2 & 0xFF);
                        break Label_0052;
                    }
                }
                else if (n != 0) {
                    n &= 0xFF;
                    break Label_0052;
                }
                --i;
                continue;
                if (n == 0) {
                    return 0;
                }
                final int n3 = 1;
                int n4 = n;
                n = n3;
                while (true) {
                    n4 <<= 1;
                    if ((n4 & 0xFF) == 0x0) {
                        break;
                    }
                    ++n;
                }
                return 8 - n;
            }
            n = 0;
            continue Label_0052;
        }
    }
    
    @Override
    boolean asn1Equals(final ASN1Primitive asn1Primitive) {
        final boolean b = asn1Primitive instanceof ASN1BitString;
        boolean b2 = false;
        if (!b) {
            return false;
        }
        final ASN1BitString asn1BitString = (ASN1BitString)asn1Primitive;
        if (this.padBits != asn1BitString.padBits) {
            return false;
        }
        final byte[] data = this.data;
        final byte[] data2 = asn1BitString.data;
        final int length = data.length;
        if (length != data2.length) {
            return false;
        }
        final int n = length - 1;
        if (n < 0) {
            return true;
        }
        for (int i = 0; i < n; ++i) {
            if (data[i] != data2[i]) {
                return false;
            }
        }
        final byte b3 = data[n];
        final int padBits = this.padBits;
        if ((byte)(b3 & 255 << padBits) == (byte)(data2[n] & 255 << padBits)) {
            b2 = true;
        }
        return b2;
    }
    
    @Override
    abstract void encode(final ASN1OutputStream p0, final boolean p1) throws IOException;
    
    public byte[] getBytes() {
        return derForm(this.data, this.padBits);
    }
    
    public ASN1Primitive getLoadedObject() {
        return this.toASN1Primitive();
    }
    
    public byte[] getOctets() {
        if (this.padBits == 0) {
            return Arrays.clone(this.data);
        }
        throw new IllegalStateException("attempt to get non-octet aligned data from BIT STRING");
    }
    
    public int getPadBits() {
        return this.padBits;
    }
    
    @Override
    public String getString() {
        final StringBuffer sb = new StringBuffer("#");
        try {
            final byte[] encoded = this.getEncoded();
            for (int i = 0; i != encoded.length; ++i) {
                sb.append(ASN1BitString.table[encoded[i] >>> 4 & 0xF]);
                sb.append(ASN1BitString.table[encoded[i] & 0xF]);
            }
            return sb.toString();
        }
        catch (IOException ex) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Internal error encoding BitString: ");
            sb2.append(ex.getMessage());
            throw new ASN1ParsingException(sb2.toString(), ex);
        }
    }
    
    @Override
    public int hashCode() {
        final byte[] data = this.data;
        final int n = data.length - 1;
        if (n < 0) {
            return 1;
        }
        return Arrays.hashCode(data, 0, n) * 257 ^ (byte)(data[n] & 255 << this.padBits) ^ this.padBits;
    }
    
    public int intValue() {
        final int min = Math.min(4, this.data.length - 1);
        int i = 0;
        int n = 0;
        while (i < min) {
            n |= (0xFF & this.data[i]) << i * 8;
            ++i;
        }
        int n2 = n;
        if (min >= 0) {
            n2 = n;
            if (min < 4) {
                n2 = (n | ((byte)(this.data[min] & 255 << this.padBits) & 0xFF) << min * 8);
            }
        }
        return n2;
    }
    
    @Override
    ASN1Primitive toDERObject() {
        return new DERBitString(this.data, this.padBits);
    }
    
    @Override
    ASN1Primitive toDLObject() {
        return new DLBitString(this.data, this.padBits);
    }
    
    @Override
    public String toString() {
        return this.getString();
    }
}
