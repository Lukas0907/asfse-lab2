// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.util;

import java.io.IOException;
import org.bouncycastle.asn1.ASN1ApplicationSpecific;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.asn1.ASN1Encodable;
import java.math.BigInteger;
import java.util.Enumeration;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.BERSet;
import org.bouncycastle.asn1.BEROctetString;
import org.bouncycastle.asn1.ASN1External;
import org.bouncycastle.asn1.ASN1Enumerated;
import org.bouncycastle.asn1.DLApplicationSpecific;
import org.bouncycastle.asn1.DERApplicationSpecific;
import org.bouncycastle.asn1.BERApplicationSpecific;
import org.bouncycastle.asn1.ASN1GeneralizedTime;
import org.bouncycastle.asn1.ASN1UTCTime;
import org.bouncycastle.asn1.DERVideotexString;
import org.bouncycastle.asn1.DERGraphicString;
import org.bouncycastle.asn1.DERT61String;
import org.bouncycastle.asn1.DERBMPString;
import org.bouncycastle.asn1.DERVisibleString;
import org.bouncycastle.asn1.DERPrintableString;
import org.bouncycastle.asn1.DERUTF8String;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Boolean;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.BERTaggedObject;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.BERSequence;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.util.Strings;
import org.bouncycastle.asn1.ASN1Primitive;

public class ASN1Dump
{
    private static final int SAMPLE_SIZE = 32;
    private static final String TAB = "    ";
    
    static void _dumpAsString(String str, final boolean b, ASN1Primitive externalContent, final StringBuffer sb) {
        final String lineSeparator = Strings.lineSeparator();
        Label_1749: {
            while (true) {
                ASN1External asn1External = null;
                Label_1683: {
                    Label_0682: {
                        StringBuilder sb5 = null;
                        if (externalContent instanceof ASN1Sequence) {
                            final Enumeration objects = ((ASN1Sequence)externalContent).getObjects();
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append(str);
                            sb2.append("    ");
                            final String string = sb2.toString();
                            sb.append(str);
                            if (externalContent instanceof BERSequence) {
                                str = "BER Sequence";
                            }
                            else if (externalContent instanceof DERSequence) {
                                str = "DER Sequence";
                            }
                            else {
                                str = "Sequence";
                            }
                            sb.append(str);
                        Label_0096:
                            while (true) {
                                sb.append(lineSeparator);
                                while (objects.hasMoreElements()) {
                                    final ASN1Primitive nextElement = objects.nextElement();
                                    if (nextElement == null || nextElement.equals(DERNull.INSTANCE)) {
                                        sb.append(string);
                                        sb.append("NULL");
                                        continue Label_0096;
                                    }
                                    ASN1Primitive asn1Primitive;
                                    if (nextElement instanceof ASN1Primitive) {
                                        asn1Primitive = nextElement;
                                    }
                                    else {
                                        asn1Primitive = nextElement.toASN1Primitive();
                                    }
                                    _dumpAsString(string, b, asn1Primitive, sb);
                                }
                                return;
                            }
                        }
                        else {
                            if (externalContent instanceof ASN1TaggedObject) {
                                final StringBuilder sb3 = new StringBuilder();
                                sb3.append(str);
                                sb3.append("    ");
                                final String string2 = sb3.toString();
                                sb.append(str);
                                if (externalContent instanceof BERTaggedObject) {
                                    str = "BER Tagged [";
                                }
                                else {
                                    str = "Tagged [";
                                }
                                sb.append(str);
                                final ASN1TaggedObject asn1TaggedObject = (ASN1TaggedObject)externalContent;
                                sb.append(Integer.toString(asn1TaggedObject.getTagNo()));
                                sb.append(']');
                                if (!asn1TaggedObject.isExplicit()) {
                                    sb.append(" IMPLICIT ");
                                }
                                sb.append(lineSeparator);
                                _dumpAsString(string2, b, asn1TaggedObject.getObject(), sb);
                                return;
                            }
                            if (!(externalContent instanceof ASN1Set)) {
                                Label_0608: {
                                    if (!(externalContent instanceof ASN1OctetString)) {
                                        if (externalContent instanceof ASN1ObjectIdentifier) {
                                            final StringBuilder sb4 = new StringBuilder();
                                            sb4.append(str);
                                            sb4.append("ObjectIdentifier(");
                                            sb4.append(((ASN1ObjectIdentifier)externalContent).getId());
                                            sb5 = sb4;
                                        }
                                        else if (externalContent instanceof ASN1Boolean) {
                                            final StringBuilder sb6 = new StringBuilder();
                                            sb6.append(str);
                                            sb6.append("Boolean(");
                                            sb6.append(((ASN1Boolean)externalContent).isTrue());
                                            sb5 = sb6;
                                        }
                                        else {
                                            BigInteger obj = null;
                                            Label_0781: {
                                                if (externalContent instanceof ASN1Integer) {
                                                    final StringBuilder sb7 = new StringBuilder();
                                                    sb7.append(str);
                                                    sb7.append("Integer(");
                                                    obj = ((ASN1Integer)externalContent).getValue();
                                                    sb5 = sb7;
                                                }
                                                else {
                                                    if (!(externalContent instanceof DERBitString)) {
                                                        String str2;
                                                        if (externalContent instanceof DERIA5String) {
                                                            final StringBuilder sb8 = new StringBuilder();
                                                            sb8.append(str);
                                                            sb8.append("IA5String(");
                                                            str2 = ((DERIA5String)externalContent).getString();
                                                            sb5 = sb8;
                                                        }
                                                        else if (externalContent instanceof DERUTF8String) {
                                                            final StringBuilder sb9 = new StringBuilder();
                                                            sb9.append(str);
                                                            sb9.append("UTF8String(");
                                                            str2 = ((DERUTF8String)externalContent).getString();
                                                            sb5 = sb9;
                                                        }
                                                        else if (externalContent instanceof DERPrintableString) {
                                                            final StringBuilder sb10 = new StringBuilder();
                                                            sb10.append(str);
                                                            sb10.append("PrintableString(");
                                                            str2 = ((DERPrintableString)externalContent).getString();
                                                            sb5 = sb10;
                                                        }
                                                        else if (externalContent instanceof DERVisibleString) {
                                                            final StringBuilder sb11 = new StringBuilder();
                                                            sb11.append(str);
                                                            sb11.append("VisibleString(");
                                                            str2 = ((DERVisibleString)externalContent).getString();
                                                            sb5 = sb11;
                                                        }
                                                        else if (externalContent instanceof DERBMPString) {
                                                            final StringBuilder sb12 = new StringBuilder();
                                                            sb12.append(str);
                                                            sb12.append("BMPString(");
                                                            str2 = ((DERBMPString)externalContent).getString();
                                                            sb5 = sb12;
                                                        }
                                                        else if (externalContent instanceof DERT61String) {
                                                            final StringBuilder sb13 = new StringBuilder();
                                                            sb13.append(str);
                                                            sb13.append("T61String(");
                                                            str2 = ((DERT61String)externalContent).getString();
                                                            sb5 = sb13;
                                                        }
                                                        else if (externalContent instanceof DERGraphicString) {
                                                            final StringBuilder sb14 = new StringBuilder();
                                                            sb14.append(str);
                                                            sb14.append("GraphicString(");
                                                            str2 = ((DERGraphicString)externalContent).getString();
                                                            sb5 = sb14;
                                                        }
                                                        else if (externalContent instanceof DERVideotexString) {
                                                            final StringBuilder sb15 = new StringBuilder();
                                                            sb15.append(str);
                                                            sb15.append("VideotexString(");
                                                            str2 = ((DERVideotexString)externalContent).getString();
                                                            sb5 = sb15;
                                                        }
                                                        else if (externalContent instanceof ASN1UTCTime) {
                                                            final StringBuilder sb16 = new StringBuilder();
                                                            sb16.append(str);
                                                            sb16.append("UTCTime(");
                                                            str2 = ((ASN1UTCTime)externalContent).getTime();
                                                            sb5 = sb16;
                                                        }
                                                        else {
                                                            if (!(externalContent instanceof ASN1GeneralizedTime)) {
                                                                String s;
                                                                if (externalContent instanceof BERApplicationSpecific) {
                                                                    s = "BER";
                                                                }
                                                                else if (externalContent instanceof DERApplicationSpecific) {
                                                                    s = "DER";
                                                                }
                                                                else if (externalContent instanceof DLApplicationSpecific) {
                                                                    s = "";
                                                                }
                                                                else {
                                                                    if (externalContent instanceof ASN1Enumerated) {
                                                                        final ASN1Enumerated asn1Enumerated = (ASN1Enumerated)externalContent;
                                                                        final StringBuilder sb17 = new StringBuilder();
                                                                        sb17.append(str);
                                                                        sb17.append("DER Enumerated(");
                                                                        obj = asn1Enumerated.getValue();
                                                                        sb5 = sb17;
                                                                        break Label_0781;
                                                                    }
                                                                    if (!(externalContent instanceof ASN1External)) {
                                                                        break Label_1749;
                                                                    }
                                                                    asn1External = (ASN1External)externalContent;
                                                                    final StringBuilder sb18 = new StringBuilder();
                                                                    sb18.append(str);
                                                                    sb18.append("External ");
                                                                    sb18.append(lineSeparator);
                                                                    sb.append(sb18.toString());
                                                                    final StringBuilder sb19 = new StringBuilder();
                                                                    sb19.append(str);
                                                                    sb19.append("    ");
                                                                    str = sb19.toString();
                                                                    if (asn1External.getDirectReference() != null) {
                                                                        final StringBuilder sb20 = new StringBuilder();
                                                                        sb20.append(str);
                                                                        sb20.append("Direct Reference: ");
                                                                        sb20.append(asn1External.getDirectReference().getId());
                                                                        sb20.append(lineSeparator);
                                                                        sb.append(sb20.toString());
                                                                    }
                                                                    if (asn1External.getIndirectReference() != null) {
                                                                        final StringBuilder sb21 = new StringBuilder();
                                                                        sb21.append(str);
                                                                        sb21.append("Indirect Reference: ");
                                                                        sb21.append(asn1External.getIndirectReference().toString());
                                                                        sb21.append(lineSeparator);
                                                                        sb.append(sb21.toString());
                                                                    }
                                                                    if (asn1External.getDataValueDescriptor() != null) {
                                                                        _dumpAsString(str, b, asn1External.getDataValueDescriptor(), sb);
                                                                    }
                                                                    break Label_1683;
                                                                }
                                                                str = outputApplicationSpecific(s, str, b, externalContent, lineSeparator);
                                                                break Label_0682;
                                                            }
                                                            final StringBuilder sb22 = new StringBuilder();
                                                            sb22.append(str);
                                                            sb22.append("GeneralizedTime(");
                                                            str2 = ((ASN1GeneralizedTime)externalContent).getTime();
                                                            sb5 = sb22;
                                                        }
                                                        sb5.append(str2);
                                                        sb5.append(") ");
                                                        break Label_0670;
                                                    }
                                                    final DERBitString derBitString = (DERBitString)externalContent;
                                                    final StringBuilder sb23 = new StringBuilder();
                                                    sb23.append(str);
                                                    sb23.append("DER Bit String[");
                                                    sb23.append(derBitString.getBytes().length);
                                                    sb23.append(", ");
                                                    sb23.append(derBitString.getPadBits());
                                                    sb23.append("] ");
                                                    sb.append(sb23.toString());
                                                    if (b) {
                                                        str = dumpBinaryDataAsString(str, derBitString.getBytes());
                                                        break Label_0682;
                                                    }
                                                    break Label_0608;
                                                }
                                            }
                                            sb5.append(obj);
                                        }
                                        sb5.append(")");
                                        break Label_0670;
                                    }
                                    final ASN1OctetString asn1OctetString = (ASN1OctetString)externalContent;
                                    StringBuilder sb24;
                                    int i;
                                    if (externalContent instanceof BEROctetString) {
                                        sb24 = new StringBuilder();
                                        sb24.append(str);
                                        sb24.append("BER Constructed Octet String[");
                                        i = asn1OctetString.getOctets().length;
                                    }
                                    else {
                                        sb24 = new StringBuilder();
                                        sb24.append(str);
                                        sb24.append("DER Octet String[");
                                        i = asn1OctetString.getOctets().length;
                                    }
                                    sb24.append(i);
                                    sb24.append("] ");
                                    sb.append(sb24.toString());
                                    if (b) {
                                        str = dumpBinaryDataAsString(str, asn1OctetString.getOctets());
                                        break Label_0682;
                                    }
                                }
                                sb.append(lineSeparator);
                                return;
                            }
                            final Enumeration objects2 = ((ASN1Set)externalContent).getObjects();
                            final StringBuilder sb25 = new StringBuilder();
                            sb25.append(str);
                            sb25.append("    ");
                            final String string3 = sb25.toString();
                            sb.append(str);
                            if (externalContent instanceof BERSet) {
                                str = "BER Set";
                            }
                            else if (externalContent instanceof DERSet) {
                                str = "DER Set";
                            }
                            else {
                                str = "Set";
                            }
                            sb.append(str);
                        Label_0405:
                            while (true) {
                                sb.append(lineSeparator);
                                while (objects2.hasMoreElements()) {
                                    final ASN1Primitive nextElement2 = objects2.nextElement();
                                    if (nextElement2 == null) {
                                        sb.append(string3);
                                        sb.append("NULL");
                                        continue Label_0405;
                                    }
                                    ASN1Primitive asn1Primitive2;
                                    if (nextElement2 instanceof ASN1Primitive) {
                                        asn1Primitive2 = nextElement2;
                                    }
                                    else {
                                        asn1Primitive2 = nextElement2.toASN1Primitive();
                                    }
                                    _dumpAsString(string3, b, asn1Primitive2, sb);
                                }
                                return;
                            }
                        }
                        sb5.append(lineSeparator);
                        str = sb5.toString();
                    }
                    sb.append(str);
                    return;
                }
                final StringBuilder sb26 = new StringBuilder();
                sb26.append(str);
                sb26.append("Encoding: ");
                sb26.append(asn1External.getEncoding());
                sb26.append(lineSeparator);
                sb.append(sb26.toString());
                externalContent = asn1External.getExternalContent();
                try {
                    _dumpAsString(str, b, externalContent, sb);
                    return;
                    final StringBuilder sb27 = new StringBuilder();
                    sb27.append(str);
                    sb27.append(externalContent.toString());
                    continue;
                }
                finally {}
                break;
            }
        }
    }
    
    private static String calculateAscString(final byte[] array, final int n, final int n2) {
        final StringBuffer sb = new StringBuffer();
        for (int i = n; i != n + n2; ++i) {
            if (array[i] >= 32 && array[i] <= 126) {
                sb.append((char)array[i]);
            }
        }
        return sb.toString();
    }
    
    public static String dumpAsString(final Object o) {
        return dumpAsString(o, false);
    }
    
    public static String dumpAsString(final Object o, final boolean b) {
        final StringBuffer sb = new StringBuffer();
        ASN1Primitive asn1Primitive;
        if (o instanceof ASN1Primitive) {
            asn1Primitive = (ASN1Primitive)o;
        }
        else {
            if (!(o instanceof ASN1Encodable)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("unknown object type ");
                sb2.append(o.toString());
                return sb2.toString();
            }
            asn1Primitive = ((ASN1Encodable)o).toASN1Primitive();
        }
        _dumpAsString("", b, asn1Primitive, sb);
        return sb.toString();
    }
    
    private static String dumpBinaryDataAsString(String s, final byte[] array) {
        final String lineSeparator = Strings.lineSeparator();
        final StringBuffer sb = new StringBuffer();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(s);
        sb2.append("    ");
        final String string = sb2.toString();
        sb.append(lineSeparator);
        for (int i = 0; i < array.length; i += 32) {
            final int length = array.length;
            sb.append(string);
            if (length - i > 32) {
                sb.append(Strings.fromByteArray(Hex.encode(array, i, 32)));
                sb.append("    ");
                s = calculateAscString(array, i, 32);
            }
            else {
                sb.append(Strings.fromByteArray(Hex.encode(array, i, array.length - i)));
                for (int j = array.length - i; j != 32; ++j) {
                    sb.append("  ");
                }
                sb.append("    ");
                s = calculateAscString(array, i, array.length - i);
            }
            sb.append(s);
            sb.append(lineSeparator);
        }
        return sb.toString();
    }
    
    private static String outputApplicationSpecific(final String s, final String str, final boolean b, ASN1Primitive asn1Primitive, final String s2) {
        final ASN1ApplicationSpecific instance = ASN1ApplicationSpecific.getInstance(asn1Primitive);
        asn1Primitive = (ASN1Primitive)new StringBuffer();
        if (instance.isConstructed()) {
            try {
                final ASN1Sequence instance2 = ASN1Sequence.getInstance(instance.getObject(16));
                final StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(s);
                sb.append(" ApplicationSpecific[");
                sb.append(instance.getApplicationTag());
                sb.append("]");
                sb.append(s2);
                ((StringBuffer)asn1Primitive).append(sb.toString());
                final Enumeration objects = instance2.getObjects();
                while (objects.hasMoreElements()) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(str);
                    sb2.append("    ");
                    _dumpAsString(sb2.toString(), b, objects.nextElement(), (StringBuffer)asn1Primitive);
                }
            }
            catch (IOException obj) {
                ((StringBuffer)asn1Primitive).append(obj);
            }
            return ((StringBuffer)asn1Primitive).toString();
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(str);
        sb3.append(s);
        sb3.append(" ApplicationSpecific[");
        sb3.append(instance.getApplicationTag());
        sb3.append("] (");
        sb3.append(Strings.fromByteArray(Hex.encode(instance.getContents())));
        sb3.append(")");
        sb3.append(s2);
        return sb3.toString();
    }
}
