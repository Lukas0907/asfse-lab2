// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.util;

import org.bouncycastle.asn1.ASN1Primitive;
import java.io.InputStream;
import org.bouncycastle.asn1.ASN1InputStream;
import java.io.FileInputStream;

public class Dump
{
    public static void main(final String[] array) throws Exception {
        final ASN1InputStream asn1InputStream = new ASN1InputStream(new FileInputStream(array[0]));
        while (true) {
            final ASN1Primitive object = asn1InputStream.readObject();
            if (object == null) {
                break;
            }
            System.out.println(ASN1Dump.dumpAsString(object));
        }
    }
}
