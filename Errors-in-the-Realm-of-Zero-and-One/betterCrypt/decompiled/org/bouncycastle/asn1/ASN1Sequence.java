// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Enumeration;
import java.io.IOException;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.util.Iterable;

public abstract class ASN1Sequence extends ASN1Primitive implements Iterable<ASN1Encodable>
{
    ASN1Encodable[] elements;
    
    protected ASN1Sequence() {
        this.elements = ASN1EncodableVector.EMPTY_ELEMENTS;
    }
    
    protected ASN1Sequence(final ASN1Encodable asn1Encodable) {
        if (asn1Encodable != null) {
            this.elements = new ASN1Encodable[] { asn1Encodable };
            return;
        }
        throw new NullPointerException("'element' cannot be null");
    }
    
    protected ASN1Sequence(final ASN1EncodableVector asn1EncodableVector) {
        if (asn1EncodableVector != null) {
            this.elements = asn1EncodableVector.takeElements();
            return;
        }
        throw new NullPointerException("'elementVector' cannot be null");
    }
    
    protected ASN1Sequence(final ASN1Encodable[] array) {
        if (!Arrays.isNullOrContainsNull(array)) {
            this.elements = ASN1EncodableVector.cloneElements(array);
            return;
        }
        throw new NullPointerException("'elements' cannot be null, or contain null");
    }
    
    ASN1Sequence(final ASN1Encodable[] array, final boolean b) {
        ASN1Encodable[] cloneElements = array;
        if (b) {
            cloneElements = ASN1EncodableVector.cloneElements(array);
        }
        this.elements = cloneElements;
    }
    
    public static ASN1Sequence getInstance(final Object o) {
        if (o == null || o instanceof ASN1Sequence) {
            return (ASN1Sequence)o;
        }
        if (o instanceof ASN1SequenceParser) {
            return getInstance(((ASN1SequenceParser)o).toASN1Primitive());
        }
        if (o instanceof byte[]) {
            try {
                return getInstance(ASN1Primitive.fromByteArray((byte[])o));
            }
            catch (IOException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("failed to construct sequence from byte[]: ");
                sb.append(ex.getMessage());
                throw new IllegalArgumentException(sb.toString());
            }
        }
        if (o instanceof ASN1Encodable) {
            final ASN1Primitive asn1Primitive = ((ASN1Encodable)o).toASN1Primitive();
            if (asn1Primitive instanceof ASN1Sequence) {
                return (ASN1Sequence)asn1Primitive;
            }
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("unknown object in getInstance: ");
        sb2.append(o.getClass().getName());
        throw new IllegalArgumentException(sb2.toString());
    }
    
    public static ASN1Sequence getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        if (b) {
            if (asn1TaggedObject.isExplicit()) {
                return getInstance(asn1TaggedObject.getObject());
            }
            throw new IllegalArgumentException("object implicit - explicit expected.");
        }
        else {
            final ASN1Primitive object = asn1TaggedObject.getObject();
            if (asn1TaggedObject.isExplicit()) {
                if (asn1TaggedObject instanceof BERTaggedObject) {
                    return new BERSequence(object);
                }
                return new DLSequence(object);
            }
            else {
                if (!(object instanceof ASN1Sequence)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("unknown object in getInstance: ");
                    sb.append(asn1TaggedObject.getClass().getName());
                    throw new IllegalArgumentException(sb.toString());
                }
                final ASN1Sequence asn1Sequence = (ASN1Sequence)object;
                if (asn1TaggedObject instanceof BERTaggedObject) {
                    return asn1Sequence;
                }
                return (ASN1Sequence)asn1Sequence.toDLObject();
            }
        }
    }
    
    @Override
    boolean asn1Equals(final ASN1Primitive asn1Primitive) {
        if (!(asn1Primitive instanceof ASN1Sequence)) {
            return false;
        }
        final ASN1Sequence asn1Sequence = (ASN1Sequence)asn1Primitive;
        final int size = this.size();
        if (asn1Sequence.size() != size) {
            return false;
        }
        for (int i = 0; i < size; ++i) {
            final ASN1Primitive asn1Primitive2 = this.elements[i].toASN1Primitive();
            final ASN1Primitive asn1Primitive3 = asn1Sequence.elements[i].toASN1Primitive();
            if (asn1Primitive2 != asn1Primitive3 && !asn1Primitive2.asn1Equals(asn1Primitive3)) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    abstract void encode(final ASN1OutputStream p0, final boolean p1) throws IOException;
    
    public ASN1Encodable getObjectAt(final int n) {
        return this.elements[n];
    }
    
    public Enumeration getObjects() {
        return new Enumeration() {
            private int pos = 0;
            
            @Override
            public boolean hasMoreElements() {
                return this.pos < ASN1Sequence.this.elements.length;
            }
            
            @Override
            public Object nextElement() {
                if (this.pos < ASN1Sequence.this.elements.length) {
                    return ASN1Sequence.this.elements[this.pos++];
                }
                throw new NoSuchElementException("ASN1Sequence Enumeration");
            }
        };
    }
    
    @Override
    public int hashCode() {
        int length = this.elements.length;
        int n = length + 1;
        while (true) {
            --length;
            if (length < 0) {
                break;
            }
            n = (n * 257 ^ this.elements[length].toASN1Primitive().hashCode());
        }
        return n;
    }
    
    @Override
    boolean isConstructed() {
        return true;
    }
    
    @Override
    public Iterator<ASN1Encodable> iterator() {
        return new Arrays.Iterator<ASN1Encodable>(this.elements);
    }
    
    public ASN1SequenceParser parser() {
        return new ASN1SequenceParser() {
            private int pos = 0;
            final /* synthetic */ int val$count = ASN1Sequence.this.size();
            
            @Override
            public ASN1Primitive getLoadedObject() {
                return ASN1Sequence.this;
            }
            
            @Override
            public ASN1Encodable readObject() throws IOException {
                if (this.val$count == this.pos) {
                    return null;
                }
                final ASN1Encodable asn1Encodable = ASN1Sequence.this.elements[this.pos++];
                if (asn1Encodable instanceof ASN1Sequence) {
                    return ((ASN1Sequence)asn1Encodable).parser();
                }
                Object parser = asn1Encodable;
                if (asn1Encodable instanceof ASN1Set) {
                    parser = ((ASN1Set)asn1Encodable).parser();
                }
                return (ASN1Encodable)parser;
            }
            
            @Override
            public ASN1Primitive toASN1Primitive() {
                return ASN1Sequence.this;
            }
        };
    }
    
    public int size() {
        return this.elements.length;
    }
    
    public ASN1Encodable[] toArray() {
        return ASN1EncodableVector.cloneElements(this.elements);
    }
    
    ASN1Encodable[] toArrayInternal() {
        return this.elements;
    }
    
    @Override
    ASN1Primitive toDERObject() {
        return new DERSequence(this.elements, false);
    }
    
    @Override
    ASN1Primitive toDLObject() {
        return new DLSequence(this.elements, false);
    }
    
    @Override
    public String toString() {
        final int size = this.size();
        if (size == 0) {
            return "[]";
        }
        final StringBuffer sb = new StringBuffer();
        sb.append('[');
        int n = 0;
        while (true) {
            sb.append(this.elements[n]);
            ++n;
            if (n >= size) {
                break;
            }
            sb.append(", ");
        }
        sb.append(']');
        return sb.toString();
    }
}
