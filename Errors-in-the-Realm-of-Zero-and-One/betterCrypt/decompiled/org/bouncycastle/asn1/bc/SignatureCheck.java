// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.bc;

import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1BitString;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Object;

public class SignatureCheck extends ASN1Object
{
    private final ASN1Sequence certificates;
    private final AlgorithmIdentifier signatureAlgorithm;
    private final ASN1BitString signatureValue;
    
    private SignatureCheck(final ASN1Sequence asn1Sequence) {
        this.signatureAlgorithm = AlgorithmIdentifier.getInstance(asn1Sequence.getObjectAt(0));
        int n = 1;
        if (asn1Sequence.getObjectAt(1) instanceof ASN1TaggedObject) {
            this.certificates = ASN1Sequence.getInstance(ASN1TaggedObject.getInstance(asn1Sequence.getObjectAt(1)).getObject());
            n = 2;
        }
        else {
            this.certificates = null;
        }
        this.signatureValue = DERBitString.getInstance(asn1Sequence.getObjectAt(n));
    }
    
    public SignatureCheck(final AlgorithmIdentifier signatureAlgorithm, final byte[] array) {
        this.signatureAlgorithm = signatureAlgorithm;
        this.certificates = null;
        this.signatureValue = new DERBitString(Arrays.clone(array));
    }
    
    public SignatureCheck(final AlgorithmIdentifier signatureAlgorithm, final Certificate[] array, final byte[] array2) {
        this.signatureAlgorithm = signatureAlgorithm;
        this.certificates = new DERSequence(array);
        this.signatureValue = new DERBitString(Arrays.clone(array2));
    }
    
    public static SignatureCheck getInstance(final Object o) {
        if (o instanceof SignatureCheck) {
            return (SignatureCheck)o;
        }
        if (o != null) {
            return new SignatureCheck(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public Certificate[] getCertificates() {
        final ASN1Sequence certificates = this.certificates;
        if (certificates == null) {
            return null;
        }
        final Certificate[] array = new Certificate[certificates.size()];
        for (int i = 0; i != array.length; ++i) {
            array[i] = Certificate.getInstance(this.certificates.getObjectAt(i));
        }
        return array;
    }
    
    public ASN1BitString getSignature() {
        return new DERBitString(this.signatureValue.getBytes(), this.signatureValue.getPadBits());
    }
    
    public AlgorithmIdentifier getSignatureAlgorithm() {
        return this.signatureAlgorithm;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(3);
        asn1EncodableVector.add(this.signatureAlgorithm);
        final ASN1Sequence certificates = this.certificates;
        if (certificates != null) {
            asn1EncodableVector.add(new DERTaggedObject(0, certificates));
        }
        asn1EncodableVector.add(this.signatureValue);
        return new DERSequence(asn1EncodableVector);
    }
}
