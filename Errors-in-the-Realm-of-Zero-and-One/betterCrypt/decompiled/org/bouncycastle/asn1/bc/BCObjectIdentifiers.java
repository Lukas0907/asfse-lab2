// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.bc;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;

public interface BCObjectIdentifiers
{
    public static final ASN1ObjectIdentifier bc = new ASN1ObjectIdentifier("1.3.6.1.4.1.22554");
    public static final ASN1ObjectIdentifier bc_exch = BCObjectIdentifiers.bc.branch("3");
    public static final ASN1ObjectIdentifier bc_ext = BCObjectIdentifiers.bc.branch("4");
    public static final ASN1ObjectIdentifier bc_pbe = BCObjectIdentifiers.bc.branch("1");
    public static final ASN1ObjectIdentifier bc_pbe_sha1 = BCObjectIdentifiers.bc_pbe.branch("1");
    public static final ASN1ObjectIdentifier bc_pbe_sha1_pkcs12 = BCObjectIdentifiers.bc_pbe_sha1.branch("2");
    public static final ASN1ObjectIdentifier bc_pbe_sha1_pkcs12_aes128_cbc = BCObjectIdentifiers.bc_pbe_sha1_pkcs12.branch("1.2");
    public static final ASN1ObjectIdentifier bc_pbe_sha1_pkcs12_aes192_cbc = BCObjectIdentifiers.bc_pbe_sha1_pkcs12.branch("1.22");
    public static final ASN1ObjectIdentifier bc_pbe_sha1_pkcs12_aes256_cbc = BCObjectIdentifiers.bc_pbe_sha1_pkcs12.branch("1.42");
    public static final ASN1ObjectIdentifier bc_pbe_sha1_pkcs5 = BCObjectIdentifiers.bc_pbe_sha1.branch("1");
    public static final ASN1ObjectIdentifier bc_pbe_sha224 = BCObjectIdentifiers.bc_pbe.branch("2.4");
    public static final ASN1ObjectIdentifier bc_pbe_sha256 = BCObjectIdentifiers.bc_pbe.branch("2.1");
    public static final ASN1ObjectIdentifier bc_pbe_sha256_pkcs12 = BCObjectIdentifiers.bc_pbe_sha256.branch("2");
    public static final ASN1ObjectIdentifier bc_pbe_sha256_pkcs12_aes128_cbc = BCObjectIdentifiers.bc_pbe_sha256_pkcs12.branch("1.2");
    public static final ASN1ObjectIdentifier bc_pbe_sha256_pkcs12_aes192_cbc = BCObjectIdentifiers.bc_pbe_sha256_pkcs12.branch("1.22");
    public static final ASN1ObjectIdentifier bc_pbe_sha256_pkcs12_aes256_cbc = BCObjectIdentifiers.bc_pbe_sha256_pkcs12.branch("1.42");
    public static final ASN1ObjectIdentifier bc_pbe_sha256_pkcs5 = BCObjectIdentifiers.bc_pbe_sha256.branch("1");
    public static final ASN1ObjectIdentifier bc_pbe_sha384 = BCObjectIdentifiers.bc_pbe.branch("2.2");
    public static final ASN1ObjectIdentifier bc_pbe_sha512 = BCObjectIdentifiers.bc_pbe.branch("2.3");
    public static final ASN1ObjectIdentifier bc_sig = BCObjectIdentifiers.bc.branch("2");
    public static final ASN1ObjectIdentifier linkedCertificate = BCObjectIdentifiers.bc_ext.branch("1");
    public static final ASN1ObjectIdentifier newHope = BCObjectIdentifiers.bc_exch.branch("1");
    public static final ASN1ObjectIdentifier qTESLA = BCObjectIdentifiers.bc_sig.branch("4");
    public static final ASN1ObjectIdentifier qTESLA_Rnd1_I = BCObjectIdentifiers.qTESLA.branch("1");
    public static final ASN1ObjectIdentifier qTESLA_Rnd1_III_size = BCObjectIdentifiers.qTESLA.branch("2");
    public static final ASN1ObjectIdentifier qTESLA_Rnd1_III_speed = BCObjectIdentifiers.qTESLA.branch("3");
    public static final ASN1ObjectIdentifier qTESLA_Rnd1_p_I = BCObjectIdentifiers.qTESLA.branch("4");
    public static final ASN1ObjectIdentifier qTESLA_Rnd1_p_III = BCObjectIdentifiers.qTESLA.branch("5");
    public static final ASN1ObjectIdentifier qTESLA_p_I = BCObjectIdentifiers.qTESLA.branch("11");
    public static final ASN1ObjectIdentifier qTESLA_p_III = BCObjectIdentifiers.qTESLA.branch("12");
    public static final ASN1ObjectIdentifier sphincs256 = BCObjectIdentifiers.bc_sig.branch("1");
    public static final ASN1ObjectIdentifier sphincs256_with_BLAKE512 = BCObjectIdentifiers.sphincs256.branch("1");
    public static final ASN1ObjectIdentifier sphincs256_with_SHA3_512 = BCObjectIdentifiers.sphincs256.branch("3");
    public static final ASN1ObjectIdentifier sphincs256_with_SHA512 = BCObjectIdentifiers.sphincs256.branch("2");
    public static final ASN1ObjectIdentifier xmss = BCObjectIdentifiers.bc_sig.branch("2");
    public static final ASN1ObjectIdentifier xmss_SHA256 = BCObjectIdentifiers.xmss.branch("5");
    public static final ASN1ObjectIdentifier xmss_SHA256ph = BCObjectIdentifiers.xmss.branch("1");
    public static final ASN1ObjectIdentifier xmss_SHA512 = BCObjectIdentifiers.xmss.branch("6");
    public static final ASN1ObjectIdentifier xmss_SHA512ph = BCObjectIdentifiers.xmss.branch("2");
    public static final ASN1ObjectIdentifier xmss_SHAKE128 = BCObjectIdentifiers.xmss.branch("7");
    public static final ASN1ObjectIdentifier xmss_SHAKE128ph = BCObjectIdentifiers.xmss.branch("3");
    public static final ASN1ObjectIdentifier xmss_SHAKE256 = BCObjectIdentifiers.xmss.branch("8");
    public static final ASN1ObjectIdentifier xmss_SHAKE256ph = BCObjectIdentifiers.xmss.branch("4");
    public static final ASN1ObjectIdentifier xmss_mt = BCObjectIdentifiers.bc_sig.branch("3");
    public static final ASN1ObjectIdentifier xmss_mt_SHA256 = BCObjectIdentifiers.xmss_mt.branch("5");
    public static final ASN1ObjectIdentifier xmss_mt_SHA256ph = BCObjectIdentifiers.xmss_mt.branch("1");
    public static final ASN1ObjectIdentifier xmss_mt_SHA512 = BCObjectIdentifiers.xmss_mt.branch("6");
    public static final ASN1ObjectIdentifier xmss_mt_SHA512ph = BCObjectIdentifiers.xmss_mt.branch("2");
    public static final ASN1ObjectIdentifier xmss_mt_SHAKE128 = BCObjectIdentifiers.xmss_mt.branch("7");
    public static final ASN1ObjectIdentifier xmss_mt_SHAKE128ph = BCObjectIdentifiers.xmss_mt.branch("3");
    public static final ASN1ObjectIdentifier xmss_mt_SHAKE256 = BCObjectIdentifiers.xmss_mt.branch("8");
    public static final ASN1ObjectIdentifier xmss_mt_SHAKE256ph = BCObjectIdentifiers.xmss_mt.branch("4");
    public static final ASN1ObjectIdentifier xmss_mt_with_SHA256 = BCObjectIdentifiers.xmss_mt_SHA256ph;
    public static final ASN1ObjectIdentifier xmss_mt_with_SHA512 = BCObjectIdentifiers.xmss_mt_SHA512ph;
    public static final ASN1ObjectIdentifier xmss_mt_with_SHAKE128 = BCObjectIdentifiers.xmss_mt_SHAKE128;
    public static final ASN1ObjectIdentifier xmss_mt_with_SHAKE256 = BCObjectIdentifiers.xmss_mt_SHAKE256;
    public static final ASN1ObjectIdentifier xmss_with_SHA256 = BCObjectIdentifiers.xmss_SHA256ph;
    public static final ASN1ObjectIdentifier xmss_with_SHA512 = BCObjectIdentifiers.xmss_SHA512ph;
    public static final ASN1ObjectIdentifier xmss_with_SHAKE128 = BCObjectIdentifiers.xmss_SHAKE128ph;
    public static final ASN1ObjectIdentifier xmss_with_SHAKE256 = BCObjectIdentifiers.xmss_SHAKE256ph;
}
