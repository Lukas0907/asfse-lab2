// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.bc;

import java.io.IOException;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Choice;
import org.bouncycastle.asn1.ASN1Object;

public class ObjectStoreIntegrityCheck extends ASN1Object implements ASN1Choice
{
    public static final int PBKD_MAC_CHECK = 0;
    public static final int SIG_CHECK = 1;
    private final ASN1Object integrityCheck;
    private final int type;
    
    private ObjectStoreIntegrityCheck(final ASN1Encodable asn1Encodable) {
        ASN1Object integrityCheck;
        if (!(asn1Encodable instanceof ASN1Sequence) && !(asn1Encodable instanceof PbkdMacIntegrityCheck)) {
            if (!(asn1Encodable instanceof ASN1TaggedObject)) {
                throw new IllegalArgumentException("Unknown check object in integrity check.");
            }
            this.type = 1;
            integrityCheck = SignatureCheck.getInstance(((ASN1TaggedObject)asn1Encodable).getObject());
        }
        else {
            this.type = 0;
            integrityCheck = PbkdMacIntegrityCheck.getInstance(asn1Encodable);
        }
        this.integrityCheck = integrityCheck;
    }
    
    public ObjectStoreIntegrityCheck(final PbkdMacIntegrityCheck pbkdMacIntegrityCheck) {
        this((ASN1Encodable)pbkdMacIntegrityCheck);
    }
    
    public ObjectStoreIntegrityCheck(final SignatureCheck signatureCheck) {
        this(new DERTaggedObject(0, signatureCheck));
    }
    
    public static ObjectStoreIntegrityCheck getInstance(final Object o) {
        if (o instanceof ObjectStoreIntegrityCheck) {
            return (ObjectStoreIntegrityCheck)o;
        }
        Label_0049: {
            if (!(o instanceof byte[])) {
                break Label_0049;
            }
            while (true) {
                while (true) {
                    try {
                        return new ObjectStoreIntegrityCheck(ASN1Primitive.fromByteArray((byte[])o));
                        throw new IllegalArgumentException("Unable to parse integrity check details.");
                        Label_0068: {
                            return null;
                        }
                        // iftrue(Label_0068:, o == null)
                        return new ObjectStoreIntegrityCheck((ASN1Encodable)o);
                    }
                    catch (IOException ex) {}
                    continue;
                }
            }
        }
    }
    
    public ASN1Object getIntegrityCheck() {
        return this.integrityCheck;
    }
    
    public int getType() {
        return this.type;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1Object integrityCheck = this.integrityCheck;
        if (integrityCheck instanceof SignatureCheck) {
            return new DERTaggedObject(0, integrityCheck);
        }
        return integrityCheck.toASN1Primitive();
    }
}
