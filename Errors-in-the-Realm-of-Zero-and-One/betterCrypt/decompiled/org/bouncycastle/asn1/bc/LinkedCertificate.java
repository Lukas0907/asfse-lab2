// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.bc;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x509.DigestInfo;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.ASN1Object;

public class LinkedCertificate extends ASN1Object
{
    private GeneralNames cACerts;
    private X500Name certIssuer;
    private final GeneralName certLocation;
    private final DigestInfo digest;
    
    private LinkedCertificate(final ASN1Sequence asn1Sequence) {
        this.digest = DigestInfo.getInstance(asn1Sequence.getObjectAt(0));
        this.certLocation = GeneralName.getInstance(asn1Sequence.getObjectAt(1));
        final int size = asn1Sequence.size();
        int i = 2;
        if (size > 2) {
            while (i != asn1Sequence.size()) {
                final ASN1TaggedObject instance = ASN1TaggedObject.getInstance(asn1Sequence.getObjectAt(i));
                final int tagNo = instance.getTagNo();
                if (tagNo != 0) {
                    if (tagNo != 1) {
                        throw new IllegalArgumentException("unknown tag in tagged field");
                    }
                    this.cACerts = GeneralNames.getInstance(instance, false);
                }
                else {
                    this.certIssuer = X500Name.getInstance(instance, false);
                }
                ++i;
            }
        }
    }
    
    public LinkedCertificate(final DigestInfo digestInfo, final GeneralName generalName) {
        this(digestInfo, generalName, null, null);
    }
    
    public LinkedCertificate(final DigestInfo digest, final GeneralName certLocation, final X500Name certIssuer, final GeneralNames caCerts) {
        this.digest = digest;
        this.certLocation = certLocation;
        this.certIssuer = certIssuer;
        this.cACerts = caCerts;
    }
    
    public static LinkedCertificate getInstance(final Object o) {
        if (o instanceof LinkedCertificate) {
            return (LinkedCertificate)o;
        }
        if (o != null) {
            return new LinkedCertificate(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public GeneralNames getCACerts() {
        return this.cACerts;
    }
    
    public X500Name getCertIssuer() {
        return this.certIssuer;
    }
    
    public GeneralName getCertLocation() {
        return this.certLocation;
    }
    
    public DigestInfo getDigest() {
        return this.digest;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(4);
        asn1EncodableVector.add(this.digest);
        asn1EncodableVector.add(this.certLocation);
        final X500Name certIssuer = this.certIssuer;
        if (certIssuer != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 0, certIssuer));
        }
        final GeneralNames caCerts = this.cACerts;
        if (caCerts != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 1, caCerts));
        }
        return new DERSequence(asn1EncodableVector);
    }
}
