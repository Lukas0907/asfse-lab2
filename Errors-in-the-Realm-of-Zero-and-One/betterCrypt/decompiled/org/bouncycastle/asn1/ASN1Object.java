// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.bouncycastle.util.Encodable;

public abstract class ASN1Object implements ASN1Encodable, Encodable
{
    protected static boolean hasEncodedTagValue(final Object o, final int n) {
        final boolean b = o instanceof byte[];
        boolean b2 = false;
        if (b) {
            b2 = b2;
            if (((byte[])o)[0] == n) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public void encodeTo(final OutputStream outputStream) throws IOException {
        ASN1OutputStream.create(outputStream).writeObject(this);
    }
    
    public void encodeTo(final OutputStream outputStream, final String s) throws IOException {
        ASN1OutputStream.create(outputStream, s).writeObject(this);
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof ASN1Encodable && this.toASN1Primitive().equals(((ASN1Encodable)o).toASN1Primitive()));
    }
    
    @Override
    public byte[] getEncoded() throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        this.encodeTo(byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    
    public byte[] getEncoded(final String s) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        this.encodeTo(byteArrayOutputStream, s);
        return byteArrayOutputStream.toByteArray();
    }
    
    @Override
    public int hashCode() {
        return this.toASN1Primitive().hashCode();
    }
    
    public ASN1Primitive toASN1Object() {
        return this.toASN1Primitive();
    }
    
    @Override
    public abstract ASN1Primitive toASN1Primitive();
}
