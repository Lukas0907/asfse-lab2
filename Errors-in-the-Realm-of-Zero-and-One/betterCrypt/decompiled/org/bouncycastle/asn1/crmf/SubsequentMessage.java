// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.crmf;

import org.bouncycastle.asn1.ASN1Integer;

public class SubsequentMessage extends ASN1Integer
{
    public static final SubsequentMessage challengeResp;
    public static final SubsequentMessage encrCert;
    
    static {
        encrCert = new SubsequentMessage(0);
        challengeResp = new SubsequentMessage(1);
    }
    
    private SubsequentMessage(final int n) {
        super(n);
    }
    
    public static SubsequentMessage valueOf(final int i) {
        if (i == 0) {
            return SubsequentMessage.encrCert;
        }
        if (i == 1) {
            return SubsequentMessage.challengeResp;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown value: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
}
