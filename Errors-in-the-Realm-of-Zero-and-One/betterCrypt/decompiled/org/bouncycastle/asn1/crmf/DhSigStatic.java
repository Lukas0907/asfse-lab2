// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.crmf;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Object;

public class DhSigStatic extends ASN1Object
{
    private final ASN1OctetString hashValue;
    private final IssuerAndSerialNumber issuerAndSerial;
    
    private DhSigStatic(final ASN1Sequence asn1Sequence) {
        ASN1Encodable asn1Encodable;
        if (asn1Sequence.size() == 1) {
            this.issuerAndSerial = null;
            asn1Encodable = asn1Sequence.getObjectAt(0);
        }
        else {
            if (asn1Sequence.size() != 2) {
                throw new IllegalArgumentException("sequence wrong length for DhSigStatic");
            }
            this.issuerAndSerial = IssuerAndSerialNumber.getInstance(asn1Sequence.getObjectAt(0));
            asn1Encodable = asn1Sequence.getObjectAt(1);
        }
        this.hashValue = ASN1OctetString.getInstance(asn1Encodable);
    }
    
    public DhSigStatic(final IssuerAndSerialNumber issuerAndSerial, final byte[] array) {
        this.issuerAndSerial = issuerAndSerial;
        this.hashValue = new DEROctetString(Arrays.clone(array));
    }
    
    public DhSigStatic(final byte[] array) {
        this(null, array);
    }
    
    public static DhSigStatic getInstance(final Object o) {
        if (o instanceof DhSigStatic) {
            return (DhSigStatic)o;
        }
        if (o != null) {
            return new DhSigStatic(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public byte[] getHashValue() {
        return Arrays.clone(this.hashValue.getOctets());
    }
    
    public IssuerAndSerialNumber getIssuerAndSerial() {
        return this.issuerAndSerial;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(2);
        final IssuerAndSerialNumber issuerAndSerial = this.issuerAndSerial;
        if (issuerAndSerial != null) {
            asn1EncodableVector.add(issuerAndSerial);
        }
        asn1EncodableVector.add(this.hashValue);
        return new DERSequence(asn1EncodableVector);
    }
}
