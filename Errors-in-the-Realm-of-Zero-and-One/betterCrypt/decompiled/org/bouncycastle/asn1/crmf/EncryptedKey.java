// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.crmf;

import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.cms.EnvelopedData;
import org.bouncycastle.asn1.ASN1Choice;
import org.bouncycastle.asn1.ASN1Object;

public class EncryptedKey extends ASN1Object implements ASN1Choice
{
    private EncryptedValue encryptedValue;
    private EnvelopedData envelopedData;
    
    public EncryptedKey(final EnvelopedData envelopedData) {
        this.envelopedData = envelopedData;
    }
    
    public EncryptedKey(final EncryptedValue encryptedValue) {
        this.encryptedValue = encryptedValue;
    }
    
    public static EncryptedKey getInstance(final Object o) {
        if (o instanceof EncryptedKey) {
            return (EncryptedKey)o;
        }
        if (o instanceof ASN1TaggedObject) {
            return new EncryptedKey(EnvelopedData.getInstance((ASN1TaggedObject)o, false));
        }
        if (o instanceof EncryptedValue) {
            return new EncryptedKey((EncryptedValue)o);
        }
        return new EncryptedKey(EncryptedValue.getInstance(o));
    }
    
    public ASN1Encodable getValue() {
        final EncryptedValue encryptedValue = this.encryptedValue;
        if (encryptedValue != null) {
            return encryptedValue;
        }
        return this.envelopedData;
    }
    
    public boolean isEncryptedValue() {
        return this.encryptedValue != null;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final EncryptedValue encryptedValue = this.encryptedValue;
        if (encryptedValue != null) {
            return encryptedValue.toASN1Primitive();
        }
        return new DERTaggedObject(false, 0, this.envelopedData);
    }
}
