// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.crmf;

import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERSequence;
import java.math.BigInteger;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Object;

public class PKIPublicationInfo extends ASN1Object
{
    public static final ASN1Integer dontPublish;
    public static final ASN1Integer pleasePublish;
    private ASN1Integer action;
    private ASN1Sequence pubInfos;
    
    static {
        dontPublish = new ASN1Integer(0L);
        pleasePublish = new ASN1Integer(1L);
    }
    
    public PKIPublicationInfo(final BigInteger bigInteger) {
        this(new ASN1Integer(bigInteger));
    }
    
    public PKIPublicationInfo(final ASN1Integer action) {
        this.action = action;
    }
    
    private PKIPublicationInfo(final ASN1Sequence asn1Sequence) {
        this.action = ASN1Integer.getInstance(asn1Sequence.getObjectAt(0));
        if (asn1Sequence.size() > 1) {
            this.pubInfos = ASN1Sequence.getInstance(asn1Sequence.getObjectAt(1));
        }
    }
    
    public PKIPublicationInfo(final SinglePubInfo singlePubInfo) {
        SinglePubInfo[] array;
        if (singlePubInfo != null) {
            array = new SinglePubInfo[] { singlePubInfo };
        }
        else {
            array = null;
        }
        this(array);
    }
    
    public PKIPublicationInfo(final SinglePubInfo[] array) {
        this.action = PKIPublicationInfo.pleasePublish;
        if (array != null) {
            this.pubInfos = new DERSequence(array);
            return;
        }
        this.pubInfos = null;
    }
    
    public static PKIPublicationInfo getInstance(final Object o) {
        if (o instanceof PKIPublicationInfo) {
            return (PKIPublicationInfo)o;
        }
        if (o != null) {
            return new PKIPublicationInfo(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public ASN1Integer getAction() {
        return this.action;
    }
    
    public SinglePubInfo[] getPubInfos() {
        final ASN1Sequence pubInfos = this.pubInfos;
        if (pubInfos == null) {
            return null;
        }
        final SinglePubInfo[] array = new SinglePubInfo[pubInfos.size()];
        for (int i = 0; i != array.length; ++i) {
            array[i] = SinglePubInfo.getInstance(this.pubInfos.getObjectAt(i));
        }
        return array;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(2);
        asn1EncodableVector.add(this.action);
        final ASN1Sequence pubInfos = this.pubInfos;
        if (pubInfos != null) {
            asn1EncodableVector.add(pubInfos);
        }
        return new DERSequence(asn1EncodableVector);
    }
}
