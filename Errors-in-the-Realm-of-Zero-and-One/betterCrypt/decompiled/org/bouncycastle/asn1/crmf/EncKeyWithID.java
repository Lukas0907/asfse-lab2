// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.crmf;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.DERUTF8String;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Object;

public class EncKeyWithID extends ASN1Object
{
    private final ASN1Encodable identifier;
    private final PrivateKeyInfo privKeyInfo;
    
    private EncKeyWithID(final ASN1Sequence asn1Sequence) {
        this.privKeyInfo = PrivateKeyInfo.getInstance(asn1Sequence.getObjectAt(0));
        ASN1Encodable identifier;
        if (asn1Sequence.size() > 1) {
            final boolean b = asn1Sequence.getObjectAt(1) instanceof DERUTF8String;
            identifier = asn1Sequence.getObjectAt(1);
            if (!b) {
                identifier = GeneralName.getInstance(identifier);
            }
        }
        else {
            identifier = null;
        }
        this.identifier = identifier;
    }
    
    public EncKeyWithID(final PrivateKeyInfo privKeyInfo) {
        this.privKeyInfo = privKeyInfo;
        this.identifier = null;
    }
    
    public EncKeyWithID(final PrivateKeyInfo privKeyInfo, final DERUTF8String identifier) {
        this.privKeyInfo = privKeyInfo;
        this.identifier = identifier;
    }
    
    public EncKeyWithID(final PrivateKeyInfo privKeyInfo, final GeneralName identifier) {
        this.privKeyInfo = privKeyInfo;
        this.identifier = identifier;
    }
    
    public static EncKeyWithID getInstance(final Object o) {
        if (o instanceof EncKeyWithID) {
            return (EncKeyWithID)o;
        }
        if (o != null) {
            return new EncKeyWithID(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public ASN1Encodable getIdentifier() {
        return this.identifier;
    }
    
    public PrivateKeyInfo getPrivateKey() {
        return this.privKeyInfo;
    }
    
    public boolean hasIdentifier() {
        return this.identifier != null;
    }
    
    public boolean isIdentifierUTF8String() {
        return this.identifier instanceof DERUTF8String;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(2);
        asn1EncodableVector.add(this.privKeyInfo);
        final ASN1Encodable identifier = this.identifier;
        if (identifier != null) {
            asn1EncodableVector.add(identifier);
        }
        return new DERSequence(asn1EncodableVector);
    }
}
