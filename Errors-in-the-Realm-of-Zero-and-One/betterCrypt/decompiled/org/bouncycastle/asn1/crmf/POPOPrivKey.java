// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.crmf;

import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.cms.EnvelopedData;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Choice;
import org.bouncycastle.asn1.ASN1Object;

public class POPOPrivKey extends ASN1Object implements ASN1Choice
{
    public static final int agreeMAC = 3;
    public static final int dhMAC = 2;
    public static final int encryptedKey = 4;
    public static final int subsequentMessage = 1;
    public static final int thisMessage = 0;
    private ASN1Encodable obj;
    private int tagNo;
    
    private POPOPrivKey(final ASN1TaggedObject asn1TaggedObject) {
        this.tagNo = asn1TaggedObject.getTagNo();
        final int tagNo = this.tagNo;
        ASN1Object obj = null;
        Label_0090: {
            if (tagNo != 0) {
                if (tagNo == 1) {
                    obj = SubsequentMessage.valueOf(ASN1Integer.getInstance(asn1TaggedObject, false).intValueExact());
                    break Label_0090;
                }
                if (tagNo != 2) {
                    if (tagNo == 3) {
                        obj = PKMACValue.getInstance(asn1TaggedObject, false);
                        break Label_0090;
                    }
                    if (tagNo == 4) {
                        obj = EnvelopedData.getInstance(asn1TaggedObject, false);
                        break Label_0090;
                    }
                    throw new IllegalArgumentException("unknown tag in POPOPrivKey");
                }
            }
            obj = DERBitString.getInstance(asn1TaggedObject, false);
        }
        this.obj = obj;
    }
    
    public POPOPrivKey(final PKMACValue obj) {
        this.tagNo = 3;
        this.obj = obj;
    }
    
    public POPOPrivKey(final SubsequentMessage obj) {
        this.tagNo = 1;
        this.obj = obj;
    }
    
    public static POPOPrivKey getInstance(final Object o) {
        if (o instanceof POPOPrivKey) {
            return (POPOPrivKey)o;
        }
        if (o != null) {
            return new POPOPrivKey(ASN1TaggedObject.getInstance(o));
        }
        return null;
    }
    
    public static POPOPrivKey getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        return getInstance(ASN1TaggedObject.getInstance(asn1TaggedObject, b));
    }
    
    public int getType() {
        return this.tagNo;
    }
    
    public ASN1Encodable getValue() {
        return this.obj;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        return new DERTaggedObject(false, this.tagNo, this.obj);
    }
}
