// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.crmf;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Object;

public class SinglePubInfo extends ASN1Object
{
    public static final ASN1Integer dontCare;
    public static final ASN1Integer ldap;
    public static final ASN1Integer web;
    public static final ASN1Integer x500;
    private GeneralName pubLocation;
    private ASN1Integer pubMethod;
    
    static {
        dontCare = new ASN1Integer(0L);
        x500 = new ASN1Integer(1L);
        web = new ASN1Integer(2L);
        ldap = new ASN1Integer(3L);
    }
    
    public SinglePubInfo(final ASN1Integer pubMethod, final GeneralName pubLocation) {
        this.pubMethod = pubMethod;
        this.pubLocation = pubLocation;
    }
    
    private SinglePubInfo(final ASN1Sequence asn1Sequence) {
        this.pubMethod = ASN1Integer.getInstance(asn1Sequence.getObjectAt(0));
        if (asn1Sequence.size() == 2) {
            this.pubLocation = GeneralName.getInstance(asn1Sequence.getObjectAt(1));
        }
    }
    
    public static SinglePubInfo getInstance(final Object o) {
        if (o instanceof SinglePubInfo) {
            return (SinglePubInfo)o;
        }
        if (o != null) {
            return new SinglePubInfo(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public GeneralName getPubLocation() {
        return this.pubLocation;
    }
    
    public ASN1Integer getPubMethod() {
        return this.pubMethod;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(2);
        asn1EncodableVector.add(this.pubMethod);
        final GeneralName pubLocation = this.pubLocation;
        if (pubLocation != null) {
            asn1EncodableVector.add(pubLocation);
        }
        return new DERSequence(asn1EncodableVector);
    }
}
