// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.crmf;

import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Object;

public class CertReqMessages extends ASN1Object
{
    private ASN1Sequence content;
    
    private CertReqMessages(final ASN1Sequence content) {
        this.content = content;
    }
    
    public CertReqMessages(final CertReqMsg certReqMsg) {
        this.content = new DERSequence(certReqMsg);
    }
    
    public CertReqMessages(final CertReqMsg[] array) {
        this.content = new DERSequence(array);
    }
    
    public static CertReqMessages getInstance(final Object o) {
        if (o instanceof CertReqMessages) {
            return (CertReqMessages)o;
        }
        if (o != null) {
            return new CertReqMessages(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        return this.content;
    }
    
    public CertReqMsg[] toCertReqMsgArray() {
        final CertReqMsg[] array = new CertReqMsg[this.content.size()];
        for (int i = 0; i != array.length; ++i) {
            array[i] = CertReqMsg.getInstance(this.content.getObjectAt(i));
        }
        return array;
    }
}
