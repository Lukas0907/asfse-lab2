// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.icao;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.ASN1Object;

public class LDSSecurityObject extends ASN1Object implements ICAOObjectIdentifiers
{
    public static final int ub_DataGroups = 16;
    private DataGroupHash[] datagroupHash;
    private AlgorithmIdentifier digestAlgorithmIdentifier;
    private ASN1Integer version;
    private LDSVersionInfo versionInfo;
    
    private LDSSecurityObject(final ASN1Sequence asn1Sequence) {
        this.version = new ASN1Integer(0L);
        if (asn1Sequence != null && asn1Sequence.size() != 0) {
            final Enumeration objects = asn1Sequence.getObjects();
            this.version = ASN1Integer.getInstance(objects.nextElement());
            this.digestAlgorithmIdentifier = AlgorithmIdentifier.getInstance(objects.nextElement());
            final ASN1Sequence instance = ASN1Sequence.getInstance(objects.nextElement());
            if (this.version.intValueExact() == 1) {
                this.versionInfo = LDSVersionInfo.getInstance(objects.nextElement());
            }
            this.checkDatagroupHashSeqSize(instance.size());
            this.datagroupHash = new DataGroupHash[instance.size()];
            for (int i = 0; i < instance.size(); ++i) {
                this.datagroupHash[i] = DataGroupHash.getInstance(instance.getObjectAt(i));
            }
            return;
        }
        throw new IllegalArgumentException("null or empty sequence passed.");
    }
    
    public LDSSecurityObject(final AlgorithmIdentifier digestAlgorithmIdentifier, final DataGroupHash[] array) {
        this.version = new ASN1Integer(0L);
        this.version = new ASN1Integer(0L);
        this.digestAlgorithmIdentifier = digestAlgorithmIdentifier;
        this.datagroupHash = this.copy(array);
        this.checkDatagroupHashSeqSize(array.length);
    }
    
    public LDSSecurityObject(final AlgorithmIdentifier digestAlgorithmIdentifier, final DataGroupHash[] array, final LDSVersionInfo versionInfo) {
        this.version = new ASN1Integer(0L);
        this.version = new ASN1Integer(1L);
        this.digestAlgorithmIdentifier = digestAlgorithmIdentifier;
        this.datagroupHash = this.copy(array);
        this.versionInfo = versionInfo;
        this.checkDatagroupHashSeqSize(array.length);
    }
    
    private void checkDatagroupHashSeqSize(final int n) {
        if (n >= 2 && n <= 16) {
            return;
        }
        throw new IllegalArgumentException("wrong size in DataGroupHashValues : not in (2..16)");
    }
    
    private DataGroupHash[] copy(final DataGroupHash[] array) {
        final DataGroupHash[] array2 = new DataGroupHash[array.length];
        System.arraycopy(array, 0, array2, 0, array2.length);
        return array2;
    }
    
    public static LDSSecurityObject getInstance(final Object o) {
        if (o instanceof LDSSecurityObject) {
            return (LDSSecurityObject)o;
        }
        if (o != null) {
            return new LDSSecurityObject(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public DataGroupHash[] getDatagroupHash() {
        return this.copy(this.datagroupHash);
    }
    
    public AlgorithmIdentifier getDigestAlgorithmIdentifier() {
        return this.digestAlgorithmIdentifier;
    }
    
    public int getVersion() {
        return this.version.intValueExact();
    }
    
    public LDSVersionInfo getVersionInfo() {
        return this.versionInfo;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(4);
        asn1EncodableVector.add(this.version);
        asn1EncodableVector.add(this.digestAlgorithmIdentifier);
        asn1EncodableVector.add(new DERSequence(this.datagroupHash));
        final LDSVersionInfo versionInfo = this.versionInfo;
        if (versionInfo != null) {
            asn1EncodableVector.add(versionInfo);
        }
        return new DERSequence(asn1EncodableVector);
    }
}
