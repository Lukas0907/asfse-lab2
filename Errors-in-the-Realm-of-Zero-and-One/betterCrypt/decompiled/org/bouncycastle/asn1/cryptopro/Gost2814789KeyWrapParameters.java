// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.cryptopro;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Object;

public class Gost2814789KeyWrapParameters extends ASN1Object
{
    private final ASN1ObjectIdentifier encryptionParamSet;
    private final byte[] ukm;
    
    public Gost2814789KeyWrapParameters(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        this(asn1ObjectIdentifier, null);
    }
    
    public Gost2814789KeyWrapParameters(final ASN1ObjectIdentifier encryptionParamSet, final byte[] array) {
        this.encryptionParamSet = encryptionParamSet;
        this.ukm = Arrays.clone(array);
    }
    
    private Gost2814789KeyWrapParameters(final ASN1Sequence asn1Sequence) {
        if (asn1Sequence.size() == 2) {
            this.encryptionParamSet = ASN1ObjectIdentifier.getInstance(asn1Sequence.getObjectAt(0));
            this.ukm = ASN1OctetString.getInstance(asn1Sequence.getObjectAt(1)).getOctets();
            return;
        }
        if (asn1Sequence.size() == 1) {
            this.encryptionParamSet = ASN1ObjectIdentifier.getInstance(asn1Sequence.getObjectAt(0));
            this.ukm = null;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown sequence length: ");
        sb.append(asn1Sequence.size());
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static Gost2814789KeyWrapParameters getInstance(final Object o) {
        if (o instanceof Gost2814789KeyWrapParameters) {
            return (Gost2814789KeyWrapParameters)o;
        }
        if (o != null) {
            return new Gost2814789KeyWrapParameters(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public ASN1ObjectIdentifier getEncryptionParamSet() {
        return this.encryptionParamSet;
    }
    
    public byte[] getUkm() {
        return Arrays.clone(this.ukm);
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(2);
        asn1EncodableVector.add(this.encryptionParamSet);
        final byte[] ukm = this.ukm;
        if (ukm != null) {
            asn1EncodableVector.add(new DEROctetString(ukm));
        }
        return new DERSequence(asn1EncodableVector);
    }
}
