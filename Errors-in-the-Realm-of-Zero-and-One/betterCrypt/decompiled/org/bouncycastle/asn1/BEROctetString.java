// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.util.Enumeration;
import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.util.Vector;

public class BEROctetString extends ASN1OctetString
{
    private static final int DEFAULT_LENGTH = 1000;
    private final int chunkSize;
    private final ASN1OctetString[] octs;
    
    public BEROctetString(final byte[] array) {
        this(array, 1000);
    }
    
    public BEROctetString(final byte[] array, final int n) {
        this(array, null, n);
    }
    
    private BEROctetString(final byte[] array, final ASN1OctetString[] octs, final int chunkSize) {
        super(array);
        this.octs = octs;
        this.chunkSize = chunkSize;
    }
    
    public BEROctetString(final ASN1OctetString[] array) {
        this(array, 1000);
    }
    
    public BEROctetString(final ASN1OctetString[] array, final int n) {
        this(toBytes(array), array, n);
    }
    
    static BEROctetString fromSequence(final ASN1Sequence asn1Sequence) {
        final int size = asn1Sequence.size();
        final ASN1OctetString[] array = new ASN1OctetString[size];
        for (int i = 0; i < size; ++i) {
            array[i] = ASN1OctetString.getInstance(asn1Sequence.getObjectAt(i));
        }
        return new BEROctetString(array);
    }
    
    private Vector generateOcts() {
        final Vector<DEROctetString> vector = new Vector<DEROctetString>();
        for (int i = 0; i < this.string.length; i += this.chunkSize) {
            int length;
            if (this.chunkSize + i > this.string.length) {
                length = this.string.length;
            }
            else {
                length = this.chunkSize + i;
            }
            final byte[] array = new byte[length - i];
            System.arraycopy(this.string, i, array, 0, array.length);
            vector.addElement(new DEROctetString(array));
        }
        return vector;
    }
    
    private static byte[] toBytes(final ASN1OctetString[] array) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 0;
        while (i != array.length) {
            try {
                byteArrayOutputStream.write(((DEROctetString)array[i]).getOctets());
                ++i;
                continue;
            }
            catch (IOException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("exception converting octets ");
                sb.append(ex.toString());
                throw new IllegalArgumentException(sb.toString());
            }
            catch (ClassCastException ex2) {}
            goto Label_0073;
        }
        goto Label_0114;
    }
    
    public void encode(final ASN1OutputStream asn1OutputStream) throws IOException {
        asn1OutputStream.writeObject(this);
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream, final boolean b) throws IOException {
        asn1OutputStream.writeEncodedIndef(b, 36, this.getObjects());
    }
    
    @Override
    int encodedLength() throws IOException {
        final Enumeration objects = this.getObjects();
        int n = 0;
        while (objects.hasMoreElements()) {
            n += objects.nextElement().toASN1Primitive().encodedLength();
        }
        return n + 2 + 2;
    }
    
    public Enumeration getObjects() {
        if (this.octs == null) {
            return this.generateOcts().elements();
        }
        return new Enumeration() {
            int counter = 0;
            
            @Override
            public boolean hasMoreElements() {
                return this.counter < BEROctetString.this.octs.length;
            }
            
            @Override
            public Object nextElement() {
                return BEROctetString.this.octs[this.counter++];
            }
        };
    }
    
    @Override
    public byte[] getOctets() {
        return this.string;
    }
    
    @Override
    boolean isConstructed() {
        return true;
    }
}
