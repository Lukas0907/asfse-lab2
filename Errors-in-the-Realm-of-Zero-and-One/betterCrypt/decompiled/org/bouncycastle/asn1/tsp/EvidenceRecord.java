// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.tsp;

import org.bouncycastle.asn1.ASN1Primitive;
import java.util.Enumeration;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Object;

public class EvidenceRecord extends ASN1Object
{
    private static final ASN1ObjectIdentifier OID;
    private ArchiveTimeStampSequence archiveTimeStampSequence;
    private CryptoInfos cryptoInfos;
    private ASN1Sequence digestAlgorithms;
    private EncryptionInfo encryptionInfo;
    private ASN1Integer version;
    
    static {
        OID = new ASN1ObjectIdentifier("1.3.6.1.5.5.11.0.2.1");
    }
    
    private EvidenceRecord(final ASN1Sequence asn1Sequence) {
        this.version = new ASN1Integer(1L);
        if (asn1Sequence.size() < 3 && asn1Sequence.size() > 5) {
            final StringBuilder sb = new StringBuilder();
            sb.append("wrong sequence size in constructor: ");
            sb.append(asn1Sequence.size());
            throw new IllegalArgumentException(sb.toString());
        }
        final ASN1Integer instance = ASN1Integer.getInstance(asn1Sequence.getObjectAt(0));
        if (instance.intValueExact() == 1) {
            this.version = instance;
            this.digestAlgorithms = ASN1Sequence.getInstance(asn1Sequence.getObjectAt(1));
            for (int i = 2; i != asn1Sequence.size() - 1; ++i) {
                final ASN1Encodable object = asn1Sequence.getObjectAt(i);
                if (!(object instanceof ASN1TaggedObject)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("unknown object in getInstance: ");
                    sb2.append(((ASN1TaggedObject)object).getClass().getName());
                    throw new IllegalArgumentException(sb2.toString());
                }
                final ASN1TaggedObject asn1TaggedObject = (ASN1TaggedObject)object;
                final int tagNo = asn1TaggedObject.getTagNo();
                if (tagNo != 0) {
                    if (tagNo != 1) {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("unknown tag in getInstance: ");
                        sb3.append(asn1TaggedObject.getTagNo());
                        throw new IllegalArgumentException(sb3.toString());
                    }
                    this.encryptionInfo = EncryptionInfo.getInstance(asn1TaggedObject, false);
                }
                else {
                    this.cryptoInfos = CryptoInfos.getInstance(asn1TaggedObject, false);
                }
            }
            this.archiveTimeStampSequence = ArchiveTimeStampSequence.getInstance(asn1Sequence.getObjectAt(asn1Sequence.size() - 1));
            return;
        }
        throw new IllegalArgumentException("incompatible version");
    }
    
    private EvidenceRecord(final EvidenceRecord evidenceRecord, final ArchiveTimeStampSequence archiveTimeStampSequence, final ArchiveTimeStamp archiveTimeStamp) {
        this.version = new ASN1Integer(1L);
        this.version = evidenceRecord.version;
        ASN1Sequence digestAlgorithms = null;
        Label_0128: {
            Label_0123: {
                if (archiveTimeStamp != null) {
                    final AlgorithmIdentifier digestAlgorithmIdentifier = archiveTimeStamp.getDigestAlgorithmIdentifier();
                    final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
                    final Enumeration objects = evidenceRecord.digestAlgorithms.getObjects();
                    final int n = 0;
                    while (true) {
                        AlgorithmIdentifier instance;
                        do {
                            final int n2 = n;
                            if (objects.hasMoreElements()) {
                                instance = AlgorithmIdentifier.getInstance(objects.nextElement());
                                asn1EncodableVector.add(instance);
                            }
                            else {
                                if (n2 == 0) {
                                    asn1EncodableVector.add(digestAlgorithmIdentifier);
                                    digestAlgorithms = new DERSequence(asn1EncodableVector);
                                    break Label_0128;
                                }
                                break Label_0123;
                            }
                        } while (!instance.equals(digestAlgorithmIdentifier));
                        final int n2 = 1;
                        continue;
                    }
                }
            }
            digestAlgorithms = evidenceRecord.digestAlgorithms;
        }
        this.digestAlgorithms = digestAlgorithms;
        this.cryptoInfos = evidenceRecord.cryptoInfos;
        this.encryptionInfo = evidenceRecord.encryptionInfo;
        this.archiveTimeStampSequence = archiveTimeStampSequence;
    }
    
    public EvidenceRecord(final AlgorithmIdentifier[] array, final CryptoInfos cryptoInfos, final EncryptionInfo encryptionInfo, final ArchiveTimeStampSequence archiveTimeStampSequence) {
        this.version = new ASN1Integer(1L);
        this.digestAlgorithms = new DERSequence(array);
        this.cryptoInfos = cryptoInfos;
        this.encryptionInfo = encryptionInfo;
        this.archiveTimeStampSequence = archiveTimeStampSequence;
    }
    
    public static EvidenceRecord getInstance(final Object o) {
        if (o instanceof EvidenceRecord) {
            return (EvidenceRecord)o;
        }
        if (o != null) {
            return new EvidenceRecord(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public static EvidenceRecord getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        return getInstance(ASN1Sequence.getInstance(asn1TaggedObject, b));
    }
    
    public EvidenceRecord addArchiveTimeStamp(final ArchiveTimeStamp archiveTimeStamp, final boolean b) {
        if (b) {
            return new EvidenceRecord(this, this.archiveTimeStampSequence.append(new ArchiveTimeStampChain(archiveTimeStamp)), archiveTimeStamp);
        }
        final ArchiveTimeStampChain[] archiveTimeStampChains = this.archiveTimeStampSequence.getArchiveTimeStampChains();
        archiveTimeStampChains[archiveTimeStampChains.length - 1] = archiveTimeStampChains[archiveTimeStampChains.length - 1].append(archiveTimeStamp);
        return new EvidenceRecord(this, new ArchiveTimeStampSequence(archiveTimeStampChains), null);
    }
    
    public ArchiveTimeStampSequence getArchiveTimeStampSequence() {
        return this.archiveTimeStampSequence;
    }
    
    public AlgorithmIdentifier[] getDigestAlgorithms() {
        final AlgorithmIdentifier[] array = new AlgorithmIdentifier[this.digestAlgorithms.size()];
        for (int i = 0; i != array.length; ++i) {
            array[i] = AlgorithmIdentifier.getInstance(this.digestAlgorithms.getObjectAt(i));
        }
        return array;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(5);
        asn1EncodableVector.add(this.version);
        asn1EncodableVector.add(this.digestAlgorithms);
        final CryptoInfos cryptoInfos = this.cryptoInfos;
        if (cryptoInfos != null) {
            asn1EncodableVector.add(cryptoInfos);
        }
        final EncryptionInfo encryptionInfo = this.encryptionInfo;
        if (encryptionInfo != null) {
            asn1EncodableVector.add(encryptionInfo);
        }
        asn1EncodableVector.add(this.archiveTimeStampSequence);
        return new DERSequence(asn1EncodableVector);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("EvidenceRecord: Oid(");
        sb.append(EvidenceRecord.OID);
        sb.append(")");
        return sb.toString();
    }
}
