// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.tsp;

import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Object;

public class CryptoInfos extends ASN1Object
{
    private ASN1Sequence attributes;
    
    private CryptoInfos(final ASN1Sequence attributes) {
        this.attributes = attributes;
    }
    
    public CryptoInfos(final Attribute[] array) {
        this.attributes = new DERSequence(array);
    }
    
    public static CryptoInfos getInstance(final Object o) {
        if (o instanceof CryptoInfos) {
            return (CryptoInfos)o;
        }
        if (o != null) {
            return new CryptoInfos(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public static CryptoInfos getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        return getInstance(ASN1Sequence.getInstance(asn1TaggedObject, b));
    }
    
    public Attribute[] getAttributes() {
        final Attribute[] array = new Attribute[this.attributes.size()];
        for (int i = 0; i != array.length; ++i) {
            array[i] = Attribute.getInstance(this.attributes.getObjectAt(i));
        }
        return array;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        return this.attributes;
    }
}
