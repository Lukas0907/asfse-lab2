// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.tsp;

import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.cms.SignedData;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.cms.Attributes;
import org.bouncycastle.asn1.ASN1Object;

public class ArchiveTimeStamp extends ASN1Object
{
    private Attributes attributes;
    private AlgorithmIdentifier digestAlgorithm;
    private ASN1Sequence reducedHashTree;
    private ContentInfo timeStamp;
    
    private ArchiveTimeStamp(final ASN1Sequence asn1Sequence) {
        if (asn1Sequence.size() >= 1 && asn1Sequence.size() <= 4) {
            for (int i = 0; i < asn1Sequence.size() - 1; ++i) {
                final ASN1Encodable object = asn1Sequence.getObjectAt(i);
                if (object instanceof ASN1TaggedObject) {
                    final ASN1TaggedObject instance = ASN1TaggedObject.getInstance(object);
                    final int tagNo = instance.getTagNo();
                    if (tagNo != 0) {
                        if (tagNo != 1) {
                            if (tagNo != 2) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("invalid tag no in constructor: ");
                                sb.append(instance.getTagNo());
                                throw new IllegalArgumentException(sb.toString());
                            }
                            this.reducedHashTree = ASN1Sequence.getInstance(instance, false);
                        }
                        else {
                            this.attributes = Attributes.getInstance(instance, false);
                        }
                    }
                    else {
                        this.digestAlgorithm = AlgorithmIdentifier.getInstance(instance, false);
                    }
                }
            }
            this.timeStamp = ContentInfo.getInstance(asn1Sequence.getObjectAt(asn1Sequence.size() - 1));
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("wrong sequence size in constructor: ");
        sb2.append(asn1Sequence.size());
        throw new IllegalArgumentException(sb2.toString());
    }
    
    public ArchiveTimeStamp(final ContentInfo timeStamp) {
        this.timeStamp = timeStamp;
    }
    
    public ArchiveTimeStamp(final AlgorithmIdentifier digestAlgorithm, final Attributes attributes, final PartialHashtree[] array, final ContentInfo timeStamp) {
        this.digestAlgorithm = digestAlgorithm;
        this.attributes = attributes;
        this.reducedHashTree = new DERSequence(array);
        this.timeStamp = timeStamp;
    }
    
    public ArchiveTimeStamp(final AlgorithmIdentifier digestAlgorithm, final PartialHashtree[] array, final ContentInfo timeStamp) {
        this.digestAlgorithm = digestAlgorithm;
        this.reducedHashTree = new DERSequence(array);
        this.timeStamp = timeStamp;
    }
    
    public static ArchiveTimeStamp getInstance(final Object o) {
        if (o instanceof ArchiveTimeStamp) {
            return (ArchiveTimeStamp)o;
        }
        if (o != null) {
            return new ArchiveTimeStamp(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public AlgorithmIdentifier getDigestAlgorithm() {
        return this.digestAlgorithm;
    }
    
    public AlgorithmIdentifier getDigestAlgorithmIdentifier() {
        final AlgorithmIdentifier digestAlgorithm = this.digestAlgorithm;
        if (digestAlgorithm != null) {
            return digestAlgorithm;
        }
        if (!this.timeStamp.getContentType().equals(CMSObjectIdentifiers.signedData)) {
            throw new IllegalStateException("cannot identify algorithm identifier for digest");
        }
        final SignedData instance = SignedData.getInstance(this.timeStamp.getContent());
        if (instance.getEncapContentInfo().getContentType().equals(PKCSObjectIdentifiers.id_ct_TSTInfo)) {
            return TSTInfo.getInstance(instance.getEncapContentInfo()).getMessageImprint().getHashAlgorithm();
        }
        throw new IllegalStateException("cannot parse time stamp");
    }
    
    public PartialHashtree[] getReducedHashTree() {
        final ASN1Sequence reducedHashTree = this.reducedHashTree;
        if (reducedHashTree == null) {
            return null;
        }
        final PartialHashtree[] array = new PartialHashtree[reducedHashTree.size()];
        for (int i = 0; i != array.length; ++i) {
            array[i] = PartialHashtree.getInstance(this.reducedHashTree.getObjectAt(i));
        }
        return array;
    }
    
    public ContentInfo getTimeStamp() {
        return this.timeStamp;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(4);
        final AlgorithmIdentifier digestAlgorithm = this.digestAlgorithm;
        if (digestAlgorithm != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 0, digestAlgorithm));
        }
        final Attributes attributes = this.attributes;
        if (attributes != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 1, attributes));
        }
        final ASN1Sequence reducedHashTree = this.reducedHashTree;
        if (reducedHashTree != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 2, reducedHashTree));
        }
        asn1EncodableVector.add(this.timeStamp);
        return new DERSequence(asn1EncodableVector);
    }
}
