// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.tsp;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Object;

public class Accuracy extends ASN1Object
{
    protected static final int MAX_MICROS = 999;
    protected static final int MAX_MILLIS = 999;
    protected static final int MIN_MICROS = 1;
    protected static final int MIN_MILLIS = 1;
    ASN1Integer micros;
    ASN1Integer millis;
    ASN1Integer seconds;
    
    protected Accuracy() {
    }
    
    public Accuracy(final ASN1Integer seconds, final ASN1Integer millis, final ASN1Integer micros) {
        if (millis != null) {
            final int intValueExact = millis.intValueExact();
            if (intValueExact < 1 || intValueExact > 999) {
                throw new IllegalArgumentException("Invalid millis field : not in (1..999)");
            }
        }
        if (micros != null) {
            final int intValueExact2 = micros.intValueExact();
            if (intValueExact2 < 1 || intValueExact2 > 999) {
                throw new IllegalArgumentException("Invalid micros field : not in (1..999)");
            }
        }
        this.seconds = seconds;
        this.millis = millis;
        this.micros = micros;
    }
    
    private Accuracy(final ASN1Sequence asn1Sequence) {
        this.seconds = null;
        this.millis = null;
        this.micros = null;
        for (int i = 0; i < asn1Sequence.size(); ++i) {
            if (asn1Sequence.getObjectAt(i) instanceof ASN1Integer) {
                this.seconds = (ASN1Integer)asn1Sequence.getObjectAt(i);
            }
            else if (asn1Sequence.getObjectAt(i) instanceof ASN1TaggedObject) {
                final ASN1TaggedObject asn1TaggedObject = (ASN1TaggedObject)asn1Sequence.getObjectAt(i);
                final int tagNo = asn1TaggedObject.getTagNo();
                if (tagNo != 0) {
                    if (tagNo != 1) {
                        throw new IllegalArgumentException("Invalid tag number");
                    }
                    this.micros = ASN1Integer.getInstance(asn1TaggedObject, false);
                    final int intValueExact = this.micros.intValueExact();
                    if (intValueExact < 1 || intValueExact > 999) {
                        throw new IllegalArgumentException("Invalid micros field : not in (1..999)");
                    }
                }
                else {
                    this.millis = ASN1Integer.getInstance(asn1TaggedObject, false);
                    final int intValueExact2 = this.millis.intValueExact();
                    if (intValueExact2 < 1 || intValueExact2 > 999) {
                        throw new IllegalArgumentException("Invalid millis field : not in (1..999)");
                    }
                }
            }
        }
    }
    
    public static Accuracy getInstance(final Object o) {
        if (o instanceof Accuracy) {
            return (Accuracy)o;
        }
        if (o != null) {
            return new Accuracy(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public ASN1Integer getMicros() {
        return this.micros;
    }
    
    public ASN1Integer getMillis() {
        return this.millis;
    }
    
    public ASN1Integer getSeconds() {
        return this.seconds;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(3);
        final ASN1Integer seconds = this.seconds;
        if (seconds != null) {
            asn1EncodableVector.add(seconds);
        }
        final ASN1Integer millis = this.millis;
        if (millis != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 0, millis));
        }
        final ASN1Integer micros = this.micros;
        if (micros != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 1, micros));
        }
        return new DERSequence(asn1EncodableVector);
    }
}
