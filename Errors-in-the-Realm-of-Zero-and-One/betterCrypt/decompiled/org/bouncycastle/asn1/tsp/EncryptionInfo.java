// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.tsp;

import org.bouncycastle.asn1.DLSequence;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Object;

public class EncryptionInfo extends ASN1Object
{
    private ASN1ObjectIdentifier encryptionInfoType;
    private ASN1Encodable encryptionInfoValue;
    
    public EncryptionInfo(final ASN1ObjectIdentifier encryptionInfoType, final ASN1Encodable encryptionInfoValue) {
        this.encryptionInfoType = encryptionInfoType;
        this.encryptionInfoValue = encryptionInfoValue;
    }
    
    private EncryptionInfo(final ASN1Sequence asn1Sequence) {
        if (asn1Sequence.size() == 2) {
            this.encryptionInfoType = ASN1ObjectIdentifier.getInstance(asn1Sequence.getObjectAt(0));
            this.encryptionInfoValue = asn1Sequence.getObjectAt(1);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("wrong sequence size in constructor: ");
        sb.append(asn1Sequence.size());
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static EncryptionInfo getInstance(final ASN1Object asn1Object) {
        if (asn1Object instanceof EncryptionInfo) {
            return (EncryptionInfo)asn1Object;
        }
        if (asn1Object != null) {
            return new EncryptionInfo(ASN1Sequence.getInstance(asn1Object));
        }
        return null;
    }
    
    public static EncryptionInfo getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        return getInstance(ASN1Sequence.getInstance(asn1TaggedObject, b));
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(2);
        asn1EncodableVector.add(this.encryptionInfoType);
        asn1EncodableVector.add(this.encryptionInfoValue);
        return new DLSequence(asn1EncodableVector);
    }
}
