// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.tsp;

import org.bouncycastle.asn1.ASN1Primitive;
import java.util.Enumeration;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Object;

public class ArchiveTimeStampSequence extends ASN1Object
{
    private ASN1Sequence archiveTimeStampChains;
    
    private ArchiveTimeStampSequence(final ASN1Sequence asn1Sequence) throws IllegalArgumentException {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(asn1Sequence.size());
        final Enumeration objects = asn1Sequence.getObjects();
        while (objects.hasMoreElements()) {
            asn1EncodableVector.add(ArchiveTimeStampChain.getInstance(objects.nextElement()));
        }
        this.archiveTimeStampChains = new DERSequence(asn1EncodableVector);
    }
    
    public ArchiveTimeStampSequence(final ArchiveTimeStampChain archiveTimeStampChain) {
        this.archiveTimeStampChains = new DERSequence(archiveTimeStampChain);
    }
    
    public ArchiveTimeStampSequence(final ArchiveTimeStampChain[] array) {
        this.archiveTimeStampChains = new DERSequence(array);
    }
    
    public static ArchiveTimeStampSequence getInstance(final Object o) {
        if (o instanceof ArchiveTimeStampChain) {
            return (ArchiveTimeStampSequence)o;
        }
        if (o != null) {
            return new ArchiveTimeStampSequence(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public ArchiveTimeStampSequence append(final ArchiveTimeStampChain archiveTimeStampChain) {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(this.archiveTimeStampChains.size() + 1);
        for (int i = 0; i != this.archiveTimeStampChains.size(); ++i) {
            asn1EncodableVector.add(this.archiveTimeStampChains.getObjectAt(i));
        }
        asn1EncodableVector.add(archiveTimeStampChain);
        return new ArchiveTimeStampSequence(new DERSequence(asn1EncodableVector));
    }
    
    public ArchiveTimeStampChain[] getArchiveTimeStampChains() {
        final ArchiveTimeStampChain[] array = new ArchiveTimeStampChain[this.archiveTimeStampChains.size()];
        for (int i = 0; i != array.length; ++i) {
            array[i] = ArchiveTimeStampChain.getInstance(this.archiveTimeStampChains.getObjectAt(i));
        }
        return array;
    }
    
    public int size() {
        return this.archiveTimeStampChains.size();
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        return this.archiveTimeStampChains;
    }
}
