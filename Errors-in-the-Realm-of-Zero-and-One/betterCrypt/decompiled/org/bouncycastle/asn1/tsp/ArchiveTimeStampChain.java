// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.tsp;

import org.bouncycastle.asn1.ASN1Primitive;
import java.util.Enumeration;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Object;

public class ArchiveTimeStampChain extends ASN1Object
{
    private ASN1Sequence archiveTimestamps;
    
    private ArchiveTimeStampChain(final ASN1Sequence asn1Sequence) {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(asn1Sequence.size());
        final Enumeration objects = asn1Sequence.getObjects();
        while (objects.hasMoreElements()) {
            asn1EncodableVector.add(ArchiveTimeStamp.getInstance(objects.nextElement()));
        }
        this.archiveTimestamps = new DERSequence(asn1EncodableVector);
    }
    
    public ArchiveTimeStampChain(final ArchiveTimeStamp archiveTimeStamp) {
        this.archiveTimestamps = new DERSequence(archiveTimeStamp);
    }
    
    public ArchiveTimeStampChain(final ArchiveTimeStamp[] array) {
        this.archiveTimestamps = new DERSequence(array);
    }
    
    public static ArchiveTimeStampChain getInstance(final Object o) {
        if (o instanceof ArchiveTimeStampChain) {
            return (ArchiveTimeStampChain)o;
        }
        if (o != null) {
            return new ArchiveTimeStampChain(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public ArchiveTimeStampChain append(final ArchiveTimeStamp archiveTimeStamp) {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(this.archiveTimestamps.size() + 1);
        for (int i = 0; i != this.archiveTimestamps.size(); ++i) {
            asn1EncodableVector.add(this.archiveTimestamps.getObjectAt(i));
        }
        asn1EncodableVector.add(archiveTimeStamp);
        return new ArchiveTimeStampChain(new DERSequence(asn1EncodableVector));
    }
    
    public ArchiveTimeStamp[] getArchiveTimestamps() {
        final ArchiveTimeStamp[] array = new ArchiveTimeStamp[this.archiveTimestamps.size()];
        for (int i = 0; i != array.length; ++i) {
            array[i] = ArchiveTimeStamp.getInstance(this.archiveTimestamps.getObjectAt(i));
        }
        return array;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        return this.archiveTimestamps;
    }
}
