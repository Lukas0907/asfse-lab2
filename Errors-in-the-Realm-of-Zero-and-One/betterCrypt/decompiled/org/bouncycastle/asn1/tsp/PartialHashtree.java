// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.tsp;

import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Object;

public class PartialHashtree extends ASN1Object
{
    private ASN1Sequence values;
    
    private PartialHashtree(final ASN1Sequence values) {
        for (int i = 0; i != values.size(); ++i) {
            if (!(values.getObjectAt(i) instanceof DEROctetString)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("unknown object in constructor: ");
                sb.append(values.getObjectAt(i).getClass().getName());
                throw new IllegalArgumentException(sb.toString());
            }
        }
        this.values = values;
    }
    
    public PartialHashtree(final byte[] array) {
        this(new byte[][] { array });
    }
    
    public PartialHashtree(final byte[][] array) {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(array.length);
        for (int i = 0; i != array.length; ++i) {
            asn1EncodableVector.add(new DEROctetString(Arrays.clone(array[i])));
        }
        this.values = new DERSequence(asn1EncodableVector);
    }
    
    public static PartialHashtree getInstance(final Object o) {
        if (o instanceof PartialHashtree) {
            return (PartialHashtree)o;
        }
        if (o != null) {
            return new PartialHashtree(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public byte[][] getValues() {
        final byte[][] array = new byte[this.values.size()][];
        for (int i = 0; i != array.length; ++i) {
            array[i] = Arrays.clone(ASN1OctetString.getInstance(this.values.getObjectAt(i)).getOctets());
        }
        return array;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        return this.values;
    }
}
