// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;
import org.bouncycastle.util.Arrays;

public class DERBMPString extends ASN1Primitive implements ASN1String
{
    private final char[] string;
    
    public DERBMPString(final String s) {
        this.string = s.toCharArray();
    }
    
    DERBMPString(final byte[] array) {
        final char[] string = new char[array.length / 2];
        for (int i = 0; i != string.length; ++i) {
            final int n = i * 2;
            string[i] = (char)((array[n + 1] & 0xFF) | array[n] << 8);
        }
        this.string = string;
    }
    
    DERBMPString(final char[] string) {
        this.string = string;
    }
    
    public static DERBMPString getInstance(final Object o) {
        if (o != null && !(o instanceof DERBMPString)) {
            if (o instanceof byte[]) {
                try {
                    return (DERBMPString)ASN1Primitive.fromByteArray((byte[])o);
                }
                catch (Exception ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("encoding error in getInstance: ");
                    sb.append(ex.toString());
                    throw new IllegalArgumentException(sb.toString());
                }
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("illegal object in getInstance: ");
            sb2.append(o.getClass().getName());
            throw new IllegalArgumentException(sb2.toString());
        }
        return (DERBMPString)o;
    }
    
    public static DERBMPString getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        final ASN1Primitive object = asn1TaggedObject.getObject();
        if (!b && !(object instanceof DERBMPString)) {
            return new DERBMPString(ASN1OctetString.getInstance(object).getOctets());
        }
        return getInstance(object);
    }
    
    protected boolean asn1Equals(final ASN1Primitive asn1Primitive) {
        return asn1Primitive instanceof DERBMPString && Arrays.areEqual(this.string, ((DERBMPString)asn1Primitive).string);
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream, final boolean b) throws IOException {
        final int length = this.string.length;
        if (b) {
            asn1OutputStream.write(30);
        }
        asn1OutputStream.writeLength(length * 2);
        final byte[] array = new byte[8];
        int i = 0;
        while (i < (length & 0xFFFFFFFC)) {
            final char[] string = this.string;
            final char c = string[i];
            final char c2 = string[i + 1];
            final char c3 = string[i + 2];
            final char c4 = string[i + 3];
            i += 4;
            array[0] = (byte)(c >> 8);
            array[1] = (byte)c;
            array[2] = (byte)(c2 >> 8);
            array[3] = (byte)c2;
            array[4] = (byte)(c3 >> 8);
            array[5] = (byte)c3;
            array[6] = (byte)(c4 >> 8);
            array[7] = (byte)c4;
            asn1OutputStream.write(array, 0, 8);
        }
        if (i < length) {
            int n = 0;
            int j;
            int n3;
            do {
                final char c5 = this.string[i];
                j = i + 1;
                final int n2 = n + 1;
                array[n] = (byte)(c5 >> 8);
                n3 = n2 + 1;
                array[n2] = (byte)c5;
                i = j;
                n = n3;
            } while (j < length);
            asn1OutputStream.write(array, 0, n3);
        }
    }
    
    @Override
    int encodedLength() {
        return StreamUtil.calculateBodyLength(this.string.length * 2) + 1 + this.string.length * 2;
    }
    
    @Override
    public String getString() {
        return new String(this.string);
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.string);
    }
    
    @Override
    boolean isConstructed() {
        return false;
    }
    
    @Override
    public String toString() {
        return this.getString();
    }
}
