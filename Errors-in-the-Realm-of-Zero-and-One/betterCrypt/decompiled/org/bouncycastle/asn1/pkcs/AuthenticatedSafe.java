// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.pkcs;

import org.bouncycastle.asn1.DLSequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.BERSequence;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Object;

public class AuthenticatedSafe extends ASN1Object
{
    private ContentInfo[] info;
    private boolean isBer;
    
    private AuthenticatedSafe(final ASN1Sequence asn1Sequence) {
        this.isBer = true;
        this.info = new ContentInfo[asn1Sequence.size()];
        int n = 0;
        while (true) {
            final ContentInfo[] info = this.info;
            if (n == info.length) {
                break;
            }
            info[n] = ContentInfo.getInstance(asn1Sequence.getObjectAt(n));
            ++n;
        }
        this.isBer = (asn1Sequence instanceof BERSequence);
    }
    
    public AuthenticatedSafe(final ContentInfo[] array) {
        this.isBer = true;
        this.info = this.copy(array);
    }
    
    private ContentInfo[] copy(final ContentInfo[] array) {
        final ContentInfo[] array2 = new ContentInfo[array.length];
        System.arraycopy(array, 0, array2, 0, array2.length);
        return array2;
    }
    
    public static AuthenticatedSafe getInstance(final Object o) {
        if (o instanceof AuthenticatedSafe) {
            return (AuthenticatedSafe)o;
        }
        if (o != null) {
            return new AuthenticatedSafe(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public ContentInfo[] getContentInfo() {
        return this.copy(this.info);
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        if (this.isBer) {
            return new BERSequence(this.info);
        }
        return new DLSequence(this.info);
    }
}
