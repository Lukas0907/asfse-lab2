// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.pkcs;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import java.math.BigInteger;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.util.BigIntegers;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1Encodable;
import java.util.Enumeration;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1BitString;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.ASN1Object;

public class PrivateKeyInfo extends ASN1Object
{
    private ASN1Set attributes;
    private ASN1OctetString privateKey;
    private AlgorithmIdentifier privateKeyAlgorithm;
    private ASN1BitString publicKey;
    private ASN1Integer version;
    
    private PrivateKeyInfo(final ASN1Sequence asn1Sequence) {
        final Enumeration objects = asn1Sequence.getObjects();
        this.version = ASN1Integer.getInstance(objects.nextElement());
        final int versionValue = getVersionValue(this.version);
        this.privateKeyAlgorithm = AlgorithmIdentifier.getInstance(objects.nextElement());
        this.privateKey = ASN1OctetString.getInstance(objects.nextElement());
        int n = -1;
        while (objects.hasMoreElements()) {
            final ASN1TaggedObject asn1TaggedObject = objects.nextElement();
            final int tagNo = asn1TaggedObject.getTagNo();
            if (tagNo <= n) {
                throw new IllegalArgumentException("invalid optional field in private key info");
            }
            if (tagNo != 0) {
                if (tagNo != 1) {
                    throw new IllegalArgumentException("unknown optional field in private key info");
                }
                if (versionValue < 1) {
                    throw new IllegalArgumentException("'publicKey' requires version v2(1) or later");
                }
                this.publicKey = DERBitString.getInstance(asn1TaggedObject, false);
            }
            else {
                this.attributes = ASN1Set.getInstance(asn1TaggedObject, false);
            }
            n = tagNo;
        }
    }
    
    public PrivateKeyInfo(final AlgorithmIdentifier algorithmIdentifier, final ASN1Encodable asn1Encodable) throws IOException {
        this(algorithmIdentifier, asn1Encodable, null, null);
    }
    
    public PrivateKeyInfo(final AlgorithmIdentifier algorithmIdentifier, final ASN1Encodable asn1Encodable, final ASN1Set set) throws IOException {
        this(algorithmIdentifier, asn1Encodable, set, null);
    }
    
    public PrivateKeyInfo(final AlgorithmIdentifier privateKeyAlgorithm, final ASN1Encodable asn1Encodable, final ASN1Set attributes, final byte[] array) throws IOException {
        BigInteger bigInteger;
        if (array != null) {
            bigInteger = BigIntegers.ONE;
        }
        else {
            bigInteger = BigIntegers.ZERO;
        }
        this.version = new ASN1Integer(bigInteger);
        this.privateKeyAlgorithm = privateKeyAlgorithm;
        this.privateKey = new DEROctetString(asn1Encodable);
        this.attributes = attributes;
        ASN1BitString publicKey;
        if (array == null) {
            publicKey = null;
        }
        else {
            publicKey = new DERBitString(array);
        }
        this.publicKey = publicKey;
    }
    
    public static PrivateKeyInfo getInstance(final Object o) {
        if (o instanceof PrivateKeyInfo) {
            return (PrivateKeyInfo)o;
        }
        if (o != null) {
            return new PrivateKeyInfo(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public static PrivateKeyInfo getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        return getInstance(ASN1Sequence.getInstance(asn1TaggedObject, b));
    }
    
    private static int getVersionValue(final ASN1Integer asn1Integer) {
        final int intValueExact = asn1Integer.intValueExact();
        if (intValueExact >= 0 && intValueExact <= 1) {
            return intValueExact;
        }
        throw new IllegalArgumentException("invalid version for private key info");
    }
    
    public ASN1Set getAttributes() {
        return this.attributes;
    }
    
    public AlgorithmIdentifier getPrivateKeyAlgorithm() {
        return this.privateKeyAlgorithm;
    }
    
    public ASN1BitString getPublicKeyData() {
        return this.publicKey;
    }
    
    public boolean hasPublicKey() {
        return this.publicKey != null;
    }
    
    public ASN1Encodable parsePrivateKey() throws IOException {
        return ASN1Primitive.fromByteArray(this.privateKey.getOctets());
    }
    
    public ASN1Encodable parsePublicKey() throws IOException {
        final ASN1BitString publicKey = this.publicKey;
        if (publicKey == null) {
            return null;
        }
        return ASN1Primitive.fromByteArray(publicKey.getOctets());
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(5);
        asn1EncodableVector.add(this.version);
        asn1EncodableVector.add(this.privateKeyAlgorithm);
        asn1EncodableVector.add(this.privateKey);
        final ASN1Set attributes = this.attributes;
        if (attributes != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 0, attributes));
        }
        final ASN1BitString publicKey = this.publicKey;
        if (publicKey != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 1, publicKey));
        }
        return new DERSequence(asn1EncodableVector);
    }
}
