// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Locale;

class DateUtil
{
    static Locale EN_Locale;
    private static Long ZERO;
    private static final Map localeCache;
    
    static {
        DateUtil.ZERO = longValueOf(0L);
        localeCache = new HashMap();
        DateUtil.EN_Locale = forEN();
    }
    
    static Date epochAdjust(final Date date) throws ParseException {
        final Locale default1 = Locale.getDefault();
        if (default1 == null) {
            return date;
        }
        synchronized (DateUtil.localeCache) {
            Long n;
            if ((n = DateUtil.localeCache.get(default1)) == null) {
                final long time = new SimpleDateFormat("yyyyMMddHHmmssz").parse("19700101000000GMT+00:00").getTime();
                if (time == 0L) {
                    n = DateUtil.ZERO;
                }
                else {
                    n = longValueOf(time);
                }
                DateUtil.localeCache.put(default1, n);
            }
            if (n != DateUtil.ZERO) {
                return new Date(date.getTime() - n);
            }
            return date;
        }
    }
    
    private static Locale forEN() {
        if ("en".equalsIgnoreCase(Locale.getDefault().getLanguage())) {
            return Locale.getDefault();
        }
        final Locale[] availableLocales = Locale.getAvailableLocales();
        for (int i = 0; i != availableLocales.length; ++i) {
            if ("en".equalsIgnoreCase(availableLocales[i].getLanguage())) {
                return availableLocales[i];
            }
        }
        return Locale.getDefault();
    }
    
    private static Long longValueOf(final long l) {
        return l;
    }
}
