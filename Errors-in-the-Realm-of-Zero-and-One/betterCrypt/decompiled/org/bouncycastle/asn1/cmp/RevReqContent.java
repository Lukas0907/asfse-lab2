// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.cmp;

import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Object;

public class RevReqContent extends ASN1Object
{
    private ASN1Sequence content;
    
    private RevReqContent(final ASN1Sequence content) {
        this.content = content;
    }
    
    public RevReqContent(final RevDetails revDetails) {
        this.content = new DERSequence(revDetails);
    }
    
    public RevReqContent(final RevDetails[] array) {
        this.content = new DERSequence(array);
    }
    
    public static RevReqContent getInstance(final Object o) {
        if (o instanceof RevReqContent) {
            return (RevReqContent)o;
        }
        if (o != null) {
            return new RevReqContent(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        return this.content;
    }
    
    public RevDetails[] toRevDetailsArray() {
        final RevDetails[] array = new RevDetails[this.content.size()];
        for (int i = 0; i != array.length; ++i) {
            array[i] = RevDetails.getInstance(this.content.getObjectAt(i));
        }
        return array;
    }
}
