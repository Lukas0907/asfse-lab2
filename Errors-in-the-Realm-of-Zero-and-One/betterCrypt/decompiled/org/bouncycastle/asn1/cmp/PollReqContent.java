// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.cmp;

import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Integer;
import java.math.BigInteger;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Object;

public class PollReqContent extends ASN1Object
{
    private ASN1Sequence content;
    
    public PollReqContent(final BigInteger bigInteger) {
        this(new ASN1Integer(bigInteger));
    }
    
    public PollReqContent(final ASN1Integer asn1Integer) {
        this(new DERSequence(new DERSequence(asn1Integer)));
    }
    
    private PollReqContent(final ASN1Sequence content) {
        this.content = content;
    }
    
    public PollReqContent(final BigInteger[] array) {
        this(intsToASN1(array));
    }
    
    public PollReqContent(final ASN1Integer[] array) {
        this(new DERSequence(intsToSequence(array)));
    }
    
    public static PollReqContent getInstance(final Object o) {
        if (o instanceof PollReqContent) {
            return (PollReqContent)o;
        }
        if (o != null) {
            return new PollReqContent(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    private static ASN1Integer[] intsToASN1(final BigInteger[] array) {
        final ASN1Integer[] array2 = new ASN1Integer[array.length];
        for (int i = 0; i != array2.length; ++i) {
            array2[i] = new ASN1Integer(array[i]);
        }
        return array2;
    }
    
    private static DERSequence[] intsToSequence(final ASN1Integer[] array) {
        final DERSequence[] array2 = new DERSequence[array.length];
        for (int i = 0; i != array2.length; ++i) {
            array2[i] = new DERSequence(array[i]);
        }
        return array2;
    }
    
    private static ASN1Integer[] sequenceToASN1IntegerArray(final ASN1Sequence asn1Sequence) {
        final ASN1Integer[] array = new ASN1Integer[asn1Sequence.size()];
        for (int i = 0; i != array.length; ++i) {
            array[i] = ASN1Integer.getInstance(asn1Sequence.getObjectAt(i));
        }
        return array;
    }
    
    public BigInteger[] getCertReqIdValues() {
        final BigInteger[] array = new BigInteger[this.content.size()];
        for (int i = 0; i != array.length; ++i) {
            array[i] = ASN1Integer.getInstance(ASN1Sequence.getInstance(this.content.getObjectAt(i)).getObjectAt(0)).getValue();
        }
        return array;
    }
    
    public ASN1Integer[][] getCertReqIds() {
        final ASN1Integer[][] array = new ASN1Integer[this.content.size()][];
        for (int i = 0; i != array.length; ++i) {
            array[i] = sequenceToASN1IntegerArray((ASN1Sequence)this.content.getObjectAt(i));
        }
        return array;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        return this.content;
    }
}
