// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.cmp;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.crmf.PKIPublicationInfo;
import org.bouncycastle.asn1.crmf.EncryptedValue;
import org.bouncycastle.asn1.ASN1Object;

public class CertifiedKeyPair extends ASN1Object
{
    private CertOrEncCert certOrEncCert;
    private EncryptedValue privateKey;
    private PKIPublicationInfo publicationInfo;
    
    private CertifiedKeyPair(final ASN1Sequence asn1Sequence) {
        this.certOrEncCert = CertOrEncCert.getInstance(asn1Sequence.getObjectAt(0));
        if (asn1Sequence.size() >= 2) {
            ASN1Primitive asn1Primitive;
            if (asn1Sequence.size() == 2) {
                final ASN1TaggedObject instance = ASN1TaggedObject.getInstance(asn1Sequence.getObjectAt(1));
                final int tagNo = instance.getTagNo();
                asn1Primitive = instance.getObject();
                if (tagNo == 0) {
                    this.privateKey = EncryptedValue.getInstance(asn1Primitive);
                    return;
                }
            }
            else {
                this.privateKey = EncryptedValue.getInstance(ASN1TaggedObject.getInstance(asn1Sequence.getObjectAt(1)).getObject());
                asn1Primitive = ASN1TaggedObject.getInstance(asn1Sequence.getObjectAt(2)).getObject();
            }
            this.publicationInfo = PKIPublicationInfo.getInstance(asn1Primitive);
        }
    }
    
    public CertifiedKeyPair(final CertOrEncCert certOrEncCert) {
        this(certOrEncCert, null, null);
    }
    
    public CertifiedKeyPair(final CertOrEncCert certOrEncCert, final EncryptedValue privateKey, final PKIPublicationInfo publicationInfo) {
        if (certOrEncCert != null) {
            this.certOrEncCert = certOrEncCert;
            this.privateKey = privateKey;
            this.publicationInfo = publicationInfo;
            return;
        }
        throw new IllegalArgumentException("'certOrEncCert' cannot be null");
    }
    
    public static CertifiedKeyPair getInstance(final Object o) {
        if (o instanceof CertifiedKeyPair) {
            return (CertifiedKeyPair)o;
        }
        if (o != null) {
            return new CertifiedKeyPair(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public CertOrEncCert getCertOrEncCert() {
        return this.certOrEncCert;
    }
    
    public EncryptedValue getPrivateKey() {
        return this.privateKey;
    }
    
    public PKIPublicationInfo getPublicationInfo() {
        return this.publicationInfo;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(3);
        asn1EncodableVector.add(this.certOrEncCert);
        final EncryptedValue privateKey = this.privateKey;
        if (privateKey != null) {
            asn1EncodableVector.add(new DERTaggedObject(true, 0, privateKey));
        }
        final PKIPublicationInfo publicationInfo = this.publicationInfo;
        if (publicationInfo != null) {
            asn1EncodableVector.add(new DERTaggedObject(true, 1, publicationInfo));
        }
        return new DERSequence(asn1EncodableVector);
    }
}
