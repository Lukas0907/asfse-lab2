// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.cmp;

import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Object;

public class CertRepMessage extends ASN1Object
{
    private ASN1Sequence caPubs;
    private ASN1Sequence response;
    
    private CertRepMessage(final ASN1Sequence asn1Sequence) {
        final int size = asn1Sequence.size();
        int n = 1;
        if (size > 1) {
            this.caPubs = ASN1Sequence.getInstance((ASN1TaggedObject)asn1Sequence.getObjectAt(0), true);
        }
        else {
            n = 0;
        }
        this.response = ASN1Sequence.getInstance(asn1Sequence.getObjectAt(n));
    }
    
    public CertRepMessage(final CMPCertificate[] array, final CertResponse[] array2) {
        if (array2 != null) {
            if (array != null) {
                this.caPubs = new DERSequence(array);
            }
            this.response = new DERSequence(array2);
            return;
        }
        throw new IllegalArgumentException("'response' cannot be null");
    }
    
    public static CertRepMessage getInstance(final Object o) {
        if (o instanceof CertRepMessage) {
            return (CertRepMessage)o;
        }
        if (o != null) {
            return new CertRepMessage(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public CMPCertificate[] getCaPubs() {
        final ASN1Sequence caPubs = this.caPubs;
        if (caPubs == null) {
            return null;
        }
        final CMPCertificate[] array = new CMPCertificate[caPubs.size()];
        for (int i = 0; i != array.length; ++i) {
            array[i] = CMPCertificate.getInstance(this.caPubs.getObjectAt(i));
        }
        return array;
    }
    
    public CertResponse[] getResponse() {
        final CertResponse[] array = new CertResponse[this.response.size()];
        for (int i = 0; i != array.length; ++i) {
            array[i] = CertResponse.getInstance(this.response.getObjectAt(i));
        }
        return array;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(2);
        final ASN1Sequence caPubs = this.caPubs;
        if (caPubs != null) {
            asn1EncodableVector.add(new DERTaggedObject(true, 1, caPubs));
        }
        asn1EncodableVector.add(this.response);
        return new DERSequence(asn1EncodableVector);
    }
}
