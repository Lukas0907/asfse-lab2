// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.cmp;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Object;

public class CertResponse extends ASN1Object
{
    private ASN1Integer certReqId;
    private CertifiedKeyPair certifiedKeyPair;
    private ASN1OctetString rspInfo;
    private PKIStatusInfo status;
    
    public CertResponse(final ASN1Integer asn1Integer, final PKIStatusInfo pkiStatusInfo) {
        this(asn1Integer, pkiStatusInfo, null, null);
    }
    
    public CertResponse(final ASN1Integer certReqId, final PKIStatusInfo status, final CertifiedKeyPair certifiedKeyPair, final ASN1OctetString rspInfo) {
        if (certReqId == null) {
            throw new IllegalArgumentException("'certReqId' cannot be null");
        }
        if (status != null) {
            this.certReqId = certReqId;
            this.status = status;
            this.certifiedKeyPair = certifiedKeyPair;
            this.rspInfo = rspInfo;
            return;
        }
        throw new IllegalArgumentException("'status' cannot be null");
    }
    
    private CertResponse(final ASN1Sequence asn1Sequence) {
        this.certReqId = ASN1Integer.getInstance(asn1Sequence.getObjectAt(0));
        this.status = PKIStatusInfo.getInstance(asn1Sequence.getObjectAt(1));
        if (asn1Sequence.size() >= 3) {
            ASN1Encodable asn1Encodable;
            if (asn1Sequence.size() == 3) {
                asn1Encodable = asn1Sequence.getObjectAt(2);
                if (!(asn1Encodable instanceof ASN1OctetString)) {
                    this.certifiedKeyPair = CertifiedKeyPair.getInstance(asn1Encodable);
                    return;
                }
            }
            else {
                this.certifiedKeyPair = CertifiedKeyPair.getInstance(asn1Sequence.getObjectAt(2));
                asn1Encodable = asn1Sequence.getObjectAt(3);
            }
            this.rspInfo = ASN1OctetString.getInstance(asn1Encodable);
        }
    }
    
    public static CertResponse getInstance(final Object o) {
        if (o instanceof CertResponse) {
            return (CertResponse)o;
        }
        if (o != null) {
            return new CertResponse(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public ASN1Integer getCertReqId() {
        return this.certReqId;
    }
    
    public CertifiedKeyPair getCertifiedKeyPair() {
        return this.certifiedKeyPair;
    }
    
    public PKIStatusInfo getStatus() {
        return this.status;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(4);
        asn1EncodableVector.add(this.certReqId);
        asn1EncodableVector.add(this.status);
        final CertifiedKeyPair certifiedKeyPair = this.certifiedKeyPair;
        if (certifiedKeyPair != null) {
            asn1EncodableVector.add(certifiedKeyPair);
        }
        final ASN1OctetString rspInfo = this.rspInfo;
        if (rspInfo != null) {
            asn1EncodableVector.add(rspInfo);
        }
        return new DERSequence(asn1EncodableVector);
    }
}
