// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;

public abstract class ASN1TaggedObject extends ASN1Primitive implements ASN1TaggedObjectParser
{
    final boolean explicit;
    final ASN1Encodable obj;
    final int tagNo;
    
    public ASN1TaggedObject(final boolean b, final int tagNo, final ASN1Encodable obj) {
        if (obj != null) {
            this.tagNo = tagNo;
            this.explicit = (b || obj instanceof ASN1Choice);
            this.obj = obj;
            return;
        }
        throw new NullPointerException("'obj' cannot be null");
    }
    
    public static ASN1TaggedObject getInstance(final Object o) {
        if (o != null && !(o instanceof ASN1TaggedObject)) {
            if (o instanceof byte[]) {
                try {
                    return getInstance(ASN1Primitive.fromByteArray((byte[])o));
                }
                catch (IOException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("failed to construct tagged object from byte[]: ");
                    sb.append(ex.getMessage());
                    throw new IllegalArgumentException(sb.toString());
                }
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("unknown object in getInstance: ");
            sb2.append(o.getClass().getName());
            throw new IllegalArgumentException(sb2.toString());
        }
        return (ASN1TaggedObject)o;
    }
    
    public static ASN1TaggedObject getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        if (b) {
            return getInstance(asn1TaggedObject.getObject());
        }
        throw new IllegalArgumentException("implicitly tagged tagged object");
    }
    
    @Override
    boolean asn1Equals(ASN1Primitive asn1Primitive) {
        final boolean b = asn1Primitive instanceof ASN1TaggedObject;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final ASN1TaggedObject asn1TaggedObject = (ASN1TaggedObject)asn1Primitive;
        boolean b3 = b2;
        if (this.tagNo == asn1TaggedObject.tagNo) {
            if (this.explicit != asn1TaggedObject.explicit) {
                return false;
            }
            asn1Primitive = this.obj.toASN1Primitive();
            final ASN1Primitive asn1Primitive2 = asn1TaggedObject.obj.toASN1Primitive();
            if (asn1Primitive != asn1Primitive2) {
                b3 = b2;
                if (!asn1Primitive.asn1Equals(asn1Primitive2)) {
                    return b3;
                }
            }
            b3 = true;
        }
        return b3;
    }
    
    @Override
    abstract void encode(final ASN1OutputStream p0, final boolean p1) throws IOException;
    
    @Override
    public ASN1Primitive getLoadedObject() {
        return this.toASN1Primitive();
    }
    
    public ASN1Primitive getObject() {
        return this.obj.toASN1Primitive();
    }
    
    @Override
    public ASN1Encodable getObjectParser(final int i, final boolean b) throws IOException {
        if (i == 4) {
            return ASN1OctetString.getInstance(this, b).parser();
        }
        if (i == 16) {
            return ASN1Sequence.getInstance(this, b).parser();
        }
        if (i == 17) {
            return ASN1Set.getInstance(this, b).parser();
        }
        if (b) {
            return this.getObject();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("implicit tagging not implemented for tag: ");
        sb.append(i);
        throw new ASN1Exception(sb.toString());
    }
    
    @Override
    public int getTagNo() {
        return this.tagNo;
    }
    
    @Override
    public int hashCode() {
        final int tagNo = this.tagNo;
        int n;
        if (this.explicit) {
            n = 15;
        }
        else {
            n = 240;
        }
        return tagNo ^ n ^ this.obj.toASN1Primitive().hashCode();
    }
    
    public boolean isEmpty() {
        return false;
    }
    
    public boolean isExplicit() {
        return this.explicit;
    }
    
    @Override
    ASN1Primitive toDERObject() {
        return new DERTaggedObject(this.explicit, this.tagNo, this.obj);
    }
    
    @Override
    ASN1Primitive toDLObject() {
        return new DLTaggedObject(this.explicit, this.tagNo, this.obj);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(this.tagNo);
        sb.append("]");
        sb.append(this.obj);
        return sb.toString();
    }
}
