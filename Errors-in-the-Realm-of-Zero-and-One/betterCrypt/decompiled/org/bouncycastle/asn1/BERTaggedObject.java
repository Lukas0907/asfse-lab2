// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;
import java.util.Enumeration;

public class BERTaggedObject extends ASN1TaggedObject
{
    public BERTaggedObject(final int n) {
        super(false, n, new BERSequence());
    }
    
    public BERTaggedObject(final int n, final ASN1Encodable asn1Encodable) {
        super(true, n, asn1Encodable);
    }
    
    public BERTaggedObject(final boolean b, final int n, final ASN1Encodable asn1Encodable) {
        super(b, n, asn1Encodable);
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream, final boolean b) throws IOException {
        asn1OutputStream.writeTag(b, 160, this.tagNo);
        asn1OutputStream.write(128);
        if (!this.explicit) {
            Enumeration enumeration;
            if (this.obj instanceof ASN1OctetString) {
                if (this.obj instanceof BEROctetString) {
                    enumeration = ((BEROctetString)this.obj).getObjects();
                }
                else {
                    enumeration = new BEROctetString(((ASN1OctetString)this.obj).getOctets()).getObjects();
                }
            }
            else if (this.obj instanceof ASN1Sequence) {
                enumeration = ((ASN1Sequence)this.obj).getObjects();
            }
            else {
                if (!(this.obj instanceof ASN1Set)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("not implemented: ");
                    sb.append(this.obj.getClass().getName());
                    throw new ASN1Exception(sb.toString());
                }
                enumeration = ((ASN1Set)this.obj).getObjects();
            }
            asn1OutputStream.writeElements(enumeration);
        }
        else {
            asn1OutputStream.writePrimitive(this.obj.toASN1Primitive(), true);
        }
        asn1OutputStream.write(0);
        asn1OutputStream.write(0);
    }
    
    @Override
    int encodedLength() throws IOException {
        int encodedLength = this.obj.toASN1Primitive().encodedLength();
        int calculateTagLength;
        if (this.explicit) {
            calculateTagLength = StreamUtil.calculateTagLength(this.tagNo) + StreamUtil.calculateBodyLength(encodedLength);
        }
        else {
            --encodedLength;
            calculateTagLength = StreamUtil.calculateTagLength(this.tagNo);
        }
        return calculateTagLength + encodedLength;
    }
    
    @Override
    boolean isConstructed() {
        return this.explicit || this.obj.toASN1Primitive().isConstructed();
    }
}
