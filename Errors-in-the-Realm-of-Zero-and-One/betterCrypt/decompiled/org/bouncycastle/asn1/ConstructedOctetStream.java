// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;
import java.io.InputStream;

class ConstructedOctetStream extends InputStream
{
    private InputStream _currentStream;
    private boolean _first;
    private final ASN1StreamParser _parser;
    
    ConstructedOctetStream(final ASN1StreamParser parser) {
        this._first = true;
        this._parser = parser;
    }
    
    @Override
    public int read() throws IOException {
        while (true) {
            ASN1OctetStringParser asn1OctetStringParser = null;
            Label_0131: {
                if (this._currentStream == null) {
                    if (!this._first) {
                        return -1;
                    }
                    final ASN1Encodable object = this._parser.readObject();
                    if (object == null) {
                        return -1;
                    }
                    if (object instanceof ASN1OctetStringParser) {
                        asn1OctetStringParser = (ASN1OctetStringParser)object;
                        this._first = false;
                        break Label_0131;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("unknown object encountered: ");
                    sb.append(((ASN1OctetStringParser)object).getClass());
                    throw new IOException(sb.toString());
                }
                final int read = this._currentStream.read();
                if (read >= 0) {
                    return read;
                }
                final ASN1Encodable object2 = this._parser.readObject();
                if (object2 == null) {
                    this._currentStream = null;
                    return -1;
                }
                if (!(object2 instanceof ASN1OctetStringParser)) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("unknown object encountered: ");
                    sb2.append(((ASN1OctetStringParser)object2).getClass());
                    throw new IOException(sb2.toString());
                }
                asn1OctetStringParser = (ASN1OctetStringParser)object2;
            }
            this._currentStream = asn1OctetStringParser.getOctetStream();
            continue;
        }
    }
    
    @Override
    public int read(final byte[] b, int n, final int n2) throws IOException {
        final InputStream currentStream = this._currentStream;
        int n3 = 0;
        final int n4 = 0;
        while (true) {
            ASN1OctetStringParser asn1OctetStringParser = null;
            Label_0066: {
                if (currentStream == null) {
                    if (!this._first) {
                        return -1;
                    }
                    final ASN1Encodable object = this._parser.readObject();
                    if (object == null) {
                        return -1;
                    }
                    if (object instanceof ASN1OctetStringParser) {
                        asn1OctetStringParser = (ASN1OctetStringParser)object;
                        this._first = false;
                        n3 = n4;
                        break Label_0066;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("unknown object encountered: ");
                    sb.append(((ASN1OctetStringParser)object).getClass());
                    throw new IOException(sb.toString());
                }
                int n5;
                do {
                    final int read = this._currentStream.read(b, n + n3, n2 - n3);
                    if (read >= 0) {
                        n5 = n3 + read;
                    }
                    else {
                        final ASN1Encodable object2 = this._parser.readObject();
                        if (object2 == null) {
                            this._currentStream = null;
                            if ((n = n3) < 1) {
                                n = -1;
                            }
                            return n;
                        }
                        if (object2 instanceof ASN1OctetStringParser) {
                            asn1OctetStringParser = (ASN1OctetStringParser)object2;
                            break Label_0066;
                        }
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("unknown object encountered: ");
                        sb2.append(((ASN1OctetStringParser)object2).getClass());
                        throw new IOException(sb2.toString());
                    }
                } while ((n3 = n5) != n2);
                return n5;
            }
            this._currentStream = asn1OctetStringParser.getOctetStream();
            continue;
        }
    }
}
