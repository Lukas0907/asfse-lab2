// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.ocsp;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.asn1.ASN1GeneralizedTime;
import org.bouncycastle.asn1.ASN1Object;

public class SingleResponse extends ASN1Object
{
    private CertID certID;
    private CertStatus certStatus;
    private ASN1GeneralizedTime nextUpdate;
    private Extensions singleExtensions;
    private ASN1GeneralizedTime thisUpdate;
    
    private SingleResponse(final ASN1Sequence asn1Sequence) {
        this.certID = CertID.getInstance(asn1Sequence.getObjectAt(0));
        this.certStatus = CertStatus.getInstance(asn1Sequence.getObjectAt(1));
        this.thisUpdate = ASN1GeneralizedTime.getInstance(asn1Sequence.getObjectAt(2));
        ASN1TaggedObject asn1TaggedObject2 = null;
        Label_0073: {
            if (asn1Sequence.size() <= 4) {
                if (asn1Sequence.size() > 3) {
                    final ASN1TaggedObject asn1TaggedObject = asn1TaggedObject2 = (ASN1TaggedObject)asn1Sequence.getObjectAt(3);
                    if (asn1TaggedObject.getTagNo() != 0) {
                        break Label_0073;
                    }
                    this.nextUpdate = ASN1GeneralizedTime.getInstance(asn1TaggedObject, true);
                }
                return;
            }
            this.nextUpdate = ASN1GeneralizedTime.getInstance((ASN1TaggedObject)asn1Sequence.getObjectAt(3), true);
            asn1TaggedObject2 = (ASN1TaggedObject)asn1Sequence.getObjectAt(4);
        }
        this.singleExtensions = Extensions.getInstance(asn1TaggedObject2, true);
    }
    
    public SingleResponse(final CertID certID, final CertStatus certStatus, final ASN1GeneralizedTime thisUpdate, final ASN1GeneralizedTime nextUpdate, final Extensions singleExtensions) {
        this.certID = certID;
        this.certStatus = certStatus;
        this.thisUpdate = thisUpdate;
        this.nextUpdate = nextUpdate;
        this.singleExtensions = singleExtensions;
    }
    
    public SingleResponse(final CertID certID, final CertStatus certStatus, final ASN1GeneralizedTime asn1GeneralizedTime, final ASN1GeneralizedTime asn1GeneralizedTime2, final X509Extensions x509Extensions) {
        this(certID, certStatus, asn1GeneralizedTime, asn1GeneralizedTime2, Extensions.getInstance(x509Extensions));
    }
    
    public static SingleResponse getInstance(final Object o) {
        if (o instanceof SingleResponse) {
            return (SingleResponse)o;
        }
        if (o != null) {
            return new SingleResponse(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public static SingleResponse getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        return getInstance(ASN1Sequence.getInstance(asn1TaggedObject, b));
    }
    
    public CertID getCertID() {
        return this.certID;
    }
    
    public CertStatus getCertStatus() {
        return this.certStatus;
    }
    
    public ASN1GeneralizedTime getNextUpdate() {
        return this.nextUpdate;
    }
    
    public Extensions getSingleExtensions() {
        return this.singleExtensions;
    }
    
    public ASN1GeneralizedTime getThisUpdate() {
        return this.thisUpdate;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(5);
        asn1EncodableVector.add(this.certID);
        asn1EncodableVector.add(this.certStatus);
        asn1EncodableVector.add(this.thisUpdate);
        final ASN1GeneralizedTime nextUpdate = this.nextUpdate;
        if (nextUpdate != null) {
            asn1EncodableVector.add(new DERTaggedObject(true, 0, nextUpdate));
        }
        final Extensions singleExtensions = this.singleExtensions;
        if (singleExtensions != null) {
            asn1EncodableVector.add(new DERTaggedObject(true, 1, singleExtensions));
        }
        return new DERSequence(asn1EncodableVector);
    }
}
