// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.util.Date;

public class DERUTCTime extends ASN1UTCTime
{
    public DERUTCTime(final String s) {
        super(s);
    }
    
    public DERUTCTime(final Date date) {
        super(date);
    }
    
    DERUTCTime(final byte[] array) {
        super(array);
    }
}
