// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.cmc;

import org.bouncycastle.asn1.DERTaggedObject;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.crmf.CertReqMsg;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Choice;
import org.bouncycastle.asn1.ASN1Object;

public class TaggedRequest extends ASN1Object implements ASN1Choice
{
    public static final int CRM = 1;
    public static final int ORM = 2;
    public static final int TCR = 0;
    private final int tagNo;
    private final ASN1Encodable value;
    
    private TaggedRequest(final ASN1Sequence value) {
        this.tagNo = 2;
        this.value = value;
    }
    
    public TaggedRequest(final TaggedCertificationRequest value) {
        this.tagNo = 0;
        this.value = value;
    }
    
    public TaggedRequest(final CertReqMsg value) {
        this.tagNo = 1;
        this.value = value;
    }
    
    public static TaggedRequest getInstance(final Object o) {
        if (o instanceof TaggedRequest) {
            return (TaggedRequest)o;
        }
        Label_0202: {
            if (o == null) {
                break Label_0202;
            }
            Label_0163: {
                if (o instanceof ASN1Encodable) {
                    final ASN1TaggedObject instance = ASN1TaggedObject.getInstance(((ASN1Encodable)o).toASN1Primitive());
                    final int tagNo = instance.getTagNo();
                    if (tagNo == 0) {
                        return new TaggedRequest(TaggedCertificationRequest.getInstance(instance, false));
                    }
                    if (tagNo == 1) {
                        return new TaggedRequest(CertReqMsg.getInstance(instance, false));
                    }
                    if (tagNo == 2) {
                        return new TaggedRequest(ASN1Sequence.getInstance(instance, false));
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("unknown tag in getInstance(): ");
                    sb.append(instance.getTagNo());
                    throw new IllegalArgumentException(sb.toString());
                }
                else if (!(o instanceof byte[])) {
                    break Label_0163;
                }
                while (true) {
                    while (true) {
                        try {
                            return getInstance(ASN1Primitive.fromByteArray((byte[])o));
                            return null;
                            throw new IllegalArgumentException("unknown encoding in getInstance()");
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("unknown object in getInstance(): ");
                            sb2.append(o.getClass().getName());
                            throw new IllegalArgumentException(sb2.toString());
                        }
                        catch (IOException ex) {}
                        continue;
                    }
                }
            }
        }
    }
    
    public int getTagNo() {
        return this.tagNo;
    }
    
    public ASN1Encodable getValue() {
        return this.value;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        return new DERTaggedObject(false, this.tagNo, this.value);
    }
}
