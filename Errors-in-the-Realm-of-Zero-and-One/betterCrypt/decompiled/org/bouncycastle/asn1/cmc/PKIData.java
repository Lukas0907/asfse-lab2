// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.cmc;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Object;

public class PKIData extends ASN1Object
{
    private final TaggedContentInfo[] cmsSequence;
    private final TaggedAttribute[] controlSequence;
    private final OtherMsg[] otherMsgSequence;
    private final TaggedRequest[] reqSequence;
    
    private PKIData(ASN1Sequence asn1Sequence) {
        if (asn1Sequence.size() == 4) {
            final int n = 0;
            final ASN1Sequence asn1Sequence2 = (ASN1Sequence)asn1Sequence.getObjectAt(0);
            this.controlSequence = new TaggedAttribute[asn1Sequence2.size()];
            int n2 = 0;
            while (true) {
                final TaggedAttribute[] controlSequence = this.controlSequence;
                if (n2 >= controlSequence.length) {
                    break;
                }
                controlSequence[n2] = TaggedAttribute.getInstance(asn1Sequence2.getObjectAt(n2));
                ++n2;
            }
            final ASN1Sequence asn1Sequence3 = (ASN1Sequence)asn1Sequence.getObjectAt(1);
            this.reqSequence = new TaggedRequest[asn1Sequence3.size()];
            int n3 = 0;
            while (true) {
                final TaggedRequest[] reqSequence = this.reqSequence;
                if (n3 >= reqSequence.length) {
                    break;
                }
                reqSequence[n3] = TaggedRequest.getInstance(asn1Sequence3.getObjectAt(n3));
                ++n3;
            }
            final ASN1Sequence asn1Sequence4 = (ASN1Sequence)asn1Sequence.getObjectAt(2);
            this.cmsSequence = new TaggedContentInfo[asn1Sequence4.size()];
            int n4 = 0;
            while (true) {
                final TaggedContentInfo[] cmsSequence = this.cmsSequence;
                if (n4 >= cmsSequence.length) {
                    break;
                }
                cmsSequence[n4] = TaggedContentInfo.getInstance(asn1Sequence4.getObjectAt(n4));
                ++n4;
            }
            asn1Sequence = (ASN1Sequence)asn1Sequence.getObjectAt(3);
            this.otherMsgSequence = new OtherMsg[asn1Sequence.size()];
            int n5 = n;
            while (true) {
                final OtherMsg[] otherMsgSequence = this.otherMsgSequence;
                if (n5 >= otherMsgSequence.length) {
                    break;
                }
                otherMsgSequence[n5] = OtherMsg.getInstance(asn1Sequence.getObjectAt(n5));
                ++n5;
            }
            return;
        }
        throw new IllegalArgumentException("Sequence not 4 elements.");
    }
    
    public PKIData(final TaggedAttribute[] array, final TaggedRequest[] array2, final TaggedContentInfo[] array3, final OtherMsg[] array4) {
        this.controlSequence = this.copy(array);
        this.reqSequence = this.copy(array2);
        this.cmsSequence = this.copy(array3);
        this.otherMsgSequence = this.copy(array4);
    }
    
    private OtherMsg[] copy(final OtherMsg[] array) {
        final OtherMsg[] array2 = new OtherMsg[array.length];
        System.arraycopy(array, 0, array2, 0, array2.length);
        return array2;
    }
    
    private TaggedAttribute[] copy(final TaggedAttribute[] array) {
        final TaggedAttribute[] array2 = new TaggedAttribute[array.length];
        System.arraycopy(array, 0, array2, 0, array2.length);
        return array2;
    }
    
    private TaggedContentInfo[] copy(final TaggedContentInfo[] array) {
        final TaggedContentInfo[] array2 = new TaggedContentInfo[array.length];
        System.arraycopy(array, 0, array2, 0, array2.length);
        return array2;
    }
    
    private TaggedRequest[] copy(final TaggedRequest[] array) {
        final TaggedRequest[] array2 = new TaggedRequest[array.length];
        System.arraycopy(array, 0, array2, 0, array2.length);
        return array2;
    }
    
    public static PKIData getInstance(final Object o) {
        if (o instanceof PKIData) {
            return (PKIData)o;
        }
        if (o != null) {
            return new PKIData(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public TaggedContentInfo[] getCmsSequence() {
        return this.copy(this.cmsSequence);
    }
    
    public TaggedAttribute[] getControlSequence() {
        return this.copy(this.controlSequence);
    }
    
    public OtherMsg[] getOtherMsgSequence() {
        return this.copy(this.otherMsgSequence);
    }
    
    public TaggedRequest[] getReqSequence() {
        return this.copy(this.reqSequence);
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        return new DERSequence(new ASN1Encodable[] { new DERSequence(this.controlSequence), new DERSequence(this.reqSequence), new DERSequence(this.cmsSequence), new DERSequence(this.otherMsgSequence) });
    }
}
