// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;
import java.io.ByteArrayOutputStream;

public class DLExternal extends ASN1External
{
    public DLExternal(final ASN1EncodableVector asn1EncodableVector) {
        super(asn1EncodableVector);
    }
    
    public DLExternal(final ASN1ObjectIdentifier asn1ObjectIdentifier, final ASN1Integer asn1Integer, final ASN1Primitive asn1Primitive, final int n, final ASN1Primitive asn1Primitive2) {
        super(asn1ObjectIdentifier, asn1Integer, asn1Primitive, n, asn1Primitive2);
    }
    
    public DLExternal(final ASN1ObjectIdentifier asn1ObjectIdentifier, final ASN1Integer asn1Integer, final ASN1Primitive asn1Primitive, final DERTaggedObject derTaggedObject) {
        this(asn1ObjectIdentifier, asn1Integer, asn1Primitive, derTaggedObject.getTagNo(), derTaggedObject.toASN1Primitive());
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream, final boolean b) throws IOException {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (this.directReference != null) {
            byteArrayOutputStream.write(this.directReference.getEncoded("DL"));
        }
        if (this.indirectReference != null) {
            byteArrayOutputStream.write(this.indirectReference.getEncoded("DL"));
        }
        if (this.dataValueDescriptor != null) {
            byteArrayOutputStream.write(this.dataValueDescriptor.getEncoded("DL"));
        }
        byteArrayOutputStream.write(new DERTaggedObject(true, this.encoding, this.externalContent).getEncoded("DL"));
        asn1OutputStream.writeEncoded(b, 32, 8, byteArrayOutputStream.toByteArray());
    }
    
    @Override
    int encodedLength() throws IOException {
        return this.getEncoded().length;
    }
    
    @Override
    ASN1Primitive toDLObject() {
        return this;
    }
}
