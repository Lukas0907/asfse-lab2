// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;

public class DLSet extends ASN1Set
{
    private int bodyLength;
    
    public DLSet() {
        this.bodyLength = -1;
    }
    
    public DLSet(final ASN1Encodable asn1Encodable) {
        super(asn1Encodable);
        this.bodyLength = -1;
    }
    
    public DLSet(final ASN1EncodableVector asn1EncodableVector) {
        super(asn1EncodableVector, false);
        this.bodyLength = -1;
    }
    
    DLSet(final boolean b, final ASN1Encodable[] array) {
        super(b, array);
        this.bodyLength = -1;
    }
    
    public DLSet(final ASN1Encodable[] array) {
        super(array, false);
        this.bodyLength = -1;
    }
    
    private int getBodyLength() throws IOException {
        if (this.bodyLength < 0) {
            final int length = this.elements.length;
            int i = 0;
            int bodyLength = 0;
            while (i < length) {
                bodyLength += this.elements[i].toASN1Primitive().toDLObject().encodedLength();
                ++i;
            }
            this.bodyLength = bodyLength;
        }
        return this.bodyLength;
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream, final boolean b) throws IOException {
        if (b) {
            asn1OutputStream.write(49);
        }
        final ASN1OutputStream dlSubStream = asn1OutputStream.getDLSubStream();
        final int length = this.elements.length;
        final int bodyLength = this.bodyLength;
        int i = 0;
        final int n = 0;
        if (bodyLength < 0 && length <= 16) {
            final ASN1Primitive[] array = new ASN1Primitive[length];
            int bodyLength2;
            for (int j = bodyLength2 = 0; j < length; ++j) {
                final ASN1Primitive dlObject = this.elements[j].toASN1Primitive().toDLObject();
                array[j] = dlObject;
                bodyLength2 += dlObject.encodedLength();
            }
            asn1OutputStream.writeLength(this.bodyLength = bodyLength2);
            for (int k = n; k < length; ++k) {
                dlSubStream.writePrimitive(array[k], true);
            }
        }
        else {
            asn1OutputStream.writeLength(this.getBodyLength());
            while (i < length) {
                dlSubStream.writePrimitive(this.elements[i].toASN1Primitive(), true);
                ++i;
            }
        }
    }
    
    @Override
    int encodedLength() throws IOException {
        final int bodyLength = this.getBodyLength();
        return StreamUtil.calculateBodyLength(bodyLength) + 1 + bodyLength;
    }
    
    @Override
    ASN1Primitive toDLObject() {
        return this;
    }
}
