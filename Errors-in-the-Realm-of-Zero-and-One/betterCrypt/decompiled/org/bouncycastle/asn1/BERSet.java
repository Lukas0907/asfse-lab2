// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;

public class BERSet extends ASN1Set
{
    public BERSet() {
    }
    
    public BERSet(final ASN1Encodable asn1Encodable) {
        super(asn1Encodable);
    }
    
    public BERSet(final ASN1EncodableVector asn1EncodableVector) {
        super(asn1EncodableVector, false);
    }
    
    BERSet(final boolean b, final ASN1Encodable[] array) {
        super(b, array);
    }
    
    public BERSet(final ASN1Encodable[] array) {
        super(array, false);
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream, final boolean b) throws IOException {
        asn1OutputStream.writeEncodedIndef(b, 49, this.elements);
    }
    
    @Override
    int encodedLength() throws IOException {
        final int length = this.elements.length;
        int i = 0;
        int n = 0;
        while (i < length) {
            n += this.elements[i].toASN1Primitive().encodedLength();
            ++i;
        }
        return n + 2 + 2;
    }
}
