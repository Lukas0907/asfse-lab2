// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.cms;

import org.bouncycastle.asn1.ASN1Encodable;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1SequenceParser;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1TaggedObjectParser;

public class ContentInfoParser
{
    private ASN1TaggedObjectParser content;
    private ASN1ObjectIdentifier contentType;
    
    public ContentInfoParser(final ASN1SequenceParser asn1SequenceParser) throws IOException {
        this.contentType = (ASN1ObjectIdentifier)asn1SequenceParser.readObject();
        this.content = (ASN1TaggedObjectParser)asn1SequenceParser.readObject();
    }
    
    public ASN1Encodable getContent(final int n) throws IOException {
        final ASN1TaggedObjectParser content = this.content;
        if (content != null) {
            return content.getObjectParser(n, true);
        }
        return null;
    }
    
    public ASN1ObjectIdentifier getContentType() {
        return this.contentType;
    }
}
