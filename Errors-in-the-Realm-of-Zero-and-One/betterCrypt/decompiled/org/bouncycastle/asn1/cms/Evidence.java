// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.cms;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.tsp.EvidenceRecord;
import org.bouncycastle.asn1.ASN1Choice;
import org.bouncycastle.asn1.ASN1Object;

public class Evidence extends ASN1Object implements ASN1Choice
{
    private EvidenceRecord ersEvidence;
    private ASN1Sequence otherEvidence;
    private TimeStampTokenEvidence tstEvidence;
    
    private Evidence(final ASN1TaggedObject asn1TaggedObject) {
        if (asn1TaggedObject.getTagNo() == 0) {
            this.tstEvidence = TimeStampTokenEvidence.getInstance(asn1TaggedObject, false);
            return;
        }
        if (asn1TaggedObject.getTagNo() == 1) {
            this.ersEvidence = EvidenceRecord.getInstance(asn1TaggedObject, false);
            return;
        }
        if (asn1TaggedObject.getTagNo() == 2) {
            this.otherEvidence = ASN1Sequence.getInstance(asn1TaggedObject, false);
            return;
        }
        throw new IllegalArgumentException("unknown tag in Evidence");
    }
    
    public Evidence(final TimeStampTokenEvidence tstEvidence) {
        this.tstEvidence = tstEvidence;
    }
    
    public Evidence(final EvidenceRecord ersEvidence) {
        this.ersEvidence = ersEvidence;
    }
    
    public static Evidence getInstance(final Object o) {
        if (o == null || o instanceof Evidence) {
            return (Evidence)o;
        }
        if (o instanceof ASN1TaggedObject) {
            return new Evidence(ASN1TaggedObject.getInstance(o));
        }
        throw new IllegalArgumentException("unknown object in getInstance");
    }
    
    public static Evidence getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        return getInstance(asn1TaggedObject.getObject());
    }
    
    public EvidenceRecord getErsEvidence() {
        return this.ersEvidence;
    }
    
    public TimeStampTokenEvidence getTstEvidence() {
        return this.tstEvidence;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final TimeStampTokenEvidence tstEvidence = this.tstEvidence;
        if (tstEvidence != null) {
            return new DERTaggedObject(false, 0, tstEvidence);
        }
        final EvidenceRecord ersEvidence = this.ersEvidence;
        if (ersEvidence != null) {
            return new DERTaggedObject(false, 1, ersEvidence);
        }
        return new DERTaggedObject(false, 2, this.otherEvidence);
    }
}
