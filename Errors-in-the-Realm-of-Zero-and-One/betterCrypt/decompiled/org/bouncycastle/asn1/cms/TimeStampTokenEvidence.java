// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.cms;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1TaggedObject;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Object;

public class TimeStampTokenEvidence extends ASN1Object
{
    private TimeStampAndCRL[] timeStampAndCRLs;
    
    private TimeStampTokenEvidence(final ASN1Sequence asn1Sequence) {
        this.timeStampAndCRLs = new TimeStampAndCRL[asn1Sequence.size()];
        final Enumeration objects = asn1Sequence.getObjects();
        int n = 0;
        while (objects.hasMoreElements()) {
            this.timeStampAndCRLs[n] = TimeStampAndCRL.getInstance(objects.nextElement());
            ++n;
        }
    }
    
    public TimeStampTokenEvidence(final TimeStampAndCRL timeStampAndCRL) {
        (this.timeStampAndCRLs = new TimeStampAndCRL[1])[0] = timeStampAndCRL;
    }
    
    public TimeStampTokenEvidence(final TimeStampAndCRL[] array) {
        this.timeStampAndCRLs = this.copy(array);
    }
    
    private TimeStampAndCRL[] copy(final TimeStampAndCRL[] array) {
        final TimeStampAndCRL[] array2 = new TimeStampAndCRL[array.length];
        System.arraycopy(array, 0, array2, 0, array2.length);
        return array2;
    }
    
    public static TimeStampTokenEvidence getInstance(final Object o) {
        if (o instanceof TimeStampTokenEvidence) {
            return (TimeStampTokenEvidence)o;
        }
        if (o != null) {
            return new TimeStampTokenEvidence(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public static TimeStampTokenEvidence getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        return getInstance(ASN1Sequence.getInstance(asn1TaggedObject, b));
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(this.timeStampAndCRLs.length);
        int n = 0;
        while (true) {
            final TimeStampAndCRL[] timeStampAndCRLs = this.timeStampAndCRLs;
            if (n == timeStampAndCRLs.length) {
                break;
            }
            asn1EncodableVector.add(timeStampAndCRLs[n]);
            ++n;
        }
        return new DERSequence(asn1EncodableVector);
    }
    
    public TimeStampAndCRL[] toTimeStampAndCRLArray() {
        return this.copy(this.timeStampAndCRLs);
    }
}
