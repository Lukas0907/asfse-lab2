// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.cms;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1GeneralizedTime;
import org.bouncycastle.asn1.ASN1Object;

public class KEKIdentifier extends ASN1Object
{
    private ASN1GeneralizedTime date;
    private ASN1OctetString keyIdentifier;
    private OtherKeyAttribute other;
    
    private KEKIdentifier(final ASN1Sequence asn1Sequence) {
        this.keyIdentifier = (ASN1OctetString)asn1Sequence.getObjectAt(0);
        final int size = asn1Sequence.size();
        if (size != 1) {
            ASN1Encodable asn1Encodable;
            if (size != 2) {
                if (size != 3) {
                    throw new IllegalArgumentException("Invalid KEKIdentifier");
                }
                this.date = (ASN1GeneralizedTime)asn1Sequence.getObjectAt(1);
                asn1Encodable = asn1Sequence.getObjectAt(2);
            }
            else {
                final boolean b = asn1Sequence.getObjectAt(1) instanceof ASN1GeneralizedTime;
                asn1Encodable = asn1Sequence.getObjectAt(1);
                if (b) {
                    this.date = (ASN1GeneralizedTime)asn1Encodable;
                    return;
                }
            }
            this.other = OtherKeyAttribute.getInstance(asn1Encodable);
        }
    }
    
    public KEKIdentifier(final byte[] array, final ASN1GeneralizedTime date, final OtherKeyAttribute other) {
        this.keyIdentifier = new DEROctetString(array);
        this.date = date;
        this.other = other;
    }
    
    public static KEKIdentifier getInstance(final Object o) {
        if (o == null || o instanceof KEKIdentifier) {
            return (KEKIdentifier)o;
        }
        if (o instanceof ASN1Sequence) {
            return new KEKIdentifier((ASN1Sequence)o);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid KEKIdentifier: ");
        sb.append(o.getClass().getName());
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static KEKIdentifier getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        return getInstance(ASN1Sequence.getInstance(asn1TaggedObject, b));
    }
    
    public ASN1GeneralizedTime getDate() {
        return this.date;
    }
    
    public ASN1OctetString getKeyIdentifier() {
        return this.keyIdentifier;
    }
    
    public OtherKeyAttribute getOther() {
        return this.other;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(3);
        asn1EncodableVector.add(this.keyIdentifier);
        final ASN1GeneralizedTime date = this.date;
        if (date != null) {
            asn1EncodableVector.add(date);
        }
        final OtherKeyAttribute other = this.other;
        if (other != null) {
            asn1EncodableVector.add(other);
        }
        return new DERSequence(asn1EncodableVector);
    }
}
