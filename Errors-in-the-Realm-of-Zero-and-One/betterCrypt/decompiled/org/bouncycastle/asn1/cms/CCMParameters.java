// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.cms;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Object;

public class CCMParameters extends ASN1Object
{
    private int icvLen;
    private byte[] nonce;
    
    private CCMParameters(final ASN1Sequence asn1Sequence) {
        this.nonce = ASN1OctetString.getInstance(asn1Sequence.getObjectAt(0)).getOctets();
        int intValueExact;
        if (asn1Sequence.size() == 2) {
            intValueExact = ASN1Integer.getInstance(asn1Sequence.getObjectAt(1)).intValueExact();
        }
        else {
            intValueExact = 12;
        }
        this.icvLen = intValueExact;
    }
    
    public CCMParameters(final byte[] array, final int icvLen) {
        this.nonce = Arrays.clone(array);
        this.icvLen = icvLen;
    }
    
    public static CCMParameters getInstance(final Object o) {
        if (o instanceof CCMParameters) {
            return (CCMParameters)o;
        }
        if (o != null) {
            return new CCMParameters(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public int getIcvLen() {
        return this.icvLen;
    }
    
    public byte[] getNonce() {
        return Arrays.clone(this.nonce);
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(2);
        asn1EncodableVector.add(new DEROctetString(this.nonce));
        final int icvLen = this.icvLen;
        if (icvLen != 12) {
            asn1EncodableVector.add(new ASN1Integer(icvLen));
        }
        return new DERSequence(asn1EncodableVector);
    }
}
