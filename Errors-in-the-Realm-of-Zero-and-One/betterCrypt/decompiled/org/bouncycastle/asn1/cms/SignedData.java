// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.cms;

import org.bouncycastle.asn1.BERSequence;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.BERSet;
import java.util.Enumeration;
import org.bouncycastle.asn1.BERTaggedObject;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Object;

public class SignedData extends ASN1Object
{
    private static final ASN1Integer VERSION_1;
    private static final ASN1Integer VERSION_3;
    private static final ASN1Integer VERSION_4;
    private static final ASN1Integer VERSION_5;
    private ASN1Set certificates;
    private boolean certsBer;
    private ContentInfo contentInfo;
    private ASN1Set crls;
    private boolean crlsBer;
    private ASN1Set digestAlgorithms;
    private ASN1Set signerInfos;
    private ASN1Integer version;
    
    static {
        VERSION_1 = new ASN1Integer(1L);
        VERSION_3 = new ASN1Integer(3L);
        VERSION_4 = new ASN1Integer(4L);
        VERSION_5 = new ASN1Integer(5L);
    }
    
    private SignedData(final ASN1Sequence asn1Sequence) {
        final Enumeration objects = asn1Sequence.getObjects();
        this.version = ASN1Integer.getInstance(objects.nextElement());
        this.digestAlgorithms = objects.nextElement();
        this.contentInfo = ContentInfo.getInstance(objects.nextElement());
        while (objects.hasMoreElements()) {
            final ASN1Set set = objects.nextElement();
            if (set instanceof ASN1TaggedObject) {
                final ASN1TaggedObject asn1TaggedObject = (ASN1TaggedObject)set;
                final int tagNo = asn1TaggedObject.getTagNo();
                if (tagNo != 0) {
                    if (tagNo != 1) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("unknown tag value ");
                        sb.append(asn1TaggedObject.getTagNo());
                        throw new IllegalArgumentException(sb.toString());
                    }
                    this.crlsBer = (asn1TaggedObject instanceof BERTaggedObject);
                    this.crls = ASN1Set.getInstance(asn1TaggedObject, false);
                }
                else {
                    this.certsBer = (asn1TaggedObject instanceof BERTaggedObject);
                    this.certificates = ASN1Set.getInstance(asn1TaggedObject, false);
                }
            }
            else {
                this.signerInfos = set;
            }
        }
    }
    
    public SignedData(final ASN1Set digestAlgorithms, final ContentInfo contentInfo, final ASN1Set certificates, final ASN1Set crls, final ASN1Set signerInfos) {
        this.version = this.calculateVersion(contentInfo.getContentType(), certificates, crls, signerInfos);
        this.digestAlgorithms = digestAlgorithms;
        this.contentInfo = contentInfo;
        this.certificates = certificates;
        this.crls = crls;
        this.signerInfos = signerInfos;
        this.crlsBer = (crls instanceof BERSet);
        this.certsBer = (certificates instanceof BERSet);
    }
    
    private ASN1Integer calculateVersion(final ASN1ObjectIdentifier asn1ObjectIdentifier, final ASN1Set set, final ASN1Set set2, final ASN1Set set3) {
        final int n = 0;
        final int n2 = 0;
        int n6;
        int n7;
        int n8;
        if (set != null) {
            final Enumeration objects = set.getObjects();
            int n3 = 0;
            int n5;
            int n4 = n5 = n3;
            while (true) {
                n6 = n3;
                n7 = n4;
                n8 = n5;
                if (!objects.hasMoreElements()) {
                    break;
                }
                final Object nextElement = objects.nextElement();
                if (!(nextElement instanceof ASN1TaggedObject)) {
                    continue;
                }
                final ASN1TaggedObject instance = ASN1TaggedObject.getInstance(nextElement);
                if (instance.getTagNo() == 1) {
                    n4 = 1;
                }
                else if (instance.getTagNo() == 2) {
                    n5 = 1;
                }
                else {
                    if (instance.getTagNo() != 3) {
                        continue;
                    }
                    n3 = 1;
                }
            }
        }
        else {
            n6 = 0;
            n8 = (n7 = n6);
        }
        if (n6 != 0) {
            return new ASN1Integer(5L);
        }
        int n9 = n;
        if (set2 != null) {
            final Enumeration objects2 = set2.getObjects();
            int n10 = n2;
            while (true) {
                n9 = n10;
                if (!objects2.hasMoreElements()) {
                    break;
                }
                if (!(objects2.nextElement() instanceof ASN1TaggedObject)) {
                    continue;
                }
                n10 = 1;
            }
        }
        if (n9 != 0) {
            return SignedData.VERSION_5;
        }
        if (n8 != 0) {
            return SignedData.VERSION_4;
        }
        if (n7 != 0) {
            return SignedData.VERSION_3;
        }
        if (this.checkForVersion3(set3)) {
            return SignedData.VERSION_3;
        }
        if (!CMSObjectIdentifiers.data.equals(asn1ObjectIdentifier)) {
            return SignedData.VERSION_3;
        }
        return SignedData.VERSION_1;
    }
    
    private boolean checkForVersion3(final ASN1Set set) {
        final Enumeration objects = set.getObjects();
        while (objects.hasMoreElements()) {
            if (SignerInfo.getInstance(objects.nextElement()).getVersion().intValueExact() == 3) {
                return true;
            }
        }
        return false;
    }
    
    public static SignedData getInstance(final Object o) {
        if (o instanceof SignedData) {
            return (SignedData)o;
        }
        if (o != null) {
            return new SignedData(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public ASN1Set getCRLs() {
        return this.crls;
    }
    
    public ASN1Set getCertificates() {
        return this.certificates;
    }
    
    public ASN1Set getDigestAlgorithms() {
        return this.digestAlgorithms;
    }
    
    public ContentInfo getEncapContentInfo() {
        return this.contentInfo;
    }
    
    public ASN1Set getSignerInfos() {
        return this.signerInfos;
    }
    
    public ASN1Integer getVersion() {
        return this.version;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(6);
        asn1EncodableVector.add(this.version);
        asn1EncodableVector.add(this.digestAlgorithms);
        asn1EncodableVector.add(this.contentInfo);
        final ASN1Set certificates = this.certificates;
        if (certificates != null) {
            ASN1TaggedObject asn1TaggedObject;
            if (this.certsBer) {
                asn1TaggedObject = new BERTaggedObject(false, 0, certificates);
            }
            else {
                asn1TaggedObject = new DERTaggedObject(false, 0, certificates);
            }
            asn1EncodableVector.add(asn1TaggedObject);
        }
        final ASN1Set crls = this.crls;
        if (crls != null) {
            ASN1TaggedObject asn1TaggedObject2;
            if (this.crlsBer) {
                asn1TaggedObject2 = new BERTaggedObject(false, 1, crls);
            }
            else {
                asn1TaggedObject2 = new DERTaggedObject(false, 1, crls);
            }
            asn1EncodableVector.add(asn1TaggedObject2);
        }
        asn1EncodableVector.add(this.signerInfos);
        return new BERSequence(asn1EncodableVector);
    }
}
