// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.cms;

import org.bouncycastle.asn1.BERSequence;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.ASN1Object;

public class AuthEnvelopedData extends ASN1Object
{
    private ASN1Set authAttrs;
    private EncryptedContentInfo authEncryptedContentInfo;
    private ASN1OctetString mac;
    private OriginatorInfo originatorInfo;
    private ASN1Set recipientInfos;
    private ASN1Set unauthAttrs;
    private ASN1Integer version;
    
    private AuthEnvelopedData(final ASN1Sequence asn1Sequence) {
        this.version = ASN1Integer.getInstance(asn1Sequence.getObjectAt(0).toASN1Primitive());
        if (this.version.intValueExact() != 0) {
            throw new IllegalArgumentException("AuthEnvelopedData version number must be 0");
        }
        ASN1Primitive asn1Primitive = asn1Sequence.getObjectAt(1).toASN1Primitive();
        int n;
        if (asn1Primitive instanceof ASN1TaggedObject) {
            this.originatorInfo = OriginatorInfo.getInstance((ASN1TaggedObject)asn1Primitive, false);
            n = 3;
            asn1Primitive = asn1Sequence.getObjectAt(2).toASN1Primitive();
        }
        else {
            n = 2;
        }
        this.recipientInfos = ASN1Set.getInstance(asn1Primitive);
        if (this.recipientInfos.size() != 0) {
            final int n2 = n + 1;
            this.authEncryptedContentInfo = EncryptedContentInfo.getInstance(asn1Sequence.getObjectAt(n).toASN1Primitive());
            final int n3 = n2 + 1;
            final ASN1Primitive asn1Primitive2 = asn1Sequence.getObjectAt(n2).toASN1Primitive();
            ASN1Primitive asn1Primitive3;
            int n4;
            if (asn1Primitive2 instanceof ASN1TaggedObject) {
                this.authAttrs = ASN1Set.getInstance((ASN1TaggedObject)asn1Primitive2, false);
                asn1Primitive3 = asn1Sequence.getObjectAt(n3).toASN1Primitive();
                n4 = n3 + 1;
            }
            else {
                asn1Primitive3 = asn1Primitive2;
                n4 = n3;
                if (!this.authEncryptedContentInfo.getContentType().equals(CMSObjectIdentifiers.data)) {
                    final ASN1Set authAttrs = this.authAttrs;
                    if (authAttrs == null || authAttrs.size() == 0) {
                        throw new IllegalArgumentException("authAttrs must be present with non-data content");
                    }
                    asn1Primitive3 = asn1Primitive2;
                    n4 = n3;
                }
            }
            this.mac = ASN1OctetString.getInstance(asn1Primitive3);
            if (asn1Sequence.size() > n4) {
                this.unauthAttrs = ASN1Set.getInstance((ASN1TaggedObject)asn1Sequence.getObjectAt(n4).toASN1Primitive(), false);
            }
            return;
        }
        throw new IllegalArgumentException("AuthEnvelopedData requires at least 1 RecipientInfo");
    }
    
    public AuthEnvelopedData(final OriginatorInfo originatorInfo, final ASN1Set recipientInfos, final EncryptedContentInfo authEncryptedContentInfo, final ASN1Set authAttrs, final ASN1OctetString mac, final ASN1Set unauthAttrs) {
        this.version = new ASN1Integer(0L);
        this.originatorInfo = originatorInfo;
        this.recipientInfos = recipientInfos;
        if (this.recipientInfos.size() == 0) {
            throw new IllegalArgumentException("AuthEnvelopedData requires at least 1 RecipientInfo");
        }
        this.authEncryptedContentInfo = authEncryptedContentInfo;
        this.authAttrs = authAttrs;
        if (!authEncryptedContentInfo.getContentType().equals(CMSObjectIdentifiers.data) && (authAttrs == null || authAttrs.size() == 0)) {
            throw new IllegalArgumentException("authAttrs must be present with non-data content");
        }
        this.mac = mac;
        this.unauthAttrs = unauthAttrs;
    }
    
    public static AuthEnvelopedData getInstance(final Object o) {
        if (o == null || o instanceof AuthEnvelopedData) {
            return (AuthEnvelopedData)o;
        }
        if (o instanceof ASN1Sequence) {
            return new AuthEnvelopedData((ASN1Sequence)o);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid AuthEnvelopedData: ");
        sb.append(o.getClass().getName());
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static AuthEnvelopedData getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        return getInstance(ASN1Sequence.getInstance(asn1TaggedObject, b));
    }
    
    public ASN1Set getAuthAttrs() {
        return this.authAttrs;
    }
    
    public EncryptedContentInfo getAuthEncryptedContentInfo() {
        return this.authEncryptedContentInfo;
    }
    
    public ASN1OctetString getMac() {
        return this.mac;
    }
    
    public OriginatorInfo getOriginatorInfo() {
        return this.originatorInfo;
    }
    
    public ASN1Set getRecipientInfos() {
        return this.recipientInfos;
    }
    
    public ASN1Set getUnauthAttrs() {
        return this.unauthAttrs;
    }
    
    public ASN1Integer getVersion() {
        return this.version;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(7);
        asn1EncodableVector.add(this.version);
        final OriginatorInfo originatorInfo = this.originatorInfo;
        if (originatorInfo != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 0, originatorInfo));
        }
        asn1EncodableVector.add(this.recipientInfos);
        asn1EncodableVector.add(this.authEncryptedContentInfo);
        final ASN1Set authAttrs = this.authAttrs;
        if (authAttrs != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 1, authAttrs));
        }
        asn1EncodableVector.add(this.mac);
        final ASN1Set unauthAttrs = this.unauthAttrs;
        if (unauthAttrs != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 2, unauthAttrs));
        }
        return new BERSequence(asn1EncodableVector);
    }
}
