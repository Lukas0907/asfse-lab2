// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x9;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.math.ec.ECAlgorithms;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import java.math.BigInteger;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.asn1.ASN1Object;

public class X9Curve extends ASN1Object implements X9ObjectIdentifiers
{
    private ECCurve curve;
    private ASN1ObjectIdentifier fieldIdentifier;
    private byte[] seed;
    
    public X9Curve(final X9FieldID x9FieldID, final BigInteger bigInteger, final BigInteger bigInteger2, final ASN1Sequence asn1Sequence) {
        this.fieldIdentifier = null;
        this.fieldIdentifier = x9FieldID.getIdentifier();
        ECCurve curve;
        if (this.fieldIdentifier.equals(X9Curve.prime_field)) {
            curve = new ECCurve.Fp(((ASN1Integer)x9FieldID.getParameters()).getValue(), new BigInteger(1, ASN1OctetString.getInstance(asn1Sequence.getObjectAt(0)).getOctets()), new BigInteger(1, ASN1OctetString.getInstance(asn1Sequence.getObjectAt(1)).getOctets()), bigInteger, bigInteger2);
        }
        else {
            if (!this.fieldIdentifier.equals(X9Curve.characteristic_two_field)) {
                throw new IllegalArgumentException("This type of ECCurve is not implemented");
            }
            final ASN1Sequence instance = ASN1Sequence.getInstance(x9FieldID.getParameters());
            final int intValueExact = ((ASN1Integer)instance.getObjectAt(0)).intValueExact();
            final ASN1ObjectIdentifier asn1ObjectIdentifier = (ASN1ObjectIdentifier)instance.getObjectAt(1);
            int intValueExact2;
            int intValueExact4;
            int intValueExact3;
            if (asn1ObjectIdentifier.equals(X9Curve.tpBasis)) {
                intValueExact2 = ASN1Integer.getInstance(instance.getObjectAt(2)).intValueExact();
                intValueExact3 = (intValueExact4 = 0);
            }
            else {
                if (!asn1ObjectIdentifier.equals(X9Curve.ppBasis)) {
                    throw new IllegalArgumentException("This type of EC basis is not implemented");
                }
                final ASN1Sequence instance2 = ASN1Sequence.getInstance(instance.getObjectAt(2));
                final int intValueExact5 = ASN1Integer.getInstance(instance2.getObjectAt(0)).intValueExact();
                intValueExact3 = ASN1Integer.getInstance(instance2.getObjectAt(1)).intValueExact();
                intValueExact4 = ASN1Integer.getInstance(instance2.getObjectAt(2)).intValueExact();
                intValueExact2 = intValueExact5;
            }
            curve = new ECCurve.F2m(intValueExact, intValueExact2, intValueExact3, intValueExact4, new BigInteger(1, ASN1OctetString.getInstance(asn1Sequence.getObjectAt(0)).getOctets()), new BigInteger(1, ASN1OctetString.getInstance(asn1Sequence.getObjectAt(1)).getOctets()), bigInteger, bigInteger2);
        }
        this.curve = curve;
        if (asn1Sequence.size() == 3) {
            this.seed = ((DERBitString)asn1Sequence.getObjectAt(2)).getBytes();
        }
    }
    
    public X9Curve(final ECCurve ecCurve) {
        this(ecCurve, null);
    }
    
    public X9Curve(final ECCurve curve, final byte[] array) {
        this.fieldIdentifier = null;
        this.curve = curve;
        this.seed = Arrays.clone(array);
        this.setFieldIdentifier();
    }
    
    private void setFieldIdentifier() {
        ASN1ObjectIdentifier fieldIdentifier;
        if (ECAlgorithms.isFpCurve(this.curve)) {
            fieldIdentifier = X9Curve.prime_field;
        }
        else {
            if (!ECAlgorithms.isF2mCurve(this.curve)) {
                throw new IllegalArgumentException("This type of ECCurve is not implemented");
            }
            fieldIdentifier = X9Curve.characteristic_two_field;
        }
        this.fieldIdentifier = fieldIdentifier;
    }
    
    public ECCurve getCurve() {
        return this.curve;
    }
    
    public byte[] getSeed() {
        return Arrays.clone(this.seed);
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(3);
        Label_0121: {
            X9FieldElement x9FieldElement;
            if (this.fieldIdentifier.equals(X9Curve.prime_field)) {
                asn1EncodableVector.add(new X9FieldElement(this.curve.getA()).toASN1Primitive());
                x9FieldElement = new X9FieldElement(this.curve.getB());
            }
            else {
                if (!this.fieldIdentifier.equals(X9Curve.characteristic_two_field)) {
                    break Label_0121;
                }
                asn1EncodableVector.add(new X9FieldElement(this.curve.getA()).toASN1Primitive());
                x9FieldElement = new X9FieldElement(this.curve.getB());
            }
            asn1EncodableVector.add(x9FieldElement.toASN1Primitive());
        }
        final byte[] seed = this.seed;
        if (seed != null) {
            asn1EncodableVector.add(new DERBitString(seed));
        }
        return new DERSequence(asn1EncodableVector);
    }
}
