// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.edec;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;

public interface EdECObjectIdentifiers
{
    public static final ASN1ObjectIdentifier id_Ed25519 = EdECObjectIdentifiers.id_edwards_curve_algs.branch("112").intern();
    public static final ASN1ObjectIdentifier id_Ed448 = EdECObjectIdentifiers.id_edwards_curve_algs.branch("113").intern();
    public static final ASN1ObjectIdentifier id_X25519 = EdECObjectIdentifiers.id_edwards_curve_algs.branch("110").intern();
    public static final ASN1ObjectIdentifier id_X448 = EdECObjectIdentifiers.id_edwards_curve_algs.branch("111").intern();
    public static final ASN1ObjectIdentifier id_edwards_curve_algs = new ASN1ObjectIdentifier("1.3.101");
}
