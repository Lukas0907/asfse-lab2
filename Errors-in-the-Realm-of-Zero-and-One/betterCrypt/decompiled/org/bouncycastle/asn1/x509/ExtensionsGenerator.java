// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x509;

import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.DEROctetString;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.util.Hashtable;
import java.util.Vector;

public class ExtensionsGenerator
{
    private Vector extOrdering;
    private Hashtable extensions;
    
    public ExtensionsGenerator() {
        this.extensions = new Hashtable();
        this.extOrdering = new Vector();
    }
    
    public void addExtension(final ASN1ObjectIdentifier asn1ObjectIdentifier, final boolean b, final ASN1Encodable asn1Encodable) throws IOException {
        this.addExtension(asn1ObjectIdentifier, b, asn1Encodable.toASN1Primitive().getEncoded("DER"));
    }
    
    public void addExtension(final ASN1ObjectIdentifier asn1ObjectIdentifier, final boolean b, final byte[] array) {
        if (!this.extensions.containsKey(asn1ObjectIdentifier)) {
            this.extOrdering.addElement(asn1ObjectIdentifier);
            this.extensions.put(asn1ObjectIdentifier, new Extension(asn1ObjectIdentifier, b, new DEROctetString(array)));
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("extension ");
        sb.append(asn1ObjectIdentifier);
        sb.append(" already added");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void addExtension(final Extension value) {
        if (!this.extensions.containsKey(value.getExtnId())) {
            this.extOrdering.addElement(value.getExtnId());
            this.extensions.put(value.getExtnId(), value);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("extension ");
        sb.append(value.getExtnId());
        sb.append(" already added");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public Extensions generate() {
        final Extension[] array = new Extension[this.extOrdering.size()];
        for (int i = 0; i != this.extOrdering.size(); ++i) {
            array[i] = (Extension)this.extensions.get(this.extOrdering.elementAt(i));
        }
        return new Extensions(array);
    }
    
    public Extension getExtension(final ASN1ObjectIdentifier key) {
        return this.extensions.get(key);
    }
    
    public boolean hasExtension(final ASN1ObjectIdentifier key) {
        return this.extensions.containsKey(key);
    }
    
    public boolean isEmpty() {
        return this.extOrdering.isEmpty();
    }
    
    public void removeExtension(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        if (this.extensions.containsKey(asn1ObjectIdentifier)) {
            this.extOrdering.removeElement(asn1ObjectIdentifier);
            this.extensions.remove(asn1ObjectIdentifier);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("extension ");
        sb.append(asn1ObjectIdentifier);
        sb.append(" not present");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void replaceExtension(final ASN1ObjectIdentifier asn1ObjectIdentifier, final boolean b, final ASN1Encodable asn1Encodable) throws IOException {
        this.replaceExtension(asn1ObjectIdentifier, b, asn1Encodable.toASN1Primitive().getEncoded("DER"));
    }
    
    public void replaceExtension(final ASN1ObjectIdentifier asn1ObjectIdentifier, final boolean b, final byte[] array) {
        this.replaceExtension(new Extension(asn1ObjectIdentifier, b, array));
    }
    
    public void replaceExtension(final Extension value) {
        if (this.extensions.containsKey(value.getExtnId())) {
            this.extensions.put(value.getExtnId(), value);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("extension ");
        sb.append(value.getExtnId());
        sb.append(" not present");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void reset() {
        this.extensions = new Hashtable();
        this.extOrdering = new Vector();
    }
}
