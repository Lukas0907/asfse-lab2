// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x509;

import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.ASN1Object;

public class TBSCertificateStructure extends ASN1Object implements X509ObjectIdentifiers, PKCSObjectIdentifiers
{
    Time endDate;
    X509Extensions extensions;
    X500Name issuer;
    DERBitString issuerUniqueId;
    ASN1Sequence seq;
    ASN1Integer serialNumber;
    AlgorithmIdentifier signature;
    Time startDate;
    X500Name subject;
    SubjectPublicKeyInfo subjectPublicKeyInfo;
    DERBitString subjectUniqueId;
    ASN1Integer version;
    
    public TBSCertificateStructure(final ASN1Sequence seq) {
        this.seq = seq;
        int n;
        if (seq.getObjectAt(0) instanceof ASN1TaggedObject) {
            this.version = ASN1Integer.getInstance((ASN1TaggedObject)seq.getObjectAt(0), true);
            n = 0;
        }
        else {
            this.version = new ASN1Integer(0L);
            n = -1;
        }
        this.serialNumber = ASN1Integer.getInstance(seq.getObjectAt(n + 1));
        this.signature = AlgorithmIdentifier.getInstance(seq.getObjectAt(n + 2));
        this.issuer = X500Name.getInstance(seq.getObjectAt(n + 3));
        final ASN1Sequence asn1Sequence = (ASN1Sequence)seq.getObjectAt(n + 4);
        this.startDate = Time.getInstance(asn1Sequence.getObjectAt(0));
        this.endDate = Time.getInstance(asn1Sequence.getObjectAt(1));
        this.subject = X500Name.getInstance(seq.getObjectAt(n + 5));
        final int n2 = n + 6;
        this.subjectPublicKeyInfo = SubjectPublicKeyInfo.getInstance(seq.getObjectAt(n2));
        for (int i = seq.size() - n2 - 1; i > 0; --i) {
            final ASN1TaggedObject instance = ASN1TaggedObject.getInstance(seq.getObjectAt(n2 + i));
            final int tagNo = instance.getTagNo();
            if (tagNo != 1) {
                if (tagNo != 2) {
                    if (tagNo == 3) {
                        this.extensions = X509Extensions.getInstance(instance);
                    }
                }
                else {
                    this.subjectUniqueId = DERBitString.getInstance(instance, false);
                }
            }
            else {
                this.issuerUniqueId = DERBitString.getInstance(instance, false);
            }
        }
    }
    
    public static TBSCertificateStructure getInstance(final Object o) {
        if (o instanceof TBSCertificateStructure) {
            return (TBSCertificateStructure)o;
        }
        if (o != null) {
            return new TBSCertificateStructure(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public static TBSCertificateStructure getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        return getInstance(ASN1Sequence.getInstance(asn1TaggedObject, b));
    }
    
    public Time getEndDate() {
        return this.endDate;
    }
    
    public X509Extensions getExtensions() {
        return this.extensions;
    }
    
    public X500Name getIssuer() {
        return this.issuer;
    }
    
    public DERBitString getIssuerUniqueId() {
        return this.issuerUniqueId;
    }
    
    public ASN1Integer getSerialNumber() {
        return this.serialNumber;
    }
    
    public AlgorithmIdentifier getSignature() {
        return this.signature;
    }
    
    public Time getStartDate() {
        return this.startDate;
    }
    
    public X500Name getSubject() {
        return this.subject;
    }
    
    public SubjectPublicKeyInfo getSubjectPublicKeyInfo() {
        return this.subjectPublicKeyInfo;
    }
    
    public DERBitString getSubjectUniqueId() {
        return this.subjectUniqueId;
    }
    
    public int getVersion() {
        return this.version.intValueExact() + 1;
    }
    
    public ASN1Integer getVersionNumber() {
        return this.version;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        return this.seq;
    }
}
