// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x509;

import java.util.Map;
import org.bouncycastle.util.Integers;
import java.util.HashMap;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.bouncycastle.asn1.x500.style.RFC4519Style;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.x500.RDN;
import java.io.IOException;
import org.bouncycastle.util.encoders.Hex;
import java.util.Collections;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.util.Strings;
import java.util.HashSet;
import java.util.Set;

public class PKIXNameConstraintValidator implements NameConstraintValidator
{
    private Set excludedSubtreesDN;
    private Set excludedSubtreesDNS;
    private Set excludedSubtreesEmail;
    private Set excludedSubtreesIP;
    private Set excludedSubtreesOtherName;
    private Set excludedSubtreesURI;
    private Set permittedSubtreesDN;
    private Set permittedSubtreesDNS;
    private Set permittedSubtreesEmail;
    private Set permittedSubtreesIP;
    private Set permittedSubtreesOtherName;
    private Set permittedSubtreesURI;
    
    public PKIXNameConstraintValidator() {
        this.excludedSubtreesDN = new HashSet();
        this.excludedSubtreesDNS = new HashSet();
        this.excludedSubtreesEmail = new HashSet();
        this.excludedSubtreesURI = new HashSet();
        this.excludedSubtreesIP = new HashSet();
        this.excludedSubtreesOtherName = new HashSet();
    }
    
    private final void addLine(final StringBuilder sb, final String str) {
        sb.append(str);
        sb.append(Strings.lineSeparator());
    }
    
    private void checkExcludedDN(final Set set, final ASN1Sequence asn1Sequence) throws NameConstraintValidatorException {
        if (set.isEmpty()) {
            return;
        }
        final Iterator<ASN1Sequence> iterator = set.iterator();
        while (iterator.hasNext()) {
            if (!withinDNSubtree(asn1Sequence, iterator.next())) {
                continue;
            }
            throw new NameConstraintValidatorException("Subject distinguished name is from an excluded subtree");
        }
    }
    
    private void checkExcludedDNS(final Set set, final String s) throws NameConstraintValidatorException {
        if (set.isEmpty()) {
            return;
        }
        for (final String anotherString : set) {
            if (!this.withinDomain(s, anotherString) && !s.equalsIgnoreCase(anotherString)) {
                continue;
            }
            throw new NameConstraintValidatorException("DNS is from an excluded subtree.");
        }
    }
    
    private void checkExcludedEmail(final Set set, final String s) throws NameConstraintValidatorException {
        if (set.isEmpty()) {
            return;
        }
        final Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            if (!this.emailIsConstrained(s, iterator.next())) {
                continue;
            }
            throw new NameConstraintValidatorException("Email address is from an excluded subtree.");
        }
    }
    
    private void checkExcludedIP(final Set set, final byte[] array) throws NameConstraintValidatorException {
        if (set.isEmpty()) {
            return;
        }
        final Iterator<byte[]> iterator = set.iterator();
        while (iterator.hasNext()) {
            if (!this.isIPConstrained(array, iterator.next())) {
                continue;
            }
            throw new NameConstraintValidatorException("IP is from an excluded subtree.");
        }
    }
    
    private void checkExcludedOtherName(final Set set, final OtherName otherName) throws NameConstraintValidatorException {
        if (set.isEmpty()) {
            return;
        }
        final Iterator<Object> iterator = set.iterator();
        while (iterator.hasNext()) {
            if (!this.otherNameIsConstrained(otherName, OtherName.getInstance(iterator.next()))) {
                continue;
            }
            throw new NameConstraintValidatorException("OtherName is from an excluded subtree.");
        }
    }
    
    private void checkExcludedURI(final Set set, final String s) throws NameConstraintValidatorException {
        if (set.isEmpty()) {
            return;
        }
        final Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            if (!this.isUriConstrained(s, iterator.next())) {
                continue;
            }
            throw new NameConstraintValidatorException("URI is from an excluded subtree.");
        }
    }
    
    private void checkPermittedDN(final Set set, final ASN1Sequence asn1Sequence) throws NameConstraintValidatorException {
        if (set == null) {
            return;
        }
        if (set.isEmpty() && asn1Sequence.size() == 0) {
            return;
        }
        final Iterator<ASN1Sequence> iterator = set.iterator();
        while (iterator.hasNext()) {
            if (withinDNSubtree(asn1Sequence, iterator.next())) {
                return;
            }
        }
        throw new NameConstraintValidatorException("Subject distinguished name is not from a permitted subtree");
    }
    
    private void checkPermittedDNS(final Set set, final String s) throws NameConstraintValidatorException {
        if (set == null) {
            return;
        }
        for (final String anotherString : set) {
            if (this.withinDomain(s, anotherString) || s.equalsIgnoreCase(anotherString)) {
                return;
            }
        }
        if (s.length() == 0 && set.size() == 0) {
            return;
        }
        throw new NameConstraintValidatorException("DNS is not from a permitted subtree.");
    }
    
    private void checkPermittedEmail(final Set set, final String s) throws NameConstraintValidatorException {
        if (set == null) {
            return;
        }
        final Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            if (this.emailIsConstrained(s, iterator.next())) {
                return;
            }
        }
        if (s.length() == 0 && set.size() == 0) {
            return;
        }
        throw new NameConstraintValidatorException("Subject email address is not from a permitted subtree.");
    }
    
    private void checkPermittedIP(final Set set, final byte[] array) throws NameConstraintValidatorException {
        if (set == null) {
            return;
        }
        final Iterator<byte[]> iterator = set.iterator();
        while (iterator.hasNext()) {
            if (this.isIPConstrained(array, iterator.next())) {
                return;
            }
        }
        if (array.length == 0 && set.size() == 0) {
            return;
        }
        throw new NameConstraintValidatorException("IP is not from a permitted subtree.");
    }
    
    private void checkPermittedOtherName(final Set set, final OtherName otherName) throws NameConstraintValidatorException {
        if (set == null) {
            return;
        }
        final Iterator<OtherName> iterator = set.iterator();
        while (iterator.hasNext()) {
            if (this.otherNameIsConstrained(otherName, iterator.next())) {
                return;
            }
        }
        throw new NameConstraintValidatorException("Subject OtherName is not from a permitted subtree.");
    }
    
    private void checkPermittedURI(final Set set, final String s) throws NameConstraintValidatorException {
        if (set == null) {
            return;
        }
        final Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            if (this.isUriConstrained(s, iterator.next())) {
                return;
            }
        }
        if (s.length() == 0 && set.size() == 0) {
            return;
        }
        throw new NameConstraintValidatorException("URI is not from a permitted subtree.");
    }
    
    private boolean collectionsAreEqual(final Collection collection, final Collection collection2) {
        if (collection == collection2) {
            return true;
        }
        if (collection == null) {
            return false;
        }
        if (collection2 == null) {
            return false;
        }
        if (collection.size() != collection2.size()) {
            return false;
        }
    Label_0041:
        for (final Object next : collection) {
            final Iterator<Object> iterator2 = collection2.iterator();
            while (true) {
                while (iterator2.hasNext()) {
                    if (this.equals(next, iterator2.next())) {
                        final boolean b = true;
                        if (!b) {
                            return false;
                        }
                        continue Label_0041;
                    }
                }
                final boolean b = false;
                continue;
            }
        }
        return true;
    }
    
    private static int compareTo(final byte[] array, final byte[] array2) {
        if (Arrays.areEqual(array, array2)) {
            return 0;
        }
        if (Arrays.areEqual(max(array, array2), array)) {
            return 1;
        }
        return -1;
    }
    
    private boolean emailIsConstrained(final String s, final String s2) {
        final String substring = s.substring(s.indexOf(64) + 1);
        if (s2.indexOf(64) != -1) {
            if (s.equalsIgnoreCase(s2)) {
                return true;
            }
            if (substring.equalsIgnoreCase(s2.substring(1))) {
                return true;
            }
        }
        else if (s2.charAt(0) != '.') {
            if (substring.equalsIgnoreCase(s2)) {
                return true;
            }
        }
        else if (this.withinDomain(substring, s2)) {
            return true;
        }
        return false;
    }
    
    private boolean equals(final Object o, final Object obj) {
        if (o == obj) {
            return true;
        }
        if (o == null || obj == null) {
            return false;
        }
        if (o instanceof byte[] && obj instanceof byte[]) {
            return Arrays.areEqual((byte[])o, (byte[])obj);
        }
        return o.equals(obj);
    }
    
    private static String extractHostFromURL(String s) {
        final String s2 = s = s.substring(s.indexOf(58) + 1);
        if (s2.indexOf("//") != -1) {
            s = s2.substring(s2.indexOf("//") + 2);
        }
        String substring = s;
        if (s.lastIndexOf(58) != -1) {
            substring = s.substring(0, s.lastIndexOf(58));
        }
        s = substring.substring(substring.indexOf(58) + 1);
        final String s3 = s = s.substring(s.indexOf(64) + 1);
        if (s3.indexOf(47) != -1) {
            s = s3.substring(0, s3.indexOf(47));
        }
        return s;
    }
    
    private byte[][] extractIPsAndSubnetMasks(byte[] array, final byte[] array2) {
        final int n = array.length / 2;
        final byte[] array3 = new byte[n];
        final byte[] array4 = new byte[n];
        System.arraycopy(array, 0, array3, 0, n);
        System.arraycopy(array, n, array4, 0, n);
        array = new byte[n];
        final byte[] array5 = new byte[n];
        System.arraycopy(array2, 0, array, 0, n);
        System.arraycopy(array2, n, array5, 0, n);
        return new byte[][] { array3, array4, array, array5 };
    }
    
    private String extractNameAsString(final GeneralName generalName) {
        return DERIA5String.getInstance(generalName.getName()).getString();
    }
    
    private int hashCollection(final Collection collection) {
        int n = 0;
        if (collection == null) {
            return 0;
        }
        for (final byte[] next : collection) {
            int n2;
            if (next instanceof byte[]) {
                n2 = Arrays.hashCode(next);
            }
            else {
                n2 = next.hashCode();
            }
            n += n2;
        }
        return n;
    }
    
    private Set intersectDN(final Set set, final Set set2) {
        final HashSet<ASN1Sequence> set3 = new HashSet<ASN1Sequence>();
        final Iterator<GeneralSubtree> iterator = set2.iterator();
        while (iterator.hasNext()) {
            final ASN1Sequence instance = ASN1Sequence.getInstance(iterator.next().getBase().getName().toASN1Primitive());
            if (set == null) {
                if (instance == null) {
                    continue;
                }
                set3.add(instance);
            }
            else {
                for (final ASN1Sequence asn1Sequence : set) {
                    if (withinDNSubtree(instance, asn1Sequence)) {
                        set3.add(instance);
                    }
                    else {
                        if (!withinDNSubtree(asn1Sequence, instance)) {
                            continue;
                        }
                        set3.add(asn1Sequence);
                    }
                }
            }
        }
        return set3;
    }
    
    private Set intersectDNS(final Set set, final Set set2) {
        final HashSet<String> set3 = new HashSet<String>();
        final Iterator<GeneralSubtree> iterator = set2.iterator();
        while (iterator.hasNext()) {
            final String nameAsString = this.extractNameAsString(iterator.next().getBase());
            if (set == null) {
                if (nameAsString == null) {
                    continue;
                }
                set3.add(nameAsString);
            }
            else {
                for (final String s : set) {
                    if (this.withinDomain(s, nameAsString)) {
                        set3.add(s);
                    }
                    else {
                        if (!this.withinDomain(nameAsString, s)) {
                            continue;
                        }
                        set3.add(nameAsString);
                    }
                }
            }
        }
        return set3;
    }
    
    private Set intersectEmail(final Set set, final Set set2) {
        final HashSet<String> set3 = new HashSet<String>();
        final Iterator<GeneralSubtree> iterator = set2.iterator();
        while (iterator.hasNext()) {
            final String nameAsString = this.extractNameAsString(iterator.next().getBase());
            if (set == null) {
                if (nameAsString == null) {
                    continue;
                }
                set3.add(nameAsString);
            }
            else {
                final Iterator<String> iterator2 = set.iterator();
                while (iterator2.hasNext()) {
                    this.intersectEmail(nameAsString, iterator2.next(), set3);
                }
            }
        }
        return set3;
    }
    
    private void intersectEmail(final String anotherString, final String s, final Set set) {
        Label_0245: {
            if (anotherString.indexOf(64) == -1) {
                if (anotherString.startsWith(".")) {
                    if (s.indexOf(64) != -1) {
                        if (!this.withinDomain(s.substring(anotherString.indexOf(64) + 1), anotherString)) {
                            return;
                        }
                    }
                    else if (s.startsWith(".")) {
                        if (this.withinDomain(anotherString, s)) {
                            break Label_0245;
                        }
                        if (anotherString.equalsIgnoreCase(s)) {
                            break Label_0245;
                        }
                        if (!this.withinDomain(s, anotherString)) {
                            return;
                        }
                    }
                    else if (!this.withinDomain(s, anotherString)) {
                        return;
                    }
                }
                else if (s.indexOf(64) != -1) {
                    if (!s.substring(s.indexOf(64) + 1).equalsIgnoreCase(anotherString)) {
                        return;
                    }
                }
                else if (s.startsWith(".")) {
                    if (this.withinDomain(anotherString, s)) {
                        break Label_0245;
                    }
                    return;
                }
                else {
                    if (anotherString.equalsIgnoreCase(s)) {
                        break Label_0245;
                    }
                    return;
                }
                set.add(s);
                return;
            }
            final String substring = anotherString.substring(anotherString.indexOf(64) + 1);
            if (s.indexOf(64) != -1) {
                if (!anotherString.equalsIgnoreCase(s)) {
                    return;
                }
            }
            else if (s.startsWith(".")) {
                if (!this.withinDomain(substring, s)) {
                    return;
                }
            }
            else if (!substring.equalsIgnoreCase(s)) {
                return;
            }
        }
        set.add(anotherString);
    }
    
    private Set intersectIP(final Set set, final Set set2) {
        final HashSet<Object> set3 = new HashSet<Object>();
        final Iterator<GeneralSubtree> iterator = set2.iterator();
        while (iterator.hasNext()) {
            final byte[] octets = ASN1OctetString.getInstance(iterator.next().getBase().getName()).getOctets();
            if (set == null) {
                if (octets == null) {
                    continue;
                }
                set3.add(octets);
            }
            else {
                final Iterator<byte[]> iterator2 = set.iterator();
                while (iterator2.hasNext()) {
                    set3.addAll(this.intersectIPRange(iterator2.next(), octets));
                }
            }
        }
        return set3;
    }
    
    private Set intersectIPRange(byte[] array, byte[] array2) {
        if (array.length != array2.length) {
            return Collections.EMPTY_SET;
        }
        final byte[][] iPsAndSubnetMasks = this.extractIPsAndSubnetMasks(array, array2);
        final byte[] array3 = iPsAndSubnetMasks[0];
        array = iPsAndSubnetMasks[1];
        final byte[] array4 = iPsAndSubnetMasks[2];
        array2 = iPsAndSubnetMasks[3];
        final byte[][] minMaxIPs = this.minMaxIPs(array3, array, array4, array2);
        if (compareTo(max(minMaxIPs[0], minMaxIPs[2]), min(minMaxIPs[1], minMaxIPs[3])) == 1) {
            return Collections.EMPTY_SET;
        }
        return Collections.singleton(this.ipWithSubnetMask(or(minMaxIPs[0], minMaxIPs[2]), or(array, array2)));
    }
    
    private Set intersectOtherName(final Set set, final Set set2) {
        final HashSet<Object> set3 = new HashSet<Object>();
        for (final Object next : set2) {
            if (set == null) {
                if (next == null) {
                    continue;
                }
                set3.add(next);
            }
            else {
                final Iterator<String> iterator2 = set.iterator();
                while (iterator2.hasNext()) {
                    this.intersectOtherName(next, iterator2.next(), set3);
                }
            }
        }
        return set3;
    }
    
    private void intersectOtherName(final Object o, final Object obj, final Set set) {
        if (o.equals(obj)) {
            set.add(o);
        }
    }
    
    private Set intersectURI(final Set set, final Set set2) {
        final HashSet<String> set3 = new HashSet<String>();
        final Iterator<GeneralSubtree> iterator = set2.iterator();
        while (iterator.hasNext()) {
            final String nameAsString = this.extractNameAsString(iterator.next().getBase());
            if (set == null) {
                if (nameAsString == null) {
                    continue;
                }
                set3.add(nameAsString);
            }
            else {
                final Iterator<String> iterator2 = set.iterator();
                while (iterator2.hasNext()) {
                    this.intersectURI(iterator2.next(), nameAsString, set3);
                }
            }
        }
        return set3;
    }
    
    private void intersectURI(final String anotherString, final String s, final Set set) {
        Label_0245: {
            if (anotherString.indexOf(64) == -1) {
                if (anotherString.startsWith(".")) {
                    if (s.indexOf(64) != -1) {
                        if (!this.withinDomain(s.substring(anotherString.indexOf(64) + 1), anotherString)) {
                            return;
                        }
                    }
                    else if (s.startsWith(".")) {
                        if (this.withinDomain(anotherString, s)) {
                            break Label_0245;
                        }
                        if (anotherString.equalsIgnoreCase(s)) {
                            break Label_0245;
                        }
                        if (!this.withinDomain(s, anotherString)) {
                            return;
                        }
                    }
                    else if (!this.withinDomain(s, anotherString)) {
                        return;
                    }
                }
                else if (s.indexOf(64) != -1) {
                    if (!s.substring(s.indexOf(64) + 1).equalsIgnoreCase(anotherString)) {
                        return;
                    }
                }
                else if (s.startsWith(".")) {
                    if (this.withinDomain(anotherString, s)) {
                        break Label_0245;
                    }
                    return;
                }
                else {
                    if (anotherString.equalsIgnoreCase(s)) {
                        break Label_0245;
                    }
                    return;
                }
                set.add(s);
                return;
            }
            final String substring = anotherString.substring(anotherString.indexOf(64) + 1);
            if (s.indexOf(64) != -1) {
                if (!anotherString.equalsIgnoreCase(s)) {
                    return;
                }
            }
            else if (s.startsWith(".")) {
                if (!this.withinDomain(substring, s)) {
                    return;
                }
            }
            else if (!substring.equalsIgnoreCase(s)) {
                return;
            }
        }
        set.add(anotherString);
    }
    
    private byte[] ipWithSubnetMask(final byte[] array, final byte[] array2) {
        final int length = array.length;
        final byte[] array3 = new byte[length * 2];
        System.arraycopy(array, 0, array3, 0, length);
        System.arraycopy(array2, 0, array3, length, length);
        return array3;
    }
    
    private boolean isIPConstrained(final byte[] array, final byte[] array2) {
        final int length = array.length;
        final int n = array2.length / 2;
        int i = 0;
        if (length != n) {
            return false;
        }
        final byte[] array3 = new byte[length];
        System.arraycopy(array2, length, array3, 0, length);
        final byte[] array4 = new byte[length];
        final byte[] array5 = new byte[length];
        while (i < length) {
            array4[i] = (byte)(array2[i] & array3[i]);
            array5[i] = (byte)(array[i] & array3[i]);
            ++i;
        }
        return Arrays.areEqual(array4, array5);
    }
    
    private boolean isUriConstrained(String hostFromURL, final String anotherString) {
        hostFromURL = extractHostFromURL(hostFromURL);
        if (!anotherString.startsWith(".")) {
            if (hostFromURL.equalsIgnoreCase(anotherString)) {
                return true;
            }
        }
        else if (this.withinDomain(hostFromURL, anotherString)) {
            return true;
        }
        return false;
    }
    
    private static byte[] max(final byte[] array, final byte[] array2) {
        for (int i = 0; i < array.length; ++i) {
            if ((array[i] & 0xFFFF) > (0xFFFF & array2[i])) {
                return array;
            }
        }
        return array2;
    }
    
    private static byte[] min(final byte[] array, final byte[] array2) {
        for (int i = 0; i < array.length; ++i) {
            if ((array[i] & 0xFFFF) < (0xFFFF & array2[i])) {
                return array;
            }
        }
        return array2;
    }
    
    private byte[][] minMaxIPs(final byte[] array, final byte[] array2, final byte[] array3, final byte[] array4) {
        final int length = array.length;
        final byte[] array5 = new byte[length];
        final byte[] array6 = new byte[length];
        final byte[] array7 = new byte[length];
        final byte[] array8 = new byte[length];
        for (int i = 0; i < length; ++i) {
            array5[i] = (byte)(array[i] & array2[i]);
            array6[i] = (byte)((array[i] & array2[i]) | array2[i]);
            array7[i] = (byte)(array3[i] & array4[i]);
            array8[i] = (byte)((array3[i] & array4[i]) | array4[i]);
        }
        return new byte[][] { array5, array6, array7, array8 };
    }
    
    private static byte[] or(final byte[] array, final byte[] array2) {
        final byte[] array3 = new byte[array.length];
        for (int i = 0; i < array.length; ++i) {
            array3[i] = (byte)(array[i] | array2[i]);
        }
        return array3;
    }
    
    private boolean otherNameIsConstrained(final OtherName otherName, final OtherName otherName2) {
        return otherName2.equals(otherName);
    }
    
    private String stringifyIP(final byte[] array) {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < array.length / 2; ++i) {
            if (sb.length() > 0) {
                sb.append(".");
            }
            sb.append(Integer.toString(array[i] & 0xFF));
        }
        sb.append("/");
        int j = array.length / 2;
        int n = 1;
        while (j < array.length) {
            if (n != 0) {
                n = 0;
            }
            else {
                sb.append(".");
            }
            sb.append(Integer.toString(array[j] & 0xFF));
            ++j;
        }
        return sb.toString();
    }
    
    private String stringifyIPCollection(final Set set) {
        final StringBuilder sb = new StringBuilder();
        sb.append("[");
        final Iterator<byte[]> iterator = set.iterator();
        while (iterator.hasNext()) {
            if (sb.length() > 1) {
                sb.append(",");
            }
            sb.append(this.stringifyIP(iterator.next()));
        }
        sb.append("]");
        return sb.toString();
    }
    
    private String stringifyOtherNameCollection(Set iterator) {
        final StringBuilder sb = new StringBuilder();
        sb.append("[");
        iterator = ((Set)iterator).iterator();
        while (iterator.hasNext()) {
            if (sb.length() > 1) {
                sb.append(",");
            }
            final OtherName instance = OtherName.getInstance(iterator.next());
            sb.append(instance.getTypeID().getId());
            sb.append(":");
            try {
                sb.append(Hex.toHexString(instance.getValue().toASN1Primitive().getEncoded()));
            }
            catch (IOException ex) {
                sb.append(ex.toString());
            }
        }
        sb.append("]");
        return sb.toString();
    }
    
    private Set unionDN(final Set set, final ASN1Sequence asn1Sequence) {
        if (!set.isEmpty()) {
            final HashSet<ASN1Sequence> set2 = new HashSet<ASN1Sequence>();
            for (final ASN1Sequence asn1Sequence2 : set) {
                if (withinDNSubtree(asn1Sequence, asn1Sequence2)) {
                    set2.add(asn1Sequence2);
                }
                else {
                    if (!withinDNSubtree(asn1Sequence2, asn1Sequence)) {
                        set2.add(asn1Sequence2);
                    }
                    set2.add(asn1Sequence);
                }
            }
            return set2;
        }
        if (asn1Sequence == null) {
            return set;
        }
        set.add(asn1Sequence);
        return set;
    }
    
    private Set unionDNS(final Set set, final String s) {
        if (!set.isEmpty()) {
            final HashSet<String> set2 = new HashSet<String>();
            for (final String s2 : set) {
                if (!this.withinDomain(s2, s)) {
                    final boolean withinDomain = this.withinDomain(s, s2);
                    set2.add(s2);
                    if (withinDomain) {
                        continue;
                    }
                }
                set2.add(s);
            }
            return set2;
        }
        if (s == null) {
            return set;
        }
        set.add(s);
        return set;
    }
    
    private Set unionEmail(final Set set, final String s) {
        if (!set.isEmpty()) {
            final HashSet set2 = new HashSet();
            final Iterator<String> iterator = set.iterator();
            while (iterator.hasNext()) {
                this.unionEmail(iterator.next(), s, set2);
            }
            return set2;
        }
        if (s == null) {
            return set;
        }
        set.add(s);
        return set;
    }
    
    private void unionEmail(final String anotherString, final String s, final Set set) {
        Label_0256: {
            Label_0248: {
                Label_0239: {
                    if (anotherString.indexOf(64) != -1) {
                        final String substring = anotherString.substring(anotherString.indexOf(64) + 1);
                        if (s.indexOf(64) != -1) {
                            if (anotherString.equalsIgnoreCase(s)) {
                                break Label_0239;
                            }
                            break Label_0248;
                        }
                        else if (s.startsWith(".")) {
                            if (!this.withinDomain(substring, s)) {
                                break Label_0248;
                            }
                        }
                        else if (!substring.equalsIgnoreCase(s)) {
                            break Label_0248;
                        }
                    }
                    else if (anotherString.startsWith(".")) {
                        if (s.indexOf(64) != -1) {
                            if (this.withinDomain(s.substring(anotherString.indexOf(64) + 1), anotherString)) {
                                break Label_0239;
                            }
                            break Label_0248;
                        }
                        else if (s.startsWith(".")) {
                            if (!this.withinDomain(anotherString, s)) {
                                if (!anotherString.equalsIgnoreCase(s)) {
                                    if (this.withinDomain(s, anotherString)) {
                                        break Label_0239;
                                    }
                                    break Label_0248;
                                }
                            }
                        }
                        else {
                            if (this.withinDomain(s, anotherString)) {
                                break Label_0239;
                            }
                            break Label_0248;
                        }
                    }
                    else if (s.indexOf(64) != -1) {
                        if (s.substring(anotherString.indexOf(64) + 1).equalsIgnoreCase(anotherString)) {
                            break Label_0239;
                        }
                        break Label_0248;
                    }
                    else if (s.startsWith(".")) {
                        if (!this.withinDomain(anotherString, s)) {
                            break Label_0248;
                        }
                    }
                    else {
                        if (anotherString.equalsIgnoreCase(s)) {
                            break Label_0239;
                        }
                        break Label_0248;
                    }
                    break Label_0256;
                }
                set.add(anotherString);
                return;
            }
            set.add(anotherString);
        }
        set.add(s);
    }
    
    private Set unionIP(final Set set, final byte[] array) {
        if (!set.isEmpty()) {
            final HashSet set2 = new HashSet();
            final Iterator<byte[]> iterator = set.iterator();
            while (iterator.hasNext()) {
                set2.addAll(this.unionIPRange(iterator.next(), array));
            }
            return set2;
        }
        if (array == null) {
            return set;
        }
        set.add(array);
        return set;
    }
    
    private Set unionIPRange(final byte[] array, final byte[] array2) {
        final HashSet<byte[]> set = new HashSet<byte[]>();
        final boolean equal = Arrays.areEqual(array, array2);
        set.add(array);
        if (equal) {
            return set;
        }
        set.add(array2);
        return set;
    }
    
    private Set unionOtherName(final Set c, final OtherName otherName) {
        HashSet<OtherName> set;
        if (c != null) {
            set = new HashSet<OtherName>(c);
        }
        else {
            set = new HashSet<OtherName>();
        }
        set.add(otherName);
        return set;
    }
    
    private Set unionURI(final Set set, final String s) {
        if (!set.isEmpty()) {
            final HashSet set2 = new HashSet();
            final Iterator<String> iterator = set.iterator();
            while (iterator.hasNext()) {
                this.unionURI(iterator.next(), s, set2);
            }
            return set2;
        }
        if (s == null) {
            return set;
        }
        set.add(s);
        return set;
    }
    
    private void unionURI(final String anotherString, final String s, final Set set) {
        Label_0256: {
            Label_0248: {
                Label_0239: {
                    if (anotherString.indexOf(64) != -1) {
                        final String substring = anotherString.substring(anotherString.indexOf(64) + 1);
                        if (s.indexOf(64) != -1) {
                            if (anotherString.equalsIgnoreCase(s)) {
                                break Label_0239;
                            }
                            break Label_0248;
                        }
                        else if (s.startsWith(".")) {
                            if (!this.withinDomain(substring, s)) {
                                break Label_0248;
                            }
                        }
                        else if (!substring.equalsIgnoreCase(s)) {
                            break Label_0248;
                        }
                    }
                    else if (anotherString.startsWith(".")) {
                        if (s.indexOf(64) != -1) {
                            if (this.withinDomain(s.substring(anotherString.indexOf(64) + 1), anotherString)) {
                                break Label_0239;
                            }
                            break Label_0248;
                        }
                        else if (s.startsWith(".")) {
                            if (!this.withinDomain(anotherString, s)) {
                                if (!anotherString.equalsIgnoreCase(s)) {
                                    if (this.withinDomain(s, anotherString)) {
                                        break Label_0239;
                                    }
                                    break Label_0248;
                                }
                            }
                        }
                        else {
                            if (this.withinDomain(s, anotherString)) {
                                break Label_0239;
                            }
                            break Label_0248;
                        }
                    }
                    else if (s.indexOf(64) != -1) {
                        if (s.substring(anotherString.indexOf(64) + 1).equalsIgnoreCase(anotherString)) {
                            break Label_0239;
                        }
                        break Label_0248;
                    }
                    else if (s.startsWith(".")) {
                        if (!this.withinDomain(anotherString, s)) {
                            break Label_0248;
                        }
                    }
                    else {
                        if (anotherString.equalsIgnoreCase(s)) {
                            break Label_0239;
                        }
                        break Label_0248;
                    }
                    break Label_0256;
                }
                set.add(anotherString);
                return;
            }
            set.add(anotherString);
        }
        set.add(s);
    }
    
    private static boolean withinDNSubtree(final ASN1Sequence asn1Sequence, final ASN1Sequence asn1Sequence2) {
        if (asn1Sequence2.size() < 1) {
            return false;
        }
        if (asn1Sequence2.size() > asn1Sequence.size()) {
            return false;
        }
        final RDN instance = RDN.getInstance(asn1Sequence2.getObjectAt(0));
        int n;
        int i = n = 0;
        while (true) {
            while (i < asn1Sequence.size()) {
                if (RDN.getInstance(asn1Sequence.getObjectAt(i)).equals(instance)) {
                    if (asn1Sequence2.size() > asn1Sequence.size() - i) {
                        return false;
                    }
                    for (int j = 0; j < asn1Sequence2.size(); ++j) {
                        final RDN instance2 = RDN.getInstance(asn1Sequence2.getObjectAt(j));
                        final RDN instance3 = RDN.getInstance(asn1Sequence.getObjectAt(i + j));
                        if (instance2.size() != instance3.size()) {
                            return false;
                        }
                        if (!instance2.getFirst().getType().equals(instance3.getFirst().getType())) {
                            return false;
                        }
                        if (instance2.size() == 1 && instance2.getFirst().getType().equals(RFC4519Style.serialNumber)) {
                            if (!instance3.getFirst().getValue().toString().startsWith(instance2.getFirst().getValue().toString())) {
                                return false;
                            }
                        }
                        else if (!IETFUtils.rDNAreEqual(instance2, instance3)) {
                            return false;
                        }
                    }
                    return true;
                }
                else {
                    n = i;
                    ++i;
                }
            }
            i = n;
            continue;
        }
    }
    
    private boolean withinDomain(final String s, final String s2) {
        String substring = s2;
        if (s2.startsWith(".")) {
            substring = s2.substring(1);
        }
        final String[] split = Strings.split(substring, '.');
        final String[] split2 = Strings.split(s, '.');
        if (split2.length <= split.length) {
            return false;
        }
        final int n = split2.length - split.length;
        for (int i = -1; i < split.length; ++i) {
            if (i == -1) {
                if (split2[i + n].equals("")) {
                    return false;
                }
            }
            else if (!split[i].equalsIgnoreCase(split2[i + n])) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public void addExcludedSubtree(final GeneralSubtree generalSubtree) {
        final GeneralName base = generalSubtree.getBase();
        final int tagNo = base.getTagNo();
        if (tagNo == 0) {
            this.excludedSubtreesOtherName = this.unionOtherName(this.excludedSubtreesOtherName, OtherName.getInstance(base.getName()));
            return;
        }
        if (tagNo == 1) {
            this.excludedSubtreesEmail = this.unionEmail(this.excludedSubtreesEmail, this.extractNameAsString(base));
            return;
        }
        if (tagNo == 2) {
            this.excludedSubtreesDNS = this.unionDNS(this.excludedSubtreesDNS, this.extractNameAsString(base));
            return;
        }
        if (tagNo == 4) {
            this.excludedSubtreesDN = this.unionDN(this.excludedSubtreesDN, (ASN1Sequence)base.getName().toASN1Primitive());
            return;
        }
        if (tagNo == 6) {
            this.excludedSubtreesURI = this.unionURI(this.excludedSubtreesURI, this.extractNameAsString(base));
            return;
        }
        if (tagNo == 7) {
            this.excludedSubtreesIP = this.unionIP(this.excludedSubtreesIP, ASN1OctetString.getInstance(base.getName()).getOctets());
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown tag encountered: ");
        sb.append(base.getTagNo());
        throw new IllegalStateException(sb.toString());
    }
    
    @Override
    public void checkExcluded(final GeneralName generalName) throws NameConstraintValidatorException {
        final int tagNo = generalName.getTagNo();
        if (tagNo == 0) {
            this.checkExcludedOtherName(this.excludedSubtreesOtherName, OtherName.getInstance(generalName.getName()));
            return;
        }
        if (tagNo == 1) {
            this.checkExcludedEmail(this.excludedSubtreesEmail, this.extractNameAsString(generalName));
            return;
        }
        if (tagNo == 2) {
            this.checkExcludedDNS(this.excludedSubtreesDNS, DERIA5String.getInstance(generalName.getName()).getString());
            return;
        }
        if (tagNo == 4) {
            this.checkExcludedDN(X500Name.getInstance(generalName.getName()));
            return;
        }
        if (tagNo == 6) {
            this.checkExcludedURI(this.excludedSubtreesURI, DERIA5String.getInstance(generalName.getName()).getString());
            return;
        }
        if (tagNo != 7) {
            return;
        }
        this.checkExcludedIP(this.excludedSubtreesIP, ASN1OctetString.getInstance(generalName.getName()).getOctets());
    }
    
    public void checkExcludedDN(final X500Name x500Name) throws NameConstraintValidatorException {
        this.checkExcludedDN(this.excludedSubtreesDN, ASN1Sequence.getInstance(x500Name));
    }
    
    @Override
    public void checkPermitted(final GeneralName generalName) throws NameConstraintValidatorException {
        final int tagNo = generalName.getTagNo();
        if (tagNo == 0) {
            this.checkPermittedOtherName(this.permittedSubtreesOtherName, OtherName.getInstance(generalName.getName()));
            return;
        }
        if (tagNo == 1) {
            this.checkPermittedEmail(this.permittedSubtreesEmail, this.extractNameAsString(generalName));
            return;
        }
        if (tagNo == 2) {
            this.checkPermittedDNS(this.permittedSubtreesDNS, DERIA5String.getInstance(generalName.getName()).getString());
            return;
        }
        if (tagNo == 4) {
            this.checkPermittedDN(X500Name.getInstance(generalName.getName()));
            return;
        }
        if (tagNo == 6) {
            this.checkPermittedURI(this.permittedSubtreesURI, DERIA5String.getInstance(generalName.getName()).getString());
            return;
        }
        if (tagNo != 7) {
            return;
        }
        this.checkPermittedIP(this.permittedSubtreesIP, ASN1OctetString.getInstance(generalName.getName()).getOctets());
    }
    
    public void checkPermittedDN(final X500Name x500Name) throws NameConstraintValidatorException {
        this.checkPermittedDN(this.permittedSubtreesDN, ASN1Sequence.getInstance(x500Name.toASN1Primitive()));
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof PKIXNameConstraintValidator;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final PKIXNameConstraintValidator pkixNameConstraintValidator = (PKIXNameConstraintValidator)o;
        boolean b3 = b2;
        if (this.collectionsAreEqual(pkixNameConstraintValidator.excludedSubtreesDN, this.excludedSubtreesDN)) {
            b3 = b2;
            if (this.collectionsAreEqual(pkixNameConstraintValidator.excludedSubtreesDNS, this.excludedSubtreesDNS)) {
                b3 = b2;
                if (this.collectionsAreEqual(pkixNameConstraintValidator.excludedSubtreesEmail, this.excludedSubtreesEmail)) {
                    b3 = b2;
                    if (this.collectionsAreEqual(pkixNameConstraintValidator.excludedSubtreesIP, this.excludedSubtreesIP)) {
                        b3 = b2;
                        if (this.collectionsAreEqual(pkixNameConstraintValidator.excludedSubtreesURI, this.excludedSubtreesURI)) {
                            b3 = b2;
                            if (this.collectionsAreEqual(pkixNameConstraintValidator.excludedSubtreesOtherName, this.excludedSubtreesOtherName)) {
                                b3 = b2;
                                if (this.collectionsAreEqual(pkixNameConstraintValidator.permittedSubtreesDN, this.permittedSubtreesDN)) {
                                    b3 = b2;
                                    if (this.collectionsAreEqual(pkixNameConstraintValidator.permittedSubtreesDNS, this.permittedSubtreesDNS)) {
                                        b3 = b2;
                                        if (this.collectionsAreEqual(pkixNameConstraintValidator.permittedSubtreesEmail, this.permittedSubtreesEmail)) {
                                            b3 = b2;
                                            if (this.collectionsAreEqual(pkixNameConstraintValidator.permittedSubtreesIP, this.permittedSubtreesIP)) {
                                                b3 = b2;
                                                if (this.collectionsAreEqual(pkixNameConstraintValidator.permittedSubtreesURI, this.permittedSubtreesURI)) {
                                                    b3 = b2;
                                                    if (this.collectionsAreEqual(pkixNameConstraintValidator.permittedSubtreesOtherName, this.permittedSubtreesOtherName)) {
                                                        b3 = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        return this.hashCollection(this.excludedSubtreesDN) + this.hashCollection(this.excludedSubtreesDNS) + this.hashCollection(this.excludedSubtreesEmail) + this.hashCollection(this.excludedSubtreesIP) + this.hashCollection(this.excludedSubtreesURI) + this.hashCollection(this.excludedSubtreesOtherName) + this.hashCollection(this.permittedSubtreesDN) + this.hashCollection(this.permittedSubtreesDNS) + this.hashCollection(this.permittedSubtreesEmail) + this.hashCollection(this.permittedSubtreesIP) + this.hashCollection(this.permittedSubtreesURI) + this.hashCollection(this.permittedSubtreesOtherName);
    }
    
    @Override
    public void intersectEmptyPermittedSubtree(final int i) {
        if (i == 0) {
            this.permittedSubtreesOtherName = new HashSet();
            return;
        }
        if (i == 1) {
            this.permittedSubtreesEmail = new HashSet();
            return;
        }
        if (i == 2) {
            this.permittedSubtreesDNS = new HashSet();
            return;
        }
        if (i == 4) {
            this.permittedSubtreesDN = new HashSet();
            return;
        }
        if (i == 6) {
            this.permittedSubtreesURI = new HashSet();
            return;
        }
        if (i == 7) {
            this.permittedSubtreesIP = new HashSet();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown tag encountered: ");
        sb.append(i);
        throw new IllegalStateException(sb.toString());
    }
    
    @Override
    public void intersectPermittedSubtree(final GeneralSubtree generalSubtree) {
        this.intersectPermittedSubtree(new GeneralSubtree[] { generalSubtree });
    }
    
    @Override
    public void intersectPermittedSubtree(final GeneralSubtree[] array) {
        final HashMap<Object, HashSet<GeneralSubtree>> hashMap = new HashMap<Object, HashSet<GeneralSubtree>>();
        for (int i = 0; i != array.length; ++i) {
            final GeneralSubtree generalSubtree = array[i];
            final Integer value = Integers.valueOf(generalSubtree.getBase().getTagNo());
            if (hashMap.get(value) == null) {
                hashMap.put(value, new HashSet<GeneralSubtree>());
            }
            hashMap.get(value).add(generalSubtree);
        }
        for (final Map.Entry<Integer, HashSet<GeneralSubtree>> entry : hashMap.entrySet()) {
            final int intValue = entry.getKey();
            if (intValue != 0) {
                if (intValue != 1) {
                    if (intValue != 2) {
                        if (intValue != 4) {
                            if (intValue != 6) {
                                if (intValue != 7) {
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append("Unknown tag encountered: ");
                                    sb.append(intValue);
                                    throw new IllegalStateException(sb.toString());
                                }
                                this.permittedSubtreesIP = this.intersectIP(this.permittedSubtreesIP, entry.getValue());
                            }
                            else {
                                this.permittedSubtreesURI = this.intersectURI(this.permittedSubtreesURI, entry.getValue());
                            }
                        }
                        else {
                            this.permittedSubtreesDN = this.intersectDN(this.permittedSubtreesDN, entry.getValue());
                        }
                    }
                    else {
                        this.permittedSubtreesDNS = this.intersectDNS(this.permittedSubtreesDNS, entry.getValue());
                    }
                }
                else {
                    this.permittedSubtreesEmail = this.intersectEmail(this.permittedSubtreesEmail, entry.getValue());
                }
            }
            else {
                this.permittedSubtreesOtherName = this.intersectOtherName(this.permittedSubtreesOtherName, entry.getValue());
            }
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        this.addLine(sb, "permitted:");
        if (this.permittedSubtreesDN != null) {
            this.addLine(sb, "DN:");
            this.addLine(sb, this.permittedSubtreesDN.toString());
        }
        if (this.permittedSubtreesDNS != null) {
            this.addLine(sb, "DNS:");
            this.addLine(sb, this.permittedSubtreesDNS.toString());
        }
        if (this.permittedSubtreesEmail != null) {
            this.addLine(sb, "Email:");
            this.addLine(sb, this.permittedSubtreesEmail.toString());
        }
        if (this.permittedSubtreesURI != null) {
            this.addLine(sb, "URI:");
            this.addLine(sb, this.permittedSubtreesURI.toString());
        }
        if (this.permittedSubtreesIP != null) {
            this.addLine(sb, "IP:");
            this.addLine(sb, this.stringifyIPCollection(this.permittedSubtreesIP));
        }
        if (this.permittedSubtreesOtherName != null) {
            this.addLine(sb, "OtherName:");
            this.addLine(sb, this.stringifyOtherNameCollection(this.permittedSubtreesOtherName));
        }
        this.addLine(sb, "excluded:");
        if (!this.excludedSubtreesDN.isEmpty()) {
            this.addLine(sb, "DN:");
            this.addLine(sb, this.excludedSubtreesDN.toString());
        }
        if (!this.excludedSubtreesDNS.isEmpty()) {
            this.addLine(sb, "DNS:");
            this.addLine(sb, this.excludedSubtreesDNS.toString());
        }
        if (!this.excludedSubtreesEmail.isEmpty()) {
            this.addLine(sb, "Email:");
            this.addLine(sb, this.excludedSubtreesEmail.toString());
        }
        if (!this.excludedSubtreesURI.isEmpty()) {
            this.addLine(sb, "URI:");
            this.addLine(sb, this.excludedSubtreesURI.toString());
        }
        if (!this.excludedSubtreesIP.isEmpty()) {
            this.addLine(sb, "IP:");
            this.addLine(sb, this.stringifyIPCollection(this.excludedSubtreesIP));
        }
        if (!this.excludedSubtreesOtherName.isEmpty()) {
            this.addLine(sb, "OtherName:");
            this.addLine(sb, this.stringifyOtherNameCollection(this.excludedSubtreesOtherName));
        }
        return sb.toString();
    }
}
