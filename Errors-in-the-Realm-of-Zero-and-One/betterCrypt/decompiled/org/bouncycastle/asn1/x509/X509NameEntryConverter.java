// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x509;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.io.IOException;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.DERPrintableString;

public abstract class X509NameEntryConverter
{
    protected boolean canBePrintable(final String s) {
        return DERPrintableString.isPrintableString(s);
    }
    
    protected ASN1Primitive convertHexEncoded(final String s, final int n) throws IOException {
        return ASN1Primitive.fromByteArray(Hex.decodeStrict(s, n, s.length() - n));
    }
    
    public abstract ASN1Primitive getConvertedValue(final ASN1ObjectIdentifier p0, final String p1);
}
