// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x509;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Object;

public class OtherName extends ASN1Object
{
    private final ASN1ObjectIdentifier typeID;
    private final ASN1Encodable value;
    
    public OtherName(final ASN1ObjectIdentifier typeID, final ASN1Encodable value) {
        this.typeID = typeID;
        this.value = value;
    }
    
    private OtherName(final ASN1Sequence asn1Sequence) {
        this.typeID = ASN1ObjectIdentifier.getInstance(asn1Sequence.getObjectAt(0));
        this.value = ASN1TaggedObject.getInstance(asn1Sequence.getObjectAt(1)).getObject();
    }
    
    public static OtherName getInstance(final Object o) {
        if (o instanceof OtherName) {
            return (OtherName)o;
        }
        if (o != null) {
            return new OtherName(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public ASN1ObjectIdentifier getTypeID() {
        return this.typeID;
    }
    
    public ASN1Encodable getValue() {
        return this.value;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(2);
        asn1EncodableVector.add(this.typeID);
        asn1EncodableVector.add(new DERTaggedObject(true, 0, this.value));
        return new DERSequence(asn1EncodableVector);
    }
}
