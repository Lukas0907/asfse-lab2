// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x509.sigi;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.DERPrintableString;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x500.DirectoryString;
import java.math.BigInteger;
import org.bouncycastle.asn1.ASN1GeneralizedTime;
import org.bouncycastle.asn1.ASN1Object;

public class PersonalData extends ASN1Object
{
    private ASN1GeneralizedTime dateOfBirth;
    private String gender;
    private BigInteger nameDistinguisher;
    private NameOrPseudonym nameOrPseudonym;
    private DirectoryString placeOfBirth;
    private DirectoryString postalAddress;
    
    private PersonalData(final ASN1Sequence asn1Sequence) {
        if (asn1Sequence.size() >= 1) {
            final Enumeration objects = asn1Sequence.getObjects();
            this.nameOrPseudonym = NameOrPseudonym.getInstance(objects.nextElement());
            while (objects.hasMoreElements()) {
                final ASN1TaggedObject instance = ASN1TaggedObject.getInstance(objects.nextElement());
                final int tagNo = instance.getTagNo();
                if (tagNo != 0) {
                    if (tagNo != 1) {
                        if (tagNo != 2) {
                            if (tagNo != 3) {
                                if (tagNo != 4) {
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append("Bad tag number: ");
                                    sb.append(instance.getTagNo());
                                    throw new IllegalArgumentException(sb.toString());
                                }
                                this.postalAddress = DirectoryString.getInstance(instance, true);
                            }
                            else {
                                this.gender = DERPrintableString.getInstance(instance, false).getString();
                            }
                        }
                        else {
                            this.placeOfBirth = DirectoryString.getInstance(instance, true);
                        }
                    }
                    else {
                        this.dateOfBirth = ASN1GeneralizedTime.getInstance(instance, false);
                    }
                }
                else {
                    this.nameDistinguisher = ASN1Integer.getInstance(instance, false).getValue();
                }
            }
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Bad sequence size: ");
        sb2.append(asn1Sequence.size());
        throw new IllegalArgumentException(sb2.toString());
    }
    
    public PersonalData(final NameOrPseudonym nameOrPseudonym, final BigInteger nameDistinguisher, final ASN1GeneralizedTime dateOfBirth, final DirectoryString placeOfBirth, final String gender, final DirectoryString postalAddress) {
        this.nameOrPseudonym = nameOrPseudonym;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.nameDistinguisher = nameDistinguisher;
        this.postalAddress = postalAddress;
        this.placeOfBirth = placeOfBirth;
    }
    
    public static PersonalData getInstance(final Object o) {
        if (o == null || o instanceof PersonalData) {
            return (PersonalData)o;
        }
        if (o instanceof ASN1Sequence) {
            return new PersonalData((ASN1Sequence)o);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("illegal object in getInstance: ");
        sb.append(o.getClass().getName());
        throw new IllegalArgumentException(sb.toString());
    }
    
    public ASN1GeneralizedTime getDateOfBirth() {
        return this.dateOfBirth;
    }
    
    public String getGender() {
        return this.gender;
    }
    
    public BigInteger getNameDistinguisher() {
        return this.nameDistinguisher;
    }
    
    public NameOrPseudonym getNameOrPseudonym() {
        return this.nameOrPseudonym;
    }
    
    public DirectoryString getPlaceOfBirth() {
        return this.placeOfBirth;
    }
    
    public DirectoryString getPostalAddress() {
        return this.postalAddress;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(6);
        asn1EncodableVector.add(this.nameOrPseudonym);
        final BigInteger nameDistinguisher = this.nameDistinguisher;
        if (nameDistinguisher != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 0, new ASN1Integer(nameDistinguisher)));
        }
        final ASN1GeneralizedTime dateOfBirth = this.dateOfBirth;
        if (dateOfBirth != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 1, dateOfBirth));
        }
        final DirectoryString placeOfBirth = this.placeOfBirth;
        if (placeOfBirth != null) {
            asn1EncodableVector.add(new DERTaggedObject(true, 2, placeOfBirth));
        }
        final String gender = this.gender;
        if (gender != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 3, new DERPrintableString(gender, true)));
        }
        final DirectoryString postalAddress = this.postalAddress;
        if (postalAddress != null) {
            asn1EncodableVector.add(new DERTaggedObject(true, 4, postalAddress));
        }
        return new DERSequence(asn1EncodableVector);
    }
}
