// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x509;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Object;

public class CertificatePolicies extends ASN1Object
{
    private final PolicyInformation[] policyInformation;
    
    private CertificatePolicies(final ASN1Sequence asn1Sequence) {
        this.policyInformation = new PolicyInformation[asn1Sequence.size()];
        for (int i = 0; i != asn1Sequence.size(); ++i) {
            this.policyInformation[i] = PolicyInformation.getInstance(asn1Sequence.getObjectAt(i));
        }
    }
    
    public CertificatePolicies(final PolicyInformation policyInformation) {
        this.policyInformation = new PolicyInformation[] { policyInformation };
    }
    
    public CertificatePolicies(final PolicyInformation[] array) {
        this.policyInformation = this.copyPolicyInfo(array);
    }
    
    private PolicyInformation[] copyPolicyInfo(final PolicyInformation[] array) {
        final PolicyInformation[] array2 = new PolicyInformation[array.length];
        System.arraycopy(array, 0, array2, 0, array.length);
        return array2;
    }
    
    public static CertificatePolicies fromExtensions(final Extensions extensions) {
        return getInstance(extensions.getExtensionParsedValue(Extension.certificatePolicies));
    }
    
    public static CertificatePolicies getInstance(final Object o) {
        if (o instanceof CertificatePolicies) {
            return (CertificatePolicies)o;
        }
        if (o != null) {
            return new CertificatePolicies(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public static CertificatePolicies getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        return getInstance(ASN1Sequence.getInstance(asn1TaggedObject, b));
    }
    
    public PolicyInformation getPolicyInformation(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        int n = 0;
        while (true) {
            final PolicyInformation[] policyInformation = this.policyInformation;
            if (n == policyInformation.length) {
                return null;
            }
            if (asn1ObjectIdentifier.equals(policyInformation[n].getPolicyIdentifier())) {
                return this.policyInformation[n];
            }
            ++n;
        }
    }
    
    public PolicyInformation[] getPolicyInformation() {
        return this.copyPolicyInfo(this.policyInformation);
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        return new DERSequence(this.policyInformation);
    }
    
    @Override
    public String toString() {
        final StringBuffer obj = new StringBuffer();
        for (int i = 0; i < this.policyInformation.length; ++i) {
            if (obj.length() != 0) {
                obj.append(", ");
            }
            obj.append(this.policyInformation[i]);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("CertificatePolicies: [");
        sb.append((Object)obj);
        sb.append("]");
        return sb.toString();
    }
}
