// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x509;

public class NameConstraintValidatorException extends Exception
{
    public NameConstraintValidatorException(final String message) {
        super(message);
    }
}
