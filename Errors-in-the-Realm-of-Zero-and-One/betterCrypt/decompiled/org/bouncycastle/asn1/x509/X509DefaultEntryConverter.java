// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x509;

import java.io.IOException;
import org.bouncycastle.asn1.DERGeneralizedTime;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERUTF8String;
import org.bouncycastle.asn1.DERPrintableString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;

public class X509DefaultEntryConverter extends X509NameEntryConverter
{
    @Override
    public ASN1Primitive getConvertedValue(final ASN1ObjectIdentifier asn1ObjectIdentifier, final String s) {
        Label_0062: {
            if (s.length() == 0 || s.charAt(0) != '#') {
                break Label_0062;
            }
        Label_0026_Outer:
            while (true) {
                while (true) {
                    try {
                        return this.convertHexEncoded(s, 1);
                        Label_0183: {
                            final String substring;
                            return new DERPrintableString(substring);
                        }
                        String substring = s;
                        // iftrue(Label_0089:, s.length() == 0)
                        // iftrue(Label_0183:, asn1ObjectIdentifier.equals((ASN1Primitive)X509Name.C) || asn1ObjectIdentifier.equals((ASN1Primitive)X509Name.SN) || asn1ObjectIdentifier.equals((ASN1Primitive)X509Name.DN_QUALIFIER) || asn1ObjectIdentifier.equals((ASN1Primitive)X509Name.TELEPHONE_NUMBER))
                        // iftrue(Label_0192:, asn1ObjectIdentifier.equals((ASN1Primitive)X509Name.EmailAddress) || asn1ObjectIdentifier.equals((ASN1Primitive)X509Name.DC))
                        // iftrue(Label_0089:, s.charAt(0) != '\\')
                        Label_0112: {
                            while (true) {
                                Block_4: {
                                    break Block_4;
                                    Label_0131:
                                    return new DERUTF8String(substring);
                                    break Label_0112;
                                }
                                substring = s;
                                Block_5: {
                                    break Block_5;
                                    Label_0192:
                                    return new DERIA5String(substring);
                                }
                                substring = s.substring(1);
                                continue Label_0026_Outer;
                            }
                            final StringBuilder sb = new StringBuilder();
                            sb.append("can't recode value for oid ");
                            sb.append(asn1ObjectIdentifier.getId());
                            throw new RuntimeException(sb.toString());
                        }
                        // iftrue(Label_0131:, !asn1ObjectIdentifier.equals((ASN1Primitive)X509Name.DATE_OF_BIRTH))
                        return new DERGeneralizedTime(substring);
                    }
                    catch (IOException ex) {}
                    continue;
                }
            }
        }
    }
}
