// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x509;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.util.Arrays;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Object;

public class DigestInfo extends ASN1Object
{
    private AlgorithmIdentifier algId;
    private byte[] digest;
    
    public DigestInfo(final ASN1Sequence asn1Sequence) {
        final Enumeration objects = asn1Sequence.getObjects();
        this.algId = AlgorithmIdentifier.getInstance(objects.nextElement());
        this.digest = ASN1OctetString.getInstance(objects.nextElement()).getOctets();
    }
    
    public DigestInfo(final AlgorithmIdentifier algId, final byte[] array) {
        this.digest = Arrays.clone(array);
        this.algId = algId;
    }
    
    public static DigestInfo getInstance(final Object o) {
        if (o instanceof DigestInfo) {
            return (DigestInfo)o;
        }
        if (o != null) {
            return new DigestInfo(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public static DigestInfo getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        return getInstance(ASN1Sequence.getInstance(asn1TaggedObject, b));
    }
    
    public AlgorithmIdentifier getAlgorithmId() {
        return this.algId;
    }
    
    public byte[] getDigest() {
        return Arrays.clone(this.digest);
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(2);
        asn1EncodableVector.add(this.algId);
        asn1EncodableVector.add(new DEROctetString(this.digest));
        return new DERSequence(asn1EncodableVector);
    }
}
