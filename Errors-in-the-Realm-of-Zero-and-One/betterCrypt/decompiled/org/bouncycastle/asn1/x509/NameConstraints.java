// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.x509;

import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Object;

public class NameConstraints extends ASN1Object
{
    private GeneralSubtree[] excluded;
    private GeneralSubtree[] permitted;
    
    private NameConstraints(final ASN1Sequence asn1Sequence) {
        final Enumeration objects = asn1Sequence.getObjects();
        while (objects.hasMoreElements()) {
            final ASN1TaggedObject instance = ASN1TaggedObject.getInstance(objects.nextElement());
            final int tagNo = instance.getTagNo();
            if (tagNo != 0) {
                if (tagNo != 1) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unknown tag encountered: ");
                    sb.append(instance.getTagNo());
                    throw new IllegalArgumentException(sb.toString());
                }
                this.excluded = this.createArray(ASN1Sequence.getInstance(instance, false));
            }
            else {
                this.permitted = this.createArray(ASN1Sequence.getInstance(instance, false));
            }
        }
    }
    
    public NameConstraints(final GeneralSubtree[] array, final GeneralSubtree[] array2) {
        this.permitted = cloneSubtree(array);
        this.excluded = cloneSubtree(array2);
    }
    
    private static GeneralSubtree[] cloneSubtree(final GeneralSubtree[] array) {
        if (array != null) {
            final GeneralSubtree[] array2 = new GeneralSubtree[array.length];
            System.arraycopy(array, 0, array2, 0, array2.length);
            return array2;
        }
        return null;
    }
    
    private GeneralSubtree[] createArray(final ASN1Sequence asn1Sequence) {
        final GeneralSubtree[] array = new GeneralSubtree[asn1Sequence.size()];
        for (int i = 0; i != array.length; ++i) {
            array[i] = GeneralSubtree.getInstance(asn1Sequence.getObjectAt(i));
        }
        return array;
    }
    
    public static NameConstraints getInstance(final Object o) {
        if (o instanceof NameConstraints) {
            return (NameConstraints)o;
        }
        if (o != null) {
            return new NameConstraints(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public GeneralSubtree[] getExcludedSubtrees() {
        return cloneSubtree(this.excluded);
    }
    
    public GeneralSubtree[] getPermittedSubtrees() {
        return cloneSubtree(this.permitted);
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(2);
        final GeneralSubtree[] permitted = this.permitted;
        if (permitted != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 0, new DERSequence(permitted)));
        }
        final GeneralSubtree[] excluded = this.excluded;
        if (excluded != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 1, new DERSequence(excluded)));
        }
        return new DERSequence(asn1EncodableVector);
    }
}
