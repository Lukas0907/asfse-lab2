// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;
import java.io.OutputStream;

public class DLOutputStream extends ASN1OutputStream
{
    public DLOutputStream(final OutputStream outputStream) {
        super(outputStream);
    }
    
    @Override
    ASN1OutputStream getDLSubStream() {
        return this;
    }
    
    @Override
    void writePrimitive(final ASN1Primitive asn1Primitive, final boolean b) throws IOException {
        asn1Primitive.toDLObject().encode(this, b);
    }
}
