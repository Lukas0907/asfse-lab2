// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.dvcs;

import org.bouncycastle.asn1.ASN1Primitive;
import java.math.BigInteger;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Enumerated;
import org.bouncycastle.asn1.ASN1Object;

public class ServiceType extends ASN1Object
{
    public static final ServiceType CCPD;
    public static final ServiceType CPD;
    public static final ServiceType VPKC;
    public static final ServiceType VSD;
    private ASN1Enumerated value;
    
    static {
        CPD = new ServiceType(1);
        VSD = new ServiceType(2);
        VPKC = new ServiceType(3);
        CCPD = new ServiceType(4);
    }
    
    public ServiceType(final int n) {
        this.value = new ASN1Enumerated(n);
    }
    
    private ServiceType(final ASN1Enumerated value) {
        this.value = value;
    }
    
    public static ServiceType getInstance(final Object o) {
        if (o instanceof ServiceType) {
            return (ServiceType)o;
        }
        if (o != null) {
            return new ServiceType(ASN1Enumerated.getInstance(o));
        }
        return null;
    }
    
    public static ServiceType getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        return getInstance(ASN1Enumerated.getInstance(asn1TaggedObject, b));
    }
    
    public BigInteger getValue() {
        return this.value.getValue();
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        return this.value;
    }
    
    @Override
    public String toString() {
        final int intValueExact = this.value.intValueExact();
        final StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(intValueExact);
        String str;
        if (intValueExact == ServiceType.CPD.value.intValueExact()) {
            str = "(CPD)";
        }
        else if (intValueExact == ServiceType.VSD.value.intValueExact()) {
            str = "(VSD)";
        }
        else if (intValueExact == ServiceType.VPKC.value.intValueExact()) {
            str = "(VPKC)";
        }
        else if (intValueExact == ServiceType.CCPD.value.intValueExact()) {
            str = "(CCPD)";
        }
        else {
            str = "?";
        }
        sb.append(str);
        return sb.toString();
    }
}
