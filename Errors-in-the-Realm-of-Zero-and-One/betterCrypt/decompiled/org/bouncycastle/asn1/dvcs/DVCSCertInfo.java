// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.dvcs;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.x509.PolicyInformation;
import org.bouncycastle.asn1.x509.DigestInfo;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.asn1.cmp.PKIStatusInfo;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Object;

public class DVCSCertInfo extends ASN1Object
{
    private static final int DEFAULT_VERSION = 1;
    private static final int TAG_CERTS = 3;
    private static final int TAG_DV_STATUS = 0;
    private static final int TAG_POLICY = 1;
    private static final int TAG_REQ_SIGNATURE = 2;
    private ASN1Sequence certs;
    private DVCSRequestInformation dvReqInfo;
    private PKIStatusInfo dvStatus;
    private Extensions extensions;
    private DigestInfo messageImprint;
    private PolicyInformation policy;
    private ASN1Set reqSignature;
    private DVCSTime responseTime;
    private ASN1Integer serialNumber;
    private int version;
    
    private DVCSCertInfo(final ASN1Sequence p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokespecial   org/bouncycastle/asn1/ASN1Object.<init>:()V
        //     4: aload_0        
        //     5: iconst_1       
        //     6: putfield        org/bouncycastle/asn1/dvcs/DVCSCertInfo.version:I
        //     9: aload_1        
        //    10: iconst_0       
        //    11: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.getObjectAt:(I)Lorg/bouncycastle/asn1/ASN1Encodable;
        //    14: astore          5
        //    16: aload_0        
        //    17: aload           5
        //    19: invokestatic    org/bouncycastle/asn1/ASN1Integer.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/ASN1Integer;
        //    22: invokevirtual   org/bouncycastle/asn1/ASN1Integer.intValueExact:()I
        //    25: putfield        org/bouncycastle/asn1/dvcs/DVCSCertInfo.version:I
        //    28: aload_1        
        //    29: iconst_1       
        //    30: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.getObjectAt:(I)Lorg/bouncycastle/asn1/ASN1Encodable;
        //    33: astore          6
        //    35: aload           6
        //    37: astore          5
        //    39: iconst_2       
        //    40: istore_2       
        //    41: goto            46
        //    44: iconst_1       
        //    45: istore_2       
        //    46: aload_0        
        //    47: aload           5
        //    49: invokestatic    org/bouncycastle/asn1/dvcs/DVCSRequestInformation.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/dvcs/DVCSRequestInformation;
        //    52: putfield        org/bouncycastle/asn1/dvcs/DVCSCertInfo.dvReqInfo:Lorg/bouncycastle/asn1/dvcs/DVCSRequestInformation;
        //    55: iload_2        
        //    56: iconst_1       
        //    57: iadd           
        //    58: istore_3       
        //    59: aload_0        
        //    60: aload_1        
        //    61: iload_2        
        //    62: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.getObjectAt:(I)Lorg/bouncycastle/asn1/ASN1Encodable;
        //    65: invokestatic    org/bouncycastle/asn1/x509/DigestInfo.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/DigestInfo;
        //    68: putfield        org/bouncycastle/asn1/dvcs/DVCSCertInfo.messageImprint:Lorg/bouncycastle/asn1/x509/DigestInfo;
        //    71: iload_3        
        //    72: iconst_1       
        //    73: iadd           
        //    74: istore          4
        //    76: aload_0        
        //    77: aload_1        
        //    78: iload_3        
        //    79: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.getObjectAt:(I)Lorg/bouncycastle/asn1/ASN1Encodable;
        //    82: invokestatic    org/bouncycastle/asn1/ASN1Integer.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/ASN1Integer;
        //    85: putfield        org/bouncycastle/asn1/dvcs/DVCSCertInfo.serialNumber:Lorg/bouncycastle/asn1/ASN1Integer;
        //    88: iload           4
        //    90: iconst_1       
        //    91: iadd           
        //    92: istore_2       
        //    93: aload_0        
        //    94: aload_1        
        //    95: iload           4
        //    97: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.getObjectAt:(I)Lorg/bouncycastle/asn1/ASN1Encodable;
        //   100: invokestatic    org/bouncycastle/asn1/dvcs/DVCSTime.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/dvcs/DVCSTime;
        //   103: putfield        org/bouncycastle/asn1/dvcs/DVCSCertInfo.responseTime:Lorg/bouncycastle/asn1/dvcs/DVCSTime;
        //   106: iload_2        
        //   107: aload_1        
        //   108: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.size:()I
        //   111: if_icmpge       265
        //   114: aload_1        
        //   115: iload_2        
        //   116: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.getObjectAt:(I)Lorg/bouncycastle/asn1/ASN1Encodable;
        //   119: astore          5
        //   121: aload           5
        //   123: instanceof      Lorg/bouncycastle/asn1/ASN1TaggedObject;
        //   126: ifeq            249
        //   129: aload           5
        //   131: invokestatic    org/bouncycastle/asn1/ASN1TaggedObject.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/ASN1TaggedObject;
        //   134: astore          5
        //   136: aload           5
        //   138: invokevirtual   org/bouncycastle/asn1/ASN1TaggedObject.getTagNo:()I
        //   141: istore_3       
        //   142: iload_3        
        //   143: ifeq            236
        //   146: iload_3        
        //   147: iconst_1       
        //   148: if_icmpeq       220
        //   151: iload_3        
        //   152: iconst_2       
        //   153: if_icmpeq       207
        //   156: iload_3        
        //   157: iconst_3       
        //   158: if_icmpne       174
        //   161: aload_0        
        //   162: aload           5
        //   164: iconst_0       
        //   165: invokestatic    org/bouncycastle/asn1/ASN1Sequence.getInstance:(Lorg/bouncycastle/asn1/ASN1TaggedObject;Z)Lorg/bouncycastle/asn1/ASN1Sequence;
        //   168: putfield        org/bouncycastle/asn1/dvcs/DVCSCertInfo.certs:Lorg/bouncycastle/asn1/ASN1Sequence;
        //   171: goto            258
        //   174: new             Ljava/lang/StringBuilder;
        //   177: dup            
        //   178: invokespecial   java/lang/StringBuilder.<init>:()V
        //   181: astore_1       
        //   182: aload_1        
        //   183: ldc             "Unknown tag encountered: "
        //   185: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   188: pop            
        //   189: aload_1        
        //   190: iload_3        
        //   191: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   194: pop            
        //   195: new             Ljava/lang/IllegalArgumentException;
        //   198: dup            
        //   199: aload_1        
        //   200: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   203: invokespecial   java/lang/IllegalArgumentException.<init>:(Ljava/lang/String;)V
        //   206: athrow         
        //   207: aload_0        
        //   208: aload           5
        //   210: iconst_0       
        //   211: invokestatic    org/bouncycastle/asn1/ASN1Set.getInstance:(Lorg/bouncycastle/asn1/ASN1TaggedObject;Z)Lorg/bouncycastle/asn1/ASN1Set;
        //   214: putfield        org/bouncycastle/asn1/dvcs/DVCSCertInfo.reqSignature:Lorg/bouncycastle/asn1/ASN1Set;
        //   217: goto            258
        //   220: aload_0        
        //   221: aload           5
        //   223: iconst_0       
        //   224: invokestatic    org/bouncycastle/asn1/ASN1Sequence.getInstance:(Lorg/bouncycastle/asn1/ASN1TaggedObject;Z)Lorg/bouncycastle/asn1/ASN1Sequence;
        //   227: invokestatic    org/bouncycastle/asn1/x509/PolicyInformation.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/PolicyInformation;
        //   230: putfield        org/bouncycastle/asn1/dvcs/DVCSCertInfo.policy:Lorg/bouncycastle/asn1/x509/PolicyInformation;
        //   233: goto            258
        //   236: aload_0        
        //   237: aload           5
        //   239: iconst_0       
        //   240: invokestatic    org/bouncycastle/asn1/cmp/PKIStatusInfo.getInstance:(Lorg/bouncycastle/asn1/ASN1TaggedObject;Z)Lorg/bouncycastle/asn1/cmp/PKIStatusInfo;
        //   243: putfield        org/bouncycastle/asn1/dvcs/DVCSCertInfo.dvStatus:Lorg/bouncycastle/asn1/cmp/PKIStatusInfo;
        //   246: goto            258
        //   249: aload_0        
        //   250: aload           5
        //   252: invokestatic    org/bouncycastle/asn1/x509/Extensions.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/Extensions;
        //   255: putfield        org/bouncycastle/asn1/dvcs/DVCSCertInfo.extensions:Lorg/bouncycastle/asn1/x509/Extensions;
        //   258: iload_2        
        //   259: iconst_1       
        //   260: iadd           
        //   261: istore_2       
        //   262: goto            106
        //   265: return         
        //   266: astore          6
        //   268: goto            44
        //   271: astore          6
        //   273: goto            39
        //   276: astore          5
        //   278: goto            258
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  16     28     266    46     Ljava/lang/IllegalArgumentException;
        //  28     35     271    276    Ljava/lang/IllegalArgumentException;
        //  249    258    276    281    Ljava/lang/IllegalArgumentException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 146 out of bounds for length 146
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:373)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:426)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:713)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:549)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public DVCSCertInfo(final DVCSRequestInformation dvReqInfo, final DigestInfo messageImprint, final ASN1Integer serialNumber, final DVCSTime responseTime) {
        this.version = 1;
        this.dvReqInfo = dvReqInfo;
        this.messageImprint = messageImprint;
        this.serialNumber = serialNumber;
        this.responseTime = responseTime;
    }
    
    public static DVCSCertInfo getInstance(final Object o) {
        if (o instanceof DVCSCertInfo) {
            return (DVCSCertInfo)o;
        }
        if (o != null) {
            return new DVCSCertInfo(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public static DVCSCertInfo getInstance(final ASN1TaggedObject asn1TaggedObject, final boolean b) {
        return getInstance(ASN1Sequence.getInstance(asn1TaggedObject, b));
    }
    
    private void setDvReqInfo(final DVCSRequestInformation dvReqInfo) {
        this.dvReqInfo = dvReqInfo;
    }
    
    private void setMessageImprint(final DigestInfo messageImprint) {
        this.messageImprint = messageImprint;
    }
    
    private void setVersion(final int version) {
        this.version = version;
    }
    
    public TargetEtcChain[] getCerts() {
        final ASN1Sequence certs = this.certs;
        if (certs != null) {
            return TargetEtcChain.arrayFromSequence(certs);
        }
        return null;
    }
    
    public DVCSRequestInformation getDvReqInfo() {
        return this.dvReqInfo;
    }
    
    public PKIStatusInfo getDvStatus() {
        return this.dvStatus;
    }
    
    public Extensions getExtensions() {
        return this.extensions;
    }
    
    public DigestInfo getMessageImprint() {
        return this.messageImprint;
    }
    
    public PolicyInformation getPolicy() {
        return this.policy;
    }
    
    public ASN1Set getReqSignature() {
        return this.reqSignature;
    }
    
    public DVCSTime getResponseTime() {
        return this.responseTime;
    }
    
    public ASN1Integer getSerialNumber() {
        return this.serialNumber;
    }
    
    public int getVersion() {
        return this.version;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(10);
        final int version = this.version;
        if (version != 1) {
            asn1EncodableVector.add(new ASN1Integer(version));
        }
        asn1EncodableVector.add(this.dvReqInfo);
        asn1EncodableVector.add(this.messageImprint);
        asn1EncodableVector.add(this.serialNumber);
        asn1EncodableVector.add(this.responseTime);
        final PKIStatusInfo dvStatus = this.dvStatus;
        if (dvStatus != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 0, dvStatus));
        }
        final PolicyInformation policy = this.policy;
        if (policy != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 1, policy));
        }
        final ASN1Set reqSignature = this.reqSignature;
        if (reqSignature != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 2, reqSignature));
        }
        final ASN1Sequence certs = this.certs;
        if (certs != null) {
            asn1EncodableVector.add(new DERTaggedObject(false, 3, certs));
        }
        final Extensions extensions = this.extensions;
        if (extensions != null) {
            asn1EncodableVector.add(extensions);
        }
        return new DERSequence(asn1EncodableVector);
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("DVCSCertInfo {\n");
        if (this.version != 1) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("version: ");
            sb2.append(this.version);
            sb2.append("\n");
            sb.append(sb2.toString());
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("dvReqInfo: ");
        sb3.append(this.dvReqInfo);
        sb3.append("\n");
        sb.append(sb3.toString());
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("messageImprint: ");
        sb4.append(this.messageImprint);
        sb4.append("\n");
        sb.append(sb4.toString());
        final StringBuilder sb5 = new StringBuilder();
        sb5.append("serialNumber: ");
        sb5.append(this.serialNumber);
        sb5.append("\n");
        sb.append(sb5.toString());
        final StringBuilder sb6 = new StringBuilder();
        sb6.append("responseTime: ");
        sb6.append(this.responseTime);
        sb6.append("\n");
        sb.append(sb6.toString());
        if (this.dvStatus != null) {
            final StringBuilder sb7 = new StringBuilder();
            sb7.append("dvStatus: ");
            sb7.append(this.dvStatus);
            sb7.append("\n");
            sb.append(sb7.toString());
        }
        if (this.policy != null) {
            final StringBuilder sb8 = new StringBuilder();
            sb8.append("policy: ");
            sb8.append(this.policy);
            sb8.append("\n");
            sb.append(sb8.toString());
        }
        if (this.reqSignature != null) {
            final StringBuilder sb9 = new StringBuilder();
            sb9.append("reqSignature: ");
            sb9.append(this.reqSignature);
            sb9.append("\n");
            sb.append(sb9.toString());
        }
        if (this.certs != null) {
            final StringBuilder sb10 = new StringBuilder();
            sb10.append("certs: ");
            sb10.append(this.certs);
            sb10.append("\n");
            sb.append(sb10.toString());
        }
        if (this.extensions != null) {
            final StringBuilder sb11 = new StringBuilder();
            sb11.append("extensions: ");
            sb11.append(this.extensions);
            sb11.append("\n");
            sb.append(sb11.toString());
        }
        sb.append("}\n");
        return sb.toString();
    }
}
