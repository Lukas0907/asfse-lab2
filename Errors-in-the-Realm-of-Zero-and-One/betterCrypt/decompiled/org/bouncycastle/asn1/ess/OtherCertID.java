// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1.ess;

import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.DigestInfo;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.IssuerSerial;
import org.bouncycastle.asn1.ASN1Object;

public class OtherCertID extends ASN1Object
{
    private IssuerSerial issuerSerial;
    private ASN1Encodable otherCertHash;
    
    private OtherCertID(final ASN1Sequence asn1Sequence) {
        if (asn1Sequence.size() >= 1 && asn1Sequence.size() <= 2) {
            final boolean b = asn1Sequence.getObjectAt(0).toASN1Primitive() instanceof ASN1OctetString;
            final ASN1Encodable object = asn1Sequence.getObjectAt(0);
            ASN1Object otherCertHash;
            if (b) {
                otherCertHash = ASN1OctetString.getInstance(object);
            }
            else {
                otherCertHash = DigestInfo.getInstance(object);
            }
            this.otherCertHash = otherCertHash;
            if (asn1Sequence.size() > 1) {
                this.issuerSerial = IssuerSerial.getInstance(asn1Sequence.getObjectAt(1));
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Bad sequence size: ");
        sb.append(asn1Sequence.size());
        throw new IllegalArgumentException(sb.toString());
    }
    
    public OtherCertID(final AlgorithmIdentifier algorithmIdentifier, final byte[] array) {
        this.otherCertHash = new DigestInfo(algorithmIdentifier, array);
    }
    
    public OtherCertID(final AlgorithmIdentifier algorithmIdentifier, final byte[] array, final IssuerSerial issuerSerial) {
        this.otherCertHash = new DigestInfo(algorithmIdentifier, array);
        this.issuerSerial = issuerSerial;
    }
    
    public static OtherCertID getInstance(final Object o) {
        if (o instanceof OtherCertID) {
            return (OtherCertID)o;
        }
        if (o != null) {
            return new OtherCertID(ASN1Sequence.getInstance(o));
        }
        return null;
    }
    
    public AlgorithmIdentifier getAlgorithmHash() {
        if (this.otherCertHash.toASN1Primitive() instanceof ASN1OctetString) {
            return new AlgorithmIdentifier(OIWObjectIdentifiers.idSHA1);
        }
        return DigestInfo.getInstance(this.otherCertHash).getAlgorithmId();
    }
    
    public byte[] getCertHash() {
        if (this.otherCertHash.toASN1Primitive() instanceof ASN1OctetString) {
            return ((ASN1OctetString)this.otherCertHash.toASN1Primitive()).getOctets();
        }
        return DigestInfo.getInstance(this.otherCertHash).getDigest();
    }
    
    public IssuerSerial getIssuerSerial() {
        return this.issuerSerial;
    }
    
    @Override
    public ASN1Primitive toASN1Primitive() {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector(2);
        asn1EncodableVector.add(this.otherCertHash);
        final IssuerSerial issuerSerial = this.issuerSerial;
        if (issuerSerial != null) {
            asn1EncodableVector.add(issuerSerial);
        }
        return new DERSequence(asn1EncodableVector);
    }
}
