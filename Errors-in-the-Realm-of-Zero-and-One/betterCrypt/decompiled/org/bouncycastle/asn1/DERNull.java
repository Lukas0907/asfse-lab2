// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.asn1;

import java.io.IOException;

public class DERNull extends ASN1Null
{
    public static final DERNull INSTANCE;
    private static final byte[] zeroBytes;
    
    static {
        INSTANCE = new DERNull();
        zeroBytes = new byte[0];
    }
    
    @Override
    void encode(final ASN1OutputStream asn1OutputStream, final boolean b) throws IOException {
        asn1OutputStream.writeEncoded(b, 5, DERNull.zeroBytes);
    }
    
    @Override
    int encodedLength() {
        return 2;
    }
    
    @Override
    boolean isConstructed() {
        return false;
    }
}
