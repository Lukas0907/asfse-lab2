// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.interfaces;

import org.bouncycastle.jce.spec.GOST3410PublicKeyParameterSetSpec;

public interface GOST3410Params
{
    String getDigestParamSetOID();
    
    String getEncryptionParamSetOID();
    
    String getPublicKeyParamSetOID();
    
    GOST3410PublicKeyParameterSetSpec getPublicKeyParameters();
}
