// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce;

import java.security.spec.X509EncodedKeySpec;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import java.security.PublicKey;
import java.io.UnsupportedEncodingException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import java.security.KeyFactory;
import org.bouncycastle.asn1.x9.X9ECPoint;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x9.X962Parameters;
import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.ASN1Primitive;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.PrivateKey;

public class ECKeyUtil
{
    public static PrivateKey privateToExplicitParameters(final PrivateKey privateKey, final String s) throws IllegalArgumentException, NoSuchAlgorithmException, NoSuchProviderException {
        final Provider provider = Security.getProvider(s);
        if (provider != null) {
            return privateToExplicitParameters(privateKey, provider);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("cannot find provider: ");
        sb.append(s);
        throw new NoSuchProviderException(sb.toString());
    }
    
    public static PrivateKey privateToExplicitParameters(final PrivateKey privateKey, final Provider provider) throws IllegalArgumentException, NoSuchAlgorithmException {
        try {
            final PrivateKeyInfo instance = PrivateKeyInfo.getInstance(ASN1Primitive.fromByteArray(privateKey.getEncoded()));
            if (!instance.getPrivateKeyAlgorithm().getAlgorithm().equals(CryptoProObjectIdentifiers.gostR3410_2001)) {
                final X962Parameters instance2 = X962Parameters.getInstance(instance.getPrivateKeyAlgorithm().getParameters());
                X9ECParameters namedCurveByOid;
                if (instance2.isNamedCurve()) {
                    final X9ECParameters x9ECParameters = namedCurveByOid = ECUtil.getNamedCurveByOid(ASN1ObjectIdentifier.getInstance(instance2.getParameters()));
                    if (x9ECParameters.hasSeed()) {
                        namedCurveByOid = new X9ECParameters(x9ECParameters.getCurve(), x9ECParameters.getBaseEntry(), x9ECParameters.getN(), x9ECParameters.getH());
                    }
                }
                else {
                    if (!instance2.isImplicitlyCA()) {
                        return privateKey;
                    }
                    namedCurveByOid = new X9ECParameters(BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa().getCurve(), new X9ECPoint(BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa().getG(), false), BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa().getN(), BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa().getH());
                }
                return KeyFactory.getInstance(privateKey.getAlgorithm(), provider).generatePrivate(new PKCS8EncodedKeySpec(new PrivateKeyInfo(new AlgorithmIdentifier(X9ObjectIdentifiers.id_ecPublicKey, new X962Parameters(namedCurveByOid)), instance.parsePrivateKey()).getEncoded()));
            }
            throw new UnsupportedEncodingException("cannot convert GOST key to explicit parameters.");
        }
        catch (Exception ex) {
            throw new UnexpectedException(ex);
        }
        catch (NoSuchAlgorithmException ex2) {
            throw ex2;
        }
        catch (IllegalArgumentException ex3) {
            throw ex3;
        }
        return privateKey;
    }
    
    public static PublicKey publicToExplicitParameters(final PublicKey publicKey, final String s) throws IllegalArgumentException, NoSuchAlgorithmException, NoSuchProviderException {
        final Provider provider = Security.getProvider(s);
        if (provider != null) {
            return publicToExplicitParameters(publicKey, provider);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("cannot find provider: ");
        sb.append(s);
        throw new NoSuchProviderException(sb.toString());
    }
    
    public static PublicKey publicToExplicitParameters(final PublicKey publicKey, final Provider provider) throws IllegalArgumentException, NoSuchAlgorithmException {
        try {
            final SubjectPublicKeyInfo instance = SubjectPublicKeyInfo.getInstance(ASN1Primitive.fromByteArray(publicKey.getEncoded()));
            if (!instance.getAlgorithm().getAlgorithm().equals(CryptoProObjectIdentifiers.gostR3410_2001)) {
                final X962Parameters instance2 = X962Parameters.getInstance(instance.getAlgorithm().getParameters());
                X9ECParameters namedCurveByOid;
                if (instance2.isNamedCurve()) {
                    final X9ECParameters x9ECParameters = namedCurveByOid = ECUtil.getNamedCurveByOid(ASN1ObjectIdentifier.getInstance(instance2.getParameters()));
                    if (x9ECParameters.hasSeed()) {
                        namedCurveByOid = new X9ECParameters(x9ECParameters.getCurve(), x9ECParameters.getBaseEntry(), x9ECParameters.getN(), x9ECParameters.getH());
                    }
                }
                else {
                    if (!instance2.isImplicitlyCA()) {
                        return publicKey;
                    }
                    namedCurveByOid = new X9ECParameters(BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa().getCurve(), new X9ECPoint(BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa().getG(), false), BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa().getN(), BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa().getH());
                }
                return KeyFactory.getInstance(publicKey.getAlgorithm(), provider).generatePublic(new X509EncodedKeySpec(new SubjectPublicKeyInfo(new AlgorithmIdentifier(X9ObjectIdentifiers.id_ecPublicKey, new X962Parameters(namedCurveByOid)), instance.getPublicKeyData().getBytes()).getEncoded()));
            }
            throw new IllegalArgumentException("cannot convert GOST key to explicit parameters.");
        }
        catch (Exception ex) {
            throw new UnexpectedException(ex);
        }
        catch (NoSuchAlgorithmException ex2) {
            throw ex2;
        }
        catch (IllegalArgumentException ex3) {
            throw ex3;
        }
        return publicKey;
    }
    
    private static class UnexpectedException extends RuntimeException
    {
        private Throwable cause;
        
        UnexpectedException(final Throwable cause) {
            super(cause.toString());
            this.cause = cause;
        }
        
        @Override
        public Throwable getCause() {
            return this.cause;
        }
    }
}
