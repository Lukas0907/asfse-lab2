// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.spec;

import org.bouncycastle.math.field.Polynomial;
import java.security.spec.ECFieldF2m;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.math.field.PolynomialExtensionField;
import java.security.spec.ECFieldFp;
import org.bouncycastle.math.ec.ECAlgorithms;
import java.security.spec.ECField;
import org.bouncycastle.math.field.FiniteField;
import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util;
import org.bouncycastle.math.ec.ECCurve;
import java.math.BigInteger;
import java.security.spec.ECPoint;
import java.security.spec.EllipticCurve;
import java.security.spec.ECParameterSpec;

public class ECNamedCurveSpec extends ECParameterSpec
{
    private String name;
    
    public ECNamedCurveSpec(final String name, final EllipticCurve curve, final ECPoint g, final BigInteger n) {
        super(curve, g, n, 1);
        this.name = name;
    }
    
    public ECNamedCurveSpec(final String name, final EllipticCurve curve, final ECPoint g, final BigInteger n, final BigInteger bigInteger) {
        super(curve, g, n, bigInteger.intValue());
        this.name = name;
    }
    
    public ECNamedCurveSpec(final String name, final ECCurve ecCurve, final org.bouncycastle.math.ec.ECPoint ecPoint, final BigInteger n) {
        super(convertCurve(ecCurve, null), EC5Util.convertPoint(ecPoint), n, 1);
        this.name = name;
    }
    
    public ECNamedCurveSpec(final String name, final ECCurve ecCurve, final org.bouncycastle.math.ec.ECPoint ecPoint, final BigInteger n, final BigInteger bigInteger) {
        super(convertCurve(ecCurve, null), EC5Util.convertPoint(ecPoint), n, bigInteger.intValue());
        this.name = name;
    }
    
    public ECNamedCurveSpec(final String name, final ECCurve ecCurve, final org.bouncycastle.math.ec.ECPoint ecPoint, final BigInteger n, final BigInteger bigInteger, final byte[] array) {
        super(convertCurve(ecCurve, array), EC5Util.convertPoint(ecPoint), n, bigInteger.intValue());
        this.name = name;
    }
    
    private static EllipticCurve convertCurve(final ECCurve ecCurve, final byte[] seed) {
        return new EllipticCurve(convertField(ecCurve.getField()), ecCurve.getA().toBigInteger(), ecCurve.getB().toBigInteger(), seed);
    }
    
    private static ECField convertField(final FiniteField finiteField) {
        if (ECAlgorithms.isFpField(finiteField)) {
            return new ECFieldFp(finiteField.getCharacteristic());
        }
        final Polynomial minimalPolynomial = ((PolynomialExtensionField)finiteField).getMinimalPolynomial();
        final int[] exponentsPresent = minimalPolynomial.getExponentsPresent();
        return new ECFieldF2m(minimalPolynomial.getDegree(), Arrays.reverse(Arrays.copyOfRange(exponentsPresent, 1, exponentsPresent.length - 1)));
    }
    
    public String getName() {
        return this.name;
    }
}
