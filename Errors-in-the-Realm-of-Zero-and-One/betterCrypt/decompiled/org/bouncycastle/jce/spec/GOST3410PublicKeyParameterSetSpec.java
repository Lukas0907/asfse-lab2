// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.spec;

import java.math.BigInteger;

public class GOST3410PublicKeyParameterSetSpec
{
    private BigInteger a;
    private BigInteger p;
    private BigInteger q;
    
    public GOST3410PublicKeyParameterSetSpec(final BigInteger p3, final BigInteger q, final BigInteger a) {
        this.p = p3;
        this.q = q;
        this.a = a;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof GOST3410PublicKeyParameterSetSpec;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final GOST3410PublicKeyParameterSetSpec gost3410PublicKeyParameterSetSpec = (GOST3410PublicKeyParameterSetSpec)o;
            b3 = b2;
            if (this.a.equals(gost3410PublicKeyParameterSetSpec.a)) {
                b3 = b2;
                if (this.p.equals(gost3410PublicKeyParameterSetSpec.p)) {
                    b3 = b2;
                    if (this.q.equals(gost3410PublicKeyParameterSetSpec.q)) {
                        b3 = true;
                    }
                }
            }
        }
        return b3;
    }
    
    public BigInteger getA() {
        return this.a;
    }
    
    public BigInteger getP() {
        return this.p;
    }
    
    public BigInteger getQ() {
        return this.q;
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode() ^ this.p.hashCode() ^ this.q.hashCode();
    }
}
