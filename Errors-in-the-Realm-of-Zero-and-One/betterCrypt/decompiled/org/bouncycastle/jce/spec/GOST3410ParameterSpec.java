// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.spec;

import org.bouncycastle.asn1.cryptopro.GOST3410PublicKeyAlgParameters;
import org.bouncycastle.asn1.cryptopro.GOST3410ParamSetParameters;
import org.bouncycastle.asn1.cryptopro.GOST3410NamedParameters;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import org.bouncycastle.jce.interfaces.GOST3410Params;
import java.security.spec.AlgorithmParameterSpec;

public class GOST3410ParameterSpec implements AlgorithmParameterSpec, GOST3410Params
{
    private String digestParamSetOID;
    private String encryptionParamSetOID;
    private String keyParamSetOID;
    private GOST3410PublicKeyParameterSetSpec keyParameters;
    
    public GOST3410ParameterSpec(final String s) {
        this(s, CryptoProObjectIdentifiers.gostR3411_94_CryptoProParamSet.getId(), null);
    }
    
    public GOST3410ParameterSpec(final String s, final String s2) {
        this(s, s2, null);
    }
    
    public GOST3410ParameterSpec(String id, final String digestParamSetOID, final String encryptionParamSetOID) {
        while (true) {
            try {
                GOST3410ParamSetParameters gost3410ParamSetParameters = GOST3410NamedParameters.getByOID(new ASN1ObjectIdentifier(id));
                while (true) {
                    if (gost3410ParamSetParameters != null) {
                        this.keyParameters = new GOST3410PublicKeyParameterSetSpec(gost3410ParamSetParameters.getP(), gost3410ParamSetParameters.getQ(), gost3410ParamSetParameters.getA());
                        this.keyParamSetOID = id;
                        this.digestParamSetOID = digestParamSetOID;
                        this.encryptionParamSetOID = encryptionParamSetOID;
                        return;
                    }
                    throw new IllegalArgumentException("no key parameter set for passed in name/OID.");
                    final ASN1ObjectIdentifier oid = GOST3410NamedParameters.getOID(id);
                    id = oid.getId();
                    gost3410ParamSetParameters = GOST3410NamedParameters.getByOID(oid);
                    continue;
                    Label_0047: {
                        gost3410ParamSetParameters = null;
                    }
                    continue;
                }
            }
            // iftrue(Label_0047:, oid == null)
            catch (IllegalArgumentException ex) {}
            continue;
        }
    }
    
    public GOST3410ParameterSpec(final GOST3410PublicKeyParameterSetSpec keyParameters) {
        this.keyParameters = keyParameters;
        this.digestParamSetOID = CryptoProObjectIdentifiers.gostR3411_94_CryptoProParamSet.getId();
        this.encryptionParamSetOID = null;
    }
    
    public static GOST3410ParameterSpec fromPublicKeyAlg(final GOST3410PublicKeyAlgParameters gost3410PublicKeyAlgParameters) {
        if (gost3410PublicKeyAlgParameters.getEncryptionParamSet() != null) {
            return new GOST3410ParameterSpec(gost3410PublicKeyAlgParameters.getPublicKeyParamSet().getId(), gost3410PublicKeyAlgParameters.getDigestParamSet().getId(), gost3410PublicKeyAlgParameters.getEncryptionParamSet().getId());
        }
        return new GOST3410ParameterSpec(gost3410PublicKeyAlgParameters.getPublicKeyParamSet().getId(), gost3410PublicKeyAlgParameters.getDigestParamSet().getId());
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof GOST3410ParameterSpec;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final GOST3410ParameterSpec gost3410ParameterSpec = (GOST3410ParameterSpec)o;
            b3 = b2;
            if (this.keyParameters.equals(gost3410ParameterSpec.keyParameters)) {
                b3 = b2;
                if (this.digestParamSetOID.equals(gost3410ParameterSpec.digestParamSetOID)) {
                    final String encryptionParamSetOID = this.encryptionParamSetOID;
                    final String encryptionParamSetOID2 = gost3410ParameterSpec.encryptionParamSetOID;
                    if (encryptionParamSetOID != encryptionParamSetOID2) {
                        b3 = b2;
                        if (encryptionParamSetOID == null) {
                            return b3;
                        }
                        b3 = b2;
                        if (!encryptionParamSetOID.equals(encryptionParamSetOID2)) {
                            return b3;
                        }
                    }
                    b3 = true;
                }
            }
        }
        return b3;
    }
    
    @Override
    public String getDigestParamSetOID() {
        return this.digestParamSetOID;
    }
    
    @Override
    public String getEncryptionParamSetOID() {
        return this.encryptionParamSetOID;
    }
    
    @Override
    public String getPublicKeyParamSetOID() {
        return this.keyParamSetOID;
    }
    
    @Override
    public GOST3410PublicKeyParameterSetSpec getPublicKeyParameters() {
        return this.keyParameters;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.keyParameters.hashCode();
        final int hashCode2 = this.digestParamSetOID.hashCode();
        final String encryptionParamSetOID = this.encryptionParamSetOID;
        int hashCode3;
        if (encryptionParamSetOID != null) {
            hashCode3 = encryptionParamSetOID.hashCode();
        }
        else {
            hashCode3 = 0;
        }
        return hashCode ^ hashCode2 ^ hashCode3;
    }
}
