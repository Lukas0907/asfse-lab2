// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.spec;

import org.bouncycastle.math.ec.ECPoint;

public class ECPublicKeySpec extends ECKeySpec
{
    private ECPoint q;
    
    public ECPublicKeySpec(final ECPoint ecPoint, final ECParameterSpec ecParameterSpec) {
        super(ecParameterSpec);
        ECPoint normalize = ecPoint;
        if (ecPoint.getCurve() != null) {
            normalize = ecPoint.normalize();
        }
        this.q = normalize;
    }
    
    public ECPoint getQ() {
        return this.q;
    }
}
