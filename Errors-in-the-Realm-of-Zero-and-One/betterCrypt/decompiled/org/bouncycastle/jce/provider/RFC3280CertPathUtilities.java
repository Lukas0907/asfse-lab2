// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

import java.math.BigInteger;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.ASN1String;
import org.bouncycastle.asn1.x500.style.BCStyle;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.GeneralSecurityException;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathParameters;
import org.bouncycastle.jcajce.PKIXExtendedBuilderParameters;
import java.security.cert.CertSelector;
import org.bouncycastle.jcajce.PKIXCertStoreSelector;
import java.security.cert.X509CertSelector;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.ASN1Primitive;
import java.io.IOException;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.asn1.x509.IssuingDistributionPoint;
import java.security.cert.CRL;
import java.security.cert.CRLSelector;
import org.bouncycastle.jcajce.PKIXCRLStoreSelector;
import java.security.cert.X509CRLSelector;
import org.bouncycastle.jcajce.PKIXCRLStore;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import java.security.cert.PKIXCertPathChecker;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.x509.GeneralSubtree;
import org.bouncycastle.asn1.x509.NameConstraints;
import java.util.Enumeration;
import java.security.cert.Certificate;
import java.security.cert.PolicyNode;
import java.util.ArrayList;
import java.security.cert.CertPathValidatorException;
import org.bouncycastle.jce.exception.ExtCertPathValidatorException;
import org.bouncycastle.asn1.x509.PolicyInformation;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.util.HashMap;
import org.bouncycastle.asn1.ASN1Sequence;
import java.security.cert.X509Extension;
import java.security.cert.CertPath;
import java.util.Set;
import java.util.Iterator;
import java.util.Collection;
import java.util.HashSet;
import java.security.cert.X509CRL;
import org.bouncycastle.jcajce.util.JcaJceHelper;
import java.util.List;
import java.security.PublicKey;
import java.util.Date;
import java.security.cert.X509Certificate;
import org.bouncycastle.jcajce.PKIXExtendedParameters;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.asn1.x509.Extension;

class RFC3280CertPathUtilities
{
    public static final String ANY_POLICY = "2.5.29.32.0";
    public static final String AUTHORITY_KEY_IDENTIFIER;
    public static final String BASIC_CONSTRAINTS;
    public static final String CERTIFICATE_POLICIES;
    public static final String CRL_DISTRIBUTION_POINTS;
    public static final String CRL_NUMBER;
    protected static final int CRL_SIGN = 6;
    private static final PKIXCRLUtil CRL_UTIL;
    public static final String DELTA_CRL_INDICATOR;
    public static final String FRESHEST_CRL;
    public static final String INHIBIT_ANY_POLICY;
    public static final String ISSUING_DISTRIBUTION_POINT;
    protected static final int KEY_CERT_SIGN = 5;
    public static final String KEY_USAGE;
    public static final String NAME_CONSTRAINTS;
    public static final String POLICY_CONSTRAINTS;
    public static final String POLICY_MAPPINGS;
    public static final String SUBJECT_ALTERNATIVE_NAME;
    protected static final String[] crlReasons;
    
    static {
        CRL_UTIL = new PKIXCRLUtil();
        CERTIFICATE_POLICIES = Extension.certificatePolicies.getId();
        POLICY_MAPPINGS = Extension.policyMappings.getId();
        INHIBIT_ANY_POLICY = Extension.inhibitAnyPolicy.getId();
        ISSUING_DISTRIBUTION_POINT = Extension.issuingDistributionPoint.getId();
        FRESHEST_CRL = Extension.freshestCRL.getId();
        DELTA_CRL_INDICATOR = Extension.deltaCRLIndicator.getId();
        POLICY_CONSTRAINTS = Extension.policyConstraints.getId();
        BASIC_CONSTRAINTS = Extension.basicConstraints.getId();
        CRL_DISTRIBUTION_POINTS = Extension.cRLDistributionPoints.getId();
        SUBJECT_ALTERNATIVE_NAME = Extension.subjectAlternativeName.getId();
        NAME_CONSTRAINTS = Extension.nameConstraints.getId();
        AUTHORITY_KEY_IDENTIFIER = Extension.authorityKeyIdentifier.getId();
        KEY_USAGE = Extension.keyUsage.getId();
        CRL_NUMBER = Extension.cRLNumber.getId();
        crlReasons = new String[] { "unspecified", "keyCompromise", "cACompromise", "affiliationChanged", "superseded", "cessationOfOperation", "certificateHold", "unknown", "removeFromCRL", "privilegeWithdrawn", "aACompromise" };
    }
    
    private static void checkCRL(final DistributionPoint distributionPoint, final PKIXExtendedParameters pkixExtendedParameters, final X509Certificate x509Certificate, final Date date, final X509Certificate x509Certificate2, final PublicKey publicKey, final CertStatus certStatus, final ReasonsMask reasonsMask, final List list, final JcaJceHelper jcaJceHelper) throws AnnotatedException {
        Object o = reasonsMask;
        Object o2 = new Date(System.currentTimeMillis());
        if (date.getTime() > ((Date)o2).getTime()) {
            throw new AnnotatedException("Validation time is in future.");
        }
        final Iterator iterator = CertPathValidatorUtilities.getCompleteCRLs(distributionPoint, x509Certificate, (Date)o2, pkixExtendedParameters).iterator();
        final int n = 1;
        int n2 = 0;
        ReasonsMask reasonsMask2 = null;
    Label_0053:
        while (iterator.hasNext() && certStatus.getCertStatus() == 11 && !reasonsMask.isAllReasons()) {
            ReasonsMask reasonsMask3 = null;
        Label_0192_Outer:
            while (true) {
                while (true) {
                Label_0542:
                    while (true) {
                        Label_0535: {
                            try {
                                final X509CRL x509CRL = iterator.next();
                                final ReasonsMask processCRLD = processCRLD(x509CRL, distributionPoint);
                                if (!processCRLD.hasNewReasons((ReasonsMask)o)) {
                                    continue Label_0053;
                                }
                                o = o2;
                                try {
                                    final PublicKey processCRLG = processCRLG(x509CRL, processCRLF(x509CRL, x509Certificate, x509Certificate2, publicKey, pkixExtendedParameters, list, jcaJceHelper));
                                    if (pkixExtendedParameters.getDate() == null) {
                                        break Label_0535;
                                    }
                                    final Object date2 = pkixExtendedParameters.getDate();
                                    if (!pkixExtendedParameters.isUseDeltasEnabled()) {
                                        break Label_0542;
                                    }
                                    final X509CRL processCRLH = processCRLH(CertPathValidatorUtilities.getDeltaCRLs((Date)date2, x509CRL, pkixExtendedParameters.getCertStores(), pkixExtendedParameters.getCRLStores()), processCRLG);
                                    if (pkixExtendedParameters.getValidityModel() != n && x509Certificate.getNotAfter().getTime() < x509CRL.getThisUpdate().getTime()) {
                                        throw new AnnotatedException("No valid CRL for current time found.");
                                    }
                                    processCRLB1(distributionPoint, x509Certificate, x509CRL);
                                    processCRLB2(distributionPoint, x509Certificate, x509CRL);
                                    processCRLC(processCRLH, x509CRL, pkixExtendedParameters);
                                    processCRLI(date, processCRLH, x509Certificate, certStatus, pkixExtendedParameters);
                                    processCRLJ(date, x509CRL, x509Certificate, certStatus);
                                    if (certStatus.getCertStatus() == 8) {
                                        certStatus.setCertStatus(11);
                                    }
                                    try {
                                        reasonsMask.addReasons(processCRLD);
                                        final Set<String> criticalExtensionOIDs = x509CRL.getCriticalExtensionOIDs();
                                        if (criticalExtensionOIDs != null) {
                                            final HashSet set = new HashSet(criticalExtensionOIDs);
                                            set.remove(Extension.issuingDistributionPoint.getId());
                                            set.remove(Extension.deltaCRLIndicator.getId());
                                            if (!set.isEmpty()) {
                                                throw new AnnotatedException("CRL contains unsupported critical extensions.");
                                            }
                                        }
                                        if (processCRLH != null) {
                                            final Set<String> criticalExtensionOIDs2 = processCRLH.getCriticalExtensionOIDs();
                                            if (criticalExtensionOIDs2 != null) {
                                                final HashSet set2 = new HashSet(criticalExtensionOIDs2);
                                                set2.remove(Extension.issuingDistributionPoint.getId());
                                                set2.remove(Extension.deltaCRLIndicator.getId());
                                                if (!set2.isEmpty()) {
                                                    throw new AnnotatedException("Delta CRL contains unsupported critical extension.");
                                                }
                                            }
                                        }
                                        n2 = n;
                                        o2 = o;
                                        o = reasonsMask;
                                    }
                                    catch (AnnotatedException o) {
                                        reasonsMask3 = reasonsMask;
                                    }
                                }
                                catch (AnnotatedException o) {
                                    reasonsMask3 = reasonsMask;
                                }
                            }
                            catch (AnnotatedException ex) {
                                reasonsMask3 = (ReasonsMask)o;
                                o = ex;
                            }
                            break;
                        }
                        final Object date2 = o;
                        continue Label_0192_Outer;
                    }
                    final X509CRL processCRLH = null;
                    continue;
                }
            }
            final ReasonsMask reasonsMask4 = reasonsMask3;
            reasonsMask2 = (ReasonsMask)o;
            o = reasonsMask4;
        }
        if (n2 != 0) {
            return;
        }
        throw reasonsMask2;
    }
    
    protected static void checkCRLs(final PKIXExtendedParameters p0, final X509Certificate p1, final Date p2, final X509Certificate p3, final PublicKey p4, final List p5, final JcaJceHelper p6) throws AnnotatedException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getstatic       org/bouncycastle/jce/provider/RFC3280CertPathUtilities.CRL_DISTRIBUTION_POINTS:Ljava/lang/String;
        //     4: invokestatic    org/bouncycastle/jce/provider/CertPathValidatorUtilities.getExtensionValue:(Ljava/security/cert/X509Extension;Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1Primitive;
        //     7: invokestatic    org/bouncycastle/asn1/x509/CRLDistPoint.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/CRLDistPoint;
        //    10: astore          10
        //    12: new             Lorg/bouncycastle/jcajce/PKIXExtendedParameters$Builder;
        //    15: dup            
        //    16: aload_0        
        //    17: invokespecial   org/bouncycastle/jcajce/PKIXExtendedParameters$Builder.<init>:(Lorg/bouncycastle/jcajce/PKIXExtendedParameters;)V
        //    20: astore          11
        //    22: aload           10
        //    24: aload_0        
        //    25: invokevirtual   org/bouncycastle/jcajce/PKIXExtendedParameters.getNamedCRLStoreMap:()Ljava/util/Map;
        //    28: invokestatic    org/bouncycastle/jce/provider/CertPathValidatorUtilities.getAdditionalStoresFromCRLDistributionPoint:(Lorg/bouncycastle/asn1/x509/CRLDistPoint;Ljava/util/Map;)Ljava/util/List;
        //    31: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //    36: astore          12
        //    38: aload           12
        //    40: invokeinterface java/util/Iterator.hasNext:()Z
        //    45: ifeq            67
        //    48: aload           11
        //    50: aload           12
        //    52: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //    57: checkcast       Lorg/bouncycastle/jcajce/PKIXCRLStore;
        //    60: invokevirtual   org/bouncycastle/jcajce/PKIXExtendedParameters$Builder.addCRLStore:(Lorg/bouncycastle/jcajce/PKIXCRLStore;)Lorg/bouncycastle/jcajce/PKIXExtendedParameters$Builder;
        //    63: pop            
        //    64: goto            38
        //    67: new             Lorg/bouncycastle/jce/provider/CertStatus;
        //    70: dup            
        //    71: invokespecial   org/bouncycastle/jce/provider/CertStatus.<init>:()V
        //    74: astore          12
        //    76: new             Lorg/bouncycastle/jce/provider/ReasonsMask;
        //    79: dup            
        //    80: invokespecial   org/bouncycastle/jce/provider/ReasonsMask.<init>:()V
        //    83: astore          13
        //    85: aload           11
        //    87: invokevirtual   org/bouncycastle/jcajce/PKIXExtendedParameters$Builder.build:()Lorg/bouncycastle/jcajce/PKIXExtendedParameters;
        //    90: astore          14
        //    92: bipush          11
        //    94: istore          7
        //    96: aload           10
        //    98: ifnull          212
        //   101: aload           10
        //   103: invokevirtual   org/bouncycastle/asn1/x509/CRLDistPoint.getDistributionPoints:()[Lorg/bouncycastle/asn1/x509/DistributionPoint;
        //   106: astore          11
        //   108: aload           11
        //   110: ifnull          212
        //   113: aconst_null    
        //   114: astore          10
        //   116: iconst_0       
        //   117: istore          9
        //   119: iload           9
        //   121: istore          8
        //   123: iload           9
        //   125: aload           11
        //   127: arraylength    
        //   128: if_icmpge       196
        //   131: aload           12
        //   133: invokevirtual   org/bouncycastle/jce/provider/CertStatus.getCertStatus:()I
        //   136: iload           7
        //   138: if_icmpne       196
        //   141: aload           13
        //   143: invokevirtual   org/bouncycastle/jce/provider/ReasonsMask.isAllReasons:()Z
        //   146: ifne            196
        //   149: aload           11
        //   151: iload           9
        //   153: aaload         
        //   154: astore          15
        //   156: aload           15
        //   158: aload           14
        //   160: aload_1        
        //   161: aload_2        
        //   162: aload_3        
        //   163: aload           4
        //   165: aload           12
        //   167: aload           13
        //   169: aload           5
        //   171: aload           6
        //   173: invokestatic    org/bouncycastle/jce/provider/RFC3280CertPathUtilities.checkCRL:(Lorg/bouncycastle/asn1/x509/DistributionPoint;Lorg/bouncycastle/jcajce/PKIXExtendedParameters;Ljava/security/cert/X509Certificate;Ljava/util/Date;Ljava/security/cert/X509Certificate;Ljava/security/PublicKey;Lorg/bouncycastle/jce/provider/CertStatus;Lorg/bouncycastle/jce/provider/ReasonsMask;Ljava/util/List;Lorg/bouncycastle/jcajce/util/JcaJceHelper;)V
        //   176: iconst_1       
        //   177: istore          8
        //   179: goto            187
        //   182: astore          10
        //   184: goto            187
        //   187: iload           9
        //   189: iconst_1       
        //   190: iadd           
        //   191: istore          9
        //   193: goto            123
        //   196: goto            222
        //   199: astore_0       
        //   200: new             Lorg/bouncycastle/jce/provider/AnnotatedException;
        //   203: dup            
        //   204: ldc_w           "Distribution points could not be read."
        //   207: aload_0        
        //   208: invokespecial   org/bouncycastle/jce/provider/AnnotatedException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   211: athrow         
        //   212: bipush          11
        //   214: istore          7
        //   216: aconst_null    
        //   217: astore          10
        //   219: iconst_0       
        //   220: istore          8
        //   222: aload           10
        //   224: astore          11
        //   226: iload           8
        //   228: istore          9
        //   230: aload           12
        //   232: invokevirtual   org/bouncycastle/jce/provider/CertStatus.getCertStatus:()I
        //   235: iload           7
        //   237: if_icmpne       342
        //   240: aload           10
        //   242: astore          11
        //   244: iload           8
        //   246: istore          9
        //   248: aload           13
        //   250: invokevirtual   org/bouncycastle/jce/provider/ReasonsMask.isAllReasons:()Z
        //   253: ifne            342
        //   256: aload_1        
        //   257: invokestatic    org/bouncycastle/jce/provider/PrincipalUtils.getEncodedIssuerPrincipal:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x500/X500Name;
        //   260: astore          11
        //   262: new             Lorg/bouncycastle/asn1/x509/DistributionPoint;
        //   265: dup            
        //   266: new             Lorg/bouncycastle/asn1/x509/DistributionPointName;
        //   269: dup            
        //   270: iconst_0       
        //   271: new             Lorg/bouncycastle/asn1/x509/GeneralNames;
        //   274: dup            
        //   275: new             Lorg/bouncycastle/asn1/x509/GeneralName;
        //   278: dup            
        //   279: iconst_4       
        //   280: aload           11
        //   282: invokespecial   org/bouncycastle/asn1/x509/GeneralName.<init>:(ILorg/bouncycastle/asn1/ASN1Encodable;)V
        //   285: invokespecial   org/bouncycastle/asn1/x509/GeneralNames.<init>:(Lorg/bouncycastle/asn1/x509/GeneralName;)V
        //   288: invokespecial   org/bouncycastle/asn1/x509/DistributionPointName.<init>:(ILorg/bouncycastle/asn1/ASN1Encodable;)V
        //   291: aconst_null    
        //   292: aconst_null    
        //   293: invokespecial   org/bouncycastle/asn1/x509/DistributionPoint.<init>:(Lorg/bouncycastle/asn1/x509/DistributionPointName;Lorg/bouncycastle/asn1/x509/ReasonFlags;Lorg/bouncycastle/asn1/x509/GeneralNames;)V
        //   296: aload_0        
        //   297: invokevirtual   org/bouncycastle/jcajce/PKIXExtendedParameters.clone:()Ljava/lang/Object;
        //   300: checkcast       Lorg/bouncycastle/jcajce/PKIXExtendedParameters;
        //   303: aload_1        
        //   304: aload_2        
        //   305: aload_3        
        //   306: aload           4
        //   308: aload           12
        //   310: aload           13
        //   312: aload           5
        //   314: aload           6
        //   316: invokestatic    org/bouncycastle/jce/provider/RFC3280CertPathUtilities.checkCRL:(Lorg/bouncycastle/asn1/x509/DistributionPoint;Lorg/bouncycastle/jcajce/PKIXExtendedParameters;Ljava/security/cert/X509Certificate;Ljava/util/Date;Ljava/security/cert/X509Certificate;Ljava/security/PublicKey;Lorg/bouncycastle/jce/provider/CertStatus;Lorg/bouncycastle/jce/provider/ReasonsMask;Ljava/util/List;Lorg/bouncycastle/jcajce/util/JcaJceHelper;)V
        //   319: iconst_1       
        //   320: istore          9
        //   322: aload           10
        //   324: astore          11
        //   326: goto            342
        //   329: astore_0       
        //   330: new             Lorg/bouncycastle/jce/provider/AnnotatedException;
        //   333: dup            
        //   334: ldc_w           "Issuer from certificate for CRL could not be reencoded."
        //   337: aload_0        
        //   338: invokespecial   org/bouncycastle/jce/provider/AnnotatedException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   341: athrow         
        //   342: iload           9
        //   344: ifne            371
        //   347: aload           11
        //   349: instanceof      Lorg/bouncycastle/jce/provider/AnnotatedException;
        //   352: ifeq            358
        //   355: aload           11
        //   357: athrow         
        //   358: new             Lorg/bouncycastle/jce/provider/AnnotatedException;
        //   361: dup            
        //   362: ldc_w           "No valid CRL found."
        //   365: aload           11
        //   367: invokespecial   org/bouncycastle/jce/provider/AnnotatedException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   370: athrow         
        //   371: aload           12
        //   373: invokevirtual   org/bouncycastle/jce/provider/CertStatus.getCertStatus:()I
        //   376: iload           7
        //   378: if_icmpne       428
        //   381: aload           13
        //   383: invokevirtual   org/bouncycastle/jce/provider/ReasonsMask.isAllReasons:()Z
        //   386: ifne            406
        //   389: aload           12
        //   391: invokevirtual   org/bouncycastle/jce/provider/CertStatus.getCertStatus:()I
        //   394: iload           7
        //   396: if_icmpne       406
        //   399: aload           12
        //   401: bipush          12
        //   403: invokevirtual   org/bouncycastle/jce/provider/CertStatus.setCertStatus:(I)V
        //   406: aload           12
        //   408: invokevirtual   org/bouncycastle/jce/provider/CertStatus.getCertStatus:()I
        //   411: bipush          12
        //   413: if_icmpeq       417
        //   416: return         
        //   417: new             Lorg/bouncycastle/jce/provider/AnnotatedException;
        //   420: dup            
        //   421: ldc_w           "Certificate status could not be determined."
        //   424: invokespecial   org/bouncycastle/jce/provider/AnnotatedException.<init>:(Ljava/lang/String;)V
        //   427: athrow         
        //   428: new             Ljava/text/SimpleDateFormat;
        //   431: dup            
        //   432: ldc_w           "yyyy-MM-dd HH:mm:ss Z"
        //   435: invokespecial   java/text/SimpleDateFormat.<init>:(Ljava/lang/String;)V
        //   438: astore_0       
        //   439: aload_0        
        //   440: ldc_w           "UTC"
        //   443: invokestatic    java/util/TimeZone.getTimeZone:(Ljava/lang/String;)Ljava/util/TimeZone;
        //   446: invokevirtual   java/text/SimpleDateFormat.setTimeZone:(Ljava/util/TimeZone;)V
        //   449: new             Ljava/lang/StringBuilder;
        //   452: dup            
        //   453: invokespecial   java/lang/StringBuilder.<init>:()V
        //   456: astore_1       
        //   457: aload_1        
        //   458: ldc_w           "Certificate revocation after "
        //   461: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   464: pop            
        //   465: aload_1        
        //   466: aload_0        
        //   467: aload           12
        //   469: invokevirtual   org/bouncycastle/jce/provider/CertStatus.getRevocationDate:()Ljava/util/Date;
        //   472: invokevirtual   java/text/SimpleDateFormat.format:(Ljava/util/Date;)Ljava/lang/String;
        //   475: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   478: pop            
        //   479: aload_1        
        //   480: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   483: astore_0       
        //   484: new             Ljava/lang/StringBuilder;
        //   487: dup            
        //   488: invokespecial   java/lang/StringBuilder.<init>:()V
        //   491: astore_1       
        //   492: aload_1        
        //   493: aload_0        
        //   494: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   497: pop            
        //   498: aload_1        
        //   499: ldc_w           ", reason: "
        //   502: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   505: pop            
        //   506: aload_1        
        //   507: getstatic       org/bouncycastle/jce/provider/RFC3280CertPathUtilities.crlReasons:[Ljava/lang/String;
        //   510: aload           12
        //   512: invokevirtual   org/bouncycastle/jce/provider/CertStatus.getCertStatus:()I
        //   515: aaload         
        //   516: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   519: pop            
        //   520: new             Lorg/bouncycastle/jce/provider/AnnotatedException;
        //   523: dup            
        //   524: aload_1        
        //   525: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   528: invokespecial   org/bouncycastle/jce/provider/AnnotatedException.<init>:(Ljava/lang/String;)V
        //   531: athrow         
        //   532: astore_0       
        //   533: new             Lorg/bouncycastle/jce/provider/AnnotatedException;
        //   536: dup            
        //   537: ldc_w           "No additional CRL locations could be decoded from CRL distribution point extension."
        //   540: aload_0        
        //   541: invokespecial   org/bouncycastle/jce/provider/AnnotatedException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   544: athrow         
        //   545: astore_0       
        //   546: new             Lorg/bouncycastle/jce/provider/AnnotatedException;
        //   549: dup            
        //   550: ldc_w           "CRL distribution point extension could not be read."
        //   553: aload_0        
        //   554: invokespecial   org/bouncycastle/jce/provider/AnnotatedException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   557: athrow         
        //   558: astore          11
        //   560: iload           8
        //   562: istore          9
        //   564: goto            342
        //    Exceptions:
        //  throws org.bouncycastle.jce.provider.AnnotatedException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                              
        //  -----  -----  -----  -----  --------------------------------------------------
        //  0      12     545    558    Ljava/lang/Exception;
        //  22     38     532    545    Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  38     64     532    545    Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  101    108    199    212    Ljava/lang/Exception;
        //  156    176    182    187    Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  256    262    329    342    Ljava/lang/RuntimeException;
        //  256    262    558    567    Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  262    319    558    567    Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  330    342    558    567    Lorg/bouncycastle/jce/provider/AnnotatedException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0342:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    protected static PKIXPolicyNode prepareCertB(final CertPath certPath, final int index, final List[] array, PKIXPolicyNode removePolicyNode, final int n) throws CertPathValidatorException {
        final List<? extends Certificate> certificates = certPath.getCertificates();
        final X509Certificate x509Certificate = (X509Certificate)certificates.get(index);
        final int n2 = certificates.size() - index;
        try {
            final ASN1Sequence instance = ASN1Sequence.getInstance(CertPathValidatorUtilities.getExtensionValue(x509Certificate, RFC3280CertPathUtilities.POLICY_MAPPINGS));
            PKIXPolicyNode pkixPolicyNode = null;
            if (instance != null) {
                final HashMap<Object, HashSet<String>> hashMap = new HashMap<Object, HashSet<String>>();
                final HashSet<String> set = new HashSet<String>();
                for (int i = 0; i < instance.size(); ++i) {
                    final ASN1Sequence asn1Sequence = (ASN1Sequence)instance.getObjectAt(i);
                    final String id = ((ASN1ObjectIdentifier)asn1Sequence.getObjectAt(0)).getId();
                    final String id2 = ((ASN1ObjectIdentifier)asn1Sequence.getObjectAt(1)).getId();
                    if (!hashMap.containsKey(id)) {
                        final HashSet<String> set2 = new HashSet<String>();
                        set2.add(id2);
                        hashMap.put(id, set2);
                        set.add(id);
                    }
                    else {
                        hashMap.get(id).add(id2);
                    }
                }
                final Iterator<Object> iterator = set.iterator();
            Label_0315_Outer:
                while (true) {
                    pkixPolicyNode = removePolicyNode;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    final String s = iterator.next();
                    PKIXPolicyNode pkixPolicyNode3 = null;
                    Label_0801: {
                        Label_0613: {
                            if (n > 0) {
                                while (true) {
                                    for (final PKIXPolicyNode pkixPolicyNode2 : array[n2]) {
                                        if (pkixPolicyNode2.getValidPolicy().equals(s)) {
                                            pkixPolicyNode2.expectedPolicies = hashMap.get(s);
                                            final boolean b = true;
                                            pkixPolicyNode3 = removePolicyNode;
                                            if (!b) {
                                                final Iterator iterator3 = array[n2].iterator();
                                                PKIXPolicyNode pkixPolicyNode4;
                                                do {
                                                    pkixPolicyNode3 = removePolicyNode;
                                                    if (!iterator3.hasNext()) {
                                                        break Label_0801;
                                                    }
                                                    pkixPolicyNode4 = iterator3.next();
                                                } while (!"2.5.29.32.0".equals(pkixPolicyNode4.getValidPolicy()));
                                                final Set set3 = null;
                                                try {
                                                    final Enumeration objects = ((ASN1Sequence)CertPathValidatorUtilities.getExtensionValue(x509Certificate, RFC3280CertPathUtilities.CERTIFICATE_POLICIES)).getObjects();
                                                    Set qualifierSet;
                                                    while (true) {
                                                        qualifierSet = set3;
                                                        if (objects.hasMoreElements()) {
                                                            try {
                                                                final PolicyInformation instance2 = PolicyInformation.getInstance(objects.nextElement());
                                                                if (!"2.5.29.32.0".equals(instance2.getPolicyIdentifier().getId())) {
                                                                    continue Label_0315_Outer;
                                                                }
                                                                try {
                                                                    qualifierSet = CertPathValidatorUtilities.getQualifierSet(instance2.getPolicyQualifiers());
                                                                }
                                                                catch (CertPathValidatorException ex) {
                                                                    throw new ExtCertPathValidatorException("Policy qualifier info set could not be decoded.", ex, certPath, index);
                                                                }
                                                            }
                                                            catch (Exception cause) {
                                                                throw new CertPathValidatorException("Policy information could not be decoded.", cause, certPath, index);
                                                            }
                                                            break;
                                                        }
                                                        break;
                                                    }
                                                    final boolean b2 = x509Certificate.getCriticalExtensionOIDs() != null && x509Certificate.getCriticalExtensionOIDs().contains(RFC3280CertPathUtilities.CERTIFICATE_POLICIES);
                                                    final PKIXPolicyNode pkixPolicyNode5 = (PKIXPolicyNode)pkixPolicyNode4.getParent();
                                                    pkixPolicyNode3 = removePolicyNode;
                                                    if ("2.5.29.32.0".equals(pkixPolicyNode5.getValidPolicy())) {
                                                        final PKIXPolicyNode pkixPolicyNode6 = new PKIXPolicyNode(new ArrayList(), n2, hashMap.get(s), pkixPolicyNode5, qualifierSet, s, b2);
                                                        pkixPolicyNode5.addChild(pkixPolicyNode6);
                                                        array[n2].add(pkixPolicyNode6);
                                                        pkixPolicyNode3 = removePolicyNode;
                                                    }
                                                    break Label_0801;
                                                }
                                                catch (AnnotatedException ex2) {
                                                    throw new ExtCertPathValidatorException("Certificate policies extension could not be decoded.", ex2, certPath, index);
                                                }
                                                break Label_0613;
                                            }
                                            break Label_0801;
                                        }
                                    }
                                    final boolean b = false;
                                    continue;
                                }
                            }
                        }
                        pkixPolicyNode3 = removePolicyNode;
                        if (n <= 0) {
                            final Iterator iterator4 = array[n2].iterator();
                            while (true) {
                                pkixPolicyNode3 = removePolicyNode;
                                if (!iterator4.hasNext()) {
                                    break;
                                }
                                final PKIXPolicyNode pkixPolicyNode7 = iterator4.next();
                                if (!pkixPolicyNode7.getValidPolicy().equals(s)) {
                                    continue;
                                }
                                ((PKIXPolicyNode)pkixPolicyNode7.getParent()).removeChild(pkixPolicyNode7);
                                iterator4.remove();
                                int n3 = n2 - 1;
                                PKIXPolicyNode pkixPolicyNode8 = removePolicyNode;
                                while (true) {
                                    removePolicyNode = pkixPolicyNode8;
                                    if (n3 < 0) {
                                        break;
                                    }
                                    final List list = array[n3];
                                    removePolicyNode = pkixPolicyNode8;
                                    int n4 = 0;
                                    while (true) {
                                        pkixPolicyNode8 = removePolicyNode;
                                        if (n4 >= list.size()) {
                                            break;
                                        }
                                        final PKIXPolicyNode pkixPolicyNode9 = list.get(n4);
                                        PKIXPolicyNode pkixPolicyNode10 = removePolicyNode;
                                        if (!pkixPolicyNode9.hasChildren()) {
                                            removePolicyNode = CertPathValidatorUtilities.removePolicyNode(removePolicyNode, array, pkixPolicyNode9);
                                            if ((pkixPolicyNode10 = removePolicyNode) == null) {
                                                pkixPolicyNode8 = removePolicyNode;
                                                break;
                                            }
                                        }
                                        ++n4;
                                        removePolicyNode = pkixPolicyNode10;
                                    }
                                    --n3;
                                }
                            }
                        }
                    }
                    removePolicyNode = pkixPolicyNode3;
                }
            }
            else {
                pkixPolicyNode = removePolicyNode;
            }
            return pkixPolicyNode;
        }
        catch (AnnotatedException ex3) {
            throw new ExtCertPathValidatorException("Policy mappings extension could not be decoded.", ex3, certPath, index);
        }
    }
    
    protected static void prepareNextCertA(final CertPath certPath, final int n) throws CertPathValidatorException {
        final X509Certificate x509Certificate = (X509Certificate)certPath.getCertificates().get(n);
        try {
            final ASN1Sequence instance = ASN1Sequence.getInstance(CertPathValidatorUtilities.getExtensionValue(x509Certificate, RFC3280CertPathUtilities.POLICY_MAPPINGS));
            if (instance != null) {
                int i = 0;
                while (i < instance.size()) {
                    try {
                        final ASN1Sequence instance2 = ASN1Sequence.getInstance(instance.getObjectAt(i));
                        final ASN1ObjectIdentifier instance3 = ASN1ObjectIdentifier.getInstance(instance2.getObjectAt(0));
                        final ASN1ObjectIdentifier instance4 = ASN1ObjectIdentifier.getInstance(instance2.getObjectAt(1));
                        if ("2.5.29.32.0".equals(instance3.getId())) {
                            throw new CertPathValidatorException("IssuerDomainPolicy is anyPolicy", null, certPath, n);
                        }
                        if (!"2.5.29.32.0".equals(instance4.getId())) {
                            ++i;
                            continue;
                        }
                        throw new CertPathValidatorException("SubjectDomainPolicy is anyPolicy", null, certPath, n);
                    }
                    catch (Exception ex) {
                        throw new ExtCertPathValidatorException("Policy mappings extension contents could not be decoded.", ex, certPath, n);
                    }
                    break;
                }
            }
        }
        catch (AnnotatedException ex2) {
            throw new ExtCertPathValidatorException("Policy mappings extension could not be decoded.", ex2, certPath, n);
        }
    }
    
    protected static void prepareNextCertG(final CertPath certPath, final int n, final PKIXNameConstraintValidator pkixNameConstraintValidator) throws CertPathValidatorException {
        final X509Certificate x509Certificate = (X509Certificate)certPath.getCertificates().get(n);
        try {
            final ASN1Sequence instance = ASN1Sequence.getInstance(CertPathValidatorUtilities.getExtensionValue(x509Certificate, RFC3280CertPathUtilities.NAME_CONSTRAINTS));
            NameConstraints instance2;
            if (instance != null) {
                instance2 = NameConstraints.getInstance(instance);
            }
            else {
                instance2 = null;
            }
            if (instance2 != null) {
                final GeneralSubtree[] permittedSubtrees = instance2.getPermittedSubtrees();
                if (permittedSubtrees != null) {
                    try {
                        pkixNameConstraintValidator.intersectPermittedSubtree(permittedSubtrees);
                    }
                    catch (Exception ex) {
                        throw new ExtCertPathValidatorException("Permitted subtrees cannot be build from name constraints extension.", ex, certPath, n);
                    }
                }
                final GeneralSubtree[] excludedSubtrees = instance2.getExcludedSubtrees();
                if (excludedSubtrees != null) {
                    int i = 0;
                    while (i != excludedSubtrees.length) {
                        try {
                            pkixNameConstraintValidator.addExcludedSubtree(excludedSubtrees[i]);
                            ++i;
                            continue;
                        }
                        catch (Exception ex2) {
                            throw new ExtCertPathValidatorException("Excluded subtrees cannot be build from name constraints extension.", ex2, certPath, n);
                        }
                        break;
                    }
                }
            }
        }
        catch (Exception ex3) {
            throw new ExtCertPathValidatorException("Name constraints extension could not be decoded.", ex3, certPath, n);
        }
    }
    
    protected static int prepareNextCertH1(final CertPath certPath, final int n, final int n2) {
        int n3 = n2;
        if (!CertPathValidatorUtilities.isSelfIssued((X509Certificate)certPath.getCertificates().get(n)) && (n3 = n2) != 0) {
            n3 = n2 - 1;
        }
        return n3;
    }
    
    protected static int prepareNextCertH2(final CertPath certPath, final int n, final int n2) {
        int n3 = n2;
        if (!CertPathValidatorUtilities.isSelfIssued((X509Certificate)certPath.getCertificates().get(n)) && (n3 = n2) != 0) {
            n3 = n2 - 1;
        }
        return n3;
    }
    
    protected static int prepareNextCertH3(final CertPath certPath, final int n, final int n2) {
        int n3 = n2;
        if (!CertPathValidatorUtilities.isSelfIssued((X509Certificate)certPath.getCertificates().get(n)) && (n3 = n2) != 0) {
            n3 = n2 - 1;
        }
        return n3;
    }
    
    protected static int prepareNextCertI1(final CertPath certPath, final int n, final int n2) throws CertPathValidatorException {
        final X509Certificate x509Certificate = (X509Certificate)certPath.getCertificates().get(n);
        try {
            final ASN1Sequence instance = ASN1Sequence.getInstance(CertPathValidatorUtilities.getExtensionValue(x509Certificate, RFC3280CertPathUtilities.POLICY_CONSTRAINTS));
            if (instance != null) {
                final Enumeration objects = instance.getObjects();
                while (objects.hasMoreElements()) {
                    try {
                        final ASN1TaggedObject instance2 = ASN1TaggedObject.getInstance(objects.nextElement());
                        if (instance2.getTagNo() != 0) {
                            continue;
                        }
                        final int intValueExact = ASN1Integer.getInstance(instance2, false).intValueExact();
                        if (intValueExact < n2) {
                            return intValueExact;
                        }
                    }
                    catch (IllegalArgumentException ex) {
                        throw new ExtCertPathValidatorException("Policy constraints extension contents cannot be decoded.", ex, certPath, n);
                    }
                    break;
                }
            }
            return n2;
        }
        catch (Exception ex2) {
            throw new ExtCertPathValidatorException("Policy constraints extension cannot be decoded.", ex2, certPath, n);
        }
    }
    
    protected static int prepareNextCertI2(final CertPath certPath, final int n, final int n2) throws CertPathValidatorException {
        final X509Certificate x509Certificate = (X509Certificate)certPath.getCertificates().get(n);
        try {
            final ASN1Sequence instance = ASN1Sequence.getInstance(CertPathValidatorUtilities.getExtensionValue(x509Certificate, RFC3280CertPathUtilities.POLICY_CONSTRAINTS));
            if (instance != null) {
                final Enumeration objects = instance.getObjects();
                while (objects.hasMoreElements()) {
                    try {
                        final ASN1TaggedObject instance2 = ASN1TaggedObject.getInstance(objects.nextElement());
                        if (instance2.getTagNo() != 1) {
                            continue;
                        }
                        final int intValueExact = ASN1Integer.getInstance(instance2, false).intValueExact();
                        if (intValueExact < n2) {
                            return intValueExact;
                        }
                    }
                    catch (IllegalArgumentException ex) {
                        throw new ExtCertPathValidatorException("Policy constraints extension contents cannot be decoded.", ex, certPath, n);
                    }
                    break;
                }
            }
            return n2;
        }
        catch (Exception ex2) {
            throw new ExtCertPathValidatorException("Policy constraints extension cannot be decoded.", ex2, certPath, n);
        }
    }
    
    protected static int prepareNextCertJ(final CertPath certPath, int intValueExact, final int n) throws CertPathValidatorException {
        final X509Certificate x509Certificate = (X509Certificate)certPath.getCertificates().get(intValueExact);
        try {
            final ASN1Integer instance = ASN1Integer.getInstance(CertPathValidatorUtilities.getExtensionValue(x509Certificate, RFC3280CertPathUtilities.INHIBIT_ANY_POLICY));
            if (instance != null) {
                intValueExact = instance.intValueExact();
                if (intValueExact < n) {
                    return intValueExact;
                }
            }
            return n;
        }
        catch (Exception ex) {
            throw new ExtCertPathValidatorException("Inhibit any-policy extension cannot be decoded.", ex, certPath, intValueExact);
        }
    }
    
    protected static void prepareNextCertK(final CertPath certPath, final int n) throws CertPathValidatorException {
        final X509Certificate x509Certificate = (X509Certificate)certPath.getCertificates().get(n);
        try {
            final BasicConstraints instance = BasicConstraints.getInstance(CertPathValidatorUtilities.getExtensionValue(x509Certificate, RFC3280CertPathUtilities.BASIC_CONSTRAINTS));
            if (instance == null) {
                throw new CertPathValidatorException("Intermediate certificate lacks BasicConstraints", null, certPath, n);
            }
            if (instance.isCA()) {
                return;
            }
            throw new CertPathValidatorException("Not a CA certificate", null, certPath, n);
        }
        catch (Exception ex) {
            throw new ExtCertPathValidatorException("Basic constraints extension cannot be decoded.", ex, certPath, n);
        }
    }
    
    protected static int prepareNextCertL(final CertPath certPath, final int n, final int n2) throws CertPathValidatorException {
        if (CertPathValidatorUtilities.isSelfIssued((X509Certificate)certPath.getCertificates().get(n))) {
            return n2;
        }
        if (n2 > 0) {
            return n2 - 1;
        }
        throw new ExtCertPathValidatorException("Max path length not greater than zero", null, certPath, n);
    }
    
    protected static int prepareNextCertM(CertPath pathLenConstraint, int intValue, final int n) throws CertPathValidatorException {
        final X509Certificate x509Certificate = (X509Certificate)pathLenConstraint.getCertificates().get(intValue);
        try {
            final BasicConstraints instance = BasicConstraints.getInstance(CertPathValidatorUtilities.getExtensionValue(x509Certificate, RFC3280CertPathUtilities.BASIC_CONSTRAINTS));
            if (instance != null) {
                pathLenConstraint = (CertPath)instance.getPathLenConstraint();
                if (pathLenConstraint != null) {
                    intValue = ((BigInteger)pathLenConstraint).intValue();
                    if (intValue < n) {
                        return intValue;
                    }
                }
            }
            return n;
        }
        catch (Exception ex) {
            throw new ExtCertPathValidatorException("Basic constraints extension cannot be decoded.", ex, pathLenConstraint, intValue);
        }
    }
    
    protected static void prepareNextCertN(final CertPath certPath, final int n) throws CertPathValidatorException {
        final boolean[] keyUsage = ((X509Certificate)certPath.getCertificates().get(n)).getKeyUsage();
        if (keyUsage == null) {
            return;
        }
        if (keyUsage[5]) {
            return;
        }
        throw new ExtCertPathValidatorException("Issuer certificate keyusage extension is critical and does not permit key signing.", null, certPath, n);
    }
    
    protected static void prepareNextCertO(final CertPath certPath, final int index, final Set obj, final List list) throws CertPathValidatorException {
        final X509Certificate x509Certificate = (X509Certificate)certPath.getCertificates().get(index);
        final Iterator<PKIXCertPathChecker> iterator = list.iterator();
        while (iterator.hasNext()) {
            try {
                iterator.next().check(x509Certificate, obj);
                continue;
            }
            catch (CertPathValidatorException ex) {
                throw new CertPathValidatorException(ex.getMessage(), ex.getCause(), certPath, index);
            }
            break;
        }
        if (obj.isEmpty()) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Certificate has unsupported critical extension: ");
        sb.append(obj);
        throw new ExtCertPathValidatorException(sb.toString(), null, certPath, index);
    }
    
    protected static Set processCRLA1i(final Date date, final PKIXExtendedParameters pkixExtendedParameters, final X509Certificate x509Certificate, final X509CRL x509CRL) throws AnnotatedException {
        final HashSet set = new HashSet();
        if (pkixExtendedParameters.isUseDeltasEnabled()) {
            try {
                CRLDistPoint crlDistPoint;
                if ((crlDistPoint = CRLDistPoint.getInstance(CertPathValidatorUtilities.getExtensionValue(x509Certificate, RFC3280CertPathUtilities.FRESHEST_CRL))) == null) {
                    try {
                        crlDistPoint = CRLDistPoint.getInstance(CertPathValidatorUtilities.getExtensionValue(x509CRL, RFC3280CertPathUtilities.FRESHEST_CRL));
                    }
                    catch (AnnotatedException ex) {
                        throw new AnnotatedException("Freshest CRL extension could not be decoded from CRL.", ex);
                    }
                }
                if (crlDistPoint != null) {
                    final ArrayList<PKIXCRLStore> list = new ArrayList<PKIXCRLStore>();
                    list.addAll((Collection<?>)pkixExtendedParameters.getCRLStores());
                    try {
                        list.addAll((Collection<?>)CertPathValidatorUtilities.getAdditionalStoresFromCRLDistributionPoint(crlDistPoint, pkixExtendedParameters.getNamedCRLStoreMap()));
                        try {
                            set.addAll(CertPathValidatorUtilities.getDeltaCRLs(date, x509CRL, pkixExtendedParameters.getCertStores(), list));
                            return set;
                        }
                        catch (AnnotatedException ex2) {
                            throw new AnnotatedException("Exception obtaining delta CRLs.", ex2);
                        }
                    }
                    catch (AnnotatedException ex3) {
                        throw new AnnotatedException("No new delta CRL locations could be added from Freshest CRL extension.", ex3);
                    }
                }
            }
            catch (AnnotatedException ex4) {
                throw new AnnotatedException("Freshest CRL extension could not be decoded from certificate.", ex4);
            }
        }
        return set;
    }
    
    protected static Set[] processCRLA1ii(Date date, final PKIXExtendedParameters pkixExtendedParameters, final X509Certificate certificateChecking, final X509CRL x509CRL) throws AnnotatedException {
        final HashSet set = new HashSet();
        final X509CRLSelector x509CRLSelector = new X509CRLSelector();
        x509CRLSelector.setCertificateChecking(certificateChecking);
        try {
            x509CRLSelector.addIssuerName(PrincipalUtils.getIssuerPrincipal(x509CRL).getEncoded());
            final PKIXCRLStoreSelector<? extends CRL> build = new PKIXCRLStoreSelector.Builder(x509CRLSelector).setCompleteCRLEnabled(true).build();
            if (pkixExtendedParameters.getDate() != null) {
                date = pkixExtendedParameters.getDate();
            }
            final Set crLs = RFC3280CertPathUtilities.CRL_UTIL.findCRLs(build, date, pkixExtendedParameters.getCertStores(), pkixExtendedParameters.getCRLStores());
            if (pkixExtendedParameters.isUseDeltasEnabled()) {
                try {
                    set.addAll(CertPathValidatorUtilities.getDeltaCRLs(date, x509CRL, pkixExtendedParameters.getCertStores(), pkixExtendedParameters.getCRLStores()));
                }
                catch (AnnotatedException ex) {
                    throw new AnnotatedException("Exception obtaining delta CRLs.", ex);
                }
            }
            return new Set[] { crLs, set };
        }
        catch (Exception obj) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot extract issuer from CRL.");
            sb.append(obj);
            throw new AnnotatedException(sb.toString(), obj);
        }
    }
    
    protected static void processCRLB1(final DistributionPoint distributionPoint, Object o, final X509CRL x509CRL) throws AnnotatedException {
        final ASN1Primitive extensionValue = CertPathValidatorUtilities.getExtensionValue(x509CRL, RFC3280CertPathUtilities.ISSUING_DISTRIBUTION_POINT);
        int i = 0;
        boolean b;
        if (extensionValue != null && IssuingDistributionPoint.getInstance(extensionValue).isIndirectCRL()) {
            b = true;
        }
        else {
            b = false;
        }
        try {
            final byte[] encoded = PrincipalUtils.getIssuerPrincipal(x509CRL).getEncoded();
            int n;
            if (distributionPoint.getCRLIssuer() != null) {
                final GeneralName[] names = distributionPoint.getCRLIssuer().getNames();
                n = 0;
                while (i < names.length) {
                    int n2 = n;
                    if (names[i].getTagNo() == 4) {
                        try {
                            final boolean equal = Arrays.areEqual(names[i].getName().toASN1Primitive().getEncoded(), encoded);
                            n2 = n;
                            if (equal) {
                                n2 = 1;
                            }
                        }
                        catch (IOException ex) {
                            throw new AnnotatedException("CRL issuer information from distribution point cannot be decoded.", ex);
                        }
                    }
                    ++i;
                    n = n2;
                }
                if (n != 0 && !b) {
                    throw new AnnotatedException("Distribution point contains cRLIssuer field but CRL is not indirect.");
                }
                if (n == 0) {
                    throw new AnnotatedException("CRL issuer of CRL does not match CRL issuer of distribution point.");
                }
            }
            else if (PrincipalUtils.getIssuerPrincipal(x509CRL).equals(PrincipalUtils.getEncodedIssuerPrincipal(o))) {
                n = 1;
            }
            else {
                n = 0;
            }
            if (n != 0) {
                return;
            }
            throw new AnnotatedException("Cannot find matching CRL issuer for certificate.");
        }
        catch (IOException ex2) {
            o = new StringBuilder();
            ((StringBuilder)o).append("Exception encoding CRL issuer: ");
            ((StringBuilder)o).append(ex2.getMessage());
            throw new AnnotatedException(((StringBuilder)o).toString(), ex2);
        }
    }
    
    protected static void processCRLB2(final DistributionPoint distributionPoint, final Object o, final X509CRL x509CRL) throws AnnotatedException {
        try {
            final IssuingDistributionPoint instance = IssuingDistributionPoint.getInstance(CertPathValidatorUtilities.getExtensionValue(x509CRL, RFC3280CertPathUtilities.ISSUING_DISTRIBUTION_POINT));
            if (instance != null) {
                if (instance.getDistributionPoint() != null) {
                    final DistributionPointName distributionPoint2 = IssuingDistributionPoint.getInstance(instance).getDistributionPoint();
                    final ArrayList<GeneralName> list = new ArrayList<GeneralName>();
                    final int type = distributionPoint2.getType();
                    final int n = 0;
                    final boolean b = false;
                    if (type == 0) {
                        final GeneralName[] names = GeneralNames.getInstance(distributionPoint2.getName()).getNames();
                        for (int i = 0; i < names.length; ++i) {
                            list.add(names[i]);
                        }
                    }
                    if (distributionPoint2.getType() == 1) {
                        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
                        try {
                            final Enumeration objects = ASN1Sequence.getInstance(PrincipalUtils.getIssuerPrincipal(x509CRL)).getObjects();
                            while (objects.hasMoreElements()) {
                                asn1EncodableVector.add(objects.nextElement());
                            }
                            asn1EncodableVector.add(distributionPoint2.getName());
                            list.add(new GeneralName(X500Name.getInstance(new DERSequence(asn1EncodableVector))));
                        }
                        catch (Exception ex) {
                            throw new AnnotatedException("Could not read CRL issuer.", ex);
                        }
                    }
                    if (distributionPoint.getDistributionPoint() != null) {
                        final DistributionPointName distributionPoint3 = distributionPoint.getDistributionPoint();
                        GeneralName[] names2 = null;
                        if (distributionPoint3.getType() == 0) {
                            names2 = GeneralNames.getInstance(distributionPoint3.getName()).getNames();
                        }
                        if (distributionPoint3.getType() == 1) {
                        Label_0336_Outer:
                            while (true) {
                                if (distributionPoint.getCRLIssuer() != null) {
                                    final GeneralName[] names3 = distributionPoint.getCRLIssuer().getNames();
                                    break Label_0268;
                                }
                                Label_0299: {
                                    Label_0271: {
                                        break Label_0271;
                                        break Label_0299;
                                    }
                                    final GeneralName[] names3 = { null };
                                    try {
                                        names3[0] = new GeneralName(X500Name.getInstance(PrincipalUtils.getEncodedIssuerPrincipal(o).getEncoded()));
                                        continue Label_0336_Outer;
                                        int n2 = 0;
                                        // iftrue(Label_0362:, !objects2.hasMoreElements())
                                        while (true) {
                                            Label_0301: {
                                                break Label_0301;
                                                final ASN1EncodableVector asn1EncodableVector2;
                                                Block_27: {
                                                    break Block_27;
                                                    Label_0362: {
                                                        asn1EncodableVector2.add(distributionPoint3.getName());
                                                    }
                                                    names3[n2] = new GeneralName(X500Name.getInstance(new DERSequence(asn1EncodableVector2)));
                                                    ++n2;
                                                    break Label_0301;
                                                }
                                                final Enumeration objects2;
                                                asn1EncodableVector2.add(objects2.nextElement());
                                                continue;
                                            }
                                            names2 = names3;
                                            final Enumeration objects2 = ASN1Sequence.getInstance(names3[n2].getName().toASN1Primitive()).getObjects();
                                            final ASN1EncodableVector asn1EncodableVector2 = new ASN1EncodableVector();
                                            continue;
                                        }
                                    }
                                    // iftrue(Label_0414:, n2 >= names3.length)
                                    catch (Exception ex2) {
                                        throw new AnnotatedException("Could not read certificate issuer.", ex2);
                                    }
                                }
                                break;
                            }
                        }
                        int n3 = 0;
                        Label_0414: {
                            n3 = (b ? 1 : 0);
                        }
                        if (names2 != null) {
                            int n4 = 0;
                            while (true) {
                                n3 = (b ? 1 : 0);
                                if (n4 >= names2.length) {
                                    break;
                                }
                                if (list.contains(names2[n4])) {
                                    n3 = 1;
                                    break;
                                }
                                ++n4;
                            }
                        }
                        if (n3 == 0) {
                            throw new AnnotatedException("No match for certificate CRL issuing distribution point name to cRLIssuer CRL distribution point.");
                        }
                    }
                    else {
                        if (distributionPoint.getCRLIssuer() == null) {
                            throw new AnnotatedException("Either the cRLIssuer or the distributionPoint field must be contained in DistributionPoint.");
                        }
                        final GeneralName[] names4 = distributionPoint.getCRLIssuer().getNames();
                        int n5 = 0;
                        int n6;
                        while (true) {
                            n6 = n;
                            if (n5 >= names4.length) {
                                break;
                            }
                            if (list.contains(names4[n5])) {
                                n6 = 1;
                                break;
                            }
                            ++n5;
                        }
                        if (n6 == 0) {
                            throw new AnnotatedException("No match for certificate CRL issuing distribution point name to cRLIssuer CRL distribution point.");
                        }
                    }
                }
                try {
                    final BasicConstraints instance2 = BasicConstraints.getInstance(CertPathValidatorUtilities.getExtensionValue((X509Extension)o, RFC3280CertPathUtilities.BASIC_CONSTRAINTS));
                    if (o instanceof X509Certificate) {
                        if (instance.onlyContainsUserCerts() && instance2 != null && instance2.isCA()) {
                            throw new AnnotatedException("CA Cert CRL only contains user certificates.");
                        }
                        if (instance.onlyContainsCACerts()) {
                            if (instance2 == null || !instance2.isCA()) {
                                throw new AnnotatedException("End CRL only contains CA certificates.");
                            }
                        }
                    }
                    if (!instance.onlyContainsAttributeCerts()) {
                        return;
                    }
                    throw new AnnotatedException("onlyContainsAttributeCerts boolean is asserted.");
                }
                catch (Exception ex3) {
                    throw new AnnotatedException("Basic constraints extension could not be decoded.", ex3);
                }
            }
        }
        catch (Exception ex4) {
            throw new AnnotatedException("Issuing distribution point extension could not be decoded.", ex4);
        }
    }
    
    protected static void processCRLC(final X509CRL x509CRL, final X509CRL x509CRL2, final PKIXExtendedParameters pkixExtendedParameters) throws AnnotatedException {
        if (x509CRL == null) {
            return;
        }
        if (!x509CRL.hasUnsupportedCriticalExtension()) {
            try {
                final IssuingDistributionPoint instance = IssuingDistributionPoint.getInstance(CertPathValidatorUtilities.getExtensionValue(x509CRL2, RFC3280CertPathUtilities.ISSUING_DISTRIBUTION_POINT));
                if (pkixExtendedParameters.isUseDeltasEnabled()) {
                    if (PrincipalUtils.getIssuerPrincipal(x509CRL).equals(PrincipalUtils.getIssuerPrincipal(x509CRL2))) {
                        try {
                            final IssuingDistributionPoint instance2 = IssuingDistributionPoint.getInstance(CertPathValidatorUtilities.getExtensionValue(x509CRL, RFC3280CertPathUtilities.ISSUING_DISTRIBUTION_POINT));
                            boolean b = true;
                            Label_0084: {
                                if (instance == null) {
                                    if (instance2 == null) {
                                        break Label_0084;
                                    }
                                }
                                else if (instance.equals(instance2)) {
                                    break Label_0084;
                                }
                                b = false;
                            }
                            if (b) {
                                try {
                                    final ASN1Primitive extensionValue = CertPathValidatorUtilities.getExtensionValue(x509CRL2, RFC3280CertPathUtilities.AUTHORITY_KEY_IDENTIFIER);
                                    try {
                                        final ASN1Primitive extensionValue2 = CertPathValidatorUtilities.getExtensionValue(x509CRL, RFC3280CertPathUtilities.AUTHORITY_KEY_IDENTIFIER);
                                        if (extensionValue == null) {
                                            throw new AnnotatedException("CRL authority key identifier is null.");
                                        }
                                        if (extensionValue2 == null) {
                                            throw new AnnotatedException("Delta CRL authority key identifier is null.");
                                        }
                                        if (extensionValue.equals(extensionValue2)) {
                                            return;
                                        }
                                        throw new AnnotatedException("Delta CRL authority key identifier does not match complete CRL authority key identifier.");
                                    }
                                    catch (AnnotatedException ex) {
                                        throw new AnnotatedException("Authority key identifier extension could not be extracted from delta CRL.", ex);
                                    }
                                }
                                catch (AnnotatedException ex2) {
                                    throw new AnnotatedException("Authority key identifier extension could not be extracted from complete CRL.", ex2);
                                }
                            }
                            throw new AnnotatedException("Issuing distribution point extension from delta CRL and complete CRL does not match.");
                        }
                        catch (Exception ex3) {
                            throw new AnnotatedException("Issuing distribution point extension from delta CRL could not be decoded.", ex3);
                        }
                    }
                    throw new AnnotatedException("Complete CRL issuer does not match delta CRL issuer.");
                }
                return;
            }
            catch (Exception ex4) {
                throw new AnnotatedException("Issuing distribution point extension could not be decoded.", ex4);
            }
        }
        throw new AnnotatedException("delta CRL has unsupported critical extensions");
    }
    
    protected static ReasonsMask processCRLD(final X509CRL x509CRL, final DistributionPoint distributionPoint) throws AnnotatedException {
        try {
            final IssuingDistributionPoint instance = IssuingDistributionPoint.getInstance(CertPathValidatorUtilities.getExtensionValue(x509CRL, RFC3280CertPathUtilities.ISSUING_DISTRIBUTION_POINT));
            if (instance != null && instance.getOnlySomeReasons() != null && distributionPoint.getReasons() != null) {
                return new ReasonsMask(distributionPoint.getReasons()).intersect(new ReasonsMask(instance.getOnlySomeReasons()));
            }
            if ((instance == null || instance.getOnlySomeReasons() == null) && distributionPoint.getReasons() == null) {
                return ReasonsMask.allReasons;
            }
            ReasonsMask allReasons;
            if (distributionPoint.getReasons() == null) {
                allReasons = ReasonsMask.allReasons;
            }
            else {
                allReasons = new ReasonsMask(distributionPoint.getReasons());
            }
            ReasonsMask allReasons2;
            if (instance == null) {
                allReasons2 = ReasonsMask.allReasons;
            }
            else {
                allReasons2 = new ReasonsMask(instance.getOnlySomeReasons());
            }
            return allReasons.intersect(allReasons2);
        }
        catch (Exception ex) {
            throw new AnnotatedException("Issuing distribution point extension could not be decoded.", ex);
        }
    }
    
    protected static Set processCRLF(final X509CRL x509CRL, final Object o, final X509Certificate other, final PublicKey publicKey, final PKIXExtendedParameters pkixExtendedParameters, final List list, final JcaJceHelper jcaJceHelper) throws AnnotatedException {
        final X509CertSelector x509CertSelector = new X509CertSelector();
        try {
            x509CertSelector.setSubject(PrincipalUtils.getIssuerPrincipal(x509CRL).getEncoded());
            final PKIXCertStoreSelector<? extends Certificate> build = new PKIXCertStoreSelector.Builder(x509CertSelector).build();
            try {
                final Collection certificates = CertPathValidatorUtilities.findCertificates(build, pkixExtendedParameters.getCertificateStores());
                certificates.addAll(CertPathValidatorUtilities.findCertificates(build, pkixExtendedParameters.getCertStores()));
                certificates.add(other);
                final Iterator<X509Certificate> iterator = certificates.iterator();
                final ArrayList<X509Certificate> list2 = new ArrayList<X509Certificate>();
                final ArrayList<PublicKey> list3 = new ArrayList<PublicKey>();
                int i;
                while (true) {
                    final boolean hasNext = iterator.hasNext();
                    i = 0;
                    if (!hasNext) {
                        break;
                    }
                    final X509Certificate certificate = iterator.next();
                    if (!certificate.equals(other)) {
                        try {
                            final PKIXCertPathBuilderSpi pkixCertPathBuilderSpi = new PKIXCertPathBuilderSpi(true);
                            final X509CertSelector x509CertSelector2 = new X509CertSelector();
                            x509CertSelector2.setCertificate(certificate);
                            final PKIXExtendedParameters.Builder setTargetConstraints = new PKIXExtendedParameters.Builder(pkixExtendedParameters).setTargetConstraints(new PKIXCertStoreSelector.Builder(x509CertSelector2).build());
                            if (list.contains(certificate)) {
                                setTargetConstraints.setRevocationEnabled(false);
                            }
                            else {
                                setTargetConstraints.setRevocationEnabled(true);
                            }
                            final List<? extends Certificate> certificates2 = pkixCertPathBuilderSpi.engineBuild(new PKIXExtendedBuilderParameters.Builder(setTargetConstraints.build()).build()).getCertPath().getCertificates();
                            list2.add(certificate);
                            list3.add(CertPathValidatorUtilities.getNextWorkingKey(certificates2, 0, jcaJceHelper));
                            continue;
                        }
                        catch (Exception ex) {
                            throw new AnnotatedException(ex.getMessage());
                        }
                        catch (CertPathValidatorException ex2) {
                            throw new AnnotatedException("Public key of issuer certificate of CRL could not be retrieved.", ex2);
                        }
                        catch (CertPathBuilderException ex3) {
                            throw new AnnotatedException("CertPath for CRL signer failed to validate.", ex3);
                        }
                        break;
                    }
                    list2.add(certificate);
                    list3.add(publicKey);
                }
                final HashSet<PublicKey> set = new HashSet<PublicKey>();
                Object o2 = null;
                while (i < list2.size()) {
                    final boolean[] keyUsage = list2.get(i).getKeyUsage();
                    if (keyUsage != null && (keyUsage.length < 7 || !keyUsage[6])) {
                        o2 = new AnnotatedException("Issuer certificate key usage extension does not permit CRL signing.");
                    }
                    else {
                        set.add(list3.get(i));
                    }
                    ++i;
                }
                if (set.isEmpty() && o2 == null) {
                    throw new AnnotatedException("Cannot find a valid issuer certificate.");
                }
                if (!set.isEmpty()) {
                    return set;
                }
                if (o2 == null) {
                    return set;
                }
                throw o2;
            }
            catch (AnnotatedException ex4) {
                throw new AnnotatedException("Issuer certificate for CRL cannot be searched.", ex4);
            }
        }
        catch (IOException ex5) {
            throw new AnnotatedException("Subject criteria for certificate selector to find issuer certificate for CRL could not be set.", ex5);
        }
    }
    
    protected static PublicKey processCRLG(final X509CRL x509CRL, final Set set) throws AnnotatedException {
        final Iterator<PublicKey> iterator = set.iterator();
        final Exception ex = null;
        while (iterator.hasNext()) {
            final PublicKey publicKey = iterator.next();
            try {
                x509CRL.verify(publicKey);
                return publicKey;
            }
            catch (Exception ex) {
                continue;
            }
            break;
        }
        throw new AnnotatedException("Cannot verify CRL.", ex);
    }
    
    protected static X509CRL processCRLH(final Set set, final PublicKey publicKey) throws AnnotatedException {
        final Iterator<X509CRL> iterator = set.iterator();
        final Exception ex = null;
        while (iterator.hasNext()) {
            final X509CRL x509CRL = iterator.next();
            try {
                x509CRL.verify(publicKey);
                return x509CRL;
            }
            catch (Exception ex) {
                continue;
            }
            break;
        }
        if (ex == null) {
            return null;
        }
        throw new AnnotatedException("Cannot verify delta CRL.", ex);
    }
    
    protected static void processCRLI(final Date date, final X509CRL x509CRL, final Object o, final CertStatus certStatus, final PKIXExtendedParameters pkixExtendedParameters) throws AnnotatedException {
        if (pkixExtendedParameters.isUseDeltasEnabled() && x509CRL != null) {
            CertPathValidatorUtilities.getCertStatus(date, x509CRL, o, certStatus);
        }
    }
    
    protected static void processCRLJ(final Date date, final X509CRL x509CRL, final Object o, final CertStatus certStatus) throws AnnotatedException {
        if (certStatus.getCertStatus() == 11) {
            CertPathValidatorUtilities.getCertStatus(date, x509CRL, o, certStatus);
        }
    }
    
    protected static void processCertA(final CertPath certPath, final PKIXExtendedParameters pkixExtendedParameters, final int n, final PublicKey publicKey, final boolean b, final X500Name obj, final X509Certificate x509Certificate, final JcaJceHelper jcaJceHelper) throws ExtCertPathValidatorException {
        final List<? extends Certificate> certificates = certPath.getCertificates();
        final X509Certificate x509Certificate2 = (X509Certificate)certificates.get(n);
        if (!b) {
            try {
                CertPathValidatorUtilities.verifyX509Certificate(x509Certificate2, publicKey, pkixExtendedParameters.getSigProvider());
            }
            catch (GeneralSecurityException ex) {
                throw new ExtCertPathValidatorException("Could not validate certificate signature.", ex, certPath, n);
            }
        }
        try {
            x509Certificate2.checkValidity(CertPathValidatorUtilities.getValidCertDateFromValidityModel(pkixExtendedParameters, certPath, n));
            if (pkixExtendedParameters.isRevocationEnabled()) {
                try {
                    checkCRLs(pkixExtendedParameters, x509Certificate2, CertPathValidatorUtilities.getValidCertDateFromValidityModel(pkixExtendedParameters, certPath, n), x509Certificate, publicKey, certificates, jcaJceHelper);
                }
                catch (AnnotatedException ex2) {
                    Throwable cause;
                    if (ex2.getCause() != null) {
                        cause = ex2.getCause();
                    }
                    else {
                        cause = ex2;
                    }
                    throw new ExtCertPathValidatorException(ex2.getMessage(), cause, certPath, n);
                }
            }
            if (PrincipalUtils.getEncodedIssuerPrincipal(x509Certificate2).equals(obj)) {
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("IssuerName(");
            sb.append(PrincipalUtils.getEncodedIssuerPrincipal(x509Certificate2));
            sb.append(") does not match SubjectName(");
            sb.append(obj);
            sb.append(") of signing certificate.");
            throw new ExtCertPathValidatorException(sb.toString(), null, certPath, n);
        }
        catch (AnnotatedException ex3) {
            throw new ExtCertPathValidatorException("Could not validate time of certificate.", ex3, certPath, n);
        }
        catch (CertificateNotYetValidException ex4) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Could not validate certificate: ");
            sb2.append(ex4.getMessage());
            throw new ExtCertPathValidatorException(sb2.toString(), ex4, certPath, n);
        }
        catch (CertificateExpiredException ex5) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Could not validate certificate: ");
            sb3.append(ex5.getMessage());
            throw new ExtCertPathValidatorException(sb3.toString(), ex5, certPath, n);
        }
    }
    
    protected static void processCertBC(final CertPath certPath, final int n, final PKIXNameConstraintValidator pkixNameConstraintValidator, final boolean b) throws CertPathValidatorException {
        final List<? extends Certificate> certificates = certPath.getCertificates();
        final X509Certificate x509Certificate = (X509Certificate)certificates.get(n);
        final int size = certificates.size();
        if (CertPathValidatorUtilities.isSelfIssued(x509Certificate) && (size - n < size || b)) {
            return;
        }
        final X500Name subjectPrincipal = PrincipalUtils.getSubjectPrincipal(x509Certificate);
        try {
            final ASN1Sequence instance = ASN1Sequence.getInstance(subjectPrincipal.getEncoded());
            try {
                pkixNameConstraintValidator.checkPermittedDN(instance);
                pkixNameConstraintValidator.checkExcludedDN(instance);
                try {
                    final GeneralNames instance2 = GeneralNames.getInstance(CertPathValidatorUtilities.getExtensionValue(x509Certificate, RFC3280CertPathUtilities.SUBJECT_ALTERNATIVE_NAME));
                    final RDN[] rdNs = X500Name.getInstance(instance).getRDNs(BCStyle.EmailAddress);
                    final int n2 = 0;
                    int i = 0;
                    while (i != rdNs.length) {
                        final GeneralName generalName = new GeneralName(1, ((ASN1String)rdNs[i].getFirst().getValue()).getString());
                        try {
                            pkixNameConstraintValidator.checkPermitted(generalName);
                            pkixNameConstraintValidator.checkExcluded(generalName);
                            ++i;
                            continue;
                        }
                        catch (PKIXNameConstraintValidatorException cause) {
                            throw new CertPathValidatorException("Subtree check for certificate subject alternative email failed.", cause, certPath, n);
                        }
                        break;
                    }
                    if (instance2 != null) {
                        try {
                            final GeneralName[] names = instance2.getNames();
                            int j = n2;
                            while (j < names.length) {
                                try {
                                    pkixNameConstraintValidator.checkPermitted(names[j]);
                                    pkixNameConstraintValidator.checkExcluded(names[j]);
                                    ++j;
                                }
                                catch (PKIXNameConstraintValidatorException cause2) {
                                    throw new CertPathValidatorException("Subtree check for certificate subject alternative name failed.", cause2, certPath, n);
                                }
                            }
                        }
                        catch (Exception cause3) {
                            throw new CertPathValidatorException("Subject alternative name contents could not be decoded.", cause3, certPath, n);
                        }
                    }
                }
                catch (Exception cause4) {
                    throw new CertPathValidatorException("Subject alternative name extension could not be decoded.", cause4, certPath, n);
                }
            }
            catch (PKIXNameConstraintValidatorException cause5) {
                throw new CertPathValidatorException("Subtree check for certificate subject failed.", cause5, certPath, n);
            }
        }
        catch (Exception cause6) {
            throw new CertPathValidatorException("Exception extracting subject name when checking subtrees.", cause6, certPath, n);
        }
    }
    
    protected static PKIXPolicyNode processCertD(CertPath certPath, int i, Set set, final PKIXPolicyNode pkixPolicyNode, final List[] array, int n, final boolean b) throws CertPathValidatorException {
        final List<? extends Certificate> certificates = certPath.getCertificates();
        final X509Certificate x509Certificate = (X509Certificate)certificates.get(i);
        final int size = certificates.size();
        final int n2 = size - i;
        try {
            final ASN1Sequence instance = ASN1Sequence.getInstance(CertPathValidatorUtilities.getExtensionValue(x509Certificate, RFC3280CertPathUtilities.CERTIFICATE_POLICIES));
            if (instance != null && pkixPolicyNode != null) {
                final Enumeration objects = instance.getObjects();
                final HashSet<String> set2 = new HashSet<String>();
                while (objects.hasMoreElements()) {
                    final PolicyInformation instance2 = PolicyInformation.getInstance(objects.nextElement());
                    final ASN1ObjectIdentifier policyIdentifier = instance2.getPolicyIdentifier();
                    set2.add(policyIdentifier.getId());
                    if (!"2.5.29.32.0".equals(policyIdentifier.getId())) {
                        try {
                            final Set qualifierSet = CertPathValidatorUtilities.getQualifierSet(instance2.getPolicyQualifiers());
                            if (!CertPathValidatorUtilities.processCertD1i(n2, array, policyIdentifier, qualifierSet)) {
                                CertPathValidatorUtilities.processCertD1ii(n2, array, policyIdentifier, qualifierSet);
                                continue;
                            }
                            continue;
                        }
                        catch (CertPathValidatorException ex) {
                            throw new ExtCertPathValidatorException("Policy qualifier info set could not be build.", ex, certPath, i);
                        }
                        break;
                    }
                }
                if (!set.isEmpty() && !set.contains("2.5.29.32.0")) {
                    certPath = (CertPath)set.iterator();
                    final HashSet<String> set3 = new HashSet<String>();
                    while (((Iterator)certPath).hasNext()) {
                        final String next = ((Iterator<String>)certPath).next();
                        if (set2.contains(next)) {
                            set3.add(next);
                        }
                    }
                    set.clear();
                    set.addAll(set3);
                }
                else {
                    set.clear();
                    set.addAll(set2);
                }
                if (n > 0 || ((n2 < size || b) && CertPathValidatorUtilities.isSelfIssued(x509Certificate))) {
                    certPath = (CertPath)instance.getObjects();
                    while (((Enumeration)certPath).hasMoreElements()) {
                        final PolicyInformation instance3 = PolicyInformation.getInstance(((Enumeration<Object>)certPath).nextElement());
                        if ("2.5.29.32.0".equals(instance3.getPolicyIdentifier().getId())) {
                            set = (Set<String>)CertPathValidatorUtilities.getQualifierSet(instance3.getPolicyQualifiers());
                            List list;
                            PKIXPolicyNode pkixPolicyNode2;
                            Iterator iterator;
                            Iterator children;
                            HashSet<CertPath> set4;
                            for (list = array[n2 - 1], i = 0; i < list.size(); ++i) {
                                pkixPolicyNode2 = list.get(i);
                                iterator = pkixPolicyNode2.getExpectedPolicies().iterator();
                                while (iterator.hasNext()) {
                                    certPath = iterator.next();
                                    if (certPath instanceof String) {
                                        certPath = certPath;
                                    }
                                    else {
                                        if (!(certPath instanceof ASN1ObjectIdentifier)) {
                                            continue;
                                        }
                                        certPath = (CertPath)((ASN1ObjectIdentifier)certPath).getId();
                                    }
                                    children = pkixPolicyNode2.getChildren();
                                    n = 0;
                                    while (children.hasNext()) {
                                        if (((String)certPath).equals(children.next().getValidPolicy())) {
                                            n = 1;
                                        }
                                    }
                                    if (n == 0) {
                                        set4 = new HashSet<CertPath>();
                                        set4.add(certPath);
                                        certPath = (CertPath)new PKIXPolicyNode(new ArrayList(), n2, set4, pkixPolicyNode2, set, (String)certPath, false);
                                        pkixPolicyNode2.addChild((PKIXPolicyNode)certPath);
                                        array[n2].add(certPath);
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
                i = n2 - 1;
                certPath = (CertPath)pkixPolicyNode;
                while (i >= 0) {
                    final List list2 = array[i];
                    n = 0;
                    CertPath certPath2;
                    while (true) {
                        certPath2 = certPath;
                        if (n >= list2.size()) {
                            break;
                        }
                        final PKIXPolicyNode pkixPolicyNode3 = list2.get(n);
                        CertPath certPath3 = certPath;
                        if (!pkixPolicyNode3.hasChildren()) {
                            certPath = (CertPath)CertPathValidatorUtilities.removePolicyNode((PKIXPolicyNode)certPath, array, pkixPolicyNode3);
                            if ((certPath3 = certPath) == null) {
                                certPath2 = certPath;
                                break;
                            }
                        }
                        ++n;
                        certPath = certPath3;
                    }
                    certPath = certPath2;
                    --i;
                }
                set = x509Certificate.getCriticalExtensionOIDs();
                if (set != null) {
                    final boolean contains = set.contains(RFC3280CertPathUtilities.CERTIFICATE_POLICIES);
                    List list3;
                    for (list3 = array[n2], i = 0; i < list3.size(); ++i) {
                        list3.get(i).setCritical(contains);
                    }
                }
                return (PKIXPolicyNode)certPath;
            }
            return null;
        }
        catch (AnnotatedException ex2) {
            throw new ExtCertPathValidatorException("Could not read certificate policies extension from certificate.", ex2, certPath, i);
        }
    }
    
    protected static PKIXPolicyNode processCertE(final CertPath certPath, final int n, PKIXPolicyNode pkixPolicyNode) throws CertPathValidatorException {
        final X509Certificate x509Certificate = (X509Certificate)certPath.getCertificates().get(n);
        try {
            if (ASN1Sequence.getInstance(CertPathValidatorUtilities.getExtensionValue(x509Certificate, RFC3280CertPathUtilities.CERTIFICATE_POLICIES)) == null) {
                pkixPolicyNode = null;
            }
            return pkixPolicyNode;
        }
        catch (AnnotatedException ex) {
            throw new ExtCertPathValidatorException("Could not read certificate policies extension from certificate.", ex, certPath, n);
        }
    }
    
    protected static void processCertF(final CertPath certPath, final int n, final PKIXPolicyNode pkixPolicyNode, final int n2) throws CertPathValidatorException {
        if (n2 > 0) {
            return;
        }
        if (pkixPolicyNode != null) {
            return;
        }
        throw new ExtCertPathValidatorException("No valid policy tree found when one expected.", null, certPath, n);
    }
    
    protected static int wrapupCertA(final int n, final X509Certificate x509Certificate) {
        int n2 = n;
        if (!CertPathValidatorUtilities.isSelfIssued(x509Certificate) && (n2 = n) != 0) {
            n2 = n - 1;
        }
        return n2;
    }
    
    protected static int wrapupCertB(final CertPath certPath, final int n, final int n2) throws CertPathValidatorException {
        final X509Certificate x509Certificate = (X509Certificate)certPath.getCertificates().get(n);
        try {
            final ASN1Sequence instance = ASN1Sequence.getInstance(CertPathValidatorUtilities.getExtensionValue(x509Certificate, RFC3280CertPathUtilities.POLICY_CONSTRAINTS));
            if (instance != null) {
                final Enumeration objects = instance.getObjects();
                while (objects.hasMoreElements()) {
                    final ASN1TaggedObject asn1TaggedObject = objects.nextElement();
                    if (asn1TaggedObject.getTagNo() != 0) {
                        continue;
                    }
                    try {
                        if (ASN1Integer.getInstance(asn1TaggedObject, false).intValueExact() == 0) {
                            return 0;
                        }
                        continue;
                    }
                    catch (Exception ex) {
                        throw new ExtCertPathValidatorException("Policy constraints requireExplicitPolicy field could not be decoded.", ex, certPath, n);
                    }
                    break;
                }
            }
            return n2;
        }
        catch (AnnotatedException ex2) {
            throw new ExtCertPathValidatorException("Policy constraints could not be decoded.", ex2, certPath, n);
        }
    }
    
    protected static void wrapupCertF(final CertPath certPath, final int index, final List list, final Set obj) throws CertPathValidatorException {
        final X509Certificate x509Certificate = (X509Certificate)certPath.getCertificates().get(index);
        final Iterator<PKIXCertPathChecker> iterator = list.iterator();
        while (iterator.hasNext()) {
            try {
                iterator.next().check(x509Certificate, obj);
                continue;
            }
            catch (Exception cause) {
                throw new CertPathValidatorException("Additional certificate path checker failed.", cause, certPath, index);
            }
            catch (CertPathValidatorException ex) {
                throw new ExtCertPathValidatorException(ex.getMessage(), ex, certPath, index);
            }
            break;
        }
        if (obj.isEmpty()) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Certificate has unsupported critical extension: ");
        sb.append(obj);
        throw new ExtCertPathValidatorException(sb.toString(), null, certPath, index);
    }
    
    protected static PKIXPolicyNode wrapupCertG(final CertPath certPath, final PKIXExtendedParameters pkixExtendedParameters, final Set set, int i, final List[] array, PKIXPolicyNode removePolicyNode, final Set set2) throws CertPathValidatorException {
        final int size = certPath.getCertificates().size();
        if (removePolicyNode != null) {
            PKIXPolicyNode pkixPolicyNode;
            if (CertPathValidatorUtilities.isAnyPolicy(set)) {
                pkixPolicyNode = removePolicyNode;
                if (pkixExtendedParameters.isExplicitPolicyRequired()) {
                    if (set2.isEmpty()) {
                        throw new ExtCertPathValidatorException("Explicit policy requested but none available.", null, certPath, i);
                    }
                    final HashSet<PKIXPolicyNode> set3 = new HashSet<PKIXPolicyNode>();
                    List list;
                    int j;
                    PKIXPolicyNode pkixPolicyNode2;
                    Iterator children;
                    for (i = 0; i < array.length; ++i) {
                        for (list = array[i], j = 0; j < list.size(); ++j) {
                            pkixPolicyNode2 = list.get(j);
                            if ("2.5.29.32.0".equals(pkixPolicyNode2.getValidPolicy())) {
                                children = pkixPolicyNode2.getChildren();
                                while (children.hasNext()) {
                                    set3.add(children.next());
                                }
                            }
                        }
                    }
                    final Iterator<Object> iterator = set3.iterator();
                    while (iterator.hasNext()) {
                        set2.contains(iterator.next().getValidPolicy());
                    }
                    if ((pkixPolicyNode = removePolicyNode) != null) {
                        i = size - 1;
                        while (true) {
                            pkixPolicyNode = removePolicyNode;
                            if (i < 0) {
                                break;
                            }
                            final List list2 = array[i];
                            PKIXPolicyNode removePolicyNode2;
                            for (int k = 0; k < list2.size(); ++k, removePolicyNode = removePolicyNode2) {
                                final PKIXPolicyNode pkixPolicyNode3 = list2.get(k);
                                removePolicyNode2 = removePolicyNode;
                                if (!pkixPolicyNode3.hasChildren()) {
                                    removePolicyNode2 = CertPathValidatorUtilities.removePolicyNode(removePolicyNode, array, pkixPolicyNode3);
                                }
                            }
                            --i;
                        }
                    }
                }
            }
            else {
                final HashSet<PKIXPolicyNode> set4 = new HashSet<PKIXPolicyNode>();
                List list3;
                int l;
                PKIXPolicyNode pkixPolicyNode4;
                Iterator children2;
                PKIXPolicyNode pkixPolicyNode5;
                for (i = 0; i < array.length; ++i) {
                    for (list3 = array[i], l = 0; l < list3.size(); ++l) {
                        pkixPolicyNode4 = list3.get(l);
                        if ("2.5.29.32.0".equals(pkixPolicyNode4.getValidPolicy())) {
                            children2 = pkixPolicyNode4.getChildren();
                            while (children2.hasNext()) {
                                pkixPolicyNode5 = children2.next();
                                if (!"2.5.29.32.0".equals(pkixPolicyNode5.getValidPolicy())) {
                                    set4.add(pkixPolicyNode5);
                                }
                            }
                        }
                    }
                }
                for (final PKIXPolicyNode pkixPolicyNode6 : set4) {
                    if (!set.contains(pkixPolicyNode6.getValidPolicy())) {
                        removePolicyNode = CertPathValidatorUtilities.removePolicyNode(removePolicyNode, array, pkixPolicyNode6);
                    }
                }
                if ((pkixPolicyNode = removePolicyNode) != null) {
                    i = size - 1;
                    while (true) {
                        pkixPolicyNode = removePolicyNode;
                        if (i < 0) {
                            break;
                        }
                        final List list4 = array[i];
                        PKIXPolicyNode removePolicyNode3;
                        for (int n = 0; n < list4.size(); ++n, removePolicyNode = removePolicyNode3) {
                            final PKIXPolicyNode pkixPolicyNode7 = list4.get(n);
                            removePolicyNode3 = removePolicyNode;
                            if (!pkixPolicyNode7.hasChildren()) {
                                removePolicyNode3 = CertPathValidatorUtilities.removePolicyNode(removePolicyNode, array, pkixPolicyNode7);
                            }
                        }
                        --i;
                    }
                }
            }
            return pkixPolicyNode;
        }
        if (!pkixExtendedParameters.isExplicitPolicyRequired()) {
            return null;
        }
        throw new ExtCertPathValidatorException("Explicit policy requested but none available.", null, certPath, i);
    }
}
