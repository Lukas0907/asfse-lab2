// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util;
import org.bouncycastle.crypto.params.DSAParameters;
import java.security.spec.DSAParameterSpec;
import org.bouncycastle.jcajce.spec.DHDomainParameterSpec;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.crypto.params.DHParameters;
import javax.crypto.spec.DHParameterSpec;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import org.bouncycastle.jcajce.provider.config.ProviderConfigurationPermission;
import org.bouncycastle.jce.spec.ECParameterSpec;
import java.util.Map;
import java.util.Set;
import java.security.Permission;
import org.bouncycastle.jcajce.provider.config.ProviderConfiguration;

class BouncyCastleProviderConfiguration implements ProviderConfiguration
{
    private static Permission BC_ADDITIONAL_EC_CURVE_PERMISSION;
    private static Permission BC_DH_LOCAL_PERMISSION;
    private static Permission BC_DH_PERMISSION;
    private static Permission BC_EC_CURVE_PERMISSION;
    private static Permission BC_EC_LOCAL_PERMISSION;
    private static Permission BC_EC_PERMISSION;
    private volatile Set acceptableNamedCurves;
    private volatile Map additionalECParameters;
    private volatile Object dhDefaultParams;
    private ThreadLocal dhThreadSpec;
    private volatile ECParameterSpec ecImplicitCaParams;
    private ThreadLocal ecThreadSpec;
    
    static {
        BouncyCastleProviderConfiguration.BC_EC_LOCAL_PERMISSION = new ProviderConfigurationPermission("BC", "threadLocalEcImplicitlyCa");
        BouncyCastleProviderConfiguration.BC_EC_PERMISSION = new ProviderConfigurationPermission("BC", "ecImplicitlyCa");
        BouncyCastleProviderConfiguration.BC_DH_LOCAL_PERMISSION = new ProviderConfigurationPermission("BC", "threadLocalDhDefaultParams");
        BouncyCastleProviderConfiguration.BC_DH_PERMISSION = new ProviderConfigurationPermission("BC", "DhDefaultParams");
        BouncyCastleProviderConfiguration.BC_EC_CURVE_PERMISSION = new ProviderConfigurationPermission("BC", "acceptableEcCurves");
        BouncyCastleProviderConfiguration.BC_ADDITIONAL_EC_CURVE_PERMISSION = new ProviderConfigurationPermission("BC", "additionalEcParameters");
    }
    
    BouncyCastleProviderConfiguration() {
        this.ecThreadSpec = new ThreadLocal();
        this.dhThreadSpec = new ThreadLocal();
        this.acceptableNamedCurves = new HashSet();
        this.additionalECParameters = new HashMap();
    }
    
    @Override
    public Set getAcceptableNamedCurves() {
        return Collections.unmodifiableSet((Set<?>)this.acceptableNamedCurves);
    }
    
    @Override
    public Map getAdditionalECParameters() {
        return Collections.unmodifiableMap((Map<?, ?>)this.additionalECParameters);
    }
    
    @Override
    public DHParameterSpec getDHDefaultParameters(final int n) {
        Object o;
        if ((o = this.dhThreadSpec.get()) == null) {
            o = this.dhDefaultParams;
        }
        if (o instanceof DHParameterSpec) {
            final DHParameterSpec dhParameterSpec = (DHParameterSpec)o;
            if (dhParameterSpec.getP().bitLength() == n) {
                return dhParameterSpec;
            }
        }
        else if (o instanceof DHParameterSpec[]) {
            final DHParameterSpec[] array = (DHParameterSpec[])o;
            for (int i = 0; i != array.length; ++i) {
                if (array[i].getP().bitLength() == n) {
                    return array[i];
                }
            }
        }
        final DHParameters dhParameters = CryptoServicesRegistrar.getSizedProperty(CryptoServicesRegistrar.Property.DH_DEFAULT_PARAMS, n);
        if (dhParameters != null) {
            return new DHDomainParameterSpec(dhParameters);
        }
        return null;
    }
    
    @Override
    public DSAParameterSpec getDSADefaultParameters(final int n) {
        final DSAParameters dsaParameters = CryptoServicesRegistrar.getSizedProperty(CryptoServicesRegistrar.Property.DSA_DEFAULT_PARAMS, n);
        if (dsaParameters != null) {
            return new DSAParameterSpec(dsaParameters.getP(), dsaParameters.getQ(), dsaParameters.getG());
        }
        return null;
    }
    
    @Override
    public ECParameterSpec getEcImplicitlyCa() {
        final ECParameterSpec ecParameterSpec = this.ecThreadSpec.get();
        if (ecParameterSpec != null) {
            return ecParameterSpec;
        }
        return this.ecImplicitCaParams;
    }
    
    void setParameter(final String s, final Object o) {
        final SecurityManager securityManager = System.getSecurityManager();
        ThreadLocal threadLocal;
        if (s.equals("threadLocalEcImplicitlyCa")) {
            if (securityManager != null) {
                securityManager.checkPermission(BouncyCastleProviderConfiguration.BC_EC_LOCAL_PERMISSION);
            }
            ECParameterSpec convertSpec;
            if (!(o instanceof ECParameterSpec) && o != null) {
                convertSpec = EC5Util.convertSpec((java.security.spec.ECParameterSpec)o);
            }
            else {
                convertSpec = (ECParameterSpec)o;
            }
            if (convertSpec != null) {
                this.ecThreadSpec.set(convertSpec);
                return;
            }
            threadLocal = this.ecThreadSpec;
        }
        else if (s.equals("ecImplicitlyCa")) {
            if (securityManager != null) {
                securityManager.checkPermission(BouncyCastleProviderConfiguration.BC_EC_PERMISSION);
            }
            if (!(o instanceof ECParameterSpec) && o != null) {
                this.ecImplicitCaParams = EC5Util.convertSpec((java.security.spec.ECParameterSpec)o);
                return;
            }
            this.ecImplicitCaParams = (ECParameterSpec)o;
            return;
        }
        else if (s.equals("threadLocalDhDefaultParams")) {
            if (securityManager != null) {
                securityManager.checkPermission(BouncyCastleProviderConfiguration.BC_DH_LOCAL_PERMISSION);
            }
            if (!(o instanceof DHParameterSpec) && !(o instanceof DHParameterSpec[]) && o != null) {
                throw new IllegalArgumentException("not a valid DHParameterSpec");
            }
            threadLocal = this.dhThreadSpec;
            if (o != null) {
                threadLocal.set(o);
                return;
            }
        }
        else if (s.equals("DhDefaultParams")) {
            if (securityManager != null) {
                securityManager.checkPermission(BouncyCastleProviderConfiguration.BC_DH_PERMISSION);
            }
            if (!(o instanceof DHParameterSpec) && !(o instanceof DHParameterSpec[]) && o != null) {
                throw new IllegalArgumentException("not a valid DHParameterSpec or DHParameterSpec[]");
            }
            this.dhDefaultParams = o;
            return;
        }
        else {
            if (s.equals("acceptableEcCurves")) {
                if (securityManager != null) {
                    securityManager.checkPermission(BouncyCastleProviderConfiguration.BC_EC_CURVE_PERMISSION);
                }
                this.acceptableNamedCurves = (Set)o;
                return;
            }
            if (s.equals("additionalEcParameters")) {
                if (securityManager != null) {
                    securityManager.checkPermission(BouncyCastleProviderConfiguration.BC_ADDITIONAL_EC_CURVE_PERMISSION);
                }
                this.additionalECParameters = (Map)o;
            }
            return;
        }
        threadLocal.remove();
    }
}
