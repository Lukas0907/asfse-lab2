// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

import java.util.Iterator;
import org.bouncycastle.pqc.jcajce.provider.qtesla.QTESLAKeyFactorySpi;
import org.bouncycastle.pqc.jcajce.provider.rainbow.RainbowKeyFactorySpi;
import org.bouncycastle.pqc.jcajce.provider.mceliece.McElieceCCA2KeyFactorySpi;
import org.bouncycastle.pqc.jcajce.provider.mceliece.McElieceKeyFactorySpi;
import org.bouncycastle.pqc.jcajce.provider.xmss.XMSSMTKeyFactorySpi;
import org.bouncycastle.pqc.jcajce.provider.xmss.XMSSKeyFactorySpi;
import org.bouncycastle.pqc.jcajce.provider.newhope.NHKeyFactorySpi;
import org.bouncycastle.pqc.jcajce.provider.sphincs.Sphincs256KeyFactorySpi;
import org.bouncycastle.pqc.asn1.PQCObjectIdentifiers;
import org.bouncycastle.jcajce.provider.util.AlgorithmProvider;
import org.bouncycastle.jcajce.provider.symmetric.util.ClassUtil;
import java.security.PublicKey;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import java.io.IOException;
import java.security.PrivateKey;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.jcajce.provider.util.AsymmetricKeyInfoConverter;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.HashMap;
import java.util.Map;
import org.bouncycastle.jcajce.provider.config.ProviderConfiguration;
import org.bouncycastle.jcajce.provider.config.ConfigurableProvider;
import java.security.Provider;

public final class BouncyCastleProvider extends Provider implements ConfigurableProvider
{
    private static final String[] ASYMMETRIC_CIPHERS;
    private static final String[] ASYMMETRIC_GENERIC;
    private static final String ASYMMETRIC_PACKAGE = "org.bouncycastle.jcajce.provider.asymmetric.";
    public static final ProviderConfiguration CONFIGURATION;
    private static final String[] DIGESTS;
    private static final String DIGEST_PACKAGE = "org.bouncycastle.jcajce.provider.digest.";
    private static final String[] KEYSTORES;
    private static final String KEYSTORE_PACKAGE = "org.bouncycastle.jcajce.provider.keystore.";
    public static final String PROVIDER_NAME = "BC";
    private static final String[] SECURE_RANDOMS;
    private static final String SECURE_RANDOM_PACKAGE = "org.bouncycastle.jcajce.provider.drbg.";
    private static final String[] SYMMETRIC_CIPHERS;
    private static final String[] SYMMETRIC_GENERIC;
    private static final String[] SYMMETRIC_MACS;
    private static final String SYMMETRIC_PACKAGE = "org.bouncycastle.jcajce.provider.symmetric.";
    private static String info = "BouncyCastle Security Provider v1.64";
    private static final Map keyInfoConverters;
    
    static {
        CONFIGURATION = new BouncyCastleProviderConfiguration();
        keyInfoConverters = new HashMap();
        SYMMETRIC_GENERIC = new String[] { "PBEPBKDF1", "PBEPBKDF2", "PBEPKCS12", "TLSKDF", "SCRYPT" };
        SYMMETRIC_MACS = new String[] { "SipHash", "Poly1305" };
        SYMMETRIC_CIPHERS = new String[] { "AES", "ARC4", "ARIA", "Blowfish", "Camellia", "CAST5", "CAST6", "ChaCha", "DES", "DESede", "GOST28147", "Grainv1", "Grain128", "HC128", "HC256", "IDEA", "Noekeon", "RC2", "RC5", "RC6", "Rijndael", "Salsa20", "SEED", "Serpent", "Shacal2", "Skipjack", "SM4", "TEA", "Twofish", "Threefish", "VMPC", "VMPCKSA3", "XTEA", "XSalsa20", "OpenSSLPBKDF", "DSTU7624", "GOST3412_2015", "Zuc" };
        ASYMMETRIC_GENERIC = new String[] { "X509", "IES" };
        ASYMMETRIC_CIPHERS = new String[] { "DSA", "DH", "EC", "RSA", "GOST", "ECGOST", "ElGamal", "DSTU4145", "GM", "EdEC" };
        DIGESTS = new String[] { "GOST3411", "Keccak", "MD2", "MD4", "MD5", "SHA1", "RIPEMD128", "RIPEMD160", "RIPEMD256", "RIPEMD320", "SHA224", "SHA256", "SHA384", "SHA512", "SHA3", "Skein", "SM3", "Tiger", "Whirlpool", "Blake2b", "Blake2s", "DSTU7564", "Haraka" };
        KEYSTORES = new String[] { "BC", "BCFKS", "PKCS12" };
        SECURE_RANDOMS = new String[] { "DRBG" };
    }
    
    public BouncyCastleProvider() {
        super("BC", 1.64, BouncyCastleProvider.info);
        AccessController.doPrivileged((PrivilegedAction<Object>)new PrivilegedAction() {
            @Override
            public Object run() {
                BouncyCastleProvider.this.setup();
                return null;
            }
        });
    }
    
    private static AsymmetricKeyInfoConverter getAsymmetricKeyInfoConverter(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        synchronized (BouncyCastleProvider.keyInfoConverters) {
            return BouncyCastleProvider.keyInfoConverters.get(asn1ObjectIdentifier);
        }
    }
    
    public static PrivateKey getPrivateKey(final PrivateKeyInfo privateKeyInfo) throws IOException {
        final AsymmetricKeyInfoConverter asymmetricKeyInfoConverter = getAsymmetricKeyInfoConverter(privateKeyInfo.getPrivateKeyAlgorithm().getAlgorithm());
        if (asymmetricKeyInfoConverter == null) {
            return null;
        }
        return asymmetricKeyInfoConverter.generatePrivate(privateKeyInfo);
    }
    
    public static PublicKey getPublicKey(final SubjectPublicKeyInfo subjectPublicKeyInfo) throws IOException {
        final AsymmetricKeyInfoConverter asymmetricKeyInfoConverter = getAsymmetricKeyInfoConverter(subjectPublicKeyInfo.getAlgorithm().getAlgorithm());
        if (asymmetricKeyInfoConverter == null) {
            return null;
        }
        return asymmetricKeyInfoConverter.generatePublic(subjectPublicKeyInfo);
    }
    
    private void loadAlgorithms(final String s, final String[] array) {
        for (int i = 0; i != array.length; ++i) {
            final StringBuilder sb = new StringBuilder();
            sb.append(s);
            sb.append(array[i]);
            sb.append("$Mappings");
            final Class loadClass = ClassUtil.loadClass(BouncyCastleProvider.class, sb.toString());
            if (loadClass != null) {
                try {
                    loadClass.newInstance().configure(this);
                }
                catch (Exception obj) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("cannot create instance of ");
                    sb2.append(s);
                    sb2.append(array[i]);
                    sb2.append("$Mappings : ");
                    sb2.append(obj);
                    throw new InternalError(sb2.toString());
                }
            }
        }
    }
    
    private void loadPQCKeys() {
        this.addKeyInfoConverter(PQCObjectIdentifiers.sphincs256, new Sphincs256KeyFactorySpi());
        this.addKeyInfoConverter(PQCObjectIdentifiers.newHope, new NHKeyFactorySpi());
        this.addKeyInfoConverter(PQCObjectIdentifiers.xmss, new XMSSKeyFactorySpi());
        this.addKeyInfoConverter(PQCObjectIdentifiers.xmss_mt, new XMSSMTKeyFactorySpi());
        this.addKeyInfoConverter(PQCObjectIdentifiers.mcEliece, new McElieceKeyFactorySpi());
        this.addKeyInfoConverter(PQCObjectIdentifiers.mcElieceCca2, new McElieceCCA2KeyFactorySpi());
        this.addKeyInfoConverter(PQCObjectIdentifiers.rainbow, new RainbowKeyFactorySpi());
        this.addKeyInfoConverter(PQCObjectIdentifiers.qTESLA_p_I, new QTESLAKeyFactorySpi());
        this.addKeyInfoConverter(PQCObjectIdentifiers.qTESLA_p_III, new QTESLAKeyFactorySpi());
    }
    
    private void setup() {
        this.loadAlgorithms("org.bouncycastle.jcajce.provider.digest.", BouncyCastleProvider.DIGESTS);
        this.loadAlgorithms("org.bouncycastle.jcajce.provider.symmetric.", BouncyCastleProvider.SYMMETRIC_GENERIC);
        this.loadAlgorithms("org.bouncycastle.jcajce.provider.symmetric.", BouncyCastleProvider.SYMMETRIC_MACS);
        this.loadAlgorithms("org.bouncycastle.jcajce.provider.symmetric.", BouncyCastleProvider.SYMMETRIC_CIPHERS);
        this.loadAlgorithms("org.bouncycastle.jcajce.provider.asymmetric.", BouncyCastleProvider.ASYMMETRIC_GENERIC);
        this.loadAlgorithms("org.bouncycastle.jcajce.provider.asymmetric.", BouncyCastleProvider.ASYMMETRIC_CIPHERS);
        this.loadAlgorithms("org.bouncycastle.jcajce.provider.keystore.", BouncyCastleProvider.KEYSTORES);
        this.loadAlgorithms("org.bouncycastle.jcajce.provider.drbg.", BouncyCastleProvider.SECURE_RANDOMS);
        this.loadPQCKeys();
        this.put("X509Store.CERTIFICATE/COLLECTION", "org.bouncycastle.jce.provider.X509StoreCertCollection");
        this.put("X509Store.ATTRIBUTECERTIFICATE/COLLECTION", "org.bouncycastle.jce.provider.X509StoreAttrCertCollection");
        this.put("X509Store.CRL/COLLECTION", "org.bouncycastle.jce.provider.X509StoreCRLCollection");
        this.put("X509Store.CERTIFICATEPAIR/COLLECTION", "org.bouncycastle.jce.provider.X509StoreCertPairCollection");
        this.put("X509Store.CERTIFICATE/LDAP", "org.bouncycastle.jce.provider.X509StoreLDAPCerts");
        this.put("X509Store.CRL/LDAP", "org.bouncycastle.jce.provider.X509StoreLDAPCRLs");
        this.put("X509Store.ATTRIBUTECERTIFICATE/LDAP", "org.bouncycastle.jce.provider.X509StoreLDAPAttrCerts");
        this.put("X509Store.CERTIFICATEPAIR/LDAP", "org.bouncycastle.jce.provider.X509StoreLDAPCertPairs");
        this.put("X509StreamParser.CERTIFICATE", "org.bouncycastle.jce.provider.X509CertParser");
        this.put("X509StreamParser.ATTRIBUTECERTIFICATE", "org.bouncycastle.jce.provider.X509AttrCertParser");
        this.put("X509StreamParser.CRL", "org.bouncycastle.jce.provider.X509CRLParser");
        this.put("X509StreamParser.CERTIFICATEPAIR", "org.bouncycastle.jce.provider.X509CertPairParser");
        this.put("Cipher.BROKENPBEWITHMD5ANDDES", "org.bouncycastle.jce.provider.BrokenJCEBlockCipher$BrokePBEWithMD5AndDES");
        this.put("Cipher.BROKENPBEWITHSHA1ANDDES", "org.bouncycastle.jce.provider.BrokenJCEBlockCipher$BrokePBEWithSHA1AndDES");
        this.put("Cipher.OLDPBEWITHSHAANDTWOFISH-CBC", "org.bouncycastle.jce.provider.BrokenJCEBlockCipher$OldPBEWithSHAAndTwofish");
        this.put("CertPathValidator.RFC3281", "org.bouncycastle.jce.provider.PKIXAttrCertPathValidatorSpi");
        this.put("CertPathBuilder.RFC3281", "org.bouncycastle.jce.provider.PKIXAttrCertPathBuilderSpi");
        this.put("CertPathValidator.RFC3280", "org.bouncycastle.jce.provider.PKIXCertPathValidatorSpi");
        this.put("CertPathBuilder.RFC3280", "org.bouncycastle.jce.provider.PKIXCertPathBuilderSpi");
        this.put("CertPathValidator.PKIX", "org.bouncycastle.jce.provider.PKIXCertPathValidatorSpi");
        this.put("CertPathBuilder.PKIX", "org.bouncycastle.jce.provider.PKIXCertPathBuilderSpi");
        this.put("CertStore.Collection", "org.bouncycastle.jce.provider.CertStoreCollectionSpi");
        this.put("CertStore.LDAP", "org.bouncycastle.jce.provider.X509LDAPCertStoreSpi");
        this.put("CertStore.Multi", "org.bouncycastle.jce.provider.MultiCertStoreSpi");
        this.put("Alg.Alias.CertStore.X509LDAP", "LDAP");
    }
    
    @Override
    public void addAlgorithm(final String str, final String value) {
        if (!this.containsKey(str)) {
            this.put(str, value);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("duplicate provider key (");
        sb.append(str);
        sb.append(") found");
        throw new IllegalStateException(sb.toString());
    }
    
    @Override
    public void addAlgorithm(final String s, final ASN1ObjectIdentifier asn1ObjectIdentifier, final String s2) {
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append(".");
        sb.append(asn1ObjectIdentifier);
        this.addAlgorithm(sb.toString(), s2);
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(s);
        sb2.append(".OID.");
        sb2.append(asn1ObjectIdentifier);
        this.addAlgorithm(sb2.toString(), s2);
    }
    
    @Override
    public void addAttributes(final String str, final Map<String, String> map) {
        for (final String str2 : map.keySet()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" ");
            sb.append(str2);
            final String string = sb.toString();
            if (this.containsKey(string)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("duplicate provider attribute key (");
                sb2.append(string);
                sb2.append(") found");
                throw new IllegalStateException(sb2.toString());
            }
            this.put(string, map.get(str2));
        }
    }
    
    @Override
    public void addKeyInfoConverter(final ASN1ObjectIdentifier asn1ObjectIdentifier, final AsymmetricKeyInfoConverter asymmetricKeyInfoConverter) {
        synchronized (BouncyCastleProvider.keyInfoConverters) {
            BouncyCastleProvider.keyInfoConverters.put(asn1ObjectIdentifier, asymmetricKeyInfoConverter);
        }
    }
    
    @Override
    public boolean hasAlgorithm(final String s, final String s2) {
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append(".");
        sb.append(s2);
        if (!this.containsKey(sb.toString())) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Alg.Alias.");
            sb2.append(s);
            sb2.append(".");
            sb2.append(s2);
            if (!this.containsKey(sb2.toString())) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public void setParameter(final String s, final Object o) {
        synchronized (BouncyCastleProvider.CONFIGURATION) {
            ((BouncyCastleProviderConfiguration)BouncyCastleProvider.CONFIGURATION).setParameter(s, o);
        }
    }
}
