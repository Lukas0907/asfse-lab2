// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

public class PKIXNameConstraintValidatorException extends Exception
{
    private Throwable cause;
    
    public PKIXNameConstraintValidatorException(final String message) {
        super(message);
    }
    
    public PKIXNameConstraintValidatorException(final String message, final Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
