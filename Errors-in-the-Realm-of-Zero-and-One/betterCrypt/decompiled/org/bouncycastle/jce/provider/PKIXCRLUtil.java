// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

import java.security.cert.X509Certificate;
import java.security.cert.X509CRL;
import java.util.Set;
import java.util.Date;
import java.util.Iterator;
import org.bouncycastle.util.StoreException;
import org.bouncycastle.util.Selector;
import java.security.cert.CertStoreException;
import java.security.cert.CRL;
import java.security.cert.CertStore;
import org.bouncycastle.util.Store;
import java.util.HashSet;
import java.util.Collection;
import java.util.List;
import org.bouncycastle.jcajce.PKIXCRLStoreSelector;

class PKIXCRLUtil
{
    private final Collection findCRLs(final PKIXCRLStoreSelector pkixcrlStoreSelector, final List list) throws AnnotatedException {
        final HashSet set = new HashSet();
        final Iterator<Store<? extends E>> iterator = list.iterator();
        Object o = null;
        boolean b = false;
        while (iterator.hasNext()) {
            final Store<? extends E> next = iterator.next();
            Label_0071: {
                if (!(next instanceof Store)) {
                    final CertStore certStore = (CertStore)next;
                    try {
                        set.addAll(PKIXCRLStoreSelector.getCRLs(pkixcrlStoreSelector, certStore));
                        break Label_0071;
                    }
                    catch (CertStoreException ex) {
                        o = new AnnotatedException("Exception searching in X.509 CRL store.", ex);
                        continue;
                    }
                    break;
                }
                final Store<? extends E> store = next;
                try {
                    set.addAll(store.getMatches(pkixcrlStoreSelector));
                    b = true;
                }
                catch (StoreException ex2) {
                    o = new AnnotatedException("Exception searching in X.509 CRL store.", ex2);
                }
            }
        }
        if (b) {
            return set;
        }
        if (o == null) {
            return set;
        }
        throw o;
    }
    
    public Set findCRLs(final PKIXCRLStoreSelector pkixcrlStoreSelector, final Date when, final List list, final List list2) throws AnnotatedException {
        final HashSet<X509CRL> set = new HashSet<X509CRL>();
        try {
            set.addAll((Collection<?>)this.findCRLs(pkixcrlStoreSelector, list2));
            set.addAll((Collection<?>)this.findCRLs(pkixcrlStoreSelector, list));
            final HashSet<X509CRL> set2 = new HashSet<X509CRL>();
            for (final X509CRL x509CRL : set) {
                if (x509CRL.getNextUpdate().after(when)) {
                    final X509Certificate certificateChecking = pkixcrlStoreSelector.getCertificateChecking();
                    if (certificateChecking != null && !x509CRL.getThisUpdate().before(certificateChecking.getNotAfter())) {
                        continue;
                    }
                    set2.add(x509CRL);
                }
            }
            return set2;
        }
        catch (AnnotatedException ex) {
            throw new AnnotatedException("Exception obtaining complete CRLs.", ex);
        }
    }
}
