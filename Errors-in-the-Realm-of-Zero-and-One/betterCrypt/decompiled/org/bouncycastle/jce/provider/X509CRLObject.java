// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

import java.security.Provider;
import java.security.NoSuchProviderException;
import java.util.Iterator;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.util.ASN1Dump;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.CRLNumber;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.util.Strings;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.security.cert.Certificate;
import java.util.Collections;
import java.security.cert.X509CRLEntry;
import java.math.BigInteger;
import java.util.Date;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.jce.X509Principal;
import java.security.Principal;
import java.io.IOException;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.TBSCertList;
import org.bouncycastle.asn1.x509.IssuingDistributionPoint;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.x509.Extension;
import java.util.Enumeration;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.util.HashSet;
import java.util.Set;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.Signature;
import java.security.PublicKey;
import java.security.cert.CRLException;
import org.bouncycastle.asn1.x509.CertificateList;
import java.security.cert.X509CRL;

public class X509CRLObject extends X509CRL
{
    private CertificateList c;
    private int hashCodeValue;
    private boolean isHashCodeSet;
    private boolean isIndirect;
    private String sigAlgName;
    private byte[] sigAlgParams;
    
    public X509CRLObject(final CertificateList c) throws CRLException {
        this.isHashCodeSet = false;
        this.c = c;
        try {
            this.sigAlgName = X509SignatureUtil.getSignatureName(c.getSignatureAlgorithm());
            if (c.getSignatureAlgorithm().getParameters() != null) {
                this.sigAlgParams = c.getSignatureAlgorithm().getParameters().toASN1Primitive().getEncoded("DER");
            }
            else {
                this.sigAlgParams = null;
            }
            this.isIndirect = isIndirectCRL(this);
        }
        catch (Exception obj) {
            final StringBuilder sb = new StringBuilder();
            sb.append("CRL contents invalid: ");
            sb.append(obj);
            throw new CRLException(sb.toString());
        }
    }
    
    private void doVerify(final PublicKey publicKey, final Signature signature) throws CRLException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        if (!this.c.getSignatureAlgorithm().equals(this.c.getTBSCertList().getSignature())) {
            throw new CRLException("Signature algorithm on CertificateList does not match TBSCertList.");
        }
        signature.initVerify(publicKey);
        signature.update(this.getTBSCertList());
        if (signature.verify(this.getSignature())) {
            return;
        }
        throw new SignatureException("CRL does not verify with supplied public key.");
    }
    
    private Set getExtensionOIDs(final boolean b) {
        if (this.getVersion() == 2) {
            final Extensions extensions = this.c.getTBSCertList().getExtensions();
            if (extensions != null) {
                final HashSet<String> set = new HashSet<String>();
                final Enumeration oids = extensions.oids();
                while (oids.hasMoreElements()) {
                    final ASN1ObjectIdentifier asn1ObjectIdentifier = oids.nextElement();
                    if (b == extensions.getExtension(asn1ObjectIdentifier).isCritical()) {
                        set.add(asn1ObjectIdentifier.getId());
                    }
                }
                return set;
            }
        }
        return null;
    }
    
    public static boolean isIndirectCRL(final X509CRL x509CRL) throws CRLException {
        try {
            final byte[] extensionValue = x509CRL.getExtensionValue(Extension.issuingDistributionPoint.getId());
            return extensionValue != null && IssuingDistributionPoint.getInstance(ASN1OctetString.getInstance(extensionValue).getOctets()).isIndirectCRL();
        }
        catch (Exception ex) {
            throw new ExtCRLException("Exception reading IssuingDistributionPoint", ex);
        }
    }
    
    private Set loadCRLEntries() {
        final HashSet<X509CRLEntryObject> set = new HashSet<X509CRLEntryObject>();
        final Enumeration revokedCertificateEnumeration = this.c.getRevokedCertificateEnumeration();
        X500Name instance = null;
        while (revokedCertificateEnumeration.hasMoreElements()) {
            final TBSCertList.CRLEntry crlEntry = revokedCertificateEnumeration.nextElement();
            set.add(new X509CRLEntryObject(crlEntry, this.isIndirect, instance));
            if (this.isIndirect && crlEntry.hasExtensions()) {
                final Extension extension = crlEntry.getExtensions().getExtension(Extension.certificateIssuer);
                if (extension == null) {
                    continue;
                }
                instance = X500Name.getInstance(GeneralNames.getInstance(extension.getParsedValue()).getNames()[0].getName());
            }
        }
        return set;
    }
    
    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof X509CRL)) {
            return false;
        }
        if (other instanceof X509CRLObject) {
            final X509CRLObject x509CRLObject = (X509CRLObject)other;
            return (!this.isHashCodeSet || !x509CRLObject.isHashCodeSet || x509CRLObject.hashCodeValue == this.hashCodeValue) && this.c.equals(x509CRLObject.c);
        }
        return super.equals(other);
    }
    
    @Override
    public Set getCriticalExtensionOIDs() {
        return this.getExtensionOIDs(true);
    }
    
    @Override
    public byte[] getEncoded() throws CRLException {
        try {
            return this.c.getEncoded("DER");
        }
        catch (IOException ex) {
            throw new CRLException(ex.toString());
        }
    }
    
    @Override
    public byte[] getExtensionValue(final String s) {
        final Extensions extensions = this.c.getTBSCertList().getExtensions();
        if (extensions != null) {
            final Extension extension = extensions.getExtension(new ASN1ObjectIdentifier(s));
            if (extension != null) {
                try {
                    return extension.getExtnValue().getEncoded();
                }
                catch (Exception ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("error parsing ");
                    sb.append(ex.toString());
                    throw new IllegalStateException(sb.toString());
                }
            }
        }
        return null;
    }
    
    @Override
    public Principal getIssuerDN() {
        return new X509Principal(X500Name.getInstance(this.c.getIssuer().toASN1Primitive()));
    }
    
    @Override
    public X500Principal getIssuerX500Principal() {
        try {
            return new X500Principal(this.c.getIssuer().getEncoded());
        }
        catch (IOException ex) {
            throw new IllegalStateException("can't encode issuer DN");
        }
    }
    
    @Override
    public Date getNextUpdate() {
        if (this.c.getNextUpdate() != null) {
            return this.c.getNextUpdate().getDate();
        }
        return null;
    }
    
    @Override
    public Set getNonCriticalExtensionOIDs() {
        return this.getExtensionOIDs(false);
    }
    
    @Override
    public X509CRLEntry getRevokedCertificate(final BigInteger bigInteger) {
        final Enumeration revokedCertificateEnumeration = this.c.getRevokedCertificateEnumeration();
        X500Name instance = null;
        while (revokedCertificateEnumeration.hasMoreElements()) {
            final TBSCertList.CRLEntry crlEntry = revokedCertificateEnumeration.nextElement();
            if (crlEntry.getUserCertificate().hasValue(bigInteger)) {
                return new X509CRLEntryObject(crlEntry, this.isIndirect, instance);
            }
            if (!this.isIndirect || !crlEntry.hasExtensions()) {
                continue;
            }
            final Extension extension = crlEntry.getExtensions().getExtension(Extension.certificateIssuer);
            if (extension == null) {
                continue;
            }
            instance = X500Name.getInstance(GeneralNames.getInstance(extension.getParsedValue()).getNames()[0].getName());
        }
        return null;
    }
    
    @Override
    public Set getRevokedCertificates() {
        final Set loadCRLEntries = this.loadCRLEntries();
        if (!loadCRLEntries.isEmpty()) {
            return Collections.unmodifiableSet((Set<?>)loadCRLEntries);
        }
        return null;
    }
    
    @Override
    public String getSigAlgName() {
        return this.sigAlgName;
    }
    
    @Override
    public String getSigAlgOID() {
        return this.c.getSignatureAlgorithm().getAlgorithm().getId();
    }
    
    @Override
    public byte[] getSigAlgParams() {
        final byte[] sigAlgParams = this.sigAlgParams;
        if (sigAlgParams != null) {
            final byte[] array = new byte[sigAlgParams.length];
            System.arraycopy(sigAlgParams, 0, array, 0, array.length);
            return array;
        }
        return null;
    }
    
    @Override
    public byte[] getSignature() {
        return this.c.getSignature().getOctets();
    }
    
    @Override
    public byte[] getTBSCertList() throws CRLException {
        try {
            return this.c.getTBSCertList().getEncoded("DER");
        }
        catch (IOException ex) {
            throw new CRLException(ex.toString());
        }
    }
    
    @Override
    public Date getThisUpdate() {
        return this.c.getThisUpdate().getDate();
    }
    
    @Override
    public int getVersion() {
        return this.c.getVersionNumber();
    }
    
    @Override
    public boolean hasUnsupportedCriticalExtension() {
        final Set criticalExtensionOIDs = this.getCriticalExtensionOIDs();
        if (criticalExtensionOIDs == null) {
            return false;
        }
        criticalExtensionOIDs.remove(RFC3280CertPathUtilities.ISSUING_DISTRIBUTION_POINT);
        criticalExtensionOIDs.remove(RFC3280CertPathUtilities.DELTA_CRL_INDICATOR);
        return criticalExtensionOIDs.isEmpty() ^ true;
    }
    
    @Override
    public int hashCode() {
        if (!this.isHashCodeSet) {
            this.isHashCodeSet = true;
            this.hashCodeValue = super.hashCode();
        }
        return this.hashCodeValue;
    }
    
    @Override
    public boolean isRevoked(final Certificate certificate) {
        Label_0202: {
            if (!certificate.getType().equals("X.509")) {
                break Label_0202;
            }
            final Enumeration revokedCertificateEnumeration = this.c.getRevokedCertificateEnumeration();
            X500Name issuer = this.c.getIssuer();
            Label_0166: {
                if (revokedCertificateEnumeration != null) {
                    final X509Certificate x509Certificate = (X509Certificate)certificate;
                    final BigInteger serialNumber = x509Certificate.getSerialNumber();
                    while (revokedCertificateEnumeration.hasMoreElements()) {
                        final TBSCertList.CRLEntry instance = TBSCertList.CRLEntry.getInstance(revokedCertificateEnumeration.nextElement());
                        X500Name instance2 = issuer;
                        if (this.isIndirect) {
                            instance2 = issuer;
                            if (instance.hasExtensions()) {
                                final Extension extension = instance.getExtensions().getExtension(Extension.certificateIssuer);
                                instance2 = issuer;
                                if (extension != null) {
                                    instance2 = X500Name.getInstance(GeneralNames.getInstance(extension.getParsedValue()).getNames()[0].getName());
                                }
                            }
                        }
                        issuer = instance2;
                        if (instance.getUserCertificate().hasValue(serialNumber)) {
                            if (certificate instanceof X509Certificate) {
                                final X500Name x500Name = X500Name.getInstance(x509Certificate.getIssuerX500Principal().getEncoded());
                                break Label_0166;
                            }
                            break Label_0166;
                        }
                    }
                    return false;
                }
                return false;
                while (true) {
                    while (true) {
                        try {
                            final X500Name x500Name = org.bouncycastle.asn1.x509.Certificate.getInstance(certificate.getEncoded()).getIssuer();
                            final X500Name instance2;
                            return instance2.equals(x500Name);
                            throw new RuntimeException("Cannot process certificate");
                            throw new RuntimeException("X.509 CRL used with non X.509 Cert");
                        }
                        catch (CertificateEncodingException ex) {}
                        continue;
                    }
                }
            }
        }
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        final String lineSeparator = Strings.lineSeparator();
        sb.append("              Version: ");
        sb.append(this.getVersion());
        sb.append(lineSeparator);
        sb.append("             IssuerDN: ");
        sb.append(this.getIssuerDN());
        sb.append(lineSeparator);
        sb.append("          This update: ");
        sb.append(this.getThisUpdate());
        sb.append(lineSeparator);
        sb.append("          Next update: ");
        sb.append(this.getNextUpdate());
        sb.append(lineSeparator);
        sb.append("  Signature Algorithm: ");
        sb.append(this.getSigAlgName());
        sb.append(lineSeparator);
        final byte[] signature = this.getSignature();
        sb.append("            Signature: ");
        sb.append(new String(Hex.encode(signature, 0, 20)));
        sb.append(lineSeparator);
        for (int i = 20; i < signature.length; i += 20) {
            final int length = signature.length;
            sb.append("                       ");
            String str;
            if (i < length - 20) {
                str = new String(Hex.encode(signature, i, 20));
            }
            else {
                str = new String(Hex.encode(signature, i, signature.length - i));
            }
            sb.append(str);
            sb.append(lineSeparator);
        }
        final Extensions extensions = this.c.getTBSCertList().getExtensions();
        while (true) {
            Label_0661: {
                if (extensions == null) {
                    break Label_0661;
                }
                final Enumeration oids = extensions.oids();
            Label_0323_Outer:
                while (true) {
                    if (!oids.hasMoreElements()) {
                        break Label_0338;
                    }
                    String str2 = "           Extensions: ";
                Label_0330_Outer:
                    while (true) {
                        sb.append(str2);
                        ASN1ObjectIdentifier asn1ObjectIdentifier;
                        Extension extension;
                        while (true) {
                            sb.append(lineSeparator);
                            if (!oids.hasMoreElements()) {
                                break Label_0661;
                            }
                            asn1ObjectIdentifier = oids.nextElement();
                            extension = extensions.getExtension(asn1ObjectIdentifier);
                            if (extension.getExtnValue() == null) {
                                continue;
                            }
                            break;
                        }
                        final ASN1InputStream asn1InputStream = new ASN1InputStream(extension.getExtnValue().getOctets());
                        sb.append("                       critical(");
                        sb.append(extension.isCritical());
                        sb.append(") ");
                        try {
                            Label_0458: {
                                ASN1Object obj = null;
                                Label_0451: {
                                    if (!asn1ObjectIdentifier.equals(Extension.cRLNumber)) {
                                        String str3;
                                        if (asn1ObjectIdentifier.equals(Extension.deltaCRLIndicator)) {
                                            final StringBuilder sb2 = new StringBuilder();
                                            sb2.append("Base CRL: ");
                                            sb2.append(new CRLNumber(ASN1Integer.getInstance(asn1InputStream.readObject()).getPositiveValue()));
                                            str3 = sb2.toString();
                                        }
                                        else {
                                            if (asn1ObjectIdentifier.equals(Extension.issuingDistributionPoint)) {
                                                obj = IssuingDistributionPoint.getInstance(asn1InputStream.readObject());
                                                break Label_0451;
                                            }
                                            if (asn1ObjectIdentifier.equals(Extension.cRLDistributionPoints)) {
                                                obj = CRLDistPoint.getInstance(asn1InputStream.readObject());
                                                break Label_0451;
                                            }
                                            if (asn1ObjectIdentifier.equals(Extension.freshestCRL)) {
                                                obj = CRLDistPoint.getInstance(asn1InputStream.readObject());
                                                break Label_0451;
                                            }
                                            sb.append(asn1ObjectIdentifier.getId());
                                            sb.append(" value = ");
                                            str3 = ASN1Dump.dumpAsString(asn1InputStream.readObject());
                                        }
                                        sb.append(str3);
                                        break Label_0458;
                                    }
                                    obj = new CRLNumber(ASN1Integer.getInstance(asn1InputStream.readObject()).getPositiveValue());
                                }
                                sb.append(obj);
                            }
                            sb.append(lineSeparator);
                            continue Label_0323_Outer;
                            final Set revokedCertificates = this.getRevokedCertificates();
                            // iftrue(Label_0709:, revokedCertificates == null)
                            while (true) {
                                Label_0677: {
                                    Block_14: {
                                        break Block_14;
                                        Label_0709: {
                                            return sb.toString();
                                        }
                                        sb.append(asn1ObjectIdentifier.getId());
                                        sb.append(" value = ");
                                        str2 = "*****";
                                        continue Label_0330_Outer;
                                        final Iterator<Object> iterator;
                                        sb.append(iterator.next());
                                        sb.append(lineSeparator);
                                        break Label_0677;
                                    }
                                    final Iterator<Object> iterator = revokedCertificates.iterator();
                                }
                                continue;
                            }
                        }
                        // iftrue(Label_0709:, !iterator.hasNext())
                        catch (Exception ex) {}
                        break;
                    }
                    break;
                }
            }
            continue;
        }
    }
    
    @Override
    public void verify(final PublicKey publicKey) throws CRLException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException, SignatureException {
    Label_0014_Outer:
        while (true) {
            while (true) {
                try {
                    Signature signature = Signature.getInstance(this.getSigAlgName(), "BC");
                    while (true) {
                        this.doVerify(publicKey, signature);
                        return;
                        signature = Signature.getInstance(this.getSigAlgName());
                        continue Label_0014_Outer;
                    }
                }
                catch (Exception ex) {}
                continue;
            }
        }
    }
    
    @Override
    public void verify(final PublicKey publicKey, final String provider) throws CRLException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException, SignatureException {
        Signature signature;
        if (provider != null) {
            signature = Signature.getInstance(this.getSigAlgName(), provider);
        }
        else {
            signature = Signature.getInstance(this.getSigAlgName());
        }
        this.doVerify(publicKey, signature);
    }
    
    @Override
    public void verify(final PublicKey publicKey, final Provider provider) throws CRLException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        Signature signature;
        if (provider != null) {
            signature = Signature.getInstance(this.getSigAlgName(), provider);
        }
        else {
            signature = Signature.getInstance(this.getSigAlgName());
        }
        this.doVerify(publicKey, signature);
    }
}
