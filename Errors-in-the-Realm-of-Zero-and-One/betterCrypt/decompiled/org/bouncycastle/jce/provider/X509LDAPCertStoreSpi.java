// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

import java.security.cert.CertSelector;
import java.util.Iterator;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.security.cert.CertificateFactory;
import java.security.cert.CRL;
import java.security.cert.X509CRLSelector;
import java.security.cert.CRLSelector;
import javax.naming.NamingException;
import java.util.Hashtable;
import javax.naming.directory.InitialDirContext;
import java.util.Properties;
import javax.naming.directory.DirContext;
import java.io.IOException;
import java.security.cert.CertStoreException;
import java.util.Collection;
import javax.security.auth.x500.X500Principal;
import java.util.HashSet;
import java.util.Set;
import java.security.cert.X509CertSelector;
import java.security.InvalidAlgorithmParameterException;
import java.security.cert.CertStoreParameters;
import org.bouncycastle.jce.X509LDAPCertStoreParameters;
import java.security.cert.CertStoreSpi;

public class X509LDAPCertStoreSpi extends CertStoreSpi
{
    private static String LDAP_PROVIDER = "com.sun.jndi.ldap.LdapCtxFactory";
    private static String REFERRALS_IGNORE = "ignore";
    private static final String SEARCH_SECURITY_LEVEL = "none";
    private static final String URL_CONTEXT_PREFIX = "com.sun.jndi.url";
    private X509LDAPCertStoreParameters params;
    
    public X509LDAPCertStoreSpi(final CertStoreParameters params) throws InvalidAlgorithmParameterException {
        super(params);
        if (params instanceof X509LDAPCertStoreParameters) {
            this.params = (X509LDAPCertStoreParameters)params;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(X509LDAPCertStoreSpi.class.getName());
        sb.append(": parameter must be a ");
        sb.append(X509LDAPCertStoreParameters.class.getName());
        sb.append(" object\n");
        sb.append(params.toString());
        throw new InvalidAlgorithmParameterException(sb.toString());
    }
    
    private Set certSubjectSerialSearch(final X509CertSelector x509CertSelector, final String[] array, final String s, final String s2) throws CertStoreException {
        final HashSet set = new HashSet();
        try {
            Set set2;
            if (x509CertSelector.getSubjectAsBytes() == null && x509CertSelector.getSubjectAsString() == null && x509CertSelector.getCertificate() == null) {
                set2 = this.search(s, "*", array);
            }
            else {
                String str = null;
                String s3;
                if (x509CertSelector.getCertificate() != null) {
                    final String name = x509CertSelector.getCertificate().getSubjectX500Principal().getName("RFC1779");
                    final String string = x509CertSelector.getCertificate().getSerialNumber().toString();
                    s3 = name;
                    str = string;
                }
                else if (x509CertSelector.getSubjectAsBytes() != null) {
                    s3 = new X500Principal(x509CertSelector.getSubjectAsBytes()).getName("RFC1779");
                }
                else {
                    s3 = x509CertSelector.getSubjectAsString();
                }
                final String dn = this.parseDN(s3, s2);
                final StringBuilder sb = new StringBuilder();
                sb.append("*");
                sb.append(dn);
                sb.append("*");
                set.addAll(this.search(s, sb.toString(), array));
                if (str == null || this.params.getSearchForSerialNumberIn() == null) {
                    return set;
                }
                final String searchForSerialNumberIn = this.params.getSearchForSerialNumberIn();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("*");
                sb2.append(str);
                sb2.append("*");
                set2 = this.search(searchForSerialNumberIn, sb2.toString(), array);
            }
            set.addAll(set2);
            return set;
        }
        catch (IOException obj) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("exception processing selector: ");
            sb3.append(obj);
            throw new CertStoreException(sb3.toString());
        }
    }
    
    private DirContext connectLDAP() throws NamingException {
        final Properties environment = new Properties();
        environment.setProperty("java.naming.factory.initial", X509LDAPCertStoreSpi.LDAP_PROVIDER);
        environment.setProperty("java.naming.batchsize", "0");
        environment.setProperty("java.naming.provider.url", this.params.getLdapURL());
        environment.setProperty("java.naming.factory.url.pkgs", "com.sun.jndi.url");
        environment.setProperty("java.naming.referral", X509LDAPCertStoreSpi.REFERRALS_IGNORE);
        environment.setProperty("java.naming.security.authentication", "none");
        return new InitialDirContext(environment);
    }
    
    private Set getCACertificates(final X509CertSelector x509CertSelector) throws CertStoreException {
        final String[] array = { this.params.getCACertificateAttribute() };
        final Set certSubjectSerialSearch = this.certSubjectSerialSearch(x509CertSelector, array, this.params.getLdapCACertificateAttributeName(), this.params.getCACertificateSubjectAttributeName());
        if (certSubjectSerialSearch.isEmpty()) {
            certSubjectSerialSearch.addAll(this.search(null, "*", array));
        }
        return certSubjectSerialSearch;
    }
    
    private Set getCrossCertificates(final X509CertSelector x509CertSelector) throws CertStoreException {
        final String[] array = { this.params.getCrossCertificateAttribute() };
        final Set certSubjectSerialSearch = this.certSubjectSerialSearch(x509CertSelector, array, this.params.getLdapCrossCertificateAttributeName(), this.params.getCrossCertificateSubjectAttributeName());
        if (certSubjectSerialSearch.isEmpty()) {
            certSubjectSerialSearch.addAll(this.search(null, "*", array));
        }
        return certSubjectSerialSearch;
    }
    
    private Set getEndCertificates(final X509CertSelector x509CertSelector) throws CertStoreException {
        return this.certSubjectSerialSearch(x509CertSelector, new String[] { this.params.getUserCertificateAttribute() }, this.params.getLdapUserCertificateAttributeName(), this.params.getUserCertificateSubjectAttributeName());
    }
    
    private String parseDN(String s, String substring) {
        s = s.substring(s.toLowerCase().indexOf(substring.toLowerCase()) + substring.length());
        while (true) {
            Label_0038: {
                int endIndex;
                if ((endIndex = s.indexOf(44)) == -1) {
                    break Label_0038;
                }
                while (s.charAt(endIndex - 1) == '\\') {
                    if ((endIndex = s.indexOf(44, endIndex + 1)) == -1) {
                        break Label_0038;
                    }
                }
                s = s.substring(0, endIndex);
                substring = (s = s.substring(s.indexOf(61) + 1));
                if (substring.charAt(0) == ' ') {
                    s = substring.substring(1);
                }
                substring = s;
                if (s.startsWith("\"")) {
                    substring = s.substring(1);
                }
                s = substring;
                if (substring.endsWith("\"")) {
                    s = substring.substring(0, substring.length() - 1);
                }
                return s;
            }
            int endIndex = s.length();
            continue;
        }
    }
    
    private Set search(final String p0, final String p1, final String[] p2) throws CertStoreException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   java/lang/StringBuilder.<init>:()V
        //     7: astore          5
        //     9: aload           5
        //    11: aload_1        
        //    12: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    15: pop            
        //    16: aload           5
        //    18: ldc             "="
        //    20: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    23: pop            
        //    24: aload           5
        //    26: aload_2        
        //    27: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    30: pop            
        //    31: aload           5
        //    33: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    36: astore          5
        //    38: aconst_null    
        //    39: astore_2       
        //    40: aconst_null    
        //    41: astore          6
        //    43: aload_1        
        //    44: ifnonnull       50
        //    47: aconst_null    
        //    48: astore          5
        //    50: new             Ljava/util/HashSet;
        //    53: dup            
        //    54: invokespecial   java/util/HashSet.<init>:()V
        //    57: astore          8
        //    59: aload           6
        //    61: astore_1       
        //    62: aload_0        
        //    63: invokespecial   org/bouncycastle/jce/provider/X509LDAPCertStoreSpi.connectLDAP:()Ljavax/naming/directory/DirContext;
        //    66: astore          6
        //    68: aload           6
        //    70: astore_1       
        //    71: aload           6
        //    73: astore_2       
        //    74: new             Ljavax/naming/directory/SearchControls;
        //    77: dup            
        //    78: invokespecial   javax/naming/directory/SearchControls.<init>:()V
        //    81: astore          9
        //    83: aload           6
        //    85: astore_1       
        //    86: aload           6
        //    88: astore_2       
        //    89: aload           9
        //    91: iconst_2       
        //    92: invokevirtual   javax/naming/directory/SearchControls.setSearchScope:(I)V
        //    95: aload           6
        //    97: astore_1       
        //    98: aload           6
        //   100: astore_2       
        //   101: aload           9
        //   103: lconst_0       
        //   104: invokevirtual   javax/naming/directory/SearchControls.setCountLimit:(J)V
        //   107: iconst_0       
        //   108: istore          4
        //   110: aload           6
        //   112: astore_1       
        //   113: aload           6
        //   115: astore_2       
        //   116: iload           4
        //   118: aload_3        
        //   119: arraylength    
        //   120: if_icmpge       468
        //   123: aload           6
        //   125: astore_1       
        //   126: aload           6
        //   128: astore_2       
        //   129: iconst_1       
        //   130: anewarray       Ljava/lang/String;
        //   133: astore          10
        //   135: aload           10
        //   137: iconst_0       
        //   138: aload_3        
        //   139: iload           4
        //   141: aaload         
        //   142: aastore        
        //   143: aload           6
        //   145: astore_1       
        //   146: aload           6
        //   148: astore_2       
        //   149: aload           9
        //   151: aload           10
        //   153: invokevirtual   javax/naming/directory/SearchControls.setReturningAttributes:([Ljava/lang/String;)V
        //   156: aload           6
        //   158: astore_1       
        //   159: aload           6
        //   161: astore_2       
        //   162: new             Ljava/lang/StringBuilder;
        //   165: dup            
        //   166: invokespecial   java/lang/StringBuilder.<init>:()V
        //   169: astore          7
        //   171: aload           6
        //   173: astore_1       
        //   174: aload           6
        //   176: astore_2       
        //   177: aload           7
        //   179: ldc_w           "(&("
        //   182: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   185: pop            
        //   186: aload           6
        //   188: astore_1       
        //   189: aload           6
        //   191: astore_2       
        //   192: aload           7
        //   194: aload           5
        //   196: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   199: pop            
        //   200: aload           6
        //   202: astore_1       
        //   203: aload           6
        //   205: astore_2       
        //   206: aload           7
        //   208: ldc_w           ")("
        //   211: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   214: pop            
        //   215: aload           6
        //   217: astore_1       
        //   218: aload           6
        //   220: astore_2       
        //   221: aload           7
        //   223: aload           10
        //   225: iconst_0       
        //   226: aaload         
        //   227: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   230: pop            
        //   231: aload           6
        //   233: astore_1       
        //   234: aload           6
        //   236: astore_2       
        //   237: aload           7
        //   239: ldc_w           "=*))"
        //   242: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   245: pop            
        //   246: aload           6
        //   248: astore_1       
        //   249: aload           6
        //   251: astore_2       
        //   252: aload           7
        //   254: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   257: astore          7
        //   259: aload           5
        //   261: ifnonnull       338
        //   264: aload           6
        //   266: astore_1       
        //   267: aload           6
        //   269: astore_2       
        //   270: new             Ljava/lang/StringBuilder;
        //   273: dup            
        //   274: invokespecial   java/lang/StringBuilder.<init>:()V
        //   277: astore          7
        //   279: aload           6
        //   281: astore_1       
        //   282: aload           6
        //   284: astore_2       
        //   285: aload           7
        //   287: ldc_w           "("
        //   290: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   293: pop            
        //   294: aload           6
        //   296: astore_1       
        //   297: aload           6
        //   299: astore_2       
        //   300: aload           7
        //   302: aload           10
        //   304: iconst_0       
        //   305: aaload         
        //   306: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   309: pop            
        //   310: aload           6
        //   312: astore_1       
        //   313: aload           6
        //   315: astore_2       
        //   316: aload           7
        //   318: ldc_w           "=*)"
        //   321: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   324: pop            
        //   325: aload           6
        //   327: astore_1       
        //   328: aload           6
        //   330: astore_2       
        //   331: aload           7
        //   333: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   336: astore          7
        //   338: aload           6
        //   340: astore_1       
        //   341: aload           6
        //   343: astore_2       
        //   344: aload           6
        //   346: aload_0        
        //   347: getfield        org/bouncycastle/jce/provider/X509LDAPCertStoreSpi.params:Lorg/bouncycastle/jce/X509LDAPCertStoreParameters;
        //   350: invokevirtual   org/bouncycastle/jce/X509LDAPCertStoreParameters.getBaseDN:()Ljava/lang/String;
        //   353: aload           7
        //   355: aload           9
        //   357: invokeinterface javax/naming/directory/DirContext.search:(Ljava/lang/String;Ljava/lang/String;Ljavax/naming/directory/SearchControls;)Ljavax/naming/NamingEnumeration;
        //   362: astore          7
        //   364: aload           6
        //   366: astore_1       
        //   367: aload           6
        //   369: astore_2       
        //   370: aload           7
        //   372: invokeinterface javax/naming/NamingEnumeration.hasMoreElements:()Z
        //   377: ifeq            459
        //   380: aload           6
        //   382: astore_1       
        //   383: aload           6
        //   385: astore_2       
        //   386: aload           7
        //   388: invokeinterface javax/naming/NamingEnumeration.next:()Ljava/lang/Object;
        //   393: checkcast       Ljavax/naming/directory/SearchResult;
        //   396: invokevirtual   javax/naming/directory/SearchResult.getAttributes:()Ljavax/naming/directory/Attributes;
        //   399: invokeinterface javax/naming/directory/Attributes.getAll:()Ljavax/naming/NamingEnumeration;
        //   404: invokeinterface javax/naming/NamingEnumeration.next:()Ljava/lang/Object;
        //   409: checkcast       Ljavax/naming/directory/Attribute;
        //   412: invokeinterface javax/naming/directory/Attribute.getAll:()Ljavax/naming/NamingEnumeration;
        //   417: astore          10
        //   419: aload           6
        //   421: astore_1       
        //   422: aload           6
        //   424: astore_2       
        //   425: aload           10
        //   427: invokeinterface javax/naming/NamingEnumeration.hasMore:()Z
        //   432: ifeq            364
        //   435: aload           6
        //   437: astore_1       
        //   438: aload           6
        //   440: astore_2       
        //   441: aload           8
        //   443: aload           10
        //   445: invokeinterface javax/naming/NamingEnumeration.next:()Ljava/lang/Object;
        //   450: invokeinterface java/util/Set.add:(Ljava/lang/Object;)Z
        //   455: pop            
        //   456: goto            419
        //   459: iload           4
        //   461: iconst_1       
        //   462: iadd           
        //   463: istore          4
        //   465: goto            110
        //   468: aload           6
        //   470: ifnull          480
        //   473: aload           6
        //   475: invokeinterface javax/naming/directory/DirContext.close:()V
        //   480: aload           8
        //   482: areturn        
        //   483: astore_2       
        //   484: goto            534
        //   487: astore_3       
        //   488: aload_2        
        //   489: astore_1       
        //   490: new             Ljava/lang/StringBuilder;
        //   493: dup            
        //   494: invokespecial   java/lang/StringBuilder.<init>:()V
        //   497: astore          5
        //   499: aload_2        
        //   500: astore_1       
        //   501: aload           5
        //   503: ldc_w           "Error getting results from LDAP directory "
        //   506: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   509: pop            
        //   510: aload_2        
        //   511: astore_1       
        //   512: aload           5
        //   514: aload_3        
        //   515: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   518: pop            
        //   519: aload_2        
        //   520: astore_1       
        //   521: new             Ljava/security/cert/CertStoreException;
        //   524: dup            
        //   525: aload           5
        //   527: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   530: invokespecial   java/security/cert/CertStoreException.<init>:(Ljava/lang/String;)V
        //   533: athrow         
        //   534: aload_1        
        //   535: ifnull          544
        //   538: aload_1        
        //   539: invokeinterface javax/naming/directory/DirContext.close:()V
        //   544: aload_2        
        //   545: athrow         
        //   546: astore_1       
        //   547: aload           8
        //   549: areturn        
        //   550: astore_1       
        //   551: goto            544
        //    Exceptions:
        //  throws java.security.cert.CertStoreException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  62     68     487    534    Ljava/lang/Exception;
        //  62     68     483    546    Any
        //  74     83     487    534    Ljava/lang/Exception;
        //  74     83     483    546    Any
        //  89     95     487    534    Ljava/lang/Exception;
        //  89     95     483    546    Any
        //  101    107    487    534    Ljava/lang/Exception;
        //  101    107    483    546    Any
        //  116    123    487    534    Ljava/lang/Exception;
        //  116    123    483    546    Any
        //  129    135    487    534    Ljava/lang/Exception;
        //  129    135    483    546    Any
        //  149    156    487    534    Ljava/lang/Exception;
        //  149    156    483    546    Any
        //  162    171    487    534    Ljava/lang/Exception;
        //  162    171    483    546    Any
        //  177    186    487    534    Ljava/lang/Exception;
        //  177    186    483    546    Any
        //  192    200    487    534    Ljava/lang/Exception;
        //  192    200    483    546    Any
        //  206    215    487    534    Ljava/lang/Exception;
        //  206    215    483    546    Any
        //  221    231    487    534    Ljava/lang/Exception;
        //  221    231    483    546    Any
        //  237    246    487    534    Ljava/lang/Exception;
        //  237    246    483    546    Any
        //  252    259    487    534    Ljava/lang/Exception;
        //  252    259    483    546    Any
        //  270    279    487    534    Ljava/lang/Exception;
        //  270    279    483    546    Any
        //  285    294    487    534    Ljava/lang/Exception;
        //  285    294    483    546    Any
        //  300    310    487    534    Ljava/lang/Exception;
        //  300    310    483    546    Any
        //  316    325    487    534    Ljava/lang/Exception;
        //  316    325    483    546    Any
        //  331    338    487    534    Ljava/lang/Exception;
        //  331    338    483    546    Any
        //  344    364    487    534    Ljava/lang/Exception;
        //  344    364    483    546    Any
        //  370    380    487    534    Ljava/lang/Exception;
        //  370    380    483    546    Any
        //  386    419    487    534    Ljava/lang/Exception;
        //  386    419    483    546    Any
        //  425    435    487    534    Ljava/lang/Exception;
        //  425    435    483    546    Any
        //  441    456    487    534    Ljava/lang/Exception;
        //  441    456    483    546    Any
        //  473    480    546    550    Ljava/lang/Exception;
        //  490    499    483    546    Any
        //  501    510    483    546    Any
        //  512    519    483    546    Any
        //  521    534    483    546    Any
        //  538    544    550    554    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 288 out of bounds for length 288
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:373)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:426)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public Collection engineGetCRLs(final CRLSelector crlSelector) throws CertStoreException {
        final String[] array = { this.params.getCertificateRevocationListAttribute() };
        if (crlSelector instanceof X509CRLSelector) {
            final X509CRLSelector x509CRLSelector = (X509CRLSelector)crlSelector;
            final HashSet<CRL> set = new HashSet<CRL>();
            final String ldapCertificateRevocationListAttributeName = this.params.getLdapCertificateRevocationListAttributeName();
            final HashSet<byte[]> set2 = new HashSet<byte[]>();
            if (x509CRLSelector.getIssuerNames() != null) {
                for (final byte[] next : x509CRLSelector.getIssuerNames()) {
                    String s;
                    String name;
                    if (next instanceof String) {
                        s = this.params.getCertificateRevocationListIssuerAttributeName();
                        name = (String)(Object)next;
                    }
                    else {
                        s = this.params.getCertificateRevocationListIssuerAttributeName();
                        name = new X500Principal(next).getName("RFC1779");
                    }
                    final String dn = this.parseDN(name, s);
                    final StringBuilder sb = new StringBuilder();
                    sb.append("*");
                    sb.append(dn);
                    sb.append("*");
                    set2.addAll((Collection<?>)this.search(ldapCertificateRevocationListAttributeName, sb.toString(), array));
                }
            }
            else {
                set2.addAll((Collection<?>)this.search(ldapCertificateRevocationListAttributeName, "*", array));
            }
            set2.addAll((Collection<?>)this.search(null, "*", array));
            final Iterator<Object> iterator2 = set2.iterator();
            try {
                final CertificateFactory instance = CertificateFactory.getInstance("X.509", "BC");
                while (iterator2.hasNext()) {
                    final CRL generateCRL = instance.generateCRL(new ByteArrayInputStream(iterator2.next()));
                    if (x509CRLSelector.match(generateCRL)) {
                        set.add(generateCRL);
                    }
                }
                return set;
            }
            catch (Exception obj) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("CRL cannot be constructed from LDAP result ");
                sb2.append(obj);
                throw new CertStoreException(sb2.toString());
            }
        }
        throw new CertStoreException("selector is not a X509CRLSelector");
    }
    
    @Override
    public Collection engineGetCertificates(final CertSelector p0) throws CertStoreException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: instanceof      Ljava/security/cert/X509CertSelector;
        //     4: ifeq            304
        //     7: aload_1        
        //     8: checkcast       Ljava/security/cert/X509CertSelector;
        //    11: astore_1       
        //    12: new             Ljava/util/HashSet;
        //    15: dup            
        //    16: invokespecial   java/util/HashSet.<init>:()V
        //    19: astore_2       
        //    20: aload_0        
        //    21: aload_1        
        //    22: invokespecial   org/bouncycastle/jce/provider/X509LDAPCertStoreSpi.getEndCertificates:(Ljava/security/cert/X509CertSelector;)Ljava/util/Set;
        //    25: astore_3       
        //    26: aload_3        
        //    27: aload_0        
        //    28: aload_1        
        //    29: invokespecial   org/bouncycastle/jce/provider/X509LDAPCertStoreSpi.getCACertificates:(Ljava/security/cert/X509CertSelector;)Ljava/util/Set;
        //    32: invokeinterface java/util/Set.addAll:(Ljava/util/Collection;)Z
        //    37: pop            
        //    38: aload_3        
        //    39: aload_0        
        //    40: aload_1        
        //    41: invokespecial   org/bouncycastle/jce/provider/X509LDAPCertStoreSpi.getCrossCertificates:(Ljava/security/cert/X509CertSelector;)Ljava/util/Set;
        //    44: invokeinterface java/util/Set.addAll:(Ljava/util/Collection;)Z
        //    49: pop            
        //    50: aload_3        
        //    51: invokeinterface java/util/Set.iterator:()Ljava/util/Iterator;
        //    56: astore_3       
        //    57: ldc_w           "X.509"
        //    60: ldc_w           "BC"
        //    63: invokestatic    java/security/cert/CertificateFactory.getInstance:(Ljava/lang/String;Ljava/lang/String;)Ljava/security/cert/CertificateFactory;
        //    66: astore          4
        //    68: aload_3        
        //    69: invokeinterface java/util/Iterator.hasNext:()Z
        //    74: ifeq            267
        //    77: aload_3        
        //    78: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //    83: checkcast       [B
        //    86: checkcast       [B
        //    89: astore          6
        //    91: aload           6
        //    93: ifnull          68
        //    96: aload           6
        //    98: arraylength    
        //    99: ifne            105
        //   102: goto            68
        //   105: new             Ljava/util/ArrayList;
        //   108: dup            
        //   109: invokespecial   java/util/ArrayList.<init>:()V
        //   112: astore          5
        //   114: aload           5
        //   116: aload           6
        //   118: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   123: pop            
        //   124: new             Lorg/bouncycastle/asn1/ASN1InputStream;
        //   127: dup            
        //   128: aload           6
        //   130: invokespecial   org/bouncycastle/asn1/ASN1InputStream.<init>:([B)V
        //   133: invokevirtual   org/bouncycastle/asn1/ASN1InputStream.readObject:()Lorg/bouncycastle/asn1/ASN1Primitive;
        //   136: invokestatic    org/bouncycastle/asn1/x509/CertificatePair.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/CertificatePair;
        //   139: astore          6
        //   141: aload           5
        //   143: invokeinterface java/util/List.clear:()V
        //   148: aload           6
        //   150: invokevirtual   org/bouncycastle/asn1/x509/CertificatePair.getForward:()Lorg/bouncycastle/asn1/x509/Certificate;
        //   153: ifnull          172
        //   156: aload           5
        //   158: aload           6
        //   160: invokevirtual   org/bouncycastle/asn1/x509/CertificatePair.getForward:()Lorg/bouncycastle/asn1/x509/Certificate;
        //   163: invokevirtual   org/bouncycastle/asn1/x509/Certificate.getEncoded:()[B
        //   166: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   171: pop            
        //   172: aload           6
        //   174: invokevirtual   org/bouncycastle/asn1/x509/CertificatePair.getReverse:()Lorg/bouncycastle/asn1/x509/Certificate;
        //   177: ifnull          196
        //   180: aload           5
        //   182: aload           6
        //   184: invokevirtual   org/bouncycastle/asn1/x509/CertificatePair.getReverse:()Lorg/bouncycastle/asn1/x509/Certificate;
        //   187: invokevirtual   org/bouncycastle/asn1/x509/Certificate.getEncoded:()[B
        //   190: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   195: pop            
        //   196: aload           5
        //   198: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //   203: astore          5
        //   205: aload           5
        //   207: invokeinterface java/util/Iterator.hasNext:()Z
        //   212: ifeq            68
        //   215: new             Ljava/io/ByteArrayInputStream;
        //   218: dup            
        //   219: aload           5
        //   221: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   226: checkcast       [B
        //   229: checkcast       [B
        //   232: invokespecial   java/io/ByteArrayInputStream.<init>:([B)V
        //   235: astore          6
        //   237: aload           4
        //   239: aload           6
        //   241: invokevirtual   java/security/cert/CertificateFactory.generateCertificate:(Ljava/io/InputStream;)Ljava/security/cert/Certificate;
        //   244: astore          6
        //   246: aload_1        
        //   247: aload           6
        //   249: invokevirtual   java/security/cert/X509CertSelector.match:(Ljava/security/cert/Certificate;)Z
        //   252: ifeq            205
        //   255: aload_2        
        //   256: aload           6
        //   258: invokeinterface java/util/Set.add:(Ljava/lang/Object;)Z
        //   263: pop            
        //   264: goto            205
        //   267: aload_2        
        //   268: areturn        
        //   269: astore_1       
        //   270: new             Ljava/lang/StringBuilder;
        //   273: dup            
        //   274: invokespecial   java/lang/StringBuilder.<init>:()V
        //   277: astore_2       
        //   278: aload_2        
        //   279: ldc_w           "certificate cannot be constructed from LDAP result: "
        //   282: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   285: pop            
        //   286: aload_2        
        //   287: aload_1        
        //   288: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   291: pop            
        //   292: new             Ljava/security/cert/CertStoreException;
        //   295: dup            
        //   296: aload_2        
        //   297: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   300: invokespecial   java/security/cert/CertStoreException.<init>:(Ljava/lang/String;)V
        //   303: athrow         
        //   304: new             Ljava/security/cert/CertStoreException;
        //   307: dup            
        //   308: ldc_w           "selector is not a X509CertSelector"
        //   311: invokespecial   java/security/cert/CertStoreException.<init>:(Ljava/lang/String;)V
        //   314: athrow         
        //   315: astore          6
        //   317: goto            196
        //   320: astore          6
        //   322: goto            205
        //    Exceptions:
        //  throws java.security.cert.CertStoreException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  57     68     269    304    Ljava/lang/Exception;
        //  68     91     269    304    Ljava/lang/Exception;
        //  96     102    269    304    Ljava/lang/Exception;
        //  105    124    269    304    Ljava/lang/Exception;
        //  124    172    315    320    Ljava/io/IOException;
        //  124    172    315    320    Ljava/lang/IllegalArgumentException;
        //  124    172    269    304    Ljava/lang/Exception;
        //  172    196    315    320    Ljava/io/IOException;
        //  172    196    315    320    Ljava/lang/IllegalArgumentException;
        //  172    196    269    304    Ljava/lang/Exception;
        //  196    205    269    304    Ljava/lang/Exception;
        //  205    237    269    304    Ljava/lang/Exception;
        //  237    264    320    325    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 139 out of bounds for length 139
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:373)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:426)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
