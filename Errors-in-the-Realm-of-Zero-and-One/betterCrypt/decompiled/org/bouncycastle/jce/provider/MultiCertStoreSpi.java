// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

import java.security.cert.Certificate;
import java.security.cert.CertSelector;
import java.security.cert.CertStoreException;
import java.security.cert.CRL;
import java.util.List;
import java.util.Iterator;
import java.security.cert.CertStore;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Collection;
import java.security.cert.CRLSelector;
import java.security.InvalidAlgorithmParameterException;
import java.security.cert.CertStoreParameters;
import org.bouncycastle.jce.MultiCertStoreParameters;
import java.security.cert.CertStoreSpi;

public class MultiCertStoreSpi extends CertStoreSpi
{
    private MultiCertStoreParameters params;
    
    public MultiCertStoreSpi(final CertStoreParameters params) throws InvalidAlgorithmParameterException {
        super(params);
        if (params instanceof MultiCertStoreParameters) {
            this.params = (MultiCertStoreParameters)params;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("org.bouncycastle.jce.provider.MultiCertStoreSpi: parameter must be a MultiCertStoreParameters object\n");
        sb.append(params.toString());
        throw new InvalidAlgorithmParameterException(sb.toString());
    }
    
    @Override
    public Collection engineGetCRLs(final CRLSelector selector) throws CertStoreException {
        final boolean searchAllStores = this.params.getSearchAllStores();
        final Iterator<CertStore> iterator = this.params.getCertStores().iterator();
        List empty_LIST;
        if (searchAllStores) {
            empty_LIST = new ArrayList();
        }
        else {
            empty_LIST = Collections.EMPTY_LIST;
        }
        while (iterator.hasNext()) {
            final Collection<? extends CRL> crLs = iterator.next().getCRLs(selector);
            if (searchAllStores) {
                empty_LIST.addAll(crLs);
            }
            else {
                if (!crLs.isEmpty()) {
                    return crLs;
                }
                continue;
            }
        }
        return empty_LIST;
    }
    
    @Override
    public Collection engineGetCertificates(final CertSelector selector) throws CertStoreException {
        final boolean searchAllStores = this.params.getSearchAllStores();
        final Iterator<CertStore> iterator = this.params.getCertStores().iterator();
        List empty_LIST;
        if (searchAllStores) {
            empty_LIST = new ArrayList();
        }
        else {
            empty_LIST = Collections.EMPTY_LIST;
        }
        while (iterator.hasNext()) {
            final Collection<? extends Certificate> certificates = iterator.next().getCertificates(selector);
            if (searchAllStores) {
                empty_LIST.addAll(certificates);
            }
            else {
                if (!certificates.isEmpty()) {
                    return certificates;
                }
                continue;
            }
        }
        return empty_LIST;
    }
}
