// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

import java.security.Principal;
import java.io.IOException;
import org.bouncycastle.jce.exception.ExtCertPathBuilderException;
import java.security.cert.CertSelector;
import org.bouncycastle.jcajce.PKIXCertStoreSelector;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.x509.X509CertStoreSelector;
import java.security.cert.CertPathBuilderException;
import java.util.Set;
import org.bouncycastle.x509.ExtendedPKIXParameters;
import java.security.InvalidAlgorithmParameterException;
import org.bouncycastle.x509.ExtendedPKIXBuilderParameters;
import java.security.cert.PKIXBuilderParameters;
import org.bouncycastle.util.StoreException;
import org.bouncycastle.util.Selector;
import org.bouncycastle.util.Store;
import org.bouncycastle.x509.X509AttributeCertStoreSelector;
import java.util.Iterator;
import java.security.cert.CertPath;
import java.security.cert.CertificateParsingException;
import org.bouncycastle.jcajce.PKIXCertStore;
import java.util.HashSet;
import org.bouncycastle.asn1.x509.Extension;
import java.util.Collection;
import java.util.ArrayList;
import java.security.cert.PKIXCertPathBuilderResult;
import java.security.cert.CertPathParameters;
import java.security.cert.PKIXCertPathValidatorResult;
import java.security.cert.Certificate;
import java.security.cert.CertPathValidator;
import java.security.cert.CertificateFactory;
import java.security.cert.CertPathBuilderResult;
import java.util.List;
import org.bouncycastle.jcajce.PKIXExtendedBuilderParameters;
import java.security.cert.X509Certificate;
import org.bouncycastle.x509.X509AttributeCertificate;
import java.security.cert.CertPathBuilderSpi;

public class PKIXAttrCertPathBuilderSpi extends CertPathBuilderSpi
{
    private Exception certPathException;
    
    private CertPathBuilderResult build(final X509AttributeCertificate x509AttributeCertificate, final X509Certificate x509Certificate, final PKIXExtendedBuilderParameters params, final List certificates) {
        final boolean contains = certificates.contains(x509Certificate);
        CertPathBuilderResult build = null;
        final CertPathBuilderResult certPathBuilderResult = null;
        if (contains) {
            return null;
        }
        if (params.getExcludedCerts().contains(x509Certificate)) {
            return null;
        }
        if (params.getMaxPathLength() != -1 && certificates.size() - 1 > params.getMaxPathLength()) {
            return null;
        }
        certificates.add(x509Certificate);
        try {
            final CertificateFactory instance = CertificateFactory.getInstance("X.509", "BC");
            final CertPathValidator instance2 = CertPathValidator.getInstance("RFC3281", "BC");
            CertPathBuilderResult certPathBuilderResult2 = build;
            CertPathBuilderResult certPathBuilderResult3;
            try {
                if (CertPathValidatorUtilities.isIssuerTrustAnchor(x509Certificate, params.getBaseParameters().getTrustAnchors(), params.getBaseParameters().getSigProvider())) {
                    certPathBuilderResult2 = build;
                    try {
                        final CertPath generateCertPath = instance.generateCertPath(certificates);
                        certPathBuilderResult2 = build;
                        try {
                            final PKIXCertPathValidatorResult pkixCertPathValidatorResult = (PKIXCertPathValidatorResult)instance2.validate(generateCertPath, params);
                            certPathBuilderResult2 = build;
                            return new PKIXCertPathBuilderResult(generateCertPath, pkixCertPathValidatorResult.getTrustAnchor(), pkixCertPathValidatorResult.getPolicyTree(), pkixCertPathValidatorResult.getPublicKey());
                        }
                        catch (Exception ex) {
                            certPathBuilderResult2 = build;
                            throw new AnnotatedException("Certification path could not be validated.", ex);
                        }
                    }
                    catch (Exception ex2) {
                        certPathBuilderResult2 = build;
                        throw new AnnotatedException("Certification path could not be constructed from certificate list.", ex2);
                    }
                }
                certPathBuilderResult2 = build;
                final ArrayList<Object> list = new ArrayList<Object>();
                certPathBuilderResult2 = build;
                list.addAll(params.getBaseParameters().getCertificateStores());
                certPathBuilderResult2 = build;
                try {
                    list.addAll(CertPathValidatorUtilities.getAdditionalStoresFromAltNames(x509Certificate.getExtensionValue(Extension.issuerAlternativeName.getId()), params.getBaseParameters().getNamedCertificateStoreMap()));
                    certPathBuilderResult2 = build;
                    final HashSet<X509Certificate> set = new HashSet<X509Certificate>();
                    try {
                        set.addAll((Collection<?>)CertPathValidatorUtilities.findIssuerCerts(x509Certificate, params.getBaseParameters().getCertStores(), (List<PKIXCertStore>)list));
                        certPathBuilderResult2 = build;
                        if (set.isEmpty()) {
                            certPathBuilderResult2 = build;
                            throw new AnnotatedException("No issuer certificate for certificate in certification path found.");
                        }
                        certPathBuilderResult2 = build;
                        final Iterator<Object> iterator = set.iterator();
                        build = certPathBuilderResult;
                        while (true) {
                            certPathBuilderResult2 = build;
                            certPathBuilderResult3 = build;
                            if (!iterator.hasNext() || (certPathBuilderResult3 = build) != null) {
                                break;
                            }
                            certPathBuilderResult2 = build;
                            final X509Certificate x509Certificate2 = iterator.next();
                            certPathBuilderResult2 = build;
                            if (x509Certificate2.getIssuerX500Principal().equals(x509Certificate2.getSubjectX500Principal())) {
                                continue;
                            }
                            certPathBuilderResult2 = build;
                            build = this.build(x509AttributeCertificate, x509Certificate2, params, certificates);
                        }
                    }
                    catch (AnnotatedException ex3) {
                        certPathBuilderResult2 = build;
                        throw new AnnotatedException("Cannot find issuer certificate for certificate in certification path.", ex3);
                    }
                }
                catch (CertificateParsingException ex4) {
                    certPathBuilderResult2 = build;
                    throw new AnnotatedException("No additional X.509 stores can be added from certificate locations.", ex4);
                }
            }
            catch (AnnotatedException ex5) {
                this.certPathException = new AnnotatedException("No valid certification path could be build.", ex5);
                certPathBuilderResult3 = certPathBuilderResult2;
            }
            if (certPathBuilderResult3 == null) {
                certificates.remove(x509Certificate);
            }
            return certPathBuilderResult3;
        }
        catch (Exception ex6) {
            throw new RuntimeException("Exception creating support classes.");
        }
    }
    
    protected static Collection findCertificates(final X509AttributeCertStoreSelector x509AttributeCertStoreSelector, final List list) throws AnnotatedException {
        final HashSet set = new HashSet();
        for (final Store<? extends E> next : list) {
            if (next instanceof Store) {
                final Store<? extends E> store = next;
                try {
                    set.addAll(store.getMatches(x509AttributeCertStoreSelector));
                    continue;
                }
                catch (StoreException ex) {
                    throw new AnnotatedException("Problem while picking certificates from X.509 store.", ex);
                }
                break;
            }
        }
        return set;
    }
    
    @Override
    public CertPathBuilderResult engineBuild(final CertPathParameters certPathParameters) throws CertPathBuilderException, InvalidAlgorithmParameterException {
        final boolean b = certPathParameters instanceof PKIXBuilderParameters;
        if (!b && !(certPathParameters instanceof ExtendedPKIXBuilderParameters) && !(certPathParameters instanceof PKIXExtendedBuilderParameters)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Parameters must be an instance of ");
            sb.append(PKIXBuilderParameters.class.getName());
            sb.append(" or ");
            sb.append(PKIXExtendedBuilderParameters.class.getName());
            sb.append(".");
            throw new InvalidAlgorithmParameterException(sb.toString());
        }
        List stores = new ArrayList();
        PKIXExtendedBuilderParameters build;
        if (b) {
            final PKIXExtendedBuilderParameters.Builder builder = new PKIXExtendedBuilderParameters.Builder((PKIXBuilderParameters)certPathParameters);
            if (certPathParameters instanceof ExtendedPKIXParameters) {
                final ExtendedPKIXBuilderParameters extendedPKIXBuilderParameters = (ExtendedPKIXBuilderParameters)certPathParameters;
                builder.addExcludedCerts(extendedPKIXBuilderParameters.getExcludedCerts());
                builder.setMaxPathLength(extendedPKIXBuilderParameters.getMaxPathLength());
                stores = extendedPKIXBuilderParameters.getStores();
            }
            build = builder.build();
        }
        else {
            build = (PKIXExtendedBuilderParameters)certPathParameters;
        }
        final ArrayList list = new ArrayList();
        final PKIXCertStoreSelector targetConstraints = build.getBaseParameters().getTargetConstraints();
        if (targetConstraints instanceof X509AttributeCertStoreSelector) {
            try {
                final Collection certificates = findCertificates((X509AttributeCertStoreSelector)targetConstraints, stores);
                if (certificates.isEmpty()) {
                    throw new CertPathBuilderException("No attribute certificate found matching targetContraints.");
                }
                CertPathBuilderResult certPathBuilderResult = null;
                final Iterator<X509AttributeCertificate> iterator = certificates.iterator();
                while (iterator.hasNext() && certPathBuilderResult == null) {
                    final X509AttributeCertificate x509AttributeCertificate = iterator.next();
                    final X509CertStoreSelector x509CertStoreSelector = new X509CertStoreSelector();
                    final Principal[] principals = x509AttributeCertificate.getIssuer().getPrincipals();
                    final HashSet<X509Certificate> set = new HashSet<X509Certificate>();
                    int i = 0;
                    while (i < principals.length) {
                        try {
                            if (principals[i] instanceof X500Principal) {
                                x509CertStoreSelector.setSubject(((X500Principal)principals[i]).getEncoded());
                            }
                            final PKIXCertStoreSelector<? extends Certificate> build2 = new PKIXCertStoreSelector.Builder(x509CertStoreSelector).build();
                            set.addAll((Collection<?>)CertPathValidatorUtilities.findCertificates(build2, build.getBaseParameters().getCertStores()));
                            set.addAll((Collection<?>)CertPathValidatorUtilities.findCertificates(build2, build.getBaseParameters().getCertificateStores()));
                            ++i;
                            continue;
                        }
                        catch (IOException ex) {
                            throw new ExtCertPathBuilderException("cannot encode X500Principal.", ex);
                        }
                        catch (AnnotatedException ex2) {
                            throw new ExtCertPathBuilderException("Public key certificate for attribute certificate cannot be searched.", ex2);
                        }
                        break;
                    }
                    if (set.isEmpty()) {
                        throw new CertPathBuilderException("Public key certificate for attribute certificate cannot be found.");
                    }
                    final Iterator<Object> iterator2 = set.iterator();
                    CertPathBuilderResult build3 = certPathBuilderResult;
                    while (true) {
                        certPathBuilderResult = build3;
                        if (!iterator2.hasNext() || (certPathBuilderResult = build3) != null) {
                            break;
                        }
                        build3 = this.build(x509AttributeCertificate, iterator2.next(), build, list);
                    }
                }
                if (certPathBuilderResult == null) {
                    final Exception certPathException = this.certPathException;
                    if (certPathException != null) {
                        throw new ExtCertPathBuilderException("Possible certificate chain could not be validated.", certPathException);
                    }
                }
                if (certPathBuilderResult != null) {
                    return certPathBuilderResult;
                }
                if (this.certPathException != null) {
                    return certPathBuilderResult;
                }
                throw new CertPathBuilderException("Unable to find certificate chain.");
            }
            catch (AnnotatedException ex3) {
                throw new ExtCertPathBuilderException("Error finding target attribute certificate.", ex3);
            }
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("TargetConstraints must be an instance of ");
        sb2.append(X509AttributeCertStoreSelector.class.getName());
        sb2.append(" for ");
        sb2.append(this.getClass().getName());
        sb2.append(" class.");
        throw new CertPathBuilderException(sb2.toString());
    }
}
