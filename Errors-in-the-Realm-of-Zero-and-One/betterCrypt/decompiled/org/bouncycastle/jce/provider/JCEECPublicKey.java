// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

import org.bouncycastle.util.Strings;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.asn1.ASN1Null;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.jcajce.provider.asymmetric.util.KeyUtil;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x9.X962Parameters;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.asn1.x9.X9ECPoint;
import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import org.bouncycastle.asn1.cryptopro.ECGOST3410NamedCurves;
import org.bouncycastle.jce.spec.ECNamedCurveSpec;
import java.io.ObjectOutputStream;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1Primitive;
import java.io.ObjectInputStream;
import java.math.BigInteger;
import java.security.spec.EllipticCurve;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util;
import java.security.spec.ECPublicKeySpec;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.asn1.cryptopro.GOST3410PublicKeyAlgParameters;
import java.security.spec.ECParameterSpec;
import org.bouncycastle.jce.interfaces.ECPointEncoder;
import java.security.interfaces.ECPublicKey;

public class JCEECPublicKey implements ECPublicKey, org.bouncycastle.jce.interfaces.ECPublicKey, ECPointEncoder
{
    private String algorithm;
    private ECParameterSpec ecSpec;
    private GOST3410PublicKeyAlgParameters gostParams;
    private ECPoint q;
    private boolean withCompression;
    
    public JCEECPublicKey(final String algorithm, final ECPublicKeySpec ecPublicKeySpec) {
        this.algorithm = "EC";
        this.algorithm = algorithm;
        this.ecSpec = ecPublicKeySpec.getParams();
        this.q = EC5Util.convertPoint(this.ecSpec, ecPublicKeySpec.getW());
    }
    
    public JCEECPublicKey(final String algorithm, final ECPublicKeyParameters ecPublicKeyParameters) {
        this.algorithm = "EC";
        this.algorithm = algorithm;
        this.q = ecPublicKeyParameters.getQ();
        this.ecSpec = null;
    }
    
    public JCEECPublicKey(final String algorithm, final ECPublicKeyParameters ecPublicKeyParameters, final ECParameterSpec ecSpec) {
        this.algorithm = "EC";
        final ECDomainParameters parameters = ecPublicKeyParameters.getParameters();
        this.algorithm = algorithm;
        this.q = ecPublicKeyParameters.getQ();
        if (ecSpec == null) {
            this.ecSpec = this.createSpec(EC5Util.convertCurve(parameters.getCurve(), parameters.getSeed()), parameters);
            return;
        }
        this.ecSpec = ecSpec;
    }
    
    public JCEECPublicKey(final String algorithm, final ECPublicKeyParameters ecPublicKeyParameters, final org.bouncycastle.jce.spec.ECParameterSpec ecParameterSpec) {
        this.algorithm = "EC";
        final ECDomainParameters parameters = ecPublicKeyParameters.getParameters();
        this.algorithm = algorithm;
        this.q = ecPublicKeyParameters.getQ();
        ECParameterSpec ecSpec;
        if (ecParameterSpec == null) {
            ecSpec = this.createSpec(EC5Util.convertCurve(parameters.getCurve(), parameters.getSeed()), parameters);
        }
        else {
            ecSpec = EC5Util.convertSpec(EC5Util.convertCurve(ecParameterSpec.getCurve(), ecParameterSpec.getSeed()), ecParameterSpec);
        }
        this.ecSpec = ecSpec;
    }
    
    public JCEECPublicKey(final String algorithm, final JCEECPublicKey jceecPublicKey) {
        this.algorithm = "EC";
        this.algorithm = algorithm;
        this.q = jceecPublicKey.q;
        this.ecSpec = jceecPublicKey.ecSpec;
        this.withCompression = jceecPublicKey.withCompression;
        this.gostParams = jceecPublicKey.gostParams;
    }
    
    public JCEECPublicKey(final String algorithm, final org.bouncycastle.jce.spec.ECPublicKeySpec ecPublicKeySpec) {
        this.algorithm = "EC";
        this.algorithm = algorithm;
        this.q = ecPublicKeySpec.getQ();
        ECParameterSpec convertSpec;
        if (ecPublicKeySpec.getParams() != null) {
            convertSpec = EC5Util.convertSpec(EC5Util.convertCurve(ecPublicKeySpec.getParams().getCurve(), ecPublicKeySpec.getParams().getSeed()), ecPublicKeySpec.getParams());
        }
        else {
            if (this.q.getCurve() == null) {
                this.q = BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa().getCurve().createPoint(this.q.getAffineXCoord().toBigInteger(), this.q.getAffineYCoord().toBigInteger());
            }
            convertSpec = null;
        }
        this.ecSpec = convertSpec;
    }
    
    public JCEECPublicKey(final ECPublicKey ecPublicKey) {
        this.algorithm = "EC";
        this.algorithm = ecPublicKey.getAlgorithm();
        this.ecSpec = ecPublicKey.getParams();
        this.q = EC5Util.convertPoint(this.ecSpec, ecPublicKey.getW());
    }
    
    JCEECPublicKey(final SubjectPublicKeyInfo subjectPublicKeyInfo) {
        this.algorithm = "EC";
        this.populateFromPubKeyInfo(subjectPublicKeyInfo);
    }
    
    private ECParameterSpec createSpec(final EllipticCurve curve, final ECDomainParameters ecDomainParameters) {
        return new ECParameterSpec(curve, EC5Util.convertPoint(ecDomainParameters.getG()), ecDomainParameters.getN(), ecDomainParameters.getH().intValue());
    }
    
    private void extractBytes(final byte[] array, final int n, final BigInteger bigInteger) {
        final byte[] byteArray = bigInteger.toByteArray();
        final int length = byteArray.length;
        int i;
        final int n2 = i = 0;
        byte[] array2 = byteArray;
        if (length < 32) {
            array2 = new byte[32];
            System.arraycopy(byteArray, 0, array2, array2.length - byteArray.length, byteArray.length);
            i = n2;
        }
        while (i != 32) {
            array[n + i] = array2[array2.length - 1 - i];
            ++i;
        }
    }
    
    private void populateFromPubKeyInfo(final SubjectPublicKeyInfo p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   org/bouncycastle/asn1/x509/SubjectPublicKeyInfo.getAlgorithm:()Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //     4: astore          4
        //     6: aload           4
        //     8: invokevirtual   org/bouncycastle/asn1/x509/AlgorithmIdentifier.getAlgorithm:()Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //    11: getstatic       org/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers.gostR3410_2001:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //    14: invokevirtual   org/bouncycastle/asn1/ASN1ObjectIdentifier.equals:(Lorg/bouncycastle/asn1/ASN1Primitive;)Z
        //    17: istore_3       
        //    18: iconst_1       
        //    19: istore_2       
        //    20: iload_3        
        //    21: ifeq            206
        //    24: aload_1        
        //    25: invokevirtual   org/bouncycastle/asn1/x509/SubjectPublicKeyInfo.getPublicKeyData:()Lorg/bouncycastle/asn1/DERBitString;
        //    28: astore_1       
        //    29: aload_0        
        //    30: ldc             "ECGOST3410"
        //    32: putfield        org/bouncycastle/jce/provider/JCEECPublicKey.algorithm:Ljava/lang/String;
        //    35: aload_1        
        //    36: invokevirtual   org/bouncycastle/asn1/DERBitString.getBytes:()[B
        //    39: invokestatic    org/bouncycastle/asn1/ASN1Primitive.fromByteArray:([B)Lorg/bouncycastle/asn1/ASN1Primitive;
        //    42: checkcast       Lorg/bouncycastle/asn1/ASN1OctetString;
        //    45: astore_1       
        //    46: aload_1        
        //    47: invokevirtual   org/bouncycastle/asn1/ASN1OctetString.getOctets:()[B
        //    50: astore          5
        //    52: bipush          65
        //    54: newarray        B
        //    56: astore_1       
        //    57: aload_1        
        //    58: iconst_0       
        //    59: iconst_4       
        //    60: bastore        
        //    61: iload_2        
        //    62: bipush          32
        //    64: if_icmpgt       97
        //    67: aload_1        
        //    68: iload_2        
        //    69: aload           5
        //    71: bipush          32
        //    73: iload_2        
        //    74: isub           
        //    75: baload         
        //    76: bastore        
        //    77: aload_1        
        //    78: iload_2        
        //    79: bipush          32
        //    81: iadd           
        //    82: aload           5
        //    84: bipush          64
        //    86: iload_2        
        //    87: isub           
        //    88: baload         
        //    89: bastore        
        //    90: iload_2        
        //    91: iconst_1       
        //    92: iadd           
        //    93: istore_2       
        //    94: goto            61
        //    97: aload_0        
        //    98: aload           4
        //   100: invokevirtual   org/bouncycastle/asn1/x509/AlgorithmIdentifier.getParameters:()Lorg/bouncycastle/asn1/ASN1Encodable;
        //   103: invokestatic    org/bouncycastle/asn1/cryptopro/GOST3410PublicKeyAlgParameters.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/cryptopro/GOST3410PublicKeyAlgParameters;
        //   106: putfield        org/bouncycastle/jce/provider/JCEECPublicKey.gostParams:Lorg/bouncycastle/asn1/cryptopro/GOST3410PublicKeyAlgParameters;
        //   109: aload_0        
        //   110: getfield        org/bouncycastle/jce/provider/JCEECPublicKey.gostParams:Lorg/bouncycastle/asn1/cryptopro/GOST3410PublicKeyAlgParameters;
        //   113: invokevirtual   org/bouncycastle/asn1/cryptopro/GOST3410PublicKeyAlgParameters.getPublicKeyParamSet:()Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //   116: invokestatic    org/bouncycastle/asn1/cryptopro/ECGOST3410NamedCurves.getName:(Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;)Ljava/lang/String;
        //   119: invokestatic    org/bouncycastle/jce/ECGOST3410NamedCurveTable.getParameterSpec:(Ljava/lang/String;)Lorg/bouncycastle/jce/spec/ECNamedCurveParameterSpec;
        //   122: astore          4
        //   124: aload           4
        //   126: invokevirtual   org/bouncycastle/jce/spec/ECNamedCurveParameterSpec.getCurve:()Lorg/bouncycastle/math/ec/ECCurve;
        //   129: astore          5
        //   131: aload           5
        //   133: aload           4
        //   135: invokevirtual   org/bouncycastle/jce/spec/ECNamedCurveParameterSpec.getSeed:()[B
        //   138: invokestatic    org/bouncycastle/jcajce/provider/asymmetric/util/EC5Util.convertCurve:(Lorg/bouncycastle/math/ec/ECCurve;[B)Ljava/security/spec/EllipticCurve;
        //   141: astore          6
        //   143: aload_0        
        //   144: aload           5
        //   146: aload_1        
        //   147: invokevirtual   org/bouncycastle/math/ec/ECCurve.decodePoint:([B)Lorg/bouncycastle/math/ec/ECPoint;
        //   150: putfield        org/bouncycastle/jce/provider/JCEECPublicKey.q:Lorg/bouncycastle/math/ec/ECPoint;
        //   153: aload_0        
        //   154: new             Lorg/bouncycastle/jce/spec/ECNamedCurveSpec;
        //   157: dup            
        //   158: aload_0        
        //   159: getfield        org/bouncycastle/jce/provider/JCEECPublicKey.gostParams:Lorg/bouncycastle/asn1/cryptopro/GOST3410PublicKeyAlgParameters;
        //   162: invokevirtual   org/bouncycastle/asn1/cryptopro/GOST3410PublicKeyAlgParameters.getPublicKeyParamSet:()Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //   165: invokestatic    org/bouncycastle/asn1/cryptopro/ECGOST3410NamedCurves.getName:(Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;)Ljava/lang/String;
        //   168: aload           6
        //   170: aload           4
        //   172: invokevirtual   org/bouncycastle/jce/spec/ECNamedCurveParameterSpec.getG:()Lorg/bouncycastle/math/ec/ECPoint;
        //   175: invokestatic    org/bouncycastle/jcajce/provider/asymmetric/util/EC5Util.convertPoint:(Lorg/bouncycastle/math/ec/ECPoint;)Ljava/security/spec/ECPoint;
        //   178: aload           4
        //   180: invokevirtual   org/bouncycastle/jce/spec/ECNamedCurveParameterSpec.getN:()Ljava/math/BigInteger;
        //   183: aload           4
        //   185: invokevirtual   org/bouncycastle/jce/spec/ECNamedCurveParameterSpec.getH:()Ljava/math/BigInteger;
        //   188: invokespecial   org/bouncycastle/jce/spec/ECNamedCurveSpec.<init>:(Ljava/lang/String;Ljava/security/spec/EllipticCurve;Ljava/security/spec/ECPoint;Ljava/math/BigInteger;Ljava/math/BigInteger;)V
        //   191: putfield        org/bouncycastle/jce/provider/JCEECPublicKey.ecSpec:Ljava/security/spec/ECParameterSpec;
        //   194: return         
        //   195: new             Ljava/lang/IllegalArgumentException;
        //   198: dup            
        //   199: ldc_w           "error recovering public key"
        //   202: invokespecial   java/lang/IllegalArgumentException.<init>:(Ljava/lang/String;)V
        //   205: athrow         
        //   206: aload           4
        //   208: invokevirtual   org/bouncycastle/asn1/x509/AlgorithmIdentifier.getParameters:()Lorg/bouncycastle/asn1/ASN1Encodable;
        //   211: invokestatic    org/bouncycastle/asn1/x9/X962Parameters.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x9/X962Parameters;
        //   214: astore          4
        //   216: aload           4
        //   218: invokevirtual   org/bouncycastle/asn1/x9/X962Parameters.isNamedCurve:()Z
        //   221: ifeq            303
        //   224: aload           4
        //   226: invokevirtual   org/bouncycastle/asn1/x9/X962Parameters.getParameters:()Lorg/bouncycastle/asn1/ASN1Primitive;
        //   229: checkcast       Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //   232: astore          5
        //   234: aload           5
        //   236: invokestatic    org/bouncycastle/jcajce/provider/asymmetric/util/ECUtil.getNamedCurveByOid:(Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;)Lorg/bouncycastle/asn1/x9/X9ECParameters;
        //   239: astore          6
        //   241: aload           6
        //   243: invokevirtual   org/bouncycastle/asn1/x9/X9ECParameters.getCurve:()Lorg/bouncycastle/math/ec/ECCurve;
        //   246: astore          4
        //   248: aload           4
        //   250: aload           6
        //   252: invokevirtual   org/bouncycastle/asn1/x9/X9ECParameters.getSeed:()[B
        //   255: invokestatic    org/bouncycastle/jcajce/provider/asymmetric/util/EC5Util.convertCurve:(Lorg/bouncycastle/math/ec/ECCurve;[B)Ljava/security/spec/EllipticCurve;
        //   258: astore          7
        //   260: new             Lorg/bouncycastle/jce/spec/ECNamedCurveSpec;
        //   263: dup            
        //   264: aload           5
        //   266: invokestatic    org/bouncycastle/jcajce/provider/asymmetric/util/ECUtil.getCurveName:(Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;)Ljava/lang/String;
        //   269: aload           7
        //   271: aload           6
        //   273: invokevirtual   org/bouncycastle/asn1/x9/X9ECParameters.getG:()Lorg/bouncycastle/math/ec/ECPoint;
        //   276: invokestatic    org/bouncycastle/jcajce/provider/asymmetric/util/EC5Util.convertPoint:(Lorg/bouncycastle/math/ec/ECPoint;)Ljava/security/spec/ECPoint;
        //   279: aload           6
        //   281: invokevirtual   org/bouncycastle/asn1/x9/X9ECParameters.getN:()Ljava/math/BigInteger;
        //   284: aload           6
        //   286: invokevirtual   org/bouncycastle/asn1/x9/X9ECParameters.getH:()Ljava/math/BigInteger;
        //   289: invokespecial   org/bouncycastle/jce/spec/ECNamedCurveSpec.<init>:(Ljava/lang/String;Ljava/security/spec/EllipticCurve;Ljava/security/spec/ECPoint;Ljava/math/BigInteger;Ljava/math/BigInteger;)V
        //   292: astore          5
        //   294: aload_0        
        //   295: aload           5
        //   297: putfield        org/bouncycastle/jce/provider/JCEECPublicKey.ecSpec:Ljava/security/spec/ECParameterSpec;
        //   300: goto            392
        //   303: aload           4
        //   305: invokevirtual   org/bouncycastle/asn1/x9/X962Parameters.isImplicitlyCA:()Z
        //   308: ifeq            332
        //   311: aload_0        
        //   312: aconst_null    
        //   313: putfield        org/bouncycastle/jce/provider/JCEECPublicKey.ecSpec:Ljava/security/spec/ECParameterSpec;
        //   316: getstatic       org/bouncycastle/jce/provider/BouncyCastleProvider.CONFIGURATION:Lorg/bouncycastle/jcajce/provider/config/ProviderConfiguration;
        //   319: invokeinterface org/bouncycastle/jcajce/provider/config/ProviderConfiguration.getEcImplicitlyCa:()Lorg/bouncycastle/jce/spec/ECParameterSpec;
        //   324: invokevirtual   org/bouncycastle/jce/spec/ECParameterSpec.getCurve:()Lorg/bouncycastle/math/ec/ECCurve;
        //   327: astore          4
        //   329: goto            392
        //   332: aload           4
        //   334: invokevirtual   org/bouncycastle/asn1/x9/X962Parameters.getParameters:()Lorg/bouncycastle/asn1/ASN1Primitive;
        //   337: invokestatic    org/bouncycastle/asn1/x9/X9ECParameters.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x9/X9ECParameters;
        //   340: astore          5
        //   342: aload           5
        //   344: invokevirtual   org/bouncycastle/asn1/x9/X9ECParameters.getCurve:()Lorg/bouncycastle/math/ec/ECCurve;
        //   347: astore          4
        //   349: new             Ljava/security/spec/ECParameterSpec;
        //   352: dup            
        //   353: aload           4
        //   355: aload           5
        //   357: invokevirtual   org/bouncycastle/asn1/x9/X9ECParameters.getSeed:()[B
        //   360: invokestatic    org/bouncycastle/jcajce/provider/asymmetric/util/EC5Util.convertCurve:(Lorg/bouncycastle/math/ec/ECCurve;[B)Ljava/security/spec/EllipticCurve;
        //   363: aload           5
        //   365: invokevirtual   org/bouncycastle/asn1/x9/X9ECParameters.getG:()Lorg/bouncycastle/math/ec/ECPoint;
        //   368: invokestatic    org/bouncycastle/jcajce/provider/asymmetric/util/EC5Util.convertPoint:(Lorg/bouncycastle/math/ec/ECPoint;)Ljava/security/spec/ECPoint;
        //   371: aload           5
        //   373: invokevirtual   org/bouncycastle/asn1/x9/X9ECParameters.getN:()Ljava/math/BigInteger;
        //   376: aload           5
        //   378: invokevirtual   org/bouncycastle/asn1/x9/X9ECParameters.getH:()Ljava/math/BigInteger;
        //   381: invokevirtual   java/math/BigInteger.intValue:()I
        //   384: invokespecial   java/security/spec/ECParameterSpec.<init>:(Ljava/security/spec/EllipticCurve;Ljava/security/spec/ECPoint;Ljava/math/BigInteger;I)V
        //   387: astore          5
        //   389: goto            294
        //   392: aload_1        
        //   393: invokevirtual   org/bouncycastle/asn1/x509/SubjectPublicKeyInfo.getPublicKeyData:()Lorg/bouncycastle/asn1/DERBitString;
        //   396: invokevirtual   org/bouncycastle/asn1/DERBitString.getBytes:()[B
        //   399: astore          6
        //   401: new             Lorg/bouncycastle/asn1/DEROctetString;
        //   404: dup            
        //   405: aload           6
        //   407: invokespecial   org/bouncycastle/asn1/DEROctetString.<init>:([B)V
        //   410: astore          5
        //   412: aload           5
        //   414: astore_1       
        //   415: aload           6
        //   417: iconst_0       
        //   418: baload         
        //   419: iconst_4       
        //   420: if_icmpne       503
        //   423: aload           5
        //   425: astore_1       
        //   426: aload           6
        //   428: iconst_1       
        //   429: baload         
        //   430: aload           6
        //   432: arraylength    
        //   433: iconst_2       
        //   434: isub           
        //   435: if_icmpne       503
        //   438: aload           6
        //   440: iconst_2       
        //   441: baload         
        //   442: iconst_2       
        //   443: if_icmpeq       457
        //   446: aload           5
        //   448: astore_1       
        //   449: aload           6
        //   451: iconst_2       
        //   452: baload         
        //   453: iconst_3       
        //   454: if_icmpne       503
        //   457: aload           5
        //   459: astore_1       
        //   460: new             Lorg/bouncycastle/asn1/x9/X9IntegerConverter;
        //   463: dup            
        //   464: invokespecial   org/bouncycastle/asn1/x9/X9IntegerConverter.<init>:()V
        //   467: aload           4
        //   469: invokevirtual   org/bouncycastle/asn1/x9/X9IntegerConverter.getByteLength:(Lorg/bouncycastle/math/ec/ECCurve;)I
        //   472: aload           6
        //   474: arraylength    
        //   475: iconst_3       
        //   476: isub           
        //   477: if_icmplt       503
        //   480: aload           6
        //   482: invokestatic    org/bouncycastle/asn1/ASN1Primitive.fromByteArray:([B)Lorg/bouncycastle/asn1/ASN1Primitive;
        //   485: checkcast       Lorg/bouncycastle/asn1/ASN1OctetString;
        //   488: astore_1       
        //   489: goto            503
        //   492: new             Ljava/lang/IllegalArgumentException;
        //   495: dup            
        //   496: ldc_w           "error recovering public key"
        //   499: invokespecial   java/lang/IllegalArgumentException.<init>:(Ljava/lang/String;)V
        //   502: athrow         
        //   503: aload_0        
        //   504: new             Lorg/bouncycastle/asn1/x9/X9ECPoint;
        //   507: dup            
        //   508: aload           4
        //   510: aload_1        
        //   511: invokespecial   org/bouncycastle/asn1/x9/X9ECPoint.<init>:(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/asn1/ASN1OctetString;)V
        //   514: invokevirtual   org/bouncycastle/asn1/x9/X9ECPoint.getPoint:()Lorg/bouncycastle/math/ec/ECPoint;
        //   517: putfield        org/bouncycastle/jce/provider/JCEECPublicKey.q:Lorg/bouncycastle/math/ec/ECPoint;
        //   520: return         
        //   521: astore_1       
        //   522: goto            195
        //   525: astore_1       
        //   526: goto            492
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  35     46     521    206    Ljava/io/IOException;
        //  480    489    525    503    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0492:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        this.populateFromPubKeyInfo(SubjectPublicKeyInfo.getInstance(ASN1Primitive.fromByteArray((byte[])objectInputStream.readObject())));
        this.algorithm = (String)objectInputStream.readObject();
        this.withCompression = objectInputStream.readBoolean();
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeObject(this.getEncoded());
        objectOutputStream.writeObject(this.algorithm);
        objectOutputStream.writeBoolean(this.withCompression);
    }
    
    public ECPoint engineGetQ() {
        return this.q;
    }
    
    org.bouncycastle.jce.spec.ECParameterSpec engineGetSpec() {
        final ECParameterSpec ecSpec = this.ecSpec;
        if (ecSpec != null) {
            return EC5Util.convertSpec(ecSpec);
        }
        return BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa();
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof JCEECPublicKey;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final JCEECPublicKey jceecPublicKey = (JCEECPublicKey)o;
        boolean b3 = b2;
        if (this.engineGetQ().equals(jceecPublicKey.engineGetQ())) {
            b3 = b2;
            if (this.engineGetSpec().equals(jceecPublicKey.engineGetSpec())) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public String getAlgorithm() {
        return this.algorithm;
    }
    
    @Override
    public byte[] getEncoded() {
        while (true) {
            Label_0212: {
                if (!this.algorithm.equals("ECGOST3410")) {
                    break Label_0212;
                }
                ASN1Object gostParams = this.gostParams;
                if (gostParams == null) {
                    final ECParameterSpec ecSpec = this.ecSpec;
                    if (ecSpec instanceof ECNamedCurveSpec) {
                        gostParams = new GOST3410PublicKeyAlgParameters(ECGOST3410NamedCurves.getOID(((ECNamedCurveSpec)ecSpec).getName()), CryptoProObjectIdentifiers.gostR3411_94_CryptoProParamSet);
                    }
                    else {
                        final ECCurve convertCurve = EC5Util.convertCurve(ecSpec.getCurve());
                        gostParams = new X962Parameters(new X9ECParameters(convertCurve, new X9ECPoint(EC5Util.convertPoint(convertCurve, this.ecSpec.getGenerator()), this.withCompression), this.ecSpec.getOrder(), BigInteger.valueOf(this.ecSpec.getCofactor()), this.ecSpec.getCurve().getSeed()));
                    }
                }
                final BigInteger bigInteger = this.q.getAffineXCoord().toBigInteger();
                final BigInteger bigInteger2 = this.q.getAffineYCoord().toBigInteger();
                final byte[] array = new byte[64];
                this.extractBytes(array, 0, bigInteger);
                this.extractBytes(array, 32, bigInteger2);
                try {
                    SubjectPublicKeyInfo subjectPublicKeyInfo = new SubjectPublicKeyInfo(new AlgorithmIdentifier(CryptoProObjectIdentifiers.gostR3410_2001, gostParams), new DEROctetString(array));
                    return KeyUtil.getEncodedSubjectPublicKeyInfo(subjectPublicKeyInfo);
                    // iftrue(Label_0289:, ecSpec2 != null)
                    // iftrue(Label_0259:, namedCurveOid = ECUtil.getNamedCurveOid((ECNamedCurveSpec)ecSpec2.getName()) != null)
                    ECParameterSpec ecSpec2;
                    ASN1ObjectIdentifier namedCurveOid;
                    X962Parameters x962Parameters;
                    ECCurve convertCurve2;
                    Label_0363:Block_6_Outer:
                    while (true) {
                        while (true) {
                            while (true) {
                                Block_8: {
                                    break Block_8;
                                    x962Parameters = new X962Parameters(namedCurveOid);
                                    break Label_0363;
                                }
                                x962Parameters = new X962Parameters(DERNull.INSTANCE);
                                subjectPublicKeyInfo = new SubjectPublicKeyInfo(new AlgorithmIdentifier(X9ObjectIdentifiers.id_ecPublicKey, x962Parameters), this.getQ().getEncoded(this.withCompression));
                                return KeyUtil.getEncodedSubjectPublicKeyInfo(subjectPublicKeyInfo);
                                namedCurveOid = new ASN1ObjectIdentifier(((ECNamedCurveSpec)this.ecSpec).getName());
                                continue Block_6_Outer;
                            }
                            return null;
                            ecSpec2 = this.ecSpec;
                            continue;
                        }
                        Label_0289: {
                            convertCurve2 = EC5Util.convertCurve(ecSpec2.getCurve());
                        }
                        x962Parameters = new X962Parameters(new X9ECParameters(convertCurve2, new X9ECPoint(EC5Util.convertPoint(convertCurve2, this.ecSpec.getGenerator()), this.withCompression), this.ecSpec.getOrder(), BigInteger.valueOf(this.ecSpec.getCofactor()), this.ecSpec.getCurve().getSeed()));
                        continue Label_0363;
                    }
                }
                // iftrue(Label_0271:, !ecSpec2 instanceof ECNamedCurveSpec)
                catch (IOException ex) {}
            }
            continue;
        }
    }
    
    @Override
    public String getFormat() {
        return "X.509";
    }
    
    @Override
    public org.bouncycastle.jce.spec.ECParameterSpec getParameters() {
        final ECParameterSpec ecSpec = this.ecSpec;
        if (ecSpec == null) {
            return null;
        }
        return EC5Util.convertSpec(ecSpec);
    }
    
    @Override
    public ECParameterSpec getParams() {
        return this.ecSpec;
    }
    
    @Override
    public ECPoint getQ() {
        if (this.ecSpec == null) {
            return this.q.getDetachedPoint();
        }
        return this.q;
    }
    
    @Override
    public java.security.spec.ECPoint getW() {
        return EC5Util.convertPoint(this.q);
    }
    
    @Override
    public int hashCode() {
        return this.engineGetQ().hashCode() ^ this.engineGetSpec().hashCode();
    }
    
    @Override
    public void setPointFormat(final String anotherString) {
        this.withCompression = ("UNCOMPRESSED".equalsIgnoreCase(anotherString) ^ true);
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        final String lineSeparator = Strings.lineSeparator();
        sb.append("EC Public Key");
        sb.append(lineSeparator);
        sb.append("            X: ");
        sb.append(this.q.getAffineXCoord().toBigInteger().toString(16));
        sb.append(lineSeparator);
        sb.append("            Y: ");
        sb.append(this.q.getAffineYCoord().toBigInteger().toString(16));
        sb.append(lineSeparator);
        return sb.toString();
    }
}
