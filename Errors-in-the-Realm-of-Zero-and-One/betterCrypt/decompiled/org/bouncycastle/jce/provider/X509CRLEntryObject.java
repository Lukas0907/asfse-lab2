// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.util.ASN1Dump;
import org.bouncycastle.asn1.x509.CRLReason;
import org.bouncycastle.asn1.ASN1Enumerated;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.x509.X509Extension;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.util.Strings;
import java.math.BigInteger;
import java.util.Date;
import java.security.cert.CRLException;
import java.io.IOException;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.TBSCertList;
import java.security.cert.X509CRLEntry;

public class X509CRLEntryObject extends X509CRLEntry
{
    private TBSCertList.CRLEntry c;
    private X500Name certificateIssuer;
    private int hashValue;
    private boolean isHashValueSet;
    
    public X509CRLEntryObject(final TBSCertList.CRLEntry c) {
        this.c = c;
        this.certificateIssuer = null;
    }
    
    public X509CRLEntryObject(final TBSCertList.CRLEntry c, final boolean b, final X500Name x500Name) {
        this.c = c;
        this.certificateIssuer = this.loadCertificateIssuer(b, x500Name);
    }
    
    private Extension getExtension(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        final Extensions extensions = this.c.getExtensions();
        if (extensions != null) {
            return extensions.getExtension(asn1ObjectIdentifier);
        }
        return null;
    }
    
    private Set getExtensionOIDs(final boolean b) {
        final Extensions extensions = this.c.getExtensions();
        if (extensions != null) {
            final HashSet<String> set = new HashSet<String>();
            final Enumeration oids = extensions.oids();
            while (oids.hasMoreElements()) {
                final ASN1ObjectIdentifier asn1ObjectIdentifier = oids.nextElement();
                if (b == extensions.getExtension(asn1ObjectIdentifier).isCritical()) {
                    set.add(asn1ObjectIdentifier.getId());
                }
            }
            return set;
        }
        return null;
    }
    
    private X500Name loadCertificateIssuer(final boolean b, X500Name instance) {
        if (!b) {
            return null;
        }
        final Extension extension = this.getExtension(Extension.certificateIssuer);
        if (extension == null) {
            return instance;
        }
        try {
            final GeneralName[] names = GeneralNames.getInstance(extension.getParsedValue()).getNames();
            for (int i = 0; i < names.length; ++i) {
                if (names[i].getTagNo() == 4) {
                    instance = X500Name.getInstance(names[i].getName());
                    return instance;
                }
            }
            return null;
        }
        catch (Exception ex) {
            return null;
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof X509CRLEntryObject) {
            return this.c.equals(((X509CRLEntryObject)o).c);
        }
        return super.equals(this);
    }
    
    @Override
    public X500Principal getCertificateIssuer() {
        final X500Name certificateIssuer = this.certificateIssuer;
        if (certificateIssuer == null) {
            return null;
        }
        try {
            return new X500Principal(certificateIssuer.getEncoded());
        }
        catch (IOException ex) {
            return null;
        }
    }
    
    @Override
    public Set getCriticalExtensionOIDs() {
        return this.getExtensionOIDs(true);
    }
    
    @Override
    public byte[] getEncoded() throws CRLException {
        try {
            return this.c.getEncoded("DER");
        }
        catch (IOException ex) {
            throw new CRLException(ex.toString());
        }
    }
    
    @Override
    public byte[] getExtensionValue(final String s) {
        final Extension extension = this.getExtension(new ASN1ObjectIdentifier(s));
        if (extension != null) {
            try {
                return extension.getExtnValue().getEncoded();
            }
            catch (Exception ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("error encoding ");
                sb.append(ex.toString());
                throw new RuntimeException(sb.toString());
            }
        }
        return null;
    }
    
    @Override
    public Set getNonCriticalExtensionOIDs() {
        return this.getExtensionOIDs(false);
    }
    
    @Override
    public Date getRevocationDate() {
        return this.c.getRevocationDate().getDate();
    }
    
    @Override
    public BigInteger getSerialNumber() {
        return this.c.getUserCertificate().getValue();
    }
    
    @Override
    public boolean hasExtensions() {
        return this.c.getExtensions() != null;
    }
    
    @Override
    public boolean hasUnsupportedCriticalExtension() {
        final Set criticalExtensionOIDs = this.getCriticalExtensionOIDs();
        return criticalExtensionOIDs != null && !criticalExtensionOIDs.isEmpty();
    }
    
    @Override
    public int hashCode() {
        if (!this.isHashValueSet) {
            this.hashValue = super.hashCode();
            this.isHashValueSet = true;
        }
        return this.hashValue;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        final String lineSeparator = Strings.lineSeparator();
        sb.append("      userCertificate: ");
        sb.append(this.getSerialNumber());
        sb.append(lineSeparator);
        sb.append("       revocationDate: ");
        sb.append(this.getRevocationDate());
        sb.append(lineSeparator);
        sb.append("       certificateIssuer: ");
        sb.append(this.getCertificateIssuer());
        sb.append(lineSeparator);
        final Extensions extensions = this.c.getExtensions();
        while (true) {
            Label_0328: {
                if (extensions == null) {
                    break Label_0328;
                }
                final Enumeration oids = extensions.oids();
                if (!oids.hasMoreElements()) {
                    break Label_0328;
                }
                String str = "   crlEntryExtensions:";
                ASN1ObjectIdentifier asn1ObjectIdentifier;
                Extension extension;
                ASN1InputStream asn1InputStream;
                ASN1Object obj;
                Label_0118_Outer:Label_0124_Outer:
                while (true) {
                    sb.append(str);
                    while (true) {
                        while (true) {
                            sb.append(lineSeparator);
                            if (!oids.hasMoreElements()) {
                                break Label_0328;
                            }
                            asn1ObjectIdentifier = oids.nextElement();
                            extension = extensions.getExtension(asn1ObjectIdentifier);
                            if (extension.getExtnValue() == null) {
                                continue Label_0124_Outer;
                            }
                            break;
                        }
                        asn1InputStream = new ASN1InputStream(extension.getExtnValue().getOctets());
                        sb.append("                       critical(");
                        sb.append(extension.isCritical());
                        sb.append(") ");
                        try {
                            Label_0229: {
                                if (asn1ObjectIdentifier.equals(org.bouncycastle.asn1.x509.X509Extension.reasonCode)) {
                                    obj = CRLReason.getInstance(ASN1Enumerated.getInstance(asn1InputStream.readObject()));
                                }
                                else {
                                    if (!asn1ObjectIdentifier.equals(org.bouncycastle.asn1.x509.X509Extension.certificateIssuer)) {
                                        sb.append(asn1ObjectIdentifier.getId());
                                        sb.append(" value = ");
                                        sb.append(ASN1Dump.dumpAsString(asn1InputStream.readObject()));
                                        break Label_0229;
                                    }
                                    sb.append("Certificate issuer: ");
                                    obj = GeneralNames.getInstance(asn1InputStream.readObject());
                                }
                                sb.append(obj);
                            }
                            sb.append(lineSeparator);
                            continue;
                            return sb.toString();
                            sb.append(asn1ObjectIdentifier.getId());
                            sb.append(" value = ");
                            str = "*****";
                            continue Label_0118_Outer;
                        }
                        catch (Exception ex) {}
                        break;
                    }
                    break;
                }
            }
            continue;
        }
    }
}
