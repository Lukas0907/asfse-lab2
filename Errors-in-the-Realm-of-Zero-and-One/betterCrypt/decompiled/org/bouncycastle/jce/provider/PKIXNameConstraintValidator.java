// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x509.NameConstraintValidatorException;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralSubtree;

public class PKIXNameConstraintValidator
{
    org.bouncycastle.asn1.x509.PKIXNameConstraintValidator validator;
    
    public PKIXNameConstraintValidator() {
        this.validator = new org.bouncycastle.asn1.x509.PKIXNameConstraintValidator();
    }
    
    public void addExcludedSubtree(final GeneralSubtree generalSubtree) {
        this.validator.addExcludedSubtree(generalSubtree);
    }
    
    public void checkExcluded(final GeneralName generalName) throws PKIXNameConstraintValidatorException {
        try {
            this.validator.checkExcluded(generalName);
        }
        catch (NameConstraintValidatorException ex) {
            throw new PKIXNameConstraintValidatorException(ex.getMessage(), ex);
        }
    }
    
    public void checkExcludedDN(final ASN1Sequence asn1Sequence) throws PKIXNameConstraintValidatorException {
        try {
            this.validator.checkExcludedDN(X500Name.getInstance(asn1Sequence));
        }
        catch (NameConstraintValidatorException ex) {
            throw new PKIXNameConstraintValidatorException(ex.getMessage(), ex);
        }
    }
    
    public void checkPermitted(final GeneralName generalName) throws PKIXNameConstraintValidatorException {
        try {
            this.validator.checkPermitted(generalName);
        }
        catch (NameConstraintValidatorException ex) {
            throw new PKIXNameConstraintValidatorException(ex.getMessage(), ex);
        }
    }
    
    public void checkPermittedDN(final ASN1Sequence asn1Sequence) throws PKIXNameConstraintValidatorException {
        try {
            this.validator.checkPermittedDN(X500Name.getInstance(asn1Sequence));
        }
        catch (NameConstraintValidatorException ex) {
            throw new PKIXNameConstraintValidatorException(ex.getMessage(), ex);
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof PKIXNameConstraintValidator && this.validator.equals(((PKIXNameConstraintValidator)o).validator);
    }
    
    @Override
    public int hashCode() {
        return this.validator.hashCode();
    }
    
    public void intersectEmptyPermittedSubtree(final int n) {
        this.validator.intersectEmptyPermittedSubtree(n);
    }
    
    public void intersectPermittedSubtree(final GeneralSubtree generalSubtree) {
        this.validator.intersectPermittedSubtree(generalSubtree);
    }
    
    public void intersectPermittedSubtree(final GeneralSubtree[] array) {
        this.validator.intersectPermittedSubtree(array);
    }
    
    @Override
    public String toString() {
        return this.validator.toString();
    }
}
