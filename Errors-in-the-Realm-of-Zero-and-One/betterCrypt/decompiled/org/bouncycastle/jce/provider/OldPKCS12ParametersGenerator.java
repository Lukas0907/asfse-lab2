// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.digests.RIPEMD160Digest;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.digests.MD5Digest;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.PBEParametersGenerator;

class OldPKCS12ParametersGenerator extends PBEParametersGenerator
{
    public static final int IV_MATERIAL = 2;
    public static final int KEY_MATERIAL = 1;
    public static final int MAC_MATERIAL = 3;
    private Digest digest;
    private int u;
    private int v;
    
    public OldPKCS12ParametersGenerator(final Digest digest) {
        this.digest = digest;
        if (digest instanceof MD5Digest) {
            this.u = 16;
        }
        else {
            if (!(digest instanceof SHA1Digest) && !(digest instanceof RIPEMD160Digest)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Digest ");
                sb.append(digest.getAlgorithmName());
                sb.append(" unsupported");
                throw new IllegalArgumentException(sb.toString());
            }
            this.u = 20;
        }
        this.v = 64;
    }
    
    private void adjust(final byte[] array, final int n, final byte[] array2) {
        final int n2 = (array2[array2.length - 1] & 0xFF) + (array[array2.length + n - 1] & 0xFF) + 1;
        array[array2.length + n - 1] = (byte)n2;
        int n3 = n2 >>> 8;
        for (int i = array2.length - 2; i >= 0; --i) {
            final byte b = array2[i];
            final int n4 = n + i;
            final int n5 = n3 + ((b & 0xFF) + (array[n4] & 0xFF));
            array[n4] = (byte)n5;
            n3 = n5 >>> 8;
        }
    }
    
    private byte[] generateDerivedKey(int i, int j) {
        final byte[] array = new byte[this.v];
        final byte[] array2 = new byte[j];
        for (int k = 0; k != array.length; ++k) {
            array[k] = (byte)i;
        }
        byte[] array4;
        if (this.salt != null && this.salt.length != 0) {
            i = this.v;
            final int length = this.salt.length;
            final int v = this.v;
            final byte[] array3 = new byte[i * ((length + v - 1) / v)];
            i = 0;
            while (true) {
                array4 = array3;
                if (i == array3.length) {
                    break;
                }
                array3[i] = this.salt[i % this.salt.length];
                ++i;
            }
        }
        else {
            array4 = new byte[0];
        }
        byte[] array6;
        if (this.password != null && this.password.length != 0) {
            i = this.v;
            final int length2 = this.password.length;
            final int v2 = this.v;
            final byte[] array5 = new byte[i * ((length2 + v2 - 1) / v2)];
            i = 0;
            while (true) {
                array6 = array5;
                if (i == array5.length) {
                    break;
                }
                array5[i] = this.password[i % this.password.length];
                ++i;
            }
        }
        else {
            array6 = new byte[0];
        }
        final byte[] array7 = new byte[array4.length + array6.length];
        System.arraycopy(array4, 0, array7, 0, array4.length);
        System.arraycopy(array6, 0, array7, array4.length, array6.length);
        final byte[] array8 = new byte[this.v];
        int n;
        byte[] array9;
        int length3;
        int v3;
        int u;
        for (i = this.u, n = (j + i - 1) / i, i = 1; i <= n; ++i) {
            array9 = new byte[this.u];
            this.digest.update(array, 0, array.length);
            this.digest.update(array7, 0, array7.length);
            this.digest.doFinal(array9, 0);
            for (j = 1; j != this.iterationCount; ++j) {
                this.digest.update(array9, 0, array9.length);
                this.digest.doFinal(array9, 0);
            }
            for (j = 0; j != array8.length; ++j) {
                array8[i] = array9[j % array9.length];
            }
            j = 0;
            while (true) {
                length3 = array7.length;
                v3 = this.v;
                if (j == length3 / v3) {
                    break;
                }
                this.adjust(array7, v3 * j, array8);
                ++j;
            }
            j = i - 1;
            u = this.u;
            if (i == n) {
                System.arraycopy(array9, 0, array2, j * u, array2.length - j * u);
            }
            else {
                System.arraycopy(array9, 0, array2, j * u, array9.length);
            }
        }
        return array2;
    }
    
    @Override
    public CipherParameters generateDerivedMacParameters(int n) {
        n /= 8;
        return new KeyParameter(this.generateDerivedKey(3, n), 0, n);
    }
    
    @Override
    public CipherParameters generateDerivedParameters(int n) {
        n /= 8;
        return new KeyParameter(this.generateDerivedKey(1, n), 0, n);
    }
    
    @Override
    public CipherParameters generateDerivedParameters(int n, int n2) {
        n /= 8;
        n2 /= 8;
        return new ParametersWithIV(new KeyParameter(this.generateDerivedKey(1, n), 0, n), this.generateDerivedKey(2, n2), 0, n2);
    }
}
