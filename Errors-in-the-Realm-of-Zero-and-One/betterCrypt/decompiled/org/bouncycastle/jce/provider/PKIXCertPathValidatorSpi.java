// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

import java.util.Iterator;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x500.X500Name;
import java.security.cert.TrustAnchor;
import java.security.cert.PKIXCertPathValidatorResult;
import org.bouncycastle.asn1.x509.Extension;
import java.util.Collection;
import java.security.cert.CertPathValidatorException;
import java.security.cert.PKIXCertPathChecker;
import org.bouncycastle.jce.exception.ExtCertPathValidatorException;
import java.security.cert.Certificate;
import java.security.PublicKey;
import java.security.cert.PolicyNode;
import java.util.Set;
import java.util.List;
import java.util.HashSet;
import java.util.ArrayList;
import java.security.InvalidAlgorithmParameterException;
import org.bouncycastle.jcajce.PKIXExtendedBuilderParameters;
import org.bouncycastle.x509.ExtendedPKIXParameters;
import org.bouncycastle.jcajce.PKIXExtendedParameters;
import java.security.cert.PKIXParameters;
import java.security.cert.CertPathValidatorResult;
import java.security.cert.CertPathParameters;
import java.security.cert.CertPath;
import java.security.cert.CertificateEncodingException;
import org.bouncycastle.asn1.x509.TBSCertificate;
import org.bouncycastle.jcajce.interfaces.BCX509Certificate;
import java.security.cert.X509Certificate;
import org.bouncycastle.jcajce.util.BCJcaJceHelper;
import org.bouncycastle.jcajce.util.JcaJceHelper;
import java.security.cert.CertPathValidatorSpi;

public class PKIXCertPathValidatorSpi extends CertPathValidatorSpi
{
    private final JcaJceHelper helper;
    private final boolean isForCRLCheck;
    
    public PKIXCertPathValidatorSpi() {
        this(false);
    }
    
    public PKIXCertPathValidatorSpi(final boolean isForCRLCheck) {
        this.helper = new BCJcaJceHelper();
        this.isForCRLCheck = isForCRLCheck;
    }
    
    static void checkCertificate(X509Certificate x509Certificate) throws AnnotatedException {
        if (x509Certificate instanceof BCX509Certificate) {
            final X509Certificate x509Certificate2 = null;
            try {
                final TBSCertificate tbsCertificateNative = ((BCX509Certificate)x509Certificate).getTBSCertificateNative();
                x509Certificate = x509Certificate2;
                if (tbsCertificateNative != null) {
                    return;
                }
            }
            catch (RuntimeException ex3) {}
            throw new AnnotatedException("unable to process TBSCertificate", (Throwable)x509Certificate);
        }
        try {
            TBSCertificate.getInstance(x509Certificate.getTBSCertificate());
        }
        catch (IllegalArgumentException ex) {
            throw new AnnotatedException(ex.getMessage());
        }
        catch (CertificateEncodingException ex2) {
            throw new AnnotatedException("unable to process TBSCertificate", ex2);
        }
    }
    
    @Override
    public CertPathValidatorResult engineValidate(final CertPath certPath, CertPathParameters ex) throws CertPathValidatorException, InvalidAlgorithmParameterException {
        if (ex instanceof PKIXParameters) {
            final PKIXExtendedParameters.Builder builder = new PKIXExtendedParameters.Builder((PKIXParameters)ex);
            if (ex instanceof ExtendedPKIXParameters) {
                final ExtendedPKIXParameters extendedPKIXParameters = (ExtendedPKIXParameters)ex;
                builder.setUseDeltasEnabled(extendedPKIXParameters.isUseDeltasEnabled());
                builder.setValidityModel(extendedPKIXParameters.getValidityModel());
            }
            ex = (RuntimeException)builder.build();
        }
        else if (ex instanceof PKIXExtendedBuilderParameters) {
            ex = (RuntimeException)((PKIXExtendedBuilderParameters)ex).getBaseParameters();
        }
        else {
            if (!(ex instanceof PKIXExtendedParameters)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Parameters must be a ");
                sb.append(PKIXParameters.class.getName());
                sb.append(" instance.");
                throw new InvalidAlgorithmParameterException(sb.toString());
            }
            ex = ex;
        }
        if (((PKIXExtendedParameters)ex).getTrustAnchors() == null) {
            throw new InvalidAlgorithmParameterException("trustAnchors is null, this is not allowed for certification path validation.");
        }
        final List<X509Certificate> certificates;
        Object o = certificates = (List<X509Certificate>)certPath.getCertificates();
        final int size = certificates.size();
        if (!certificates.isEmpty()) {
            final Set initialPolicies = ((PKIXExtendedParameters)ex).getInitialPolicies();
            try {
                final TrustAnchor trustAnchor = CertPathValidatorUtilities.findTrustAnchor(certificates.get(certificates.size() - 1), ((PKIXExtendedParameters)ex).getTrustAnchors(), ((PKIXExtendedParameters)ex).getSigProvider());
                if (trustAnchor != null) {
                    checkCertificate(trustAnchor.getTrustedCert());
                    final PKIXExtendedParameters build = new PKIXExtendedParameters.Builder((PKIXExtendedParameters)ex).setTrustAnchor(trustAnchor).build();
                    int n = size + 1;
                    final ArrayList[] array = new ArrayList[n];
                    for (int i = 0; i < array.length; ++i) {
                        array[i] = new ArrayList();
                    }
                    ex = (RuntimeException)new HashSet();
                    ((Set<String>)ex).add("2.5.29.32.0");
                    final PKIXPolicyNode pkixPolicyNode = new PKIXPolicyNode(new ArrayList(), 0, (Set)ex, null, new HashSet(), "2.5.29.32.0", false);
                    array[0].add(pkixPolicyNode);
                    final PKIXNameConstraintValidator pkixNameConstraintValidator = new PKIXNameConstraintValidator();
                    final HashSet set = new HashSet();
                    int n2;
                    if (build.isExplicitPolicyRequired()) {
                        n2 = 0;
                    }
                    else {
                        n2 = n;
                    }
                    int n3;
                    if (build.isAnyPolicyInhibited()) {
                        n3 = 0;
                    }
                    else {
                        n3 = n;
                    }
                    if (build.isPolicyMappingInhibited()) {
                        n = 0;
                    }
                    X509Certificate trustedCert = trustAnchor.getTrustedCert();
                    Label_0387: {
                        if (trustedCert == null) {
                            break Label_0387;
                        }
                        try {
                            X500Name x500Name = PrincipalUtils.getSubjectPrincipal(trustedCert);
                            o = trustedCert.getPublicKey();
                            while (true) {
                                try {
                                    final AlgorithmIdentifier algorithmIdentifier = CertPathValidatorUtilities.getAlgorithmIdentifier((PublicKey)o);
                                    algorithmIdentifier.getAlgorithm();
                                    algorithmIdentifier.getParameters();
                                    if (build.getTargetConstraints() != null && !build.getTargetConstraints().match(certificates.get(0))) {
                                        throw new ExtCertPathValidatorException("Target certificate in certification path does not match targetConstraints.", null, certPath, 0);
                                    }
                                    final List certPathCheckers = build.getCertPathCheckers();
                                    final Iterator<PKIXCertPathChecker> iterator = certPathCheckers.iterator();
                                    while (iterator.hasNext()) {
                                        iterator.next().init(false);
                                    }
                                    final int size2 = certificates.size();
                                    final int n4 = n;
                                    int n5 = size;
                                    final int n6 = n3;
                                    int j = size2 - 1;
                                    final X509Certificate x509Certificate = null;
                                    int prepareNextCertI2 = n4;
                                    PublicKey nextWorkingKey = (PublicKey)o;
                                    X500Name x500Name2 = x500Name;
                                    PKIXPolicyNode pkixPolicyNode2 = pkixPolicyNode;
                                    o = initialPolicies;
                                    final TrustAnchor trustAnchor2 = trustAnchor;
                                    final ArrayList[] array2 = array;
                                    final PKIXNameConstraintValidator pkixNameConstraintValidator2 = pkixNameConstraintValidator;
                                    int n7 = n2;
                                    X509Certificate x509Certificate2 = x509Certificate;
                                    int n8 = n6;
                                    while (j >= 0) {
                                        final int n9 = size - j;
                                        x509Certificate2 = certificates.get(j);
                                        boolean b;
                                        if (j == certificates.size() - 1) {
                                            b = true;
                                        }
                                        else {
                                            b = false;
                                        }
                                        try {
                                            checkCertificate(x509Certificate2);
                                            final JcaJceHelper helper = this.helper;
                                            final int n10 = n7;
                                            RFC3280CertPathUtilities.processCertA(certPath, build, j, nextWorkingKey, b, x500Name2, trustedCert, helper);
                                            RFC3280CertPathUtilities.processCertBC(certPath, j, pkixNameConstraintValidator2, this.isForCRLCheck);
                                            final PKIXPolicyNode processCertE = RFC3280CertPathUtilities.processCertE(certPath, j, RFC3280CertPathUtilities.processCertD(certPath, j, set, pkixPolicyNode2, array2, n8, this.isForCRLCheck));
                                            RFC3280CertPathUtilities.processCertF(certPath, j, processCertE, n10);
                                            int prepareNextCertJ = 0;
                                            int n11 = 0;
                                            int n12 = 0;
                                            Label_1130: {
                                                if (n9 != size) {
                                                    if (x509Certificate2 != null && x509Certificate2.getVersion() == 1) {
                                                        if (n9 != 1 || !x509Certificate2.equals(trustAnchor2.getTrustedCert())) {
                                                            throw new CertPathValidatorException("Version 1 certificates can't be used as CA ones.", null, certPath, j);
                                                        }
                                                    }
                                                    else {
                                                        final X509Certificate x509Certificate3 = x509Certificate2;
                                                        RFC3280CertPathUtilities.prepareNextCertA(certPath, j);
                                                        final PKIXPolicyNode prepareCertB = RFC3280CertPathUtilities.prepareCertB(certPath, j, array2, processCertE, prepareNextCertI2);
                                                        RFC3280CertPathUtilities.prepareNextCertG(certPath, j, pkixNameConstraintValidator2);
                                                        final int prepareNextCertH1 = RFC3280CertPathUtilities.prepareNextCertH1(certPath, j, n10);
                                                        final int prepareNextCertH2 = RFC3280CertPathUtilities.prepareNextCertH2(certPath, j, prepareNextCertI2);
                                                        final int prepareNextCertH3 = RFC3280CertPathUtilities.prepareNextCertH3(certPath, j, n8);
                                                        final int prepareNextCertI3 = RFC3280CertPathUtilities.prepareNextCertI1(certPath, j, prepareNextCertH1);
                                                        prepareNextCertI2 = RFC3280CertPathUtilities.prepareNextCertI2(certPath, j, prepareNextCertH2);
                                                        prepareNextCertJ = RFC3280CertPathUtilities.prepareNextCertJ(certPath, j, prepareNextCertH3);
                                                        RFC3280CertPathUtilities.prepareNextCertK(certPath, j);
                                                        final int prepareNextCertM = RFC3280CertPathUtilities.prepareNextCertM(certPath, j, RFC3280CertPathUtilities.prepareNextCertL(certPath, j, n5));
                                                        RFC3280CertPathUtilities.prepareNextCertN(certPath, j);
                                                        final Set<String> criticalExtensionOIDs = x509Certificate3.getCriticalExtensionOIDs();
                                                        HashSet set2;
                                                        if (criticalExtensionOIDs != null) {
                                                            set2 = new HashSet(criticalExtensionOIDs);
                                                            set2.remove(RFC3280CertPathUtilities.KEY_USAGE);
                                                            set2.remove(RFC3280CertPathUtilities.CERTIFICATE_POLICIES);
                                                            set2.remove(RFC3280CertPathUtilities.POLICY_MAPPINGS);
                                                            set2.remove(RFC3280CertPathUtilities.INHIBIT_ANY_POLICY);
                                                            set2.remove(RFC3280CertPathUtilities.ISSUING_DISTRIBUTION_POINT);
                                                            set2.remove(RFC3280CertPathUtilities.DELTA_CRL_INDICATOR);
                                                            set2.remove(RFC3280CertPathUtilities.POLICY_CONSTRAINTS);
                                                            set2.remove(RFC3280CertPathUtilities.BASIC_CONSTRAINTS);
                                                            set2.remove(RFC3280CertPathUtilities.SUBJECT_ALTERNATIVE_NAME);
                                                            set2.remove(RFC3280CertPathUtilities.NAME_CONSTRAINTS);
                                                        }
                                                        else {
                                                            set2 = new HashSet();
                                                        }
                                                        RFC3280CertPathUtilities.prepareNextCertO(certPath, j, set2, certPathCheckers);
                                                        final X500Name subjectPrincipal = PrincipalUtils.getSubjectPrincipal(x509Certificate3);
                                                        try {
                                                            nextWorkingKey = CertPathValidatorUtilities.getNextWorkingKey(certPath.getCertificates(), j, this.helper);
                                                            final AlgorithmIdentifier algorithmIdentifier2 = CertPathValidatorUtilities.getAlgorithmIdentifier(nextWorkingKey);
                                                            algorithmIdentifier2.getAlgorithm();
                                                            algorithmIdentifier2.getParameters();
                                                            final X509Certificate x509Certificate4 = x509Certificate3;
                                                            n11 = prepareNextCertI3;
                                                            n12 = prepareNextCertM;
                                                            pkixPolicyNode2 = prepareCertB;
                                                            trustedCert = x509Certificate4;
                                                            x500Name2 = subjectPrincipal;
                                                            break Label_1130;
                                                        }
                                                        catch (CertPathValidatorException cause) {
                                                            throw new CertPathValidatorException("Next working key could not be retrieved.", cause, certPath, j);
                                                        }
                                                    }
                                                }
                                                prepareNextCertJ = n8;
                                                pkixPolicyNode2 = processCertE;
                                                n12 = n5;
                                                n11 = n10;
                                            }
                                            final int n13 = n12;
                                            --j;
                                            n8 = prepareNextCertJ;
                                            n7 = n11;
                                            n5 = n13;
                                            continue;
                                        }
                                        catch (AnnotatedException ex2) {
                                            throw new CertPathValidatorException(ex2.getMessage(), ex2.getUnderlyingException(), certPath, j);
                                        }
                                        break;
                                    }
                                    final int wrapupCertA = RFC3280CertPathUtilities.wrapupCertA(n7, x509Certificate2);
                                    final int n14 = j + 1;
                                    final int wrapupCertB = RFC3280CertPathUtilities.wrapupCertB(certPath, n14, wrapupCertA);
                                    final Set<String> criticalExtensionOIDs2 = x509Certificate2.getCriticalExtensionOIDs();
                                    HashSet set3;
                                    if (criticalExtensionOIDs2 != null) {
                                        set3 = new HashSet(criticalExtensionOIDs2);
                                        set3.remove(RFC3280CertPathUtilities.KEY_USAGE);
                                        set3.remove(RFC3280CertPathUtilities.CERTIFICATE_POLICIES);
                                        set3.remove(RFC3280CertPathUtilities.POLICY_MAPPINGS);
                                        set3.remove(RFC3280CertPathUtilities.INHIBIT_ANY_POLICY);
                                        set3.remove(RFC3280CertPathUtilities.ISSUING_DISTRIBUTION_POINT);
                                        set3.remove(RFC3280CertPathUtilities.DELTA_CRL_INDICATOR);
                                        set3.remove(RFC3280CertPathUtilities.POLICY_CONSTRAINTS);
                                        set3.remove(RFC3280CertPathUtilities.BASIC_CONSTRAINTS);
                                        set3.remove(RFC3280CertPathUtilities.SUBJECT_ALTERNATIVE_NAME);
                                        set3.remove(RFC3280CertPathUtilities.NAME_CONSTRAINTS);
                                        set3.remove(RFC3280CertPathUtilities.CRL_DISTRIBUTION_POINTS);
                                        set3.remove(Extension.extendedKeyUsage.getId());
                                    }
                                    else {
                                        set3 = new HashSet();
                                    }
                                    RFC3280CertPathUtilities.wrapupCertF(certPath, n14, certPathCheckers, set3);
                                    final PKIXPolicyNode wrapupCertG = RFC3280CertPathUtilities.wrapupCertG(certPath, build, (Set)o, n14, array2, pkixPolicyNode2, set);
                                    if (wrapupCertB <= 0 && wrapupCertG == null) {
                                        throw new CertPathValidatorException("Path processing failed on policy.", null, certPath, j);
                                    }
                                    return new PKIXCertPathValidatorResult(trustAnchor2, wrapupCertG, x509Certificate2.getPublicKey());
                                }
                                catch (CertPathValidatorException ex3) {
                                    throw new ExtCertPathValidatorException("Algorithm identifier of public key of trust anchor could not be read.", ex3, certPath, -1);
                                }
                                x500Name = PrincipalUtils.getCA(trustAnchor);
                                o = trustAnchor.getCAPublicKey();
                                continue;
                            }
                        }
                        catch (RuntimeException ex) {
                            throw new ExtCertPathValidatorException("Subject of trust anchor could not be (re)encoded.", ex, certPath, -1);
                        }
                    }
                }
                try {
                    throw new CertPathValidatorException("Trust anchor for certification path not found.", null, certPath, -1);
                }
                catch (AnnotatedException ex) {}
            }
            catch (AnnotatedException ex4) {}
            throw new CertPathValidatorException(ex.getMessage(), ((AnnotatedException)ex).getUnderlyingException(), certPath, ((List)o).size() - 1);
        }
        throw new CertPathValidatorException("Certification path is empty.", null, certPath, -1);
    }
}
