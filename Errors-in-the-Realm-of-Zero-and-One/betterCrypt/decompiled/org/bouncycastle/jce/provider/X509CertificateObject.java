// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

import java.security.NoSuchProviderException;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.misc.VerisignCzagExtension;
import org.bouncycastle.asn1.util.ASN1Dump;
import org.bouncycastle.asn1.misc.NetscapeRevocationURL;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.misc.NetscapeCertType;
import org.bouncycastle.asn1.misc.MiscObjectIdentifiers;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.util.Strings;
import java.security.Provider;
import java.security.Security;
import java.math.BigInteger;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.jce.X509Principal;
import java.security.Principal;
import java.util.Collections;
import java.util.ArrayList;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import java.util.List;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.util.Arrays;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateExpiredException;
import java.util.Date;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.util.Collection;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.Signature;
import java.security.PublicKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateParsingException;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.jcajce.provider.asymmetric.util.PKCS12BagAttributeCarrierImpl;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.jce.interfaces.PKCS12BagAttributeCarrier;
import java.security.cert.X509Certificate;

public class X509CertificateObject extends X509Certificate implements PKCS12BagAttributeCarrier
{
    private PKCS12BagAttributeCarrier attrCarrier;
    private BasicConstraints basicConstraints;
    private org.bouncycastle.asn1.x509.Certificate c;
    private int hashValue;
    private boolean hashValueSet;
    private boolean[] keyUsage;
    
    public X509CertificateObject(final org.bouncycastle.asn1.x509.Certificate c) throws CertificateParsingException {
    Label_0101_Outer:
        while (true) {
            this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
            this.c = c;
            while (true) {
                int n3 = 0;
                boolean[] keyUsage = null;
                boolean b = false;
                Label_0226: {
                Label_0223:
                    while (true) {
                        int n = 0;
                        Label_0218: {
                            try {
                                final byte[] extensionBytes = this.getExtensionBytes("2.5.29.19");
                                if (extensionBytes != null) {
                                    this.basicConstraints = BasicConstraints.getInstance(ASN1Primitive.fromByteArray(extensionBytes));
                                }
                                try {
                                    final byte[] extensionBytes2 = this.getExtensionBytes("2.5.29.15");
                                    if (extensionBytes2 != null) {
                                        final DERBitString instance = DERBitString.getInstance(ASN1Primitive.fromByteArray(extensionBytes2));
                                        final byte[] bytes = instance.getBytes();
                                        n = bytes.length * 8 - instance.getPadBits();
                                        final int n2 = 9;
                                        if (n >= 9) {
                                            break Label_0218;
                                        }
                                        this.keyUsage = new boolean[n2];
                                        n3 = 0;
                                        if (n3 != n) {
                                            keyUsage = this.keyUsage;
                                            if ((bytes[n3 / 8] & 128 >>> n3 % 8) != 0x0) {
                                                b = true;
                                                break Label_0226;
                                            }
                                            break Label_0223;
                                        }
                                    }
                                    else {
                                        this.keyUsage = null;
                                    }
                                    return;
                                }
                                catch (Exception obj) {
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append("cannot construct KeyUsage: ");
                                    sb.append(obj);
                                    throw new CertificateParsingException(sb.toString());
                                }
                            }
                            catch (Exception obj2) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("cannot construct BasicConstraints: ");
                                sb2.append(obj2);
                                throw new CertificateParsingException(sb2.toString());
                            }
                        }
                        final int n2 = n;
                        continue Label_0101_Outer;
                    }
                    b = false;
                }
                keyUsage[n3] = b;
                ++n3;
                continue;
            }
        }
    }
    
    private int calculateHashCode() {
        try {
            final byte[] encoded = this.getEncoded();
            int i = 1;
            int n = 0;
            while (i < encoded.length) {
                n += encoded[i] * i;
                ++i;
            }
            return n;
        }
        catch (CertificateEncodingException ex) {
            return 0;
        }
    }
    
    private void checkSignature(final PublicKey publicKey, final Signature signature) throws CertificateException, NoSuchAlgorithmException, SignatureException, InvalidKeyException {
        if (!this.isAlgIdEqual(this.c.getSignatureAlgorithm(), this.c.getTBSCertificate().getSignature())) {
            throw new CertificateException("signature algorithm in TBS cert not same as outer cert");
        }
        X509SignatureUtil.setSignatureParameters(signature, this.c.getSignatureAlgorithm().getParameters());
        signature.initVerify(publicKey);
        signature.update(this.getTBSCertificate());
        if (signature.verify(this.getSignature())) {
            return;
        }
        throw new SignatureException("certificate does not verify with supplied key");
    }
    
    private static Collection getAlternativeNames(final byte[] p0) throws CertificateParsingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: ifnonnull       6
        //     4: aconst_null    
        //     5: areturn        
        //     6: new             Ljava/util/ArrayList;
        //     9: dup            
        //    10: invokespecial   java/util/ArrayList.<init>:()V
        //    13: astore_1       
        //    14: aload_0        
        //    15: invokestatic    org/bouncycastle/asn1/ASN1Sequence.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/ASN1Sequence;
        //    18: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.getObjects:()Ljava/util/Enumeration;
        //    21: astore_2       
        //    22: aload_2        
        //    23: invokeinterface java/util/Enumeration.hasMoreElements:()Z
        //    28: ifeq            258
        //    31: aload_2        
        //    32: invokeinterface java/util/Enumeration.nextElement:()Ljava/lang/Object;
        //    37: invokestatic    org/bouncycastle/asn1/x509/GeneralName.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/GeneralName;
        //    40: astore_0       
        //    41: new             Ljava/util/ArrayList;
        //    44: dup            
        //    45: invokespecial   java/util/ArrayList.<init>:()V
        //    48: astore_3       
        //    49: aload_3        
        //    50: aload_0        
        //    51: invokevirtual   org/bouncycastle/asn1/x509/GeneralName.getTagNo:()I
        //    54: invokestatic    org/bouncycastle/util/Integers.valueOf:(I)Ljava/lang/Integer;
        //    57: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //    62: pop            
        //    63: aload_0        
        //    64: invokevirtual   org/bouncycastle/asn1/x509/GeneralName.getTagNo:()I
        //    67: tableswitch {
        //                0: 196
        //                1: 180
        //                2: 180
        //                3: 196
        //                4: 163
        //                5: 196
        //                6: 180
        //                7: 141
        //                8: 119
        //          default: 293
        //        }
        //   116: goto            221
        //   119: aload_0        
        //   120: invokevirtual   org/bouncycastle/asn1/x509/GeneralName.getName:()Lorg/bouncycastle/asn1/ASN1Encodable;
        //   123: invokestatic    org/bouncycastle/asn1/ASN1ObjectIdentifier.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //   126: invokevirtual   org/bouncycastle/asn1/ASN1ObjectIdentifier.getId:()Ljava/lang/String;
        //   129: astore_0       
        //   130: aload_3        
        //   131: aload_0        
        //   132: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   137: pop            
        //   138: goto            207
        //   141: aload_0        
        //   142: invokevirtual   org/bouncycastle/asn1/x509/GeneralName.getName:()Lorg/bouncycastle/asn1/ASN1Encodable;
        //   145: invokestatic    org/bouncycastle/asn1/DEROctetString.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/ASN1OctetString;
        //   148: invokevirtual   org/bouncycastle/asn1/ASN1OctetString.getOctets:()[B
        //   151: astore_0       
        //   152: aload_0        
        //   153: invokestatic    java/net/InetAddress.getByAddress:([B)Ljava/net/InetAddress;
        //   156: invokevirtual   java/net/InetAddress.getHostAddress:()Ljava/lang/String;
        //   159: astore_0       
        //   160: goto            130
        //   163: getstatic       org/bouncycastle/asn1/x500/style/RFC4519Style.INSTANCE:Lorg/bouncycastle/asn1/x500/X500NameStyle;
        //   166: aload_0        
        //   167: invokevirtual   org/bouncycastle/asn1/x509/GeneralName.getName:()Lorg/bouncycastle/asn1/ASN1Encodable;
        //   170: invokestatic    org/bouncycastle/asn1/x500/X500Name.getInstance:(Lorg/bouncycastle/asn1/x500/X500NameStyle;Ljava/lang/Object;)Lorg/bouncycastle/asn1/x500/X500Name;
        //   173: invokevirtual   org/bouncycastle/asn1/x500/X500Name.toString:()Ljava/lang/String;
        //   176: astore_0       
        //   177: goto            130
        //   180: aload_0        
        //   181: invokevirtual   org/bouncycastle/asn1/x509/GeneralName.getName:()Lorg/bouncycastle/asn1/ASN1Encodable;
        //   184: checkcast       Lorg/bouncycastle/asn1/ASN1String;
        //   187: invokeinterface org/bouncycastle/asn1/ASN1String.getString:()Ljava/lang/String;
        //   192: astore_0       
        //   193: goto            130
        //   196: aload_3        
        //   197: aload_0        
        //   198: invokevirtual   org/bouncycastle/asn1/x509/GeneralName.getEncoded:()[B
        //   201: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   206: pop            
        //   207: aload_1        
        //   208: aload_3        
        //   209: invokestatic    java/util/Collections.unmodifiableList:(Ljava/util/List;)Ljava/util/List;
        //   212: invokeinterface java/util/Collection.add:(Ljava/lang/Object;)Z
        //   217: pop            
        //   218: goto            22
        //   221: new             Ljava/lang/StringBuilder;
        //   224: dup            
        //   225: invokespecial   java/lang/StringBuilder.<init>:()V
        //   228: astore_1       
        //   229: aload_1        
        //   230: ldc_w           "Bad tag number: "
        //   233: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   236: pop            
        //   237: aload_1        
        //   238: aload_0        
        //   239: invokevirtual   org/bouncycastle/asn1/x509/GeneralName.getTagNo:()I
        //   242: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   245: pop            
        //   246: new             Ljava/io/IOException;
        //   249: dup            
        //   250: aload_1        
        //   251: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   254: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   257: athrow         
        //   258: aload_1        
        //   259: invokeinterface java/util/Collection.size:()I
        //   264: ifne            269
        //   267: aconst_null    
        //   268: areturn        
        //   269: aload_1        
        //   270: invokestatic    java/util/Collections.unmodifiableCollection:(Ljava/util/Collection;)Ljava/util/Collection;
        //   273: astore_0       
        //   274: aload_0        
        //   275: areturn        
        //   276: astore_0       
        //   277: new             Ljava/security/cert/CertificateParsingException;
        //   280: dup            
        //   281: aload_0        
        //   282: invokevirtual   java/lang/Exception.getMessage:()Ljava/lang/String;
        //   285: invokespecial   java/security/cert/CertificateParsingException.<init>:(Ljava/lang/String;)V
        //   288: athrow         
        //   289: astore_0       
        //   290: goto            22
        //   293: goto            116
        //    Exceptions:
        //  throws java.security.cert.CertificateParsingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                           
        //  -----  -----  -----  -----  -------------------------------
        //  6      22     276    289    Ljava/lang/Exception;
        //  22     116    276    289    Ljava/lang/Exception;
        //  119    130    276    289    Ljava/lang/Exception;
        //  130    138    276    289    Ljava/lang/Exception;
        //  141    152    276    289    Ljava/lang/Exception;
        //  152    160    289    293    Ljava/net/UnknownHostException;
        //  152    160    276    289    Ljava/lang/Exception;
        //  163    177    276    289    Ljava/lang/Exception;
        //  180    193    276    289    Ljava/lang/Exception;
        //  196    207    276    289    Ljava/lang/Exception;
        //  207    218    276    289    Ljava/lang/Exception;
        //  221    258    276    289    Ljava/lang/Exception;
        //  258    267    276    289    Ljava/lang/Exception;
        //  269    274    276    289    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0163:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private byte[] getExtensionBytes(final String s) {
        final Extensions extensions = this.c.getTBSCertificate().getExtensions();
        if (extensions != null) {
            final Extension extension = extensions.getExtension(new ASN1ObjectIdentifier(s));
            if (extension != null) {
                return extension.getExtnValue().getOctets();
            }
        }
        return null;
    }
    
    private boolean isAlgIdEqual(final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2) {
        if (!algorithmIdentifier.getAlgorithm().equals(algorithmIdentifier2.getAlgorithm())) {
            return false;
        }
        if (algorithmIdentifier.getParameters() == null) {
            return algorithmIdentifier2.getParameters() == null || algorithmIdentifier2.getParameters().equals(DERNull.INSTANCE);
        }
        if (algorithmIdentifier2.getParameters() == null) {
            return algorithmIdentifier.getParameters() == null || algorithmIdentifier.getParameters().equals(DERNull.INSTANCE);
        }
        return algorithmIdentifier.getParameters().equals(algorithmIdentifier2.getParameters());
    }
    
    @Override
    public void checkValidity() throws CertificateExpiredException, CertificateNotYetValidException {
        this.checkValidity(new Date());
    }
    
    @Override
    public void checkValidity(final Date date) throws CertificateExpiredException, CertificateNotYetValidException {
        if (date.getTime() > this.getNotAfter().getTime()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("certificate expired on ");
            sb.append(this.c.getEndDate().getTime());
            throw new CertificateExpiredException(sb.toString());
        }
        if (date.getTime() >= this.getNotBefore().getTime()) {
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("certificate not valid till ");
        sb2.append(this.c.getStartDate().getTime());
        throw new CertificateNotYetValidException(sb2.toString());
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Certificate)) {
            return false;
        }
        final Certificate certificate = (Certificate)o;
        try {
            return Arrays.areEqual(this.getEncoded(), certificate.getEncoded());
        }
        catch (CertificateEncodingException ex) {
            return false;
        }
    }
    
    @Override
    public ASN1Encodable getBagAttribute(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        return this.attrCarrier.getBagAttribute(asn1ObjectIdentifier);
    }
    
    @Override
    public Enumeration getBagAttributeKeys() {
        return this.attrCarrier.getBagAttributeKeys();
    }
    
    @Override
    public int getBasicConstraints() {
        final BasicConstraints basicConstraints = this.basicConstraints;
        if (basicConstraints == null || !basicConstraints.isCA()) {
            return -1;
        }
        if (this.basicConstraints.getPathLenConstraint() == null) {
            return Integer.MAX_VALUE;
        }
        return this.basicConstraints.getPathLenConstraint().intValue();
    }
    
    @Override
    public Set getCriticalExtensionOIDs() {
        if (this.getVersion() == 3) {
            final HashSet<String> set = new HashSet<String>();
            final Extensions extensions = this.c.getTBSCertificate().getExtensions();
            if (extensions != null) {
                final Enumeration oids = extensions.oids();
                while (oids.hasMoreElements()) {
                    final ASN1ObjectIdentifier asn1ObjectIdentifier = oids.nextElement();
                    if (extensions.getExtension(asn1ObjectIdentifier).isCritical()) {
                        set.add(asn1ObjectIdentifier.getId());
                    }
                }
                return set;
            }
        }
        return null;
    }
    
    @Override
    public byte[] getEncoded() throws CertificateEncodingException {
        try {
            return this.c.getEncoded("DER");
        }
        catch (IOException ex) {
            throw new CertificateEncodingException(ex.toString());
        }
    }
    
    @Override
    public List getExtendedKeyUsage() throws CertificateParsingException {
        final byte[] extensionBytes = this.getExtensionBytes("2.5.29.37");
        Label_0088: {
            if (extensionBytes == null) {
                break Label_0088;
            }
            while (true) {
                while (true) {
                    try {
                        final ASN1Sequence asn1Sequence = (ASN1Sequence)new ASN1InputStream(extensionBytes).readObject();
                        final ArrayList<String> list = new ArrayList<String>();
                        for (int i = 0; i != asn1Sequence.size(); ++i) {
                            list.add(((ASN1ObjectIdentifier)asn1Sequence.getObjectAt(i)).getId());
                        }
                        return Collections.unmodifiableList((List<?>)list);
                        return null;
                        throw new CertificateParsingException("error processing extended key usage extension");
                    }
                    catch (Exception ex) {}
                    continue;
                }
            }
        }
    }
    
    @Override
    public byte[] getExtensionValue(final String s) {
        final Extensions extensions = this.c.getTBSCertificate().getExtensions();
        if (extensions != null) {
            final Extension extension = extensions.getExtension(new ASN1ObjectIdentifier(s));
            if (extension != null) {
                try {
                    return extension.getExtnValue().getEncoded();
                }
                catch (Exception ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("error parsing ");
                    sb.append(ex.toString());
                    throw new IllegalStateException(sb.toString());
                }
            }
        }
        return null;
    }
    
    @Override
    public Collection getIssuerAlternativeNames() throws CertificateParsingException {
        return getAlternativeNames(this.getExtensionBytes(Extension.issuerAlternativeName.getId()));
    }
    
    @Override
    public Principal getIssuerDN() {
        return new X509Principal(this.c.getIssuer());
    }
    
    @Override
    public boolean[] getIssuerUniqueID() {
        final DERBitString issuerUniqueId = this.c.getTBSCertificate().getIssuerUniqueId();
        if (issuerUniqueId != null) {
            final byte[] bytes = issuerUniqueId.getBytes();
            final boolean[] array = new boolean[bytes.length * 8 - issuerUniqueId.getPadBits()];
            for (int i = 0; i != array.length; ++i) {
                array[i] = ((bytes[i / 8] & 128 >>> i % 8) != 0x0);
            }
            return array;
        }
        return null;
    }
    
    @Override
    public X500Principal getIssuerX500Principal() {
        try {
            return new X500Principal(this.c.getIssuer().getEncoded());
        }
        catch (IOException ex) {
            throw new IllegalStateException("can't encode issuer DN");
        }
    }
    
    @Override
    public boolean[] getKeyUsage() {
        return this.keyUsage;
    }
    
    @Override
    public Set getNonCriticalExtensionOIDs() {
        if (this.getVersion() == 3) {
            final HashSet<String> set = new HashSet<String>();
            final Extensions extensions = this.c.getTBSCertificate().getExtensions();
            if (extensions != null) {
                final Enumeration oids = extensions.oids();
                while (oids.hasMoreElements()) {
                    final ASN1ObjectIdentifier asn1ObjectIdentifier = oids.nextElement();
                    if (!extensions.getExtension(asn1ObjectIdentifier).isCritical()) {
                        set.add(asn1ObjectIdentifier.getId());
                    }
                }
                return set;
            }
        }
        return null;
    }
    
    @Override
    public Date getNotAfter() {
        return this.c.getEndDate().getDate();
    }
    
    @Override
    public Date getNotBefore() {
        return this.c.getStartDate().getDate();
    }
    
    @Override
    public PublicKey getPublicKey() {
        try {
            return BouncyCastleProvider.getPublicKey(this.c.getSubjectPublicKeyInfo());
        }
        catch (IOException ex) {
            return null;
        }
    }
    
    @Override
    public BigInteger getSerialNumber() {
        return this.c.getSerialNumber().getValue();
    }
    
    @Override
    public String getSigAlgName() {
        final Provider provider = Security.getProvider("BC");
        if (provider != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Alg.Alias.Signature.");
            sb.append(this.getSigAlgOID());
            final String property = provider.getProperty(sb.toString());
            if (property != null) {
                return property;
            }
        }
        final Provider[] providers = Security.getProviders();
        for (int i = 0; i != providers.length; ++i) {
            final Provider provider2 = providers[i];
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Alg.Alias.Signature.");
            sb2.append(this.getSigAlgOID());
            final String property2 = provider2.getProperty(sb2.toString());
            if (property2 != null) {
                return property2;
            }
        }
        return this.getSigAlgOID();
    }
    
    @Override
    public String getSigAlgOID() {
        return this.c.getSignatureAlgorithm().getAlgorithm().getId();
    }
    
    @Override
    public byte[] getSigAlgParams() {
        Label_0037: {
            if (this.c.getSignatureAlgorithm().getParameters() == null) {
                break Label_0037;
            }
            try {
                return this.c.getSignatureAlgorithm().getParameters().toASN1Primitive().getEncoded("DER");
                return null;
            }
            catch (IOException ex) {
                return null;
            }
        }
    }
    
    @Override
    public byte[] getSignature() {
        return this.c.getSignature().getOctets();
    }
    
    @Override
    public Collection getSubjectAlternativeNames() throws CertificateParsingException {
        return getAlternativeNames(this.getExtensionBytes(Extension.subjectAlternativeName.getId()));
    }
    
    @Override
    public Principal getSubjectDN() {
        return new X509Principal(this.c.getSubject());
    }
    
    @Override
    public boolean[] getSubjectUniqueID() {
        final DERBitString subjectUniqueId = this.c.getTBSCertificate().getSubjectUniqueId();
        if (subjectUniqueId != null) {
            final byte[] bytes = subjectUniqueId.getBytes();
            final boolean[] array = new boolean[bytes.length * 8 - subjectUniqueId.getPadBits()];
            for (int i = 0; i != array.length; ++i) {
                array[i] = ((bytes[i / 8] & 128 >>> i % 8) != 0x0);
            }
            return array;
        }
        return null;
    }
    
    @Override
    public X500Principal getSubjectX500Principal() {
        try {
            return new X500Principal(this.c.getSubject().getEncoded());
        }
        catch (IOException ex) {
            throw new IllegalStateException("can't encode issuer DN");
        }
    }
    
    @Override
    public byte[] getTBSCertificate() throws CertificateEncodingException {
        try {
            return this.c.getTBSCertificate().getEncoded("DER");
        }
        catch (IOException ex) {
            throw new CertificateEncodingException(ex.toString());
        }
    }
    
    @Override
    public int getVersion() {
        return this.c.getVersionNumber();
    }
    
    @Override
    public boolean hasUnsupportedCriticalExtension() {
        if (this.getVersion() == 3) {
            final Extensions extensions = this.c.getTBSCertificate().getExtensions();
            if (extensions != null) {
                final Enumeration oids = extensions.oids();
                while (oids.hasMoreElements()) {
                    final ASN1ObjectIdentifier asn1ObjectIdentifier = oids.nextElement();
                    final String id = asn1ObjectIdentifier.getId();
                    if (!id.equals(RFC3280CertPathUtilities.KEY_USAGE) && !id.equals(RFC3280CertPathUtilities.CERTIFICATE_POLICIES) && !id.equals(RFC3280CertPathUtilities.POLICY_MAPPINGS) && !id.equals(RFC3280CertPathUtilities.INHIBIT_ANY_POLICY) && !id.equals(RFC3280CertPathUtilities.CRL_DISTRIBUTION_POINTS) && !id.equals(RFC3280CertPathUtilities.ISSUING_DISTRIBUTION_POINT) && !id.equals(RFC3280CertPathUtilities.DELTA_CRL_INDICATOR) && !id.equals(RFC3280CertPathUtilities.POLICY_CONSTRAINTS) && !id.equals(RFC3280CertPathUtilities.BASIC_CONSTRAINTS) && !id.equals(RFC3280CertPathUtilities.SUBJECT_ALTERNATIVE_NAME)) {
                        if (id.equals(RFC3280CertPathUtilities.NAME_CONSTRAINTS)) {
                            continue;
                        }
                        if (extensions.getExtension(asn1ObjectIdentifier).isCritical()) {
                            return true;
                        }
                        continue;
                    }
                }
            }
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        synchronized (this) {
            if (!this.hashValueSet) {
                this.hashValue = this.calculateHashCode();
                this.hashValueSet = true;
            }
            return this.hashValue;
        }
    }
    
    @Override
    public void setBagAttribute(final ASN1ObjectIdentifier asn1ObjectIdentifier, final ASN1Encodable asn1Encodable) {
        this.attrCarrier.setBagAttribute(asn1ObjectIdentifier, asn1Encodable);
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        final String lineSeparator = Strings.lineSeparator();
        sb.append("  [0]         Version: ");
        sb.append(this.getVersion());
        sb.append(lineSeparator);
        sb.append("         SerialNumber: ");
        sb.append(this.getSerialNumber());
        sb.append(lineSeparator);
        sb.append("             IssuerDN: ");
        sb.append(this.getIssuerDN());
        sb.append(lineSeparator);
        sb.append("           Start Date: ");
        sb.append(this.getNotBefore());
        sb.append(lineSeparator);
        sb.append("           Final Date: ");
        sb.append(this.getNotAfter());
        sb.append(lineSeparator);
        sb.append("            SubjectDN: ");
        sb.append(this.getSubjectDN());
        sb.append(lineSeparator);
        sb.append("           Public Key: ");
        sb.append(this.getPublicKey());
        sb.append(lineSeparator);
        sb.append("  Signature Algorithm: ");
        sb.append(this.getSigAlgName());
        sb.append(lineSeparator);
        final byte[] signature = this.getSignature();
        sb.append("            Signature: ");
        sb.append(new String(Hex.encode(signature, 0, 20)));
        sb.append(lineSeparator);
        for (int i = 20; i < signature.length; i += 20) {
            final int length = signature.length;
            sb.append("                       ");
            String str;
            if (i < length - 20) {
                str = new String(Hex.encode(signature, i, 20));
            }
            else {
                str = new String(Hex.encode(signature, i, signature.length - i));
            }
            sb.append(str);
            sb.append(lineSeparator);
        }
        final Extensions extensions = this.c.getTBSCertificate().getExtensions();
        while (true) {
            Label_0718: {
                if (extensions == null) {
                    break Label_0718;
                }
                final Enumeration oids = extensions.oids();
                if (oids.hasMoreElements()) {
                    sb.append("       Extensions: \n");
                }
                while (true) {
                    if (!oids.hasMoreElements()) {
                        break Label_0718;
                    }
                    final ASN1ObjectIdentifier asn1ObjectIdentifier = oids.nextElement();
                    final Extension extension = extensions.getExtension(asn1ObjectIdentifier);
                    Label_0707: {
                        if (extension.getExtnValue() == null) {
                            break Label_0707;
                        }
                        final ASN1InputStream asn1InputStream = new ASN1InputStream(extension.getExtnValue().getOctets());
                        sb.append("                       critical(");
                        sb.append(extension.isCritical());
                        sb.append(") ");
                        try {
                            Label_0517: {
                                ASN1Object obj;
                                if (asn1ObjectIdentifier.equals(Extension.basicConstraints)) {
                                    obj = BasicConstraints.getInstance(asn1InputStream.readObject());
                                }
                                else if (asn1ObjectIdentifier.equals(Extension.keyUsage)) {
                                    obj = KeyUsage.getInstance(asn1InputStream.readObject());
                                }
                                else if (asn1ObjectIdentifier.equals(MiscObjectIdentifiers.netscapeCertType)) {
                                    obj = new NetscapeCertType((DERBitString)asn1InputStream.readObject());
                                }
                                else if (asn1ObjectIdentifier.equals(MiscObjectIdentifiers.netscapeRevocationURL)) {
                                    obj = new NetscapeRevocationURL((DERIA5String)asn1InputStream.readObject());
                                }
                                else {
                                    if (!asn1ObjectIdentifier.equals(MiscObjectIdentifiers.verisignCzagExtension)) {
                                        sb.append(asn1ObjectIdentifier.getId());
                                        sb.append(" value = ");
                                        sb.append(ASN1Dump.dumpAsString(asn1InputStream.readObject()));
                                        break Label_0517;
                                    }
                                    obj = new VerisignCzagExtension((DERIA5String)asn1InputStream.readObject());
                                }
                                sb.append(obj);
                            }
                            sb.append(lineSeparator);
                            continue;
                            sb.append(asn1ObjectIdentifier.getId());
                            sb.append(" value = ");
                            sb.append("*****");
                            sb.append(lineSeparator);
                            continue;
                            return sb.toString();
                        }
                        catch (Exception ex) {}
                    }
                    break;
                }
            }
            continue;
        }
    }
    
    @Override
    public final void verify(final PublicKey publicKey) throws CertificateException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException, SignatureException {
        final String signatureName = X509SignatureUtil.getSignatureName(this.c.getSignatureAlgorithm());
        while (true) {
            try {
                Signature signature = Signature.getInstance(signatureName, "BC");
                while (true) {
                    this.checkSignature(publicKey, signature);
                    return;
                    signature = Signature.getInstance(signatureName);
                    continue;
                }
            }
            catch (Exception ex) {}
            continue;
        }
    }
    
    @Override
    public final void verify(final PublicKey publicKey, final String provider) throws CertificateException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException, SignatureException {
        final String signatureName = X509SignatureUtil.getSignatureName(this.c.getSignatureAlgorithm());
        Signature signature;
        if (provider != null) {
            signature = Signature.getInstance(signatureName, provider);
        }
        else {
            signature = Signature.getInstance(signatureName);
        }
        this.checkSignature(publicKey, signature);
    }
    
    @Override
    public final void verify(final PublicKey publicKey, final Provider provider) throws CertificateException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        final String signatureName = X509SignatureUtil.getSignatureName(this.c.getSignatureAlgorithm());
        Signature signature;
        if (provider != null) {
            signature = Signature.getInstance(signatureName, provider);
        }
        else {
            signature = Signature.getInstance(signatureName);
        }
        this.checkSignature(publicKey, signature);
    }
}
