// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

import org.bouncycastle.asn1.x500.X500NameStyle;
import org.bouncycastle.jcajce.interfaces.BCX509Certificate;
import java.security.cert.X509CRL;
import org.bouncycastle.x509.X509AttributeCertificate;
import java.security.cert.X509Certificate;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.asn1.x500.X500Name;
import java.security.cert.TrustAnchor;

class PrincipalUtils
{
    static X500Name getCA(final TrustAnchor trustAnchor) {
        return getX500Name(notNull(trustAnchor).getCA());
    }
    
    private static byte[] getEncoded(final X500Principal x500Principal) {
        return notNull(notNull(x500Principal).getEncoded());
    }
    
    static X500Name getEncodedIssuerPrincipal(final Object o) {
        if (o instanceof X509Certificate) {
            return getIssuerPrincipal((X509Certificate)o);
        }
        return getX500Name((X500Principal)((X509AttributeCertificate)o).getIssuer().getPrincipals()[0]);
    }
    
    static X500Name getIssuerPrincipal(final X509CRL x509CRL) {
        return getX500Name(notNull(x509CRL).getIssuerX500Principal());
    }
    
    static X500Name getIssuerPrincipal(final X509Certificate x509Certificate) {
        if (x509Certificate instanceof BCX509Certificate) {
            return notNull(((BCX509Certificate)x509Certificate).getIssuerX500Name());
        }
        return getX500Name(notNull(x509Certificate).getIssuerX500Principal());
    }
    
    static X500Name getSubjectPrincipal(final X509Certificate x509Certificate) {
        if (x509Certificate instanceof BCX509Certificate) {
            return notNull(((BCX509Certificate)x509Certificate).getSubjectX500Name());
        }
        return getX500Name(notNull(x509Certificate).getSubjectX500Principal());
    }
    
    static X500Name getX500Name(final X500Principal x500Principal) {
        return notNull(X500Name.getInstance(getEncoded(x500Principal)));
    }
    
    static X500Name getX500Name(final X500NameStyle x500NameStyle, final X500Principal x500Principal) {
        return notNull(X500Name.getInstance(x500NameStyle, getEncoded(x500Principal)));
    }
    
    private static TrustAnchor notNull(final TrustAnchor trustAnchor) {
        if (trustAnchor != null) {
            return trustAnchor;
        }
        throw new IllegalStateException();
    }
    
    private static X509CRL notNull(final X509CRL x509CRL) {
        if (x509CRL != null) {
            return x509CRL;
        }
        throw new IllegalStateException();
    }
    
    private static X509Certificate notNull(final X509Certificate x509Certificate) {
        if (x509Certificate != null) {
            return x509Certificate;
        }
        throw new IllegalStateException();
    }
    
    private static X500Principal notNull(final X500Principal x500Principal) {
        if (x500Principal != null) {
            return x500Principal;
        }
        throw new IllegalStateException();
    }
    
    private static X500Name notNull(final X500Name x500Name) {
        if (x500Name != null) {
            return x500Name;
        }
        throw new IllegalStateException();
    }
    
    private static byte[] notNull(final byte[] array) {
        if (array != null) {
            return array;
        }
        throw new IllegalStateException();
    }
}
