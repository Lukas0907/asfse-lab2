// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

import java.security.GeneralSecurityException;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.security.cert.PolicyNode;
import org.bouncycastle.asn1.x509.PolicyInformation;
import java.text.ParseException;
import org.bouncycastle.asn1.ASN1GeneralizedTime;
import org.bouncycastle.asn1.isismtt.ISISMTTObjectIdentifiers;
import java.security.cert.CertPath;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1Encodable;
import java.io.OutputStream;
import org.bouncycastle.asn1.ASN1OutputStream;
import java.io.ByteArrayOutputStream;
import java.security.cert.PolicyQualifierInfo;
import org.bouncycastle.asn1.ASN1Sequence;
import java.security.interfaces.DSAParams;
import java.security.spec.KeySpec;
import java.security.spec.DSAPublicKeySpec;
import java.security.interfaces.DSAPublicKey;
import org.bouncycastle.jcajce.util.JcaJceHelper;
import org.bouncycastle.asn1.ASN1Primitive;
import java.math.BigInteger;
import org.bouncycastle.asn1.ASN1Integer;
import java.security.cert.CRL;
import java.security.cert.CRLSelector;
import org.bouncycastle.jcajce.PKIXCRLStoreSelector;
import java.util.HashSet;
import org.bouncycastle.jcajce.PKIXExtendedParameters;
import java.security.cert.X509CRLEntry;
import java.security.cert.CRLException;
import org.bouncycastle.asn1.ASN1Enumerated;
import java.security.cert.X509Extension;
import java.security.cert.X509CRL;
import java.util.Date;
import java.io.IOException;
import java.security.cert.X509CRLSelector;
import java.security.cert.CertPathValidatorException;
import org.bouncycastle.jce.exception.ExtCertPathValidatorException;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.jcajce.PKIXCRLStore;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import java.security.cert.CertificateParsingException;
import java.util.Collections;
import java.util.ArrayList;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.x509.GeneralName;
import java.util.Map;
import javax.security.auth.x500.X500Principal;
import java.security.PublicKey;
import org.bouncycastle.asn1.x500.X500Name;
import java.security.cert.X509CertSelector;
import java.security.cert.TrustAnchor;
import org.bouncycastle.jcajce.PKIXCertStore;
import java.util.Iterator;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertStore;
import org.bouncycastle.util.StoreException;
import org.bouncycastle.util.Selector;
import org.bouncycastle.util.Store;
import java.util.LinkedHashSet;
import java.util.Collection;
import java.util.List;
import org.bouncycastle.jcajce.PKIXCertStoreSelector;
import org.bouncycastle.asn1.x500.style.RFC4519Style;
import java.security.cert.X509Certificate;
import org.bouncycastle.x509.X509AttributeCertificate;
import java.util.Set;
import org.bouncycastle.asn1.x509.Extension;

class CertPathValidatorUtilities
{
    protected static final String ANY_POLICY = "2.5.29.32.0";
    protected static final String AUTHORITY_KEY_IDENTIFIER;
    protected static final String BASIC_CONSTRAINTS;
    protected static final String CERTIFICATE_POLICIES;
    protected static final String CRL_DISTRIBUTION_POINTS;
    protected static final String CRL_NUMBER;
    protected static final int CRL_SIGN = 6;
    protected static final PKIXCRLUtil CRL_UTIL;
    protected static final String DELTA_CRL_INDICATOR;
    protected static final String FRESHEST_CRL;
    protected static final String INHIBIT_ANY_POLICY;
    protected static final String ISSUING_DISTRIBUTION_POINT;
    protected static final int KEY_CERT_SIGN = 5;
    protected static final String KEY_USAGE;
    protected static final String NAME_CONSTRAINTS;
    protected static final String POLICY_CONSTRAINTS;
    protected static final String POLICY_MAPPINGS;
    protected static final String SUBJECT_ALTERNATIVE_NAME;
    protected static final String[] crlReasons;
    
    static {
        CRL_UTIL = new PKIXCRLUtil();
        CERTIFICATE_POLICIES = Extension.certificatePolicies.getId();
        BASIC_CONSTRAINTS = Extension.basicConstraints.getId();
        POLICY_MAPPINGS = Extension.policyMappings.getId();
        SUBJECT_ALTERNATIVE_NAME = Extension.subjectAlternativeName.getId();
        NAME_CONSTRAINTS = Extension.nameConstraints.getId();
        KEY_USAGE = Extension.keyUsage.getId();
        INHIBIT_ANY_POLICY = Extension.inhibitAnyPolicy.getId();
        ISSUING_DISTRIBUTION_POINT = Extension.issuingDistributionPoint.getId();
        DELTA_CRL_INDICATOR = Extension.deltaCRLIndicator.getId();
        POLICY_CONSTRAINTS = Extension.policyConstraints.getId();
        FRESHEST_CRL = Extension.freshestCRL.getId();
        CRL_DISTRIBUTION_POINTS = Extension.cRLDistributionPoints.getId();
        AUTHORITY_KEY_IDENTIFIER = Extension.authorityKeyIdentifier.getId();
        CRL_NUMBER = Extension.cRLNumber.getId();
        crlReasons = new String[] { "unspecified", "keyCompromise", "cACompromise", "affiliationChanged", "superseded", "cessationOfOperation", "certificateHold", "unknown", "removeFromCRL", "privilegeWithdrawn", "aACompromise" };
    }
    
    static void checkCRLsNotEmpty(final Set set, final Object o) throws AnnotatedException {
        if (!set.isEmpty()) {
            return;
        }
        if (o instanceof X509AttributeCertificate) {
            final X509AttributeCertificate x509AttributeCertificate = (X509AttributeCertificate)o;
            final StringBuilder sb = new StringBuilder();
            sb.append("No CRLs found for issuer \"");
            sb.append(x509AttributeCertificate.getIssuer().getPrincipals()[0]);
            sb.append("\"");
            throw new AnnotatedException(sb.toString());
        }
        final X509Certificate x509Certificate = (X509Certificate)o;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("No CRLs found for issuer \"");
        sb2.append(RFC4519Style.INSTANCE.toString(PrincipalUtils.getIssuerPrincipal(x509Certificate)));
        sb2.append("\"");
        throw new AnnotatedException(sb2.toString());
    }
    
    protected static Collection findCertificates(final PKIXCertStoreSelector pkixCertStoreSelector, final List list) throws AnnotatedException {
        final LinkedHashSet set = new LinkedHashSet();
        for (final Store<? extends E> next : list) {
            if (next instanceof Store) {
                final Store<? extends E> store = next;
                try {
                    set.addAll(store.getMatches(pkixCertStoreSelector));
                    continue;
                }
                catch (StoreException ex) {
                    throw new AnnotatedException("Problem while picking certificates from X.509 store.", ex);
                }
            }
            final CertStore certStore = (CertStore)next;
            try {
                set.addAll(PKIXCertStoreSelector.getCertificates(pkixCertStoreSelector, certStore));
                continue;
            }
            catch (CertStoreException ex2) {
                throw new AnnotatedException("Problem while picking certificates from certificate store.", ex2);
            }
            break;
        }
        return set;
    }
    
    static Collection findIssuerCerts(final X509Certificate p0, final List<CertStore> p1, final List<PKIXCertStore> p2) throws AnnotatedException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   java/security/cert/X509CertSelector.<init>:()V
        //     7: astore_3       
        //     8: aload_3        
        //     9: aload_0        
        //    10: invokestatic    org/bouncycastle/jce/provider/PrincipalUtils.getIssuerPrincipal:(Ljava/security/cert/X509Certificate;)Lorg/bouncycastle/asn1/x500/X500Name;
        //    13: invokevirtual   org/bouncycastle/asn1/x500/X500Name.getEncoded:()[B
        //    16: invokevirtual   java/security/cert/X509CertSelector.setSubject:([B)V
        //    19: aload_0        
        //    20: getstatic       org/bouncycastle/jce/provider/CertPathValidatorUtilities.AUTHORITY_KEY_IDENTIFIER:Ljava/lang/String;
        //    23: invokevirtual   java/security/cert/X509Certificate.getExtensionValue:(Ljava/lang/String;)[B
        //    26: astore_0       
        //    27: aload_0        
        //    28: ifnull          64
        //    31: aload_0        
        //    32: invokestatic    org/bouncycastle/asn1/ASN1OctetString.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/ASN1OctetString;
        //    35: invokevirtual   org/bouncycastle/asn1/ASN1OctetString.getOctets:()[B
        //    38: invokestatic    org/bouncycastle/asn1/x509/AuthorityKeyIdentifier.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/AuthorityKeyIdentifier;
        //    41: invokevirtual   org/bouncycastle/asn1/x509/AuthorityKeyIdentifier.getKeyIdentifier:()[B
        //    44: astore_0       
        //    45: aload_0        
        //    46: ifnull          64
        //    49: aload_3        
        //    50: new             Lorg/bouncycastle/asn1/DEROctetString;
        //    53: dup            
        //    54: aload_0        
        //    55: invokespecial   org/bouncycastle/asn1/DEROctetString.<init>:([B)V
        //    58: invokevirtual   org/bouncycastle/asn1/DEROctetString.getEncoded:()[B
        //    61: invokevirtual   java/security/cert/X509CertSelector.setSubjectKeyIdentifier:([B)V
        //    64: new             Lorg/bouncycastle/jcajce/PKIXCertStoreSelector$Builder;
        //    67: dup            
        //    68: aload_3        
        //    69: invokespecial   org/bouncycastle/jcajce/PKIXCertStoreSelector$Builder.<init>:(Ljava/security/cert/CertSelector;)V
        //    72: invokevirtual   org/bouncycastle/jcajce/PKIXCertStoreSelector$Builder.build:()Lorg/bouncycastle/jcajce/PKIXCertStoreSelector;
        //    75: astore_3       
        //    76: new             Ljava/util/LinkedHashSet;
        //    79: dup            
        //    80: invokespecial   java/util/LinkedHashSet.<init>:()V
        //    83: astore_0       
        //    84: new             Ljava/util/ArrayList;
        //    87: dup            
        //    88: invokespecial   java/util/ArrayList.<init>:()V
        //    91: astore          4
        //    93: aload           4
        //    95: aload_3        
        //    96: aload_1        
        //    97: invokestatic    org/bouncycastle/jce/provider/CertPathValidatorUtilities.findCertificates:(Lorg/bouncycastle/jcajce/PKIXCertStoreSelector;Ljava/util/List;)Ljava/util/Collection;
        //   100: invokeinterface java/util/List.addAll:(Ljava/util/Collection;)Z
        //   105: pop            
        //   106: aload           4
        //   108: aload_3        
        //   109: aload_2        
        //   110: invokestatic    org/bouncycastle/jce/provider/CertPathValidatorUtilities.findCertificates:(Lorg/bouncycastle/jcajce/PKIXCertStoreSelector;Ljava/util/List;)Ljava/util/Collection;
        //   113: invokeinterface java/util/List.addAll:(Ljava/util/Collection;)Z
        //   118: pop            
        //   119: aload           4
        //   121: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //   126: astore_1       
        //   127: aload_1        
        //   128: invokeinterface java/util/Iterator.hasNext:()Z
        //   133: ifeq            155
        //   136: aload_0        
        //   137: aload_1        
        //   138: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   143: checkcast       Ljava/security/cert/X509Certificate;
        //   146: invokeinterface java/util/Set.add:(Ljava/lang/Object;)Z
        //   151: pop            
        //   152: goto            127
        //   155: aload_0        
        //   156: areturn        
        //   157: astore_0       
        //   158: new             Lorg/bouncycastle/jce/provider/AnnotatedException;
        //   161: dup            
        //   162: ldc_w           "Issuer certificate cannot be searched."
        //   165: aload_0        
        //   166: invokespecial   org/bouncycastle/jce/provider/AnnotatedException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   169: athrow         
        //   170: astore_0       
        //   171: new             Lorg/bouncycastle/jce/provider/AnnotatedException;
        //   174: dup            
        //   175: ldc_w           "Subject criteria for certificate selector to find issuer certificate could not be set."
        //   178: aload_0        
        //   179: invokespecial   org/bouncycastle/jce/provider/AnnotatedException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   182: athrow         
        //   183: astore_0       
        //   184: goto            64
        //    Exceptions:
        //  throws org.bouncycastle.jce.provider.AnnotatedException
        //    Signature:
        //  (Ljava/security/cert/X509Certificate;Ljava/util/List<Ljava/security/cert/CertStore;>;Ljava/util/List<Lorg/bouncycastle/jcajce/PKIXCertStore;>;)Ljava/util/Collection;
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                              
        //  -----  -----  -----  -----  --------------------------------------------------
        //  8      19     170    183    Ljava/lang/Exception;
        //  19     27     183    187    Ljava/lang/Exception;
        //  31     45     183    187    Ljava/lang/Exception;
        //  49     64     183    187    Ljava/lang/Exception;
        //  84     127    157    170    Lorg/bouncycastle/jce/provider/AnnotatedException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0064:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    protected static TrustAnchor findTrustAnchor(final X509Certificate x509Certificate, final Set set) throws AnnotatedException {
        return findTrustAnchor(x509Certificate, set, null);
    }
    
    protected static TrustAnchor findTrustAnchor(final X509Certificate x509Certificate, final Set set, final String s) throws AnnotatedException {
        final X509CertSelector x509CertSelector = new X509CertSelector();
        final X500Principal issuerX500Principal = x509Certificate.getIssuerX500Principal();
        x509CertSelector.setSubject(issuerX500Principal);
        final Iterator<TrustAnchor> iterator = set.iterator();
        Object instance;
        Object o = instance = null;
        Object o2;
        X500Name x500Name = (X500Name)(o2 = instance);
        final Object o3 = instance;
    Label_0197_Outer:
        while (true) {
            Label_0258: {
                if (!iterator.hasNext() || o != null) {
                    break Label_0258;
                }
                final TrustAnchor trustAnchor = iterator.next();
                Label_0209: {
                    while (true) {
                        if (trustAnchor.getTrustedCert() != null) {
                            final X500Name x500Name2 = x500Name;
                            if (x509CertSelector.match(trustAnchor.getTrustedCert())) {
                                final Object o4 = trustAnchor.getTrustedCert().getPublicKey();
                                final TrustAnchor trustAnchor2 = trustAnchor;
                                instance = x500Name;
                                break Label_0209;
                            }
                            break Label_0197;
                        }
                        else {
                            X500Name x500Name2 = x500Name;
                            if (trustAnchor.getCA() == null) {
                                break Label_0197;
                            }
                            x500Name2 = x500Name;
                            if (trustAnchor.getCAName() == null) {
                                break Label_0197;
                            }
                            x500Name2 = x500Name;
                            if (trustAnchor.getCAPublicKey() == null) {
                                break Label_0197;
                            }
                            if ((instance = x500Name) == null) {
                                instance = X500Name.getInstance(issuerX500Principal.getEncoded());
                            }
                        }
                        X500Name x500Name2 = (X500Name)instance;
                        try {
                            Object o4;
                            TrustAnchor trustAnchor2;
                            if (((X500Name)instance).equals(X500Name.getInstance(trustAnchor.getCA().getEncoded()))) {
                                o4 = trustAnchor.getCAPublicKey();
                                trustAnchor2 = trustAnchor;
                            }
                            else {
                                final TrustAnchor trustAnchor3 = null;
                                o4 = o2;
                                instance = x500Name2;
                                trustAnchor2 = trustAnchor3;
                            }
                            o = trustAnchor2;
                            x500Name = (X500Name)instance;
                            o2 = o4;
                            if (o4 != null) {
                                try {
                                    verifyX509Certificate(x509Certificate, (PublicKey)o4, s);
                                    o = trustAnchor2;
                                    x500Name = (X500Name)instance;
                                    o2 = o4;
                                }
                                catch (Exception o3) {
                                    o = (o2 = null);
                                    x500Name = (X500Name)instance;
                                }
                                continue Label_0197_Outer;
                            }
                            continue Label_0197_Outer;
                            while (true) {
                                return (TrustAnchor)o;
                                Label_0271: {
                                    throw new AnnotatedException("TrustAnchor found but certificate validation failed.", (Throwable)o3);
                                }
                                Label_0284:
                                return (TrustAnchor)o;
                                continue;
                            }
                        }
                        // iftrue(Label_0271:, o3 != null)
                        // iftrue(Label_0284:, o != null)
                        catch (IllegalArgumentException ex) {
                            x500Name2 = (X500Name)instance;
                            continue;
                        }
                        break;
                    }
                }
            }
        }
    }
    
    static List<PKIXCertStore> getAdditionalStoresFromAltNames(final byte[] array, final Map<GeneralName, PKIXCertStore> map) throws CertificateParsingException {
        if (array != null) {
            final GeneralName[] names = GeneralNames.getInstance(ASN1OctetString.getInstance(array).getOctets()).getNames();
            final ArrayList<PKIXCertStore> list = new ArrayList<PKIXCertStore>();
            for (int i = 0; i != names.length; ++i) {
                final PKIXCertStore pkixCertStore = map.get(names[i]);
                if (pkixCertStore != null) {
                    list.add(pkixCertStore);
                }
            }
            return (List<PKIXCertStore>)list;
        }
        return (List<PKIXCertStore>)Collections.EMPTY_LIST;
    }
    
    static List<PKIXCRLStore> getAdditionalStoresFromCRLDistributionPoint(final CRLDistPoint crlDistPoint, final Map<GeneralName, PKIXCRLStore> map) throws AnnotatedException {
        if (crlDistPoint != null) {
            try {
                final DistributionPoint[] distributionPoints = crlDistPoint.getDistributionPoints();
                final ArrayList<PKIXCRLStore> list = new ArrayList<PKIXCRLStore>();
                for (int i = 0; i < distributionPoints.length; ++i) {
                    final DistributionPointName distributionPoint = distributionPoints[i].getDistributionPoint();
                    if (distributionPoint != null && distributionPoint.getType() == 0) {
                        final GeneralName[] names = GeneralNames.getInstance(distributionPoint.getName()).getNames();
                        for (int j = 0; j < names.length; ++j) {
                            final PKIXCRLStore pkixcrlStore = map.get(names[j]);
                            if (pkixcrlStore != null) {
                                list.add(pkixcrlStore);
                            }
                        }
                    }
                }
                return (List<PKIXCRLStore>)list;
            }
            catch (Exception ex) {
                throw new AnnotatedException("Distribution points could not be read.", ex);
            }
        }
        return (List<PKIXCRLStore>)Collections.EMPTY_LIST;
    }
    
    protected static AlgorithmIdentifier getAlgorithmIdentifier(final PublicKey publicKey) throws CertPathValidatorException {
        try {
            return SubjectPublicKeyInfo.getInstance(new ASN1InputStream(publicKey.getEncoded()).readObject()).getAlgorithm();
        }
        catch (Exception ex) {
            throw new ExtCertPathValidatorException("Subject public key cannot be decoded.", ex);
        }
    }
    
    protected static void getCRLIssuersFromDistributionPoint(final DistributionPoint distributionPoint, final Collection collection, final X509CRLSelector x509CRLSelector) throws AnnotatedException {
        final ArrayList<X500Name> list = new ArrayList<X500Name>();
        if (distributionPoint.getCRLIssuer() != null) {
            final GeneralName[] names = distributionPoint.getCRLIssuer().getNames();
            for (int i = 0; i < names.length; ++i) {
                if (names[i].getTagNo() == 4) {
                    try {
                        list.add(X500Name.getInstance(names[i].getName().toASN1Primitive().getEncoded()));
                    }
                    catch (IOException ex) {
                        throw new AnnotatedException("CRL issuer information from distribution point cannot be decoded.", ex);
                    }
                }
            }
        }
        else {
            if (distributionPoint.getDistributionPoint() == null) {
                throw new AnnotatedException("CRL issuer is omitted from distribution point but no distributionPoint field present.");
            }
            final Iterator<X500Name> iterator = collection.iterator();
            while (iterator.hasNext()) {
                list.add(iterator.next());
            }
        }
        final Iterator<Object> iterator2 = list.iterator();
        while (iterator2.hasNext()) {
            try {
                x509CRLSelector.addIssuerName(iterator2.next().getEncoded());
                continue;
            }
            catch (IOException ex2) {
                throw new AnnotatedException("Cannot decode CRL issuer information.", ex2);
            }
            break;
        }
    }
    
    protected static void getCertStatus(final Date date, final X509CRL x509CRL, final Object o, final CertStatus certStatus) throws AnnotatedException {
        try {
            X509CRLEntry revokedCertificate2;
            if (X509CRLObject.isIndirectCRL(x509CRL)) {
                final X509CRLEntry revokedCertificate = x509CRL.getRevokedCertificate(getSerialNumber(o));
                if (revokedCertificate == null) {
                    return;
                }
                final X500Principal certificateIssuer = revokedCertificate.getCertificateIssuer();
                X500Name x500Name;
                if (certificateIssuer == null) {
                    x500Name = PrincipalUtils.getIssuerPrincipal(x509CRL);
                }
                else {
                    x500Name = PrincipalUtils.getX500Name(certificateIssuer);
                }
                revokedCertificate2 = revokedCertificate;
                if (!PrincipalUtils.getEncodedIssuerPrincipal(o).equals(x500Name)) {
                    return;
                }
            }
            else {
                if (!PrincipalUtils.getEncodedIssuerPrincipal(o).equals(PrincipalUtils.getIssuerPrincipal(x509CRL))) {
                    return;
                }
                if ((revokedCertificate2 = x509CRL.getRevokedCertificate(getSerialNumber(o))) == null) {
                    return;
                }
            }
            ASN1Enumerated instance = null;
            Label_0159: {
                if (revokedCertificate2.hasExtensions()) {
                    if (!revokedCertificate2.hasUnsupportedCriticalExtension()) {
                        try {
                            instance = ASN1Enumerated.getInstance(getExtensionValue(revokedCertificate2, Extension.reasonCode.getId()));
                            break Label_0159;
                        }
                        catch (Exception ex) {
                            throw new AnnotatedException("Reason code CRL entry extension could not be decoded.", ex);
                        }
                    }
                    throw new AnnotatedException("CRL entry has unsupported critical extensions.");
                }
            }
            int intValueExact;
            if (instance == null) {
                intValueExact = 0;
            }
            else {
                intValueExact = instance.intValueExact();
            }
            if (date.getTime() >= revokedCertificate2.getRevocationDate().getTime() || intValueExact == 0 || intValueExact == 1 || intValueExact == 2 || intValueExact == 10) {
                certStatus.setCertStatus(intValueExact);
                certStatus.setRevocationDate(revokedCertificate2.getRevocationDate());
            }
        }
        catch (CRLException ex2) {
            throw new AnnotatedException("Failed check for indirect CRL.", ex2);
        }
    }
    
    protected static Set getCompleteCRLs(final DistributionPoint distributionPoint, final Object o, Date date, final PKIXExtendedParameters pkixExtendedParameters) throws AnnotatedException {
        final X509CRLSelector x509CRLSelector = new X509CRLSelector();
        try {
            final HashSet<X500Name> set = new HashSet<X500Name>();
            set.add(PrincipalUtils.getEncodedIssuerPrincipal(o));
            getCRLIssuersFromDistributionPoint(distributionPoint, set, x509CRLSelector);
            if (o instanceof X509Certificate) {
                x509CRLSelector.setCertificateChecking((X509Certificate)o);
            }
            final PKIXCRLStoreSelector<? extends CRL> build = new PKIXCRLStoreSelector.Builder(x509CRLSelector).setCompleteCRLEnabled(true).build();
            if (pkixExtendedParameters.getDate() != null) {
                date = pkixExtendedParameters.getDate();
            }
            final Set crLs = CertPathValidatorUtilities.CRL_UTIL.findCRLs(build, date, pkixExtendedParameters.getCertStores(), pkixExtendedParameters.getCRLStores());
            checkCRLsNotEmpty(crLs, o);
            return crLs;
        }
        catch (AnnotatedException ex) {
            throw new AnnotatedException("Could not get issuer information from distribution point.", ex);
        }
    }
    
    protected static Set getDeltaCRLs(final Date date, final X509CRL x509CRL, final List<CertStore> list, final List<PKIXCRLStore> list2) throws AnnotatedException {
        final X509CRLSelector x509CRLSelector = new X509CRLSelector();
        try {
            x509CRLSelector.addIssuerName(PrincipalUtils.getIssuerPrincipal(x509CRL).getEncoded());
            try {
                final ASN1Primitive extensionValue = getExtensionValue(x509CRL, CertPathValidatorUtilities.CRL_NUMBER);
                final BigInteger bigInteger = null;
                BigInteger positiveValue;
                if (extensionValue != null) {
                    positiveValue = ASN1Integer.getInstance(extensionValue).getPositiveValue();
                }
                else {
                    positiveValue = null;
                }
                try {
                    final byte[] extensionValue2 = x509CRL.getExtensionValue(CertPathValidatorUtilities.ISSUING_DISTRIBUTION_POINT);
                    BigInteger add;
                    if (positiveValue == null) {
                        add = bigInteger;
                    }
                    else {
                        add = positiveValue.add(BigInteger.valueOf(1L));
                    }
                    x509CRLSelector.setMinCRLNumber(add);
                    final PKIXCRLStoreSelector.Builder builder = new PKIXCRLStoreSelector.Builder(x509CRLSelector);
                    builder.setIssuingDistributionPoint(extensionValue2);
                    builder.setIssuingDistributionPointEnabled(true);
                    builder.setMaxBaseCRLNumber(positiveValue);
                    final Set crLs = CertPathValidatorUtilities.CRL_UTIL.findCRLs(builder.build(), date, list, list2);
                    final HashSet<X509CRL> set = new HashSet<X509CRL>();
                    for (final X509CRL x509CRL2 : crLs) {
                        if (isDeltaCRL(x509CRL2)) {
                            set.add(x509CRL2);
                        }
                    }
                    return set;
                }
                catch (Exception ex) {
                    throw new AnnotatedException("Issuing distribution point extension value could not be read.", ex);
                }
            }
            catch (Exception ex2) {
                throw new AnnotatedException("CRL number extension could not be extracted from CRL.", ex2);
            }
        }
        catch (IOException ex3) {
            throw new AnnotatedException("Cannot extract issuer from CRL.", ex3);
        }
    }
    
    protected static ASN1Primitive getExtensionValue(final X509Extension x509Extension, final String s) throws AnnotatedException {
        final byte[] extensionValue = x509Extension.getExtensionValue(s);
        if (extensionValue == null) {
            return null;
        }
        return getObject(s, extensionValue);
    }
    
    protected static PublicKey getNextWorkingKey(final List list, int n, final JcaJceHelper jcaJceHelper) throws CertPathValidatorException {
        final PublicKey publicKey = list.get(n).getPublicKey();
        if (!(publicKey instanceof DSAPublicKey)) {
            return publicKey;
        }
        final DSAPublicKey dsaPublicKey = (DSAPublicKey)publicKey;
        if (dsaPublicKey.getParams() != null) {
            return dsaPublicKey;
        }
        while (true) {
            ++n;
            if (n >= list.size()) {
                throw new CertPathValidatorException("DSA parameters cannot be inherited from previous certificate.");
            }
            final PublicKey publicKey2 = list.get(n).getPublicKey();
            if (!(publicKey2 instanceof DSAPublicKey)) {
                break;
            }
            final DSAPublicKey dsaPublicKey2 = (DSAPublicKey)publicKey2;
            if (dsaPublicKey2.getParams() == null) {
                continue;
            }
            final DSAParams params = dsaPublicKey2.getParams();
            final DSAPublicKeySpec keySpec = new DSAPublicKeySpec(dsaPublicKey.getY(), params.getP(), params.getQ(), params.getG());
            try {
                return jcaJceHelper.createKeyFactory("DSA").generatePublic(keySpec);
            }
            catch (Exception ex) {
                throw new RuntimeException(ex.getMessage());
            }
            break;
        }
        throw new CertPathValidatorException("DSA parameters cannot be inherited from previous certificate.");
    }
    
    private static ASN1Primitive getObject(final String str, final byte[] array) throws AnnotatedException {
        try {
            return ASN1Primitive.fromByteArray(ASN1OctetString.getInstance(new ASN1InputStream(array).readObject()).getOctets());
        }
        catch (Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("exception processing extension ");
            sb.append(str);
            throw new AnnotatedException(sb.toString(), ex);
        }
    }
    
    protected static final Set getQualifierSet(final ASN1Sequence asn1Sequence) throws CertPathValidatorException {
        final HashSet<PolicyQualifierInfo> set = new HashSet<PolicyQualifierInfo>();
        if (asn1Sequence == null) {
            return set;
        }
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final ASN1OutputStream create = ASN1OutputStream.create(byteArrayOutputStream);
        final Enumeration objects = asn1Sequence.getObjects();
        while (objects.hasMoreElements()) {
            try {
                create.writeObject(objects.nextElement());
                set.add(new PolicyQualifierInfo(byteArrayOutputStream.toByteArray()));
                byteArrayOutputStream.reset();
                continue;
            }
            catch (IOException ex) {
                throw new ExtCertPathValidatorException("Policy qualifier info cannot be decoded.", ex);
            }
            break;
        }
        return set;
    }
    
    private static BigInteger getSerialNumber(final Object o) {
        return ((X509Certificate)o).getSerialNumber();
    }
    
    protected static Date getValidCertDateFromValidityModel(final PKIXExtendedParameters pkixExtendedParameters, final CertPath certPath, int n) throws AnnotatedException {
        while (true) {
            Label_0125: {
                if (pkixExtendedParameters.getValidityModel() != 1) {
                    break Label_0125;
                }
                if (n <= 0) {
                    return getValidDate(pkixExtendedParameters);
                }
                --n;
                Label_0086: {
                    if (n != 0) {
                        break Label_0086;
                    }
                    ASN1GeneralizedTime instance = null;
                    try {
                        final byte[] extensionValue = ((X509Certificate)certPath.getCertificates().get(n)).getExtensionValue(ISISMTTObjectIdentifiers.id_isismtt_at_dateOfCertGen.getId());
                        if (extensionValue != null) {
                            instance = ASN1GeneralizedTime.getInstance(ASN1Primitive.fromByteArray(extensionValue));
                        }
                        if (instance != null) {
                            try {
                                return instance.getDate();
                            }
                            catch (ParseException ex) {
                                throw new AnnotatedException("Date from date of cert gen extension could not be parsed.", ex);
                            }
                        }
                        return ((X509Certificate)certPath.getCertificates().get(n)).getNotBefore();
                        throw new AnnotatedException("Date of cert gen extension could not be read.");
                        return getValidDate(pkixExtendedParameters);
                    }
                    catch (IOException ex2) {}
                    catch (IllegalArgumentException ex3) {}
                }
            }
            continue;
        }
    }
    
    protected static Date getValidDate(final PKIXExtendedParameters pkixExtendedParameters) {
        Date date;
        if ((date = pkixExtendedParameters.getDate()) == null) {
            date = new Date();
        }
        return date;
    }
    
    protected static boolean isAnyPolicy(final Set set) {
        return set == null || set.contains("2.5.29.32.0") || set.isEmpty();
    }
    
    private static boolean isDeltaCRL(final X509CRL x509CRL) {
        final Set<String> criticalExtensionOIDs = x509CRL.getCriticalExtensionOIDs();
        return criticalExtensionOIDs != null && criticalExtensionOIDs.contains(RFC3280CertPathUtilities.DELTA_CRL_INDICATOR);
    }
    
    static boolean isIssuerTrustAnchor(final X509Certificate x509Certificate, final Set set, final String s) throws AnnotatedException {
        boolean b = false;
        try {
            if (findTrustAnchor(x509Certificate, set, s) != null) {
                b = true;
            }
            return b;
        }
        catch (Exception ex) {
            return false;
        }
    }
    
    protected static boolean isSelfIssued(final X509Certificate x509Certificate) {
        return x509Certificate.getSubjectDN().equals(x509Certificate.getIssuerDN());
    }
    
    protected static void prepareNextCertB1(final int n, final List[] array, final String anObject, final Map map, final X509Certificate x509Certificate) throws AnnotatedException, CertPathValidatorException {
        final Iterator iterator = array[n].iterator();
        while (true) {
            PKIXPolicyNode pkixPolicyNode;
            do {
                final boolean hasNext = iterator.hasNext();
                boolean contains = false;
                if (!hasNext) {
                    final boolean b = false;
                    if (!b) {
                        for (final PKIXPolicyNode pkixPolicyNode2 : array[n]) {
                            if ("2.5.29.32.0".equals(pkixPolicyNode2.getValidPolicy())) {
                                final Set set = null;
                                try {
                                    final Enumeration objects = ASN1Sequence.getInstance(getExtensionValue(x509Certificate, CertPathValidatorUtilities.CERTIFICATE_POLICIES)).getObjects();
                                    Set qualifierSet;
                                    while (true) {
                                        qualifierSet = set;
                                        if (objects.hasMoreElements()) {
                                            try {
                                                final PolicyInformation instance = PolicyInformation.getInstance(objects.nextElement());
                                                if (!"2.5.29.32.0".equals(instance.getPolicyIdentifier().getId())) {
                                                    continue;
                                                }
                                                try {
                                                    qualifierSet = getQualifierSet(instance.getPolicyQualifiers());
                                                }
                                                catch (CertPathValidatorException ex) {
                                                    throw new ExtCertPathValidatorException("Policy qualifier info set could not be built.", ex);
                                                }
                                            }
                                            catch (Exception ex2) {
                                                throw new AnnotatedException("Policy information cannot be decoded.", ex2);
                                            }
                                            break;
                                        }
                                        break;
                                    }
                                    if (x509Certificate.getCriticalExtensionOIDs() != null) {
                                        contains = x509Certificate.getCriticalExtensionOIDs().contains(CertPathValidatorUtilities.CERTIFICATE_POLICIES);
                                    }
                                    final PKIXPolicyNode pkixPolicyNode3 = (PKIXPolicyNode)pkixPolicyNode2.getParent();
                                    if ("2.5.29.32.0".equals(pkixPolicyNode3.getValidPolicy())) {
                                        final PKIXPolicyNode pkixPolicyNode4 = new PKIXPolicyNode(new ArrayList(), n, map.get(anObject), pkixPolicyNode3, qualifierSet, anObject, contains);
                                        pkixPolicyNode3.addChild(pkixPolicyNode4);
                                        array[n].add(pkixPolicyNode4);
                                        return;
                                    }
                                }
                                catch (Exception ex3) {
                                    throw new AnnotatedException("Certificate policies cannot be decoded.", ex3);
                                }
                                break;
                            }
                        }
                    }
                    return;
                }
                pkixPolicyNode = iterator.next();
            } while (!pkixPolicyNode.getValidPolicy().equals(anObject));
            final boolean b = true;
            pkixPolicyNode.expectedPolicies = map.get(anObject);
            continue;
        }
    }
    
    protected static PKIXPolicyNode prepareNextCertB2(final int n, final List[] array, final String anObject, PKIXPolicyNode removePolicyNode) {
        final Iterator iterator = array[n].iterator();
        PKIXPolicyNode pkixPolicyNode = removePolicyNode;
        while (iterator.hasNext()) {
            removePolicyNode = iterator.next();
            if (removePolicyNode.getValidPolicy().equals(anObject)) {
                ((PKIXPolicyNode)removePolicyNode.getParent()).removeChild(removePolicyNode);
                iterator.remove();
                int n2 = n - 1;
                removePolicyNode = pkixPolicyNode;
                while (true) {
                    pkixPolicyNode = removePolicyNode;
                    if (n2 < 0) {
                        break;
                    }
                    final List list = array[n2];
                    int n3 = 0;
                    PKIXPolicyNode pkixPolicyNode2;
                    while (true) {
                        pkixPolicyNode2 = removePolicyNode;
                        if (n3 >= list.size()) {
                            break;
                        }
                        final PKIXPolicyNode pkixPolicyNode3 = list.get(n3);
                        PKIXPolicyNode pkixPolicyNode4 = removePolicyNode;
                        if (!pkixPolicyNode3.hasChildren()) {
                            removePolicyNode = removePolicyNode(removePolicyNode, array, pkixPolicyNode3);
                            if ((pkixPolicyNode4 = removePolicyNode) == null) {
                                pkixPolicyNode2 = removePolicyNode;
                                break;
                            }
                        }
                        ++n3;
                        removePolicyNode = pkixPolicyNode4;
                    }
                    --n2;
                    removePolicyNode = pkixPolicyNode2;
                }
            }
        }
        return pkixPolicyNode;
    }
    
    protected static boolean processCertD1i(final int n, final List[] array, final ASN1ObjectIdentifier asn1ObjectIdentifier, final Set set) {
        final List list = array[n - 1];
        for (int i = 0; i < list.size(); ++i) {
            final PKIXPolicyNode pkixPolicyNode = list.get(i);
            if (pkixPolicyNode.getExpectedPolicies().contains(asn1ObjectIdentifier.getId())) {
                final HashSet<String> set2 = new HashSet<String>();
                set2.add(asn1ObjectIdentifier.getId());
                final PKIXPolicyNode pkixPolicyNode2 = new PKIXPolicyNode(new ArrayList(), n, set2, pkixPolicyNode, set, asn1ObjectIdentifier.getId(), false);
                pkixPolicyNode.addChild(pkixPolicyNode2);
                array[n].add(pkixPolicyNode2);
                return true;
            }
        }
        return false;
    }
    
    protected static void processCertD1ii(final int n, final List[] array, final ASN1ObjectIdentifier asn1ObjectIdentifier, final Set set) {
        final List list = array[n - 1];
        for (int i = 0; i < list.size(); ++i) {
            final PKIXPolicyNode pkixPolicyNode = list.get(i);
            if ("2.5.29.32.0".equals(pkixPolicyNode.getValidPolicy())) {
                final HashSet<String> set2 = new HashSet<String>();
                set2.add(asn1ObjectIdentifier.getId());
                final PKIXPolicyNode pkixPolicyNode2 = new PKIXPolicyNode(new ArrayList(), n, set2, pkixPolicyNode, set, asn1ObjectIdentifier.getId(), false);
                pkixPolicyNode.addChild(pkixPolicyNode2);
                array[n].add(pkixPolicyNode2);
                return;
            }
        }
    }
    
    protected static PKIXPolicyNode removePolicyNode(final PKIXPolicyNode pkixPolicyNode, final List[] array, final PKIXPolicyNode pkixPolicyNode2) {
        final PKIXPolicyNode pkixPolicyNode3 = (PKIXPolicyNode)pkixPolicyNode2.getParent();
        if (pkixPolicyNode == null) {
            return null;
        }
        if (pkixPolicyNode3 == null) {
            for (int i = 0; i < array.length; ++i) {
                array[i] = new ArrayList();
            }
            return null;
        }
        pkixPolicyNode3.removeChild(pkixPolicyNode2);
        removePolicyNodeRecurse(array, pkixPolicyNode2);
        return pkixPolicyNode;
    }
    
    private static void removePolicyNodeRecurse(final List[] array, final PKIXPolicyNode pkixPolicyNode) {
        array[pkixPolicyNode.getDepth()].remove(pkixPolicyNode);
        if (pkixPolicyNode.hasChildren()) {
            final Iterator children = pkixPolicyNode.getChildren();
            while (children.hasNext()) {
                removePolicyNodeRecurse(array, children.next());
            }
        }
    }
    
    protected static void verifyX509Certificate(final X509Certificate x509Certificate, final PublicKey publicKey, final String s) throws GeneralSecurityException {
        if (s == null) {
            x509Certificate.verify(publicKey);
            return;
        }
        x509Certificate.verify(publicKey, s);
    }
}
