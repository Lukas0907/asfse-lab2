// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce.provider;

import java.security.cert.CertPathBuilderResult;
import org.bouncycastle.x509.PKIXAttrCertChecker;
import org.bouncycastle.asn1.x509.TargetInformation;
import java.security.cert.X509Extension;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.TrustAnchor;
import java.security.cert.CertPathValidator;
import java.security.cert.CertPathValidatorResult;
import java.security.Principal;
import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertPathBuilderException;
import java.security.InvalidAlgorithmParameterException;
import java.security.cert.CertPathParameters;
import org.bouncycastle.jcajce.PKIXExtendedBuilderParameters;
import java.security.cert.CertPathBuilder;
import org.bouncycastle.x509.X509CertStoreSelector;
import java.io.IOException;
import org.bouncycastle.jce.exception.ExtCertPathValidatorException;
import java.util.Collection;
import java.security.cert.CertSelector;
import org.bouncycastle.jcajce.PKIXCertStoreSelector;
import javax.security.auth.x500.X500Principal;
import java.security.cert.X509CertSelector;
import java.util.HashSet;
import java.security.cert.CertPath;
import java.security.PublicKey;
import java.security.cert.X509CRL;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.jcajce.util.JcaJceHelper;
import java.util.List;
import java.security.cert.X509Certificate;
import java.util.Date;
import org.bouncycastle.jcajce.PKIXExtendedParameters;
import org.bouncycastle.asn1.x509.DistributionPoint;
import java.util.Iterator;
import java.security.cert.CertPathValidatorException;
import java.util.Set;
import org.bouncycastle.x509.X509AttributeCertificate;
import org.bouncycastle.asn1.x509.Extension;

class RFC3281CertPathUtilities
{
    private static final String AUTHORITY_INFO_ACCESS;
    private static final String CRL_DISTRIBUTION_POINTS;
    private static final String NO_REV_AVAIL;
    private static final String TARGET_INFORMATION;
    
    static {
        TARGET_INFORMATION = Extension.targetInformation.getId();
        NO_REV_AVAIL = Extension.noRevAvail.getId();
        CRL_DISTRIBUTION_POINTS = Extension.cRLDistributionPoints.getId();
        AUTHORITY_INFO_ACCESS = Extension.authorityInfoAccess.getId();
    }
    
    protected static void additionalChecks(final X509AttributeCertificate x509AttributeCertificate, final Set set, final Set set2) throws CertPathValidatorException {
        for (final String str : set) {
            if (x509AttributeCertificate.getAttributes(str) == null) {
                continue;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Attribute certificate contains prohibited attribute: ");
            sb.append(str);
            sb.append(".");
            throw new CertPathValidatorException(sb.toString());
        }
        for (final String str2 : set2) {
            if (x509AttributeCertificate.getAttributes(str2) != null) {
                continue;
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Attribute certificate does not contain necessary attribute: ");
            sb2.append(str2);
            sb2.append(".");
            throw new CertPathValidatorException(sb2.toString());
        }
    }
    
    private static void checkCRL(final DistributionPoint distributionPoint, final X509AttributeCertificate x509AttributeCertificate, final PKIXExtendedParameters pkixExtendedParameters, final Date date, X509Certificate ex, final CertStatus certStatus, final ReasonsMask reasonsMask, final List list, final JcaJceHelper jcaJceHelper) throws AnnotatedException {
        if (x509AttributeCertificate.getExtensionValue(X509Extensions.NoRevAvail.getId()) != null) {
            return;
        }
        final Date date2 = new Date(System.currentTimeMillis());
        if (date.getTime() > date2.getTime()) {
            throw new AnnotatedException("Validation time is in future.");
        }
        final Iterator iterator = CertPathValidatorUtilities.getCompleteCRLs(distributionPoint, x509AttributeCertificate, date2, pkixExtendedParameters).iterator();
        int n = 1;
        int n2 = 0;
        ex = null;
        while (iterator.hasNext() && certStatus.getCertStatus() == 11 && !reasonsMask.isAllReasons()) {
            while (true) {
                while (true) {
                    Label_0334: {
                        try {
                            final X509CRL x509CRL = iterator.next();
                            final ReasonsMask processCRLD = RFC3280CertPathUtilities.processCRLD(x509CRL, distributionPoint);
                            if (processCRLD.hasNewReasons(reasonsMask)) {
                                final int n3 = n;
                                try {
                                    final PublicKey processCRLG = RFC3280CertPathUtilities.processCRLG(x509CRL, RFC3280CertPathUtilities.processCRLF(x509CRL, x509AttributeCertificate, null, null, pkixExtendedParameters, list, jcaJceHelper));
                                    if (!pkixExtendedParameters.isUseDeltasEnabled()) {
                                        break Label_0334;
                                    }
                                    final X509CRL processCRLH = RFC3280CertPathUtilities.processCRLH(CertPathValidatorUtilities.getDeltaCRLs(date2, x509CRL, pkixExtendedParameters.getCertStores(), pkixExtendedParameters.getCRLStores()), processCRLG);
                                    if (pkixExtendedParameters.getValidityModel() != n3 && x509AttributeCertificate.getNotAfter().getTime() < x509CRL.getThisUpdate().getTime()) {
                                        throw new AnnotatedException("No valid CRL for current time found.");
                                    }
                                    RFC3280CertPathUtilities.processCRLB1(distributionPoint, x509AttributeCertificate, x509CRL);
                                    RFC3280CertPathUtilities.processCRLB2(distributionPoint, x509AttributeCertificate, x509CRL);
                                    RFC3280CertPathUtilities.processCRLC(processCRLH, x509CRL, pkixExtendedParameters);
                                    RFC3280CertPathUtilities.processCRLI(date, processCRLH, x509AttributeCertificate, certStatus, pkixExtendedParameters);
                                    RFC3280CertPathUtilities.processCRLJ(date, x509CRL, x509AttributeCertificate, certStatus);
                                    if (certStatus.getCertStatus() == 8) {
                                        certStatus.setCertStatus(11);
                                    }
                                    reasonsMask.addReasons(processCRLD);
                                    n = (n2 = n3);
                                }
                                catch (AnnotatedException ex) {}
                            }
                        }
                        catch (AnnotatedException ex2) {}
                        break;
                    }
                    final X509CRL processCRLH = null;
                    continue;
                }
            }
        }
        if (n2 != 0) {
            return;
        }
        throw ex;
    }
    
    protected static void checkCRLs(final X509AttributeCertificate p0, final PKIXExtendedParameters p1, final X509Certificate p2, final Date p3, final List p4, final JcaJceHelper p5) throws CertPathValidatorException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   org/bouncycastle/jcajce/PKIXExtendedParameters.isRevocationEnabled:()Z
        //     4: ifeq            634
        //     7: aload_0        
        //     8: getstatic       org/bouncycastle/jce/provider/RFC3281CertPathUtilities.NO_REV_AVAIL:Ljava/lang/String;
        //    11: invokeinterface org/bouncycastle/x509/X509AttributeCertificate.getExtensionValue:(Ljava/lang/String;)[B
        //    16: ifnonnull       598
        //    19: aload_0        
        //    20: getstatic       org/bouncycastle/jce/provider/RFC3281CertPathUtilities.CRL_DISTRIBUTION_POINTS:Ljava/lang/String;
        //    23: invokestatic    org/bouncycastle/jce/provider/CertPathValidatorUtilities.getExtensionValue:(Ljava/security/cert/X509Extension;Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1Primitive;
        //    26: invokestatic    org/bouncycastle/asn1/x509/CRLDistPoint.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/CRLDistPoint;
        //    29: astore          9
        //    31: new             Ljava/util/ArrayList;
        //    34: dup            
        //    35: invokespecial   java/util/ArrayList.<init>:()V
        //    38: astore          10
        //    40: aload           10
        //    42: aload           9
        //    44: aload_1        
        //    45: invokevirtual   org/bouncycastle/jcajce/PKIXExtendedParameters.getNamedCRLStoreMap:()Ljava/util/Map;
        //    48: invokestatic    org/bouncycastle/jce/provider/CertPathValidatorUtilities.getAdditionalStoresFromCRLDistributionPoint:(Lorg/bouncycastle/asn1/x509/CRLDistPoint;Ljava/util/Map;)Ljava/util/List;
        //    51: invokeinterface java/util/List.addAll:(Ljava/util/Collection;)Z
        //    56: pop            
        //    57: new             Lorg/bouncycastle/jcajce/PKIXExtendedParameters$Builder;
        //    60: dup            
        //    61: aload_1        
        //    62: invokespecial   org/bouncycastle/jcajce/PKIXExtendedParameters$Builder.<init>:(Lorg/bouncycastle/jcajce/PKIXExtendedParameters;)V
        //    65: astore_1       
        //    66: aload           10
        //    68: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //    73: astore          11
        //    75: aload           11
        //    77: invokeinterface java/util/Iterator.hasNext:()Z
        //    82: ifeq            98
        //    85: aload_1        
        //    86: aload           10
        //    88: checkcast       Lorg/bouncycastle/jcajce/PKIXCRLStore;
        //    91: invokevirtual   org/bouncycastle/jcajce/PKIXExtendedParameters$Builder.addCRLStore:(Lorg/bouncycastle/jcajce/PKIXCRLStore;)Lorg/bouncycastle/jcajce/PKIXExtendedParameters$Builder;
        //    94: pop            
        //    95: goto            75
        //    98: aload_1        
        //    99: invokevirtual   org/bouncycastle/jcajce/PKIXExtendedParameters$Builder.build:()Lorg/bouncycastle/jcajce/PKIXExtendedParameters;
        //   102: astore          12
        //   104: new             Lorg/bouncycastle/jce/provider/CertStatus;
        //   107: dup            
        //   108: invokespecial   org/bouncycastle/jce/provider/CertStatus.<init>:()V
        //   111: astore          10
        //   113: new             Lorg/bouncycastle/jce/provider/ReasonsMask;
        //   116: dup            
        //   117: invokespecial   org/bouncycastle/jce/provider/ReasonsMask.<init>:()V
        //   120: astore          11
        //   122: bipush          11
        //   124: istore          6
        //   126: aload           9
        //   128: ifnull          253
        //   131: aload           9
        //   133: invokevirtual   org/bouncycastle/asn1/x509/CRLDistPoint.getDistributionPoints:()[Lorg/bouncycastle/asn1/x509/DistributionPoint;
        //   136: astore_1       
        //   137: iconst_0       
        //   138: istore          8
        //   140: iload           8
        //   142: istore          7
        //   144: iload           8
        //   146: aload_1        
        //   147: arraylength    
        //   148: if_icmpge       219
        //   151: aload           10
        //   153: invokevirtual   org/bouncycastle/jce/provider/CertStatus.getCertStatus:()I
        //   156: iload           6
        //   158: if_icmpne       219
        //   161: aload           11
        //   163: invokevirtual   org/bouncycastle/jce/provider/ReasonsMask.isAllReasons:()Z
        //   166: ifne            219
        //   169: aload           12
        //   171: invokevirtual   org/bouncycastle/jcajce/PKIXExtendedParameters.clone:()Ljava/lang/Object;
        //   174: checkcast       Lorg/bouncycastle/jcajce/PKIXExtendedParameters;
        //   177: astore          9
        //   179: aload_1        
        //   180: iload           8
        //   182: aaload         
        //   183: astore          13
        //   185: aload           13
        //   187: aload_0        
        //   188: aload           9
        //   190: aload_3        
        //   191: aload_2        
        //   192: aload           10
        //   194: aload           11
        //   196: aload           4
        //   198: aload           5
        //   200: invokestatic    org/bouncycastle/jce/provider/RFC3281CertPathUtilities.checkCRL:(Lorg/bouncycastle/asn1/x509/DistributionPoint;Lorg/bouncycastle/x509/X509AttributeCertificate;Lorg/bouncycastle/jcajce/PKIXExtendedParameters;Ljava/util/Date;Ljava/security/cert/X509Certificate;Lorg/bouncycastle/jce/provider/CertStatus;Lorg/bouncycastle/jce/provider/ReasonsMask;Ljava/util/List;Lorg/bouncycastle/jcajce/util/JcaJceHelper;)V
        //   203: iload           8
        //   205: iconst_1       
        //   206: iadd           
        //   207: istore          8
        //   209: iconst_1       
        //   210: istore          7
        //   212: goto            144
        //   215: astore_1       
        //   216: goto            225
        //   219: aconst_null    
        //   220: astore_1       
        //   221: goto            262
        //   224: astore_1       
        //   225: new             Lorg/bouncycastle/jce/provider/AnnotatedException;
        //   228: dup            
        //   229: ldc_w           "No valid CRL for distribution point found."
        //   232: aload_1        
        //   233: invokespecial   org/bouncycastle/jce/provider/AnnotatedException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   236: astore_1       
        //   237: goto            262
        //   240: astore_0       
        //   241: new             Lorg/bouncycastle/jce/exception/ExtCertPathValidatorException;
        //   244: dup            
        //   245: ldc_w           "Distribution points could not be read."
        //   248: aload_0        
        //   249: invokespecial   org/bouncycastle/jce/exception/ExtCertPathValidatorException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   252: athrow         
        //   253: bipush          11
        //   255: istore          6
        //   257: aconst_null    
        //   258: astore_1       
        //   259: iconst_0       
        //   260: istore          7
        //   262: aload_1        
        //   263: astore          9
        //   265: iload           7
        //   267: istore          8
        //   269: aload           10
        //   271: invokevirtual   org/bouncycastle/jce/provider/CertStatus.getCertStatus:()I
        //   274: iload           6
        //   276: if_icmpne       418
        //   279: aload_1        
        //   280: astore          9
        //   282: iload           7
        //   284: istore          8
        //   286: aload           11
        //   288: invokevirtual   org/bouncycastle/jce/provider/ReasonsMask.isAllReasons:()Z
        //   291: ifne            418
        //   294: new             Lorg/bouncycastle/asn1/ASN1InputStream;
        //   297: dup            
        //   298: aload_0        
        //   299: invokeinterface org/bouncycastle/x509/X509AttributeCertificate.getIssuer:()Lorg/bouncycastle/x509/AttributeCertificateIssuer;
        //   304: invokevirtual   org/bouncycastle/x509/AttributeCertificateIssuer.getPrincipals:()[Ljava/security/Principal;
        //   307: iconst_0       
        //   308: aaload         
        //   309: checkcast       Ljavax/security/auth/x500/X500Principal;
        //   312: invokevirtual   javax/security/auth/x500/X500Principal.getEncoded:()[B
        //   315: invokespecial   org/bouncycastle/asn1/ASN1InputStream.<init>:([B)V
        //   318: invokevirtual   org/bouncycastle/asn1/ASN1InputStream.readObject:()Lorg/bouncycastle/asn1/ASN1Primitive;
        //   321: astore          9
        //   323: new             Lorg/bouncycastle/asn1/x509/DistributionPoint;
        //   326: dup            
        //   327: new             Lorg/bouncycastle/asn1/x509/DistributionPointName;
        //   330: dup            
        //   331: iconst_0       
        //   332: new             Lorg/bouncycastle/asn1/x509/GeneralNames;
        //   335: dup            
        //   336: new             Lorg/bouncycastle/asn1/x509/GeneralName;
        //   339: dup            
        //   340: iconst_4       
        //   341: aload           9
        //   343: invokespecial   org/bouncycastle/asn1/x509/GeneralName.<init>:(ILorg/bouncycastle/asn1/ASN1Encodable;)V
        //   346: invokespecial   org/bouncycastle/asn1/x509/GeneralNames.<init>:(Lorg/bouncycastle/asn1/x509/GeneralName;)V
        //   349: invokespecial   org/bouncycastle/asn1/x509/DistributionPointName.<init>:(ILorg/bouncycastle/asn1/ASN1Encodable;)V
        //   352: aconst_null    
        //   353: aconst_null    
        //   354: invokespecial   org/bouncycastle/asn1/x509/DistributionPoint.<init>:(Lorg/bouncycastle/asn1/x509/DistributionPointName;Lorg/bouncycastle/asn1/x509/ReasonFlags;Lorg/bouncycastle/asn1/x509/GeneralNames;)V
        //   357: aload_0        
        //   358: aload           12
        //   360: invokevirtual   org/bouncycastle/jcajce/PKIXExtendedParameters.clone:()Ljava/lang/Object;
        //   363: checkcast       Lorg/bouncycastle/jcajce/PKIXExtendedParameters;
        //   366: aload_3        
        //   367: aload_2        
        //   368: aload           10
        //   370: aload           11
        //   372: aload           4
        //   374: aload           5
        //   376: invokestatic    org/bouncycastle/jce/provider/RFC3281CertPathUtilities.checkCRL:(Lorg/bouncycastle/asn1/x509/DistributionPoint;Lorg/bouncycastle/x509/X509AttributeCertificate;Lorg/bouncycastle/jcajce/PKIXExtendedParameters;Ljava/util/Date;Ljava/security/cert/X509Certificate;Lorg/bouncycastle/jce/provider/CertStatus;Lorg/bouncycastle/jce/provider/ReasonsMask;Ljava/util/List;Lorg/bouncycastle/jcajce/util/JcaJceHelper;)V
        //   379: iconst_1       
        //   380: istore          8
        //   382: aload_1        
        //   383: astore          9
        //   385: goto            418
        //   388: astore_0       
        //   389: new             Lorg/bouncycastle/jce/provider/AnnotatedException;
        //   392: dup            
        //   393: ldc_w           "Issuer from certificate for CRL could not be reencoded."
        //   396: aload_0        
        //   397: invokespecial   org/bouncycastle/jce/provider/AnnotatedException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   400: athrow         
        //   401: new             Lorg/bouncycastle/jce/provider/AnnotatedException;
        //   404: dup            
        //   405: ldc_w           "No valid CRL for distribution point found."
        //   408: aload_0        
        //   409: invokespecial   org/bouncycastle/jce/provider/AnnotatedException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   412: astore          9
        //   414: iload           7
        //   416: istore          8
        //   418: iload           8
        //   420: ifeq            559
        //   423: aload           10
        //   425: invokevirtual   org/bouncycastle/jce/provider/CertStatus.getCertStatus:()I
        //   428: iload           6
        //   430: if_icmpne       480
        //   433: aload           11
        //   435: invokevirtual   org/bouncycastle/jce/provider/ReasonsMask.isAllReasons:()Z
        //   438: ifne            458
        //   441: aload           10
        //   443: invokevirtual   org/bouncycastle/jce/provider/CertStatus.getCertStatus:()I
        //   446: iload           6
        //   448: if_icmpne       458
        //   451: aload           10
        //   453: bipush          12
        //   455: invokevirtual   org/bouncycastle/jce/provider/CertStatus.setCertStatus:(I)V
        //   458: aload           10
        //   460: invokevirtual   org/bouncycastle/jce/provider/CertStatus.getCertStatus:()I
        //   463: bipush          12
        //   465: if_icmpeq       469
        //   468: return         
        //   469: new             Ljava/security/cert/CertPathValidatorException;
        //   472: dup            
        //   473: ldc_w           "Attribute certificate status could not be determined."
        //   476: invokespecial   java/security/cert/CertPathValidatorException.<init>:(Ljava/lang/String;)V
        //   479: athrow         
        //   480: new             Ljava/lang/StringBuilder;
        //   483: dup            
        //   484: invokespecial   java/lang/StringBuilder.<init>:()V
        //   487: astore_0       
        //   488: aload_0        
        //   489: ldc_w           "Attribute certificate revocation after "
        //   492: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   495: pop            
        //   496: aload_0        
        //   497: aload           10
        //   499: invokevirtual   org/bouncycastle/jce/provider/CertStatus.getRevocationDate:()Ljava/util/Date;
        //   502: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   505: pop            
        //   506: aload_0        
        //   507: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   510: astore_0       
        //   511: new             Ljava/lang/StringBuilder;
        //   514: dup            
        //   515: invokespecial   java/lang/StringBuilder.<init>:()V
        //   518: astore_1       
        //   519: aload_1        
        //   520: aload_0        
        //   521: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   524: pop            
        //   525: aload_1        
        //   526: ldc_w           ", reason: "
        //   529: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   532: pop            
        //   533: aload_1        
        //   534: getstatic       org/bouncycastle/jce/provider/RFC3280CertPathUtilities.crlReasons:[Ljava/lang/String;
        //   537: aload           10
        //   539: invokevirtual   org/bouncycastle/jce/provider/CertStatus.getCertStatus:()I
        //   542: aaload         
        //   543: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   546: pop            
        //   547: new             Ljava/security/cert/CertPathValidatorException;
        //   550: dup            
        //   551: aload_1        
        //   552: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   555: invokespecial   java/security/cert/CertPathValidatorException.<init>:(Ljava/lang/String;)V
        //   558: athrow         
        //   559: new             Lorg/bouncycastle/jce/exception/ExtCertPathValidatorException;
        //   562: dup            
        //   563: ldc_w           "No valid CRL found."
        //   566: aload           9
        //   568: invokespecial   org/bouncycastle/jce/exception/ExtCertPathValidatorException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   571: athrow         
        //   572: astore_0       
        //   573: new             Ljava/security/cert/CertPathValidatorException;
        //   576: dup            
        //   577: ldc_w           "No additional CRL locations could be decoded from CRL distribution point extension."
        //   580: aload_0        
        //   581: invokespecial   java/security/cert/CertPathValidatorException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   584: athrow         
        //   585: astore_0       
        //   586: new             Ljava/security/cert/CertPathValidatorException;
        //   589: dup            
        //   590: ldc_w           "CRL distribution point extension could not be read."
        //   593: aload_0        
        //   594: invokespecial   java/security/cert/CertPathValidatorException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   597: athrow         
        //   598: aload_0        
        //   599: getstatic       org/bouncycastle/jce/provider/RFC3281CertPathUtilities.CRL_DISTRIBUTION_POINTS:Ljava/lang/String;
        //   602: invokeinterface org/bouncycastle/x509/X509AttributeCertificate.getExtensionValue:(Ljava/lang/String;)[B
        //   607: ifnonnull       623
        //   610: aload_0        
        //   611: getstatic       org/bouncycastle/jce/provider/RFC3281CertPathUtilities.AUTHORITY_INFO_ACCESS:Ljava/lang/String;
        //   614: invokeinterface org/bouncycastle/x509/X509AttributeCertificate.getExtensionValue:(Ljava/lang/String;)[B
        //   619: ifnonnull       623
        //   622: return         
        //   623: new             Ljava/security/cert/CertPathValidatorException;
        //   626: dup            
        //   627: ldc_w           "No rev avail extension is set, but also an AC revocation pointer."
        //   630: invokespecial   java/security/cert/CertPathValidatorException.<init>:(Ljava/lang/String;)V
        //   633: athrow         
        //   634: return         
        //   635: astore_0       
        //   636: goto            401
        //    Exceptions:
        //  throws java.security.cert.CertPathValidatorException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                              
        //  -----  -----  -----  -----  --------------------------------------------------
        //  19     31     585    598    Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  40     57     572    585    Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  131    137    240    253    Ljava/lang/Exception;
        //  144    179    224    225    Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  185    203    215    219    Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  294    323    388    401    Ljava/lang/Exception;
        //  294    323    635    418    Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  323    379    635    418    Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  389    401    635    418    Lorg/bouncycastle/jce/provider/AnnotatedException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0401:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    protected static CertPath processAttrCert1(X509AttributeCertificate build, final PKIXExtendedParameters pkixExtendedParameters) throws CertPathValidatorException {
        final HashSet<X509Certificate> set = new HashSet<X509Certificate>();
        final Principal[] issuer = build.getHolder().getIssuer();
        final int n = 0;
        if (issuer != null) {
            final X509CertSelector x509CertSelector = new X509CertSelector();
            x509CertSelector.setSerialNumber(build.getHolder().getSerialNumber());
            final Principal[] issuer2 = build.getHolder().getIssuer();
            int i = 0;
            while (i < issuer2.length) {
                try {
                    if (issuer2[i] instanceof X500Principal) {
                        x509CertSelector.setIssuer(((X500Principal)issuer2[i]).getEncoded());
                    }
                    set.addAll((Collection<?>)CertPathValidatorUtilities.findCertificates(new PKIXCertStoreSelector.Builder(x509CertSelector).build(), pkixExtendedParameters.getCertStores()));
                    ++i;
                    continue;
                }
                catch (IOException ex) {
                    throw new ExtCertPathValidatorException("Unable to encode X500 principal.", ex);
                }
                catch (AnnotatedException ex2) {
                    throw new ExtCertPathValidatorException("Public key certificate for attribute certificate cannot be searched.", ex2);
                }
                break;
            }
            if (set.isEmpty()) {
                throw new CertPathValidatorException("Public key certificate specified in base certificate ID for attribute certificate cannot be found.");
            }
        }
        if (build.getHolder().getEntityNames() != null) {
            final X509CertStoreSelector x509CertStoreSelector = new X509CertStoreSelector();
            final Principal[] entityNames = build.getHolder().getEntityNames();
            int j = n;
            while (j < entityNames.length) {
                try {
                    if (entityNames[j] instanceof X500Principal) {
                        x509CertStoreSelector.setIssuer(((X500Principal)entityNames[j]).getEncoded());
                    }
                    set.addAll((Collection<?>)CertPathValidatorUtilities.findCertificates(new PKIXCertStoreSelector.Builder(x509CertStoreSelector).build(), pkixExtendedParameters.getCertStores()));
                    ++j;
                    continue;
                }
                catch (IOException ex3) {
                    throw new ExtCertPathValidatorException("Unable to encode X500 principal.", ex3);
                }
                catch (AnnotatedException ex4) {
                    throw new ExtCertPathValidatorException("Public key certificate for attribute certificate cannot be searched.", ex4);
                }
                break;
            }
            if (set.isEmpty()) {
                throw new CertPathValidatorException("Public key certificate specified in entity name for attribute certificate cannot be found.");
            }
        }
        final PKIXExtendedParameters.Builder builder = new PKIXExtendedParameters.Builder(pkixExtendedParameters);
        final Iterator<Object> iterator = set.iterator();
        Object o = null;
        build = null;
        while (iterator.hasNext()) {
            final X509CertStoreSelector x509CertStoreSelector2 = new X509CertStoreSelector();
            x509CertStoreSelector2.setCertificate(iterator.next());
            builder.setTargetConstraints(new PKIXCertStoreSelector.Builder(x509CertStoreSelector2).build());
            try {
                final CertPathBuilder instance = CertPathBuilder.getInstance("PKIX", "BC");
                try {
                    build = (X509AttributeCertificate)instance.build(new PKIXExtendedBuilderParameters.Builder(builder.build()).build());
                }
                catch (InvalidAlgorithmParameterException ex5) {
                    throw new RuntimeException(ex5.getMessage());
                }
                catch (CertPathBuilderException ex6) {
                    o = new ExtCertPathValidatorException("Certification path for public key certificate of attribute certificate could not be build.", ex6);
                }
            }
            catch (NoSuchAlgorithmException ex7) {
                throw new ExtCertPathValidatorException("Support class could not be created.", ex7);
            }
            catch (NoSuchProviderException ex8) {
                throw new ExtCertPathValidatorException("Support class could not be created.", ex8);
            }
            break;
        }
        if (o == null) {
            return ((CertPathBuilderResult)build).getCertPath();
        }
        throw o;
    }
    
    protected static CertPathValidatorResult processAttrCert2(final CertPath certPath, final PKIXExtendedParameters params) throws CertPathValidatorException {
        try {
            final CertPathValidator instance = CertPathValidator.getInstance("PKIX", "BC");
            try {
                return instance.validate(certPath, params);
            }
            catch (InvalidAlgorithmParameterException ex) {
                throw new RuntimeException(ex.getMessage());
            }
            catch (CertPathValidatorException ex2) {
                throw new ExtCertPathValidatorException("Certification path for issuer certificate of attribute certificate could not be validated.", ex2);
            }
        }
        catch (NoSuchAlgorithmException ex3) {
            throw new ExtCertPathValidatorException("Support class could not be created.", ex3);
        }
        catch (NoSuchProviderException ex4) {
            throw new ExtCertPathValidatorException("Support class could not be created.", ex4);
        }
    }
    
    protected static void processAttrCert3(final X509Certificate x509Certificate, final PKIXExtendedParameters pkixExtendedParameters) throws CertPathValidatorException {
        if (x509Certificate.getKeyUsage() != null && !x509Certificate.getKeyUsage()[0] && !x509Certificate.getKeyUsage()[1]) {
            throw new CertPathValidatorException("Attribute certificate issuer public key cannot be used to validate digital signatures.");
        }
        if (x509Certificate.getBasicConstraints() == -1) {
            return;
        }
        throw new CertPathValidatorException("Attribute certificate issuer is also a public key certificate issuer.");
    }
    
    protected static void processAttrCert4(final X509Certificate x509Certificate, final Set set) throws CertPathValidatorException {
        final Iterator<TrustAnchor> iterator = set.iterator();
        boolean b = false;
        while (iterator.hasNext()) {
            final TrustAnchor trustAnchor = iterator.next();
            if (x509Certificate.getSubjectX500Principal().getName("RFC2253").equals(trustAnchor.getCAName()) || x509Certificate.equals(trustAnchor.getTrustedCert())) {
                b = true;
            }
        }
        if (b) {
            return;
        }
        throw new CertPathValidatorException("Attribute certificate issuer is not directly trusted.");
    }
    
    protected static void processAttrCert5(final X509AttributeCertificate x509AttributeCertificate, final PKIXExtendedParameters pkixExtendedParameters) throws CertPathValidatorException {
        try {
            x509AttributeCertificate.checkValidity(CertPathValidatorUtilities.getValidDate(pkixExtendedParameters));
        }
        catch (CertificateNotYetValidException ex) {
            throw new ExtCertPathValidatorException("Attribute certificate is not valid.", ex);
        }
        catch (CertificateExpiredException ex2) {
            throw new ExtCertPathValidatorException("Attribute certificate is not valid.", ex2);
        }
    }
    
    protected static void processAttrCert7(final X509AttributeCertificate x509AttributeCertificate, final CertPath certPath, final CertPath certPath2, final PKIXExtendedParameters pkixExtendedParameters, final Set set) throws CertPathValidatorException {
        final Set<String> criticalExtensionOIDs = x509AttributeCertificate.getCriticalExtensionOIDs();
        if (criticalExtensionOIDs.contains(RFC3281CertPathUtilities.TARGET_INFORMATION)) {
            try {
                TargetInformation.getInstance(CertPathValidatorUtilities.getExtensionValue(x509AttributeCertificate, RFC3281CertPathUtilities.TARGET_INFORMATION));
            }
            catch (IllegalArgumentException ex) {
                throw new ExtCertPathValidatorException("Target information extension could not be read.", ex);
            }
            catch (AnnotatedException ex2) {
                throw new ExtCertPathValidatorException("Target information extension could not be read.", ex2);
            }
        }
        criticalExtensionOIDs.remove(RFC3281CertPathUtilities.TARGET_INFORMATION);
        final Iterator<PKIXAttrCertChecker> iterator = set.iterator();
        while (iterator.hasNext()) {
            iterator.next().check(x509AttributeCertificate, certPath, certPath2, criticalExtensionOIDs);
        }
        if (criticalExtensionOIDs.isEmpty()) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Attribute certificate contains unsupported critical extensions: ");
        sb.append(criticalExtensionOIDs);
        throw new CertPathValidatorException(sb.toString());
    }
}
