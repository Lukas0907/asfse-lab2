// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce;

import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.asn1.cryptopro.ECGOST3410NamedCurves;
import java.util.Enumeration;

public class ECGOST3410NamedCurveTable
{
    public static Enumeration getNames() {
        return ECGOST3410NamedCurves.getNames();
    }
    
    public static ECNamedCurveParameterSpec getParameterSpec(final String s) {
        Label_0026: {
            ECDomainParameters ecDomainParameters;
            if ((ecDomainParameters = ECGOST3410NamedCurves.getByName(s)) != null) {
                break Label_0026;
            }
            try {
                ecDomainParameters = ECGOST3410NamedCurves.getByOID(new ASN1ObjectIdentifier(s));
                if (ecDomainParameters == null) {
                    return null;
                }
                return new ECNamedCurveParameterSpec(s, ecDomainParameters.getCurve(), ecDomainParameters.getG(), ecDomainParameters.getN(), ecDomainParameters.getH(), ecDomainParameters.getSeed());
            }
            catch (IllegalArgumentException ex) {
                return null;
            }
        }
    }
}
