// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce;

import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.KeyFactory;
import java.security.spec.X509EncodedKeySpec;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.ASN1InputStream;
import java.security.GeneralSecurityException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.PSSParameterSpec;
import java.security.AlgorithmParameters;
import java.security.Signature;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.pkcs.RSASSAPSSparams;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x509.X509Name;
import java.security.SignatureException;
import java.security.InvalidKeyException;
import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import org.bouncycastle.asn1.ASN1Set;
import java.security.PublicKey;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.teletrust.TeleTrusTObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.util.HashSet;
import java.util.Set;
import java.util.Hashtable;
import org.bouncycastle.asn1.pkcs.CertificationRequest;

public class PKCS10CertificationRequest extends CertificationRequest
{
    private static Hashtable algorithms;
    private static Hashtable keyAlgorithms;
    private static Set noParams;
    private static Hashtable oids;
    private static Hashtable params;
    
    static {
        PKCS10CertificationRequest.algorithms = new Hashtable();
        PKCS10CertificationRequest.params = new Hashtable();
        PKCS10CertificationRequest.keyAlgorithms = new Hashtable();
        PKCS10CertificationRequest.oids = new Hashtable();
        PKCS10CertificationRequest.noParams = new HashSet();
        PKCS10CertificationRequest.algorithms.put("MD2WITHRSAENCRYPTION", new ASN1ObjectIdentifier("1.2.840.113549.1.1.2"));
        PKCS10CertificationRequest.algorithms.put("MD2WITHRSA", new ASN1ObjectIdentifier("1.2.840.113549.1.1.2"));
        PKCS10CertificationRequest.algorithms.put("MD5WITHRSAENCRYPTION", new ASN1ObjectIdentifier("1.2.840.113549.1.1.4"));
        PKCS10CertificationRequest.algorithms.put("MD5WITHRSA", new ASN1ObjectIdentifier("1.2.840.113549.1.1.4"));
        PKCS10CertificationRequest.algorithms.put("RSAWITHMD5", new ASN1ObjectIdentifier("1.2.840.113549.1.1.4"));
        PKCS10CertificationRequest.algorithms.put("SHA1WITHRSAENCRYPTION", new ASN1ObjectIdentifier("1.2.840.113549.1.1.5"));
        PKCS10CertificationRequest.algorithms.put("SHA1WITHRSA", new ASN1ObjectIdentifier("1.2.840.113549.1.1.5"));
        PKCS10CertificationRequest.algorithms.put("SHA224WITHRSAENCRYPTION", PKCSObjectIdentifiers.sha224WithRSAEncryption);
        PKCS10CertificationRequest.algorithms.put("SHA224WITHRSA", PKCSObjectIdentifiers.sha224WithRSAEncryption);
        PKCS10CertificationRequest.algorithms.put("SHA256WITHRSAENCRYPTION", PKCSObjectIdentifiers.sha256WithRSAEncryption);
        PKCS10CertificationRequest.algorithms.put("SHA256WITHRSA", PKCSObjectIdentifiers.sha256WithRSAEncryption);
        PKCS10CertificationRequest.algorithms.put("SHA384WITHRSAENCRYPTION", PKCSObjectIdentifiers.sha384WithRSAEncryption);
        PKCS10CertificationRequest.algorithms.put("SHA384WITHRSA", PKCSObjectIdentifiers.sha384WithRSAEncryption);
        PKCS10CertificationRequest.algorithms.put("SHA512WITHRSAENCRYPTION", PKCSObjectIdentifiers.sha512WithRSAEncryption);
        PKCS10CertificationRequest.algorithms.put("SHA512WITHRSA", PKCSObjectIdentifiers.sha512WithRSAEncryption);
        PKCS10CertificationRequest.algorithms.put("SHA1WITHRSAANDMGF1", PKCSObjectIdentifiers.id_RSASSA_PSS);
        PKCS10CertificationRequest.algorithms.put("SHA224WITHRSAANDMGF1", PKCSObjectIdentifiers.id_RSASSA_PSS);
        PKCS10CertificationRequest.algorithms.put("SHA256WITHRSAANDMGF1", PKCSObjectIdentifiers.id_RSASSA_PSS);
        PKCS10CertificationRequest.algorithms.put("SHA384WITHRSAANDMGF1", PKCSObjectIdentifiers.id_RSASSA_PSS);
        PKCS10CertificationRequest.algorithms.put("SHA512WITHRSAANDMGF1", PKCSObjectIdentifiers.id_RSASSA_PSS);
        PKCS10CertificationRequest.algorithms.put("RSAWITHSHA1", new ASN1ObjectIdentifier("1.2.840.113549.1.1.5"));
        PKCS10CertificationRequest.algorithms.put("RIPEMD128WITHRSAENCRYPTION", TeleTrusTObjectIdentifiers.rsaSignatureWithripemd128);
        PKCS10CertificationRequest.algorithms.put("RIPEMD128WITHRSA", TeleTrusTObjectIdentifiers.rsaSignatureWithripemd128);
        PKCS10CertificationRequest.algorithms.put("RIPEMD160WITHRSAENCRYPTION", TeleTrusTObjectIdentifiers.rsaSignatureWithripemd160);
        PKCS10CertificationRequest.algorithms.put("RIPEMD160WITHRSA", TeleTrusTObjectIdentifiers.rsaSignatureWithripemd160);
        PKCS10CertificationRequest.algorithms.put("RIPEMD256WITHRSAENCRYPTION", TeleTrusTObjectIdentifiers.rsaSignatureWithripemd256);
        PKCS10CertificationRequest.algorithms.put("RIPEMD256WITHRSA", TeleTrusTObjectIdentifiers.rsaSignatureWithripemd256);
        PKCS10CertificationRequest.algorithms.put("SHA1WITHDSA", new ASN1ObjectIdentifier("1.2.840.10040.4.3"));
        PKCS10CertificationRequest.algorithms.put("DSAWITHSHA1", new ASN1ObjectIdentifier("1.2.840.10040.4.3"));
        PKCS10CertificationRequest.algorithms.put("SHA224WITHDSA", NISTObjectIdentifiers.dsa_with_sha224);
        PKCS10CertificationRequest.algorithms.put("SHA256WITHDSA", NISTObjectIdentifiers.dsa_with_sha256);
        PKCS10CertificationRequest.algorithms.put("SHA384WITHDSA", NISTObjectIdentifiers.dsa_with_sha384);
        PKCS10CertificationRequest.algorithms.put("SHA512WITHDSA", NISTObjectIdentifiers.dsa_with_sha512);
        PKCS10CertificationRequest.algorithms.put("SHA1WITHECDSA", X9ObjectIdentifiers.ecdsa_with_SHA1);
        PKCS10CertificationRequest.algorithms.put("SHA224WITHECDSA", X9ObjectIdentifiers.ecdsa_with_SHA224);
        PKCS10CertificationRequest.algorithms.put("SHA256WITHECDSA", X9ObjectIdentifiers.ecdsa_with_SHA256);
        PKCS10CertificationRequest.algorithms.put("SHA384WITHECDSA", X9ObjectIdentifiers.ecdsa_with_SHA384);
        PKCS10CertificationRequest.algorithms.put("SHA512WITHECDSA", X9ObjectIdentifiers.ecdsa_with_SHA512);
        PKCS10CertificationRequest.algorithms.put("ECDSAWITHSHA1", X9ObjectIdentifiers.ecdsa_with_SHA1);
        PKCS10CertificationRequest.algorithms.put("GOST3411WITHGOST3410", CryptoProObjectIdentifiers.gostR3411_94_with_gostR3410_94);
        PKCS10CertificationRequest.algorithms.put("GOST3410WITHGOST3411", CryptoProObjectIdentifiers.gostR3411_94_with_gostR3410_94);
        PKCS10CertificationRequest.algorithms.put("GOST3411WITHECGOST3410", CryptoProObjectIdentifiers.gostR3411_94_with_gostR3410_2001);
        PKCS10CertificationRequest.algorithms.put("GOST3411WITHECGOST3410-2001", CryptoProObjectIdentifiers.gostR3411_94_with_gostR3410_2001);
        PKCS10CertificationRequest.algorithms.put("GOST3411WITHGOST3410-2001", CryptoProObjectIdentifiers.gostR3411_94_with_gostR3410_2001);
        PKCS10CertificationRequest.oids.put(new ASN1ObjectIdentifier("1.2.840.113549.1.1.5"), "SHA1WITHRSA");
        PKCS10CertificationRequest.oids.put(PKCSObjectIdentifiers.sha224WithRSAEncryption, "SHA224WITHRSA");
        PKCS10CertificationRequest.oids.put(PKCSObjectIdentifiers.sha256WithRSAEncryption, "SHA256WITHRSA");
        PKCS10CertificationRequest.oids.put(PKCSObjectIdentifiers.sha384WithRSAEncryption, "SHA384WITHRSA");
        PKCS10CertificationRequest.oids.put(PKCSObjectIdentifiers.sha512WithRSAEncryption, "SHA512WITHRSA");
        PKCS10CertificationRequest.oids.put(CryptoProObjectIdentifiers.gostR3411_94_with_gostR3410_94, "GOST3411WITHGOST3410");
        PKCS10CertificationRequest.oids.put(CryptoProObjectIdentifiers.gostR3411_94_with_gostR3410_2001, "GOST3411WITHECGOST3410");
        PKCS10CertificationRequest.oids.put(new ASN1ObjectIdentifier("1.2.840.113549.1.1.4"), "MD5WITHRSA");
        PKCS10CertificationRequest.oids.put(new ASN1ObjectIdentifier("1.2.840.113549.1.1.2"), "MD2WITHRSA");
        PKCS10CertificationRequest.oids.put(new ASN1ObjectIdentifier("1.2.840.10040.4.3"), "SHA1WITHDSA");
        PKCS10CertificationRequest.oids.put(X9ObjectIdentifiers.ecdsa_with_SHA1, "SHA1WITHECDSA");
        PKCS10CertificationRequest.oids.put(X9ObjectIdentifiers.ecdsa_with_SHA224, "SHA224WITHECDSA");
        PKCS10CertificationRequest.oids.put(X9ObjectIdentifiers.ecdsa_with_SHA256, "SHA256WITHECDSA");
        PKCS10CertificationRequest.oids.put(X9ObjectIdentifiers.ecdsa_with_SHA384, "SHA384WITHECDSA");
        PKCS10CertificationRequest.oids.put(X9ObjectIdentifiers.ecdsa_with_SHA512, "SHA512WITHECDSA");
        PKCS10CertificationRequest.oids.put(OIWObjectIdentifiers.sha1WithRSA, "SHA1WITHRSA");
        PKCS10CertificationRequest.oids.put(OIWObjectIdentifiers.dsaWithSHA1, "SHA1WITHDSA");
        PKCS10CertificationRequest.oids.put(NISTObjectIdentifiers.dsa_with_sha224, "SHA224WITHDSA");
        PKCS10CertificationRequest.oids.put(NISTObjectIdentifiers.dsa_with_sha256, "SHA256WITHDSA");
        PKCS10CertificationRequest.keyAlgorithms.put(PKCSObjectIdentifiers.rsaEncryption, "RSA");
        PKCS10CertificationRequest.keyAlgorithms.put(X9ObjectIdentifiers.id_dsa, "DSA");
        PKCS10CertificationRequest.noParams.add(X9ObjectIdentifiers.ecdsa_with_SHA1);
        PKCS10CertificationRequest.noParams.add(X9ObjectIdentifiers.ecdsa_with_SHA224);
        PKCS10CertificationRequest.noParams.add(X9ObjectIdentifiers.ecdsa_with_SHA256);
        PKCS10CertificationRequest.noParams.add(X9ObjectIdentifiers.ecdsa_with_SHA384);
        PKCS10CertificationRequest.noParams.add(X9ObjectIdentifiers.ecdsa_with_SHA512);
        PKCS10CertificationRequest.noParams.add(X9ObjectIdentifiers.id_dsa_with_sha1);
        PKCS10CertificationRequest.noParams.add(NISTObjectIdentifiers.dsa_with_sha224);
        PKCS10CertificationRequest.noParams.add(NISTObjectIdentifiers.dsa_with_sha256);
        PKCS10CertificationRequest.noParams.add(CryptoProObjectIdentifiers.gostR3411_94_with_gostR3410_94);
        PKCS10CertificationRequest.noParams.add(CryptoProObjectIdentifiers.gostR3411_94_with_gostR3410_2001);
        PKCS10CertificationRequest.params.put("SHA1WITHRSAANDMGF1", creatPSSParams(new AlgorithmIdentifier(OIWObjectIdentifiers.idSHA1, DERNull.INSTANCE), 20));
        PKCS10CertificationRequest.params.put("SHA224WITHRSAANDMGF1", creatPSSParams(new AlgorithmIdentifier(NISTObjectIdentifiers.id_sha224, DERNull.INSTANCE), 28));
        PKCS10CertificationRequest.params.put("SHA256WITHRSAANDMGF1", creatPSSParams(new AlgorithmIdentifier(NISTObjectIdentifiers.id_sha256, DERNull.INSTANCE), 32));
        PKCS10CertificationRequest.params.put("SHA384WITHRSAANDMGF1", creatPSSParams(new AlgorithmIdentifier(NISTObjectIdentifiers.id_sha384, DERNull.INSTANCE), 48));
        PKCS10CertificationRequest.params.put("SHA512WITHRSAANDMGF1", creatPSSParams(new AlgorithmIdentifier(NISTObjectIdentifiers.id_sha512, DERNull.INSTANCE), 64));
    }
    
    public PKCS10CertificationRequest(final String s, final X500Principal x500Principal, final PublicKey publicKey, final ASN1Set set, final PrivateKey privateKey) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {
        this(s, convertName(x500Principal), publicKey, set, privateKey, "BC");
    }
    
    public PKCS10CertificationRequest(final String s, final X500Principal x500Principal, final PublicKey publicKey, final ASN1Set set, final PrivateKey privateKey, final String s2) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {
        this(s, convertName(x500Principal), publicKey, set, privateKey, s2);
    }
    
    public PKCS10CertificationRequest(final String s, final X509Name x509Name, final PublicKey publicKey, final ASN1Set set, final PrivateKey privateKey) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {
        this(s, x509Name, publicKey, set, privateKey, "BC");
    }
    
    public PKCS10CertificationRequest(final String p0, final X509Name p1, final PublicKey p2, final ASN1Set p3, final PrivateKey p4, final String p5) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokespecial   org/bouncycastle/asn1/pkcs/CertificationRequest.<init>:()V
        //     4: aload_1        
        //     5: invokestatic    org/bouncycastle/util/Strings.toUpperCase:(Ljava/lang/String;)Ljava/lang/String;
        //     8: astore          9
        //    10: getstatic       org/bouncycastle/jce/PKCS10CertificationRequest.algorithms:Ljava/util/Hashtable;
        //    13: aload           9
        //    15: invokevirtual   java/util/Hashtable.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    18: checkcast       Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //    21: astore          8
        //    23: aload           8
        //    25: astore          7
        //    27: aload           8
        //    29: ifnonnull       57
        //    32: new             Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //    35: dup            
        //    36: aload           9
        //    38: invokespecial   org/bouncycastle/asn1/ASN1ObjectIdentifier.<init>:(Ljava/lang/String;)V
        //    41: astore          7
        //    43: goto            57
        //    46: new             Ljava/lang/IllegalArgumentException;
        //    49: dup            
        //    50: ldc_w           "Unknown signature type requested"
        //    53: invokespecial   java/lang/IllegalArgumentException.<init>:(Ljava/lang/String;)V
        //    56: athrow         
        //    57: aload_2        
        //    58: ifnull          295
        //    61: aload_3        
        //    62: ifnull          284
        //    65: getstatic       org/bouncycastle/jce/PKCS10CertificationRequest.noParams:Ljava/util/Set;
        //    68: aload           7
        //    70: invokeinterface java/util/Set.contains:(Ljava/lang/Object;)Z
        //    75: ifeq            98
        //    78: new             Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //    81: dup            
        //    82: aload           7
        //    84: invokespecial   org/bouncycastle/asn1/x509/AlgorithmIdentifier.<init>:(Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;)V
        //    87: astore          7
        //    89: aload_0        
        //    90: aload           7
        //    92: putfield        org/bouncycastle/jce/PKCS10CertificationRequest.sigAlgId:Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //    95: goto            153
        //    98: getstatic       org/bouncycastle/jce/PKCS10CertificationRequest.params:Ljava/util/Hashtable;
        //   101: aload           9
        //   103: invokevirtual   java/util/Hashtable.containsKey:(Ljava/lang/Object;)Z
        //   106: ifeq            136
        //   109: aload_0        
        //   110: new             Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //   113: dup            
        //   114: aload           7
        //   116: getstatic       org/bouncycastle/jce/PKCS10CertificationRequest.params:Ljava/util/Hashtable;
        //   119: aload           9
        //   121: invokevirtual   java/util/Hashtable.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   124: checkcast       Lorg/bouncycastle/asn1/ASN1Encodable;
        //   127: invokespecial   org/bouncycastle/asn1/x509/AlgorithmIdentifier.<init>:(Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;Lorg/bouncycastle/asn1/ASN1Encodable;)V
        //   130: putfield        org/bouncycastle/jce/PKCS10CertificationRequest.sigAlgId:Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //   133: goto            153
        //   136: new             Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //   139: dup            
        //   140: aload           7
        //   142: getstatic       org/bouncycastle/asn1/DERNull.INSTANCE:Lorg/bouncycastle/asn1/DERNull;
        //   145: invokespecial   org/bouncycastle/asn1/x509/AlgorithmIdentifier.<init>:(Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;Lorg/bouncycastle/asn1/ASN1Encodable;)V
        //   148: astore          7
        //   150: goto            89
        //   153: aload_0        
        //   154: new             Lorg/bouncycastle/asn1/pkcs/CertificationRequestInfo;
        //   157: dup            
        //   158: aload_2        
        //   159: aload_3        
        //   160: invokeinterface java/security/PublicKey.getEncoded:()[B
        //   165: invokestatic    org/bouncycastle/asn1/ASN1Primitive.fromByteArray:([B)Lorg/bouncycastle/asn1/ASN1Primitive;
        //   168: checkcast       Lorg/bouncycastle/asn1/ASN1Sequence;
        //   171: invokestatic    org/bouncycastle/asn1/x509/SubjectPublicKeyInfo.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/SubjectPublicKeyInfo;
        //   174: aload           4
        //   176: invokespecial   org/bouncycastle/asn1/pkcs/CertificationRequestInfo.<init>:(Lorg/bouncycastle/asn1/x509/X509Name;Lorg/bouncycastle/asn1/x509/SubjectPublicKeyInfo;Lorg/bouncycastle/asn1/ASN1Set;)V
        //   179: putfield        org/bouncycastle/jce/PKCS10CertificationRequest.reqInfo:Lorg/bouncycastle/asn1/pkcs/CertificationRequestInfo;
        //   182: aload           6
        //   184: ifnonnull       195
        //   187: aload_1        
        //   188: invokestatic    java/security/Signature.getInstance:(Ljava/lang/String;)Ljava/security/Signature;
        //   191: astore_1       
        //   192: goto            202
        //   195: aload_1        
        //   196: aload           6
        //   198: invokestatic    java/security/Signature.getInstance:(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Signature;
        //   201: astore_1       
        //   202: aload_1        
        //   203: aload           5
        //   205: invokevirtual   java/security/Signature.initSign:(Ljava/security/PrivateKey;)V
        //   208: aload_1        
        //   209: aload_0        
        //   210: getfield        org/bouncycastle/jce/PKCS10CertificationRequest.reqInfo:Lorg/bouncycastle/asn1/pkcs/CertificationRequestInfo;
        //   213: ldc_w           "DER"
        //   216: invokevirtual   org/bouncycastle/asn1/pkcs/CertificationRequestInfo.getEncoded:(Ljava/lang/String;)[B
        //   219: invokevirtual   java/security/Signature.update:([B)V
        //   222: aload_0        
        //   223: new             Lorg/bouncycastle/asn1/DERBitString;
        //   226: dup            
        //   227: aload_1        
        //   228: invokevirtual   java/security/Signature.sign:()[B
        //   231: invokespecial   org/bouncycastle/asn1/DERBitString.<init>:([B)V
        //   234: putfield        org/bouncycastle/jce/PKCS10CertificationRequest.sigBits:Lorg/bouncycastle/asn1/DERBitString;
        //   237: return         
        //   238: astore_1       
        //   239: new             Ljava/lang/StringBuilder;
        //   242: dup            
        //   243: invokespecial   java/lang/StringBuilder.<init>:()V
        //   246: astore_2       
        //   247: aload_2        
        //   248: ldc_w           "exception encoding TBS cert request - "
        //   251: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   254: pop            
        //   255: aload_2        
        //   256: aload_1        
        //   257: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   260: pop            
        //   261: new             Ljava/lang/IllegalArgumentException;
        //   264: dup            
        //   265: aload_2        
        //   266: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   269: invokespecial   java/lang/IllegalArgumentException.<init>:(Ljava/lang/String;)V
        //   272: athrow         
        //   273: new             Ljava/lang/IllegalArgumentException;
        //   276: dup            
        //   277: ldc_w           "can't encode public key"
        //   280: invokespecial   java/lang/IllegalArgumentException.<init>:(Ljava/lang/String;)V
        //   283: athrow         
        //   284: new             Ljava/lang/IllegalArgumentException;
        //   287: dup            
        //   288: ldc_w           "public key must not be null"
        //   291: invokespecial   java/lang/IllegalArgumentException.<init>:(Ljava/lang/String;)V
        //   294: athrow         
        //   295: new             Ljava/lang/IllegalArgumentException;
        //   298: dup            
        //   299: ldc_w           "subject must not be null"
        //   302: invokespecial   java/lang/IllegalArgumentException.<init>:(Ljava/lang/String;)V
        //   305: athrow         
        //   306: astore_1       
        //   307: goto            46
        //   310: astore_1       
        //   311: goto            273
        //    Exceptions:
        //  throws java.security.NoSuchAlgorithmException
        //  throws java.security.NoSuchProviderException
        //  throws java.security.InvalidKeyException
        //  throws java.security.SignatureException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  32     43     306    57     Ljava/lang/Exception;
        //  153    182    310    284    Ljava/io/IOException;
        //  208    222    238    273    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0153:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:713)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:549)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public PKCS10CertificationRequest(final ASN1Sequence asn1Sequence) {
        super(asn1Sequence);
    }
    
    public PKCS10CertificationRequest(final byte[] array) {
        super(toDERSequence(array));
    }
    
    private static X509Name convertName(final X500Principal x500Principal) {
        try {
            return new X509Principal(x500Principal.getEncoded());
        }
        catch (IOException ex) {
            throw new IllegalArgumentException("can't convert name");
        }
    }
    
    private static RSASSAPSSparams creatPSSParams(final AlgorithmIdentifier algorithmIdentifier, final int n) {
        return new RSASSAPSSparams(algorithmIdentifier, new AlgorithmIdentifier(PKCSObjectIdentifiers.id_mgf1, algorithmIdentifier), new ASN1Integer(n), new ASN1Integer(1L));
    }
    
    private static String getDigestAlgName(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        if (PKCSObjectIdentifiers.md5.equals(asn1ObjectIdentifier)) {
            return "MD5";
        }
        if (OIWObjectIdentifiers.idSHA1.equals(asn1ObjectIdentifier)) {
            return "SHA1";
        }
        if (NISTObjectIdentifiers.id_sha224.equals(asn1ObjectIdentifier)) {
            return "SHA224";
        }
        if (NISTObjectIdentifiers.id_sha256.equals(asn1ObjectIdentifier)) {
            return "SHA256";
        }
        if (NISTObjectIdentifiers.id_sha384.equals(asn1ObjectIdentifier)) {
            return "SHA384";
        }
        if (NISTObjectIdentifiers.id_sha512.equals(asn1ObjectIdentifier)) {
            return "SHA512";
        }
        if (TeleTrusTObjectIdentifiers.ripemd128.equals(asn1ObjectIdentifier)) {
            return "RIPEMD128";
        }
        if (TeleTrusTObjectIdentifiers.ripemd160.equals(asn1ObjectIdentifier)) {
            return "RIPEMD160";
        }
        if (TeleTrusTObjectIdentifiers.ripemd256.equals(asn1ObjectIdentifier)) {
            return "RIPEMD256";
        }
        if (CryptoProObjectIdentifiers.gostR3411.equals(asn1ObjectIdentifier)) {
            return "GOST3411";
        }
        return asn1ObjectIdentifier.getId();
    }
    
    static String getSignatureName(final AlgorithmIdentifier algorithmIdentifier) {
        final ASN1Encodable parameters = algorithmIdentifier.getParameters();
        if (parameters != null && !DERNull.INSTANCE.equals(parameters) && algorithmIdentifier.getAlgorithm().equals(PKCSObjectIdentifiers.id_RSASSA_PSS)) {
            final RSASSAPSSparams instance = RSASSAPSSparams.getInstance(parameters);
            final StringBuilder sb = new StringBuilder();
            sb.append(getDigestAlgName(instance.getHashAlgorithm().getAlgorithm()));
            sb.append("withRSAandMGF1");
            return sb.toString();
        }
        return algorithmIdentifier.getAlgorithm().getId();
    }
    
    private void setSignatureParameters(final Signature signature, final ASN1Encodable asn1Encodable) throws NoSuchAlgorithmException, SignatureException, InvalidKeyException {
        if (asn1Encodable != null && !DERNull.INSTANCE.equals(asn1Encodable)) {
            final AlgorithmParameters instance = AlgorithmParameters.getInstance(signature.getAlgorithm(), signature.getProvider());
            try {
                instance.init(asn1Encodable.toASN1Primitive().getEncoded("DER"));
                if (signature.getAlgorithm().endsWith("MGF1")) {
                    try {
                        signature.setParameter(instance.getParameterSpec(PSSParameterSpec.class));
                    }
                    catch (GeneralSecurityException ex) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Exception extracting parameters: ");
                        sb.append(ex.getMessage());
                        throw new SignatureException(sb.toString());
                    }
                }
            }
            catch (IOException ex2) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("IOException decoding parameters: ");
                sb2.append(ex2.getMessage());
                throw new SignatureException(sb2.toString());
            }
        }
    }
    
    private static ASN1Sequence toDERSequence(final byte[] array) {
        try {
            return (ASN1Sequence)new ASN1InputStream(array).readObject();
        }
        catch (Exception ex) {
            throw new IllegalArgumentException("badly encoded request");
        }
    }
    
    @Override
    public byte[] getEncoded() {
        try {
            return this.getEncoded("DER");
        }
        catch (IOException ex) {
            throw new RuntimeException(ex.toString());
        }
    }
    
    public PublicKey getPublicKey() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        return this.getPublicKey("BC");
    }
    
    public PublicKey getPublicKey(final String s) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        final SubjectPublicKeyInfo subjectPublicKeyInfo = this.reqInfo.getSubjectPublicKeyInfo();
        while (true) {
            try {
                final X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(new DERBitString(subjectPublicKeyInfo).getOctets());
                final AlgorithmIdentifier algorithm = subjectPublicKeyInfo.getAlgorithm();
                while (true) {
                    if (s == null) {
                        try {
                            return KeyFactory.getInstance(algorithm.getAlgorithm().getId()).generatePublic(x509EncodedKeySpec);
                            return KeyFactory.getInstance(algorithm.getAlgorithm().getId(), s).generatePublic(x509EncodedKeySpec);
                        }
                        catch (NoSuchAlgorithmException ex) {
                            if (PKCS10CertificationRequest.keyAlgorithms.get(algorithm.getAlgorithm()) == null) {
                                throw ex;
                            }
                            final String s2 = PKCS10CertificationRequest.keyAlgorithms.get(algorithm.getAlgorithm());
                            if (s == null) {
                                return KeyFactory.getInstance(s2).generatePublic(x509EncodedKeySpec);
                            }
                            return KeyFactory.getInstance(s2, s).generatePublic(x509EncodedKeySpec);
                        }
                        throw new InvalidKeyException("error decoding public key");
                    }
                    continue;
                }
            }
            catch (InvalidKeySpecException ex2) {
                throw new InvalidKeyException("error decoding public key");
            }
            catch (IOException ex3) {}
            continue;
        }
    }
    
    public boolean verify() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {
        return this.verify("BC");
    }
    
    public boolean verify(final String s) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {
        return this.verify(this.getPublicKey(s), s);
    }
    
    public boolean verify(final PublicKey publicKey, String s) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {
        while (true) {
            if (s == null) {
                Label_0089: {
                    try {
                        s = (String)Signature.getInstance(getSignatureName(this.sigAlgId));
                        break Label_0089;
                        s = (String)Signature.getInstance(getSignatureName(this.sigAlgId), s);
                    }
                    catch (NoSuchAlgorithmException ex) {
                        if (PKCS10CertificationRequest.oids.get(this.sigAlgId.getAlgorithm()) == null) {
                            throw ex;
                        }
                        final String s2 = PKCS10CertificationRequest.oids.get(this.sigAlgId.getAlgorithm());
                        if (s == null) {
                            s = (String)Signature.getInstance(s2);
                        }
                        else {
                            s = (String)Signature.getInstance(s2, s);
                        }
                    }
                }
                this.setSignatureParameters((Signature)s, this.sigAlgId.getParameters());
                ((Signature)s).initVerify(publicKey);
                try {
                    ((Signature)s).update(this.reqInfo.getEncoded("DER"));
                    return ((Signature)s).verify(this.sigBits.getOctets());
                }
                catch (Exception obj) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("exception encoding TBS cert request - ");
                    sb.append(obj);
                    throw new SignatureException(sb.toString());
                }
                throw;
            }
            continue;
        }
    }
}
