// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce;

import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import java.util.Enumeration;

public class ECNamedCurveTable
{
    public static Enumeration getNames() {
        return org.bouncycastle.asn1.x9.ECNamedCurveTable.getNames();
    }
    
    public static ECNamedCurveParameterSpec getParameterSpec(final String p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokestatic    org/bouncycastle/crypto/ec/CustomNamedCurves.getByName:(Ljava/lang/String;)Lorg/bouncycastle/asn1/x9/X9ECParameters;
        //     4: astore_2       
        //     5: aload_2        
        //     6: astore_1       
        //     7: aload_2        
        //     8: ifnonnull       54
        //    11: new             Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //    14: dup            
        //    15: aload_0        
        //    16: invokespecial   org/bouncycastle/asn1/ASN1ObjectIdentifier.<init>:(Ljava/lang/String;)V
        //    19: invokestatic    org/bouncycastle/crypto/ec/CustomNamedCurves.getByOID:(Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;)Lorg/bouncycastle/asn1/x9/X9ECParameters;
        //    22: astore_1       
        //    23: aload_1        
        //    24: astore_2       
        //    25: aload_2        
        //    26: astore_1       
        //    27: aload_2        
        //    28: ifnonnull       54
        //    31: aload_0        
        //    32: invokestatic    org/bouncycastle/asn1/x9/ECNamedCurveTable.getByName:(Ljava/lang/String;)Lorg/bouncycastle/asn1/x9/X9ECParameters;
        //    35: astore_2       
        //    36: aload_2        
        //    37: astore_1       
        //    38: aload_2        
        //    39: ifnonnull       54
        //    42: new             Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //    45: dup            
        //    46: aload_0        
        //    47: invokespecial   org/bouncycastle/asn1/ASN1ObjectIdentifier.<init>:(Ljava/lang/String;)V
        //    50: invokestatic    org/bouncycastle/asn1/x9/ECNamedCurveTable.getByOID:(Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;)Lorg/bouncycastle/asn1/x9/X9ECParameters;
        //    53: astore_1       
        //    54: aload_1        
        //    55: ifnonnull       60
        //    58: aconst_null    
        //    59: areturn        
        //    60: new             Lorg/bouncycastle/jce/spec/ECNamedCurveParameterSpec;
        //    63: dup            
        //    64: aload_0        
        //    65: aload_1        
        //    66: invokevirtual   org/bouncycastle/asn1/x9/X9ECParameters.getCurve:()Lorg/bouncycastle/math/ec/ECCurve;
        //    69: aload_1        
        //    70: invokevirtual   org/bouncycastle/asn1/x9/X9ECParameters.getG:()Lorg/bouncycastle/math/ec/ECPoint;
        //    73: aload_1        
        //    74: invokevirtual   org/bouncycastle/asn1/x9/X9ECParameters.getN:()Ljava/math/BigInteger;
        //    77: aload_1        
        //    78: invokevirtual   org/bouncycastle/asn1/x9/X9ECParameters.getH:()Ljava/math/BigInteger;
        //    81: aload_1        
        //    82: invokevirtual   org/bouncycastle/asn1/x9/X9ECParameters.getSeed:()[B
        //    85: invokespecial   org/bouncycastle/jce/spec/ECNamedCurveParameterSpec.<init>:(Ljava/lang/String;Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/math/ec/ECPoint;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V
        //    88: areturn        
        //    89: astore_1       
        //    90: goto            25
        //    93: astore_1       
        //    94: aload_2        
        //    95: astore_1       
        //    96: goto            54
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  11     23     89     93     Ljava/lang/IllegalArgumentException;
        //  42     54     93     99     Ljava/lang/IllegalArgumentException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0054:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
