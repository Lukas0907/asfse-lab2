// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jce;

import org.bouncycastle.asn1.pkcs.MacData;
import org.bouncycastle.asn1.x509.DigestInfo;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.pkcs.ContentInfo;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1OctetString;
import java.io.IOException;
import org.bouncycastle.asn1.pkcs.Pfx;
import javax.crypto.SecretKey;
import java.security.spec.AlgorithmParameterSpec;
import java.security.Key;
import javax.crypto.Mac;
import java.security.spec.KeySpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.SecretKeyFactory;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;

public class PKCS12Util
{
    private static byte[] calculatePbeMac(final ASN1ObjectIdentifier asn1ObjectIdentifier, final byte[] salt, final int iterationCount, final char[] password, final byte[] input, final String s) throws Exception {
        final SecretKeyFactory instance = SecretKeyFactory.getInstance(asn1ObjectIdentifier.getId(), s);
        final PBEParameterSpec params = new PBEParameterSpec(salt, iterationCount);
        final SecretKey generateSecret = instance.generateSecret(new PBEKeySpec(password));
        final Mac instance2 = Mac.getInstance(asn1ObjectIdentifier.getId(), s);
        instance2.init(generateSecret, params);
        instance2.update(input);
        return instance2.doFinal();
    }
    
    public static byte[] convertToDefiniteLength(final byte[] array) throws IOException {
        return Pfx.getInstance(array).getEncoded("DER");
    }
    
    public static byte[] convertToDefiniteLength(final byte[] array, final char[] array2, final String s) throws IOException {
        final Pfx instance = Pfx.getInstance(array);
        final ContentInfo authSafe = instance.getAuthSafe();
        final ContentInfo contentInfo = new ContentInfo(authSafe.getContentType(), new DEROctetString(ASN1Primitive.fromByteArray(ASN1OctetString.getInstance(authSafe.getContent()).getOctets()).getEncoded("DER")));
        final MacData macData = instance.getMacData();
        try {
            final int intValue = macData.getIterationCount().intValue();
            return new Pfx(contentInfo, new MacData(new DigestInfo(new AlgorithmIdentifier(macData.getMac().getAlgorithmId().getAlgorithm(), DERNull.INSTANCE), calculatePbeMac(macData.getMac().getAlgorithmId().getAlgorithm(), macData.getSalt(), intValue, array2, ASN1OctetString.getInstance(contentInfo.getContent()).getOctets(), s)), macData.getSalt(), intValue)).getEncoded("DER");
        }
        catch (Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("error constructing MAC: ");
            sb.append(ex.toString());
            throw new IOException(sb.toString());
        }
    }
}
