// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec;

import java.math.BigInteger;

public abstract class WNafUtil
{
    private static final int[] DEFAULT_WINDOW_SIZE_CUTOFFS;
    private static final byte[] EMPTY_BYTES;
    private static final int[] EMPTY_INTS;
    private static final ECPoint[] EMPTY_POINTS;
    private static final int MAX_WIDTH = 16;
    public static final String PRECOMP_NAME = "bc_wnaf";
    
    static {
        DEFAULT_WINDOW_SIZE_CUTOFFS = new int[] { 13, 41, 121, 337, 897, 2305 };
        EMPTY_BYTES = new byte[0];
        EMPTY_INTS = new int[0];
        EMPTY_POINTS = new ECPoint[0];
    }
    
    public static void configureBasepoint(final ECPoint ecPoint) {
        final ECCurve curve = ecPoint.getCurve();
        if (curve == null) {
            return;
        }
        final BigInteger order = curve.getOrder();
        int bitLength;
        if (order == null) {
            bitLength = curve.getFieldSize() + 1;
        }
        else {
            bitLength = order.bitLength();
        }
        curve.precompute(ecPoint, "bc_wnaf", new PreCompCallback() {
            final /* synthetic */ int val$confWidth = Math.min(16, getWindowSize(bitLength) + 3);
            
            @Override
            public PreCompInfo precompute(final PreCompInfo preCompInfo) {
                WNafPreCompInfo wNafPreCompInfo;
                if (preCompInfo instanceof WNafPreCompInfo) {
                    wNafPreCompInfo = (WNafPreCompInfo)preCompInfo;
                }
                else {
                    wNafPreCompInfo = null;
                }
                if (wNafPreCompInfo != null && wNafPreCompInfo.getConfWidth() == this.val$confWidth) {
                    wNafPreCompInfo.setPromotionCountdown(0);
                    return wNafPreCompInfo;
                }
                final WNafPreCompInfo wNafPreCompInfo2 = new WNafPreCompInfo();
                wNafPreCompInfo2.setPromotionCountdown(0);
                wNafPreCompInfo2.setConfWidth(this.val$confWidth);
                if (wNafPreCompInfo != null) {
                    wNafPreCompInfo2.setPreComp(wNafPreCompInfo.getPreComp());
                    wNafPreCompInfo2.setPreCompNeg(wNafPreCompInfo.getPreCompNeg());
                    wNafPreCompInfo2.setTwice(wNafPreCompInfo.getTwice());
                    wNafPreCompInfo2.setWidth(wNafPreCompInfo.getWidth());
                }
                return wNafPreCompInfo2;
            }
        });
    }
    
    public static int[] generateCompactNaf(final BigInteger bigInteger) {
        if (bigInteger.bitLength() >>> 16 != 0) {
            throw new IllegalArgumentException("'k' must have bitlength < 2^16");
        }
        if (bigInteger.signum() == 0) {
            return WNafUtil.EMPTY_INTS;
        }
        final BigInteger add = bigInteger.shiftLeft(1).add(bigInteger);
        final int bitLength = add.bitLength();
        final int[] array = new int[bitLength >> 1];
        final BigInteger xor = add.xor(bigInteger);
        int n2;
        int n = n2 = 0;
        for (int i = 1; i < bitLength - 1; ++i) {
            if (!xor.testBit(i)) {
                ++n2;
            }
            else {
                int n3;
                if (bigInteger.testBit(i)) {
                    n3 = -1;
                }
                else {
                    n3 = 1;
                }
                array[n] = (n2 | n3 << 16);
                ++i;
                n2 = 1;
                ++n;
            }
        }
        final int n4 = n + 1;
        array[n] = (0x10000 | n2);
        int[] trim = array;
        if (array.length > n4) {
            trim = trim(array, n4);
        }
        return trim;
    }
    
    public static int[] generateCompactWindowNaf(final int n, final BigInteger bigInteger) {
        throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge Z and I\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
    }
    
    public static byte[] generateJSF(BigInteger bigInteger, BigInteger shiftRight) {
        final byte[] array = new byte[Math.max(bigInteger.bitLength(), shiftRight.bitLength()) + 1];
        BigInteger bigInteger2 = bigInteger;
        bigInteger = shiftRight;
        final int n2;
        int n = n2 = 0;
        int n4;
        final int n3 = n4 = n2;
        int n15;
        int n16;
        BigInteger shiftRight2;
        int n18;
        for (int n5 = n2, n6 = n3; (n | n5) != 0x0 || bigInteger2.bitLength() > n6 || bigInteger.bitLength() > n6; bigInteger2 = shiftRight2, n6 = n18, bigInteger = shiftRight, n = n15, n5 = n16) {
            final int n7 = (bigInteger2.intValue() >>> n6) + n & 0x7;
            final int n8 = (bigInteger.intValue() >>> n6) + n5 & 0x7;
            final int n9 = n7 & 0x1;
            int n10;
            if ((n10 = n9) != 0) {
                final int n11 = n10 = n9 - (n7 & 0x2);
                if (n7 + n11 == 4) {
                    n10 = n11;
                    if ((n8 & 0x3) == 0x2) {
                        n10 = -n11;
                    }
                }
            }
            final int n12 = n8 & 0x1;
            int n13;
            if ((n13 = n12) != 0) {
                final int n14 = n13 = n12 - (n8 & 0x2);
                if (n8 + n14 == 4) {
                    n13 = n14;
                    if ((n7 & 0x3) == 0x2) {
                        n13 = -n14;
                    }
                }
            }
            n15 = n;
            if (n << 1 == n10 + 1) {
                n15 = (n ^ 0x1);
            }
            n16 = n5;
            if (n5 << 1 == n13 + 1) {
                n16 = (n5 ^ 0x1);
            }
            final int n17 = n6 + 1;
            shiftRight2 = bigInteger2;
            n18 = n17;
            shiftRight = bigInteger;
            if (n17 == 30) {
                shiftRight2 = bigInteger2.shiftRight(30);
                shiftRight = bigInteger.shiftRight(30);
                n18 = 0;
            }
            array[n4] = (byte)(n10 << 4 | (n13 & 0xF));
            ++n4;
        }
        byte[] trim = array;
        if (array.length > n4) {
            trim = trim(array, n4);
        }
        return trim;
    }
    
    public static byte[] generateNaf(final BigInteger bigInteger) {
        if (bigInteger.signum() == 0) {
            return WNafUtil.EMPTY_BYTES;
        }
        final BigInteger add = bigInteger.shiftLeft(1).add(bigInteger);
        final int n = add.bitLength() - 1;
        final byte[] array = new byte[n];
        final BigInteger xor = add.xor(bigInteger);
        int n2;
        for (int i = 1; i < n; i = n2 + 1) {
            n2 = i;
            if (xor.testBit(i)) {
                int n3;
                if (bigInteger.testBit(i)) {
                    n3 = -1;
                }
                else {
                    n3 = 1;
                }
                array[i - 1] = (byte)n3;
                n2 = i + 1;
            }
        }
        array[n - 1] = 1;
        return array;
    }
    
    public static byte[] generateWindowNaf(final int n, final BigInteger bigInteger) {
        throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge Z and I\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
    }
    
    public static int getNafWeight(final BigInteger bigInteger) {
        if (bigInteger.signum() == 0) {
            return 0;
        }
        return bigInteger.shiftLeft(1).add(bigInteger).xor(bigInteger).bitCount();
    }
    
    public static WNafPreCompInfo getWNafPreCompInfo(final ECPoint ecPoint) {
        return getWNafPreCompInfo(ecPoint.getCurve().getPreCompInfo(ecPoint, "bc_wnaf"));
    }
    
    public static WNafPreCompInfo getWNafPreCompInfo(final PreCompInfo preCompInfo) {
        if (preCompInfo instanceof WNafPreCompInfo) {
            return (WNafPreCompInfo)preCompInfo;
        }
        return null;
    }
    
    public static int getWindowSize(final int n) {
        return getWindowSize(n, WNafUtil.DEFAULT_WINDOW_SIZE_CUTOFFS, 16);
    }
    
    public static int getWindowSize(final int n, final int n2) {
        return getWindowSize(n, WNafUtil.DEFAULT_WINDOW_SIZE_CUTOFFS, n2);
    }
    
    public static int getWindowSize(final int n, final int[] array) {
        return getWindowSize(n, array, 16);
    }
    
    public static int getWindowSize(final int n, final int[] array, final int a) {
        int n2;
        for (n2 = 0; n2 < array.length && n >= array[n2]; ++n2) {}
        return Math.max(2, Math.min(a, n2 + 2));
    }
    
    public static ECPoint mapPointWithPrecomp(ECPoint map, final int n, final boolean b, final ECPointMap ecPointMap) {
        final ECCurve curve = map.getCurve();
        final WNafPreCompInfo precompute = precompute(map, n, b);
        map = ecPointMap.map(map);
        curve.precompute(map, "bc_wnaf", new PreCompCallback() {
            @Override
            public PreCompInfo precompute(final PreCompInfo preCompInfo) {
                final WNafPreCompInfo wNafPreCompInfo = new WNafPreCompInfo();
                wNafPreCompInfo.setConfWidth(precompute.getConfWidth());
                final ECPoint twice = precompute.getTwice();
                if (twice != null) {
                    wNafPreCompInfo.setTwice(ecPointMap.map(twice));
                }
                final ECPoint[] preComp = precompute.getPreComp();
                final ECPoint[] preComp2 = new ECPoint[preComp.length];
                final int n = 0;
                for (int i = 0; i < preComp.length; ++i) {
                    preComp2[i] = ecPointMap.map(preComp[i]);
                }
                wNafPreCompInfo.setPreComp(preComp2);
                wNafPreCompInfo.setWidth(precompute.getWidth());
                if (b) {
                    final ECPoint[] preCompNeg = new ECPoint[preComp2.length];
                    for (int j = n; j < preCompNeg.length; ++j) {
                        preCompNeg[j] = preComp2[j].negate();
                    }
                    wNafPreCompInfo.setPreCompNeg(preCompNeg);
                }
                return wNafPreCompInfo;
            }
        });
        return map;
    }
    
    public static WNafPreCompInfo precompute(final ECPoint ecPoint, final int n, final boolean b) {
        final ECCurve curve = ecPoint.getCurve();
        return (WNafPreCompInfo)curve.precompute(ecPoint, "bc_wnaf", new PreCompCallback() {
            private boolean checkExisting(final WNafPreCompInfo wNafPreCompInfo, final int b, final int n, final boolean b2) {
                return wNafPreCompInfo != null && wNafPreCompInfo.getWidth() >= Math.max(wNafPreCompInfo.getConfWidth(), b) && this.checkTable(wNafPreCompInfo.getPreComp(), n) && (!b2 || this.checkTable(wNafPreCompInfo.getPreCompNeg(), n));
            }
            
            private boolean checkTable(final ECPoint[] array, final int n) {
                return array != null && array.length >= n;
            }
            
            @Override
            public PreCompInfo precompute(final PreCompInfo preCompInfo) {
                final boolean b = preCompInfo instanceof WNafPreCompInfo;
                final ECFieldElement ecFieldElement = null;
                final ECFieldElement ecFieldElement2 = null;
                WNafPreCompInfo wNafPreCompInfo;
                if (b) {
                    wNafPreCompInfo = (WNafPreCompInfo)preCompInfo;
                }
                else {
                    wNafPreCompInfo = null;
                }
                final int max = Math.max(2, Math.min(16, n));
                if (this.checkExisting(wNafPreCompInfo, max, 1 << max - 2, b)) {
                    wNafPreCompInfo.decrementPromotionCountdown();
                    return wNafPreCompInfo;
                }
                final WNafPreCompInfo wNafPreCompInfo2 = new WNafPreCompInfo();
                ECPoint[] array;
                ECPoint[] preCompNeg;
                ECPoint twice;
                if (wNafPreCompInfo != null) {
                    wNafPreCompInfo2.setPromotionCountdown(wNafPreCompInfo.decrementPromotionCountdown());
                    wNafPreCompInfo2.setConfWidth(wNafPreCompInfo.getConfWidth());
                    array = wNafPreCompInfo.getPreComp();
                    preCompNeg = wNafPreCompInfo.getPreCompNeg();
                    twice = wNafPreCompInfo.getTwice();
                }
                else {
                    twice = null;
                    array = (preCompNeg = (ECPoint[])(Object)twice);
                }
                final int min = Math.min(16, Math.max(wNafPreCompInfo2.getConfWidth(), max));
                final int n = 1 << min - 2;
                final int n2 = 0;
                int length;
                if (array == null) {
                    array = WNafUtil.EMPTY_POINTS;
                    length = 0;
                }
                else {
                    length = array.length;
                }
                ECPoint[] preComp = array;
                ECPoint twice2 = twice;
                if (length < n) {
                    final ECPoint[] access$100 = resizeTable(array, n);
                    if (n == 1) {
                        access$100[0] = ecPoint.normalize();
                        preComp = access$100;
                        twice2 = twice;
                    }
                    else {
                        int n3;
                        if (length == 0) {
                            access$100[0] = ecPoint;
                            n3 = 1;
                        }
                        else {
                            n3 = length;
                        }
                        ECFieldElement ecFieldElement4 = null;
                        Label_0527: {
                            if (n != 2) {
                                final ECPoint ecPoint = access$100[n3 - 1];
                                while (true) {
                                    ECPoint twice3 = null;
                                    Label_0476: {
                                        if ((twice3 = twice) != null) {
                                            break Label_0476;
                                        }
                                        final ECPoint ecPoint2 = twice3 = access$100[0].twice();
                                        if (ecPoint2.isInfinity()) {
                                            break Label_0476;
                                        }
                                        twice3 = ecPoint2;
                                        if (!ECAlgorithms.isFpCurve(curve)) {
                                            break Label_0476;
                                        }
                                        twice3 = ecPoint2;
                                        if (curve.getFieldSize() < 64) {
                                            break Label_0476;
                                        }
                                        final int coordinateSystem = curve.getCoordinateSystem();
                                        if (coordinateSystem != 2 && coordinateSystem != 3 && coordinateSystem != 4) {
                                            twice3 = ecPoint2;
                                            break Label_0476;
                                        }
                                        final ECFieldElement zCoord = ecPoint2.getZCoord(0);
                                        final ECPoint point = curve.createPoint(ecPoint2.getXCoord().toBigInteger(), ecPoint2.getYCoord().toBigInteger());
                                        final ECFieldElement square = zCoord.square();
                                        final ECPoint scaleY = ecPoint.scaleX(square).scaleY(square.multiply(zCoord));
                                        ECFieldElement ecFieldElement3 = zCoord;
                                        ECPoint ecPoint3 = point;
                                        ECPoint add = scaleY;
                                        int n4 = n3;
                                        twice3 = ecPoint2;
                                        if (length == 0) {
                                            access$100[0] = scaleY;
                                            ecFieldElement3 = zCoord;
                                            ecPoint3 = point;
                                            add = scaleY;
                                            n4 = n3;
                                            twice3 = ecPoint2;
                                        }
                                        while (true) {
                                            ecFieldElement4 = ecFieldElement3;
                                            twice2 = twice3;
                                            if (n4 >= n) {
                                                break Label_0527;
                                            }
                                            add = add.add(ecPoint3);
                                            access$100[n4] = add;
                                            ++n4;
                                        }
                                    }
                                    ECPoint ecPoint3 = twice3;
                                    int n4 = n3;
                                    ECPoint add = ecPoint;
                                    ECFieldElement ecFieldElement3 = ecFieldElement2;
                                    continue;
                                }
                            }
                            access$100[1] = ecPoint.threeTimes();
                            ecFieldElement4 = ecFieldElement;
                            twice2 = twice;
                        }
                        curve.normalizeAll(access$100, length, n - length, ecFieldElement4);
                        preComp = access$100;
                    }
                }
                ECPoint[] preCompNeg2 = preCompNeg;
                Label_0629: {
                    if (b) {
                        ECPoint[] access$101;
                        int n5;
                        if (preCompNeg == null) {
                            access$101 = new ECPoint[n];
                            n5 = n2;
                        }
                        else {
                            final int length2 = preCompNeg.length;
                            access$101 = preCompNeg;
                            if ((n5 = length2) < n) {
                                access$101 = resizeTable(preCompNeg, n);
                                n5 = length2;
                            }
                        }
                        while (true) {
                            while (true) {
                                preCompNeg2 = access$101;
                                if (n5 >= n) {
                                    break Label_0629;
                                }
                                access$101[n5] = preComp[n5].negate();
                                ++n5;
                            }
                            continue;
                        }
                    }
                }
                wNafPreCompInfo2.setPreComp(preComp);
                wNafPreCompInfo2.setPreCompNeg(preCompNeg2);
                wNafPreCompInfo2.setTwice(twice2);
                wNafPreCompInfo2.setWidth(min);
                return wNafPreCompInfo2;
            }
        });
    }
    
    public static WNafPreCompInfo precomputeWithPointMap(final ECPoint ecPoint, final ECPointMap ecPointMap, final WNafPreCompInfo wNafPreCompInfo, final boolean b) {
        return (WNafPreCompInfo)ecPoint.getCurve().precompute(ecPoint, "bc_wnaf", new PreCompCallback() {
            private boolean checkExisting(final WNafPreCompInfo wNafPreCompInfo, final int n, final int n2, final boolean b) {
                return wNafPreCompInfo != null && wNafPreCompInfo.getWidth() >= n && this.checkTable(wNafPreCompInfo.getPreComp(), n2) && (!b || this.checkTable(wNafPreCompInfo.getPreCompNeg(), n2));
            }
            
            private boolean checkTable(final ECPoint[] array, final int n) {
                return array != null && array.length >= n;
            }
            
            @Override
            public PreCompInfo precompute(final PreCompInfo preCompInfo) {
                WNafPreCompInfo wNafPreCompInfo;
                if (preCompInfo instanceof WNafPreCompInfo) {
                    wNafPreCompInfo = (WNafPreCompInfo)preCompInfo;
                }
                else {
                    wNafPreCompInfo = null;
                }
                final int width = wNafPreCompInfo.getWidth();
                if (this.checkExisting(wNafPreCompInfo, width, wNafPreCompInfo.getPreComp().length, b)) {
                    wNafPreCompInfo.decrementPromotionCountdown();
                    return wNafPreCompInfo;
                }
                final WNafPreCompInfo wNafPreCompInfo2 = new WNafPreCompInfo();
                wNafPreCompInfo2.setPromotionCountdown(wNafPreCompInfo.getPromotionCountdown());
                final ECPoint twice = wNafPreCompInfo.getTwice();
                if (twice != null) {
                    wNafPreCompInfo2.setTwice(ecPointMap.map(twice));
                }
                final ECPoint[] preComp = wNafPreCompInfo.getPreComp();
                final ECPoint[] preComp2 = new ECPoint[preComp.length];
                final int n = 0;
                for (int i = 0; i < preComp.length; ++i) {
                    preComp2[i] = ecPointMap.map(preComp[i]);
                }
                wNafPreCompInfo2.setPreComp(preComp2);
                wNafPreCompInfo2.setWidth(width);
                if (b) {
                    final ECPoint[] preCompNeg = new ECPoint[preComp2.length];
                    for (int j = n; j < preCompNeg.length; ++j) {
                        preCompNeg[j] = preComp2[j].negate();
                    }
                    wNafPreCompInfo2.setPreCompNeg(preCompNeg);
                }
                return wNafPreCompInfo2;
            }
        });
    }
    
    private static ECPoint[] resizeTable(final ECPoint[] array, final int n) {
        final ECPoint[] array2 = new ECPoint[n];
        System.arraycopy(array, 0, array2, 0, array.length);
        return array2;
    }
    
    private static byte[] trim(final byte[] array, final int n) {
        final byte[] array2 = new byte[n];
        System.arraycopy(array, 0, array2, 0, array2.length);
        return array2;
    }
    
    private static int[] trim(final int[] array, final int n) {
        final int[] array2 = new int[n];
        System.arraycopy(array, 0, array2, 0, array2.length);
        return array2;
    }
}
