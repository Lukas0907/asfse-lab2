// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec;

import org.bouncycastle.math.ec.endo.EndoUtil;
import org.bouncycastle.math.ec.endo.ECEndomorphism;
import java.math.BigInteger;
import org.bouncycastle.math.ec.endo.GLVEndomorphism;

public class GLVMultiplier extends AbstractECMultiplier
{
    protected final ECCurve curve;
    protected final GLVEndomorphism glvEndomorphism;
    
    public GLVMultiplier(final ECCurve curve, final GLVEndomorphism glvEndomorphism) {
        if (curve != null && curve.getOrder() != null) {
            this.curve = curve;
            this.glvEndomorphism = glvEndomorphism;
            return;
        }
        throw new IllegalArgumentException("Need curve with known group order");
    }
    
    @Override
    protected ECPoint multiplyPositive(final ECPoint ecPoint, BigInteger bigInteger) {
        if (!this.curve.equals(ecPoint.getCurve())) {
            throw new IllegalStateException();
        }
        final BigInteger[] decomposeScalar = this.glvEndomorphism.decomposeScalar(bigInteger.mod(ecPoint.getCurve().getOrder()));
        bigInteger = decomposeScalar[0];
        final BigInteger bigInteger2 = decomposeScalar[1];
        if (this.glvEndomorphism.hasEfficientPointMap()) {
            return ECAlgorithms.implShamirsTrickWNaf(this.glvEndomorphism, ecPoint, bigInteger, bigInteger2);
        }
        return ECAlgorithms.implShamirsTrickWNaf(ecPoint, bigInteger, EndoUtil.mapPoint(this.glvEndomorphism, ecPoint), bigInteger2);
    }
}
