// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.tools;

import org.bouncycastle.math.ec.ECPoint;
import java.util.Iterator;
import java.util.Collection;
import java.util.TreeSet;
import org.bouncycastle.util.BigIntegers;
import java.security.SecureRandom;
import org.bouncycastle.math.ec.ECFieldElement;
import java.util.ArrayList;
import java.util.Enumeration;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.math.ec.ECAlgorithms;
import java.io.PrintStream;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.asn1.x9.ECNamedCurveTable;
import org.bouncycastle.crypto.ec.CustomNamedCurves;
import org.bouncycastle.math.ec.ECConstants;
import java.math.BigInteger;

public class DiscoverEndomorphisms
{
    private static final int radix = 16;
    
    private static boolean areRelativelyPrime(final BigInteger bigInteger, final BigInteger val) {
        return bigInteger.gcd(val).equals(ECConstants.ONE);
    }
    
    private static BigInteger[] calculateRange(final BigInteger bigInteger, final BigInteger bigInteger2, final BigInteger bigInteger3) {
        return order(bigInteger.subtract(bigInteger2).divide(bigInteger3), bigInteger.add(bigInteger2).divide(bigInteger3));
    }
    
    private static BigInteger[] chooseShortest(final BigInteger[] array, final BigInteger[] array2) {
        if (isShorter(array, array2)) {
            return array;
        }
        return array2;
    }
    
    private static void discoverEndomorphisms(final String str) {
        X9ECParameters x9ECParameters;
        if ((x9ECParameters = CustomNamedCurves.getByName(str)) == null && (x9ECParameters = ECNamedCurveTable.getByName(str)) == null) {
            final PrintStream err = System.err;
            final StringBuilder sb = new StringBuilder();
            sb.append("Unknown curve: ");
            sb.append(str);
            err.println(sb.toString());
            return;
        }
        discoverEndomorphisms(x9ECParameters, str);
    }
    
    public static void discoverEndomorphisms(final X9ECParameters x9ECParameters) {
        if (x9ECParameters != null) {
            discoverEndomorphisms(x9ECParameters, "<UNKNOWN>");
            return;
        }
        throw new NullPointerException("x9");
    }
    
    private static void discoverEndomorphisms(final X9ECParameters x9ECParameters, final String s) {
        final ECCurve curve = x9ECParameters.getCurve();
        if (ECAlgorithms.isFpCurve(curve)) {
            final BigInteger characteristic = curve.getField().getCharacteristic();
            if (curve.getB().isZero() && characteristic.mod(ECConstants.FOUR).equals(ECConstants.ONE)) {
                final PrintStream out = System.out;
                final StringBuilder sb = new StringBuilder();
                sb.append("Curve '");
                sb.append(s);
                sb.append("' has a 'GLV Type A' endomorphism with these parameters:");
                out.println(sb.toString());
                printGLVTypeAParameters(x9ECParameters);
            }
            if (curve.getA().isZero() && characteristic.mod(ECConstants.THREE).equals(ECConstants.ONE)) {
                final PrintStream out2 = System.out;
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Curve '");
                sb2.append(s);
                sb2.append("' has a 'GLV Type B' endomorphism with these parameters:");
                out2.println(sb2.toString());
                printGLVTypeBParameters(x9ECParameters);
            }
        }
    }
    
    private static ArrayList enumToList(final Enumeration enumeration) {
        final ArrayList<Object> list = new ArrayList<Object>();
        while (enumeration.hasMoreElements()) {
            list.add(enumeration.nextElement());
        }
        return list;
    }
    
    private static BigInteger[] extEuclidBezout(BigInteger[] array) {
        final boolean b = array[0].compareTo(array[1]) < 0;
        if (b) {
            swap(array);
        }
        BigInteger bigInteger = array[0];
        BigInteger val = array[1];
        BigInteger one = ECConstants.ONE;
        BigInteger zero = ECConstants.ZERO;
        BigInteger zero2 = ECConstants.ZERO;
        BigInteger one2;
        BigInteger subtract;
        BigInteger subtract2;
        BigInteger bigInteger4;
        for (one2 = ECConstants.ONE; val.compareTo(ECConstants.ONE) > 0; val = bigInteger4, one = zero, zero = subtract, zero2 = one2, one2 = subtract2) {
            final BigInteger[] divideAndRemainder = bigInteger.divideAndRemainder(val);
            final BigInteger bigInteger2 = divideAndRemainder[0];
            final BigInteger bigInteger3 = divideAndRemainder[1];
            subtract = one.subtract(bigInteger2.multiply(zero));
            subtract2 = zero2.subtract(bigInteger2.multiply(one2));
            bigInteger4 = bigInteger3;
            bigInteger = val;
        }
        if (val.signum() <= 0) {
            return null;
        }
        array = new BigInteger[] { zero, one2 };
        if (b) {
            swap(array);
        }
        return array;
    }
    
    private static BigInteger[] extEuclidGLV(final BigInteger bigInteger, BigInteger val) {
        BigInteger zero = ECConstants.ZERO;
        BigInteger one = ECConstants.ONE;
        BigInteger bigInteger2 = bigInteger;
        BigInteger bigInteger4;
        BigInteger subtract;
        while (true) {
            final BigInteger[] divideAndRemainder = bigInteger2.divideAndRemainder(val);
            final BigInteger bigInteger3 = divideAndRemainder[0];
            bigInteger4 = divideAndRemainder[1];
            subtract = zero.subtract(bigInteger3.multiply(one));
            if (isLessThanSqrt(val, bigInteger)) {
                break;
            }
            bigInteger2 = val;
            val = bigInteger4;
            final BigInteger bigInteger5 = subtract;
            zero = one;
            one = bigInteger5;
        }
        return new BigInteger[] { bigInteger2, zero, val, one, bigInteger4, subtract };
    }
    
    private static ECFieldElement[] findNonTrivialOrder3FieldElements(final ECCurve ecCurve) {
        final BigInteger characteristic = ecCurve.getField().getCharacteristic();
        final BigInteger divide = characteristic.divide(ECConstants.THREE);
        final SecureRandom secureRandom = new SecureRandom();
        BigInteger modPow;
        do {
            modPow = BigIntegers.createRandomInRange(ECConstants.TWO, characteristic.subtract(ECConstants.TWO), secureRandom).modPow(divide, characteristic);
        } while (modPow.equals(ECConstants.ONE));
        final ECFieldElement fromBigInteger = ecCurve.fromBigInteger(modPow);
        return new ECFieldElement[] { fromBigInteger, fromBigInteger.square() };
    }
    
    private static ECFieldElement[] findNonTrivialOrder4FieldElements(final ECCurve ecCurve) {
        final ECFieldElement sqrt = ecCurve.fromBigInteger(ECConstants.ONE).negate().sqrt();
        if (sqrt != null) {
            return new ECFieldElement[] { sqrt, sqrt.negate() };
        }
        throw new IllegalStateException("Calculation of non-trivial order-4  field elements failed unexpectedly");
    }
    
    private static BigInteger[] intersect(final BigInteger[] array, final BigInteger[] array2) {
        final BigInteger max = array[0].max(array2[0]);
        final BigInteger min = array[1].min(array2[1]);
        if (max.compareTo(min) > 0) {
            return null;
        }
        return new BigInteger[] { max, min };
    }
    
    private static boolean isLessThanSqrt(BigInteger abs, BigInteger abs2) {
        abs = abs.abs();
        abs2 = abs2.abs();
        final int bitLength = abs2.bitLength();
        final int n = abs.bitLength() * 2;
        return n - 1 <= bitLength && (n < bitLength || abs.multiply(abs).compareTo(abs2) < 0);
    }
    
    private static boolean isShorter(final BigInteger[] array, final BigInteger[] array2) {
        final boolean b = false;
        final BigInteger abs = array[0].abs();
        final BigInteger abs2 = array[1].abs();
        final BigInteger abs3 = array2[0].abs();
        final BigInteger abs4 = array2[1].abs();
        final boolean b2 = abs.compareTo(abs3) < 0;
        if (b2 == abs2.compareTo(abs4) < 0) {
            return b2;
        }
        boolean b3 = b;
        if (abs.multiply(abs).add(abs2.multiply(abs2)).compareTo(abs3.multiply(abs3).add(abs4.multiply(abs4))) < 0) {
            b3 = true;
        }
        return b3;
    }
    
    private static boolean isVectorBoundedBySqrt(final BigInteger[] array, final BigInteger bigInteger) {
        return isLessThanSqrt(array[0].abs().max(array[1].abs()), bigInteger);
    }
    
    private static BigInteger isqrt(final BigInteger bigInteger) {
        BigInteger shiftRight = bigInteger.shiftRight(bigInteger.bitLength() / 2);
        BigInteger shiftRight2;
        while (true) {
            shiftRight2 = shiftRight.add(bigInteger.divide(shiftRight)).shiftRight(1);
            if (shiftRight2.equals(shiftRight)) {
                break;
            }
            shiftRight = shiftRight2;
        }
        return shiftRight2;
    }
    
    public static void main(final String[] array) {
        if (array.length > 0) {
            for (int i = 0; i < array.length; ++i) {
                discoverEndomorphisms(array[i]);
            }
        }
        else {
            final TreeSet<String> set = new TreeSet<String>(enumToList(ECNamedCurveTable.getNames()));
            set.addAll((Collection<?>)enumToList(CustomNamedCurves.getNames()));
            final Iterator<Object> iterator = set.iterator();
            while (iterator.hasNext()) {
                discoverEndomorphisms(iterator.next());
            }
        }
    }
    
    private static BigInteger[] order(final BigInteger bigInteger, final BigInteger val) {
        if (bigInteger.compareTo(val) <= 0) {
            return new BigInteger[] { bigInteger, val };
        }
        return new BigInteger[] { val, bigInteger };
    }
    
    private static void printGLVTypeAParameters(final X9ECParameters x9ECParameters) {
        final BigInteger[] solveQuadraticEquation = solveQuadraticEquation(x9ECParameters.getN(), ECConstants.ONE, ECConstants.ZERO, ECConstants.ONE);
        final ECFieldElement[] nonTrivialOrder4FieldElements = findNonTrivialOrder4FieldElements(x9ECParameters.getCurve());
        printGLVTypeAParameters(x9ECParameters, solveQuadraticEquation[0], nonTrivialOrder4FieldElements);
        System.out.println("OR");
        printGLVTypeAParameters(x9ECParameters, solveQuadraticEquation[1], nonTrivialOrder4FieldElements);
    }
    
    private static void printGLVTypeAParameters(final X9ECParameters x9ECParameters, final BigInteger bigInteger, final ECFieldElement[] array) {
        final ECPoint normalize = x9ECParameters.getG().normalize();
        final ECPoint normalize2 = normalize.multiply(bigInteger).normalize();
        if (normalize.getXCoord().negate().equals(normalize2.getXCoord())) {
            ECFieldElement ecFieldElement;
            if (!normalize.getYCoord().multiply(ecFieldElement = array[0]).equals(normalize2.getYCoord())) {
                ecFieldElement = array[1];
                if (!normalize.getYCoord().multiply(ecFieldElement).equals(normalize2.getYCoord())) {
                    throw new IllegalStateException("Derivation of GLV Type A parameters failed unexpectedly");
                }
            }
            printProperty("Point map", "lambda * (x, y) = (-x, i * y)");
            printProperty("i", ecFieldElement.toBigInteger().toString(16));
            printProperty("lambda", bigInteger.toString(16));
            printScalarDecompositionParameters(x9ECParameters.getN(), bigInteger);
            return;
        }
        throw new IllegalStateException("Derivation of GLV Type A parameters failed unexpectedly");
    }
    
    private static void printGLVTypeBParameters(final X9ECParameters x9ECParameters) {
        final BigInteger[] solveQuadraticEquation = solveQuadraticEquation(x9ECParameters.getN(), ECConstants.ONE, ECConstants.ONE, ECConstants.ONE);
        final ECFieldElement[] nonTrivialOrder3FieldElements = findNonTrivialOrder3FieldElements(x9ECParameters.getCurve());
        printGLVTypeBParameters(x9ECParameters, solveQuadraticEquation[0], nonTrivialOrder3FieldElements);
        System.out.println("OR");
        printGLVTypeBParameters(x9ECParameters, solveQuadraticEquation[1], nonTrivialOrder3FieldElements);
    }
    
    private static void printGLVTypeBParameters(final X9ECParameters x9ECParameters, final BigInteger bigInteger, final ECFieldElement[] array) {
        final ECPoint normalize = x9ECParameters.getG().normalize();
        final ECPoint normalize2 = normalize.multiply(bigInteger).normalize();
        if (normalize.getYCoord().equals(normalize2.getYCoord())) {
            ECFieldElement ecFieldElement;
            if (!normalize.getXCoord().multiply(ecFieldElement = array[0]).equals(normalize2.getXCoord())) {
                ecFieldElement = array[1];
                if (!normalize.getXCoord().multiply(ecFieldElement).equals(normalize2.getXCoord())) {
                    throw new IllegalStateException("Derivation of GLV Type B parameters failed unexpectedly");
                }
            }
            printProperty("Point map", "lambda * (x, y) = (beta * x, y)");
            printProperty("beta", ecFieldElement.toBigInteger().toString(16));
            printProperty("lambda", bigInteger.toString(16));
            printScalarDecompositionParameters(x9ECParameters.getN(), bigInteger);
            return;
        }
        throw new IllegalStateException("Derivation of GLV Type B parameters failed unexpectedly");
    }
    
    private static void printProperty(final String str, final Object o) {
        final StringBuffer sb = new StringBuffer("  ");
        sb.append(str);
        while (sb.length() < 20) {
            sb.append(' ');
        }
        sb.append(": ");
        sb.append(o.toString());
        System.out.println(sb.toString());
    }
    
    private static void printScalarDecompositionParameters(BigInteger roundQuotient, BigInteger bigInteger) {
        final BigInteger[] extEuclidGLV = extEuclidGLV(roundQuotient, bigInteger);
        final BigInteger[] array = { extEuclidGLV[2], extEuclidGLV[3].negate() };
        BigInteger[] chooseShortest;
        final BigInteger[] array2 = chooseShortest = chooseShortest(new BigInteger[] { extEuclidGLV[0], extEuclidGLV[1].negate() }, new BigInteger[] { extEuclidGLV[4], extEuclidGLV[5].negate() });
        if (!isVectorBoundedBySqrt(array2, roundQuotient)) {
            chooseShortest = array2;
            if (areRelativelyPrime(array[0], array[1])) {
                final BigInteger val = array[0];
                final BigInteger val2 = array[1];
                final BigInteger divide = val.add(val2.multiply(bigInteger)).divide(roundQuotient);
                final BigInteger[] extEuclidBezout = extEuclidBezout(new BigInteger[] { divide.abs(), val2.abs() });
                chooseShortest = array2;
                if (extEuclidBezout != null) {
                    final BigInteger bigInteger2 = extEuclidBezout[0];
                    final BigInteger bigInteger3 = extEuclidBezout[1];
                    BigInteger negate = bigInteger2;
                    if (divide.signum() < 0) {
                        negate = bigInteger2.negate();
                    }
                    BigInteger negate2 = bigInteger3;
                    if (val2.signum() > 0) {
                        negate2 = bigInteger3.negate();
                    }
                    if (!divide.multiply(negate).subtract(val2.multiply(negate2)).equals(ECConstants.ONE)) {
                        throw new IllegalStateException();
                    }
                    final BigInteger subtract = negate2.multiply(roundQuotient).subtract(negate.multiply(bigInteger));
                    bigInteger = negate.negate();
                    final BigInteger negate3 = subtract.negate();
                    final BigInteger add = isqrt(roundQuotient.subtract(ECConstants.ONE)).add(ECConstants.ONE);
                    final BigInteger[] intersect = intersect(calculateRange(bigInteger, add, val2), calculateRange(negate3, add, val));
                    chooseShortest = array2;
                    if (intersect != null) {
                        BigInteger add2 = intersect[0];
                        BigInteger[] array3 = array2;
                        while (true) {
                            chooseShortest = array3;
                            if (add2.compareTo(intersect[1]) > 0) {
                                break;
                            }
                            final BigInteger[] array4 = { subtract.add(add2.multiply(val)), negate.add(add2.multiply(val2)) };
                            BigInteger[] array5 = array3;
                            if (isShorter(array4, array3)) {
                                array5 = array4;
                            }
                            add2 = add2.add(ECConstants.ONE);
                            array3 = array5;
                        }
                    }
                }
            }
        }
        bigInteger = array[0].multiply(chooseShortest[1]).subtract(array[1].multiply(chooseShortest[0]));
        final int i = roundQuotient.bitLength() + 16 - (roundQuotient.bitLength() & 0x7);
        roundQuotient = roundQuotient(chooseShortest[1].shiftLeft(i), bigInteger);
        final BigInteger negate4 = roundQuotient(array[1].shiftLeft(i), bigInteger).negate();
        final StringBuilder sb = new StringBuilder();
        sb.append("{ ");
        sb.append(array[0].toString(16));
        sb.append(", ");
        sb.append(array[1].toString(16));
        sb.append(" }");
        printProperty("v1", sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("{ ");
        sb2.append(chooseShortest[0].toString(16));
        sb2.append(", ");
        sb2.append(chooseShortest[1].toString(16));
        sb2.append(" }");
        printProperty("v2", sb2.toString());
        printProperty("d", bigInteger.toString(16));
        printProperty("(OPT) g1", roundQuotient.toString(16));
        printProperty("(OPT) g2", negate4.toString(16));
        printProperty("(OPT) bits", Integer.toString(i));
    }
    
    private static BigInteger roundQuotient(BigInteger bigInteger, BigInteger abs) {
        final boolean b = bigInteger.signum() != abs.signum();
        bigInteger = bigInteger.abs();
        abs = abs.abs();
        abs = (bigInteger = bigInteger.add(abs.shiftRight(1)).divide(abs));
        if (b) {
            bigInteger = abs.negate();
        }
        return bigInteger;
    }
    
    private static BigInteger[] solveQuadraticEquation(final BigInteger bigInteger, BigInteger modInverse, final BigInteger val, BigInteger bigInteger2) {
        final ECFieldElement sqrt = new ECFieldElement.Fp(bigInteger, val.multiply(val).subtract(modInverse.multiply(bigInteger2).shiftLeft(2)).mod(bigInteger)).sqrt();
        if (sqrt != null) {
            bigInteger2 = sqrt.toBigInteger();
            modInverse = modInverse.shiftLeft(1).modInverse(bigInteger);
            return new BigInteger[] { bigInteger2.subtract(val).multiply(modInverse).mod(bigInteger), bigInteger2.negate().subtract(val).multiply(modInverse).mod(bigInteger) };
        }
        throw new IllegalStateException("Solving quadratic equation failed unexpectedly");
    }
    
    private static void swap(final BigInteger[] array) {
        final BigInteger bigInteger = array[0];
        array[0] = array[1];
        array[1] = bigInteger;
    }
}
