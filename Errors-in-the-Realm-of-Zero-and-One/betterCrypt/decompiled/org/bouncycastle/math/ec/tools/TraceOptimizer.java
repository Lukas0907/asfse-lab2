// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.tools;

import java.util.Iterator;
import org.bouncycastle.math.ec.ECAlgorithms;
import org.bouncycastle.crypto.ec.CustomNamedCurves;
import java.util.Collection;
import java.util.TreeSet;
import org.bouncycastle.asn1.x9.ECNamedCurveTable;
import java.io.PrintStream;
import org.bouncycastle.math.ec.ECCurve;
import java.util.Random;
import org.bouncycastle.asn1.x9.X9ECParameters;
import java.util.ArrayList;
import java.util.Enumeration;
import org.bouncycastle.util.Integers;
import org.bouncycastle.math.ec.ECFieldElement;
import java.security.SecureRandom;
import java.math.BigInteger;

public class TraceOptimizer
{
    private static final BigInteger ONE;
    private static final SecureRandom R;
    
    static {
        ONE = BigInteger.valueOf(1L);
        R = new SecureRandom();
    }
    
    private static int calculateTrace(final ECFieldElement ecFieldElement) {
        final int fieldSize = ecFieldElement.getFieldSize();
        int i = 31 - Integers.numberOfLeadingZeros(fieldSize);
        ECFieldElement add = ecFieldElement;
        int n = 1;
        while (i > 0) {
            final ECFieldElement add2 = add.squarePow(n).add(add);
            final int n2 = i - 1;
            final int n3 = fieldSize >>> n2;
            i = n2;
            add = add2;
            n = n3;
            if ((n3 & 0x1) != 0x0) {
                add = add2.square().add(ecFieldElement);
                i = n2;
                n = n3;
            }
        }
        if (add.isZero()) {
            return 0;
        }
        if (add.isOne()) {
            return 1;
        }
        throw new IllegalStateException("Internal error in trace calculation");
    }
    
    private static ArrayList enumToList(final Enumeration enumeration) {
        final ArrayList<Object> list = new ArrayList<Object>();
        while (enumeration.hasMoreElements()) {
            list.add(enumeration.nextElement());
        }
        return list;
    }
    
    public static void implPrintNonZeroTraceBits(final X9ECParameters x9ECParameters) {
        final ECCurve curve = x9ECParameters.getCurve();
        final int fieldSize = curve.getFieldSize();
        final ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < fieldSize; ++i) {
            PrintStream printStream;
            StringBuilder sb;
            if ((i & 0x1) == 0x0 && i != 0) {
                if (!list.contains(Integers.valueOf(i >>> 1))) {
                    continue;
                }
                list.add(Integers.valueOf(i));
                printStream = System.out;
                sb = new StringBuilder();
            }
            else {
                if (calculateTrace(curve.fromBigInteger(TraceOptimizer.ONE.shiftLeft(i))) == 0) {
                    continue;
                }
                list.add(Integers.valueOf(i));
                printStream = System.out;
                sb = new StringBuilder();
            }
            sb.append(" ");
            sb.append(i);
            printStream.print(sb.toString());
        }
        System.out.println();
        for (int j = 0; j < 1000; ++j) {
            final BigInteger bigInteger = new BigInteger(fieldSize, TraceOptimizer.R);
            final int calculateTrace = calculateTrace(curve.fromBigInteger(bigInteger));
            int n;
            int n2;
            for (int k = n = 0; k < list.size(); ++k, n = n2) {
                n2 = n;
                if (bigInteger.testBit(list.get(k))) {
                    n2 = (n ^ 0x1);
                }
            }
            if (calculateTrace != n) {
                throw new IllegalStateException("Optimized-trace sanity check failed");
            }
        }
    }
    
    public static void main(final String[] array) {
        final TreeSet<String> set = new TreeSet<String>(enumToList(ECNamedCurveTable.getNames()));
        set.addAll((Collection<?>)enumToList(CustomNamedCurves.getNames()));
        for (final String str : set) {
            X9ECParameters x9ECParameters;
            if ((x9ECParameters = CustomNamedCurves.getByName(str)) == null) {
                x9ECParameters = ECNamedCurveTable.getByName(str);
            }
            if (x9ECParameters != null && ECAlgorithms.isF2mCurve(x9ECParameters.getCurve())) {
                final PrintStream out = System.out;
                final StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(":");
                out.print(sb.toString());
                implPrintNonZeroTraceBits(x9ECParameters);
            }
        }
    }
    
    public static void printNonZeroTraceBits(final X9ECParameters x9ECParameters) {
        if (ECAlgorithms.isF2mCurve(x9ECParameters.getCurve())) {
            implPrintNonZeroTraceBits(x9ECParameters);
            return;
        }
        throw new IllegalArgumentException("Trace only defined over characteristic-2 fields");
    }
}
