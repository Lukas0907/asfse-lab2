// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec;

import java.math.BigInteger;
import java.util.Hashtable;

public abstract class ECPoint
{
    protected static final ECFieldElement[] EMPTY_ZS;
    protected ECCurve curve;
    protected Hashtable preCompTable;
    protected ECFieldElement x;
    protected ECFieldElement y;
    protected ECFieldElement[] zs;
    
    static {
        EMPTY_ZS = new ECFieldElement[0];
    }
    
    protected ECPoint(final ECCurve ecCurve, final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2) {
        this(ecCurve, ecFieldElement, ecFieldElement2, getInitialZCoords(ecCurve));
    }
    
    protected ECPoint(final ECCurve curve, final ECFieldElement x, final ECFieldElement y, final ECFieldElement[] zs) {
        this.preCompTable = null;
        this.curve = curve;
        this.x = x;
        this.y = y;
        this.zs = zs;
    }
    
    protected static ECFieldElement[] getInitialZCoords(final ECCurve ecCurve) {
        int coordinateSystem;
        if (ecCurve == null) {
            coordinateSystem = 0;
        }
        else {
            coordinateSystem = ecCurve.getCoordinateSystem();
        }
        if (coordinateSystem != 0 && coordinateSystem != 5) {
            final ECFieldElement fromBigInteger = ecCurve.fromBigInteger(ECConstants.ONE);
            if (coordinateSystem != 1 && coordinateSystem != 2) {
                if (coordinateSystem == 3) {
                    return new ECFieldElement[] { fromBigInteger, fromBigInteger, fromBigInteger };
                }
                if (coordinateSystem == 4) {
                    return new ECFieldElement[] { fromBigInteger, ecCurve.getA() };
                }
                if (coordinateSystem != 6) {
                    throw new IllegalArgumentException("unknown coordinate system");
                }
            }
            return new ECFieldElement[] { fromBigInteger };
        }
        return ECPoint.EMPTY_ZS;
    }
    
    public abstract ECPoint add(final ECPoint p0);
    
    protected void checkNormalized() {
        if (this.isNormalized()) {
            return;
        }
        throw new IllegalStateException("point not in normal form");
    }
    
    protected ECPoint createScaledPoint(final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2) {
        return this.getCurve().createRawPoint(this.getRawXCoord().multiply(ecFieldElement), this.getRawYCoord().multiply(ecFieldElement2));
    }
    
    protected abstract ECPoint detach();
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof ECPoint && this.equals((ECPoint)o));
    }
    
    public boolean equals(ECPoint normalize) {
        final boolean b = false;
        final boolean b2 = false;
        if (normalize == null) {
            return false;
        }
        final ECCurve curve = this.getCurve();
        final ECCurve curve2 = normalize.getCurve();
        final boolean b3 = curve == null;
        final boolean b4 = curve2 == null;
        final boolean infinity = this.isInfinity();
        final boolean infinity2 = normalize.isInfinity();
        if (!infinity && !infinity2) {
            ECPoint normalize2 = null;
            Label_0163: {
                if (!b3 || !b4) {
                    if (b3) {
                        normalize = normalize.normalize();
                    }
                    else {
                        if (b4) {
                            normalize2 = this.normalize();
                            break Label_0163;
                        }
                        if (!curve.equals(curve2)) {
                            return false;
                        }
                        final ECPoint[] array = { this, curve.importPoint(normalize) };
                        curve.normalizeAll(array);
                        normalize2 = array[0];
                        normalize = array[1];
                        break Label_0163;
                    }
                }
                normalize2 = this;
            }
            boolean b5 = b2;
            if (normalize2.getXCoord().equals(normalize.getXCoord())) {
                b5 = b2;
                if (normalize2.getYCoord().equals(normalize.getYCoord())) {
                    b5 = true;
                }
            }
            return b5;
        }
        boolean b6 = b;
        if (infinity) {
            b6 = b;
            if (infinity2) {
                if (!b3 && !b4) {
                    b6 = b;
                    if (!curve.equals(curve2)) {
                        return b6;
                    }
                }
                b6 = true;
            }
        }
        return b6;
    }
    
    public ECFieldElement getAffineXCoord() {
        this.checkNormalized();
        return this.getXCoord();
    }
    
    public ECFieldElement getAffineYCoord() {
        this.checkNormalized();
        return this.getYCoord();
    }
    
    protected abstract boolean getCompressionYTilde();
    
    public ECCurve getCurve() {
        return this.curve;
    }
    
    protected int getCurveCoordinateSystem() {
        final ECCurve curve = this.curve;
        if (curve == null) {
            return 0;
        }
        return curve.getCoordinateSystem();
    }
    
    public final ECPoint getDetachedPoint() {
        return this.normalize().detach();
    }
    
    public byte[] getEncoded(final boolean b) {
        if (this.isInfinity()) {
            return new byte[1];
        }
        final ECPoint normalize = this.normalize();
        final byte[] encoded = normalize.getXCoord().getEncoded();
        if (b) {
            final byte[] array = new byte[encoded.length + 1];
            int n;
            if (normalize.getCompressionYTilde()) {
                n = 3;
            }
            else {
                n = 2;
            }
            array[0] = (byte)n;
            System.arraycopy(encoded, 0, array, 1, encoded.length);
            return array;
        }
        final byte[] encoded2 = normalize.getYCoord().getEncoded();
        final byte[] array2 = new byte[encoded.length + encoded2.length + 1];
        array2[0] = 4;
        System.arraycopy(encoded, 0, array2, 1, encoded.length);
        System.arraycopy(encoded2, 0, array2, encoded.length + 1, encoded2.length);
        return array2;
    }
    
    public final ECFieldElement getRawXCoord() {
        return this.x;
    }
    
    public final ECFieldElement getRawYCoord() {
        return this.y;
    }
    
    protected final ECFieldElement[] getRawZCoords() {
        return this.zs;
    }
    
    public ECFieldElement getXCoord() {
        return this.x;
    }
    
    public ECFieldElement getYCoord() {
        return this.y;
    }
    
    public ECFieldElement getZCoord(final int n) {
        if (n >= 0) {
            final ECFieldElement[] zs = this.zs;
            if (n < zs.length) {
                return zs[n];
            }
        }
        return null;
    }
    
    public ECFieldElement[] getZCoords() {
        final ECFieldElement[] zs = this.zs;
        final int length = zs.length;
        if (length == 0) {
            return ECPoint.EMPTY_ZS;
        }
        final ECFieldElement[] array = new ECFieldElement[length];
        System.arraycopy(zs, 0, array, 0, length);
        return array;
    }
    
    @Override
    public int hashCode() {
        final ECCurve curve = this.getCurve();
        int hashCode;
        if (curve == null) {
            hashCode = 0;
        }
        else {
            hashCode = curve.hashCode();
        }
        int n = hashCode;
        if (!this.isInfinity()) {
            final ECPoint normalize = this.normalize();
            n = (hashCode ^ normalize.getXCoord().hashCode() * 17 ^ normalize.getYCoord().hashCode() * 257);
        }
        return n;
    }
    
    boolean implIsValid(final boolean b, final boolean b2) {
        return this.isInfinity() || (((ValidityPrecompInfo)this.getCurve().precompute(this, "bc_validity", new PreCompCallback() {
            @Override
            public PreCompInfo precompute(final PreCompInfo preCompInfo) {
                ValidityPrecompInfo validityPrecompInfo;
                if (preCompInfo instanceof ValidityPrecompInfo) {
                    validityPrecompInfo = (ValidityPrecompInfo)preCompInfo;
                }
                else {
                    validityPrecompInfo = null;
                }
                ValidityPrecompInfo validityPrecompInfo2 = validityPrecompInfo;
                if (validityPrecompInfo == null) {
                    validityPrecompInfo2 = new ValidityPrecompInfo();
                }
                if (validityPrecompInfo2.hasFailed()) {
                    return validityPrecompInfo2;
                }
                if (!validityPrecompInfo2.hasCurveEquationPassed()) {
                    if (!b && !ECPoint.this.satisfiesCurveEquation()) {
                        validityPrecompInfo2.reportFailed();
                        return validityPrecompInfo2;
                    }
                    validityPrecompInfo2.reportCurveEquationPassed();
                }
                if (b2 && !validityPrecompInfo2.hasOrderPassed()) {
                    if (!ECPoint.this.satisfiesOrder()) {
                        validityPrecompInfo2.reportFailed();
                        return validityPrecompInfo2;
                    }
                    validityPrecompInfo2.reportOrderPassed();
                }
                return validityPrecompInfo2;
            }
        })).hasFailed() ^ true);
    }
    
    public boolean isInfinity() {
        final ECFieldElement x = this.x;
        final boolean b = false;
        if (x != null && this.y != null) {
            final ECFieldElement[] zs = this.zs;
            boolean b2 = b;
            if (zs.length <= 0) {
                return b2;
            }
            b2 = b;
            if (!zs[0].isZero()) {
                return b2;
            }
        }
        return true;
    }
    
    public boolean isNormalized() {
        final int curveCoordinateSystem = this.getCurveCoordinateSystem();
        boolean b = false;
        if (curveCoordinateSystem == 0 || curveCoordinateSystem == 5 || this.isInfinity() || this.zs[0].isOne()) {
            b = true;
        }
        return b;
    }
    
    public boolean isValid() {
        return this.implIsValid(false, true);
    }
    
    boolean isValidPartial() {
        return this.implIsValid(false, false);
    }
    
    public ECPoint multiply(final BigInteger bigInteger) {
        return this.getCurve().getMultiplier().multiply(this, bigInteger);
    }
    
    public abstract ECPoint negate();
    
    public ECPoint normalize() {
        if (this.isInfinity()) {
            return this;
        }
        final int curveCoordinateSystem = this.getCurveCoordinateSystem();
        if (curveCoordinateSystem == 0 || curveCoordinateSystem == 5) {
            return this;
        }
        final ECFieldElement zCoord = this.getZCoord(0);
        if (zCoord.isOne()) {
            return this;
        }
        return this.normalize(zCoord.invert());
    }
    
    ECPoint normalize(final ECFieldElement ecFieldElement) {
        final int curveCoordinateSystem = this.getCurveCoordinateSystem();
        if (curveCoordinateSystem != 1) {
            if (curveCoordinateSystem == 2 || curveCoordinateSystem == 3 || curveCoordinateSystem == 4) {
                final ECFieldElement square = ecFieldElement.square();
                return this.createScaledPoint(square, square.multiply(ecFieldElement));
            }
            if (curveCoordinateSystem != 6) {
                throw new IllegalStateException("not a projective coordinate system");
            }
        }
        return this.createScaledPoint(ecFieldElement, ecFieldElement);
    }
    
    protected abstract boolean satisfiesCurveEquation();
    
    protected boolean satisfiesOrder() {
        final boolean equals = ECConstants.ONE.equals(this.curve.getCofactor());
        boolean b = true;
        if (equals) {
            return true;
        }
        final BigInteger order = this.curve.getOrder();
        if (order != null) {
            if (ECAlgorithms.referenceMultiply(this, order).isInfinity()) {
                return true;
            }
            b = false;
        }
        return b;
    }
    
    public ECPoint scaleX(final ECFieldElement ecFieldElement) {
        if (this.isInfinity()) {
            return this;
        }
        return this.getCurve().createRawPoint(this.getRawXCoord().multiply(ecFieldElement), this.getRawYCoord(), this.getRawZCoords());
    }
    
    public ECPoint scaleXNegateY(final ECFieldElement ecFieldElement) {
        if (this.isInfinity()) {
            return this;
        }
        return this.getCurve().createRawPoint(this.getRawXCoord().multiply(ecFieldElement), this.getRawYCoord().negate(), this.getRawZCoords());
    }
    
    public ECPoint scaleY(final ECFieldElement ecFieldElement) {
        if (this.isInfinity()) {
            return this;
        }
        return this.getCurve().createRawPoint(this.getRawXCoord(), this.getRawYCoord().multiply(ecFieldElement), this.getRawZCoords());
    }
    
    public ECPoint scaleYNegateX(final ECFieldElement ecFieldElement) {
        if (this.isInfinity()) {
            return this;
        }
        return this.getCurve().createRawPoint(this.getRawXCoord().negate(), this.getRawYCoord().multiply(ecFieldElement), this.getRawZCoords());
    }
    
    public abstract ECPoint subtract(final ECPoint p0);
    
    public ECPoint threeTimes() {
        return this.twicePlus(this);
    }
    
    public ECPoint timesPow2(int n) {
        if (n >= 0) {
            ECPoint twice = this;
            while (true) {
                --n;
                if (n < 0) {
                    break;
                }
                twice = twice.twice();
            }
            return twice;
        }
        throw new IllegalArgumentException("'e' cannot be negative");
    }
    
    @Override
    public String toString() {
        if (this.isInfinity()) {
            return "INF";
        }
        final StringBuffer sb = new StringBuffer();
        sb.append('(');
        sb.append(this.getRawXCoord());
        sb.append(',');
        sb.append(this.getRawYCoord());
        for (int i = 0; i < this.zs.length; ++i) {
            sb.append(',');
            sb.append(this.zs[i]);
        }
        sb.append(')');
        return sb.toString();
    }
    
    public abstract ECPoint twice();
    
    public ECPoint twicePlus(final ECPoint ecPoint) {
        return this.twice().add(ecPoint);
    }
    
    public abstract static class AbstractF2m extends ECPoint
    {
        protected AbstractF2m(final ECCurve ecCurve, final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2) {
            super(ecCurve, ecFieldElement, ecFieldElement2);
        }
        
        protected AbstractF2m(final ECCurve ecCurve, final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2, final ECFieldElement[] array) {
            super(ecCurve, ecFieldElement, ecFieldElement2, array);
        }
        
        @Override
        protected boolean satisfiesCurveEquation() {
            final ECCurve curve = this.getCurve();
            final ECFieldElement x = this.x;
            final ECFieldElement a = curve.getA();
            final ECFieldElement b = curve.getB();
            final int coordinateSystem = curve.getCoordinateSystem();
            if (coordinateSystem != 6) {
                final ECFieldElement y = this.y;
                final ECFieldElement multiply = y.add(x).multiply(y);
                ECFieldElement multiply2 = a;
                ECFieldElement multiply3 = b;
                ECFieldElement multiply4 = multiply;
                if (coordinateSystem != 0) {
                    if (coordinateSystem != 1) {
                        throw new IllegalStateException("unsupported coordinate system");
                    }
                    final ECFieldElement ecFieldElement = this.zs[0];
                    multiply2 = a;
                    multiply3 = b;
                    multiply4 = multiply;
                    if (!ecFieldElement.isOne()) {
                        final ECFieldElement multiply5 = ecFieldElement.multiply(ecFieldElement.square());
                        multiply4 = multiply.multiply(ecFieldElement);
                        multiply2 = a.multiply(ecFieldElement);
                        multiply3 = b.multiply(multiply5);
                    }
                }
                return multiply4.equals(x.add(multiply2).multiply(x.square()).add(multiply3));
            }
            final ECFieldElement ecFieldElement2 = this.zs[0];
            final boolean one = ecFieldElement2.isOne();
            if (x.isZero()) {
                final ECFieldElement square = this.y.square();
                ECFieldElement multiply6 = b;
                if (!one) {
                    multiply6 = b.multiply(ecFieldElement2.square());
                }
                return square.equals(multiply6);
            }
            final ECFieldElement y2 = this.y;
            final ECFieldElement square2 = x.square();
            ECFieldElement ecFieldElement3;
            ECFieldElement squarePlusProduct;
            if (one) {
                final ECFieldElement add = y2.square().add(y2).add(a);
                final ECFieldElement add2 = square2.square().add(b);
                ecFieldElement3 = add;
                squarePlusProduct = add2;
            }
            else {
                final ECFieldElement square3 = ecFieldElement2.square();
                final ECFieldElement square4 = square3.square();
                final ECFieldElement multiplyPlusProduct = y2.add(ecFieldElement2).multiplyPlusProduct(y2, a, square3);
                squarePlusProduct = square2.squarePlusProduct(b, square4);
                ecFieldElement3 = multiplyPlusProduct;
            }
            return ecFieldElement3.multiply(square2).equals(squarePlusProduct);
        }
        
        @Override
        protected boolean satisfiesOrder() {
            final BigInteger cofactor = this.curve.getCofactor();
            if (ECConstants.TWO.equals(cofactor)) {
                return ((ECFieldElement.AbstractF2m)this.normalize().getAffineXCoord()).trace() != 0;
            }
            if (ECConstants.FOUR.equals(cofactor)) {
                final ECPoint normalize = this.normalize();
                final ECFieldElement affineXCoord = normalize.getAffineXCoord();
                final ECFieldElement solveQuadraticEquation = ((ECCurve.AbstractF2m)this.curve).solveQuadraticEquation(affineXCoord.add(this.curve.getA()));
                return solveQuadraticEquation != null && ((ECFieldElement.AbstractF2m)affineXCoord.multiply(solveQuadraticEquation).add(normalize.getAffineYCoord())).trace() == 0;
            }
            return super.satisfiesOrder();
        }
        
        @Override
        public ECPoint scaleX(ECFieldElement ecFieldElement) {
            if (this.isInfinity()) {
                return this;
            }
            final int curveCoordinateSystem = this.getCurveCoordinateSystem();
            if (curveCoordinateSystem == 5) {
                final ECFieldElement rawXCoord = this.getRawXCoord();
                ecFieldElement = this.getRawYCoord().add(rawXCoord).divide(ecFieldElement).add(rawXCoord.multiply(ecFieldElement));
                return this.getCurve().createRawPoint(rawXCoord, ecFieldElement, this.getRawZCoords());
            }
            if (curveCoordinateSystem != 6) {
                return super.scaleX(ecFieldElement);
            }
            final ECFieldElement rawXCoord2 = this.getRawXCoord();
            final ECFieldElement rawYCoord = this.getRawYCoord();
            final ECFieldElement ecFieldElement2 = this.getRawZCoords()[0];
            final ECFieldElement multiply = rawXCoord2.multiply(ecFieldElement.square());
            final ECFieldElement add = rawYCoord.add(rawXCoord2).add(multiply);
            ecFieldElement = ecFieldElement2.multiply(ecFieldElement);
            return this.getCurve().createRawPoint(multiply, add, new ECFieldElement[] { ecFieldElement });
        }
        
        @Override
        public ECPoint scaleXNegateY(final ECFieldElement ecFieldElement) {
            return this.scaleX(ecFieldElement);
        }
        
        @Override
        public ECPoint scaleY(ECFieldElement add) {
            if (this.isInfinity()) {
                return this;
            }
            final int curveCoordinateSystem = this.getCurveCoordinateSystem();
            if (curveCoordinateSystem != 5 && curveCoordinateSystem != 6) {
                return super.scaleY(add);
            }
            final ECFieldElement rawXCoord = this.getRawXCoord();
            add = this.getRawYCoord().add(rawXCoord).multiply(add).add(rawXCoord);
            return this.getCurve().createRawPoint(rawXCoord, add, this.getRawZCoords());
        }
        
        @Override
        public ECPoint scaleYNegateX(final ECFieldElement ecFieldElement) {
            return this.scaleY(ecFieldElement);
        }
        
        @Override
        public ECPoint subtract(final ECPoint ecPoint) {
            if (ecPoint.isInfinity()) {
                return this;
            }
            return this.add(ecPoint.negate());
        }
        
        public AbstractF2m tau() {
            if (this.isInfinity()) {
                return this;
            }
            final ECCurve curve = this.getCurve();
            final int coordinateSystem = curve.getCoordinateSystem();
            final ECFieldElement x = this.x;
            Label_0102: {
                if (coordinateSystem == 0) {
                    break Label_0102;
                }
                if (coordinateSystem != 1) {
                    if (coordinateSystem == 5) {
                        break Label_0102;
                    }
                    if (coordinateSystem != 6) {
                        throw new IllegalStateException("unsupported coordinate system");
                    }
                }
                final ECPoint ecPoint = curve.createRawPoint(x.square(), this.y.square(), new ECFieldElement[] { this.zs[0].square() });
                return (AbstractF2m)ecPoint;
            }
            final ECPoint ecPoint = curve.createRawPoint(x.square(), this.y.square());
            return (AbstractF2m)ecPoint;
        }
        
        public AbstractF2m tauPow(final int n) {
            if (this.isInfinity()) {
                return this;
            }
            final ECCurve curve = this.getCurve();
            final int coordinateSystem = curve.getCoordinateSystem();
            final ECFieldElement x = this.x;
            Label_0107: {
                if (coordinateSystem == 0) {
                    break Label_0107;
                }
                if (coordinateSystem != 1) {
                    if (coordinateSystem == 5) {
                        break Label_0107;
                    }
                    if (coordinateSystem != 6) {
                        throw new IllegalStateException("unsupported coordinate system");
                    }
                }
                final ECPoint ecPoint = curve.createRawPoint(x.squarePow(n), this.y.squarePow(n), new ECFieldElement[] { this.zs[0].squarePow(n) });
                return (AbstractF2m)ecPoint;
            }
            final ECPoint ecPoint = curve.createRawPoint(x.squarePow(n), this.y.squarePow(n));
            return (AbstractF2m)ecPoint;
        }
    }
    
    public abstract static class AbstractFp extends ECPoint
    {
        protected AbstractFp(final ECCurve ecCurve, final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2) {
            super(ecCurve, ecFieldElement, ecFieldElement2);
        }
        
        protected AbstractFp(final ECCurve ecCurve, final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2, final ECFieldElement[] array) {
            super(ecCurve, ecFieldElement, ecFieldElement2, array);
        }
        
        @Override
        protected boolean getCompressionYTilde() {
            return this.getAffineYCoord().testBitZero();
        }
        
        @Override
        protected boolean satisfiesCurveEquation() {
            final ECFieldElement x = this.x;
            final ECFieldElement y = this.y;
            final ECFieldElement a = this.curve.getA();
            final ECFieldElement b = this.curve.getB();
            final ECFieldElement square = y.square();
            final int curveCoordinateSystem = this.getCurveCoordinateSystem();
            ECFieldElement multiply = square;
            ECFieldElement ecFieldElement = a;
            ECFieldElement ecFieldElement2 = b;
            if (curveCoordinateSystem != 0) {
                if (curveCoordinateSystem != 1) {
                    if (curveCoordinateSystem != 2 && curveCoordinateSystem != 3 && curveCoordinateSystem != 4) {
                        throw new IllegalStateException("unsupported coordinate system");
                    }
                    final ECFieldElement ecFieldElement3 = this.zs[0];
                    multiply = square;
                    ecFieldElement = a;
                    ecFieldElement2 = b;
                    if (!ecFieldElement3.isOne()) {
                        final ECFieldElement square2 = ecFieldElement3.square();
                        final ECFieldElement square3 = square2.square();
                        final ECFieldElement multiply2 = square2.multiply(square3);
                        ecFieldElement = a.multiply(square3);
                        ecFieldElement2 = b.multiply(multiply2);
                        multiply = square;
                    }
                }
                else {
                    final ECFieldElement ecFieldElement4 = this.zs[0];
                    multiply = square;
                    ecFieldElement = a;
                    ecFieldElement2 = b;
                    if (!ecFieldElement4.isOne()) {
                        final ECFieldElement square4 = ecFieldElement4.square();
                        final ECFieldElement multiply3 = ecFieldElement4.multiply(square4);
                        multiply = square.multiply(ecFieldElement4);
                        ecFieldElement = a.multiply(square4);
                        ecFieldElement2 = b.multiply(multiply3);
                    }
                }
            }
            return multiply.equals(x.square().add(ecFieldElement).multiply(x).add(ecFieldElement2));
        }
        
        @Override
        public ECPoint subtract(final ECPoint ecPoint) {
            if (ecPoint.isInfinity()) {
                return this;
            }
            return this.add(ecPoint.negate());
        }
    }
    
    public static class F2m extends AbstractF2m
    {
        F2m(final ECCurve ecCurve, final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2) {
            super(ecCurve, ecFieldElement, ecFieldElement2);
        }
        
        F2m(final ECCurve ecCurve, final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2, final ECFieldElement[] array) {
            super(ecCurve, ecFieldElement, ecFieldElement2, array);
        }
        
        @Override
        public ECPoint add(final ECPoint ecPoint) {
            if (this.isInfinity()) {
                return ecPoint;
            }
            if (ecPoint.isInfinity()) {
                return this;
            }
            final ECCurve curve = this.getCurve();
            final int coordinateSystem = curve.getCoordinateSystem();
            ECFieldElement ecFieldElement = this.x;
            final ECFieldElement x = ecPoint.x;
            if (coordinateSystem != 0) {
                if (coordinateSystem != 1) {
                    if (coordinateSystem != 6) {
                        throw new IllegalStateException("unsupported coordinate system");
                    }
                    if (ecFieldElement.isZero()) {
                        if (x.isZero()) {
                            return curve.getInfinity();
                        }
                        return ecPoint.add(this);
                    }
                    else {
                        final ECFieldElement y = this.y;
                        final ECFieldElement ecFieldElement2 = this.zs[0];
                        final ECFieldElement y2 = ecPoint.y;
                        final ECFieldElement ecFieldElement3 = ecPoint.zs[0];
                        final boolean one = ecFieldElement2.isOne();
                        ECFieldElement multiply;
                        ECFieldElement multiply2;
                        if (!one) {
                            multiply = x.multiply(ecFieldElement2);
                            multiply2 = y2.multiply(ecFieldElement2);
                        }
                        else {
                            multiply = x;
                            multiply2 = y2;
                        }
                        final boolean one2 = ecFieldElement3.isOne();
                        ECFieldElement multiply3;
                        if (!one2) {
                            ecFieldElement = ecFieldElement.multiply(ecFieldElement3);
                            multiply3 = y.multiply(ecFieldElement3);
                        }
                        else {
                            multiply3 = y;
                        }
                        final ECFieldElement add = multiply3.add(multiply2);
                        final ECFieldElement add2 = ecFieldElement.add(multiply);
                        if (!add2.isZero()) {
                            ECFieldElement add3;
                            ECFieldElement add4;
                            ECFieldElement ecFieldElement4;
                            if (x.isZero()) {
                                final ECPoint normalize = this.normalize();
                                final ECFieldElement xCoord = normalize.getXCoord();
                                final ECFieldElement yCoord = normalize.getYCoord();
                                final ECFieldElement divide = yCoord.add(y2).divide(xCoord);
                                add3 = divide.square().add(divide).add(xCoord).add(curve.getA());
                                if (add3.isZero()) {
                                    return new F2m(curve, add3, curve.getB().sqrt());
                                }
                                add4 = divide.multiply(xCoord.add(add3)).add(add3).add(yCoord).divide(add3).add(add3);
                                ecFieldElement4 = curve.fromBigInteger(ECConstants.ONE);
                            }
                            else {
                                final ECFieldElement square = add2.square();
                                final ECFieldElement multiply4 = add.multiply(ecFieldElement);
                                final ECFieldElement multiply5 = add.multiply(multiply);
                                final ECFieldElement multiply6 = multiply4.multiply(multiply5);
                                if (multiply6.isZero()) {
                                    return new F2m(curve, multiply6, curve.getB().sqrt());
                                }
                                ECFieldElement ecFieldElement5 = add.multiply(square);
                                if (!one2) {
                                    ecFieldElement5 = ecFieldElement5.multiply(ecFieldElement3);
                                }
                                final ECFieldElement squarePlusProduct = multiply5.add(square).squarePlusProduct(ecFieldElement5, y.add(ecFieldElement2));
                                ecFieldElement4 = ecFieldElement5;
                                add4 = squarePlusProduct;
                                add3 = multiply6;
                                if (!one) {
                                    ecFieldElement4 = ecFieldElement5.multiply(ecFieldElement2);
                                    add3 = multiply6;
                                    add4 = squarePlusProduct;
                                }
                            }
                            return new F2m(curve, add3, add4, new ECFieldElement[] { ecFieldElement4 });
                        }
                        if (add.isZero()) {
                            return this.twice();
                        }
                        return curve.getInfinity();
                    }
                }
                else {
                    final ECFieldElement y3 = this.y;
                    final ECFieldElement ecFieldElement6 = this.zs[0];
                    final ECFieldElement y4 = ecPoint.y;
                    final ECFieldElement ecFieldElement7 = ecPoint.zs[0];
                    final boolean one3 = ecFieldElement7.isOne();
                    final ECFieldElement multiply7 = ecFieldElement6.multiply(y4);
                    ECFieldElement multiply8;
                    if (one3) {
                        multiply8 = y3;
                    }
                    else {
                        multiply8 = y3.multiply(ecFieldElement7);
                    }
                    final ECFieldElement add5 = multiply7.add(multiply8);
                    final ECFieldElement multiply9 = ecFieldElement6.multiply(x);
                    ECFieldElement multiply10;
                    if (one3) {
                        multiply10 = ecFieldElement;
                    }
                    else {
                        multiply10 = ecFieldElement.multiply(ecFieldElement7);
                    }
                    final ECFieldElement add6 = multiply9.add(multiply10);
                    if (!add6.isZero()) {
                        final ECFieldElement square2 = add6.square();
                        final ECFieldElement multiply11 = square2.multiply(add6);
                        ECFieldElement multiply12;
                        if (one3) {
                            multiply12 = ecFieldElement6;
                        }
                        else {
                            multiply12 = ecFieldElement6.multiply(ecFieldElement7);
                        }
                        final ECFieldElement add7 = add5.add(add6);
                        final ECFieldElement add8 = add7.multiplyPlusProduct(add5, square2, curve.getA()).multiply(multiply12).add(multiply11);
                        final ECFieldElement multiply13 = add6.multiply(add8);
                        ECFieldElement multiply14;
                        if (one3) {
                            multiply14 = square2;
                        }
                        else {
                            multiply14 = square2.multiply(ecFieldElement7);
                        }
                        return new F2m(curve, multiply13, add5.multiplyPlusProduct(ecFieldElement, add6, y3).multiplyPlusProduct(multiply14, add7, add8), new ECFieldElement[] { multiply11.multiply(multiply12) });
                    }
                    if (add5.isZero()) {
                        return this.twice();
                    }
                    return curve.getInfinity();
                }
            }
            else {
                final ECFieldElement y5 = this.y;
                final ECFieldElement y6 = ecPoint.y;
                final ECFieldElement add9 = ecFieldElement.add(x);
                final ECFieldElement add10 = y5.add(y6);
                if (!add9.isZero()) {
                    final ECFieldElement divide2 = add10.divide(add9);
                    final ECFieldElement add11 = divide2.square().add(divide2).add(add9).add(curve.getA());
                    return new F2m(curve, add11, divide2.multiply(ecFieldElement.add(add11)).add(add11).add(y5));
                }
                if (add10.isZero()) {
                    return this.twice();
                }
                return curve.getInfinity();
            }
        }
        
        @Override
        protected ECPoint detach() {
            return new F2m(null, this.getAffineXCoord(), this.getAffineYCoord());
        }
        
        @Override
        protected boolean getCompressionYTilde() {
            final ECFieldElement rawXCoord = this.getRawXCoord();
            final boolean zero = rawXCoord.isZero();
            boolean b = false;
            if (zero) {
                return false;
            }
            final ECFieldElement rawYCoord = this.getRawYCoord();
            final int curveCoordinateSystem = this.getCurveCoordinateSystem();
            if (curveCoordinateSystem != 5 && curveCoordinateSystem != 6) {
                return rawYCoord.divide(rawXCoord).testBitZero();
            }
            if (rawYCoord.testBitZero() != rawXCoord.testBitZero()) {
                b = true;
            }
            return b;
        }
        
        @Override
        public ECFieldElement getYCoord() {
            final int curveCoordinateSystem = this.getCurveCoordinateSystem();
            if (curveCoordinateSystem != 5 && curveCoordinateSystem != 6) {
                return this.y;
            }
            final ECFieldElement x = this.x;
            final ECFieldElement y = this.y;
            if (this.isInfinity()) {
                return y;
            }
            if (x.isZero()) {
                return y;
            }
            ECFieldElement ecFieldElement2;
            final ECFieldElement ecFieldElement = ecFieldElement2 = y.add(x).multiply(x);
            if (6 == curveCoordinateSystem) {
                final ECFieldElement ecFieldElement3 = this.zs[0];
                ecFieldElement2 = ecFieldElement;
                if (!ecFieldElement3.isOne()) {
                    ecFieldElement2 = ecFieldElement.divide(ecFieldElement3);
                }
            }
            return ecFieldElement2;
        }
        
        @Override
        public ECPoint negate() {
            if (this.isInfinity()) {
                return this;
            }
            final ECFieldElement x = this.x;
            if (x.isZero()) {
                return this;
            }
            final int curveCoordinateSystem = this.getCurveCoordinateSystem();
            if (curveCoordinateSystem == 0) {
                return new F2m(this.curve, x, this.y.add(x));
            }
            if (curveCoordinateSystem == 1) {
                return new F2m(this.curve, x, this.y.add(x), new ECFieldElement[] { this.zs[0] });
            }
            if (curveCoordinateSystem == 5) {
                return new F2m(this.curve, x, this.y.addOne());
            }
            if (curveCoordinateSystem == 6) {
                final ECFieldElement y = this.y;
                final ECFieldElement ecFieldElement = this.zs[0];
                return new F2m(this.curve, x, y.add(ecFieldElement), new ECFieldElement[] { ecFieldElement });
            }
            throw new IllegalStateException("unsupported coordinate system");
        }
        
        @Override
        public ECPoint twice() {
            if (this.isInfinity()) {
                return this;
            }
            final ECCurve curve = this.getCurve();
            ECFieldElement ecFieldElement = this.x;
            if (ecFieldElement.isZero()) {
                return curve.getInfinity();
            }
            final int coordinateSystem = curve.getCoordinateSystem();
            if (coordinateSystem == 0) {
                final ECFieldElement add = this.y.divide(ecFieldElement).add(ecFieldElement);
                final ECFieldElement add2 = add.square().add(add).add(curve.getA());
                return new F2m(curve, add2, ecFieldElement.squarePlusProduct(add2, add.addOne()));
            }
            if (coordinateSystem == 1) {
                ECFieldElement ecFieldElement2 = this.y;
                final ECFieldElement ecFieldElement3 = this.zs[0];
                final boolean one = ecFieldElement3.isOne();
                ECFieldElement multiply;
                if (one) {
                    multiply = ecFieldElement;
                }
                else {
                    multiply = ecFieldElement.multiply(ecFieldElement3);
                }
                if (!one) {
                    ecFieldElement2 = ecFieldElement2.multiply(ecFieldElement3);
                }
                final ECFieldElement square = ecFieldElement.square();
                final ECFieldElement add3 = square.add(ecFieldElement2);
                final ECFieldElement square2 = multiply.square();
                final ECFieldElement add4 = add3.add(multiply);
                final ECFieldElement multiplyPlusProduct = add4.multiplyPlusProduct(add3, square2, curve.getA());
                return new F2m(curve, multiply.multiply(multiplyPlusProduct), square.square().multiplyPlusProduct(multiply, multiplyPlusProduct, add4), new ECFieldElement[] { multiply.multiply(square2) });
            }
            if (coordinateSystem != 6) {
                throw new IllegalStateException("unsupported coordinate system");
            }
            final ECFieldElement y = this.y;
            final ECFieldElement ecFieldElement4 = this.zs[0];
            final boolean one2 = ecFieldElement4.isOne();
            ECFieldElement multiply2;
            if (one2) {
                multiply2 = y;
            }
            else {
                multiply2 = y.multiply(ecFieldElement4);
            }
            ECFieldElement square3;
            if (one2) {
                square3 = ecFieldElement4;
            }
            else {
                square3 = ecFieldElement4.square();
            }
            final ECFieldElement a = curve.getA();
            ECFieldElement multiply3;
            if (one2) {
                multiply3 = a;
            }
            else {
                multiply3 = a.multiply(square3);
            }
            final ECFieldElement add5 = y.square().add(multiply2).add(multiply3);
            if (add5.isZero()) {
                return new F2m(curve, add5, curve.getB().sqrt());
            }
            final ECFieldElement square4 = add5.square();
            ECFieldElement multiply4;
            if (one2) {
                multiply4 = add5;
            }
            else {
                multiply4 = add5.multiply(square3);
            }
            final ECFieldElement b = curve.getB();
            ECFieldElement ecFieldElement6;
            if (b.bitLength() < curve.getFieldSize() >> 1) {
                final ECFieldElement square5 = y.add(ecFieldElement).square();
                ECFieldElement ecFieldElement5;
                if (b.isOne()) {
                    ecFieldElement5 = multiply3.add(square3).square();
                }
                else {
                    ecFieldElement5 = multiply3.squarePlusProduct(b, square3.square());
                }
                ecFieldElement6 = square5.add(add5).add(square3).multiply(square5).add(ecFieldElement5).add(square4);
                if (!a.isZero()) {
                    ECFieldElement ecFieldElement7 = ecFieldElement6;
                    if (!a.isOne()) {
                        ecFieldElement7 = ecFieldElement6.add(a.addOne().multiply(multiply4));
                        return new F2m(curve, square4, ecFieldElement7, new ECFieldElement[] { multiply4 });
                    }
                    return new F2m(curve, square4, ecFieldElement7, new ECFieldElement[] { multiply4 });
                }
            }
            else {
                if (!one2) {
                    ecFieldElement = ecFieldElement.multiply(ecFieldElement4);
                }
                ecFieldElement6 = ecFieldElement.squarePlusProduct(add5, multiply2).add(square4);
            }
            ECFieldElement ecFieldElement7 = ecFieldElement6.add(multiply4);
            return new F2m(curve, square4, ecFieldElement7, new ECFieldElement[] { multiply4 });
        }
        
        @Override
        public ECPoint twicePlus(final ECPoint ecPoint) {
            if (this.isInfinity()) {
                return ecPoint;
            }
            if (ecPoint.isInfinity()) {
                return this.twice();
            }
            final ECCurve curve = this.getCurve();
            final ECFieldElement x = this.x;
            if (x.isZero()) {
                return ecPoint;
            }
            if (curve.getCoordinateSystem() != 6) {
                return this.twice().add(ecPoint);
            }
            final ECFieldElement x2 = ecPoint.x;
            final ECFieldElement ecFieldElement = ecPoint.zs[0];
            if (x2.isZero() || !ecFieldElement.isOne()) {
                return this.twice().add(ecPoint);
            }
            final ECFieldElement y = this.y;
            final ECFieldElement ecFieldElement2 = this.zs[0];
            final ECFieldElement y2 = ecPoint.y;
            final ECFieldElement square = x.square();
            final ECFieldElement square2 = y.square();
            final ECFieldElement square3 = ecFieldElement2.square();
            final ECFieldElement add = curve.getA().multiply(square3).add(square2).add(y.multiply(ecFieldElement2));
            final ECFieldElement addOne = y2.addOne();
            final ECFieldElement multiplyPlusProduct = curve.getA().add(addOne).multiply(square3).add(square2).multiplyPlusProduct(add, square, square3);
            final ECFieldElement multiply = x2.multiply(square3);
            final ECFieldElement square4 = multiply.add(add).square();
            if (square4.isZero()) {
                if (multiplyPlusProduct.isZero()) {
                    return ecPoint.twice();
                }
                return curve.getInfinity();
            }
            else {
                if (multiplyPlusProduct.isZero()) {
                    return new F2m(curve, multiplyPlusProduct, curve.getB().sqrt());
                }
                final ECFieldElement multiply2 = multiplyPlusProduct.square().multiply(multiply);
                final ECFieldElement multiply3 = multiplyPlusProduct.multiply(square4).multiply(square3);
                return new F2m(curve, multiply2, multiplyPlusProduct.add(square4).square().multiplyPlusProduct(add, addOne, multiply3), new ECFieldElement[] { multiply3 });
            }
        }
    }
    
    public static class Fp extends AbstractFp
    {
        Fp(final ECCurve ecCurve, final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2) {
            super(ecCurve, ecFieldElement, ecFieldElement2);
        }
        
        Fp(final ECCurve ecCurve, final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2, final ECFieldElement[] array) {
            super(ecCurve, ecFieldElement, ecFieldElement2, array);
        }
        
        @Override
        public ECPoint add(final ECPoint ecPoint) {
            if (this.isInfinity()) {
                return ecPoint;
            }
            if (ecPoint.isInfinity()) {
                return this;
            }
            if (this == ecPoint) {
                return this.twice();
            }
            final ECCurve curve = this.getCurve();
            final int coordinateSystem = curve.getCoordinateSystem();
            ECFieldElement ecFieldElement = this.x;
            ECFieldElement ecFieldElement2 = this.y;
            ECFieldElement ecFieldElement3 = ecPoint.x;
            ECFieldElement ecFieldElement4 = ecPoint.y;
            if (coordinateSystem != 0) {
                if (coordinateSystem != 1) {
                    if (coordinateSystem != 2 && coordinateSystem != 4) {
                        throw new IllegalStateException("unsupported coordinate system");
                    }
                    final ECFieldElement ecFieldElement5 = this.zs[0];
                    final ECFieldElement obj = ecPoint.zs[0];
                    final boolean one = ecFieldElement5.isOne();
                    ECFieldElement subtract3 = null;
                    ECFieldElement subtract4 = null;
                    ECFieldElement ecFieldElement6 = null;
                    ECFieldElement ecFieldElement7 = null;
                    Label_0513: {
                        ECFieldElement multiply4;
                        if (!one && ecFieldElement5.equals(obj)) {
                            final ECFieldElement subtract = ecFieldElement.subtract(ecFieldElement3);
                            final ECFieldElement subtract2 = ecFieldElement2.subtract(ecFieldElement4);
                            if (subtract.isZero()) {
                                if (subtract2.isZero()) {
                                    return this.twice();
                                }
                                return curve.getInfinity();
                            }
                            else {
                                final ECFieldElement square = subtract.square();
                                final ECFieldElement multiply = ecFieldElement.multiply(square);
                                final ECFieldElement multiply2 = ecFieldElement3.multiply(square);
                                final ECFieldElement multiply3 = multiply.subtract(multiply2).multiply(ecFieldElement2);
                                subtract3 = subtract2.square().subtract(multiply).subtract(multiply2);
                                subtract4 = multiply.subtract(subtract3).multiply(subtract2).subtract(multiply3);
                                multiply4 = subtract.multiply(ecFieldElement5);
                            }
                        }
                        else {
                            if (!one) {
                                final ECFieldElement square2 = ecFieldElement5.square();
                                ecFieldElement3 = square2.multiply(ecFieldElement3);
                                ecFieldElement4 = square2.multiply(ecFieldElement5).multiply(ecFieldElement4);
                            }
                            final boolean one2 = obj.isOne();
                            if (!one2) {
                                final ECFieldElement square3 = obj.square();
                                ecFieldElement = square3.multiply(ecFieldElement);
                                ecFieldElement2 = square3.multiply(obj).multiply(ecFieldElement2);
                            }
                            final ECFieldElement subtract5 = ecFieldElement.subtract(ecFieldElement3);
                            final ECFieldElement subtract6 = ecFieldElement2.subtract(ecFieldElement4);
                            if (subtract5.isZero()) {
                                if (subtract6.isZero()) {
                                    return this.twice();
                                }
                                return curve.getInfinity();
                            }
                            else {
                                final ECFieldElement square4 = subtract5.square();
                                final ECFieldElement multiply5 = square4.multiply(subtract5);
                                final ECFieldElement multiply6 = square4.multiply(ecFieldElement);
                                final ECFieldElement subtract7 = subtract6.square().add(multiply5).subtract(this.two(multiply6));
                                final ECFieldElement multiplyMinusProduct = multiply6.subtract(subtract7).multiplyMinusProduct(subtract6, multiply5, ecFieldElement2);
                                if (!one) {
                                    ecFieldElement6 = subtract5.multiply(ecFieldElement5);
                                }
                                else {
                                    ecFieldElement6 = subtract5;
                                }
                                if (!one2) {
                                    ecFieldElement6 = ecFieldElement6.multiply(obj);
                                }
                                multiply4 = ecFieldElement6;
                                subtract4 = multiplyMinusProduct;
                                subtract3 = subtract7;
                                if (ecFieldElement6 == subtract5) {
                                    ecFieldElement7 = square4;
                                    subtract3 = subtract7;
                                    subtract4 = multiplyMinusProduct;
                                    break Label_0513;
                                }
                            }
                        }
                        final ECFieldElement ecFieldElement8 = null;
                        ecFieldElement6 = multiply4;
                        ecFieldElement7 = ecFieldElement8;
                    }
                    ECFieldElement[] array;
                    if (coordinateSystem == 4) {
                        array = new ECFieldElement[] { ecFieldElement6, this.calculateJacobianModifiedW(ecFieldElement6, ecFieldElement7) };
                    }
                    else {
                        array = new ECFieldElement[] { ecFieldElement6 };
                    }
                    return new Fp(curve, subtract3, subtract4, array);
                }
                else {
                    final ECFieldElement ecFieldElement9 = this.zs[0];
                    ECFieldElement multiply7 = ecPoint.zs[0];
                    final boolean one3 = ecFieldElement9.isOne();
                    final boolean one4 = multiply7.isOne();
                    if (!one3) {
                        ecFieldElement4 = ecFieldElement4.multiply(ecFieldElement9);
                    }
                    if (!one4) {
                        ecFieldElement2 = ecFieldElement2.multiply(multiply7);
                    }
                    final ECFieldElement subtract8 = ecFieldElement4.subtract(ecFieldElement2);
                    if (!one3) {
                        ecFieldElement3 = ecFieldElement3.multiply(ecFieldElement9);
                    }
                    if (!one4) {
                        ecFieldElement = ecFieldElement.multiply(multiply7);
                    }
                    final ECFieldElement subtract9 = ecFieldElement3.subtract(ecFieldElement);
                    if (!subtract9.isZero()) {
                        if (!one3) {
                            if (one4) {
                                multiply7 = ecFieldElement9;
                            }
                            else {
                                multiply7 = ecFieldElement9.multiply(multiply7);
                            }
                        }
                        final ECFieldElement square5 = subtract9.square();
                        final ECFieldElement multiply8 = square5.multiply(subtract9);
                        final ECFieldElement multiply9 = square5.multiply(ecFieldElement);
                        final ECFieldElement subtract10 = subtract8.square().multiply(multiply7).subtract(multiply8).subtract(this.two(multiply9));
                        return new Fp(curve, subtract9.multiply(subtract10), multiply9.subtract(subtract10).multiplyMinusProduct(subtract8, ecFieldElement2, multiply8), new ECFieldElement[] { multiply8.multiply(multiply7) });
                    }
                    if (subtract8.isZero()) {
                        return this.twice();
                    }
                    return curve.getInfinity();
                }
            }
            else {
                final ECFieldElement subtract11 = ecFieldElement3.subtract(ecFieldElement);
                final ECFieldElement subtract12 = ecFieldElement4.subtract(ecFieldElement2);
                if (!subtract11.isZero()) {
                    final ECFieldElement divide = subtract12.divide(subtract11);
                    final ECFieldElement subtract13 = divide.square().subtract(ecFieldElement).subtract(ecFieldElement3);
                    return new Fp(curve, subtract13, divide.multiply(ecFieldElement.subtract(subtract13)).subtract(ecFieldElement2));
                }
                if (subtract12.isZero()) {
                    return this.twice();
                }
                return curve.getInfinity();
            }
        }
        
        protected ECFieldElement calculateJacobianModifiedW(ECFieldElement square, ECFieldElement negate) {
            final ECFieldElement a = this.getCurve().getA();
            if (a.isZero()) {
                return a;
            }
            if (square.isOne()) {
                return a;
            }
            ECFieldElement square2;
            if ((square2 = negate) == null) {
                square2 = square.square();
            }
            square = square2.square();
            negate = a.negate();
            if (negate.bitLength() < a.bitLength()) {
                return square.multiply(negate).negate();
            }
            return square.multiply(a);
        }
        
        @Override
        protected ECPoint detach() {
            return new Fp(null, this.getAffineXCoord(), this.getAffineYCoord());
        }
        
        protected ECFieldElement doubleProductFromSquares(final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2, final ECFieldElement ecFieldElement3, final ECFieldElement ecFieldElement4) {
            return ecFieldElement.add(ecFieldElement2).square().subtract(ecFieldElement3).subtract(ecFieldElement4);
        }
        
        protected ECFieldElement eight(final ECFieldElement ecFieldElement) {
            return this.four(this.two(ecFieldElement));
        }
        
        protected ECFieldElement four(final ECFieldElement ecFieldElement) {
            return this.two(this.two(ecFieldElement));
        }
        
        protected ECFieldElement getJacobianModifiedW() {
            ECFieldElement calculateJacobianModifiedW;
            if ((calculateJacobianModifiedW = this.zs[1]) == null) {
                final ECFieldElement[] zs = this.zs;
                calculateJacobianModifiedW = this.calculateJacobianModifiedW(this.zs[0], null);
                zs[1] = calculateJacobianModifiedW;
            }
            return calculateJacobianModifiedW;
        }
        
        @Override
        public ECFieldElement getZCoord(final int n) {
            if (n == 1 && 4 == this.getCurveCoordinateSystem()) {
                return this.getJacobianModifiedW();
            }
            return super.getZCoord(n);
        }
        
        @Override
        public ECPoint negate() {
            if (this.isInfinity()) {
                return this;
            }
            final ECCurve curve = this.getCurve();
            if (curve.getCoordinateSystem() != 0) {
                return new Fp(curve, this.x, this.y.negate(), this.zs);
            }
            return new Fp(curve, this.x, this.y.negate());
        }
        
        protected ECFieldElement three(final ECFieldElement ecFieldElement) {
            return this.two(ecFieldElement).add(ecFieldElement);
        }
        
        @Override
        public ECPoint threeTimes() {
            if (this.isInfinity()) {
                return this;
            }
            final ECFieldElement y = this.y;
            if (y.isZero()) {
                return this;
            }
            final ECCurve curve = this.getCurve();
            final int coordinateSystem = curve.getCoordinateSystem();
            if (coordinateSystem != 0) {
                if (coordinateSystem != 4) {
                    return this.twice().add(this);
                }
                return this.twiceJacobianModified(false).add(this);
            }
            else {
                final ECFieldElement x = this.x;
                final ECFieldElement two = this.two(y);
                final ECFieldElement square = two.square();
                final ECFieldElement add = this.three(x.square()).add(this.getCurve().getA());
                final ECFieldElement subtract = this.three(x).multiply(square).subtract(add.square());
                if (subtract.isZero()) {
                    return this.getCurve().getInfinity();
                }
                final ECFieldElement invert = subtract.multiply(two).invert();
                final ECFieldElement multiply = subtract.multiply(invert).multiply(add);
                final ECFieldElement subtract2 = square.square().multiply(invert).subtract(multiply);
                final ECFieldElement add2 = subtract2.subtract(multiply).multiply(multiply.add(subtract2)).add(x);
                return new Fp(curve, add2, x.subtract(add2).multiply(subtract2).subtract(y));
            }
        }
        
        @Override
        public ECPoint timesPow2(final int n) {
            if (n < 0) {
                throw new IllegalArgumentException("'e' cannot be negative");
            }
            if (n == 0) {
                return this;
            }
            if (this.isInfinity()) {
                return this;
            }
            if (n == 1) {
                return this.twice();
            }
            final ECCurve curve = this.getCurve();
            final ECFieldElement y = this.y;
            if (y.isZero()) {
                return curve.getInfinity();
            }
            final int coordinateSystem = curve.getCoordinateSystem();
            final ECFieldElement a = curve.getA();
            final ECFieldElement x = this.x;
            ECFieldElement fromBigInteger;
            if (this.zs.length < 1) {
                fromBigInteger = curve.fromBigInteger(ECConstants.ONE);
            }
            else {
                fromBigInteger = this.zs[0];
            }
            ECFieldElement multiply = y;
            ECFieldElement ecFieldElement = a;
            ECFieldElement multiply2 = x;
            Label_0229: {
                if (!fromBigInteger.isOne()) {
                    multiply = y;
                    ecFieldElement = a;
                    multiply2 = x;
                    if (coordinateSystem != 0) {
                        ECFieldElement square;
                        if (coordinateSystem != 1) {
                            if (coordinateSystem != 2) {
                                if (coordinateSystem == 4) {
                                    ecFieldElement = this.getJacobianModifiedW();
                                    multiply = y;
                                    multiply2 = x;
                                    break Label_0229;
                                }
                                throw new IllegalStateException("unsupported coordinate system");
                            }
                            else {
                                square = null;
                                multiply = y;
                                multiply2 = x;
                            }
                        }
                        else {
                            square = fromBigInteger.square();
                            multiply2 = x.multiply(fromBigInteger);
                            multiply = y.multiply(square);
                        }
                        ecFieldElement = this.calculateJacobianModifiedW(fromBigInteger, square);
                    }
                }
            }
            ECFieldElement subtract = multiply;
            int i = 0;
            ECFieldElement ecFieldElement2 = ecFieldElement;
            ECFieldElement multiply3 = fromBigInteger;
            while (i < n) {
                if (subtract.isZero()) {
                    return curve.getInfinity();
                }
                final ECFieldElement three = this.three(multiply2.square());
                final ECFieldElement two = this.two(subtract);
                final ECFieldElement multiply4 = two.multiply(subtract);
                final ECFieldElement two2 = this.two(multiply2.multiply(multiply4));
                final ECFieldElement two3 = this.two(multiply4.square());
                ECFieldElement two4 = ecFieldElement2;
                ECFieldElement add = three;
                if (!ecFieldElement2.isZero()) {
                    add = three.add(ecFieldElement2);
                    two4 = this.two(two3.multiply(ecFieldElement2));
                }
                final ECFieldElement subtract2 = add.square().subtract(this.two(two2));
                subtract = add.multiply(two2.subtract(subtract2)).subtract(two3);
                if (multiply3.isOne()) {
                    multiply3 = two;
                }
                else {
                    multiply3 = two.multiply(multiply3);
                }
                ++i;
                multiply2 = subtract2;
                ecFieldElement2 = two4;
            }
            if (coordinateSystem == 0) {
                final ECFieldElement invert = multiply3.invert();
                final ECFieldElement square2 = invert.square();
                return new Fp(curve, multiply2.multiply(square2), subtract.multiply(square2.multiply(invert)));
            }
            if (coordinateSystem == 1) {
                return new Fp(curve, multiply2.multiply(multiply3), subtract, new ECFieldElement[] { multiply3.multiply(multiply3.square()) });
            }
            if (coordinateSystem == 2) {
                return new Fp(curve, multiply2, subtract, new ECFieldElement[] { multiply3 });
            }
            if (coordinateSystem == 4) {
                return new Fp(curve, multiply2, subtract, new ECFieldElement[] { multiply3, ecFieldElement2 });
            }
            throw new IllegalStateException("unsupported coordinate system");
        }
        
        @Override
        public ECPoint twice() {
            if (this.isInfinity()) {
                return this;
            }
            final ECCurve curve = this.getCurve();
            final ECFieldElement y = this.y;
            if (y.isZero()) {
                return curve.getInfinity();
            }
            final int coordinateSystem = curve.getCoordinateSystem();
            final ECFieldElement x = this.x;
            if (coordinateSystem == 0) {
                final ECFieldElement divide = this.three(x.square()).add(this.getCurve().getA()).divide(this.two(y));
                final ECFieldElement subtract = divide.square().subtract(this.two(x));
                return new Fp(curve, subtract, divide.multiply(x.subtract(subtract)).subtract(y));
            }
            if (coordinateSystem == 1) {
                final ECFieldElement ecFieldElement = this.zs[0];
                final boolean one = ecFieldElement.isOne();
                ECFieldElement ecFieldElement3;
                final ECFieldElement ecFieldElement2 = ecFieldElement3 = curve.getA();
                if (!ecFieldElement2.isZero()) {
                    ecFieldElement3 = ecFieldElement2;
                    if (!one) {
                        ecFieldElement3 = ecFieldElement2.multiply(ecFieldElement.square());
                    }
                }
                final ECFieldElement add = ecFieldElement3.add(this.three(x.square()));
                ECFieldElement multiply;
                if (one) {
                    multiply = y;
                }
                else {
                    multiply = y.multiply(ecFieldElement);
                }
                ECFieldElement ecFieldElement4;
                if (one) {
                    ecFieldElement4 = y.square();
                }
                else {
                    ecFieldElement4 = multiply.multiply(y);
                }
                final ECFieldElement four = this.four(x.multiply(ecFieldElement4));
                final ECFieldElement subtract2 = add.square().subtract(this.two(four));
                final ECFieldElement two = this.two(multiply);
                final ECFieldElement multiply2 = subtract2.multiply(two);
                final ECFieldElement two2 = this.two(ecFieldElement4);
                final ECFieldElement subtract3 = four.subtract(subtract2).multiply(add).subtract(this.two(two2.square()));
                ECFieldElement ecFieldElement5;
                if (one) {
                    ecFieldElement5 = this.two(two2);
                }
                else {
                    ecFieldElement5 = two.square();
                }
                return new Fp(curve, multiply2, subtract3, new ECFieldElement[] { this.two(ecFieldElement5).multiply(multiply) });
            }
            if (coordinateSystem == 2) {
                final ECFieldElement ecFieldElement6 = this.zs[0];
                final boolean one2 = ecFieldElement6.isOne();
                final ECFieldElement square = y.square();
                final ECFieldElement square2 = square.square();
                ECFieldElement ecFieldElement7 = curve.getA();
                final ECFieldElement negate = ecFieldElement7.negate();
                ECFieldElement ecFieldElement8 = null;
                ECFieldElement ecFieldElement9;
                if (negate.toBigInteger().equals(BigInteger.valueOf(3L))) {
                    ECFieldElement square3;
                    if (one2) {
                        square3 = ecFieldElement6;
                    }
                    else {
                        square3 = ecFieldElement6.square();
                    }
                    ecFieldElement8 = this.three(x.add(square3).multiply(x.subtract(square3)));
                    ecFieldElement9 = square.multiply(x);
                }
                else {
                    final ECFieldElement three = this.three(x.square());
                    Label_0270: {
                        if (!one2) {
                            if (ecFieldElement7.isZero()) {
                                ecFieldElement8 = three;
                                break Label_0270;
                            }
                            final ECFieldElement square4 = ecFieldElement6.square().square();
                            if (negate.bitLength() < ecFieldElement7.bitLength()) {
                                ecFieldElement8 = three.subtract(square4.multiply(negate));
                                break Label_0270;
                            }
                            ecFieldElement7 = square4.multiply(ecFieldElement7);
                        }
                        ecFieldElement8 = three.add(ecFieldElement7);
                    }
                    ecFieldElement9 = x.multiply(square);
                }
                final ECFieldElement four2 = this.four(ecFieldElement9);
                final ECFieldElement subtract4 = ecFieldElement8.square().subtract(this.two(four2));
                final ECFieldElement subtract5 = four2.subtract(subtract4).multiply(ecFieldElement8).subtract(this.eight(square2));
                ECFieldElement ecFieldElement10 = this.two(y);
                if (!one2) {
                    ecFieldElement10 = ecFieldElement10.multiply(ecFieldElement6);
                }
                return new Fp(curve, subtract4, subtract5, new ECFieldElement[] { ecFieldElement10 });
            }
            if (coordinateSystem == 4) {
                return this.twiceJacobianModified(true);
            }
            throw new IllegalStateException("unsupported coordinate system");
        }
        
        protected Fp twiceJacobianModified(final boolean b) {
            final ECFieldElement x = this.x;
            final ECFieldElement y = this.y;
            final ECFieldElement ecFieldElement = this.zs[0];
            final ECFieldElement jacobianModifiedW = this.getJacobianModifiedW();
            final ECFieldElement add = this.three(x.square()).add(jacobianModifiedW);
            ECFieldElement ecFieldElement2 = this.two(y);
            final ECFieldElement multiply = ecFieldElement2.multiply(y);
            final ECFieldElement two = this.two(x.multiply(multiply));
            final ECFieldElement subtract = add.square().subtract(this.two(two));
            final ECFieldElement two2 = this.two(multiply.square());
            final ECFieldElement subtract2 = add.multiply(two.subtract(subtract)).subtract(two2);
            ECFieldElement two3;
            if (b) {
                two3 = this.two(two2.multiply(jacobianModifiedW));
            }
            else {
                two3 = null;
            }
            if (!ecFieldElement.isOne()) {
                ecFieldElement2 = ecFieldElement2.multiply(ecFieldElement);
            }
            return new Fp(this.getCurve(), subtract, subtract2, new ECFieldElement[] { ecFieldElement2, two3 });
        }
        
        @Override
        public ECPoint twicePlus(final ECPoint ecPoint) {
            if (this == ecPoint) {
                return this.threeTimes();
            }
            if (this.isInfinity()) {
                return ecPoint;
            }
            if (ecPoint.isInfinity()) {
                return this.twice();
            }
            final ECFieldElement y = this.y;
            if (y.isZero()) {
                return ecPoint;
            }
            final ECCurve curve = this.getCurve();
            final int coordinateSystem = curve.getCoordinateSystem();
            if (coordinateSystem != 0) {
                if (coordinateSystem != 4) {
                    return this.twice().add(ecPoint);
                }
                return this.twiceJacobianModified(false).add(ecPoint);
            }
            else {
                final ECFieldElement x = this.x;
                final ECFieldElement x2 = ecPoint.x;
                final ECFieldElement y2 = ecPoint.y;
                final ECFieldElement subtract = x2.subtract(x);
                final ECFieldElement subtract2 = y2.subtract(y);
                if (subtract.isZero()) {
                    if (subtract2.isZero()) {
                        return this.threeTimes();
                    }
                    return this;
                }
                else {
                    final ECFieldElement square = subtract.square();
                    final ECFieldElement subtract3 = square.multiply(this.two(x).add(x2)).subtract(subtract2.square());
                    if (subtract3.isZero()) {
                        return curve.getInfinity();
                    }
                    final ECFieldElement invert = subtract3.multiply(subtract).invert();
                    final ECFieldElement multiply = subtract3.multiply(invert).multiply(subtract2);
                    final ECFieldElement subtract4 = this.two(y).multiply(square).multiply(subtract).multiply(invert).subtract(multiply);
                    final ECFieldElement add = subtract4.subtract(multiply).multiply(multiply.add(subtract4)).add(x2);
                    return new Fp(curve, add, x.subtract(add).multiply(subtract4).subtract(y));
                }
            }
        }
        
        protected ECFieldElement two(final ECFieldElement ecFieldElement) {
            return ecFieldElement.add(ecFieldElement);
        }
    }
}
