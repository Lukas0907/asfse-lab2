// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec;

public interface PreCompCallback
{
    PreCompInfo precompute(final PreCompInfo p0);
}
