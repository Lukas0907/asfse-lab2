// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec;

public class FixedPointPreCompInfo implements PreCompInfo
{
    protected ECLookupTable lookupTable;
    protected ECPoint offset;
    protected int width;
    
    public FixedPointPreCompInfo() {
        this.offset = null;
        this.lookupTable = null;
        this.width = -1;
    }
    
    public ECLookupTable getLookupTable() {
        return this.lookupTable;
    }
    
    public ECPoint getOffset() {
        return this.offset;
    }
    
    public int getWidth() {
        return this.width;
    }
    
    public void setLookupTable(final ECLookupTable lookupTable) {
        this.lookupTable = lookupTable;
    }
    
    public void setOffset(final ECPoint offset) {
        this.offset = offset;
    }
    
    public void setWidth(final int width) {
        this.width = width;
    }
}
