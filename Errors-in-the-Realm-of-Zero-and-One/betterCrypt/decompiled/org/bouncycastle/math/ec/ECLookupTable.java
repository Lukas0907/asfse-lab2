// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec;

public interface ECLookupTable
{
    int getSize();
    
    ECPoint lookup(final int p0);
    
    ECPoint lookupVar(final int p0);
}
