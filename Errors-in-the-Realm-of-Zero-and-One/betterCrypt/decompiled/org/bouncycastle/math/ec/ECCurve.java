// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec;

import org.bouncycastle.math.raw.Nat;
import java.util.Random;
import org.bouncycastle.math.field.FiniteFields;
import org.bouncycastle.util.Integers;
import java.util.Hashtable;
import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.math.ec.endo.GLVEndomorphism;
import org.bouncycastle.math.field.FiniteField;
import org.bouncycastle.math.ec.endo.ECEndomorphism;
import java.math.BigInteger;

public abstract class ECCurve
{
    public static final int COORD_AFFINE = 0;
    public static final int COORD_HOMOGENEOUS = 1;
    public static final int COORD_JACOBIAN = 2;
    public static final int COORD_JACOBIAN_CHUDNOVSKY = 3;
    public static final int COORD_JACOBIAN_MODIFIED = 4;
    public static final int COORD_LAMBDA_AFFINE = 5;
    public static final int COORD_LAMBDA_PROJECTIVE = 6;
    public static final int COORD_SKEWED = 7;
    protected ECFieldElement a;
    protected ECFieldElement b;
    protected BigInteger cofactor;
    protected int coord;
    protected ECEndomorphism endomorphism;
    protected FiniteField field;
    protected ECMultiplier multiplier;
    protected BigInteger order;
    
    protected ECCurve(final FiniteField field) {
        this.coord = 0;
        this.endomorphism = null;
        this.multiplier = null;
        this.field = field;
    }
    
    public static int[] getAllCoordinateSystems() {
        return new int[] { 0, 1, 2, 3, 4, 5, 6, 7 };
    }
    
    protected void checkPoint(final ECPoint ecPoint) {
        if (ecPoint != null && this == ecPoint.getCurve()) {
            return;
        }
        throw new IllegalArgumentException("'point' must be non-null and on this curve");
    }
    
    protected void checkPoints(final ECPoint[] array) {
        this.checkPoints(array, 0, array.length);
    }
    
    protected void checkPoints(final ECPoint[] array, final int n, final int n2) {
        if (array == null) {
            throw new IllegalArgumentException("'points' cannot be null");
        }
        if (n >= 0 && n2 >= 0 && n <= array.length - n2) {
            for (int i = 0; i < n2; ++i) {
                final ECPoint ecPoint = array[n + i];
                if (ecPoint != null && this != ecPoint.getCurve()) {
                    throw new IllegalArgumentException("'points' entries must be null or on this curve");
                }
            }
            return;
        }
        throw new IllegalArgumentException("invalid range specified for 'points'");
    }
    
    protected abstract ECCurve cloneCurve();
    
    public Config configure() {
        synchronized (this) {
            return new Config(this.coord, this.endomorphism, this.multiplier);
        }
    }
    
    public ECLookupTable createCacheSafeLookupTable(final ECPoint[] array, final int n, final int n2) {
        final int n3 = this.getFieldSize() + 7 >>> 3;
        final byte[] array2 = new byte[n2 * n3 * 2];
        int n4;
        for (int i = n4 = 0; i < n2; ++i) {
            final ECPoint ecPoint = array[n + i];
            final byte[] byteArray = ecPoint.getRawXCoord().toBigInteger().toByteArray();
            final byte[] byteArray2 = ecPoint.getRawYCoord().toBigInteger().toByteArray();
            final int length = byteArray.length;
            int n5 = 1;
            int n6;
            if (length > n3) {
                n6 = 1;
            }
            else {
                n6 = 0;
            }
            final int n7 = byteArray.length - n6;
            if (byteArray2.length <= n3) {
                n5 = 0;
            }
            final int n8 = byteArray2.length - n5;
            final int n9 = n4 + n3;
            System.arraycopy(byteArray, n6, array2, n9 - n7, n7);
            n4 = n9 + n3;
            System.arraycopy(byteArray2, n5, array2, n4 - n8, n8);
        }
        return new AbstractECLookupTable() {
            private ECPoint createPoint(final byte[] magnitude, final byte[] magnitude2) {
                final ECCurve this$0 = ECCurve.this;
                return this$0.createRawPoint(this$0.fromBigInteger(new BigInteger(1, magnitude)), ECCurve.this.fromBigInteger(new BigInteger(1, magnitude2)));
            }
            
            @Override
            public int getSize() {
                return n2;
            }
            
            @Override
            public ECPoint lookup(final int n) {
                final int val$FE_BYTES = n3;
                final byte[] array = new byte[val$FE_BYTES];
                final byte[] array2 = new byte[val$FE_BYTES];
                int n2;
                for (int i = n2 = 0; i < n2; ++i) {
                    final int n3 = (i ^ n) - 1 >> 31;
                    int n4 = 0;
                    int val$FE_BYTES2;
                    while (true) {
                        val$FE_BYTES2 = n3;
                        if (n4 >= val$FE_BYTES2) {
                            break;
                        }
                        final byte b = array[n4];
                        final byte[] val$table = array2;
                        array[n4] = (byte)(b ^ (val$table[n2 + n4] & n3));
                        array2[n4] ^= (byte)(val$table[val$FE_BYTES2 + n2 + n4] & n3);
                        ++n4;
                    }
                    n2 += val$FE_BYTES2 * 2;
                }
                return this.createPoint(array, array2);
            }
            
            @Override
            public ECPoint lookupVar(int n) {
                final int val$FE_BYTES = n3;
                final byte[] array = new byte[val$FE_BYTES];
                final byte[] array2 = new byte[val$FE_BYTES];
                final int n2 = n * val$FE_BYTES * 2;
                n = 0;
                while (true) {
                    final int val$FE_BYTES2 = n3;
                    if (n >= val$FE_BYTES2) {
                        break;
                    }
                    final byte[] val$table = array2;
                    array[n] = val$table[n2 + n];
                    array2[n] = val$table[val$FE_BYTES2 + n2 + n];
                    ++n;
                }
                return this.createPoint(array, array2);
            }
        };
    }
    
    protected ECMultiplier createDefaultMultiplier() {
        final ECEndomorphism endomorphism = this.endomorphism;
        if (endomorphism instanceof GLVEndomorphism) {
            return new GLVMultiplier(this, (GLVEndomorphism)endomorphism);
        }
        return new WNafL2RMultiplier();
    }
    
    public ECPoint createPoint(final BigInteger bigInteger, final BigInteger bigInteger2) {
        return this.createRawPoint(this.fromBigInteger(bigInteger), this.fromBigInteger(bigInteger2));
    }
    
    protected abstract ECPoint createRawPoint(final ECFieldElement p0, final ECFieldElement p1);
    
    protected abstract ECPoint createRawPoint(final ECFieldElement p0, final ECFieldElement p1, final ECFieldElement[] p2);
    
    public ECPoint decodePoint(final byte[] array) {
        final int n = (this.getFieldSize() + 7) / 8;
        int n2 = false ? 1 : 0;
        final byte i = array[0];
        ECPoint ecPoint;
        if (i != 0) {
            if (i != 2 && i != 3) {
                if (i != 4) {
                    if (i != 6 && i != 7) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Invalid point encoding 0x");
                        sb.append(Integer.toString(i, 16));
                        throw new IllegalArgumentException(sb.toString());
                    }
                    if (array.length != n * 2 + 1) {
                        throw new IllegalArgumentException("Incorrect length for hybrid encoding");
                    }
                    final BigInteger fromUnsignedByteArray = BigIntegers.fromUnsignedByteArray(array, 1, n);
                    final BigInteger fromUnsignedByteArray2 = BigIntegers.fromUnsignedByteArray(array, n + 1, n);
                    final boolean testBit = fromUnsignedByteArray2.testBit(0);
                    if (i == 7) {
                        n2 = (true ? 1 : 0);
                    }
                    if ((testBit ? 1 : 0) != n2) {
                        throw new IllegalArgumentException("Inconsistent Y coordinate in hybrid encoding");
                    }
                    ecPoint = this.validatePoint(fromUnsignedByteArray, fromUnsignedByteArray2);
                }
                else {
                    if (array.length != n * 2 + 1) {
                        throw new IllegalArgumentException("Incorrect length for uncompressed encoding");
                    }
                    ecPoint = this.validatePoint(BigIntegers.fromUnsignedByteArray(array, 1, n), BigIntegers.fromUnsignedByteArray(array, n + 1, n));
                }
            }
            else {
                if (array.length != n + 1) {
                    throw new IllegalArgumentException("Incorrect length for compressed encoding");
                }
                ecPoint = this.decompressPoint(i & 0x1, BigIntegers.fromUnsignedByteArray(array, 1, n));
                if (!ecPoint.implIsValid(true, true)) {
                    throw new IllegalArgumentException("Invalid point");
                }
            }
        }
        else {
            if (array.length != 1) {
                throw new IllegalArgumentException("Incorrect length for infinity encoding");
            }
            ecPoint = this.getInfinity();
        }
        if (i == 0) {
            return ecPoint;
        }
        if (!ecPoint.isInfinity()) {
            return ecPoint;
        }
        throw new IllegalArgumentException("Invalid infinity encoding");
    }
    
    protected abstract ECPoint decompressPoint(final int p0, final BigInteger p1);
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof ECCurve && this.equals((ECCurve)o));
    }
    
    public boolean equals(final ECCurve ecCurve) {
        return this == ecCurve || (ecCurve != null && this.getField().equals(ecCurve.getField()) && this.getA().toBigInteger().equals(ecCurve.getA().toBigInteger()) && this.getB().toBigInteger().equals(ecCurve.getB().toBigInteger()));
    }
    
    public abstract ECFieldElement fromBigInteger(final BigInteger p0);
    
    public ECFieldElement getA() {
        return this.a;
    }
    
    public ECFieldElement getB() {
        return this.b;
    }
    
    public BigInteger getCofactor() {
        return this.cofactor;
    }
    
    public int getCoordinateSystem() {
        return this.coord;
    }
    
    public ECEndomorphism getEndomorphism() {
        return this.endomorphism;
    }
    
    public FiniteField getField() {
        return this.field;
    }
    
    public abstract int getFieldSize();
    
    public abstract ECPoint getInfinity();
    
    public ECMultiplier getMultiplier() {
        synchronized (this) {
            if (this.multiplier == null) {
                this.multiplier = this.createDefaultMultiplier();
            }
            return this.multiplier;
        }
    }
    
    public BigInteger getOrder() {
        return this.order;
    }
    
    public PreCompInfo getPreCompInfo(final ECPoint ecPoint, final String key) {
        this.checkPoint(ecPoint);
        synchronized (ecPoint) {
            final Hashtable preCompTable = ecPoint.preCompTable;
            // monitorexit(ecPoint)
            if (preCompTable == null) {
                return null;
            }
            synchronized (preCompTable) {
                return preCompTable.get(key);
            }
        }
    }
    
    @Override
    public int hashCode() {
        return this.getField().hashCode() ^ Integers.rotateLeft(this.getA().toBigInteger().hashCode(), 8) ^ Integers.rotateLeft(this.getB().toBigInteger().hashCode(), 16);
    }
    
    public ECPoint importPoint(ECPoint normalize) {
        if (this == normalize.getCurve()) {
            return normalize;
        }
        if (normalize.isInfinity()) {
            return this.getInfinity();
        }
        normalize = normalize.normalize();
        return this.createPoint(normalize.getXCoord().toBigInteger(), normalize.getYCoord().toBigInteger());
    }
    
    public abstract boolean isValidFieldElement(final BigInteger p0);
    
    public void normalizeAll(final ECPoint[] array) {
        this.normalizeAll(array, 0, array.length, null);
    }
    
    public void normalizeAll(final ECPoint[] array, int i, int n, final ECFieldElement ecFieldElement) {
        this.checkPoints(array, i, n);
        final int coordinateSystem = this.getCoordinateSystem();
        if (coordinateSystem != 0 && coordinateSystem != 5) {
            final ECFieldElement[] array2 = new ECFieldElement[n];
            final int[] array3 = new int[n];
            final int n2 = 0;
            int n3;
            int n5;
            for (int j = n3 = 0; j < n; ++j, n3 = n5) {
                final int n4 = i + j;
                final ECPoint ecPoint = array[n4];
                n5 = n3;
                if (ecPoint != null) {
                    if (ecFieldElement == null) {
                        n5 = n3;
                        if (ecPoint.isNormalized()) {
                            continue;
                        }
                    }
                    array2[n3] = ecPoint.getZCoord(0);
                    array3[n3] = n4;
                    n5 = n3 + 1;
                }
            }
            if (n3 == 0) {
                return;
            }
            ECAlgorithms.montgomeryTrick(array2, 0, n3, ecFieldElement);
            for (i = n2; i < n3; ++i) {
                n = array3[i];
                array[n] = array[n].normalize(array2[i]);
            }
        }
        else {
            if (ecFieldElement == null) {
                return;
            }
            throw new IllegalArgumentException("'iso' not valid for affine coordinates");
        }
    }
    
    public PreCompInfo precompute(final ECPoint ecPoint, final String s, final PreCompCallback preCompCallback) {
        this.checkPoint(ecPoint);
        synchronized (ecPoint) {
            Hashtable<Object, PreCompInfo> preCompTable;
            if ((preCompTable = (Hashtable<Object, PreCompInfo>)ecPoint.preCompTable) == null) {
                preCompTable = new Hashtable<Object, PreCompInfo>(4);
                ecPoint.preCompTable = preCompTable;
            }
            // monitorexit(ecPoint)
            synchronized (preCompTable) {
                final PreCompInfo preCompInfo = preCompTable.get(s);
                final PreCompInfo precompute = preCompCallback.precompute(preCompInfo);
                if (precompute != preCompInfo) {
                    preCompTable.put(s, precompute);
                }
                return precompute;
            }
        }
    }
    
    public boolean supportsCoordinateSystem(final int n) {
        return n == 0;
    }
    
    public ECPoint validatePoint(final BigInteger bigInteger, final BigInteger bigInteger2) {
        final ECPoint point = this.createPoint(bigInteger, bigInteger2);
        if (point.isValid()) {
            return point;
        }
        throw new IllegalArgumentException("Invalid point coordinates");
    }
    
    public abstract static class AbstractF2m extends ECCurve
    {
        private BigInteger[] si;
        
        protected AbstractF2m(final int n, final int n2, final int n3, final int n4) {
            super(buildField(n, n2, n3, n4));
            this.si = null;
        }
        
        private static FiniteField buildField(final int n, final int n2, final int n3, final int n4) {
            if (n2 == 0) {
                throw new IllegalArgumentException("k1 must be > 0");
            }
            if (n3 == 0) {
                if (n4 == 0) {
                    return FiniteFields.getBinaryExtensionField(new int[] { 0, n2, n });
                }
                throw new IllegalArgumentException("k3 must be 0 if k2 == 0");
            }
            else {
                if (n3 <= n2) {
                    throw new IllegalArgumentException("k2 must be > k1");
                }
                if (n4 > n3) {
                    return FiniteFields.getBinaryExtensionField(new int[] { 0, n2, n3, n4, n });
                }
                throw new IllegalArgumentException("k3 must be > k2");
            }
        }
        
        public static BigInteger inverse(final int n, final int[] array, final BigInteger bigInteger) {
            return new LongArray(bigInteger).modInverse(n, array).toBigInteger();
        }
        
        @Override
        public ECPoint createPoint(final BigInteger bigInteger, final BigInteger bigInteger2) {
            final ECFieldElement fromBigInteger = this.fromBigInteger(bigInteger);
            ECFieldElement ecFieldElement = this.fromBigInteger(bigInteger2);
            final int coordinateSystem = this.getCoordinateSystem();
            if (coordinateSystem == 5 || coordinateSystem == 6) {
                if (fromBigInteger.isZero()) {
                    if (!ecFieldElement.square().equals(this.getB())) {
                        throw new IllegalArgumentException();
                    }
                }
                else {
                    ecFieldElement = ecFieldElement.divide(fromBigInteger).add(fromBigInteger);
                }
            }
            return this.createRawPoint(fromBigInteger, ecFieldElement);
        }
        
        @Override
        protected ECPoint decompressPoint(int coordinateSystem, final BigInteger bigInteger) {
            final ECFieldElement fromBigInteger = this.fromBigInteger(bigInteger);
            ECFieldElement ecFieldElement;
            if (fromBigInteger.isZero()) {
                ecFieldElement = this.getB().sqrt();
            }
            else {
                final ECFieldElement solveQuadraticEquation = this.solveQuadraticEquation(fromBigInteger.square().invert().multiply(this.getB()).add(this.getA()).add(fromBigInteger));
                if (solveQuadraticEquation != null) {
                    final boolean testBitZero = solveQuadraticEquation.testBitZero();
                    boolean b = true;
                    if (coordinateSystem != 1) {
                        b = false;
                    }
                    ECFieldElement addOne = solveQuadraticEquation;
                    if (testBitZero != b) {
                        addOne = solveQuadraticEquation.addOne();
                    }
                    coordinateSystem = this.getCoordinateSystem();
                    if (coordinateSystem != 5 && coordinateSystem != 6) {
                        ecFieldElement = addOne.multiply(fromBigInteger);
                    }
                    else {
                        ecFieldElement = addOne.add(fromBigInteger);
                    }
                }
                else {
                    ecFieldElement = null;
                }
            }
            if (ecFieldElement != null) {
                return this.createRawPoint(fromBigInteger, ecFieldElement);
            }
            throw new IllegalArgumentException("Invalid point compression");
        }
        
        BigInteger[] getSi() {
            synchronized (this) {
                if (this.si == null) {
                    this.si = Tnaf.getSi(this);
                }
                return this.si;
            }
        }
        
        public boolean isKoblitz() {
            return this.order != null && this.cofactor != null && this.b.isOne() && (this.a.isZero() || this.a.isOne());
        }
        
        @Override
        public boolean isValidFieldElement(final BigInteger bigInteger) {
            return bigInteger != null && bigInteger.signum() >= 0 && bigInteger.bitLength() <= this.getFieldSize();
        }
        
        protected ECFieldElement solveQuadraticEquation(final ECFieldElement ecFieldElement) {
            final ECFieldElement.AbstractF2m abstractF2m = (ECFieldElement.AbstractF2m)ecFieldElement;
            final boolean hasFastTrace = abstractF2m.hasFastTrace();
            if (hasFastTrace && abstractF2m.trace() != 0) {
                return null;
            }
            final int fieldSize = this.getFieldSize();
            if ((fieldSize & 0x1) != 0x0) {
                final ECFieldElement halfTrace = abstractF2m.halfTrace();
                if (hasFastTrace) {
                    return halfTrace;
                }
                if (halfTrace.square().add(halfTrace).add(ecFieldElement).isZero()) {
                    return halfTrace;
                }
                return null;
            }
            else {
                if (ecFieldElement.isZero()) {
                    return ecFieldElement;
                }
                final ECFieldElement fromBigInteger = this.fromBigInteger(ECConstants.ZERO);
                final Random rnd = new Random();
                ECFieldElement add;
                do {
                    final ECFieldElement fromBigInteger2 = this.fromBigInteger(new BigInteger(fieldSize, rnd));
                    int i = 1;
                    ECFieldElement add2 = ecFieldElement;
                    add = fromBigInteger;
                    while (i < fieldSize) {
                        final ECFieldElement square = add2.square();
                        add = add.square().add(square.multiply(fromBigInteger2));
                        add2 = square.add(ecFieldElement);
                        ++i;
                    }
                    if (!add2.isZero()) {
                        return null;
                    }
                } while (add.square().add(add).isZero());
                return add;
            }
        }
    }
    
    public abstract static class AbstractFp extends ECCurve
    {
        protected AbstractFp(final BigInteger bigInteger) {
            super(FiniteFields.getPrimeField(bigInteger));
        }
        
        @Override
        protected ECPoint decompressPoint(final int n, final BigInteger bigInteger) {
            final ECFieldElement fromBigInteger = this.fromBigInteger(bigInteger);
            final ECFieldElement sqrt = fromBigInteger.square().add(this.a).multiply(fromBigInteger).add(this.b).sqrt();
            if (sqrt != null) {
                final boolean testBitZero = sqrt.testBitZero();
                boolean b = true;
                if (n != 1) {
                    b = false;
                }
                ECFieldElement negate = sqrt;
                if (testBitZero != b) {
                    negate = sqrt.negate();
                }
                return this.createRawPoint(fromBigInteger, negate);
            }
            throw new IllegalArgumentException("Invalid point compression");
        }
        
        @Override
        public boolean isValidFieldElement(final BigInteger bigInteger) {
            return bigInteger != null && bigInteger.signum() >= 0 && bigInteger.compareTo(this.getField().getCharacteristic()) < 0;
        }
    }
    
    public class Config
    {
        protected int coord;
        protected ECEndomorphism endomorphism;
        protected ECMultiplier multiplier;
        
        Config(final int coord, final ECEndomorphism endomorphism, final ECMultiplier multiplier) {
            this.coord = coord;
            this.endomorphism = endomorphism;
            this.multiplier = multiplier;
        }
        
        public ECCurve create() {
            if (ECCurve.this.supportsCoordinateSystem(this.coord)) {
                final ECCurve cloneCurve = ECCurve.this.cloneCurve();
                if (cloneCurve != ECCurve.this) {
                    synchronized (cloneCurve) {
                        cloneCurve.coord = this.coord;
                        cloneCurve.endomorphism = this.endomorphism;
                        cloneCurve.multiplier = this.multiplier;
                        return cloneCurve;
                    }
                }
                throw new IllegalStateException("implementation returned current curve");
            }
            throw new IllegalStateException("unsupported coordinate system");
        }
        
        public Config setCoordinateSystem(final int coord) {
            this.coord = coord;
            return this;
        }
        
        public Config setEndomorphism(final ECEndomorphism endomorphism) {
            this.endomorphism = endomorphism;
            return this;
        }
        
        public Config setMultiplier(final ECMultiplier multiplier) {
            this.multiplier = multiplier;
            return this;
        }
    }
    
    public static class F2m extends AbstractF2m
    {
        private static final int F2M_DEFAULT_COORDS = 6;
        private ECPoint.F2m infinity;
        private int k1;
        private int k2;
        private int k3;
        private int m;
        
        public F2m(final int n, final int n2, final int n3, final int n4, final BigInteger bigInteger, final BigInteger bigInteger2) {
            this(n, n2, n3, n4, bigInteger, bigInteger2, null, null);
        }
        
        public F2m(final int m, final int k1, final int k2, final int k3, final BigInteger bigInteger, final BigInteger bigInteger2, final BigInteger order, final BigInteger cofactor) {
            super(m, k1, k2, k3);
            this.m = m;
            this.k1 = k1;
            this.k2 = k2;
            this.k3 = k3;
            this.order = order;
            this.cofactor = cofactor;
            this.infinity = new ECPoint.F2m(this, null, null);
            this.a = this.fromBigInteger(bigInteger);
            this.b = this.fromBigInteger(bigInteger2);
            this.coord = 6;
        }
        
        protected F2m(final int m, final int k1, final int k2, final int k3, final ECFieldElement a, final ECFieldElement b, final BigInteger order, final BigInteger cofactor) {
            super(m, k1, k2, k3);
            this.m = m;
            this.k1 = k1;
            this.k2 = k2;
            this.k3 = k3;
            this.order = order;
            this.cofactor = cofactor;
            this.infinity = new ECPoint.F2m(this, null, null);
            this.a = a;
            this.b = b;
            this.coord = 6;
        }
        
        public F2m(final int n, final int n2, final BigInteger bigInteger, final BigInteger bigInteger2) {
            this(n, n2, 0, 0, bigInteger, bigInteger2, null, null);
        }
        
        public F2m(final int n, final int n2, final BigInteger bigInteger, final BigInteger bigInteger2, final BigInteger bigInteger3, final BigInteger bigInteger4) {
            this(n, n2, 0, 0, bigInteger, bigInteger2, bigInteger3, bigInteger4);
        }
        
        @Override
        protected ECCurve cloneCurve() {
            return new F2m(this.m, this.k1, this.k2, this.k3, this.a, this.b, this.order, this.cofactor);
        }
        
        @Override
        public ECLookupTable createCacheSafeLookupTable(final ECPoint[] array, final int n, final int n2) {
            final int n3 = this.m + 63 >>> 6;
            final boolean trinomial = this.isTrinomial();
            int i = 0;
            int[] array2;
            if (trinomial) {
                array2 = new int[] { this.k1 };
            }
            else {
                array2 = new int[] { this.k1, this.k2, this.k3 };
            }
            final long[] array3 = new long[n2 * n3 * 2];
            int n4 = 0;
            while (i < n2) {
                final ECPoint ecPoint = array[n + i];
                ((ECFieldElement.F2m)ecPoint.getRawXCoord()).x.copyTo(array3, n4);
                final int n5 = n4 + n3;
                ((ECFieldElement.F2m)ecPoint.getRawYCoord()).x.copyTo(array3, n5);
                n4 = n5 + n3;
                ++i;
            }
            return new AbstractECLookupTable() {
                private ECPoint createPoint(final long[] array, final long[] array2) {
                    return F2m.this.createRawPoint(new ECFieldElement.F2m(F2m.this.m, array2, new LongArray(array)), new ECFieldElement.F2m(F2m.this.m, array2, new LongArray(array2)));
                }
                
                @Override
                public int getSize() {
                    return n2;
                }
                
                @Override
                public ECPoint lookup(final int n) {
                    final long[] create64 = Nat.create64(n3);
                    final long[] create65 = Nat.create64(n3);
                    int n2;
                    for (int i = n2 = 0; i < n2; ++i) {
                        final long n3 = (i ^ n) - 1 >> 31;
                        int n4 = 0;
                        int val$FE_LONGS;
                        while (true) {
                            val$FE_LONGS = n3;
                            if (n4 >= val$FE_LONGS) {
                                break;
                            }
                            final long n5 = create64[n4];
                            final long[] val$table = array3;
                            create64[n4] = (n5 ^ (val$table[n2 + n4] & n3));
                            create65[n4] ^= (val$table[val$FE_LONGS + n2 + n4] & n3);
                            ++n4;
                        }
                        n2 += val$FE_LONGS * 2;
                    }
                    return this.createPoint(create64, create65);
                }
                
                @Override
                public ECPoint lookupVar(int n) {
                    final long[] create64 = Nat.create64(n3);
                    final long[] create65 = Nat.create64(n3);
                    final int n2 = n * n3 * 2;
                    n = 0;
                    while (true) {
                        final int val$FE_LONGS = n3;
                        if (n >= val$FE_LONGS) {
                            break;
                        }
                        final long[] val$table = array3;
                        create64[n] = val$table[n2 + n];
                        create65[n] = val$table[val$FE_LONGS + n2 + n];
                        ++n;
                    }
                    return this.createPoint(create64, create65);
                }
            };
        }
        
        @Override
        protected ECMultiplier createDefaultMultiplier() {
            if (((AbstractF2m)this).isKoblitz()) {
                return new WTauNafMultiplier();
            }
            return super.createDefaultMultiplier();
        }
        
        @Override
        protected ECPoint createRawPoint(final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2) {
            return new ECPoint.F2m(this, ecFieldElement, ecFieldElement2);
        }
        
        @Override
        protected ECPoint createRawPoint(final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2, final ECFieldElement[] array) {
            return new ECPoint.F2m(this, ecFieldElement, ecFieldElement2, array);
        }
        
        @Override
        public ECFieldElement fromBigInteger(final BigInteger bigInteger) {
            return new ECFieldElement.F2m(this.m, this.k1, this.k2, this.k3, bigInteger);
        }
        
        @Override
        public int getFieldSize() {
            return this.m;
        }
        
        @Override
        public ECPoint getInfinity() {
            return this.infinity;
        }
        
        public int getK1() {
            return this.k1;
        }
        
        public int getK2() {
            return this.k2;
        }
        
        public int getK3() {
            return this.k3;
        }
        
        public int getM() {
            return this.m;
        }
        
        public boolean isTrinomial() {
            return this.k2 == 0 && this.k3 == 0;
        }
        
        @Override
        public boolean supportsCoordinateSystem(final int n) {
            return n == 0 || n == 1 || n == 6;
        }
    }
    
    public static class Fp extends AbstractFp
    {
        private static final int FP_DEFAULT_COORDS = 4;
        ECPoint.Fp infinity;
        BigInteger q;
        BigInteger r;
        
        public Fp(final BigInteger bigInteger, final BigInteger bigInteger2, final BigInteger bigInteger3) {
            this(bigInteger, bigInteger2, bigInteger3, null, null);
        }
        
        public Fp(final BigInteger q, final BigInteger bigInteger, final BigInteger bigInteger2, final BigInteger order, final BigInteger cofactor) {
            super(q);
            this.q = q;
            this.r = ECFieldElement.Fp.calculateResidue(q);
            this.infinity = new ECPoint.Fp(this, null, null);
            this.a = this.fromBigInteger(bigInteger);
            this.b = this.fromBigInteger(bigInteger2);
            this.order = order;
            this.cofactor = cofactor;
            this.coord = 4;
        }
        
        protected Fp(final BigInteger bigInteger, final BigInteger bigInteger2, final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2) {
            this(bigInteger, bigInteger2, ecFieldElement, ecFieldElement2, null, null);
        }
        
        protected Fp(final BigInteger q, final BigInteger r, final ECFieldElement a, final ECFieldElement b, final BigInteger order, final BigInteger cofactor) {
            super(q);
            this.q = q;
            this.r = r;
            this.infinity = new ECPoint.Fp(this, null, null);
            this.a = a;
            this.b = b;
            this.order = order;
            this.cofactor = cofactor;
            this.coord = 4;
        }
        
        @Override
        protected ECCurve cloneCurve() {
            return new Fp(this.q, this.r, this.a, this.b, this.order, this.cofactor);
        }
        
        @Override
        protected ECPoint createRawPoint(final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2) {
            return new ECPoint.Fp(this, ecFieldElement, ecFieldElement2);
        }
        
        @Override
        protected ECPoint createRawPoint(final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2, final ECFieldElement[] array) {
            return new ECPoint.Fp(this, ecFieldElement, ecFieldElement2, array);
        }
        
        @Override
        public ECFieldElement fromBigInteger(final BigInteger bigInteger) {
            return new ECFieldElement.Fp(this.q, this.r, bigInteger);
        }
        
        @Override
        public int getFieldSize() {
            return this.q.bitLength();
        }
        
        @Override
        public ECPoint getInfinity() {
            return this.infinity;
        }
        
        public BigInteger getQ() {
            return this.q;
        }
        
        @Override
        public ECPoint importPoint(final ECPoint ecPoint) {
            if (this != ecPoint.getCurve() && this.getCoordinateSystem() == 2 && !ecPoint.isInfinity()) {
                final int coordinateSystem = ecPoint.getCurve().getCoordinateSystem();
                if (coordinateSystem == 2 || coordinateSystem == 3 || coordinateSystem == 4) {
                    return new ECPoint.Fp(this, this.fromBigInteger(ecPoint.x.toBigInteger()), this.fromBigInteger(ecPoint.y.toBigInteger()), new ECFieldElement[] { this.fromBigInteger(ecPoint.zs[0].toBigInteger()) });
                }
            }
            return super.importPoint(ecPoint);
        }
        
        @Override
        public boolean supportsCoordinateSystem(final int n) {
            return n == 0 || n == 1 || n == 2 || n == 4;
        }
    }
}
