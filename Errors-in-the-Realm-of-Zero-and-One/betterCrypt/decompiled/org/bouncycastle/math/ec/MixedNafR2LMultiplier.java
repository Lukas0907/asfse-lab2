// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec;

import java.math.BigInteger;

public class MixedNafR2LMultiplier extends AbstractECMultiplier
{
    protected int additionCoord;
    protected int doublingCoord;
    
    public MixedNafR2LMultiplier() {
        this(2, 4);
    }
    
    public MixedNafR2LMultiplier(final int additionCoord, final int doublingCoord) {
        this.additionCoord = additionCoord;
        this.doublingCoord = doublingCoord;
    }
    
    protected ECCurve configureCurve(final ECCurve ecCurve, final int n) {
        if (ecCurve.getCoordinateSystem() == n) {
            return ecCurve;
        }
        if (ecCurve.supportsCoordinateSystem(n)) {
            return ecCurve.configure().setCoordinateSystem(n).create();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Coordinate system ");
        sb.append(n);
        sb.append(" not supported by this curve");
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    protected ECPoint multiplyPositive(ECPoint add, final BigInteger bigInteger) {
        final ECCurve curve = add.getCurve();
        final ECCurve configureCurve = this.configureCurve(curve, this.additionCoord);
        final ECCurve configureCurve2 = this.configureCurve(curve, this.doublingCoord);
        final int[] generateCompactNaf = WNafUtil.generateCompactNaf(bigInteger);
        final ECPoint infinity = configureCurve.getInfinity();
        ECPoint importPoint = configureCurve2.importPoint(add);
        int i = 0;
        int n = 0;
        add = infinity;
        while (i < generateCompactNaf.length) {
            final int n2 = generateCompactNaf[i];
            final ECPoint timesPow2 = importPoint.timesPow2(n + (n2 & 0xFFFF));
            ECPoint ecPoint = configureCurve.importPoint(timesPow2);
            if (n2 >> 16 < 0) {
                ecPoint = ecPoint.negate();
            }
            add = add.add(ecPoint);
            ++i;
            n = 1;
            importPoint = timesPow2;
        }
        return curve.importPoint(add);
    }
}
