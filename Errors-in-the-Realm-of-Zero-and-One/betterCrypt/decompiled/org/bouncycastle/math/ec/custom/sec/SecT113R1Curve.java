// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.custom.sec;

import org.bouncycastle.math.ec.AbstractECLookupTable;
import org.bouncycastle.math.raw.Nat128;
import org.bouncycastle.math.ec.ECLookupTable;
import org.bouncycastle.math.ec.ECPoint;
import java.math.BigInteger;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.math.ec.ECConstants;
import org.bouncycastle.math.ec.ECFieldElement;
import org.bouncycastle.math.ec.ECCurve;

public class SecT113R1Curve extends AbstractF2m
{
    private static final ECFieldElement[] SECT113R1_AFFINE_ZS;
    private static final int SECT113R1_DEFAULT_COORDS = 6;
    protected SecT113R1Point infinity;
    
    static {
        SECT113R1_AFFINE_ZS = new ECFieldElement[] { new SecT113FieldElement(ECConstants.ONE) };
    }
    
    public SecT113R1Curve() {
        super(113, 9, 0, 0);
        this.infinity = new SecT113R1Point(this, null, null);
        this.a = this.fromBigInteger(new BigInteger(1, Hex.decodeStrict("003088250CA6E7C7FE649CE85820F7")));
        this.b = this.fromBigInteger(new BigInteger(1, Hex.decodeStrict("00E8BEE4D3E2260744188BE0E9C723")));
        this.order = new BigInteger(1, Hex.decodeStrict("0100000000000000D9CCEC8A39E56F"));
        this.cofactor = BigInteger.valueOf(2L);
        this.coord = 6;
    }
    
    @Override
    protected ECCurve cloneCurve() {
        return new SecT113R1Curve();
    }
    
    @Override
    public ECLookupTable createCacheSafeLookupTable(final ECPoint[] array, final int n, final int n2) {
        final long[] array2 = new long[n2 * 2 * 2];
        int n3;
        for (int i = n3 = 0; i < n2; ++i) {
            final ECPoint ecPoint = array[n + i];
            Nat128.copy64(((SecT113FieldElement)ecPoint.getRawXCoord()).x, 0, array2, n3);
            final int n4 = n3 + 2;
            Nat128.copy64(((SecT113FieldElement)ecPoint.getRawYCoord()).x, 0, array2, n4);
            n3 = n4 + 2;
        }
        return new AbstractECLookupTable() {
            private ECPoint createPoint(final long[] array, final long[] array2) {
                return SecT113R1Curve.this.createRawPoint(new SecT113FieldElement(array), new SecT113FieldElement(array2), SecT113R1Curve.SECT113R1_AFFINE_ZS);
            }
            
            @Override
            public int getSize() {
                return n2;
            }
            
            @Override
            public ECPoint lookup(final int n) {
                final long[] create64 = Nat128.create64();
                final long[] create65 = Nat128.create64();
                int n2;
                for (int i = n2 = 0; i < n2; ++i) {
                    final long n3 = (i ^ n) - 1 >> 31;
                    for (int j = 0; j < 2; ++j) {
                        final long n4 = create64[j];
                        final long[] val$table = array2;
                        create64[j] = (n4 ^ (val$table[n2 + j] & n3));
                        create65[j] ^= (val$table[n2 + 2 + j] & n3);
                    }
                    n2 += 4;
                }
                return this.createPoint(create64, create65);
            }
            
            @Override
            public ECPoint lookupVar(int i) {
                final long[] create64 = Nat128.create64();
                final long[] create65 = Nat128.create64();
                final int n = i * 2 * 2;
                long[] val$table;
                for (i = 0; i < 2; ++i) {
                    val$table = array2;
                    create64[i] = val$table[n + i];
                    create65[i] = val$table[n + 2 + i];
                }
                return this.createPoint(create64, create65);
            }
        };
    }
    
    @Override
    protected ECPoint createRawPoint(final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2) {
        return new SecT113R1Point(this, ecFieldElement, ecFieldElement2);
    }
    
    @Override
    protected ECPoint createRawPoint(final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2, final ECFieldElement[] array) {
        return new SecT113R1Point(this, ecFieldElement, ecFieldElement2, array);
    }
    
    @Override
    public ECFieldElement fromBigInteger(final BigInteger bigInteger) {
        return new SecT113FieldElement(bigInteger);
    }
    
    @Override
    public int getFieldSize() {
        return 113;
    }
    
    @Override
    public ECPoint getInfinity() {
        return this.infinity;
    }
    
    public int getK1() {
        return 9;
    }
    
    public int getK2() {
        return 0;
    }
    
    public int getK3() {
        return 0;
    }
    
    public int getM() {
        return 113;
    }
    
    @Override
    public boolean isKoblitz() {
        return false;
    }
    
    public boolean isTrinomial() {
        return true;
    }
    
    @Override
    public boolean supportsCoordinateSystem(final int n) {
        return n == 6;
    }
}
