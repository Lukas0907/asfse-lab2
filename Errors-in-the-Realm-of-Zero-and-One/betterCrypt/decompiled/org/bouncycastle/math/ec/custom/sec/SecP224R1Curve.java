// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.custom.sec;

import org.bouncycastle.math.ec.AbstractECLookupTable;
import org.bouncycastle.math.raw.Nat224;
import org.bouncycastle.math.ec.ECLookupTable;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.math.ec.ECConstants;
import java.math.BigInteger;
import org.bouncycastle.math.ec.ECFieldElement;
import org.bouncycastle.math.ec.ECCurve;

public class SecP224R1Curve extends AbstractFp
{
    private static final ECFieldElement[] SECP224R1_AFFINE_ZS;
    private static final int SECP224R1_DEFAULT_COORDS = 2;
    public static final BigInteger q;
    protected SecP224R1Point infinity;
    
    static {
        q = SecP224R1FieldElement.Q;
        SECP224R1_AFFINE_ZS = new ECFieldElement[] { new SecP224R1FieldElement(ECConstants.ONE) };
    }
    
    public SecP224R1Curve() {
        super(SecP224R1Curve.q);
        this.infinity = new SecP224R1Point(this, null, null);
        this.a = this.fromBigInteger(new BigInteger(1, Hex.decodeStrict("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFE")));
        this.b = this.fromBigInteger(new BigInteger(1, Hex.decodeStrict("B4050A850C04B3ABF54132565044B0B7D7BFD8BA270B39432355FFB4")));
        this.order = new BigInteger(1, Hex.decodeStrict("FFFFFFFFFFFFFFFFFFFFFFFFFFFF16A2E0B8F03E13DD29455C5C2A3D"));
        this.cofactor = BigInteger.valueOf(1L);
        this.coord = 2;
    }
    
    @Override
    protected ECCurve cloneCurve() {
        return new SecP224R1Curve();
    }
    
    @Override
    public ECLookupTable createCacheSafeLookupTable(final ECPoint[] array, final int n, final int n2) {
        final int[] array2 = new int[n2 * 7 * 2];
        int n3;
        for (int i = n3 = 0; i < n2; ++i) {
            final ECPoint ecPoint = array[n + i];
            Nat224.copy(((SecP224R1FieldElement)ecPoint.getRawXCoord()).x, 0, array2, n3);
            final int n4 = n3 + 7;
            Nat224.copy(((SecP224R1FieldElement)ecPoint.getRawYCoord()).x, 0, array2, n4);
            n3 = n4 + 7;
        }
        return new AbstractECLookupTable() {
            private ECPoint createPoint(final int[] array, final int[] array2) {
                return SecP224R1Curve.this.createRawPoint(new SecP224R1FieldElement(array), new SecP224R1FieldElement(array2), SecP224R1Curve.SECP224R1_AFFINE_ZS);
            }
            
            @Override
            public int getSize() {
                return n2;
            }
            
            @Override
            public ECPoint lookup(final int n) {
                final int[] create = Nat224.create();
                final int[] create2 = Nat224.create();
                int n2;
                for (int i = n2 = 0; i < n2; ++i) {
                    final int n3 = (i ^ n) - 1 >> 31;
                    for (int j = 0; j < 7; ++j) {
                        final int n4 = create[j];
                        final int[] val$table = array2;
                        create[j] = (n4 ^ (val$table[n2 + j] & n3));
                        create2[j] ^= (val$table[n2 + 7 + j] & n3);
                    }
                    n2 += 14;
                }
                return this.createPoint(create, create2);
            }
            
            @Override
            public ECPoint lookupVar(int i) {
                final int[] create = Nat224.create();
                final int[] create2 = Nat224.create();
                final int n = i * 7 * 2;
                int[] val$table;
                for (i = 0; i < 7; ++i) {
                    val$table = array2;
                    create[i] = val$table[n + i];
                    create2[i] = val$table[n + 7 + i];
                }
                return this.createPoint(create, create2);
            }
        };
    }
    
    @Override
    protected ECPoint createRawPoint(final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2) {
        return new SecP224R1Point(this, ecFieldElement, ecFieldElement2);
    }
    
    @Override
    protected ECPoint createRawPoint(final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2, final ECFieldElement[] array) {
        return new SecP224R1Point(this, ecFieldElement, ecFieldElement2, array);
    }
    
    @Override
    public ECFieldElement fromBigInteger(final BigInteger bigInteger) {
        return new SecP224R1FieldElement(bigInteger);
    }
    
    @Override
    public int getFieldSize() {
        return SecP224R1Curve.q.bitLength();
    }
    
    @Override
    public ECPoint getInfinity() {
        return this.infinity;
    }
    
    public BigInteger getQ() {
        return SecP224R1Curve.q;
    }
    
    @Override
    public boolean supportsCoordinateSystem(final int n) {
        return n == 2;
    }
}
