// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.custom.sec;

import org.bouncycastle.math.ec.AbstractECLookupTable;
import org.bouncycastle.math.raw.Nat160;
import org.bouncycastle.math.ec.ECLookupTable;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.math.ec.ECConstants;
import java.math.BigInteger;
import org.bouncycastle.math.ec.ECFieldElement;
import org.bouncycastle.math.ec.ECCurve;

public class SecP160R2Curve extends AbstractFp
{
    private static final ECFieldElement[] SECP160R2_AFFINE_ZS;
    private static final int SECP160R2_DEFAULT_COORDS = 2;
    public static final BigInteger q;
    protected SecP160R2Point infinity;
    
    static {
        q = SecP160R2FieldElement.Q;
        SECP160R2_AFFINE_ZS = new ECFieldElement[] { new SecP160R2FieldElement(ECConstants.ONE) };
    }
    
    public SecP160R2Curve() {
        super(SecP160R2Curve.q);
        this.infinity = new SecP160R2Point(this, null, null);
        this.a = this.fromBigInteger(new BigInteger(1, Hex.decodeStrict("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFAC70")));
        this.b = this.fromBigInteger(new BigInteger(1, Hex.decodeStrict("B4E134D3FB59EB8BAB57274904664D5AF50388BA")));
        this.order = new BigInteger(1, Hex.decodeStrict("0100000000000000000000351EE786A818F3A1A16B"));
        this.cofactor = BigInteger.valueOf(1L);
        this.coord = 2;
    }
    
    @Override
    protected ECCurve cloneCurve() {
        return new SecP160R2Curve();
    }
    
    @Override
    public ECLookupTable createCacheSafeLookupTable(final ECPoint[] array, final int n, final int n2) {
        final int[] array2 = new int[n2 * 5 * 2];
        int n3;
        for (int i = n3 = 0; i < n2; ++i) {
            final ECPoint ecPoint = array[n + i];
            Nat160.copy(((SecP160R2FieldElement)ecPoint.getRawXCoord()).x, 0, array2, n3);
            final int n4 = n3 + 5;
            Nat160.copy(((SecP160R2FieldElement)ecPoint.getRawYCoord()).x, 0, array2, n4);
            n3 = n4 + 5;
        }
        return new AbstractECLookupTable() {
            private ECPoint createPoint(final int[] array, final int[] array2) {
                return SecP160R2Curve.this.createRawPoint(new SecP160R2FieldElement(array), new SecP160R2FieldElement(array2), SecP160R2Curve.SECP160R2_AFFINE_ZS);
            }
            
            @Override
            public int getSize() {
                return n2;
            }
            
            @Override
            public ECPoint lookup(final int n) {
                final int[] create = Nat160.create();
                final int[] create2 = Nat160.create();
                int n2;
                for (int i = n2 = 0; i < n2; ++i) {
                    final int n3 = (i ^ n) - 1 >> 31;
                    for (int j = 0; j < 5; ++j) {
                        final int n4 = create[j];
                        final int[] val$table = array2;
                        create[j] = (n4 ^ (val$table[n2 + j] & n3));
                        create2[j] ^= (val$table[n2 + 5 + j] & n3);
                    }
                    n2 += 10;
                }
                return this.createPoint(create, create2);
            }
            
            @Override
            public ECPoint lookupVar(int i) {
                final int[] create = Nat160.create();
                final int[] create2 = Nat160.create();
                final int n = i * 5 * 2;
                int[] val$table;
                for (i = 0; i < 5; ++i) {
                    val$table = array2;
                    create[i] = val$table[n + i];
                    create2[i] = val$table[n + 5 + i];
                }
                return this.createPoint(create, create2);
            }
        };
    }
    
    @Override
    protected ECPoint createRawPoint(final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2) {
        return new SecP160R2Point(this, ecFieldElement, ecFieldElement2);
    }
    
    @Override
    protected ECPoint createRawPoint(final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2, final ECFieldElement[] array) {
        return new SecP160R2Point(this, ecFieldElement, ecFieldElement2, array);
    }
    
    @Override
    public ECFieldElement fromBigInteger(final BigInteger bigInteger) {
        return new SecP160R2FieldElement(bigInteger);
    }
    
    @Override
    public int getFieldSize() {
        return SecP160R2Curve.q.bitLength();
    }
    
    @Override
    public ECPoint getInfinity() {
        return this.infinity;
    }
    
    public BigInteger getQ() {
        return SecP160R2Curve.q;
    }
    
    @Override
    public boolean supportsCoordinateSystem(final int n) {
        return n == 2;
    }
}
