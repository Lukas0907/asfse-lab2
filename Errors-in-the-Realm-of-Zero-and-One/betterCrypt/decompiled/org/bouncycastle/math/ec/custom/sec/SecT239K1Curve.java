// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.custom.sec;

import org.bouncycastle.math.ec.WTauNafMultiplier;
import org.bouncycastle.math.ec.ECMultiplier;
import org.bouncycastle.math.ec.AbstractECLookupTable;
import org.bouncycastle.math.raw.Nat256;
import org.bouncycastle.math.ec.ECLookupTable;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.util.encoders.Hex;
import java.math.BigInteger;
import org.bouncycastle.math.ec.ECConstants;
import org.bouncycastle.math.ec.ECFieldElement;
import org.bouncycastle.math.ec.ECCurve;

public class SecT239K1Curve extends AbstractF2m
{
    private static final ECFieldElement[] SECT239K1_AFFINE_ZS;
    private static final int SECT239K1_DEFAULT_COORDS = 6;
    protected SecT239K1Point infinity;
    
    static {
        SECT239K1_AFFINE_ZS = new ECFieldElement[] { new SecT239FieldElement(ECConstants.ONE) };
    }
    
    public SecT239K1Curve() {
        super(239, 158, 0, 0);
        this.infinity = new SecT239K1Point(this, null, null);
        this.a = this.fromBigInteger(BigInteger.valueOf(0L));
        this.b = this.fromBigInteger(BigInteger.valueOf(1L));
        this.order = new BigInteger(1, Hex.decodeStrict("2000000000000000000000000000005A79FEC67CB6E91F1C1DA800E478A5"));
        this.cofactor = BigInteger.valueOf(4L);
        this.coord = 6;
    }
    
    @Override
    protected ECCurve cloneCurve() {
        return new SecT239K1Curve();
    }
    
    @Override
    public ECLookupTable createCacheSafeLookupTable(final ECPoint[] array, final int n, final int n2) {
        final long[] array2 = new long[n2 * 4 * 2];
        int n3;
        for (int i = n3 = 0; i < n2; ++i) {
            final ECPoint ecPoint = array[n + i];
            Nat256.copy64(((SecT239FieldElement)ecPoint.getRawXCoord()).x, 0, array2, n3);
            final int n4 = n3 + 4;
            Nat256.copy64(((SecT239FieldElement)ecPoint.getRawYCoord()).x, 0, array2, n4);
            n3 = n4 + 4;
        }
        return new AbstractECLookupTable() {
            private ECPoint createPoint(final long[] array, final long[] array2) {
                return SecT239K1Curve.this.createRawPoint(new SecT239FieldElement(array), new SecT239FieldElement(array2), SecT239K1Curve.SECT239K1_AFFINE_ZS);
            }
            
            @Override
            public int getSize() {
                return n2;
            }
            
            @Override
            public ECPoint lookup(final int n) {
                final long[] create64 = Nat256.create64();
                final long[] create65 = Nat256.create64();
                int n2;
                for (int i = n2 = 0; i < n2; ++i) {
                    final long n3 = (i ^ n) - 1 >> 31;
                    for (int j = 0; j < 4; ++j) {
                        final long n4 = create64[j];
                        final long[] val$table = array2;
                        create64[j] = (n4 ^ (val$table[n2 + j] & n3));
                        create65[j] ^= (val$table[n2 + 4 + j] & n3);
                    }
                    n2 += 8;
                }
                return this.createPoint(create64, create65);
            }
            
            @Override
            public ECPoint lookupVar(int i) {
                final long[] create64 = Nat256.create64();
                final long[] create65 = Nat256.create64();
                final int n = i * 4 * 2;
                long[] val$table;
                for (i = 0; i < 4; ++i) {
                    val$table = array2;
                    create64[i] = val$table[n + i];
                    create65[i] = val$table[n + 4 + i];
                }
                return this.createPoint(create64, create65);
            }
        };
    }
    
    @Override
    protected ECMultiplier createDefaultMultiplier() {
        return new WTauNafMultiplier();
    }
    
    @Override
    protected ECPoint createRawPoint(final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2) {
        return new SecT239K1Point(this, ecFieldElement, ecFieldElement2);
    }
    
    @Override
    protected ECPoint createRawPoint(final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2, final ECFieldElement[] array) {
        return new SecT239K1Point(this, ecFieldElement, ecFieldElement2, array);
    }
    
    @Override
    public ECFieldElement fromBigInteger(final BigInteger bigInteger) {
        return new SecT239FieldElement(bigInteger);
    }
    
    @Override
    public int getFieldSize() {
        return 239;
    }
    
    @Override
    public ECPoint getInfinity() {
        return this.infinity;
    }
    
    public int getK1() {
        return 158;
    }
    
    public int getK2() {
        return 0;
    }
    
    public int getK3() {
        return 0;
    }
    
    public int getM() {
        return 239;
    }
    
    @Override
    public boolean isKoblitz() {
        return true;
    }
    
    public boolean isTrinomial() {
        return true;
    }
    
    @Override
    public boolean supportsCoordinateSystem(final int n) {
        return n == 6;
    }
}
