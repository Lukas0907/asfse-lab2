// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.custom.sec;

import org.bouncycastle.math.ec.AbstractECLookupTable;
import org.bouncycastle.math.raw.Nat256;
import org.bouncycastle.math.ec.ECLookupTable;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.math.ec.ECConstants;
import java.math.BigInteger;
import org.bouncycastle.math.ec.ECFieldElement;
import org.bouncycastle.math.ec.ECCurve;

public class SecP256R1Curve extends AbstractFp
{
    private static final ECFieldElement[] SECP256R1_AFFINE_ZS;
    private static final int SECP256R1_DEFAULT_COORDS = 2;
    public static final BigInteger q;
    protected SecP256R1Point infinity;
    
    static {
        q = SecP256R1FieldElement.Q;
        SECP256R1_AFFINE_ZS = new ECFieldElement[] { new SecP256R1FieldElement(ECConstants.ONE) };
    }
    
    public SecP256R1Curve() {
        super(SecP256R1Curve.q);
        this.infinity = new SecP256R1Point(this, null, null);
        this.a = this.fromBigInteger(new BigInteger(1, Hex.decodeStrict("FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFC")));
        this.b = this.fromBigInteger(new BigInteger(1, Hex.decodeStrict("5AC635D8AA3A93E7B3EBBD55769886BC651D06B0CC53B0F63BCE3C3E27D2604B")));
        this.order = new BigInteger(1, Hex.decodeStrict("FFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632551"));
        this.cofactor = BigInteger.valueOf(1L);
        this.coord = 2;
    }
    
    @Override
    protected ECCurve cloneCurve() {
        return new SecP256R1Curve();
    }
    
    @Override
    public ECLookupTable createCacheSafeLookupTable(final ECPoint[] array, final int n, final int n2) {
        final int[] array2 = new int[n2 * 8 * 2];
        int n3;
        for (int i = n3 = 0; i < n2; ++i) {
            final ECPoint ecPoint = array[n + i];
            Nat256.copy(((SecP256R1FieldElement)ecPoint.getRawXCoord()).x, 0, array2, n3);
            final int n4 = n3 + 8;
            Nat256.copy(((SecP256R1FieldElement)ecPoint.getRawYCoord()).x, 0, array2, n4);
            n3 = n4 + 8;
        }
        return new AbstractECLookupTable() {
            private ECPoint createPoint(final int[] array, final int[] array2) {
                return SecP256R1Curve.this.createRawPoint(new SecP256R1FieldElement(array), new SecP256R1FieldElement(array2), SecP256R1Curve.SECP256R1_AFFINE_ZS);
            }
            
            @Override
            public int getSize() {
                return n2;
            }
            
            @Override
            public ECPoint lookup(final int n) {
                final int[] create = Nat256.create();
                final int[] create2 = Nat256.create();
                int n2;
                for (int i = n2 = 0; i < n2; ++i) {
                    final int n3 = (i ^ n) - 1 >> 31;
                    for (int j = 0; j < 8; ++j) {
                        final int n4 = create[j];
                        final int[] val$table = array2;
                        create[j] = (n4 ^ (val$table[n2 + j] & n3));
                        create2[j] ^= (val$table[n2 + 8 + j] & n3);
                    }
                    n2 += 16;
                }
                return this.createPoint(create, create2);
            }
            
            @Override
            public ECPoint lookupVar(int i) {
                final int[] create = Nat256.create();
                final int[] create2 = Nat256.create();
                final int n = i * 8 * 2;
                int[] val$table;
                for (i = 0; i < 8; ++i) {
                    val$table = array2;
                    create[i] = val$table[n + i];
                    create2[i] = val$table[n + 8 + i];
                }
                return this.createPoint(create, create2);
            }
        };
    }
    
    @Override
    protected ECPoint createRawPoint(final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2) {
        return new SecP256R1Point(this, ecFieldElement, ecFieldElement2);
    }
    
    @Override
    protected ECPoint createRawPoint(final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2, final ECFieldElement[] array) {
        return new SecP256R1Point(this, ecFieldElement, ecFieldElement2, array);
    }
    
    @Override
    public ECFieldElement fromBigInteger(final BigInteger bigInteger) {
        return new SecP256R1FieldElement(bigInteger);
    }
    
    @Override
    public int getFieldSize() {
        return SecP256R1Curve.q.bitLength();
    }
    
    @Override
    public ECPoint getInfinity() {
        return this.infinity;
    }
    
    public BigInteger getQ() {
        return SecP256R1Curve.q;
    }
    
    @Override
    public boolean supportsCoordinateSystem(final int n) {
        return n == 2;
    }
}
