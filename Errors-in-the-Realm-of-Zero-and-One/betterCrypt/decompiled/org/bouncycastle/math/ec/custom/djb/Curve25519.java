// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.custom.djb;

import org.bouncycastle.math.ec.AbstractECLookupTable;
import org.bouncycastle.math.raw.Nat256;
import org.bouncycastle.math.ec.ECLookupTable;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.math.ec.ECConstants;
import org.bouncycastle.util.encoders.Hex;
import java.math.BigInteger;
import org.bouncycastle.math.ec.ECFieldElement;
import org.bouncycastle.math.ec.ECCurve;

public class Curve25519 extends AbstractFp
{
    private static final ECFieldElement[] CURVE25519_AFFINE_ZS;
    private static final int CURVE25519_DEFAULT_COORDS = 4;
    private static final BigInteger C_a;
    private static final BigInteger C_b;
    public static final BigInteger q;
    protected Curve25519Point infinity;
    
    static {
        q = Curve25519FieldElement.Q;
        C_a = new BigInteger(1, Hex.decodeStrict("2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA984914A144"));
        C_b = new BigInteger(1, Hex.decodeStrict("7B425ED097B425ED097B425ED097B425ED097B425ED097B4260B5E9C7710C864"));
        CURVE25519_AFFINE_ZS = new ECFieldElement[] { new Curve25519FieldElement(ECConstants.ONE), new Curve25519FieldElement(Curve25519.C_a) };
    }
    
    public Curve25519() {
        super(Curve25519.q);
        this.infinity = new Curve25519Point(this, null, null);
        this.a = this.fromBigInteger(Curve25519.C_a);
        this.b = this.fromBigInteger(Curve25519.C_b);
        this.order = new BigInteger(1, Hex.decodeStrict("1000000000000000000000000000000014DEF9DEA2F79CD65812631A5CF5D3ED"));
        this.cofactor = BigInteger.valueOf(8L);
        this.coord = 4;
    }
    
    @Override
    protected ECCurve cloneCurve() {
        return new Curve25519();
    }
    
    @Override
    public ECLookupTable createCacheSafeLookupTable(final ECPoint[] array, final int n, final int n2) {
        final int[] array2 = new int[n2 * 8 * 2];
        int n3;
        for (int i = n3 = 0; i < n2; ++i) {
            final ECPoint ecPoint = array[n + i];
            Nat256.copy(((Curve25519FieldElement)ecPoint.getRawXCoord()).x, 0, array2, n3);
            final int n4 = n3 + 8;
            Nat256.copy(((Curve25519FieldElement)ecPoint.getRawYCoord()).x, 0, array2, n4);
            n3 = n4 + 8;
        }
        return new AbstractECLookupTable() {
            private ECPoint createPoint(final int[] array, final int[] array2) {
                return Curve25519.this.createRawPoint(new Curve25519FieldElement(array), new Curve25519FieldElement(array2), Curve25519.CURVE25519_AFFINE_ZS);
            }
            
            @Override
            public int getSize() {
                return n2;
            }
            
            @Override
            public ECPoint lookup(final int n) {
                final int[] create = Nat256.create();
                final int[] create2 = Nat256.create();
                int n2;
                for (int i = n2 = 0; i < n2; ++i) {
                    final int n3 = (i ^ n) - 1 >> 31;
                    for (int j = 0; j < 8; ++j) {
                        final int n4 = create[j];
                        final int[] val$table = array2;
                        create[j] = (n4 ^ (val$table[n2 + j] & n3));
                        create2[j] ^= (val$table[n2 + 8 + j] & n3);
                    }
                    n2 += 16;
                }
                return this.createPoint(create, create2);
            }
            
            @Override
            public ECPoint lookupVar(int i) {
                final int[] create = Nat256.create();
                final int[] create2 = Nat256.create();
                final int n = i * 8 * 2;
                int[] val$table;
                for (i = 0; i < 8; ++i) {
                    val$table = array2;
                    create[i] = val$table[n + i];
                    create2[i] = val$table[n + 8 + i];
                }
                return this.createPoint(create, create2);
            }
        };
    }
    
    @Override
    protected ECPoint createRawPoint(final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2) {
        return new Curve25519Point(this, ecFieldElement, ecFieldElement2);
    }
    
    @Override
    protected ECPoint createRawPoint(final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2, final ECFieldElement[] array) {
        return new Curve25519Point(this, ecFieldElement, ecFieldElement2, array);
    }
    
    @Override
    public ECFieldElement fromBigInteger(final BigInteger bigInteger) {
        return new Curve25519FieldElement(bigInteger);
    }
    
    @Override
    public int getFieldSize() {
        return Curve25519.q.bitLength();
    }
    
    @Override
    public ECPoint getInfinity() {
        return this.infinity;
    }
    
    public BigInteger getQ() {
        return Curve25519.q;
    }
    
    @Override
    public boolean supportsCoordinateSystem(final int n) {
        return n == 4;
    }
}
