// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.custom.sec;

import org.bouncycastle.math.ec.AbstractECLookupTable;
import org.bouncycastle.math.raw.Nat192;
import org.bouncycastle.math.ec.ECLookupTable;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.util.encoders.Hex;
import java.math.BigInteger;
import org.bouncycastle.math.ec.ECConstants;
import org.bouncycastle.math.ec.ECFieldElement;
import org.bouncycastle.math.ec.ECCurve;

public class SecT163R2Curve extends AbstractF2m
{
    private static final ECFieldElement[] SECT163R2_AFFINE_ZS;
    private static final int SECT163R2_DEFAULT_COORDS = 6;
    protected SecT163R2Point infinity;
    
    static {
        SECT163R2_AFFINE_ZS = new ECFieldElement[] { new SecT163FieldElement(ECConstants.ONE) };
    }
    
    public SecT163R2Curve() {
        super(163, 3, 6, 7);
        this.infinity = new SecT163R2Point(this, null, null);
        this.a = this.fromBigInteger(BigInteger.valueOf(1L));
        this.b = this.fromBigInteger(new BigInteger(1, Hex.decodeStrict("020A601907B8C953CA1481EB10512F78744A3205FD")));
        this.order = new BigInteger(1, Hex.decodeStrict("040000000000000000000292FE77E70C12A4234C33"));
        this.cofactor = BigInteger.valueOf(2L);
        this.coord = 6;
    }
    
    @Override
    protected ECCurve cloneCurve() {
        return new SecT163R2Curve();
    }
    
    @Override
    public ECLookupTable createCacheSafeLookupTable(final ECPoint[] array, final int n, final int n2) {
        final long[] array2 = new long[n2 * 3 * 2];
        int n3;
        for (int i = n3 = 0; i < n2; ++i) {
            final ECPoint ecPoint = array[n + i];
            Nat192.copy64(((SecT163FieldElement)ecPoint.getRawXCoord()).x, 0, array2, n3);
            final int n4 = n3 + 3;
            Nat192.copy64(((SecT163FieldElement)ecPoint.getRawYCoord()).x, 0, array2, n4);
            n3 = n4 + 3;
        }
        return new AbstractECLookupTable() {
            private ECPoint createPoint(final long[] array, final long[] array2) {
                return SecT163R2Curve.this.createRawPoint(new SecT163FieldElement(array), new SecT163FieldElement(array2), SecT163R2Curve.SECT163R2_AFFINE_ZS);
            }
            
            @Override
            public int getSize() {
                return n2;
            }
            
            @Override
            public ECPoint lookup(final int n) {
                final long[] create64 = Nat192.create64();
                final long[] create65 = Nat192.create64();
                int n2;
                for (int i = n2 = 0; i < n2; ++i) {
                    final long n3 = (i ^ n) - 1 >> 31;
                    for (int j = 0; j < 3; ++j) {
                        final long n4 = create64[j];
                        final long[] val$table = array2;
                        create64[j] = (n4 ^ (val$table[n2 + j] & n3));
                        create65[j] ^= (val$table[n2 + 3 + j] & n3);
                    }
                    n2 += 6;
                }
                return this.createPoint(create64, create65);
            }
            
            @Override
            public ECPoint lookupVar(int i) {
                final long[] create64 = Nat192.create64();
                final long[] create65 = Nat192.create64();
                final int n = i * 3 * 2;
                long[] val$table;
                for (i = 0; i < 3; ++i) {
                    val$table = array2;
                    create64[i] = val$table[n + i];
                    create65[i] = val$table[n + 3 + i];
                }
                return this.createPoint(create64, create65);
            }
        };
    }
    
    @Override
    protected ECPoint createRawPoint(final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2) {
        return new SecT163R2Point(this, ecFieldElement, ecFieldElement2);
    }
    
    @Override
    protected ECPoint createRawPoint(final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2, final ECFieldElement[] array) {
        return new SecT163R2Point(this, ecFieldElement, ecFieldElement2, array);
    }
    
    @Override
    public ECFieldElement fromBigInteger(final BigInteger bigInteger) {
        return new SecT163FieldElement(bigInteger);
    }
    
    @Override
    public int getFieldSize() {
        return 163;
    }
    
    @Override
    public ECPoint getInfinity() {
        return this.infinity;
    }
    
    public int getK1() {
        return 3;
    }
    
    public int getK2() {
        return 6;
    }
    
    public int getK3() {
        return 7;
    }
    
    public int getM() {
        return 163;
    }
    
    @Override
    public boolean isKoblitz() {
        return false;
    }
    
    public boolean isTrinomial() {
        return false;
    }
    
    @Override
    public boolean supportsCoordinateSystem(final int n) {
        return n == 6;
    }
}
