// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.custom.sec;

import org.bouncycastle.math.ec.ECConstants;
import org.bouncycastle.math.ec.ECFieldElement;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.math.ec.ECPoint;

public class SecT163R2Point extends AbstractF2m
{
    SecT163R2Point(final ECCurve ecCurve, final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2) {
        super(ecCurve, ecFieldElement, ecFieldElement2);
    }
    
    SecT163R2Point(final ECCurve ecCurve, final ECFieldElement ecFieldElement, final ECFieldElement ecFieldElement2, final ECFieldElement[] array) {
        super(ecCurve, ecFieldElement, ecFieldElement2, array);
    }
    
    @Override
    public ECPoint add(final ECPoint ecPoint) {
        if (this.isInfinity()) {
            return ecPoint;
        }
        if (ecPoint.isInfinity()) {
            return this;
        }
        final ECCurve curve = this.getCurve();
        ECFieldElement ecFieldElement = this.x;
        final ECFieldElement rawXCoord = ecPoint.getRawXCoord();
        if (ecFieldElement.isZero()) {
            if (rawXCoord.isZero()) {
                return curve.getInfinity();
            }
            return ecPoint.add(this);
        }
        else {
            final ECFieldElement y = this.y;
            final ECFieldElement ecFieldElement2 = this.zs[0];
            final ECFieldElement rawYCoord = ecPoint.getRawYCoord();
            final ECFieldElement zCoord = ecPoint.getZCoord(0);
            final boolean one = ecFieldElement2.isOne();
            ECFieldElement multiply;
            ECFieldElement multiply2;
            if (!one) {
                multiply = rawXCoord.multiply(ecFieldElement2);
                multiply2 = rawYCoord.multiply(ecFieldElement2);
            }
            else {
                multiply = rawXCoord;
                multiply2 = rawYCoord;
            }
            final boolean one2 = zCoord.isOne();
            ECFieldElement multiply3;
            if (!one2) {
                ecFieldElement = ecFieldElement.multiply(zCoord);
                multiply3 = y.multiply(zCoord);
            }
            else {
                multiply3 = y;
            }
            final ECFieldElement add = multiply3.add(multiply2);
            final ECFieldElement add2 = ecFieldElement.add(multiply);
            if (!add2.isZero()) {
                ECFieldElement addOne;
                ECFieldElement add3;
                ECFieldElement ecFieldElement3;
                if (rawXCoord.isZero()) {
                    final ECPoint normalize = this.normalize();
                    final ECFieldElement xCoord = normalize.getXCoord();
                    final ECFieldElement yCoord = normalize.getYCoord();
                    final ECFieldElement divide = yCoord.add(rawYCoord).divide(xCoord);
                    addOne = divide.square().add(divide).add(xCoord).addOne();
                    if (addOne.isZero()) {
                        return new SecT163R2Point(curve, addOne, curve.getB().sqrt());
                    }
                    add3 = divide.multiply(xCoord.add(addOne)).add(addOne).add(yCoord).divide(addOne).add(addOne);
                    ecFieldElement3 = curve.fromBigInteger(ECConstants.ONE);
                }
                else {
                    final ECFieldElement square = add2.square();
                    final ECFieldElement multiply4 = add.multiply(ecFieldElement);
                    final ECFieldElement multiply5 = add.multiply(multiply);
                    final ECFieldElement multiply6 = multiply4.multiply(multiply5);
                    if (multiply6.isZero()) {
                        return new SecT163R2Point(curve, multiply6, curve.getB().sqrt());
                    }
                    ECFieldElement ecFieldElement4 = add.multiply(square);
                    if (!one2) {
                        ecFieldElement4 = ecFieldElement4.multiply(zCoord);
                    }
                    final ECFieldElement squarePlusProduct = multiply5.add(square).squarePlusProduct(ecFieldElement4, y.add(ecFieldElement2));
                    addOne = multiply6;
                    add3 = squarePlusProduct;
                    ecFieldElement3 = ecFieldElement4;
                    if (!one) {
                        ecFieldElement3 = ecFieldElement4.multiply(ecFieldElement2);
                        add3 = squarePlusProduct;
                        addOne = multiply6;
                    }
                }
                return new SecT163R2Point(curve, addOne, add3, new ECFieldElement[] { ecFieldElement3 });
            }
            if (add.isZero()) {
                return this.twice();
            }
            return curve.getInfinity();
        }
    }
    
    @Override
    protected ECPoint detach() {
        return new SecT163R2Point(null, this.getAffineXCoord(), this.getAffineYCoord());
    }
    
    @Override
    protected boolean getCompressionYTilde() {
        final ECFieldElement rawXCoord = this.getRawXCoord();
        final boolean zero = rawXCoord.isZero();
        boolean b = false;
        if (zero) {
            return false;
        }
        if (this.getRawYCoord().testBitZero() != rawXCoord.testBitZero()) {
            b = true;
        }
        return b;
    }
    
    @Override
    public ECFieldElement getYCoord() {
        final ECFieldElement x = this.x;
        final ECFieldElement y = this.y;
        if (this.isInfinity()) {
            return y;
        }
        if (x.isZero()) {
            return y;
        }
        final ECFieldElement multiply = y.add(x).multiply(x);
        final ECFieldElement ecFieldElement = this.zs[0];
        ECFieldElement divide = multiply;
        if (!ecFieldElement.isOne()) {
            divide = multiply.divide(ecFieldElement);
        }
        return divide;
    }
    
    @Override
    public ECPoint negate() {
        if (this.isInfinity()) {
            return this;
        }
        final ECFieldElement x = this.x;
        if (x.isZero()) {
            return this;
        }
        final ECFieldElement y = this.y;
        final ECFieldElement ecFieldElement = this.zs[0];
        return new SecT163R2Point(this.curve, x, y.add(ecFieldElement), new ECFieldElement[] { ecFieldElement });
    }
    
    @Override
    public ECPoint twice() {
        if (this.isInfinity()) {
            return this;
        }
        final ECCurve curve = this.getCurve();
        ECFieldElement ecFieldElement = this.x;
        if (ecFieldElement.isZero()) {
            return curve.getInfinity();
        }
        final ECFieldElement y = this.y;
        final ECFieldElement ecFieldElement2 = this.zs[0];
        final boolean one = ecFieldElement2.isOne();
        ECFieldElement multiply;
        if (one) {
            multiply = y;
        }
        else {
            multiply = y.multiply(ecFieldElement2);
        }
        ECFieldElement square;
        if (one) {
            square = ecFieldElement2;
        }
        else {
            square = ecFieldElement2.square();
        }
        final ECFieldElement add = y.square().add(multiply).add(square);
        if (add.isZero()) {
            return new SecT163R2Point(curve, add, curve.getB().sqrt());
        }
        final ECFieldElement square2 = add.square();
        ECFieldElement multiply2;
        if (one) {
            multiply2 = add;
        }
        else {
            multiply2 = add.multiply(square);
        }
        if (!one) {
            ecFieldElement = ecFieldElement.multiply(ecFieldElement2);
        }
        return new SecT163R2Point(curve, square2, ecFieldElement.squarePlusProduct(add, multiply).add(square2).add(multiply2), new ECFieldElement[] { multiply2 });
    }
    
    @Override
    public ECPoint twicePlus(final ECPoint ecPoint) {
        if (this.isInfinity()) {
            return ecPoint;
        }
        if (ecPoint.isInfinity()) {
            return this.twice();
        }
        final ECCurve curve = this.getCurve();
        final ECFieldElement x = this.x;
        if (x.isZero()) {
            return ecPoint;
        }
        final ECFieldElement rawXCoord = ecPoint.getRawXCoord();
        final ECFieldElement zCoord = ecPoint.getZCoord(0);
        if (rawXCoord.isZero() || !zCoord.isOne()) {
            return this.twice().add(ecPoint);
        }
        final ECFieldElement y = this.y;
        final ECFieldElement ecFieldElement = this.zs[0];
        final ECFieldElement rawYCoord = ecPoint.getRawYCoord();
        final ECFieldElement square = x.square();
        final ECFieldElement square2 = y.square();
        final ECFieldElement square3 = ecFieldElement.square();
        final ECFieldElement add = square3.add(square2).add(y.multiply(ecFieldElement));
        final ECFieldElement multiplyPlusProduct = rawYCoord.multiply(square3).add(square2).multiplyPlusProduct(add, square, square3);
        final ECFieldElement multiply = rawXCoord.multiply(square3);
        final ECFieldElement square4 = multiply.add(add).square();
        if (square4.isZero()) {
            if (multiplyPlusProduct.isZero()) {
                return ecPoint.twice();
            }
            return curve.getInfinity();
        }
        else {
            if (multiplyPlusProduct.isZero()) {
                return new SecT163R2Point(curve, multiplyPlusProduct, curve.getB().sqrt());
            }
            final ECFieldElement multiply2 = multiplyPlusProduct.square().multiply(multiply);
            final ECFieldElement multiply3 = multiplyPlusProduct.multiply(square4).multiply(square3);
            return new SecT163R2Point(curve, multiply2, multiplyPlusProduct.add(square4).square().multiplyPlusProduct(add, rawYCoord.addOne(), multiply3), new ECFieldElement[] { multiply3 });
        }
    }
}
