// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec;

public class SimpleLookupTable extends AbstractECLookupTable
{
    private final ECPoint[] points;
    
    public SimpleLookupTable(final ECPoint[] array, final int n, final int n2) {
        this.points = copy(array, n, n2);
    }
    
    private static ECPoint[] copy(final ECPoint[] array, final int n, final int n2) {
        final ECPoint[] array2 = new ECPoint[n2];
        for (int i = 0; i < n2; ++i) {
            array2[i] = array[n + i];
        }
        return array2;
    }
    
    @Override
    public int getSize() {
        return this.points.length;
    }
    
    @Override
    public ECPoint lookup(final int n) {
        throw new UnsupportedOperationException("Constant-time lookup not supported");
    }
    
    @Override
    public ECPoint lookupVar(final int n) {
        return this.points[n];
    }
}
