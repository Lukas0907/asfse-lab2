// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec;

public interface ECPointMap
{
    ECPoint map(final ECPoint p0);
}
