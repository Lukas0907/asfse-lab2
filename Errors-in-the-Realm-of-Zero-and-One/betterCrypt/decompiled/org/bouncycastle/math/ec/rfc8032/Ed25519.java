// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.rfc8032;

import org.bouncycastle.math.ec.rfc7748.X25519;
import org.bouncycastle.math.raw.Interleave;
import org.bouncycastle.math.raw.Nat;
import java.security.SecureRandom;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.digests.SHA512Digest;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.math.ec.rfc7748.X25519Field;
import org.bouncycastle.math.raw.Nat256;
import org.bouncycastle.util.Strings;

public abstract class Ed25519
{
    private static final int[] B_x;
    private static final int[] B_y;
    private static final int[] C_d;
    private static final int[] C_d2;
    private static final int[] C_d4;
    private static final byte[] DOM2_PREFIX;
    private static final int[] L;
    private static final int L0 = -50998291;
    private static final int L1 = 19280294;
    private static final int L2 = 127719000;
    private static final int L3 = -6428113;
    private static final int L4 = 5343;
    private static final long M28L = 268435455L;
    private static final long M32L = 4294967295L;
    private static final int[] P;
    private static final int POINT_BYTES = 32;
    private static final int PRECOMP_BLOCKS = 8;
    private static final int PRECOMP_MASK = 7;
    private static final int PRECOMP_POINTS = 8;
    private static final int PRECOMP_SPACING = 8;
    private static final int PRECOMP_TEETH = 4;
    public static final int PREHASH_SIZE = 64;
    public static final int PUBLIC_KEY_SIZE = 32;
    private static final int SCALAR_BYTES = 32;
    private static final int SCALAR_INTS = 8;
    public static final int SECRET_KEY_SIZE = 32;
    public static final int SIGNATURE_SIZE = 64;
    private static final int WNAF_WIDTH_BASE = 7;
    private static int[] precompBase;
    private static PointExt[] precompBaseTable;
    private static final Object precompLock;
    
    static {
        DOM2_PREFIX = Strings.toByteArray("SigEd25519 no Ed25519 collisions");
        P = new int[] { -19, -1, -1, -1, -1, -1, -1, Integer.MAX_VALUE };
        L = new int[] { 1559614445, 1477600026, -1560830762, 350157278, 0, 0, 0, 268435456 };
        B_x = new int[] { 52811034, 25909283, 8072341, 50637101, 13785486, 30858332, 20483199, 20966410, 43936626, 4379245 };
        B_y = new int[] { 40265304, 26843545, 6710886, 53687091, 13421772, 40265318, 26843545, 6710886, 53687091, 13421772 };
        C_d = new int[] { 56195235, 47411844, 25868126, 40503822, 57364, 58321048, 30416477, 31930572, 57760639, 10749657 };
        C_d2 = new int[] { 45281625, 27714825, 18181821, 13898781, 114729, 49533232, 60832955, 30306712, 48412415, 4722099 };
        C_d4 = new int[] { 23454386, 55429651, 2809210, 27797563, 229458, 31957600, 54557047, 27058993, 29715967, 9444199 };
        precompLock = new Object();
        Ed25519.precompBaseTable = null;
        Ed25519.precompBase = null;
    }
    
    private static byte[] calculateS(byte[] array, final byte[] array2, final byte[] array3) {
        final int[] array4 = new int[16];
        int i = 0;
        decodeScalar(array, 0, array4);
        final int[] array5 = new int[8];
        decodeScalar(array2, 0, array5);
        final int[] array6 = new int[8];
        decodeScalar(array3, 0, array6);
        Nat256.mulAddTo(array5, array6, array4);
        array = new byte[64];
        while (i < array4.length) {
            encode32(array4[i], array, i * 4);
            ++i;
        }
        return reduceScalar(array);
    }
    
    private static boolean checkContextVar(final byte[] array, final byte b) {
        return (array == null && b == 0) || (array != null && array.length < 256);
    }
    
    private static int checkPoint(final int[] array, final int[] array2) {
        final int[] create = X25519Field.create();
        final int[] create2 = X25519Field.create();
        final int[] create3 = X25519Field.create();
        X25519Field.sqr(array, create2);
        X25519Field.sqr(array2, create3);
        X25519Field.mul(create2, create3, create);
        X25519Field.sub(create3, create2, create3);
        X25519Field.mul(create, Ed25519.C_d, create);
        X25519Field.addOne(create);
        X25519Field.sub(create, create3, create);
        X25519Field.normalize(create);
        return X25519Field.isZero(create);
    }
    
    private static int checkPoint(final int[] array, final int[] array2, final int[] array3) {
        final int[] create = X25519Field.create();
        final int[] create2 = X25519Field.create();
        final int[] create3 = X25519Field.create();
        final int[] create4 = X25519Field.create();
        X25519Field.sqr(array, create2);
        X25519Field.sqr(array2, create3);
        X25519Field.sqr(array3, create4);
        X25519Field.mul(create2, create3, create);
        X25519Field.sub(create3, create2, create3);
        X25519Field.mul(create3, create4, create3);
        X25519Field.sqr(create4, create4);
        X25519Field.mul(create, Ed25519.C_d, create);
        X25519Field.add(create, create4, create);
        X25519Field.sub(create, create3, create);
        X25519Field.normalize(create);
        return X25519Field.isZero(create);
    }
    
    private static boolean checkPointVar(final byte[] array) {
        final int[] array2 = new int[8];
        decode32(array, 0, array2, 0, 8);
        array2[7] &= Integer.MAX_VALUE;
        return Nat256.gte(array2, Ed25519.P) ^ true;
    }
    
    private static boolean checkScalarVar(final byte[] array) {
        final int[] array2 = new int[8];
        decodeScalar(array, 0, array2);
        return Nat256.gte(array2, Ed25519.L) ^ true;
    }
    
    private static Digest createDigest() {
        return new SHA512Digest();
    }
    
    public static Digest createPrehash() {
        return createDigest();
    }
    
    private static int decode24(final byte[] array, int n) {
        final byte b = array[n];
        ++n;
        return (array[n + 1] & 0xFF) << 16 | ((b & 0xFF) | (array[n] & 0xFF) << 8);
    }
    
    private static int decode32(final byte[] array, int n) {
        final byte b = array[n];
        final int n2 = n + 1;
        n = array[n2];
        final int n3 = n2 + 1;
        return array[n3 + 1] << 24 | ((b & 0xFF) | (n & 0xFF) << 8 | (array[n3] & 0xFF) << 16);
    }
    
    private static void decode32(final byte[] array, final int n, final int[] array2, final int n2, final int n3) {
        for (int i = 0; i < n3; ++i) {
            array2[n2 + i] = decode32(array, i * 4 + n);
        }
    }
    
    private static boolean decodePointVar(byte[] copyOfRange, int n, final boolean b, final PointAffine pointAffine) {
        copyOfRange = Arrays.copyOfRange(copyOfRange, n, n + 32);
        final boolean checkPointVar = checkPointVar(copyOfRange);
        n = 0;
        if (!checkPointVar) {
            return false;
        }
        final int n2 = (copyOfRange[31] & 0x80) >>> 7;
        copyOfRange[31] &= 0x7F;
        X25519Field.decode(copyOfRange, 0, pointAffine.y);
        final int[] create = X25519Field.create();
        final int[] create2 = X25519Field.create();
        X25519Field.sqr(pointAffine.y, create);
        X25519Field.mul(Ed25519.C_d, create, create2);
        X25519Field.subOne(create);
        X25519Field.addOne(create2);
        if (!X25519Field.sqrtRatioVar(create, create2, pointAffine.x)) {
            return false;
        }
        X25519Field.normalize(pointAffine.x);
        if (n2 == 1 && X25519Field.isZeroVar(pointAffine.x)) {
            return false;
        }
        if (n2 != (pointAffine.x[0] & 0x1)) {
            n = 1;
        }
        if (((b ? 1 : 0) ^ n) != 0x0) {
            X25519Field.negate(pointAffine.x, pointAffine.x);
        }
        return true;
    }
    
    private static void decodeScalar(final byte[] array, final int n, final int[] array2) {
        decode32(array, n, array2, 0, 8);
    }
    
    private static void dom2(final Digest digest, final byte b, final byte[] array) {
        if (array != null) {
            final byte[] dom2_PREFIX = Ed25519.DOM2_PREFIX;
            digest.update(dom2_PREFIX, 0, dom2_PREFIX.length);
            digest.update(b);
            digest.update((byte)array.length);
            digest.update(array, 0, array.length);
        }
    }
    
    private static void encode24(final int n, final byte[] array, int n2) {
        array[n2] = (byte)n;
        ++n2;
        array[n2] = (byte)(n >>> 8);
        array[n2 + 1] = (byte)(n >>> 16);
    }
    
    private static void encode32(final int n, final byte[] array, int n2) {
        array[n2] = (byte)n;
        ++n2;
        array[n2] = (byte)(n >>> 8);
        ++n2;
        array[n2] = (byte)(n >>> 16);
        array[n2 + 1] = (byte)(n >>> 24);
    }
    
    private static void encode56(final long n, final byte[] array, final int n2) {
        encode32((int)n, array, n2);
        encode24((int)(n >>> 32), array, n2 + 4);
    }
    
    private static int encodePoint(final PointAccum pointAccum, final byte[] array, int n) {
        final int[] create = X25519Field.create();
        final int[] create2 = X25519Field.create();
        X25519Field.inv(pointAccum.z, create2);
        X25519Field.mul(pointAccum.x, create2, create);
        X25519Field.mul(pointAccum.y, create2, create2);
        X25519Field.normalize(create);
        X25519Field.normalize(create2);
        final int checkPoint = checkPoint(create, create2);
        X25519Field.encode(create2, array, n);
        n = n + 32 - 1;
        array[n] |= (byte)((create[0] & 0x1) << 7);
        return checkPoint;
    }
    
    public static void generatePrivateKey(final SecureRandom secureRandom, final byte[] bytes) {
        secureRandom.nextBytes(bytes);
    }
    
    public static void generatePublicKey(byte[] array, final int n, final byte[] array2, final int n2) {
        final Digest digest = createDigest();
        final byte[] array3 = new byte[digest.getDigestSize()];
        digest.update(array, n, 32);
        digest.doFinal(array3, 0);
        array = new byte[32];
        pruneScalar(array3, 0, array);
        scalarMultBaseEncoded(array, array2, n2);
    }
    
    private static byte[] getWNAF(final int[] array, final int n) {
        final int[] array2 = new int[16];
        int length = array2.length;
        final int n2 = 0;
        int n3 = 8;
        int n4 = 0;
        while (true) {
            --n3;
            if (n3 < 0) {
                break;
            }
            final int n5 = array[n3];
            final int n6 = length - 1;
            array2[n6] = (n4 << 16 | n5 >>> 16);
            length = n6 - 1;
            array2[length] = n5;
            n4 = n5;
        }
        final byte[] array3 = new byte[253];
        final int n7 = 1 << n;
        int n8;
        for (int i = n8 = 0, j = n2; j < array2.length; ++j, i -= 16) {
            final int n9 = array2[j];
            while (i < 16) {
                final int n10 = n9 >>> i;
                if ((n10 & 0x1) == n8) {
                    ++i;
                }
                else {
                    final int n11 = (n10 & n7 - 1) + n8;
                    final int n12 = n11 & n7 >>> 1;
                    n8 = n12 >>> n - 1;
                    array3[(j << 4) + i] = (byte)(n11 - (n12 << 1));
                    i += n;
                }
            }
        }
        return array3;
    }
    
    private static int getWindow4(final int[] array, final int n) {
        return array[n >>> 3] >>> ((n & 0x7) << 2) & 0xF;
    }
    
    private static void implSign(final Digest digest, final byte[] array, final byte[] array2, final byte[] array3, final int n, final byte[] array4, final byte b, final byte[] array5, final int n2, final int n3, final byte[] array6, final int n4) {
        dom2(digest, b, array4);
        digest.update(array, 32, 32);
        digest.update(array5, n2, n3);
        digest.doFinal(array, 0);
        final byte[] reduceScalar = reduceScalar(array);
        final byte[] array7 = new byte[32];
        scalarMultBaseEncoded(reduceScalar, array7, 0);
        dom2(digest, b, array4);
        digest.update(array7, 0, 32);
        digest.update(array3, n, 32);
        digest.update(array5, n2, n3);
        digest.doFinal(array, 0);
        final byte[] calculateS = calculateS(reduceScalar, reduceScalar(array), array2);
        System.arraycopy(array7, 0, array6, n4, 32);
        System.arraycopy(calculateS, 0, array6, n4 + 32, 32);
    }
    
    private static void implSign(byte[] array, final int n, final byte[] array2, final byte b, final byte[] array3, final int n2, final int n3, final byte[] array4, final int n4) {
        if (checkContextVar(array2, b)) {
            final Digest digest = createDigest();
            final byte[] array5 = new byte[digest.getDigestSize()];
            digest.update(array, n, 32);
            digest.doFinal(array5, 0);
            array = new byte[32];
            pruneScalar(array5, 0, array);
            final byte[] array6 = new byte[32];
            scalarMultBaseEncoded(array, array6, 0);
            implSign(digest, array5, array, array6, 0, array2, b, array3, n2, n3, array4, n4);
            return;
        }
        throw new IllegalArgumentException("ctx");
    }
    
    private static void implSign(byte[] array, final int n, final byte[] array2, final int n2, final byte[] array3, final byte b, final byte[] array4, final int n3, final int n4, final byte[] array5, final int n5) {
        if (checkContextVar(array3, b)) {
            final Digest digest = createDigest();
            final byte[] array6 = new byte[digest.getDigestSize()];
            digest.update(array, n, 32);
            digest.doFinal(array6, 0);
            array = new byte[32];
            pruneScalar(array6, 0, array);
            implSign(digest, array6, array, array2, n2, array3, b, array4, n3, n4, array5, n5);
            return;
        }
        throw new IllegalArgumentException("ctx");
    }
    
    private static boolean implVerify(byte[] array, final int n, final byte[] array2, final int n2, byte[] reduceScalar, final byte b, final byte[] array3, final int n3, final int n4) {
        if (!checkContextVar(reduceScalar, b)) {
            throw new IllegalArgumentException("ctx");
        }
        final int n5 = n + 32;
        final byte[] copyOfRange = Arrays.copyOfRange(array, n, n5);
        final byte[] copyOfRange2 = Arrays.copyOfRange(array, n5, n + 64);
        final boolean checkPointVar = checkPointVar(copyOfRange);
        final boolean b2 = false;
        if (!checkPointVar) {
            return false;
        }
        if (!checkScalarVar(copyOfRange2)) {
            return false;
        }
        final PointAffine pointAffine = new PointAffine();
        if (!decodePointVar(array2, n2, true, pointAffine)) {
            return false;
        }
        final Digest digest = createDigest();
        final byte[] array4 = new byte[digest.getDigestSize()];
        dom2(digest, b, reduceScalar);
        digest.update(copyOfRange, 0, 32);
        digest.update(array2, n2, 32);
        digest.update(array3, n3, n4);
        digest.doFinal(array4, 0);
        reduceScalar = reduceScalar(array4);
        final int[] array5 = new int[8];
        decodeScalar(copyOfRange2, 0, array5);
        final int[] array6 = new int[8];
        decodeScalar(reduceScalar, 0, array6);
        final PointAccum pointAccum = new PointAccum();
        scalarMultStrausVar(array5, array6, pointAffine, pointAccum);
        array = new byte[32];
        boolean b3 = b2;
        if (encodePoint(pointAccum, array, 0) != 0) {
            b3 = b2;
            if (Arrays.areEqual(array, copyOfRange)) {
                b3 = true;
            }
        }
        return b3;
    }
    
    private static void pointAdd(final PointExt pointExt, final PointAccum pointAccum) {
        final int[] create = X25519Field.create();
        final int[] create2 = X25519Field.create();
        final int[] create3 = X25519Field.create();
        final int[] create4 = X25519Field.create();
        final int[] u = pointAccum.u;
        final int[] create5 = X25519Field.create();
        final int[] create6 = X25519Field.create();
        final int[] v = pointAccum.v;
        X25519Field.apm(pointAccum.y, pointAccum.x, create2, create);
        X25519Field.apm(pointExt.y, pointExt.x, create4, create3);
        X25519Field.mul(create, create3, create);
        X25519Field.mul(create2, create4, create2);
        X25519Field.mul(pointAccum.u, pointAccum.v, create3);
        X25519Field.mul(create3, pointExt.t, create3);
        X25519Field.mul(create3, Ed25519.C_d2, create3);
        X25519Field.mul(pointAccum.z, pointExt.z, create4);
        X25519Field.add(create4, create4, create4);
        X25519Field.apm(create2, create, v, u);
        X25519Field.apm(create4, create3, create6, create5);
        X25519Field.carry(create6);
        X25519Field.mul(u, create5, pointAccum.x);
        X25519Field.mul(create6, v, pointAccum.y);
        X25519Field.mul(create5, create6, pointAccum.z);
    }
    
    private static void pointAdd(final PointExt pointExt, final PointExt pointExt2) {
        final int[] create = X25519Field.create();
        final int[] create2 = X25519Field.create();
        final int[] create3 = X25519Field.create();
        final int[] create4 = X25519Field.create();
        final int[] create5 = X25519Field.create();
        final int[] create6 = X25519Field.create();
        final int[] create7 = X25519Field.create();
        final int[] create8 = X25519Field.create();
        X25519Field.apm(pointExt.y, pointExt.x, create2, create);
        X25519Field.apm(pointExt2.y, pointExt2.x, create4, create3);
        X25519Field.mul(create, create3, create);
        X25519Field.mul(create2, create4, create2);
        X25519Field.mul(pointExt.t, pointExt2.t, create3);
        X25519Field.mul(create3, Ed25519.C_d2, create3);
        X25519Field.mul(pointExt.z, pointExt2.z, create4);
        X25519Field.add(create4, create4, create4);
        X25519Field.apm(create2, create, create8, create5);
        X25519Field.apm(create4, create3, create7, create6);
        X25519Field.carry(create7);
        X25519Field.mul(create5, create6, pointExt2.x);
        X25519Field.mul(create7, create8, pointExt2.y);
        X25519Field.mul(create6, create7, pointExt2.z);
        X25519Field.mul(create5, create8, pointExt2.t);
    }
    
    private static void pointAddPrecomp(final PointPrecomp pointPrecomp, final PointAccum pointAccum) {
        final int[] create = X25519Field.create();
        final int[] create2 = X25519Field.create();
        final int[] create3 = X25519Field.create();
        final int[] u = pointAccum.u;
        final int[] create4 = X25519Field.create();
        final int[] create5 = X25519Field.create();
        final int[] v = pointAccum.v;
        X25519Field.apm(pointAccum.y, pointAccum.x, create2, create);
        X25519Field.mul(create, pointPrecomp.ymx_h, create);
        X25519Field.mul(create2, pointPrecomp.ypx_h, create2);
        X25519Field.mul(pointAccum.u, pointAccum.v, create3);
        X25519Field.mul(create3, pointPrecomp.xyd, create3);
        X25519Field.apm(create2, create, v, u);
        X25519Field.apm(pointAccum.z, create3, create5, create4);
        X25519Field.carry(create5);
        X25519Field.mul(u, create4, pointAccum.x);
        X25519Field.mul(create5, v, pointAccum.y);
        X25519Field.mul(create4, create5, pointAccum.z);
    }
    
    private static void pointAddVar(final boolean b, final PointExt pointExt, final PointAccum pointAccum) {
        final int[] create = X25519Field.create();
        final int[] create2 = X25519Field.create();
        final int[] create3 = X25519Field.create();
        final int[] create4 = X25519Field.create();
        final int[] u = pointAccum.u;
        final int[] create5 = X25519Field.create();
        final int[] create6 = X25519Field.create();
        final int[] v = pointAccum.v;
        int[] array2;
        int[] array3;
        int[] array4;
        int[] array5;
        if (b) {
            final int[] array = create3;
            array2 = create4;
            array3 = create5;
            array4 = create6;
            array5 = array;
        }
        else {
            final int[] array6 = create3;
            array5 = create4;
            array4 = create5;
            array3 = create6;
            array2 = array6;
        }
        X25519Field.apm(pointAccum.y, pointAccum.x, create2, create);
        X25519Field.apm(pointExt.y, pointExt.x, array5, array2);
        X25519Field.mul(create, create3, create);
        X25519Field.mul(create2, create4, create2);
        X25519Field.mul(pointAccum.u, pointAccum.v, create3);
        X25519Field.mul(create3, pointExt.t, create3);
        X25519Field.mul(create3, Ed25519.C_d2, create3);
        X25519Field.mul(pointAccum.z, pointExt.z, create4);
        X25519Field.add(create4, create4, create4);
        X25519Field.apm(create2, create, v, u);
        X25519Field.apm(create4, create3, array3, array4);
        X25519Field.carry(array3);
        X25519Field.mul(u, create5, pointAccum.x);
        X25519Field.mul(create6, v, pointAccum.y);
        X25519Field.mul(create5, create6, pointAccum.z);
    }
    
    private static void pointAddVar(final boolean b, final PointExt pointExt, final PointExt pointExt2, final PointExt pointExt3) {
        final int[] create = X25519Field.create();
        final int[] create2 = X25519Field.create();
        final int[] create3 = X25519Field.create();
        final int[] create4 = X25519Field.create();
        final int[] create5 = X25519Field.create();
        final int[] create6 = X25519Field.create();
        final int[] create7 = X25519Field.create();
        final int[] create8 = X25519Field.create();
        int[] array;
        int[] array2;
        int[] array3;
        int[] array4;
        if (b) {
            array = create3;
            array2 = create4;
            array3 = create6;
            array4 = create7;
        }
        else {
            array2 = create3;
            final int[] array5 = create4;
            array4 = create6;
            array3 = create7;
            array = array5;
        }
        X25519Field.apm(pointExt.y, pointExt.x, create2, create);
        X25519Field.apm(pointExt2.y, pointExt2.x, array, array2);
        X25519Field.mul(create, create3, create);
        X25519Field.mul(create2, create4, create2);
        X25519Field.mul(pointExt.t, pointExt2.t, create3);
        X25519Field.mul(create3, Ed25519.C_d2, create3);
        X25519Field.mul(pointExt.z, pointExt2.z, create4);
        X25519Field.add(create4, create4, create4);
        X25519Field.apm(create2, create, create8, create5);
        X25519Field.apm(create4, create3, array3, array4);
        X25519Field.carry(array3);
        X25519Field.mul(create5, create6, pointExt3.x);
        X25519Field.mul(create7, create8, pointExt3.y);
        X25519Field.mul(create6, create7, pointExt3.z);
        X25519Field.mul(create5, create8, pointExt3.t);
    }
    
    private static PointExt pointCopy(final PointAccum pointAccum) {
        final PointExt pointExt = new PointExt();
        X25519Field.copy(pointAccum.x, 0, pointExt.x, 0);
        X25519Field.copy(pointAccum.y, 0, pointExt.y, 0);
        X25519Field.copy(pointAccum.z, 0, pointExt.z, 0);
        X25519Field.mul(pointAccum.u, pointAccum.v, pointExt.t);
        return pointExt;
    }
    
    private static PointExt pointCopy(final PointAffine pointAffine) {
        final PointExt pointExt = new PointExt();
        X25519Field.copy(pointAffine.x, 0, pointExt.x, 0);
        X25519Field.copy(pointAffine.y, 0, pointExt.y, 0);
        pointExtendXY(pointExt);
        return pointExt;
    }
    
    private static PointExt pointCopy(final PointExt pointExt) {
        final PointExt pointExt2 = new PointExt();
        pointCopy(pointExt, pointExt2);
        return pointExt2;
    }
    
    private static void pointCopy(final PointAffine pointAffine, final PointAccum pointAccum) {
        X25519Field.copy(pointAffine.x, 0, pointAccum.x, 0);
        X25519Field.copy(pointAffine.y, 0, pointAccum.y, 0);
        pointExtendXY(pointAccum);
    }
    
    private static void pointCopy(final PointExt pointExt, final PointExt pointExt2) {
        X25519Field.copy(pointExt.x, 0, pointExt2.x, 0);
        X25519Field.copy(pointExt.y, 0, pointExt2.y, 0);
        X25519Field.copy(pointExt.z, 0, pointExt2.z, 0);
        X25519Field.copy(pointExt.t, 0, pointExt2.t, 0);
    }
    
    private static void pointDouble(final PointAccum pointAccum) {
        final int[] create = X25519Field.create();
        final int[] create2 = X25519Field.create();
        final int[] create3 = X25519Field.create();
        final int[] u = pointAccum.u;
        final int[] create4 = X25519Field.create();
        final int[] create5 = X25519Field.create();
        final int[] v = pointAccum.v;
        X25519Field.sqr(pointAccum.x, create);
        X25519Field.sqr(pointAccum.y, create2);
        X25519Field.sqr(pointAccum.z, create3);
        X25519Field.add(create3, create3, create3);
        X25519Field.apm(create, create2, v, create5);
        X25519Field.add(pointAccum.x, pointAccum.y, u);
        X25519Field.sqr(u, u);
        X25519Field.sub(v, u, u);
        X25519Field.add(create3, create5, create4);
        X25519Field.carry(create4);
        X25519Field.mul(u, create4, pointAccum.x);
        X25519Field.mul(create5, v, pointAccum.y);
        X25519Field.mul(create4, create5, pointAccum.z);
    }
    
    private static void pointExtendXY(final PointAccum pointAccum) {
        X25519Field.one(pointAccum.z);
        X25519Field.copy(pointAccum.x, 0, pointAccum.u, 0);
        X25519Field.copy(pointAccum.y, 0, pointAccum.v, 0);
    }
    
    private static void pointExtendXY(final PointExt pointExt) {
        X25519Field.one(pointExt.z);
        X25519Field.mul(pointExt.x, pointExt.y, pointExt.t);
    }
    
    private static void pointLookup(int i, final int n, final PointPrecomp pointPrecomp) {
        int n2 = i * 8 * 3 * 10;
        int n3;
        int n4;
        int n5;
        for (i = 0; i < 8; ++i) {
            n3 = (i ^ n) - 1 >> 31;
            X25519Field.cmov(n3, Ed25519.precompBase, n2, pointPrecomp.ypx_h, 0);
            n4 = n2 + 10;
            X25519Field.cmov(n3, Ed25519.precompBase, n4, pointPrecomp.ymx_h, 0);
            n5 = n4 + 10;
            X25519Field.cmov(n3, Ed25519.precompBase, n5, pointPrecomp.xyd, 0);
            n2 = n5 + 10;
        }
    }
    
    private static void pointLookup(final int[] array, int n, final PointExt pointExt) {
        n *= 40;
        X25519Field.copy(array, n, pointExt.x, 0);
        n += 10;
        X25519Field.copy(array, n, pointExt.y, 0);
        n += 10;
        X25519Field.copy(array, n, pointExt.z, 0);
        X25519Field.copy(array, n + 10, pointExt.t, 0);
    }
    
    private static void pointLookup(final int[] array, int i, final int[] array2, final PointExt pointExt) {
        final int window4 = getWindow4(array, i);
        final int n = window4 >>> 3 ^ 0x1;
        final int n2 = -n;
        int n3;
        int n4;
        int n5;
        int n6;
        int n7;
        for (i = (n3 = 0); i < 8; ++i) {
            n4 = (i ^ ((window4 ^ n2) & 0x7)) - 1 >> 31;
            X25519Field.cmov(n4, array2, n3, pointExt.x, 0);
            n5 = n3 + 10;
            X25519Field.cmov(n4, array2, n5, pointExt.y, 0);
            n6 = n5 + 10;
            X25519Field.cmov(n4, array2, n6, pointExt.z, 0);
            n7 = n6 + 10;
            X25519Field.cmov(n4, array2, n7, pointExt.t, 0);
            n3 = n7 + 10;
        }
        X25519Field.cnegate(n, pointExt.x);
        X25519Field.cnegate(n, pointExt.t);
    }
    
    private static int[] pointPrecomp(final PointAffine pointAffine, final int n) {
        final PointExt pointCopy = pointCopy(pointAffine);
        final PointExt pointCopy2 = pointCopy(pointCopy);
        pointAdd(pointCopy, pointCopy2);
        final int[] table = X25519Field.createTable(n * 4);
        int n3;
        int n2 = n3 = 0;
        while (true) {
            X25519Field.copy(pointCopy.x, 0, table, n2);
            final int n4 = n2 + 10;
            X25519Field.copy(pointCopy.y, 0, table, n4);
            final int n5 = n4 + 10;
            X25519Field.copy(pointCopy.z, 0, table, n5);
            final int n6 = n5 + 10;
            X25519Field.copy(pointCopy.t, 0, table, n6);
            n2 = n6 + 10;
            ++n3;
            if (n3 == n) {
                break;
            }
            pointAdd(pointCopy2, pointCopy);
        }
        return table;
    }
    
    private static PointExt[] pointPrecompVar(PointExt pointExt, final int n) {
        final PointExt pointExt2 = new PointExt();
        pointAddVar(false, pointExt, pointExt, pointExt2);
        final PointExt[] array = new PointExt[n];
        array[0] = pointCopy(pointExt);
        for (int i = 1; i < n; ++i) {
            pointExt = array[i - 1];
            pointAddVar(false, pointExt, pointExt2, array[i] = new PointExt());
        }
        return array;
    }
    
    private static void pointSetNeutral(final PointAccum pointAccum) {
        X25519Field.zero(pointAccum.x);
        X25519Field.one(pointAccum.y);
        X25519Field.one(pointAccum.z);
        X25519Field.zero(pointAccum.u);
        X25519Field.one(pointAccum.v);
    }
    
    private static void pointSetNeutral(final PointExt pointExt) {
        X25519Field.zero(pointExt.x);
        X25519Field.one(pointExt.y);
        X25519Field.one(pointExt.z);
        X25519Field.zero(pointExt.t);
    }
    
    public static void precompute() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: astore          6
        //     5: aload           6
        //     7: monitorenter   
        //     8: getstatic       org/bouncycastle/math/ec/rfc8032/Ed25519.precompBase:[I
        //    11: ifnull          18
        //    14: aload           6
        //    16: monitorexit    
        //    17: return         
        //    18: new             Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointExt;
        //    21: dup            
        //    22: aconst_null    
        //    23: invokespecial   org/bouncycastle/math/ec/rfc8032/Ed25519$PointExt.<init>:(Lorg/bouncycastle/math/ec/rfc8032/Ed25519$1;)V
        //    26: astore          7
        //    28: getstatic       org/bouncycastle/math/ec/rfc8032/Ed25519.B_x:[I
        //    31: iconst_0       
        //    32: aload           7
        //    34: getfield        org/bouncycastle/math/ec/rfc8032/Ed25519$PointExt.x:[I
        //    37: iconst_0       
        //    38: invokestatic    org/bouncycastle/math/ec/rfc7748/X25519Field.copy:([II[II)V
        //    41: getstatic       org/bouncycastle/math/ec/rfc8032/Ed25519.B_y:[I
        //    44: iconst_0       
        //    45: aload           7
        //    47: getfield        org/bouncycastle/math/ec/rfc8032/Ed25519$PointExt.y:[I
        //    50: iconst_0       
        //    51: invokestatic    org/bouncycastle/math/ec/rfc7748/X25519Field.copy:([II[II)V
        //    54: aload           7
        //    56: invokestatic    org/bouncycastle/math/ec/rfc8032/Ed25519.pointExtendXY:(Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointExt;)V
        //    59: aload           7
        //    61: bipush          32
        //    63: invokestatic    org/bouncycastle/math/ec/rfc8032/Ed25519.pointPrecompVar:(Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointExt;I)[Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointExt;
        //    66: putstatic       org/bouncycastle/math/ec/rfc8032/Ed25519.precompBaseTable:[Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointExt;
        //    69: new             Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointAccum;
        //    72: dup            
        //    73: aconst_null    
        //    74: invokespecial   org/bouncycastle/math/ec/rfc8032/Ed25519$PointAccum.<init>:(Lorg/bouncycastle/math/ec/rfc8032/Ed25519$1;)V
        //    77: astore          7
        //    79: getstatic       org/bouncycastle/math/ec/rfc8032/Ed25519.B_x:[I
        //    82: iconst_0       
        //    83: aload           7
        //    85: getfield        org/bouncycastle/math/ec/rfc8032/Ed25519$PointAccum.x:[I
        //    88: iconst_0       
        //    89: invokestatic    org/bouncycastle/math/ec/rfc7748/X25519Field.copy:([II[II)V
        //    92: getstatic       org/bouncycastle/math/ec/rfc8032/Ed25519.B_y:[I
        //    95: iconst_0       
        //    96: aload           7
        //    98: getfield        org/bouncycastle/math/ec/rfc8032/Ed25519$PointAccum.y:[I
        //   101: iconst_0       
        //   102: invokestatic    org/bouncycastle/math/ec/rfc7748/X25519Field.copy:([II[II)V
        //   105: aload           7
        //   107: invokestatic    org/bouncycastle/math/ec/rfc8032/Ed25519.pointExtendXY:(Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointAccum;)V
        //   110: sipush          192
        //   113: invokestatic    org/bouncycastle/math/ec/rfc7748/X25519Field.createTable:(I)[I
        //   116: putstatic       org/bouncycastle/math/ec/rfc8032/Ed25519.precompBase:[I
        //   119: iconst_0       
        //   120: istore_2       
        //   121: iload_2        
        //   122: istore_0       
        //   123: iload_2        
        //   124: bipush          8
        //   126: if_icmpge       493
        //   129: iconst_4       
        //   130: anewarray       Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointExt;
        //   133: astore          9
        //   135: new             Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointExt;
        //   138: dup            
        //   139: aconst_null    
        //   140: invokespecial   org/bouncycastle/math/ec/rfc8032/Ed25519$PointExt.<init>:(Lorg/bouncycastle/math/ec/rfc8032/Ed25519$1;)V
        //   143: astore          10
        //   145: aload           10
        //   147: invokestatic    org/bouncycastle/math/ec/rfc8032/Ed25519.pointSetNeutral:(Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointExt;)V
        //   150: iconst_0       
        //   151: istore_1       
        //   152: iconst_1       
        //   153: istore_3       
        //   154: iload_1        
        //   155: iconst_4       
        //   156: if_icmpge       212
        //   159: iconst_1       
        //   160: aload           10
        //   162: aload           7
        //   164: invokestatic    org/bouncycastle/math/ec/rfc8032/Ed25519.pointCopy:(Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointAccum;)Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointExt;
        //   167: aload           10
        //   169: invokestatic    org/bouncycastle/math/ec/rfc8032/Ed25519.pointAddVar:(ZLorg/bouncycastle/math/ec/rfc8032/Ed25519$PointExt;Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointExt;Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointExt;)V
        //   172: aload           7
        //   174: invokestatic    org/bouncycastle/math/ec/rfc8032/Ed25519.pointDouble:(Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointAccum;)V
        //   177: aload           9
        //   179: iload_1        
        //   180: aload           7
        //   182: invokestatic    org/bouncycastle/math/ec/rfc8032/Ed25519.pointCopy:(Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointAccum;)Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointExt;
        //   185: aastore        
        //   186: iload_2        
        //   187: iload_1        
        //   188: iadd           
        //   189: bipush          10
        //   191: if_icmpeq       505
        //   194: iload_3        
        //   195: bipush          8
        //   197: if_icmpge       505
        //   200: aload           7
        //   202: invokestatic    org/bouncycastle/math/ec/rfc8032/Ed25519.pointDouble:(Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointAccum;)V
        //   205: iload_3        
        //   206: iconst_1       
        //   207: iadd           
        //   208: istore_3       
        //   209: goto            194
        //   212: bipush          8
        //   214: anewarray       Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointExt;
        //   217: astore          8
        //   219: aload           8
        //   221: iconst_0       
        //   222: aload           10
        //   224: aastore        
        //   225: iconst_0       
        //   226: istore_3       
        //   227: iconst_1       
        //   228: istore_1       
        //   229: goto            512
        //   232: iload           4
        //   234: iload           5
        //   236: if_icmpge       528
        //   239: aload           8
        //   241: iload_1        
        //   242: iload           5
        //   244: isub           
        //   245: aaload         
        //   246: astore          10
        //   248: aload           9
        //   250: iload_3        
        //   251: aaload         
        //   252: astore          11
        //   254: new             Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointExt;
        //   257: dup            
        //   258: aconst_null    
        //   259: invokespecial   org/bouncycastle/math/ec/rfc8032/Ed25519$PointExt.<init>:(Lorg/bouncycastle/math/ec/rfc8032/Ed25519$1;)V
        //   262: astore          12
        //   264: aload           8
        //   266: iload_1        
        //   267: aload           12
        //   269: aastore        
        //   270: iconst_0       
        //   271: aload           10
        //   273: aload           11
        //   275: aload           12
        //   277: invokestatic    org/bouncycastle/math/ec/rfc8032/Ed25519.pointAddVar:(ZLorg/bouncycastle/math/ec/rfc8032/Ed25519$PointExt;Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointExt;Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointExt;)V
        //   280: iload           4
        //   282: iconst_1       
        //   283: iadd           
        //   284: istore          4
        //   286: iload_1        
        //   287: iconst_1       
        //   288: iadd           
        //   289: istore_1       
        //   290: goto            232
        //   293: iload_1        
        //   294: bipush          8
        //   296: if_icmpge       540
        //   299: aload           8
        //   301: iload_1        
        //   302: aaload         
        //   303: astore          11
        //   305: invokestatic    org/bouncycastle/math/ec/rfc7748/X25519Field.create:()[I
        //   308: astore          9
        //   310: invokestatic    org/bouncycastle/math/ec/rfc7748/X25519Field.create:()[I
        //   313: astore          10
        //   315: aload           11
        //   317: getfield        org/bouncycastle/math/ec/rfc8032/Ed25519$PointExt.z:[I
        //   320: aload           11
        //   322: getfield        org/bouncycastle/math/ec/rfc8032/Ed25519$PointExt.z:[I
        //   325: aload           9
        //   327: invokestatic    org/bouncycastle/math/ec/rfc7748/X25519Field.add:([I[I[I)V
        //   330: aload           9
        //   332: aload           10
        //   334: invokestatic    org/bouncycastle/math/ec/rfc7748/X25519Field.inv:([I[I)V
        //   337: aload           11
        //   339: getfield        org/bouncycastle/math/ec/rfc8032/Ed25519$PointExt.x:[I
        //   342: aload           10
        //   344: aload           9
        //   346: invokestatic    org/bouncycastle/math/ec/rfc7748/X25519Field.mul:([I[I[I)V
        //   349: aload           11
        //   351: getfield        org/bouncycastle/math/ec/rfc8032/Ed25519$PointExt.y:[I
        //   354: aload           10
        //   356: aload           10
        //   358: invokestatic    org/bouncycastle/math/ec/rfc7748/X25519Field.mul:([I[I[I)V
        //   361: new             Lorg/bouncycastle/math/ec/rfc8032/Ed25519$PointPrecomp;
        //   364: dup            
        //   365: aconst_null    
        //   366: invokespecial   org/bouncycastle/math/ec/rfc8032/Ed25519$PointPrecomp.<init>:(Lorg/bouncycastle/math/ec/rfc8032/Ed25519$1;)V
        //   369: astore          11
        //   371: aload           10
        //   373: aload           9
        //   375: aload           11
        //   377: getfield        org/bouncycastle/math/ec/rfc8032/Ed25519$PointPrecomp.ypx_h:[I
        //   380: aload           11
        //   382: getfield        org/bouncycastle/math/ec/rfc8032/Ed25519$PointPrecomp.ymx_h:[I
        //   385: invokestatic    org/bouncycastle/math/ec/rfc7748/X25519Field.apm:([I[I[I[I)V
        //   388: aload           9
        //   390: aload           10
        //   392: aload           11
        //   394: getfield        org/bouncycastle/math/ec/rfc8032/Ed25519$PointPrecomp.xyd:[I
        //   397: invokestatic    org/bouncycastle/math/ec/rfc7748/X25519Field.mul:([I[I[I)V
        //   400: aload           11
        //   402: getfield        org/bouncycastle/math/ec/rfc8032/Ed25519$PointPrecomp.xyd:[I
        //   405: getstatic       org/bouncycastle/math/ec/rfc8032/Ed25519.C_d4:[I
        //   408: aload           11
        //   410: getfield        org/bouncycastle/math/ec/rfc8032/Ed25519$PointPrecomp.xyd:[I
        //   413: invokestatic    org/bouncycastle/math/ec/rfc7748/X25519Field.mul:([I[I[I)V
        //   416: aload           11
        //   418: getfield        org/bouncycastle/math/ec/rfc8032/Ed25519$PointPrecomp.ypx_h:[I
        //   421: invokestatic    org/bouncycastle/math/ec/rfc7748/X25519Field.normalize:([I)V
        //   424: aload           11
        //   426: getfield        org/bouncycastle/math/ec/rfc8032/Ed25519$PointPrecomp.ymx_h:[I
        //   429: invokestatic    org/bouncycastle/math/ec/rfc7748/X25519Field.normalize:([I)V
        //   432: aload           11
        //   434: getfield        org/bouncycastle/math/ec/rfc8032/Ed25519$PointPrecomp.ypx_h:[I
        //   437: iconst_0       
        //   438: getstatic       org/bouncycastle/math/ec/rfc8032/Ed25519.precompBase:[I
        //   441: iload_0        
        //   442: invokestatic    org/bouncycastle/math/ec/rfc7748/X25519Field.copy:([II[II)V
        //   445: iload_0        
        //   446: bipush          10
        //   448: iadd           
        //   449: istore_0       
        //   450: aload           11
        //   452: getfield        org/bouncycastle/math/ec/rfc8032/Ed25519$PointPrecomp.ymx_h:[I
        //   455: iconst_0       
        //   456: getstatic       org/bouncycastle/math/ec/rfc8032/Ed25519.precompBase:[I
        //   459: iload_0        
        //   460: invokestatic    org/bouncycastle/math/ec/rfc7748/X25519Field.copy:([II[II)V
        //   463: iload_0        
        //   464: bipush          10
        //   466: iadd           
        //   467: istore_0       
        //   468: aload           11
        //   470: getfield        org/bouncycastle/math/ec/rfc8032/Ed25519$PointPrecomp.xyd:[I
        //   473: iconst_0       
        //   474: getstatic       org/bouncycastle/math/ec/rfc8032/Ed25519.precompBase:[I
        //   477: iload_0        
        //   478: invokestatic    org/bouncycastle/math/ec/rfc7748/X25519Field.copy:([II[II)V
        //   481: iload_0        
        //   482: bipush          10
        //   484: iadd           
        //   485: istore_0       
        //   486: iload_1        
        //   487: iconst_1       
        //   488: iadd           
        //   489: istore_1       
        //   490: goto            293
        //   493: aload           6
        //   495: monitorexit    
        //   496: return         
        //   497: astore          7
        //   499: aload           6
        //   501: monitorexit    
        //   502: aload           7
        //   504: athrow         
        //   505: iload_1        
        //   506: iconst_1       
        //   507: iadd           
        //   508: istore_1       
        //   509: goto            152
        //   512: iload_3        
        //   513: iconst_3       
        //   514: if_icmpge       535
        //   517: iconst_1       
        //   518: iload_3        
        //   519: ishl           
        //   520: istore          5
        //   522: iconst_0       
        //   523: istore          4
        //   525: goto            232
        //   528: iload_3        
        //   529: iconst_1       
        //   530: iadd           
        //   531: istore_3       
        //   532: goto            512
        //   535: iconst_0       
        //   536: istore_1       
        //   537: goto            293
        //   540: iload_2        
        //   541: iconst_1       
        //   542: iadd           
        //   543: istore_2       
        //   544: goto            123
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  8      17     497    505    Any
        //  18     119    497    505    Any
        //  129    150    497    505    Any
        //  159    186    497    505    Any
        //  200    205    497    505    Any
        //  212    219    497    505    Any
        //  254    264    497    505    Any
        //  270    280    497    505    Any
        //  305    445    497    505    Any
        //  450    463    497    505    Any
        //  468    481    497    505    Any
        //  493    496    497    505    Any
        //  499    502    497    505    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static void pruneScalar(final byte[] array, final int n, final byte[] array2) {
        System.arraycopy(array, n, array2, 0, 32);
        array2[0] &= (byte)248;
        array2[31] &= 0x7F;
        array2[31] |= 0x40;
    }
    
    private static byte[] reduceScalar(byte[] array) {
        final long n = decode32(array, 0);
        final long n2 = decode24(array, 4) << 4;
        final long n3 = decode32(array, 7);
        final long n4 = decode24(array, 11) << 4;
        final long n5 = decode32(array, 14);
        final long n6 = decode24(array, 18) << 4;
        final long n7 = decode32(array, 21);
        final long n8 = decode24(array, 25) << 4;
        final long n9 = decode32(array, 28);
        final long n10 = decode24(array, 32) << 4;
        final long n11 = decode32(array, 35);
        final long n12 = decode24(array, 39) << 4;
        final long n13 = decode32(array, 42);
        final long n14 = decode24(array, 46) << 4;
        final long n15 = (long)decode32(array, 49) & 0xFFFFFFFFL;
        final long n16 = decode24(array, 53) << 4;
        final long n17 = (long)decode32(array, 56) & 0xFFFFFFFFL;
        final long n18 = decode24(array, 60) << 4;
        final long n19 = (long)array[63] & 0xFFL;
        final long n20 = (n18 & 0xFFFFFFFFL) + (n17 >> 28);
        final long n21 = n17 & 0xFFFFFFFL;
        final long n22 = (n13 & 0xFFFFFFFFL) - n19 * -6428113L - n20 * 5343L;
        final long n23 = (n12 & 0xFFFFFFFFL) - n19 * 127719000L - n20 * -6428113L - n21 * 5343L;
        final long n24 = (n16 & 0xFFFFFFFFL) + (n15 >> 28);
        final long n25 = n15 & 0xFFFFFFFL;
        final long n26 = (n11 & 0xFFFFFFFFL) - n19 * 19280294L - n20 * 127719000L - n21 * -6428113L - n24 * 5343L;
        final long n27 = (n10 & 0xFFFFFFFFL) - n19 * -50998291L - n20 * 19280294L - n21 * 127719000L - n24 * -6428113L - n25 * 5343L;
        final long n28 = (n14 & 0xFFFFFFFFL) - n19 * 5343L + (n22 >> 28);
        final long n29 = (n22 & 0xFFFFFFFL) + (n23 >> 28);
        final long n30 = (n8 & 0xFFFFFFFFL) - n21 * -50998291L - n24 * 19280294L - n25 * 127719000L - n28 * -6428113L - n29 * 5343L;
        final long n31 = (n23 & 0xFFFFFFFL) + (n26 >> 28);
        final long n32 = (n26 & 0xFFFFFFFL) + (n27 >> 28);
        final long n33 = (n9 & 0xFFFFFFFFL) - n20 * -50998291L - n21 * 19280294L - n24 * 127719000L - n25 * -6428113L - n28 * 5343L + (n30 >> 28);
        final long n34 = n33 & 0xFFFFFFFL;
        final long n35 = n34 >>> 27;
        final long n36 = (n27 & 0xFFFFFFFL) + (n33 >> 28) + n35;
        final long n37 = (n & 0xFFFFFFFFL) - n36 * -50998291L;
        final long n38 = (n2 & 0xFFFFFFFFL) - n32 * -50998291L - n36 * 19280294L + (n37 >> 28);
        final long n39 = (n3 & 0xFFFFFFFFL) - n31 * -50998291L - n32 * 19280294L - n36 * 127719000L + (n38 >> 28);
        final long n40 = (n4 & 0xFFFFFFFFL) - n29 * -50998291L - n31 * 19280294L - n32 * 127719000L - n36 * -6428113L + (n39 >> 28);
        final long n41 = (n5 & 0xFFFFFFFFL) - n28 * -50998291L - n29 * 19280294L - n31 * 127719000L - n32 * -6428113L - n36 * 5343L + (n40 >> 28);
        final long n42 = (n6 & 0xFFFFFFFFL) - n25 * -50998291L - n28 * 19280294L - n29 * 127719000L - n31 * -6428113L - n32 * 5343L + (n41 >> 28);
        final long n43 = (n7 & 0xFFFFFFFFL) - n24 * -50998291L - n25 * 19280294L - n28 * 127719000L - n29 * -6428113L - n31 * 5343L + (n42 >> 28);
        final long n44 = (n30 & 0xFFFFFFFL) + (n43 >> 28);
        final long n45 = n34 + (n44 >> 28);
        final long n46 = (n45 >> 28) - n35;
        final long n47 = (n37 & 0xFFFFFFFL) + (n46 & 0xFFFFFFFFFCF5D3EDL);
        final long n48 = (n38 & 0xFFFFFFFL) + (n46 & 0x12631A6L) + (n47 >> 28);
        final long n49 = (n39 & 0xFFFFFFFL) + (n46 & 0x79CD658L) + (n48 >> 28);
        final long n50 = (n40 & 0xFFFFFFFL) + (n46 & 0xFFFFFFFFFF9DEA2FL) + (n49 >> 28);
        final long n51 = (n41 & 0xFFFFFFFL) + (n46 & 0x14DFL) + (n50 >> 28);
        final long n52 = (n42 & 0xFFFFFFFL) + (n51 >> 28);
        final long n53 = (n43 & 0xFFFFFFFL) + (n52 >> 28);
        final long n54 = (n44 & 0xFFFFFFFL) + (n53 >> 28);
        array = new byte[32];
        encode56((n47 & 0xFFFFFFFL) | (n48 & 0xFFFFFFFL) << 28, array, 0);
        encode56((n50 & 0xFFFFFFFL) << 28 | (n49 & 0xFFFFFFFL), array, 7);
        encode56((n51 & 0xFFFFFFFL) | (n52 & 0xFFFFFFFL) << 28, array, 14);
        encode56((n53 & 0xFFFFFFFL) | (n54 & 0xFFFFFFFL) << 28, array, 21);
        encode32((int)((n45 & 0xFFFFFFFL) + (n54 >> 28)), array, 28);
        return array;
    }
    
    private static void scalarMult(final byte[] array, final PointAffine pointAffine, final PointAccum pointAccum) {
        precompute();
        final int[] array2 = new int[8];
        decodeScalar(array, 0, array2);
        Nat.shiftDownBits(8, array2, 3, 1);
        Nat.cadd(8, 0x1 & array2[0], array2, Ed25519.L, array2);
        Nat.shiftDownBit(8, array2, 0);
        pointCopy(pointAffine, pointAccum);
        final int[] pointPrecomp = pointPrecomp(pointAffine, 8);
        final PointExt pointExt = new PointExt();
        pointLookup(pointPrecomp, 7, pointExt);
        pointAdd(pointExt, pointAccum);
        int n = 62;
        while (true) {
            pointLookup(array2, n, pointPrecomp, pointExt);
            pointAdd(pointExt, pointAccum);
            pointDouble(pointAccum);
            pointDouble(pointAccum);
            pointDouble(pointAccum);
            --n;
            if (n < 0) {
                break;
            }
            pointDouble(pointAccum);
        }
    }
    
    private static void scalarMultBase(final byte[] array, final PointAccum pointAccum) {
        precompute();
        pointSetNeutral(pointAccum);
        final int[] array2 = new int[8];
        decodeScalar(array, 0, array2);
        Nat.cadd(8, array2[0] & 0x1, array2, Ed25519.L, array2);
        Nat.shiftDownBit(8, array2, 1);
        for (int i = 0; i < 8; ++i) {
            array2[i] = Interleave.shuffle2(array2[i]);
        }
        final PointPrecomp pointPrecomp = new PointPrecomp();
        int n = 28;
        while (true) {
            for (int j = 0; j < 8; ++j) {
                final int n2 = array2[j] >>> n;
                final int n3 = n2 >>> 3 & 0x1;
                pointLookup(j, (n2 ^ -n3) & 0x7, pointPrecomp);
                X25519Field.cswap(n3, pointPrecomp.ypx_h, pointPrecomp.ymx_h);
                X25519Field.cnegate(n3, pointPrecomp.xyd);
                pointAddPrecomp(pointPrecomp, pointAccum);
            }
            n -= 4;
            if (n < 0) {
                break;
            }
            pointDouble(pointAccum);
        }
    }
    
    private static void scalarMultBaseEncoded(final byte[] array, final byte[] array2, final int n) {
        final PointAccum pointAccum = new PointAccum();
        scalarMultBase(array, pointAccum);
        if (encodePoint(pointAccum, array2, n) != 0) {
            return;
        }
        throw new IllegalStateException();
    }
    
    public static void scalarMultBaseYZ(final X25519.Friend friend, final byte[] array, final int n, final int[] array2, final int[] array3) {
        if (friend == null) {
            throw new NullPointerException("This method is only for use by X25519");
        }
        final byte[] array4 = new byte[32];
        pruneScalar(array, n, array4);
        final PointAccum pointAccum = new PointAccum();
        scalarMultBase(array4, pointAccum);
        if (checkPoint(pointAccum.x, pointAccum.y, pointAccum.z) != 0) {
            X25519Field.copy(pointAccum.y, 0, array2, 0);
            X25519Field.copy(pointAccum.z, 0, array3, 0);
            return;
        }
        throw new IllegalStateException();
    }
    
    private static void scalarMultStrausVar(final int[] array, final int[] array2, final PointAffine pointAffine, final PointAccum pointAccum) {
        precompute();
        final byte[] wnaf = getWNAF(array, 7);
        final byte[] wnaf2 = getWNAF(array2, 5);
        final PointExt[] pointPrecompVar = pointPrecompVar(pointCopy(pointAffine), 8);
        pointSetNeutral(pointAccum);
        int n = 252;
        while (true) {
            final byte b = wnaf[n];
            final boolean b2 = false;
            if (b != 0) {
                final int n2 = b >> 31;
                pointAddVar(n2 != 0, Ed25519.precompBaseTable[(b ^ n2) >>> 1], pointAccum);
            }
            final byte b3 = wnaf2[n];
            if (b3 != 0) {
                final int n3 = b3 >> 31;
                boolean b4 = b2;
                if (n3 != 0) {
                    b4 = true;
                }
                pointAddVar(b4, pointPrecompVar[(b3 ^ n3) >>> 1], pointAccum);
            }
            --n;
            if (n < 0) {
                break;
            }
            pointDouble(pointAccum);
        }
    }
    
    public static void sign(final byte[] array, final int n, final byte[] array2, final int n2, final int n3, final byte[] array3, final int n4) {
        implSign(array, n, null, (byte)0, array2, n2, n3, array3, n4);
    }
    
    public static void sign(final byte[] array, final int n, final byte[] array2, final int n2, final byte[] array3, final int n3, final int n4, final byte[] array4, final int n5) {
        implSign(array, n, array2, n2, null, (byte)0, array3, n3, n4, array4, n5);
    }
    
    public static void sign(final byte[] array, final int n, final byte[] array2, final int n2, final byte[] array3, final byte[] array4, final int n3, final int n4, final byte[] array5, final int n5) {
        implSign(array, n, array2, n2, array3, (byte)0, array4, n3, n4, array5, n5);
    }
    
    public static void sign(final byte[] array, final int n, final byte[] array2, final byte[] array3, final int n2, final int n3, final byte[] array4, final int n4) {
        implSign(array, n, array2, (byte)0, array3, n2, n3, array4, n4);
    }
    
    public static void signPrehash(final byte[] array, final int n, final byte[] array2, final int n2, final byte[] array3, final Digest digest, final byte[] array4, final int n3) {
        final byte[] array5 = new byte[64];
        if (64 == digest.doFinal(array5, 0)) {
            implSign(array, n, array2, n2, array3, (byte)1, array5, 0, array5.length, array4, n3);
            return;
        }
        throw new IllegalArgumentException("ph");
    }
    
    public static void signPrehash(final byte[] array, final int n, final byte[] array2, final int n2, final byte[] array3, final byte[] array4, final int n3, final byte[] array5, final int n4) {
        implSign(array, n, array2, n2, array3, (byte)1, array4, n3, 64, array5, n4);
    }
    
    public static void signPrehash(final byte[] array, final int n, final byte[] array2, final Digest digest, final byte[] array3, final int n2) {
        final byte[] array4 = new byte[64];
        if (64 == digest.doFinal(array4, 0)) {
            implSign(array, n, array2, (byte)1, array4, 0, array4.length, array3, n2);
            return;
        }
        throw new IllegalArgumentException("ph");
    }
    
    public static void signPrehash(final byte[] array, final int n, final byte[] array2, final byte[] array3, final int n2, final byte[] array4, final int n3) {
        implSign(array, n, array2, (byte)1, array3, n2, 64, array4, n3);
    }
    
    public static boolean verify(final byte[] array, final int n, final byte[] array2, final int n2, final byte[] array3, final int n3, final int n4) {
        return implVerify(array, n, array2, n2, null, (byte)0, array3, n3, n4);
    }
    
    public static boolean verify(final byte[] array, final int n, final byte[] array2, final int n2, final byte[] array3, final byte[] array4, final int n3, final int n4) {
        return implVerify(array, n, array2, n2, array3, (byte)0, array4, n3, n4);
    }
    
    public static boolean verifyPrehash(final byte[] array, final int n, final byte[] array2, final int n2, final byte[] array3, final Digest digest) {
        final byte[] array4 = new byte[64];
        if (64 == digest.doFinal(array4, 0)) {
            return implVerify(array, n, array2, n2, array3, (byte)1, array4, 0, array4.length);
        }
        throw new IllegalArgumentException("ph");
    }
    
    public static boolean verifyPrehash(final byte[] array, final int n, final byte[] array2, final int n2, final byte[] array3, final byte[] array4, final int n3) {
        return implVerify(array, n, array2, n2, array3, (byte)1, array4, n3, 64);
    }
    
    public static final class Algorithm
    {
        public static final int Ed25519 = 0;
        public static final int Ed25519ctx = 1;
        public static final int Ed25519ph = 2;
    }
    
    private static class PointAccum
    {
        int[] u;
        int[] v;
        int[] x;
        int[] y;
        int[] z;
        
        private PointAccum() {
            this.x = X25519Field.create();
            this.y = X25519Field.create();
            this.z = X25519Field.create();
            this.u = X25519Field.create();
            this.v = X25519Field.create();
        }
    }
    
    private static class PointAffine
    {
        int[] x;
        int[] y;
        
        private PointAffine() {
            this.x = X25519Field.create();
            this.y = X25519Field.create();
        }
    }
    
    private static class PointExt
    {
        int[] t;
        int[] x;
        int[] y;
        int[] z;
        
        private PointExt() {
            this.x = X25519Field.create();
            this.y = X25519Field.create();
            this.z = X25519Field.create();
            this.t = X25519Field.create();
        }
    }
    
    private static class PointPrecomp
    {
        int[] xyd;
        int[] ymx_h;
        int[] ypx_h;
        
        private PointPrecomp() {
            this.ypx_h = X25519Field.create();
            this.ymx_h = X25519Field.create();
            this.xyd = X25519Field.create();
        }
    }
}
