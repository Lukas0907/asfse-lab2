// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.rfc8032;

import org.bouncycastle.math.ec.rfc7748.X448;
import java.security.SecureRandom;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.digests.SHAKEDigest;
import org.bouncycastle.crypto.Xof;
import org.bouncycastle.math.ec.rfc7748.X448Field;
import org.bouncycastle.math.raw.Nat;
import org.bouncycastle.util.Strings;

public abstract class Ed448
{
    private static final int[] B_x;
    private static final int[] B_y;
    private static final int C_d = -39081;
    private static final byte[] DOM4_PREFIX;
    private static final int[] L;
    private static final int L4_0 = 43969588;
    private static final int L4_1 = 30366549;
    private static final int L4_2 = 163752818;
    private static final int L4_3 = 258169998;
    private static final int L4_4 = 96434764;
    private static final int L4_5 = 227822194;
    private static final int L4_6 = 149865618;
    private static final int L4_7 = 550336261;
    private static final int L_0 = 78101261;
    private static final int L_1 = 141809365;
    private static final int L_2 = 175155932;
    private static final int L_3 = 64542499;
    private static final int L_4 = 158326419;
    private static final int L_5 = 191173276;
    private static final int L_6 = 104575268;
    private static final int L_7 = 137584065;
    private static final long M26L = 67108863L;
    private static final long M28L = 268435455L;
    private static final long M32L = 4294967295L;
    private static final int[] P;
    private static final int POINT_BYTES = 57;
    private static final int PRECOMP_BLOCKS = 5;
    private static final int PRECOMP_MASK = 15;
    private static final int PRECOMP_POINTS = 16;
    private static final int PRECOMP_SPACING = 18;
    private static final int PRECOMP_TEETH = 5;
    public static final int PREHASH_SIZE = 64;
    public static final int PUBLIC_KEY_SIZE = 57;
    private static final int SCALAR_BYTES = 57;
    private static final int SCALAR_INTS = 14;
    public static final int SECRET_KEY_SIZE = 57;
    public static final int SIGNATURE_SIZE = 114;
    private static final int WNAF_WIDTH_BASE = 7;
    private static int[] precompBase;
    private static PointExt[] precompBaseTable;
    private static final Object precompLock;
    
    static {
        DOM4_PREFIX = Strings.toByteArray("SigEd448");
        P = new int[] { -1, -1, -1, -1, -1, -1, -1, -2, -1, -1, -1, -1, -1, -1 };
        L = new int[] { -1420278541, 595116690, -1916432555, 560775794, -1361693040, -1001465015, 2093622249, -1, -1, -1, -1, -1, -1, 1073741823 };
        B_x = new int[] { 118276190, 40534716, 9670182, 135141552, 85017403, 259173222, 68333082, 171784774, 174973732, 15824510, 73756743, 57518561, 94773951, 248652241, 107736333, 82941708 };
        B_y = new int[] { 36764180, 8885695, 130592152, 20104429, 163904957, 30304195, 121295871, 5901357, 125344798, 171541512, 175338348, 209069246, 3626697, 38307682, 24032956, 110359655 };
        precompLock = new Object();
        Ed448.precompBaseTable = null;
        Ed448.precompBase = null;
    }
    
    private static byte[] calculateS(byte[] array, final byte[] array2, final byte[] array3) {
        final int[] array4 = new int[28];
        int i = 0;
        decodeScalar(array, 0, array4);
        final int[] array5 = new int[14];
        decodeScalar(array2, 0, array5);
        final int[] array6 = new int[14];
        decodeScalar(array3, 0, array6);
        Nat.mulAddTo(14, array5, array6, array4);
        array = new byte[114];
        while (i < array4.length) {
            encode32(array4[i], array, i * 4);
            ++i;
        }
        return reduceScalar(array);
    }
    
    private static boolean checkContextVar(final byte[] array) {
        return array != null && array.length < 256;
    }
    
    private static int checkPoint(final int[] array, final int[] array2) {
        final int[] create = X448Field.create();
        final int[] create2 = X448Field.create();
        final int[] create3 = X448Field.create();
        X448Field.sqr(array, create2);
        X448Field.sqr(array2, create3);
        X448Field.mul(create2, create3, create);
        X448Field.add(create2, create3, create2);
        X448Field.mul(create, 39081, create);
        X448Field.subOne(create);
        X448Field.add(create, create2, create);
        X448Field.normalize(create);
        return X448Field.isZero(create);
    }
    
    private static int checkPoint(final int[] array, final int[] array2, final int[] array3) {
        final int[] create = X448Field.create();
        final int[] create2 = X448Field.create();
        final int[] create3 = X448Field.create();
        final int[] create4 = X448Field.create();
        X448Field.sqr(array, create2);
        X448Field.sqr(array2, create3);
        X448Field.sqr(array3, create4);
        X448Field.mul(create2, create3, create);
        X448Field.add(create2, create3, create2);
        X448Field.mul(create2, create4, create2);
        X448Field.sqr(create4, create4);
        X448Field.mul(create, 39081, create);
        X448Field.sub(create, create4, create);
        X448Field.add(create, create2, create);
        X448Field.normalize(create);
        return X448Field.isZero(create);
    }
    
    private static boolean checkPointVar(final byte[] array) {
        if ((array[56] & 0x7F) != 0x0) {
            return false;
        }
        final int[] array2 = new int[14];
        decode32(array, 0, array2, 0, 14);
        return Nat.gte(14, array2, Ed448.P) ^ true;
    }
    
    private static boolean checkScalarVar(final byte[] array) {
        if (array[56] != 0) {
            return false;
        }
        final int[] array2 = new int[14];
        decodeScalar(array, 0, array2);
        return Nat.gte(14, array2, Ed448.L) ^ true;
    }
    
    public static Xof createPrehash() {
        return createXof();
    }
    
    private static Xof createXof() {
        return new SHAKEDigest(256);
    }
    
    private static int decode16(final byte[] array, final int n) {
        return (array[n + 1] & 0xFF) << 8 | (array[n] & 0xFF);
    }
    
    private static int decode24(final byte[] array, int n) {
        final byte b = array[n];
        ++n;
        return (array[n + 1] & 0xFF) << 16 | ((b & 0xFF) | (array[n] & 0xFF) << 8);
    }
    
    private static int decode32(final byte[] array, int n) {
        final byte b = array[n];
        final int n2 = n + 1;
        n = array[n2];
        final int n3 = n2 + 1;
        return array[n3 + 1] << 24 | ((b & 0xFF) | (n & 0xFF) << 8 | (array[n3] & 0xFF) << 16);
    }
    
    private static void decode32(final byte[] array, final int n, final int[] array2, final int n2, final int n3) {
        for (int i = 0; i < n3; ++i) {
            array2[n2 + i] = decode32(array, i * 4 + n);
        }
    }
    
    private static boolean decodePointVar(byte[] copyOfRange, int n, final boolean b, final PointExt pointExt) {
        copyOfRange = Arrays.copyOfRange(copyOfRange, n, n + 57);
        final boolean checkPointVar = checkPointVar(copyOfRange);
        n = 0;
        if (!checkPointVar) {
            return false;
        }
        final int n2 = (copyOfRange[56] & 0x80) >>> 7;
        copyOfRange[56] &= 0x7F;
        X448Field.decode(copyOfRange, 0, pointExt.y);
        final int[] create = X448Field.create();
        final int[] create2 = X448Field.create();
        X448Field.sqr(pointExt.y, create);
        X448Field.mul(create, 39081, create2);
        X448Field.negate(create, create);
        X448Field.addOne(create);
        X448Field.addOne(create2);
        if (!X448Field.sqrtRatioVar(create, create2, pointExt.x)) {
            return false;
        }
        X448Field.normalize(pointExt.x);
        if (n2 == 1 && X448Field.isZeroVar(pointExt.x)) {
            return false;
        }
        if (n2 != (pointExt.x[0] & 0x1)) {
            n = 1;
        }
        if (((b ? 1 : 0) ^ n) != 0x0) {
            X448Field.negate(pointExt.x, pointExt.x);
        }
        pointExtendXY(pointExt);
        return true;
    }
    
    private static void decodeScalar(final byte[] array, final int n, final int[] array2) {
        decode32(array, n, array2, 0, 14);
    }
    
    private static void dom4(final Xof xof, final byte b, final byte[] array) {
        final byte[] dom4_PREFIX = Ed448.DOM4_PREFIX;
        xof.update(dom4_PREFIX, 0, dom4_PREFIX.length);
        xof.update(b);
        xof.update((byte)array.length);
        xof.update(array, 0, array.length);
    }
    
    private static void encode24(final int n, final byte[] array, int n2) {
        array[n2] = (byte)n;
        ++n2;
        array[n2] = (byte)(n >>> 8);
        array[n2 + 1] = (byte)(n >>> 16);
    }
    
    private static void encode32(final int n, final byte[] array, int n2) {
        array[n2] = (byte)n;
        ++n2;
        array[n2] = (byte)(n >>> 8);
        ++n2;
        array[n2] = (byte)(n >>> 16);
        array[n2 + 1] = (byte)(n >>> 24);
    }
    
    private static void encode56(final long n, final byte[] array, final int n2) {
        encode32((int)n, array, n2);
        encode24((int)(n >>> 32), array, n2 + 4);
    }
    
    private static int encodePoint(final PointExt pointExt, final byte[] array, final int n) {
        final int[] create = X448Field.create();
        final int[] create2 = X448Field.create();
        X448Field.inv(pointExt.z, create2);
        X448Field.mul(pointExt.x, create2, create);
        X448Field.mul(pointExt.y, create2, create2);
        X448Field.normalize(create);
        X448Field.normalize(create2);
        final int checkPoint = checkPoint(create, create2);
        X448Field.encode(create2, array, n);
        array[n + 57 - 1] = (byte)((create[0] & 0x1) << 7);
        return checkPoint;
    }
    
    public static void generatePrivateKey(final SecureRandom secureRandom, final byte[] bytes) {
        secureRandom.nextBytes(bytes);
    }
    
    public static void generatePublicKey(byte[] array, final int n, final byte[] array2, final int n2) {
        final Xof xof = createXof();
        final byte[] array3 = new byte[114];
        xof.update(array, n, 57);
        xof.doFinal(array3, 0, array3.length);
        array = new byte[57];
        pruneScalar(array3, 0, array);
        scalarMultBaseEncoded(array, array2, n2);
    }
    
    private static byte[] getWNAF(final int[] array, final int n) {
        final int[] array2 = new int[28];
        int length = array2.length;
        final int n2 = 0;
        int n3 = 14;
        int n4 = 0;
        while (true) {
            --n3;
            if (n3 < 0) {
                break;
            }
            final int n5 = array[n3];
            final int n6 = length - 1;
            array2[n6] = (n4 << 16 | n5 >>> 16);
            length = n6 - 1;
            array2[length] = n5;
            n4 = n5;
        }
        final byte[] array3 = new byte[447];
        final int n7 = 1 << n;
        int n8;
        for (int i = n8 = 0, j = n2; j < array2.length; ++j, i -= 16) {
            final int n9 = array2[j];
            while (i < 16) {
                final int n10 = n9 >>> i;
                if ((n10 & 0x1) == n8) {
                    ++i;
                }
                else {
                    final int n11 = (n10 & n7 - 1) + n8;
                    final int n12 = n11 & n7 >>> 1;
                    n8 = n12 >>> n - 1;
                    array3[(j << 4) + i] = (byte)(n11 - (n12 << 1));
                    i += n;
                }
            }
        }
        return array3;
    }
    
    private static int getWindow4(final int[] array, final int n) {
        return array[n >>> 3] >>> ((n & 0x7) << 2) & 0xF;
    }
    
    private static void implSign(final Xof xof, final byte[] array, final byte[] array2, final byte[] array3, final int n, final byte[] array4, final byte b, final byte[] array5, final int n2, final int n3, final byte[] array6, final int n4) {
        dom4(xof, b, array4);
        xof.update(array, 57, 57);
        xof.update(array5, n2, n3);
        xof.doFinal(array, 0, array.length);
        final byte[] reduceScalar = reduceScalar(array);
        final byte[] array7 = new byte[57];
        scalarMultBaseEncoded(reduceScalar, array7, 0);
        dom4(xof, b, array4);
        xof.update(array7, 0, 57);
        xof.update(array3, n, 57);
        xof.update(array5, n2, n3);
        xof.doFinal(array, 0, array.length);
        final byte[] calculateS = calculateS(reduceScalar, reduceScalar(array), array2);
        System.arraycopy(array7, 0, array6, n4, 57);
        System.arraycopy(calculateS, 0, array6, n4 + 57, 57);
    }
    
    private static void implSign(byte[] array, final int n, final byte[] array2, final byte b, final byte[] array3, final int n2, final int n3, final byte[] array4, final int n4) {
        if (checkContextVar(array2)) {
            final Xof xof = createXof();
            final byte[] array5 = new byte[114];
            xof.update(array, n, 57);
            xof.doFinal(array5, 0, array5.length);
            array = new byte[57];
            pruneScalar(array5, 0, array);
            final byte[] array6 = new byte[57];
            scalarMultBaseEncoded(array, array6, 0);
            implSign(xof, array5, array, array6, 0, array2, b, array3, n2, n3, array4, n4);
            return;
        }
        throw new IllegalArgumentException("ctx");
    }
    
    private static void implSign(byte[] array, final int n, final byte[] array2, final int n2, final byte[] array3, final byte b, final byte[] array4, final int n3, final int n4, final byte[] array5, final int n5) {
        if (checkContextVar(array3)) {
            final Xof xof = createXof();
            final byte[] array6 = new byte[114];
            xof.update(array, n, 57);
            xof.doFinal(array6, 0, array6.length);
            array = new byte[57];
            pruneScalar(array6, 0, array);
            implSign(xof, array6, array, array2, n2, array3, b, array4, n3, n4, array5, n5);
            return;
        }
        throw new IllegalArgumentException("ctx");
    }
    
    private static boolean implVerify(byte[] array, final int n, final byte[] array2, final int n2, byte[] reduceScalar, final byte b, final byte[] array3, final int n3, final int n4) {
        if (!checkContextVar(reduceScalar)) {
            throw new IllegalArgumentException("ctx");
        }
        final int n5 = n + 57;
        final byte[] copyOfRange = Arrays.copyOfRange(array, n, n5);
        final byte[] copyOfRange2 = Arrays.copyOfRange(array, n5, n + 114);
        final boolean checkPointVar = checkPointVar(copyOfRange);
        final boolean b2 = false;
        if (!checkPointVar) {
            return false;
        }
        if (!checkScalarVar(copyOfRange2)) {
            return false;
        }
        final PointExt pointExt = new PointExt();
        if (!decodePointVar(array2, n2, true, pointExt)) {
            return false;
        }
        final Xof xof = createXof();
        final byte[] array4 = new byte[114];
        dom4(xof, b, reduceScalar);
        xof.update(copyOfRange, 0, 57);
        xof.update(array2, n2, 57);
        xof.update(array3, n3, n4);
        xof.doFinal(array4, 0, array4.length);
        reduceScalar = reduceScalar(array4);
        final int[] array5 = new int[14];
        decodeScalar(copyOfRange2, 0, array5);
        final int[] array6 = new int[14];
        decodeScalar(reduceScalar, 0, array6);
        final PointExt pointExt2 = new PointExt();
        scalarMultStrausVar(array5, array6, pointExt, pointExt2);
        array = new byte[57];
        boolean b3 = b2;
        if (encodePoint(pointExt2, array, 0) != 0) {
            b3 = b2;
            if (Arrays.areEqual(array, copyOfRange)) {
                b3 = true;
            }
        }
        return b3;
    }
    
    private static void pointAdd(final PointExt pointExt, final PointExt pointExt2) {
        final int[] create = X448Field.create();
        final int[] create2 = X448Field.create();
        final int[] create3 = X448Field.create();
        final int[] create4 = X448Field.create();
        final int[] create5 = X448Field.create();
        final int[] create6 = X448Field.create();
        final int[] create7 = X448Field.create();
        final int[] create8 = X448Field.create();
        X448Field.mul(pointExt.z, pointExt2.z, create);
        X448Field.sqr(create, create2);
        X448Field.mul(pointExt.x, pointExt2.x, create3);
        X448Field.mul(pointExt.y, pointExt2.y, create4);
        X448Field.mul(create3, create4, create5);
        X448Field.mul(create5, 39081, create5);
        X448Field.add(create2, create5, create6);
        X448Field.sub(create2, create5, create7);
        X448Field.add(pointExt.x, pointExt.y, create2);
        X448Field.add(pointExt2.x, pointExt2.y, create5);
        X448Field.mul(create2, create5, create8);
        X448Field.add(create4, create3, create2);
        X448Field.sub(create4, create3, create5);
        X448Field.carry(create2);
        X448Field.sub(create8, create2, create8);
        X448Field.mul(create8, create, create8);
        X448Field.mul(create5, create, create5);
        X448Field.mul(create6, create8, pointExt2.x);
        X448Field.mul(create5, create7, pointExt2.y);
        X448Field.mul(create6, create7, pointExt2.z);
    }
    
    private static void pointAddPrecomp(final PointPrecomp pointPrecomp, final PointExt pointExt) {
        final int[] create = X448Field.create();
        final int[] create2 = X448Field.create();
        final int[] create3 = X448Field.create();
        final int[] create4 = X448Field.create();
        final int[] create5 = X448Field.create();
        final int[] create6 = X448Field.create();
        final int[] create7 = X448Field.create();
        X448Field.sqr(pointExt.z, create);
        X448Field.mul(pointPrecomp.x, pointExt.x, create2);
        X448Field.mul(pointPrecomp.y, pointExt.y, create3);
        X448Field.mul(create2, create3, create4);
        X448Field.mul(create4, 39081, create4);
        X448Field.add(create, create4, create5);
        X448Field.sub(create, create4, create6);
        X448Field.add(pointPrecomp.x, pointPrecomp.y, create);
        X448Field.add(pointExt.x, pointExt.y, create4);
        X448Field.mul(create, create4, create7);
        X448Field.add(create3, create2, create);
        X448Field.sub(create3, create2, create4);
        X448Field.carry(create);
        X448Field.sub(create7, create, create7);
        X448Field.mul(create7, pointExt.z, create7);
        X448Field.mul(create4, pointExt.z, create4);
        X448Field.mul(create5, create7, pointExt.x);
        X448Field.mul(create4, create6, pointExt.y);
        X448Field.mul(create5, create6, pointExt.z);
    }
    
    private static void pointAddVar(final boolean b, final PointExt pointExt, final PointExt pointExt2) {
        final int[] create = X448Field.create();
        final int[] create2 = X448Field.create();
        final int[] create3 = X448Field.create();
        final int[] create4 = X448Field.create();
        final int[] create5 = X448Field.create();
        final int[] create6 = X448Field.create();
        final int[] create7 = X448Field.create();
        final int[] create8 = X448Field.create();
        int[] array;
        int[] array2;
        int[] array3;
        int[] array4;
        if (b) {
            X448Field.sub(pointExt.y, pointExt.x, create8);
            array = create2;
            array2 = create5;
            array3 = create6;
            array4 = create7;
        }
        else {
            X448Field.add(pointExt.y, pointExt.x, create8);
            array2 = create2;
            array = create5;
            array4 = create6;
            array3 = create7;
        }
        X448Field.mul(pointExt.z, pointExt2.z, create);
        X448Field.sqr(create, create2);
        X448Field.mul(pointExt.x, pointExt2.x, create3);
        X448Field.mul(pointExt.y, pointExt2.y, create4);
        X448Field.mul(create3, create4, create5);
        X448Field.mul(create5, 39081, create5);
        X448Field.add(create2, create5, array4);
        X448Field.sub(create2, create5, array3);
        X448Field.add(pointExt2.x, pointExt2.y, create5);
        X448Field.mul(create8, create5, create8);
        X448Field.add(create4, create3, array2);
        X448Field.sub(create4, create3, array);
        X448Field.carry(array2);
        X448Field.sub(create8, create2, create8);
        X448Field.mul(create8, create, create8);
        X448Field.mul(create5, create, create5);
        X448Field.mul(create6, create8, pointExt2.x);
        X448Field.mul(create5, create7, pointExt2.y);
        X448Field.mul(create6, create7, pointExt2.z);
    }
    
    private static PointExt pointCopy(final PointExt pointExt) {
        final PointExt pointExt2 = new PointExt();
        pointCopy(pointExt, pointExt2);
        return pointExt2;
    }
    
    private static void pointCopy(final PointExt pointExt, final PointExt pointExt2) {
        X448Field.copy(pointExt.x, 0, pointExt2.x, 0);
        X448Field.copy(pointExt.y, 0, pointExt2.y, 0);
        X448Field.copy(pointExt.z, 0, pointExt2.z, 0);
    }
    
    private static void pointDouble(final PointExt pointExt) {
        final int[] create = X448Field.create();
        final int[] create2 = X448Field.create();
        final int[] create3 = X448Field.create();
        final int[] create4 = X448Field.create();
        final int[] create5 = X448Field.create();
        final int[] create6 = X448Field.create();
        X448Field.add(pointExt.x, pointExt.y, create);
        X448Field.sqr(create, create);
        X448Field.sqr(pointExt.x, create2);
        X448Field.sqr(pointExt.y, create3);
        X448Field.add(create2, create3, create4);
        X448Field.carry(create4);
        X448Field.sqr(pointExt.z, create5);
        X448Field.add(create5, create5, create5);
        X448Field.carry(create5);
        X448Field.sub(create4, create5, create6);
        X448Field.sub(create, create4, create);
        X448Field.sub(create2, create3, create2);
        X448Field.mul(create, create6, pointExt.x);
        X448Field.mul(create4, create2, pointExt.y);
        X448Field.mul(create4, create6, pointExt.z);
    }
    
    private static void pointExtendXY(final PointExt pointExt) {
        X448Field.one(pointExt.z);
    }
    
    private static void pointLookup(int i, final int n, final PointPrecomp pointPrecomp) {
        int n2 = i * 16 * 2 * 16;
        int n3;
        int n4;
        for (i = 0; i < 16; ++i) {
            n3 = (i ^ n) - 1 >> 31;
            X448Field.cmov(n3, Ed448.precompBase, n2, pointPrecomp.x, 0);
            n4 = n2 + 16;
            X448Field.cmov(n3, Ed448.precompBase, n4, pointPrecomp.y, 0);
            n2 = n4 + 16;
        }
    }
    
    private static void pointLookup(final int[] array, int i, final int[] array2, final PointExt pointExt) {
        final int window4 = getWindow4(array, i);
        final int n = window4 >>> 3 ^ 0x1;
        final int n2 = -n;
        int n3;
        int n4;
        int n5;
        int n6;
        for (i = (n3 = 0); i < 8; ++i) {
            n4 = (i ^ ((window4 ^ n2) & 0x7)) - 1 >> 31;
            X448Field.cmov(n4, array2, n3, pointExt.x, 0);
            n5 = n3 + 16;
            X448Field.cmov(n4, array2, n5, pointExt.y, 0);
            n6 = n5 + 16;
            X448Field.cmov(n4, array2, n6, pointExt.z, 0);
            n3 = n6 + 16;
        }
        X448Field.cnegate(n, pointExt.x);
    }
    
    private static int[] pointPrecomp(PointExt pointCopy, final int n) {
        pointCopy = pointCopy(pointCopy);
        final PointExt pointCopy2 = pointCopy(pointCopy);
        pointDouble(pointCopy2);
        final int[] table = X448Field.createTable(n * 3);
        int n3;
        int n2 = n3 = 0;
        while (true) {
            X448Field.copy(pointCopy.x, 0, table, n2);
            final int n4 = n2 + 16;
            X448Field.copy(pointCopy.y, 0, table, n4);
            final int n5 = n4 + 16;
            X448Field.copy(pointCopy.z, 0, table, n5);
            n2 = n5 + 16;
            ++n3;
            if (n3 == n) {
                break;
            }
            pointAdd(pointCopy2, pointCopy);
        }
        return table;
    }
    
    private static PointExt[] pointPrecompVar(final PointExt pointExt, final int n) {
        final PointExt pointCopy = pointCopy(pointExt);
        pointDouble(pointCopy);
        final PointExt[] array = new PointExt[n];
        array[0] = pointCopy(pointExt);
        for (int i = 1; i < n; ++i) {
            pointAddVar(false, pointCopy, array[i] = pointCopy(array[i - 1]));
        }
        return array;
    }
    
    private static void pointSetNeutral(final PointExt pointExt) {
        X448Field.zero(pointExt.x);
        X448Field.one(pointExt.y);
        X448Field.one(pointExt.z);
    }
    
    public static void precompute() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: astore          6
        //     5: aload           6
        //     7: monitorenter   
        //     8: getstatic       org/bouncycastle/math/ec/rfc8032/Ed448.precompBase:[I
        //    11: ifnull          18
        //    14: aload           6
        //    16: monitorexit    
        //    17: return         
        //    18: new             Lorg/bouncycastle/math/ec/rfc8032/Ed448$PointExt;
        //    21: dup            
        //    22: aconst_null    
        //    23: invokespecial   org/bouncycastle/math/ec/rfc8032/Ed448$PointExt.<init>:(Lorg/bouncycastle/math/ec/rfc8032/Ed448$1;)V
        //    26: astore          7
        //    28: getstatic       org/bouncycastle/math/ec/rfc8032/Ed448.B_x:[I
        //    31: iconst_0       
        //    32: aload           7
        //    34: getfield        org/bouncycastle/math/ec/rfc8032/Ed448$PointExt.x:[I
        //    37: iconst_0       
        //    38: invokestatic    org/bouncycastle/math/ec/rfc7748/X448Field.copy:([II[II)V
        //    41: getstatic       org/bouncycastle/math/ec/rfc8032/Ed448.B_y:[I
        //    44: iconst_0       
        //    45: aload           7
        //    47: getfield        org/bouncycastle/math/ec/rfc8032/Ed448$PointExt.y:[I
        //    50: iconst_0       
        //    51: invokestatic    org/bouncycastle/math/ec/rfc7748/X448Field.copy:([II[II)V
        //    54: aload           7
        //    56: invokestatic    org/bouncycastle/math/ec/rfc8032/Ed448.pointExtendXY:(Lorg/bouncycastle/math/ec/rfc8032/Ed448$PointExt;)V
        //    59: aload           7
        //    61: bipush          32
        //    63: invokestatic    org/bouncycastle/math/ec/rfc8032/Ed448.pointPrecompVar:(Lorg/bouncycastle/math/ec/rfc8032/Ed448$PointExt;I)[Lorg/bouncycastle/math/ec/rfc8032/Ed448$PointExt;
        //    66: putstatic       org/bouncycastle/math/ec/rfc8032/Ed448.precompBaseTable:[Lorg/bouncycastle/math/ec/rfc8032/Ed448$PointExt;
        //    69: sipush          160
        //    72: invokestatic    org/bouncycastle/math/ec/rfc7748/X448Field.createTable:(I)[I
        //    75: putstatic       org/bouncycastle/math/ec/rfc8032/Ed448.precompBase:[I
        //    78: iconst_0       
        //    79: istore_2       
        //    80: iload_2        
        //    81: istore_0       
        //    82: iload_2        
        //    83: iconst_5       
        //    84: if_icmpge       335
        //    87: iconst_5       
        //    88: anewarray       Lorg/bouncycastle/math/ec/rfc8032/Ed448$PointExt;
        //    91: astore          9
        //    93: new             Lorg/bouncycastle/math/ec/rfc8032/Ed448$PointExt;
        //    96: dup            
        //    97: aconst_null    
        //    98: invokespecial   org/bouncycastle/math/ec/rfc8032/Ed448$PointExt.<init>:(Lorg/bouncycastle/math/ec/rfc8032/Ed448$1;)V
        //   101: astore          10
        //   103: aload           10
        //   105: invokestatic    org/bouncycastle/math/ec/rfc8032/Ed448.pointSetNeutral:(Lorg/bouncycastle/math/ec/rfc8032/Ed448$PointExt;)V
        //   108: iconst_0       
        //   109: istore_1       
        //   110: iconst_1       
        //   111: istore_3       
        //   112: iload_1        
        //   113: iconst_5       
        //   114: if_icmpge       165
        //   117: iconst_1       
        //   118: aload           7
        //   120: aload           10
        //   122: invokestatic    org/bouncycastle/math/ec/rfc8032/Ed448.pointAddVar:(ZLorg/bouncycastle/math/ec/rfc8032/Ed448$PointExt;Lorg/bouncycastle/math/ec/rfc8032/Ed448$PointExt;)V
        //   125: aload           7
        //   127: invokestatic    org/bouncycastle/math/ec/rfc8032/Ed448.pointDouble:(Lorg/bouncycastle/math/ec/rfc8032/Ed448$PointExt;)V
        //   130: aload           9
        //   132: iload_1        
        //   133: aload           7
        //   135: invokestatic    org/bouncycastle/math/ec/rfc8032/Ed448.pointCopy:(Lorg/bouncycastle/math/ec/rfc8032/Ed448$PointExt;)Lorg/bouncycastle/math/ec/rfc8032/Ed448$PointExt;
        //   138: aastore        
        //   139: iload_2        
        //   140: iload_1        
        //   141: iadd           
        //   142: bipush          8
        //   144: if_icmpeq       347
        //   147: iload_3        
        //   148: bipush          18
        //   150: if_icmpge       347
        //   153: aload           7
        //   155: invokestatic    org/bouncycastle/math/ec/rfc8032/Ed448.pointDouble:(Lorg/bouncycastle/math/ec/rfc8032/Ed448$PointExt;)V
        //   158: iload_3        
        //   159: iconst_1       
        //   160: iadd           
        //   161: istore_3       
        //   162: goto            147
        //   165: bipush          16
        //   167: anewarray       Lorg/bouncycastle/math/ec/rfc8032/Ed448$PointExt;
        //   170: astore          8
        //   172: aload           8
        //   174: iconst_0       
        //   175: aload           10
        //   177: aastore        
        //   178: iconst_0       
        //   179: istore_3       
        //   180: iconst_1       
        //   181: istore_1       
        //   182: goto            354
        //   185: iload           4
        //   187: iload           5
        //   189: if_icmpge       370
        //   192: aload           8
        //   194: iload_1        
        //   195: aload           8
        //   197: iload_1        
        //   198: iload           5
        //   200: isub           
        //   201: aaload         
        //   202: invokestatic    org/bouncycastle/math/ec/rfc8032/Ed448.pointCopy:(Lorg/bouncycastle/math/ec/rfc8032/Ed448$PointExt;)Lorg/bouncycastle/math/ec/rfc8032/Ed448$PointExt;
        //   205: aastore        
        //   206: iconst_0       
        //   207: aload           9
        //   209: iload_3        
        //   210: aaload         
        //   211: aload           8
        //   213: iload_1        
        //   214: aaload         
        //   215: invokestatic    org/bouncycastle/math/ec/rfc8032/Ed448.pointAddVar:(ZLorg/bouncycastle/math/ec/rfc8032/Ed448$PointExt;Lorg/bouncycastle/math/ec/rfc8032/Ed448$PointExt;)V
        //   218: iload           4
        //   220: iconst_1       
        //   221: iadd           
        //   222: istore          4
        //   224: iload_1        
        //   225: iconst_1       
        //   226: iadd           
        //   227: istore_1       
        //   228: goto            185
        //   231: iload_1        
        //   232: bipush          16
        //   234: if_icmpge       382
        //   237: aload           8
        //   239: iload_1        
        //   240: aaload         
        //   241: astore          9
        //   243: aload           9
        //   245: getfield        org/bouncycastle/math/ec/rfc8032/Ed448$PointExt.z:[I
        //   248: aload           9
        //   250: getfield        org/bouncycastle/math/ec/rfc8032/Ed448$PointExt.z:[I
        //   253: invokestatic    org/bouncycastle/math/ec/rfc7748/X448Field.inv:([I[I)V
        //   256: aload           9
        //   258: getfield        org/bouncycastle/math/ec/rfc8032/Ed448$PointExt.x:[I
        //   261: aload           9
        //   263: getfield        org/bouncycastle/math/ec/rfc8032/Ed448$PointExt.z:[I
        //   266: aload           9
        //   268: getfield        org/bouncycastle/math/ec/rfc8032/Ed448$PointExt.x:[I
        //   271: invokestatic    org/bouncycastle/math/ec/rfc7748/X448Field.mul:([I[I[I)V
        //   274: aload           9
        //   276: getfield        org/bouncycastle/math/ec/rfc8032/Ed448$PointExt.y:[I
        //   279: aload           9
        //   281: getfield        org/bouncycastle/math/ec/rfc8032/Ed448$PointExt.z:[I
        //   284: aload           9
        //   286: getfield        org/bouncycastle/math/ec/rfc8032/Ed448$PointExt.y:[I
        //   289: invokestatic    org/bouncycastle/math/ec/rfc7748/X448Field.mul:([I[I[I)V
        //   292: aload           9
        //   294: getfield        org/bouncycastle/math/ec/rfc8032/Ed448$PointExt.x:[I
        //   297: iconst_0       
        //   298: getstatic       org/bouncycastle/math/ec/rfc8032/Ed448.precompBase:[I
        //   301: iload_0        
        //   302: invokestatic    org/bouncycastle/math/ec/rfc7748/X448Field.copy:([II[II)V
        //   305: iload_0        
        //   306: bipush          16
        //   308: iadd           
        //   309: istore_0       
        //   310: aload           9
        //   312: getfield        org/bouncycastle/math/ec/rfc8032/Ed448$PointExt.y:[I
        //   315: iconst_0       
        //   316: getstatic       org/bouncycastle/math/ec/rfc8032/Ed448.precompBase:[I
        //   319: iload_0        
        //   320: invokestatic    org/bouncycastle/math/ec/rfc7748/X448Field.copy:([II[II)V
        //   323: iload_0        
        //   324: bipush          16
        //   326: iadd           
        //   327: istore_0       
        //   328: iload_1        
        //   329: iconst_1       
        //   330: iadd           
        //   331: istore_1       
        //   332: goto            231
        //   335: aload           6
        //   337: monitorexit    
        //   338: return         
        //   339: astore          7
        //   341: aload           6
        //   343: monitorexit    
        //   344: aload           7
        //   346: athrow         
        //   347: iload_1        
        //   348: iconst_1       
        //   349: iadd           
        //   350: istore_1       
        //   351: goto            110
        //   354: iload_3        
        //   355: iconst_4       
        //   356: if_icmpge       377
        //   359: iconst_1       
        //   360: iload_3        
        //   361: ishl           
        //   362: istore          5
        //   364: iconst_0       
        //   365: istore          4
        //   367: goto            185
        //   370: iload_3        
        //   371: iconst_1       
        //   372: iadd           
        //   373: istore_3       
        //   374: goto            354
        //   377: iconst_0       
        //   378: istore_1       
        //   379: goto            231
        //   382: iload_2        
        //   383: iconst_1       
        //   384: iadd           
        //   385: istore_2       
        //   386: goto            82
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  8      17     339    347    Any
        //  18     78     339    347    Any
        //  87     108    339    347    Any
        //  117    139    339    347    Any
        //  153    158    339    347    Any
        //  165    172    339    347    Any
        //  192    218    339    347    Any
        //  243    305    339    347    Any
        //  310    323    339    347    Any
        //  335    338    339    347    Any
        //  341    344    339    347    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:833)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2030)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static void pruneScalar(final byte[] array, final int n, final byte[] array2) {
        System.arraycopy(array, n, array2, 0, 56);
        array2[0] &= (byte)252;
        array2[55] |= (byte)128;
        array2[56] = 0;
    }
    
    private static byte[] reduceScalar(byte[] array) {
        final long n = decode32(array, 0);
        final long n2 = decode24(array, 4) << 4;
        final long n3 = decode32(array, 7);
        final long n4 = decode24(array, 11) << 4;
        final long n5 = decode32(array, 14);
        final long n6 = decode24(array, 18) << 4;
        final long n7 = decode32(array, 21);
        final long n8 = decode24(array, 25) << 4;
        final long n9 = decode32(array, 28);
        final long n10 = decode24(array, 32) << 4;
        final long n11 = decode32(array, 35);
        final long n12 = decode24(array, 39) << 4;
        final long n13 = decode32(array, 42);
        final long n14 = decode24(array, 46) << 4;
        final long n15 = decode32(array, 49);
        final long n16 = decode24(array, 53) << 4;
        final long n17 = decode32(array, 56);
        final long n18 = decode24(array, 60) << 4;
        final long n19 = decode32(array, 63);
        final long n20 = decode24(array, 67) << 4;
        final long n21 = decode32(array, 70);
        final long n22 = decode24(array, 74) << 4;
        final long n23 = decode32(array, 77);
        final long n24 = decode24(array, 81) << 4;
        final long n25 = (long)decode32(array, 84) & 0xFFFFFFFFL;
        final long n26 = decode24(array, 88) << 4;
        final long n27 = (long)decode32(array, 91) & 0xFFFFFFFFL;
        final long n28 = decode24(array, 95) << 4;
        final long n29 = (long)decode32(array, 98) & 0xFFFFFFFFL;
        final long n30 = decode24(array, 102) << 4;
        final long n31 = (long)decode32(array, 105) & 0xFFFFFFFFL;
        final long n32 = decode24(array, 109) << 4;
        final long n33 = (long)decode16(array, 112) & 0xFFFFFFFFL;
        final long n34 = (n32 & 0xFFFFFFFFL) + (n31 >>> 28);
        final long n35 = n31 & 0xFFFFFFFL;
        final long n36 = (n30 & 0xFFFFFFFFL) + (n29 >>> 28);
        final long n37 = n29 & 0xFFFFFFFL;
        final long n38 = (n21 & 0xFFFFFFFFL) + n33 * 96434764L + n34 * 227822194L + n35 * 149865618L + n36 * 550336261L;
        final long n39 = (n28 & 0xFFFFFFFFL) + (n27 >>> 28);
        final long n40 = n27 & 0xFFFFFFFL;
        final long n41 = (n18 & 0xFFFFFFFFL) + n33 * 30366549L + n34 * 163752818L + n35 * 258169998L + n36 * 96434764L + n37 * 227822194L + n39 * 149865618L + n40 * 550336261L;
        final long n42 = (n26 & 0xFFFFFFFFL) + (n25 >>> 28);
        final long n43 = (n22 & 0xFFFFFFFFL) + n33 * 227822194L + n34 * 149865618L + n35 * 550336261L + (n38 >>> 28);
        final long n44 = (n23 & 0xFFFFFFFFL) + n33 * 149865618L + n34 * 550336261L + (n43 >>> 28);
        final long n45 = (n24 & 0xFFFFFFFFL) + n33 * 550336261L + (n44 >>> 28);
        final long n46 = n44 & 0xFFFFFFFL;
        final long n47 = (n25 & 0xFFFFFFFL) + (n45 >>> 28);
        final long n48 = n45 & 0xFFFFFFFL;
        final long n49 = (n15 & 0xFFFFFFFFL) + n35 * 43969588L + n36 * 30366549L + n37 * 163752818L + n39 * 258169998L + n40 * 96434764L + n42 * 227822194L + n47 * 149865618L + n48 * 550336261L;
        final long n50 = (n19 & 0xFFFFFFFFL) + n33 * 163752818L + n34 * 258169998L + n35 * 96434764L + n36 * 227822194L + n37 * 149865618L + n39 * 550336261L + (n41 >>> 28);
        final long n51 = (n20 & 0xFFFFFFFFL) + n33 * 258169998L + n34 * 96434764L + n35 * 227822194L + n36 * 149865618L + n37 * 550336261L + (n50 >>> 28);
        final long n52 = (n38 & 0xFFFFFFFL) + (n51 >>> 28);
        final long n53 = n51 & 0xFFFFFFFL;
        final long n54 = (n43 & 0xFFFFFFFL) + (n52 >>> 28);
        final long n55 = n52 & 0xFFFFFFFL;
        final long n56 = (n16 & 0xFFFFFFFFL) + n34 * 43969588L + n35 * 30366549L + n36 * 163752818L + n37 * 258169998L + n39 * 96434764L + n40 * 227822194L + n42 * 149865618L + n47 * 550336261L + (n49 >>> 28);
        final long n57 = (n17 & 0xFFFFFFFFL) + n33 * 43969588L + n34 * 30366549L + n35 * 163752818L + n36 * 258169998L + n37 * 96434764L + n39 * 227822194L + n40 * 149865618L + n42 * 550336261L + (n56 >>> 28);
        final long n58 = n56 & 0xFFFFFFFL;
        final long n59 = (n41 & 0xFFFFFFFL) + (n57 >>> 28);
        final long n60 = (n50 & 0xFFFFFFFL) + (n59 >>> 28);
        final long n61 = n59 & 0xFFFFFFFL;
        final long n62 = (n57 & 0xFFFFFFFL) * 4L + (n58 >>> 26) + 1L;
        final long n63 = (n & 0xFFFFFFFFL) + 78101261L * n62;
        final long n64 = (n2 & 0xFFFFFFFFL) + 43969588L * n61 + 141809365L * n62 + (n63 >>> 28);
        final long n65 = (n3 & 0xFFFFFFFFL) + n60 * 43969588L + 30366549L * n61 + 175155932L * n62 + (n64 >>> 28);
        final long n66 = (n4 & 0xFFFFFFFFL) + n53 * 43969588L + n60 * 30366549L + 163752818L * n61 + 64542499L * n62 + (n65 >>> 28);
        final long n67 = (n5 & 0xFFFFFFFFL) + n55 * 43969588L + n53 * 30366549L + n60 * 163752818L + 258169998L * n61 + 158326419L * n62 + (n66 >>> 28);
        final long n68 = (n6 & 0xFFFFFFFFL) + n54 * 43969588L + n55 * 30366549L + n53 * 163752818L + n60 * 258169998L + 96434764L * n61 + 191173276L * n62 + (n67 >>> 28);
        final long n69 = (n7 & 0xFFFFFFFFL) + n46 * 43969588L + n54 * 30366549L + n55 * 163752818L + n53 * 258169998L + n60 * 96434764L + 227822194L * n61 + 104575268L * n62 + (n68 >>> 28);
        final long n70 = (n8 & 0xFFFFFFFFL) + n48 * 43969588L + n46 * 30366549L + n54 * 163752818L + n55 * 258169998L + n53 * 96434764L + n60 * 227822194L + 149865618L * n61 + n62 * 137584065L + (n69 >>> 28);
        final long n71 = (n9 & 0xFFFFFFFFL) + n47 * 43969588L + n48 * 30366549L + n46 * 163752818L + n54 * 258169998L + n55 * 96434764L + n53 * 227822194L + n60 * 149865618L + n61 * 550336261L + (n70 >>> 28);
        final long n72 = (n10 & 0xFFFFFFFFL) + n42 * 43969588L + n47 * 30366549L + n48 * 163752818L + n46 * 258169998L + n54 * 96434764L + n55 * 227822194L + n53 * 149865618L + n60 * 550336261L + (n71 >>> 28);
        final long n73 = (n11 & 0xFFFFFFFFL) + n40 * 43969588L + n42 * 30366549L + n47 * 163752818L + n48 * 258169998L + n46 * 96434764L + n54 * 227822194L + n55 * 149865618L + n53 * 550336261L + (n72 >>> 28);
        final long n74 = (n12 & 0xFFFFFFFFL) + n39 * 43969588L + n40 * 30366549L + n42 * 163752818L + n47 * 258169998L + n48 * 96434764L + n46 * 227822194L + n54 * 149865618L + n55 * 550336261L + (n73 >>> 28);
        final long n75 = (n13 & 0xFFFFFFFFL) + n37 * 43969588L + n39 * 30366549L + n40 * 163752818L + n42 * 258169998L + n47 * 96434764L + n48 * 227822194L + n46 * 149865618L + n54 * 550336261L + (n74 >>> 28);
        final long n76 = (n14 & 0xFFFFFFFFL) + n36 * 43969588L + n37 * 30366549L + n39 * 163752818L + n40 * 258169998L + n42 * 96434764L + n47 * 227822194L + n48 * 149865618L + n46 * 550336261L + (n75 >>> 28);
        final long n77 = (n49 & 0xFFFFFFFL) + (n76 >>> 28);
        final long n78 = (n58 & 0x3FFFFFFL) + (n77 >>> 28);
        final long n79 = (n78 >>> 26) - 1L;
        final long n80 = (n63 & 0xFFFFFFFL) - (n79 & 0x4A7BB0DL);
        final long n81 = (n64 & 0xFFFFFFFL) - (n79 & 0x873D6D5L) + (n80 >> 28);
        final long n82 = (n65 & 0xFFFFFFFL) - (n79 & 0xA70AADCL) + (n81 >> 28);
        final long n83 = (n66 & 0xFFFFFFFL) - (n79 & 0x3D8D723L) + (n82 >> 28);
        final long n84 = (n67 & 0xFFFFFFFL) - (n79 & 0x96FDE93L) + (n83 >> 28);
        final long n85 = (n68 & 0xFFFFFFFL) - (n79 & 0xB65129CL) + (n84 >> 28);
        final long n86 = (n69 & 0xFFFFFFFL) - (n79 & 0x63BB124L) + (n85 >> 28);
        final long n87 = (n70 & 0xFFFFFFFL) - (n79 & 0x8335DC1L) + (n86 >> 28);
        final long n88 = (n71 & 0xFFFFFFFL) + (n87 >> 28);
        final long n89 = (n72 & 0xFFFFFFFL) + (n88 >> 28);
        final long n90 = (n73 & 0xFFFFFFFL) + (n89 >> 28);
        final long n91 = (n74 & 0xFFFFFFFL) + (n90 >> 28);
        final long n92 = (n75 & 0xFFFFFFFL) + (n91 >> 28);
        final long n93 = (n76 & 0xFFFFFFFL) + (n92 >> 28);
        final long n94 = (n77 & 0xFFFFFFFL) + (n93 >> 28);
        array = new byte[57];
        encode56((n81 & 0xFFFFFFFL) << 28 | (n80 & 0xFFFFFFFL), array, 0);
        encode56((n83 & 0xFFFFFFFL) << 28 | (n82 & 0xFFFFFFFL), array, 7);
        encode56((n84 & 0xFFFFFFFL) | (n85 & 0xFFFFFFFL) << 28, array, 14);
        encode56((n86 & 0xFFFFFFFL) | (n87 & 0xFFFFFFFL) << 28, array, 21);
        encode56((n88 & 0xFFFFFFFL) | (n89 & 0xFFFFFFFL) << 28, array, 28);
        encode56((n90 & 0xFFFFFFFL) | (n91 & 0xFFFFFFFL) << 28, array, 35);
        encode56((n92 & 0xFFFFFFFL) | (n93 & 0xFFFFFFFL) << 28, array, 42);
        encode56((n78 & 0x3FFFFFFL) + (n94 >> 28) << 28 | (n94 & 0xFFFFFFFL), array, 49);
        return array;
    }
    
    private static void scalarMult(final byte[] array, PointExt pointExt, final PointExt pointExt2) {
        precompute();
        final int[] array2 = new int[14];
        final int n = 0;
        decodeScalar(array, 0, array2);
        Nat.shiftDownBits(14, array2, 2, 0);
        Nat.cadd(14, array2[0] & 0x1, array2, Ed448.L, array2);
        Nat.shiftDownBit(14, array2, 1);
        final int[] pointPrecomp = pointPrecomp(pointExt, 8);
        pointLookup(array2, 111, pointPrecomp, pointExt2);
        pointExt = new PointExt();
        int n2 = 110;
        int i;
        while (true) {
            i = n;
            if (n2 < 0) {
                break;
            }
            for (int j = 0; j < 4; ++j) {
                pointDouble(pointExt2);
            }
            pointLookup(array2, n2, pointPrecomp, pointExt);
            pointAdd(pointExt, pointExt2);
            --n2;
        }
        while (i < 2) {
            pointDouble(pointExt2);
            ++i;
        }
    }
    
    private static void scalarMultBase(final byte[] array, final PointExt pointExt) {
        precompute();
        pointSetNeutral(pointExt);
        final int[] array2 = new int[15];
        decodeScalar(array, 0, array2);
        array2[14] = Nat.cadd(14, array2[0] & 0x1, array2, Ed448.L, array2) + 4;
        Nat.shiftDownBit(array2.length, array2, 0);
        final PointPrecomp pointPrecomp = new PointPrecomp();
        int n = 17;
        while (true) {
            int i = 0;
            int n2 = n;
            while (i < 5) {
                int j;
                int n3;
                for (n3 = (j = 0); j < 5; ++j) {
                    n3 = ((n3 & 1 << j) ^ array2[n2 >>> 5] >>> (n2 & 0x1F) << j);
                    n2 += 18;
                }
                final int n4 = n3 >>> 4 & 0x1;
                pointLookup(i, (-n4 ^ n3) & 0xF, pointPrecomp);
                X448Field.cnegate(n4, pointPrecomp.x);
                pointAddPrecomp(pointPrecomp, pointExt);
                ++i;
            }
            --n;
            if (n < 0) {
                break;
            }
            pointDouble(pointExt);
        }
    }
    
    private static void scalarMultBaseEncoded(final byte[] array, final byte[] array2, final int n) {
        final PointExt pointExt = new PointExt();
        scalarMultBase(array, pointExt);
        if (encodePoint(pointExt, array2, n) != 0) {
            return;
        }
        throw new IllegalStateException();
    }
    
    public static void scalarMultBaseXY(final X448.Friend friend, final byte[] array, final int n, final int[] array2, final int[] array3) {
        if (friend == null) {
            throw new NullPointerException("This method is only for use by X448");
        }
        final byte[] array4 = new byte[57];
        pruneScalar(array, n, array4);
        final PointExt pointExt = new PointExt();
        scalarMultBase(array4, pointExt);
        if (checkPoint(pointExt.x, pointExt.y, pointExt.z) != 0) {
            X448Field.copy(pointExt.x, 0, array2, 0);
            X448Field.copy(pointExt.y, 0, array3, 0);
            return;
        }
        throw new IllegalStateException();
    }
    
    private static void scalarMultStrausVar(final int[] array, final int[] array2, final PointExt pointExt, final PointExt pointExt2) {
        precompute();
        final byte[] wnaf = getWNAF(array, 7);
        final byte[] wnaf2 = getWNAF(array2, 5);
        final PointExt[] pointPrecompVar = pointPrecompVar(pointExt, 8);
        pointSetNeutral(pointExt2);
        int n = 446;
        while (true) {
            final byte b = wnaf[n];
            final boolean b2 = false;
            if (b != 0) {
                final int n2 = b >> 31;
                pointAddVar(n2 != 0, Ed448.precompBaseTable[(b ^ n2) >>> 1], pointExt2);
            }
            final byte b3 = wnaf2[n];
            if (b3 != 0) {
                final int n3 = b3 >> 31;
                boolean b4 = b2;
                if (n3 != 0) {
                    b4 = true;
                }
                pointAddVar(b4, pointPrecompVar[(b3 ^ n3) >>> 1], pointExt2);
            }
            --n;
            if (n < 0) {
                break;
            }
            pointDouble(pointExt2);
        }
    }
    
    public static void sign(final byte[] array, final int n, final byte[] array2, final int n2, final byte[] array3, final byte[] array4, final int n3, final int n4, final byte[] array5, final int n5) {
        implSign(array, n, array2, n2, array3, (byte)0, array4, n3, n4, array5, n5);
    }
    
    public static void sign(final byte[] array, final int n, final byte[] array2, final byte[] array3, final int n2, final int n3, final byte[] array4, final int n4) {
        implSign(array, n, array2, (byte)0, array3, n2, n3, array4, n4);
    }
    
    public static void signPrehash(final byte[] array, final int n, final byte[] array2, final int n2, final byte[] array3, final Xof xof, final byte[] array4, final int n3) {
        final byte[] array5 = new byte[64];
        if (64 == xof.doFinal(array5, 0, 64)) {
            implSign(array, n, array2, n2, array3, (byte)1, array5, 0, array5.length, array4, n3);
            return;
        }
        throw new IllegalArgumentException("ph");
    }
    
    public static void signPrehash(final byte[] array, final int n, final byte[] array2, final int n2, final byte[] array3, final byte[] array4, final int n3, final byte[] array5, final int n4) {
        implSign(array, n, array2, n2, array3, (byte)1, array4, n3, 64, array5, n4);
    }
    
    public static void signPrehash(final byte[] array, final int n, final byte[] array2, final Xof xof, final byte[] array3, final int n2) {
        final byte[] array4 = new byte[64];
        if (64 == xof.doFinal(array4, 0, 64)) {
            implSign(array, n, array2, (byte)1, array4, 0, array4.length, array3, n2);
            return;
        }
        throw new IllegalArgumentException("ph");
    }
    
    public static void signPrehash(final byte[] array, final int n, final byte[] array2, final byte[] array3, final int n2, final byte[] array4, final int n3) {
        implSign(array, n, array2, (byte)1, array3, n2, 64, array4, n3);
    }
    
    public static boolean verify(final byte[] array, final int n, final byte[] array2, final int n2, final byte[] array3, final byte[] array4, final int n3, final int n4) {
        return implVerify(array, n, array2, n2, array3, (byte)0, array4, n3, n4);
    }
    
    public static boolean verifyPrehash(final byte[] array, final int n, final byte[] array2, final int n2, final byte[] array3, final Xof xof) {
        final byte[] array4 = new byte[64];
        if (64 == xof.doFinal(array4, 0, 64)) {
            return implVerify(array, n, array2, n2, array3, (byte)1, array4, 0, array4.length);
        }
        throw new IllegalArgumentException("ph");
    }
    
    public static boolean verifyPrehash(final byte[] array, final int n, final byte[] array2, final int n2, final byte[] array3, final byte[] array4, final int n3) {
        return implVerify(array, n, array2, n2, array3, (byte)1, array4, n3, 64);
    }
    
    public static final class Algorithm
    {
        public static final int Ed448 = 0;
        public static final int Ed448ph = 1;
    }
    
    private static class PointExt
    {
        int[] x;
        int[] y;
        int[] z;
        
        private PointExt() {
            this.x = X448Field.create();
            this.y = X448Field.create();
            this.z = X448Field.create();
        }
    }
    
    private static class PointPrecomp
    {
        int[] x;
        int[] y;
        
        private PointPrecomp() {
            this.x = X448Field.create();
            this.y = X448Field.create();
        }
    }
}
