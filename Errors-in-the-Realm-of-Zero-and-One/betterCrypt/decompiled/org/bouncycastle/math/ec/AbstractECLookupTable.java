// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec;

public abstract class AbstractECLookupTable implements ECLookupTable
{
    @Override
    public ECPoint lookupVar(final int n) {
        return this.lookup(n);
    }
}
