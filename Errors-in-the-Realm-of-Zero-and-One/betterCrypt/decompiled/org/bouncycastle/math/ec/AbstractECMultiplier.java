// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec;

import java.math.BigInteger;

public abstract class AbstractECMultiplier implements ECMultiplier
{
    protected ECPoint checkResult(final ECPoint ecPoint) {
        return ECAlgorithms.implCheckResult(ecPoint);
    }
    
    @Override
    public ECPoint multiply(ECPoint ecPoint, final BigInteger bigInteger) {
        final int signum = bigInteger.signum();
        if (signum != 0 && !ecPoint.isInfinity()) {
            ecPoint = this.multiplyPositive(ecPoint, bigInteger.abs());
            if (signum <= 0) {
                ecPoint = ecPoint.negate();
            }
            return this.checkResult(ecPoint);
        }
        return ecPoint.getCurve().getInfinity();
    }
    
    protected abstract ECPoint multiplyPositive(final ECPoint p0, final BigInteger p1);
}
