// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec;

import org.bouncycastle.util.Integers;
import java.math.BigInteger;

public class WNafL2RMultiplier extends AbstractECMultiplier
{
    @Override
    protected ECPoint multiplyPositive(ECPoint ecPoint, final BigInteger bigInteger) {
        final WNafPreCompInfo precompute = WNafUtil.precompute(ecPoint, WNafUtil.getWindowSize(bigInteger.bitLength()), true);
        final ECPoint[] preComp = precompute.getPreComp();
        final ECPoint[] preCompNeg = precompute.getPreCompNeg();
        final int width = precompute.getWidth();
        final int[] generateCompactWindowNaf = WNafUtil.generateCompactWindowNaf(width, bigInteger);
        ecPoint = ecPoint.getCurve().getInfinity();
        int i;
        final int n = i = generateCompactWindowNaf.length;
        if (n > 1) {
            final int n2 = n - 1;
            final int n3 = generateCompactWindowNaf[n2];
            final int a = n3 >> 16;
            int n4 = n3 & 0xFFFF;
            final int abs = Math.abs(a);
            ECPoint[] array;
            if (a < 0) {
                array = preCompNeg;
            }
            else {
                array = preComp;
            }
            if (abs << 2 < 1 << width) {
                final int n5 = 32 - Integers.numberOfLeadingZeros(abs);
                final int n6 = width - n5;
                ecPoint = array[(1 << width - 1) - 1 >>> 1].add(array[((abs ^ 1 << n5 - 1) << n6) + 1 >>> 1]);
                n4 -= n6;
            }
            else {
                ecPoint = array[abs >>> 1];
            }
            ecPoint = ecPoint.timesPow2(n4);
            i = n2;
        }
        while (i > 0) {
            --i;
            final int n7 = generateCompactWindowNaf[i];
            final int a2 = n7 >> 16;
            final int abs2 = Math.abs(a2);
            ECPoint[] array2;
            if (a2 < 0) {
                array2 = preCompNeg;
            }
            else {
                array2 = preComp;
            }
            ecPoint = ecPoint.twicePlus(array2[abs2 >>> 1]).timesPow2(n7 & 0xFFFF);
        }
        return ecPoint;
    }
}
