// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec;

import java.math.BigInteger;

public class NafR2LMultiplier extends AbstractECMultiplier
{
    @Override
    protected ECPoint multiplyPositive(ECPoint add, final BigInteger bigInteger) {
        final int[] generateCompactNaf = WNafUtil.generateCompactNaf(bigInteger);
        final ECPoint infinity = add.getCurve().getInfinity();
        int i = 0;
        int n = 0;
        ECPoint timesPow2 = add;
        add = infinity;
        while (i < generateCompactNaf.length) {
            final int n2 = generateCompactNaf[i];
            timesPow2 = timesPow2.timesPow2(n + (n2 & 0xFFFF));
            ECPoint negate;
            if (n2 >> 16 < 0) {
                negate = timesPow2.negate();
            }
            else {
                negate = timesPow2;
            }
            add = add.add(negate);
            ++i;
            n = 1;
        }
        return add;
    }
}
