// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec;

import org.bouncycastle.math.field.PolynomialExtensionField;
import org.bouncycastle.math.field.FiniteField;
import org.bouncycastle.math.ec.endo.GLVEndomorphism;
import org.bouncycastle.math.ec.endo.EndoUtil;
import org.bouncycastle.math.ec.endo.ECEndomorphism;
import org.bouncycastle.math.raw.Nat;
import java.math.BigInteger;

public class ECAlgorithms
{
    public static ECPoint cleanPoint(final ECCurve ecCurve, final ECPoint ecPoint) {
        if (ecCurve.equals(ecPoint.getCurve())) {
            return ecCurve.decodePoint(ecPoint.getEncoded(false));
        }
        throw new IllegalArgumentException("Point must be on the same curve");
    }
    
    static ECPoint implCheckResult(final ECPoint ecPoint) {
        if (ecPoint.isValidPartial()) {
            return ecPoint;
        }
        throw new IllegalStateException("Invalid result");
    }
    
    private static ECPoint implShamirsTrickFixedPoint(ECPoint ecPoint, final BigInteger bigInteger, final ECPoint ecPoint2, final BigInteger bigInteger2) {
        final ECCurve curve = ecPoint.getCurve();
        final int combSize = FixedPointUtil.getCombSize(curve);
        if (bigInteger.bitLength() <= combSize && bigInteger2.bitLength() <= combSize) {
            final FixedPointPreCompInfo precompute = FixedPointUtil.precompute(ecPoint);
            final FixedPointPreCompInfo precompute2 = FixedPointUtil.precompute(ecPoint2);
            final ECLookupTable lookupTable = precompute.getLookupTable();
            final ECLookupTable lookupTable2 = precompute2.getLookupTable();
            final int width = precompute.getWidth();
            ECPoint ecPoint3;
            if (width != precompute2.getWidth()) {
                final FixedPointCombMultiplier fixedPointCombMultiplier = new FixedPointCombMultiplier();
                ecPoint = fixedPointCombMultiplier.multiply(ecPoint, bigInteger);
                ecPoint3 = fixedPointCombMultiplier.multiply(ecPoint2, bigInteger2);
            }
            else {
                final int n = (combSize + width - 1) / width;
                ecPoint = curve.getInfinity();
                final int n2 = width * n;
                final int[] fromBigInteger = Nat.fromBigInteger(n2, bigInteger);
                final int[] fromBigInteger2 = Nat.fromBigInteger(n2, bigInteger2);
                for (int i = 0; i < n; ++i) {
                    int j = n2 - 1 - i;
                    int n4;
                    int n3 = n4 = 0;
                    while (j >= 0) {
                        final int n5 = j >>> 5;
                        final int n6 = fromBigInteger[n5];
                        final int n7 = j & 0x1F;
                        final int n8 = n6 >>> n7;
                        n3 = ((n3 ^ n8 >>> 1) << 1 ^ n8);
                        final int n9 = fromBigInteger2[n5] >>> n7;
                        n4 = ((n4 ^ n9 >>> 1) << 1 ^ n9);
                        j -= n;
                    }
                    ecPoint = ecPoint.twicePlus(lookupTable.lookupVar(n3).add(lookupTable2.lookupVar(n4)));
                }
                ecPoint = ecPoint.add(precompute.getOffset());
                ecPoint3 = precompute2.getOffset();
            }
            return ecPoint.add(ecPoint3);
        }
        throw new IllegalStateException("fixed-point comb doesn't support scalars larger than the curve order");
    }
    
    static ECPoint implShamirsTrickJsf(ECPoint twicePlus, final BigInteger bigInteger, ECPoint negate, final BigInteger bigInteger2) {
        final ECCurve curve = twicePlus.getCurve();
        final ECPoint infinity = curve.getInfinity();
        final ECPoint[] array = { negate, twicePlus.subtract(negate), twicePlus, twicePlus.add(negate) };
        curve.normalizeAll(array);
        negate = array[3].negate();
        final ECPoint negate2 = array[2].negate();
        final ECPoint negate3 = array[1].negate();
        final ECPoint negate4 = array[0].negate();
        final ECPoint ecPoint = array[0];
        final ECPoint ecPoint2 = array[1];
        final ECPoint ecPoint3 = array[2];
        final ECPoint ecPoint4 = array[3];
        final byte[] generateJSF = WNafUtil.generateJSF(bigInteger, bigInteger2);
        int length = generateJSF.length;
        twicePlus = infinity;
        while (true) {
            --length;
            if (length < 0) {
                break;
            }
            final byte b = generateJSF[length];
            twicePlus = twicePlus.twicePlus((new ECPoint[] { negate, negate2, negate3, negate4, infinity, ecPoint, ecPoint2, ecPoint3, ecPoint4 })[(b << 24 >> 28) * 3 + 4 + (b << 28 >> 28)]);
        }
        return twicePlus;
    }
    
    static ECPoint implShamirsTrickWNaf(final ECPoint ecPoint, final BigInteger bigInteger, final ECPoint ecPoint2, final BigInteger bigInteger2) {
        final int signum = bigInteger.signum();
        boolean b = false;
        final boolean b2 = signum < 0;
        if (bigInteger2.signum() < 0) {
            b = true;
        }
        final BigInteger abs = bigInteger.abs();
        final BigInteger abs2 = bigInteger2.abs();
        final int windowSize = WNafUtil.getWindowSize(abs.bitLength(), 8);
        final int windowSize2 = WNafUtil.getWindowSize(abs2.bitLength(), 8);
        final WNafPreCompInfo precompute = WNafUtil.precompute(ecPoint, windowSize, true);
        final WNafPreCompInfo precompute2 = WNafUtil.precompute(ecPoint2, windowSize2, true);
        final int combSize = FixedPointUtil.getCombSize(ecPoint.getCurve());
        if (!b2 && !b && bigInteger.bitLength() <= combSize && bigInteger2.bitLength() <= combSize && precompute.isPromoted() && precompute2.isPromoted()) {
            return implShamirsTrickFixedPoint(ecPoint, bigInteger, ecPoint2, bigInteger2);
        }
        final int min = Math.min(8, precompute.getWidth());
        final int min2 = Math.min(8, precompute2.getWidth());
        ECPoint[] array;
        if (b2) {
            array = precompute.getPreCompNeg();
        }
        else {
            array = precompute.getPreComp();
        }
        ECPoint[] array2;
        if (b) {
            array2 = precompute2.getPreCompNeg();
        }
        else {
            array2 = precompute2.getPreComp();
        }
        ECPoint[] array3;
        if (b2) {
            array3 = precompute.getPreComp();
        }
        else {
            array3 = precompute.getPreCompNeg();
        }
        ECPoint[] array4;
        if (b) {
            array4 = precompute2.getPreComp();
        }
        else {
            array4 = precompute2.getPreCompNeg();
        }
        return implShamirsTrickWNaf(array, array3, WNafUtil.generateWindowNaf(min, abs), array2, array4, WNafUtil.generateWindowNaf(min2, abs2));
    }
    
    static ECPoint implShamirsTrickWNaf(final ECEndomorphism ecEndomorphism, final ECPoint ecPoint, final BigInteger bigInteger, final BigInteger bigInteger2) {
        final int signum = bigInteger.signum();
        boolean b = false;
        final boolean b2 = signum < 0;
        if (bigInteger2.signum() < 0) {
            b = true;
        }
        final BigInteger abs = bigInteger.abs();
        final BigInteger abs2 = bigInteger2.abs();
        final WNafPreCompInfo precompute = WNafUtil.precompute(ecPoint, WNafUtil.getWindowSize(Math.max(abs.bitLength(), abs2.bitLength()), 8), true);
        final WNafPreCompInfo precomputeWithPointMap = WNafUtil.precomputeWithPointMap(EndoUtil.mapPoint(ecEndomorphism, ecPoint), ecEndomorphism.getPointMap(), precompute, true);
        final int min = Math.min(8, precompute.getWidth());
        final int min2 = Math.min(8, precomputeWithPointMap.getWidth());
        ECPoint[] array;
        if (b2) {
            array = precompute.getPreCompNeg();
        }
        else {
            array = precompute.getPreComp();
        }
        ECPoint[] array2;
        if (b) {
            array2 = precomputeWithPointMap.getPreCompNeg();
        }
        else {
            array2 = precomputeWithPointMap.getPreComp();
        }
        ECPoint[] array3;
        if (b2) {
            array3 = precompute.getPreComp();
        }
        else {
            array3 = precompute.getPreCompNeg();
        }
        ECPoint[] array4;
        if (b) {
            array4 = precomputeWithPointMap.getPreComp();
        }
        else {
            array4 = precomputeWithPointMap.getPreCompNeg();
        }
        return implShamirsTrickWNaf(array, array3, WNafUtil.generateWindowNaf(min, abs), array2, array4, WNafUtil.generateWindowNaf(min2, abs2));
    }
    
    private static ECPoint implShamirsTrickWNaf(final ECPoint[] array, final ECPoint[] array2, final byte[] array3, final ECPoint[] array4, final ECPoint[] array5, final byte[] array6) {
        final int max = Math.max(array3.length, array6.length);
        final ECPoint infinity = array[0].getCurve().getInfinity();
        int i = max - 1;
        int n = 0;
        ECPoint twicePlus = infinity;
        while (i >= 0) {
            byte a;
            if (i < array3.length) {
                a = array3[i];
            }
            else {
                a = 0;
            }
            byte a2;
            if (i < array6.length) {
                a2 = array6[i];
            }
            else {
                a2 = 0;
            }
            if ((a | a2) == 0x0) {
                ++n;
            }
            else {
                ECPoint add;
                if (a != 0) {
                    final int abs = Math.abs(a);
                    ECPoint[] array7;
                    if (a < 0) {
                        array7 = array2;
                    }
                    else {
                        array7 = array;
                    }
                    add = infinity.add(array7[abs >>> 1]);
                }
                else {
                    add = infinity;
                }
                ECPoint add2 = add;
                if (a2 != 0) {
                    final int abs2 = Math.abs(a2);
                    ECPoint[] array8;
                    if (a2 < 0) {
                        array8 = array5;
                    }
                    else {
                        array8 = array4;
                    }
                    add2 = add.add(array8[abs2 >>> 1]);
                }
                int n2 = n;
                ECPoint timesPow2 = twicePlus;
                if (n > 0) {
                    timesPow2 = twicePlus.timesPow2(n);
                    n2 = 0;
                }
                twicePlus = timesPow2.twicePlus(add2);
                n = n2;
            }
            --i;
        }
        ECPoint timesPow3 = twicePlus;
        if (n > 0) {
            timesPow3 = twicePlus.timesPow2(n);
        }
        return timesPow3;
    }
    
    static ECPoint implSumOfMultiplies(final ECEndomorphism ecEndomorphism, final ECPoint[] array, final BigInteger[] array2) {
        final int length = array.length;
        final int n = length << 1;
        final boolean[] array3 = new boolean[n];
        final WNafPreCompInfo[] array4 = new WNafPreCompInfo[n];
        final byte[][] array5 = new byte[n][];
        final ECPointMap pointMap = ecEndomorphism.getPointMap();
        for (int i = 0; i < length; ++i) {
            final int n2 = i << 1;
            final int n3 = n2 + 1;
            final BigInteger bigInteger = array2[n2];
            array3[n2] = (bigInteger.signum() < 0);
            final BigInteger abs = bigInteger.abs();
            final BigInteger bigInteger2 = array2[n3];
            array3[n3] = (bigInteger2.signum() < 0);
            final BigInteger abs2 = bigInteger2.abs();
            final int windowSize = WNafUtil.getWindowSize(Math.max(abs.bitLength(), abs2.bitLength()), 8);
            final ECPoint ecPoint = array[i];
            final WNafPreCompInfo precompute = WNafUtil.precompute(ecPoint, windowSize, true);
            final WNafPreCompInfo precomputeWithPointMap = WNafUtil.precomputeWithPointMap(EndoUtil.mapPoint(ecEndomorphism, ecPoint), pointMap, precompute, true);
            final int min = Math.min(8, precompute.getWidth());
            final int min2 = Math.min(8, precomputeWithPointMap.getWidth());
            array4[n2] = precompute;
            array4[n3] = precomputeWithPointMap;
            array5[n2] = WNafUtil.generateWindowNaf(min, abs);
            array5[n3] = WNafUtil.generateWindowNaf(min2, abs2);
        }
        return implSumOfMultiplies(array3, array4, array5);
    }
    
    static ECPoint implSumOfMultiplies(final ECPoint[] array, final BigInteger[] array2) {
        final int length = array.length;
        final boolean[] array3 = new boolean[length];
        final WNafPreCompInfo[] array4 = new WNafPreCompInfo[length];
        final byte[][] array5 = new byte[length][];
        for (int i = 0; i < length; ++i) {
            final BigInteger bigInteger = array2[i];
            array3[i] = (bigInteger.signum() < 0);
            final BigInteger abs = bigInteger.abs();
            final WNafPreCompInfo precompute = WNafUtil.precompute(array[i], WNafUtil.getWindowSize(abs.bitLength(), 8), true);
            final int min = Math.min(8, precompute.getWidth());
            array4[i] = precompute;
            array5[i] = WNafUtil.generateWindowNaf(min, abs);
        }
        return implSumOfMultiplies(array3, array4, array5);
    }
    
    private static ECPoint implSumOfMultiplies(final boolean[] array, final WNafPreCompInfo[] array2, final byte[][] array3) {
        final int length = array3.length;
        int max;
        for (int i = max = 0; i < length; ++i) {
            max = Math.max(max, array3[i].length);
        }
        final ECPoint infinity = array2[0].getPreComp()[0].getCurve().getInfinity();
        int j = max - 1;
        int n = 0;
        ECPoint twicePlus = infinity;
        while (j >= 0) {
            int k = 0;
            ECPoint ecPoint = infinity;
            while (k < length) {
                final byte[] array4 = array3[k];
                byte a;
                if (j < array4.length) {
                    a = array4[j];
                }
                else {
                    a = 0;
                }
                ECPoint add = ecPoint;
                if (a != 0) {
                    final int abs = Math.abs(a);
                    final WNafPreCompInfo wNafPreCompInfo = array2[k];
                    ECPoint[] array5;
                    if (a < 0 == array[k]) {
                        array5 = wNafPreCompInfo.getPreComp();
                    }
                    else {
                        array5 = wNafPreCompInfo.getPreCompNeg();
                    }
                    add = ecPoint.add(array5[abs >>> 1]);
                }
                ++k;
                ecPoint = add;
            }
            if (ecPoint == infinity) {
                ++n;
            }
            else {
                int n2 = n;
                ECPoint timesPow2 = twicePlus;
                if (n > 0) {
                    timesPow2 = twicePlus.timesPow2(n);
                    n2 = 0;
                }
                twicePlus = timesPow2.twicePlus(ecPoint);
                n = n2;
            }
            --j;
        }
        ECPoint timesPow3 = twicePlus;
        if (n > 0) {
            timesPow3 = twicePlus.timesPow2(n);
        }
        return timesPow3;
    }
    
    static ECPoint implSumOfMultipliesGLV(final ECPoint[] array, final BigInteger[] array2, final GLVEndomorphism glvEndomorphism) {
        final int n = 0;
        final BigInteger order = array[0].getCurve().getOrder();
        final int length = array.length;
        final int n2 = length << 1;
        final BigInteger[] array3 = new BigInteger[n2];
        int n3;
        for (int i = n3 = 0; i < length; ++i) {
            final BigInteger[] decomposeScalar = glvEndomorphism.decomposeScalar(array2[i].mod(order));
            final int n4 = n3 + 1;
            array3[n3] = decomposeScalar[0];
            n3 = n4 + 1;
            array3[n4] = decomposeScalar[1];
        }
        if (glvEndomorphism.hasEfficientPointMap()) {
            return implSumOfMultiplies(glvEndomorphism, array, array3);
        }
        final ECPoint[] array4 = new ECPoint[n2];
        int n5 = 0;
        for (int j = n; j < length; ++j) {
            final ECPoint ecPoint = array[j];
            final ECPoint mapPoint = EndoUtil.mapPoint(glvEndomorphism, ecPoint);
            final int n6 = n5 + 1;
            array4[n5] = ecPoint;
            n5 = n6 + 1;
            array4[n6] = mapPoint;
        }
        return implSumOfMultiplies(array4, array3);
    }
    
    public static ECPoint importPoint(final ECCurve ecCurve, final ECPoint ecPoint) {
        if (ecCurve.equals(ecPoint.getCurve())) {
            return ecCurve.importPoint(ecPoint);
        }
        throw new IllegalArgumentException("Point must be on the same curve");
    }
    
    public static boolean isF2mCurve(final ECCurve ecCurve) {
        return isF2mField(ecCurve.getField());
    }
    
    public static boolean isF2mField(final FiniteField finiteField) {
        return finiteField.getDimension() > 1 && finiteField.getCharacteristic().equals(ECConstants.TWO) && finiteField instanceof PolynomialExtensionField;
    }
    
    public static boolean isFpCurve(final ECCurve ecCurve) {
        return isFpField(ecCurve.getField());
    }
    
    public static boolean isFpField(final FiniteField finiteField) {
        return finiteField.getDimension() == 1;
    }
    
    public static void montgomeryTrick(final ECFieldElement[] array, final int n, final int n2) {
        montgomeryTrick(array, n, n2, null);
    }
    
    public static void montgomeryTrick(final ECFieldElement[] array, final int n, int i, ECFieldElement ecFieldElement) {
        final ECFieldElement[] array2 = new ECFieldElement[i];
        final ECFieldElement ecFieldElement2 = array[n];
        int n2 = 0;
        array2[0] = ecFieldElement2;
        while (true) {
            ++n2;
            if (n2 >= i) {
                break;
            }
            array2[n2] = array2[n2 - 1].multiply(array[n + n2]);
        }
        i = n2 - 1;
        if (ecFieldElement != null) {
            array2[i] = array2[i].multiply(ecFieldElement);
        }
        int n3;
        ECFieldElement ecFieldElement3;
        for (ecFieldElement = array2[i].invert(); i > 0; i += n, ecFieldElement3 = array[i], array[i] = array2[n3].multiply(ecFieldElement), ecFieldElement = ecFieldElement.multiply(ecFieldElement3), i = n3) {
            n3 = i - 1;
        }
        array[n] = ecFieldElement;
    }
    
    public static ECPoint referenceMultiply(ECPoint negate, final BigInteger bigInteger) {
        final BigInteger abs = bigInteger.abs();
        final ECPoint infinity = negate.getCurve().getInfinity();
        final int bitLength = abs.bitLength();
        ECPoint ecPoint = infinity;
        if (bitLength > 0) {
            ECPoint ecPoint2 = infinity;
            if (abs.testBit(0)) {
                ecPoint2 = negate;
            }
            int n = 1;
            ECPoint twice = negate;
            negate = ecPoint2;
            while (true) {
                ecPoint = negate;
                if (n >= bitLength) {
                    break;
                }
                twice = twice.twice();
                ECPoint add = negate;
                if (abs.testBit(n)) {
                    add = negate.add(twice);
                }
                ++n;
                negate = add;
            }
        }
        negate = ecPoint;
        if (bigInteger.signum() < 0) {
            negate = ecPoint.negate();
        }
        return negate;
    }
    
    public static ECPoint shamirsTrick(final ECPoint ecPoint, final BigInteger bigInteger, final ECPoint ecPoint2, final BigInteger bigInteger2) {
        return implCheckResult(implShamirsTrickJsf(ecPoint, bigInteger, importPoint(ecPoint.getCurve(), ecPoint2), bigInteger2));
    }
    
    public static ECPoint sumOfMultiplies(final ECPoint[] array, final BigInteger[] array2) {
        if (array != null && array2 != null && array.length == array2.length) {
            final int length = array.length;
            int i = 1;
            if (length >= 1) {
                final int length2 = array.length;
                if (length2 == 1) {
                    return array[0].multiply(array2[0]);
                }
                if (length2 == 2) {
                    return sumOfTwoMultiplies(array[0], array2[0], array[1], array2[1]);
                }
                final ECPoint ecPoint = array[0];
                final ECCurve curve = ecPoint.getCurve();
                final ECPoint[] array3 = new ECPoint[length2];
                array3[0] = ecPoint;
                while (i < length2) {
                    array3[i] = importPoint(curve, array[i]);
                    ++i;
                }
                final ECEndomorphism endomorphism = curve.getEndomorphism();
                if (endomorphism instanceof GLVEndomorphism) {
                    return implCheckResult(implSumOfMultipliesGLV(array3, array2, (GLVEndomorphism)endomorphism));
                }
                return implCheckResult(implSumOfMultiplies(array3, array2));
            }
        }
        throw new IllegalArgumentException("point and scalar arrays should be non-null, and of equal, non-zero, length");
    }
    
    public static ECPoint sumOfTwoMultiplies(ECPoint ecPoint, final BigInteger bigInteger, ECPoint importPoint, final BigInteger bigInteger2) {
        final ECCurve curve = ecPoint.getCurve();
        importPoint = importPoint(curve, importPoint);
        if (curve instanceof ECCurve.AbstractF2m && ((ECCurve.AbstractF2m)curve).isKoblitz()) {
            ecPoint = ecPoint.multiply(bigInteger).add(importPoint.multiply(bigInteger2));
        }
        else {
            final ECEndomorphism endomorphism = curve.getEndomorphism();
            if (endomorphism instanceof GLVEndomorphism) {
                ecPoint = implSumOfMultipliesGLV(new ECPoint[] { ecPoint, importPoint }, new BigInteger[] { bigInteger, bigInteger2 }, (GLVEndomorphism)endomorphism);
            }
            else {
                ecPoint = implShamirsTrickWNaf(ecPoint, bigInteger, importPoint, bigInteger2);
            }
        }
        return implCheckResult(ecPoint);
    }
    
    public static ECPoint validatePoint(final ECPoint ecPoint) {
        if (ecPoint.isValid()) {
            return ecPoint;
        }
        throw new IllegalStateException("Invalid point");
    }
}
