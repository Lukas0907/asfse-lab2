// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec;

public class ScaleYNegateXPointMap implements ECPointMap
{
    protected final ECFieldElement scale;
    
    public ScaleYNegateXPointMap(final ECFieldElement scale) {
        this.scale = scale;
    }
    
    @Override
    public ECPoint map(final ECPoint ecPoint) {
        return ecPoint.scaleYNegateX(this.scale);
    }
}
