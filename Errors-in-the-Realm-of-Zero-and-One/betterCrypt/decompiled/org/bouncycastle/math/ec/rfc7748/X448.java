// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.rfc7748;

import org.bouncycastle.math.ec.rfc8032.Ed448;
import java.security.SecureRandom;
import org.bouncycastle.util.Arrays;

public abstract class X448
{
    private static final int C_A = 156326;
    private static final int C_A24 = 39082;
    public static final int POINT_SIZE = 56;
    public static final int SCALAR_SIZE = 56;
    
    public static boolean calculateAgreement(final byte[] array, final int n, final byte[] array2, final int n2, final byte[] array3, final int n3) {
        scalarMult(array, n, array2, n2, array3, n3);
        return Arrays.areAllZeroes(array3, n3, 56) ^ true;
    }
    
    private static int decode32(final byte[] array, int n) {
        final byte b = array[n];
        final int n2 = n + 1;
        n = array[n2];
        final int n3 = n2 + 1;
        return array[n3 + 1] << 24 | ((b & 0xFF) | (n & 0xFF) << 8 | (array[n3] & 0xFF) << 16);
    }
    
    private static void decodeScalar(final byte[] array, final int n, final int[] array2) {
        for (int i = 0; i < 14; ++i) {
            array2[i] = decode32(array, i * 4 + n);
        }
        array2[0] &= 0xFFFFFFFC;
        array2[13] |= Integer.MIN_VALUE;
    }
    
    public static void generatePrivateKey(final SecureRandom secureRandom, final byte[] bytes) {
        secureRandom.nextBytes(bytes);
        bytes[0] &= (byte)252;
        bytes[55] |= (byte)128;
    }
    
    public static void generatePublicKey(final byte[] array, final int n, final byte[] array2, final int n2) {
        scalarMultBase(array, n, array2, n2);
    }
    
    private static void pointDouble(final int[] array, final int[] array2) {
        final int[] create = X448Field.create();
        final int[] create2 = X448Field.create();
        X448Field.add(array, array2, create);
        X448Field.sub(array, array2, create2);
        X448Field.sqr(create, create);
        X448Field.sqr(create2, create2);
        X448Field.mul(create, create2, array);
        X448Field.sub(create, create2, create);
        X448Field.mul(create, 39082, array2);
        X448Field.add(array2, create2, array2);
        X448Field.mul(array2, create, array2);
    }
    
    public static void precompute() {
        Ed448.precompute();
    }
    
    public static void scalarMult(final byte[] array, int i, final byte[] array2, int n, final byte[] array3, final int n2) {
        final int[] array4 = new int[14];
        decodeScalar(array, i, array4);
        final int[] create = X448Field.create();
        X448Field.decode(array2, n, create);
        final int[] create2 = X448Field.create();
        final int n3 = 0;
        X448Field.copy(create, 0, create2, 0);
        final int[] create3 = X448Field.create();
        create3[0] = 1;
        final int[] create4 = X448Field.create();
        create4[0] = 1;
        final int[] create5 = X448Field.create();
        final int[] create6 = X448Field.create();
        final int[] create7 = X448Field.create();
        n = 447;
        i = 1;
        while (true) {
            X448Field.add(create4, create5, create6);
            X448Field.sub(create4, create5, create4);
            X448Field.add(create2, create3, create5);
            X448Field.sub(create2, create3, create2);
            X448Field.mul(create6, create2, create6);
            X448Field.mul(create4, create5, create4);
            X448Field.sqr(create5, create5);
            X448Field.sqr(create2, create2);
            X448Field.sub(create5, create2, create7);
            X448Field.mul(create7, 39082, create3);
            X448Field.add(create3, create2, create3);
            X448Field.mul(create3, create7, create3);
            X448Field.mul(create2, create5, create2);
            X448Field.sub(create6, create4, create5);
            X448Field.add(create6, create4, create4);
            X448Field.sqr(create4, create4);
            X448Field.sqr(create5, create5);
            X448Field.mul(create5, create, create5);
            --n;
            final int n4 = array4[n >>> 5] >>> (n & 0x1F) & 0x1;
            i ^= n4;
            X448Field.cswap(i, create2, create4);
            X448Field.cswap(i, create3, create5);
            if (n < 2) {
                break;
            }
            i = n4;
        }
        for (i = n3; i < 2; ++i) {
            pointDouble(create2, create3);
        }
        X448Field.inv(create3, create3);
        X448Field.mul(create2, create3, create2);
        X448Field.normalize(create2);
        X448Field.encode(create2, array3, n2);
    }
    
    public static void scalarMultBase(final byte[] array, final int n, final byte[] array2, final int n2) {
        final int[] create = X448Field.create();
        final int[] create2 = X448Field.create();
        Ed448.scalarMultBaseXY(Friend.INSTANCE, array, n, create, create2);
        X448Field.inv(create, create);
        X448Field.mul(create, create2, create);
        X448Field.sqr(create, create);
        X448Field.normalize(create);
        X448Field.encode(create, array2, n2);
    }
    
    public static class Friend
    {
        private static final Friend INSTANCE;
        
        static {
            INSTANCE = new Friend();
        }
        
        private Friend() {
        }
    }
}
