// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.rfc7748;

public abstract class X25519Field
{
    private static final int M24 = 16777215;
    private static final int M25 = 33554431;
    private static final int M26 = 67108863;
    private static final int[] ROOT_NEG_ONE;
    public static final int SIZE = 10;
    
    static {
        ROOT_NEG_ONE = new int[] { 34513072, 59165138, 4688974, 3500415, 6194736, 33281959, 54535759, 32551604, 163342, 5703241 };
    }
    
    protected X25519Field() {
    }
    
    public static void add(final int[] array, final int[] array2, final int[] array3) {
        for (int i = 0; i < 10; ++i) {
            array3[i] = array[i] + array2[i];
        }
    }
    
    public static void addOne(final int[] array) {
        ++array[0];
    }
    
    public static void addOne(final int[] array, final int n) {
        ++array[n];
    }
    
    public static void apm(final int[] array, final int[] array2, final int[] array3, final int[] array4) {
        for (int i = 0; i < 10; ++i) {
            final int n = array[i];
            final int n2 = array2[i];
            array3[i] = n + n2;
            array4[i] = n - n2;
        }
    }
    
    public static void carry(final int[] array) {
        final int n = array[0];
        final int n2 = array[1];
        final int n3 = array[2];
        final int n4 = array[3];
        final int n5 = array[4];
        final int n6 = array[5];
        final int n7 = array[6];
        final int n8 = array[7];
        final int n9 = array[8];
        final int n10 = array[9];
        final int n11 = n4 + (n3 >> 25);
        final int n12 = n6 + (n5 >> 25);
        final int n13 = n9 + (n8 >> 25);
        final int n14 = n + (n10 >> 25) * 38;
        final int n15 = n2 + (n14 >> 26);
        final int n16 = n7 + (n12 >> 26);
        array[0] = (n14 & 0x3FFFFFF);
        array[1] = (n15 & 0x3FFFFFF);
        array[2] = (n3 & 0x1FFFFFF) + (n15 >> 26);
        array[3] = (n11 & 0x3FFFFFF);
        array[4] = (n5 & 0x1FFFFFF) + (n11 >> 26);
        array[5] = (n12 & 0x3FFFFFF);
        array[6] = (n16 & 0x3FFFFFF);
        array[7] = (n8 & 0x1FFFFFF) + (n16 >> 26);
        array[8] = (n13 & 0x3FFFFFF);
        array[9] = (n10 & 0x1FFFFFF) + (n13 >> 26);
    }
    
    public static void cmov(final int n, final int[] array, final int n2, final int[] array2, final int n3) {
        for (int i = 0; i < 10; ++i) {
            final int n4 = n3 + i;
            final int n5 = array2[n4];
            array2[n4] = (n5 ^ ((array[n2 + i] ^ n5) & n));
        }
    }
    
    public static void cnegate(int i, final int[] array) {
        final int n = 0;
        final int n2 = 0 - i;
        for (i = n; i < 10; ++i) {
            array[i] = (array[i] ^ n2) - n2;
        }
    }
    
    public static void copy(final int[] array, final int n, final int[] array2, final int n2) {
        for (int i = 0; i < 10; ++i) {
            array2[n2 + i] = array[n + i];
        }
    }
    
    public static int[] create() {
        return new int[10];
    }
    
    public static int[] createTable(final int n) {
        return new int[n * 10];
    }
    
    public static void cswap(final int n, final int[] array, final int[] array2) {
        for (int i = 0; i < 10; ++i) {
            final int n2 = array[i];
            final int n3 = array2[i];
            final int n4 = (n2 ^ n3) & 0 - n;
            array[i] = (n2 ^ n4);
            array2[i] = (n3 ^ n4);
        }
    }
    
    public static void decode(final byte[] array, final int n, final int[] array2) {
        decode128(array, n, array2, 0);
        decode128(array, n + 16, array2, 5);
        array2[9] &= 0xFFFFFF;
    }
    
    private static void decode128(final byte[] array, int decode32, final int[] array2, final int n) {
        final int decode33 = decode32(array, decode32 + 0);
        final int decode34 = decode32(array, decode32 + 4);
        final int decode35 = decode32(array, decode32 + 8);
        decode32 = decode32(array, decode32 + 12);
        array2[n + 0] = (decode33 & 0x3FFFFFF);
        array2[n + 1] = ((decode33 >>> 26 | decode34 << 6) & 0x3FFFFFF);
        array2[n + 2] = ((decode35 << 12 | decode34 >>> 20) & 0x1FFFFFF);
        array2[n + 3] = ((decode32 << 19 | decode35 >>> 13) & 0x3FFFFFF);
        array2[n + 4] = decode32 >>> 7;
    }
    
    private static int decode32(final byte[] array, int n) {
        final byte b = array[n];
        final int n2 = n + 1;
        n = array[n2];
        final int n3 = n2 + 1;
        return array[n3 + 1] << 24 | ((b & 0xFF) | (n & 0xFF) << 8 | (array[n3] & 0xFF) << 16);
    }
    
    public static void encode(final int[] array, final byte[] array2, final int n) {
        encode128(array, 0, array2, n);
        encode128(array, 5, array2, n + 16);
    }
    
    private static void encode128(final int[] array, int n, final byte[] array2, final int n2) {
        final int n3 = array[n + 0];
        final int n4 = array[n + 1];
        final int n5 = array[n + 2];
        final int n6 = array[n + 3];
        n = array[n + 4];
        encode32(n4 << 26 | n3, array2, n2 + 0);
        encode32(n4 >>> 6 | n5 << 20, array2, n2 + 4);
        encode32(n5 >>> 12 | n6 << 13, array2, n2 + 8);
        encode32(n << 7 | n6 >>> 19, array2, n2 + 12);
    }
    
    private static void encode32(final int n, final byte[] array, int n2) {
        array[n2] = (byte)n;
        ++n2;
        array[n2] = (byte)(n >>> 8);
        ++n2;
        array[n2] = (byte)(n >>> 16);
        array[n2 + 1] = (byte)(n >>> 24);
    }
    
    public static void inv(final int[] array, final int[] array2) {
        final int[] create = create();
        final int[] create2 = create();
        powPm5d8(array, create, create2);
        sqr(create2, 3, create2);
        mul(create2, create, array2);
    }
    
    public static int isZero(final int[] array) {
        int i = 0;
        int n = 0;
        while (i < 10) {
            n |= array[i];
            ++i;
        }
        return (n >>> 1 | (n & 0x1)) - 1 >> 31;
    }
    
    public static boolean isZeroVar(final int[] array) {
        return isZero(array) != 0;
    }
    
    public static void mul(final int[] array, int n, final int[] array2) {
        final int n2 = array[0];
        final int n3 = array[1];
        final int n4 = array[2];
        final int n5 = array[3];
        final int n6 = array[4];
        final int n7 = array[5];
        final int n8 = array[6];
        final int n9 = array[7];
        final int n10 = array[8];
        final int n11 = array[9];
        final long n12 = n4;
        final long n13 = n;
        final long n14 = n12 * n13;
        n = (int)n14;
        final long n15 = n6 * n13;
        final int n16 = (int)n15;
        final long n17 = n9 * n13;
        final int n18 = (int)n17;
        final long n19 = n11 * n13;
        final int n20 = (int)n19;
        final long n21 = (n19 >> 25) * 38L + n2 * n13;
        array2[0] = ((int)n21 & 0x3FFFFFF);
        final long n22 = (n15 >> 25) + n7 * n13;
        array2[5] = ((int)n22 & 0x3FFFFFF);
        final long n23 = (n21 >> 26) + n3 * n13;
        array2[1] = ((int)n23 & 0x3FFFFFF);
        final long n24 = (n14 >> 25) + n5 * n13;
        array2[3] = ((int)n24 & 0x3FFFFFF);
        final long n25 = (n22 >> 26) + n8 * n13;
        array2[6] = ((int)n25 & 0x3FFFFFF);
        final long n26 = (n17 >> 25) + n10 * n13;
        array2[8] = ((int)n26 & 0x3FFFFFF);
        array2[2] = (n & 0x1FFFFFF) + (int)(n23 >> 26);
        array2[4] = (n16 & 0x1FFFFFF) + (int)(n24 >> 26);
        array2[7] = (n18 & 0x1FFFFFF) + (int)(n25 >> 26);
        array2[9] = (n20 & 0x1FFFFFF) + (int)(n26 >> 26);
    }
    
    public static void mul(final int[] array, final int[] array2, final int[] array3) {
        final int n = array[0];
        final int n2 = array2[0];
        final int n3 = array[1];
        final int n4 = array2[1];
        final int n5 = array[2];
        final int n6 = array2[2];
        final int n7 = array[3];
        final int n8 = array2[3];
        final int n9 = array[4];
        final int n10 = array2[4];
        final int n11 = array[5];
        final int n12 = array2[5];
        final int n13 = array[6];
        final int n14 = array2[6];
        final int n15 = array[7];
        final int n16 = array2[7];
        final int n17 = array[8];
        final int n18 = array2[8];
        final int n19 = array[9];
        final int n20 = array2[9];
        final long n21 = n;
        final long n22 = n2;
        final long n23 = n4;
        final long n24 = n3;
        final long n25 = n6;
        final long n26 = n5;
        final long n27 = n8;
        final long n28 = n7;
        final long n29 = n10;
        final long n30 = n9;
        final long n31 = (n26 * n25 << 1) + (n21 * n29 + n24 * n27 + n28 * n23 + n22 * n30);
        final long n32 = n11;
        final long n33 = n12;
        final long n34 = n14;
        final long n35 = n13;
        final long n36 = n16;
        final long n37 = n15;
        final long n38 = n18;
        final long n39 = n17;
        final long n40 = n20;
        final long n41 = n19;
        final long n42 = (n37 * n36 << 1) + (n32 * n40 + n35 * n38 + n39 * n34 + n33 * n41);
        final long n43 = n21 * n22 - (n35 * n40 + n37 * n38 + n39 * n36 + n41 * n34) * 76L;
        final long n44 = n21 * n23 + n24 * n22 - ((n37 * n40 + n41 * n36 << 1) + n39 * n38) * 38L;
        final long n45 = n21 * n25 + n24 * n23 + n26 * n22 - (n39 * n40 + n38 * n41) * 38L;
        final long n46 = (n24 * n25 + n26 * n23 << 1) + (n21 * n27 + n28 * n22) - n41 * n40 * 76L;
        final long n47 = (n24 * n29 + n26 * n27 + n28 * n25 + n30 * n23 << 1) - n32 * n33;
        final long n48 = (n26 * n29 + n30 * n25 << 1) + n28 * n27 - (n32 * n34 + n35 * n33);
        final long n49 = n28 * n29 + n30 * n27 - (n32 * n36 + n35 * n34 + n37 * n33);
        final long n50 = (n30 * n29 << 1) - ((n35 * n36 + n37 * n34 << 1) + (n32 * n38 + n39 * n33));
        final long n51 = n + n11;
        final long n52 = n2 + n12;
        final long n53 = n4 + n14;
        final long n54 = n3 + n13;
        final long n55 = n6 + n16;
        final long n56 = n5 + n15;
        final long n57 = n8 + n18;
        final long n58 = n7 + n17;
        final long n59 = n10 + n20;
        final long n60 = n9 + n19;
        final long n61 = n50 + ((n54 * n55 + n56 * n53 << 1) + (n51 * n57 + n58 * n52) - n46);
        final int n62 = (int)n61;
        final long n63 = (n61 >> 26) + ((n56 * n55 << 1) + (n51 * n59 + n54 * n57 + n58 * n53 + n52 * n60) - n31 - n42);
        final int n64 = (int)n63;
        final long n65 = n43 + ((n63 >> 25) + (n54 * n59 + n56 * n57 + n58 * n55 + n60 * n53 << 1) - n47) * 38L;
        array3[0] = ((int)n65 & 0x3FFFFFF);
        final long n66 = (n65 >> 26) + (n44 + ((n56 * n59 + n60 * n55 << 1) + n58 * n57 - n48) * 38L);
        array3[1] = ((int)n66 & 0x3FFFFFF);
        final long n67 = (n66 >> 26) + (n45 + (n58 * n59 + n60 * n57 - n49) * 38L);
        array3[2] = ((int)n67 & 0x1FFFFFF);
        final long n68 = (n67 >> 25) + (n46 + ((n60 * n59 << 1) - n50) * 38L);
        array3[3] = ((int)n68 & 0x3FFFFFF);
        final long n69 = (n68 >> 26) + (n31 + n42 * 38L);
        array3[4] = ((int)n69 & 0x1FFFFFF);
        final long n70 = (n69 >> 25) + (n47 + (n51 * n52 - n43));
        array3[5] = ((int)n70 & 0x3FFFFFF);
        final long n71 = (n70 >> 26) + (n48 + (n51 * n53 + n54 * n52 - n44));
        array3[6] = ((int)n71 & 0x3FFFFFF);
        final long n72 = (n71 >> 26) + (n49 + (n51 * n55 + n54 * n53 + n56 * n52 - n45));
        array3[7] = ((int)n72 & 0x1FFFFFF);
        final long n73 = (n72 >> 25) + (n62 & 0x3FFFFFF);
        array3[8] = ((int)n73 & 0x3FFFFFF);
        array3[9] = (n64 & 0x1FFFFFF) + (int)(n73 >> 26);
    }
    
    public static void negate(final int[] array, final int[] array2) {
        for (int i = 0; i < 10; ++i) {
            array2[i] = -array[i];
        }
    }
    
    public static void normalize(final int[] array) {
        final int n = array[9] >>> 23 & 0x1;
        reduce(array, n);
        reduce(array, -n);
    }
    
    public static void one(final int[] array) {
        int i = 1;
        array[0] = 1;
        while (i < 10) {
            array[i] = 0;
            ++i;
        }
    }
    
    private static void powPm5d8(final int[] array, int[] create, final int[] array2) {
        sqr(array, create);
        mul(array, create, create);
        final int[] create2 = create();
        sqr(create, create2);
        mul(array, create2, create2);
        sqr(create2, 2, create2);
        mul(create, create2, create2);
        create = create();
        sqr(create2, 5, create);
        mul(create2, create, create);
        final int[] create3 = create();
        sqr(create, 5, create3);
        mul(create2, create3, create3);
        sqr(create3, 10, create2);
        mul(create, create2, create2);
        sqr(create2, 25, create);
        mul(create2, create, create);
        sqr(create, 25, create3);
        mul(create2, create3, create3);
        sqr(create3, 50, create2);
        mul(create, create2, create2);
        sqr(create2, 125, create);
        mul(create2, create, create);
        sqr(create, 2, create2);
        mul(create2, array, array2);
    }
    
    private static void reduce(final int[] array, int n) {
        final int n2 = array[9];
        n = ((n2 >> 24) + n) * 19 + array[0];
        array[0] = (n & 0x3FFFFFF);
        n = (n >> 26) + array[1];
        array[1] = (n & 0x3FFFFFF);
        n = (n >> 26) + array[2];
        array[2] = (n & 0x1FFFFFF);
        n = (n >> 25) + array[3];
        array[3] = (n & 0x3FFFFFF);
        n = (n >> 26) + array[4];
        array[4] = (n & 0x1FFFFFF);
        n = (n >> 25) + array[5];
        array[5] = (n & 0x3FFFFFF);
        n = (n >> 26) + array[6];
        array[6] = (n & 0x3FFFFFF);
        n = (n >> 26) + array[7];
        array[7] = (0x1FFFFFF & n);
        n = (n >> 25) + array[8];
        array[8] = (0x3FFFFFF & n);
        array[9] = (n >> 26) + (0xFFFFFF & n2);
    }
    
    public static void sqr(final int[] array, int n, final int[] array2) {
        sqr(array, array2);
        while (true) {
            --n;
            if (n <= 0) {
                break;
            }
            sqr(array2, array2);
        }
    }
    
    public static void sqr(final int[] array, final int[] array2) {
        final int n = array[0];
        final int n2 = array[1];
        final int n3 = array[2];
        final int n4 = array[3];
        final int n5 = array[4];
        final int n6 = array[5];
        final int n7 = array[6];
        final int n8 = array[7];
        final int n9 = array[8];
        final int n10 = array[9];
        final long n11 = n;
        final long n12 = n2 * 2;
        final long n13 = n3 * 2;
        final long n14 = n2;
        final long n15 = n4 * 2;
        final long n16 = n3;
        final long n17 = n5 * 2;
        final long n18 = n16 * n13 + n11 * n17 + n14 * n15;
        final long n19 = n4;
        final long n20 = n5;
        final long n21 = n6;
        final long n22 = n7 * 2;
        final long n23 = n8 * 2;
        final long n24 = n7;
        final long n25 = n9 * 2;
        final long n26 = n8;
        final long n27 = n10 * 2;
        final long n28 = n26 * n23 + n21 * n27 + n24 * n25;
        final long n29 = n9;
        final long n30 = n10;
        final long n31 = n11 * n11 - (n22 * n27 + n25 * n23) * 38L;
        final long n32 = n11 * n12 - (n23 * n27 + n29 * n29) * 38L;
        final long n33 = n11 * n13 + n14 * n14 - n29 * n27 * 38L;
        final long n34 = n12 * n13 + n11 * n15 - n30 * n27 * 38L;
        final long n35 = n12 * n17 + n15 * n13 - n21 * n21;
        final long n36 = n13 * n17 + n19 * n19 - n21 * n22;
        final long n37 = n19 * n17 - (n21 * n23 + n24 * n24);
        final long n38 = n20 * n17 - (n22 * n23 + n21 * n25);
        final int n39 = n2 + n7;
        final int n40 = n3 + n8;
        final int n41 = n4 + n9;
        final int n42 = n5 + n10;
        final long n43 = n + n6;
        final long n44 = n39 * 2;
        final long n45 = n40 * 2;
        final long n46 = n39;
        final long n47 = n41 * 2;
        final long n48 = n40;
        final long n49 = n42 * 2;
        final long n50 = n41;
        final long n51 = n42;
        final long n52 = n38 + (n44 * n45 + n43 * n47 - n34);
        final int n53 = (int)n52;
        final long n54 = (n52 >> 26) + (n48 * n45 + n43 * n49 + n46 * n47 - n18 - n28);
        final int n55 = (int)n54;
        final long n56 = n31 + ((n54 >> 25) + (n44 * n49 + n47 * n45) - n35) * 38L;
        array2[0] = ((int)n56 & 0x3FFFFFF);
        final long n57 = (n56 >> 26) + (n32 + (n45 * n49 + n50 * n50 - n36) * 38L);
        array2[1] = ((int)n57 & 0x3FFFFFF);
        final long n58 = (n57 >> 26) + (n33 + (n50 * n49 - n37) * 38L);
        array2[2] = ((int)n58 & 0x1FFFFFF);
        final long n59 = (n58 >> 25) + (n34 + (n51 * n49 - n38) * 38L);
        array2[3] = ((int)n59 & 0x3FFFFFF);
        final long n60 = (n59 >> 26) + (n18 + 38L * n28);
        array2[4] = ((int)n60 & 0x1FFFFFF);
        final long n61 = (n60 >> 25) + (n35 + (n43 * n43 - n31));
        array2[5] = ((int)n61 & 0x3FFFFFF);
        final long n62 = (n61 >> 26) + (n36 + (n43 * n44 - n32));
        array2[6] = ((int)n62 & 0x3FFFFFF);
        final long n63 = (n62 >> 26) + (n37 + (n43 * n45 + n46 * n46 - n33));
        array2[7] = ((int)n63 & 0x1FFFFFF);
        final long n64 = (n63 >> 25) + (n53 & 0x3FFFFFF);
        array2[8] = ((int)n64 & 0x3FFFFFF);
        array2[9] = (n55 & 0x1FFFFFF) + (int)(n64 >> 26);
    }
    
    public static boolean sqrtRatioVar(final int[] array, final int[] array2, final int[] array3) {
        final int[] create = create();
        final int[] create2 = create();
        mul(array, array2, create);
        sqr(array2, create2);
        mul(create, create2, create);
        sqr(create2, create2);
        mul(create2, create, create2);
        final int[] create3 = create();
        final int[] create4 = create();
        powPm5d8(create2, create3, create4);
        mul(create4, create, create4);
        final int[] create5 = create();
        sqr(create4, create5);
        mul(create5, array2, create5);
        sub(create5, array, create3);
        normalize(create3);
        if (isZeroVar(create3)) {
            copy(create4, 0, array3, 0);
            return true;
        }
        add(create5, array, create3);
        normalize(create3);
        if (isZeroVar(create3)) {
            mul(create4, X25519Field.ROOT_NEG_ONE, array3);
            return true;
        }
        return false;
    }
    
    public static void sub(final int[] array, final int[] array2, final int[] array3) {
        for (int i = 0; i < 10; ++i) {
            array3[i] = array[i] - array2[i];
        }
    }
    
    public static void subOne(final int[] array) {
        --array[0];
    }
    
    public static void zero(final int[] array) {
        for (int i = 0; i < 10; ++i) {
            array[i] = 0;
        }
    }
}
