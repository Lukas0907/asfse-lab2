// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.rfc7748;

public abstract class X448Field
{
    private static final int M28 = 268435455;
    public static final int SIZE = 16;
    private static final long U32 = 4294967295L;
    
    protected X448Field() {
    }
    
    public static void add(final int[] array, final int[] array2, final int[] array3) {
        for (int i = 0; i < 16; ++i) {
            array3[i] = array[i] + array2[i];
        }
    }
    
    public static void addOne(final int[] array) {
        ++array[0];
    }
    
    public static void addOne(final int[] array, final int n) {
        ++array[n];
    }
    
    public static void carry(final int[] array) {
        final int n = array[0];
        final int n2 = array[1];
        final int n3 = array[2];
        final int n4 = array[3];
        final int n5 = array[4];
        final int n6 = array[5];
        final int n7 = array[6];
        final int n8 = array[7];
        final int n9 = array[8];
        final int n10 = array[9];
        final int n11 = array[10];
        final int n12 = array[11];
        final int n13 = array[12];
        final int n14 = array[13];
        final int n15 = array[14];
        final int n16 = array[15];
        final int n17 = n3 + (n2 >>> 28);
        final int n18 = n7 + (n6 >>> 28);
        final int n19 = n11 + (n10 >>> 28);
        final int n20 = n15 + (n14 >>> 28);
        final int n21 = n4 + (n17 >>> 28);
        final int n22 = n8 + (n18 >>> 28);
        final int n23 = n12 + (n19 >>> 28);
        final int n24 = n16 + (n20 >>> 28);
        final int n25 = n24 >>> 28;
        final int n26 = n + n25;
        final int n27 = n5 + (n21 >>> 28);
        final int n28 = n9 + n25 + (n22 >>> 28);
        final int n29 = n13 + (n23 >>> 28);
        array[0] = (n26 & 0xFFFFFFF);
        array[1] = (n2 & 0xFFFFFFF) + (n26 >>> 28);
        array[2] = (n17 & 0xFFFFFFF);
        array[3] = (n21 & 0xFFFFFFF);
        array[4] = (n27 & 0xFFFFFFF);
        array[5] = (n6 & 0xFFFFFFF) + (n27 >>> 28);
        array[6] = (n18 & 0xFFFFFFF);
        array[7] = (n22 & 0xFFFFFFF);
        array[8] = (n28 & 0xFFFFFFF);
        array[9] = (n10 & 0xFFFFFFF) + (n28 >>> 28);
        array[10] = (n19 & 0xFFFFFFF);
        array[11] = (n23 & 0xFFFFFFF);
        array[12] = (n29 & 0xFFFFFFF);
        array[13] = (n14 & 0xFFFFFFF) + (n29 >>> 28);
        array[14] = (n20 & 0xFFFFFFF);
        array[15] = (n24 & 0xFFFFFFF);
    }
    
    public static void cmov(final int n, final int[] array, final int n2, final int[] array2, final int n3) {
        for (int i = 0; i < 16; ++i) {
            final int n4 = n3 + i;
            final int n5 = array2[n4];
            array2[n4] = (n5 ^ ((array[n2 + i] ^ n5) & n));
        }
    }
    
    public static void cnegate(final int n, final int[] array) {
        final int[] create = create();
        sub(create, array, create);
        cmov(-n, create, 0, array, 0);
    }
    
    public static void copy(final int[] array, final int n, final int[] array2, final int n2) {
        for (int i = 0; i < 16; ++i) {
            array2[n2 + i] = array[n + i];
        }
    }
    
    public static int[] create() {
        return new int[16];
    }
    
    public static int[] createTable(final int n) {
        return new int[n * 16];
    }
    
    public static void cswap(final int n, final int[] array, final int[] array2) {
        for (int i = 0; i < 16; ++i) {
            final int n2 = array[i];
            final int n3 = array2[i];
            final int n4 = (n2 ^ n3) & 0 - n;
            array[i] = (n2 ^ n4);
            array2[i] = (n3 ^ n4);
        }
    }
    
    public static void decode(final byte[] array, final int n, final int[] array2) {
        decode56(array, n, array2, 0);
        decode56(array, n + 7, array2, 2);
        decode56(array, n + 14, array2, 4);
        decode56(array, n + 21, array2, 6);
        decode56(array, n + 28, array2, 8);
        decode56(array, n + 35, array2, 10);
        decode56(array, n + 42, array2, 12);
        decode56(array, n + 49, array2, 14);
    }
    
    private static int decode24(final byte[] array, int n) {
        final byte b = array[n];
        ++n;
        return (array[n + 1] & 0xFF) << 16 | ((b & 0xFF) | (array[n] & 0xFF) << 8);
    }
    
    private static int decode32(final byte[] array, int n) {
        final byte b = array[n];
        final int n2 = n + 1;
        n = array[n2];
        final int n3 = n2 + 1;
        return array[n3 + 1] << 24 | ((b & 0xFF) | (n & 0xFF) << 8 | (array[n3] & 0xFF) << 16);
    }
    
    private static void decode56(final byte[] array, int decode24, final int[] array2, final int n) {
        final int decode25 = decode32(array, decode24);
        decode24 = decode24(array, decode24 + 4);
        array2[n] = (0xFFFFFFF & decode25);
        array2[n + 1] = (decode24 << 4 | decode25 >>> 28);
    }
    
    public static void encode(final int[] array, final byte[] array2, final int n) {
        encode56(array, 0, array2, n);
        encode56(array, 2, array2, n + 7);
        encode56(array, 4, array2, n + 14);
        encode56(array, 6, array2, n + 21);
        encode56(array, 8, array2, n + 28);
        encode56(array, 10, array2, n + 35);
        encode56(array, 12, array2, n + 42);
        encode56(array, 14, array2, n + 49);
    }
    
    private static void encode24(final int n, final byte[] array, int n2) {
        array[n2] = (byte)n;
        ++n2;
        array[n2] = (byte)(n >>> 8);
        array[n2 + 1] = (byte)(n >>> 16);
    }
    
    private static void encode32(final int n, final byte[] array, int n2) {
        array[n2] = (byte)n;
        ++n2;
        array[n2] = (byte)(n >>> 8);
        ++n2;
        array[n2] = (byte)(n >>> 16);
        array[n2 + 1] = (byte)(n >>> 24);
    }
    
    private static void encode56(final int[] array, int n, final byte[] array2, final int n2) {
        final int n3 = array[n];
        n = array[n + 1];
        encode32(n << 28 | n3, array2, n2);
        encode24(n >>> 4, array2, n2 + 4);
    }
    
    public static void inv(final int[] array, final int[] array2) {
        final int[] create = create();
        powPm3d4(array, create);
        sqr(create, 2, create);
        mul(create, array, array2);
    }
    
    public static int isZero(final int[] array) {
        int i = 0;
        int n = 0;
        while (i < 16) {
            n |= array[i];
            ++i;
        }
        return (n >>> 1 | (n & 0x1)) - 1 >> 31;
    }
    
    public static boolean isZeroVar(final int[] array) {
        return isZero(array) != 0;
    }
    
    public static void mul(final int[] array, int n, final int[] array2) {
        final int n2 = array[0];
        final int n3 = array[1];
        final int n4 = array[2];
        final int n5 = array[3];
        final int n6 = array[4];
        final int n7 = array[5];
        final int n8 = array[6];
        final int n9 = array[7];
        final int n10 = array[8];
        final int n11 = array[9];
        final int n12 = array[10];
        final int n13 = array[11];
        final int n14 = array[12];
        final int n15 = array[13];
        final int n16 = array[14];
        final int n17 = array[15];
        final long n18 = n3;
        final long n19 = n;
        final long n20 = n18 * n19;
        n = (int)n20;
        final long n21 = n7 * n19;
        final int n22 = (int)n21;
        final long n23 = n11 * n19;
        final int n24 = (int)n23;
        final long n25 = n15 * n19;
        final int n26 = (int)n25;
        final long n27 = (n20 >>> 28) + n4 * n19;
        array2[2] = ((int)n27 & 0xFFFFFFF);
        final long n28 = (n21 >>> 28) + n8 * n19;
        array2[6] = ((int)n28 & 0xFFFFFFF);
        final long n29 = (n23 >>> 28) + n12 * n19;
        array2[10] = ((int)n29 & 0xFFFFFFF);
        final long n30 = (n25 >>> 28) + n16 * n19;
        array2[14] = ((int)n30 & 0xFFFFFFF);
        final long n31 = (n27 >>> 28) + n5 * n19;
        array2[3] = ((int)n31 & 0xFFFFFFF);
        final long n32 = (n28 >>> 28) + n9 * n19;
        array2[7] = ((int)n32 & 0xFFFFFFF);
        final long n33 = (n29 >>> 28) + n13 * n19;
        array2[11] = ((int)n33 & 0xFFFFFFF);
        final long n34 = (n30 >>> 28) + n17 * n19;
        array2[15] = ((int)n34 & 0xFFFFFFF);
        final long n35 = n34 >>> 28;
        final long n36 = (n31 >>> 28) + n6 * n19;
        array2[4] = ((int)n36 & 0xFFFFFFF);
        final long n37 = (n32 >>> 28) + n35 + n10 * n19;
        array2[8] = ((int)n37 & 0xFFFFFFF);
        final long n38 = (n33 >>> 28) + n14 * n19;
        array2[12] = ((int)n38 & 0xFFFFFFF);
        final long n39 = n35 + n2 * n19;
        array2[0] = ((int)n39 & 0xFFFFFFF);
        array2[1] = (n & 0xFFFFFFF) + (int)(n39 >>> 28);
        array2[5] = (n22 & 0xFFFFFFF) + (int)(n36 >>> 28);
        array2[9] = (n24 & 0xFFFFFFF) + (int)(n37 >>> 28);
        array2[13] = (n26 & 0xFFFFFFF) + (int)(n38 >>> 28);
    }
    
    public static void mul(final int[] array, final int[] array2, final int[] array3) {
        final int n = array[0];
        final int n2 = array[1];
        final int n3 = array[2];
        final int n4 = array[3];
        final int n5 = array[4];
        final int n6 = array[5];
        final int n7 = array[6];
        final int n8 = array[7];
        final int n9 = array[8];
        final int n10 = array[9];
        final int n11 = array[10];
        final int n12 = array[11];
        final int n13 = array[12];
        final int n14 = array[13];
        final int n15 = array[14];
        final int n16 = array[15];
        final int n17 = array2[0];
        final int n18 = array2[1];
        final int n19 = array2[2];
        final int n20 = array2[3];
        final int n21 = array2[4];
        final int n22 = array2[5];
        final int n23 = array2[6];
        final int n24 = array2[7];
        final int n25 = array2[8];
        final int n26 = array2[9];
        final int n27 = array2[10];
        final int n28 = array2[11];
        final int n29 = array2[12];
        final int n30 = array2[13];
        final int n31 = array2[14];
        final int n32 = array2[15];
        final long n33 = n;
        final long n34 = n17;
        final long n35 = n33 * n34;
        final long n36 = n8;
        final long n37 = n18;
        final long n38 = n7;
        final long n39 = n19;
        final long n40 = n6;
        final long n41 = n20;
        final long n42 = n5;
        final long n43 = n21;
        final long n44 = n4;
        final long n45 = n22;
        final long n46 = n3;
        final long n47 = n23;
        final long n48 = n2;
        final long n49 = n24;
        final long n50 = n9;
        final long n51 = n25;
        final long n52 = n16;
        final long n53 = n26;
        final long n54 = n15;
        final long n55 = n27;
        final long n56 = n14;
        final long n57 = n28;
        final long n58 = n13;
        final long n59 = n29;
        final long n60 = n12;
        final long n61 = n30;
        final long n62 = n11;
        final long n63 = n31;
        final long n64 = n10;
        final long n65 = n32;
        final long n66 = n + n9;
        final long n67 = n17 + n25;
        final long n68 = n8 + n16;
        final long n69 = n18 + n26;
        final long n70 = n7 + n15;
        final long n71 = n19 + n27;
        final long n72 = n6 + n14;
        final long n73 = n20 + n28;
        final long n74 = n5 + n13;
        final long n75 = n21 + n29;
        final long n76 = n4 + n12;
        final long n77 = n22 + n30;
        final long n78 = n3 + n11;
        final long n79 = n23 + n31;
        final long n80 = n2 + n10;
        final long n81 = n24 + n32;
        final long n82 = n68 * n69 + n70 * n71 + n72 * n73 + n74 * n75 + n76 * n77 + n78 * n79 + n80 * n81;
        final long n83 = n35 + n50 * n51 + n82 - (n36 * n37 + n38 * n39 + n40 * n41 + n42 * n43 + n44 * n45 + n46 * n47 + n48 * n49);
        final int n84 = (int)n83;
        final long n85 = n52 * n53 + n54 * n55 + n56 * n57 + n58 * n59 + n60 * n61 + n62 * n63 + n64 * n65 + n66 * n67 - n35 + n82;
        final int n86 = (int)n85;
        final long n87 = n48 * n34 + n33 * n37;
        final long n88 = n68 * n71 + n70 * n73 + n72 * n75 + n74 * n77 + n76 * n79 + n78 * n81;
        final long n89 = (n83 >>> 28) + (n87 + (n64 * n51 + n50 * n53) + n88 - (n36 * n39 + n38 * n41 + n40 * n43 + n42 * n45 + n44 * n47 + n46 * n49));
        final int n90 = (int)n89;
        final long n91 = (n85 >>> 28) + (n52 * n55 + n54 * n57 + n56 * n59 + n58 * n61 + n60 * n63 + n62 * n65 + (n80 * n67 + n66 * n69) - n87 + n88);
        final int n92 = (int)n91;
        final long n93 = n46 * n34 + n48 * n37 + n33 * n39;
        final long n94 = n68 * n73 + n70 * n75 + n72 * n77 + n74 * n79 + n76 * n81;
        final long n95 = (n89 >>> 28) + (n93 + (n62 * n51 + n64 * n53 + n50 * n55) + n94 - (n36 * n41 + n38 * n43 + n40 * n45 + n42 * n47 + n44 * n49));
        final int n96 = (int)n95;
        final long n97 = (n91 >>> 28) + (n52 * n57 + n54 * n59 + n56 * n61 + n58 * n63 + n60 * n65 + (n78 * n67 + n80 * n69 + n66 * n71) - n93 + n94);
        final int n98 = (int)n97;
        final long n99 = n44 * n34 + n46 * n37 + n48 * n39 + n33 * n41;
        final long n100 = n68 * n75 + n70 * n77 + n72 * n79 + n74 * n81;
        final long n101 = (n95 >>> 28) + (n99 + (n60 * n51 + n62 * n53 + n64 * n55 + n50 * n57) + n100 - (n36 * n43 + n38 * n45 + n40 * n47 + n42 * n49));
        final int n102 = (int)n101;
        final long n103 = (n97 >>> 28) + (n52 * n59 + n54 * n61 + n56 * n63 + n58 * n65 + (n76 * n67 + n78 * n69 + n80 * n71 + n66 * n73) - n99 + n100);
        final int n104 = (int)n103;
        final long n105 = n42 * n34 + n44 * n37 + n46 * n39 + n48 * n41 + n33 * n43;
        final long n106 = n68 * n77 + n70 * n79 + n72 * n81;
        final long n107 = (n101 >>> 28) + (n105 + (n58 * n51 + n60 * n53 + n62 * n55 + n64 * n57 + n50 * n59) + n106 - (n36 * n45 + n38 * n47 + n40 * n49));
        final int n108 = (int)n107;
        final long n109 = (n103 >>> 28) + (n52 * n61 + n54 * n63 + n56 * n65 + (n74 * n67 + n76 * n69 + n78 * n71 + n80 * n73 + n66 * n75) - n105 + n106);
        final int n110 = (int)n109;
        final long n111 = n40 * n34 + n42 * n37 + n44 * n39 + n46 * n41 + n48 * n43 + n33 * n45;
        final long n112 = n68 * n79 + n70 * n81;
        final long n113 = (n107 >>> 28) + (n111 + (n56 * n51 + n58 * n53 + n60 * n55 + n62 * n57 + n64 * n59 + n50 * n61) + n112 - (n36 * n47 + n38 * n49));
        final int n114 = (int)n113;
        final long n115 = (n109 >>> 28) + (n52 * n63 + n54 * n65 + (n72 * n67 + n74 * n69 + n76 * n71 + n78 * n73 + n80 * n75 + n66 * n77) - n111 + n112);
        final int n116 = (int)n115;
        final long n117 = n38 * n34 + n40 * n37 + n42 * n39 + n44 * n41 + n46 * n43 + n48 * n45 + n33 * n47;
        final long n118 = n68 * n81;
        final long n119 = (n113 >>> 28) + (n117 + (n54 * n51 + n56 * n53 + n58 * n55 + n60 * n57 + n62 * n59 + n64 * n61 + n50 * n63) + n118 - n36 * n49);
        final int n120 = (int)n119;
        final long n121 = (n115 >>> 28) + (n52 * n65 + (n70 * n67 + n72 * n69 + n74 * n71 + n76 * n73 + n78 * n75 + n80 * n77 + n66 * n79) - n117 + n118);
        final int n122 = (int)n121;
        final long n123 = n34 * n36 + n38 * n37 + n39 * n40 + n42 * n41 + n44 * n43 + n46 * n45 + n48 * n47 + n33 * n49;
        final long n124 = (n119 >>> 28) + (n123 + (n52 * n51 + n53 * n54 + n56 * n55 + n58 * n57 + n60 * n59 + n62 * n61 + n64 * n63 + n50 * n65));
        final int n125 = (int)n124;
        final long n126 = (n121 >>> 28) + (n68 * n67 + n70 * n69 + n72 * n71 + n74 * n73 + n76 * n75 + n78 * n77 + n80 * n79 + n66 * n81 - n123);
        final int n127 = (int)n126;
        final long n128 = n126 >>> 28;
        final long n129 = (n124 >>> 28) + n128 + (n86 & 0xFFFFFFF);
        final int n130 = (int)n129;
        final long n131 = n128 + (n84 & 0xFFFFFFF);
        final int n132 = (int)n131;
        final int n133 = (int)(n129 >>> 28);
        final int n134 = (int)(n131 >>> 28);
        array3[0] = (n132 & 0xFFFFFFF);
        array3[1] = (n90 & 0xFFFFFFF) + n134;
        array3[2] = (n96 & 0xFFFFFFF);
        array3[3] = (n102 & 0xFFFFFFF);
        array3[4] = (n108 & 0xFFFFFFF);
        array3[5] = (n114 & 0xFFFFFFF);
        array3[6] = (n120 & 0xFFFFFFF);
        array3[7] = (n125 & 0xFFFFFFF);
        array3[8] = (n130 & 0xFFFFFFF);
        array3[9] = (n92 & 0xFFFFFFF) + n133;
        array3[10] = (n98 & 0xFFFFFFF);
        array3[11] = (n104 & 0xFFFFFFF);
        array3[12] = (n110 & 0xFFFFFFF);
        array3[13] = (n116 & 0xFFFFFFF);
        array3[14] = (n122 & 0xFFFFFFF);
        array3[15] = (n127 & 0xFFFFFFF);
    }
    
    public static void negate(final int[] array, final int[] array2) {
        sub(create(), array, array2);
    }
    
    public static void normalize(final int[] array) {
        reduce(array, 1);
        reduce(array, -1);
    }
    
    public static void one(final int[] array) {
        int i = 1;
        array[0] = 1;
        while (i < 16) {
            array[i] = 0;
            ++i;
        }
    }
    
    private static void powPm3d4(int[] create, final int[] array) {
        final int[] create2 = create();
        sqr(create, create2);
        mul(create, create2, create2);
        final int[] create3 = create();
        sqr(create2, create3);
        mul(create, create3, create3);
        final int[] create4 = create();
        sqr(create3, 3, create4);
        mul(create3, create4, create4);
        final int[] create5 = create();
        sqr(create4, 3, create5);
        mul(create3, create5, create5);
        final int[] create6 = create();
        sqr(create5, 9, create6);
        mul(create5, create6, create6);
        final int[] create7 = create();
        sqr(create6, create7);
        mul(create, create7, create7);
        final int[] create8 = create();
        sqr(create7, 18, create8);
        mul(create6, create8, create8);
        final int[] create9 = create();
        sqr(create8, 37, create9);
        mul(create8, create9, create9);
        final int[] create10 = create();
        sqr(create9, 37, create10);
        mul(create8, create10, create10);
        final int[] create11 = create();
        sqr(create10, 111, create11);
        mul(create10, create11, create11);
        final int[] create12 = create();
        sqr(create11, create12);
        mul(create, create12, create12);
        create = create();
        sqr(create12, 223, create);
        mul(create, create11, array);
    }
    
    private static void reduce(final int[] array, int i) {
        final int n = array[15];
        int n2 = (n >> 28) + i;
        array[8] += n2;
        int n3;
        for (i = 0; i < 15; ++i) {
            n3 = n2 + array[i];
            array[i] = (n3 & 0xFFFFFFF);
            n2 = n3 >> 28;
        }
        array[15] = (n & 0xFFFFFFF) + n2;
    }
    
    public static void sqr(final int[] array, int n, final int[] array2) {
        sqr(array, array2);
        while (true) {
            --n;
            if (n <= 0) {
                break;
            }
            sqr(array2, array2);
        }
    }
    
    public static void sqr(final int[] array, final int[] array2) {
        final int n = array[0];
        final int n2 = array[1];
        final int n3 = array[2];
        final int n4 = array[3];
        final int n5 = array[4];
        final int n6 = array[5];
        final int n7 = array[6];
        final int n8 = array[7];
        final int n9 = array[8];
        final int n10 = array[9];
        final int n11 = array[10];
        final int n12 = array[11];
        final int n13 = array[12];
        final int n14 = array[13];
        final int n15 = array[14];
        final int n16 = array[15];
        final int n17 = n + n9;
        final int n18 = n2 + n10;
        final int n19 = n3 + n11;
        final int n20 = n4 + n12;
        final int n21 = n5 + n13;
        final int n22 = n6 + n14;
        final int n23 = n7 + n15;
        final long n24 = n;
        final long n25 = n24 * n24;
        final long n26 = n8;
        final long n27 = n2 * 2;
        final long n28 = n7;
        final long n29 = n3 * 2;
        final long n30 = n6;
        final long n31 = n4 * 2;
        final long n32 = n5;
        final long n33 = n9;
        final long n34 = n16;
        final long n35 = n10 * 2;
        final long n36 = n15;
        final long n37 = n11 * 2;
        final long n38 = n14;
        final long n39 = n12 * 2;
        final long n40 = n13;
        final long n41 = n17;
        final long n42 = n8 + n16;
        final long n43 = (long)(n18 * 2) & 0xFFFFFFFFL;
        final long n44 = n23;
        final long n45 = (long)(n19 * 2) & 0xFFFFFFFFL;
        final long n46 = n22;
        final long n47 = (long)(n20 * 2) & 0xFFFFFFFFL;
        final long n48 = n21;
        final long n49 = n42 * n43 + n44 * n45 + n46 * n47 + n48 * n48;
        final long n50 = n25 + n33 * n33 + n49 - (n26 * n27 + n28 * n29 + n30 * n31 + n32 * n32);
        final int n51 = (int)n50;
        final long n52 = n34 * n35 + n36 * n37 + n38 * n39 + n40 * n40 + n41 * n41 - n25 + n49;
        final int n53 = (int)n52;
        final long n54 = n2;
        final long n55 = n * 2;
        final long n56 = n54 * n55;
        final long n57 = n5 * 2;
        final long n58 = n10;
        final long n59 = n9 * 2;
        final long n60 = n13 * 2;
        final long n61 = n18;
        final long n62 = (long)(n17 * 2) & 0xFFFFFFFFL;
        final long n63 = (long)(n21 * 2) & 0xFFFFFFFFL;
        final long n64 = n42 * n45 + n44 * n47 + n46 * n63;
        final long n65 = (n50 >>> 28) + (n56 + n58 * n59 + n64 - (n26 * n29 + n28 * n31 + n30 * n57));
        final int n66 = (int)n65;
        final long n67 = (n52 >>> 28) + (n34 * n37 + n36 * n39 + n38 * n60 + n61 * n62 - n56 + n64);
        final int n68 = (int)n67;
        final long n69 = n3;
        final long n70 = n69 * n55 + n54 * n54;
        final long n71 = n11;
        final long n72 = n19;
        final long n73 = n42 * n47 + n44 * n63 + n46 * n46;
        final long n74 = (n65 >>> 28) + (n70 + (n71 * n59 + n58 * n58) + n73 - (n26 * n31 + n28 * n57 + n30 * n30));
        final int n75 = (int)n74;
        final long n76 = (n67 >>> 28) + (n34 * n39 + n36 * n60 + n38 * n38 + (n72 * n62 + n61 * n61) - n70 + n73);
        final int n77 = (int)n76;
        final long n78 = n4;
        final long n79 = n78 * n55 + n69 * n27;
        final long n80 = n6 * 2;
        final long n81 = n12;
        final long n82 = n14 * 2;
        final long n83 = n20;
        final long n84 = (long)(n22 * 2) & 0xFFFFFFFFL;
        final long n85 = n63 * n42 + n44 * n84;
        final long n86 = (n74 >>> 28) + (n79 + (n81 * n59 + n71 * n35) + n85 - (n26 * n57 + n28 * n80));
        final int n87 = (int)n86;
        final long n88 = (n76 >>> 28) + (n34 * n60 + n36 * n82 + (n83 * n62 + n72 * n43) - n79 + n85);
        final int n89 = (int)n88;
        final long n90 = n32 * n55 + n78 * n27 + n69 * n69;
        final long n91 = n42 * n84 + n44 * n44;
        final long n92 = (n86 >>> 28) + (n90 + (n40 * n59 + n81 * n35 + n71 * n71) + n91 - (n26 * n80 + n28 * n28));
        final int n93 = (int)n92;
        final long n94 = (n88 >>> 28) + (n34 * n82 + n36 * n36 + (n48 * n62 + n83 * n43 + n72 * n72) - n90 + n91);
        final int n95 = (int)n94;
        final long n96 = n30 * n55 + n32 * n27 + n78 * n29;
        final long n97 = n7 * 2;
        final long n98 = n15 * 2;
        final long n99 = ((long)(n23 * 2) & 0xFFFFFFFFL) * n42;
        final long n100 = (n92 >>> 28) + (n96 + (n38 * n59 + n40 * n35 + n81 * n37) + n99 - n97 * n26);
        final int n101 = (int)n100;
        final long n102 = (n94 >>> 28) + (n98 * n34 + (n46 * n62 + n48 * n43 + n83 * n45) - n96 + n99);
        final int n103 = (int)n102;
        final long n104 = n28 * n55 + n30 * n27 + n32 * n29 + n78 * n78;
        final long n105 = n42 * n42;
        final long n106 = (n100 >>> 28) + (n104 + (n36 * n59 + n38 * n35 + n40 * n37 + n81 * n81) + n105 - n26 * n26);
        final int n107 = (int)n106;
        final long n108 = (n102 >>> 28) + (n34 * n34 + (n44 * n62 + n46 * n43 + n48 * n45 + n83 * n83) - n104 + n105);
        final int n109 = (int)n108;
        final long n110 = n26 * n55 + n28 * n27 + n30 * n29 + n32 * n31;
        final long n111 = (n106 >>> 28) + (n59 * n34 + n36 * n35 + n38 * n37 + n40 * n39 + n110);
        final int n112 = (int)n111;
        final long n113 = (n108 >>> 28) + (n62 * n42 + n44 * n43 + n46 * n45 + n48 * n47 - n110);
        final int n114 = (int)n113;
        final long n115 = n113 >>> 28;
        final long n116 = (n111 >>> 28) + n115 + (n53 & 0xFFFFFFF);
        final int n117 = (int)n116;
        final long n118 = n115 + (n51 & 0xFFFFFFF);
        final int n119 = (int)n118;
        final int n120 = (int)(n116 >>> 28);
        final int n121 = (int)(n118 >>> 28);
        array2[0] = (n119 & 0xFFFFFFF);
        array2[1] = (n66 & 0xFFFFFFF) + n121;
        array2[2] = (n75 & 0xFFFFFFF);
        array2[3] = (n87 & 0xFFFFFFF);
        array2[4] = (n93 & 0xFFFFFFF);
        array2[5] = (n101 & 0xFFFFFFF);
        array2[6] = (n107 & 0xFFFFFFF);
        array2[7] = (n112 & 0xFFFFFFF);
        array2[8] = (n117 & 0xFFFFFFF);
        array2[9] = (n68 & 0xFFFFFFF) + n120;
        array2[10] = (n77 & 0xFFFFFFF);
        array2[11] = (n89 & 0xFFFFFFF);
        array2[12] = (n95 & 0xFFFFFFF);
        array2[13] = (n103 & 0xFFFFFFF);
        array2[14] = (n109 & 0xFFFFFFF);
        array2[15] = (n114 & 0xFFFFFFF);
    }
    
    public static boolean sqrtRatioVar(final int[] array, final int[] array2, final int[] array3) {
        final int[] create = create();
        final int[] create2 = create();
        sqr(array, create);
        mul(create, array2, create);
        sqr(create, create2);
        mul(create, array, create);
        mul(create2, array, create2);
        mul(create2, array2, create2);
        final int[] create3 = create();
        powPm3d4(create2, create3);
        mul(create3, create, create3);
        final int[] create4 = create();
        sqr(create3, create4);
        mul(create4, array2, create4);
        sub(array, create4, create4);
        normalize(create4);
        if (isZeroVar(create4)) {
            copy(create3, 0, array3, 0);
            return true;
        }
        return false;
    }
    
    public static void sub(final int[] array, final int[] array2, final int[] array3) {
        final int n = array[0];
        final int n2 = array[1];
        final int n3 = array[2];
        final int n4 = array[3];
        final int n5 = array[4];
        final int n6 = array[5];
        final int n7 = array[6];
        final int n8 = array[7];
        final int n9 = array[8];
        final int n10 = array[9];
        final int n11 = array[10];
        final int n12 = array[11];
        final int n13 = array[12];
        final int n14 = array[13];
        final int n15 = array[14];
        final int n16 = array[15];
        final int n17 = array2[0];
        final int n18 = array2[1];
        final int n19 = array2[2];
        final int n20 = array2[3];
        final int n21 = array2[4];
        final int n22 = array2[5];
        final int n23 = array2[6];
        final int n24 = array2[7];
        final int n25 = array2[8];
        final int n26 = array2[9];
        final int n27 = array2[10];
        final int n28 = array2[11];
        final int n29 = array2[12];
        final int n30 = array2[13];
        final int n31 = array2[14];
        final int n32 = array2[15];
        final int n33 = n2 + 536870910 - n18;
        final int n34 = n6 + 536870910 - n22;
        final int n35 = n10 + 536870910 - n26;
        final int n36 = n14 + 536870910 - n30;
        final int n37 = n3 + 536870910 - n19 + (n33 >>> 28);
        final int n38 = n7 + 536870910 - n23 + (n34 >>> 28);
        final int n39 = n11 + 536870910 - n27 + (n35 >>> 28);
        final int n40 = n15 + 536870910 - n31 + (n36 >>> 28);
        final int n41 = n4 + 536870910 - n20 + (n37 >>> 28);
        final int n42 = n8 + 536870910 - n24 + (n38 >>> 28);
        final int n43 = n12 + 536870910 - n28 + (n39 >>> 28);
        final int n44 = n16 + 536870910 - n32 + (n40 >>> 28);
        final int n45 = n44 >>> 28;
        final int n46 = n + 536870910 - n17 + n45;
        final int n47 = n5 + 536870910 - n21 + (n41 >>> 28);
        final int n48 = n9 + 536870908 - n25 + n45 + (n42 >>> 28);
        final int n49 = n13 + 536870910 - n29 + (n43 >>> 28);
        array3[0] = (n46 & 0xFFFFFFF);
        array3[1] = (n33 & 0xFFFFFFF) + (n46 >>> 28);
        array3[2] = (n37 & 0xFFFFFFF);
        array3[3] = (n41 & 0xFFFFFFF);
        array3[4] = (n47 & 0xFFFFFFF);
        array3[5] = (n34 & 0xFFFFFFF) + (n47 >>> 28);
        array3[6] = (n38 & 0xFFFFFFF);
        array3[7] = (n42 & 0xFFFFFFF);
        array3[8] = (n48 & 0xFFFFFFF);
        array3[9] = (n35 & 0xFFFFFFF) + (n48 >>> 28);
        array3[10] = (n39 & 0xFFFFFFF);
        array3[11] = (n43 & 0xFFFFFFF);
        array3[12] = (n49 & 0xFFFFFFF);
        array3[13] = (n36 & 0xFFFFFFF) + (n49 >>> 28);
        array3[14] = (n40 & 0xFFFFFFF);
        array3[15] = (n44 & 0xFFFFFFF);
    }
    
    public static void subOne(final int[] array) {
        final int[] create = create();
        create[0] = 1;
        sub(array, create, array);
    }
    
    public static void zero(final int[] array) {
        for (int i = 0; i < 16; ++i) {
            array[i] = 0;
        }
    }
}
