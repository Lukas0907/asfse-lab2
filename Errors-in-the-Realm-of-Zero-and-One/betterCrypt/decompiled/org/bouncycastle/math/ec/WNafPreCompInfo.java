// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec;

public class WNafPreCompInfo implements PreCompInfo
{
    protected int confWidth;
    protected ECPoint[] preComp;
    protected ECPoint[] preCompNeg;
    volatile int promotionCountdown;
    protected ECPoint twice;
    protected int width;
    
    public WNafPreCompInfo() {
        this.promotionCountdown = 4;
        this.confWidth = -1;
        this.preComp = null;
        this.preCompNeg = null;
        this.twice = null;
        this.width = -1;
    }
    
    int decrementPromotionCountdown() {
        int promotionCountdown;
        final int n = promotionCountdown = this.promotionCountdown;
        if (n > 0) {
            promotionCountdown = n - 1;
            this.promotionCountdown = promotionCountdown;
        }
        return promotionCountdown;
    }
    
    public int getConfWidth() {
        return this.confWidth;
    }
    
    public ECPoint[] getPreComp() {
        return this.preComp;
    }
    
    public ECPoint[] getPreCompNeg() {
        return this.preCompNeg;
    }
    
    int getPromotionCountdown() {
        return this.promotionCountdown;
    }
    
    public ECPoint getTwice() {
        return this.twice;
    }
    
    public int getWidth() {
        return this.width;
    }
    
    public boolean isPromoted() {
        return this.promotionCountdown <= 0;
    }
    
    public void setConfWidth(final int confWidth) {
        this.confWidth = confWidth;
    }
    
    public void setPreComp(final ECPoint[] preComp) {
        this.preComp = preComp;
    }
    
    public void setPreCompNeg(final ECPoint[] preCompNeg) {
        this.preCompNeg = preCompNeg;
    }
    
    void setPromotionCountdown(final int promotionCountdown) {
        this.promotionCountdown = promotionCountdown;
    }
    
    public void setTwice(final ECPoint twice) {
        this.twice = twice;
    }
    
    public void setWidth(final int width) {
        this.width = width;
    }
}
