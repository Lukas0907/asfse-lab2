// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec;

public class ScaleXNegateYPointMap implements ECPointMap
{
    protected final ECFieldElement scale;
    
    public ScaleXNegateYPointMap(final ECFieldElement scale) {
        this.scale = scale;
    }
    
    @Override
    public ECPoint map(final ECPoint ecPoint) {
        return ecPoint.scaleXNegateY(this.scale);
    }
}
