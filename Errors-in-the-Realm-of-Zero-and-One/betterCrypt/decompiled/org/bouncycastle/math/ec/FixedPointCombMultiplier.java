// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec;

import org.bouncycastle.math.raw.Nat;
import java.math.BigInteger;

public class FixedPointCombMultiplier extends AbstractECMultiplier
{
    @Override
    protected ECPoint multiplyPositive(ECPoint ecPoint, final BigInteger bigInteger) {
        final ECCurve curve = ecPoint.getCurve();
        final int combSize = FixedPointUtil.getCombSize(curve);
        if (bigInteger.bitLength() <= combSize) {
            final FixedPointPreCompInfo precompute = FixedPointUtil.precompute(ecPoint);
            final ECLookupTable lookupTable = precompute.getLookupTable();
            final int width = precompute.getWidth();
            final int n = (combSize + width - 1) / width;
            ecPoint = curve.getInfinity();
            final int n2 = width * n;
            final int[] fromBigInteger = Nat.fromBigInteger(n2, bigInteger);
            for (int i = 0; i < n; ++i) {
                int j = n2 - 1 - i;
                int n3 = 0;
                while (j >= 0) {
                    final int n4 = fromBigInteger[j >>> 5] >>> (j & 0x1F);
                    n3 = ((n3 ^ n4 >>> 1) << 1 ^ n4);
                    j -= n;
                }
                ecPoint = ecPoint.twicePlus(lookupTable.lookup(n3));
            }
            return ecPoint.add(precompute.getOffset());
        }
        throw new IllegalStateException("fixed-point comb doesn't support scalars larger than the curve order");
    }
}
