// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.endo;

import org.bouncycastle.math.ec.PreCompInfo;
import org.bouncycastle.math.ec.PreCompCallback;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.math.ec.ECConstants;
import java.math.BigInteger;

public abstract class EndoUtil
{
    public static final String PRECOMP_NAME = "bc_endo";
    
    private static BigInteger calculateB(BigInteger bigInteger, BigInteger negate, final int n) {
        final boolean b = negate.signum() < 0;
        bigInteger = bigInteger.multiply(negate.abs());
        final boolean testBit = bigInteger.testBit(n - 1);
        negate = (bigInteger = bigInteger.shiftRight(n));
        if (testBit) {
            bigInteger = negate.add(ECConstants.ONE);
        }
        negate = bigInteger;
        if (b) {
            negate = bigInteger.negate();
        }
        return negate;
    }
    
    public static BigInteger[] decomposeScalar(final ScalarSplitParameters scalarSplitParameters, final BigInteger bigInteger) {
        final int bits = scalarSplitParameters.getBits();
        final BigInteger calculateB = calculateB(bigInteger, scalarSplitParameters.getG1(), bits);
        final BigInteger calculateB2 = calculateB(bigInteger, scalarSplitParameters.getG2(), bits);
        return new BigInteger[] { bigInteger.subtract(calculateB.multiply(scalarSplitParameters.getV1A()).add(calculateB2.multiply(scalarSplitParameters.getV2A()))), calculateB.multiply(scalarSplitParameters.getV1B()).add(calculateB2.multiply(scalarSplitParameters.getV2B())).negate() };
    }
    
    public static ECPoint mapPoint(final ECEndomorphism ecEndomorphism, final ECPoint ecPoint) {
        return ((EndoPreCompInfo)ecPoint.getCurve().precompute(ecPoint, "bc_endo", new PreCompCallback() {
            private boolean checkExisting(final EndoPreCompInfo endoPreCompInfo, final ECEndomorphism ecEndomorphism) {
                return endoPreCompInfo != null && endoPreCompInfo.getEndomorphism() == ecEndomorphism && endoPreCompInfo.getMappedPoint() != null;
            }
            
            @Override
            public PreCompInfo precompute(final PreCompInfo preCompInfo) {
                EndoPreCompInfo endoPreCompInfo;
                if (preCompInfo instanceof EndoPreCompInfo) {
                    endoPreCompInfo = (EndoPreCompInfo)preCompInfo;
                }
                else {
                    endoPreCompInfo = null;
                }
                if (this.checkExisting(endoPreCompInfo, ecEndomorphism)) {
                    return endoPreCompInfo;
                }
                final ECPoint map = ecEndomorphism.getPointMap().map(ecPoint);
                final EndoPreCompInfo endoPreCompInfo2 = new EndoPreCompInfo();
                endoPreCompInfo2.setEndomorphism(ecEndomorphism);
                endoPreCompInfo2.setMappedPoint(map);
                return endoPreCompInfo2;
            }
        })).getMappedPoint();
    }
}
