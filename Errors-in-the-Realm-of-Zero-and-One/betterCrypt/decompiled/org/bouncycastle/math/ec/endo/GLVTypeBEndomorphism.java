// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.endo;

import java.math.BigInteger;
import org.bouncycastle.math.ec.ScaleXPointMap;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.math.ec.ECPointMap;

public class GLVTypeBEndomorphism implements GLVEndomorphism
{
    protected final GLVTypeBParameters parameters;
    protected final ECPointMap pointMap;
    
    public GLVTypeBEndomorphism(final ECCurve ecCurve, final GLVTypeBParameters parameters) {
        this.parameters = parameters;
        this.pointMap = new ScaleXPointMap(ecCurve.fromBigInteger(parameters.getBeta()));
    }
    
    @Override
    public BigInteger[] decomposeScalar(final BigInteger bigInteger) {
        return EndoUtil.decomposeScalar(this.parameters.getSplitParams(), bigInteger);
    }
    
    @Override
    public ECPointMap getPointMap() {
        return this.pointMap;
    }
    
    @Override
    public boolean hasEfficientPointMap() {
        return true;
    }
}
