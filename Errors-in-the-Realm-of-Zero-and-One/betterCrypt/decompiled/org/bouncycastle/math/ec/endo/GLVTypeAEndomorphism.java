// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.endo;

import java.math.BigInteger;
import org.bouncycastle.math.ec.ScaleYNegateXPointMap;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.math.ec.ECPointMap;

public class GLVTypeAEndomorphism implements GLVEndomorphism
{
    protected final GLVTypeAParameters parameters;
    protected final ECPointMap pointMap;
    
    public GLVTypeAEndomorphism(final ECCurve ecCurve, final GLVTypeAParameters parameters) {
        this.parameters = parameters;
        this.pointMap = new ScaleYNegateXPointMap(ecCurve.fromBigInteger(parameters.getI()));
    }
    
    @Override
    public BigInteger[] decomposeScalar(final BigInteger bigInteger) {
        return EndoUtil.decomposeScalar(this.parameters.getSplitParams(), bigInteger);
    }
    
    @Override
    public ECPointMap getPointMap() {
        return this.pointMap;
    }
    
    @Override
    public boolean hasEfficientPointMap() {
        return true;
    }
}
