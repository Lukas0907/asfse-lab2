// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.endo;

import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.math.ec.PreCompInfo;

public class EndoPreCompInfo implements PreCompInfo
{
    protected ECEndomorphism endomorphism;
    protected ECPoint mappedPoint;
    
    public ECEndomorphism getEndomorphism() {
        return this.endomorphism;
    }
    
    public ECPoint getMappedPoint() {
        return this.mappedPoint;
    }
    
    public void setEndomorphism(final ECEndomorphism endomorphism) {
        this.endomorphism = endomorphism;
    }
    
    public void setMappedPoint(final ECPoint mappedPoint) {
        this.mappedPoint = mappedPoint;
    }
}
