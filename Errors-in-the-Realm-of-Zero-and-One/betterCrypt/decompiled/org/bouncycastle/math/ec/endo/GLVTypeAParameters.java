// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.endo;

import java.math.BigInteger;

public class GLVTypeAParameters
{
    protected final BigInteger i;
    protected final BigInteger lambda;
    protected final ScalarSplitParameters splitParams;
    
    public GLVTypeAParameters(final BigInteger i, final BigInteger lambda, final ScalarSplitParameters splitParams) {
        this.i = i;
        this.lambda = lambda;
        this.splitParams = splitParams;
    }
    
    public BigInteger getI() {
        return this.i;
    }
    
    public BigInteger getLambda() {
        return this.lambda;
    }
    
    public ScalarSplitParameters getSplitParams() {
        return this.splitParams;
    }
}
