// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.ec.endo;

import java.math.BigInteger;

public class GLVTypeBParameters
{
    protected final BigInteger beta;
    protected final BigInteger lambda;
    protected final ScalarSplitParameters splitParams;
    
    public GLVTypeBParameters(final BigInteger beta, final BigInteger lambda, final ScalarSplitParameters splitParams) {
        this.beta = beta;
        this.lambda = lambda;
        this.splitParams = splitParams;
    }
    
    public GLVTypeBParameters(final BigInteger beta, final BigInteger lambda, final BigInteger[] array, final BigInteger[] array2, final BigInteger bigInteger, final BigInteger bigInteger2, final int n) {
        this.beta = beta;
        this.lambda = lambda;
        this.splitParams = new ScalarSplitParameters(array, array2, bigInteger, bigInteger2, n);
    }
    
    public BigInteger getBeta() {
        return this.beta;
    }
    
    public int getBits() {
        return this.getSplitParams().getBits();
    }
    
    public BigInteger getG1() {
        return this.getSplitParams().getG1();
    }
    
    public BigInteger getG2() {
        return this.getSplitParams().getG2();
    }
    
    public BigInteger getLambda() {
        return this.lambda;
    }
    
    public ScalarSplitParameters getSplitParams() {
        return this.splitParams;
    }
    
    public BigInteger getV1A() {
        return this.getSplitParams().getV1A();
    }
    
    public BigInteger getV1B() {
        return this.getSplitParams().getV1B();
    }
    
    public BigInteger getV2A() {
        return this.getSplitParams().getV2A();
    }
    
    public BigInteger getV2B() {
        return this.getSplitParams().getV2B();
    }
}
