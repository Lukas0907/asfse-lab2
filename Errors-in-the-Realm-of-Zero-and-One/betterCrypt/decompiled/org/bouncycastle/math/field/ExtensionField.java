// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.field;

public interface ExtensionField extends FiniteField
{
    int getDegree();
    
    FiniteField getSubfield();
}
