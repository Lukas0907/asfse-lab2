// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.field;

public interface PolynomialExtensionField extends ExtensionField
{
    Polynomial getMinimalPolynomial();
}
