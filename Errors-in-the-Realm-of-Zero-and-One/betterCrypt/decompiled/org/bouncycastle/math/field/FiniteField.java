// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.math.field;

import java.math.BigInteger;

public interface FiniteField
{
    BigInteger getCharacteristic();
    
    int getDimension();
}
