// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.parsers;

import java.io.IOException;
import org.bouncycastle.crypto.params.DHPublicKeyParameters;
import java.math.BigInteger;
import org.bouncycastle.util.io.Streams;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import java.io.InputStream;
import org.bouncycastle.crypto.params.DHParameters;
import org.bouncycastle.crypto.KeyParser;

public class DHIESPublicKeyParser implements KeyParser
{
    private DHParameters dhParams;
    
    public DHIESPublicKeyParser(final DHParameters dhParams) {
        this.dhParams = dhParams;
    }
    
    @Override
    public AsymmetricKeyParameter readKey(final InputStream inputStream) throws IOException {
        final byte[] magnitude = new byte[(this.dhParams.getP().bitLength() + 7) / 8];
        Streams.readFully(inputStream, magnitude, 0, magnitude.length);
        return new DHPublicKeyParameters(new BigInteger(1, magnitude), this.dhParams);
    }
}
