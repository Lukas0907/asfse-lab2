// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.util;

import org.bouncycastle.util.Strings;
import java.math.BigInteger;
import org.bouncycastle.util.Arrays;

class SSHBuffer
{
    private final byte[] buffer;
    private int pos;
    
    public SSHBuffer(final byte[] buffer) {
        this.pos = 0;
        this.buffer = buffer;
    }
    
    public SSHBuffer(final byte[] array, final byte[] buffer) {
        int i = 0;
        this.pos = 0;
        this.buffer = buffer;
        while (i != array.length) {
            if (array[i] != buffer[i]) {
                throw new IllegalArgumentException("magic-number incorrect");
            }
            ++i;
        }
        this.pos += array.length;
    }
    
    public byte[] getBuffer() {
        return Arrays.clone(this.buffer);
    }
    
    public boolean hasRemaining() {
        return this.pos < this.buffer.length;
    }
    
    public BigInteger readBigNumPositive() {
        final int u32 = this.readU32();
        final int pos = this.pos;
        final byte[] buffer = this.buffer;
        if (pos + u32 <= buffer.length) {
            this.pos = u32 + pos;
            return new BigInteger(1, Arrays.copyOfRange(buffer, pos, this.pos));
        }
        throw new IllegalArgumentException("not enough data for big num");
    }
    
    public byte[] readBlock() {
        final int u32 = this.readU32();
        if (u32 == 0) {
            return new byte[0];
        }
        final int pos = this.pos;
        final byte[] buffer = this.buffer;
        if (pos <= buffer.length - u32) {
            this.pos = u32 + pos;
            return Arrays.copyOfRange(buffer, pos, this.pos);
        }
        throw new IllegalArgumentException("not enough data for block");
    }
    
    public byte[] readPaddedBlock() {
        return this.readPaddedBlock(8);
    }
    
    public byte[] readPaddedBlock(int n) {
        final int u32 = this.readU32();
        if (u32 == 0) {
            return new byte[0];
        }
        final int pos = this.pos;
        final byte[] buffer = this.buffer;
        if (pos > buffer.length - u32) {
            throw new IllegalArgumentException("not enough data for block");
        }
        if (u32 % n == 0) {
            this.pos = pos + u32;
            int pos2;
            final int n2 = pos2 = this.pos;
            if (u32 > 0) {
                final int n3 = buffer[n2 - 1] & 0xFF;
                pos2 = n2;
                if (n3 > 0) {
                    pos2 = n2;
                    if (n3 < n) {
                        n = n2 - n3;
                        int n4 = 1;
                        int n5 = n;
                        while (true) {
                            pos2 = n;
                            if (n4 > n3) {
                                break;
                            }
                            if (n4 != (this.buffer[n5] & 0xFF)) {
                                throw new IllegalArgumentException("incorrect padding");
                            }
                            ++n4;
                            ++n5;
                        }
                    }
                }
            }
            return Arrays.copyOfRange(this.buffer, pos, pos2);
        }
        throw new IllegalArgumentException("missing padding");
    }
    
    public String readString() {
        return Strings.fromByteArray(this.readBlock());
    }
    
    public int readU32() {
        final int pos = this.pos;
        final byte[] buffer = this.buffer;
        if (pos <= buffer.length - 4) {
            this.pos = pos + 1;
            return (buffer[pos] & 0xFF) << 24 | (buffer[this.pos++] & 0xFF) << 16 | (buffer[this.pos++] & 0xFF) << 8 | (buffer[this.pos++] & 0xFF);
        }
        throw new IllegalArgumentException("4 bytes for U32 exceeds buffer.");
    }
    
    public void skipBlock() {
        final int u32 = this.readU32();
        final int pos = this.pos;
        if (pos <= this.buffer.length - u32) {
            this.pos = pos + u32;
            return;
        }
        throw new IllegalArgumentException("not enough data for block");
    }
}
