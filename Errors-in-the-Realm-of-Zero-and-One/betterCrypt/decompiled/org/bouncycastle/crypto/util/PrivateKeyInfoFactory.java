// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.util;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.math.BigInteger;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.DSAParameters;
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.params.Ed448PrivateKeyParameters;
import org.bouncycastle.crypto.params.X25519PrivateKeyParameters;
import org.bouncycastle.asn1.edec.EdECObjectIdentifiers;
import org.bouncycastle.crypto.params.X448PrivateKeyParameters;
import org.bouncycastle.asn1.sec.ECPrivateKey;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.math.ec.FixedPointCombMultiplier;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.asn1.x9.X9ECPoint;
import org.bouncycastle.crypto.params.ECNamedDomainParameters;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.rosstandart.RosstandartObjectIdentifiers;
import org.bouncycastle.asn1.cryptopro.GOST3410PublicKeyAlgParameters;
import org.bouncycastle.crypto.params.ECGOST3410Parameters;
import org.bouncycastle.asn1.ASN1Null;
import org.bouncycastle.asn1.x9.X962Parameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.x509.DSAParameter;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.crypto.params.DSAPrivateKeyParameters;
import org.bouncycastle.asn1.pkcs.RSAPrivateKey;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import java.util.HashSet;
import java.util.Set;

public class PrivateKeyInfoFactory
{
    private static Set cryptoProOids;
    
    static {
        (PrivateKeyInfoFactory.cryptoProOids = new HashSet(5)).add(CryptoProObjectIdentifiers.gostR3410_2001_CryptoPro_A);
        PrivateKeyInfoFactory.cryptoProOids.add(CryptoProObjectIdentifiers.gostR3410_2001_CryptoPro_B);
        PrivateKeyInfoFactory.cryptoProOids.add(CryptoProObjectIdentifiers.gostR3410_2001_CryptoPro_C);
        PrivateKeyInfoFactory.cryptoProOids.add(CryptoProObjectIdentifiers.gostR3410_2001_CryptoPro_XchA);
        PrivateKeyInfoFactory.cryptoProOids.add(CryptoProObjectIdentifiers.gostR3410_2001_CryptoPro_XchB);
    }
    
    private PrivateKeyInfoFactory() {
    }
    
    public static PrivateKeyInfo createPrivateKeyInfo(final AsymmetricKeyParameter asymmetricKeyParameter) throws IOException {
        return createPrivateKeyInfo(asymmetricKeyParameter, null);
    }
    
    public static PrivateKeyInfo createPrivateKeyInfo(final AsymmetricKeyParameter asymmetricKeyParameter, final ASN1Set set) throws IOException {
        if (asymmetricKeyParameter instanceof RSAKeyParameters) {
            final RSAPrivateCrtKeyParameters rsaPrivateCrtKeyParameters = (RSAPrivateCrtKeyParameters)asymmetricKeyParameter;
            return new PrivateKeyInfo(new AlgorithmIdentifier(PKCSObjectIdentifiers.rsaEncryption, DERNull.INSTANCE), new RSAPrivateKey(rsaPrivateCrtKeyParameters.getModulus(), rsaPrivateCrtKeyParameters.getPublicExponent(), rsaPrivateCrtKeyParameters.getExponent(), rsaPrivateCrtKeyParameters.getP(), rsaPrivateCrtKeyParameters.getQ(), rsaPrivateCrtKeyParameters.getDP(), rsaPrivateCrtKeyParameters.getDQ(), rsaPrivateCrtKeyParameters.getQInv()), set);
        }
        if (asymmetricKeyParameter instanceof DSAPrivateKeyParameters) {
            final DSAPrivateKeyParameters dsaPrivateKeyParameters = (DSAPrivateKeyParameters)asymmetricKeyParameter;
            final DSAParameters parameters = dsaPrivateKeyParameters.getParameters();
            return new PrivateKeyInfo(new AlgorithmIdentifier(X9ObjectIdentifiers.id_dsa, new DSAParameter(parameters.getP(), parameters.getQ(), parameters.getG())), new ASN1Integer(dsaPrivateKeyParameters.getX()), set);
        }
        if (asymmetricKeyParameter instanceof ECPrivateKeyParameters) {
            final ECPrivateKeyParameters ecPrivateKeyParameters = (ECPrivateKeyParameters)asymmetricKeyParameter;
            final ECDomainParameters parameters2 = ecPrivateKeyParameters.getParameters();
            X962Parameters x962Parameters;
            BigInteger bigInteger;
            if (parameters2 == null) {
                x962Parameters = new X962Parameters(DERNull.INSTANCE);
                bigInteger = ecPrivateKeyParameters.getD();
            }
            else {
                if (parameters2 instanceof ECGOST3410Parameters) {
                    final ECGOST3410Parameters ecgost3410Parameters = (ECGOST3410Parameters)parameters2;
                    final GOST3410PublicKeyAlgParameters gost3410PublicKeyAlgParameters = new GOST3410PublicKeyAlgParameters(ecgost3410Parameters.getPublicKeyParamSet(), ecgost3410Parameters.getDigestParamSet(), ecgost3410Parameters.getEncryptionParamSet());
                    final boolean contains = PrivateKeyInfoFactory.cryptoProOids.contains(gost3410PublicKeyAlgParameters.getPublicKeyParamSet());
                    int n = 32;
                    ASN1ObjectIdentifier asn1ObjectIdentifier;
                    if (contains) {
                        asn1ObjectIdentifier = CryptoProObjectIdentifiers.gostR3410_2001;
                    }
                    else {
                        final boolean b = ecPrivateKeyParameters.getD().bitLength() > 256;
                        if (b) {
                            asn1ObjectIdentifier = RosstandartObjectIdentifiers.id_tc26_gost_3410_12_512;
                        }
                        else {
                            asn1ObjectIdentifier = RosstandartObjectIdentifiers.id_tc26_gost_3410_12_256;
                        }
                        if (b) {
                            n = 64;
                        }
                    }
                    final byte[] array = new byte[n];
                    extractBytes(array, n, 0, ecPrivateKeyParameters.getD());
                    return new PrivateKeyInfo(new AlgorithmIdentifier(asn1ObjectIdentifier, gost3410PublicKeyAlgParameters), new DEROctetString(array));
                }
                if (!(parameters2 instanceof ECNamedDomainParameters)) {
                    x962Parameters = new X962Parameters(new X9ECParameters(parameters2.getCurve(), new X9ECPoint(parameters2.getG(), false), parameters2.getN(), parameters2.getH(), parameters2.getSeed()));
                    final int n2 = parameters2.getN().bitLength();
                    return new PrivateKeyInfo(new AlgorithmIdentifier(X9ObjectIdentifiers.id_ecPublicKey, x962Parameters), new ECPrivateKey(n2, ecPrivateKeyParameters.getD(), new DERBitString(new FixedPointCombMultiplier().multiply(parameters2.getG(), ecPrivateKeyParameters.getD()).getEncoded(false)), x962Parameters), set);
                }
                x962Parameters = new X962Parameters(((ECGOST3410Parameters)parameters2).getName());
                bigInteger = parameters2.getN();
            }
            final int n2 = bigInteger.bitLength();
            return new PrivateKeyInfo(new AlgorithmIdentifier(X9ObjectIdentifiers.id_ecPublicKey, x962Parameters), new ECPrivateKey(n2, ecPrivateKeyParameters.getD(), new DERBitString(new FixedPointCombMultiplier().multiply(parameters2.getG(), ecPrivateKeyParameters.getD()).getEncoded(false)), x962Parameters), set);
        }
        if (asymmetricKeyParameter instanceof X448PrivateKeyParameters) {
            final X448PrivateKeyParameters x448PrivateKeyParameters = (X448PrivateKeyParameters)asymmetricKeyParameter;
            return new PrivateKeyInfo(new AlgorithmIdentifier(EdECObjectIdentifiers.id_X448), new DEROctetString(x448PrivateKeyParameters.getEncoded()), set, x448PrivateKeyParameters.generatePublicKey().getEncoded());
        }
        if (asymmetricKeyParameter instanceof X25519PrivateKeyParameters) {
            final X25519PrivateKeyParameters x25519PrivateKeyParameters = (X25519PrivateKeyParameters)asymmetricKeyParameter;
            return new PrivateKeyInfo(new AlgorithmIdentifier(EdECObjectIdentifiers.id_X25519), new DEROctetString(x25519PrivateKeyParameters.getEncoded()), set, x25519PrivateKeyParameters.generatePublicKey().getEncoded());
        }
        if (asymmetricKeyParameter instanceof Ed448PrivateKeyParameters) {
            final Ed448PrivateKeyParameters ed448PrivateKeyParameters = (Ed448PrivateKeyParameters)asymmetricKeyParameter;
            return new PrivateKeyInfo(new AlgorithmIdentifier(EdECObjectIdentifiers.id_Ed448), new DEROctetString(ed448PrivateKeyParameters.getEncoded()), set, ed448PrivateKeyParameters.generatePublicKey().getEncoded());
        }
        if (asymmetricKeyParameter instanceof Ed25519PrivateKeyParameters) {
            final Ed25519PrivateKeyParameters ed25519PrivateKeyParameters = (Ed25519PrivateKeyParameters)asymmetricKeyParameter;
            return new PrivateKeyInfo(new AlgorithmIdentifier(EdECObjectIdentifiers.id_Ed25519), new DEROctetString(ed25519PrivateKeyParameters.getEncoded()), set, ed25519PrivateKeyParameters.generatePublicKey().getEncoded());
        }
        throw new IOException("key parameters not recognized");
    }
    
    private static void extractBytes(final byte[] array, final int n, final int n2, final BigInteger bigInteger) {
        final byte[] byteArray = bigInteger.toByteArray();
        final int length = byteArray.length;
        int i;
        final int n3 = i = 0;
        byte[] array2 = byteArray;
        if (length < n) {
            array2 = new byte[n];
            System.arraycopy(byteArray, 0, array2, array2.length - byteArray.length, byteArray.length);
            i = n3;
        }
        while (i != n) {
            array[n2 + i] = array2[array2.length - 1 - i];
            ++i;
        }
    }
}
