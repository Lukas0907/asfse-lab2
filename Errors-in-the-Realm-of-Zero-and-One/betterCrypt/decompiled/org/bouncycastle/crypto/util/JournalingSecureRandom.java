// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import java.security.SecureRandom;

public class JournalingSecureRandom extends SecureRandom
{
    private static byte[] EMPTY_TRANSCRIPT;
    private final SecureRandom base;
    private int index;
    private TranscriptStream tOut;
    private byte[] transcript;
    
    static {
        JournalingSecureRandom.EMPTY_TRANSCRIPT = new byte[0];
    }
    
    public JournalingSecureRandom() {
        this(CryptoServicesRegistrar.getSecureRandom());
    }
    
    public JournalingSecureRandom(final SecureRandom base) {
        this.tOut = new TranscriptStream();
        this.index = 0;
        this.base = base;
        this.transcript = JournalingSecureRandom.EMPTY_TRANSCRIPT;
    }
    
    public JournalingSecureRandom(final byte[] array, final SecureRandom base) {
        this.tOut = new TranscriptStream();
        this.index = 0;
        this.base = base;
        this.transcript = Arrays.clone(array);
    }
    
    public void clear() {
        Arrays.fill(this.transcript, (byte)0);
        this.tOut.clear();
    }
    
    public byte[] getFullTranscript() {
        final int index = this.index;
        final byte[] transcript = this.transcript;
        if (index == transcript.length) {
            return this.tOut.toByteArray();
        }
        return Arrays.clone(transcript);
    }
    
    public byte[] getTranscript() {
        return this.tOut.toByteArray();
    }
    
    @Override
    public final void nextBytes(final byte[] array) {
        if (this.index >= this.transcript.length) {
            this.base.nextBytes(array);
        }
        else {
            int i;
            for (i = 0; i != array.length; ++i) {
                final int index = this.index;
                final byte[] transcript = this.transcript;
                if (index >= transcript.length) {
                    break;
                }
                this.index = index + 1;
                array[i] = transcript[index];
            }
            if (i != array.length) {
                final byte[] bytes = new byte[array.length - i];
                this.base.nextBytes(bytes);
                System.arraycopy(bytes, 0, array, i, bytes.length);
            }
        }
        try {
            this.tOut.write(array);
        }
        catch (IOException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("unable to record transcript: ");
            sb.append(ex.getMessage());
            throw new IllegalStateException(sb.toString());
        }
    }
    
    public void reset() {
        this.index = 0;
        if (this.index == this.transcript.length) {
            this.transcript = this.tOut.toByteArray();
        }
        this.tOut.reset();
    }
    
    private class TranscriptStream extends ByteArrayOutputStream
    {
        public void clear() {
            Arrays.fill(this.buf, (byte)0);
        }
    }
}
