// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.util;

import java.io.IOException;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ASN1OctetString;

class DerUtil
{
    static ASN1OctetString getOctetString(final byte[] array) {
        if (array == null) {
            return new DEROctetString(new byte[0]);
        }
        return new DEROctetString(Arrays.clone(array));
    }
    
    static byte[] toByteArray(final ASN1Primitive asn1Primitive) {
        try {
            return asn1Primitive.getEncoded();
        }
        catch (IOException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot get encoding: ");
            sb.append(ex.getMessage());
            throw new IllegalStateException(sb.toString()) {
                @Override
                public Throwable getCause() {
                    return ex;
                }
            };
        }
    }
}
