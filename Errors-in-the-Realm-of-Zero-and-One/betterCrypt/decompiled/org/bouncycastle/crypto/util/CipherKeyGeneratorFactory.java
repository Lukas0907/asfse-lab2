// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.util;

import org.bouncycastle.crypto.generators.DESKeyGenerator;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.asn1.kisa.KISAObjectIdentifiers;
import org.bouncycastle.asn1.ntt.NTTObjectIdentifiers;
import org.bouncycastle.crypto.generators.DESedeKeyGenerator;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.CipherKeyGenerator;
import java.security.SecureRandom;

public class CipherKeyGeneratorFactory
{
    private CipherKeyGeneratorFactory() {
    }
    
    private static CipherKeyGenerator createCipherKeyGenerator(final SecureRandom secureRandom, final int n) {
        final CipherKeyGenerator cipherKeyGenerator = new CipherKeyGenerator();
        cipherKeyGenerator.init(new KeyGenerationParameters(secureRandom, n));
        return cipherKeyGenerator;
    }
    
    public static CipherKeyGenerator createKeyGenerator(final ASN1ObjectIdentifier obj, final SecureRandom secureRandom) throws IllegalArgumentException {
        if (NISTObjectIdentifiers.id_aes128_CBC.equals(obj)) {
            return createCipherKeyGenerator(secureRandom, 128);
        }
        if (NISTObjectIdentifiers.id_aes192_CBC.equals(obj)) {
            return createCipherKeyGenerator(secureRandom, 192);
        }
        if (NISTObjectIdentifiers.id_aes256_CBC.equals(obj)) {
            return createCipherKeyGenerator(secureRandom, 256);
        }
        if (NISTObjectIdentifiers.id_aes128_GCM.equals(obj)) {
            return createCipherKeyGenerator(secureRandom, 128);
        }
        if (NISTObjectIdentifiers.id_aes192_GCM.equals(obj)) {
            return createCipherKeyGenerator(secureRandom, 192);
        }
        if (NISTObjectIdentifiers.id_aes256_GCM.equals(obj)) {
            return createCipherKeyGenerator(secureRandom, 256);
        }
        if (PKCSObjectIdentifiers.des_EDE3_CBC.equals(obj)) {
            final DESedeKeyGenerator deSedeKeyGenerator = new DESedeKeyGenerator();
            deSedeKeyGenerator.init(new KeyGenerationParameters(secureRandom, 192));
            return deSedeKeyGenerator;
        }
        if (NTTObjectIdentifiers.id_camellia128_cbc.equals(obj)) {
            return createCipherKeyGenerator(secureRandom, 128);
        }
        if (NTTObjectIdentifiers.id_camellia192_cbc.equals(obj)) {
            return createCipherKeyGenerator(secureRandom, 192);
        }
        if (NTTObjectIdentifiers.id_camellia256_cbc.equals(obj)) {
            return createCipherKeyGenerator(secureRandom, 256);
        }
        if (KISAObjectIdentifiers.id_seedCBC.equals(obj)) {
            return createCipherKeyGenerator(secureRandom, 128);
        }
        if (AlgorithmIdentifierFactory.CAST5_CBC.equals(obj)) {
            return createCipherKeyGenerator(secureRandom, 128);
        }
        if (OIWObjectIdentifiers.desCBC.equals(obj)) {
            final DESKeyGenerator desKeyGenerator = new DESKeyGenerator();
            desKeyGenerator.init(new KeyGenerationParameters(secureRandom, 64));
            return desKeyGenerator;
        }
        if (PKCSObjectIdentifiers.rc4.equals(obj)) {
            return createCipherKeyGenerator(secureRandom, 128);
        }
        if (PKCSObjectIdentifiers.RC2_CBC.equals(obj)) {
            return createCipherKeyGenerator(secureRandom, 128);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("cannot recognise cipher: ");
        sb.append(obj);
        throw new IllegalArgumentException(sb.toString());
    }
}
