// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.util;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.math.BigInteger;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.DSAParameters;
import java.io.IOException;
import org.bouncycastle.crypto.params.Ed448PublicKeyParameters;
import org.bouncycastle.crypto.params.X448PublicKeyParameters;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import org.bouncycastle.asn1.edec.EdECObjectIdentifiers;
import org.bouncycastle.crypto.params.X25519PublicKeyParameters;
import org.bouncycastle.crypto.params.ECNamedDomainParameters;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.asn1.x9.X9ECPoint;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.rosstandart.RosstandartObjectIdentifiers;
import org.bouncycastle.asn1.cryptopro.GOST3410PublicKeyAlgParameters;
import org.bouncycastle.crypto.params.ECGOST3410Parameters;
import org.bouncycastle.asn1.ASN1Null;
import org.bouncycastle.asn1.x9.X962Parameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.asn1.x509.DSAParameter;
import org.bouncycastle.crypto.params.DSAPublicKeyParameters;
import org.bouncycastle.asn1.pkcs.RSAPublicKey;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import java.util.HashSet;
import java.util.Set;

public class SubjectPublicKeyInfoFactory
{
    private static Set cryptoProOids;
    
    static {
        (SubjectPublicKeyInfoFactory.cryptoProOids = new HashSet(5)).add(CryptoProObjectIdentifiers.gostR3410_2001_CryptoPro_A);
        SubjectPublicKeyInfoFactory.cryptoProOids.add(CryptoProObjectIdentifiers.gostR3410_2001_CryptoPro_B);
        SubjectPublicKeyInfoFactory.cryptoProOids.add(CryptoProObjectIdentifiers.gostR3410_2001_CryptoPro_C);
        SubjectPublicKeyInfoFactory.cryptoProOids.add(CryptoProObjectIdentifiers.gostR3410_2001_CryptoPro_XchA);
        SubjectPublicKeyInfoFactory.cryptoProOids.add(CryptoProObjectIdentifiers.gostR3410_2001_CryptoPro_XchB);
    }
    
    private SubjectPublicKeyInfoFactory() {
    }
    
    public static SubjectPublicKeyInfo createSubjectPublicKeyInfo(final AsymmetricKeyParameter asymmetricKeyParameter) throws IOException {
        if (asymmetricKeyParameter instanceof RSAKeyParameters) {
            final RSAKeyParameters rsaKeyParameters = (RSAKeyParameters)asymmetricKeyParameter;
            return new SubjectPublicKeyInfo(new AlgorithmIdentifier(PKCSObjectIdentifiers.rsaEncryption, DERNull.INSTANCE), new RSAPublicKey(rsaKeyParameters.getModulus(), rsaKeyParameters.getExponent()));
        }
        final boolean b = asymmetricKeyParameter instanceof DSAPublicKeyParameters;
        final ASN1Encodable asn1Encodable = null;
        if (b) {
            final DSAPublicKeyParameters dsaPublicKeyParameters = (DSAPublicKeyParameters)asymmetricKeyParameter;
            final DSAParameters parameters = dsaPublicKeyParameters.getParameters();
            ASN1Encodable asn1Encodable2 = asn1Encodable;
            if (parameters != null) {
                asn1Encodable2 = new DSAParameter(parameters.getP(), parameters.getQ(), parameters.getG());
            }
            return new SubjectPublicKeyInfo(new AlgorithmIdentifier(X9ObjectIdentifiers.id_dsa, asn1Encodable2), new ASN1Integer(dsaPublicKeyParameters.getY()));
        }
        Label_0461: {
            if (!(asymmetricKeyParameter instanceof ECPublicKeyParameters)) {
                break Label_0461;
            }
            final ECPublicKeyParameters ecPublicKeyParameters = (ECPublicKeyParameters)asymmetricKeyParameter;
            final ECDomainParameters parameters2 = ecPublicKeyParameters.getParameters();
            Label_0429: {
                if (parameters2 == null) {
                    final X962Parameters x962Parameters = new X962Parameters(DERNull.INSTANCE);
                    break Label_0429;
                }
                Label_0361: {
                    if (!(parameters2 instanceof ECGOST3410Parameters)) {
                        break Label_0361;
                    }
                    final ECGOST3410Parameters ecgost3410Parameters = (ECGOST3410Parameters)parameters2;
                    final BigInteger bigInteger = ecPublicKeyParameters.getQ().getAffineXCoord().toBigInteger();
                    final BigInteger bigInteger2 = ecPublicKeyParameters.getQ().getAffineYCoord().toBigInteger();
                    final GOST3410PublicKeyAlgParameters gost3410PublicKeyAlgParameters = new GOST3410PublicKeyAlgParameters(ecgost3410Parameters.getPublicKeyParamSet(), ecgost3410Parameters.getDigestParamSet());
                    final boolean contains = SubjectPublicKeyInfoFactory.cryptoProOids.contains(ecgost3410Parameters.getPublicKeyParamSet());
                    final int n = 32;
                    int n2 = 64;
                    ASN1ObjectIdentifier asn1ObjectIdentifier;
                    int n3;
                    if (contains) {
                        asn1ObjectIdentifier = CryptoProObjectIdentifiers.gostR3410_2001;
                        n3 = n;
                    }
                    else if (bigInteger.bitLength() > 256) {
                        asn1ObjectIdentifier = RosstandartObjectIdentifiers.id_tc26_gost_3410_12_512;
                        n2 = 128;
                        n3 = 64;
                    }
                    else {
                        asn1ObjectIdentifier = RosstandartObjectIdentifiers.id_tc26_gost_3410_12_256;
                        n3 = n;
                    }
                    final byte[] array = new byte[n2];
                    final int n4 = n2 / 2;
                    extractBytes(array, n4, 0, bigInteger);
                    extractBytes(array, n4, n3, bigInteger2);
                    try {
                        return new SubjectPublicKeyInfo(new AlgorithmIdentifier(asn1ObjectIdentifier, gost3410PublicKeyAlgParameters), new DEROctetString(array));
                        X962Parameters x962Parameters = null;
                        Label_0386: {
                            x962Parameters = new X962Parameters(new X9ECParameters(parameters2.getCurve(), new X9ECPoint(parameters2.getG(), false), parameters2.getN(), parameters2.getH(), parameters2.getSeed()));
                        }
                        return new SubjectPublicKeyInfo(new AlgorithmIdentifier(X9ObjectIdentifiers.id_ecPublicKey, x962Parameters), ecPublicKeyParameters.getQ().getEncoded(false));
                        // iftrue(Label_0386:, !parameters2 instanceof ECNamedDomainParameters)
                        // iftrue(Label_0529:, !asymmetricKeyParameter instanceof X25519PublicKeyParameters)
                        // iftrue(Label_0597:, !asymmetricKeyParameter instanceof Ed25519PublicKeyParameters)
                        Block_12: {
                            break Block_12;
                            Label_0495:
                            return new SubjectPublicKeyInfo(new AlgorithmIdentifier(EdECObjectIdentifiers.id_X25519), ((X25519PublicKeyParameters)asymmetricKeyParameter).getEncoded());
                            Label_0563:
                            return new SubjectPublicKeyInfo(new AlgorithmIdentifier(EdECObjectIdentifiers.id_Ed25519), ((Ed25519PublicKeyParameters)asymmetricKeyParameter).getEncoded());
                            return new SubjectPublicKeyInfo(new AlgorithmIdentifier(X9ObjectIdentifiers.id_ecPublicKey, x962Parameters), ecPublicKeyParameters.getQ().getEncoded(false));
                        }
                        x962Parameters = new X962Parameters(((ECGOST3410Parameters)parameters2).getName());
                        return new SubjectPublicKeyInfo(new AlgorithmIdentifier(X9ObjectIdentifiers.id_ecPublicKey, x962Parameters), ecPublicKeyParameters.getQ().getEncoded(false));
                        // iftrue(Label_0495:, !asymmetricKeyParameter instanceof X448PublicKeyParameters)
                        return new SubjectPublicKeyInfo(new AlgorithmIdentifier(EdECObjectIdentifiers.id_X448), ((X448PublicKeyParameters)asymmetricKeyParameter).getEncoded());
                        Label_0529:
                        // iftrue(Label_0563:, !asymmetricKeyParameter instanceof Ed448PublicKeyParameters)
                        return new SubjectPublicKeyInfo(new AlgorithmIdentifier(EdECObjectIdentifiers.id_Ed448), ((Ed448PublicKeyParameters)asymmetricKeyParameter).getEncoded());
                        Label_0597:
                        throw new IOException("key parameters not recognized");
                    }
                    catch (IOException ex) {
                        return null;
                    }
                }
            }
        }
    }
    
    private static void extractBytes(final byte[] array, final int n, final int n2, final BigInteger bigInteger) {
        final byte[] byteArray = bigInteger.toByteArray();
        final int length = byteArray.length;
        int i;
        final int n3 = i = 0;
        byte[] array2 = byteArray;
        if (length < n) {
            array2 = new byte[n];
            System.arraycopy(byteArray, 0, array2, array2.length - byteArray.length, byteArray.length);
            i = n3;
        }
        while (i != n) {
            array[n2 + i] = array2[array2.length - 1 - i];
            ++i;
        }
    }
}
