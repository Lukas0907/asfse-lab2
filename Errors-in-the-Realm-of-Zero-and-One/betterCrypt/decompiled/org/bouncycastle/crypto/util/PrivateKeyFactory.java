// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.util;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.crypto.params.ECGOST3410Parameters;
import org.bouncycastle.asn1.cryptopro.ECGOST3410NamedCurves;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.cryptopro.GOST3410PublicKeyAlgParameters;
import org.bouncycastle.asn1.rosstandart.RosstandartObjectIdentifiers;
import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import org.bouncycastle.crypto.params.Ed448PrivateKeyParameters;
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.params.X448PrivateKeyParameters;
import org.bouncycastle.crypto.params.X25519PrivateKeyParameters;
import org.bouncycastle.asn1.edec.EdECObjectIdentifiers;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.asn1.sec.ECPrivateKey;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.params.ECNamedDomainParameters;
import org.bouncycastle.asn1.x9.ECNamedCurveTable;
import org.bouncycastle.crypto.ec.CustomNamedCurves;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x9.X962Parameters;
import org.bouncycastle.crypto.params.DSAPrivateKeyParameters;
import org.bouncycastle.crypto.params.DSAParameters;
import org.bouncycastle.asn1.x509.DSAParameter;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.crypto.params.ElGamalPrivateKeyParameters;
import org.bouncycastle.crypto.params.ElGamalParameters;
import org.bouncycastle.asn1.oiw.ElGamalParameter;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.crypto.params.DHPrivateKeyParameters;
import java.math.BigInteger;
import org.bouncycastle.crypto.params.DHParameters;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.pkcs.DHParameter;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;
import org.bouncycastle.asn1.pkcs.RSAPrivateKey;
import org.bouncycastle.asn1.x509.X509ObjectIdentifiers;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import java.io.IOException;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import java.io.InputStream;

public class PrivateKeyFactory
{
    public static AsymmetricKeyParameter createKey(final InputStream inputStream) throws IOException {
        return createKey(PrivateKeyInfo.getInstance(new ASN1InputStream(inputStream).readObject()));
    }
    
    public static AsymmetricKeyParameter createKey(final PrivateKeyInfo privateKeyInfo) throws IOException {
        final AlgorithmIdentifier privateKeyAlgorithm = privateKeyInfo.getPrivateKeyAlgorithm();
        final ASN1ObjectIdentifier algorithm = privateKeyAlgorithm.getAlgorithm();
        if (algorithm.equals(PKCSObjectIdentifiers.rsaEncryption) || algorithm.equals(PKCSObjectIdentifiers.id_RSASSA_PSS) || algorithm.equals(X509ObjectIdentifiers.id_ea_rsa)) {
            final RSAPrivateKey instance = RSAPrivateKey.getInstance(privateKeyInfo.parsePrivateKey());
            return new RSAPrivateCrtKeyParameters(instance.getModulus(), instance.getPublicExponent(), instance.getPrivateExponent(), instance.getPrime1(), instance.getPrime2(), instance.getExponent1(), instance.getExponent2(), instance.getCoefficient());
        }
        final boolean equals = algorithm.equals(PKCSObjectIdentifiers.dhKeyAgreement);
        ECDomainParameters ecDomainParameters = null;
        final DSAParameters dsaParameters = null;
        int intValue = 0;
        if (equals) {
            final DHParameter instance2 = DHParameter.getInstance(privateKeyAlgorithm.getParameters());
            final ASN1Integer asn1Integer = (ASN1Integer)privateKeyInfo.parsePrivateKey();
            final BigInteger l = instance2.getL();
            if (l != null) {
                intValue = l.intValue();
            }
            return new DHPrivateKeyParameters(asn1Integer.getValue(), new DHParameters(instance2.getP(), instance2.getG(), null, intValue));
        }
        if (algorithm.equals(OIWObjectIdentifiers.elGamalAlgorithm)) {
            final ElGamalParameter instance3 = ElGamalParameter.getInstance(privateKeyAlgorithm.getParameters());
            return new ElGamalPrivateKeyParameters(((ASN1Integer)privateKeyInfo.parsePrivateKey()).getValue(), new ElGamalParameters(instance3.getP(), instance3.getG()));
        }
        if (algorithm.equals(X9ObjectIdentifiers.id_dsa)) {
            final ASN1Integer asn1Integer2 = (ASN1Integer)privateKeyInfo.parsePrivateKey();
            final ASN1Encodable parameters = privateKeyAlgorithm.getParameters();
            DSAParameters dsaParameters2 = dsaParameters;
            if (parameters != null) {
                final DSAParameter instance4 = DSAParameter.getInstance(parameters.toASN1Primitive());
                dsaParameters2 = new DSAParameters(instance4.getP(), instance4.getQ(), instance4.getG());
            }
            return new DSAPrivateKeyParameters(asn1Integer2.getValue(), dsaParameters2);
        }
        if (algorithm.equals(X9ObjectIdentifiers.id_ecPublicKey)) {
            final X962Parameters instance5 = X962Parameters.getInstance(privateKeyAlgorithm.getParameters());
            final boolean namedCurve = instance5.isNamedCurve();
            final ASN1Primitive parameters2 = instance5.getParameters();
            ECDomainParameters ecDomainParameters2;
            if (namedCurve) {
                final ASN1ObjectIdentifier asn1ObjectIdentifier = (ASN1ObjectIdentifier)parameters2;
                X9ECParameters x9ECParameters;
                if ((x9ECParameters = CustomNamedCurves.getByOID(asn1ObjectIdentifier)) == null) {
                    x9ECParameters = ECNamedCurveTable.getByOID(asn1ObjectIdentifier);
                }
                ecDomainParameters2 = new ECNamedDomainParameters(asn1ObjectIdentifier, x9ECParameters.getCurve(), x9ECParameters.getG(), x9ECParameters.getN(), x9ECParameters.getH(), x9ECParameters.getSeed());
            }
            else {
                final X9ECParameters instance6 = X9ECParameters.getInstance(parameters2);
                ecDomainParameters2 = new ECDomainParameters(instance6.getCurve(), instance6.getG(), instance6.getN(), instance6.getH(), instance6.getSeed());
            }
            return new ECPrivateKeyParameters(ECPrivateKey.getInstance(privateKeyInfo.parsePrivateKey()).getKey(), ecDomainParameters2);
        }
        if (algorithm.equals(EdECObjectIdentifiers.id_X25519)) {
            return new X25519PrivateKeyParameters(getRawKey(privateKeyInfo, 32), 0);
        }
        if (algorithm.equals(EdECObjectIdentifiers.id_X448)) {
            return new X448PrivateKeyParameters(getRawKey(privateKeyInfo, 56), 0);
        }
        if (algorithm.equals(EdECObjectIdentifiers.id_Ed25519)) {
            return new Ed25519PrivateKeyParameters(getRawKey(privateKeyInfo, 32), 0);
        }
        if (algorithm.equals(EdECObjectIdentifiers.id_Ed448)) {
            return new Ed448PrivateKeyParameters(getRawKey(privateKeyInfo, 57), 0);
        }
        if (!algorithm.equals(CryptoProObjectIdentifiers.gostR3410_2001) && !algorithm.equals(RosstandartObjectIdentifiers.id_tc26_gost_3410_12_512) && !algorithm.equals(RosstandartObjectIdentifiers.id_tc26_gost_3410_12_256)) {
            throw new RuntimeException("algorithm identifier in private key not recognised");
        }
        final GOST3410PublicKeyAlgParameters instance7 = GOST3410PublicKeyAlgParameters.getInstance(privateKeyInfo.getPrivateKeyAlgorithm().getParameters());
        final ASN1Primitive asn1Primitive = privateKeyInfo.getPrivateKeyAlgorithm().getParameters().toASN1Primitive();
        BigInteger bigInteger;
        if (asn1Primitive instanceof ASN1Sequence && (ASN1Sequence.getInstance(asn1Primitive).size() == 2 || ASN1Sequence.getInstance(asn1Primitive).size() == 3)) {
            ecDomainParameters = new ECGOST3410Parameters(new ECNamedDomainParameters(instance7.getPublicKeyParamSet(), ECGOST3410NamedCurves.getByOID(instance7.getPublicKeyParamSet())), instance7.getPublicKeyParamSet(), instance7.getDigestParamSet(), instance7.getEncryptionParamSet());
            final ASN1Encodable privateKey = privateKeyInfo.parsePrivateKey();
            if (privateKey instanceof ASN1Integer) {
                bigInteger = ASN1Integer.getInstance(privateKey).getPositiveValue();
            }
            else {
                bigInteger = new BigInteger(1, Arrays.reverse(ASN1OctetString.getInstance(privateKey).getOctets()));
            }
        }
        else {
            final X962Parameters instance8 = X962Parameters.getInstance(privateKeyInfo.getPrivateKeyAlgorithm().getParameters());
            if (instance8.isNamedCurve()) {
                final ASN1ObjectIdentifier instance9 = ASN1ObjectIdentifier.getInstance(instance8.getParameters());
                final X9ECParameters byOID = ECNamedCurveTable.getByOID(instance9);
                if (byOID == null) {
                    final ECDomainParameters byOID2 = ECGOST3410NamedCurves.getByOID(instance9);
                    ecDomainParameters = new ECGOST3410Parameters(new ECNamedDomainParameters(instance9, byOID2.getCurve(), byOID2.getG(), byOID2.getN(), byOID2.getH(), byOID2.getSeed()), instance7.getPublicKeyParamSet(), instance7.getDigestParamSet(), instance7.getEncryptionParamSet());
                }
                else {
                    ecDomainParameters = new ECGOST3410Parameters(new ECNamedDomainParameters(instance9, byOID.getCurve(), byOID.getG(), byOID.getN(), byOID.getH(), byOID.getSeed()), instance7.getPublicKeyParamSet(), instance7.getDigestParamSet(), instance7.getEncryptionParamSet());
                }
            }
            else if (!instance8.isImplicitlyCA()) {
                final X9ECParameters instance10 = X9ECParameters.getInstance(instance8.getParameters());
                ecDomainParameters = new ECGOST3410Parameters(new ECNamedDomainParameters(algorithm, instance10.getCurve(), instance10.getG(), instance10.getN(), instance10.getH(), instance10.getSeed()), instance7.getPublicKeyParamSet(), instance7.getDigestParamSet(), instance7.getEncryptionParamSet());
            }
            final ASN1Encodable privateKey2 = privateKeyInfo.parsePrivateKey();
            if (privateKey2 instanceof ASN1Integer) {
                bigInteger = ASN1Integer.getInstance(privateKey2).getValue();
            }
            else {
                bigInteger = ECPrivateKey.getInstance(privateKey2).getKey();
            }
        }
        return new ECPrivateKeyParameters(bigInteger, new ECGOST3410Parameters(ecDomainParameters, instance7.getPublicKeyParamSet(), instance7.getDigestParamSet(), instance7.getEncryptionParamSet()));
    }
    
    public static AsymmetricKeyParameter createKey(final byte[] array) throws IOException {
        return createKey(PrivateKeyInfo.getInstance(ASN1Primitive.fromByteArray(array)));
    }
    
    private static byte[] getRawKey(final PrivateKeyInfo privateKeyInfo, final int n) throws IOException {
        final byte[] octets = ASN1OctetString.getInstance(privateKeyInfo.parsePrivateKey()).getOctets();
        if (n == octets.length) {
            return octets;
        }
        throw new RuntimeException("private key encoding has incorrect length");
    }
}
