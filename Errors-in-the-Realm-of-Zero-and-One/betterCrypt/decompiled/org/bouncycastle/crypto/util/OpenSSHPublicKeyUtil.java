// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.util;

import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.asn1.x9.ECNamedCurveTable;
import java.io.IOException;
import org.bouncycastle.crypto.params.DSAParameters;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import org.bouncycastle.crypto.params.DSAPublicKeyParameters;
import org.bouncycastle.math.ec.custom.sec.SecP256R1Curve;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public class OpenSSHPublicKeyUtil
{
    private static final String DSS = "ssh-dss";
    private static final String ECDSA = "ecdsa";
    private static final String ED_25519 = "ssh-ed25519";
    private static final String RSA = "ssh-rsa";
    
    private OpenSSHPublicKeyUtil() {
    }
    
    public static byte[] encodePublicKey(final AsymmetricKeyParameter asymmetricKeyParameter) throws IOException {
        if (asymmetricKeyParameter == null) {
            throw new IllegalArgumentException("cipherParameters was null.");
        }
        if (asymmetricKeyParameter instanceof RSAKeyParameters) {
            if (!asymmetricKeyParameter.isPrivate()) {
                final RSAKeyParameters rsaKeyParameters = (RSAKeyParameters)asymmetricKeyParameter;
                final SSHBuilder sshBuilder = new SSHBuilder();
                sshBuilder.writeString("ssh-rsa");
                sshBuilder.writeBigNum(rsaKeyParameters.getExponent());
                sshBuilder.writeBigNum(rsaKeyParameters.getModulus());
                return sshBuilder.getBytes();
            }
            throw new IllegalArgumentException("RSAKeyParamaters was for encryption");
        }
        else if (asymmetricKeyParameter instanceof ECPublicKeyParameters) {
            final SSHBuilder sshBuilder2 = new SSHBuilder();
            final ECPublicKeyParameters ecPublicKeyParameters = (ECPublicKeyParameters)asymmetricKeyParameter;
            if (ecPublicKeyParameters.getParameters().getCurve() instanceof SecP256R1Curve) {
                final StringBuilder sb = new StringBuilder();
                sb.append("ecdsa-sha2-");
                sb.append("nistp256");
                sshBuilder2.writeString(sb.toString());
                sshBuilder2.writeString("nistp256");
                sshBuilder2.writeBlock(ecPublicKeyParameters.getQ().getEncoded(false));
                return sshBuilder2.getBytes();
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("unable to derive ssh curve name for ");
            sb2.append(ecPublicKeyParameters.getParameters().getCurve().getClass().getName());
            throw new IllegalArgumentException(sb2.toString());
        }
        else {
            if (asymmetricKeyParameter instanceof DSAPublicKeyParameters) {
                final DSAPublicKeyParameters dsaPublicKeyParameters = (DSAPublicKeyParameters)asymmetricKeyParameter;
                final DSAParameters parameters = dsaPublicKeyParameters.getParameters();
                final SSHBuilder sshBuilder3 = new SSHBuilder();
                sshBuilder3.writeString("ssh-dss");
                sshBuilder3.writeBigNum(parameters.getP());
                sshBuilder3.writeBigNum(parameters.getQ());
                sshBuilder3.writeBigNum(parameters.getG());
                sshBuilder3.writeBigNum(dsaPublicKeyParameters.getY());
                return sshBuilder3.getBytes();
            }
            if (asymmetricKeyParameter instanceof Ed25519PublicKeyParameters) {
                final SSHBuilder sshBuilder4 = new SSHBuilder();
                sshBuilder4.writeString("ssh-ed25519");
                sshBuilder4.writeBlock(((Ed25519PublicKeyParameters)asymmetricKeyParameter).getEncoded());
                return sshBuilder4.getBytes();
            }
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("unable to convert ");
            sb3.append(asymmetricKeyParameter.getClass().getName());
            sb3.append(" to private key");
            throw new IllegalArgumentException(sb3.toString());
        }
    }
    
    public static AsymmetricKeyParameter parsePublicKey(final SSHBuffer sshBuffer) {
        final String string = sshBuffer.readString();
        AsymmetricKeyParameter asymmetricKeyParameter;
        if ("ssh-rsa".equals(string)) {
            asymmetricKeyParameter = new RSAKeyParameters(false, sshBuffer.readBigNumPositive(), sshBuffer.readBigNumPositive());
        }
        else if ("ssh-dss".equals(string)) {
            asymmetricKeyParameter = new DSAPublicKeyParameters(sshBuffer.readBigNumPositive(), new DSAParameters(sshBuffer.readBigNumPositive(), sshBuffer.readBigNumPositive(), sshBuffer.readBigNumPositive()));
        }
        else if (string.startsWith("ecdsa")) {
            String str;
            final String s = str = sshBuffer.readString();
            if (s.startsWith("nist")) {
                final String substring = s.substring(4);
                final StringBuilder sb = new StringBuilder();
                sb.append(substring.substring(0, 1));
                sb.append("-");
                sb.append(substring.substring(1));
                str = sb.toString();
            }
            final X9ECParameters byName = ECNamedCurveTable.getByName(str);
            if (byName == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("unable to find curve for ");
                sb2.append(string);
                sb2.append(" using curve name ");
                sb2.append(str);
                throw new IllegalStateException(sb2.toString());
            }
            final ECCurve curve = byName.getCurve();
            asymmetricKeyParameter = new ECPublicKeyParameters(curve.decodePoint(sshBuffer.readBlock()), new ECDomainParameters(curve, byName.getG(), byName.getN(), byName.getH(), byName.getSeed()));
        }
        else if ("ssh-ed25519".equals(string)) {
            final byte[] block = sshBuffer.readBlock();
            if (block.length != 32) {
                throw new IllegalStateException("public key value of wrong length");
            }
            asymmetricKeyParameter = new Ed25519PublicKeyParameters(block, 0);
        }
        else {
            asymmetricKeyParameter = null;
        }
        if (asymmetricKeyParameter == null) {
            throw new IllegalArgumentException("unable to parse key");
        }
        if (!sshBuffer.hasRemaining()) {
            return asymmetricKeyParameter;
        }
        throw new IllegalArgumentException("decoded key has trailing data");
    }
    
    public static AsymmetricKeyParameter parsePublicKey(final byte[] array) {
        return parsePublicKey(new SSHBuffer(array));
    }
}
