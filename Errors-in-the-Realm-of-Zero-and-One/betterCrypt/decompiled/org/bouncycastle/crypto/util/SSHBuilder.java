// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.util;

import org.bouncycastle.util.Strings;
import java.io.IOException;
import java.math.BigInteger;
import java.io.ByteArrayOutputStream;

class SSHBuilder
{
    private final ByteArrayOutputStream bos;
    
    SSHBuilder() {
        this.bos = new ByteArrayOutputStream();
    }
    
    public byte[] getBytes() {
        return this.bos.toByteArray();
    }
    
    public byte[] getPaddedBytes() {
        return this.getPaddedBytes(8);
    }
    
    public byte[] getPaddedBytes(final int n) {
        final int n2 = this.bos.size() % n;
        if (n2 != 0) {
            for (int i = 1; i <= n - n2; ++i) {
                this.bos.write(i);
            }
        }
        return this.bos.toByteArray();
    }
    
    public void u32(final int n) {
        this.bos.write(n >>> 24 & 0xFF);
        this.bos.write(n >>> 16 & 0xFF);
        this.bos.write(n >>> 8 & 0xFF);
        this.bos.write(n & 0xFF);
    }
    
    public void writeBigNum(final BigInteger bigInteger) {
        this.writeBlock(bigInteger.toByteArray());
    }
    
    public void writeBlock(final byte[] b) {
        this.u32(b.length);
        try {
            this.bos.write(b);
        }
        catch (IOException cause) {
            throw new IllegalStateException(cause.getMessage(), cause);
        }
    }
    
    public void writeBytes(final byte[] b) {
        try {
            this.bos.write(b);
        }
        catch (IOException cause) {
            throw new IllegalStateException(cause.getMessage(), cause);
        }
    }
    
    public void writeString(final String s) {
        this.writeBlock(Strings.toByteArray(s));
    }
}
