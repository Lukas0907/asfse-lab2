// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.util;

import java.io.OutputStream;
import java.io.FileOutputStream;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Sequence;
import java.io.IOException;
import org.bouncycastle.util.io.Streams;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.File;
import java.security.SecureRandom;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import java.io.Serializable;
import org.bouncycastle.util.Encodable;

public class JournaledAlgorithm implements Encodable, Serializable
{
    private transient AlgorithmIdentifier algID;
    private transient JournalingSecureRandom journaling;
    
    public JournaledAlgorithm(final AlgorithmIdentifier algID, final JournalingSecureRandom journaling) {
        if (algID == null) {
            throw new NullPointerException("AlgorithmIdentifier passed to JournaledAlgorithm is null");
        }
        if (journaling != null) {
            this.journaling = journaling;
            this.algID = algID;
            return;
        }
        throw new NullPointerException("JournalingSecureRandom passed to JournaledAlgorithm is null");
    }
    
    public JournaledAlgorithm(final byte[] array) {
        this(array, CryptoServicesRegistrar.getSecureRandom());
    }
    
    public JournaledAlgorithm(final byte[] array, final SecureRandom secureRandom) {
        if (array == null) {
            throw new NullPointerException("encoding passed to JournaledAlgorithm is null");
        }
        if (secureRandom != null) {
            this.initFromEncoding(array, secureRandom);
            return;
        }
        throw new NullPointerException("random passed to JournaledAlgorithm is null");
    }
    
    public static JournaledAlgorithm getState(File file, final SecureRandom secureRandom) throws IOException, ClassNotFoundException {
        if (file != null) {
            file = (File)new BufferedInputStream(new FileInputStream(file));
            try {
                return new JournaledAlgorithm(Streams.readAll((InputStream)file), secureRandom);
            }
            finally {
                ((InputStream)file).close();
            }
        }
        throw new NullPointerException("File for loading is null in JournaledAlgorithm");
    }
    
    public static JournaledAlgorithm getState(InputStream in, final SecureRandom secureRandom) throws IOException, ClassNotFoundException {
        if (in != null) {
            in = new BufferedInputStream(in);
            try {
                return new JournaledAlgorithm(Streams.readAll(in), secureRandom);
            }
            finally {
                in.close();
            }
        }
        throw new NullPointerException("stream for loading is null in JournaledAlgorithm");
    }
    
    private void initFromEncoding(final byte[] array, final SecureRandom secureRandom) {
        final ASN1Sequence instance = ASN1Sequence.getInstance(array);
        this.algID = AlgorithmIdentifier.getInstance(instance.getObjectAt(0));
        this.journaling = new JournalingSecureRandom(ASN1OctetString.getInstance(instance.getObjectAt(1)).getOctets(), secureRandom);
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.initFromEncoding((byte[])objectInputStream.readObject(), CryptoServicesRegistrar.getSecureRandom());
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.getEncoded());
    }
    
    public AlgorithmIdentifier getAlgorithmIdentifier() {
        return this.algID;
    }
    
    @Override
    public byte[] getEncoded() throws IOException {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        asn1EncodableVector.add(this.algID);
        asn1EncodableVector.add(new DEROctetString(this.journaling.getFullTranscript()));
        return new DERSequence(asn1EncodableVector).getEncoded();
    }
    
    public JournalingSecureRandom getJournalingSecureRandom() {
        return this.journaling;
    }
    
    public void storeState(File file) throws IOException {
        if (file != null) {
            file = (File)new FileOutputStream(file);
            try {
                this.storeState((OutputStream)file);
                return;
            }
            finally {
                ((FileOutputStream)file).close();
            }
        }
        throw new NullPointerException("file for storage is null in JournaledAlgorithm");
    }
    
    public void storeState(final OutputStream outputStream) throws IOException {
        if (outputStream != null) {
            outputStream.write(this.getEncoded());
            return;
        }
        throw new NullPointerException("output stream for storage is null in JournaledAlgorithm");
    }
}
