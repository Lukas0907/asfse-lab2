// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.generators;

import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.digests.Blake2bDigest;
import org.bouncycastle.util.Pack;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.params.Argon2Parameters;

public class Argon2BytesGenerator
{
    private static final int ARGON2_ADDRESSES_IN_BLOCK = 128;
    private static final int ARGON2_BLOCK_SIZE = 1024;
    private static final int ARGON2_PREHASH_DIGEST_LENGTH = 64;
    private static final int ARGON2_PREHASH_SEED_LENGTH = 72;
    private static final int ARGON2_QWORDS_IN_BLOCK = 128;
    private static final int ARGON2_SYNC_POINTS = 4;
    private static final int MAX_PARALLELISM = 16777216;
    private static final int MIN_ITERATIONS = 1;
    private static final int MIN_OUTLEN = 4;
    private static final int MIN_PARALLELISM = 1;
    private int laneLength;
    private Block[] memory;
    private Argon2Parameters parameters;
    private byte[] result;
    private int segmentLength;
    
    private static void F(final Block block, final int n, final int n2, final int n3, final int n4) {
        fBlaMka(block, n, n2);
        rotr64(block, n4, n, 32L);
        fBlaMka(block, n3, n4);
        rotr64(block, n2, n3, 24L);
        fBlaMka(block, n, n2);
        rotr64(block, n4, n, 16L);
        fBlaMka(block, n3, n4);
        rotr64(block, n2, n3, 63L);
    }
    
    private static void addByteString(final Digest digest, final byte[] array) {
        if (array != null) {
            addIntToLittleEndian(digest, array.length);
            digest.update(array, 0, array.length);
            return;
        }
        addIntToLittleEndian(digest, 0);
    }
    
    private static void addIntToLittleEndian(final Digest digest, final int n) {
        digest.update((byte)n);
        digest.update((byte)(n >>> 8));
        digest.update((byte)(n >>> 16));
        digest.update((byte)(n >>> 24));
    }
    
    private void digest(final int n) {
        final Block block = this.memory[this.laneLength - 1];
        for (int i = 1; i < this.parameters.getLanes(); ++i) {
            final int laneLength = this.laneLength;
            block.xorWith(this.memory[i * laneLength + (laneLength - 1)]);
        }
        this.result = this.hash(block.toBytes(), n);
    }
    
    private void doInit(final Argon2Parameters argon2Parameters) {
        int memory;
        if ((memory = argon2Parameters.getMemory()) < argon2Parameters.getLanes() * 8) {
            memory = argon2Parameters.getLanes() * 8;
        }
        this.segmentLength = memory / (argon2Parameters.getLanes() * 4);
        final int segmentLength = this.segmentLength;
        this.laneLength = segmentLength * 4;
        this.initMemory(segmentLength * (argon2Parameters.getLanes() * 4));
    }
    
    private static void fBlaMka(final Block block, final int n, final int n2) {
        block.v[n] = block.v[n] + block.v[n2] + (block.v[n] & 0xFFFFFFFFL) * (block.v[n2] & 0xFFFFFFFFL) * 2L;
    }
    
    private void fillFirstBlocks(byte[] initialHashLong) {
        final byte[] initialHashLong2 = this.getInitialHashLong(initialHashLong, new byte[] { 0, 0, 0, 0 });
        initialHashLong = this.getInitialHashLong(initialHashLong, new byte[] { 1, 0, 0, 0 });
        for (int i = 0; i < this.parameters.getLanes(); ++i) {
            Pack.intToLittleEndian(i, initialHashLong2, 68);
            Pack.intToLittleEndian(i, initialHashLong, 68);
            this.memory[this.laneLength * i + 0].fromBytes(this.hash(initialHashLong2, 1024));
            this.memory[this.laneLength * i + 1].fromBytes(this.hash(initialHashLong, 1024));
        }
    }
    
    private void fillMemoryBlocks() {
        final FillBlock fillBlock = new FillBlock();
        final Position position = new Position();
        for (int i = 0; i < this.parameters.getIterations(); ++i) {
            for (int j = 0; j < 4; ++j) {
                for (int k = 0; k < this.parameters.getLanes(); ++k) {
                    position.update(i, k, j, 0);
                    this.fillSegment(fillBlock, position);
                }
            }
        }
    }
    
    private void fillSegment(final FillBlock fillBlock, final Position position) {
        final boolean dataIndependentAddressing = this.isDataIndependentAddressing(position);
        final int startingIndex = getStartingIndex(position);
        int n = position.lane * this.laneLength + position.slice * this.segmentLength + startingIndex;
        int prevOffset = this.getPrevOffset(n);
        Block clear;
        Block clear2;
        Block clear3;
        if (dataIndependentAddressing) {
            clear = fillBlock.addressBlock.clear();
            clear2 = fillBlock.zeroBlock.clear();
            clear3 = fillBlock.inputBlock.clear();
            this.initAddressBlocks(fillBlock, position, clear2, clear3, clear);
        }
        else {
            final Block block = null;
            clear3 = (clear2 = block);
            clear = block;
        }
        position.index = startingIndex;
        while (position.index < this.segmentLength) {
            final int rotatePrevOffset = this.rotatePrevOffset(n, prevOffset);
            final long pseudoRandom = this.getPseudoRandom(fillBlock, position, clear, clear3, clear2, rotatePrevOffset, dataIndependentAddressing);
            final int refLane = this.getRefLane(position, pseudoRandom);
            final int refColumn = this.getRefColumn(position, pseudoRandom, refLane == position.lane);
            final Block[] memory = this.memory;
            final Block block2 = memory[rotatePrevOffset];
            final Block block3 = memory[this.laneLength * refLane + refColumn];
            final Block block4 = memory[n];
            if (this.isWithXor(position)) {
                fillBlock.fillBlockWithXor(block2, block3, block4);
            }
            else {
                fillBlock.fillBlock(block2, block3, block4);
            }
            ++position.index;
            ++n;
            prevOffset = rotatePrevOffset + 1;
        }
    }
    
    private byte[] getInitialHashLong(final byte[] array, final byte[] array2) {
        final byte[] array3 = new byte[72];
        System.arraycopy(array, 0, array3, 0, 64);
        System.arraycopy(array2, 0, array3, 64, 4);
        return array3;
    }
    
    private int getPrevOffset(final int n) {
        final int laneLength = this.laneLength;
        if (n % laneLength == 0) {
            return n + laneLength - 1;
        }
        return n - 1;
    }
    
    private long getPseudoRandom(final FillBlock fillBlock, final Position position, final Block block, final Block block2, final Block block3, final int n, final boolean b) {
        if (b) {
            if (position.index % 128 == 0) {
                this.nextAddresses(fillBlock, block3, block2, block);
            }
            return block.v[position.index % 128];
        }
        return this.memory[n].v[0];
    }
    
    private int getRefColumn(final Position position, long n, final boolean b) {
        final int pass = position.pass;
        int n2 = -1;
        int n3;
        int n4;
        if (pass == 0) {
            if (b) {
                n3 = position.slice * this.segmentLength + position.index - 1;
            }
            else {
                final int slice = position.slice;
                final int segmentLength = this.segmentLength;
                if (position.index != 0) {
                    n2 = 0;
                }
                n3 = slice * segmentLength + n2;
            }
            n4 = 0;
        }
        else {
            final int slice2 = position.slice;
            final int segmentLength2 = this.segmentLength;
            final int laneLength = this.laneLength;
            n4 = (slice2 + 1) * segmentLength2 % laneLength;
            final int n5 = laneLength - segmentLength2;
            final int index = position.index;
            if (b) {
                n3 = n5 + index - 1;
            }
            else {
                if (index != 0) {
                    n2 = 0;
                }
                n3 = n5 + n2;
            }
        }
        n &= 0xFFFFFFFFL;
        return (int)(n4 + (n3 - 1 - (n3 * (n * n >>> 32) >>> 32))) % this.laneLength;
    }
    
    private int getRefLane(final Position position, final long n) {
        int lane = (int)((n >>> 32) % this.parameters.getLanes());
        if (position.pass == 0) {
            lane = lane;
            if (position.slice == 0) {
                lane = position.lane;
            }
        }
        return lane;
    }
    
    private static int getStartingIndex(final Position position) {
        if (position.pass == 0 && position.slice == 0) {
            return 2;
        }
        return 0;
    }
    
    private byte[] hash(final byte[] array, final int n) {
        final byte[] array2 = new byte[n];
        final byte[] intToLittleEndian = Pack.intToLittleEndian(n);
        if (n <= 64) {
            final Blake2bDigest blake2bDigest = new Blake2bDigest(n * 8);
            blake2bDigest.update(intToLittleEndian, 0, intToLittleEndian.length);
            blake2bDigest.update(array, 0, array.length);
            blake2bDigest.doFinal(array2, 0);
            return array2;
        }
        final Blake2bDigest blake2bDigest2 = new Blake2bDigest(512);
        final byte[] array3 = new byte[64];
        blake2bDigest2.update(intToLittleEndian, 0, intToLittleEndian.length);
        blake2bDigest2.update(array, 0, array.length);
        blake2bDigest2.doFinal(array3, 0);
        System.arraycopy(array3, 0, array2, 0, 32);
        final int n2 = (n + 31) / 32;
        int i;
        int n3;
        int n4;
        for (i = 2, n3 = n2 - 2, n4 = 32; i <= n3; ++i, n4 += 32) {
            blake2bDigest2.update(array3, 0, array3.length);
            blake2bDigest2.doFinal(array3, 0);
            System.arraycopy(array3, 0, array2, n4, 32);
        }
        final Blake2bDigest blake2bDigest3 = new Blake2bDigest((n - n3 * 32) * 8);
        blake2bDigest3.update(array3, 0, array3.length);
        blake2bDigest3.doFinal(array2, n4);
        return array2;
    }
    
    private void initAddressBlocks(final FillBlock fillBlock, final Position position, final Block block, final Block block2, final Block block3) {
        block2.v[0] = this.intToLong(position.pass);
        block2.v[1] = this.intToLong(position.lane);
        block2.v[2] = this.intToLong(position.slice);
        block2.v[3] = this.intToLong(this.memory.length);
        block2.v[4] = this.intToLong(this.parameters.getIterations());
        block2.v[5] = this.intToLong(this.parameters.getType());
        if (position.pass == 0 && position.slice == 0) {
            this.nextAddresses(fillBlock, block, block2, block3);
        }
    }
    
    private void initMemory(int n) {
        this.memory = new Block[n];
        n = 0;
        while (true) {
            final Block[] memory = this.memory;
            if (n >= memory.length) {
                break;
            }
            memory[n] = new Block();
            ++n;
        }
    }
    
    private byte[] initialHash(final Argon2Parameters argon2Parameters, final int n, final byte[] array) {
        final Blake2bDigest blake2bDigest = new Blake2bDigest(512);
        addIntToLittleEndian(blake2bDigest, argon2Parameters.getLanes());
        addIntToLittleEndian(blake2bDigest, n);
        addIntToLittleEndian(blake2bDigest, argon2Parameters.getMemory());
        addIntToLittleEndian(blake2bDigest, argon2Parameters.getIterations());
        addIntToLittleEndian(blake2bDigest, argon2Parameters.getVersion());
        addIntToLittleEndian(blake2bDigest, argon2Parameters.getType());
        addByteString(blake2bDigest, array);
        addByteString(blake2bDigest, argon2Parameters.getSalt());
        addByteString(blake2bDigest, argon2Parameters.getSecret());
        addByteString(blake2bDigest, argon2Parameters.getAdditional());
        final byte[] array2 = new byte[blake2bDigest.getDigestSize()];
        blake2bDigest.doFinal(array2, 0);
        return array2;
    }
    
    private void initialize(final byte[] array, final int n) {
        this.fillFirstBlocks(this.initialHash(this.parameters, n, array));
    }
    
    private long intToLong(final int n) {
        return (long)n & 0xFFFFFFFFL;
    }
    
    private boolean isDataIndependentAddressing(final Position position) {
        final int type = this.parameters.getType();
        boolean b = true;
        if (type != 1) {
            if (this.parameters.getType() == 2 && position.pass == 0 && position.slice < 2) {
                return true;
            }
            b = false;
        }
        return b;
    }
    
    private boolean isWithXor(final Position position) {
        return position.pass != 0 && this.parameters.getVersion() != 16;
    }
    
    private void nextAddresses(final FillBlock fillBlock, final Block block, final Block block2, final Block block3) {
        final long[] access$400 = block2.v;
        ++access$400[6];
        fillBlock.fillBlock(block, block2, block3);
        fillBlock.fillBlock(block, block3, block3);
    }
    
    private void reset() {
        int n = 0;
        while (true) {
            final Block[] memory = this.memory;
            if (n >= memory.length) {
                break;
            }
            memory[n].clear();
            ++n;
        }
        this.memory = null;
        Arrays.fill(this.result, (byte)0);
    }
    
    private int rotatePrevOffset(final int n, int n2) {
        if (n % this.laneLength == 1) {
            n2 = n - 1;
        }
        return n2;
    }
    
    private static void rotr64(final Block block, final int n, int n2, final long n3) {
        final long n4 = block.v[n] ^ block.v[n2];
        final long[] access$400 = block.v;
        n2 = (int)n3;
        access$400[n] = (n4 << (int)(64L - n3) | n4 >>> n2);
    }
    
    private static void roundFunction(final Block block, final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final int n7, final int n8, final int n9, final int n10, final int n11, final int n12, final int n13, final int n14, final int n15, final int n16) {
        F(block, n, n5, n9, n13);
        F(block, n2, n6, n10, n14);
        F(block, n3, n7, n11, n15);
        F(block, n4, n8, n12, n16);
        F(block, n, n6, n11, n16);
        F(block, n2, n7, n12, n13);
        F(block, n3, n8, n9, n14);
        F(block, n4, n5, n10, n15);
    }
    
    public int generateBytes(final byte[] array, final byte[] array2) {
        return this.generateBytes(array, array2, 0, array2.length);
    }
    
    public int generateBytes(final byte[] array, final byte[] array2, final int n, final int n2) {
        if (n2 >= 4) {
            this.initialize(array, n2);
            this.fillMemoryBlocks();
            this.digest(n2);
            System.arraycopy(this.result, 0, array2, n, n2);
            this.reset();
            return n2;
        }
        throw new IllegalStateException("output length less than 4");
    }
    
    public int generateBytes(final char[] array, final byte[] array2) {
        return this.generateBytes(this.parameters.getCharToByteConverter().convert(array), array2);
    }
    
    public int generateBytes(final char[] array, final byte[] array2, final int n, final int n2) {
        return this.generateBytes(this.parameters.getCharToByteConverter().convert(array), array2, n, n2);
    }
    
    public void init(final Argon2Parameters parameters) {
        this.parameters = parameters;
        if (parameters.getLanes() < 1) {
            throw new IllegalStateException("lanes must be greater than 1");
        }
        if (parameters.getLanes() > 16777216) {
            throw new IllegalStateException("lanes must be less than 16777216");
        }
        if (parameters.getMemory() < parameters.getLanes() * 2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("memory is less than: ");
            sb.append(parameters.getLanes() * 2);
            sb.append(" expected ");
            sb.append(parameters.getLanes() * 2);
            throw new IllegalStateException(sb.toString());
        }
        if (parameters.getIterations() >= 1) {
            this.doInit(parameters);
            return;
        }
        throw new IllegalStateException("iterations is less than: 1");
    }
    
    private static class Block
    {
        private static final int SIZE = 128;
        private final long[] v;
        
        private Block() {
            this.v = new long[128];
        }
        
        private void copyBlock(final Block block) {
            System.arraycopy(block.v, 0, this.v, 0, 128);
        }
        
        private void xor(final Block block, final Block block2) {
            for (int i = 0; i < 128; ++i) {
                this.v[i] = (block.v[i] ^ block2.v[i]);
            }
        }
        
        private void xorWith(final Block block) {
            int n = 0;
            while (true) {
                final long[] v = this.v;
                if (n >= v.length) {
                    break;
                }
                v[n] ^= block.v[n];
                ++n;
            }
        }
        
        public Block clear() {
            Arrays.fill(this.v, 0L);
            return this;
        }
        
        void fromBytes(final byte[] array) {
            if (array.length == 1024) {
                for (int i = 0; i < 128; ++i) {
                    this.v[i] = Pack.littleEndianToLong(array, i * 8);
                }
                return;
            }
            throw new IllegalArgumentException("input shorter than blocksize");
        }
        
        byte[] toBytes() {
            final byte[] array = new byte[1024];
            for (int i = 0; i < 128; ++i) {
                Pack.longToLittleEndian(this.v[i], array, i * 8);
            }
            return array;
        }
        
        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < 128; ++i) {
                sb.append(Hex.toHexString(Pack.longToLittleEndian(this.v[i])));
            }
            return sb.toString();
        }
        
        public void xor(final Block block, final Block block2, final Block block3) {
            for (int i = 0; i < 128; ++i) {
                this.v[i] = (block.v[i] ^ block2.v[i] ^ block3.v[i]);
            }
        }
    }
    
    private static class FillBlock
    {
        Block R;
        Block Z;
        Block addressBlock;
        Block inputBlock;
        Block zeroBlock;
        
        private FillBlock() {
            this.R = new Block();
            this.Z = new Block();
            this.addressBlock = new Block();
            this.zeroBlock = new Block();
            this.inputBlock = new Block();
        }
        
        private void applyBlake() {
            final int n = 0;
            int n2 = 0;
            int i;
            while (true) {
                i = n;
                if (n2 >= 8) {
                    break;
                }
                final int n3 = n2 * 16;
                roundFunction(this.Z, n3, n3 + 1, n3 + 2, n3 + 3, n3 + 4, n3 + 5, n3 + 6, n3 + 7, n3 + 8, n3 + 9, n3 + 10, n3 + 11, n3 + 12, n3 + 13, n3 + 14, n3 + 15);
                ++n2;
            }
            while (i < 8) {
                final int n4 = i * 2;
                roundFunction(this.Z, n4, n4 + 1, n4 + 16, n4 + 17, n4 + 32, n4 + 33, n4 + 48, n4 + 49, n4 + 64, n4 + 65, n4 + 80, n4 + 81, n4 + 96, n4 + 97, n4 + 112, n4 + 113);
                ++i;
            }
        }
        
        private void fillBlock(final Block block, final Block block2, final Block block3) {
            if (block == this.zeroBlock) {
                this.R.copyBlock(block2);
            }
            else {
                this.R.xor(block, block2);
            }
            this.Z.copyBlock(this.R);
            this.applyBlake();
            block3.xor(this.R, this.Z);
        }
        
        private void fillBlockWithXor(final Block block, final Block block2, final Block block3) {
            this.R.xor(block, block2);
            this.Z.copyBlock(this.R);
            this.applyBlake();
            block3.xor(this.R, this.Z, block3);
        }
    }
    
    private static class Position
    {
        int index;
        int lane;
        int pass;
        int slice;
        
        Position() {
        }
        
        void update(final int pass, final int lane, final int slice, final int index) {
            this.pass = pass;
            this.lane = lane;
            this.slice = slice;
            this.index = index;
        }
    }
}
