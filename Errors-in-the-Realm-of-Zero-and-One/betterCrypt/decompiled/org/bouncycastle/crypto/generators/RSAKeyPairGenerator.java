// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.generators;

import org.bouncycastle.math.Primes;
import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.math.ec.WNafUtil;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import java.math.BigInteger;
import org.bouncycastle.crypto.AsymmetricCipherKeyPairGenerator;

public class RSAKeyPairGenerator implements AsymmetricCipherKeyPairGenerator
{
    private static final BigInteger ONE;
    private RSAKeyGenerationParameters param;
    
    static {
        ONE = BigInteger.valueOf(1L);
    }
    
    private static int getNumberOfIterations(final int n, final int n2) {
        if (n >= 1536) {
            if (n2 <= 100) {
                return 3;
            }
            if (n2 <= 128) {
                return 4;
            }
            return 4 + (n2 - 128 + 1) / 2;
        }
        else if (n >= 1024) {
            if (n2 <= 100) {
                return 4;
            }
            if (n2 <= 112) {
                return 5;
            }
            return (n2 - 112 + 1) / 2 + 5;
        }
        else if (n >= 512) {
            if (n2 <= 80) {
                return 5;
            }
            if (n2 <= 100) {
                return 7;
            }
            return (n2 - 100 + 1) / 2 + 7;
        }
        else {
            if (n2 <= 80) {
                return 40;
            }
            return 40 + (n2 - 80 + 1) / 2;
        }
    }
    
    protected BigInteger chooseRandomPrime(final int n, final BigInteger m, final BigInteger val) {
        for (int i = 0; i != n * 5; ++i) {
            final BigInteger randomPrime = BigIntegers.createRandomPrime(n, 1, this.param.getRandom());
            if (!randomPrime.mod(m).equals(RSAKeyPairGenerator.ONE)) {
                if (randomPrime.multiply(randomPrime).compareTo(val) >= 0) {
                    if (this.isProbablePrime(randomPrime)) {
                        if (m.gcd(randomPrime.subtract(RSAKeyPairGenerator.ONE)).equals(RSAKeyPairGenerator.ONE)) {
                            return randomPrime;
                        }
                    }
                }
            }
        }
        throw new IllegalStateException("unable to generate prime number for RSA key");
    }
    
    @Override
    public AsymmetricCipherKeyPair generateKeyPair() {
        final int strength = this.param.getStrength();
        final int n = (strength + 1) / 2;
        final int exponent = strength / 2;
        final int n2 = exponent - 100;
        final int n3 = strength / 3;
        int n4 = n2;
        if (n2 < n3) {
            n4 = n3;
        }
        final BigInteger pow = BigInteger.valueOf(2L).pow(exponent);
        final BigInteger shiftLeft = RSAKeyPairGenerator.ONE.shiftLeft(strength - 1);
        final BigInteger shiftLeft2 = RSAKeyPairGenerator.ONE.shiftLeft(n4);
        AsymmetricCipherKeyPair asymmetricCipherKeyPair = null;
        int n5 = 0;
        final int n6 = strength;
        while (true) {
            RSAKeyPairGenerator rsaKeyPairGenerator = this;
            if (n5 != 0) {
                break;
            }
            final BigInteger publicExponent = rsaKeyPairGenerator.param.getPublicExponent();
            BigInteger val;
            BigInteger chooseRandomPrime;
            BigInteger multiply;
            do {
                val = rsaKeyPairGenerator.chooseRandomPrime(n, publicExponent, shiftLeft);
                while (true) {
                    chooseRandomPrime = rsaKeyPairGenerator.chooseRandomPrime(strength - n, publicExponent, shiftLeft);
                    final BigInteger abs = chooseRandomPrime.subtract(val).abs();
                    if (abs.bitLength() >= n4 && abs.compareTo(shiftLeft2) > 0) {
                        multiply = val.multiply(chooseRandomPrime);
                        if (multiply.bitLength() == n6) {
                            break;
                        }
                        val = val.max(chooseRandomPrime);
                    }
                    else {
                        rsaKeyPairGenerator = this;
                    }
                }
            } while (WNafUtil.getNafWeight(multiply) < strength >> 2);
            BigInteger m;
            if (val.compareTo(chooseRandomPrime) < 0) {
                m = chooseRandomPrime;
            }
            else {
                m = val;
                val = chooseRandomPrime;
            }
            final BigInteger subtract = m.subtract(RSAKeyPairGenerator.ONE);
            final BigInteger subtract2 = val.subtract(RSAKeyPairGenerator.ONE);
            final BigInteger modInverse = publicExponent.modInverse(subtract.divide(subtract.gcd(subtract2)).multiply(subtract2));
            if (modInverse.compareTo(pow) <= 0) {
                continue;
            }
            asymmetricCipherKeyPair = new AsymmetricCipherKeyPair(new RSAKeyParameters(false, multiply, publicExponent), new RSAPrivateCrtKeyParameters(multiply, publicExponent, modInverse, m, val, modInverse.remainder(subtract), modInverse.remainder(subtract2), val.modInverse(m)));
            n5 = 1;
        }
        return asymmetricCipherKeyPair;
    }
    
    @Override
    public void init(final KeyGenerationParameters keyGenerationParameters) {
        this.param = (RSAKeyGenerationParameters)keyGenerationParameters;
    }
    
    protected boolean isProbablePrime(final BigInteger bigInteger) {
        final int numberOfIterations = getNumberOfIterations(bigInteger.bitLength(), this.param.getCertainty());
        return !Primes.hasAnySmallFactors(bigInteger) && Primes.isMRProbablePrime(bigInteger, this.param.getRandom(), numberOfIterations);
    }
}
