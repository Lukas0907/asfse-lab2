// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.generators;

import org.bouncycastle.math.ec.WNafUtil;
import org.bouncycastle.util.BigIntegers;
import java.security.SecureRandom;
import java.math.BigInteger;

class DHParametersHelper
{
    private static final BigInteger ONE;
    private static final BigInteger TWO;
    
    static {
        ONE = BigInteger.valueOf(1L);
        TWO = BigInteger.valueOf(2L);
    }
    
    static BigInteger[] generateSafePrimes(final int n, final int certainty, final SecureRandom secureRandom) {
        BigInteger randomPrime;
        BigInteger add;
        while (true) {
            randomPrime = BigIntegers.createRandomPrime(n - 1, 2, secureRandom);
            add = randomPrime.shiftLeft(1).add(DHParametersHelper.ONE);
            if (!add.isProbablePrime(certainty)) {
                continue;
            }
            if (certainty > 2 && !randomPrime.isProbablePrime(certainty - 2)) {
                continue;
            }
            if (WNafUtil.getNafWeight(add) < n >>> 2) {
                continue;
            }
            break;
        }
        return new BigInteger[] { add, randomPrime };
    }
    
    static BigInteger selectGenerator(final BigInteger m, BigInteger subtract, final SecureRandom secureRandom) {
        subtract = m.subtract(DHParametersHelper.TWO);
        BigInteger modPow;
        do {
            modPow = BigIntegers.createRandomInRange(DHParametersHelper.TWO, subtract, secureRandom).modPow(DHParametersHelper.TWO, m);
        } while (modPow.equals(DHParametersHelper.ONE));
        return modPow;
    }
}
