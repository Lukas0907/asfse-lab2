// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.generators;

import org.bouncycastle.crypto.params.ISO18033KDFParameters;
import org.bouncycastle.crypto.params.KDFParameters;
import org.bouncycastle.crypto.DerivationParameters;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.util.Pack;
import org.bouncycastle.crypto.OutputLengthException;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.DigestDerivationFunction;

public class BaseKDFBytesGenerator implements DigestDerivationFunction
{
    private int counterStart;
    private Digest digest;
    private byte[] iv;
    private byte[] shared;
    
    protected BaseKDFBytesGenerator(final int counterStart, final Digest digest) {
        this.counterStart = counterStart;
        this.digest = digest;
    }
    
    @Override
    public int generateBytes(final byte[] array, int i, int n) throws DataLengthException, IllegalArgumentException {
        if (array.length - n < i) {
            throw new OutputLengthException("output buffer too small");
        }
        final long n2 = n;
        final int digestSize = this.digest.getDigestSize();
        if (n2 <= 8589934591L) {
            final long n3 = digestSize;
            final int n4 = (int)((n2 + n3 - 1L) / n3);
            final byte[] array2 = new byte[this.digest.getDigestSize()];
            final byte[] array3 = new byte[4];
            Pack.intToBigEndian(this.counterStart, array3, 0);
            final int n5 = this.counterStart & 0xFFFFFF00;
            final int n6 = i;
            i = 0;
            int n7 = n;
            n = n5;
            int n8 = n6;
            while (i < n4) {
                final Digest digest = this.digest;
                final byte[] shared = this.shared;
                digest.update(shared, 0, shared.length);
                this.digest.update(array3, 0, array3.length);
                final byte[] iv = this.iv;
                if (iv != null) {
                    this.digest.update(iv, 0, iv.length);
                }
                this.digest.doFinal(array2, 0);
                int n11;
                int n12;
                if (n7 > digestSize) {
                    System.arraycopy(array2, 0, array, n8, digestSize);
                    final int n9 = n8 + digestSize;
                    final int n10 = n7 - digestSize;
                    n11 = n9;
                    n12 = n10;
                }
                else {
                    System.arraycopy(array2, 0, array, n8, n7);
                    n12 = n7;
                    n11 = n8;
                }
                final byte b = (byte)(array3[3] + 1);
                array3[3] = b;
                int n13 = n;
                if (b == 0) {
                    n13 = n + 256;
                    Pack.intToBigEndian(n13, array3, 0);
                }
                ++i;
                n8 = n11;
                n = n13;
                n7 = n12;
            }
            this.digest.reset();
            return (int)n2;
        }
        throw new IllegalArgumentException("Output length too large");
    }
    
    @Override
    public Digest getDigest() {
        return this.digest;
    }
    
    @Override
    public void init(final DerivationParameters derivationParameters) {
        if (derivationParameters instanceof KDFParameters) {
            final KDFParameters kdfParameters = (KDFParameters)derivationParameters;
            this.shared = kdfParameters.getSharedSecret();
            this.iv = kdfParameters.getIV();
            return;
        }
        if (derivationParameters instanceof ISO18033KDFParameters) {
            this.shared = ((ISO18033KDFParameters)derivationParameters).getSeed();
            this.iv = null;
            return;
        }
        throw new IllegalArgumentException("KDF parameters required for generator");
    }
}
