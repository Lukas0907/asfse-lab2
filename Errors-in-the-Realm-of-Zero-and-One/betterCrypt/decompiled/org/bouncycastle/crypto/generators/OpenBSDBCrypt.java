// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.generators;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.util.Strings;
import java.io.ByteArrayOutputStream;
import org.bouncycastle.crypto.DataLengthException;
import java.util.HashSet;
import java.util.Set;

public class OpenBSDBCrypt
{
    private static final Set<String> allowedVersions;
    private static final byte[] decodingTable;
    private static final String defaultVersion = "2y";
    private static final byte[] encodingTable;
    
    static {
        encodingTable = new byte[] { 46, 47, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57 };
        decodingTable = new byte[128];
        (allowedVersions = new HashSet<String>()).add("2a");
        OpenBSDBCrypt.allowedVersions.add("2y");
        OpenBSDBCrypt.allowedVersions.add("2b");
        final int n = 0;
        int n2 = 0;
        int n3;
        while (true) {
            final byte[] decodingTable2 = OpenBSDBCrypt.decodingTable;
            n3 = n;
            if (n2 >= decodingTable2.length) {
                break;
            }
            decodingTable2[n2] = -1;
            ++n2;
        }
        while (true) {
            final byte[] encodingTable2 = OpenBSDBCrypt.encodingTable;
            if (n3 >= encodingTable2.length) {
                break;
            }
            OpenBSDBCrypt.decodingTable[encodingTable2[n3]] = (byte)n3;
            ++n3;
        }
    }
    
    public static boolean checkPassword(final String s, final char[] array) {
        while (true) {
            Label_0257: {
                if (s.length() != 60) {
                    break Label_0257;
                }
                Label_0247: {
                    if (s.charAt(0) != '$' || s.charAt(3) != '$' || s.charAt(6) != '$') {
                        break Label_0247;
                    }
                    final String substring = s.substring(1, 3);
                    Label_0206: {
                        if (!OpenBSDBCrypt.allowedVersions.contains(substring)) {
                            break Label_0206;
                        }
                        final String substring2 = s.substring(4, 6);
                        try {
                            final int int1 = Integer.parseInt(substring2);
                            if (int1 < 4 || int1 > 31) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Invalid cost factor: ");
                                sb.append(int1);
                                sb.append(", 4 < cost < 31 expected.");
                                throw new IllegalArgumentException(sb.toString());
                            }
                            if (array != null) {
                                return s.equals(generate(substring, array, decodeSaltString(s.substring(s.lastIndexOf(36) + 1, s.length() - 31)), int1));
                            }
                            throw new IllegalArgumentException("Missing password.");
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Bcrypt String length: ");
                            sb2.append(s.length());
                            sb2.append(", 60 required.");
                            throw new DataLengthException(sb2.toString());
                            final StringBuilder sb3 = new StringBuilder();
                            sb3.append("Invalid cost factor: ");
                            sb3.append(substring2);
                            throw new IllegalArgumentException(sb3.toString());
                            final StringBuilder sb4 = new StringBuilder();
                            sb4.append("Bcrypt version '");
                            sb4.append(substring);
                            sb4.append("' is not supported by this implementation");
                            throw new IllegalArgumentException(sb4.toString());
                            throw new IllegalArgumentException("Invalid Bcrypt String format.");
                        }
                        catch (NumberFormatException ex) {}
                    }
                }
            }
            continue;
        }
    }
    
    private static String createBcryptString(String str, final byte[] array, final byte[] array2, final int n) {
        if (OpenBSDBCrypt.allowedVersions.contains(str)) {
            final StringBuffer sb = new StringBuffer(60);
            sb.append('$');
            sb.append(str);
            sb.append('$');
            if (n < 10) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("0");
                sb2.append(n);
                str = sb2.toString();
            }
            else {
                str = Integer.toString(n);
            }
            sb.append(str);
            sb.append('$');
            sb.append(encodeData(array2));
            sb.append(encodeData(BCrypt.generate(array, array2, n)));
            return sb.toString();
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("Version ");
        sb3.append(str);
        sb3.append(" is not accepted by this implementation.");
        throw new IllegalArgumentException(sb3.toString());
    }
    
    private static byte[] decodeSaltString(final String s) {
        final char[] charArray = s.toCharArray();
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(16);
        if (charArray.length == 22) {
            for (int i = 0; i < charArray.length; ++i) {
                final char j = charArray[i];
                if (j > 'z' || j < '.' || (j > '9' && j < 'A')) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Salt string contains invalid character: ");
                    sb.append((int)j);
                    throw new IllegalArgumentException(sb.toString());
                }
            }
            final char[] array = new char[24];
            System.arraycopy(charArray, 0, array, 0, charArray.length);
            for (int length = array.length, k = 0; k < length; k += 4) {
                final byte[] decodingTable = OpenBSDBCrypt.decodingTable;
                final byte b = decodingTable[array[k]];
                final byte b2 = decodingTable[array[k + 1]];
                final byte b3 = decodingTable[array[k + 2]];
                final byte b4 = decodingTable[array[k + 3]];
                byteArrayOutputStream.write(b << 2 | b2 >> 4);
                byteArrayOutputStream.write(b2 << 4 | b3 >> 2);
                byteArrayOutputStream.write(b4 | b3 << 6);
            }
            final byte[] byteArray = byteArrayOutputStream.toByteArray();
            final byte[] array2 = new byte[16];
            System.arraycopy(byteArray, 0, array2, 0, array2.length);
            return array2;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Invalid base64 salt length: ");
        sb2.append(charArray.length);
        sb2.append(" , 22 required.");
        throw new DataLengthException(sb2.toString());
    }
    
    private static String encodeData(byte[] array) {
        if (array.length != 24 && array.length != 16) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid length: ");
            sb.append(array.length);
            sb.append(", 24 for key or 16 for salt expected");
            throw new DataLengthException(sb.toString());
        }
        int n;
        if (array.length == 16) {
            final byte[] array2 = new byte[18];
            System.arraycopy(array, 0, array2, 0, array.length);
            array = array2;
            n = 1;
        }
        else {
            array[array.length - 1] = 0;
            n = 0;
        }
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (int length = array.length, i = 0; i < length; i += 3) {
            final int n2 = array[i] & 0xFF;
            final int n3 = array[i + 1] & 0xFF;
            final int n4 = array[i + 2] & 0xFF;
            byteArrayOutputStream.write(OpenBSDBCrypt.encodingTable[n2 >>> 2 & 0x3F]);
            byteArrayOutputStream.write(OpenBSDBCrypt.encodingTable[(n2 << 4 | n3 >>> 4) & 0x3F]);
            byteArrayOutputStream.write(OpenBSDBCrypt.encodingTable[(n3 << 2 | n4 >>> 6) & 0x3F]);
            byteArrayOutputStream.write(OpenBSDBCrypt.encodingTable[n4 & 0x3F]);
        }
        final String fromByteArray = Strings.fromByteArray(byteArrayOutputStream.toByteArray());
        int endIndex;
        if (n == 1) {
            endIndex = 22;
        }
        else {
            endIndex = fromByteArray.length() - 1;
        }
        return fromByteArray.substring(0, endIndex);
    }
    
    public static String generate(String bcryptString, final char[] array, final byte[] array2, final int n) {
        if (!OpenBSDBCrypt.allowedVersions.contains(bcryptString)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Version ");
            sb.append(bcryptString);
            sb.append(" is not accepted by this implementation.");
            throw new IllegalArgumentException(sb.toString());
        }
        if (array == null) {
            throw new IllegalArgumentException("Password required.");
        }
        if (array2 == null) {
            throw new IllegalArgumentException("Salt required.");
        }
        if (array2.length != 16) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("16 byte salt required: ");
            sb2.append(array2.length);
            throw new DataLengthException(sb2.toString());
        }
        if (n >= 4 && n <= 31) {
            final byte[] utf8ByteArray = Strings.toUTF8ByteArray(array);
            final int length = utf8ByteArray.length;
            int n2 = 72;
            if (length < 72) {
                n2 = utf8ByteArray.length + 1;
            }
            final byte[] array3 = new byte[n2];
            int n3;
            if (array3.length > utf8ByteArray.length) {
                n3 = utf8ByteArray.length;
            }
            else {
                n3 = array3.length;
            }
            System.arraycopy(utf8ByteArray, 0, array3, 0, n3);
            Arrays.fill(utf8ByteArray, (byte)0);
            bcryptString = createBcryptString(bcryptString, array3, array2, n);
            Arrays.fill(array3, (byte)0);
            return bcryptString;
        }
        throw new IllegalArgumentException("Invalid cost factor.");
    }
    
    public static String generate(final char[] array, final byte[] array2, final int n) {
        return generate("2y", array, array2, n);
    }
}
