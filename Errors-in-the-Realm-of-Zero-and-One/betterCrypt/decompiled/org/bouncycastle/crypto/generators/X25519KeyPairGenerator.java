// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.generators;

import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.X25519PrivateKeyParameters;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import java.security.SecureRandom;
import org.bouncycastle.crypto.AsymmetricCipherKeyPairGenerator;

public class X25519KeyPairGenerator implements AsymmetricCipherKeyPairGenerator
{
    private SecureRandom random;
    
    @Override
    public AsymmetricCipherKeyPair generateKeyPair() {
        final X25519PrivateKeyParameters x25519PrivateKeyParameters = new X25519PrivateKeyParameters(this.random);
        return new AsymmetricCipherKeyPair(x25519PrivateKeyParameters.generatePublicKey(), x25519PrivateKeyParameters);
    }
    
    @Override
    public void init(final KeyGenerationParameters keyGenerationParameters) {
        this.random = keyGenerationParameters.getRandom();
    }
}
