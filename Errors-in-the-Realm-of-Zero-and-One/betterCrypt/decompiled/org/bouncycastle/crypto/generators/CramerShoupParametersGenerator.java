// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.generators;

import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.crypto.params.DHParameters;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.params.CramerShoupParameters;
import java.security.SecureRandom;
import java.math.BigInteger;

public class CramerShoupParametersGenerator
{
    private static final BigInteger ONE;
    private int certainty;
    private SecureRandom random;
    private int size;
    
    static {
        ONE = BigInteger.valueOf(1L);
    }
    
    public CramerShoupParameters generateParameters() {
        final BigInteger bigInteger = ParametersHelper.generateSafePrimes(this.size, this.certainty, this.random)[1];
        final BigInteger selectGenerator = ParametersHelper.selectGenerator(bigInteger, this.random);
        BigInteger selectGenerator2;
        do {
            selectGenerator2 = ParametersHelper.selectGenerator(bigInteger, this.random);
        } while (selectGenerator.equals(selectGenerator2));
        return new CramerShoupParameters(bigInteger, selectGenerator, selectGenerator2, new SHA256Digest());
    }
    
    public CramerShoupParameters generateParameters(final DHParameters dhParameters) {
        final BigInteger p = dhParameters.getP();
        final BigInteger g = dhParameters.getG();
        BigInteger selectGenerator;
        do {
            selectGenerator = ParametersHelper.selectGenerator(p, this.random);
        } while (g.equals(selectGenerator));
        return new CramerShoupParameters(p, g, selectGenerator, new SHA256Digest());
    }
    
    public void init(final int size, final int certainty, final SecureRandom random) {
        this.size = size;
        this.certainty = certainty;
        this.random = random;
    }
    
    private static class ParametersHelper
    {
        private static final BigInteger TWO;
        
        static {
            TWO = BigInteger.valueOf(2L);
        }
        
        static BigInteger[] generateSafePrimes(final int n, final int n2, final SecureRandom secureRandom) {
            BigInteger add;
            BigInteger randomPrime;
            do {
                randomPrime = BigIntegers.createRandomPrime(n - 1, 2, secureRandom);
                add = randomPrime.shiftLeft(1).add(CramerShoupParametersGenerator.ONE);
            } while (!add.isProbablePrime(n2) || (n2 > 2 && !randomPrime.isProbablePrime(n2)));
            return new BigInteger[] { add, randomPrime };
        }
        
        static BigInteger selectGenerator(final BigInteger m, final SecureRandom secureRandom) {
            final BigInteger subtract = m.subtract(ParametersHelper.TWO);
            BigInteger modPow;
            do {
                modPow = BigIntegers.createRandomInRange(ParametersHelper.TWO, subtract, secureRandom).modPow(ParametersHelper.TWO, m);
            } while (modPow.equals(CramerShoupParametersGenerator.ONE));
            return modPow;
        }
    }
}
