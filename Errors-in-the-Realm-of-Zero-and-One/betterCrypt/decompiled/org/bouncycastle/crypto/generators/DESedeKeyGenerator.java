// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.generators;

import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.params.DESedeParameters;
import org.bouncycastle.crypto.params.DESParameters;

public class DESedeKeyGenerator extends DESKeyGenerator
{
    private static final int MAX_IT = 20;
    
    @Override
    public byte[] generateKey() {
        final byte[] array = new byte[this.strength];
        int n = 0;
        while (true) {
            this.random.nextBytes(array);
            DESParameters.setOddParity(array);
            final int n2 = n + 1;
            if (n2 >= 20) {
                break;
            }
            n = n2;
            if (DESedeParameters.isWeakKey(array, 0, array.length)) {
                continue;
            }
            n = n2;
            if (DESedeParameters.isRealEDEKey(array, 0)) {
                break;
            }
        }
        if (!DESedeParameters.isWeakKey(array, 0, array.length) && DESedeParameters.isRealEDEKey(array, 0)) {
            return array;
        }
        throw new IllegalStateException("Unable to generate DES-EDE key");
    }
    
    @Override
    public void init(final KeyGenerationParameters keyGenerationParameters) {
        this.random = keyGenerationParameters.getRandom();
        this.strength = (keyGenerationParameters.getStrength() + 7) / 8;
        if (this.strength != 0 && this.strength != 21) {
            if (this.strength == 14) {
                this.strength = 16;
                return;
            }
            if (this.strength != 24) {
                if (this.strength == 16) {
                    return;
                }
                throw new IllegalArgumentException("DESede key must be 192 or 128 bits long.");
            }
        }
        else {
            this.strength = 24;
        }
    }
}
