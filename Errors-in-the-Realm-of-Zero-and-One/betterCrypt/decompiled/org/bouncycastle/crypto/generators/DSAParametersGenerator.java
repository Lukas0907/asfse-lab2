// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.generators;

import org.bouncycastle.crypto.params.DSAParameterGenerationParameters;
import org.bouncycastle.crypto.params.DSAValidationParameters;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.params.DSAParameters;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.crypto.util.DigestFactory;
import java.security.SecureRandom;
import org.bouncycastle.crypto.Digest;
import java.math.BigInteger;

public class DSAParametersGenerator
{
    private static final BigInteger ONE;
    private static final BigInteger TWO;
    private static final BigInteger ZERO;
    private int L;
    private int N;
    private int certainty;
    private Digest digest;
    private int iterations;
    private SecureRandom random;
    private int usageIndex;
    private boolean use186_3;
    
    static {
        ZERO = BigInteger.valueOf(0L);
        ONE = BigInteger.valueOf(1L);
        TWO = BigInteger.valueOf(2L);
    }
    
    public DSAParametersGenerator() {
        this(DigestFactory.createSHA1());
    }
    
    public DSAParametersGenerator(final Digest digest) {
        this.digest = digest;
    }
    
    private static BigInteger calculateGenerator_FIPS186_2(final BigInteger m, BigInteger divide, final SecureRandom secureRandom) {
        divide = m.subtract(DSAParametersGenerator.ONE).divide(divide);
        final BigInteger subtract = m.subtract(DSAParametersGenerator.TWO);
        BigInteger modPow;
        do {
            modPow = BigIntegers.createRandomInRange(DSAParametersGenerator.TWO, subtract, secureRandom).modPow(divide, m);
        } while (modPow.bitLength() <= 1);
        return modPow;
    }
    
    private static BigInteger calculateGenerator_FIPS186_3_Unverifiable(final BigInteger bigInteger, final BigInteger bigInteger2, final SecureRandom secureRandom) {
        return calculateGenerator_FIPS186_2(bigInteger, bigInteger2, secureRandom);
    }
    
    private static BigInteger calculateGenerator_FIPS186_3_Verifiable(final Digest digest, final BigInteger m, BigInteger divide, byte[] magnitude, int i) {
        divide = m.subtract(DSAParametersGenerator.ONE).divide(divide);
        final byte[] decodeStrict = Hex.decodeStrict("6767656E");
        final byte[] array = new byte[magnitude.length + decodeStrict.length + 1 + 2];
        System.arraycopy(magnitude, 0, array, 0, magnitude.length);
        System.arraycopy(decodeStrict, 0, array, magnitude.length, decodeStrict.length);
        array[array.length - 3] = (byte)i;
        magnitude = new byte[digest.getDigestSize()];
        BigInteger modPow;
        for (i = 1; i < 65536; ++i) {
            inc(array);
            hash(digest, array, magnitude, 0);
            modPow = new BigInteger(1, magnitude).modPow(divide, m);
            if (modPow.compareTo(DSAParametersGenerator.TWO) >= 0) {
                return modPow;
            }
        }
        return null;
    }
    
    private DSAParameters generateParameters_FIPS186_2() {
        final byte[] bytes = new byte[20];
        final byte[] array = new byte[20];
        final byte[] array2 = new byte[20];
        final byte[] magnitude = new byte[20];
        final int l = this.L;
        final int n = (l - 1) / 160;
        final byte[] magnitude2 = new byte[l / 8];
        if (this.digest instanceof SHA1Digest) {
            BigInteger bigInteger = null;
            int j = 0;
            BigInteger subtract = null;
        Block_6:
            while (true) {
                this.random.nextBytes(bytes);
                hash(this.digest, bytes, array, 0);
                System.arraycopy(bytes, 0, array2, 0, bytes.length);
                inc(array2);
                hash(this.digest, array2, array2, 0);
                for (int i = 0; i != magnitude.length; ++i) {
                    magnitude[i] = (byte)(array[i] ^ array2[i]);
                }
                magnitude[0] |= 0xFFFFFF80;
                magnitude[19] |= 0x1;
                bigInteger = new BigInteger(1, magnitude);
                if (!this.isProbablePrime(bigInteger)) {
                    continue;
                }
                final byte[] clone = Arrays.clone(bytes);
                inc(clone);
                for (j = 0; j < 4096; ++j) {
                    for (int k = 1; k <= n; ++k) {
                        inc(clone);
                        hash(this.digest, clone, magnitude2, magnitude2.length - array.length * k);
                    }
                    final int n2 = magnitude2.length - array.length * n;
                    inc(clone);
                    hash(this.digest, clone, array, 0);
                    System.arraycopy(array, array.length - n2, magnitude2, 0, n2);
                    magnitude2[0] |= 0xFFFFFF80;
                    final BigInteger bigInteger2 = new BigInteger(1, magnitude2);
                    subtract = bigInteger2.subtract(bigInteger2.mod(bigInteger.shiftLeft(1)).subtract(DSAParametersGenerator.ONE));
                    if (subtract.bitLength() == this.L) {
                        if (this.isProbablePrime(subtract)) {
                            break Block_6;
                        }
                    }
                }
            }
            return new DSAParameters(subtract, bigInteger, calculateGenerator_FIPS186_2(subtract, bigInteger, this.random), new DSAValidationParameters(bytes, j));
        }
        throw new IllegalStateException("can only use SHA-1 for generating FIPS 186-2 parameters");
    }
    
    private DSAParameters generateParameters_FIPS186_3() {
        final Digest digest = this.digest;
        final int n = digest.getDigestSize() * 8;
        final byte[] bytes = new byte[this.N / 8];
        final int l = this.L;
        final int n2 = (l - 1) / n;
        final byte[] magnitude = new byte[l / 8];
        final byte[] magnitude2 = new byte[digest.getDigestSize()];
        BigInteger setBit = null;
        int j = 0;
        BigInteger subtract = null;
    Block_5:
        while (true) {
            this.random.nextBytes(bytes);
            hash(digest, bytes, magnitude2, 0);
            setBit = new BigInteger(1, magnitude2).mod(DSAParametersGenerator.ONE.shiftLeft(this.N - 1)).setBit(0).setBit(this.N - 1);
            if (!this.isProbablePrime(setBit)) {
                continue;
            }
            final byte[] clone = Arrays.clone(bytes);
            for (final int i = this.L, j = 0; j < i * 4; ++j) {
                for (int k = 1; k <= n2; ++k) {
                    inc(clone);
                    hash(digest, clone, magnitude, magnitude.length - magnitude2.length * k);
                }
                final int n3 = magnitude.length - magnitude2.length * n2;
                inc(clone);
                hash(digest, clone, magnitude2, 0);
                System.arraycopy(magnitude2, magnitude2.length - n3, magnitude, 0, n3);
                magnitude[0] |= 0xFFFFFF80;
                final BigInteger bigInteger = new BigInteger(1, magnitude);
                subtract = bigInteger.subtract(bigInteger.mod(setBit.shiftLeft(1)).subtract(DSAParametersGenerator.ONE));
                if (subtract.bitLength() == this.L) {
                    if (this.isProbablePrime(subtract)) {
                        break Block_5;
                    }
                }
            }
        }
        final int usageIndex = this.usageIndex;
        if (usageIndex >= 0) {
            final BigInteger calculateGenerator_FIPS186_3_Verifiable = calculateGenerator_FIPS186_3_Verifiable(digest, subtract, setBit, bytes, usageIndex);
            if (calculateGenerator_FIPS186_3_Verifiable != null) {
                return new DSAParameters(subtract, setBit, calculateGenerator_FIPS186_3_Verifiable, new DSAValidationParameters(bytes, j, this.usageIndex));
            }
        }
        return new DSAParameters(subtract, setBit, calculateGenerator_FIPS186_3_Unverifiable(subtract, setBit, this.random), new DSAValidationParameters(bytes, j));
    }
    
    private static int getDefaultN(final int n) {
        if (n > 1024) {
            return 256;
        }
        return 160;
    }
    
    private static int getMinimumIterations(final int n) {
        if (n <= 1024) {
            return 40;
        }
        return (n - 1) / 1024 * 8 + 48;
    }
    
    private static void hash(final Digest digest, final byte[] array, final byte[] array2, final int n) {
        digest.update(array, 0, array.length);
        digest.doFinal(array2, n);
    }
    
    private static void inc(final byte[] array) {
        for (int i = array.length - 1; i >= 0; --i) {
            if ((array[i] = (byte)(array[i] + 1 & 0xFF)) != 0) {
                return;
            }
        }
    }
    
    private boolean isProbablePrime(final BigInteger bigInteger) {
        return bigInteger.isProbablePrime(this.certainty);
    }
    
    public DSAParameters generateParameters() {
        if (this.use186_3) {
            return this.generateParameters_FIPS186_3();
        }
        return this.generateParameters_FIPS186_2();
    }
    
    public void init(final int l, final int certainty, final SecureRandom random) {
        this.L = l;
        this.N = getDefaultN(l);
        this.certainty = certainty;
        this.iterations = Math.max(getMinimumIterations(this.L), (certainty + 1) / 2);
        this.random = random;
        this.use186_3 = false;
        this.usageIndex = -1;
    }
    
    public void init(final DSAParameterGenerationParameters dsaParameterGenerationParameters) {
        final int l = dsaParameterGenerationParameters.getL();
        final int n = dsaParameterGenerationParameters.getN();
        if (l < 1024 || l > 3072 || l % 1024 != 0) {
            throw new IllegalArgumentException("L values must be between 1024 and 3072 and a multiple of 1024");
        }
        if (l == 1024 && n != 160) {
            throw new IllegalArgumentException("N must be 160 for L = 1024");
        }
        if (l == 2048 && n != 224 && n != 256) {
            throw new IllegalArgumentException("N must be 224 or 256 for L = 2048");
        }
        if (l == 3072 && n != 256) {
            throw new IllegalArgumentException("N must be 256 for L = 3072");
        }
        if (this.digest.getDigestSize() * 8 >= n) {
            this.L = l;
            this.N = n;
            this.certainty = dsaParameterGenerationParameters.getCertainty();
            this.iterations = Math.max(getMinimumIterations(l), (this.certainty + 1) / 2);
            this.random = dsaParameterGenerationParameters.getRandom();
            this.use186_3 = true;
            this.usageIndex = dsaParameterGenerationParameters.getUsageIndex();
            return;
        }
        throw new IllegalStateException("Digest output size too small for value of N");
    }
}
