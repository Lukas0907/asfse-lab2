// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.generators;

import org.bouncycastle.crypto.params.HKDFParameters;
import org.bouncycastle.crypto.DerivationParameters;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.DerivationFunction;

public class HKDFBytesGenerator implements DerivationFunction
{
    private byte[] currentT;
    private int generatedBytes;
    private HMac hMacHash;
    private int hashLen;
    private byte[] info;
    
    public HKDFBytesGenerator(final Digest digest) {
        this.hMacHash = new HMac(digest);
        this.hashLen = digest.getDigestSize();
    }
    
    private void expandNext() throws DataLengthException {
        final int generatedBytes = this.generatedBytes;
        final int hashLen = this.hashLen;
        final int n = generatedBytes / hashLen + 1;
        if (n < 256) {
            if (generatedBytes != 0) {
                this.hMacHash.update(this.currentT, 0, hashLen);
            }
            final HMac hMacHash = this.hMacHash;
            final byte[] info = this.info;
            hMacHash.update(info, 0, info.length);
            this.hMacHash.update((byte)n);
            this.hMacHash.doFinal(this.currentT, 0);
            return;
        }
        throw new DataLengthException("HKDF cannot generate more than 255 blocks of HashLen size");
    }
    
    private KeyParameter extract(byte[] array, final byte[] array2) {
        if (array == null) {
            this.hMacHash.init(new KeyParameter(new byte[this.hashLen]));
        }
        else {
            this.hMacHash.init(new KeyParameter(array));
        }
        this.hMacHash.update(array2, 0, array2.length);
        array = new byte[this.hashLen];
        this.hMacHash.doFinal(array, 0);
        return new KeyParameter(array);
    }
    
    @Override
    public int generateBytes(final byte[] array, int b, final int b2) throws DataLengthException, IllegalArgumentException {
        final int generatedBytes = this.generatedBytes;
        final int hashLen = this.hashLen;
        if (generatedBytes + b2 <= hashLen * 255) {
            if (generatedBytes % hashLen == 0) {
                this.expandNext();
            }
            final int generatedBytes2 = this.generatedBytes;
            final int hashLen2 = this.hashLen;
            int n = Math.min(hashLen2 - generatedBytes2 % hashLen2, b2);
            System.arraycopy(this.currentT, generatedBytes2 % hashLen2, array, b, n);
            this.generatedBytes += n;
            final int n2 = b2 - n;
            int n3 = b;
            b = n2;
            while (true) {
                n3 += n;
                if (b <= 0) {
                    break;
                }
                this.expandNext();
                n = Math.min(this.hashLen, b);
                System.arraycopy(this.currentT, 0, array, n3, n);
                this.generatedBytes += n;
                b -= n;
            }
            return b2;
        }
        throw new DataLengthException("HKDF may only be used for 255 * HashLen bytes of output");
    }
    
    public Digest getDigest() {
        return this.hMacHash.getUnderlyingDigest();
    }
    
    @Override
    public void init(final DerivationParameters derivationParameters) {
        if (derivationParameters instanceof HKDFParameters) {
            final HKDFParameters hkdfParameters = (HKDFParameters)derivationParameters;
            HMac hMac;
            KeyParameter extract;
            if (hkdfParameters.skipExtract()) {
                hMac = this.hMacHash;
                extract = new KeyParameter(hkdfParameters.getIKM());
            }
            else {
                hMac = this.hMacHash;
                extract = this.extract(hkdfParameters.getSalt(), hkdfParameters.getIKM());
            }
            hMac.init(extract);
            this.info = hkdfParameters.getInfo();
            this.generatedBytes = 0;
            this.currentT = new byte[this.hashLen];
            return;
        }
        throw new IllegalArgumentException("HKDF parameters required for HKDFBytesGenerator");
    }
}
