// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.generators;

import org.bouncycastle.crypto.params.GOST3410ValidationParameters;
import org.bouncycastle.crypto.params.GOST3410Parameters;
import org.bouncycastle.util.BigIntegers;
import java.security.SecureRandom;
import java.math.BigInteger;

public class GOST3410ParametersGenerator
{
    private static final BigInteger ONE;
    private static final BigInteger TWO;
    private SecureRandom init_random;
    private int size;
    private int typeproc;
    
    static {
        ONE = BigInteger.valueOf(1L);
        TWO = BigInteger.valueOf(2L);
    }
    
    private int procedure_A(int i, int j, final BigInteger[] array, int n) {
        while (i < 0 || i > 65536) {
            i = this.init_random.nextInt() / 32768;
        }
        while (j < 0 || j > 65536 || j / 2 == 0) {
            j = this.init_random.nextInt() / 32768 + 1;
        }
        final BigInteger val = new BigInteger(Integer.toString(j));
        final BigInteger val2 = new BigInteger("19381");
        final BigInteger bigInteger = new BigInteger(Integer.toString(i));
        int[] array2;
        int[] array3;
        for (array2 = new int[] { n }, j = (i = 0); array2[j] >= 17; j = i) {
            array3 = new int[array2.length + 1];
            System.arraycopy(array2, 0, array3, 0, array2.length);
            array2 = new int[array3.length];
            System.arraycopy(array3, 0, array2, 0, array3.length);
            i = j + 1;
            array2[i] = array2[j] / 2;
        }
        final BigInteger[] array4 = new BigInteger[i + 1];
        array4[i] = new BigInteger("8003", 16);
        n = i - 1;
        BigInteger[] array5 = { bigInteger };
        int n2;
        BigInteger[] array6;
        int k;
        int n3;
        BigInteger add;
        int l;
        BigInteger pow;
        int n4;
        BigInteger bigInteger3;
        BigInteger bigInteger2;
        int n5;
        BigInteger bigInteger4;
        long val3;
        BigInteger bigInteger5;
        for (j = 0; j < i; ++j) {
            n2 = array2[n] / 16;
        Block_12:
            while (true) {
                array6 = new BigInteger[array5.length];
                System.arraycopy(array5, 0, array6, 0, array5.length);
                array5 = new BigInteger[n2 + 1];
                System.arraycopy(array6, 0, array5, 0, array6.length);
                for (k = 0; k < n2; k = n3) {
                    n3 = k + 1;
                    array5[n3] = array5[k].multiply(val2).add(val).mod(GOST3410ParametersGenerator.TWO.pow(16));
                }
                add = new BigInteger("0");
                for (l = 0; l < n2; ++l) {
                    add = add.add(array5[l].multiply(GOST3410ParametersGenerator.TWO.pow(l * 16)));
                }
                array5[0] = array5[n2];
                pow = GOST3410ParametersGenerator.TWO.pow(array2[n] - 1);
                n4 = n + 1;
                bigInteger2 = (bigInteger3 = pow.divide(array4[n4]).add(GOST3410ParametersGenerator.TWO.pow(array2[n] - 1).multiply(add).divide(array4[n4].multiply(GOST3410ParametersGenerator.TWO.pow(n2 * 16)))));
                if (bigInteger2.mod(GOST3410ParametersGenerator.TWO).compareTo(GOST3410ParametersGenerator.ONE) == 0) {
                    bigInteger3 = bigInteger2.add(GOST3410ParametersGenerator.ONE);
                }
                n5 = 0;
                while (true) {
                    bigInteger4 = array4[n4];
                    val3 = n5;
                    array4[n] = bigInteger4.multiply(bigInteger3.add(BigInteger.valueOf(val3))).add(GOST3410ParametersGenerator.ONE);
                    if (array4[n].compareTo(GOST3410ParametersGenerator.TWO.pow(array2[n])) == 1) {
                        break;
                    }
                    if (GOST3410ParametersGenerator.TWO.modPow(array4[n4].multiply(bigInteger3.add(BigInteger.valueOf(val3))), array4[n]).compareTo(GOST3410ParametersGenerator.ONE) == 0 && GOST3410ParametersGenerator.TWO.modPow(bigInteger3.add(BigInteger.valueOf(val3)), array4[n]).compareTo(GOST3410ParametersGenerator.ONE) != 0) {
                        break Block_12;
                    }
                    n5 += 2;
                }
            }
            --n;
            if (n < 0) {
                array[0] = array4[0];
                array[1] = array4[1];
                bigInteger5 = array5[0];
                return bigInteger5.intValue();
            }
        }
        bigInteger5 = array5[0];
        return bigInteger5.intValue();
    }
    
    private long procedure_Aa(long n, long i, final BigInteger[] array, int n2) {
        while (n < 0L || n > 4294967296L) {
            n = this.init_random.nextInt() * 2;
        }
        while (i < 0L || i > 4294967296L || i / 2L == 0L) {
            i = this.init_random.nextInt() * 2 + 1;
        }
        final BigInteger val = new BigInteger(Long.toString(i));
        final BigInteger val2 = new BigInteger("97781173");
        final BigInteger bigInteger = new BigInteger(Long.toString(n));
        int[] array2 = { n2 };
        for (int n3 = n2 = 0; array2[n3] >= 33; n3 = n2) {
            final int[] array3 = new int[array2.length + 1];
            System.arraycopy(array2, 0, array3, 0, array2.length);
            array2 = new int[array3.length];
            System.arraycopy(array3, 0, array2, 0, array3.length);
            n2 = n3 + 1;
            array2[n2] = array2[n3] / 2;
        }
        final BigInteger[] array4 = new BigInteger[n2 + 1];
        array4[n2] = new BigInteger("8000000B", 16);
        int n4 = n2 - 1;
        BigInteger[] array5 = { bigInteger };
        for (int j = 0; j < n2; ++j) {
            final int n5 = array2[n4] / 32;
        Block_12:
            while (true) {
                final BigInteger[] array6 = new BigInteger[array5.length];
                System.arraycopy(array5, 0, array6, 0, array5.length);
                array5 = new BigInteger[n5 + 1];
                System.arraycopy(array6, 0, array5, 0, array6.length);
                int n6;
                for (int k = 0; k < n5; k = n6) {
                    n6 = k + 1;
                    array5[n6] = array5[k].multiply(val2).add(val).mod(GOST3410ParametersGenerator.TWO.pow(32));
                }
                BigInteger add = new BigInteger("0");
                for (int l = 0; l < n5; ++l) {
                    add = add.add(array5[l].multiply(GOST3410ParametersGenerator.TWO.pow(l * 32)));
                }
                array5[0] = array5[n5];
                final BigInteger pow = GOST3410ParametersGenerator.TWO.pow(array2[n4] - 1);
                final int n7 = n4 + 1;
                BigInteger bigInteger3;
                final BigInteger bigInteger2 = bigInteger3 = pow.divide(array4[n7]).add(GOST3410ParametersGenerator.TWO.pow(array2[n4] - 1).multiply(add).divide(array4[n7].multiply(GOST3410ParametersGenerator.TWO.pow(n5 * 32))));
                if (bigInteger2.mod(GOST3410ParametersGenerator.TWO).compareTo(GOST3410ParametersGenerator.ONE) == 0) {
                    bigInteger3 = bigInteger2.add(GOST3410ParametersGenerator.ONE);
                }
                int n8 = 0;
                while (true) {
                    final BigInteger bigInteger4 = array4[n7];
                    n = n8;
                    array4[n4] = bigInteger4.multiply(bigInteger3.add(BigInteger.valueOf(n))).add(GOST3410ParametersGenerator.ONE);
                    if (array4[n4].compareTo(GOST3410ParametersGenerator.TWO.pow(array2[n4])) == 1) {
                        break;
                    }
                    if (GOST3410ParametersGenerator.TWO.modPow(array4[n7].multiply(bigInteger3.add(BigInteger.valueOf(n))), array4[n4]).compareTo(GOST3410ParametersGenerator.ONE) == 0 && GOST3410ParametersGenerator.TWO.modPow(bigInteger3.add(BigInteger.valueOf(n)), array4[n4]).compareTo(GOST3410ParametersGenerator.ONE) != 0) {
                        break Block_12;
                    }
                    n8 += 2;
                }
            }
            --n4;
            if (n4 < 0) {
                array[0] = array4[0];
                array[1] = array4[1];
                final BigInteger bigInteger5 = array5[0];
                return bigInteger5.longValue();
            }
        }
        final BigInteger bigInteger5 = array5[0];
        return bigInteger5.longValue();
    }
    
    private void procedure_B(int i, int j, final BigInteger[] array) {
        while (i < 0 || i > 65536) {
            i = this.init_random.nextInt() / 32768;
        }
        while (j < 0 || j > 65536 || j / 2 == 0) {
            j = this.init_random.nextInt() / 32768 + 1;
        }
        final BigInteger[] array2 = new BigInteger[2];
        final BigInteger val = new BigInteger(Integer.toString(j));
        final BigInteger val2 = new BigInteger("19381");
        i = this.procedure_A(i, j, array2, 256);
        final BigInteger bigInteger = array2[0];
        i = this.procedure_A(i, j, array2, 512);
        final BigInteger bigInteger2 = array2[0];
        final BigInteger[] array3 = new BigInteger[65];
        array3[0] = new BigInteger(Integer.toString(i));
        BigInteger add2 = null;
    Block_10:
        while (true) {
            for (i = 0; i < 64; i = j) {
                j = i + 1;
                array3[j] = array3[i].multiply(val2).add(val).mod(GOST3410ParametersGenerator.TWO.pow(16));
            }
            BigInteger add = new BigInteger("0");
            for (i = 0; i < 64; ++i) {
                add = add.add(array3[i].multiply(GOST3410ParametersGenerator.TWO.pow(i * 16)));
            }
            array3[0] = array3[64];
            BigInteger bigInteger4;
            final BigInteger bigInteger3 = bigInteger4 = GOST3410ParametersGenerator.TWO.pow(1023).divide(bigInteger.multiply(bigInteger2)).add(GOST3410ParametersGenerator.TWO.pow(1023).multiply(add).divide(bigInteger.multiply(bigInteger2).multiply(GOST3410ParametersGenerator.TWO.pow(1024))));
            if (bigInteger3.mod(GOST3410ParametersGenerator.TWO).compareTo(GOST3410ParametersGenerator.ONE) == 0) {
                bigInteger4 = bigInteger3.add(GOST3410ParametersGenerator.ONE);
            }
            i = 0;
            while (true) {
                final BigInteger multiply = bigInteger.multiply(bigInteger2);
                final long val3 = i;
                add2 = multiply.multiply(bigInteger4.add(BigInteger.valueOf(val3))).add(GOST3410ParametersGenerator.ONE);
                if (add2.compareTo(GOST3410ParametersGenerator.TWO.pow(1024)) == 1) {
                    break;
                }
                if (GOST3410ParametersGenerator.TWO.modPow(bigInteger.multiply(bigInteger2).multiply(bigInteger4.add(BigInteger.valueOf(val3))), add2).compareTo(GOST3410ParametersGenerator.ONE) == 0 && GOST3410ParametersGenerator.TWO.modPow(bigInteger.multiply(bigInteger4.add(BigInteger.valueOf(val3))), add2).compareTo(GOST3410ParametersGenerator.ONE) != 0) {
                    break Block_10;
                }
                i += 2;
            }
        }
        array[0] = add2;
        array[1] = bigInteger;
    }
    
    private void procedure_Bb(long n, long i, final BigInteger[] array) {
        while (n < 0L || n > 4294967296L) {
            n = this.init_random.nextInt() * 2;
        }
        while (i < 0L || i > 4294967296L || i / 2L == 0L) {
            i = this.init_random.nextInt() * 2 + 1;
        }
        final BigInteger[] array2 = new BigInteger[2];
        final BigInteger val = new BigInteger(Long.toString(i));
        final BigInteger val2 = new BigInteger("97781173");
        n = this.procedure_Aa(n, i, array2, 256);
        final BigInteger bigInteger = array2[0];
        n = this.procedure_Aa(n, i, array2, 512);
        final BigInteger bigInteger2 = array2[0];
        final BigInteger[] array3 = new BigInteger[33];
        array3[0] = new BigInteger(Long.toString(n));
        BigInteger add2 = null;
    Block_10:
        while (true) {
            int n2;
            for (int j = 0; j < 32; j = n2) {
                n2 = j + 1;
                array3[n2] = array3[j].multiply(val2).add(val).mod(GOST3410ParametersGenerator.TWO.pow(32));
            }
            BigInteger add = new BigInteger("0");
            for (int k = 0; k < 32; ++k) {
                add = add.add(array3[k].multiply(GOST3410ParametersGenerator.TWO.pow(k * 32)));
            }
            array3[0] = array3[32];
            BigInteger bigInteger4;
            final BigInteger bigInteger3 = bigInteger4 = GOST3410ParametersGenerator.TWO.pow(1023).divide(bigInteger.multiply(bigInteger2)).add(GOST3410ParametersGenerator.TWO.pow(1023).multiply(add).divide(bigInteger.multiply(bigInteger2).multiply(GOST3410ParametersGenerator.TWO.pow(1024))));
            if (bigInteger3.mod(GOST3410ParametersGenerator.TWO).compareTo(GOST3410ParametersGenerator.ONE) == 0) {
                bigInteger4 = bigInteger3.add(GOST3410ParametersGenerator.ONE);
            }
            int n3 = 0;
            while (true) {
                final BigInteger multiply = bigInteger.multiply(bigInteger2);
                n = n3;
                add2 = multiply.multiply(bigInteger4.add(BigInteger.valueOf(n))).add(GOST3410ParametersGenerator.ONE);
                if (add2.compareTo(GOST3410ParametersGenerator.TWO.pow(1024)) == 1) {
                    break;
                }
                if (GOST3410ParametersGenerator.TWO.modPow(bigInteger.multiply(bigInteger2).multiply(bigInteger4.add(BigInteger.valueOf(n))), add2).compareTo(GOST3410ParametersGenerator.ONE) == 0 && GOST3410ParametersGenerator.TWO.modPow(bigInteger.multiply(bigInteger4.add(BigInteger.valueOf(n))), add2).compareTo(GOST3410ParametersGenerator.ONE) != 0) {
                    break Block_10;
                }
                n3 += 2;
            }
        }
        array[0] = add2;
        array[1] = bigInteger;
    }
    
    private BigInteger procedure_C(final BigInteger m, BigInteger divide) {
        final BigInteger subtract = m.subtract(GOST3410ParametersGenerator.ONE);
        divide = subtract.divide(divide);
        final int bitLength = m.bitLength();
        BigInteger modPow;
        while (true) {
            final BigInteger randomBigInteger = BigIntegers.createRandomBigInteger(bitLength, this.init_random);
            if (randomBigInteger.compareTo(GOST3410ParametersGenerator.ONE) > 0 && randomBigInteger.compareTo(subtract) < 0) {
                modPow = randomBigInteger.modPow(divide, m);
                if (modPow.compareTo(GOST3410ParametersGenerator.ONE) != 0) {
                    break;
                }
                continue;
            }
        }
        return modPow;
    }
    
    public GOST3410Parameters generateParameters() {
        final BigInteger[] array = new BigInteger[2];
        if (this.typeproc == 1) {
            final int nextInt = this.init_random.nextInt();
            final int nextInt2 = this.init_random.nextInt();
            final int size = this.size;
            if (size != 512) {
                if (size != 1024) {
                    throw new IllegalArgumentException("Ooops! key size 512 or 1024 bit.");
                }
                this.procedure_B(nextInt, nextInt2, array);
            }
            else {
                this.procedure_A(nextInt, nextInt2, array, 512);
            }
            final BigInteger bigInteger = array[0];
            final BigInteger bigInteger2 = array[1];
            return new GOST3410Parameters(bigInteger, bigInteger2, this.procedure_C(bigInteger, bigInteger2), new GOST3410ValidationParameters(nextInt, nextInt2));
        }
        final long nextLong = this.init_random.nextLong();
        final long nextLong2 = this.init_random.nextLong();
        final int size2 = this.size;
        if (size2 != 512) {
            if (size2 != 1024) {
                throw new IllegalStateException("Ooops! key size 512 or 1024 bit.");
            }
            this.procedure_Bb(nextLong, nextLong2, array);
        }
        else {
            this.procedure_Aa(nextLong, nextLong2, array, 512);
        }
        final BigInteger bigInteger3 = array[0];
        final BigInteger bigInteger4 = array[1];
        return new GOST3410Parameters(bigInteger3, bigInteger4, this.procedure_C(bigInteger3, bigInteger4), new GOST3410ValidationParameters(nextLong, nextLong2));
    }
    
    public void init(final int size, final int typeproc, final SecureRandom init_random) {
        this.size = size;
        this.typeproc = typeproc;
        this.init_random = init_random;
    }
}
