// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.generators;

import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.util.Pack;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.engines.Salsa20Engine;

public class SCrypt
{
    private SCrypt() {
    }
    
    private static void BlockMix(final int[] array, final int[] array2, final int[] array3, final int[] array4, int i) {
        System.arraycopy(array, array.length - 16, array2, 0, 16);
        final int length = array.length;
        i *= 2;
        int n2;
        int n = n2 = 0;
        while (i > 0) {
            Xor(array2, array, n, array3);
            Salsa20Engine.salsaCore(8, array3, array2);
            System.arraycopy(array2, 0, array4, n2, 16);
            n2 = (length >>> 1) + n - n2;
            n += 16;
            --i;
        }
    }
    
    private static void Clear(final byte[] array) {
        if (array != null) {
            Arrays.fill(array, (byte)0);
        }
    }
    
    private static void Clear(final int[] array) {
        if (array != null) {
            Arrays.fill(array, 0);
        }
    }
    
    private static void ClearAll(final int[][] array) {
        for (int i = 0; i < array.length; ++i) {
            Clear(array[i]);
        }
    }
    
    private static byte[] MFcrypt(byte[] singleIterationPBKDF2, byte[] array, final int n, final int n2, int i, final int n3) {
        final int n4 = n2 * 128;
        final byte[] singleIterationPBKDF3 = SingleIterationPBKDF2(singleIterationPBKDF2, array, i * n4);
        final byte[] array2 = array = null;
        try {
            final int n5 = singleIterationPBKDF3.length >>> 2;
            array = array2;
            final int[] array3 = (int[])(array = (byte[])new int[n5]);
            Pack.littleEndianToInt(singleIterationPBKDF3, 0, array3);
            for (i = 0; i < n5; i += n4 >>> 2) {
                array = (byte[])array3;
                SMix(array3, i, n, n2);
            }
            array = (byte[])array3;
            Pack.intToLittleEndian(array3, singleIterationPBKDF3, 0);
            array = (byte[])array3;
            singleIterationPBKDF2 = SingleIterationPBKDF2(singleIterationPBKDF2, singleIterationPBKDF3, n3);
            return singleIterationPBKDF2;
        }
        finally {
            Clear(singleIterationPBKDF3);
            Clear((int[])array);
        }
    }
    
    private static void SMix(final int[] array, final int n, final int n2, final int n3) {
        while (true) {
            final int n4 = n3 * 32;
            final int[] array2 = new int[16];
            final int[] array3 = new int[16];
            final int[] array4 = new int[n4];
            final int[] array5 = new int[n4];
            final int[] array6 = new int[n2 * n4];
            while (true) {
                Label_0272: {
                    try {
                        System.arraycopy(array, n, array5, 0, n4);
                        int i;
                        for (int n5 = i = 0; i < n2; i += 2) {
                            System.arraycopy(array5, 0, array6, n5, n4);
                            final int n6 = n5 + n4;
                            BlockMix(array5, array2, array3, array4, n3);
                            System.arraycopy(array4, 0, array6, n6, n4);
                            n5 = n6 + n4;
                            BlockMix(array4, array2, array3, array5, n3);
                        }
                        break Label_0272;
                        Label_0194: {
                            final Throwable t;
                            System.arraycopy(array5, 0, t, n, n4);
                        }
                        return;
                        // iftrue(Label_0194:, n7 >= n2)
                        System.arraycopy(array6, (array5[n4 - 16] & n2 - 1) * n4, array4, 0, n4);
                        Xor(array4, array5, 0, array4);
                        BlockMix(array4, array2, array3, array5, n3);
                        final int n7 = n7 + 1;
                        continue;
                    }
                    finally {
                        Clear(array6);
                        ClearAll(new int[][] { array5, array2, array3, array4 });
                    }
                }
                final int n7 = 0;
                continue;
            }
        }
    }
    
    private static byte[] SingleIterationPBKDF2(final byte[] array, final byte[] array2, final int n) {
        final PKCS5S2ParametersGenerator pkcs5S2ParametersGenerator = new PKCS5S2ParametersGenerator(new SHA256Digest());
        pkcs5S2ParametersGenerator.init(array, array2, 1);
        return ((KeyParameter)pkcs5S2ParametersGenerator.generateDerivedMacParameters(n * 8)).getKey();
    }
    
    private static void Xor(final int[] array, final int[] array2, final int n, final int[] array3) {
        for (int i = array3.length - 1; i >= 0; --i) {
            array3[i] = (array[i] ^ array2[n + i]);
        }
    }
    
    public static byte[] generate(final byte[] array, final byte[] array2, final int n, final int i, final int n2, final int n3) {
        if (array == null) {
            throw new IllegalArgumentException("Passphrase P must be provided.");
        }
        if (array2 == null) {
            throw new IllegalArgumentException("Salt S must be provided.");
        }
        if (n <= 1 || !isPowerOf2(n)) {
            throw new IllegalArgumentException("Cost parameter N must be > 1 and a power of 2");
        }
        if (i == 1 && n >= 65536) {
            throw new IllegalArgumentException("Cost parameter N must be > 1 and < 65536.");
        }
        if (i < 1) {
            throw new IllegalArgumentException("Block size r must be >= 1.");
        }
        final int j = Integer.MAX_VALUE / (i * 128 * 8);
        if (n2 < 1 || n2 > j) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Parallelisation parameter p must be >= 1 and <= ");
            sb.append(j);
            sb.append(" (based on block size r of ");
            sb.append(i);
            sb.append(")");
            throw new IllegalArgumentException(sb.toString());
        }
        if (n3 >= 1) {
            return MFcrypt(array, array2, n, i, n2, n3);
        }
        throw new IllegalArgumentException("Generated key length dkLen must be >= 1.");
    }
    
    private static boolean isPowerOf2(final int n) {
        return (n & n - 1) == 0x0;
    }
}
