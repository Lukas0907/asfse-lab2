// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.generators;

import org.bouncycastle.crypto.KeyGenerationParameters;
import java.math.BigInteger;
import java.security.SecureRandom;
import org.bouncycastle.crypto.params.GOST3410Parameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.GOST3410PrivateKeyParameters;
import org.bouncycastle.crypto.params.GOST3410PublicKeyParameters;
import org.bouncycastle.math.ec.WNafUtil;
import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.params.GOST3410KeyGenerationParameters;
import org.bouncycastle.crypto.AsymmetricCipherKeyPairGenerator;

public class GOST3410KeyPairGenerator implements AsymmetricCipherKeyPairGenerator
{
    private GOST3410KeyGenerationParameters param;
    
    @Override
    public AsymmetricCipherKeyPair generateKeyPair() {
        final GOST3410Parameters parameters = this.param.getParameters();
        final SecureRandom random = this.param.getRandom();
        final BigInteger q = parameters.getQ();
        final BigInteger p = parameters.getP();
        final BigInteger a = parameters.getA();
        BigInteger randomBigInteger;
        while (true) {
            randomBigInteger = BigIntegers.createRandomBigInteger(256, random);
            if (randomBigInteger.signum() >= 1) {
                if (randomBigInteger.compareTo(q) >= 0) {
                    continue;
                }
                if (WNafUtil.getNafWeight(randomBigInteger) < 64) {
                    continue;
                }
                break;
            }
        }
        return new AsymmetricCipherKeyPair(new GOST3410PublicKeyParameters(a.modPow(randomBigInteger, p), parameters), new GOST3410PrivateKeyParameters(randomBigInteger, parameters));
    }
    
    @Override
    public void init(final KeyGenerationParameters keyGenerationParameters) {
        this.param = (GOST3410KeyGenerationParameters)keyGenerationParameters;
    }
}
