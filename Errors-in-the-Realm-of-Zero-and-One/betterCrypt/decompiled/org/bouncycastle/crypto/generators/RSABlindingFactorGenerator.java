// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.generators;

import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.util.BigIntegers;
import java.security.SecureRandom;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import java.math.BigInteger;

public class RSABlindingFactorGenerator
{
    private static BigInteger ONE;
    private static BigInteger ZERO;
    private RSAKeyParameters key;
    private SecureRandom random;
    
    static {
        RSABlindingFactorGenerator.ZERO = BigInteger.valueOf(0L);
        RSABlindingFactorGenerator.ONE = BigInteger.valueOf(1L);
    }
    
    public BigInteger generateBlindingFactor() {
        final RSAKeyParameters key = this.key;
        if (key != null) {
            final BigInteger modulus = key.getModulus();
            final int bitLength = modulus.bitLength();
            BigInteger randomBigInteger;
            BigInteger gcd;
            do {
                randomBigInteger = BigIntegers.createRandomBigInteger(bitLength - 1, this.random);
                gcd = randomBigInteger.gcd(modulus);
            } while (randomBigInteger.equals(RSABlindingFactorGenerator.ZERO) || randomBigInteger.equals(RSABlindingFactorGenerator.ONE) || !gcd.equals(RSABlindingFactorGenerator.ONE));
            return randomBigInteger;
        }
        throw new IllegalStateException("generator not initialised");
    }
    
    public void init(final CipherParameters cipherParameters) {
        SecureRandom random;
        if (cipherParameters instanceof ParametersWithRandom) {
            final ParametersWithRandom parametersWithRandom = (ParametersWithRandom)cipherParameters;
            this.key = (RSAKeyParameters)parametersWithRandom.getParameters();
            random = parametersWithRandom.getRandom();
        }
        else {
            this.key = (RSAKeyParameters)cipherParameters;
            random = CryptoServicesRegistrar.getSecureRandom();
        }
        this.random = random;
        if (!(this.key instanceof RSAPrivateCrtKeyParameters)) {
            return;
        }
        throw new IllegalArgumentException("generator requires RSA public key");
    }
}
