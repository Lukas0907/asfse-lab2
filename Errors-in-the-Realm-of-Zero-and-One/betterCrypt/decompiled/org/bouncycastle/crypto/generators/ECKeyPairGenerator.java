// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.generators;

import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.crypto.params.ECKeyGenerationParameters;
import org.bouncycastle.crypto.KeyGenerationParameters;
import java.math.BigInteger;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.math.ec.WNafUtil;
import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.math.ec.FixedPointCombMultiplier;
import org.bouncycastle.math.ec.ECMultiplier;
import java.security.SecureRandom;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.math.ec.ECConstants;
import org.bouncycastle.crypto.AsymmetricCipherKeyPairGenerator;

public class ECKeyPairGenerator implements AsymmetricCipherKeyPairGenerator, ECConstants
{
    ECDomainParameters params;
    SecureRandom random;
    
    protected ECMultiplier createBasePointMultiplier() {
        return new FixedPointCombMultiplier();
    }
    
    @Override
    public AsymmetricCipherKeyPair generateKeyPair() {
        final BigInteger n = this.params.getN();
        final int bitLength = n.bitLength();
        BigInteger randomBigInteger;
        while (true) {
            randomBigInteger = BigIntegers.createRandomBigInteger(bitLength, this.random);
            if (randomBigInteger.compareTo(ECKeyPairGenerator.ONE) >= 0) {
                if (randomBigInteger.compareTo(n) >= 0) {
                    continue;
                }
                if (WNafUtil.getNafWeight(randomBigInteger) < bitLength >>> 2) {
                    continue;
                }
                break;
            }
        }
        return new AsymmetricCipherKeyPair(new ECPublicKeyParameters(this.createBasePointMultiplier().multiply(this.params.getG(), randomBigInteger), this.params), new ECPrivateKeyParameters(randomBigInteger, this.params));
    }
    
    @Override
    public void init(final KeyGenerationParameters keyGenerationParameters) {
        final ECKeyGenerationParameters ecKeyGenerationParameters = (ECKeyGenerationParameters)keyGenerationParameters;
        this.random = ecKeyGenerationParameters.getRandom();
        this.params = ecKeyGenerationParameters.getDomainParameters();
        if (this.random == null) {
            this.random = CryptoServicesRegistrar.getSecureRandom();
        }
    }
}
