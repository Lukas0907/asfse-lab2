// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.generators;

import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.PBEParametersGenerator;

public class PKCS5S1ParametersGenerator extends PBEParametersGenerator
{
    private Digest digest;
    
    public PKCS5S1ParametersGenerator(final Digest digest) {
        this.digest = digest;
    }
    
    private byte[] generateDerivedKey() {
        final byte[] array = new byte[this.digest.getDigestSize()];
        this.digest.update(this.password, 0, this.password.length);
        this.digest.update(this.salt, 0, this.salt.length);
        this.digest.doFinal(array, 0);
        for (int i = 1; i < this.iterationCount; ++i) {
            this.digest.update(array, 0, array.length);
            this.digest.doFinal(array, 0);
        }
        return array;
    }
    
    @Override
    public CipherParameters generateDerivedMacParameters(final int n) {
        return this.generateDerivedParameters(n);
    }
    
    @Override
    public CipherParameters generateDerivedParameters(int i) {
        i /= 8;
        if (i <= this.digest.getDigestSize()) {
            return new KeyParameter(this.generateDerivedKey(), 0, i);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Can't generate a derived key ");
        sb.append(i);
        sb.append(" bytes long.");
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public CipherParameters generateDerivedParameters(int n, int n2) {
        n /= 8;
        n2 /= 8;
        final int i = n + n2;
        if (i <= this.digest.getDigestSize()) {
            final byte[] generateDerivedKey = this.generateDerivedKey();
            return new ParametersWithIV(new KeyParameter(generateDerivedKey, 0, n), generateDerivedKey, n, n2);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Can't generate a derived key ");
        sb.append(i);
        sb.append(" bytes long.");
        throw new IllegalArgumentException(sb.toString());
    }
}
