// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.generators;

import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.math.ec.WNafUtil;
import java.security.SecureRandom;
import org.bouncycastle.crypto.params.DHParameters;
import java.math.BigInteger;

class DHKeyGeneratorHelper
{
    static final DHKeyGeneratorHelper INSTANCE;
    private static final BigInteger ONE;
    private static final BigInteger TWO;
    
    static {
        INSTANCE = new DHKeyGeneratorHelper();
        ONE = BigInteger.valueOf(1L);
        TWO = BigInteger.valueOf(2L);
    }
    
    private DHKeyGeneratorHelper() {
    }
    
    BigInteger calculatePrivate(final DHParameters dhParameters, final SecureRandom secureRandom) {
        final int l = dhParameters.getL();
        if (l != 0) {
            BigInteger setBit;
            do {
                setBit = BigIntegers.createRandomBigInteger(l, secureRandom).setBit(l - 1);
            } while (WNafUtil.getNafWeight(setBit) < l >>> 2);
            return setBit;
        }
        BigInteger bigInteger = DHKeyGeneratorHelper.TWO;
        final int m = dhParameters.getM();
        if (m != 0) {
            bigInteger = DHKeyGeneratorHelper.ONE.shiftLeft(m - 1);
        }
        BigInteger bigInteger2;
        if ((bigInteger2 = dhParameters.getQ()) == null) {
            bigInteger2 = dhParameters.getP();
        }
        final BigInteger subtract = bigInteger2.subtract(DHKeyGeneratorHelper.TWO);
        BigInteger randomInRange;
        do {
            randomInRange = BigIntegers.createRandomInRange(bigInteger, subtract, secureRandom);
        } while (WNafUtil.getNafWeight(randomInRange) < subtract.bitLength() >>> 2);
        return randomInRange;
    }
    
    BigInteger calculatePublic(final DHParameters dhParameters, final BigInteger exponent) {
        return dhParameters.getG().modPow(exponent, dhParameters.getP());
    }
}
