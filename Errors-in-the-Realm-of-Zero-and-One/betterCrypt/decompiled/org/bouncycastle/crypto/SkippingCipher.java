// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto;

public interface SkippingCipher
{
    long getPosition();
    
    long seekTo(final long p0);
    
    long skip(final long p0);
}
