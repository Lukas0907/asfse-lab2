// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.engines;

import org.bouncycastle.crypto.params.ISO18033KDFParameters;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.OutputLengthException;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.DigestDerivationFunction;
import org.bouncycastle.crypto.EphemeralKeyPair;
import org.bouncycastle.crypto.DerivationParameters;
import org.bouncycastle.crypto.params.KDFParameters;
import org.bouncycastle.util.BigIntegers;
import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.util.Pack;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.IESWithCipherParameters;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.IESParameters;
import org.bouncycastle.crypto.Mac;
import org.bouncycastle.crypto.KeyParser;
import org.bouncycastle.crypto.generators.EphemeralKeyPairGenerator;
import org.bouncycastle.crypto.DerivationFunction;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.BasicAgreement;

public class EthereumIESEngine
{
    private byte[] IV;
    byte[] V;
    BasicAgreement agree;
    BufferedBlockCipher cipher;
    byte[] commonMac;
    boolean forEncryption;
    DerivationFunction kdf;
    private EphemeralKeyPairGenerator keyPairGenerator;
    private KeyParser keyParser;
    Mac mac;
    byte[] macBuf;
    IESParameters param;
    CipherParameters privParam;
    CipherParameters pubParam;
    
    public EthereumIESEngine(final BasicAgreement agree, final DerivationFunction kdf, final Mac mac, final byte[] commonMac) {
        this.agree = agree;
        this.kdf = kdf;
        this.mac = mac;
        this.macBuf = new byte[mac.getMacSize()];
        this.commonMac = commonMac;
        this.cipher = null;
    }
    
    public EthereumIESEngine(final BasicAgreement agree, final DerivationFunction kdf, final Mac mac, final byte[] commonMac, final BufferedBlockCipher cipher) {
        this.agree = agree;
        this.kdf = kdf;
        this.mac = mac;
        this.macBuf = new byte[mac.getMacSize()];
        this.commonMac = commonMac;
        this.cipher = cipher;
    }
    
    private byte[] decryptBlock(final byte[] array, final int n, final int n2) throws InvalidCipherTextException {
        if (n2 < this.V.length + this.mac.getMacSize()) {
            throw new InvalidCipherTextException("length of input must be greater than the MAC and V combined");
        }
        byte[] array3;
        byte[] array5;
        int processBytes;
        if (this.cipher == null) {
            final byte[] array2 = new byte[n2 - this.V.length - this.mac.getMacSize()];
            array3 = new byte[this.param.getMacKeySize() / 8];
            final byte[] array4 = new byte[array2.length + array3.length];
            this.kdf.generateBytes(array4, 0, array4.length);
            if (this.V.length != 0) {
                System.arraycopy(array4, 0, array3, 0, array3.length);
                System.arraycopy(array4, array3.length, array2, 0, array2.length);
            }
            else {
                System.arraycopy(array4, 0, array2, 0, array2.length);
                System.arraycopy(array4, array2.length, array3, 0, array3.length);
            }
            array5 = new byte[array2.length];
            for (int i = 0; i != array2.length; ++i) {
                array5[i] = (byte)(array[this.V.length + n + i] ^ array2[i]);
            }
            processBytes = 0;
        }
        else {
            final byte[] array6 = new byte[((IESWithCipherParameters)this.param).getCipherKeySize() / 8];
            array3 = new byte[this.param.getMacKeySize() / 8];
            final byte[] array7 = new byte[array6.length + array3.length];
            this.kdf.generateBytes(array7, 0, array7.length);
            System.arraycopy(array7, 0, array6, 0, array6.length);
            System.arraycopy(array7, array6.length, array3, 0, array3.length);
            final KeyParameter keyParameter = new KeyParameter(array6);
            final byte[] iv = this.IV;
            CipherParameters cipherParameters = keyParameter;
            if (iv != null) {
                cipherParameters = new ParametersWithIV(keyParameter, iv);
            }
            this.cipher.init(false, cipherParameters);
            array5 = new byte[this.cipher.getOutputSize(n2 - this.V.length - this.mac.getMacSize())];
            final BufferedBlockCipher cipher = this.cipher;
            final byte[] v = this.V;
            processBytes = cipher.processBytes(array, v.length + n, n2 - v.length - this.mac.getMacSize(), array5, 0);
        }
        final byte[] encodingV = this.param.getEncodingV();
        byte[] lengthTag = null;
        if (this.V.length != 0) {
            lengthTag = this.getLengthTag(encodingV);
        }
        final int n3 = n + n2;
        final byte[] copyOfRange = Arrays.copyOfRange(array, n3 - this.mac.getMacSize(), n3);
        final byte[] array8 = new byte[copyOfRange.length];
        final SHA256Digest sha256Digest = new SHA256Digest();
        final byte[] array9 = new byte[sha256Digest.getDigestSize()];
        sha256Digest.reset();
        sha256Digest.update(array3, 0, array3.length);
        sha256Digest.doFinal(array9, 0);
        this.mac.init(new KeyParameter(array9));
        final Mac mac = this.mac;
        final byte[] iv2 = this.IV;
        mac.update(iv2, 0, iv2.length);
        final Mac mac2 = this.mac;
        final byte[] v2 = this.V;
        mac2.update(array, n + v2.length, n2 - v2.length - array8.length);
        if (encodingV != null) {
            this.mac.update(encodingV, 0, encodingV.length);
        }
        if (this.V.length != 0) {
            this.mac.update(lengthTag, 0, lengthTag.length);
        }
        final Mac mac3 = this.mac;
        final byte[] commonMac = this.commonMac;
        mac3.update(commonMac, 0, commonMac.length);
        this.mac.doFinal(array8, 0);
        if (!Arrays.constantTimeAreEqual(copyOfRange, array8)) {
            throw new InvalidCipherTextException("invalid MAC");
        }
        final BufferedBlockCipher cipher2 = this.cipher;
        if (cipher2 == null) {
            return array5;
        }
        return Arrays.copyOfRange(array5, 0, processBytes + cipher2.doFinal(array5, processBytes));
    }
    
    private byte[] encryptBlock(byte[] array, int processBytes, int n) throws InvalidCipherTextException {
        byte[] array3;
        if (this.cipher == null) {
            final byte[] array2 = new byte[n];
            array3 = new byte[this.param.getMacKeySize() / 8];
            final byte[] array4 = new byte[array2.length + array3.length];
            this.kdf.generateBytes(array4, 0, array4.length);
            if (this.V.length != 0) {
                System.arraycopy(array4, 0, array3, 0, array3.length);
                System.arraycopy(array4, array3.length, array2, 0, array2.length);
            }
            else {
                System.arraycopy(array4, 0, array2, 0, array2.length);
                System.arraycopy(array4, n, array3, 0, array3.length);
            }
            final byte[] array5 = new byte[n];
            for (int i = 0; i != n; ++i) {
                array5[i] = (byte)(array[processBytes + i] ^ array2[i]);
            }
            array = array5;
        }
        else {
            final byte[] array6 = new byte[((IESWithCipherParameters)this.param).getCipherKeySize() / 8];
            final byte[] array7 = new byte[this.param.getMacKeySize() / 8];
            final byte[] array8 = new byte[array6.length + array7.length];
            this.kdf.generateBytes(array8, 0, array8.length);
            System.arraycopy(array8, 0, array6, 0, array6.length);
            System.arraycopy(array8, array6.length, array7, 0, array7.length);
            BufferedBlockCipher bufferedBlockCipher;
            CipherParameters cipherParameters;
            if (this.IV != null) {
                bufferedBlockCipher = this.cipher;
                cipherParameters = new ParametersWithIV(new KeyParameter(array6), this.IV);
            }
            else {
                bufferedBlockCipher = this.cipher;
                cipherParameters = new KeyParameter(array6);
            }
            bufferedBlockCipher.init(true, cipherParameters);
            final byte[] array9 = new byte[this.cipher.getOutputSize(n)];
            processBytes = this.cipher.processBytes(array, processBytes, n, array9, 0);
            n = processBytes + this.cipher.doFinal(array9, processBytes);
            array3 = array7;
            array = array9;
        }
        final byte[] encodingV = this.param.getEncodingV();
        byte[] lengthTag = null;
        if (this.V.length != 0) {
            lengthTag = this.getLengthTag(encodingV);
        }
        final byte[] array10 = new byte[this.mac.getMacSize()];
        final SHA256Digest sha256Digest = new SHA256Digest();
        final byte[] array11 = new byte[sha256Digest.getDigestSize()];
        sha256Digest.reset();
        sha256Digest.update(array3, 0, array3.length);
        sha256Digest.doFinal(array11, 0);
        this.mac.init(new KeyParameter(array11));
        final Mac mac = this.mac;
        final byte[] iv = this.IV;
        mac.update(iv, 0, iv.length);
        this.mac.update(array, 0, array.length);
        if (encodingV != null) {
            this.mac.update(encodingV, 0, encodingV.length);
        }
        if (this.V.length != 0) {
            this.mac.update(lengthTag, 0, lengthTag.length);
        }
        final Mac mac2 = this.mac;
        final byte[] commonMac = this.commonMac;
        mac2.update(commonMac, 0, commonMac.length);
        this.mac.doFinal(array10, 0);
        final byte[] v = this.V;
        final byte[] array12 = new byte[v.length + n + array10.length];
        System.arraycopy(v, 0, array12, 0, v.length);
        System.arraycopy(array, 0, array12, this.V.length, n);
        System.arraycopy(array10, 0, array12, this.V.length + n, array10.length);
        return array12;
    }
    
    private void extractParams(CipherParameters parameters) {
        if (parameters instanceof ParametersWithIV) {
            final ParametersWithIV parametersWithIV = (ParametersWithIV)parameters;
            this.IV = parametersWithIV.getIV();
            parameters = parametersWithIV.getParameters();
        }
        else {
            this.IV = null;
        }
        this.param = (IESParameters)parameters;
    }
    
    public BufferedBlockCipher getCipher() {
        return this.cipher;
    }
    
    protected byte[] getLengthTag(final byte[] array) {
        final byte[] array2 = new byte[8];
        if (array != null) {
            Pack.longToBigEndian(array.length * 8L, array2, 0);
        }
        return array2;
    }
    
    public Mac getMac() {
        return this.mac;
    }
    
    public void init(final AsymmetricKeyParameter privParam, final CipherParameters cipherParameters, final KeyParser keyParser) {
        this.forEncryption = false;
        this.privParam = privParam;
        this.keyParser = keyParser;
        this.extractParams(cipherParameters);
    }
    
    public void init(final AsymmetricKeyParameter pubParam, final CipherParameters cipherParameters, final EphemeralKeyPairGenerator keyPairGenerator) {
        this.forEncryption = true;
        this.pubParam = pubParam;
        this.keyPairGenerator = keyPairGenerator;
        this.extractParams(cipherParameters);
    }
    
    public void init(final boolean forEncryption, final CipherParameters privParam, final CipherParameters pubParam, final CipherParameters cipherParameters) {
        this.forEncryption = forEncryption;
        this.privParam = privParam;
        this.pubParam = pubParam;
        this.V = new byte[0];
        this.extractParams(cipherParameters);
    }
    
    public byte[] processBlock(byte[] buf, final int offset, final int length) throws InvalidCipherTextException {
        if (this.forEncryption) {
            final EphemeralKeyPairGenerator keyPairGenerator = this.keyPairGenerator;
            if (keyPairGenerator != null) {
                final EphemeralKeyPair generate = keyPairGenerator.generate();
                this.privParam = generate.getKeyPair().getPrivate();
                this.V = generate.getEncodedPublicKey();
            }
        }
        else if (this.keyParser != null) {
            final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf, offset, length);
            try {
                this.pubParam = this.keyParser.readKey(byteArrayInputStream);
                this.V = Arrays.copyOfRange(buf, offset, length - byteArrayInputStream.available() + offset);
            }
            catch (IllegalArgumentException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("unable to recover ephemeral public key: ");
                sb.append(ex.getMessage());
                throw new InvalidCipherTextException(sb.toString(), ex);
            }
            catch (IOException ex2) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("unable to recover ephemeral public key: ");
                sb2.append(ex2.getMessage());
                throw new InvalidCipherTextException(sb2.toString(), ex2);
            }
        }
        this.agree.init(this.privParam);
        final byte[] unsignedByteArray = BigIntegers.asUnsignedByteArray(this.agree.getFieldSize(), this.agree.calculateAgreement(this.pubParam));
        final byte[] v = this.V;
        byte[] concatenate = unsignedByteArray;
        if (v.length != 0) {
            concatenate = Arrays.concatenate(v, unsignedByteArray);
            Arrays.fill(unsignedByteArray, (byte)0);
        }
        try {
            this.kdf.init(new KDFParameters(concatenate, this.param.getDerivationV()));
            if (this.forEncryption) {
                buf = this.encryptBlock(buf, offset, length);
            }
            else {
                buf = this.decryptBlock(buf, offset, length);
            }
            return buf;
        }
        finally {
            Arrays.fill(concatenate, (byte)0);
        }
    }
    
    public static class HandshakeKDFFunction implements DigestDerivationFunction
    {
        private int counterStart;
        private Digest digest;
        private byte[] iv;
        private byte[] shared;
        
        public HandshakeKDFFunction(final int counterStart, final Digest digest) {
            this.counterStart = counterStart;
            this.digest = digest;
        }
        
        @Override
        public int generateBytes(final byte[] array, int i, int n) throws DataLengthException, IllegalArgumentException {
            if (array.length - n < i) {
                throw new OutputLengthException("output buffer too small");
            }
            final long n2 = n;
            final int digestSize = this.digest.getDigestSize();
            if (n2 <= 8589934591L) {
                final long n3 = digestSize;
                final int n4 = (int)((n2 + n3 - 1L) / n3);
                final byte[] array2 = new byte[this.digest.getDigestSize()];
                final byte[] array3 = new byte[4];
                Pack.intToBigEndian(this.counterStart, array3, 0);
                final int n5 = this.counterStart & 0xFFFFFF00;
                final int n6 = i;
                i = 0;
                int n7 = n;
                n = n5;
                int n8 = n6;
                while (i < n4) {
                    this.digest.update(array3, 0, array3.length);
                    final Digest digest = this.digest;
                    final byte[] shared = this.shared;
                    digest.update(shared, 0, shared.length);
                    final byte[] iv = this.iv;
                    if (iv != null) {
                        this.digest.update(iv, 0, iv.length);
                    }
                    this.digest.doFinal(array2, 0);
                    int n11;
                    int n12;
                    if (n7 > digestSize) {
                        System.arraycopy(array2, 0, array, n8, digestSize);
                        final int n9 = n8 + digestSize;
                        final int n10 = n7 - digestSize;
                        n11 = n9;
                        n12 = n10;
                    }
                    else {
                        System.arraycopy(array2, 0, array, n8, n7);
                        n12 = n7;
                        n11 = n8;
                    }
                    final byte b = (byte)(array3[3] + 1);
                    array3[3] = b;
                    int n13 = n;
                    if (b == 0) {
                        n13 = n + 256;
                        Pack.intToBigEndian(n13, array3, 0);
                    }
                    ++i;
                    n8 = n11;
                    n = n13;
                    n7 = n12;
                }
                this.digest.reset();
                return (int)n2;
            }
            throw new IllegalArgumentException("output length too large");
        }
        
        @Override
        public Digest getDigest() {
            return this.digest;
        }
        
        @Override
        public void init(final DerivationParameters derivationParameters) {
            if (derivationParameters instanceof KDFParameters) {
                final KDFParameters kdfParameters = (KDFParameters)derivationParameters;
                this.shared = kdfParameters.getSharedSecret();
                this.iv = kdfParameters.getIV();
                return;
            }
            if (derivationParameters instanceof ISO18033KDFParameters) {
                this.shared = ((ISO18033KDFParameters)derivationParameters).getSeed();
                this.iv = null;
                return;
            }
            throw new IllegalArgumentException("KDF parameters required for generator");
        }
    }
}
