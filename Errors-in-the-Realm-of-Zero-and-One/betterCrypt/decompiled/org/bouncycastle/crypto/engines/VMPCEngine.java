// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.engines;

import org.bouncycastle.crypto.OutputLengthException;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.StreamCipher;

public class VMPCEngine implements StreamCipher
{
    protected byte[] P;
    protected byte n;
    protected byte s;
    protected byte[] workingIV;
    protected byte[] workingKey;
    
    public VMPCEngine() {
        this.n = 0;
        this.P = null;
        this.s = 0;
    }
    
    @Override
    public String getAlgorithmName() {
        return "VMPC";
    }
    
    @Override
    public void init(final boolean b, final CipherParameters cipherParameters) {
        if (!(cipherParameters instanceof ParametersWithIV)) {
            throw new IllegalArgumentException("VMPC init parameters must include an IV");
        }
        final ParametersWithIV parametersWithIV = (ParametersWithIV)cipherParameters;
        if (!(parametersWithIV.getParameters() instanceof KeyParameter)) {
            throw new IllegalArgumentException("VMPC init parameters must include a key");
        }
        final KeyParameter keyParameter = (KeyParameter)parametersWithIV.getParameters();
        this.workingIV = parametersWithIV.getIV();
        final byte[] workingIV = this.workingIV;
        if (workingIV != null && workingIV.length >= 1 && workingIV.length <= 768) {
            this.initKey(this.workingKey = keyParameter.getKey(), this.workingIV);
            return;
        }
        throw new IllegalArgumentException("VMPC requires 1 to 768 bytes of IV");
    }
    
    protected void initKey(byte[] p2, final byte[] array) {
        this.s = 0;
        this.P = new byte[256];
        for (int i = 0; i < 256; ++i) {
            this.P[i] = (byte)i;
        }
        for (int j = 0; j < 768; ++j) {
            final byte[] p3 = this.P;
            final byte s = this.s;
            final int n = j & 0xFF;
            this.s = p3[s + p3[n] + p2[j % p2.length] & 0xFF];
            final byte b = p3[n];
            final byte s2 = this.s;
            p3[n] = p3[s2 & 0xFF];
            p3[s2 & 0xFF] = b;
        }
        for (int k = 0; k < 768; ++k) {
            p2 = this.P;
            final byte s3 = this.s;
            final int n2 = k & 0xFF;
            this.s = p2[s3 + p2[n2] + array[k % array.length] & 0xFF];
            final byte b2 = p2[n2];
            final byte s4 = this.s;
            p2[n2] = p2[s4 & 0xFF];
            p2[s4 & 0xFF] = b2;
        }
        this.n = 0;
    }
    
    @Override
    public int processBytes(final byte[] array, final int n, final int n2, final byte[] array2, final int n3) {
        if (n + n2 > array.length) {
            throw new DataLengthException("input buffer too short");
        }
        if (n3 + n2 <= array2.length) {
            for (int i = 0; i < n2; ++i) {
                final byte[] p5 = this.P;
                final byte s = this.s;
                final byte n4 = this.n;
                this.s = p5[s + p5[n4 & 0xFF] & 0xFF];
                final byte s2 = this.s;
                final byte b = p5[p5[p5[s2 & 0xFF] & 0xFF] + 1 & 0xFF];
                final byte b2 = p5[n4 & 0xFF];
                p5[n4 & 0xFF] = p5[s2 & 0xFF];
                p5[s2 & 0xFF] = b2;
                this.n = (byte)(n4 + 1 & 0xFF);
                array2[i + n3] = (byte)(array[i + n] ^ b);
            }
            return n2;
        }
        throw new OutputLengthException("output buffer too short");
    }
    
    @Override
    public void reset() {
        this.initKey(this.workingKey, this.workingIV);
    }
    
    @Override
    public byte returnByte(final byte b) {
        final byte[] p = this.P;
        final byte s = this.s;
        final byte n = this.n;
        this.s = p[s + p[n & 0xFF] & 0xFF];
        final byte s2 = this.s;
        final byte b2 = p[p[p[s2 & 0xFF] & 0xFF] + 1 & 0xFF];
        final byte b3 = p[n & 0xFF];
        p[n & 0xFF] = p[s2 & 0xFF];
        p[s2 & 0xFF] = b3;
        this.n = (byte)(n + 1 & 0xFF);
        return (byte)(b ^ b2);
    }
}
