// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.engines;

import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import java.security.SecureRandom;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import java.math.BigInteger;
import org.bouncycastle.crypto.AsymmetricBlockCipher;

public class RSABlindedEngine implements AsymmetricBlockCipher
{
    private static final BigInteger ONE;
    private RSACoreEngine core;
    private RSAKeyParameters key;
    private SecureRandom random;
    
    static {
        ONE = BigInteger.valueOf(1L);
    }
    
    public RSABlindedEngine() {
        this.core = new RSACoreEngine();
    }
    
    @Override
    public int getInputBlockSize() {
        return this.core.getInputBlockSize();
    }
    
    @Override
    public int getOutputBlockSize() {
        return this.core.getOutputBlockSize();
    }
    
    @Override
    public void init(final boolean b, final CipherParameters cipherParameters) {
        this.core.init(b, cipherParameters);
        Label_0078: {
            SecureRandom random;
            if (cipherParameters instanceof ParametersWithRandom) {
                final ParametersWithRandom parametersWithRandom = (ParametersWithRandom)cipherParameters;
                this.key = (RSAKeyParameters)parametersWithRandom.getParameters();
                if (!(this.key instanceof RSAPrivateCrtKeyParameters)) {
                    break Label_0078;
                }
                random = parametersWithRandom.getRandom();
            }
            else {
                this.key = (RSAKeyParameters)cipherParameters;
                if (!(this.key instanceof RSAPrivateCrtKeyParameters)) {
                    break Label_0078;
                }
                random = CryptoServicesRegistrar.getSecureRandom();
            }
            this.random = random;
            return;
        }
        this.random = null;
    }
    
    @Override
    public byte[] processBlock(final byte[] array, final int n, final int n2) {
        if (this.key != null) {
            final BigInteger convertInput = this.core.convertInput(array, n, n2);
            final RSAKeyParameters key = this.key;
            if (key instanceof RSAPrivateCrtKeyParameters) {
                final RSAPrivateCrtKeyParameters rsaPrivateCrtKeyParameters = (RSAPrivateCrtKeyParameters)key;
                final BigInteger publicExponent = rsaPrivateCrtKeyParameters.getPublicExponent();
                if (publicExponent != null) {
                    final BigInteger modulus = rsaPrivateCrtKeyParameters.getModulus();
                    final BigInteger one = RSABlindedEngine.ONE;
                    final BigInteger randomInRange = BigIntegers.createRandomInRange(one, modulus.subtract(one), this.random);
                    final BigInteger bigInteger = this.core.processBlock(randomInRange.modPow(publicExponent, modulus).multiply(convertInput).mod(modulus)).multiply(randomInRange.modInverse(modulus)).mod(modulus);
                    if (convertInput.equals(bigInteger.modPow(publicExponent, modulus))) {
                        return this.core.convertOutput(bigInteger);
                    }
                    throw new IllegalStateException("RSA engine faulty decryption/signing detected");
                }
            }
            final BigInteger bigInteger = this.core.processBlock(convertInput);
            return this.core.convertOutput(bigInteger);
        }
        throw new IllegalStateException("RSA engine not initialised");
    }
}
