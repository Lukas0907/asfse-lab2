// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.engines;

import org.bouncycastle.crypto.OutputLengthException;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.TweakableBlockCipherParameters;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.BlockCipher;

public class ThreefishEngine implements BlockCipher
{
    public static final int BLOCKSIZE_1024 = 1024;
    public static final int BLOCKSIZE_256 = 256;
    public static final int BLOCKSIZE_512 = 512;
    private static final long C_240 = 2004413935125273122L;
    private static final int MAX_ROUNDS = 80;
    private static int[] MOD17;
    private static int[] MOD3;
    private static int[] MOD5;
    private static int[] MOD9;
    private static final int ROUNDS_1024 = 80;
    private static final int ROUNDS_256 = 72;
    private static final int ROUNDS_512 = 72;
    private static final int TWEAK_SIZE_BYTES = 16;
    private static final int TWEAK_SIZE_WORDS = 2;
    private int blocksizeBytes;
    private int blocksizeWords;
    private ThreefishCipher cipher;
    private long[] currentBlock;
    private boolean forEncryption;
    private long[] kw;
    private long[] t;
    
    static {
        ThreefishEngine.MOD9 = new int[80];
        final int[] mod9 = ThreefishEngine.MOD9;
        ThreefishEngine.MOD17 = new int[mod9.length];
        ThreefishEngine.MOD5 = new int[mod9.length];
        ThreefishEngine.MOD3 = new int[mod9.length];
        int n = 0;
        while (true) {
            final int[] mod10 = ThreefishEngine.MOD9;
            if (n >= mod10.length) {
                break;
            }
            ThreefishEngine.MOD17[n] = n % 17;
            mod10[n] = n % 9;
            ThreefishEngine.MOD5[n] = n % 5;
            ThreefishEngine.MOD3[n] = n % 3;
            ++n;
        }
    }
    
    public ThreefishEngine(final int n) {
        this.t = new long[5];
        this.blocksizeBytes = n / 8;
        this.blocksizeWords = this.blocksizeBytes / 8;
        final int blocksizeWords = this.blocksizeWords;
        this.currentBlock = new long[blocksizeWords];
        this.kw = new long[blocksizeWords * 2 + 1];
        ThreefishCipher cipher;
        if (n != 256) {
            if (n != 512) {
                if (n != 1024) {
                    throw new IllegalArgumentException("Invalid blocksize - Threefish is defined with block size of 256, 512, or 1024 bits");
                }
                cipher = new Threefish1024Cipher(this.kw, this.t);
            }
            else {
                cipher = new Threefish512Cipher(this.kw, this.t);
            }
        }
        else {
            cipher = new Threefish256Cipher(this.kw, this.t);
        }
        this.cipher = cipher;
    }
    
    public static long bytesToWord(final byte[] array, int n) {
        if (n + 8 <= array.length) {
            final int n2 = n + 1;
            final long n3 = array[n];
            n = n2 + 1;
            final long n4 = array[n2];
            final int n5 = n + 1;
            final long n6 = array[n];
            n = n5 + 1;
            final long n7 = array[n5];
            final int n8 = n + 1;
            final long n9 = array[n];
            n = n8 + 1;
            return ((long)array[n + 1] & 0xFFL) << 56 | ((n3 & 0xFFL) | (n4 & 0xFFL) << 8 | (n6 & 0xFFL) << 16 | (n7 & 0xFFL) << 24 | (n9 & 0xFFL) << 32 | ((long)array[n8] & 0xFFL) << 40 | ((long)array[n] & 0xFFL) << 48);
        }
        throw new IllegalArgumentException();
    }
    
    static long rotlXor(final long n, final int n2, final long n3) {
        return (n >>> -n2 | n << n2) ^ n3;
    }
    
    private void setKey(long[] kw) {
        if (kw.length == this.blocksizeWords) {
            long n = 2004413935125273122L;
            int n2 = 0;
            int blocksizeWords;
            while (true) {
                blocksizeWords = this.blocksizeWords;
                if (n2 >= blocksizeWords) {
                    break;
                }
                final long[] kw2 = this.kw;
                kw2[n2] = kw[n2];
                n ^= kw2[n2];
                ++n2;
            }
            kw = this.kw;
            kw[blocksizeWords] = n;
            System.arraycopy(kw, 0, kw, blocksizeWords + 1, blocksizeWords);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Threefish key must be same size as block (");
        sb.append(this.blocksizeWords);
        sb.append(" words)");
        throw new IllegalArgumentException(sb.toString());
    }
    
    private void setTweak(final long[] array) {
        if (array.length == 2) {
            final long[] t = this.t;
            t[0] = array[0];
            t[1] = array[1];
            t[2] = (t[0] ^ t[1]);
            t[3] = t[0];
            t[4] = t[1];
            return;
        }
        throw new IllegalArgumentException("Tweak must be 2 words.");
    }
    
    public static void wordToBytes(final long n, final byte[] array, int n2) {
        if (n2 + 8 <= array.length) {
            final int n3 = n2 + 1;
            array[n2] = (byte)n;
            n2 = n3 + 1;
            array[n3] = (byte)(n >> 8);
            final int n4 = n2 + 1;
            array[n2] = (byte)(n >> 16);
            n2 = n4 + 1;
            array[n4] = (byte)(n >> 24);
            final int n5 = n2 + 1;
            array[n2] = (byte)(n >> 32);
            n2 = n5 + 1;
            array[n5] = (byte)(n >> 40);
            array[n2] = (byte)(n >> 48);
            array[n2 + 1] = (byte)(n >> 56);
            return;
        }
        throw new IllegalArgumentException();
    }
    
    static long xorRotr(long n, final int n2, final long n3) {
        n ^= n3;
        return n << -n2 | n >>> n2;
    }
    
    @Override
    public String getAlgorithmName() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Threefish-");
        sb.append(this.blocksizeBytes * 8);
        return sb.toString();
    }
    
    @Override
    public int getBlockSize() {
        return this.blocksizeBytes;
    }
    
    @Override
    public void init(final boolean b, final CipherParameters cipherParameters) throws IllegalArgumentException {
        final boolean b2 = cipherParameters instanceof TweakableBlockCipherParameters;
        final long[] array = null;
        byte[] array2;
        byte[] tweak;
        if (b2) {
            final TweakableBlockCipherParameters tweakableBlockCipherParameters = (TweakableBlockCipherParameters)cipherParameters;
            array2 = tweakableBlockCipherParameters.getKey().getKey();
            tweak = tweakableBlockCipherParameters.getTweak();
        }
        else {
            if (!(cipherParameters instanceof KeyParameter)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid parameter passed to Threefish init - ");
                sb.append(cipherParameters.getClass().getName());
                throw new IllegalArgumentException(sb.toString());
            }
            array2 = ((KeyParameter)cipherParameters).getKey();
            tweak = null;
        }
        long[] array4;
        if (array2 != null) {
            if (array2.length != this.blocksizeBytes) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Threefish key must be same size as block (");
                sb2.append(this.blocksizeBytes);
                sb2.append(" bytes)");
                throw new IllegalArgumentException(sb2.toString());
            }
            final long[] array3 = new long[this.blocksizeWords];
            int n = 0;
            while (true) {
                array4 = array3;
                if (n >= array3.length) {
                    break;
                }
                array3[n] = bytesToWord(array2, n * 8);
                ++n;
            }
        }
        else {
            array4 = null;
        }
        long[] array5 = array;
        if (tweak != null) {
            if (tweak.length != 16) {
                throw new IllegalArgumentException("Threefish tweak must be 16 bytes");
            }
            array5 = new long[] { bytesToWord(tweak, 0), bytesToWord(tweak, 8) };
        }
        this.init(b, array4, array5);
    }
    
    public void init(final boolean forEncryption, final long[] key, final long[] tweak) {
        this.forEncryption = forEncryption;
        if (key != null) {
            this.setKey(key);
        }
        if (tweak != null) {
            this.setTweak(tweak);
        }
    }
    
    @Override
    public int processBlock(final byte[] array, int n, final byte[] array2, final int n2) throws DataLengthException, IllegalStateException {
        final int blocksizeBytes = this.blocksizeBytes;
        if (n + blocksizeBytes > array.length) {
            throw new DataLengthException("Input buffer too short");
        }
        if (blocksizeBytes + n2 <= array2.length) {
            final int n3 = 0;
            for (int i = 0; i < this.blocksizeBytes; i += 8) {
                this.currentBlock[i >> 3] = bytesToWord(array, n + i);
            }
            final long[] currentBlock = this.currentBlock;
            this.processBlock(currentBlock, currentBlock);
            n = n3;
            int blocksizeBytes2;
            while (true) {
                blocksizeBytes2 = this.blocksizeBytes;
                if (n >= blocksizeBytes2) {
                    break;
                }
                wordToBytes(this.currentBlock[n >> 3], array2, n2 + n);
                n += 8;
            }
            return blocksizeBytes2;
        }
        throw new OutputLengthException("Output buffer too short");
    }
    
    public int processBlock(final long[] array, final long[] array2) throws DataLengthException, IllegalStateException {
        final long[] kw = this.kw;
        final int blocksizeWords = this.blocksizeWords;
        if (kw[blocksizeWords] == 0L) {
            throw new IllegalStateException("Threefish engine not initialised");
        }
        if (array.length != blocksizeWords) {
            throw new DataLengthException("Input buffer too short");
        }
        if (array2.length == blocksizeWords) {
            if (this.forEncryption) {
                this.cipher.encryptBlock(array, array2);
            }
            else {
                this.cipher.decryptBlock(array, array2);
            }
            return this.blocksizeWords;
        }
        throw new OutputLengthException("Output buffer too short");
    }
    
    @Override
    public void reset() {
    }
    
    private static final class Threefish1024Cipher extends ThreefishCipher
    {
        private static final int ROTATION_0_0 = 24;
        private static final int ROTATION_0_1 = 13;
        private static final int ROTATION_0_2 = 8;
        private static final int ROTATION_0_3 = 47;
        private static final int ROTATION_0_4 = 8;
        private static final int ROTATION_0_5 = 17;
        private static final int ROTATION_0_6 = 22;
        private static final int ROTATION_0_7 = 37;
        private static final int ROTATION_1_0 = 38;
        private static final int ROTATION_1_1 = 19;
        private static final int ROTATION_1_2 = 10;
        private static final int ROTATION_1_3 = 55;
        private static final int ROTATION_1_4 = 49;
        private static final int ROTATION_1_5 = 18;
        private static final int ROTATION_1_6 = 23;
        private static final int ROTATION_1_7 = 52;
        private static final int ROTATION_2_0 = 33;
        private static final int ROTATION_2_1 = 4;
        private static final int ROTATION_2_2 = 51;
        private static final int ROTATION_2_3 = 13;
        private static final int ROTATION_2_4 = 34;
        private static final int ROTATION_2_5 = 41;
        private static final int ROTATION_2_6 = 59;
        private static final int ROTATION_2_7 = 17;
        private static final int ROTATION_3_0 = 5;
        private static final int ROTATION_3_1 = 20;
        private static final int ROTATION_3_2 = 48;
        private static final int ROTATION_3_3 = 41;
        private static final int ROTATION_3_4 = 47;
        private static final int ROTATION_3_5 = 28;
        private static final int ROTATION_3_6 = 16;
        private static final int ROTATION_3_7 = 25;
        private static final int ROTATION_4_0 = 41;
        private static final int ROTATION_4_1 = 9;
        private static final int ROTATION_4_2 = 37;
        private static final int ROTATION_4_3 = 31;
        private static final int ROTATION_4_4 = 12;
        private static final int ROTATION_4_5 = 47;
        private static final int ROTATION_4_6 = 44;
        private static final int ROTATION_4_7 = 30;
        private static final int ROTATION_5_0 = 16;
        private static final int ROTATION_5_1 = 34;
        private static final int ROTATION_5_2 = 56;
        private static final int ROTATION_5_3 = 51;
        private static final int ROTATION_5_4 = 4;
        private static final int ROTATION_5_5 = 53;
        private static final int ROTATION_5_6 = 42;
        private static final int ROTATION_5_7 = 41;
        private static final int ROTATION_6_0 = 31;
        private static final int ROTATION_6_1 = 44;
        private static final int ROTATION_6_2 = 47;
        private static final int ROTATION_6_3 = 46;
        private static final int ROTATION_6_4 = 19;
        private static final int ROTATION_6_5 = 42;
        private static final int ROTATION_6_6 = 44;
        private static final int ROTATION_6_7 = 25;
        private static final int ROTATION_7_0 = 9;
        private static final int ROTATION_7_1 = 48;
        private static final int ROTATION_7_2 = 35;
        private static final int ROTATION_7_3 = 52;
        private static final int ROTATION_7_4 = 23;
        private static final int ROTATION_7_5 = 31;
        private static final int ROTATION_7_6 = 37;
        private static final int ROTATION_7_7 = 20;
        
        public Threefish1024Cipher(final long[] array, final long[] array2) {
            super(array, array2);
        }
        
        @Override
        void decryptBlock(final long[] array, final long[] array2) {
            final long[] kw = this.kw;
            final long[] t = this.t;
            final int[] access$300 = ThreefishEngine.MOD17;
            final int[] access$301 = ThreefishEngine.MOD3;
            if (kw.length != 33) {
                throw new IllegalArgumentException();
            }
            if (t.length == 5) {
                long n = array[0];
                long n2 = array[1];
                long n3 = array[2];
                long xorRotr = array[3];
                long n4 = array[4];
                long xorRotr2 = array[5];
                long n5 = array[6];
                long xorRotr3 = array[7];
                long n6 = array[8];
                long xorRotr4 = array[9];
                long n7 = array[10];
                long xorRotr5 = array[11];
                long n8 = array[12];
                long xorRotr6 = array[13];
                long n9 = array[14];
                long xorRotr7 = array[15];
                int i = 19;
                final int[] array3 = access$301;
                while (i >= 1) {
                    final int n10 = access$300[i];
                    final int n11 = array3[i];
                    final int n12 = n10 + 1;
                    final long n13 = n - kw[n12];
                    final int n14 = n10 + 2;
                    final long n15 = kw[n14];
                    final int n16 = n10 + 3;
                    final long n17 = n3 - kw[n16];
                    final int n18 = n10 + 4;
                    final long n19 = kw[n18];
                    final int n20 = n10 + 5;
                    final long n21 = n4 - kw[n20];
                    final int n22 = n10 + 6;
                    final long n23 = kw[n22];
                    final int n24 = n10 + 7;
                    final long n25 = n5 - kw[n24];
                    final int n26 = n10 + 8;
                    final long n27 = kw[n26];
                    final int n28 = n10 + 9;
                    final long n29 = n6 - kw[n28];
                    final int n30 = n10 + 10;
                    final long n31 = kw[n30];
                    final int n32 = n10 + 11;
                    final long n33 = n7 - kw[n32];
                    final int n34 = n10 + 12;
                    final long n35 = kw[n34];
                    final int n36 = n10 + 13;
                    final long n37 = n8 - kw[n36];
                    final int n38 = n10 + 14;
                    final long n39 = kw[n38];
                    final int n40 = n11 + 1;
                    final long n41 = t[n40];
                    final int n42 = n10 + 15;
                    final long n43 = n9 - (kw[n42] + t[n11 + 2]);
                    final long n44 = kw[n10 + 16];
                    final long n45 = i;
                    final long xorRotr8 = ThreefishEngine.xorRotr(xorRotr7 - (n44 + n45 + 1L), 9, n13);
                    final long n46 = n13 - xorRotr8;
                    final long xorRotr9 = ThreefishEngine.xorRotr(xorRotr5 - n35, 48, n17);
                    final long n47 = n17 - xorRotr9;
                    final long xorRotr10 = ThreefishEngine.xorRotr(xorRotr6 - (n39 + n41), 35, n25);
                    final long n48 = n25 - xorRotr10;
                    final long xorRotr11 = ThreefishEngine.xorRotr(xorRotr4 - n31, 52, n21);
                    final long n49 = n21 - xorRotr11;
                    final long xorRotr12 = ThreefishEngine.xorRotr(n2 - n15, 23, n43);
                    final long n50 = n43 - xorRotr12;
                    final long xorRotr13 = ThreefishEngine.xorRotr(xorRotr2 - n23, 31, n29);
                    final long n51 = n29 - xorRotr13;
                    final long xorRotr14 = ThreefishEngine.xorRotr(xorRotr - n19, 37, n33);
                    final long n52 = n33 - xorRotr14;
                    final long xorRotr15 = ThreefishEngine.xorRotr(xorRotr3 - n27, 20, n37);
                    final long n53 = n37 - xorRotr15;
                    final long xorRotr16 = ThreefishEngine.xorRotr(xorRotr15, 31, n46);
                    final long n54 = n46 - xorRotr16;
                    final long xorRotr17 = ThreefishEngine.xorRotr(xorRotr13, 44, n47);
                    final long n55 = n47 - xorRotr17;
                    final long xorRotr18 = ThreefishEngine.xorRotr(xorRotr14, 47, n49);
                    final long n56 = n49 - xorRotr18;
                    final long xorRotr19 = ThreefishEngine.xorRotr(xorRotr12, 46, n48);
                    final long n57 = n48 - xorRotr19;
                    final long xorRotr20 = ThreefishEngine.xorRotr(xorRotr8, 19, n53);
                    final long n58 = n53 - xorRotr20;
                    final long xorRotr21 = ThreefishEngine.xorRotr(xorRotr10, 42, n50);
                    final long n59 = n50 - xorRotr21;
                    final long xorRotr22 = ThreefishEngine.xorRotr(xorRotr9, 44, n51);
                    final long n60 = n51 - xorRotr22;
                    final long xorRotr23 = ThreefishEngine.xorRotr(xorRotr11, 25, n52);
                    final long n61 = n52 - xorRotr23;
                    final long xorRotr24 = ThreefishEngine.xorRotr(xorRotr23, 16, n54);
                    final long n62 = n54 - xorRotr24;
                    final long xorRotr25 = ThreefishEngine.xorRotr(xorRotr21, 34, n55);
                    final long n63 = n55 - xorRotr25;
                    final long xorRotr26 = ThreefishEngine.xorRotr(xorRotr22, 56, n57);
                    final long n64 = n57 - xorRotr26;
                    final long xorRotr27 = ThreefishEngine.xorRotr(xorRotr20, 51, n56);
                    final long n65 = n56 - xorRotr27;
                    final long xorRotr28 = ThreefishEngine.xorRotr(xorRotr16, 4, n61);
                    final long n66 = n61 - xorRotr28;
                    final long xorRotr29 = ThreefishEngine.xorRotr(xorRotr18, 53, n58);
                    final long n67 = n58 - xorRotr29;
                    final long xorRotr30 = ThreefishEngine.xorRotr(xorRotr17, 42, n59);
                    final long n68 = n59 - xorRotr30;
                    final long xorRotr31 = ThreefishEngine.xorRotr(xorRotr19, 41, n60);
                    final long n69 = n60 - xorRotr31;
                    final long xorRotr32 = ThreefishEngine.xorRotr(xorRotr31, 41, n62);
                    final long xorRotr33 = ThreefishEngine.xorRotr(xorRotr29, 9, n63);
                    final long xorRotr34 = ThreefishEngine.xorRotr(xorRotr30, 37, n65);
                    final long xorRotr35 = ThreefishEngine.xorRotr(xorRotr28, 31, n64);
                    final long xorRotr36 = ThreefishEngine.xorRotr(xorRotr24, 12, n69);
                    final long xorRotr37 = ThreefishEngine.xorRotr(xorRotr26, 47, n66);
                    final long xorRotr38 = ThreefishEngine.xorRotr(xorRotr25, 44, n67);
                    final long xorRotr39 = ThreefishEngine.xorRotr(xorRotr27, 30, n68);
                    final long n70 = n62 - xorRotr32 - kw[n10];
                    final long n71 = kw[n12];
                    final long n72 = n63 - xorRotr33 - kw[n14];
                    final long n73 = kw[n16];
                    final long n74 = n65 - xorRotr34 - kw[n18];
                    final long n75 = kw[n20];
                    final long n76 = n64 - xorRotr35 - kw[n22];
                    final long n77 = kw[n24];
                    final long n78 = n69 - xorRotr36 - kw[n26];
                    final long n79 = kw[n28];
                    final long n80 = n66 - xorRotr37 - kw[n30];
                    final long n81 = kw[n32];
                    final long n82 = n67 - xorRotr38 - kw[n34];
                    final long n83 = kw[n36];
                    final long n84 = t[n11];
                    final long n85 = n68 - xorRotr39 - (kw[n38] + t[n40]);
                    final long xorRotr40 = ThreefishEngine.xorRotr(xorRotr39 - (kw[n42] + n45), 5, n70);
                    final long n86 = n70 - xorRotr40;
                    final long xorRotr41 = ThreefishEngine.xorRotr(xorRotr37 - n81, 20, n72);
                    final long n87 = n72 - xorRotr41;
                    final long xorRotr42 = ThreefishEngine.xorRotr(xorRotr38 - (n83 + n84), 48, n76);
                    final long n88 = n76 - xorRotr42;
                    final long xorRotr43 = ThreefishEngine.xorRotr(xorRotr36 - n79, 41, n74);
                    final long n89 = n74 - xorRotr43;
                    final long xorRotr44 = ThreefishEngine.xorRotr(xorRotr32 - n71, 47, n85);
                    final long n90 = n85 - xorRotr44;
                    final long xorRotr45 = ThreefishEngine.xorRotr(xorRotr34 - n75, 28, n78);
                    final long n91 = n78 - xorRotr45;
                    final long xorRotr46 = ThreefishEngine.xorRotr(xorRotr33 - n73, 16, n80);
                    final long n92 = n80 - xorRotr46;
                    final long xorRotr47 = ThreefishEngine.xorRotr(xorRotr35 - n77, 25, n82);
                    final long n93 = n82 - xorRotr47;
                    final long xorRotr48 = ThreefishEngine.xorRotr(xorRotr47, 33, n86);
                    final long n94 = n86 - xorRotr48;
                    final long xorRotr49 = ThreefishEngine.xorRotr(xorRotr45, 4, n87);
                    final long n95 = n87 - xorRotr49;
                    final long xorRotr50 = ThreefishEngine.xorRotr(xorRotr46, 51, n89);
                    final long n96 = n89 - xorRotr50;
                    final long xorRotr51 = ThreefishEngine.xorRotr(xorRotr44, 13, n88);
                    final long n97 = n88 - xorRotr51;
                    final long xorRotr52 = ThreefishEngine.xorRotr(xorRotr40, 34, n93);
                    final long n98 = n93 - xorRotr52;
                    final long xorRotr53 = ThreefishEngine.xorRotr(xorRotr42, 41, n90);
                    final long n99 = n90 - xorRotr53;
                    final long xorRotr54 = ThreefishEngine.xorRotr(xorRotr41, 59, n91);
                    final long n100 = n91 - xorRotr54;
                    final long xorRotr55 = ThreefishEngine.xorRotr(xorRotr43, 17, n92);
                    final long n101 = n92 - xorRotr55;
                    final long xorRotr56 = ThreefishEngine.xorRotr(xorRotr55, 38, n94);
                    final long n102 = n94 - xorRotr56;
                    final long xorRotr57 = ThreefishEngine.xorRotr(xorRotr53, 19, n95);
                    final long n103 = n95 - xorRotr57;
                    final long xorRotr58 = ThreefishEngine.xorRotr(xorRotr54, 10, n97);
                    final long n104 = n97 - xorRotr58;
                    final long xorRotr59 = ThreefishEngine.xorRotr(xorRotr52, 55, n96);
                    final long n105 = n96 - xorRotr59;
                    final long xorRotr60 = ThreefishEngine.xorRotr(xorRotr48, 49, n101);
                    final long n106 = n101 - xorRotr60;
                    final long xorRotr61 = ThreefishEngine.xorRotr(xorRotr50, 18, n98);
                    final long n107 = n98 - xorRotr61;
                    final long xorRotr62 = ThreefishEngine.xorRotr(xorRotr49, 23, n99);
                    final long n108 = n99 - xorRotr62;
                    final long xorRotr63 = ThreefishEngine.xorRotr(xorRotr51, 52, n100);
                    final long n109 = n100 - xorRotr63;
                    final long xorRotr64 = ThreefishEngine.xorRotr(xorRotr63, 24, n102);
                    xorRotr = ThreefishEngine.xorRotr(xorRotr61, 13, n103);
                    xorRotr2 = ThreefishEngine.xorRotr(xorRotr62, 8, n105);
                    xorRotr3 = ThreefishEngine.xorRotr(xorRotr60, 47, n104);
                    xorRotr4 = ThreefishEngine.xorRotr(xorRotr56, 8, n109);
                    xorRotr5 = ThreefishEngine.xorRotr(xorRotr58, 17, n106);
                    xorRotr6 = ThreefishEngine.xorRotr(xorRotr57, 22, n107);
                    xorRotr7 = ThreefishEngine.xorRotr(xorRotr59, 37, n108);
                    n9 = n108 - xorRotr7;
                    n7 = n106 - xorRotr5;
                    n4 = n105 - xorRotr2;
                    n3 = n103 - xorRotr;
                    n2 = xorRotr64;
                    n = n102 - xorRotr64;
                    n5 = n104 - xorRotr3;
                    i -= 2;
                    n6 = n109 - xorRotr4;
                    n8 = n107 - xorRotr6;
                }
                final long n110 = kw[0];
                final long n111 = kw[1];
                final long n112 = kw[2];
                final long n113 = kw[3];
                final long n114 = kw[4];
                final long n115 = kw[5];
                final long n116 = kw[6];
                final long n117 = kw[7];
                final long n118 = kw[8];
                final long n119 = kw[9];
                final long n120 = kw[10];
                final long n121 = kw[11];
                final long n122 = kw[12];
                final long n123 = kw[13];
                final long n124 = t[0];
                final long n125 = kw[14];
                final long n126 = t[1];
                final long n127 = kw[15];
                array2[0] = n - n110;
                array2[1] = n2 - n111;
                array2[2] = n3 - n112;
                array2[3] = xorRotr - n113;
                array2[4] = n4 - n114;
                array2[5] = xorRotr2 - n115;
                array2[6] = n5 - n116;
                array2[7] = xorRotr3 - n117;
                array2[8] = n6 - n118;
                array2[9] = xorRotr4 - n119;
                array2[10] = n7 - n120;
                array2[11] = xorRotr5 - n121;
                array2[12] = n8 - n122;
                array2[13] = xorRotr6 - (n123 + n124);
                array2[14] = n9 - (n125 + n126);
                array2[15] = xorRotr7 - n127;
                return;
            }
            throw new IllegalArgumentException();
        }
        
        @Override
        void encryptBlock(long[] array, final long[] array2) {
            final long[] kw = this.kw;
            final long[] t = this.t;
            final int[] access$300 = ThreefishEngine.MOD17;
            final int[] access$301 = ThreefishEngine.MOD3;
            if (kw.length != 33) {
                throw new IllegalArgumentException();
            }
            if (t.length == 5) {
                final long n = array[0];
                final long n2 = array[1];
                final long n3 = array[2];
                final long n4 = array[3];
                final long n5 = array[4];
                final long n6 = array[5];
                final long n7 = array[6];
                final long n8 = array[7];
                final long n9 = array[8];
                final long n10 = array[9];
                final long n11 = array[10];
                final long n12 = array[11];
                final long n13 = array[12];
                final long n14 = array[13];
                final long n15 = array[14];
                final long n16 = array[15];
                final long n17 = kw[0];
                final long n18 = kw[1];
                final long n19 = kw[2];
                final long n20 = kw[3];
                final long n21 = kw[4];
                final long n22 = kw[5];
                long n23 = n7 + kw[6];
                final long n24 = kw[7];
                long n25 = n9 + kw[8];
                final long n26 = kw[9];
                long n27 = n11 + kw[10];
                final long n28 = kw[11];
                long n29 = n13 + kw[12];
                final long n30 = kw[13];
                final long n31 = t[0];
                long n32 = n15 + (kw[14] + t[1]);
                final long n33 = kw[15];
                long n34 = n4 + n20;
                long n35 = n8 + n24;
                long n36 = n10 + n26;
                long n37 = n12 + n28;
                long n38 = n14 + (n30 + n31);
                long n39 = n16 + n33;
                long n40 = n3 + n19;
                long n41 = n2 + n18;
                long n42 = n + n17;
                int i = 1;
                long n43 = n6 + n22;
                long n44 = n5 + n21;
                array = t;
                while (i < 20) {
                    final int n45 = access$300[i];
                    final int n46 = access$301[i];
                    final long n47 = n42 + n41;
                    final long rotlXor = ThreefishEngine.rotlXor(n41, 24, n47);
                    final long n48 = n40 + n34;
                    final long rotlXor2 = ThreefishEngine.rotlXor(n34, 13, n48);
                    final long n49 = n44 + n43;
                    final long rotlXor3 = ThreefishEngine.rotlXor(n43, 8, n49);
                    final long n50 = n23 + n35;
                    final long rotlXor4 = ThreefishEngine.rotlXor(n35, 47, n50);
                    final long n51 = n25 + n36;
                    final long rotlXor5 = ThreefishEngine.rotlXor(n36, 8, n51);
                    final long n52 = n27 + n37;
                    final long rotlXor6 = ThreefishEngine.rotlXor(n37, 17, n52);
                    final long n53 = n29 + n38;
                    final long rotlXor7 = ThreefishEngine.rotlXor(n38, 22, n53);
                    final long n54 = n32 + n39;
                    final long rotlXor8 = ThreefishEngine.rotlXor(n39, 37, n54);
                    final long n55 = n47 + rotlXor5;
                    final long rotlXor9 = ThreefishEngine.rotlXor(rotlXor5, 38, n55);
                    final long n56 = n48 + rotlXor7;
                    final long rotlXor10 = ThreefishEngine.rotlXor(rotlXor7, 19, n56);
                    final long n57 = n50 + rotlXor6;
                    final long rotlXor11 = ThreefishEngine.rotlXor(rotlXor6, 10, n57);
                    final long n58 = n49 + rotlXor8;
                    final long rotlXor12 = ThreefishEngine.rotlXor(rotlXor8, 55, n58);
                    final long n59 = n52 + rotlXor4;
                    final long rotlXor13 = ThreefishEngine.rotlXor(rotlXor4, 49, n59);
                    final long n60 = n53 + rotlXor2;
                    final long rotlXor14 = ThreefishEngine.rotlXor(rotlXor2, 18, n60);
                    final long n61 = n54 + rotlXor3;
                    final long rotlXor15 = ThreefishEngine.rotlXor(rotlXor3, 23, n61);
                    final long n62 = n51 + rotlXor;
                    final long rotlXor16 = ThreefishEngine.rotlXor(rotlXor, 52, n62);
                    final long n63 = n55 + rotlXor13;
                    final long rotlXor17 = ThreefishEngine.rotlXor(rotlXor13, 33, n63);
                    final long n64 = n56 + rotlXor15;
                    final long rotlXor18 = ThreefishEngine.rotlXor(rotlXor15, 4, n64);
                    final long n65 = n58 + rotlXor14;
                    final long rotlXor19 = ThreefishEngine.rotlXor(rotlXor14, 51, n65);
                    final long n66 = n57 + rotlXor16;
                    final long rotlXor20 = ThreefishEngine.rotlXor(rotlXor16, 13, n66);
                    final long n67 = n60 + rotlXor12;
                    final long rotlXor21 = ThreefishEngine.rotlXor(rotlXor12, 34, n67);
                    final long n68 = n61 + rotlXor10;
                    final long rotlXor22 = ThreefishEngine.rotlXor(rotlXor10, 41, n68);
                    final long n69 = n62 + rotlXor11;
                    final long rotlXor23 = ThreefishEngine.rotlXor(rotlXor11, 59, n69);
                    final long n70 = n59 + rotlXor9;
                    final long rotlXor24 = ThreefishEngine.rotlXor(rotlXor9, 17, n70);
                    final long n71 = n63 + rotlXor21;
                    final long rotlXor25 = ThreefishEngine.rotlXor(rotlXor21, 5, n71);
                    final long n72 = n64 + rotlXor23;
                    final long rotlXor26 = ThreefishEngine.rotlXor(rotlXor23, 20, n72);
                    final long n73 = n66 + rotlXor22;
                    final long rotlXor27 = ThreefishEngine.rotlXor(rotlXor22, 48, n73);
                    final long n74 = n65 + rotlXor24;
                    final long rotlXor28 = ThreefishEngine.rotlXor(rotlXor24, 41, n74);
                    final long n75 = n68 + rotlXor20;
                    final long rotlXor29 = ThreefishEngine.rotlXor(rotlXor20, 47, n75);
                    final long n76 = n69 + rotlXor18;
                    final long rotlXor30 = ThreefishEngine.rotlXor(rotlXor18, 28, n76);
                    final long n77 = n70 + rotlXor19;
                    final long rotlXor31 = ThreefishEngine.rotlXor(rotlXor19, 16, n77);
                    final long n78 = n67 + rotlXor17;
                    final long rotlXor32 = ThreefishEngine.rotlXor(rotlXor17, 25, n78);
                    final long n79 = kw[n45];
                    final int n80 = n45 + 1;
                    final long n81 = rotlXor29 + kw[n80];
                    final int n82 = n45 + 2;
                    final long n83 = kw[n82];
                    final int n84 = n45 + 3;
                    final long n85 = rotlXor31 + kw[n84];
                    final int n86 = n45 + 4;
                    final long n87 = kw[n86];
                    final int n88 = n45 + 5;
                    final long n89 = rotlXor30 + kw[n88];
                    final int n90 = n45 + 6;
                    final long n91 = kw[n90];
                    final int n92 = n45 + 7;
                    final long n93 = rotlXor32 + kw[n92];
                    final int n94 = n45 + 8;
                    final long n95 = kw[n94];
                    final int n96 = n45 + 9;
                    final long n97 = rotlXor28 + kw[n96];
                    final int n98 = n45 + 10;
                    final long n99 = kw[n98];
                    final int n100 = n45 + 11;
                    final long n101 = rotlXor26 + kw[n100];
                    final int n102 = n45 + 12;
                    final long n103 = kw[n102];
                    final int n104 = n45 + 13;
                    final long n105 = rotlXor27 + (kw[n104] + array[n46]);
                    final int n106 = n45 + 14;
                    final long n107 = kw[n106];
                    final int n108 = n46 + 1;
                    final long n109 = array[n108];
                    final int n110 = n45 + 15;
                    final long n111 = kw[n110];
                    final long n112 = i;
                    final long n113 = rotlXor25 + (n111 + n112);
                    final long n114 = n71 + n79 + n81;
                    final long rotlXor33 = ThreefishEngine.rotlXor(n81, 41, n114);
                    final long n115 = n72 + n83 + n85;
                    final long rotlXor34 = ThreefishEngine.rotlXor(n85, 9, n115);
                    final long n116 = n74 + n87 + n89;
                    final long rotlXor35 = ThreefishEngine.rotlXor(n89, 37, n116);
                    final long n117 = n73 + n91 + n93;
                    final long rotlXor36 = ThreefishEngine.rotlXor(n93, 31, n117);
                    final long n118 = n76 + n95 + n97;
                    final long rotlXor37 = ThreefishEngine.rotlXor(n97, 12, n118);
                    final long n119 = n77 + n99 + n101;
                    final long rotlXor38 = ThreefishEngine.rotlXor(n101, 47, n119);
                    final long n120 = n78 + n103 + n105;
                    final long rotlXor39 = ThreefishEngine.rotlXor(n105, 44, n120);
                    final long n121 = n75 + (n107 + n109) + n113;
                    final long rotlXor40 = ThreefishEngine.rotlXor(n113, 30, n121);
                    final long n122 = n114 + rotlXor37;
                    final long rotlXor41 = ThreefishEngine.rotlXor(rotlXor37, 16, n122);
                    final long n123 = n115 + rotlXor39;
                    final long rotlXor42 = ThreefishEngine.rotlXor(rotlXor39, 34, n123);
                    final long n124 = n117 + rotlXor38;
                    final long rotlXor43 = ThreefishEngine.rotlXor(rotlXor38, 56, n124);
                    final long n125 = n116 + rotlXor40;
                    final long rotlXor44 = ThreefishEngine.rotlXor(rotlXor40, 51, n125);
                    final long n126 = n119 + rotlXor36;
                    final long rotlXor45 = ThreefishEngine.rotlXor(rotlXor36, 4, n126);
                    final long n127 = n120 + rotlXor34;
                    final long rotlXor46 = ThreefishEngine.rotlXor(rotlXor34, 53, n127);
                    final long n128 = n121 + rotlXor35;
                    final long rotlXor47 = ThreefishEngine.rotlXor(rotlXor35, 42, n128);
                    final long n129 = n118 + rotlXor33;
                    final long rotlXor48 = ThreefishEngine.rotlXor(rotlXor33, 41, n129);
                    final long n130 = n122 + rotlXor45;
                    final long rotlXor49 = ThreefishEngine.rotlXor(rotlXor45, 31, n130);
                    final long n131 = n123 + rotlXor47;
                    final long rotlXor50 = ThreefishEngine.rotlXor(rotlXor47, 44, n131);
                    final long n132 = n125 + rotlXor46;
                    final long rotlXor51 = ThreefishEngine.rotlXor(rotlXor46, 47, n132);
                    final long n133 = n124 + rotlXor48;
                    final long rotlXor52 = ThreefishEngine.rotlXor(rotlXor48, 46, n133);
                    final long n134 = n127 + rotlXor44;
                    final long rotlXor53 = ThreefishEngine.rotlXor(rotlXor44, 19, n134);
                    final long n135 = n128 + rotlXor42;
                    final long rotlXor54 = ThreefishEngine.rotlXor(rotlXor42, 42, n135);
                    final long n136 = n129 + rotlXor43;
                    final long rotlXor55 = ThreefishEngine.rotlXor(rotlXor43, 44, n136);
                    final long n137 = n126 + rotlXor41;
                    final long rotlXor56 = ThreefishEngine.rotlXor(rotlXor41, 25, n137);
                    final long n138 = n130 + rotlXor53;
                    final long rotlXor57 = ThreefishEngine.rotlXor(rotlXor53, 9, n138);
                    final long n139 = n131 + rotlXor55;
                    final long rotlXor58 = ThreefishEngine.rotlXor(rotlXor55, 48, n139);
                    final long n140 = n133 + rotlXor54;
                    final long rotlXor59 = ThreefishEngine.rotlXor(rotlXor54, 35, n140);
                    final long n141 = n132 + rotlXor56;
                    final long rotlXor60 = ThreefishEngine.rotlXor(rotlXor56, 52, n141);
                    final long n142 = n135 + rotlXor52;
                    final long rotlXor61 = ThreefishEngine.rotlXor(rotlXor52, 23, n142);
                    final long n143 = n136 + rotlXor50;
                    final long rotlXor62 = ThreefishEngine.rotlXor(rotlXor50, 31, n143);
                    final long n144 = n137 + rotlXor51;
                    final long rotlXor63 = ThreefishEngine.rotlXor(rotlXor51, 37, n144);
                    final long n145 = n134 + rotlXor49;
                    final long rotlXor64 = ThreefishEngine.rotlXor(rotlXor49, 20, n145);
                    n42 = n138 + kw[n80];
                    final long n146 = kw[n82];
                    final long n147 = kw[n84];
                    final long n148 = kw[n86];
                    final long n149 = kw[n88];
                    final long n150 = kw[n90];
                    final long n151 = kw[n92];
                    final long n152 = kw[n94];
                    final long n153 = kw[n96];
                    n36 = rotlXor60 + kw[n98];
                    n27 = n144 + kw[n100];
                    n37 = rotlXor58 + kw[n102];
                    final long n154 = kw[n104];
                    final long n155 = kw[n106];
                    final long n156 = array[n108];
                    n32 = n142 + (kw[n110] + array[n46 + 2]);
                    n39 = rotlXor57 + (kw[n45 + 16] + n112 + 1L);
                    n44 = n141 + n149;
                    n35 = rotlXor64 + n152;
                    i += 2;
                    n43 = rotlXor62 + n150;
                    n23 = n140 + n151;
                    n25 = n143 + n153;
                    n29 = n145 + n154;
                    n41 = rotlXor61 + n146;
                    n34 = rotlXor63 + n148;
                    n38 = rotlXor59 + (n155 + n156);
                    n40 = n139 + n147;
                }
                array2[0] = n42;
                array2[1] = n41;
                array2[2] = n40;
                array2[3] = n34;
                array2[4] = n44;
                array2[5] = n43;
                array2[6] = n23;
                array2[7] = n35;
                array2[8] = n25;
                array2[9] = n36;
                array2[10] = n27;
                array2[11] = n37;
                array2[12] = n29;
                array2[13] = n38;
                array2[14] = n32;
                array2[15] = n39;
                return;
            }
            throw new IllegalArgumentException();
        }
    }
    
    private static final class Threefish256Cipher extends ThreefishCipher
    {
        private static final int ROTATION_0_0 = 14;
        private static final int ROTATION_0_1 = 16;
        private static final int ROTATION_1_0 = 52;
        private static final int ROTATION_1_1 = 57;
        private static final int ROTATION_2_0 = 23;
        private static final int ROTATION_2_1 = 40;
        private static final int ROTATION_3_0 = 5;
        private static final int ROTATION_3_1 = 37;
        private static final int ROTATION_4_0 = 25;
        private static final int ROTATION_4_1 = 33;
        private static final int ROTATION_5_0 = 46;
        private static final int ROTATION_5_1 = 12;
        private static final int ROTATION_6_0 = 58;
        private static final int ROTATION_6_1 = 22;
        private static final int ROTATION_7_0 = 32;
        private static final int ROTATION_7_1 = 32;
        
        public Threefish256Cipher(final long[] array, final long[] array2) {
            super(array, array2);
        }
        
        @Override
        void decryptBlock(final long[] array, final long[] array2) {
            final long[] kw = this.kw;
            final long[] t = this.t;
            final int[] access$000 = ThreefishEngine.MOD5;
            final int[] access$2 = ThreefishEngine.MOD3;
            if (kw.length != 9) {
                throw new IllegalArgumentException();
            }
            if (t.length == 5) {
                long n = array[0];
                long n2 = array[1];
                long n3 = array[2];
                long xorRotr = array[3];
                int i = 17;
                final int[] array3 = access$2;
                while (i >= 1) {
                    final int n4 = access$000[i];
                    final int n5 = array3[i];
                    final int n6 = n4 + 1;
                    final long n7 = n - kw[n6];
                    final int n8 = n4 + 2;
                    final long n9 = kw[n8];
                    final int n10 = n5 + 1;
                    final long n11 = t[n10];
                    final int n12 = n4 + 3;
                    final long n13 = n3 - (kw[n12] + t[n5 + 2]);
                    final long n14 = kw[n4 + 4];
                    final long n15 = i;
                    final long xorRotr2 = ThreefishEngine.xorRotr(xorRotr - (n14 + n15 + 1L), 32, n7);
                    final long n16 = n7 - xorRotr2;
                    final long xorRotr3 = ThreefishEngine.xorRotr(n2 - (n9 + n11), 32, n13);
                    final long n17 = n13 - xorRotr3;
                    final long xorRotr4 = ThreefishEngine.xorRotr(xorRotr3, 58, n16);
                    final long n18 = n16 - xorRotr4;
                    final long xorRotr5 = ThreefishEngine.xorRotr(xorRotr2, 22, n17);
                    final long n19 = n17 - xorRotr5;
                    final long xorRotr6 = ThreefishEngine.xorRotr(xorRotr5, 46, n18);
                    final long n20 = n18 - xorRotr6;
                    final long xorRotr7 = ThreefishEngine.xorRotr(xorRotr4, 12, n19);
                    final long n21 = n19 - xorRotr7;
                    final long xorRotr8 = ThreefishEngine.xorRotr(xorRotr7, 25, n20);
                    final long xorRotr9 = ThreefishEngine.xorRotr(xorRotr6, 33, n21);
                    final long n22 = n20 - xorRotr8 - kw[n4];
                    final long n23 = kw[n6];
                    final long n24 = t[n5];
                    final long n25 = n21 - xorRotr9 - (kw[n8] + t[n10]);
                    final long xorRotr10 = ThreefishEngine.xorRotr(xorRotr9 - (kw[n12] + n15), 5, n22);
                    final long n26 = n22 - xorRotr10;
                    final long xorRotr11 = ThreefishEngine.xorRotr(xorRotr8 - (n23 + n24), 37, n25);
                    final long n27 = n25 - xorRotr11;
                    final long xorRotr12 = ThreefishEngine.xorRotr(xorRotr11, 23, n26);
                    final long n28 = n26 - xorRotr12;
                    final long xorRotr13 = ThreefishEngine.xorRotr(xorRotr10, 40, n27);
                    final long n29 = n27 - xorRotr13;
                    final long xorRotr14 = ThreefishEngine.xorRotr(xorRotr13, 52, n28);
                    final long n30 = n28 - xorRotr14;
                    final long xorRotr15 = ThreefishEngine.xorRotr(xorRotr12, 57, n29);
                    final long n31 = n29 - xorRotr15;
                    final long xorRotr16 = ThreefishEngine.xorRotr(xorRotr15, 14, n30);
                    xorRotr = ThreefishEngine.xorRotr(xorRotr14, 16, n31);
                    n3 = n31 - xorRotr;
                    i -= 2;
                    n2 = xorRotr16;
                    n = n30 - xorRotr16;
                }
                final long n32 = kw[0];
                final long n33 = kw[1];
                final long n34 = t[0];
                final long n35 = kw[2];
                final long n36 = t[1];
                final long n37 = kw[3];
                array2[0] = n - n32;
                array2[1] = n2 - (n33 + n34);
                array2[2] = n3 - (n35 + n36);
                array2[3] = xorRotr - n37;
                return;
            }
            throw new IllegalArgumentException();
        }
        
        @Override
        void encryptBlock(final long[] array, final long[] array2) {
            final long[] kw = this.kw;
            final long[] t = this.t;
            final int[] access$000 = ThreefishEngine.MOD5;
            final int[] access$2 = ThreefishEngine.MOD3;
            if (kw.length != 9) {
                throw new IllegalArgumentException();
            }
            if (t.length == 5) {
                final long n = array[0];
                final long n2 = array[1];
                final long n3 = array[2];
                final long n4 = array[3];
                final long n5 = kw[0];
                final long n6 = kw[1];
                final long n7 = t[0];
                final long n8 = kw[2];
                final long n9 = t[1];
                long n10 = n4 + kw[3];
                long n11 = n3 + (n8 + n9);
                long n12 = n2 + (n6 + n7);
                long n13 = n + n5;
                int i = 1;
                final int[] array3 = access$2;
                while (i < 18) {
                    final int n14 = access$000[i];
                    final int n15 = array3[i];
                    final long n16 = n13 + n12;
                    final long rotlXor = ThreefishEngine.rotlXor(n12, 14, n16);
                    final long n17 = n11 + n10;
                    final long rotlXor2 = ThreefishEngine.rotlXor(n10, 16, n17);
                    final long n18 = n16 + rotlXor2;
                    final long rotlXor3 = ThreefishEngine.rotlXor(rotlXor2, 52, n18);
                    final long n19 = n17 + rotlXor;
                    final long rotlXor4 = ThreefishEngine.rotlXor(rotlXor, 57, n19);
                    final long n20 = n18 + rotlXor4;
                    final long rotlXor5 = ThreefishEngine.rotlXor(rotlXor4, 23, n20);
                    final long n21 = n19 + rotlXor3;
                    final long rotlXor6 = ThreefishEngine.rotlXor(rotlXor3, 40, n21);
                    final long n22 = n20 + rotlXor6;
                    final long rotlXor7 = ThreefishEngine.rotlXor(rotlXor6, 5, n22);
                    final long n23 = n21 + rotlXor5;
                    final long rotlXor8 = ThreefishEngine.rotlXor(rotlXor5, 37, n23);
                    final long n24 = kw[n14];
                    final int n25 = n14 + 1;
                    final long n26 = rotlXor8 + (kw[n25] + t[n15]);
                    final int n27 = n14 + 2;
                    final long n28 = kw[n27];
                    final int n29 = n15 + 1;
                    final long n30 = t[n29];
                    final int n31 = n14 + 3;
                    final long n32 = kw[n31];
                    final long n33 = i;
                    final long n34 = rotlXor7 + (n32 + n33);
                    final long n35 = n22 + n24 + n26;
                    final long rotlXor9 = ThreefishEngine.rotlXor(n26, 25, n35);
                    final long n36 = n23 + (n28 + n30) + n34;
                    final long rotlXor10 = ThreefishEngine.rotlXor(n34, 33, n36);
                    final long n37 = n35 + rotlXor10;
                    final long rotlXor11 = ThreefishEngine.rotlXor(rotlXor10, 46, n37);
                    final long n38 = n36 + rotlXor9;
                    final long rotlXor12 = ThreefishEngine.rotlXor(rotlXor9, 12, n38);
                    final long n39 = n37 + rotlXor12;
                    final long rotlXor13 = ThreefishEngine.rotlXor(rotlXor12, 58, n39);
                    final long n40 = n38 + rotlXor11;
                    final long rotlXor14 = ThreefishEngine.rotlXor(rotlXor11, 22, n40);
                    final long n41 = n39 + rotlXor14;
                    final long rotlXor15 = ThreefishEngine.rotlXor(rotlXor14, 32, n41);
                    final long n42 = n40 + rotlXor13;
                    final long rotlXor16 = ThreefishEngine.rotlXor(rotlXor13, 32, n42);
                    final long n43 = kw[n25];
                    n12 = kw[n27] + t[n29] + rotlXor16;
                    n11 = n42 + (kw[n31] + t[n15 + 2]);
                    n10 = rotlXor15 + (kw[n14 + 4] + n33 + 1L);
                    i += 2;
                    n13 = n41 + n43;
                }
                array2[0] = n13;
                array2[1] = n12;
                array2[2] = n11;
                array2[3] = n10;
                return;
            }
            throw new IllegalArgumentException();
        }
    }
    
    private static final class Threefish512Cipher extends ThreefishCipher
    {
        private static final int ROTATION_0_0 = 46;
        private static final int ROTATION_0_1 = 36;
        private static final int ROTATION_0_2 = 19;
        private static final int ROTATION_0_3 = 37;
        private static final int ROTATION_1_0 = 33;
        private static final int ROTATION_1_1 = 27;
        private static final int ROTATION_1_2 = 14;
        private static final int ROTATION_1_3 = 42;
        private static final int ROTATION_2_0 = 17;
        private static final int ROTATION_2_1 = 49;
        private static final int ROTATION_2_2 = 36;
        private static final int ROTATION_2_3 = 39;
        private static final int ROTATION_3_0 = 44;
        private static final int ROTATION_3_1 = 9;
        private static final int ROTATION_3_2 = 54;
        private static final int ROTATION_3_3 = 56;
        private static final int ROTATION_4_0 = 39;
        private static final int ROTATION_4_1 = 30;
        private static final int ROTATION_4_2 = 34;
        private static final int ROTATION_4_3 = 24;
        private static final int ROTATION_5_0 = 13;
        private static final int ROTATION_5_1 = 50;
        private static final int ROTATION_5_2 = 10;
        private static final int ROTATION_5_3 = 17;
        private static final int ROTATION_6_0 = 25;
        private static final int ROTATION_6_1 = 29;
        private static final int ROTATION_6_2 = 39;
        private static final int ROTATION_6_3 = 43;
        private static final int ROTATION_7_0 = 8;
        private static final int ROTATION_7_1 = 35;
        private static final int ROTATION_7_2 = 56;
        private static final int ROTATION_7_3 = 22;
        
        protected Threefish512Cipher(final long[] array, final long[] array2) {
            super(array, array2);
        }
        
        public void decryptBlock(long[] array, final long[] array2) {
            final long[] kw = this.kw;
            final long[] t = this.t;
            final int[] access$200 = ThreefishEngine.MOD9;
            final int[] access$201 = ThreefishEngine.MOD3;
            if (kw.length != 17) {
                throw new IllegalArgumentException();
            }
            if (t.length == 5) {
                long n = array[0];
                long xorRotr = array[1];
                long n2 = array[2];
                long xorRotr2 = array[3];
                long n3 = array[4];
                long xorRotr3 = array[5];
                long n4 = array[6];
                long xorRotr4 = array[7];
                int i = 17;
                array = t;
                while (i >= 1) {
                    final int n5 = access$200[i];
                    final int n6 = access$201[i];
                    final int n7 = n5 + 1;
                    final long n8 = n - kw[n7];
                    final int n9 = n5 + 2;
                    final long n10 = kw[n9];
                    final int n11 = n5 + 3;
                    final long n12 = n2 - kw[n11];
                    final int n13 = n5 + 4;
                    final long n14 = kw[n13];
                    final int n15 = n5 + 5;
                    final long n16 = n3 - kw[n15];
                    final int n17 = n5 + 6;
                    final long n18 = kw[n17];
                    final int n19 = n6 + 1;
                    final long n20 = array[n19];
                    final int n21 = n5 + 7;
                    final long n22 = n4 - (kw[n21] + array[n6 + 2]);
                    final long n23 = kw[n5 + 8];
                    final long n24 = i;
                    final long xorRotr5 = ThreefishEngine.xorRotr(xorRotr - n10, 8, n22);
                    final long n25 = n22 - xorRotr5;
                    final long xorRotr6 = ThreefishEngine.xorRotr(xorRotr4 - (n23 + n24 + 1L), 35, n8);
                    final long n26 = n8 - xorRotr6;
                    final long xorRotr7 = ThreefishEngine.xorRotr(xorRotr3 - (n18 + n20), 56, n12);
                    final long n27 = n12 - xorRotr7;
                    final long xorRotr8 = ThreefishEngine.xorRotr(xorRotr2 - n14, 22, n16);
                    final long n28 = n16 - xorRotr8;
                    final long xorRotr9 = ThreefishEngine.xorRotr(xorRotr5, 25, n28);
                    final long n29 = n28 - xorRotr9;
                    final long xorRotr10 = ThreefishEngine.xorRotr(xorRotr8, 29, n25);
                    final long n30 = n25 - xorRotr10;
                    final long xorRotr11 = ThreefishEngine.xorRotr(xorRotr7, 39, n26);
                    final long n31 = n26 - xorRotr11;
                    final long xorRotr12 = ThreefishEngine.xorRotr(xorRotr6, 43, n27);
                    final long n32 = n27 - xorRotr12;
                    final long xorRotr13 = ThreefishEngine.xorRotr(xorRotr9, 13, n32);
                    final long n33 = n32 - xorRotr13;
                    final long xorRotr14 = ThreefishEngine.xorRotr(xorRotr12, 50, n29);
                    final long n34 = n29 - xorRotr14;
                    final long xorRotr15 = ThreefishEngine.xorRotr(xorRotr11, 10, n30);
                    final long n35 = n30 - xorRotr15;
                    final long xorRotr16 = ThreefishEngine.xorRotr(xorRotr10, 17, n31);
                    final long n36 = n31 - xorRotr16;
                    final long xorRotr17 = ThreefishEngine.xorRotr(xorRotr13, 39, n36);
                    final long xorRotr18 = ThreefishEngine.xorRotr(xorRotr16, 30, n33);
                    final long xorRotr19 = ThreefishEngine.xorRotr(xorRotr15, 34, n34);
                    final long xorRotr20 = ThreefishEngine.xorRotr(xorRotr14, 24, n35);
                    final long n37 = n36 - xorRotr17 - kw[n5];
                    final long n38 = kw[n7];
                    final long n39 = n33 - xorRotr18 - kw[n9];
                    final long n40 = kw[n11];
                    final long n41 = n34 - xorRotr19 - kw[n13];
                    final long n42 = kw[n15];
                    final long n43 = array[n6];
                    final long n44 = n35 - xorRotr20 - (kw[n17] + array[n19]);
                    final long n45 = kw[n21];
                    final long xorRotr21 = ThreefishEngine.xorRotr(xorRotr17 - n38, 44, n44);
                    final long n46 = n44 - xorRotr21;
                    final long xorRotr22 = ThreefishEngine.xorRotr(xorRotr20 - (n45 + n24), 9, n37);
                    final long n47 = n37 - xorRotr22;
                    final long xorRotr23 = ThreefishEngine.xorRotr(xorRotr19 - (n42 + n43), 54, n39);
                    final long n48 = n39 - xorRotr23;
                    final long xorRotr24 = ThreefishEngine.xorRotr(xorRotr18 - n40, 56, n41);
                    final long n49 = n41 - xorRotr24;
                    final long xorRotr25 = ThreefishEngine.xorRotr(xorRotr21, 17, n49);
                    final long n50 = n49 - xorRotr25;
                    final long xorRotr26 = ThreefishEngine.xorRotr(xorRotr24, 49, n46);
                    final long n51 = n46 - xorRotr26;
                    final long xorRotr27 = ThreefishEngine.xorRotr(xorRotr23, 36, n47);
                    final long n52 = n47 - xorRotr27;
                    final long xorRotr28 = ThreefishEngine.xorRotr(xorRotr22, 39, n48);
                    final long n53 = n48 - xorRotr28;
                    final long xorRotr29 = ThreefishEngine.xorRotr(xorRotr25, 33, n53);
                    final long n54 = n53 - xorRotr29;
                    final long xorRotr30 = ThreefishEngine.xorRotr(xorRotr28, 27, n50);
                    final long n55 = n50 - xorRotr30;
                    final long xorRotr31 = ThreefishEngine.xorRotr(xorRotr27, 14, n51);
                    final long n56 = n51 - xorRotr31;
                    final long xorRotr32 = ThreefishEngine.xorRotr(xorRotr26, 42, n52);
                    final long n57 = n52 - xorRotr32;
                    xorRotr = ThreefishEngine.xorRotr(xorRotr29, 46, n57);
                    n = n57 - xorRotr;
                    xorRotr2 = ThreefishEngine.xorRotr(xorRotr32, 36, n54);
                    xorRotr3 = ThreefishEngine.xorRotr(xorRotr31, 19, n55);
                    n3 = n55 - xorRotr3;
                    xorRotr4 = ThreefishEngine.xorRotr(xorRotr30, 37, n56);
                    n4 = n56 - xorRotr4;
                    i -= 2;
                    n2 = n54 - xorRotr2;
                }
                final long n58 = kw[0];
                final long n59 = kw[1];
                final long n60 = kw[2];
                final long n61 = kw[3];
                final long n62 = kw[4];
                final long n63 = kw[5];
                final long n64 = array[0];
                final long n65 = kw[6];
                final long n66 = array[1];
                final long n67 = kw[7];
                array2[0] = n - n58;
                array2[1] = xorRotr - n59;
                array2[2] = n2 - n60;
                array2[3] = xorRotr2 - n61;
                array2[4] = n3 - n62;
                array2[5] = xorRotr3 - (n63 + n64);
                array2[6] = n4 - (n65 + n66);
                array2[7] = xorRotr4 - n67;
                return;
            }
            throw new IllegalArgumentException();
        }
        
        public void encryptBlock(long[] array, final long[] array2) {
            final long[] kw = this.kw;
            final long[] t = this.t;
            final int[] access$200 = ThreefishEngine.MOD9;
            final int[] access$201 = ThreefishEngine.MOD3;
            if (kw.length != 17) {
                throw new IllegalArgumentException();
            }
            if (t.length == 5) {
                final long n = array[0];
                final long n2 = array[1];
                final long n3 = array[2];
                final long n4 = array[3];
                final long n5 = array[4];
                final long n6 = array[5];
                final long n7 = array[6];
                final long n8 = array[7];
                final long n9 = kw[0];
                final long n10 = kw[1];
                final long n11 = kw[2];
                final long n12 = kw[3];
                final long n13 = kw[4];
                final long n14 = kw[5];
                final long n15 = t[0];
                long n16 = n7 + (kw[6] + t[1]);
                final long n17 = kw[7];
                long n18 = n4 + n12;
                long n19 = n8 + n17;
                long n20 = n3 + n11;
                long n21 = n2 + n10;
                long n22 = n + n9;
                int i = 1;
                long n23 = n6 + (n14 + n15);
                long n24 = n5 + n13;
                array = t;
                while (i < 18) {
                    final int n25 = access$200[i];
                    final int n26 = access$201[i];
                    final long n27 = n22 + n21;
                    final long rotlXor = ThreefishEngine.rotlXor(n21, 46, n27);
                    final long n28 = n20 + n18;
                    final long rotlXor2 = ThreefishEngine.rotlXor(n18, 36, n28);
                    final long n29 = n24 + n23;
                    final long rotlXor3 = ThreefishEngine.rotlXor(n23, 19, n29);
                    final long n30 = n16 + n19;
                    final long rotlXor4 = ThreefishEngine.rotlXor(n19, 37, n30);
                    final long n31 = n28 + rotlXor;
                    final long rotlXor5 = ThreefishEngine.rotlXor(rotlXor, 33, n31);
                    final long n32 = n29 + rotlXor4;
                    final long rotlXor6 = ThreefishEngine.rotlXor(rotlXor4, 27, n32);
                    final long n33 = n30 + rotlXor3;
                    final long rotlXor7 = ThreefishEngine.rotlXor(rotlXor3, 14, n33);
                    final long n34 = n27 + rotlXor2;
                    final long rotlXor8 = ThreefishEngine.rotlXor(rotlXor2, 42, n34);
                    final long n35 = n32 + rotlXor5;
                    final long rotlXor9 = ThreefishEngine.rotlXor(rotlXor5, 17, n35);
                    final long n36 = n33 + rotlXor8;
                    final long rotlXor10 = ThreefishEngine.rotlXor(rotlXor8, 49, n36);
                    final long n37 = n34 + rotlXor7;
                    final long rotlXor11 = ThreefishEngine.rotlXor(rotlXor7, 36, n37);
                    final long n38 = n31 + rotlXor6;
                    final long rotlXor12 = ThreefishEngine.rotlXor(rotlXor6, 39, n38);
                    final long n39 = n36 + rotlXor9;
                    final long rotlXor13 = ThreefishEngine.rotlXor(rotlXor9, 44, n39);
                    final long n40 = n37 + rotlXor12;
                    final long rotlXor14 = ThreefishEngine.rotlXor(rotlXor12, 9, n40);
                    final long n41 = n38 + rotlXor11;
                    final long rotlXor15 = ThreefishEngine.rotlXor(rotlXor11, 54, n41);
                    final long n42 = n35 + rotlXor10;
                    final long rotlXor16 = ThreefishEngine.rotlXor(rotlXor10, 56, n42);
                    final long n43 = kw[n25];
                    final int n44 = n25 + 1;
                    final long n45 = rotlXor13 + kw[n44];
                    final int n46 = n25 + 2;
                    final long n47 = kw[n46];
                    final int n48 = n25 + 3;
                    final long n49 = rotlXor16 + kw[n48];
                    final int n50 = n25 + 4;
                    final long n51 = kw[n50];
                    final int n52 = n25 + 5;
                    final long n53 = rotlXor15 + (kw[n52] + array[n26]);
                    final int n54 = n25 + 6;
                    final long n55 = kw[n54];
                    final int n56 = n26 + 1;
                    final long n57 = array[n56];
                    final int n58 = n25 + 7;
                    final long n59 = kw[n58];
                    final long n60 = i;
                    final long n61 = rotlXor14 + (n59 + n60);
                    final long n62 = n40 + n43 + n45;
                    final long rotlXor17 = ThreefishEngine.rotlXor(n45, 39, n62);
                    final long n63 = n41 + n47 + n49;
                    final long rotlXor18 = ThreefishEngine.rotlXor(n49, 30, n63);
                    final long n64 = n42 + n51 + n53;
                    final long rotlXor19 = ThreefishEngine.rotlXor(n53, 34, n64);
                    final long n65 = n39 + (n55 + n57) + n61;
                    final long rotlXor20 = ThreefishEngine.rotlXor(n61, 24, n65);
                    final long n66 = n63 + rotlXor17;
                    final long rotlXor21 = ThreefishEngine.rotlXor(rotlXor17, 13, n66);
                    final long n67 = n64 + rotlXor20;
                    final long rotlXor22 = ThreefishEngine.rotlXor(rotlXor20, 50, n67);
                    final long n68 = n65 + rotlXor19;
                    final long rotlXor23 = ThreefishEngine.rotlXor(rotlXor19, 10, n68);
                    final long n69 = n62 + rotlXor18;
                    final long rotlXor24 = ThreefishEngine.rotlXor(rotlXor18, 17, n69);
                    final long n70 = n67 + rotlXor21;
                    final long rotlXor25 = ThreefishEngine.rotlXor(rotlXor21, 25, n70);
                    final long n71 = n68 + rotlXor24;
                    final long rotlXor26 = ThreefishEngine.rotlXor(rotlXor24, 29, n71);
                    final long n72 = n69 + rotlXor23;
                    final long rotlXor27 = ThreefishEngine.rotlXor(rotlXor23, 39, n72);
                    final long n73 = n66 + rotlXor22;
                    final long rotlXor28 = ThreefishEngine.rotlXor(rotlXor22, 43, n73);
                    final long n74 = n71 + rotlXor25;
                    final long rotlXor29 = ThreefishEngine.rotlXor(rotlXor25, 8, n74);
                    final long n75 = n72 + rotlXor28;
                    final long rotlXor30 = ThreefishEngine.rotlXor(rotlXor28, 35, n75);
                    final long n76 = n73 + rotlXor27;
                    final long rotlXor31 = ThreefishEngine.rotlXor(rotlXor27, 56, n76);
                    final long n77 = n70 + rotlXor26;
                    final long rotlXor32 = ThreefishEngine.rotlXor(rotlXor26, 22, n77);
                    n22 = n75 + kw[n44];
                    final long n78 = kw[n46];
                    n20 = n76 + kw[n48];
                    n18 = rotlXor32 + kw[n50];
                    n24 = n77 + kw[n52];
                    n23 = rotlXor31 + (kw[n54] + array[n56]);
                    n16 = n74 + (kw[n58] + array[n26 + 2]);
                    n19 = rotlXor30 + (kw[n25 + 8] + n60 + 1L);
                    i += 2;
                    n21 = rotlXor29 + n78;
                }
                array2[0] = n22;
                array2[1] = n21;
                array2[2] = n20;
                array2[3] = n18;
                array2[4] = n24;
                array2[5] = n23;
                array2[6] = n16;
                array2[7] = n19;
                return;
            }
            throw new IllegalArgumentException();
        }
    }
    
    private abstract static class ThreefishCipher
    {
        protected final long[] kw;
        protected final long[] t;
        
        protected ThreefishCipher(final long[] kw, final long[] t) {
            this.kw = kw;
            this.t = t;
        }
        
        abstract void decryptBlock(final long[] p0, final long[] p1);
        
        abstract void encryptBlock(final long[] p0, final long[] p1);
    }
}
