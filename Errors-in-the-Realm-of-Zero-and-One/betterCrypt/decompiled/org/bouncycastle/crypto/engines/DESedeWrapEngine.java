// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.engines;

import org.bouncycastle.crypto.InvalidCipherTextException;
import java.security.SecureRandom;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.util.DigestFactory;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.Wrapper;

public class DESedeWrapEngine implements Wrapper
{
    private static final byte[] IV2;
    byte[] digest;
    private CBCBlockCipher engine;
    private boolean forWrapping;
    private byte[] iv;
    private KeyParameter param;
    private ParametersWithIV paramPlusIV;
    Digest sha1;
    
    static {
        IV2 = new byte[] { 74, -35, -94, 44, 121, -24, 33, 5 };
    }
    
    public DESedeWrapEngine() {
        this.sha1 = DigestFactory.createSHA1();
        this.digest = new byte[20];
    }
    
    private byte[] calculateCMSKeyChecksum(final byte[] array) {
        final byte[] array2 = new byte[8];
        this.sha1.update(array, 0, array.length);
        this.sha1.doFinal(this.digest, 0);
        System.arraycopy(this.digest, 0, array2, 0, 8);
        return array2;
    }
    
    private boolean checkCMSKeyChecksum(final byte[] array, final byte[] array2) {
        return Arrays.constantTimeAreEqual(this.calculateCMSKeyChecksum(array), array2);
    }
    
    private static byte[] reverse(final byte[] array) {
        final byte[] array2 = new byte[array.length];
        int n;
        for (int i = 0; i < array.length; i = n) {
            final int length = array.length;
            n = i + 1;
            array2[i] = array[length - n];
        }
        return array2;
    }
    
    @Override
    public String getAlgorithmName() {
        return "DESede";
    }
    
    @Override
    public void init(final boolean forWrapping, CipherParameters parameters) {
        this.forWrapping = forWrapping;
        this.engine = new CBCBlockCipher(new DESedeEngine());
        SecureRandom secureRandom;
        if (parameters instanceof ParametersWithRandom) {
            final ParametersWithRandom parametersWithRandom = (ParametersWithRandom)parameters;
            parameters = parametersWithRandom.getParameters();
            secureRandom = parametersWithRandom.getRandom();
        }
        else {
            secureRandom = CryptoServicesRegistrar.getSecureRandom();
        }
        if (parameters instanceof KeyParameter) {
            this.param = (KeyParameter)parameters;
            if (this.forWrapping) {
                secureRandom.nextBytes(this.iv = new byte[8]);
                this.paramPlusIV = new ParametersWithIV(this.param, this.iv);
            }
        }
        else if (parameters instanceof ParametersWithIV) {
            this.paramPlusIV = (ParametersWithIV)parameters;
            this.iv = this.paramPlusIV.getIV();
            this.param = (KeyParameter)this.paramPlusIV.getParameters();
            if (!this.forWrapping) {
                throw new IllegalArgumentException("You should not supply an IV for unwrapping");
            }
            final byte[] iv = this.iv;
            if (iv != null && iv.length == 8) {
                return;
            }
            throw new IllegalArgumentException("IV is not 8 octets");
        }
    }
    
    @Override
    public byte[] unwrap(byte[] reverse, int i, final int n) throws InvalidCipherTextException {
        if (this.forWrapping) {
            throw new IllegalStateException("Not set for unwrapping");
        }
        if (reverse == null) {
            throw new InvalidCipherTextException("Null pointer as ciphertext");
        }
        final int blockSize = this.engine.getBlockSize();
        if (n % blockSize != 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Ciphertext not multiple of ");
            sb.append(blockSize);
            throw new InvalidCipherTextException(sb.toString());
        }
        this.engine.init(false, new ParametersWithIV(this.param, DESedeWrapEngine.IV2));
        final byte[] array = new byte[n];
        for (int j = 0; j != n; j += blockSize) {
            this.engine.processBlock(reverse, i + j, array, j);
        }
        reverse = reverse(array);
        this.iv = new byte[8];
        final byte[] array2 = new byte[reverse.length - 8];
        System.arraycopy(reverse, 0, this.iv, 0, 8);
        System.arraycopy(reverse, 8, array2, 0, reverse.length - 8);
        this.paramPlusIV = new ParametersWithIV(this.param, this.iv);
        this.engine.init(false, this.paramPlusIV);
        for (reverse = new byte[array2.length], i = 0; i != reverse.length; i += blockSize) {
            this.engine.processBlock(array2, i, reverse, i);
        }
        final byte[] array3 = new byte[reverse.length - 8];
        final byte[] array4 = new byte[8];
        System.arraycopy(reverse, 0, array3, 0, reverse.length - 8);
        System.arraycopy(reverse, reverse.length - 8, array4, 0, 8);
        if (this.checkCMSKeyChecksum(array3, array4)) {
            return array3;
        }
        throw new InvalidCipherTextException("Checksum inside ciphertext is corrupted");
    }
    
    @Override
    public byte[] wrap(byte[] array, int i, int blockSize) {
        if (!this.forWrapping) {
            throw new IllegalStateException("Not initialized for wrapping");
        }
        final byte[] array2 = new byte[blockSize];
        final int n = 0;
        System.arraycopy(array, i, array2, 0, blockSize);
        final byte[] calculateCMSKeyChecksum = this.calculateCMSKeyChecksum(array2);
        array = new byte[array2.length + calculateCMSKeyChecksum.length];
        System.arraycopy(array2, 0, array, 0, array2.length);
        System.arraycopy(calculateCMSKeyChecksum, 0, array, array2.length, calculateCMSKeyChecksum.length);
        blockSize = this.engine.getBlockSize();
        if (array.length % blockSize == 0) {
            this.engine.init(true, this.paramPlusIV);
            final byte[] array3 = new byte[array.length];
            for (i = 0; i != array.length; i += blockSize) {
                this.engine.processBlock(array, i, array3, i);
            }
            array = this.iv;
            final byte[] array4 = new byte[array.length + array3.length];
            System.arraycopy(array, 0, array4, 0, array.length);
            System.arraycopy(array3, 0, array4, this.iv.length, array3.length);
            array = reverse(array4);
            this.engine.init(true, new ParametersWithIV(this.param, DESedeWrapEngine.IV2));
            for (i = n; i != array.length; i += blockSize) {
                this.engine.processBlock(array, i, array, i);
            }
            return array;
        }
        throw new IllegalStateException("Not multiple of block length");
    }
}
