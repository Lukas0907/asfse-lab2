// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.engines;

import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import java.util.ArrayList;
import org.bouncycastle.crypto.Wrapper;

public class DSTU7624WrapEngine implements Wrapper
{
    private static final int BYTES_IN_INTEGER = 4;
    private byte[] B;
    private ArrayList<byte[]> Btemp;
    private byte[] checkSumArray;
    private DSTU7624Engine engine;
    private boolean forWrapping;
    private byte[] intArray;
    private byte[] zeroArray;
    
    public DSTU7624WrapEngine(final int n) {
        this.engine = new DSTU7624Engine(n);
        this.B = new byte[this.engine.getBlockSize() / 2];
        this.checkSumArray = new byte[this.engine.getBlockSize()];
        this.zeroArray = new byte[this.engine.getBlockSize()];
        this.Btemp = new ArrayList<byte[]>();
        this.intArray = new byte[4];
    }
    
    private void intToBytes(final int n, final byte[] array, final int n2) {
        array[n2 + 3] = (byte)(n >> 24);
        array[n2 + 2] = (byte)(n >> 16);
        array[n2 + 1] = (byte)(n >> 8);
        array[n2] = (byte)n;
    }
    
    @Override
    public String getAlgorithmName() {
        return "DSTU7624WrapEngine";
    }
    
    @Override
    public void init(final boolean forWrapping, final CipherParameters cipherParameters) {
        CipherParameters parameters = cipherParameters;
        if (cipherParameters instanceof ParametersWithRandom) {
            parameters = ((ParametersWithRandom)cipherParameters).getParameters();
        }
        this.forWrapping = forWrapping;
        if (parameters instanceof KeyParameter) {
            this.engine.init(forWrapping, parameters);
            return;
        }
        throw new IllegalArgumentException("invalid parameters passed to DSTU7624WrapEngine");
    }
    
    @Override
    public byte[] unwrap(byte[] array, int i, int j) throws InvalidCipherTextException {
        if (this.forWrapping) {
            throw new IllegalStateException("not set for unwrapping");
        }
        if (j % this.engine.getBlockSize() != 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("unwrap data must be a multiple of ");
            sb.append(this.engine.getBlockSize());
            sb.append(" bytes");
            throw new DataLengthException(sb.toString());
        }
        final int n = j * 2 / this.engine.getBlockSize();
        final int n2 = n - 1;
        final int n3 = n2 * 6;
        final byte[] array2 = new byte[j];
        System.arraycopy(array, i, array2, 0, j);
        array = new byte[this.engine.getBlockSize() / 2];
        System.arraycopy(array2, 0, array, 0, this.engine.getBlockSize() / 2);
        this.Btemp.clear();
        byte[] e;
        for (j = array2.length - this.engine.getBlockSize() / 2, i = this.engine.getBlockSize() / 2; j != 0; j -= this.engine.getBlockSize() / 2, i += this.engine.getBlockSize() / 2) {
            e = new byte[this.engine.getBlockSize() / 2];
            System.arraycopy(array2, i, e, 0, this.engine.getBlockSize() / 2);
            this.Btemp.add(e);
        }
        int n4;
        ArrayList<byte[]> btemp;
        int index;
        for (i = 0; i < n3; ++i) {
            System.arraycopy(this.Btemp.get(n - 2), 0, array2, 0, this.engine.getBlockSize() / 2);
            System.arraycopy(array, 0, array2, this.engine.getBlockSize() / 2, this.engine.getBlockSize() / 2);
            this.intToBytes(n3 - i, this.intArray, 0);
            for (j = 0; j < 4; ++j) {
                n4 = this.engine.getBlockSize() / 2 + j;
                array2[n4] ^= this.intArray[j];
            }
            this.engine.processBlock(array2, 0, array2, 0);
            System.arraycopy(array2, 0, array, 0, this.engine.getBlockSize() / 2);
            for (j = 2; j < n; ++j) {
                btemp = this.Btemp;
                index = n - j;
                System.arraycopy(btemp.get(index - 1), 0, this.Btemp.get(index), 0, this.engine.getBlockSize() / 2);
            }
            System.arraycopy(array2, this.engine.getBlockSize() / 2, this.Btemp.get(0), 0, this.engine.getBlockSize() / 2);
        }
        System.arraycopy(array, 0, array2, 0, this.engine.getBlockSize() / 2);
        j = this.engine.getBlockSize() / 2;
        for (i = 0; i < n2; ++i) {
            System.arraycopy(this.Btemp.get(i), 0, array2, j, this.engine.getBlockSize() / 2);
            j += this.engine.getBlockSize() / 2;
        }
        System.arraycopy(array2, array2.length - this.engine.getBlockSize(), this.checkSumArray, 0, this.engine.getBlockSize());
        array = new byte[array2.length - this.engine.getBlockSize()];
        if (Arrays.areEqual(this.checkSumArray, this.zeroArray)) {
            System.arraycopy(array2, 0, array, 0, array2.length - this.engine.getBlockSize());
            return array;
        }
        throw new InvalidCipherTextException("checksum failed");
    }
    
    @Override
    public byte[] wrap(byte[] e, int i, int j) {
        if (!this.forWrapping) {
            throw new IllegalStateException("not set for wrapping");
        }
        if (j % this.engine.getBlockSize() != 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("wrap data must be a multiple of ");
            sb.append(this.engine.getBlockSize());
            sb.append(" bytes");
            throw new DataLengthException(sb.toString());
        }
        if (i + j <= e.length) {
            final int n = (j / this.engine.getBlockSize() + 1) * 2;
            final int n2 = n - 1;
            final byte[] array = new byte[this.engine.getBlockSize() + j];
            System.arraycopy(e, i, array, 0, j);
            System.arraycopy(array, 0, this.B, 0, this.engine.getBlockSize() / 2);
            this.Btemp.clear();
            for (j = array.length - this.engine.getBlockSize() / 2, i = this.engine.getBlockSize() / 2; j != 0; j -= this.engine.getBlockSize() / 2, i += this.engine.getBlockSize() / 2) {
                e = new byte[this.engine.getBlockSize() / 2];
                System.arraycopy(array, i, e, 0, this.engine.getBlockSize() / 2);
                this.Btemp.add(e);
            }
            int n3;
            for (i = 0; i < n2 * 6; i = j) {
                System.arraycopy(this.B, 0, array, 0, this.engine.getBlockSize() / 2);
                System.arraycopy(this.Btemp.get(0), 0, array, this.engine.getBlockSize() / 2, this.engine.getBlockSize() / 2);
                this.engine.processBlock(array, 0, array, 0);
                j = i + 1;
                this.intToBytes(j, this.intArray, 0);
                for (i = 0; i < 4; ++i) {
                    n3 = this.engine.getBlockSize() / 2 + i;
                    array[n3] ^= this.intArray[i];
                }
                System.arraycopy(array, this.engine.getBlockSize() / 2, this.B, 0, this.engine.getBlockSize() / 2);
                for (i = 2; i < n; ++i) {
                    System.arraycopy(this.Btemp.get(i - 1), 0, this.Btemp.get(i - 2), 0, this.engine.getBlockSize() / 2);
                }
                System.arraycopy(array, 0, this.Btemp.get(n - 2), 0, this.engine.getBlockSize() / 2);
            }
            System.arraycopy(this.B, 0, array, 0, this.engine.getBlockSize() / 2);
            j = this.engine.getBlockSize() / 2;
            for (i = 0; i < n2; ++i) {
                System.arraycopy(this.Btemp.get(i), 0, array, j, this.engine.getBlockSize() / 2);
                j += this.engine.getBlockSize() / 2;
            }
            return array;
        }
        throw new DataLengthException("input buffer too short");
    }
}
