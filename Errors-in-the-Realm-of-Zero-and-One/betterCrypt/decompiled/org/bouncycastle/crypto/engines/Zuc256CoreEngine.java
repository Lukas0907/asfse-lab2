// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.engines;

import org.bouncycastle.util.Memoable;

public class Zuc256CoreEngine extends Zuc128CoreEngine
{
    private static final byte[] EK_d;
    private static final byte[] EK_d128;
    private static final byte[] EK_d32;
    private static final byte[] EK_d64;
    private byte[] theD;
    
    static {
        EK_d = new byte[] { 34, 47, 36, 42, 109, 64, 64, 64, 64, 64, 64, 64, 64, 82, 16, 48 };
        EK_d32 = new byte[] { 34, 47, 37, 42, 109, 64, 64, 64, 64, 64, 64, 64, 64, 82, 16, 48 };
        EK_d64 = new byte[] { 35, 47, 36, 42, 109, 64, 64, 64, 64, 64, 64, 64, 64, 82, 16, 48 };
        EK_d128 = new byte[] { 35, 47, 37, 42, 109, 64, 64, 64, 64, 64, 64, 64, 64, 82, 16, 48 };
    }
    
    protected Zuc256CoreEngine() {
        this.theD = Zuc256CoreEngine.EK_d;
    }
    
    protected Zuc256CoreEngine(final int i) {
        if (i == 32) {
            this.theD = Zuc256CoreEngine.EK_d32;
            return;
        }
        if (i == 64) {
            this.theD = Zuc256CoreEngine.EK_d64;
            return;
        }
        if (i == 128) {
            this.theD = Zuc256CoreEngine.EK_d128;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unsupported length: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    protected Zuc256CoreEngine(final Zuc256CoreEngine zuc256CoreEngine) {
        super(zuc256CoreEngine);
    }
    
    private static int MAKEU31(final byte b, final byte b2, final byte b3, final byte b4) {
        return (b & 0xFF) << 23 | (b2 & 0xFF) << 16 | (b3 & 0xFF) << 8 | (b4 & 0xFF);
    }
    
    @Override
    public Memoable copy() {
        return new Zuc256CoreEngine(this);
    }
    
    @Override
    public String getAlgorithmName() {
        return "Zuc-256";
    }
    
    @Override
    protected int getMaxIterations() {
        return 625;
    }
    
    @Override
    public void reset(final Memoable memoable) {
        final Zuc256CoreEngine zuc256CoreEngine = (Zuc256CoreEngine)memoable;
        super.reset(memoable);
        this.theD = zuc256CoreEngine.theD;
    }
    
    @Override
    protected void setKeyAndIV(final int[] array, final byte[] array2, final byte[] array3) {
        if (array2 == null || array2.length != 32) {
            throw new IllegalArgumentException("A key of 32 bytes is needed");
        }
        if (array3 != null && array3.length == 25) {
            array[0] = MAKEU31(array2[0], this.theD[0], array2[21], array2[16]);
            array[1] = MAKEU31(array2[1], this.theD[1], array2[22], array2[17]);
            array[2] = MAKEU31(array2[2], this.theD[2], array2[23], array2[18]);
            array[3] = MAKEU31(array2[3], this.theD[3], array2[24], array2[19]);
            array[4] = MAKEU31(array2[4], this.theD[4], array2[25], array2[20]);
            array[5] = MAKEU31(array3[0], (byte)(this.theD[5] | (array3[17] & 0x3F)), array2[5], array2[26]);
            array[6] = MAKEU31(array3[1], (byte)(this.theD[6] | (array3[18] & 0x3F)), array2[6], array2[27]);
            array[7] = MAKEU31(array3[10], (byte)(this.theD[7] | (array3[19] & 0x3F)), array2[7], array3[2]);
            array[8] = MAKEU31(array2[8], (byte)(this.theD[8] | (array3[20] & 0x3F)), array3[3], array3[11]);
            array[9] = MAKEU31(array2[9], (byte)(this.theD[9] | (array3[21] & 0x3F)), array3[12], array3[4]);
            array[10] = MAKEU31(array3[5], (byte)(this.theD[10] | (array3[22] & 0x3F)), array2[10], array2[28]);
            array[11] = MAKEU31(array2[11], (byte)(this.theD[11] | (array3[23] & 0x3F)), array3[6], array3[13]);
            array[12] = MAKEU31(array2[12], (byte)(this.theD[12] | (array3[24] & 0x3F)), array3[7], array3[14]);
            array[13] = MAKEU31(array2[13], this.theD[13], array3[15], array3[8]);
            array[14] = MAKEU31(array2[14], (byte)(this.theD[14] | (array2[31] >>> 4 & 0xF)), array3[16], array3[9]);
            array[15] = MAKEU31(array2[15], (byte)(this.theD[15] | (array2[31] & 0xF)), array2[30], array2[29]);
            return;
        }
        throw new IllegalArgumentException("An IV of 25 bytes is needed");
    }
}
