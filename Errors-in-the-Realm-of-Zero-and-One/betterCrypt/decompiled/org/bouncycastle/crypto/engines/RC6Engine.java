// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.engines;

import org.bouncycastle.crypto.OutputLengthException;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.BlockCipher;

public class RC6Engine implements BlockCipher
{
    private static final int LGW = 5;
    private static final int P32 = -1209970333;
    private static final int Q32 = -1640531527;
    private static final int _noRounds = 20;
    private static final int bytesPerWord = 4;
    private static final int wordSize = 32;
    private int[] _S;
    private boolean forEncryption;
    
    public RC6Engine() {
        this._S = null;
    }
    
    private int bytesToWord(final byte[] array, final int n) {
        int n2 = 0;
        for (int i = 3; i >= 0; --i) {
            n2 = (n2 << 8) + (array[i + n] & 0xFF);
        }
        return n2;
    }
    
    private int decryptBlock(final byte[] array, int n, final byte[] array2, final int n2) {
        final int bytesToWord = this.bytesToWord(array, n);
        int bytesToWord2 = this.bytesToWord(array, n + 4);
        final int bytesToWord3 = this.bytesToWord(array, n + 8);
        int bytesToWord4 = this.bytesToWord(array, n + 12);
        final int[] s = this._S;
        n = bytesToWord3 - s[43];
        int n3 = bytesToWord - s[42];
        int rotateLeft;
        int rotateLeft2;
        int rotateRight;
        int rotateRight2;
        int n5;
        for (int i = 20; i >= 1; --i, n5 = (rotateRight2 ^ rotateLeft), bytesToWord4 = n, n = (rotateRight ^ rotateLeft2), bytesToWord2 = n3, n3 = n5) {
            rotateLeft = this.rotateLeft((n3 * 2 + 1) * n3, 5);
            rotateLeft2 = this.rotateLeft((n * 2 + 1) * n, 5);
            final int[] s2 = this._S;
            final int n4 = i * 2;
            rotateRight = this.rotateRight(bytesToWord2 - s2[n4 + 1], rotateLeft);
            rotateRight2 = this.rotateRight(bytesToWord4 - this._S[n4], rotateLeft2);
        }
        final int[] s3 = this._S;
        final int n6 = s3[1];
        final int n7 = s3[0];
        this.wordToBytes(n3, array2, n2);
        this.wordToBytes(bytesToWord2 - n7, array2, n2 + 4);
        this.wordToBytes(n, array2, n2 + 8);
        this.wordToBytes(bytesToWord4 - n6, array2, n2 + 12);
        return 16;
    }
    
    private int encryptBlock(final byte[] array, int bytesToWord, final byte[] array2, final int n) {
        int bytesToWord2 = this.bytesToWord(array, bytesToWord);
        final int bytesToWord3 = this.bytesToWord(array, bytesToWord + 4);
        int bytesToWord4 = this.bytesToWord(array, bytesToWord + 8);
        bytesToWord = this.bytesToWord(array, bytesToWord + 12);
        final int[] s = this._S;
        int n2 = bytesToWord3 + s[0];
        bytesToWord += s[1];
        int rotateLeft3;
        int n4;
        int rotateLeft4;
        int n5;
        int n6;
        int n7;
        for (int i = 1; i <= 20; ++i, n6 = rotateLeft3 + n4, n7 = rotateLeft4 + n5, bytesToWord2 = n2, n2 = n7, bytesToWord4 = bytesToWord, bytesToWord = n6) {
            final int rotateLeft = this.rotateLeft((n2 * 2 + 1) * n2, 5);
            final int rotateLeft2 = this.rotateLeft((bytesToWord * 2 + 1) * bytesToWord, 5);
            rotateLeft3 = this.rotateLeft(bytesToWord2 ^ rotateLeft, rotateLeft2);
            final int[] s2 = this._S;
            final int n3 = i * 2;
            n4 = s2[n3];
            rotateLeft4 = this.rotateLeft(bytesToWord4 ^ rotateLeft2, rotateLeft);
            n5 = this._S[n3 + 1];
        }
        final int[] s3 = this._S;
        final int n8 = s3[42];
        final int n9 = s3[43];
        this.wordToBytes(bytesToWord2 + n8, array2, n);
        this.wordToBytes(n2, array2, n + 4);
        this.wordToBytes(bytesToWord4 + n9, array2, n + 8);
        this.wordToBytes(bytesToWord, array2, n + 12);
        return 16;
    }
    
    private int rotateLeft(final int n, final int n2) {
        return n >>> -n2 | n << n2;
    }
    
    private int rotateRight(final int n, final int n2) {
        return n << -n2 | n >>> n2;
    }
    
    private void setKey(final byte[] array) {
        final int n = (array.length + 3) / 4;
        final int[] array2 = new int[(array.length + 4 - 1) / 4];
        for (int i = array.length - 1; i >= 0; --i) {
            final int n2 = i / 4;
            array2[n2] = (array2[n2] << 8) + (array[i] & 0xFF);
        }
        this._S = new int[44];
        final int[] s = this._S;
        final int n3 = 0;
        s[0] = -1209970333;
        int n4 = 1;
        int[] s2;
        while (true) {
            s2 = this._S;
            if (n4 >= s2.length) {
                break;
            }
            s2[n4] = s2[n4 - 1] - 1640531527;
            ++n4;
        }
        int n5;
        if (array2.length > s2.length) {
            n5 = array2.length;
        }
        else {
            n5 = s2.length;
        }
        final int n7;
        int n6 = n7 = 0;
        int n8;
        int rotateLeft = n8 = n7;
        int rotateLeft2 = n7;
        for (int j = n3; j < n5 * 3; ++j) {
            final int[] s3 = this._S;
            rotateLeft2 = this.rotateLeft(s3[n6] + rotateLeft2 + rotateLeft, 3);
            s3[n6] = rotateLeft2;
            rotateLeft = this.rotateLeft(array2[n8] + rotateLeft2 + rotateLeft, rotateLeft + rotateLeft2);
            array2[n8] = rotateLeft;
            n6 = (n6 + 1) % this._S.length;
            n8 = (n8 + 1) % array2.length;
        }
    }
    
    private void wordToBytes(int i, final byte[] array, final int n) {
        final int n2 = 0;
        int n3 = i;
        for (i = n2; i < 4; ++i) {
            array[i + n] = (byte)n3;
            n3 >>>= 8;
        }
    }
    
    @Override
    public String getAlgorithmName() {
        return "RC6";
    }
    
    @Override
    public int getBlockSize() {
        return 16;
    }
    
    @Override
    public void init(final boolean forEncryption, final CipherParameters cipherParameters) {
        if (cipherParameters instanceof KeyParameter) {
            final KeyParameter keyParameter = (KeyParameter)cipherParameters;
            this.forEncryption = forEncryption;
            this.setKey(keyParameter.getKey());
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("invalid parameter passed to RC6 init - ");
        sb.append(cipherParameters.getClass().getName());
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public int processBlock(final byte[] array, final int n, final byte[] array2, final int n2) {
        final int blockSize = this.getBlockSize();
        if (this._S == null) {
            throw new IllegalStateException("RC6 engine not initialised");
        }
        if (n + blockSize > array.length) {
            throw new DataLengthException("input buffer too short");
        }
        if (blockSize + n2 > array2.length) {
            throw new OutputLengthException("output buffer too short");
        }
        if (this.forEncryption) {
            return this.encryptBlock(array, n, array2, n2);
        }
        return this.decryptBlock(array, n, array2, n2);
    }
    
    @Override
    public void reset() {
    }
}
