// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.engines;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.BlockCipher;
import java.security.SecureRandom;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.Wrapper;

public class RFC3211WrapEngine implements Wrapper
{
    private CBCBlockCipher engine;
    private boolean forWrapping;
    private ParametersWithIV param;
    private SecureRandom rand;
    
    public RFC3211WrapEngine(final BlockCipher blockCipher) {
        this.engine = new CBCBlockCipher(blockCipher);
    }
    
    @Override
    public String getAlgorithmName() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.engine.getUnderlyingCipher().getAlgorithmName());
        sb.append("/RFC3211Wrap");
        return sb.toString();
    }
    
    @Override
    public void init(final boolean forWrapping, final CipherParameters cipherParameters) {
        this.forWrapping = forWrapping;
        if (cipherParameters instanceof ParametersWithRandom) {
            final ParametersWithRandom parametersWithRandom = (ParametersWithRandom)cipherParameters;
            this.rand = parametersWithRandom.getRandom();
            if (parametersWithRandom.getParameters() instanceof ParametersWithIV) {
                this.param = (ParametersWithIV)parametersWithRandom.getParameters();
                return;
            }
            throw new IllegalArgumentException("RFC3211Wrap requires an IV");
        }
        else {
            if (forWrapping) {
                this.rand = CryptoServicesRegistrar.getSecureRandom();
            }
            if (cipherParameters instanceof ParametersWithIV) {
                this.param = (ParametersWithIV)cipherParameters;
                return;
            }
            throw new IllegalArgumentException("RFC3211Wrap requires an IV");
        }
    }
    
    @Override
    public byte[] unwrap(byte[] array, int i, int length) throws InvalidCipherTextException {
        if (this.forWrapping) {
            throw new IllegalStateException("not set for unwrapping");
        }
        final int blockSize = this.engine.getBlockSize();
        if (length < blockSize * 2) {
            throw new InvalidCipherTextException("input too short");
        }
        final byte[] array2 = new byte[length];
        final byte[] array3 = new byte[blockSize];
        System.arraycopy(array, i, array2, 0, length);
        System.arraycopy(array, i, array3, 0, array3.length);
        this.engine.init(false, new ParametersWithIV(this.param.getParameters(), array3));
        for (i = blockSize; i < array2.length; i += blockSize) {
            this.engine.processBlock(array2, i, array2, i);
        }
        System.arraycopy(array2, array2.length - array3.length, array3, 0, array3.length);
        this.engine.init(false, new ParametersWithIV(this.param.getParameters(), array3));
        this.engine.processBlock(array2, 0, array2, 0);
        this.engine.init(false, this.param);
        for (i = 0; i < array2.length; i += blockSize) {
            this.engine.processBlock(array2, i, array2, i);
        }
        i = array2[0];
        length = array2.length;
        final int n = 1;
        if ((i & 0xFF) > length - 4) {
            i = 1;
        }
        else {
            i = 0;
        }
        if (i != 0) {
            length = array2.length - 4;
        }
        else {
            length = (array2[0] & 0xFF);
        }
        array = new byte[length];
        System.arraycopy(array2, 4, array, 0, array.length);
        int n2;
        for (int j = length = 0; j != 3; j = n2) {
            n2 = j + 1;
            length |= (array2[j + 4] ^ array2[n2]);
        }
        Arrays.clear(array2);
        if (length != 0) {
            length = n;
        }
        else {
            length = 0;
        }
        if ((i | length) == 0x0) {
            return array;
        }
        throw new InvalidCipherTextException("wrapped key corrupted");
    }
    
    @Override
    public byte[] wrap(byte[] bytes, int n, int i) {
        if (!this.forWrapping) {
            throw new IllegalStateException("not set for wrapping");
        }
        if (i <= 255 && i >= 0) {
            this.engine.init(true, this.param);
            final int blockSize = this.engine.getBlockSize();
            final int n2 = i + 4;
            int n3 = blockSize * 2;
            if (n2 >= n3) {
                if (n2 % blockSize == 0) {
                    n3 = n2;
                }
                else {
                    n3 = (n2 / blockSize + 1) * blockSize;
                }
            }
            final byte[] array = new byte[n3];
            final byte b = (byte)i;
            final int n4 = 0;
            array[0] = b;
            System.arraycopy(bytes, n, array, 4, i);
            bytes = new byte[array.length - n2];
            this.rand.nextBytes(bytes);
            System.arraycopy(bytes, 0, array, n2, bytes.length);
            array[1] = array[4];
            array[2] = array[5];
            array[3] = array[6];
            n = 0;
            while (true) {
                i = n4;
                if (n >= array.length) {
                    break;
                }
                this.engine.processBlock(array, n, array, n);
                n += blockSize;
            }
            while (i < array.length) {
                this.engine.processBlock(array, i, array, i);
                i += blockSize;
            }
            return array;
        }
        throw new IllegalArgumentException("input must be from 0 to 255 bytes");
    }
}
