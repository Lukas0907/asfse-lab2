// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.engines;

import org.bouncycastle.crypto.EphemeralKeyPair;
import org.bouncycastle.crypto.DerivationParameters;
import org.bouncycastle.crypto.params.KDFParameters;
import org.bouncycastle.util.BigIntegers;
import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.util.Pack;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.IESWithCipherParameters;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.IESParameters;
import org.bouncycastle.crypto.Mac;
import org.bouncycastle.crypto.KeyParser;
import org.bouncycastle.crypto.generators.EphemeralKeyPairGenerator;
import org.bouncycastle.crypto.DerivationFunction;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.BasicAgreement;

public class IESEngine
{
    private byte[] IV;
    byte[] V;
    BasicAgreement agree;
    BufferedBlockCipher cipher;
    boolean forEncryption;
    DerivationFunction kdf;
    private EphemeralKeyPairGenerator keyPairGenerator;
    private KeyParser keyParser;
    Mac mac;
    byte[] macBuf;
    IESParameters param;
    CipherParameters privParam;
    CipherParameters pubParam;
    
    public IESEngine(final BasicAgreement agree, final DerivationFunction kdf, final Mac mac) {
        this.agree = agree;
        this.kdf = kdf;
        this.mac = mac;
        this.macBuf = new byte[mac.getMacSize()];
        this.cipher = null;
    }
    
    public IESEngine(final BasicAgreement agree, final DerivationFunction kdf, final Mac mac, final BufferedBlockCipher cipher) {
        this.agree = agree;
        this.kdf = kdf;
        this.mac = mac;
        this.macBuf = new byte[mac.getMacSize()];
        this.cipher = cipher;
    }
    
    private byte[] decryptBlock(final byte[] array, final int n, final int n2) throws InvalidCipherTextException {
        if (n2 < this.V.length + this.mac.getMacSize()) {
            throw new InvalidCipherTextException("Length of input must be greater than the MAC and V combined");
        }
        byte[] array3;
        byte[] array5;
        int processBytes;
        if (this.cipher == null) {
            final byte[] array2 = new byte[n2 - this.V.length - this.mac.getMacSize()];
            array3 = new byte[this.param.getMacKeySize() / 8];
            final byte[] array4 = new byte[array2.length + array3.length];
            this.kdf.generateBytes(array4, 0, array4.length);
            if (this.V.length != 0) {
                System.arraycopy(array4, 0, array3, 0, array3.length);
                System.arraycopy(array4, array3.length, array2, 0, array2.length);
            }
            else {
                System.arraycopy(array4, 0, array2, 0, array2.length);
                System.arraycopy(array4, array2.length, array3, 0, array3.length);
            }
            array5 = new byte[array2.length];
            for (int i = 0; i != array2.length; ++i) {
                array5[i] = (byte)(array[this.V.length + n + i] ^ array2[i]);
            }
            processBytes = 0;
        }
        else {
            final byte[] array6 = new byte[((IESWithCipherParameters)this.param).getCipherKeySize() / 8];
            array3 = new byte[this.param.getMacKeySize() / 8];
            final byte[] array7 = new byte[array6.length + array3.length];
            this.kdf.generateBytes(array7, 0, array7.length);
            System.arraycopy(array7, 0, array6, 0, array6.length);
            System.arraycopy(array7, array6.length, array3, 0, array3.length);
            final KeyParameter keyParameter = new KeyParameter(array6);
            final byte[] iv = this.IV;
            CipherParameters cipherParameters = keyParameter;
            if (iv != null) {
                cipherParameters = new ParametersWithIV(keyParameter, iv);
            }
            this.cipher.init(false, cipherParameters);
            array5 = new byte[this.cipher.getOutputSize(n2 - this.V.length - this.mac.getMacSize())];
            final BufferedBlockCipher cipher = this.cipher;
            final byte[] v = this.V;
            processBytes = cipher.processBytes(array, v.length + n, n2 - v.length - this.mac.getMacSize(), array5, 0);
        }
        final byte[] encodingV = this.param.getEncodingV();
        byte[] lengthTag = null;
        if (this.V.length != 0) {
            lengthTag = this.getLengthTag(encodingV);
        }
        final int n3 = n + n2;
        final byte[] copyOfRange = Arrays.copyOfRange(array, n3 - this.mac.getMacSize(), n3);
        final byte[] array8 = new byte[copyOfRange.length];
        this.mac.init(new KeyParameter(array3));
        final Mac mac = this.mac;
        final byte[] v2 = this.V;
        mac.update(array, n + v2.length, n2 - v2.length - array8.length);
        if (encodingV != null) {
            this.mac.update(encodingV, 0, encodingV.length);
        }
        if (this.V.length != 0) {
            this.mac.update(lengthTag, 0, lengthTag.length);
        }
        this.mac.doFinal(array8, 0);
        if (!Arrays.constantTimeAreEqual(copyOfRange, array8)) {
            throw new InvalidCipherTextException("invalid MAC");
        }
        final BufferedBlockCipher cipher2 = this.cipher;
        if (cipher2 == null) {
            return array5;
        }
        return Arrays.copyOfRange(array5, 0, processBytes + cipher2.doFinal(array5, processBytes));
    }
    
    private byte[] encryptBlock(byte[] array, int processBytes, int n) throws InvalidCipherTextException {
        byte[] array3;
        if (this.cipher == null) {
            final byte[] array2 = new byte[n];
            array3 = new byte[this.param.getMacKeySize() / 8];
            final byte[] array4 = new byte[array2.length + array3.length];
            this.kdf.generateBytes(array4, 0, array4.length);
            if (this.V.length != 0) {
                System.arraycopy(array4, 0, array3, 0, array3.length);
                System.arraycopy(array4, array3.length, array2, 0, array2.length);
            }
            else {
                System.arraycopy(array4, 0, array2, 0, array2.length);
                System.arraycopy(array4, n, array3, 0, array3.length);
            }
            final byte[] array5 = new byte[n];
            for (int i = 0; i != n; ++i) {
                array5[i] = (byte)(array[processBytes + i] ^ array2[i]);
            }
            array = array5;
        }
        else {
            final byte[] array6 = new byte[((IESWithCipherParameters)this.param).getCipherKeySize() / 8];
            final byte[] array7 = new byte[this.param.getMacKeySize() / 8];
            final byte[] array8 = new byte[array6.length + array7.length];
            this.kdf.generateBytes(array8, 0, array8.length);
            System.arraycopy(array8, 0, array6, 0, array6.length);
            System.arraycopy(array8, array6.length, array7, 0, array7.length);
            BufferedBlockCipher bufferedBlockCipher;
            CipherParameters cipherParameters;
            if (this.IV != null) {
                bufferedBlockCipher = this.cipher;
                cipherParameters = new ParametersWithIV(new KeyParameter(array6), this.IV);
            }
            else {
                bufferedBlockCipher = this.cipher;
                cipherParameters = new KeyParameter(array6);
            }
            bufferedBlockCipher.init(true, cipherParameters);
            final byte[] array9 = new byte[this.cipher.getOutputSize(n)];
            processBytes = this.cipher.processBytes(array, processBytes, n, array9, 0);
            n = processBytes + this.cipher.doFinal(array9, processBytes);
            array3 = array7;
            array = array9;
        }
        final byte[] encodingV = this.param.getEncodingV();
        byte[] lengthTag = null;
        if (this.V.length != 0) {
            lengthTag = this.getLengthTag(encodingV);
        }
        final byte[] array10 = new byte[this.mac.getMacSize()];
        this.mac.init(new KeyParameter(array3));
        this.mac.update(array, 0, array.length);
        if (encodingV != null) {
            this.mac.update(encodingV, 0, encodingV.length);
        }
        if (this.V.length != 0) {
            this.mac.update(lengthTag, 0, lengthTag.length);
        }
        this.mac.doFinal(array10, 0);
        final byte[] v = this.V;
        final byte[] array11 = new byte[v.length + n + array10.length];
        System.arraycopy(v, 0, array11, 0, v.length);
        System.arraycopy(array, 0, array11, this.V.length, n);
        System.arraycopy(array10, 0, array11, this.V.length + n, array10.length);
        return array11;
    }
    
    private void extractParams(CipherParameters parameters) {
        if (parameters instanceof ParametersWithIV) {
            final ParametersWithIV parametersWithIV = (ParametersWithIV)parameters;
            this.IV = parametersWithIV.getIV();
            parameters = parametersWithIV.getParameters();
        }
        else {
            this.IV = null;
        }
        this.param = (IESParameters)parameters;
    }
    
    public BufferedBlockCipher getCipher() {
        return this.cipher;
    }
    
    protected byte[] getLengthTag(final byte[] array) {
        final byte[] array2 = new byte[8];
        if (array != null) {
            Pack.longToBigEndian(array.length * 8L, array2, 0);
        }
        return array2;
    }
    
    public Mac getMac() {
        return this.mac;
    }
    
    public void init(final AsymmetricKeyParameter privParam, final CipherParameters cipherParameters, final KeyParser keyParser) {
        this.forEncryption = false;
        this.privParam = privParam;
        this.keyParser = keyParser;
        this.extractParams(cipherParameters);
    }
    
    public void init(final AsymmetricKeyParameter pubParam, final CipherParameters cipherParameters, final EphemeralKeyPairGenerator keyPairGenerator) {
        this.forEncryption = true;
        this.pubParam = pubParam;
        this.keyPairGenerator = keyPairGenerator;
        this.extractParams(cipherParameters);
    }
    
    public void init(final boolean forEncryption, final CipherParameters privParam, final CipherParameters pubParam, final CipherParameters cipherParameters) {
        this.forEncryption = forEncryption;
        this.privParam = privParam;
        this.pubParam = pubParam;
        this.V = new byte[0];
        this.extractParams(cipherParameters);
    }
    
    public byte[] processBlock(byte[] buf, final int offset, final int length) throws InvalidCipherTextException {
        if (this.forEncryption) {
            final EphemeralKeyPairGenerator keyPairGenerator = this.keyPairGenerator;
            if (keyPairGenerator != null) {
                final EphemeralKeyPair generate = keyPairGenerator.generate();
                this.privParam = generate.getKeyPair().getPrivate();
                this.V = generate.getEncodedPublicKey();
            }
        }
        else if (this.keyParser != null) {
            final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buf, offset, length);
            try {
                this.pubParam = this.keyParser.readKey(byteArrayInputStream);
                this.V = Arrays.copyOfRange(buf, offset, length - byteArrayInputStream.available() + offset);
            }
            catch (IllegalArgumentException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("unable to recover ephemeral public key: ");
                sb.append(ex.getMessage());
                throw new InvalidCipherTextException(sb.toString(), ex);
            }
            catch (IOException ex2) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("unable to recover ephemeral public key: ");
                sb2.append(ex2.getMessage());
                throw new InvalidCipherTextException(sb2.toString(), ex2);
            }
        }
        this.agree.init(this.privParam);
        final byte[] unsignedByteArray = BigIntegers.asUnsignedByteArray(this.agree.getFieldSize(), this.agree.calculateAgreement(this.pubParam));
        final byte[] v = this.V;
        byte[] concatenate = unsignedByteArray;
        if (v.length != 0) {
            concatenate = Arrays.concatenate(v, unsignedByteArray);
            Arrays.fill(unsignedByteArray, (byte)0);
        }
        try {
            this.kdf.init(new KDFParameters(concatenate, this.param.getDerivationV()));
            if (this.forEncryption) {
                buf = this.encryptBlock(buf, offset, length);
            }
            else {
                buf = this.decryptBlock(buf, offset, length);
            }
            return buf;
        }
        finally {
            Arrays.fill(concatenate, (byte)0);
        }
    }
}
