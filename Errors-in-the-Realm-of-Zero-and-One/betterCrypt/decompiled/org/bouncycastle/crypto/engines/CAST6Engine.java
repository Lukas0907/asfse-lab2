// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.engines;

public final class CAST6Engine extends CAST5Engine
{
    protected static final int BLOCK_SIZE = 16;
    protected static final int ROUNDS = 12;
    protected int[] _Km;
    protected int[] _Kr;
    protected int[] _Tm;
    protected int[] _Tr;
    private int[] _workingKey;
    
    public CAST6Engine() {
        this._Kr = new int[48];
        this._Km = new int[48];
        this._Tr = new int[192];
        this._Tm = new int[192];
        this._workingKey = new int[8];
    }
    
    protected final void CAST_Decipher(int n, int n2, int n3, int n4, final int[] array) {
        int n5 = 0;
        int i;
        int n6;
        int n7;
        int n8;
        int n9;
        while (true) {
            i = 6;
            n6 = n;
            n7 = n2;
            n8 = n3;
            n9 = n4;
            if (n5 >= 6) {
                break;
            }
            final int n10 = (11 - n5) * 4;
            n3 ^= this.F1(n4, this._Km[n10], this._Kr[n10]);
            final int[] km = this._Km;
            final int n11 = n10 + 1;
            n2 ^= this.F2(n3, km[n11], this._Kr[n11]);
            final int[] km2 = this._Km;
            final int n12 = n10 + 2;
            n ^= this.F3(n2, km2[n12], this._Kr[n12]);
            final int[] km3 = this._Km;
            final int n13 = n10 + 3;
            n4 ^= this.F1(n, km3[n13], this._Kr[n13]);
            ++n5;
        }
        while (i < 12) {
            n = (11 - i) * 4;
            final int[] km4 = this._Km;
            n2 = n + 3;
            n9 ^= this.F1(n6, km4[n2], this._Kr[n2]);
            final int[] km5 = this._Km;
            n2 = n + 2;
            n6 ^= this.F3(n7, km5[n2], this._Kr[n2]);
            final int[] km6 = this._Km;
            n2 = n + 1;
            n7 ^= this.F2(n8, km6[n2], this._Kr[n2]);
            n8 ^= this.F1(n9, this._Km[n], this._Kr[n]);
            ++i;
        }
        array[0] = n6;
        array[1] = n7;
        array[2] = n8;
        array[3] = n9;
    }
    
    protected final void CAST_Encipher(int n, int n2, int n3, int n4, final int[] array) {
        int n5 = 0;
        int i;
        int n6;
        int n7;
        int n8;
        int n9;
        while (true) {
            i = 6;
            n6 = n;
            n7 = n2;
            n8 = n3;
            n9 = n4;
            if (n5 >= 6) {
                break;
            }
            final int n10 = n5 * 4;
            n3 ^= this.F1(n4, this._Km[n10], this._Kr[n10]);
            final int[] km = this._Km;
            final int n11 = n10 + 1;
            n2 ^= this.F2(n3, km[n11], this._Kr[n11]);
            final int[] km2 = this._Km;
            final int n12 = n10 + 2;
            n ^= this.F3(n2, km2[n12], this._Kr[n12]);
            final int[] km3 = this._Km;
            final int n13 = n10 + 3;
            n4 ^= this.F1(n, km3[n13], this._Kr[n13]);
            ++n5;
        }
        while (i < 12) {
            n = i * 4;
            final int[] km4 = this._Km;
            n2 = n + 3;
            n9 ^= this.F1(n6, km4[n2], this._Kr[n2]);
            final int[] km5 = this._Km;
            n2 = n + 2;
            n6 ^= this.F3(n7, km5[n2], this._Kr[n2]);
            final int[] km6 = this._Km;
            n2 = n + 1;
            n7 ^= this.F2(n8, km6[n2], this._Kr[n2]);
            n8 ^= this.F1(n9, this._Km[n], this._Kr[n]);
            ++i;
        }
        array[0] = n6;
        array[1] = n7;
        array[2] = n8;
        array[3] = n9;
    }
    
    @Override
    protected int decryptBlock(final byte[] array, final int n, final byte[] array2, final int n2) {
        final int[] array3 = new int[4];
        this.CAST_Decipher(this.BytesTo32bits(array, n), this.BytesTo32bits(array, n + 4), this.BytesTo32bits(array, n + 8), this.BytesTo32bits(array, n + 12), array3);
        this.Bits32ToBytes(array3[0], array2, n2);
        this.Bits32ToBytes(array3[1], array2, n2 + 4);
        this.Bits32ToBytes(array3[2], array2, n2 + 8);
        this.Bits32ToBytes(array3[3], array2, n2 + 12);
        return 16;
    }
    
    @Override
    protected int encryptBlock(final byte[] array, final int n, final byte[] array2, final int n2) {
        final int[] array3 = new int[4];
        this.CAST_Encipher(this.BytesTo32bits(array, n), this.BytesTo32bits(array, n + 4), this.BytesTo32bits(array, n + 8), this.BytesTo32bits(array, n + 12), array3);
        this.Bits32ToBytes(array3[0], array2, n2);
        this.Bits32ToBytes(array3[1], array2, n2 + 4);
        this.Bits32ToBytes(array3[2], array2, n2 + 8);
        this.Bits32ToBytes(array3[3], array2, n2 + 12);
        return 16;
    }
    
    @Override
    public String getAlgorithmName() {
        return "CAST6";
    }
    
    @Override
    public int getBlockSize() {
        return 16;
    }
    
    @Override
    public void reset() {
    }
    
    @Override
    protected void setKey(final byte[] array) {
        int n = 19;
        int n2 = 1518500249;
        int n5;
        int n6;
        int n8;
        for (int i = 0; i < 24; ++i, n8 = n5, n2 = n6, n = n8) {
            final int n3 = n2;
            final int n4 = 0;
            n5 = n;
            n6 = n3;
            for (int j = n4; j < 8; ++j) {
                final int[] tm = this._Tm;
                final int n7 = i * 8 + j;
                tm[n7] = n6;
                n6 += 1859775393;
                this._Tr[n7] = n5;
                n5 = (n5 + 17 & 0x1F);
            }
        }
        final byte[] array2 = new byte[64];
        System.arraycopy(array, 0, array2, 0, array.length);
        for (int k = 0; k < 8; ++k) {
            this._workingKey[k] = this.BytesTo32bits(array2, k * 4);
        }
        for (int l = 0; l < 12; ++l) {
            final int n9 = l * 2;
            final int n10 = n9 * 8;
            final int[] workingKey = this._workingKey;
            workingKey[6] ^= this.F1(workingKey[7], this._Tm[n10], this._Tr[n10]);
            final int[] workingKey2 = this._workingKey;
            final int n11 = workingKey2[5];
            final int n12 = workingKey2[6];
            final int[] tm2 = this._Tm;
            final int n13 = n10 + 1;
            workingKey2[5] = (n11 ^ this.F2(n12, tm2[n13], this._Tr[n13]));
            final int[] workingKey3 = this._workingKey;
            final int n14 = workingKey3[4];
            final int n15 = workingKey3[5];
            final int[] tm3 = this._Tm;
            final int n16 = n10 + 2;
            workingKey3[4] = (n14 ^ this.F3(n15, tm3[n16], this._Tr[n16]));
            final int[] workingKey4 = this._workingKey;
            final int n17 = workingKey4[3];
            final int n18 = workingKey4[4];
            final int[] tm4 = this._Tm;
            final int n19 = n10 + 3;
            workingKey4[3] = (this.F1(n18, tm4[n19], this._Tr[n19]) ^ n17);
            final int[] workingKey5 = this._workingKey;
            final int n20 = workingKey5[2];
            final int n21 = workingKey5[3];
            final int[] tm5 = this._Tm;
            final int n22 = n10 + 4;
            workingKey5[2] = (this.F2(n21, tm5[n22], this._Tr[n22]) ^ n20);
            final int[] workingKey6 = this._workingKey;
            final int n23 = workingKey6[1];
            final int n24 = workingKey6[2];
            final int[] tm6 = this._Tm;
            final int n25 = n10 + 5;
            workingKey6[1] = (this.F3(n24, tm6[n25], this._Tr[n25]) ^ n23);
            final int[] workingKey7 = this._workingKey;
            final int n26 = workingKey7[0];
            final int n27 = workingKey7[1];
            final int[] tm7 = this._Tm;
            final int n28 = n10 + 6;
            workingKey7[0] = (n26 ^ this.F1(n27, tm7[n28], this._Tr[n28]));
            final int[] workingKey8 = this._workingKey;
            final int n29 = workingKey8[7];
            final int n30 = workingKey8[0];
            final int[] tm8 = this._Tm;
            final int n31 = n10 + 7;
            workingKey8[7] = (this.F2(n30, tm8[n31], this._Tr[n31]) ^ n29);
            final int n32 = (n9 + 1) * 8;
            final int[] workingKey9 = this._workingKey;
            workingKey9[6] ^= this.F1(workingKey9[7], this._Tm[n32], this._Tr[n32]);
            final int[] workingKey10 = this._workingKey;
            final int n33 = workingKey10[5];
            final int n34 = workingKey10[6];
            final int[] tm9 = this._Tm;
            final int n35 = n32 + 1;
            workingKey10[5] = (n33 ^ this.F2(n34, tm9[n35], this._Tr[n35]));
            final int[] workingKey11 = this._workingKey;
            final int n36 = workingKey11[4];
            final int n37 = workingKey11[5];
            final int[] tm10 = this._Tm;
            final int n38 = n32 + 2;
            workingKey11[4] = (n36 ^ this.F3(n37, tm10[n38], this._Tr[n38]));
            final int[] workingKey12 = this._workingKey;
            final int n39 = workingKey12[3];
            final int n40 = workingKey12[4];
            final int[] tm11 = this._Tm;
            final int n41 = n32 + 3;
            workingKey12[3] = (this.F1(n40, tm11[n41], this._Tr[n41]) ^ n39);
            final int[] workingKey13 = this._workingKey;
            final int n42 = workingKey13[2];
            final int n43 = workingKey13[3];
            final int[] tm12 = this._Tm;
            final int n44 = n32 + 4;
            workingKey13[2] = (this.F2(n43, tm12[n44], this._Tr[n44]) ^ n42);
            final int[] workingKey14 = this._workingKey;
            final int n45 = workingKey14[1];
            final int n46 = workingKey14[2];
            final int[] tm13 = this._Tm;
            final int n47 = n32 + 5;
            workingKey14[1] = (this.F3(n46, tm13[n47], this._Tr[n47]) ^ n45);
            final int[] workingKey15 = this._workingKey;
            final int n48 = workingKey15[0];
            final int n49 = workingKey15[1];
            final int[] tm14 = this._Tm;
            final int n50 = n32 + 6;
            workingKey15[0] = (n48 ^ this.F1(n49, tm14[n50], this._Tr[n50]));
            final int[] workingKey16 = this._workingKey;
            final int n51 = workingKey16[7];
            final int n52 = workingKey16[0];
            final int[] tm15 = this._Tm;
            final int n53 = n32 + 7;
            workingKey16[7] = (this.F2(n52, tm15[n53], this._Tr[n53]) ^ n51);
            final int[] kr = this._Kr;
            final int n54 = l * 4;
            final int[] workingKey17 = this._workingKey;
            kr[n54] = (workingKey17[0] & 0x1F);
            final int n55 = n54 + 1;
            kr[n55] = (workingKey17[2] & 0x1F);
            final int n56 = n54 + 2;
            kr[n56] = (workingKey17[4] & 0x1F);
            final int n57 = n54 + 3;
            kr[n57] = (workingKey17[6] & 0x1F);
            final int[] km = this._Km;
            km[n54] = workingKey17[7];
            km[n55] = workingKey17[5];
            km[n56] = workingKey17[3];
            km[n57] = workingKey17[1];
        }
    }
}
