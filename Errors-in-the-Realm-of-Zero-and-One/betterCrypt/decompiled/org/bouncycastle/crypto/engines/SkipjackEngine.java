// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.engines;

import org.bouncycastle.crypto.OutputLengthException;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.BlockCipher;

public class SkipjackEngine implements BlockCipher
{
    static final int BLOCK_SIZE = 8;
    static short[] ftable;
    private boolean encrypting;
    private int[] key0;
    private int[] key1;
    private int[] key2;
    private int[] key3;
    
    static {
        SkipjackEngine.ftable = new short[] { 163, 215, 9, 131, 248, 72, 246, 244, 179, 33, 21, 120, 153, 177, 175, 249, 231, 45, 77, 138, 206, 76, 202, 46, 82, 149, 217, 30, 78, 56, 68, 40, 10, 223, 2, 160, 23, 241, 96, 104, 18, 183, 122, 195, 233, 250, 61, 83, 150, 132, 107, 186, 242, 99, 154, 25, 124, 174, 229, 245, 247, 22, 106, 162, 57, 182, 123, 15, 193, 147, 129, 27, 238, 180, 26, 234, 208, 145, 47, 184, 85, 185, 218, 133, 63, 65, 191, 224, 90, 88, 128, 95, 102, 11, 216, 144, 53, 213, 192, 167, 51, 6, 101, 105, 69, 0, 148, 86, 109, 152, 155, 118, 151, 252, 178, 194, 176, 254, 219, 32, 225, 235, 214, 228, 221, 71, 74, 29, 66, 237, 158, 110, 73, 60, 205, 67, 39, 210, 7, 212, 222, 199, 103, 24, 137, 203, 48, 31, 141, 198, 143, 170, 200, 116, 220, 201, 93, 92, 49, 164, 112, 136, 97, 44, 159, 13, 43, 135, 80, 130, 84, 100, 38, 125, 3, 64, 52, 75, 28, 115, 209, 196, 253, 59, 204, 251, 127, 171, 230, 62, 91, 165, 173, 4, 35, 156, 20, 81, 34, 240, 41, 121, 113, 126, 255, 140, 14, 226, 12, 239, 188, 114, 117, 111, 55, 161, 236, 211, 142, 98, 139, 134, 16, 232, 8, 119, 17, 190, 146, 79, 36, 197, 50, 54, 157, 207, 243, 166, 187, 172, 94, 108, 169, 19, 87, 37, 181, 227, 189, 168, 58, 1, 5, 89, 42, 70 };
    }
    
    private int g(final int n, int n2) {
        final int n3 = n2 & 0xFF;
        final short[] ftable = SkipjackEngine.ftable;
        n2 = ((n2 >> 8 & 0xFF) ^ ftable[this.key0[n] ^ n3]);
        final int n4 = n3 ^ ftable[this.key1[n] ^ n2];
        n2 ^= ftable[this.key2[n] ^ n4];
        return (n2 << 8) + (ftable[this.key3[n] ^ n2] ^ n4);
    }
    
    private int h(final int n, int n2) {
        final int n3 = n2 >> 8 & 0xFF;
        final short[] ftable = SkipjackEngine.ftable;
        n2 = ((n2 & 0xFF) ^ ftable[this.key3[n] ^ n3]);
        final int n4 = n3 ^ ftable[this.key2[n] ^ n2];
        n2 ^= ftable[this.key1[n] ^ n4];
        return ((ftable[this.key0[n] ^ n2] ^ n4) << 8) + n2;
    }
    
    public int decryptBlock(final byte[] array, int n, final byte[] array2, final int n2) {
        int n3 = (array[n + 0] << 8) + (array[n + 1] & 0xFF);
        int n4 = (array[n + 2] << 8) + (array[n + 3] & 0xFF);
        final int n5 = (array[n + 4] << 8) + (array[n + 5] & 0xFF);
        int n6 = (array[n + 6] << 8) + (array[n + 7] & 0xFF);
        int n7 = 31;
        int i = 0;
        n = n5;
        while (i < 2) {
            final int n8 = n3;
            final int n9 = 0;
            int n10 = n6;
            int n11 = n8;
            int n12 = n9;
            int n13;
            while (true) {
                n13 = n7;
                if (n12 >= 8) {
                    break;
                }
                final int h = this.h(n13, n4);
                final int n14 = n13 - 1;
                ++n12;
                final int n15 = n11;
                n11 = h;
                final int n16 = n ^ h ^ n13 + 1;
                n = n10;
                n10 = n15;
                n7 = n14;
                n4 = n16;
            }
            final int n17 = 0;
            int n18 = n4;
            int n19 = n13;
            int n20 = n10;
            n3 = n11;
            int n21 = n17;
            while (true) {
                n6 = n20;
                if (n21 >= 8) {
                    break;
                }
                final int h2 = this.h(n19, n18);
                final int n22 = n19 - 1;
                ++n21;
                n20 = (n3 ^ n18 ^ n19 + 1);
                n3 = h2;
                n18 = n;
                n = n6;
                n19 = n22;
            }
            ++i;
            n4 = n18;
            n7 = n19;
        }
        array2[n2 + 0] = (byte)(n3 >> 8);
        array2[n2 + 1] = (byte)n3;
        array2[n2 + 2] = (byte)(n4 >> 8);
        array2[n2 + 3] = (byte)n4;
        array2[n2 + 4] = (byte)(n >> 8);
        array2[n2 + 5] = (byte)n;
        array2[n2 + 6] = (byte)(n6 >> 8);
        array2[n2 + 7] = (byte)n6;
        return 8;
    }
    
    public int encryptBlock(final byte[] array, int n, final byte[] array2, final int n2) {
        int n3 = (array[n + 0] << 8) + (array[n + 1] & 0xFF);
        int n4 = (array[n + 2] << 8) + (array[n + 3] & 0xFF);
        int n5 = (array[n + 4] << 8) + (array[n + 5] & 0xFF);
        n = (array[n + 6] << 8) + (array[n + 7] & 0xFF);
        int n6;
        int n11;
        for (int i = n6 = 0; i < 2; ++i, n3 = n11) {
            int g;
            int n7;
            int n8;
            int n9;
            for (int j = 0; j < 8; ++j, n8 = n4, n4 = g, n9 = (n ^ g ^ n7), n = n5, n5 = n8, n6 = n7, n3 = n9) {
                g = this.g(n6, n3);
                n7 = n6 + 1;
            }
            int n10 = 0;
            while (true) {
                n11 = n3;
                if (n10 >= 8) {
                    break;
                }
                final int n12 = n6 + 1;
                final int g2 = this.g(n6, n11);
                ++n10;
                n3 = n;
                n = n5;
                n5 = (n4 ^ n11 ^ n12);
                n4 = g2;
                n6 = n12;
            }
        }
        array2[n2 + 0] = (byte)(n3 >> 8);
        array2[n2 + 1] = (byte)n3;
        array2[n2 + 2] = (byte)(n4 >> 8);
        array2[n2 + 3] = (byte)n4;
        array2[n2 + 4] = (byte)(n5 >> 8);
        array2[n2 + 5] = (byte)n5;
        array2[n2 + 6] = (byte)(n >> 8);
        array2[n2 + 7] = (byte)n;
        return 8;
    }
    
    @Override
    public String getAlgorithmName() {
        return "SKIPJACK";
    }
    
    @Override
    public int getBlockSize() {
        return 8;
    }
    
    @Override
    public void init(final boolean encrypting, final CipherParameters cipherParameters) {
        if (cipherParameters instanceof KeyParameter) {
            final byte[] key = ((KeyParameter)cipherParameters).getKey();
            this.encrypting = encrypting;
            this.key0 = new int[32];
            this.key1 = new int[32];
            this.key2 = new int[32];
            this.key3 = new int[32];
            for (int i = 0; i < 32; ++i) {
                final int[] key2 = this.key0;
                final int n = i * 4;
                key2[i] = (key[n % 10] & 0xFF);
                this.key1[i] = (key[(n + 1) % 10] & 0xFF);
                this.key2[i] = (key[(n + 2) % 10] & 0xFF);
                this.key3[i] = (key[(n + 3) % 10] & 0xFF);
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("invalid parameter passed to SKIPJACK init - ");
        sb.append(cipherParameters.getClass().getName());
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public int processBlock(final byte[] array, final int n, final byte[] array2, final int n2) {
        if (this.key1 == null) {
            throw new IllegalStateException("SKIPJACK engine not initialised");
        }
        if (n + 8 > array.length) {
            throw new DataLengthException("input buffer too short");
        }
        if (n2 + 8 <= array2.length) {
            if (this.encrypting) {
                this.encryptBlock(array, n, array2, n2);
            }
            else {
                this.decryptBlock(array, n, array2, n2);
            }
            return 8;
        }
        throw new OutputLengthException("output buffer too short");
    }
    
    @Override
    public void reset() {
    }
}
