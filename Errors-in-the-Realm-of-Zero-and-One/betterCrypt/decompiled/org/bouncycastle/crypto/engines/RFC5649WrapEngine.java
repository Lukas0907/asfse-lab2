// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.engines;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.util.Pack;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.Wrapper;

public class RFC5649WrapEngine implements Wrapper
{
    private BlockCipher engine;
    private byte[] extractedAIV;
    private boolean forWrapping;
    private byte[] highOrderIV;
    private KeyParameter param;
    private byte[] preIV;
    
    public RFC5649WrapEngine(final BlockCipher engine) {
        this.highOrderIV = new byte[] { -90, 89, 89, -90 };
        this.preIV = this.highOrderIV;
        this.extractedAIV = null;
        this.engine = engine;
    }
    
    private byte[] padPlaintext(final byte[] array) {
        final int length = array.length;
        final int n = (8 - length % 8) % 8;
        final byte[] array2 = new byte[length + n];
        System.arraycopy(array, 0, array2, 0, length);
        if (n != 0) {
            System.arraycopy(new byte[n], 0, array2, length, n);
        }
        return array2;
    }
    
    private byte[] rfc3394UnwrapNoIvCheck(final byte[] array, int i, int j) {
        final byte[] array2 = new byte[8];
        final byte[] array3 = new byte[j - array2.length];
        final byte[] extractedAIV = new byte[array2.length];
        final byte[] array4 = new byte[array2.length + 8];
        System.arraycopy(array, i, extractedAIV, 0, array2.length);
        System.arraycopy(array, i + array2.length, array3, 0, j - array2.length);
        this.engine.init(false, this.param);
        final int n = j / 8 - 1;
        int n2;
        int k;
        int n3;
        byte b;
        int n4;
        for (i = 5; i >= 0; --i) {
            for (j = n; j >= 1; --j) {
                System.arraycopy(extractedAIV, 0, array4, 0, array2.length);
                n2 = (j - 1) * 8;
                System.arraycopy(array3, n2, array4, array2.length, 8);
                for (k = n * i + j, n3 = 1; k != 0; k >>>= 8, ++n3) {
                    b = (byte)k;
                    n4 = array2.length - n3;
                    array4[n4] ^= b;
                }
                this.engine.processBlock(array4, 0, array4, 0);
                System.arraycopy(array4, 0, extractedAIV, 0, 8);
                System.arraycopy(array4, 8, array3, n2, 8);
            }
        }
        this.extractedAIV = extractedAIV;
        return array3;
    }
    
    @Override
    public String getAlgorithmName() {
        return this.engine.getAlgorithmName();
    }
    
    @Override
    public void init(final boolean forWrapping, final CipherParameters cipherParameters) {
        this.forWrapping = forWrapping;
        CipherParameters parameters = cipherParameters;
        if (cipherParameters instanceof ParametersWithRandom) {
            parameters = ((ParametersWithRandom)cipherParameters).getParameters();
        }
        if (parameters instanceof KeyParameter) {
            this.param = (KeyParameter)parameters;
            this.preIV = this.highOrderIV;
            return;
        }
        if (!(parameters instanceof ParametersWithIV)) {
            return;
        }
        final ParametersWithIV parametersWithIV = (ParametersWithIV)parameters;
        this.preIV = parametersWithIV.getIV();
        this.param = (KeyParameter)parametersWithIV.getParameters();
        if (this.preIV.length == 4) {
            return;
        }
        throw new IllegalArgumentException("IV length not equal to 4");
    }
    
    @Override
    public byte[] unwrap(byte[] array, int i, int n) throws InvalidCipherTextException {
        if (this.forWrapping) {
            throw new IllegalStateException("not set for unwrapping");
        }
        final int n2 = n / 8;
        if (n2 * 8 != n) {
            throw new InvalidCipherTextException("unwrap data must be a multiple of 8 bytes");
        }
        if (n2 == 1) {
            throw new InvalidCipherTextException("unwrap data must be at least 16 bytes");
        }
        final byte[] array2 = new byte[n];
        System.arraycopy(array, i, array2, 0, n);
        final byte[] array3 = new byte[n];
        if (n2 == 2) {
            this.engine.init(false, this.param);
            for (i = 0; i < array2.length; i += this.engine.getBlockSize()) {
                this.engine.processBlock(array2, i, array3, i);
            }
            this.extractedAIV = new byte[8];
            array = this.extractedAIV;
            System.arraycopy(array3, 0, array, 0, array.length);
            i = array3.length;
            final byte[] extractedAIV = this.extractedAIV;
            array = new byte[i - extractedAIV.length];
            System.arraycopy(array3, extractedAIV.length, array, 0, array.length);
        }
        else {
            array = this.rfc3394UnwrapNoIvCheck(array, i, n);
        }
        final byte[] array4 = new byte[4];
        final byte[] array5 = new byte[4];
        System.arraycopy(this.extractedAIV, 0, array4, 0, array4.length);
        System.arraycopy(this.extractedAIV, array4.length, array5, 0, array5.length);
        final int bigEndianToInt = Pack.bigEndianToInt(array5, 0);
        boolean constantTimeAreEqual = Arrays.constantTimeAreEqual(array4, this.preIV);
        i = array.length;
        if (bigEndianToInt <= i - 8) {
            constantTimeAreEqual = false;
        }
        if (bigEndianToInt > i) {
            constantTimeAreEqual = false;
        }
        n = i - bigEndianToInt;
        if ((i = n) >= array.length) {
            i = array.length;
            constantTimeAreEqual = false;
        }
        final byte[] array6 = new byte[i];
        final byte[] array7 = new byte[i];
        System.arraycopy(array, array.length - i, array7, 0, i);
        if (!Arrays.constantTimeAreEqual(array7, array6)) {
            constantTimeAreEqual = false;
        }
        if (constantTimeAreEqual) {
            final byte[] array8 = new byte[bigEndianToInt];
            System.arraycopy(array, 0, array8, 0, array8.length);
            return array8;
        }
        throw new InvalidCipherTextException("checksum failed");
    }
    
    @Override
    public byte[] wrap(byte[] padPlaintext, int i, final int n) {
        if (!this.forWrapping) {
            throw new IllegalStateException("not set for wrapping");
        }
        final byte[] array = new byte[8];
        final byte[] intToBigEndian = Pack.intToBigEndian(n);
        final byte[] preIV = this.preIV;
        final int length = preIV.length;
        final int n2 = 0;
        System.arraycopy(preIV, 0, array, 0, length);
        System.arraycopy(intToBigEndian, 0, array, this.preIV.length, intToBigEndian.length);
        final byte[] array2 = new byte[n];
        System.arraycopy(padPlaintext, i, array2, 0, n);
        padPlaintext = this.padPlaintext(array2);
        if (padPlaintext.length == 8) {
            final byte[] array3 = new byte[padPlaintext.length + array.length];
            System.arraycopy(array, 0, array3, 0, array.length);
            System.arraycopy(padPlaintext, 0, array3, array.length, padPlaintext.length);
            this.engine.init(true, this.param);
            for (i = n2; i < array3.length; i += this.engine.getBlockSize()) {
                this.engine.processBlock(array3, i, array3, i);
            }
            return array3;
        }
        final RFC3394WrapEngine rfc3394WrapEngine = new RFC3394WrapEngine(this.engine);
        rfc3394WrapEngine.init(true, new ParametersWithIV(this.param, array));
        return rfc3394WrapEngine.wrap(padPlaintext, 0, padPlaintext.length);
    }
}
