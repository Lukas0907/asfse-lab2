// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.engines;

import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.params.ElGamalPrivateKeyParameters;
import org.bouncycastle.crypto.params.ElGamalPublicKeyParameters;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import java.security.SecureRandom;
import org.bouncycastle.crypto.params.ElGamalKeyParameters;
import java.math.BigInteger;
import org.bouncycastle.crypto.AsymmetricBlockCipher;

public class ElGamalEngine implements AsymmetricBlockCipher
{
    private static final BigInteger ONE;
    private static final BigInteger TWO;
    private static final BigInteger ZERO;
    private int bitSize;
    private boolean forEncryption;
    private ElGamalKeyParameters key;
    private SecureRandom random;
    
    static {
        ZERO = BigInteger.valueOf(0L);
        ONE = BigInteger.valueOf(1L);
        TWO = BigInteger.valueOf(2L);
    }
    
    @Override
    public int getInputBlockSize() {
        if (this.forEncryption) {
            return (this.bitSize - 1) / 8;
        }
        return (this.bitSize + 7) / 8 * 2;
    }
    
    @Override
    public int getOutputBlockSize() {
        if (this.forEncryption) {
            return (this.bitSize + 7) / 8 * 2;
        }
        return (this.bitSize - 1) / 8;
    }
    
    @Override
    public void init(final boolean forEncryption, final CipherParameters cipherParameters) {
        SecureRandom random;
        if (cipherParameters instanceof ParametersWithRandom) {
            final ParametersWithRandom parametersWithRandom = (ParametersWithRandom)cipherParameters;
            this.key = (ElGamalKeyParameters)parametersWithRandom.getParameters();
            random = parametersWithRandom.getRandom();
        }
        else {
            this.key = (ElGamalKeyParameters)cipherParameters;
            random = CryptoServicesRegistrar.getSecureRandom();
        }
        this.random = random;
        this.forEncryption = forEncryption;
        this.bitSize = this.key.getParameters().getP().bitLength();
        if (forEncryption) {
            if (this.key instanceof ElGamalPublicKeyParameters) {
                return;
            }
            throw new IllegalArgumentException("ElGamalPublicKeyParameters are required for encryption.");
        }
        else {
            if (this.key instanceof ElGamalPrivateKeyParameters) {
                return;
            }
            throw new IllegalArgumentException("ElGamalPrivateKeyParameters are required for decryption.");
        }
    }
    
    @Override
    public byte[] processBlock(byte[] byteArray, int bitLength, int n) {
        if (this.key == null) {
            throw new IllegalStateException("ElGamal engine not initialised");
        }
        int inputBlockSize;
        if (this.forEncryption) {
            inputBlockSize = (this.bitSize - 1 + 7) / 8;
        }
        else {
            inputBlockSize = this.getInputBlockSize();
        }
        if (n > inputBlockSize) {
            throw new DataLengthException("input too large for ElGamal cipher.\n");
        }
        final BigInteger p3 = this.key.getParameters().getP();
        if (this.key instanceof ElGamalPrivateKeyParameters) {
            n /= 2;
            final byte[] magnitude = new byte[n];
            final byte[] magnitude2 = new byte[n];
            System.arraycopy(byteArray, bitLength, magnitude, 0, magnitude.length);
            System.arraycopy(byteArray, bitLength + magnitude.length, magnitude2, 0, magnitude2.length);
            return BigIntegers.asUnsignedByteArray(new BigInteger(1, magnitude).modPow(p3.subtract(ElGamalEngine.ONE).subtract(((ElGamalPrivateKeyParameters)this.key).getX()), p3).multiply(new BigInteger(1, magnitude2)).mod(p3));
        }
        byte[] magnitude3 = null;
        Label_0200: {
            if (bitLength == 0) {
                magnitude3 = byteArray;
                if (n == byteArray.length) {
                    break Label_0200;
                }
            }
            magnitude3 = new byte[n];
            System.arraycopy(byteArray, bitLength, magnitude3, 0, n);
        }
        final BigInteger bigInteger = new BigInteger(1, magnitude3);
        if (bigInteger.compareTo(p3) >= 0) {
            throw new DataLengthException("input too large for ElGamal cipher.\n");
        }
        final ElGamalPublicKeyParameters elGamalPublicKeyParameters = (ElGamalPublicKeyParameters)this.key;
        bitLength = p3.bitLength();
        BigInteger randomBigInteger;
        while (true) {
            randomBigInteger = BigIntegers.createRandomBigInteger(bitLength, this.random);
            if (!randomBigInteger.equals(ElGamalEngine.ZERO)) {
                if (randomBigInteger.compareTo(p3.subtract(ElGamalEngine.TWO)) > 0) {
                    continue;
                }
                break;
            }
        }
        final BigInteger modPow = this.key.getParameters().getG().modPow(randomBigInteger, p3);
        final BigInteger mod = bigInteger.multiply(elGamalPublicKeyParameters.getY().modPow(randomBigInteger, p3)).mod(p3);
        byteArray = modPow.toByteArray();
        final byte[] byteArray2 = mod.toByteArray();
        final byte[] array = new byte[this.getOutputBlockSize()];
        if (byteArray.length > array.length / 2) {
            System.arraycopy(byteArray, 1, array, array.length / 2 - (byteArray.length - 1), byteArray.length - 1);
        }
        else {
            System.arraycopy(byteArray, 0, array, array.length / 2 - byteArray.length, byteArray.length);
        }
        if (byteArray2.length > array.length / 2) {
            System.arraycopy(byteArray2, 1, array, array.length - (byteArray2.length - 1), byteArray2.length - 1);
            return array;
        }
        System.arraycopy(byteArray2, 0, array, array.length - byteArray2.length, byteArray2.length);
        return array;
    }
}
