// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.engines;

import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.util.DigestFactory;
import java.security.SecureRandom;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.Wrapper;

public class RC2WrapEngine implements Wrapper
{
    private static final byte[] IV2;
    byte[] digest;
    private CBCBlockCipher engine;
    private boolean forWrapping;
    private byte[] iv;
    private CipherParameters param;
    private ParametersWithIV paramPlusIV;
    Digest sha1;
    private SecureRandom sr;
    
    static {
        IV2 = new byte[] { 74, -35, -94, 44, 121, -24, 33, 5 };
    }
    
    public RC2WrapEngine() {
        this.sha1 = DigestFactory.createSHA1();
        this.digest = new byte[20];
    }
    
    private byte[] calculateCMSKeyChecksum(final byte[] array) {
        final byte[] array2 = new byte[8];
        this.sha1.update(array, 0, array.length);
        this.sha1.doFinal(this.digest, 0);
        System.arraycopy(this.digest, 0, array2, 0, 8);
        return array2;
    }
    
    private boolean checkCMSKeyChecksum(final byte[] array, final byte[] array2) {
        return Arrays.constantTimeAreEqual(this.calculateCMSKeyChecksum(array), array2);
    }
    
    @Override
    public String getAlgorithmName() {
        return "RC2";
    }
    
    @Override
    public void init(final boolean forWrapping, CipherParameters parameters) {
        this.forWrapping = forWrapping;
        this.engine = new CBCBlockCipher(new RC2Engine());
        if (parameters instanceof ParametersWithRandom) {
            final ParametersWithRandom parametersWithRandom = (ParametersWithRandom)parameters;
            this.sr = parametersWithRandom.getRandom();
            parameters = parametersWithRandom.getParameters();
        }
        else {
            this.sr = CryptoServicesRegistrar.getSecureRandom();
        }
        if (!(parameters instanceof ParametersWithIV)) {
            this.param = parameters;
            if (this.forWrapping) {
                this.iv = new byte[8];
                this.sr.nextBytes(this.iv);
                this.paramPlusIV = new ParametersWithIV(this.param, this.iv);
            }
            return;
        }
        this.paramPlusIV = (ParametersWithIV)parameters;
        this.iv = this.paramPlusIV.getIV();
        this.param = this.paramPlusIV.getParameters();
        if (!this.forWrapping) {
            throw new IllegalArgumentException("You should not supply an IV for unwrapping");
        }
        final byte[] iv = this.iv;
        if (iv != null && iv.length == 8) {
            return;
        }
        throw new IllegalArgumentException("IV is not 8 octets");
    }
    
    @Override
    public byte[] unwrap(byte[] array, int i, int n) throws InvalidCipherTextException {
        if (this.forWrapping) {
            throw new IllegalStateException("Not set for unwrapping");
        }
        if (array == null) {
            throw new InvalidCipherTextException("Null pointer as ciphertext");
        }
        if (n % this.engine.getBlockSize() != 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Ciphertext not multiple of ");
            sb.append(this.engine.getBlockSize());
            throw new InvalidCipherTextException(sb.toString());
        }
        this.engine.init(false, new ParametersWithIV(this.param, RC2WrapEngine.IV2));
        final byte[] array2 = new byte[n];
        System.arraycopy(array, i, array2, 0, n);
        for (i = 0; i < array2.length / this.engine.getBlockSize(); ++i) {
            n = this.engine.getBlockSize() * i;
            this.engine.processBlock(array2, n, array2, n);
        }
        array = new byte[array2.length];
        int length;
        for (i = 0; i < array2.length; i = n) {
            length = array2.length;
            n = i + 1;
            array[i] = array2[length - n];
        }
        this.iv = new byte[8];
        final byte[] array3 = new byte[array.length - 8];
        System.arraycopy(array, 0, this.iv, 0, 8);
        System.arraycopy(array, 8, array3, 0, array.length - 8);
        this.paramPlusIV = new ParametersWithIV(this.param, this.iv);
        this.engine.init(false, this.paramPlusIV);
        final byte[] array4 = new byte[array3.length];
        System.arraycopy(array3, 0, array4, 0, array3.length);
        for (i = 0; i < array4.length / this.engine.getBlockSize(); ++i) {
            n = this.engine.getBlockSize() * i;
            this.engine.processBlock(array4, n, array4, n);
        }
        array = new byte[array4.length - 8];
        final byte[] array5 = new byte[8];
        System.arraycopy(array4, 0, array, 0, array4.length - 8);
        System.arraycopy(array4, array4.length - 8, array5, 0, 8);
        if (!this.checkCMSKeyChecksum(array, array5)) {
            throw new InvalidCipherTextException("Checksum inside ciphertext is corrupted");
        }
        if (array.length - ((array[0] & 0xFF) + 1) <= 7) {
            final byte[] array6 = new byte[array[0]];
            System.arraycopy(array, 1, array6, 0, array6.length);
            return array6;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("too many pad bytes (");
        sb2.append(array.length - ((array[0] & 0xFF) + 1));
        sb2.append(")");
        throw new InvalidCipherTextException(sb2.toString());
    }
    
    @Override
    public byte[] wrap(byte[] bytes, int i, int n) {
        if (!this.forWrapping) {
            throw new IllegalStateException("Not initialized for wrapping");
        }
        final int n2 = n + 1;
        final int n3 = n2 % 8;
        int n4;
        if (n3 != 0) {
            n4 = 8 - n3 + n2;
        }
        else {
            n4 = n2;
        }
        final byte[] array = new byte[n4];
        final byte b = (byte)n;
        final int n5 = 0;
        array[0] = b;
        System.arraycopy(bytes, i, array, 1, n);
        bytes = new byte[array.length - n - 1];
        if (bytes.length > 0) {
            this.sr.nextBytes(bytes);
            System.arraycopy(bytes, 0, array, n2, bytes.length);
        }
        final byte[] calculateCMSKeyChecksum = this.calculateCMSKeyChecksum(array);
        bytes = new byte[array.length + calculateCMSKeyChecksum.length];
        System.arraycopy(array, 0, bytes, 0, array.length);
        System.arraycopy(calculateCMSKeyChecksum, 0, bytes, array.length, calculateCMSKeyChecksum.length);
        final byte[] array2 = new byte[bytes.length];
        System.arraycopy(bytes, 0, array2, 0, bytes.length);
        final int n6 = bytes.length / this.engine.getBlockSize();
        if (bytes.length % this.engine.getBlockSize() == 0) {
            this.engine.init(true, this.paramPlusIV);
            for (i = 0; i < n6; ++i) {
                n = this.engine.getBlockSize() * i;
                this.engine.processBlock(array2, n, array2, n);
            }
            final byte[] iv = this.iv;
            bytes = new byte[iv.length + array2.length];
            System.arraycopy(iv, 0, bytes, 0, iv.length);
            System.arraycopy(array2, 0, bytes, this.iv.length, array2.length);
            final byte[] array3 = new byte[bytes.length];
            int length;
            for (i = 0; i < bytes.length; i = n) {
                length = bytes.length;
                n = i + 1;
                array3[i] = bytes[length - n];
            }
            this.engine.init(true, new ParametersWithIV(this.param, RC2WrapEngine.IV2));
            for (i = n5; i < n6 + 1; ++i) {
                n = this.engine.getBlockSize() * i;
                this.engine.processBlock(array3, n, array3, n);
            }
            return array3;
        }
        throw new IllegalStateException("Not multiple of block length");
    }
}
