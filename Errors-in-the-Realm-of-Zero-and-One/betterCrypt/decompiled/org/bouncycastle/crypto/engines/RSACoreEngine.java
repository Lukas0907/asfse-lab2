// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.engines;

import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.DataLengthException;
import java.math.BigInteger;
import org.bouncycastle.crypto.params.RSAKeyParameters;

class RSACoreEngine
{
    private boolean forEncryption;
    private RSAKeyParameters key;
    
    public BigInteger convertInput(final byte[] array, final int n, final int n2) {
        if (n2 > this.getInputBlockSize() + 1) {
            throw new DataLengthException("input too large for RSA cipher.");
        }
        if (n2 == this.getInputBlockSize() + 1 && !this.forEncryption) {
            throw new DataLengthException("input too large for RSA cipher.");
        }
        byte[] magnitude = null;
        Label_0067: {
            if (n == 0) {
                magnitude = array;
                if (n2 == array.length) {
                    break Label_0067;
                }
            }
            magnitude = new byte[n2];
            System.arraycopy(array, n, magnitude, 0, n2);
        }
        final BigInteger bigInteger = new BigInteger(1, magnitude);
        if (bigInteger.compareTo(this.key.getModulus()) < 0) {
            return bigInteger;
        }
        throw new DataLengthException("input too large for RSA cipher.");
    }
    
    public byte[] convertOutput(final BigInteger bigInteger) {
        final byte[] byteArray = bigInteger.toByteArray();
        if (!this.forEncryption) {
            byte[] array;
            if (byteArray[0] == 0) {
                array = new byte[byteArray.length - 1];
                System.arraycopy(byteArray, 1, array, 0, array.length);
            }
            else {
                array = new byte[byteArray.length];
                System.arraycopy(byteArray, 0, array, 0, array.length);
            }
            Arrays.fill(byteArray, (byte)0);
            return array;
        }
        if (byteArray[0] == 0 && byteArray.length > this.getOutputBlockSize()) {
            final byte[] array2 = new byte[byteArray.length - 1];
            System.arraycopy(byteArray, 1, array2, 0, array2.length);
            return array2;
        }
        if (byteArray.length < this.getOutputBlockSize()) {
            final byte[] array3 = new byte[this.getOutputBlockSize()];
            System.arraycopy(byteArray, 0, array3, array3.length - byteArray.length, byteArray.length);
            return array3;
        }
        return byteArray;
    }
    
    public int getInputBlockSize() {
        final int bitLength = this.key.getModulus().bitLength();
        final boolean forEncryption = this.forEncryption;
        int n = (bitLength + 7) / 8;
        if (forEncryption) {
            --n;
        }
        return n;
    }
    
    public int getOutputBlockSize() {
        final int bitLength = this.key.getModulus().bitLength();
        final boolean forEncryption = this.forEncryption;
        final int n = (bitLength + 7) / 8;
        if (forEncryption) {
            return n;
        }
        return n - 1;
    }
    
    public void init(final boolean forEncryption, final CipherParameters cipherParameters) {
        CipherParameters parameters = cipherParameters;
        if (cipherParameters instanceof ParametersWithRandom) {
            parameters = ((ParametersWithRandom)cipherParameters).getParameters();
        }
        this.key = (RSAKeyParameters)parameters;
        this.forEncryption = forEncryption;
    }
    
    public BigInteger processBlock(BigInteger modPow) {
        final RSAKeyParameters key = this.key;
        if (key instanceof RSAPrivateCrtKeyParameters) {
            final RSAPrivateCrtKeyParameters rsaPrivateCrtKeyParameters = (RSAPrivateCrtKeyParameters)key;
            final BigInteger p = rsaPrivateCrtKeyParameters.getP();
            final BigInteger q = rsaPrivateCrtKeyParameters.getQ();
            final BigInteger dp = rsaPrivateCrtKeyParameters.getDP();
            final BigInteger dq = rsaPrivateCrtKeyParameters.getDQ();
            final BigInteger qInv = rsaPrivateCrtKeyParameters.getQInv();
            final BigInteger modPow2 = modPow.remainder(p).modPow(dp, p);
            modPow = modPow.remainder(q).modPow(dq, q);
            return modPow2.subtract(modPow).multiply(qInv).mod(p).multiply(q).add(modPow);
        }
        return modPow.modPow(key.getExponent(), this.key.getModulus());
    }
}
