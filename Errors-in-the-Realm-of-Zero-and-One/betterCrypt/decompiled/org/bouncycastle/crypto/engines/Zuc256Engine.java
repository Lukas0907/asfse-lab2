// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.engines;

import org.bouncycastle.util.Memoable;

public final class Zuc256Engine extends Zuc256CoreEngine
{
    public Zuc256Engine() {
    }
    
    public Zuc256Engine(final int n) {
        super(n);
    }
    
    private Zuc256Engine(final Zuc256Engine zuc256Engine) {
        super(zuc256Engine);
    }
    
    @Override
    public Memoable copy() {
        return new Zuc256Engine(this);
    }
}
