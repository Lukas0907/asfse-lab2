// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.macs;

import org.bouncycastle.util.Memoable;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.engines.Zuc128CoreEngine;
import org.bouncycastle.crypto.engines.Zuc256CoreEngine;
import org.bouncycastle.crypto.Mac;

public final class Zuc256Mac implements Mac
{
    private static final int TOPBIT = 128;
    private int theByteIndex;
    private final InternalZuc256Engine theEngine;
    private final int[] theKeyStream;
    private final int[] theMac;
    private final int theMacLength;
    private Zuc256CoreEngine theState;
    private int theWordIndex;
    
    public Zuc256Mac(int theMacLength) {
        this.theEngine = new InternalZuc256Engine(theMacLength);
        this.theMacLength = theMacLength;
        theMacLength /= 32;
        this.theMac = new int[theMacLength];
        this.theKeyStream = new int[theMacLength + 1];
    }
    
    private int getKeyStreamWord(final int n, final int n2) {
        final int[] theKeyStream = this.theKeyStream;
        final int theWordIndex = this.theWordIndex;
        final int n3 = theKeyStream[(theWordIndex + n) % theKeyStream.length];
        if (n2 == 0) {
            return n3;
        }
        return theKeyStream[(theWordIndex + n + 1) % theKeyStream.length] >>> 32 - n2 | n3 << n2;
    }
    
    private void initKeyStream() {
        final int n = 0;
        int n2 = 0;
        int n3;
        while (true) {
            final int[] theMac = this.theMac;
            n3 = n;
            if (n2 >= theMac.length) {
                break;
            }
            theMac[n2] = this.theEngine.createKeyStreamWord();
            ++n2;
        }
        int[] theKeyStream;
        while (true) {
            theKeyStream = this.theKeyStream;
            if (n3 >= theKeyStream.length - 1) {
                break;
            }
            theKeyStream[n3] = this.theEngine.createKeyStreamWord();
            ++n3;
        }
        this.theWordIndex = theKeyStream.length - 1;
        this.theByteIndex = 3;
    }
    
    private void shift4Final() {
        this.theByteIndex = (this.theByteIndex + 1) % 4;
        if (this.theByteIndex == 0) {
            this.theWordIndex = (this.theWordIndex + 1) % this.theKeyStream.length;
        }
    }
    
    private void shift4NextByte() {
        this.theByteIndex = (this.theByteIndex + 1) % 4;
        if (this.theByteIndex == 0) {
            this.theKeyStream[this.theWordIndex] = this.theEngine.createKeyStreamWord();
            this.theWordIndex = (this.theWordIndex + 1) % this.theKeyStream.length;
        }
    }
    
    private void updateMac(final int n) {
        int n2 = 0;
        while (true) {
            final int[] theMac = this.theMac;
            if (n2 >= theMac.length) {
                break;
            }
            theMac[n2] ^= this.getKeyStreamWord(n2, n);
            ++n2;
        }
    }
    
    @Override
    public int doFinal(final byte[] array, final int n) {
        this.shift4Final();
        this.updateMac(this.theByteIndex * 8);
        int n2 = 0;
        while (true) {
            final int[] theMac = this.theMac;
            if (n2 >= theMac.length) {
                break;
            }
            Zuc128CoreEngine.encode32be(theMac[n2], array, n2 * 4 + n);
            ++n2;
        }
        this.reset();
        return this.getMacSize();
    }
    
    @Override
    public String getAlgorithmName() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Zuc256Mac-");
        sb.append(this.theMacLength);
        return sb.toString();
    }
    
    @Override
    public int getMacSize() {
        return this.theMacLength / 8;
    }
    
    @Override
    public void init(final CipherParameters cipherParameters) {
        this.theEngine.init(true, cipherParameters);
        this.theState = (Zuc256CoreEngine)this.theEngine.copy();
        this.initKeyStream();
    }
    
    @Override
    public void reset() {
        final Zuc256CoreEngine theState = this.theState;
        if (theState != null) {
            this.theEngine.reset(theState);
        }
        this.initKeyStream();
    }
    
    @Override
    public void update(final byte b) {
        this.shift4NextByte();
        final int theByteIndex = this.theByteIndex;
        for (int i = 128, n = 0; i > 0; i >>= 1, ++n) {
            if ((b & i) != 0x0) {
                this.updateMac(theByteIndex * 8 + n);
            }
        }
    }
    
    @Override
    public void update(final byte[] array, final int n, final int n2) {
        for (int i = 0; i < n2; ++i) {
            this.update(array[n + i]);
        }
    }
    
    private static class InternalZuc256Engine extends Zuc256CoreEngine
    {
        public InternalZuc256Engine(final int n) {
            super(n);
        }
        
        int createKeyStreamWord() {
            return super.makeKeyStreamWord();
        }
    }
}
