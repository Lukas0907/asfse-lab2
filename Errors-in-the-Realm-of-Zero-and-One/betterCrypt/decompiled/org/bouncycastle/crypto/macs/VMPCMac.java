// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.macs;

import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.Mac;

public class VMPCMac implements Mac
{
    private byte[] P;
    private byte[] T;
    private byte g;
    private byte n;
    private byte s;
    private byte[] workingIV;
    private byte[] workingKey;
    private byte x1;
    private byte x2;
    private byte x3;
    private byte x4;
    
    public VMPCMac() {
        this.n = 0;
        this.P = null;
        this.s = 0;
    }
    
    private void initKey(byte[] p2, final byte[] array) {
        this.s = 0;
        this.P = new byte[256];
        for (int i = 0; i < 256; ++i) {
            this.P[i] = (byte)i;
        }
        for (int j = 0; j < 768; ++j) {
            final byte[] p3 = this.P;
            final byte s = this.s;
            final int n = j & 0xFF;
            this.s = p3[s + p3[n] + p2[j % p2.length] & 0xFF];
            final byte b = p3[n];
            final byte s2 = this.s;
            p3[n] = p3[s2 & 0xFF];
            p3[s2 & 0xFF] = b;
        }
        for (int k = 0; k < 768; ++k) {
            p2 = this.P;
            final byte s3 = this.s;
            final int n2 = k & 0xFF;
            this.s = p2[s3 + p2[n2] + array[k % array.length] & 0xFF];
            final byte b2 = p2[n2];
            final byte s4 = this.s;
            p2[n2] = p2[s4 & 0xFF];
            p2[s4 & 0xFF] = b2;
        }
        this.n = 0;
    }
    
    @Override
    public int doFinal(final byte[] array, final int n) throws DataLengthException, IllegalStateException {
        for (int i = 1; i < 25; ++i) {
            final byte[] p2 = this.P;
            final byte s = this.s;
            final byte n2 = this.n;
            this.s = p2[s + p2[n2 & 0xFF] & 0xFF];
            final byte x4 = this.x4;
            final byte x5 = this.x3;
            this.x4 = p2[x4 + x5 + i & 0xFF];
            final byte x6 = this.x2;
            this.x3 = p2[x5 + x6 + i & 0xFF];
            final byte x7 = this.x1;
            this.x2 = p2[x6 + x7 + i & 0xFF];
            final byte s2 = this.s;
            this.x1 = p2[x7 + s2 + i & 0xFF];
            final byte[] t = this.T;
            final byte g = this.g;
            t[g & 0x1F] ^= this.x1;
            t[g + 1 & 0x1F] ^= this.x2;
            t[g + 2 & 0x1F] ^= this.x3;
            t[g + 3 & 0x1F] ^= this.x4;
            this.g = (byte)(g + 4 & 0x1F);
            final byte b = p2[n2 & 0xFF];
            p2[n2 & 0xFF] = p2[s2 & 0xFF];
            p2[s2 & 0xFF] = b;
            this.n = (byte)(n2 + 1 & 0xFF);
        }
        for (int j = 0; j < 768; ++j) {
            final byte[] p3 = this.P;
            final byte s3 = this.s;
            final int n3 = j & 0xFF;
            this.s = p3[s3 + p3[n3] + this.T[j & 0x1F] & 0xFF];
            final byte b2 = p3[n3];
            final byte s4 = this.s;
            p3[n3] = p3[s4 & 0xFF];
            p3[s4 & 0xFF] = b2;
        }
        final byte[] array2 = new byte[20];
        for (int k = 0; k < 20; ++k) {
            final byte[] p4 = this.P;
            final byte s5 = this.s;
            final int n4 = k & 0xFF;
            this.s = p4[s5 + p4[n4] & 0xFF];
            final byte s6 = this.s;
            array2[k] = p4[p4[p4[s6 & 0xFF] & 0xFF] + 1 & 0xFF];
            final byte b3 = p4[n4];
            p4[n4] = p4[s6 & 0xFF];
            p4[s6 & 0xFF] = b3;
        }
        System.arraycopy(array2, 0, array, n, array2.length);
        this.reset();
        return array2.length;
    }
    
    @Override
    public String getAlgorithmName() {
        return "VMPC-MAC";
    }
    
    @Override
    public int getMacSize() {
        return 20;
    }
    
    @Override
    public void init(final CipherParameters cipherParameters) throws IllegalArgumentException {
        if (!(cipherParameters instanceof ParametersWithIV)) {
            throw new IllegalArgumentException("VMPC-MAC Init parameters must include an IV");
        }
        final ParametersWithIV parametersWithIV = (ParametersWithIV)cipherParameters;
        final KeyParameter keyParameter = (KeyParameter)parametersWithIV.getParameters();
        if (!(parametersWithIV.getParameters() instanceof KeyParameter)) {
            throw new IllegalArgumentException("VMPC-MAC Init parameters must include a key");
        }
        this.workingIV = parametersWithIV.getIV();
        final byte[] workingIV = this.workingIV;
        if (workingIV != null && workingIV.length >= 1 && workingIV.length <= 768) {
            this.workingKey = keyParameter.getKey();
            this.reset();
            return;
        }
        throw new IllegalArgumentException("VMPC-MAC requires 1 to 768 bytes of IV");
    }
    
    @Override
    public void reset() {
        this.initKey(this.workingKey, this.workingIV);
        this.n = 0;
        this.x4 = 0;
        this.x3 = 0;
        this.x2 = 0;
        this.x1 = 0;
        this.g = 0;
        this.T = new byte[32];
        for (int i = 0; i < 32; ++i) {
            this.T[i] = 0;
        }
    }
    
    @Override
    public void update(final byte b) throws IllegalStateException {
        final byte[] p = this.P;
        final byte s = this.s;
        final byte n = this.n;
        this.s = p[s + p[n & 0xFF] & 0xFF];
        final byte s2 = this.s;
        final byte b2 = (byte)(b ^ p[p[p[s2 & 0xFF] & 0xFF] + 1 & 0xFF]);
        final byte x4 = this.x4;
        final byte x5 = this.x3;
        this.x4 = p[x4 + x5 & 0xFF];
        final byte x6 = this.x2;
        this.x3 = p[x5 + x6 & 0xFF];
        final byte x7 = this.x1;
        this.x2 = p[x6 + x7 & 0xFF];
        this.x1 = p[x7 + s2 + b2 & 0xFF];
        final byte[] t = this.T;
        final byte g = this.g;
        t[g & 0x1F] ^= this.x1;
        t[g + 1 & 0x1F] ^= this.x2;
        t[g + 2 & 0x1F] ^= this.x3;
        t[g + 3 & 0x1F] ^= this.x4;
        this.g = (byte)(g + 4 & 0x1F);
        final byte b3 = p[n & 0xFF];
        p[n & 0xFF] = p[s2 & 0xFF];
        p[s2 & 0xFF] = b3;
        this.n = (byte)(n + 1 & 0xFF);
    }
    
    @Override
    public void update(final byte[] array, final int n, final int n2) throws DataLengthException, IllegalStateException {
        if (n + n2 <= array.length) {
            for (int i = 0; i < n2; ++i) {
                this.update(array[n + i]);
            }
            return;
        }
        throw new DataLengthException("input buffer too short");
    }
}
