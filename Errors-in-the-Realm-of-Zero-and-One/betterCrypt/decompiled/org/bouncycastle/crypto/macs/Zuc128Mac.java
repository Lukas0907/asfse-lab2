// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.macs;

import org.bouncycastle.util.Memoable;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.engines.Zuc128CoreEngine;
import org.bouncycastle.crypto.Mac;

public final class Zuc128Mac implements Mac
{
    private static final int TOPBIT = 128;
    private int theByteIndex;
    private final InternalZuc128Engine theEngine;
    private final int[] theKeyStream;
    private int theMac;
    private Zuc128CoreEngine theState;
    private int theWordIndex;
    
    public Zuc128Mac() {
        this.theEngine = new InternalZuc128Engine();
        this.theKeyStream = new int[2];
    }
    
    private int getFinalWord() {
        if (this.theByteIndex != 0) {
            return this.theEngine.createKeyStreamWord();
        }
        final int theWordIndex = this.theWordIndex;
        final int[] theKeyStream = this.theKeyStream;
        this.theWordIndex = (theWordIndex + 1) % theKeyStream.length;
        return theKeyStream[this.theWordIndex];
    }
    
    private int getKeyStreamWord(final int n) {
        final int[] theKeyStream = this.theKeyStream;
        final int theWordIndex = this.theWordIndex;
        final int n2 = theKeyStream[theWordIndex];
        if (n == 0) {
            return n2;
        }
        return theKeyStream[(theWordIndex + 1) % theKeyStream.length] >>> 32 - n | n2 << n;
    }
    
    private void initKeyStream() {
        int n = 0;
        this.theMac = 0;
        int[] theKeyStream;
        while (true) {
            theKeyStream = this.theKeyStream;
            if (n >= theKeyStream.length - 1) {
                break;
            }
            theKeyStream[n] = this.theEngine.createKeyStreamWord();
            ++n;
        }
        this.theWordIndex = theKeyStream.length - 1;
        this.theByteIndex = 3;
    }
    
    private void shift4NextByte() {
        this.theByteIndex = (this.theByteIndex + 1) % 4;
        if (this.theByteIndex == 0) {
            this.theKeyStream[this.theWordIndex] = this.theEngine.createKeyStreamWord();
            this.theWordIndex = (this.theWordIndex + 1) % this.theKeyStream.length;
        }
    }
    
    private void updateMac(final int n) {
        this.theMac ^= this.getKeyStreamWord(n);
    }
    
    @Override
    public int doFinal(final byte[] array, final int n) {
        this.shift4NextByte();
        this.theMac ^= this.getKeyStreamWord(this.theByteIndex * 8);
        Zuc128CoreEngine.encode32be(this.theMac ^= this.getFinalWord(), array, n);
        this.reset();
        return this.getMacSize();
    }
    
    @Override
    public String getAlgorithmName() {
        return "Zuc128Mac";
    }
    
    @Override
    public int getMacSize() {
        return 4;
    }
    
    @Override
    public void init(final CipherParameters cipherParameters) {
        this.theEngine.init(true, cipherParameters);
        this.theState = (Zuc128CoreEngine)this.theEngine.copy();
        this.initKeyStream();
    }
    
    @Override
    public void reset() {
        final Zuc128CoreEngine theState = this.theState;
        if (theState != null) {
            this.theEngine.reset(theState);
        }
        this.initKeyStream();
    }
    
    @Override
    public void update(final byte b) {
        this.shift4NextByte();
        final int theByteIndex = this.theByteIndex;
        for (int i = 128, n = 0; i > 0; i >>= 1, ++n) {
            if ((b & i) != 0x0) {
                this.updateMac(theByteIndex * 8 + n);
            }
        }
    }
    
    @Override
    public void update(final byte[] array, final int n, final int n2) {
        for (int i = 0; i < n2; ++i) {
            this.update(array[n + i]);
        }
    }
    
    private static class InternalZuc128Engine extends Zuc128CoreEngine
    {
        int createKeyStreamWord() {
            return super.makeKeyStreamWord();
        }
    }
}
