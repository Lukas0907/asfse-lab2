// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.examples;

import java.io.IOException;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.engines.DESedeEngine;
import java.io.PrintStream;
import java.io.BufferedOutputStream;
import java.io.BufferedInputStream;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;

public class DESExample
{
    private PaddedBufferedBlockCipher cipher;
    private boolean encrypt;
    private BufferedInputStream in;
    private byte[] key;
    private BufferedOutputStream out;
    
    public DESExample() {
        this.encrypt = true;
        this.cipher = null;
        this.in = null;
        this.out = null;
        this.key = null;
    }
    
    public DESExample(final String p0, final String p1, final String p2, final boolean p3) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokespecial   java/lang/Object.<init>:()V
        //     4: aload_0        
        //     5: iconst_1       
        //     6: putfield        org/bouncycastle/crypto/examples/DESExample.encrypt:Z
        //     9: aload_0        
        //    10: aconst_null    
        //    11: putfield        org/bouncycastle/crypto/examples/DESExample.cipher:Lorg/bouncycastle/crypto/paddings/PaddedBufferedBlockCipher;
        //    14: aload_0        
        //    15: aconst_null    
        //    16: putfield        org/bouncycastle/crypto/examples/DESExample.in:Ljava/io/BufferedInputStream;
        //    19: aload_0        
        //    20: aconst_null    
        //    21: putfield        org/bouncycastle/crypto/examples/DESExample.out:Ljava/io/BufferedOutputStream;
        //    24: aload_0        
        //    25: aconst_null    
        //    26: putfield        org/bouncycastle/crypto/examples/DESExample.key:[B
        //    29: aload_0        
        //    30: iload           4
        //    32: putfield        org/bouncycastle/crypto/examples/DESExample.encrypt:Z
        //    35: aload_0        
        //    36: new             Ljava/io/BufferedInputStream;
        //    39: dup            
        //    40: new             Ljava/io/FileInputStream;
        //    43: dup            
        //    44: aload_1        
        //    45: invokespecial   java/io/FileInputStream.<init>:(Ljava/lang/String;)V
        //    48: invokespecial   java/io/BufferedInputStream.<init>:(Ljava/io/InputStream;)V
        //    51: putfield        org/bouncycastle/crypto/examples/DESExample.in:Ljava/io/BufferedInputStream;
        //    54: goto            108
        //    57: getstatic       java/lang/System.err:Ljava/io/PrintStream;
        //    60: astore          6
        //    62: new             Ljava/lang/StringBuilder;
        //    65: dup            
        //    66: invokespecial   java/lang/StringBuilder.<init>:()V
        //    69: astore          7
        //    71: aload           7
        //    73: ldc             "Input file not found ["
        //    75: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    78: pop            
        //    79: aload           7
        //    81: aload_1        
        //    82: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    85: pop            
        //    86: aload           7
        //    88: ldc             "]"
        //    90: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    93: pop            
        //    94: aload           6
        //    96: aload           7
        //    98: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   101: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   104: iconst_1       
        //   105: invokestatic    java/lang/System.exit:(I)V
        //   108: aload_0        
        //   109: new             Ljava/io/BufferedOutputStream;
        //   112: dup            
        //   113: new             Ljava/io/FileOutputStream;
        //   116: dup            
        //   117: aload_2        
        //   118: invokespecial   java/io/FileOutputStream.<init>:(Ljava/lang/String;)V
        //   121: invokespecial   java/io/BufferedOutputStream.<init>:(Ljava/io/OutputStream;)V
        //   124: putfield        org/bouncycastle/crypto/examples/DESExample.out:Ljava/io/BufferedOutputStream;
        //   127: goto            179
        //   130: getstatic       java/lang/System.err:Ljava/io/PrintStream;
        //   133: astore_1       
        //   134: new             Ljava/lang/StringBuilder;
        //   137: dup            
        //   138: invokespecial   java/lang/StringBuilder.<init>:()V
        //   141: astore          6
        //   143: aload           6
        //   145: ldc             "Output file not created ["
        //   147: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   150: pop            
        //   151: aload           6
        //   153: aload_2        
        //   154: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   157: pop            
        //   158: aload           6
        //   160: ldc             "]"
        //   162: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   165: pop            
        //   166: aload_1        
        //   167: aload           6
        //   169: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   172: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   175: iconst_1       
        //   176: invokestatic    java/lang/System.exit:(I)V
        //   179: iload           4
        //   181: ifeq            311
        //   184: new             Ljava/security/SecureRandom;
        //   187: dup            
        //   188: invokespecial   java/security/SecureRandom.<init>:()V
        //   191: astore_1       
        //   192: aload_1        
        //   193: ldc             "www.bouncycastle.org"
        //   195: invokevirtual   java/lang/String.getBytes:()[B
        //   198: invokevirtual   java/security/SecureRandom.setSeed:([B)V
        //   201: goto            218
        //   204: aconst_null    
        //   205: astore_1       
        //   206: getstatic       java/lang/System.err:Ljava/io/PrintStream;
        //   209: ldc             "Hmmm, no SHA1PRNG, you need the Sun implementation"
        //   211: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   214: iconst_1       
        //   215: invokestatic    java/lang/System.exit:(I)V
        //   218: new             Lorg/bouncycastle/crypto/KeyGenerationParameters;
        //   221: dup            
        //   222: aload_1        
        //   223: sipush          192
        //   226: invokespecial   org/bouncycastle/crypto/KeyGenerationParameters.<init>:(Ljava/security/SecureRandom;I)V
        //   229: astore_1       
        //   230: new             Lorg/bouncycastle/crypto/generators/DESedeKeyGenerator;
        //   233: dup            
        //   234: invokespecial   org/bouncycastle/crypto/generators/DESedeKeyGenerator.<init>:()V
        //   237: astore_2       
        //   238: aload_2        
        //   239: aload_1        
        //   240: invokevirtual   org/bouncycastle/crypto/generators/DESedeKeyGenerator.init:(Lorg/bouncycastle/crypto/KeyGenerationParameters;)V
        //   243: aload_0        
        //   244: aload_2        
        //   245: invokevirtual   org/bouncycastle/crypto/generators/DESedeKeyGenerator.generateKey:()[B
        //   248: putfield        org/bouncycastle/crypto/examples/DESExample.key:[B
        //   251: new             Ljava/io/BufferedOutputStream;
        //   254: dup            
        //   255: new             Ljava/io/FileOutputStream;
        //   258: dup            
        //   259: aload_3        
        //   260: invokespecial   java/io/FileOutputStream.<init>:(Ljava/lang/String;)V
        //   263: invokespecial   java/io/BufferedOutputStream.<init>:(Ljava/io/OutputStream;)V
        //   266: astore_1       
        //   267: aload_0        
        //   268: getfield        org/bouncycastle/crypto/examples/DESExample.key:[B
        //   271: invokestatic    org/bouncycastle/util/encoders/Hex.encode:([B)[B
        //   274: astore_2       
        //   275: aload_1        
        //   276: aload_2        
        //   277: iconst_0       
        //   278: aload_2        
        //   279: arraylength    
        //   280: invokevirtual   java/io/BufferedOutputStream.write:([BII)V
        //   283: aload_1        
        //   284: invokevirtual   java/io/BufferedOutputStream.flush:()V
        //   287: aload_1        
        //   288: invokevirtual   java/io/BufferedOutputStream.close:()V
        //   291: return         
        //   292: getstatic       java/lang/System.err:Ljava/io/PrintStream;
        //   295: astore_1       
        //   296: new             Ljava/lang/StringBuilder;
        //   299: dup            
        //   300: invokespecial   java/lang/StringBuilder.<init>:()V
        //   303: astore_2       
        //   304: ldc             "Could not decryption create key file ["
        //   306: astore          6
        //   308: goto            372
        //   311: new             Ljava/io/BufferedInputStream;
        //   314: dup            
        //   315: new             Ljava/io/FileInputStream;
        //   318: dup            
        //   319: aload_3        
        //   320: invokespecial   java/io/FileInputStream.<init>:(Ljava/lang/String;)V
        //   323: invokespecial   java/io/BufferedInputStream.<init>:(Ljava/io/InputStream;)V
        //   326: astore_1       
        //   327: aload_1        
        //   328: invokevirtual   java/io/BufferedInputStream.available:()I
        //   331: istore          5
        //   333: iload           5
        //   335: newarray        B
        //   337: astore_2       
        //   338: aload_1        
        //   339: aload_2        
        //   340: iconst_0       
        //   341: iload           5
        //   343: invokevirtual   java/io/BufferedInputStream.read:([BII)I
        //   346: pop            
        //   347: aload_0        
        //   348: aload_2        
        //   349: invokestatic    org/bouncycastle/util/encoders/Hex.decode:([B)[B
        //   352: putfield        org/bouncycastle/crypto/examples/DESExample.key:[B
        //   355: return         
        //   356: getstatic       java/lang/System.err:Ljava/io/PrintStream;
        //   359: astore_1       
        //   360: new             Ljava/lang/StringBuilder;
        //   363: dup            
        //   364: invokespecial   java/lang/StringBuilder.<init>:()V
        //   367: astore_2       
        //   368: ldc             "Decryption key file not found, or not valid ["
        //   370: astore          6
        //   372: aload_2        
        //   373: aload           6
        //   375: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   378: pop            
        //   379: aload_2        
        //   380: aload_3        
        //   381: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   384: pop            
        //   385: aload_2        
        //   386: ldc             "]"
        //   388: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   391: pop            
        //   392: aload_1        
        //   393: aload_2        
        //   394: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   397: invokevirtual   java/io/PrintStream.println:(Ljava/lang/String;)V
        //   400: iconst_1       
        //   401: invokestatic    java/lang/System.exit:(I)V
        //   404: return         
        //   405: astore          6
        //   407: goto            57
        //   410: astore_1       
        //   411: goto            130
        //   414: astore_1       
        //   415: goto            204
        //   418: astore_1       
        //   419: goto            292
        //   422: astore_2       
        //   423: goto            206
        //   426: astore_1       
        //   427: goto            356
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                           
        //  -----  -----  -----  -----  -------------------------------
        //  35     54     405    108    Ljava/io/FileNotFoundException;
        //  108    127    410    179    Ljava/io/IOException;
        //  184    192    414    206    Ljava/lang/Exception;
        //  184    192    418    311    Ljava/io/IOException;
        //  192    201    422    426    Ljava/lang/Exception;
        //  192    201    418    311    Ljava/io/IOException;
        //  206    218    418    311    Ljava/io/IOException;
        //  218    291    418    311    Ljava/io/IOException;
        //  311    355    426    372    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 219 out of bounds for length 219
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:373)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:426)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:713)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:549)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void main(final String[] array) {
        if (array.length < 2) {
            final DESExample desExample = new DESExample();
            final PrintStream err = System.err;
            final StringBuilder sb = new StringBuilder();
            sb.append("Usage: java ");
            sb.append(desExample.getClass().getName());
            sb.append(" infile outfile [keyfile]");
            err.println(sb.toString());
            System.exit(1);
        }
        boolean b = false;
        final String s = array[0];
        final String s2 = array[1];
        String s3;
        if (array.length > 2) {
            s3 = array[2];
        }
        else {
            s3 = "deskey.dat";
            b = true;
        }
        new DESExample(s, s2, s3, b).process();
    }
    
    private void performDecrypt(final byte[] p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        org/bouncycastle/crypto/examples/DESExample.cipher:Lorg/bouncycastle/crypto/paddings/PaddedBufferedBlockCipher;
        //     4: iconst_0       
        //     5: new             Lorg/bouncycastle/crypto/params/KeyParameter;
        //     8: dup            
        //     9: aload_1        
        //    10: invokespecial   org/bouncycastle/crypto/params/KeyParameter.<init>:([B)V
        //    13: invokevirtual   org/bouncycastle/crypto/paddings/PaddedBufferedBlockCipher.init:(ZLorg/bouncycastle/crypto/CipherParameters;)V
        //    16: new             Ljava/io/BufferedReader;
        //    19: dup            
        //    20: new             Ljava/io/InputStreamReader;
        //    23: dup            
        //    24: aload_0        
        //    25: getfield        org/bouncycastle/crypto/examples/DESExample.in:Ljava/io/BufferedInputStream;
        //    28: invokespecial   java/io/InputStreamReader.<init>:(Ljava/io/InputStream;)V
        //    31: invokespecial   java/io/BufferedReader.<init>:(Ljava/io/Reader;)V
        //    34: astore          4
        //    36: aconst_null    
        //    37: astore_1       
        //    38: aload           4
        //    40: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //    43: astore_3       
        //    44: aload_3        
        //    45: ifnull          100
        //    48: aload_3        
        //    49: invokestatic    org/bouncycastle/util/encoders/Hex.decode:(Ljava/lang/String;)[B
        //    52: astore_1       
        //    53: aload_0        
        //    54: getfield        org/bouncycastle/crypto/examples/DESExample.cipher:Lorg/bouncycastle/crypto/paddings/PaddedBufferedBlockCipher;
        //    57: aload_1        
        //    58: arraylength    
        //    59: invokevirtual   org/bouncycastle/crypto/paddings/PaddedBufferedBlockCipher.getOutputSize:(I)I
        //    62: newarray        B
        //    64: astore_3       
        //    65: aload_0        
        //    66: getfield        org/bouncycastle/crypto/examples/DESExample.cipher:Lorg/bouncycastle/crypto/paddings/PaddedBufferedBlockCipher;
        //    69: aload_1        
        //    70: iconst_0       
        //    71: aload_1        
        //    72: arraylength    
        //    73: aload_3        
        //    74: iconst_0       
        //    75: invokevirtual   org/bouncycastle/crypto/paddings/PaddedBufferedBlockCipher.processBytes:([BII[BI)I
        //    78: istore_2       
        //    79: aload_3        
        //    80: astore_1       
        //    81: iload_2        
        //    82: ifle            38
        //    85: aload_0        
        //    86: getfield        org/bouncycastle/crypto/examples/DESExample.out:Ljava/io/BufferedOutputStream;
        //    89: aload_3        
        //    90: iconst_0       
        //    91: iload_2        
        //    92: invokevirtual   java/io/BufferedOutputStream.write:([BII)V
        //    95: aload_3        
        //    96: astore_1       
        //    97: goto            38
        //   100: aload_0        
        //   101: getfield        org/bouncycastle/crypto/examples/DESExample.cipher:Lorg/bouncycastle/crypto/paddings/PaddedBufferedBlockCipher;
        //   104: aload_1        
        //   105: iconst_0       
        //   106: invokevirtual   org/bouncycastle/crypto/paddings/PaddedBufferedBlockCipher.doFinal:([BI)I
        //   109: istore_2       
        //   110: iload_2        
        //   111: ifle            130
        //   114: aload_0        
        //   115: getfield        org/bouncycastle/crypto/examples/DESExample.out:Ljava/io/BufferedOutputStream;
        //   118: aload_1        
        //   119: iconst_0       
        //   120: iload_2        
        //   121: invokevirtual   java/io/BufferedOutputStream.write:([BII)V
        //   124: return         
        //   125: astore_1       
        //   126: aload_1        
        //   127: invokevirtual   java/io/IOException.printStackTrace:()V
        //   130: return         
        //   131: astore_1       
        //   132: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                     
        //  -----  -----  -----  -----  -----------------------------------------
        //  38     44     125    130    Ljava/io/IOException;
        //  48     79     125    130    Ljava/io/IOException;
        //  85     95     125    130    Ljava/io/IOException;
        //  100    110    131    133    Lorg/bouncycastle/crypto/CryptoException;
        //  100    110    125    130    Ljava/io/IOException;
        //  114    124    131    133    Lorg/bouncycastle/crypto/CryptoException;
        //  114    124    125    130    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0100:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void performEncrypt(final byte[] p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        org/bouncycastle/crypto/examples/DESExample.cipher:Lorg/bouncycastle/crypto/paddings/PaddedBufferedBlockCipher;
        //     4: iconst_1       
        //     5: new             Lorg/bouncycastle/crypto/params/KeyParameter;
        //     8: dup            
        //     9: aload_1        
        //    10: invokespecial   org/bouncycastle/crypto/params/KeyParameter.<init>:([B)V
        //    13: invokevirtual   org/bouncycastle/crypto/paddings/PaddedBufferedBlockCipher.init:(ZLorg/bouncycastle/crypto/CipherParameters;)V
        //    16: aload_0        
        //    17: getfield        org/bouncycastle/crypto/examples/DESExample.cipher:Lorg/bouncycastle/crypto/paddings/PaddedBufferedBlockCipher;
        //    20: bipush          47
        //    22: invokevirtual   org/bouncycastle/crypto/paddings/PaddedBufferedBlockCipher.getOutputSize:(I)I
        //    25: istore_2       
        //    26: bipush          47
        //    28: newarray        B
        //    30: astore_1       
        //    31: iload_2        
        //    32: newarray        B
        //    34: astore_3       
        //    35: aload_0        
        //    36: getfield        org/bouncycastle/crypto/examples/DESExample.in:Ljava/io/BufferedInputStream;
        //    39: aload_1        
        //    40: iconst_0       
        //    41: bipush          47
        //    43: invokevirtual   java/io/BufferedInputStream.read:([BII)I
        //    46: istore_2       
        //    47: iload_2        
        //    48: ifle            101
        //    51: aload_0        
        //    52: getfield        org/bouncycastle/crypto/examples/DESExample.cipher:Lorg/bouncycastle/crypto/paddings/PaddedBufferedBlockCipher;
        //    55: aload_1        
        //    56: iconst_0       
        //    57: iload_2        
        //    58: aload_3        
        //    59: iconst_0       
        //    60: invokevirtual   org/bouncycastle/crypto/paddings/PaddedBufferedBlockCipher.processBytes:([BII[BI)I
        //    63: istore_2       
        //    64: iload_2        
        //    65: ifle            35
        //    68: aload_3        
        //    69: iconst_0       
        //    70: iload_2        
        //    71: invokestatic    org/bouncycastle/util/encoders/Hex.encode:([BII)[B
        //    74: astore          4
        //    76: aload_0        
        //    77: getfield        org/bouncycastle/crypto/examples/DESExample.out:Ljava/io/BufferedOutputStream;
        //    80: aload           4
        //    82: iconst_0       
        //    83: aload           4
        //    85: arraylength    
        //    86: invokevirtual   java/io/BufferedOutputStream.write:([BII)V
        //    89: aload_0        
        //    90: getfield        org/bouncycastle/crypto/examples/DESExample.out:Ljava/io/BufferedOutputStream;
        //    93: bipush          10
        //    95: invokevirtual   java/io/BufferedOutputStream.write:(I)V
        //    98: goto            35
        //   101: aload_0        
        //   102: getfield        org/bouncycastle/crypto/examples/DESExample.cipher:Lorg/bouncycastle/crypto/paddings/PaddedBufferedBlockCipher;
        //   105: aload_3        
        //   106: iconst_0       
        //   107: invokevirtual   org/bouncycastle/crypto/paddings/PaddedBufferedBlockCipher.doFinal:([BI)I
        //   110: istore_2       
        //   111: iload_2        
        //   112: ifle            148
        //   115: aload_3        
        //   116: iconst_0       
        //   117: iload_2        
        //   118: invokestatic    org/bouncycastle/util/encoders/Hex.encode:([BII)[B
        //   121: astore_1       
        //   122: aload_0        
        //   123: getfield        org/bouncycastle/crypto/examples/DESExample.out:Ljava/io/BufferedOutputStream;
        //   126: aload_1        
        //   127: iconst_0       
        //   128: aload_1        
        //   129: arraylength    
        //   130: invokevirtual   java/io/BufferedOutputStream.write:([BII)V
        //   133: aload_0        
        //   134: getfield        org/bouncycastle/crypto/examples/DESExample.out:Ljava/io/BufferedOutputStream;
        //   137: bipush          10
        //   139: invokevirtual   java/io/BufferedOutputStream.write:(I)V
        //   142: return         
        //   143: astore_1       
        //   144: aload_1        
        //   145: invokevirtual   java/io/IOException.printStackTrace:()V
        //   148: return         
        //   149: astore_1       
        //   150: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                     
        //  -----  -----  -----  -----  -----------------------------------------
        //  35     47     143    148    Ljava/io/IOException;
        //  51     64     143    148    Ljava/io/IOException;
        //  68     98     143    148    Ljava/io/IOException;
        //  101    111    149    151    Lorg/bouncycastle/crypto/CryptoException;
        //  101    111    143    148    Ljava/io/IOException;
        //  115    142    149    151    Lorg/bouncycastle/crypto/CryptoException;
        //  115    142    143    148    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0101:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void process() {
        this.cipher = new PaddedBufferedBlockCipher(new CBCBlockCipher(new DESedeEngine()));
        if (this.encrypt) {
            this.performEncrypt(this.key);
        }
        else {
            this.performDecrypt(this.key);
        }
        try {
            this.in.close();
            this.out.flush();
            this.out.close();
        }
        catch (IOException ex) {
            final PrintStream err = System.err;
            final StringBuilder sb = new StringBuilder();
            sb.append("exception closing resources: ");
            sb.append(ex.getMessage());
            err.println(sb.toString());
        }
    }
}
