// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.agreement;

import org.bouncycastle.crypto.params.DHMQVPublicParameters;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.DHPublicKeyParameters;
import org.bouncycastle.crypto.params.DHPrivateKeyParameters;
import org.bouncycastle.crypto.params.DHParameters;
import org.bouncycastle.crypto.params.DHMQVPrivateParameters;
import java.math.BigInteger;
import org.bouncycastle.crypto.BasicAgreement;

public class MQVBasicAgreement implements BasicAgreement
{
    private static final BigInteger ONE;
    DHMQVPrivateParameters privParams;
    
    static {
        ONE = BigInteger.valueOf(1L);
    }
    
    private BigInteger calculateDHMQVAgreement(final DHParameters dhParameters, final DHPrivateKeyParameters dhPrivateKeyParameters, final DHPublicKeyParameters dhPublicKeyParameters, final DHPrivateKeyParameters dhPrivateKeyParameters2, final DHPublicKeyParameters dhPublicKeyParameters2, final DHPublicKeyParameters dhPublicKeyParameters3) {
        final BigInteger q = dhParameters.getQ();
        final BigInteger pow = BigInteger.valueOf(2L).pow((q.bitLength() + 1) / 2);
        return dhPublicKeyParameters3.getY().multiply(dhPublicKeyParameters.getY().modPow(dhPublicKeyParameters3.getY().mod(pow).add(pow), dhParameters.getP())).modPow(dhPrivateKeyParameters2.getX().add(dhPublicKeyParameters2.getY().mod(pow).add(pow).multiply(dhPrivateKeyParameters.getX())).mod(q), dhParameters.getP());
    }
    
    @Override
    public BigInteger calculateAgreement(final CipherParameters cipherParameters) {
        final DHMQVPublicParameters dhmqvPublicParameters = (DHMQVPublicParameters)cipherParameters;
        final DHPrivateKeyParameters staticPrivateKey = this.privParams.getStaticPrivateKey();
        if (!this.privParams.getStaticPrivateKey().getParameters().equals(dhmqvPublicParameters.getStaticPublicKey().getParameters())) {
            throw new IllegalStateException("MQV public key components have wrong domain parameters");
        }
        if (this.privParams.getStaticPrivateKey().getParameters().getQ() == null) {
            throw new IllegalStateException("MQV key domain parameters do not have Q set");
        }
        final BigInteger calculateDHMQVAgreement = this.calculateDHMQVAgreement(staticPrivateKey.getParameters(), staticPrivateKey, dhmqvPublicParameters.getStaticPublicKey(), this.privParams.getEphemeralPrivateKey(), this.privParams.getEphemeralPublicKey(), dhmqvPublicParameters.getEphemeralPublicKey());
        if (!calculateDHMQVAgreement.equals(MQVBasicAgreement.ONE)) {
            return calculateDHMQVAgreement;
        }
        throw new IllegalStateException("1 is not a valid agreement value for MQV");
    }
    
    @Override
    public int getFieldSize() {
        return (this.privParams.getStaticPrivateKey().getParameters().getP().bitLength() + 7) / 8;
    }
    
    @Override
    public void init(final CipherParameters cipherParameters) {
        this.privParams = (DHMQVPrivateParameters)cipherParameters;
    }
}
