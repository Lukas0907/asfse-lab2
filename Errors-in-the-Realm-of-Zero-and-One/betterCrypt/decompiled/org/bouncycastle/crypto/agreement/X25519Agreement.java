// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.agreement;

import org.bouncycastle.crypto.params.X25519PublicKeyParameters;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.X25519PrivateKeyParameters;
import org.bouncycastle.crypto.RawAgreement;

public final class X25519Agreement implements RawAgreement
{
    private X25519PrivateKeyParameters privateKey;
    
    @Override
    public void calculateAgreement(final CipherParameters cipherParameters, final byte[] array, final int n) {
        this.privateKey.generateSecret((X25519PublicKeyParameters)cipherParameters, array, n);
    }
    
    @Override
    public int getAgreementSize() {
        return 32;
    }
    
    @Override
    public void init(final CipherParameters cipherParameters) {
        this.privateKey = (X25519PrivateKeyParameters)cipherParameters;
    }
}
