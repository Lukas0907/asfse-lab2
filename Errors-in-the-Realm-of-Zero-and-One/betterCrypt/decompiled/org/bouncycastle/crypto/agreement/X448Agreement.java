// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.agreement;

import org.bouncycastle.crypto.params.X448PublicKeyParameters;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.X448PrivateKeyParameters;
import org.bouncycastle.crypto.RawAgreement;

public final class X448Agreement implements RawAgreement
{
    private X448PrivateKeyParameters privateKey;
    
    @Override
    public void calculateAgreement(final CipherParameters cipherParameters, final byte[] array, final int n) {
        this.privateKey.generateSecret((X448PublicKeyParameters)cipherParameters, array, n);
    }
    
    @Override
    public int getAgreementSize() {
        return 56;
    }
    
    @Override
    public void init(final CipherParameters cipherParameters) {
        this.privateKey = (X448PrivateKeyParameters)cipherParameters;
    }
}
