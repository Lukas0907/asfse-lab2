// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.agreement.kdf;

import org.bouncycastle.crypto.DerivationParameters;

public class GSKKDFParameters implements DerivationParameters
{
    private final byte[] nonce;
    private final int startCounter;
    private final byte[] z;
    
    public GSKKDFParameters(final byte[] array, final int n) {
        this(array, n, null);
    }
    
    public GSKKDFParameters(final byte[] z, final int startCounter, final byte[] nonce) {
        this.z = z;
        this.startCounter = startCounter;
        this.nonce = nonce;
    }
    
    public byte[] getNonce() {
        return this.nonce;
    }
    
    public int getStartCounter() {
        return this.startCounter;
    }
    
    public byte[] getZ() {
        return this.z;
    }
}
