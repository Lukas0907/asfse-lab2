// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.agreement.kdf;

import org.bouncycastle.crypto.DerivationParameters;
import org.bouncycastle.crypto.DataLengthException;
import java.io.IOException;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.util.Pack;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.crypto.OutputLengthException;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.crypto.DerivationFunction;

public class DHKEKGenerator implements DerivationFunction
{
    private ASN1ObjectIdentifier algorithm;
    private final Digest digest;
    private int keySize;
    private byte[] partyAInfo;
    private byte[] z;
    
    public DHKEKGenerator(final Digest digest) {
        this.digest = digest;
    }
    
    @Override
    public int generateBytes(final byte[] array, int i, int n) throws DataLengthException, IllegalArgumentException {
        if (array.length - n < i) {
            throw new OutputLengthException("output buffer too small");
        }
        final long n2 = n;
        final int digestSize = this.digest.getDigestSize();
        if (n2 <= 8589934591L) {
            final long n3 = digestSize;
            final int n4 = (int)((n2 + n3 - 1L) / n3);
            final byte[] array2 = new byte[this.digest.getDigestSize()];
            final int n5 = 0;
            final int n6 = 1;
            int n7 = i;
            int n8 = n;
            n = n6;
            i = n5;
            while (i < n4) {
                final Digest digest = this.digest;
                final byte[] z = this.z;
                digest.update(z, 0, z.length);
                final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
                final ASN1EncodableVector asn1EncodableVector2 = new ASN1EncodableVector();
                asn1EncodableVector2.add(this.algorithm);
                asn1EncodableVector2.add(new DEROctetString(Pack.intToBigEndian(n)));
                asn1EncodableVector.add(new DERSequence(asn1EncodableVector2));
                final byte[] partyAInfo = this.partyAInfo;
                if (partyAInfo != null) {
                    asn1EncodableVector.add(new DERTaggedObject(true, 0, new DEROctetString(partyAInfo)));
                }
                asn1EncodableVector.add(new DERTaggedObject(true, 2, new DEROctetString(Pack.intToBigEndian(this.keySize))));
                try {
                    final byte[] encoded = new DERSequence(asn1EncodableVector).getEncoded("DER");
                    this.digest.update(encoded, 0, encoded.length);
                    this.digest.doFinal(array2, 0);
                    if (n8 > digestSize) {
                        System.arraycopy(array2, 0, array, n7, digestSize);
                        n7 += digestSize;
                        n8 -= digestSize;
                    }
                    else {
                        System.arraycopy(array2, 0, array, n7, n8);
                    }
                    ++n;
                    ++i;
                    continue;
                }
                catch (IOException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("unable to encode parameter info: ");
                    sb.append(ex.getMessage());
                    throw new IllegalArgumentException(sb.toString());
                }
                break;
            }
            this.digest.reset();
            return (int)n2;
        }
        throw new IllegalArgumentException("Output length too large");
    }
    
    public Digest getDigest() {
        return this.digest;
    }
    
    @Override
    public void init(final DerivationParameters derivationParameters) {
        final DHKDFParameters dhkdfParameters = (DHKDFParameters)derivationParameters;
        this.algorithm = dhkdfParameters.getAlgorithm();
        this.keySize = dhkdfParameters.getKeySize();
        this.z = dhkdfParameters.getZ();
        this.partyAInfo = dhkdfParameters.getExtraInfo();
    }
}
