// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.agreement.kdf;

import org.bouncycastle.crypto.DerivationParameters;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.util.Pack;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.DigestDerivationFunction;

public class GSKKFDGenerator implements DigestDerivationFunction
{
    private byte[] buf;
    private int counter;
    private final Digest digest;
    private byte[] r;
    private byte[] z;
    
    public GSKKFDGenerator(final Digest digest) {
        this.digest = digest;
        this.buf = new byte[digest.getDigestSize()];
    }
    
    @Override
    public int generateBytes(final byte[] array, final int n, final int n2) throws DataLengthException, IllegalArgumentException {
        if (n + n2 <= array.length) {
            final Digest digest = this.digest;
            final byte[] z = this.z;
            digest.update(z, 0, z.length);
            final byte[] intToBigEndian = Pack.intToBigEndian(this.counter++);
            this.digest.update(intToBigEndian, 0, intToBigEndian.length);
            final byte[] r = this.r;
            if (r != null) {
                this.digest.update(r, 0, r.length);
            }
            this.digest.doFinal(this.buf, 0);
            System.arraycopy(this.buf, 0, array, n, n2);
            Arrays.clear(this.buf);
            return n2;
        }
        throw new DataLengthException("output buffer too small");
    }
    
    @Override
    public Digest getDigest() {
        return this.digest;
    }
    
    @Override
    public void init(final DerivationParameters derivationParameters) {
        if (derivationParameters instanceof GSKKDFParameters) {
            final GSKKDFParameters gskkdfParameters = (GSKKDFParameters)derivationParameters;
            this.z = gskkdfParameters.getZ();
            this.counter = gskkdfParameters.getStartCounter();
            this.r = gskkdfParameters.getNonce();
            return;
        }
        throw new IllegalArgumentException("unkown parameters type");
    }
}
