// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.agreement;

import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.params.DHPublicKeyParameters;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.DHPrivateKeyParameters;
import org.bouncycastle.crypto.params.DHParameters;
import java.math.BigInteger;
import org.bouncycastle.crypto.BasicAgreement;

public class DHBasicAgreement implements BasicAgreement
{
    private static final BigInteger ONE;
    private DHParameters dhParams;
    private DHPrivateKeyParameters key;
    
    static {
        ONE = BigInteger.valueOf(1L);
    }
    
    @Override
    public BigInteger calculateAgreement(final CipherParameters cipherParameters) {
        final DHPublicKeyParameters dhPublicKeyParameters = (DHPublicKeyParameters)cipherParameters;
        if (!dhPublicKeyParameters.getParameters().equals(this.dhParams)) {
            throw new IllegalArgumentException("Diffie-Hellman public key has wrong parameters.");
        }
        final BigInteger p = this.dhParams.getP();
        final BigInteger y = dhPublicKeyParameters.getY();
        if (y == null || y.compareTo(DHBasicAgreement.ONE) <= 0 || y.compareTo(p.subtract(DHBasicAgreement.ONE)) >= 0) {
            throw new IllegalArgumentException("Diffie-Hellman public key is weak");
        }
        final BigInteger modPow = y.modPow(this.key.getX(), p);
        if (!modPow.equals(DHBasicAgreement.ONE)) {
            return modPow;
        }
        throw new IllegalStateException("Shared key can't be 1");
    }
    
    @Override
    public int getFieldSize() {
        return (this.key.getParameters().getP().bitLength() + 7) / 8;
    }
    
    @Override
    public void init(final CipherParameters cipherParameters) {
        CipherParameters parameters = cipherParameters;
        if (cipherParameters instanceof ParametersWithRandom) {
            parameters = ((ParametersWithRandom)cipherParameters).getParameters();
        }
        final AsymmetricKeyParameter asymmetricKeyParameter = (AsymmetricKeyParameter)parameters;
        if (asymmetricKeyParameter instanceof DHPrivateKeyParameters) {
            this.key = (DHPrivateKeyParameters)asymmetricKeyParameter;
            this.dhParams = this.key.getParameters();
            return;
        }
        throw new IllegalArgumentException("DHEngine expects DHPrivateKeyParameters");
    }
}
