// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.agreement;

import org.bouncycastle.crypto.params.XDHUPublicParameters;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.XDHUPrivateParameters;
import org.bouncycastle.crypto.RawAgreement;

public class XDHUnifiedAgreement implements RawAgreement
{
    private XDHUPrivateParameters privParams;
    private final RawAgreement xAgreement;
    
    public XDHUnifiedAgreement(final RawAgreement xAgreement) {
        this.xAgreement = xAgreement;
    }
    
    @Override
    public void calculateAgreement(final CipherParameters cipherParameters, final byte[] array, final int n) {
        final XDHUPublicParameters xdhuPublicParameters = (XDHUPublicParameters)cipherParameters;
        this.xAgreement.init(this.privParams.getEphemeralPrivateKey());
        this.xAgreement.calculateAgreement(xdhuPublicParameters.getEphemeralPublicKey(), array, n);
        this.xAgreement.init(this.privParams.getStaticPrivateKey());
        this.xAgreement.calculateAgreement(xdhuPublicParameters.getStaticPublicKey(), array, n + this.xAgreement.getAgreementSize());
    }
    
    @Override
    public int getAgreementSize() {
        return this.xAgreement.getAgreementSize() * 2;
    }
    
    @Override
    public void init(final CipherParameters cipherParameters) {
        this.privParams = (XDHUPrivateParameters)cipherParameters;
    }
}
