// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.agreement.jpake;

import java.math.BigInteger;

public class JPAKERound3Payload
{
    private final BigInteger macTag;
    private final String participantId;
    
    public JPAKERound3Payload(final String participantId, final BigInteger macTag) {
        this.participantId = participantId;
        this.macTag = macTag;
    }
    
    public BigInteger getMacTag() {
        return this.macTag;
    }
    
    public String getParticipantId() {
        return this.participantId;
    }
}
