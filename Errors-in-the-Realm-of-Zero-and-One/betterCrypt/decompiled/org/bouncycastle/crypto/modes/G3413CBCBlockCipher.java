// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes;

import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.BlockCipher;

public class G3413CBCBlockCipher implements BlockCipher
{
    private byte[] R;
    private byte[] R_init;
    private int blockSize;
    private BlockCipher cipher;
    private boolean forEncryption;
    private boolean initialized;
    private int m;
    
    public G3413CBCBlockCipher(final BlockCipher cipher) {
        this.initialized = false;
        this.blockSize = cipher.getBlockSize();
        this.cipher = cipher;
    }
    
    private int decrypt(byte[] copyFromInput, final int n, final byte[] array, final int n2) {
        final byte[] msb = GOST3413CipherUtil.MSB(this.R, this.blockSize);
        copyFromInput = GOST3413CipherUtil.copyFromInput(copyFromInput, this.blockSize, n);
        final byte[] array2 = new byte[copyFromInput.length];
        this.cipher.processBlock(copyFromInput, 0, array2, 0);
        final byte[] sum = GOST3413CipherUtil.sum(array2, msb);
        System.arraycopy(sum, 0, array, n2, sum.length);
        if (array.length > n2 + sum.length) {
            this.generateR(copyFromInput);
        }
        return sum.length;
    }
    
    private int encrypt(byte[] sum, final int n, final byte[] array, final int n2) {
        sum = GOST3413CipherUtil.sum(GOST3413CipherUtil.copyFromInput(sum, this.blockSize, n), GOST3413CipherUtil.MSB(this.R, this.blockSize));
        final byte[] array2 = new byte[sum.length];
        this.cipher.processBlock(sum, 0, array2, 0);
        System.arraycopy(array2, 0, array, n2, array2.length);
        if (array.length > n2 + sum.length) {
            this.generateR(array2);
        }
        return array2.length;
    }
    
    private void generateR(final byte[] array) {
        final byte[] lsb = GOST3413CipherUtil.LSB(this.R, this.m - this.blockSize);
        System.arraycopy(lsb, 0, this.R, 0, lsb.length);
        System.arraycopy(array, 0, this.R, lsb.length, this.m - lsb.length);
    }
    
    private void initArrays() {
        final int m = this.m;
        this.R = new byte[m];
        this.R_init = new byte[m];
    }
    
    private void setupDefaultParams() {
        this.m = this.blockSize;
    }
    
    @Override
    public String getAlgorithmName() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.cipher.getAlgorithmName());
        sb.append("/CBC");
        return sb.toString();
    }
    
    @Override
    public int getBlockSize() {
        return this.blockSize;
    }
    
    @Override
    public void init(final boolean forEncryption, final CipherParameters cipherParameters) throws IllegalArgumentException {
        this.forEncryption = forEncryption;
        Label_0144: {
            BlockCipher cipher;
            CipherParameters parameters;
            if (cipherParameters instanceof ParametersWithIV) {
                final ParametersWithIV parametersWithIV = (ParametersWithIV)cipherParameters;
                final byte[] iv = parametersWithIV.getIV();
                if (iv.length < this.blockSize) {
                    throw new IllegalArgumentException("Parameter m must blockSize <= m");
                }
                this.m = iv.length;
                this.initArrays();
                this.R_init = Arrays.clone(iv);
                final byte[] r_init = this.R_init;
                System.arraycopy(r_init, 0, this.R, 0, r_init.length);
                if (parametersWithIV.getParameters() == null) {
                    break Label_0144;
                }
                cipher = this.cipher;
                parameters = parametersWithIV.getParameters();
            }
            else {
                this.setupDefaultParams();
                this.initArrays();
                final byte[] r_init2 = this.R_init;
                System.arraycopy(r_init2, 0, this.R, 0, r_init2.length);
                if (cipherParameters == null) {
                    break Label_0144;
                }
                final BlockCipher cipher2 = this.cipher;
                parameters = cipherParameters;
                cipher = cipher2;
            }
            cipher.init(forEncryption, parameters);
        }
        this.initialized = true;
    }
    
    @Override
    public int processBlock(final byte[] array, final int n, final byte[] array2, final int n2) throws DataLengthException, IllegalStateException {
        if (this.forEncryption) {
            return this.encrypt(array, n, array2, n2);
        }
        return this.decrypt(array, n, array2, n2);
    }
    
    @Override
    public void reset() {
        if (this.initialized) {
            final byte[] r_init = this.R_init;
            System.arraycopy(r_init, 0, this.R, 0, r_init.length);
            this.cipher.reset();
        }
    }
}
