// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes.kgcm;

import org.bouncycastle.math.raw.Interleave;

public class KGCMUtil_256
{
    public static final int SIZE = 4;
    
    public static void add(final long[] array, final long[] array2, final long[] array3) {
        array3[0] = (array[0] ^ array2[0]);
        array3[1] = (array[1] ^ array2[1]);
        array3[2] = (array[2] ^ array2[2]);
        array3[3] = (array2[3] ^ array[3]);
    }
    
    public static void copy(final long[] array, final long[] array2) {
        array2[0] = array[0];
        array2[1] = array[1];
        array2[2] = array[2];
        array2[3] = array[3];
    }
    
    public static boolean equal(final long[] array, final long[] array2) {
        boolean b = false;
        if (((array2[3] ^ array[3]) | ((array[0] ^ array2[0]) | 0x0L | (array[1] ^ array2[1]) | (array[2] ^ array2[2]))) == 0x0L) {
            b = true;
        }
        return b;
    }
    
    public static void multiply(final long[] array, final long[] array2, final long[] array3) {
        long n = array[0];
        long n2 = array[1];
        final long n3 = array[2];
        final long n4 = array[3];
        long n5 = array2[0];
        long n6 = array2[1];
        long n7 = array2[2];
        long n8 = array2[3];
        long n9 = 0L;
        long n11;
        long n10 = n11 = 0L;
        long n13;
        long n12 = n13 = n11;
        int n14 = 0;
        long n15;
        while (true) {
            n15 = n8;
            if (n14 >= 64) {
                break;
            }
            final long n16 = -(n & 0x1L);
            n >>>= 1;
            n9 ^= (n5 & n16);
            final long n17 = -(n2 & 0x1L);
            n2 >>>= 1;
            n10 = (n10 ^ (n6 & n16) ^ (n5 & n17));
            n11 = (n11 ^ (n7 & n16) ^ (n6 & n17));
            n12 = (n12 ^ (n15 & n16) ^ (n7 & n17));
            n13 ^= (n15 & n17);
            n8 = (n15 << 1 | n7 >>> 63);
            n7 = (n7 << 1 | n6 >>> 63);
            n6 = (n5 >>> 63 | n6 << 1);
            n5 = (n5 << 1 ^ (n15 >> 63 & 0x425L));
            ++n14;
        }
        final long n18 = n3;
        final long n19 = n4;
        final long n20 = n15 ^ n15 << 2 ^ n15 << 5 ^ n15 << 10;
        final long n21 = n7;
        long n22 = n6;
        final long n23 = n15 >>> 62 ^ n5 ^ n15 >>> 59 ^ n15 >>> 54;
        int i = 0;
        long n24 = n18;
        long n25 = n19;
        long n26 = n13;
        long n27 = n9;
        long n28 = n21;
        long n29 = n23;
        long n30 = n20;
        while (i < 64) {
            final long n31 = -(n24 & 0x1L);
            n24 >>>= 1;
            n27 ^= (n30 & n31);
            final long n32 = -(n25 & 0x1L);
            n25 >>>= 1;
            n10 = (n10 ^ (n29 & n31) ^ (n30 & n32));
            n11 = (n11 ^ (n22 & n31) ^ (n29 & n32));
            n12 = (n12 ^ (n28 & n31) ^ (n22 & n32));
            n26 ^= (n28 & n32);
            final long n33 = n28 << 1 | n22 >>> 63;
            n22 = (n29 >>> 63 | n22 << 1);
            n29 = (n30 >>> 63 | n29 << 1);
            n30 = (n30 << 1 ^ (n28 >> 63 & 0x425L));
            ++i;
            n28 = n33;
        }
        array3[0] = (n27 ^ (n26 ^ n26 << 2 ^ n26 << 5 ^ n26 << 10));
        array3[1] = (n10 ^ (n26 >>> 62 ^ n26 >>> 59 ^ n26 >>> 54));
        array3[2] = n11;
        array3[3] = n12;
    }
    
    public static void multiplyX(final long[] array, final long[] array2) {
        final long n = array[0];
        final long n2 = array[1];
        final long n3 = array[2];
        final long n4 = array[3];
        array2[0] = ((n4 >> 63 & 0x425L) ^ n << 1);
        array2[1] = (n >>> 63 | n2 << 1);
        array2[2] = (n3 << 1 | n2 >>> 63);
        array2[3] = (n4 << 1 | n3 >>> 63);
    }
    
    public static void multiplyX8(final long[] array, final long[] array2) {
        final long n = array[0];
        final long n2 = array[1];
        final long n3 = array[2];
        final long n4 = array[3];
        final long n5 = n4 >>> 56;
        array2[0] = (n << 8 ^ n5 ^ n5 << 2 ^ n5 << 5 ^ n5 << 10);
        array2[1] = (n >>> 56 | n2 << 8);
        array2[2] = (n3 << 8 | n2 >>> 56);
        array2[3] = (n4 << 8 | n3 >>> 56);
    }
    
    public static void one(final long[] array) {
        array[0] = 1L;
        array[1] = 0L;
        array[3] = (array[2] = 0L);
    }
    
    public static void square(final long[] array, final long[] array2) {
        final int n = 8;
        final long[] array3 = new long[8];
        int n2 = 0;
        int n3;
        while (true) {
            n3 = n;
            if (n2 >= 4) {
                break;
            }
            Interleave.expand64To128(array[n2], array3, n2 << 1);
            ++n2;
        }
        while (true) {
            --n3;
            if (n3 < 4) {
                break;
            }
            final long n4 = array3[n3];
            final int n5 = n3 - 4;
            array3[n5] ^= (n4 << 2 ^ n4 ^ n4 << 5 ^ n4 << 10);
            final int n6 = n5 + 1;
            array3[n6] ^= (n4 >>> 54 ^ (n4 >>> 62 ^ n4 >>> 59));
        }
        copy(array3, array2);
    }
    
    public static void x(final long[] array) {
        array[0] = 2L;
        array[1] = 0L;
        array[3] = (array[2] = 0L);
    }
    
    public static void zero(final long[] array) {
        array[1] = (array[0] = 0L);
        array[3] = (array[2] = 0L);
    }
}
