// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes.kgcm;

import org.bouncycastle.math.raw.Interleave;

public class KGCMUtil_512
{
    public static final int SIZE = 8;
    
    public static void add(final long[] array, final long[] array2, final long[] array3) {
        array3[0] = (array[0] ^ array2[0]);
        array3[1] = (array[1] ^ array2[1]);
        array3[2] = (array[2] ^ array2[2]);
        array3[3] = (array[3] ^ array2[3]);
        array3[4] = (array[4] ^ array2[4]);
        array3[5] = (array[5] ^ array2[5]);
        array3[6] = (array[6] ^ array2[6]);
        array3[7] = (array2[7] ^ array[7]);
    }
    
    public static void copy(final long[] array, final long[] array2) {
        array2[0] = array[0];
        array2[1] = array[1];
        array2[2] = array[2];
        array2[3] = array[3];
        array2[4] = array[4];
        array2[5] = array[5];
        array2[6] = array[6];
        array2[7] = array[7];
    }
    
    public static boolean equal(final long[] array, final long[] array2) {
        boolean b = false;
        if (((array2[7] ^ array[7]) | ((array[0] ^ array2[0]) | 0x0L | (array[1] ^ array2[1]) | (array[2] ^ array2[2]) | (array[3] ^ array2[3]) | (array[4] ^ array2[4]) | (array[5] ^ array2[5]) | (array[6] ^ array2[6]))) == 0x0L) {
            b = true;
        }
        return b;
    }
    
    public static void multiply(final long[] array, final long[] array2, final long[] array3) {
        long n = array2[0];
        long n2 = array2[1];
        long n3 = array2[2];
        long n4 = array2[3];
        long n5 = array2[4];
        long n6 = array2[5];
        long n7 = array2[6];
        long n8 = array2[7];
        long n9 = 0L;
        long n11;
        final long n10 = n11 = 0L;
        long n13;
        long n12 = n13 = n11;
        long n15;
        long n14 = n15 = n13;
        long n17;
        final long n16 = n17 = n15;
        int i = 0;
        long n18 = n16;
        long n19 = n10;
        while (i < 8) {
            long n20 = array[i];
            long n21 = array[i + 1];
            final long n22 = n14;
            final long n23 = n11;
            final long n24 = n19;
            int n25 = 0;
            long n26 = n15;
            long n27 = n8;
            long n28 = n;
            long n29 = n22;
            long n30 = n13;
            long n31 = n23;
            long n32 = n24;
            long n33;
            while (true) {
                n33 = n27;
                if (n25 >= 64) {
                    break;
                }
                final long n34 = -(n20 & 0x1L);
                n20 >>>= 1;
                n9 ^= (n28 & n34);
                final long n35 = -(n21 & 0x1L);
                n21 >>>= 1;
                n31 = (n31 ^ (n2 & n34) ^ (n28 & n35));
                n12 = (n12 ^ (n3 & n34) ^ (n2 & n35));
                n30 = (n30 ^ (n4 & n34) ^ (n3 & n35));
                n29 = (n29 ^ (n5 & n34) ^ (n4 & n35));
                n26 = (n26 ^ (n6 & n34) ^ (n5 & n35));
                n18 = (n18 ^ (n7 & n34) ^ (n6 & n35));
                n17 = (n17 ^ (n33 & n34) ^ (n7 & n35));
                n32 ^= (n33 & n35);
                n27 = (n33 << 1 | n7 >>> 63);
                n7 = (n7 << 1 | n6 >>> 63);
                n6 = (n6 << 1 | n5 >>> 63);
                n5 = (n5 << 1 | n4 >>> 63);
                n4 = (n4 << 1 | n3 >>> 63);
                n3 = (n3 << 1 | n2 >>> 63);
                n2 = (n2 << 1 | n28 >>> 63);
                n28 = (n28 << 1 ^ (n33 >> 63 & 0x125L));
                ++n25;
            }
            final long n36 = n33 ^ n33 << 2 ^ n33 << 5 ^ n33 << 8;
            final long n37 = n30;
            final long n38 = n29;
            n8 = n7;
            final long n39 = n26;
            n7 = n6;
            n6 = n5;
            n5 = n4;
            n4 = n3;
            n3 = n2;
            n2 = (n28 ^ n33 >>> 62 ^ n33 >>> 59 ^ n33 >>> 56);
            i += 2;
            n19 = n32;
            n11 = n31;
            n13 = n37;
            n14 = n38;
            n15 = n39;
            n = n36;
        }
        array3[0] = (n9 ^ (n19 << 2 ^ n19 ^ n19 << 5 ^ n19 << 8));
        array3[1] = (n19 >>> 62 ^ n19 >>> 59 ^ n19 >>> 56 ^ n11);
        array3[2] = n12;
        array3[3] = n13;
        array3[4] = n14;
        array3[5] = n15;
        array3[6] = n18;
        array3[7] = n17;
    }
    
    public static void multiplyX(final long[] array, final long[] array2) {
        final long n = array[0];
        final long n2 = array[1];
        final long n3 = array[2];
        final long n4 = array[3];
        final long n5 = array[4];
        final long n6 = array[5];
        final long n7 = array[6];
        final long n8 = array[7];
        array2[0] = (n << 1 ^ (n8 >> 63 & 0x125L));
        array2[1] = (n2 << 1 | n >>> 63);
        array2[2] = (n3 << 1 | n2 >>> 63);
        array2[3] = (n4 << 1 | n3 >>> 63);
        array2[4] = (n5 << 1 | n4 >>> 63);
        array2[5] = (n6 << 1 | n5 >>> 63);
        array2[6] = (n7 << 1 | n6 >>> 63);
        array2[7] = (n8 << 1 | n7 >>> 63);
    }
    
    public static void multiplyX8(final long[] array, final long[] array2) {
        final long n = array[0];
        final long n2 = array[1];
        final long n3 = array[2];
        final long n4 = array[3];
        final long n5 = array[4];
        final long n6 = array[5];
        final long n7 = array[6];
        final long n8 = array[7];
        final long n9 = n8 >>> 56;
        array2[0] = (n << 8 ^ n9 ^ n9 << 2 ^ n9 << 5 ^ n9 << 8);
        array2[1] = (n2 << 8 | n >>> 56);
        array2[2] = (n3 << 8 | n2 >>> 56);
        array2[3] = (n4 << 8 | n3 >>> 56);
        array2[4] = (n5 << 8 | n4 >>> 56);
        array2[5] = (n6 << 8 | n5 >>> 56);
        array2[6] = (n7 << 8 | n6 >>> 56);
        array2[7] = (n8 << 8 | n7 >>> 56);
    }
    
    public static void one(final long[] array) {
        array[0] = 1L;
        array[1] = 0L;
        array[3] = (array[2] = 0L);
        array[5] = (array[4] = 0L);
        array[7] = (array[6] = 0L);
    }
    
    public static void square(final long[] array, final long[] array2) {
        final int n = 16;
        final long[] array3 = new long[16];
        int n2 = 0;
        int n3;
        while (true) {
            n3 = n;
            if (n2 >= 8) {
                break;
            }
            Interleave.expand64To128(array[n2], array3, n2 << 1);
            ++n2;
        }
        while (true) {
            --n3;
            if (n3 < 8) {
                break;
            }
            final long n4 = array3[n3];
            final int n5 = n3 - 8;
            array3[n5] ^= (n4 << 2 ^ n4 ^ n4 << 5 ^ n4 << 8);
            final int n6 = n5 + 1;
            array3[n6] ^= (n4 >>> 56 ^ (n4 >>> 62 ^ n4 >>> 59));
        }
        copy(array3, array2);
    }
    
    public static void x(final long[] array) {
        array[0] = 2L;
        array[1] = 0L;
        array[3] = (array[2] = 0L);
        array[5] = (array[4] = 0L);
        array[7] = (array[6] = 0L);
    }
    
    public static void zero(final long[] array) {
        array[1] = (array[0] = 0L);
        array[3] = (array[2] = 0L);
        array[5] = (array[4] = 0L);
        array[7] = (array[6] = 0L);
    }
}
