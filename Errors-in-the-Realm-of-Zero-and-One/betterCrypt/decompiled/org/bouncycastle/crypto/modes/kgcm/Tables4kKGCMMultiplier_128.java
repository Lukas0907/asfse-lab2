// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes.kgcm;

import java.lang.reflect.Array;

public class Tables4kKGCMMultiplier_128 implements KGCMMultiplier
{
    private long[][] T;
    
    @Override
    public void init(final long[] array) {
        final long[][] t = this.T;
        int i = 2;
        if (t == null) {
            this.T = (long[][])Array.newInstance(Long.TYPE, 256, 2);
        }
        else if (KGCMUtil_128.equal(array, t[1])) {
            return;
        }
        KGCMUtil_128.copy(array, this.T[1]);
        while (i < 256) {
            final long[][] t2 = this.T;
            KGCMUtil_128.multiplyX(t2[i >> 1], t2[i]);
            final long[][] t3 = this.T;
            KGCMUtil_128.add(t3[i], t3[1], t3[i + 1]);
            i += 2;
        }
    }
    
    @Override
    public void multiplyH(final long[] array) {
        final long[] array2 = new long[2];
        KGCMUtil_128.copy(this.T[(int)(array[1] >>> 56) & 0xFF], array2);
        for (int i = 14; i >= 0; --i) {
            KGCMUtil_128.multiplyX8(array2, array2);
            KGCMUtil_128.add(this.T[(int)(array[i >>> 3] >>> ((i & 0x7) << 3)) & 0xFF], array2, array2);
        }
        KGCMUtil_128.copy(array2, array);
    }
}
