// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes.kgcm;

import java.lang.reflect.Array;

public class Tables16kKGCMMultiplier_512 implements KGCMMultiplier
{
    private long[][] T;
    
    @Override
    public void init(final long[] array) {
        final long[][] t = this.T;
        if (t == null) {
            this.T = (long[][])Array.newInstance(Long.TYPE, 256, 8);
        }
        else if (KGCMUtil_512.equal(array, t[1])) {
            return;
        }
        KGCMUtil_512.copy(array, this.T[1]);
        for (int i = 2; i < 256; i += 2) {
            final long[][] t2 = this.T;
            KGCMUtil_512.multiplyX(t2[i >> 1], t2[i]);
            final long[][] t3 = this.T;
            KGCMUtil_512.add(t3[i], t3[1], t3[i + 1]);
        }
    }
    
    @Override
    public void multiplyH(final long[] array) {
        final long[] array2 = new long[8];
        KGCMUtil_512.copy(this.T[(int)(array[7] >>> 56) & 0xFF], array2);
        for (int i = 62; i >= 0; --i) {
            KGCMUtil_512.multiplyX8(array2, array2);
            KGCMUtil_512.add(this.T[(int)(array[i >>> 3] >>> ((i & 0x7) << 3)) & 0xFF], array2, array2);
        }
        KGCMUtil_512.copy(array2, array);
    }
}
