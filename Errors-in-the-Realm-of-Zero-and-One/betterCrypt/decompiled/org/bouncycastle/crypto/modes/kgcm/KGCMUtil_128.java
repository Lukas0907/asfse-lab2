// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes.kgcm;

import org.bouncycastle.math.raw.Interleave;

public class KGCMUtil_128
{
    public static final int SIZE = 2;
    
    public static void add(final long[] array, final long[] array2, final long[] array3) {
        array3[0] = (array[0] ^ array2[0]);
        array3[1] = (array2[1] ^ array[1]);
    }
    
    public static void copy(final long[] array, final long[] array2) {
        array2[0] = array[0];
        array2[1] = array[1];
    }
    
    public static boolean equal(final long[] array, final long[] array2) {
        boolean b = false;
        if (((array2[1] ^ array[1]) | ((array[0] ^ array2[0]) | 0x0L)) == 0x0L) {
            b = true;
        }
        return b;
    }
    
    public static void multiply(final long[] array, final long[] array2, final long[] array3) {
        long n = array[0];
        long n2 = array[1];
        long n3 = array2[0];
        long n4 = array2[1];
        long n5 = 0L;
        long n7;
        long n6 = n7 = 0L;
        long n8;
        long n9;
        long n10;
        long n11;
        for (int i = 0; i < 64; ++i, n7 = ((n8 & n4) ^ n7 ^ (n3 & n9)), n3 = n11, n4 = n10) {
            n8 = -(n & 0x1L);
            n >>>= 1;
            n5 ^= (n3 & n8);
            n9 = -(n2 & 0x1L);
            n2 >>>= 1;
            n6 ^= (n9 & n4);
            n10 = (n4 << 1 | n3 >>> 63);
            n11 = (n3 << 1 ^ (n4 >> 63 & 0x87L));
        }
        array3[0] = (n6 << 1 ^ n6 ^ n6 << 2 ^ n6 << 7 ^ n5);
        array3[1] = (n6 >>> 57 ^ (n6 >>> 63 ^ n6 >>> 62) ^ n7);
    }
    
    public static void multiplyX(final long[] array, final long[] array2) {
        final long n = array[0];
        final long n2 = array[1];
        array2[0] = ((n2 >> 63 & 0x87L) ^ n << 1);
        array2[1] = (n >>> 63 | n2 << 1);
    }
    
    public static void multiplyX8(final long[] array, final long[] array2) {
        final long n = array[0];
        final long n2 = array[1];
        final long n3 = n2 >>> 56;
        array2[0] = (n3 << 7 ^ (n << 8 ^ n3 ^ n3 << 1 ^ n3 << 2));
        array2[1] = (n >>> 56 | n2 << 8);
    }
    
    public static void one(final long[] array) {
        array[0] = 1L;
        array[1] = 0L;
    }
    
    public static void square(final long[] array, final long[] array2) {
        final long[] array3 = new long[4];
        Interleave.expand64To128(array[0], array3, 0);
        Interleave.expand64To128(array[1], array3, 2);
        final long n = array3[0];
        final long n2 = array3[1];
        final long n3 = array3[2];
        final long n4 = array3[3];
        final long n5 = n3 ^ (n4 >>> 57 ^ (n4 >>> 63 ^ n4 >>> 62));
        array2[0] = (n ^ (n5 << 1 ^ n5 ^ n5 << 2 ^ n5 << 7));
        array2[1] = (n2 ^ (n4 << 1 ^ n4 ^ n4 << 2 ^ n4 << 7) ^ (n5 >>> 57 ^ (n5 >>> 63 ^ n5 >>> 62)));
    }
    
    public static void x(final long[] array) {
        array[0] = 2L;
        array[1] = 0L;
    }
    
    public static void zero(final long[] array) {
        array[1] = (array[0] = 0L);
    }
}
