// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes.gcm;

public interface GCMExponentiator
{
    void exponentiateX(final long p0, final byte[] p1);
    
    void init(final byte[] p0);
}
