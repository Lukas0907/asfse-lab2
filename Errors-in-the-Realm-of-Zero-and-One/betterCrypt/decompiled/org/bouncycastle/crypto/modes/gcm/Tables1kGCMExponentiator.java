// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes.gcm;

import org.bouncycastle.util.Arrays;
import java.util.Vector;

public class Tables1kGCMExponentiator implements GCMExponentiator
{
    private Vector lookupPowX2;
    
    private void ensureAvailable(final int n) {
        int size = this.lookupPowX2.size();
        if (size <= n) {
            long[] clone = this.lookupPowX2.elementAt(size - 1);
            do {
                clone = Arrays.clone(clone);
                GCMUtil.square(clone, clone);
                this.lookupPowX2.addElement(clone);
            } while (++size <= n);
        }
    }
    
    @Override
    public void exponentiateX(long n, final byte[] array) {
        final long[] oneAsLongs = GCMUtil.oneAsLongs();
        int index = 0;
        while (n > 0L) {
            if ((0x1L & n) != 0x0L) {
                this.ensureAvailable(index);
                GCMUtil.multiply(oneAsLongs, (long[])this.lookupPowX2.elementAt(index));
            }
            ++index;
            n >>>= 1;
        }
        GCMUtil.asBytes(oneAsLongs, array);
    }
    
    @Override
    public void init(final byte[] array) {
        final long[] longs = GCMUtil.asLongs(array);
        final Vector lookupPowX2 = this.lookupPowX2;
        if (lookupPowX2 != null && Arrays.areEqual(longs, lookupPowX2.elementAt(0))) {
            return;
        }
        (this.lookupPowX2 = new Vector(8)).addElement(longs);
    }
}
