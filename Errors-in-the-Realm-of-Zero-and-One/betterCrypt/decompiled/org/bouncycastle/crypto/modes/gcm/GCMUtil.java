// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes.gcm;

import org.bouncycastle.math.raw.Interleave;
import org.bouncycastle.util.Pack;

public abstract class GCMUtil
{
    private static final int E1 = -520093696;
    private static final long E1L = -2233785415175766016L;
    
    public static void asBytes(final int[] array, final byte[] array2) {
        Pack.intToBigEndian(array, array2, 0);
    }
    
    public static void asBytes(final long[] array, final byte[] array2) {
        Pack.longToBigEndian(array, array2, 0);
    }
    
    public static byte[] asBytes(final int[] array) {
        final byte[] array2 = new byte[16];
        Pack.intToBigEndian(array, array2, 0);
        return array2;
    }
    
    public static byte[] asBytes(final long[] array) {
        final byte[] array2 = new byte[16];
        Pack.longToBigEndian(array, array2, 0);
        return array2;
    }
    
    public static void asInts(final byte[] array, final int[] array2) {
        Pack.bigEndianToInt(array, 0, array2);
    }
    
    public static int[] asInts(final byte[] array) {
        final int[] array2 = new int[4];
        Pack.bigEndianToInt(array, 0, array2);
        return array2;
    }
    
    public static void asLongs(final byte[] array, final long[] array2) {
        Pack.bigEndianToLong(array, 0, array2);
    }
    
    public static long[] asLongs(final byte[] array) {
        final long[] array2 = new long[2];
        Pack.bigEndianToLong(array, 0, array2);
        return array2;
    }
    
    public static void copy(final int[] array, final int[] array2) {
        array2[0] = array[0];
        array2[1] = array[1];
        array2[2] = array[2];
        array2[3] = array[3];
    }
    
    public static void copy(final long[] array, final long[] array2) {
        array2[0] = array[0];
        array2[1] = array[1];
    }
    
    public static void divideP(final long[] array, final long[] array2) {
        final long n = array[0];
        final long n2 = array[1];
        final long n3 = n >> 63;
        array2[0] = ((n ^ (0xE100000000000000L & n3)) << 1 | n2 >>> 63);
        array2[1] = (n2 << 1 | -n3);
    }
    
    public static void multiply(final byte[] array, final byte[] array2) {
        final long[] longs = asLongs(array);
        multiply(longs, asLongs(array2));
        asBytes(longs, array);
    }
    
    public static void multiply(final int[] array, final int[] array2) {
        int n = array2[0];
        int n2 = array2[1];
        int n3 = array2[2];
        int n4 = array2[3];
        int n5 = 0;
        int n6;
        int i = n6 = n5;
        int n8;
        int n7 = n8 = n6;
        while (i < 4) {
            int n9 = array[i];
            final int n10 = n8;
            final int n11 = n7;
            final int n12 = n6;
            final int n13 = 0;
            int n14 = n3;
            int n15 = n2;
            int n16 = n;
            int n17 = n10;
            int n18 = n11;
            int n19 = n12;
            int n21;
            for (int j = n13; j < 32; ++j, n4 = n21) {
                final int n20 = n9 >> 31;
                n9 <<= 1;
                n19 ^= (n16 & n20);
                n18 ^= (n15 & n20);
                n17 ^= (n14 & n20);
                n5 ^= (n20 & n4);
                n21 = (n4 >>> 1 | n14 << 31);
                n14 = (n14 >>> 1 | n15 << 31);
                n15 = (n15 >>> 1 | n16 << 31);
                n16 = (n16 >>> 1 ^ (n4 << 31 >> 8 & 0xE1000000));
            }
            ++i;
            final int n22 = n16;
            final int n23 = n15;
            final int n24 = n14;
            n6 = n19;
            n7 = n18;
            n8 = n17;
            n = n22;
            n2 = n23;
            n3 = n24;
        }
        array[0] = n6;
        array[1] = n7;
        array[2] = n8;
        array[3] = n5;
    }
    
    public static void multiply(final long[] array, final long[] array2) {
        long n = array[0];
        long n2 = array[1];
        long n3 = array2[0];
        long n4 = array2[1];
        long n5 = 0L;
        long n7;
        long n6 = n7 = 0L;
        long n10;
        for (int i = 0; i < 64; ++i, n4 = n10) {
            final long n8 = n >> 63;
            n <<= 1;
            n5 ^= (n3 & n8);
            final long n9 = n2 >> 63;
            n2 <<= 1;
            n7 = (n7 ^ (n4 & n8) ^ (n3 & n9));
            n6 ^= (n4 & n9);
            n10 = (n4 >>> 1 | n3 << 63);
            n3 = (n3 >>> 1 ^ (n4 << 63 >> 8 & 0xE100000000000000L));
        }
        array[0] = (n6 >>> 1 ^ n6 ^ n6 >>> 2 ^ n6 >>> 7 ^ n5);
        array[1] = (n6 << 57 ^ (n6 << 63 ^ n6 << 62) ^ n7);
    }
    
    public static void multiplyP(final int[] array) {
        final int n = array[0];
        final int n2 = array[1];
        final int n3 = array[2];
        final int n4 = array[3];
        array[0] = ((n4 << 31 >> 31 & 0xE1000000) ^ n >>> 1);
        array[1] = (n2 >>> 1 | n << 31);
        array[2] = (n3 >>> 1 | n2 << 31);
        array[3] = (n4 >>> 1 | n3 << 31);
    }
    
    public static void multiplyP(final int[] array, final int[] array2) {
        final int n = array[0];
        final int n2 = array[1];
        final int n3 = array[2];
        final int n4 = array[3];
        array2[0] = ((n4 << 31 >> 31 & 0xE1000000) ^ n >>> 1);
        array2[1] = (n2 >>> 1 | n << 31);
        array2[2] = (n3 >>> 1 | n2 << 31);
        array2[3] = (n4 >>> 1 | n3 << 31);
    }
    
    public static void multiplyP(final long[] array) {
        final long n = array[0];
        final long n2 = array[1];
        array[0] = ((n2 << 63 >> 63 & 0xE100000000000000L) ^ n >>> 1);
        array[1] = (n << 63 | n2 >>> 1);
    }
    
    public static void multiplyP(final long[] array, final long[] array2) {
        final long n = array[0];
        final long n2 = array[1];
        array2[0] = ((n2 << 63 >> 63 & 0xE100000000000000L) ^ n >>> 1);
        array2[1] = (n << 63 | n2 >>> 1);
    }
    
    public static void multiplyP3(final long[] array, final long[] array2) {
        final long n = array[0];
        final long n2 = array[1];
        final long n3 = n2 << 61;
        array2[0] = (n3 >>> 7 ^ (n >>> 3 ^ n3 ^ n3 >>> 1 ^ n3 >>> 2));
        array2[1] = (n << 61 | n2 >>> 3);
    }
    
    public static void multiplyP4(final long[] array, final long[] array2) {
        final long n = array[0];
        final long n2 = array[1];
        final long n3 = n2 << 60;
        array2[0] = (n3 >>> 7 ^ (n >>> 4 ^ n3 ^ n3 >>> 1 ^ n3 >>> 2));
        array2[1] = (n << 60 | n2 >>> 4);
    }
    
    public static void multiplyP7(final long[] array, final long[] array2) {
        final long n = array[0];
        final long n2 = array[1];
        final long n3 = n2 << 57;
        array2[0] = (n3 >>> 7 ^ (n >>> 7 ^ n3 ^ n3 >>> 1 ^ n3 >>> 2));
        array2[1] = (n << 57 | n2 >>> 7);
    }
    
    public static void multiplyP8(final int[] array) {
        final int n = array[0];
        final int n2 = array[1];
        final int n3 = array[2];
        final int n4 = array[3];
        final int n5 = n4 << 24;
        array[0] = (n5 >>> 7 ^ (n >>> 8 ^ n5 ^ n5 >>> 1 ^ n5 >>> 2));
        array[1] = (n2 >>> 8 | n << 24);
        array[2] = (n3 >>> 8 | n2 << 24);
        array[3] = (n4 >>> 8 | n3 << 24);
    }
    
    public static void multiplyP8(final int[] array, final int[] array2) {
        final int n = array[0];
        final int n2 = array[1];
        final int n3 = array[2];
        final int n4 = array[3];
        final int n5 = n4 << 24;
        array2[0] = (n5 >>> 7 ^ (n >>> 8 ^ n5 ^ n5 >>> 1 ^ n5 >>> 2));
        array2[1] = (n2 >>> 8 | n << 24);
        array2[2] = (n3 >>> 8 | n2 << 24);
        array2[3] = (n4 >>> 8 | n3 << 24);
    }
    
    public static void multiplyP8(final long[] array) {
        final long n = array[0];
        final long n2 = array[1];
        final long n3 = n2 << 56;
        array[0] = (n3 >>> 7 ^ (n >>> 8 ^ n3 ^ n3 >>> 1 ^ n3 >>> 2));
        array[1] = (n << 56 | n2 >>> 8);
    }
    
    public static void multiplyP8(final long[] array, final long[] array2) {
        final long n = array[0];
        final long n2 = array[1];
        final long n3 = n2 << 56;
        array2[0] = (n3 >>> 7 ^ (n >>> 8 ^ n3 ^ n3 >>> 1 ^ n3 >>> 2));
        array2[1] = (n << 56 | n2 >>> 8);
    }
    
    public static byte[] oneAsBytes() {
        final byte[] array = new byte[16];
        array[0] = -128;
        return array;
    }
    
    public static int[] oneAsInts() {
        final int[] array = new int[4];
        array[0] = Integer.MIN_VALUE;
        return array;
    }
    
    public static long[] oneAsLongs() {
        return new long[] { Long.MIN_VALUE, 0L };
    }
    
    public static long[] pAsLongs() {
        return new long[] { 4611686018427387904L, 0L };
    }
    
    public static void square(final long[] array, final long[] array2) {
        final long[] array3 = new long[4];
        Interleave.expand64To128Rev(array[0], array3, 0);
        Interleave.expand64To128Rev(array[1], array3, 2);
        final long n = array3[0];
        final long n2 = array3[1];
        final long n3 = array3[2];
        final long n4 = array3[3];
        final long n5 = n3 ^ (n4 << 57 ^ (n4 << 63 ^ n4 << 62));
        array2[0] = (n ^ (n5 >>> 1 ^ n5 ^ n5 >>> 2 ^ n5 >>> 7));
        array2[1] = (n2 ^ (n4 >>> 1 ^ n4 ^ n4 >>> 2 ^ n4 >>> 7) ^ (n5 << 57 ^ (n5 << 63 ^ n5 << 62)));
    }
    
    public static void xor(final byte[] array, final int n, final byte[] array2, final int n2, int n3) {
        while (true) {
            --n3;
            if (n3 < 0) {
                break;
            }
            final int n4 = n + n3;
            array[n4] ^= array2[n2 + n3];
        }
    }
    
    public static void xor(final byte[] array, final int n, final byte[] array2, final int n2, final byte[] array3, final int n3) {
        int n4 = 0;
        int n5;
        do {
            array3[n3 + n4] = (byte)(array[n + n4] ^ array2[n2 + n4]);
            final int n6 = n4 + 1;
            array3[n3 + n6] = (byte)(array[n + n6] ^ array2[n2 + n6]);
            final int n7 = n6 + 1;
            array3[n3 + n7] = (byte)(array[n + n7] ^ array2[n2 + n7]);
            n5 = n7 + 1;
            array3[n3 + n5] = (byte)(array[n + n5] ^ array2[n2 + n5]);
        } while ((n4 = n5 + 1) < 16);
    }
    
    public static void xor(final byte[] array, final byte[] array2) {
        int n = 0;
        int n2;
        do {
            array[n] ^= array2[n];
            final int n3 = n + 1;
            array[n3] ^= array2[n3];
            final int n4 = n3 + 1;
            array[n4] ^= array2[n4];
            n2 = n4 + 1;
            array[n2] ^= array2[n2];
        } while ((n = n2 + 1) < 16);
    }
    
    public static void xor(final byte[] array, final byte[] array2, final int n) {
        int n2 = 0;
        int n3;
        do {
            array[n2] ^= array2[n + n2];
            final int n4 = n2 + 1;
            array[n4] ^= array2[n + n4];
            final int n5 = n4 + 1;
            array[n5] ^= array2[n + n5];
            n3 = n5 + 1;
            array[n3] ^= array2[n + n3];
        } while ((n2 = n3 + 1) < 16);
    }
    
    public static void xor(final byte[] array, final byte[] array2, final int n, int n2) {
        while (true) {
            --n2;
            if (n2 < 0) {
                break;
            }
            array[n2] ^= array2[n + n2];
        }
    }
    
    public static void xor(final byte[] array, final byte[] array2, final byte[] array3) {
        int n = 0;
        int n2;
        do {
            array3[n] = (byte)(array[n] ^ array2[n]);
            final int n3 = n + 1;
            array3[n3] = (byte)(array[n3] ^ array2[n3]);
            final int n4 = n3 + 1;
            array3[n4] = (byte)(array[n4] ^ array2[n4]);
            n2 = n4 + 1;
            array3[n2] = (byte)(array[n2] ^ array2[n2]);
        } while ((n = n2 + 1) < 16);
    }
    
    public static void xor(final int[] array, final int[] array2) {
        array[0] ^= array2[0];
        array[1] ^= array2[1];
        array[2] ^= array2[2];
        array[3] ^= array2[3];
    }
    
    public static void xor(final int[] array, final int[] array2, final int[] array3) {
        array3[0] = (array[0] ^ array2[0]);
        array3[1] = (array[1] ^ array2[1]);
        array3[2] = (array[2] ^ array2[2]);
        array3[3] = (array[3] ^ array2[3]);
    }
    
    public static void xor(final long[] array, final long[] array2) {
        array[0] ^= array2[0];
        array[1] ^= array2[1];
    }
    
    public static void xor(final long[] array, final long[] array2, final long[] array3) {
        array3[0] = (array[0] ^ array2[0]);
        array3[1] = (array2[1] ^ array[1]);
    }
}
