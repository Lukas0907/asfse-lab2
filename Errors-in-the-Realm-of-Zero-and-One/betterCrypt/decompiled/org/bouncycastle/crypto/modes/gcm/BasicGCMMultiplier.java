// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes.gcm;

public class BasicGCMMultiplier implements GCMMultiplier
{
    private long[] H;
    
    @Override
    public void init(final byte[] array) {
        this.H = GCMUtil.asLongs(array);
    }
    
    @Override
    public void multiplyH(final byte[] array) {
        final long[] longs = GCMUtil.asLongs(array);
        GCMUtil.multiply(longs, this.H);
        GCMUtil.asBytes(longs, array);
    }
}
