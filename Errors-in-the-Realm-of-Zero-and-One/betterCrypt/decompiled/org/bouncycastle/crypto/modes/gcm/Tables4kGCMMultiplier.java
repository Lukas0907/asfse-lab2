// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes.gcm;

import org.bouncycastle.util.Pack;
import org.bouncycastle.util.Arrays;
import java.lang.reflect.Array;

public class Tables4kGCMMultiplier implements GCMMultiplier
{
    private byte[] H;
    private long[][] T;
    
    @Override
    public void init(final byte[] array) {
        final long[][] t = this.T;
        int i = 2;
        if (t == null) {
            this.T = (long[][])Array.newInstance(Long.TYPE, 256, 2);
        }
        else if (Arrays.areEqual(this.H, array)) {
            return;
        }
        GCMUtil.asLongs(this.H = Arrays.clone(array), this.T[1]);
        final long[][] t2 = this.T;
        GCMUtil.multiplyP7(t2[1], t2[1]);
        while (i < 256) {
            final long[][] t3 = this.T;
            GCMUtil.divideP(t3[i >> 1], t3[i]);
            final long[][] t4 = this.T;
            GCMUtil.xor(t4[i], t4[1], t4[i + 1]);
            i += 2;
        }
    }
    
    @Override
    public void multiplyH(final byte[] array) {
        final long[] array2 = this.T[array[15] & 0xFF];
        long n = array2[0];
        long n2 = array2[1];
        for (int i = 14; i >= 0; --i) {
            final long[] array3 = this.T[array[i] & 0xFF];
            final long n3 = n2 << 56;
            n2 = ((n2 >>> 8 | n << 56) ^ array3[1]);
            n = (n >>> 8 ^ array3[0] ^ n3 ^ n3 >>> 1 ^ n3 >>> 2 ^ n3 >>> 7);
        }
        Pack.longToBigEndian(n, array, 0);
        Pack.longToBigEndian(n2, array, 8);
    }
}
