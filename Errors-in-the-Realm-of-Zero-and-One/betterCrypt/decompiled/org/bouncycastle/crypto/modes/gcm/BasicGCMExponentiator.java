// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes.gcm;

import org.bouncycastle.util.Arrays;

public class BasicGCMExponentiator implements GCMExponentiator
{
    private long[] x;
    
    @Override
    public void exponentiateX(long n, final byte[] array) {
        final long[] oneAsLongs = GCMUtil.oneAsLongs();
        if (n > 0L) {
            final long[] clone = Arrays.clone(this.x);
            do {
                if ((0x1L & n) != 0x0L) {
                    GCMUtil.multiply(oneAsLongs, clone);
                }
                GCMUtil.square(clone, clone);
            } while ((n >>>= 1) > 0L);
        }
        GCMUtil.asBytes(oneAsLongs, array);
    }
    
    @Override
    public void init(final byte[] array) {
        this.x = GCMUtil.asLongs(array);
    }
}
