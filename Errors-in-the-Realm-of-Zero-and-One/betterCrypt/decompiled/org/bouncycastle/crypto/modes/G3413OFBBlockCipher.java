// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes;

import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.StreamBlockCipher;

public class G3413OFBBlockCipher extends StreamBlockCipher
{
    private byte[] R;
    private byte[] R_init;
    private byte[] Y;
    private int blockSize;
    private int byteCount;
    private BlockCipher cipher;
    private boolean initialized;
    private int m;
    
    public G3413OFBBlockCipher(final BlockCipher cipher) {
        super(cipher);
        this.initialized = false;
        this.blockSize = cipher.getBlockSize();
        this.cipher = cipher;
        this.Y = new byte[this.blockSize];
    }
    
    private void generateR() {
        final byte[] lsb = GOST3413CipherUtil.LSB(this.R, this.m - this.blockSize);
        System.arraycopy(lsb, 0, this.R, 0, lsb.length);
        System.arraycopy(this.Y, 0, this.R, lsb.length, this.m - lsb.length);
    }
    
    private void generateY() {
        this.cipher.processBlock(GOST3413CipherUtil.MSB(this.R, this.blockSize), 0, this.Y, 0);
    }
    
    private void initArrays() {
        final int m = this.m;
        this.R = new byte[m];
        this.R_init = new byte[m];
    }
    
    private void setupDefaultParams() {
        this.m = this.blockSize * 2;
    }
    
    @Override
    protected byte calculateByte(final byte b) {
        if (this.byteCount == 0) {
            this.generateY();
        }
        final byte[] y = this.Y;
        final int byteCount = this.byteCount;
        final byte b2 = (byte)(b ^ y[byteCount]);
        this.byteCount = byteCount + 1;
        if (this.byteCount == this.getBlockSize()) {
            this.byteCount = 0;
            this.generateR();
        }
        return b2;
    }
    
    @Override
    public String getAlgorithmName() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.cipher.getAlgorithmName());
        sb.append("/OFB");
        return sb.toString();
    }
    
    @Override
    public int getBlockSize() {
        return this.blockSize;
    }
    
    @Override
    public void init(final boolean b, final CipherParameters cipherParameters) throws IllegalArgumentException {
        Label_0139: {
            BlockCipher cipher;
            CipherParameters parameters;
            if (cipherParameters instanceof ParametersWithIV) {
                final ParametersWithIV parametersWithIV = (ParametersWithIV)cipherParameters;
                final byte[] iv = parametersWithIV.getIV();
                if (iv.length < this.blockSize) {
                    throw new IllegalArgumentException("Parameter m must blockSize <= m");
                }
                this.m = iv.length;
                this.initArrays();
                this.R_init = Arrays.clone(iv);
                final byte[] r_init = this.R_init;
                System.arraycopy(r_init, 0, this.R, 0, r_init.length);
                if (parametersWithIV.getParameters() == null) {
                    break Label_0139;
                }
                cipher = this.cipher;
                parameters = parametersWithIV.getParameters();
            }
            else {
                this.setupDefaultParams();
                this.initArrays();
                final byte[] r_init2 = this.R_init;
                System.arraycopy(r_init2, 0, this.R, 0, r_init2.length);
                if (cipherParameters == null) {
                    break Label_0139;
                }
                final BlockCipher cipher2 = this.cipher;
                parameters = cipherParameters;
                cipher = cipher2;
            }
            cipher.init(true, parameters);
        }
        this.initialized = true;
    }
    
    @Override
    public int processBlock(final byte[] array, final int n, final byte[] array2, final int n2) throws DataLengthException, IllegalStateException {
        this.processBytes(array, n, this.blockSize, array2, n2);
        return this.blockSize;
    }
    
    @Override
    public void reset() {
        if (this.initialized) {
            final byte[] r_init = this.R_init;
            System.arraycopy(r_init, 0, this.R, 0, r_init.length);
            Arrays.clear(this.Y);
            this.byteCount = 0;
            this.cipher.reset();
        }
    }
}
