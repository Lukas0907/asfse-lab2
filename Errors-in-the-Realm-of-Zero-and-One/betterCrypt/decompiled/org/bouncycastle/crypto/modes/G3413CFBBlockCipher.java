// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes;

import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.StreamBlockCipher;

public class G3413CFBBlockCipher extends StreamBlockCipher
{
    private byte[] R;
    private byte[] R_init;
    private int blockSize;
    private int byteCount;
    private BlockCipher cipher;
    private boolean forEncryption;
    private byte[] gamma;
    private byte[] inBuf;
    private boolean initialized;
    private int m;
    private final int s;
    
    public G3413CFBBlockCipher(final BlockCipher blockCipher) {
        this(blockCipher, blockCipher.getBlockSize() * 8);
    }
    
    public G3413CFBBlockCipher(final BlockCipher cipher, final int n) {
        super(cipher);
        this.initialized = false;
        if (n >= 0 && n <= cipher.getBlockSize() * 8) {
            this.blockSize = cipher.getBlockSize();
            this.cipher = cipher;
            this.s = n / 8;
            this.inBuf = new byte[this.getBlockSize()];
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Parameter bitBlockSize must be in range 0 < bitBlockSize <= ");
        sb.append(cipher.getBlockSize() * 8);
        throw new IllegalArgumentException(sb.toString());
    }
    
    private void initArrays() {
        final int m = this.m;
        this.R = new byte[m];
        this.R_init = new byte[m];
    }
    
    private void setupDefaultParams() {
        this.m = this.blockSize * 2;
    }
    
    @Override
    protected byte calculateByte(byte b) {
        if (this.byteCount == 0) {
            this.gamma = this.createGamma();
        }
        final byte[] gamma = this.gamma;
        final int byteCount = this.byteCount;
        final byte b2 = (byte)(gamma[byteCount] ^ b);
        final byte[] inBuf = this.inBuf;
        this.byteCount = byteCount + 1;
        if (this.forEncryption) {
            b = b2;
        }
        inBuf[byteCount] = b;
        if (this.byteCount == this.getBlockSize()) {
            this.byteCount = 0;
            this.generateR(this.inBuf);
        }
        return b2;
    }
    
    byte[] createGamma() {
        final byte[] msb = GOST3413CipherUtil.MSB(this.R, this.blockSize);
        final byte[] array = new byte[msb.length];
        this.cipher.processBlock(msb, 0, array, 0);
        return GOST3413CipherUtil.MSB(array, this.s);
    }
    
    void generateR(final byte[] array) {
        final byte[] lsb = GOST3413CipherUtil.LSB(this.R, this.m - this.s);
        System.arraycopy(lsb, 0, this.R, 0, lsb.length);
        System.arraycopy(array, 0, this.R, lsb.length, this.m - lsb.length);
    }
    
    @Override
    public String getAlgorithmName() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.cipher.getAlgorithmName());
        sb.append("/CFB");
        sb.append(this.blockSize * 8);
        return sb.toString();
    }
    
    @Override
    public int getBlockSize() {
        return this.s;
    }
    
    @Override
    public void init(final boolean forEncryption, final CipherParameters cipherParameters) throws IllegalArgumentException {
        this.forEncryption = forEncryption;
        Label_0144: {
            BlockCipher cipher;
            CipherParameters parameters;
            if (cipherParameters instanceof ParametersWithIV) {
                final ParametersWithIV parametersWithIV = (ParametersWithIV)cipherParameters;
                final byte[] iv = parametersWithIV.getIV();
                if (iv.length < this.blockSize) {
                    throw new IllegalArgumentException("Parameter m must blockSize <= m");
                }
                this.m = iv.length;
                this.initArrays();
                this.R_init = Arrays.clone(iv);
                final byte[] r_init = this.R_init;
                System.arraycopy(r_init, 0, this.R, 0, r_init.length);
                if (parametersWithIV.getParameters() == null) {
                    break Label_0144;
                }
                cipher = this.cipher;
                parameters = parametersWithIV.getParameters();
            }
            else {
                this.setupDefaultParams();
                this.initArrays();
                final byte[] r_init2 = this.R_init;
                System.arraycopy(r_init2, 0, this.R, 0, r_init2.length);
                if (cipherParameters == null) {
                    break Label_0144;
                }
                final BlockCipher cipher2 = this.cipher;
                parameters = cipherParameters;
                cipher = cipher2;
            }
            cipher.init(true, parameters);
        }
        this.initialized = true;
    }
    
    @Override
    public int processBlock(final byte[] array, final int n, final byte[] array2, final int n2) throws DataLengthException, IllegalStateException {
        this.processBytes(array, n, this.getBlockSize(), array2, n2);
        return this.getBlockSize();
    }
    
    @Override
    public void reset() {
        this.byteCount = 0;
        Arrays.clear(this.inBuf);
        Arrays.clear(this.gamma);
        if (this.initialized) {
            final byte[] r_init = this.R_init;
            System.arraycopy(r_init, 0, this.R, 0, r_init.length);
            this.cipher.reset();
        }
    }
}
