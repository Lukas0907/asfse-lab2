// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes;

import org.bouncycastle.util.Arrays;

class GOST3413CipherUtil
{
    public static byte[] LSB(final byte[] array, final int n) {
        final byte[] array2 = new byte[n];
        System.arraycopy(array, array.length - n, array2, 0, n);
        return array2;
    }
    
    public static byte[] MSB(final byte[] array, final int n) {
        return Arrays.copyOf(array, n);
    }
    
    public static byte[] copyFromInput(final byte[] array, final int n, final int n2) {
        int n3 = n;
        if (array.length < n + n2) {
            n3 = array.length - n2;
        }
        final byte[] array2 = new byte[n3];
        System.arraycopy(array, n2, array2, 0, n3);
        return array2;
    }
    
    public static byte[] sum(final byte[] array, final byte[] array2) {
        final byte[] array3 = new byte[array.length];
        for (int i = 0; i < array.length; ++i) {
            array3[i] = (byte)(array[i] ^ array2[i]);
        }
        return array3;
    }
}
