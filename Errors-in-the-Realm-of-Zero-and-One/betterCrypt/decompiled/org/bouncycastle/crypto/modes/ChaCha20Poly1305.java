// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes;

import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.params.AEADParameters;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.OutputLengthException;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.util.Pack;
import org.bouncycastle.crypto.macs.Poly1305;
import org.bouncycastle.crypto.Mac;
import org.bouncycastle.crypto.engines.ChaCha7539Engine;

public class ChaCha20Poly1305 implements AEADCipher
{
    private static final long AAD_LIMIT = -1L;
    private static final int BUF_SIZE = 64;
    private static final long DATA_LIMIT = 274877906880L;
    private static final int KEY_SIZE = 32;
    private static final int MAC_SIZE = 16;
    private static final int NONCE_SIZE = 12;
    private static final byte[] ZEROES;
    private long aadCount;
    private final byte[] buf;
    private int bufPos;
    private final ChaCha7539Engine chacha20;
    private long dataCount;
    private byte[] initialAAD;
    private final byte[] key;
    private final byte[] mac;
    private final byte[] nonce;
    private final Mac poly1305;
    private int state;
    
    static {
        ZEROES = new byte[15];
    }
    
    public ChaCha20Poly1305() {
        this(new Poly1305());
    }
    
    public ChaCha20Poly1305(final Mac poly1305) {
        this.key = new byte[32];
        this.nonce = new byte[12];
        this.buf = new byte[80];
        this.mac = new byte[16];
        this.state = 0;
        if (poly1305 == null) {
            throw new NullPointerException("'poly1305' cannot be null");
        }
        if (16 == poly1305.getMacSize()) {
            this.chacha20 = new ChaCha7539Engine();
            this.poly1305 = poly1305;
            return;
        }
        throw new IllegalArgumentException("'poly1305' must be a 128-bit MAC");
    }
    
    private void checkAAD() {
        final int state = this.state;
        int state2 = 2;
        if (state != 1) {
            if (state == 2) {
                return;
            }
            if (state == 4) {
                throw new IllegalStateException("ChaCha20Poly1305 cannot be reused for encryption");
            }
            state2 = 6;
            if (state != 5) {
                if (state == 6) {
                    return;
                }
                throw new IllegalStateException();
            }
        }
        this.state = state2;
    }
    
    private void checkData() {
        int n = 0;
        switch (this.state) {
            default: {
                throw new IllegalStateException();
            }
            case 5:
            case 6: {
                n = 7;
                break;
            }
            case 4: {
                throw new IllegalStateException("ChaCha20Poly1305 cannot be reused for encryption");
            }
            case 1:
            case 2: {
                n = 3;
                break;
            }
            case 3:
            case 7: {
                return;
            }
        }
        this.finishAAD(n);
    }
    
    private void finishAAD(final int state) {
        this.padMAC(this.aadCount);
        this.state = state;
    }
    
    private void finishData(final int state) {
        this.padMAC(this.dataCount);
        final byte[] array = new byte[16];
        Pack.longToLittleEndian(this.aadCount, array, 0);
        Pack.longToLittleEndian(this.dataCount, array, 8);
        this.poly1305.update(array, 0, 16);
        this.poly1305.doFinal(this.mac, 0);
        this.state = state;
    }
    
    private long incrementCount(final long n, final int n2, final long n3) {
        final long n4 = n2;
        if (n - Long.MIN_VALUE <= n3 - n4 - Long.MIN_VALUE) {
            return n + n4;
        }
        throw new IllegalStateException("Limit exceeded");
    }
    
    private void initMAC() {
        final byte[] array = new byte[64];
        try {
            this.chacha20.processBytes(array, 0, 64, array, 0);
            this.poly1305.init(new KeyParameter(array, 0, 32));
        }
        finally {
            Arrays.clear(array);
        }
    }
    
    private void padMAC(final long n) {
        final int n2 = (int)n % 16;
        if (n2 != 0) {
            this.poly1305.update(ChaCha20Poly1305.ZEROES, 0, 16 - n2);
        }
    }
    
    private void processData(final byte[] array, final int n, final int n2, final byte[] array2, final int n3) {
        if (n3 <= array2.length - n2) {
            this.chacha20.processBytes(array, n, n2, array2, n3);
            this.dataCount = this.incrementCount(this.dataCount, n2, 274877906880L);
            return;
        }
        throw new OutputLengthException("Output buffer too short");
    }
    
    private void reset(final boolean b, final boolean b2) {
        Arrays.clear(this.buf);
        if (b) {
            Arrays.clear(this.mac);
        }
        this.aadCount = 0L;
        this.dataCount = 0L;
        this.bufPos = 0;
        switch (this.state) {
            default: {
                throw new IllegalStateException();
            }
            case 2:
            case 3:
            case 4: {
                this.state = 4;
            }
            case 6:
            case 7:
            case 8: {
                this.state = 5;
            }
            case 1:
            case 5: {
                if (b2) {
                    this.chacha20.reset();
                }
                this.initMAC();
                final byte[] initialAAD = this.initialAAD;
                if (initialAAD != null) {
                    this.processAADBytes(initialAAD, 0, initialAAD.length);
                }
            }
        }
    }
    
    @Override
    public int doFinal(final byte[] array, int n) throws IllegalStateException, InvalidCipherTextException {
        if (array == null) {
            throw new NullPointerException("'out' cannot be null");
        }
        if (n >= 0) {
            this.checkData();
            Arrays.clear(this.mac);
            final int state = this.state;
            if (state != 3) {
                if (state != 7) {
                    throw new IllegalStateException();
                }
                final int bufPos = this.bufPos;
                if (bufPos < 16) {
                    throw new InvalidCipherTextException("data too short");
                }
                final int n2 = bufPos - 16;
                if (n > array.length - n2) {
                    throw new OutputLengthException("Output buffer too short");
                }
                if (n2 > 0) {
                    this.poly1305.update(this.buf, 0, n2);
                    this.processData(this.buf, 0, n2, array, n);
                }
                this.finishData(8);
                if (!Arrays.constantTimeAreEqual(16, this.mac, 0, this.buf, n2)) {
                    throw new InvalidCipherTextException("mac check in ChaCha20Poly1305 failed");
                }
                n = n2;
            }
            else {
                final int bufPos2 = this.bufPos;
                final int n3 = bufPos2 + 16;
                if (n > array.length - n3) {
                    throw new OutputLengthException("Output buffer too short");
                }
                if (bufPos2 > 0) {
                    this.processData(this.buf, 0, bufPos2, array, n);
                    this.poly1305.update(array, n, this.bufPos);
                }
                this.finishData(4);
                System.arraycopy(this.mac, 0, array, n + this.bufPos, 16);
                n = n3;
            }
            this.reset(false, true);
            return n;
        }
        throw new IllegalArgumentException("'outOff' cannot be negative");
    }
    
    @Override
    public String getAlgorithmName() {
        return "ChaCha20Poly1305";
    }
    
    @Override
    public byte[] getMac() {
        return Arrays.clone(this.mac);
    }
    
    @Override
    public int getOutputSize(int b) {
        b = Math.max(0, b) + this.bufPos;
        final int state = this.state;
        if (state == 1 || state == 2 || state == 3) {
            return b + 16;
        }
        if (state != 5 && state != 6 && state != 7) {
            final StringBuilder sb = new StringBuilder();
            sb.append("state=");
            sb.append(this.state);
            throw new IllegalStateException(sb.toString());
        }
        return Math.max(0, b - 16);
    }
    
    @Override
    public int getUpdateOutputSize(int max) {
        final int n = Math.max(0, max) + this.bufPos;
        final int state = this.state;
        max = n;
        if (state != 1) {
            max = n;
            if (state != 2) {
                max = n;
                if (state != 3) {
                    if (state != 5 && state != 6 && state != 7) {
                        throw new IllegalStateException();
                    }
                    max = Math.max(0, n - 16);
                }
            }
        }
        return max - max % 64;
    }
    
    @Override
    public void init(final boolean b, final CipherParameters cipherParameters) throws IllegalArgumentException {
        KeyParameter key;
        byte[] array;
        ParametersWithIV parametersWithIV;
        if (cipherParameters instanceof AEADParameters) {
            final AEADParameters aeadParameters = (AEADParameters)cipherParameters;
            final int macSize = aeadParameters.getMacSize();
            if (128 != macSize) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid value for MAC size: ");
                sb.append(macSize);
                throw new IllegalArgumentException(sb.toString());
            }
            key = aeadParameters.getKey();
            array = aeadParameters.getNonce();
            parametersWithIV = new ParametersWithIV(key, array);
            this.initialAAD = aeadParameters.getAssociatedText();
        }
        else {
            if (!(cipherParameters instanceof ParametersWithIV)) {
                throw new IllegalArgumentException("invalid parameters passed to ChaCha20Poly1305");
            }
            parametersWithIV = (ParametersWithIV)cipherParameters;
            key = (KeyParameter)parametersWithIV.getParameters();
            array = parametersWithIV.getIV();
            this.initialAAD = null;
        }
        if (key == null) {
            if (this.state == 0) {
                throw new IllegalArgumentException("Key must be specified in initial init");
            }
        }
        else if (32 != key.getKey().length) {
            throw new IllegalArgumentException("Key must be 256 bits");
        }
        if (array == null || 12 != array.length) {
            throw new IllegalArgumentException("Nonce must be 96 bits");
        }
        if (this.state != 0 && b && Arrays.areEqual(this.nonce, array) && (key == null || Arrays.areEqual(this.key, key.getKey()))) {
            throw new IllegalArgumentException("cannot reuse nonce for ChaCha20Poly1305 encryption");
        }
        if (key != null) {
            System.arraycopy(key.getKey(), 0, this.key, 0, 32);
        }
        System.arraycopy(array, 0, this.nonce, 0, 12);
        this.chacha20.init(true, parametersWithIV);
        int state;
        if (b) {
            state = 1;
        }
        else {
            state = 5;
        }
        this.state = state;
        this.reset(true, false);
    }
    
    @Override
    public void processAADByte(final byte b) {
        this.checkAAD();
        this.aadCount = this.incrementCount(this.aadCount, 1, -1L);
        this.poly1305.update(b);
    }
    
    @Override
    public void processAADBytes(final byte[] array, final int n, final int n2) {
        if (array == null) {
            throw new NullPointerException("'in' cannot be null");
        }
        if (n < 0) {
            throw new IllegalArgumentException("'inOff' cannot be negative");
        }
        if (n2 < 0) {
            throw new IllegalArgumentException("'len' cannot be negative");
        }
        if (n <= array.length - n2) {
            this.checkAAD();
            if (n2 > 0) {
                this.aadCount = this.incrementCount(this.aadCount, n2, -1L);
                this.poly1305.update(array, n, n2);
            }
            return;
        }
        throw new DataLengthException("Input buffer too short");
    }
    
    @Override
    public int processByte(final byte b, byte[] buf, final int n) throws DataLengthException {
        this.checkData();
        final int state = this.state;
        if (state != 3) {
            if (state != 7) {
                throw new IllegalStateException();
            }
            final byte[] buf2 = this.buf;
            final int bufPos = this.bufPos;
            buf2[bufPos] = b;
            if ((this.bufPos = bufPos + 1) == buf2.length) {
                this.poly1305.update(buf2, 0, 64);
                this.processData(this.buf, 0, 64, buf, n);
                buf = this.buf;
                System.arraycopy(buf, 64, buf, 0, 16);
                this.bufPos = 16;
                return 64;
            }
            return 0;
        }
        else {
            final byte[] buf3 = this.buf;
            final int bufPos2 = this.bufPos;
            buf3[bufPos2] = b;
            if ((this.bufPos = bufPos2 + 1) == 64) {
                this.processData(buf3, 0, 64, buf, n);
                this.poly1305.update(buf, n, 64);
                this.bufPos = 0;
                return 64;
            }
            return 0;
        }
    }
    
    @Override
    public int processBytes(final byte[] array, int bufPos, int i, final byte[] array2, final int n) throws DataLengthException {
        if (array == null) {
            throw new NullPointerException("'in' cannot be null");
        }
        if (array2 == null) {
            throw new NullPointerException("'out' cannot be null");
        }
        if (bufPos < 0) {
            throw new IllegalArgumentException("'inOff' cannot be negative");
        }
        if (i < 0) {
            throw new IllegalArgumentException("'len' cannot be negative");
        }
        if (bufPos > array.length - i) {
            throw new DataLengthException("Input buffer too short");
        }
        if (n >= 0) {
            this.checkData();
            final int state = this.state;
            if (state == 3) {
                int n2 = bufPos;
                int n3 = i;
                while (true) {
                    Label_0314: {
                        if (this.bufPos == 0) {
                            break Label_0314;
                        }
                        while (true) {
                            n2 = bufPos;
                            if ((n3 = i) <= 0) {
                                break Label_0314;
                            }
                            --i;
                            final byte[] buf = this.buf;
                            final int bufPos2 = this.bufPos;
                            n2 = bufPos + 1;
                            buf[bufPos2] = array[bufPos];
                            bufPos = bufPos2 + 1;
                            if ((this.bufPos = bufPos) == 64) {
                                this.processData(buf, 0, 64, array2, n);
                                this.poly1305.update(array2, n, 64);
                                this.bufPos = 0;
                                bufPos = 64;
                                break;
                            }
                            bufPos = n2;
                        }
                        while (i >= 64) {
                            final int n4 = n + bufPos;
                            this.processData(array, n2, 64, array2, n4);
                            this.poly1305.update(array2, n4, 64);
                            n2 += 64;
                            i -= 64;
                            bufPos += 64;
                        }
                        int n5 = bufPos;
                        if (i > 0) {
                            System.arraycopy(array, n2, this.buf, 0, i);
                            this.bufPos = i;
                            n5 = bufPos;
                            return n5;
                        }
                        return n5;
                    }
                    bufPos = 0;
                    i = n3;
                    continue;
                }
            }
            if (state != 7) {
                throw new IllegalStateException();
            }
            int n7;
            int n6 = n7 = 0;
            int n5;
            while (true) {
                n5 = n7;
                if (n6 >= i) {
                    break;
                }
                final byte[] buf2 = this.buf;
                final int bufPos3 = this.bufPos;
                buf2[bufPos3] = array[bufPos + n6];
                final int bufPos4 = bufPos3 + 1;
                this.bufPos = bufPos4;
                int n8 = n7;
                if (bufPos4 == buf2.length) {
                    this.poly1305.update(buf2, 0, 64);
                    this.processData(this.buf, 0, 64, array2, n + n7);
                    final byte[] buf3 = this.buf;
                    System.arraycopy(buf3, 64, buf3, 0, 16);
                    this.bufPos = 16;
                    n8 = n7 + 64;
                }
                ++n6;
                n7 = n8;
            }
            return n5;
        }
        throw new IllegalArgumentException("'outOff' cannot be negative");
    }
    
    @Override
    public void reset() {
        this.reset(true, true);
    }
    
    private static final class State
    {
        static final int DEC_AAD = 6;
        static final int DEC_DATA = 7;
        static final int DEC_FINAL = 8;
        static final int DEC_INIT = 5;
        static final int ENC_AAD = 2;
        static final int ENC_DATA = 3;
        static final int ENC_FINAL = 4;
        static final int ENC_INIT = 1;
        static final int UNINITIALIZED = 0;
    }
}
