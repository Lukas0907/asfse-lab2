// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes;

import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.OutputLengthException;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.BlockCipher;

public class PGPCFBBlockCipher implements BlockCipher
{
    private byte[] FR;
    private byte[] FRE;
    private byte[] IV;
    private int blockSize;
    private BlockCipher cipher;
    private int count;
    private boolean forEncryption;
    private boolean inlineIv;
    private byte[] tmp;
    
    public PGPCFBBlockCipher(final BlockCipher cipher, final boolean inlineIv) {
        this.cipher = cipher;
        this.inlineIv = inlineIv;
        this.blockSize = cipher.getBlockSize();
        final int blockSize = this.blockSize;
        this.IV = new byte[blockSize];
        this.FR = new byte[blockSize];
        this.FRE = new byte[blockSize];
        this.tmp = new byte[blockSize];
    }
    
    private int decryptBlock(final byte[] array, final int n, final byte[] array2, int blockSize) throws DataLengthException, IllegalStateException {
        final int blockSize2 = this.blockSize;
        if (n + blockSize2 > array.length) {
            throw new DataLengthException("input buffer too short");
        }
        if (blockSize2 + blockSize <= array2.length) {
            final BlockCipher cipher = this.cipher;
            final byte[] fr = this.FR;
            final byte[] fre = this.FRE;
            final int n2 = 0;
            cipher.processBlock(fr, 0, fre, 0);
            int n3 = 0;
            int n4;
            while (true) {
                n4 = n2;
                if (n3 >= this.blockSize) {
                    break;
                }
                array2[blockSize + n3] = this.encryptByte(array[n + n3], n3);
                ++n3;
            }
            while (true) {
                blockSize = this.blockSize;
                if (n4 >= blockSize) {
                    break;
                }
                this.FR[n4] = array[n + n4];
                ++n4;
            }
            return blockSize;
        }
        throw new OutputLengthException("output buffer too short");
    }
    
    private int decryptBlockWithIV(byte[] array, int blockSize, final byte[] array2, int i) throws DataLengthException, IllegalStateException {
        final int blockSize2 = this.blockSize;
        if (blockSize + blockSize2 > array.length) {
            throw new DataLengthException("input buffer too short");
        }
        if (i + blockSize2 > array2.length) {
            throw new OutputLengthException("output buffer too short");
        }
        final int count = this.count;
        if (count == 0) {
            for (i = 0; i < this.blockSize; ++i) {
                this.FR[i] = array[blockSize + i];
            }
            this.cipher.processBlock(this.FR, 0, this.FRE, 0);
            this.count += this.blockSize;
            return 0;
        }
        if (count == blockSize2) {
            System.arraycopy(array, blockSize, this.tmp, 0, blockSize2);
            array = this.FR;
            System.arraycopy(array, 2, array, 0, this.blockSize - 2);
            array = this.FR;
            blockSize = this.blockSize;
            final byte[] tmp = this.tmp;
            array[blockSize - 2] = tmp[0];
            array[blockSize - 1] = tmp[1];
            this.cipher.processBlock(array, 0, this.FRE, 0);
            blockSize = 0;
            int blockSize3;
            while (true) {
                blockSize3 = this.blockSize;
                if (blockSize >= blockSize3 - 2) {
                    break;
                }
                array2[i + blockSize] = this.encryptByte(this.tmp[blockSize + 2], blockSize);
                ++blockSize;
            }
            System.arraycopy(this.tmp, 2, this.FR, 0, blockSize3 - 2);
            this.count += 2;
            return this.blockSize - 2;
        }
        if (count >= blockSize2 + 2) {
            System.arraycopy(array, blockSize, this.tmp, 0, blockSize2);
            array2[i + 0] = this.encryptByte(this.tmp[0], this.blockSize - 2);
            array2[i + 1] = this.encryptByte(this.tmp[1], this.blockSize - 1);
            System.arraycopy(this.tmp, 0, this.FR, this.blockSize - 2, 2);
            this.cipher.processBlock(this.FR, 0, this.FRE, 0);
            blockSize = 0;
            int blockSize4;
            while (true) {
                blockSize4 = this.blockSize;
                if (blockSize >= blockSize4 - 2) {
                    break;
                }
                array2[i + blockSize + 2] = this.encryptByte(this.tmp[blockSize + 2], blockSize);
                ++blockSize;
            }
            System.arraycopy(this.tmp, 2, this.FR, 0, blockSize4 - 2);
        }
        return this.blockSize;
    }
    
    private int encryptBlock(final byte[] array, int blockSize, final byte[] array2, final int n) throws DataLengthException, IllegalStateException {
        final int blockSize2 = this.blockSize;
        if (blockSize + blockSize2 > array.length) {
            throw new DataLengthException("input buffer too short");
        }
        if (blockSize2 + n <= array2.length) {
            final BlockCipher cipher = this.cipher;
            final byte[] fr = this.FR;
            final byte[] fre = this.FRE;
            final int n2 = 0;
            cipher.processBlock(fr, 0, fre, 0);
            int n3 = 0;
            int n4;
            while (true) {
                n4 = n2;
                if (n3 >= this.blockSize) {
                    break;
                }
                array2[n + n3] = this.encryptByte(array[blockSize + n3], n3);
                ++n3;
            }
            while (true) {
                blockSize = this.blockSize;
                if (n4 >= blockSize) {
                    break;
                }
                this.FR[n4] = array2[n + n4];
                ++n4;
            }
            return blockSize;
        }
        throw new OutputLengthException("output buffer too short");
    }
    
    private int encryptBlockWithIV(final byte[] array, int count, final byte[] array2, int blockSize) throws DataLengthException, IllegalStateException {
        final int blockSize2 = this.blockSize;
        if (count + blockSize2 > array.length) {
            throw new DataLengthException("input buffer too short");
        }
        final int count2 = this.count;
        if (count2 != 0) {
            if (count2 >= blockSize2 + 2) {
                if (blockSize2 + blockSize > array2.length) {
                    throw new OutputLengthException("output buffer too short");
                }
                this.cipher.processBlock(this.FR, 0, this.FRE, 0);
                int n = 0;
                int blockSize3;
                while (true) {
                    blockSize3 = this.blockSize;
                    if (n >= blockSize3) {
                        break;
                    }
                    array2[blockSize + n] = this.encryptByte(array[count + n], n);
                    ++n;
                }
                System.arraycopy(array2, blockSize, this.FR, 0, blockSize3);
            }
            return this.blockSize;
        }
        if (blockSize2 * 2 + blockSize + 2 <= array2.length) {
            this.cipher.processBlock(this.FR, 0, this.FRE, 0);
            int n2 = 0;
            int blockSize4;
            while (true) {
                blockSize4 = this.blockSize;
                if (n2 >= blockSize4) {
                    break;
                }
                array2[blockSize + n2] = this.encryptByte(this.IV[n2], n2);
                ++n2;
            }
            System.arraycopy(array2, blockSize, this.FR, 0, blockSize4);
            this.cipher.processBlock(this.FR, 0, this.FRE, 0);
            final int blockSize5 = this.blockSize;
            array2[blockSize + blockSize5] = this.encryptByte(this.IV[blockSize5 - 2], 0);
            final int blockSize6 = this.blockSize;
            array2[blockSize + blockSize6 + 1] = this.encryptByte(this.IV[blockSize6 - 1], 1);
            System.arraycopy(array2, blockSize + 2, this.FR, 0, this.blockSize);
            this.cipher.processBlock(this.FR, 0, this.FRE, 0);
            int n3 = 0;
            int blockSize7;
            while (true) {
                blockSize7 = this.blockSize;
                if (n3 >= blockSize7) {
                    break;
                }
                array2[blockSize7 + blockSize + 2 + n3] = this.encryptByte(array[count + n3], n3);
                ++n3;
            }
            System.arraycopy(array2, blockSize + blockSize7 + 2, this.FR, 0, blockSize7);
            count = this.count;
            blockSize = this.blockSize;
            this.count = count + (blockSize * 2 + 2);
            return blockSize * 2 + 2;
        }
        throw new OutputLengthException("output buffer too short");
    }
    
    private byte encryptByte(final byte b, final int n) {
        return (byte)(b ^ this.FRE[n]);
    }
    
    @Override
    public String getAlgorithmName() {
        StringBuilder sb;
        String str;
        if (this.inlineIv) {
            sb = new StringBuilder();
            sb.append(this.cipher.getAlgorithmName());
            str = "/PGPCFBwithIV";
        }
        else {
            sb = new StringBuilder();
            sb.append(this.cipher.getAlgorithmName());
            str = "/PGPCFB";
        }
        sb.append(str);
        return sb.toString();
    }
    
    @Override
    public int getBlockSize() {
        return this.cipher.getBlockSize();
    }
    
    public BlockCipher getUnderlyingCipher() {
        return this.cipher;
    }
    
    @Override
    public void init(final boolean forEncryption, final CipherParameters cipherParameters) throws IllegalArgumentException {
        this.forEncryption = forEncryption;
        BlockCipher cipher;
        CipherParameters parameters;
        if (cipherParameters instanceof ParametersWithIV) {
            final ParametersWithIV parametersWithIV = (ParametersWithIV)cipherParameters;
            final byte[] iv = parametersWithIV.getIV();
            final int length = iv.length;
            final byte[] iv2 = this.IV;
            if (length < iv2.length) {
                System.arraycopy(iv, 0, iv2, iv2.length - iv.length, iv.length);
                int n = 0;
                while (true) {
                    final byte[] iv3 = this.IV;
                    if (n >= iv3.length - iv.length) {
                        break;
                    }
                    iv3[n] = 0;
                    ++n;
                }
            }
            else {
                System.arraycopy(iv, 0, iv2, 0, iv2.length);
            }
            this.reset();
            cipher = this.cipher;
            parameters = parametersWithIV.getParameters();
        }
        else {
            this.reset();
            final BlockCipher cipher2 = this.cipher;
            parameters = cipherParameters;
            cipher = cipher2;
        }
        cipher.init(true, parameters);
    }
    
    @Override
    public int processBlock(final byte[] array, final int n, final byte[] array2, final int n2) throws DataLengthException, IllegalStateException {
        if (this.inlineIv) {
            if (this.forEncryption) {
                return this.encryptBlockWithIV(array, n, array2, n2);
            }
            return this.decryptBlockWithIV(array, n, array2, n2);
        }
        else {
            if (this.forEncryption) {
                return this.encryptBlock(array, n, array2, n2);
            }
            return this.decryptBlock(array, n, array2, n2);
        }
    }
    
    @Override
    public void reset() {
        this.count = 0;
        int n = 0;
        while (true) {
            final byte[] fr = this.FR;
            if (n == fr.length) {
                break;
            }
            if (this.inlineIv) {
                fr[n] = 0;
            }
            else {
                fr[n] = this.IV[n];
            }
            ++n;
        }
        this.cipher.reset();
    }
}
