// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes;

import java.io.ByteArrayOutputStream;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.params.AEADParameters;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.OutputLengthException;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.modes.kgcm.Tables16kKGCMMultiplier_512;
import org.bouncycastle.crypto.modes.kgcm.Tables8kKGCMMultiplier_256;
import org.bouncycastle.crypto.modes.kgcm.Tables4kKGCMMultiplier_128;
import org.bouncycastle.util.Pack;
import org.bouncycastle.crypto.modes.kgcm.KGCMMultiplier;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.BufferedBlockCipher;

public class KGCMBlockCipher implements AEADBlockCipher
{
    private static final int MIN_MAC_BITS = 64;
    private ExposedByteArrayOutputStream associatedText;
    private long[] b;
    private final int blockSize;
    private BufferedBlockCipher ctrEngine;
    private ExposedByteArrayOutputStream data;
    private BlockCipher engine;
    private boolean forEncryption;
    private byte[] initialAssociatedText;
    private byte[] iv;
    private byte[] macBlock;
    private int macSize;
    private KGCMMultiplier multiplier;
    
    public KGCMBlockCipher(final BlockCipher engine) {
        this.associatedText = new ExposedByteArrayOutputStream();
        this.data = new ExposedByteArrayOutputStream();
        this.engine = engine;
        this.ctrEngine = new BufferedBlockCipher(new KCTRBlockCipher(this.engine));
        this.macSize = -1;
        this.blockSize = this.engine.getBlockSize();
        final int blockSize = this.blockSize;
        this.initialAssociatedText = new byte[blockSize];
        this.iv = new byte[blockSize];
        this.multiplier = createDefaultMultiplier(blockSize);
        this.b = new long[this.blockSize >>> 3];
        this.macBlock = null;
    }
    
    private void calculateMac(final byte[] array, int n, final int n2, final int n3) {
        for (int i = n; i < n + n2; i += this.blockSize) {
            xorWithInput(this.b, array, i);
            this.multiplier.multiplyH(this.b);
        }
        final long n4 = n3;
        final long n5 = n2;
        final long[] b = this.b;
        b[0] ^= (n4 & 0xFFFFFFFFL) << 3;
        n = this.blockSize >>> 4;
        b[n] ^= (0xFFFFFFFFL & n5) << 3;
        this.macBlock = Pack.longToLittleEndian(b);
        final BlockCipher engine = this.engine;
        final byte[] macBlock = this.macBlock;
        engine.processBlock(macBlock, 0, macBlock, 0);
    }
    
    private static KGCMMultiplier createDefaultMultiplier(final int n) {
        if (n == 16) {
            return new Tables4kKGCMMultiplier_128();
        }
        if (n == 32) {
            return new Tables8kKGCMMultiplier_256();
        }
        if (n == 64) {
            return new Tables16kKGCMMultiplier_512();
        }
        throw new IllegalArgumentException("Only 128, 256, and 512 -bit block sizes supported");
    }
    
    private void processAAD(final byte[] array, final int n, final int n2) {
        for (int i = n; i < n2 + n; i += this.blockSize) {
            xorWithInput(this.b, array, i);
            this.multiplier.multiplyH(this.b);
        }
    }
    
    private static void xorWithInput(final long[] array, final byte[] array2, int i) {
        final int n = 0;
        int n2 = i;
        for (i = n; i < array.length; ++i) {
            array[i] ^= Pack.littleEndianToLong(array2, n2);
            n2 += 8;
        }
    }
    
    @Override
    public int doFinal(byte[] array, int n) throws IllegalStateException, InvalidCipherTextException {
        final int size = this.data.size();
        if (!this.forEncryption && size < this.macSize) {
            throw new InvalidCipherTextException("data too short");
        }
        final byte[] array2 = new byte[this.blockSize];
        this.engine.processBlock(array2, 0, array2, 0);
        final long[] array3 = new long[this.blockSize >>> 3];
        Pack.littleEndianToLong(array2, 0, array3);
        this.multiplier.init(array3);
        Arrays.fill(array2, (byte)0);
        Arrays.fill(array3, 0L);
        final int size2 = this.associatedText.size();
        if (size2 > 0) {
            this.processAAD(this.associatedText.getBuffer(), 0, size2);
        }
        int n2;
        if (this.forEncryption) {
            if (array.length - n - this.macSize < size) {
                throw new OutputLengthException("Output buffer too short");
            }
            final int processBytes = this.ctrEngine.processBytes(this.data.getBuffer(), 0, size, array, n);
            n2 = processBytes + this.ctrEngine.doFinal(array, n + processBytes);
            this.calculateMac(array, n, size, size2);
        }
        else {
            final int n3 = size - this.macSize;
            if (array.length - n < n3) {
                throw new OutputLengthException("Output buffer too short");
            }
            this.calculateMac(this.data.getBuffer(), 0, n3, size2);
            final int processBytes2 = this.ctrEngine.processBytes(this.data.getBuffer(), 0, n3, array, n);
            n2 = processBytes2 + this.ctrEngine.doFinal(array, n + processBytes2);
        }
        final byte[] macBlock = this.macBlock;
        if (macBlock == null) {
            throw new IllegalStateException("mac is not calculated");
        }
        if (this.forEncryption) {
            System.arraycopy(macBlock, 0, array, n + n2, this.macSize);
            this.reset();
            return n2 + this.macSize;
        }
        array = new byte[this.macSize];
        final byte[] buffer = this.data.getBuffer();
        n = this.macSize;
        System.arraycopy(buffer, size - n, array, 0, n);
        n = this.macSize;
        final byte[] array4 = new byte[n];
        System.arraycopy(this.macBlock, 0, array4, 0, n);
        if (Arrays.constantTimeAreEqual(array, array4)) {
            this.reset();
            return n2;
        }
        throw new InvalidCipherTextException("mac verification failed");
    }
    
    @Override
    public String getAlgorithmName() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.engine.getAlgorithmName());
        sb.append("/KGCM");
        return sb.toString();
    }
    
    @Override
    public byte[] getMac() {
        final int macSize = this.macSize;
        final byte[] array = new byte[macSize];
        System.arraycopy(this.macBlock, 0, array, 0, macSize);
        return array;
    }
    
    @Override
    public int getOutputSize(int n) {
        n += this.data.size();
        if (this.forEncryption) {
            return n + this.macSize;
        }
        final int macSize = this.macSize;
        if (n < macSize) {
            return 0;
        }
        return n - macSize;
    }
    
    @Override
    public BlockCipher getUnderlyingCipher() {
        return this.engine;
    }
    
    @Override
    public int getUpdateOutputSize(final int n) {
        return 0;
    }
    
    @Override
    public void init(final boolean forEncryption, final CipherParameters cipherParameters) throws IllegalArgumentException {
        this.forEncryption = forEncryption;
        KeyParameter keyParameter;
        if (cipherParameters instanceof AEADParameters) {
            final AEADParameters aeadParameters = (AEADParameters)cipherParameters;
            final byte[] nonce = aeadParameters.getNonce();
            final byte[] iv = this.iv;
            final int length = iv.length;
            final int length2 = nonce.length;
            Arrays.fill(iv, (byte)0);
            System.arraycopy(nonce, 0, this.iv, length - length2, nonce.length);
            this.initialAssociatedText = aeadParameters.getAssociatedText();
            final int macSize = aeadParameters.getMacSize();
            if (macSize < 64 || macSize > this.blockSize << 3 || (macSize & 0x7) != 0x0) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid value for MAC size: ");
                sb.append(macSize);
                throw new IllegalArgumentException(sb.toString());
            }
            this.macSize = macSize >>> 3;
            final KeyParameter key = aeadParameters.getKey();
            final byte[] initialAssociatedText = this.initialAssociatedText;
            keyParameter = key;
            if (initialAssociatedText != null) {
                this.processAADBytes(initialAssociatedText, 0, initialAssociatedText.length);
                keyParameter = key;
            }
        }
        else {
            if (!(cipherParameters instanceof ParametersWithIV)) {
                throw new IllegalArgumentException("Invalid parameter passed");
            }
            final ParametersWithIV parametersWithIV = (ParametersWithIV)cipherParameters;
            final byte[] iv2 = parametersWithIV.getIV();
            final byte[] iv3 = this.iv;
            final int length3 = iv3.length;
            final int length4 = iv2.length;
            Arrays.fill(iv3, (byte)0);
            System.arraycopy(iv2, 0, this.iv, length3 - length4, iv2.length);
            this.initialAssociatedText = null;
            this.macSize = this.blockSize;
            keyParameter = (KeyParameter)parametersWithIV.getParameters();
        }
        this.macBlock = new byte[this.blockSize];
        this.ctrEngine.init(true, new ParametersWithIV(keyParameter, this.iv));
        this.engine.init(true, keyParameter);
    }
    
    @Override
    public void processAADByte(final byte b) {
        this.associatedText.write(b);
    }
    
    @Override
    public void processAADBytes(final byte[] b, final int off, final int len) {
        this.associatedText.write(b, off, len);
    }
    
    @Override
    public int processByte(final byte b, final byte[] array, final int n) throws DataLengthException, IllegalStateException {
        this.data.write(b);
        return 0;
    }
    
    @Override
    public int processBytes(final byte[] b, final int off, final int len, final byte[] array, final int n) throws DataLengthException, IllegalStateException {
        if (b.length >= off + len) {
            this.data.write(b, off, len);
            return 0;
        }
        throw new DataLengthException("input buffer too short");
    }
    
    @Override
    public void reset() {
        Arrays.fill(this.b, 0L);
        this.engine.reset();
        this.data.reset();
        this.associatedText.reset();
        final byte[] initialAssociatedText = this.initialAssociatedText;
        if (initialAssociatedText != null) {
            this.processAADBytes(initialAssociatedText, 0, initialAssociatedText.length);
        }
    }
    
    private class ExposedByteArrayOutputStream extends ByteArrayOutputStream
    {
        public ExposedByteArrayOutputStream() {
        }
        
        public byte[] getBuffer() {
            return this.buf;
        }
    }
}
