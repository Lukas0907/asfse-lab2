// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.modes;

import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.StreamBlockCipher;

public class G3413CTRBlockCipher extends StreamBlockCipher
{
    private byte[] CTR;
    private byte[] IV;
    private final int blockSize;
    private byte[] buf;
    private int byteCount;
    private final BlockCipher cipher;
    private boolean initialized;
    private final int s;
    
    public G3413CTRBlockCipher(final BlockCipher blockCipher) {
        this(blockCipher, blockCipher.getBlockSize() * 8);
    }
    
    public G3413CTRBlockCipher(final BlockCipher cipher, final int n) {
        super(cipher);
        this.byteCount = 0;
        if (n >= 0 && n <= cipher.getBlockSize() * 8) {
            this.cipher = cipher;
            this.blockSize = cipher.getBlockSize();
            this.s = n / 8;
            this.CTR = new byte[this.blockSize];
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Parameter bitBlockSize must be in range 0 < bitBlockSize <= ");
        sb.append(cipher.getBlockSize() * 8);
        throw new IllegalArgumentException(sb.toString());
    }
    
    private byte[] generateBuf() {
        final byte[] ctr = this.CTR;
        final byte[] array = new byte[ctr.length];
        this.cipher.processBlock(ctr, 0, array, 0);
        return GOST3413CipherUtil.MSB(array, this.s);
    }
    
    private void generateCRT() {
        final byte[] ctr = this.CTR;
        final int n = ctr.length - 1;
        ++ctr[n];
    }
    
    private void initArrays() {
        final int blockSize = this.blockSize;
        this.IV = new byte[blockSize / 2];
        this.CTR = new byte[blockSize];
        this.buf = new byte[this.s];
    }
    
    @Override
    protected byte calculateByte(final byte b) {
        if (this.byteCount == 0) {
            this.buf = this.generateBuf();
        }
        final byte[] buf = this.buf;
        final int byteCount = this.byteCount;
        final byte b2 = (byte)(b ^ buf[byteCount]);
        this.byteCount = byteCount + 1;
        if (this.byteCount == this.s) {
            this.byteCount = 0;
            this.generateCRT();
        }
        return b2;
    }
    
    @Override
    public String getAlgorithmName() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.cipher.getAlgorithmName());
        sb.append("/GCTR");
        return sb.toString();
    }
    
    @Override
    public int getBlockSize() {
        return this.s;
    }
    
    @Override
    public void init(final boolean b, final CipherParameters cipherParameters) throws IllegalArgumentException {
        Label_0147: {
            BlockCipher cipher;
            CipherParameters parameters;
            if (cipherParameters instanceof ParametersWithIV) {
                final ParametersWithIV parametersWithIV = (ParametersWithIV)cipherParameters;
                this.initArrays();
                this.IV = Arrays.clone(parametersWithIV.getIV());
                final byte[] iv = this.IV;
                if (iv.length != this.blockSize / 2) {
                    throw new IllegalArgumentException("Parameter IV length must be == blockSize/2");
                }
                System.arraycopy(iv, 0, this.CTR, 0, iv.length);
                for (int i = this.IV.length; i < this.blockSize; ++i) {
                    this.CTR[i] = 0;
                }
                if (parametersWithIV.getParameters() == null) {
                    break Label_0147;
                }
                cipher = this.cipher;
                parameters = parametersWithIV.getParameters();
            }
            else {
                this.initArrays();
                if (cipherParameters == null) {
                    break Label_0147;
                }
                final BlockCipher cipher2 = this.cipher;
                parameters = cipherParameters;
                cipher = cipher2;
            }
            cipher.init(true, parameters);
        }
        this.initialized = true;
    }
    
    @Override
    public int processBlock(final byte[] array, final int n, final byte[] array2, final int n2) throws DataLengthException, IllegalStateException {
        this.processBytes(array, n, this.s, array2, n2);
        return this.s;
    }
    
    @Override
    public void reset() {
        if (this.initialized) {
            final byte[] iv = this.IV;
            System.arraycopy(iv, 0, this.CTR, 0, iv.length);
            for (int i = this.IV.length; i < this.blockSize; ++i) {
                this.CTR[i] = 0;
            }
            this.byteCount = 0;
            this.cipher.reset();
        }
    }
}
