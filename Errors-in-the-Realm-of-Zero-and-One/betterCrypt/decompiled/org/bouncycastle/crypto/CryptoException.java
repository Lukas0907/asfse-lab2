// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto;

public class CryptoException extends Exception
{
    private Throwable cause;
    
    public CryptoException() {
    }
    
    public CryptoException(final String message) {
        super(message);
    }
    
    public CryptoException(final String message, final Throwable cause) {
        super(message);
        this.cause = cause;
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
