// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.ec;

import java.math.BigInteger;

public interface ECPairFactorTransform extends ECPairTransform
{
    BigInteger getTransformValue();
}
