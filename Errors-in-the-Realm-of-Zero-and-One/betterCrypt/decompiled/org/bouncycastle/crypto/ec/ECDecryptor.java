// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.ec;

import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.math.ec.ECPoint;

public interface ECDecryptor
{
    ECPoint decrypt(final ECPair p0);
    
    void init(final CipherParameters p0);
}
