// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.ec;

import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.math.ec.ECAlgorithms;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;

public class ECElGamalDecryptor implements ECDecryptor
{
    private ECPrivateKeyParameters key;
    
    @Override
    public ECPoint decrypt(final ECPair ecPair) {
        final ECPrivateKeyParameters key = this.key;
        if (key != null) {
            final ECCurve curve = key.getParameters().getCurve();
            return ECAlgorithms.cleanPoint(curve, ecPair.getY()).subtract(ECAlgorithms.cleanPoint(curve, ecPair.getX()).multiply(this.key.getD())).normalize();
        }
        throw new IllegalStateException("ECElGamalDecryptor not initialised");
    }
    
    @Override
    public void init(final CipherParameters cipherParameters) {
        if (cipherParameters instanceof ECPrivateKeyParameters) {
            this.key = (ECPrivateKeyParameters)cipherParameters;
            return;
        }
        throw new IllegalArgumentException("ECPrivateKeyParameters are required for decryption.");
    }
}
