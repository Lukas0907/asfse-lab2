// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.ec;

import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.math.ec.ECConstants;
import java.security.SecureRandom;
import java.math.BigInteger;

class ECUtil
{
    static BigInteger generateK(final BigInteger val, final SecureRandom secureRandom) {
        final int bitLength = val.bitLength();
        BigInteger randomBigInteger;
        do {
            randomBigInteger = BigIntegers.createRandomBigInteger(bitLength, secureRandom);
        } while (randomBigInteger.equals(ECConstants.ZERO) || randomBigInteger.compareTo(val) >= 0);
        return randomBigInteger;
    }
}
