// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.ec;

import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.math.ec.ECAlgorithms;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.math.ec.FixedPointCombMultiplier;
import org.bouncycastle.math.ec.ECMultiplier;
import java.security.SecureRandom;
import java.math.BigInteger;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;

public class ECNewRandomnessTransform implements ECPairFactorTransform
{
    private ECPublicKeyParameters key;
    private BigInteger lastK;
    private SecureRandom random;
    
    protected ECMultiplier createBasePointMultiplier() {
        return new FixedPointCombMultiplier();
    }
    
    @Override
    public BigInteger getTransformValue() {
        return this.lastK;
    }
    
    @Override
    public void init(final CipherParameters cipherParameters) {
        SecureRandom random;
        if (cipherParameters instanceof ParametersWithRandom) {
            final ParametersWithRandom parametersWithRandom = (ParametersWithRandom)cipherParameters;
            if (!(parametersWithRandom.getParameters() instanceof ECPublicKeyParameters)) {
                throw new IllegalArgumentException("ECPublicKeyParameters are required for new randomness transform.");
            }
            this.key = (ECPublicKeyParameters)parametersWithRandom.getParameters();
            random = parametersWithRandom.getRandom();
        }
        else {
            if (!(cipherParameters instanceof ECPublicKeyParameters)) {
                throw new IllegalArgumentException("ECPublicKeyParameters are required for new randomness transform.");
            }
            this.key = (ECPublicKeyParameters)cipherParameters;
            random = CryptoServicesRegistrar.getSecureRandom();
        }
        this.random = random;
    }
    
    @Override
    public ECPair transform(final ECPair ecPair) {
        final ECPublicKeyParameters key = this.key;
        if (key != null) {
            final ECDomainParameters parameters = key.getParameters();
            final BigInteger n = parameters.getN();
            final ECMultiplier basePointMultiplier = this.createBasePointMultiplier();
            final BigInteger generateK = ECUtil.generateK(n, this.random);
            final ECPoint[] array = { basePointMultiplier.multiply(parameters.getG(), generateK).add(ECAlgorithms.cleanPoint(parameters.getCurve(), ecPair.getX())), this.key.getQ().multiply(generateK).add(ECAlgorithms.cleanPoint(parameters.getCurve(), ecPair.getY())) };
            parameters.getCurve().normalizeAll(array);
            this.lastK = generateK;
            return new ECPair(array[0], array[1]);
        }
        throw new IllegalStateException("ECNewRandomnessTransform not initialised");
    }
}
