// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto;

import org.bouncycastle.crypto.params.AsymmetricKeyParameter;

public interface KeyEncoder
{
    byte[] getEncoded(final AsymmetricKeyParameter p0);
}
