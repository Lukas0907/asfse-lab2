// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.encodings;

import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.util.DigestFactory;
import java.security.SecureRandom;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.AsymmetricBlockCipher;

public class OAEPEncoding implements AsymmetricBlockCipher
{
    private byte[] defHash;
    private AsymmetricBlockCipher engine;
    private boolean forEncryption;
    private Digest mgf1Hash;
    private SecureRandom random;
    
    public OAEPEncoding(final AsymmetricBlockCipher asymmetricBlockCipher) {
        this(asymmetricBlockCipher, DigestFactory.createSHA1(), null);
    }
    
    public OAEPEncoding(final AsymmetricBlockCipher asymmetricBlockCipher, final Digest digest) {
        this(asymmetricBlockCipher, digest, null);
    }
    
    public OAEPEncoding(final AsymmetricBlockCipher engine, final Digest digest, final Digest mgf1Hash, final byte[] array) {
        this.engine = engine;
        this.mgf1Hash = mgf1Hash;
        this.defHash = new byte[digest.getDigestSize()];
        digest.reset();
        if (array != null) {
            digest.update(array, 0, array.length);
        }
        digest.doFinal(this.defHash, 0);
    }
    
    public OAEPEncoding(final AsymmetricBlockCipher asymmetricBlockCipher, final Digest digest, final byte[] array) {
        this(asymmetricBlockCipher, digest, digest, array);
    }
    
    private void ItoOSP(final int n, final byte[] array) {
        array[0] = (byte)(n >>> 24);
        array[1] = (byte)(n >>> 16);
        array[2] = (byte)(n >>> 8);
        array[3] = (byte)(n >>> 0);
    }
    
    private byte[] maskGeneratorFunction1(final byte[] array, final int n, final int n2, final int n3) {
        final byte[] array2 = new byte[n3];
        final byte[] array3 = new byte[this.mgf1Hash.getDigestSize()];
        final byte[] array4 = new byte[4];
        this.mgf1Hash.reset();
        int i;
        for (i = 0; i < n3 / array3.length; ++i) {
            this.ItoOSP(i, array4);
            this.mgf1Hash.update(array, n, n2);
            this.mgf1Hash.update(array4, 0, array4.length);
            this.mgf1Hash.doFinal(array3, 0);
            System.arraycopy(array3, 0, array2, array3.length * i, array3.length);
        }
        if (array3.length * i < n3) {
            this.ItoOSP(i, array4);
            this.mgf1Hash.update(array, n, n2);
            this.mgf1Hash.update(array4, 0, array4.length);
            this.mgf1Hash.doFinal(array3, 0);
            System.arraycopy(array3, 0, array2, array3.length * i, array2.length - i * array3.length);
        }
        return array2;
    }
    
    public byte[] decodeBlock(byte[] array, int i, int n) throws InvalidCipherTextException {
        final byte[] processBlock = this.engine.processBlock(array, i, n);
        array = new byte[this.engine.getOutputBlockSize()];
        if (array.length < this.defHash.length * 2 + 1) {
            i = 1;
        }
        else {
            i = 0;
        }
        if (processBlock.length <= array.length) {
            System.arraycopy(processBlock, 0, array, array.length - processBlock.length, processBlock.length);
            n = i;
        }
        else {
            System.arraycopy(processBlock, 0, array, 0, array.length);
            n = 1;
        }
        final byte[] defHash = this.defHash;
        final byte[] maskGeneratorFunction1 = this.maskGeneratorFunction1(array, defHash.length, array.length - defHash.length, defHash.length);
        i = 0;
        byte[] defHash2;
        while (true) {
            defHash2 = this.defHash;
            if (i == defHash2.length) {
                break;
            }
            array[i] ^= maskGeneratorFunction1[i];
            ++i;
        }
        final byte[] maskGeneratorFunction2 = this.maskGeneratorFunction1(array, 0, defHash2.length, array.length - defHash2.length);
        for (i = this.defHash.length; i != array.length; ++i) {
            array[i] ^= maskGeneratorFunction2[i - this.defHash.length];
        }
        int n2;
        i = (n2 = 0);
        byte[] defHash3;
        while (true) {
            defHash3 = this.defHash;
            if (i == defHash3.length) {
                break;
            }
            if (defHash3[i] != array[defHash3.length + i]) {
                n2 = 1;
            }
            ++i;
        }
        int length = array.length;
        for (i = defHash3.length * 2; i != array.length; ++i) {
            if (array[i] != 0 & length == array.length) {
                length = i;
            }
        }
        if (length > array.length - 1) {
            i = 1;
        }
        else {
            i = 0;
        }
        final boolean b = array[length] != 1;
        final int n3 = length + 1;
        if ((n | n2 | (i | (b ? 1 : 0))) == 0x0) {
            final byte[] array2 = new byte[array.length - n3];
            System.arraycopy(array, n3, array2, 0, array2.length);
            return array2;
        }
        Arrays.fill(array, (byte)0);
        throw new InvalidCipherTextException("data wrong");
    }
    
    public byte[] encodeBlock(byte[] bytes, int i, final int n) throws InvalidCipherTextException {
        if (n <= this.getInputBlockSize()) {
            final byte[] array = new byte[this.getInputBlockSize() + 1 + this.defHash.length * 2];
            System.arraycopy(bytes, i, array, array.length - n, n);
            array[array.length - n - 1] = 1;
            bytes = this.defHash;
            System.arraycopy(bytes, 0, array, bytes.length, bytes.length);
            bytes = new byte[this.defHash.length];
            this.random.nextBytes(bytes);
            final byte[] maskGeneratorFunction1 = this.maskGeneratorFunction1(bytes, 0, bytes.length, array.length - this.defHash.length);
            for (i = this.defHash.length; i != array.length; ++i) {
                array[i] ^= maskGeneratorFunction1[i - this.defHash.length];
            }
            System.arraycopy(bytes, 0, array, 0, this.defHash.length);
            bytes = this.defHash;
            bytes = this.maskGeneratorFunction1(array, bytes.length, array.length - bytes.length, bytes.length);
            for (i = 0; i != this.defHash.length; ++i) {
                array[i] ^= bytes[i];
            }
            return this.engine.processBlock(array, 0, array.length);
        }
        throw new DataLengthException("input data too long");
    }
    
    @Override
    public int getInputBlockSize() {
        int inputBlockSize = this.engine.getInputBlockSize();
        if (this.forEncryption) {
            inputBlockSize = inputBlockSize - 1 - this.defHash.length * 2;
        }
        return inputBlockSize;
    }
    
    @Override
    public int getOutputBlockSize() {
        final int outputBlockSize = this.engine.getOutputBlockSize();
        if (this.forEncryption) {
            return outputBlockSize;
        }
        return outputBlockSize - 1 - this.defHash.length * 2;
    }
    
    public AsymmetricBlockCipher getUnderlyingCipher() {
        return this.engine;
    }
    
    @Override
    public void init(final boolean forEncryption, final CipherParameters cipherParameters) {
        SecureRandom random;
        if (cipherParameters instanceof ParametersWithRandom) {
            random = ((ParametersWithRandom)cipherParameters).getRandom();
        }
        else {
            random = CryptoServicesRegistrar.getSecureRandom();
        }
        this.random = random;
        this.engine.init(forEncryption, cipherParameters);
        this.forEncryption = forEncryption;
    }
    
    @Override
    public byte[] processBlock(final byte[] array, final int n, final int n2) throws InvalidCipherTextException {
        if (this.forEncryption) {
            return this.encodeBlock(array, n, n2);
        }
        return this.decodeBlock(array, n, n2);
    }
}
