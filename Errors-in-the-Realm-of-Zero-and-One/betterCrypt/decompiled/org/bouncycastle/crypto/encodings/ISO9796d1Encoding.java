// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.encodings;

import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.InvalidCipherTextException;
import java.math.BigInteger;
import org.bouncycastle.crypto.AsymmetricBlockCipher;

public class ISO9796d1Encoding implements AsymmetricBlockCipher
{
    private static final BigInteger SIX;
    private static final BigInteger SIXTEEN;
    private static byte[] inverse;
    private static byte[] shadows;
    private int bitSize;
    private AsymmetricBlockCipher engine;
    private boolean forEncryption;
    private BigInteger modulus;
    private int padBits;
    
    static {
        SIXTEEN = BigInteger.valueOf(16L);
        SIX = BigInteger.valueOf(6L);
        ISO9796d1Encoding.shadows = new byte[] { 14, 3, 5, 8, 9, 4, 2, 15, 0, 13, 11, 6, 7, 10, 12, 1 };
        ISO9796d1Encoding.inverse = new byte[] { 8, 15, 6, 1, 5, 2, 11, 12, 3, 4, 13, 10, 14, 9, 0, 7 };
    }
    
    public ISO9796d1Encoding(final AsymmetricBlockCipher engine) {
        this.padBits = 0;
        this.engine = engine;
    }
    
    private static byte[] convertOutputDecryptOnly(final BigInteger bigInteger) {
        final byte[] byteArray = bigInteger.toByteArray();
        if (byteArray[0] == 0) {
            final byte[] array = new byte[byteArray.length - 1];
            System.arraycopy(byteArray, 1, array, 0, array.length);
            return array;
        }
        return byteArray;
    }
    
    private byte[] decodeBlock(byte[] magnitude, int i, int n) throws InvalidCipherTextException {
        magnitude = this.engine.processBlock(magnitude, i, n);
        final int n2 = (this.bitSize + 13) / 16;
        BigInteger subtract = new BigInteger(1, magnitude);
        if (!subtract.mod(ISO9796d1Encoding.SIXTEEN).equals(ISO9796d1Encoding.SIX)) {
            if (!this.modulus.subtract(subtract).mod(ISO9796d1Encoding.SIXTEEN).equals(ISO9796d1Encoding.SIX)) {
                throw new InvalidCipherTextException("resulting integer iS or (modulus - iS) is not congruent to 6 mod 16");
            }
            subtract = this.modulus.subtract(subtract);
        }
        magnitude = convertOutputDecryptOnly(subtract);
        if ((magnitude[magnitude.length - 1] & 0xF) == 0x6) {
            magnitude[magnitude.length - 1] = (byte)((magnitude[magnitude.length - 1] & 0xFF) >>> 4 | ISO9796d1Encoding.inverse[(magnitude[magnitude.length - 2] & 0xFF) >> 4] << 4);
            final byte[] shadows = ISO9796d1Encoding.shadows;
            i = shadows[(magnitude[1] & 0xFF) >>> 4];
            final byte b = (byte)(shadows[magnitude[1] & 0xF] | i << 4);
            final int n3 = 0;
            magnitude[0] = b;
            i = magnitude.length - 1;
            n = 1;
            int n5;
            int n4 = n5 = 0;
            while (i >= magnitude.length - n2 * 2) {
                final byte[] shadows2 = ISO9796d1Encoding.shadows;
                final int n6 = shadows2[magnitude[i] & 0xF] | shadows2[(magnitude[i] & 0xFF) >>> 4] << 4;
                final int n7 = i - 1;
                int n8 = n5;
                if (((magnitude[n7] ^ n6) & 0xFF) != 0x0) {
                    if (n5 != 0) {
                        throw new InvalidCipherTextException("invalid tsums in block");
                    }
                    n = magnitude[n7];
                    n8 = 1;
                    n = ((n ^ n6) & 0xFF);
                    n4 = n7;
                }
                i -= 2;
                n5 = n8;
            }
            magnitude[n4] = 0;
            byte[] array;
            for (array = new byte[(magnitude.length - n4) / 2], i = n3; i < array.length; ++i) {
                array[i] = magnitude[i * 2 + n4 + 1];
            }
            this.padBits = n - 1;
            return array;
        }
        throw new InvalidCipherTextException("invalid forcing byte in block");
    }
    
    private byte[] encodeBlock(byte[] shadows, int i, final int n) throws InvalidCipherTextException {
        final int bitSize = this.bitSize;
        final byte[] array = new byte[(bitSize + 7) / 8];
        final int padBits = this.padBits;
        final int n2 = 1;
        final int n3 = (bitSize + 13) / 16;
        for (int j = 0; j < n3; j += n) {
            if (j > n3 - n) {
                final int n4 = n3 - j;
                System.arraycopy(shadows, i + n - n4, array, array.length - n3, n4);
            }
            else {
                System.arraycopy(shadows, i, array, array.length - (j + n), n);
            }
        }
        byte b;
        for (i = array.length - n3 * 2; i != array.length; i += 2) {
            b = array[array.length - n3 + i / 2];
            shadows = ISO9796d1Encoding.shadows;
            array[i] = (byte)(shadows[b & 0xF] | shadows[(b & 0xFF) >>> 4] << 4);
            array[i + 1] = b;
        }
        i = array.length - n * 2;
        array[i] ^= (byte)(padBits + 1);
        array[array.length - 1] = (byte)(array[array.length - 1] << 4 | 0x6);
        i = 8 - (this.bitSize - 1) % 8;
        if (i != 8) {
            array[0] &= (byte)(255 >>> i);
            array[0] |= (byte)(128 >>> i);
            i = 0;
        }
        else {
            array[0] = 0;
            array[1] |= (byte)128;
            i = n2;
        }
        return this.engine.processBlock(array, i, array.length - i);
    }
    
    @Override
    public int getInputBlockSize() {
        int inputBlockSize = this.engine.getInputBlockSize();
        if (this.forEncryption) {
            inputBlockSize = (inputBlockSize + 1) / 2;
        }
        return inputBlockSize;
    }
    
    @Override
    public int getOutputBlockSize() {
        final int outputBlockSize = this.engine.getOutputBlockSize();
        if (this.forEncryption) {
            return outputBlockSize;
        }
        return (outputBlockSize + 1) / 2;
    }
    
    public int getPadBits() {
        return this.padBits;
    }
    
    public AsymmetricBlockCipher getUnderlyingCipher() {
        return this.engine;
    }
    
    @Override
    public void init(final boolean forEncryption, final CipherParameters cipherParameters) {
        RSAKeyParameters rsaKeyParameters;
        if (cipherParameters instanceof ParametersWithRandom) {
            rsaKeyParameters = (RSAKeyParameters)((ParametersWithRandom)cipherParameters).getParameters();
        }
        else {
            rsaKeyParameters = (RSAKeyParameters)cipherParameters;
        }
        this.engine.init(forEncryption, cipherParameters);
        this.modulus = rsaKeyParameters.getModulus();
        this.bitSize = this.modulus.bitLength();
        this.forEncryption = forEncryption;
    }
    
    @Override
    public byte[] processBlock(final byte[] array, final int n, final int n2) throws InvalidCipherTextException {
        if (this.forEncryption) {
            return this.encodeBlock(array, n, n2);
        }
        return this.decodeBlock(array, n, n2);
    }
    
    public void setPadBits(final int padBits) {
        if (padBits <= 7) {
            this.padBits = padBits;
            return;
        }
        throw new IllegalArgumentException("padBits > 7");
    }
}
