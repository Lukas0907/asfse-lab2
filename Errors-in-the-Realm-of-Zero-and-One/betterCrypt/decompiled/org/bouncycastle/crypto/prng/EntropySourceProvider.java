// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.prng;

public interface EntropySourceProvider
{
    EntropySource get(final int p0);
}
