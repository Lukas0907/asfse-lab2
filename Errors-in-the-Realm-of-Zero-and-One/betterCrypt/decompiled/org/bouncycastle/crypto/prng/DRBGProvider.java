// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.prng;

import org.bouncycastle.crypto.prng.drbg.SP80090DRBG;

interface DRBGProvider
{
    SP80090DRBG get(final EntropySource p0);
}
