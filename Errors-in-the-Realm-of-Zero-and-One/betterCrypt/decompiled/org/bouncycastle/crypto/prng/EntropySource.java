// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.prng;

public interface EntropySource
{
    int entropySize();
    
    byte[] getEntropy();
    
    boolean isPredictionResistant();
}
