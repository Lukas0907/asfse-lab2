// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.prng.drbg;

import org.bouncycastle.crypto.Mac;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.util.Integers;
import java.util.Hashtable;

class Utils
{
    static final Hashtable maxSecurityStrengths;
    
    static {
        (maxSecurityStrengths = new Hashtable()).put("SHA-1", Integers.valueOf(128));
        Utils.maxSecurityStrengths.put("SHA-224", Integers.valueOf(192));
        Utils.maxSecurityStrengths.put("SHA-256", Integers.valueOf(256));
        Utils.maxSecurityStrengths.put("SHA-384", Integers.valueOf(256));
        Utils.maxSecurityStrengths.put("SHA-512", Integers.valueOf(256));
        Utils.maxSecurityStrengths.put("SHA-512/224", Integers.valueOf(192));
        Utils.maxSecurityStrengths.put("SHA-512/256", Integers.valueOf(256));
    }
    
    static int getMaxSecurityStrength(final Digest digest) {
        return Utils.maxSecurityStrengths.get(digest.getAlgorithmName());
    }
    
    static int getMaxSecurityStrength(final Mac mac) {
        final String algorithmName = mac.getAlgorithmName();
        return (int)Utils.maxSecurityStrengths.get(algorithmName.substring(0, algorithmName.indexOf("/")));
    }
    
    static byte[] hash_df(final Digest digest, final byte[] array, int i) {
        final byte[] array2 = new byte[(i + 7) / 8];
        final int n = array2.length / digest.getDigestSize();
        final byte[] array3 = new byte[digest.getDigestSize()];
        final int n2 = 0;
        int n3 = 1;
        for (int j = 0; j <= n; ++j) {
            digest.update((byte)n3);
            digest.update((byte)(i >> 24));
            digest.update((byte)(i >> 16));
            digest.update((byte)(i >> 8));
            digest.update((byte)i);
            digest.update(array, 0, array.length);
            digest.doFinal(array3, 0);
            int length;
            if (array2.length - array3.length * j > array3.length) {
                length = array3.length;
            }
            else {
                length = array2.length - array3.length * j;
            }
            System.arraycopy(array3, 0, array2, array3.length * j, length);
            ++n3;
        }
        i %= 8;
        if (i != 0) {
            final int n4 = 8 - i;
            int n5 = 0;
            int n6;
            for (i = n2; i != array2.length; ++i, n5 = n6) {
                n6 = (array2[i] & 0xFF);
                array2[i] = (byte)(n5 << 8 - n4 | n6 >>> n4);
            }
        }
        return array2;
    }
    
    static boolean isTooLarge(final byte[] array, final int n) {
        return array != null && array.length > n;
    }
}
