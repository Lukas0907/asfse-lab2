// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.prng.drbg;

public interface SP80090DRBG
{
    int generate(final byte[] p0, final byte[] p1, final boolean p2);
    
    int getBlockSize();
    
    void reseed(final byte[] p0);
}
