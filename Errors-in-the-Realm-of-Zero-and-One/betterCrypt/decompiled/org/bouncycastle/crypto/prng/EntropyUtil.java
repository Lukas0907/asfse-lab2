// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.prng;

public class EntropyUtil
{
    public static byte[] generateSeed(final EntropySource entropySource, int i) {
        final byte[] array = new byte[i];
        if (i * 8 <= entropySource.entropySize()) {
            System.arraycopy(entropySource.getEntropy(), 0, array, 0, array.length);
            return array;
        }
        int n;
        byte[] entropy;
        int length;
        for (n = entropySource.entropySize() / 8, i = 0; i < array.length; i += n) {
            entropy = entropySource.getEntropy();
            if (entropy.length <= array.length - i) {
                length = entropy.length;
            }
            else {
                length = array.length - i;
            }
            System.arraycopy(entropy, 0, array, i, length);
        }
        return array;
    }
}
