// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.prng;

public class ThreadedSeedGenerator
{
    public byte[] generateSeed(final int n, final boolean b) {
        return new SeedGenerator().generateSeed(n, b);
    }
    
    private class SeedGenerator implements Runnable
    {
        private volatile int counter;
        private volatile boolean stop;
        
        private SeedGenerator() {
            this.counter = 0;
            this.stop = false;
        }
        
        public byte[] generateSeed(int n, final boolean b) {
            final Thread thread = new Thread(this);
            final byte[] array = new byte[n];
            int n2 = 0;
            this.counter = 0;
            this.stop = false;
            thread.start();
            if (!b) {
                n *= 8;
            }
            int counter = 0;
            while (true) {
                Label_0123: {
                    if (n2 >= n) {
                        break Label_0123;
                    }
                Block_4_Outer:
                    while (true) {
                        Label_0068: {
                            if (this.counter != counter) {
                                break Label_0068;
                            }
                            try {
                                Thread.sleep(1L);
                                continue Block_4_Outer;
                                // iftrue(Label_0092:, !b)
                            Label_0116:
                                while (true) {
                                    array[n2] = (byte)(counter & 0xFF);
                                    break Label_0116;
                                    final int n3;
                                    Label_0092: {
                                        n3 = n2 / 8;
                                    }
                                    array[n3] = (byte)(array[n3] << 1 | (counter & 0x1));
                                    break Label_0116;
                                    this.stop = true;
                                    return array;
                                    counter = this.counter;
                                    continue;
                                }
                                ++n2;
                            }
                            catch (InterruptedException ex) {
                                continue;
                            }
                        }
                        break;
                    }
                }
            }
        }
        
        @Override
        public void run() {
            while (!this.stop) {
                ++this.counter;
            }
        }
    }
}
