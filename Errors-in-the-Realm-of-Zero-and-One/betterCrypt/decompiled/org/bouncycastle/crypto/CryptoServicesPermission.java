// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.security.Permission;

public class CryptoServicesPermission extends Permission
{
    public static final String DEFAULT_RANDOM = "defaultRandomConfig";
    public static final String GLOBAL_CONFIG = "globalConfig";
    public static final String THREAD_LOCAL_CONFIG = "threadLocalConfig";
    private final Set<String> actions;
    
    public CryptoServicesPermission(final String name) {
        super(name);
        (this.actions = new HashSet<String>()).add(name);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof CryptoServicesPermission && this.actions.equals(((CryptoServicesPermission)o).actions);
    }
    
    @Override
    public String getActions() {
        return this.actions.toString();
    }
    
    @Override
    public int hashCode() {
        return this.actions.hashCode();
    }
    
    @Override
    public boolean implies(final Permission permission) {
        if (permission instanceof CryptoServicesPermission) {
            final CryptoServicesPermission cryptoServicesPermission = (CryptoServicesPermission)permission;
            if (this.getName().equals(cryptoServicesPermission.getName())) {
                return true;
            }
            if (this.actions.containsAll(cryptoServicesPermission.actions)) {
                return true;
            }
        }
        return false;
    }
}
