// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.paddings;

import org.bouncycastle.crypto.InvalidCipherTextException;
import java.security.SecureRandom;

public class ZeroBytePadding implements BlockCipherPadding
{
    @Override
    public int addPadding(final byte[] array, final int n) {
        final int length = array.length;
        for (int i = n; i < array.length; ++i) {
            array[i] = 0;
        }
        return length - n;
    }
    
    @Override
    public String getPaddingName() {
        return "ZeroByte";
    }
    
    @Override
    public void init(final SecureRandom secureRandom) throws IllegalArgumentException {
    }
    
    @Override
    public int padCount(final byte[] array) throws InvalidCipherTextException {
        int length;
        for (length = array.length; length > 0 && array[length - 1] == 0; --length) {}
        return array.length - length;
    }
}
