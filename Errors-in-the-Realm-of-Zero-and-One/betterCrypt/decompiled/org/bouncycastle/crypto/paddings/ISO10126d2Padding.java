// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.paddings;

import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import java.security.SecureRandom;

public class ISO10126d2Padding implements BlockCipherPadding
{
    SecureRandom random;
    
    @Override
    public int addPadding(final byte[] array, int i) {
        final byte b = (byte)(array.length - i);
        while (i < array.length - 1) {
            array[i] = (byte)this.random.nextInt();
            ++i;
        }
        return array[i] = b;
    }
    
    @Override
    public String getPaddingName() {
        return "ISO10126-2";
    }
    
    @Override
    public void init(SecureRandom secureRandom) throws IllegalArgumentException {
        if (secureRandom == null) {
            secureRandom = CryptoServicesRegistrar.getSecureRandom();
        }
        this.random = secureRandom;
    }
    
    @Override
    public int padCount(final byte[] array) throws InvalidCipherTextException {
        final int n = array[array.length - 1] & 0xFF;
        if (n <= array.length) {
            return n;
        }
        throw new InvalidCipherTextException("pad block corrupted");
    }
}
