// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.paddings;

import org.bouncycastle.crypto.InvalidCipherTextException;
import java.security.SecureRandom;

public class TBCPadding implements BlockCipherPadding
{
    @Override
    public int addPadding(final byte[] array, final int n) {
        final int length = array.length;
        int n2 = 255;
        Label_0043: {
            if (n > 0) {
                if ((array[n - 1] & 0x1) == 0x0) {
                    break Label_0043;
                }
            }
            else if ((array[array.length - 1] & 0x1) == 0x0) {
                break Label_0043;
            }
            n2 = 0;
        }
        final byte b = (byte)n2;
        for (int i = n; i < array.length; ++i) {
            array[i] = b;
        }
        return length - n;
    }
    
    @Override
    public String getPaddingName() {
        return "TBC";
    }
    
    @Override
    public void init(final SecureRandom secureRandom) throws IllegalArgumentException {
    }
    
    @Override
    public int padCount(final byte[] array) throws InvalidCipherTextException {
        byte b;
        int n;
        for (b = array[array.length - 1], n = array.length - 1; n > 0 && array[n - 1] == b; --n) {}
        return array.length - n;
    }
}
