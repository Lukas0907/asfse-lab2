// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.paddings;

import org.bouncycastle.crypto.InvalidCipherTextException;
import java.security.SecureRandom;

public class ISO7816d4Padding implements BlockCipherPadding
{
    @Override
    public int addPadding(final byte[] array, final int n) {
        final int length = array.length;
        array[n] = -128;
        int n2 = n;
        while (true) {
            ++n2;
            if (n2 >= array.length) {
                break;
            }
            array[n2] = 0;
        }
        return length - n;
    }
    
    @Override
    public String getPaddingName() {
        return "ISO7816-4";
    }
    
    @Override
    public void init(final SecureRandom secureRandom) throws IllegalArgumentException {
    }
    
    @Override
    public int padCount(final byte[] array) throws InvalidCipherTextException {
        int n;
        for (n = array.length - 1; n > 0 && array[n] == 0; --n) {}
        if (array[n] == -128) {
            return array.length - n;
        }
        throw new InvalidCipherTextException("pad block corrupted");
    }
}
