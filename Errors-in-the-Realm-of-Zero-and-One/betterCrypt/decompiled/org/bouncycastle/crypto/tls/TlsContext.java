// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.security.SecureRandom;
import org.bouncycastle.crypto.prng.RandomGenerator;

public interface TlsContext
{
    byte[] exportKeyingMaterial(final String p0, final byte[] p1, final int p2);
    
    ProtocolVersion getClientVersion();
    
    RandomGenerator getNonceRandomGenerator();
    
    TlsSession getResumableSession();
    
    SecureRandom getSecureRandom();
    
    SecurityParameters getSecurityParameters();
    
    ProtocolVersion getServerVersion();
    
    Object getUserObject();
    
    boolean isServer();
    
    void setUserObject(final Object p0);
}
