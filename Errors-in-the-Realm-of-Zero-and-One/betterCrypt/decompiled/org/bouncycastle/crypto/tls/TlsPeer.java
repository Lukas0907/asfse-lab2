// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.IOException;

public interface TlsPeer
{
    void cancel() throws IOException;
    
    TlsCipher getCipher() throws IOException;
    
    TlsCompression getCompression() throws IOException;
    
    void notifyAlertRaised(final short p0, final short p1, final String p2, final Throwable p3);
    
    void notifyAlertReceived(final short p0, final short p1);
    
    void notifyCloseHandle(final TlsCloseable p0);
    
    void notifyHandshakeComplete() throws IOException;
    
    void notifySecureRenegotiation(final boolean p0) throws IOException;
    
    boolean requiresExtendedMasterSecret();
    
    boolean shouldUseGMTUnixTime();
}
