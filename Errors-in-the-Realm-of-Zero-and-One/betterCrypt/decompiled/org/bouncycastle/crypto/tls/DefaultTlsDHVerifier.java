// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.math.BigInteger;
import org.bouncycastle.crypto.params.DHParameters;
import org.bouncycastle.crypto.agreement.DHStandardGroups;
import java.util.Vector;

public class DefaultTlsDHVerifier implements TlsDHVerifier
{
    protected static final Vector DEFAULT_GROUPS;
    public static final int DEFAULT_MINIMUM_PRIME_BITS = 2048;
    protected Vector groups;
    protected int minimumPrimeBits;
    
    static {
        DEFAULT_GROUPS = new Vector();
        addDefaultGroup(DHStandardGroups.rfc7919_ffdhe2048);
        addDefaultGroup(DHStandardGroups.rfc7919_ffdhe3072);
        addDefaultGroup(DHStandardGroups.rfc7919_ffdhe4096);
        addDefaultGroup(DHStandardGroups.rfc7919_ffdhe6144);
        addDefaultGroup(DHStandardGroups.rfc7919_ffdhe8192);
        addDefaultGroup(DHStandardGroups.rfc3526_1536);
        addDefaultGroup(DHStandardGroups.rfc3526_2048);
        addDefaultGroup(DHStandardGroups.rfc3526_3072);
        addDefaultGroup(DHStandardGroups.rfc3526_4096);
        addDefaultGroup(DHStandardGroups.rfc3526_6144);
        addDefaultGroup(DHStandardGroups.rfc3526_8192);
    }
    
    public DefaultTlsDHVerifier() {
        this(2048);
    }
    
    public DefaultTlsDHVerifier(final int n) {
        this(DefaultTlsDHVerifier.DEFAULT_GROUPS, n);
    }
    
    public DefaultTlsDHVerifier(final Vector groups, final int minimumPrimeBits) {
        this.groups = groups;
        this.minimumPrimeBits = minimumPrimeBits;
    }
    
    private static void addDefaultGroup(final DHParameters obj) {
        DefaultTlsDHVerifier.DEFAULT_GROUPS.addElement(obj);
    }
    
    @Override
    public boolean accept(final DHParameters dhParameters) {
        return this.checkMinimumPrimeBits(dhParameters) && this.checkGroup(dhParameters);
    }
    
    protected boolean areGroupsEqual(final DHParameters dhParameters, final DHParameters dhParameters2) {
        return dhParameters == dhParameters2 || (this.areParametersEqual(dhParameters.getP(), dhParameters2.getP()) && this.areParametersEqual(dhParameters.getG(), dhParameters2.getG()));
    }
    
    protected boolean areParametersEqual(final BigInteger bigInteger, final BigInteger x) {
        return bigInteger == x || bigInteger.equals(x);
    }
    
    protected boolean checkGroup(final DHParameters dhParameters) {
        for (int i = 0; i < this.groups.size(); ++i) {
            if (this.areGroupsEqual(dhParameters, (DHParameters)this.groups.elementAt(i))) {
                return true;
            }
        }
        return false;
    }
    
    protected boolean checkMinimumPrimeBits(final DHParameters dhParameters) {
        return dhParameters.getP().bitLength() >= this.getMinimumPrimeBits();
    }
    
    public int getMinimumPrimeBits() {
        return this.minimumPrimeBits;
    }
}
