// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.net.DatagramPacket;
import java.io.IOException;
import java.net.DatagramSocket;

public class UDPTransport implements DatagramTransport
{
    protected static final int MAX_IP_OVERHEAD = 84;
    protected static final int MIN_IP_OVERHEAD = 20;
    protected static final int UDP_OVERHEAD = 8;
    protected final int receiveLimit;
    protected final int sendLimit;
    protected final DatagramSocket socket;
    
    public UDPTransport(final DatagramSocket socket, final int n) throws IOException {
        if (socket.isBound() && socket.isConnected()) {
            this.socket = socket;
            this.receiveLimit = n - 20 - 8;
            this.sendLimit = n - 84 - 8;
            return;
        }
        throw new IllegalArgumentException("'socket' must be bound and connected");
    }
    
    @Override
    public void close() throws IOException {
        this.socket.close();
    }
    
    @Override
    public int getReceiveLimit() {
        return this.receiveLimit;
    }
    
    @Override
    public int getSendLimit() {
        return this.sendLimit;
    }
    
    @Override
    public int receive(final byte[] buf, final int offset, final int length, final int soTimeout) throws IOException {
        this.socket.setSoTimeout(soTimeout);
        final DatagramPacket p4 = new DatagramPacket(buf, offset, length);
        this.socket.receive(p4);
        return p4.getLength();
    }
    
    @Override
    public void send(final byte[] buf, final int offset, final int length) throws IOException {
        if (length <= this.getSendLimit()) {
            this.socket.send(new DatagramPacket(buf, offset, length));
            return;
        }
        throw new TlsFatalAlert((short)80);
    }
}
