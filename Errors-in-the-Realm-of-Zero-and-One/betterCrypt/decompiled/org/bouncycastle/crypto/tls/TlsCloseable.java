// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.IOException;

public interface TlsCloseable
{
    void close() throws IOException;
}
