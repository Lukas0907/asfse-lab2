// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

public interface TlsPSKIdentity
{
    byte[] getPSK();
    
    byte[] getPSKIdentity();
    
    void notifyIdentityHint(final byte[] p0);
    
    void skipIdentityHint();
}
