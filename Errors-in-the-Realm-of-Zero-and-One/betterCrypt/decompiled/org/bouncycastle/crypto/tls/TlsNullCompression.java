// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.OutputStream;

public class TlsNullCompression implements TlsCompression
{
    @Override
    public OutputStream compress(final OutputStream outputStream) {
        return outputStream;
    }
    
    @Override
    public OutputStream decompress(final OutputStream outputStream) {
        return outputStream;
    }
}
