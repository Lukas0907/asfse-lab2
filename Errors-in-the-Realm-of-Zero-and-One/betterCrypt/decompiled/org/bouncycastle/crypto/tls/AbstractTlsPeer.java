// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.IOException;

public abstract class AbstractTlsPeer implements TlsPeer
{
    private volatile TlsCloseable closeHandle;
    
    @Override
    public void cancel() throws IOException {
        final TlsCloseable closeHandle = this.closeHandle;
        if (closeHandle != null) {
            closeHandle.close();
        }
    }
    
    @Override
    public void notifyAlertRaised(final short n, final short n2, final String s, final Throwable t) {
    }
    
    @Override
    public void notifyAlertReceived(final short n, final short n2) {
    }
    
    @Override
    public void notifyCloseHandle(final TlsCloseable closeHandle) {
        this.closeHandle = closeHandle;
    }
    
    @Override
    public void notifyHandshakeComplete() throws IOException {
    }
    
    @Override
    public void notifySecureRenegotiation(final boolean b) throws IOException {
        if (b) {
            return;
        }
        throw new TlsFatalAlert((short)40);
    }
    
    @Override
    public boolean requiresExtendedMasterSecret() {
        return false;
    }
    
    @Override
    public boolean shouldUseGMTUnixTime() {
        return false;
    }
}
