// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.crypto.Digest;

public interface TlsHandshakeHash extends Digest
{
    Digest forkPRFHash();
    
    byte[] getFinalHash(final short p0);
    
    void init(final TlsContext p0);
    
    TlsHandshakeHash notifyPRFDetermined();
    
    void sealHashAlgorithms();
    
    TlsHandshakeHash stopTracking();
    
    void trackHashAlgorithm(final short p0);
}
