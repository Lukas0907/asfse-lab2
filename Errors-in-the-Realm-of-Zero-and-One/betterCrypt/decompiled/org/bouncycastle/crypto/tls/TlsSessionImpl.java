// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.util.Arrays;

class TlsSessionImpl implements TlsSession
{
    boolean resumable;
    final byte[] sessionID;
    final SessionParameters sessionParameters;
    
    TlsSessionImpl(final byte[] array, final SessionParameters sessionParameters) {
        if (array == null) {
            throw new IllegalArgumentException("'sessionID' cannot be null");
        }
        if (array.length <= 32) {
            this.sessionID = Arrays.clone(array);
            this.sessionParameters = sessionParameters;
            this.resumable = (array.length > 0 && sessionParameters != null && sessionParameters.isExtendedMasterSecret());
            return;
        }
        throw new IllegalArgumentException("'sessionID' cannot be longer than 32 bytes");
    }
    
    @Override
    public SessionParameters exportSessionParameters() {
        synchronized (this) {
            SessionParameters copy;
            if (this.sessionParameters == null) {
                copy = null;
            }
            else {
                copy = this.sessionParameters.copy();
            }
            return copy;
        }
    }
    
    @Override
    public byte[] getSessionID() {
        synchronized (this) {
            return this.sessionID;
        }
    }
    
    @Override
    public void invalidate() {
        synchronized (this) {
            this.resumable = false;
        }
    }
    
    @Override
    public boolean isResumable() {
        synchronized (this) {
            return this.resumable;
        }
    }
}
