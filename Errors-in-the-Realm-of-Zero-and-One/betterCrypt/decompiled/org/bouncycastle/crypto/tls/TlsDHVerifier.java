// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.crypto.params.DHParameters;

public interface TlsDHVerifier
{
    boolean accept(final DHParameters p0);
}
