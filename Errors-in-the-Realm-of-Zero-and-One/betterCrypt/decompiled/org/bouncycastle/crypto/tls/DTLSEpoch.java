// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.IOException;

class DTLSEpoch
{
    private final TlsCipher cipher;
    private final int epoch;
    private final DTLSReplayWindow replayWindow;
    private long sequenceNumber;
    
    DTLSEpoch(final int epoch, final TlsCipher cipher) {
        this.replayWindow = new DTLSReplayWindow();
        this.sequenceNumber = 0L;
        if (epoch < 0) {
            throw new IllegalArgumentException("'epoch' must be >= 0");
        }
        if (cipher != null) {
            this.epoch = epoch;
            this.cipher = cipher;
            return;
        }
        throw new IllegalArgumentException("'cipher' cannot be null");
    }
    
    long allocateSequenceNumber() throws IOException {
        synchronized (this) {
            if (this.sequenceNumber < 281474976710656L) {
                final long sequenceNumber = this.sequenceNumber;
                this.sequenceNumber = 1L + sequenceNumber;
                return sequenceNumber;
            }
            throw new TlsFatalAlert((short)80);
        }
    }
    
    TlsCipher getCipher() {
        return this.cipher;
    }
    
    int getEpoch() {
        return this.epoch;
    }
    
    DTLSReplayWindow getReplayWindow() {
        return this.replayWindow;
    }
    
    long getSequenceNumber() {
        synchronized (this) {
            return this.sequenceNumber;
        }
    }
}
