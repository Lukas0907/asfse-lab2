// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

public class TlsFatalAlertReceived extends TlsException
{
    protected short alertDescription;
    
    public TlsFatalAlertReceived(final short alertDescription) {
        super(AlertDescription.getText(alertDescription), null);
        this.alertDescription = alertDescription;
    }
    
    public short getAlertDescription() {
        return this.alertDescription;
    }
}
