// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.IOException;

class DTLSRecordLayer implements DatagramTransport
{
    private static final int MAX_FRAGMENT_LENGTH = 16384;
    private static final int RECORD_HEADER_LENGTH = 13;
    private static final long RETRANSMIT_TIMEOUT = 240000L;
    private static final long TCP_MSL = 120000L;
    private volatile boolean closed;
    private final TlsContext context;
    private DTLSEpoch currentEpoch;
    private volatile boolean failed;
    private volatile boolean inHandshake;
    private final TlsPeer peer;
    private DTLSEpoch pendingEpoch;
    private volatile int plaintextLimit;
    private DTLSEpoch readEpoch;
    private volatile ProtocolVersion readVersion;
    private final ByteQueue recordQueue;
    private DTLSHandshakeRetransmit retransmit;
    private DTLSEpoch retransmitEpoch;
    private long retransmitExpiry;
    private final DatagramTransport transport;
    private DTLSEpoch writeEpoch;
    private volatile ProtocolVersion writeVersion;
    
    DTLSRecordLayer(final DatagramTransport transport, final TlsContext context, final TlsPeer peer, final short n) {
        this.recordQueue = new ByteQueue();
        this.closed = false;
        this.failed = false;
        this.readVersion = null;
        this.writeVersion = null;
        this.retransmit = null;
        this.retransmitEpoch = null;
        this.retransmitExpiry = 0L;
        this.transport = transport;
        this.context = context;
        this.peer = peer;
        this.inHandshake = true;
        this.currentEpoch = new DTLSEpoch(0, new TlsNullCipher(context));
        this.pendingEpoch = null;
        final DTLSEpoch currentEpoch = this.currentEpoch;
        this.readEpoch = currentEpoch;
        this.writeEpoch = currentEpoch;
        this.setPlaintextLimit(16384);
    }
    
    private void closeTransport() {
        if (this.closed) {
            return;
        }
        while (true) {
            try {
                if (!this.failed) {
                    this.warn((short)0, null);
                }
                this.transport.close();
                this.closed = true;
            }
            catch (Exception ex) {
                continue;
            }
            break;
        }
    }
    
    private static long getMacSequenceNumber(final int n, final long n2) {
        return ((long)n & 0xFFFFFFFFL) << 48 | n2;
    }
    
    private void raiseAlert(final short n, final short n2, final String s, final Throwable t) throws IOException {
        this.peer.notifyAlertRaised(n, n2, s, t);
        this.sendRecord((short)21, new byte[] { (byte)n, (byte)n2 }, 0, 2);
    }
    
    private int receiveRecord(final byte[] array, final int n, int n2, int receive) throws IOException {
        if (this.recordQueue.available() > 0) {
            if (this.recordQueue.available() >= 13) {
                final byte[] array2 = new byte[2];
                this.recordQueue.read(array2, 0, 2, 11);
                n2 = TlsUtils.readUint16(array2, 0);
            }
            else {
                n2 = 0;
            }
            n2 = Math.min(this.recordQueue.available(), n2 + 13);
            this.recordQueue.removeData(array, n, n2, 0);
            return n2;
        }
        receive = this.transport.receive(array, n, n2, receive);
        if ((n2 = receive) >= 13) {
            final int n3 = TlsUtils.readUint16(array, n + 11) + 13;
            if ((n2 = receive) > n3) {
                this.recordQueue.addData(array, n + n3, receive - n3);
                n2 = n3;
            }
        }
        return n2;
    }
    
    private void sendRecord(final short n, byte[] encodePlaintext, final int n2, final int n3) throws IOException {
        if (this.writeVersion == null) {
            return;
        }
        if (n3 > this.plaintextLimit) {
            throw new TlsFatalAlert((short)80);
        }
        if (n3 < 1 && n != 23) {
            throw new TlsFatalAlert((short)80);
        }
        final int epoch = this.writeEpoch.getEpoch();
        final long allocateSequenceNumber = this.writeEpoch.allocateSequenceNumber();
        encodePlaintext = this.writeEpoch.getCipher().encodePlaintext(getMacSequenceNumber(epoch, allocateSequenceNumber), n, encodePlaintext, n2, n3);
        final byte[] array = new byte[encodePlaintext.length + 13];
        TlsUtils.writeUint8(n, array, 0);
        TlsUtils.writeVersion(this.writeVersion, array, 1);
        TlsUtils.writeUint16(epoch, array, 3);
        TlsUtils.writeUint48(allocateSequenceNumber, array, 5);
        TlsUtils.writeUint16(encodePlaintext.length, array, 11);
        System.arraycopy(encodePlaintext, 0, array, 13, encodePlaintext.length);
        this.transport.send(array, 0, array.length);
    }
    
    @Override
    public void close() throws IOException {
        if (!this.closed) {
            if (this.inHandshake) {
                this.warn((short)90, "User canceled handshake");
            }
            this.closeTransport();
        }
    }
    
    void fail(final short n) {
        if (this.closed) {
            return;
        }
        while (true) {
            try {
                this.raiseAlert((short)2, n, null, null);
                this.failed = true;
                this.closeTransport();
            }
            catch (Exception ex) {
                continue;
            }
            break;
        }
    }
    
    void failed() {
        if (!this.closed) {
            this.failed = true;
            this.closeTransport();
        }
    }
    
    int getReadEpoch() {
        return this.readEpoch.getEpoch();
    }
    
    ProtocolVersion getReadVersion() {
        return this.readVersion;
    }
    
    @Override
    public int getReceiveLimit() throws IOException {
        return Math.min(this.plaintextLimit, this.readEpoch.getCipher().getPlaintextLimit(this.transport.getReceiveLimit() - 13));
    }
    
    @Override
    public int getSendLimit() throws IOException {
        return Math.min(this.plaintextLimit, this.writeEpoch.getCipher().getPlaintextLimit(this.transport.getSendLimit() - 13));
    }
    
    void handshakeSuccessful(final DTLSHandshakeRetransmit retransmit) {
        final DTLSEpoch readEpoch = this.readEpoch;
        final DTLSEpoch currentEpoch = this.currentEpoch;
        if (readEpoch != currentEpoch && this.writeEpoch != currentEpoch) {
            if (retransmit != null) {
                this.retransmit = retransmit;
                this.retransmitEpoch = currentEpoch;
                this.retransmitExpiry = System.currentTimeMillis() + 240000L;
            }
            this.inHandshake = false;
            this.currentEpoch = this.pendingEpoch;
            this.pendingEpoch = null;
            return;
        }
        throw new IllegalStateException();
    }
    
    void initPendingEpoch(final TlsCipher tlsCipher) {
        if (this.pendingEpoch == null) {
            this.pendingEpoch = new DTLSEpoch(this.writeEpoch.getEpoch() + 1, tlsCipher);
            return;
        }
        throw new IllegalStateException();
    }
    
    boolean isClosed() {
        return this.closed;
    }
    
    @Override
    public int receive(final byte[] array, int length, final int a, final int n) throws IOException {
        byte[] array2 = null;
    Block_2_Outer:
        while (true) {
            final int n2 = Math.min(a, this.getReceiveLimit()) + 13;
            while (true) {
                Label_0033: {
                    if (array2 == null) {
                        break Label_0033;
                    }
                    final byte[] array3 = array2;
                    if (array2.length < n2) {
                        break Label_0033;
                    }
                    Label_0578: {
                        int receiveRecord = 0;
                        byte[] decodeCiphertext;
                        short n3;
                        short n4;
                        short uint8 = 0;
                        DTLSEpoch dtlsEpoch = null;
                        ProtocolVersion version;
                        int uint9;
                        int n5 = 0;
                        long uint10;
                        Label_0088_Outer:Label_0397_Outer:Label_0463_Outer:
                        while (true) {
                        Label_0463:
                            while (true) {
                                Label_0629:Label_0353_Outer:
                                while (true) {
                                Label_0363_Outer:
                                    while (true) {
                                    Label_0503_Outer:
                                        while (true) {
                                            while (true) {
                                                Label_0585: {
                                                    while (true) {
                                                        Label_0564:Block_8_Outer:Block_12_Outer:
                                                        while (true) {
                                                            Label_0547: {
                                                                try {
                                                                    if (this.retransmit != null && System.currentTimeMillis() > this.retransmitExpiry) {
                                                                        this.retransmit = null;
                                                                        this.retransmitEpoch = null;
                                                                    }
                                                                    receiveRecord = this.receiveRecord(array3, 0, n2, n);
                                                                    if (receiveRecord < 0) {
                                                                        return receiveRecord;
                                                                    }
                                                                    break Label_0547;
                                                                    // iftrue(Label_0449:, n3 == 2)
                                                                    // iftrue(Label_0106:, receiveRecord == TlsUtils.readUint16(array3, 11) + 13)
                                                                    // switch([Lcom.strobel.decompiler.ast.Label;@36414118, uint8)
                                                                    // iftrue(Label_0259:, version.isDTLS())
                                                                    // iftrue(Label_0578:, decodeCiphertext.length != 2)
                                                                    // iftrue(Label_0567:, uint8 != 22 || this.retransmitEpoch == null || uint9 != this.retransmitEpoch.getEpoch())
                                                                    // iftrue(Label_0503:, !this.inHandshake)
                                                                    // iftrue(Label_0503:, this.inHandshake)
                                                                    // iftrue(Label_0281:, this.readVersion == null || this.readVersion.equals(version))
                                                                    // iftrue(Label_0578:, this.retransmit == null)
                                                                    // iftrue(Label_0578:, n5 >= decodeCiphertext.length)
                                                                    // iftrue(Label_0485:, TlsUtils.readUint8(decodeCiphertext, n5) == 1)
                                                                    // iftrue(Label_0629:, this.pendingEpoch == null)
                                                                    // iftrue(Label_0585:, this.readVersion != null)
                                                                    // iftrue(Label_0578:, n4 != 0)
                                                                    // iftrue(Label_0527:, this.inHandshake || this.retransmit == null)
                                                                    // iftrue(Label_0337:, decodeCiphertext.length <= this.plaintextLimit)
                                                                    // iftrue(Label_0181:, uint9 != this.readEpoch.getEpoch())
                                                                    while (true) {
                                                                        while (true) {
                                                                            while (true) {
                                                                            Block_23_Outer:
                                                                                while (true) {
                                                                                    Block_28: {
                                                                                        Block_26: {
                                                                                            while (true) {
                                                                                                Block_22: {
                                                                                                    while (true) {
                                                                                                        while (true) {
                                                                                                        Block_11_Outer:
                                                                                                            while (true) {
                                                                                                                while (true) {
                                                                                                                    while (true) {
                                                                                                                        n3 = decodeCiphertext[0];
                                                                                                                        n4 = decodeCiphertext[1];
                                                                                                                        this.peer.notifyAlertReceived(n3, n4);
                                                                                                                        break Block_22;
                                                                                                                        break;
                                                                                                                        Label_0106: {
                                                                                                                            uint8 = TlsUtils.readUint8(array3, 0);
                                                                                                                        }
                                                                                                                        break Label_0578;
                                                                                                                        dtlsEpoch = this.retransmitEpoch;
                                                                                                                        break Label_0564;
                                                                                                                        dtlsEpoch = this.readEpoch;
                                                                                                                        break Label_0564;
                                                                                                                        break;
                                                                                                                        Label_0240:
                                                                                                                        version = TlsUtils.readVersion(array3, 1);
                                                                                                                        break;
                                                                                                                        continue Label_0088_Outer;
                                                                                                                    }
                                                                                                                    break Label_0629;
                                                                                                                    Label_0181: {
                                                                                                                        continue Block_8_Outer;
                                                                                                                    }
                                                                                                                }
                                                                                                                break;
                                                                                                                continue Block_11_Outer;
                                                                                                            }
                                                                                                            Block_19: {
                                                                                                                break Block_19;
                                                                                                                break Label_0578;
                                                                                                                this.readVersion = version;
                                                                                                                break Label_0585;
                                                                                                                Label_0259: {
                                                                                                                    break;
                                                                                                                }
                                                                                                            }
                                                                                                            this.retransmit.receivedHandshakeRecord(uint9, decodeCiphertext, 0, decodeCiphertext.length);
                                                                                                            break Label_0578;
                                                                                                            Block_24: {
                                                                                                                break Block_24;
                                                                                                                System.arraycopy(decodeCiphertext, 0, array, length, decodeCiphertext.length);
                                                                                                                length = decodeCiphertext.length;
                                                                                                                return length;
                                                                                                                Label_0449: {
                                                                                                                    this.failed();
                                                                                                                }
                                                                                                                throw new TlsFatalAlert(n4);
                                                                                                                this.closeTransport();
                                                                                                                break Label_0578;
                                                                                                            }
                                                                                                            continue Label_0353_Outer;
                                                                                                        }
                                                                                                        Label_0485: {
                                                                                                            break Block_26;
                                                                                                        }
                                                                                                        break;
                                                                                                        Label_0337:
                                                                                                        continue Label_0463_Outer;
                                                                                                    }
                                                                                                    break;
                                                                                                }
                                                                                                continue Label_0503_Outer;
                                                                                            }
                                                                                            break Block_28;
                                                                                        }
                                                                                        this.readEpoch = this.pendingEpoch;
                                                                                        break Label_0629;
                                                                                    }
                                                                                    this.retransmit = null;
                                                                                    this.retransmitEpoch = null;
                                                                                    continue Block_23_Outer;
                                                                                }
                                                                                Label_0281: {
                                                                                    decodeCiphertext = dtlsEpoch.getCipher().decodeCiphertext(getMacSequenceNumber(dtlsEpoch.getEpoch(), uint10), uint8, array3, 13, receiveRecord - 13);
                                                                                }
                                                                                dtlsEpoch.getReplayWindow().reportAuthenticated(uint10);
                                                                                continue Label_0463_Outer;
                                                                            }
                                                                            Label_0152: {
                                                                                uint9 = TlsUtils.readUint16(array3, 3);
                                                                            }
                                                                            continue Block_12_Outer;
                                                                        }
                                                                        uint10 = TlsUtils.readUint48(array3, 5);
                                                                        continue Label_0397_Outer;
                                                                    }
                                                                }
                                                                // iftrue(Label_0240:, !dtlsEpoch.getReplayWindow().shouldDiscard(uint10))
                                                                catch (IOException ex) {
                                                                    throw ex;
                                                                }
                                                            }
                                                            if (receiveRecord < 13) {
                                                                break;
                                                            }
                                                            continue Label_0397_Outer;
                                                        }
                                                        Label_0570: {
                                                            break Label_0570;
                                                            Label_0567: {
                                                                dtlsEpoch = null;
                                                            }
                                                        }
                                                        if (dtlsEpoch == null) {
                                                            break;
                                                        }
                                                        continue;
                                                    }
                                                }
                                                switch (uint8) {
                                                    case 23: {
                                                        continue Label_0363_Outer;
                                                    }
                                                    case 22: {
                                                        continue Label_0463_Outer;
                                                    }
                                                    case 21: {
                                                        continue Label_0353_Outer;
                                                    }
                                                    case 24: {
                                                        break Label_0578;
                                                    }
                                                    default: {
                                                        continue;
                                                    }
                                                    case 20: {
                                                        n5 = 0;
                                                        continue Label_0463;
                                                    }
                                                }
                                                break;
                                            }
                                            break;
                                        }
                                        break;
                                    }
                                    break;
                                }
                                ++n5;
                                continue Label_0463;
                            }
                            Label_0561: {
                                break;
                            }
                        }
                        array2 = array3;
                        continue Block_2_Outer;
                    }
                    array2 = array3;
                    continue Block_2_Outer;
                }
                final byte[] array3 = new byte[n2];
                continue;
            }
        }
    }
    
    void resetWriteEpoch() {
        DTLSEpoch writeEpoch = this.retransmitEpoch;
        if (writeEpoch == null) {
            writeEpoch = this.currentEpoch;
        }
        this.writeEpoch = writeEpoch;
    }
    
    @Override
    public void send(final byte[] array, final int n, final int n2) throws IOException {
        short n3;
        if (!this.inHandshake && this.writeEpoch != this.retransmitEpoch) {
            n3 = 23;
        }
        else {
            n3 = 22;
            if (TlsUtils.readUint8(array, n) == 20) {
                DTLSEpoch writeEpoch = null;
                if (this.inHandshake) {
                    writeEpoch = this.pendingEpoch;
                }
                else if (this.writeEpoch == this.retransmitEpoch) {
                    writeEpoch = this.currentEpoch;
                }
                if (writeEpoch == null) {
                    throw new IllegalStateException();
                }
                final byte[] array2 = { 1 };
                this.sendRecord((short)20, array2, 0, array2.length);
                this.writeEpoch = writeEpoch;
                n3 = n3;
            }
        }
        this.sendRecord(n3, array, n, n2);
    }
    
    void setPlaintextLimit(final int plaintextLimit) {
        this.plaintextLimit = plaintextLimit;
    }
    
    void setReadVersion(final ProtocolVersion readVersion) {
        this.readVersion = readVersion;
    }
    
    void setWriteVersion(final ProtocolVersion writeVersion) {
        this.writeVersion = writeVersion;
    }
    
    void warn(final short n, final String s) throws IOException {
        this.raiseAlert((short)1, n, s, null);
    }
}
