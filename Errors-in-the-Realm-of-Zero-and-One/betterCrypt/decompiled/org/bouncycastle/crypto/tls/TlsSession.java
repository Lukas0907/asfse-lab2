// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

public interface TlsSession
{
    SessionParameters exportSessionParameters();
    
    byte[] getSessionID();
    
    void invalidate();
    
    boolean isResumable();
}
