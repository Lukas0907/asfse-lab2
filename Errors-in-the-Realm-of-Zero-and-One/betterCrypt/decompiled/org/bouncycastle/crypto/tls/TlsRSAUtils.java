// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import org.bouncycastle.util.Arrays;
import java.io.IOException;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.encodings.PKCS1Encoding;
import org.bouncycastle.crypto.engines.RSABlindedEngine;
import java.io.OutputStream;
import org.bouncycastle.crypto.params.RSAKeyParameters;

public class TlsRSAUtils
{
    public static byte[] generateEncryptedPreMasterSecret(final TlsContext tlsContext, final RSAKeyParameters rsaKeyParameters, final OutputStream outputStream) throws IOException {
        final byte[] bytes = new byte[48];
        tlsContext.getSecureRandom().nextBytes(bytes);
        TlsUtils.writeVersion(tlsContext.getClientVersion(), bytes, 0);
        final PKCS1Encoding pkcs1Encoding = new PKCS1Encoding(new RSABlindedEngine());
        pkcs1Encoding.init(true, new ParametersWithRandom(rsaKeyParameters, tlsContext.getSecureRandom()));
        try {
            final byte[] processBlock = pkcs1Encoding.processBlock(bytes, 0, bytes.length);
            if (TlsUtils.isSSL(tlsContext)) {
                outputStream.write(processBlock);
                return bytes;
            }
            TlsUtils.writeOpaque16(processBlock, outputStream);
            return bytes;
        }
        catch (InvalidCipherTextException ex) {
            throw new TlsFatalAlert((short)80, ex);
        }
    }
    
    public static byte[] safeDecryptPreMasterSecret(final TlsContext tlsContext, final RSAKeyParameters rsaKeyParameters, final byte[] array) {
        final ProtocolVersion clientVersion = tlsContext.getClientVersion();
        final byte[] bytes = new byte[48];
        tlsContext.getSecureRandom().nextBytes(bytes);
        final byte[] clone = Arrays.clone(bytes);
        int i = 0;
        while (true) {
            try {
                final PKCS1Encoding pkcs1Encoding = new PKCS1Encoding(new RSABlindedEngine(), bytes);
                pkcs1Encoding.init(false, new ParametersWithRandom(rsaKeyParameters, tlsContext.getSecureRandom()));
                final byte[] processBlock = pkcs1Encoding.processBlock(array, 0, array.length);
                final int n = (clientVersion.getMajorVersion() ^ (processBlock[0] & 0xFF)) | (clientVersion.getMinorVersion() ^ (processBlock[1] & 0xFF));
                final int n2 = n | n >> 1;
                final int n3 = n2 | n2 >> 2;
                final int n4 = ((n3 | n3 >> 4) & 0x1) - 1;
                while (i < 48) {
                    processBlock[i] = (byte)((processBlock[i] & n4) | (bytes[i] & n4));
                    ++i;
                }
                return processBlock;
            }
            catch (Exception ex) {
                final byte[] processBlock = clone;
                continue;
            }
            break;
        }
    }
}
