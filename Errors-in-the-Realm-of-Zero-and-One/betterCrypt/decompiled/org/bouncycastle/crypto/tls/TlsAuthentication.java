// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.IOException;

public interface TlsAuthentication
{
    TlsCredentials getClientCredentials(final CertificateRequest p0) throws IOException;
    
    void notifyServerCertificate(final Certificate p0) throws IOException;
}
