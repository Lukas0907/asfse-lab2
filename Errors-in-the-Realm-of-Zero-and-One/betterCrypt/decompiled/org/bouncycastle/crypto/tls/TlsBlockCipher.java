// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.security.SecureRandom;
import java.io.IOException;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.BlockCipher;

public class TlsBlockCipher implements TlsCipher
{
    protected TlsContext context;
    protected BlockCipher decryptCipher;
    protected BlockCipher encryptCipher;
    protected boolean encryptThenMAC;
    protected byte[] randomData;
    protected TlsMac readMac;
    protected boolean useExplicitIV;
    protected TlsMac writeMac;
    
    public TlsBlockCipher(final TlsContext context, final BlockCipher blockCipher, final BlockCipher blockCipher2, final Digest digest, final Digest digest2, int n) throws IOException {
        this.context = context;
        this.randomData = new byte[256];
        context.getNonceRandomGenerator().nextBytes(this.randomData);
        this.useExplicitIV = TlsUtils.isTLSv11(context);
        this.encryptThenMAC = context.getSecurityParameters().encryptThenMAC;
        int n3;
        final int n2 = n3 = n * 2 + digest.getDigestSize() + digest2.getDigestSize();
        if (!this.useExplicitIV) {
            n3 = n2 + (blockCipher.getBlockSize() + blockCipher2.getBlockSize());
        }
        final byte[] calculateKeyBlock = TlsUtils.calculateKeyBlock(context, n3);
        final TlsMac tlsMac = new TlsMac(context, digest, calculateKeyBlock, 0, digest.getDigestSize());
        final int n4 = digest.getDigestSize() + 0;
        final TlsMac tlsMac2 = new TlsMac(context, digest2, calculateKeyBlock, n4, digest2.getDigestSize());
        final int n5 = n4 + digest2.getDigestSize();
        final KeyParameter keyParameter = new KeyParameter(calculateKeyBlock, n5, n);
        final int n6 = n5 + n;
        final KeyParameter keyParameter2 = new KeyParameter(calculateKeyBlock, n6, n);
        n += n6;
        byte[] copyOfRange;
        byte[] copyOfRange2;
        if (this.useExplicitIV) {
            copyOfRange = new byte[blockCipher.getBlockSize()];
            copyOfRange2 = new byte[blockCipher2.getBlockSize()];
        }
        else {
            copyOfRange = Arrays.copyOfRange(calculateKeyBlock, n, blockCipher.getBlockSize() + n);
            n += blockCipher.getBlockSize();
            copyOfRange2 = Arrays.copyOfRange(calculateKeyBlock, n, blockCipher2.getBlockSize() + n);
            n += blockCipher2.getBlockSize();
        }
        if (n == n3) {
            ParametersWithIV parametersWithIV;
            ParametersWithIV parametersWithIV2;
            if (context.isServer()) {
                this.writeMac = tlsMac2;
                this.readMac = tlsMac;
                this.encryptCipher = blockCipher2;
                this.decryptCipher = blockCipher;
                parametersWithIV = new ParametersWithIV(keyParameter2, copyOfRange2);
                parametersWithIV2 = new ParametersWithIV(keyParameter, copyOfRange);
            }
            else {
                this.writeMac = tlsMac;
                this.readMac = tlsMac2;
                this.encryptCipher = blockCipher;
                this.decryptCipher = blockCipher2;
                parametersWithIV = new ParametersWithIV(keyParameter, copyOfRange);
                parametersWithIV2 = new ParametersWithIV(keyParameter2, copyOfRange2);
            }
            this.encryptCipher.init(true, parametersWithIV);
            this.decryptCipher.init(false, parametersWithIV2);
            return;
        }
        throw new TlsFatalAlert((short)80);
    }
    
    protected int checkPaddingConstantTime(byte[] randomData, int n, int i, int n2, int n3) {
        final int n4 = n + i;
        final byte b = randomData[n4 - 1];
        n = (b & 0xFF) + 1;
        if ((TlsUtils.isSSL(this.context) && n > n2) || n3 + n > i) {
            i = 0;
            n2 = (n = i);
        }
        else {
            i = n4 - n;
            n3 = 0;
            while (true) {
                n2 = i + 1;
                n3 = (byte)(n3 | (randomData[i] ^ b));
                if (n2 >= n4) {
                    break;
                }
                i = n2;
            }
            final int n5 = i = n;
            if ((n2 = n3) != 0) {
                n = 0;
                n2 = n3;
                i = n5;
            }
        }
        randomData = this.randomData;
        while (i < 256) {
            n2 = (byte)((randomData[i] ^ b) | n2);
            ++i;
        }
        randomData[0] ^= (byte)n2;
        return n;
    }
    
    protected int chooseExtraPadBlocks(final SecureRandom secureRandom, final int b) {
        return Math.min(this.lowestBitSet(secureRandom.nextInt()), b);
    }
    
    @Override
    public byte[] decodeCiphertext(final long n, final short n2, final byte[] array, int n3, int i) throws IOException {
        final int n4 = n3;
        final int blockSize = this.decryptCipher.getBlockSize();
        final int size = this.readMac.getSize();
        int max;
        if (this.encryptThenMAC) {
            max = blockSize + size;
        }
        else {
            max = Math.max(blockSize, size + 1);
        }
        int n5 = max;
        if (this.useExplicitIV) {
            n5 = max + blockSize;
        }
        if (i < n5) {
            throw new TlsFatalAlert((short)50);
        }
        int n6;
        if (this.encryptThenMAC) {
            n6 = i - size;
        }
        else {
            n6 = i;
        }
        if (n6 % blockSize != 0) {
            throw new TlsFatalAlert((short)21);
        }
        if (this.encryptThenMAC) {
            final int n7 = n4 + i;
            if (Arrays.constantTimeAreEqual(this.readMac.calculateMac(n, n2, array, n3, i - size), Arrays.copyOfRange(array, n7 - size, n7)) ^ true) {
                throw new TlsFatalAlert((short)20);
            }
        }
        n3 = n4;
        int n8 = n6;
        if (this.useExplicitIV) {
            this.decryptCipher.init(false, new ParametersWithIV(null, array, n4, blockSize));
            n3 = n4 + blockSize;
            n8 = n6 - blockSize;
        }
        BlockCipher decryptCipher;
        int n9;
        for (i = 0; i < n8; i += blockSize) {
            decryptCipher = this.decryptCipher;
            n9 = n3 + i;
            decryptCipher.processBlock(array, n9, array, n9);
        }
        if (this.encryptThenMAC) {
            i = 0;
        }
        else {
            i = size;
        }
        final int checkPaddingConstantTime = this.checkPaddingConstantTime(array, n3, n8, blockSize, i);
        if (checkPaddingConstantTime == 0) {
            i = 1;
        }
        else {
            i = 0;
        }
        final int n10 = n8 - checkPaddingConstantTime;
        int n14;
        if (!this.encryptThenMAC) {
            final int n11 = n10 - size;
            final int n12 = n3 + n11;
            final int n13 = i | ((Arrays.constantTimeAreEqual(this.readMac.calculateMacConstantTime(n, n2, array, n3, n11, n8 - size, this.randomData), Arrays.copyOfRange(array, n12, n12 + size)) ^ true) ? 1 : 0);
            i = n11;
            n14 = n13;
        }
        else {
            n14 = i;
            i = n10;
        }
        if (n14 == 0) {
            return Arrays.copyOfRange(array, n3, n3 + i);
        }
        throw new TlsFatalAlert((short)20);
    }
    
    @Override
    public byte[] encodePlaintext(final long n, final short n2, byte[] array, int length, int n3) {
        final int blockSize = this.encryptCipher.getBlockSize();
        final int size = this.writeMac.getSize();
        final ProtocolVersion serverVersion = this.context.getServerVersion();
        int n4;
        if (!this.encryptThenMAC) {
            n4 = n3 + size;
        }
        else {
            n4 = n3;
        }
        final int n5 = blockSize - 1 - n4 % blockSize;
        int n6 = 0;
        Label_0148: {
            if (!this.encryptThenMAC) {
                n6 = n5;
                if (this.context.getSecurityParameters().truncatedHMac) {
                    break Label_0148;
                }
            }
            n6 = n5;
            if (!serverVersion.isDTLS()) {
                n6 = n5;
                if (!serverVersion.isSSL()) {
                    n6 = n5 + this.chooseExtraPadBlocks(this.context.getSecureRandom(), (255 - n5) / blockSize) * blockSize;
                }
            }
        }
        int n7 = size + n3 + n6 + 1;
        if (this.useExplicitIV) {
            n7 += blockSize;
        }
        final byte[] array2 = new byte[n7];
        int n8;
        if (this.useExplicitIV) {
            final byte[] array3 = new byte[blockSize];
            this.context.getNonceRandomGenerator().nextBytes(array3);
            this.encryptCipher.init(true, new ParametersWithIV(null, array3));
            System.arraycopy(array3, 0, array2, 0, blockSize);
            n8 = blockSize + 0;
        }
        else {
            n8 = 0;
        }
        System.arraycopy(array, length, array2, n8, n3);
        int n10;
        final int n9 = n10 = n8 + n3;
        if (!this.encryptThenMAC) {
            array = this.writeMac.calculateMac(n, n2, array, length, n3);
            System.arraycopy(array, 0, array2, n9, array.length);
            n10 = n9 + array.length;
        }
        length = n10;
        n3 = 0;
        int i;
        while (true) {
            i = n8;
            if (n3 > n6) {
                break;
            }
            array2[length] = (byte)n6;
            ++n3;
            ++length;
        }
        while (i < length) {
            this.encryptCipher.processBlock(array2, i, array2, i);
            i += blockSize;
        }
        if (this.encryptThenMAC) {
            array = this.writeMac.calculateMac(n, n2, array2, 0, length);
            System.arraycopy(array, 0, array2, length, array.length);
            length = array.length;
            return array2;
        }
        return array2;
    }
    
    @Override
    public int getPlaintextLimit(int n) {
        final int blockSize = this.encryptCipher.getBlockSize();
        final int size = this.writeMac.getSize();
        int n2 = n;
        if (this.useExplicitIV) {
            n2 = n - blockSize;
        }
        if (this.encryptThenMAC) {
            n = n2 - size;
            n -= n % blockSize;
        }
        else {
            n = n2 - n2 % blockSize - size;
        }
        return n - 1;
    }
    
    public TlsMac getReadMac() {
        return this.readMac;
    }
    
    public TlsMac getWriteMac() {
        return this.writeMac;
    }
    
    protected int lowestBitSet(int n) {
        if (n == 0) {
            return 32;
        }
        int n2 = 0;
        while ((n & 0x1) == 0x0) {
            ++n2;
            n >>= 1;
        }
        return n2;
    }
}
