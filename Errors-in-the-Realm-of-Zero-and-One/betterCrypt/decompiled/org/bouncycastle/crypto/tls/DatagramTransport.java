// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.IOException;

public interface DatagramTransport extends TlsCloseable
{
    int getReceiveLimit() throws IOException;
    
    int getSendLimit() throws IOException;
    
    int receive(final byte[] p0, final int p1, final int p2, final int p3) throws IOException;
    
    void send(final byte[] p0, final int p1, final int p2) throws IOException;
}
