// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.InputStream;
import java.io.IOException;
import java.util.Vector;

public abstract class AbstractTlsKeyExchange implements TlsKeyExchange
{
    protected TlsContext context;
    protected int keyExchange;
    protected Vector supportedSignatureAlgorithms;
    
    protected AbstractTlsKeyExchange(final int keyExchange, final Vector supportedSignatureAlgorithms) {
        this.keyExchange = keyExchange;
        this.supportedSignatureAlgorithms = supportedSignatureAlgorithms;
    }
    
    @Override
    public byte[] generateServerKeyExchange() throws IOException {
        if (!this.requiresServerKeyExchange()) {
            return null;
        }
        throw new TlsFatalAlert((short)80);
    }
    
    @Override
    public void init(final TlsContext context) {
        this.context = context;
        final ProtocolVersion clientVersion = context.getClientVersion();
        if (TlsUtils.isSignatureAlgorithmsExtensionAllowed(clientVersion)) {
            if (this.supportedSignatureAlgorithms == null) {
                final int keyExchange = this.keyExchange;
                Vector supportedSignatureAlgorithms = null;
                Label_0160: {
                    Label_0156: {
                        if (keyExchange != 1) {
                            Label_0149: {
                                if (keyExchange != 3) {
                                    if (keyExchange == 5) {
                                        break Label_0156;
                                    }
                                    if (keyExchange != 7) {
                                        if (keyExchange == 9) {
                                            break Label_0156;
                                        }
                                        switch (keyExchange) {
                                            default: {
                                                switch (keyExchange) {
                                                    default: {
                                                        throw new IllegalStateException("unsupported key exchange algorithm");
                                                    }
                                                    case 22: {
                                                        break Label_0149;
                                                    }
                                                    case 23: {
                                                        break Label_0156;
                                                    }
                                                    case 21:
                                                    case 24: {
                                                        return;
                                                    }
                                                }
                                                break;
                                            }
                                            case 16:
                                            case 17: {
                                                supportedSignatureAlgorithms = TlsUtils.getDefaultECDSASignatureAlgorithms();
                                                break Label_0160;
                                            }
                                            case 15:
                                            case 18:
                                            case 19: {
                                                break Label_0156;
                                            }
                                            case 13:
                                            case 14: {
                                                return;
                                            }
                                        }
                                    }
                                }
                            }
                            supportedSignatureAlgorithms = TlsUtils.getDefaultDSSSignatureAlgorithms();
                            break Label_0160;
                        }
                    }
                    supportedSignatureAlgorithms = TlsUtils.getDefaultRSASignatureAlgorithms();
                }
                this.supportedSignatureAlgorithms = supportedSignatureAlgorithms;
            }
        }
        else if (this.supportedSignatureAlgorithms != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("supported_signature_algorithms not allowed for ");
            sb.append(clientVersion);
            throw new IllegalStateException(sb.toString());
        }
    }
    
    protected DigitallySigned parseSignature(final InputStream inputStream) throws IOException {
        final DigitallySigned parse = DigitallySigned.parse(this.context, inputStream);
        final SignatureAndHashAlgorithm algorithm = parse.getAlgorithm();
        if (algorithm != null) {
            TlsUtils.verifySupportedSignatureAlgorithm(this.supportedSignatureAlgorithms, algorithm);
        }
        return parse;
    }
    
    @Override
    public void processClientCertificate(final Certificate certificate) throws IOException {
    }
    
    @Override
    public void processClientKeyExchange(final InputStream inputStream) throws IOException {
        throw new TlsFatalAlert((short)80);
    }
    
    @Override
    public void processServerCertificate(final Certificate certificate) throws IOException {
        final Vector supportedSignatureAlgorithms = this.supportedSignatureAlgorithms;
    }
    
    @Override
    public void processServerCredentials(final TlsCredentials tlsCredentials) throws IOException {
        this.processServerCertificate(tlsCredentials.getCertificate());
    }
    
    @Override
    public void processServerKeyExchange(final InputStream inputStream) throws IOException {
        if (this.requiresServerKeyExchange()) {
            return;
        }
        throw new TlsFatalAlert((short)10);
    }
    
    @Override
    public boolean requiresServerKeyExchange() {
        return false;
    }
    
    @Override
    public void skipClientCredentials() throws IOException {
    }
    
    @Override
    public void skipServerKeyExchange() throws IOException {
        if (!this.requiresServerKeyExchange()) {
            return;
        }
        throw new TlsFatalAlert((short)10);
    }
}
