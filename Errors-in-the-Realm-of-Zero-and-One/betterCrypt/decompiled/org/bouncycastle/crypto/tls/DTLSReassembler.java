// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.util.Vector;

class DTLSReassembler
{
    private byte[] body;
    private Vector missing;
    private short msg_type;
    
    DTLSReassembler(final short msg_type, final int n) {
        this.missing = new Vector();
        this.msg_type = msg_type;
        this.body = new byte[n];
        this.missing.addElement(new Range(0, n));
    }
    
    void contributeFragment(final short n, int index, final byte[] array, final int n2, final int b, int max) {
        final int b2 = b + max;
        if (this.msg_type == n && this.body.length == index) {
            if (b2 > index) {
                return;
            }
            int i = 0;
            if (max == 0) {
                if (b == 0 && !this.missing.isEmpty() && this.missing.firstElement().getEnd() == 0) {
                    this.missing.removeElementAt(0);
                }
                return;
            }
            while (i < this.missing.size()) {
                final Range range = this.missing.elementAt(i);
                if (range.getStart() >= b2) {
                    return;
                }
                index = i;
                if (range.getEnd() > b) {
                    max = Math.max(range.getStart(), b);
                    final int min = Math.min(range.getEnd(), b2);
                    System.arraycopy(array, n2 + max - b, this.body, max, min - max);
                    if (max == range.getStart()) {
                        if (min == range.getEnd()) {
                            this.missing.removeElementAt(i);
                            index = i - 1;
                        }
                        else {
                            range.setStart(min);
                            index = i;
                        }
                    }
                    else {
                        index = i;
                        if (min != range.getEnd()) {
                            final Vector missing = this.missing;
                            final Range obj = new Range(min, range.getEnd());
                            index = i + 1;
                            missing.insertElementAt(obj, index);
                        }
                        range.setEnd(max);
                    }
                }
                i = index + 1;
            }
        }
    }
    
    byte[] getBodyIfComplete() {
        if (this.missing.isEmpty()) {
            return this.body;
        }
        return null;
    }
    
    short getMsgType() {
        return this.msg_type;
    }
    
    void reset() {
        this.missing.removeAllElements();
        this.missing.addElement(new Range(0, this.body.length));
    }
    
    private static class Range
    {
        private int end;
        private int start;
        
        Range(final int start, final int end) {
            this.start = start;
            this.end = end;
        }
        
        public int getEnd() {
            return this.end;
        }
        
        public int getStart() {
            return this.start;
        }
        
        public void setEnd(final int end) {
            this.end = end;
        }
        
        public void setStart(final int start) {
            this.start = start;
        }
    }
}
