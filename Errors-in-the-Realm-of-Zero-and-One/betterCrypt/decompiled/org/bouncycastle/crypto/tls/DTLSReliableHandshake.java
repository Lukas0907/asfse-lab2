// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import org.bouncycastle.util.Integers;
import java.util.Enumeration;
import java.io.IOException;
import java.util.Vector;
import java.util.Hashtable;

class DTLSReliableHandshake
{
    private static final int MAX_RECEIVE_AHEAD = 16;
    private static final int MESSAGE_HEADER_LENGTH = 12;
    private Hashtable currentInboundFlight;
    private TlsHandshakeHash handshakeHash;
    private int message_seq;
    private int next_receive_seq;
    private Vector outboundFlight;
    private Hashtable previousInboundFlight;
    private DTLSRecordLayer recordLayer;
    private boolean sending;
    
    DTLSReliableHandshake(final TlsContext tlsContext, final DTLSRecordLayer recordLayer) {
        this.currentInboundFlight = new Hashtable();
        this.previousInboundFlight = null;
        this.outboundFlight = new Vector();
        this.sending = true;
        this.message_seq = 0;
        this.next_receive_seq = 0;
        this.recordLayer = recordLayer;
        (this.handshakeHash = new DeferredHash()).init(tlsContext);
    }
    
    private int backOff(final int n) {
        return Math.min(n * 2, 60000);
    }
    
    private static boolean checkAll(final Hashtable hashtable) {
        final Enumeration<DTLSReassembler> elements = hashtable.elements();
        while (elements.hasMoreElements()) {
            if (elements.nextElement().getBodyIfComplete() == null) {
                return false;
            }
        }
        return true;
    }
    
    private void checkInboundFlight() {
        final Enumeration<Integer> keys = this.currentInboundFlight.keys();
        while (keys.hasMoreElements()) {
            keys.nextElement();
            final int next_receive_seq = this.next_receive_seq;
        }
    }
    
    private Message getPendingMessage() throws IOException {
        final DTLSReassembler dtlsReassembler = this.currentInboundFlight.get(Integers.valueOf(this.next_receive_seq));
        if (dtlsReassembler != null) {
            final byte[] bodyIfComplete = dtlsReassembler.getBodyIfComplete();
            if (bodyIfComplete != null) {
                this.previousInboundFlight = null;
                return this.updateHandshakeMessagesDigest(new Message(this.next_receive_seq++, dtlsReassembler.getMsgType(), bodyIfComplete));
            }
        }
        return null;
    }
    
    private void prepareInboundFlight(final Hashtable currentInboundFlight) {
        resetAll(this.currentInboundFlight);
        this.previousInboundFlight = this.currentInboundFlight;
        this.currentInboundFlight = currentInboundFlight;
    }
    
    private boolean processRecord(final int n, final int n2, final byte[] array, int n3, int n4) throws IOException {
        final int n5 = n4;
        int n6 = 0;
        n4 = n3;
        n3 = n5;
        boolean b;
        while (true) {
            b = true;
            if (n3 < 12) {
                break;
            }
            final int uint24 = TlsUtils.readUint24(array, n4 + 9);
            final int n7 = uint24 + 12;
            if (n3 < n7) {
                break;
            }
            final int uint25 = TlsUtils.readUint24(array, n4 + 1);
            final int uint26 = TlsUtils.readUint24(array, n4 + 6);
            if (uint26 + uint24 > uint25) {
                break;
            }
            final short uint27 = TlsUtils.readUint8(array, n4 + 0);
            if (n2 != ((uint27 == 20) ? 1 : 0)) {
                break;
            }
            final int uint28 = TlsUtils.readUint16(array, n4 + 4);
            final int next_receive_seq = this.next_receive_seq;
            int n8;
            if (uint28 >= next_receive_seq + n) {
                n8 = n6;
            }
            else if (uint28 >= next_receive_seq) {
                DTLSReassembler value;
                if ((value = this.currentInboundFlight.get(Integers.valueOf(uint28))) == null) {
                    value = new DTLSReassembler(uint27, uint25);
                    this.currentInboundFlight.put(Integers.valueOf(uint28), value);
                }
                value.contributeFragment(uint27, uint25, array, n4 + 12, uint26, uint24);
                n8 = n6;
            }
            else {
                final Hashtable previousInboundFlight = this.previousInboundFlight;
                n8 = n6;
                if (previousInboundFlight != null) {
                    final DTLSReassembler dtlsReassembler = previousInboundFlight.get(Integers.valueOf(uint28));
                    n8 = n6;
                    if (dtlsReassembler != null) {
                        dtlsReassembler.contributeFragment(uint27, uint25, array, n4 + 12, uint26, uint24);
                        n8 = 1;
                    }
                }
            }
            n4 += n7;
            n3 -= n7;
            n6 = n8;
        }
        if (n6 == 0 || !checkAll(this.previousInboundFlight)) {
            b = false;
        }
        if (b) {
            this.resendOutboundFlight();
            resetAll(this.previousInboundFlight);
        }
        return b;
    }
    
    private void resendOutboundFlight() throws IOException {
        this.recordLayer.resetWriteEpoch();
        for (int i = 0; i < this.outboundFlight.size(); ++i) {
            this.writeMessage((Message)this.outboundFlight.elementAt(i));
        }
    }
    
    private static void resetAll(final Hashtable hashtable) {
        final Enumeration<DTLSReassembler> elements = hashtable.elements();
        while (elements.hasMoreElements()) {
            elements.nextElement().reset();
        }
    }
    
    private Message updateHandshakeMessagesDigest(final Message message) throws IOException {
        if (message.getType() != 0) {
            final byte[] body = message.getBody();
            final byte[] array = new byte[12];
            TlsUtils.writeUint8(message.getType(), array, 0);
            TlsUtils.writeUint24(body.length, array, 1);
            TlsUtils.writeUint16(message.getSeq(), array, 4);
            TlsUtils.writeUint24(0, array, 6);
            TlsUtils.writeUint24(body.length, array, 9);
            this.handshakeHash.update(array, 0, array.length);
            this.handshakeHash.update(body, 0, body.length);
        }
        return message;
    }
    
    private void writeHandshakeFragment(final Message message, final int off, final int len) throws IOException {
        final RecordLayerBuffer recordLayerBuffer = new RecordLayerBuffer(len + 12);
        TlsUtils.writeUint8(message.getType(), recordLayerBuffer);
        TlsUtils.writeUint24(message.getBody().length, recordLayerBuffer);
        TlsUtils.writeUint16(message.getSeq(), recordLayerBuffer);
        TlsUtils.writeUint24(off, recordLayerBuffer);
        TlsUtils.writeUint24(len, recordLayerBuffer);
        recordLayerBuffer.write(message.getBody(), off, len);
        recordLayerBuffer.sendToRecordLayer(this.recordLayer);
    }
    
    private void writeMessage(final Message message) throws IOException {
        final int b = this.recordLayer.getSendLimit() - 12;
        if (b >= 1) {
            final int length = message.getBody().length;
            int n = 0;
            int min;
            do {
                min = Math.min(length - n, b);
                this.writeHandshakeFragment(message, n, min);
            } while ((n += min) < length);
            return;
        }
        throw new TlsFatalAlert((short)80);
    }
    
    void finish() {
        final boolean sending = this.sending;
        DTLSHandshakeRetransmit dtlsHandshakeRetransmit = null;
        if (!sending) {
            this.checkInboundFlight();
        }
        else {
            this.prepareInboundFlight(null);
            if (this.previousInboundFlight != null) {
                dtlsHandshakeRetransmit = new DTLSHandshakeRetransmit() {
                    @Override
                    public void receivedHandshakeRecord(final int n, final byte[] array, final int n2, final int n3) throws IOException {
                        DTLSReliableHandshake.this.processRecord(0, n, array, n2, n3);
                    }
                };
            }
        }
        this.recordLayer.handshakeSuccessful(dtlsHandshakeRetransmit);
    }
    
    TlsHandshakeHash getHandshakeHash() {
        return this.handshakeHash;
    }
    
    void notifyHelloComplete() {
        this.handshakeHash = this.handshakeHash.notifyPRFDetermined();
    }
    
    TlsHandshakeHash prepareToFinish() {
        final TlsHandshakeHash handshakeHash = this.handshakeHash;
        this.handshakeHash = handshakeHash.stopTracking();
        return handshakeHash;
    }
    
    Message receiveMessage() throws IOException {
        if (this.sending) {
            this.sending = false;
            this.prepareInboundFlight(new Hashtable());
        }
        byte[] array = null;
        int n = 1000;
    Label_0194_Outer:
        while (true) {
            final int n2 = n;
            byte[] array2 = array;
            while (true) {
                try {
                    if (this.recordLayer.isClosed()) {
                        array2 = array;
                        throw new TlsFatalAlert((short)90);
                    }
                    array2 = array;
                    final Message pendingMessage = this.getPendingMessage();
                    if (pendingMessage != null) {
                        return pendingMessage;
                    }
                    array2 = array;
                    final int receiveLimit = this.recordLayer.getReceiveLimit();
                    byte[] array3 = null;
                    Label_0105: {
                        if (array != null) {
                            array3 = array;
                            array2 = array;
                            if (array.length >= receiveLimit) {
                                break Label_0105;
                            }
                        }
                        array2 = array;
                        array3 = new byte[receiveLimit];
                    }
                    array2 = array3;
                    final int receive = this.recordLayer.receive(array3, 0, receiveLimit, n2);
                    if (receive < 0) {
                        array = array3;
                        this.resendOutboundFlight();
                        n = this.backOff(n2);
                    }
                    else {
                        array = array3;
                        n = n2;
                        array2 = array3;
                        if (!this.processRecord(16, this.recordLayer.getReadEpoch(), array3, 0, receive)) {
                            continue Label_0194_Outer;
                        }
                        array2 = array3;
                        n = this.backOff(n2);
                        array = array3;
                    }
                }
                catch (IOException ex) {
                    array = array2;
                    continue;
                }
                break;
            }
        }
    }
    
    byte[] receiveMessageBody(final short n) throws IOException {
        final Message receiveMessage = this.receiveMessage();
        if (receiveMessage.getType() == n) {
            return receiveMessage.getBody();
        }
        throw new TlsFatalAlert((short)10);
    }
    
    void resetHandshakeMessagesDigest() {
        this.handshakeHash.reset();
    }
    
    void sendMessage(final short n, final byte[] array) throws IOException {
        TlsUtils.checkUint24(array.length);
        if (!this.sending) {
            this.checkInboundFlight();
            this.sending = true;
            this.outboundFlight.removeAllElements();
        }
        final Message obj = new Message(this.message_seq++, n, array);
        this.outboundFlight.addElement(obj);
        this.writeMessage(obj);
        this.updateHandshakeMessagesDigest(obj);
    }
    
    static class Message
    {
        private final byte[] body;
        private final int message_seq;
        private final short msg_type;
        
        private Message(final int message_seq, final short msg_type, final byte[] body) {
            this.message_seq = message_seq;
            this.msg_type = msg_type;
            this.body = body;
        }
        
        public byte[] getBody() {
            return this.body;
        }
        
        public int getSeq() {
            return this.message_seq;
        }
        
        public short getType() {
            return this.msg_type;
        }
    }
    
    static class RecordLayerBuffer extends ByteArrayOutputStream
    {
        RecordLayerBuffer(final int size) {
            super(size);
        }
        
        void sendToRecordLayer(final DTLSRecordLayer dtlsRecordLayer) throws IOException {
            dtlsRecordLayer.send(this.buf, 0, this.count);
            this.buf = null;
        }
    }
}
