// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.tls;

import java.security.SecureRandom;

class TlsServerContextImpl extends AbstractTlsContext implements TlsServerContext
{
    TlsServerContextImpl(final SecureRandom secureRandom, final SecurityParameters securityParameters) {
        super(secureRandom, securityParameters);
    }
    
    @Override
    public boolean isServer() {
        return true;
    }
}
