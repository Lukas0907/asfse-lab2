// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto;

public interface Committer
{
    Commitment commit(final byte[] p0);
    
    boolean isRevealed(final Commitment p0, final byte[] p1);
}
