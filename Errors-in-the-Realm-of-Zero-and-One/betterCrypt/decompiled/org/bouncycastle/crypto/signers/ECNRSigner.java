// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.signers;

import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.params.ECKeyGenerationParameters;
import org.bouncycastle.crypto.generators.ECKeyPairGenerator;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.math.ec.ECAlgorithms;
import org.bouncycastle.math.ec.ECConstants;
import java.math.BigInteger;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import java.security.SecureRandom;
import org.bouncycastle.crypto.params.ECKeyParameters;
import org.bouncycastle.crypto.DSAExt;

public class ECNRSigner implements DSAExt
{
    private boolean forSigning;
    private ECKeyParameters key;
    private SecureRandom random;
    
    private BigInteger extractT(final ECPublicKeyParameters ecPublicKeyParameters, final BigInteger bigInteger, final BigInteger bigInteger2) {
        final BigInteger n = ecPublicKeyParameters.getParameters().getN();
        if (bigInteger.compareTo(ECConstants.ONE) >= 0) {
            if (bigInteger.compareTo(n) >= 0) {
                return null;
            }
            if (bigInteger2.compareTo(ECConstants.ZERO) >= 0) {
                if (bigInteger2.compareTo(n) >= 0) {
                    return null;
                }
                final ECPoint normalize = ECAlgorithms.sumOfTwoMultiplies(ecPublicKeyParameters.getParameters().getG(), bigInteger2, ecPublicKeyParameters.getQ(), bigInteger).normalize();
                if (normalize.isInfinity()) {
                    return null;
                }
                return bigInteger.subtract(normalize.getAffineXCoord().toBigInteger()).mod(n);
            }
        }
        return null;
    }
    
    @Override
    public BigInteger[] generateSignature(final byte[] magnitude) {
        if (!this.forSigning) {
            throw new IllegalStateException("not initialised for signing");
        }
        final BigInteger order = this.getOrder();
        final BigInteger val = new BigInteger(1, magnitude);
        final ECPrivateKeyParameters ecPrivateKeyParameters = (ECPrivateKeyParameters)this.key;
        if (val.compareTo(order) < 0) {
            BigInteger mod;
            AsymmetricCipherKeyPair generateKeyPair;
            do {
                final ECKeyPairGenerator ecKeyPairGenerator = new ECKeyPairGenerator();
                ecKeyPairGenerator.init(new ECKeyGenerationParameters(ecPrivateKeyParameters.getParameters(), this.random));
                generateKeyPair = ecKeyPairGenerator.generateKeyPair();
                mod = ((ECPublicKeyParameters)generateKeyPair.getPublic()).getQ().getAffineXCoord().toBigInteger().add(val).mod(order);
            } while (mod.equals(ECConstants.ZERO));
            return new BigInteger[] { mod, ((ECPrivateKeyParameters)generateKeyPair.getPrivate()).getD().subtract(mod.multiply(ecPrivateKeyParameters.getD())).mod(order) };
        }
        throw new DataLengthException("input too large for ECNR key");
    }
    
    @Override
    public BigInteger getOrder() {
        return this.key.getParameters().getN();
    }
    
    public byte[] getRecoveredMessage(BigInteger t, final BigInteger bigInteger) {
        if (this.forSigning) {
            throw new IllegalStateException("not initialised for verifying/recovery");
        }
        t = this.extractT((ECPublicKeyParameters)this.key, t, bigInteger);
        if (t != null) {
            return BigIntegers.asUnsignedByteArray(t);
        }
        return null;
    }
    
    @Override
    public void init(final boolean forSigning, final CipherParameters cipherParameters) {
        this.forSigning = forSigning;
        ECKeyParameters key;
        if (forSigning) {
            if (cipherParameters instanceof ParametersWithRandom) {
                final ParametersWithRandom parametersWithRandom = (ParametersWithRandom)cipherParameters;
                this.random = parametersWithRandom.getRandom();
                this.key = (ECPrivateKeyParameters)parametersWithRandom.getParameters();
                return;
            }
            this.random = CryptoServicesRegistrar.getSecureRandom();
            key = (ECPrivateKeyParameters)cipherParameters;
        }
        else {
            key = (ECPublicKeyParameters)cipherParameters;
        }
        this.key = key;
    }
    
    @Override
    public boolean verifySignature(final byte[] magnitude, BigInteger t, final BigInteger bigInteger) {
        if (this.forSigning) {
            throw new IllegalStateException("not initialised for verifying");
        }
        final ECPublicKeyParameters ecPublicKeyParameters = (ECPublicKeyParameters)this.key;
        final BigInteger n = ecPublicKeyParameters.getParameters().getN();
        final int bitLength = n.bitLength();
        final BigInteger bigInteger2 = new BigInteger(1, magnitude);
        if (bigInteger2.bitLength() <= bitLength) {
            t = this.extractT(ecPublicKeyParameters, t, bigInteger);
            return t != null && t.equals(bigInteger2.mod(n));
        }
        throw new DataLengthException("input too large for ECNR key.");
    }
}
