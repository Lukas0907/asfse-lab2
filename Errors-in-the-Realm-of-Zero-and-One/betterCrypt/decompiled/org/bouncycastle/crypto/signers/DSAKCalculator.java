// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.signers;

import java.security.SecureRandom;
import java.math.BigInteger;

public interface DSAKCalculator
{
    void init(final BigInteger p0, final BigInteger p1, final byte[] p2);
    
    void init(final BigInteger p0, final SecureRandom p1);
    
    boolean isDeterministic();
    
    BigInteger nextK();
}
