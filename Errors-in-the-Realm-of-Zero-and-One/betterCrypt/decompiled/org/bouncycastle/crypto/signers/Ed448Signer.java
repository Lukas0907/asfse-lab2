// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.signers;

import org.bouncycastle.math.ec.rfc8032.Ed448;
import java.io.ByteArrayOutputStream;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.params.Ed448PublicKeyParameters;
import org.bouncycastle.crypto.params.Ed448PrivateKeyParameters;
import org.bouncycastle.crypto.Signer;

public class Ed448Signer implements Signer
{
    private final Buffer buffer;
    private final byte[] context;
    private boolean forSigning;
    private Ed448PrivateKeyParameters privateKey;
    private Ed448PublicKeyParameters publicKey;
    
    public Ed448Signer(final byte[] array) {
        this.buffer = new Buffer();
        this.context = Arrays.clone(array);
    }
    
    @Override
    public byte[] generateSignature() {
        if (this.forSigning) {
            final Ed448PrivateKeyParameters privateKey = this.privateKey;
            if (privateKey != null) {
                return this.buffer.generateSignature(privateKey, this.publicKey, this.context);
            }
        }
        throw new IllegalStateException("Ed448Signer not initialised for signature generation.");
    }
    
    @Override
    public void init(final boolean forSigning, final CipherParameters cipherParameters) {
        this.forSigning = forSigning;
        if (forSigning) {
            this.privateKey = (Ed448PrivateKeyParameters)cipherParameters;
            this.publicKey = this.privateKey.generatePublicKey();
        }
        else {
            this.privateKey = null;
            this.publicKey = (Ed448PublicKeyParameters)cipherParameters;
        }
        this.reset();
    }
    
    @Override
    public void reset() {
        this.buffer.reset();
    }
    
    @Override
    public void update(final byte b) {
        this.buffer.write(b);
    }
    
    @Override
    public void update(final byte[] b, final int off, final int len) {
        this.buffer.write(b, off, len);
    }
    
    @Override
    public boolean verifySignature(final byte[] array) {
        if (!this.forSigning) {
            final Ed448PublicKeyParameters publicKey = this.publicKey;
            if (publicKey != null) {
                return this.buffer.verifySignature(publicKey, this.context, array);
            }
        }
        throw new IllegalStateException("Ed448Signer not initialised for verification");
    }
    
    private static class Buffer extends ByteArrayOutputStream
    {
        byte[] generateSignature(final Ed448PrivateKeyParameters ed448PrivateKeyParameters, final Ed448PublicKeyParameters ed448PublicKeyParameters, final byte[] array) {
            synchronized (this) {
                final byte[] array2 = new byte[114];
                ed448PrivateKeyParameters.sign(0, ed448PublicKeyParameters, array, this.buf, 0, this.count, array2, 0);
                this.reset();
                return array2;
            }
        }
        
        @Override
        public void reset() {
            synchronized (this) {
                Arrays.fill(this.buf, 0, this.count, (byte)0);
                this.count = 0;
            }
        }
        
        boolean verifySignature(final Ed448PublicKeyParameters ed448PublicKeyParameters, final byte[] array, final byte[] array2) {
            synchronized (this) {
                if (114 != array2.length) {
                    return false;
                }
                final boolean verify = Ed448.verify(array2, 0, ed448PublicKeyParameters.getEncoded(), 0, array, this.buf, 0, this.count);
                this.reset();
                return verify;
            }
        }
    }
}
