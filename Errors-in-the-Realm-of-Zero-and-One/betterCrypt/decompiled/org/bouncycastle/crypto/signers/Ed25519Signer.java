// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.signers;

import org.bouncycastle.math.ec.rfc8032.Ed25519;
import org.bouncycastle.util.Arrays;
import java.io.ByteArrayOutputStream;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.Signer;

public class Ed25519Signer implements Signer
{
    private final Buffer buffer;
    private boolean forSigning;
    private Ed25519PrivateKeyParameters privateKey;
    private Ed25519PublicKeyParameters publicKey;
    
    public Ed25519Signer() {
        this.buffer = new Buffer();
    }
    
    @Override
    public byte[] generateSignature() {
        if (this.forSigning) {
            final Ed25519PrivateKeyParameters privateKey = this.privateKey;
            if (privateKey != null) {
                return this.buffer.generateSignature(privateKey, this.publicKey);
            }
        }
        throw new IllegalStateException("Ed25519Signer not initialised for signature generation.");
    }
    
    @Override
    public void init(final boolean forSigning, final CipherParameters cipherParameters) {
        this.forSigning = forSigning;
        if (forSigning) {
            this.privateKey = (Ed25519PrivateKeyParameters)cipherParameters;
            this.publicKey = this.privateKey.generatePublicKey();
        }
        else {
            this.privateKey = null;
            this.publicKey = (Ed25519PublicKeyParameters)cipherParameters;
        }
        this.reset();
    }
    
    @Override
    public void reset() {
        this.buffer.reset();
    }
    
    @Override
    public void update(final byte b) {
        this.buffer.write(b);
    }
    
    @Override
    public void update(final byte[] b, final int off, final int len) {
        this.buffer.write(b, off, len);
    }
    
    @Override
    public boolean verifySignature(final byte[] array) {
        if (!this.forSigning) {
            final Ed25519PublicKeyParameters publicKey = this.publicKey;
            if (publicKey != null) {
                return this.buffer.verifySignature(publicKey, array);
            }
        }
        throw new IllegalStateException("Ed25519Signer not initialised for verification");
    }
    
    private static class Buffer extends ByteArrayOutputStream
    {
        byte[] generateSignature(final Ed25519PrivateKeyParameters ed25519PrivateKeyParameters, final Ed25519PublicKeyParameters ed25519PublicKeyParameters) {
            synchronized (this) {
                final byte[] array = new byte[64];
                ed25519PrivateKeyParameters.sign(0, ed25519PublicKeyParameters, null, this.buf, 0, this.count, array, 0);
                this.reset();
                return array;
            }
        }
        
        @Override
        public void reset() {
            synchronized (this) {
                Arrays.fill(this.buf, 0, this.count, (byte)0);
                this.count = 0;
            }
        }
        
        boolean verifySignature(final Ed25519PublicKeyParameters ed25519PublicKeyParameters, final byte[] array) {
            synchronized (this) {
                if (64 != array.length) {
                    return false;
                }
                final boolean verify = Ed25519.verify(array, 0, ed25519PublicKeyParameters.getEncoded(), 0, this.buf, 0, this.count);
                this.reset();
                return verify;
            }
        }
    }
}
