// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.signers;

import org.bouncycastle.util.BigIntegers;
import java.security.SecureRandom;
import java.math.BigInteger;

public class RandomDSAKCalculator implements DSAKCalculator
{
    private static final BigInteger ZERO;
    private BigInteger q;
    private SecureRandom random;
    
    static {
        ZERO = BigInteger.valueOf(0L);
    }
    
    @Override
    public void init(final BigInteger bigInteger, final BigInteger bigInteger2, final byte[] array) {
        throw new IllegalStateException("Operation not supported");
    }
    
    @Override
    public void init(final BigInteger q, final SecureRandom random) {
        this.q = q;
        this.random = random;
    }
    
    @Override
    public boolean isDeterministic() {
        return false;
    }
    
    @Override
    public BigInteger nextK() {
        final int bitLength = this.q.bitLength();
        BigInteger randomBigInteger;
        do {
            randomBigInteger = BigIntegers.createRandomBigInteger(bitLength, this.random);
        } while (randomBigInteger.equals(RandomDSAKCalculator.ZERO) || randomBigInteger.compareTo(this.q) >= 0);
        return randomBigInteger;
    }
}
