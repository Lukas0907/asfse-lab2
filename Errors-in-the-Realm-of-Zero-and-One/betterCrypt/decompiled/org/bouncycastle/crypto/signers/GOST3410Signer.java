// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.signers;

import org.bouncycastle.crypto.params.GOST3410PublicKeyParameters;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.GOST3410Parameters;
import org.bouncycastle.crypto.params.GOST3410PrivateKeyParameters;
import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.util.Arrays;
import java.math.BigInteger;
import java.security.SecureRandom;
import org.bouncycastle.crypto.params.GOST3410KeyParameters;
import org.bouncycastle.crypto.DSAExt;

public class GOST3410Signer implements DSAExt
{
    GOST3410KeyParameters key;
    SecureRandom random;
    
    @Override
    public BigInteger[] generateSignature(final byte[] array) {
        final BigInteger val = new BigInteger(1, Arrays.reverse(array));
        final GOST3410Parameters parameters = this.key.getParameters();
        BigInteger randomBigInteger;
        do {
            randomBigInteger = BigIntegers.createRandomBigInteger(parameters.getQ().bitLength(), this.random);
        } while (randomBigInteger.compareTo(parameters.getQ()) >= 0);
        final BigInteger mod = parameters.getA().modPow(randomBigInteger, parameters.getP()).mod(parameters.getQ());
        return new BigInteger[] { mod, randomBigInteger.multiply(val).add(((GOST3410PrivateKeyParameters)this.key).getX().multiply(mod)).mod(parameters.getQ()) };
    }
    
    @Override
    public BigInteger getOrder() {
        return this.key.getParameters().getQ();
    }
    
    @Override
    public void init(final boolean b, final CipherParameters cipherParameters) {
        GOST3410KeyParameters key;
        if (b) {
            if (cipherParameters instanceof ParametersWithRandom) {
                final ParametersWithRandom parametersWithRandom = (ParametersWithRandom)cipherParameters;
                this.random = parametersWithRandom.getRandom();
                this.key = (GOST3410PrivateKeyParameters)parametersWithRandom.getParameters();
                return;
            }
            this.random = CryptoServicesRegistrar.getSecureRandom();
            key = (GOST3410PrivateKeyParameters)cipherParameters;
        }
        else {
            key = (GOST3410PublicKeyParameters)cipherParameters;
        }
        this.key = key;
    }
    
    @Override
    public boolean verifySignature(final byte[] array, final BigInteger bigInteger, BigInteger mod) {
        final BigInteger bigInteger2 = new BigInteger(1, Arrays.reverse(array));
        final GOST3410Parameters parameters = this.key.getParameters();
        final BigInteger value = BigInteger.valueOf(0L);
        if (value.compareTo(bigInteger) < 0) {
            if (parameters.getQ().compareTo(bigInteger) <= 0) {
                return false;
            }
            if (value.compareTo(mod) < 0) {
                if (parameters.getQ().compareTo(mod) <= 0) {
                    return false;
                }
                final BigInteger modPow = bigInteger2.modPow(parameters.getQ().subtract(new BigInteger("2")), parameters.getQ());
                mod = mod.multiply(modPow).mod(parameters.getQ());
                return parameters.getA().modPow(mod, parameters.getP()).multiply(((GOST3410PublicKeyParameters)this.key).getY().modPow(parameters.getQ().subtract(bigInteger).multiply(modPow).mod(parameters.getQ()), parameters.getP())).mod(parameters.getP()).mod(parameters.getQ()).equals(bigInteger);
            }
        }
        return false;
    }
}
