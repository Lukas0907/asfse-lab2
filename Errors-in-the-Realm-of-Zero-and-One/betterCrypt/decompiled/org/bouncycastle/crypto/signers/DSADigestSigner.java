// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.signers;

import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import java.math.BigInteger;
import org.bouncycastle.crypto.DSAExt;
import org.bouncycastle.crypto.DSA;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.Signer;

public class DSADigestSigner implements Signer
{
    private final Digest digest;
    private final DSA dsa;
    private final DSAEncoding encoding;
    private boolean forSigning;
    
    public DSADigestSigner(final DSA dsa, final Digest digest) {
        this.dsa = dsa;
        this.digest = digest;
        this.encoding = StandardDSAEncoding.INSTANCE;
    }
    
    public DSADigestSigner(final DSAExt dsa, final Digest digest, final DSAEncoding encoding) {
        this.dsa = dsa;
        this.digest = digest;
        this.encoding = encoding;
    }
    
    @Override
    public byte[] generateSignature() {
        while (true) {
            if (this.forSigning) {
                final byte[] array = new byte[this.digest.getDigestSize()];
                this.digest.doFinal(array, 0);
                final BigInteger[] generateSignature = this.dsa.generateSignature(array);
                while (true) {
                    try {
                        return this.encoding.encode(this.getOrder(), generateSignature[0], generateSignature[1]);
                        throw new IllegalStateException("unable to encode signature");
                        throw new IllegalStateException("DSADigestSigner not initialised for signature generation.");
                    }
                    catch (Exception ex) {}
                    continue;
                }
            }
            continue;
        }
    }
    
    protected BigInteger getOrder() {
        final DSA dsa = this.dsa;
        if (dsa instanceof DSAExt) {
            return ((DSAExt)dsa).getOrder();
        }
        return null;
    }
    
    @Override
    public void init(final boolean forSigning, final CipherParameters cipherParameters) {
        this.forSigning = forSigning;
        AsymmetricKeyParameter asymmetricKeyParameter;
        if (cipherParameters instanceof ParametersWithRandom) {
            asymmetricKeyParameter = (AsymmetricKeyParameter)((ParametersWithRandom)cipherParameters).getParameters();
        }
        else {
            asymmetricKeyParameter = (AsymmetricKeyParameter)cipherParameters;
        }
        if (forSigning && !asymmetricKeyParameter.isPrivate()) {
            throw new IllegalArgumentException("Signing Requires Private Key.");
        }
        if (!forSigning && asymmetricKeyParameter.isPrivate()) {
            throw new IllegalArgumentException("Verification Requires Public Key.");
        }
        this.reset();
        this.dsa.init(forSigning, cipherParameters);
    }
    
    @Override
    public void reset() {
        this.digest.reset();
    }
    
    @Override
    public void update(final byte b) {
        this.digest.update(b);
    }
    
    @Override
    public void update(final byte[] array, final int n, final int n2) {
        this.digest.update(array, n, n2);
    }
    
    @Override
    public boolean verifySignature(final byte[] array) {
        Label_0065: {
            if (this.forSigning) {
                break Label_0065;
            }
            final byte[] array2 = new byte[this.digest.getDigestSize()];
            this.digest.doFinal(array2, 0);
            try {
                final BigInteger[] decode = this.encoding.decode(this.getOrder(), array);
                return this.dsa.verifySignature(array2, decode[0], decode[1]);
                throw new IllegalStateException("DSADigestSigner not initialised for verification");
            }
            catch (Exception ex) {
                return false;
            }
        }
    }
}
