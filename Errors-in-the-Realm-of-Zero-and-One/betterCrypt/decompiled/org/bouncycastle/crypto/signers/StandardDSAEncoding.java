// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.signers;

import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Integer;
import java.io.IOException;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import java.math.BigInteger;

public class StandardDSAEncoding implements DSAEncoding
{
    public static final StandardDSAEncoding INSTANCE;
    
    static {
        INSTANCE = new StandardDSAEncoding();
    }
    
    protected BigInteger checkValue(final BigInteger val, final BigInteger bigInteger) {
        if (bigInteger.signum() >= 0 && (val == null || bigInteger.compareTo(val) < 0)) {
            return bigInteger;
        }
        throw new IllegalArgumentException("Value out of range");
    }
    
    @Override
    public BigInteger[] decode(final BigInteger bigInteger, final byte[] array) throws IOException {
        final ASN1Sequence asn1Sequence = (ASN1Sequence)ASN1Primitive.fromByteArray(array);
        if (asn1Sequence.size() == 2) {
            final BigInteger decodeValue = this.decodeValue(bigInteger, asn1Sequence, 0);
            final BigInteger decodeValue2 = this.decodeValue(bigInteger, asn1Sequence, 1);
            if (Arrays.areEqual(this.encode(bigInteger, decodeValue, decodeValue2), array)) {
                return new BigInteger[] { decodeValue, decodeValue2 };
            }
        }
        throw new IllegalArgumentException("Malformed signature");
    }
    
    protected BigInteger decodeValue(final BigInteger bigInteger, final ASN1Sequence asn1Sequence, final int n) {
        return this.checkValue(bigInteger, ((ASN1Integer)asn1Sequence.getObjectAt(n)).getValue());
    }
    
    @Override
    public byte[] encode(final BigInteger bigInteger, final BigInteger bigInteger2, final BigInteger bigInteger3) throws IOException {
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        this.encodeValue(bigInteger, asn1EncodableVector, bigInteger2);
        this.encodeValue(bigInteger, asn1EncodableVector, bigInteger3);
        return new DERSequence(asn1EncodableVector).getEncoded("DER");
    }
    
    protected void encodeValue(final BigInteger bigInteger, final ASN1EncodableVector asn1EncodableVector, final BigInteger bigInteger2) {
        asn1EncodableVector.add(new ASN1Integer(this.checkValue(bigInteger, bigInteger2)));
    }
}
