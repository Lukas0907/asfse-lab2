// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.signers;

import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.math.ec.ECAlgorithms;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.math.ec.ECConstants;
import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.util.Arrays;
import java.math.BigInteger;
import org.bouncycastle.math.ec.FixedPointCombMultiplier;
import org.bouncycastle.math.ec.ECMultiplier;
import java.security.SecureRandom;
import org.bouncycastle.crypto.params.ECKeyParameters;
import org.bouncycastle.crypto.DSAExt;

public class ECGOST3410_2012Signer implements DSAExt
{
    ECKeyParameters key;
    SecureRandom random;
    
    protected ECMultiplier createBasePointMultiplier() {
        return new FixedPointCombMultiplier();
    }
    
    @Override
    public BigInteger[] generateSignature(final byte[] array) {
        final BigInteger val = new BigInteger(1, Arrays.reverse(array));
        final ECDomainParameters parameters = this.key.getParameters();
        final BigInteger n = parameters.getN();
        final BigInteger d = ((ECPrivateKeyParameters)this.key).getD();
        final ECMultiplier basePointMultiplier = this.createBasePointMultiplier();
        BigInteger mod;
        BigInteger mod2;
        while (true) {
            final BigInteger randomBigInteger = BigIntegers.createRandomBigInteger(n.bitLength(), this.random);
            if (!randomBigInteger.equals(ECConstants.ZERO)) {
                mod = basePointMultiplier.multiply(parameters.getG(), randomBigInteger).normalize().getAffineXCoord().toBigInteger().mod(n);
                if (mod.equals(ECConstants.ZERO)) {
                    continue;
                }
                mod2 = randomBigInteger.multiply(val).add(d.multiply(mod)).mod(n);
                if (!mod2.equals(ECConstants.ZERO)) {
                    break;
                }
                continue;
            }
        }
        return new BigInteger[] { mod, mod2 };
    }
    
    @Override
    public BigInteger getOrder() {
        return this.key.getParameters().getN();
    }
    
    @Override
    public void init(final boolean b, final CipherParameters cipherParameters) {
        ECKeyParameters key;
        if (b) {
            if (cipherParameters instanceof ParametersWithRandom) {
                final ParametersWithRandom parametersWithRandom = (ParametersWithRandom)cipherParameters;
                this.random = parametersWithRandom.getRandom();
                this.key = (ECPrivateKeyParameters)parametersWithRandom.getParameters();
                return;
            }
            this.random = CryptoServicesRegistrar.getSecureRandom();
            key = (ECPrivateKeyParameters)cipherParameters;
        }
        else {
            key = (ECPublicKeyParameters)cipherParameters;
        }
        this.key = key;
    }
    
    @Override
    public boolean verifySignature(final byte[] array, final BigInteger bigInteger, BigInteger mod) {
        final BigInteger bigInteger2 = new BigInteger(1, Arrays.reverse(array));
        final BigInteger n = this.key.getParameters().getN();
        if (bigInteger.compareTo(ECConstants.ONE) >= 0) {
            if (bigInteger.compareTo(n) >= 0) {
                return false;
            }
            if (mod.compareTo(ECConstants.ONE) >= 0) {
                if (mod.compareTo(n) >= 0) {
                    return false;
                }
                final BigInteger modInverse = bigInteger2.modInverse(n);
                mod = mod.multiply(modInverse).mod(n);
                final ECPoint normalize = ECAlgorithms.sumOfTwoMultiplies(this.key.getParameters().getG(), mod, ((ECPublicKeyParameters)this.key).getQ(), n.subtract(bigInteger).multiply(modInverse).mod(n)).normalize();
                return !normalize.isInfinity() && normalize.getAffineXCoord().toBigInteger().mod(n).equals(bigInteger);
            }
        }
        return false;
    }
}
