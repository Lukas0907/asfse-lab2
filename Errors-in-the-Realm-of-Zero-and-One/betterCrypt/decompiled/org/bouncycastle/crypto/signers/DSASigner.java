// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.signers;

import org.bouncycastle.crypto.params.DSAPublicKeyParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.DSAParameters;
import org.bouncycastle.crypto.params.DSAPrivateKeyParameters;
import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import java.math.BigInteger;
import java.security.SecureRandom;
import org.bouncycastle.crypto.params.DSAKeyParameters;
import org.bouncycastle.crypto.DSAExt;

public class DSASigner implements DSAExt
{
    private final DSAKCalculator kCalculator;
    private DSAKeyParameters key;
    private SecureRandom random;
    
    public DSASigner() {
        this.kCalculator = new RandomDSAKCalculator();
    }
    
    public DSASigner(final DSAKCalculator kCalculator) {
        this.kCalculator = kCalculator;
    }
    
    private BigInteger calculateE(final BigInteger bigInteger, final byte[] magnitude) {
        if (bigInteger.bitLength() >= magnitude.length * 8) {
            return new BigInteger(1, magnitude);
        }
        final byte[] magnitude2 = new byte[bigInteger.bitLength() / 8];
        System.arraycopy(magnitude, 0, magnitude2, 0, magnitude2.length);
        return new BigInteger(1, magnitude2);
    }
    
    private BigInteger getRandomizer(final BigInteger val, SecureRandom secureRandom) {
        if (secureRandom == null) {
            secureRandom = CryptoServicesRegistrar.getSecureRandom();
        }
        return BigIntegers.createRandomBigInteger(7, secureRandom).add(BigInteger.valueOf(128L)).multiply(val);
    }
    
    @Override
    public BigInteger[] generateSignature(final byte[] array) {
        final DSAParameters parameters = this.key.getParameters();
        final BigInteger q = parameters.getQ();
        final BigInteger calculateE = this.calculateE(q, array);
        final BigInteger x = ((DSAPrivateKeyParameters)this.key).getX();
        if (this.kCalculator.isDeterministic()) {
            this.kCalculator.init(q, x, array);
        }
        else {
            this.kCalculator.init(q, this.random);
        }
        final BigInteger nextK = this.kCalculator.nextK();
        final BigInteger mod = parameters.getG().modPow(nextK.add(this.getRandomizer(q, this.random)), parameters.getP()).mod(q);
        return new BigInteger[] { mod, nextK.modInverse(q).multiply(calculateE.add(x.multiply(mod))).mod(q) };
    }
    
    @Override
    public BigInteger getOrder() {
        return this.key.getParameters().getQ();
    }
    
    @Override
    public void init(final boolean b, final CipherParameters cipherParameters) {
        SecureRandom random = null;
        Label_0055: {
            DSAKeyParameters key;
            if (b) {
                if (cipherParameters instanceof ParametersWithRandom) {
                    final ParametersWithRandom parametersWithRandom = (ParametersWithRandom)cipherParameters;
                    this.key = (DSAPrivateKeyParameters)parametersWithRandom.getParameters();
                    random = parametersWithRandom.getRandom();
                    break Label_0055;
                }
                key = (DSAPrivateKeyParameters)cipherParameters;
            }
            else {
                key = (DSAPublicKeyParameters)cipherParameters;
            }
            this.key = key;
            random = null;
        }
        this.random = this.initSecureRandom(b && !this.kCalculator.isDeterministic(), random);
    }
    
    protected SecureRandom initSecureRandom(final boolean b, final SecureRandom secureRandom) {
        if (!b) {
            return null;
        }
        if (secureRandom != null) {
            return secureRandom;
        }
        return CryptoServicesRegistrar.getSecureRandom();
    }
    
    @Override
    public boolean verifySignature(final byte[] array, final BigInteger x, BigInteger exponent) {
        final DSAParameters parameters = this.key.getParameters();
        final BigInteger q = parameters.getQ();
        final BigInteger calculateE = this.calculateE(q, array);
        final BigInteger value = BigInteger.valueOf(0L);
        if (value.compareTo(x) < 0) {
            if (q.compareTo(x) <= 0) {
                return false;
            }
            if (value.compareTo(exponent) < 0) {
                if (q.compareTo(exponent) <= 0) {
                    return false;
                }
                exponent = exponent.modInverse(q);
                final BigInteger mod = calculateE.multiply(exponent).mod(q);
                exponent = x.multiply(exponent).mod(q);
                final BigInteger p3 = parameters.getP();
                return parameters.getG().modPow(mod, p3).multiply(((DSAPublicKeyParameters)this.key).getY().modPow(exponent, p3)).mod(p3).mod(q).equals(x);
            }
        }
        return false;
    }
}
