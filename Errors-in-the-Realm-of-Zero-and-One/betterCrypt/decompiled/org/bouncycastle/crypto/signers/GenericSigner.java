// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.signers;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.Signer;

public class GenericSigner implements Signer
{
    private final Digest digest;
    private final AsymmetricBlockCipher engine;
    private boolean forSigning;
    
    public GenericSigner(final AsymmetricBlockCipher engine, final Digest digest) {
        this.engine = engine;
        this.digest = digest;
    }
    
    @Override
    public byte[] generateSignature() throws CryptoException, DataLengthException {
        if (this.forSigning) {
            final byte[] array = new byte[this.digest.getDigestSize()];
            this.digest.doFinal(array, 0);
            return this.engine.processBlock(array, 0, array.length);
        }
        throw new IllegalStateException("GenericSigner not initialised for signature generation.");
    }
    
    @Override
    public void init(final boolean forSigning, final CipherParameters cipherParameters) {
        this.forSigning = forSigning;
        AsymmetricKeyParameter asymmetricKeyParameter;
        if (cipherParameters instanceof ParametersWithRandom) {
            asymmetricKeyParameter = (AsymmetricKeyParameter)((ParametersWithRandom)cipherParameters).getParameters();
        }
        else {
            asymmetricKeyParameter = (AsymmetricKeyParameter)cipherParameters;
        }
        if (forSigning && !asymmetricKeyParameter.isPrivate()) {
            throw new IllegalArgumentException("signing requires private key");
        }
        if (!forSigning && asymmetricKeyParameter.isPrivate()) {
            throw new IllegalArgumentException("verification requires public key");
        }
        this.reset();
        this.engine.init(forSigning, cipherParameters);
    }
    
    @Override
    public void reset() {
        this.digest.reset();
    }
    
    @Override
    public void update(final byte b) {
        this.digest.update(b);
    }
    
    @Override
    public void update(final byte[] array, final int n, final int n2) {
        this.digest.update(array, n, n2);
    }
    
    @Override
    public boolean verifySignature(byte[] processBlock) {
        Label_0085: {
            if (this.forSigning) {
                break Label_0085;
            }
            final byte[] array = new byte[this.digest.getDigestSize()];
            this.digest.doFinal(array, 0);
            try {
                final byte[] array2 = processBlock = this.engine.processBlock(processBlock, 0, processBlock.length);
                if (array2.length < array.length) {
                    processBlock = new byte[array.length];
                    System.arraycopy(array2, 0, processBlock, processBlock.length - array2.length, array2.length);
                }
                return Arrays.constantTimeAreEqual(processBlock, array);
                throw new IllegalStateException("GenericSigner not initialised for verification");
            }
            catch (Exception ex) {
                return false;
            }
        }
    }
}
