// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.signers;

import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.util.Arrays;
import java.math.BigInteger;

public class PlainDSAEncoding implements DSAEncoding
{
    public static final PlainDSAEncoding INSTANCE;
    
    static {
        INSTANCE = new PlainDSAEncoding();
    }
    
    private void encodeValue(final BigInteger bigInteger, final BigInteger bigInteger2, final byte[] array, final int n, int n2) {
        final byte[] byteArray = this.checkValue(bigInteger, bigInteger2).toByteArray();
        final int max = Math.max(0, byteArray.length - n2);
        final int n3 = byteArray.length - max;
        n2 = n2 - n3 + n;
        Arrays.fill(array, n, n2, (byte)0);
        System.arraycopy(byteArray, max, array, n2, n3);
    }
    
    protected BigInteger checkValue(final BigInteger val, final BigInteger bigInteger) {
        if (bigInteger.signum() >= 0 && bigInteger.compareTo(val) < 0) {
            return bigInteger;
        }
        throw new IllegalArgumentException("Value out of range");
    }
    
    @Override
    public BigInteger[] decode(final BigInteger bigInteger, final byte[] array) {
        final int unsignedByteLength = BigIntegers.getUnsignedByteLength(bigInteger);
        if (array.length == unsignedByteLength * 2) {
            return new BigInteger[] { this.decodeValue(bigInteger, array, 0, unsignedByteLength), this.decodeValue(bigInteger, array, unsignedByteLength, unsignedByteLength) };
        }
        throw new IllegalArgumentException("Encoding has incorrect length");
    }
    
    protected BigInteger decodeValue(final BigInteger bigInteger, final byte[] array, final int n, final int n2) {
        return this.checkValue(bigInteger, new BigInteger(1, Arrays.copyOfRange(array, n, n2 + n)));
    }
    
    @Override
    public byte[] encode(final BigInteger bigInteger, final BigInteger bigInteger2, final BigInteger bigInteger3) {
        final int unsignedByteLength = BigIntegers.getUnsignedByteLength(bigInteger);
        final byte[] array = new byte[unsignedByteLength * 2];
        this.encodeValue(bigInteger, bigInteger2, array, 0, unsignedByteLength);
        this.encodeValue(bigInteger, bigInteger3, array, unsignedByteLength, unsignedByteLength);
        return array;
    }
}
