// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.signers;

import java.io.IOException;
import java.math.BigInteger;

public interface DSAEncoding
{
    BigInteger[] decode(final BigInteger p0, final byte[] p1) throws IOException;
    
    byte[] encode(final BigInteger p0, final BigInteger p1, final BigInteger p2) throws IOException;
}
