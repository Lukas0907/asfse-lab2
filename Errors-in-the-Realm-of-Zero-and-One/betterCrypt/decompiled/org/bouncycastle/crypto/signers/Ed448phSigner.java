// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.signers;

import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.math.ec.rfc8032.Ed448;
import org.bouncycastle.crypto.params.Ed448PublicKeyParameters;
import org.bouncycastle.crypto.params.Ed448PrivateKeyParameters;
import org.bouncycastle.crypto.Xof;
import org.bouncycastle.crypto.Signer;

public class Ed448phSigner implements Signer
{
    private final byte[] context;
    private boolean forSigning;
    private final Xof prehash;
    private Ed448PrivateKeyParameters privateKey;
    private Ed448PublicKeyParameters publicKey;
    
    public Ed448phSigner(final byte[] array) {
        this.prehash = Ed448.createPrehash();
        this.context = Arrays.clone(array);
    }
    
    @Override
    public byte[] generateSignature() {
        if (!this.forSigning || this.privateKey == null) {
            throw new IllegalStateException("Ed448phSigner not initialised for signature generation.");
        }
        final byte[] array = new byte[64];
        if (64 == this.prehash.doFinal(array, 0, 64)) {
            final byte[] array2 = new byte[114];
            this.privateKey.sign(1, this.publicKey, this.context, array, 0, 64, array2, 0);
            return array2;
        }
        throw new IllegalStateException("Prehash digest failed");
    }
    
    @Override
    public void init(final boolean forSigning, final CipherParameters cipherParameters) {
        this.forSigning = forSigning;
        if (forSigning) {
            this.privateKey = (Ed448PrivateKeyParameters)cipherParameters;
            this.publicKey = this.privateKey.generatePublicKey();
        }
        else {
            this.privateKey = null;
            this.publicKey = (Ed448PublicKeyParameters)cipherParameters;
        }
        this.reset();
    }
    
    @Override
    public void reset() {
        this.prehash.reset();
    }
    
    @Override
    public void update(final byte b) {
        this.prehash.update(b);
    }
    
    @Override
    public void update(final byte[] array, final int n, final int n2) {
        this.prehash.update(array, n, n2);
    }
    
    @Override
    public boolean verifySignature(final byte[] array) {
        if (!this.forSigning) {
            final Ed448PublicKeyParameters publicKey = this.publicKey;
            if (publicKey != null) {
                return 114 == array.length && Ed448.verifyPrehash(array, 0, publicKey.getEncoded(), 0, this.context, this.prehash);
            }
        }
        throw new IllegalStateException("Ed448phSigner not initialised for verification");
    }
}
