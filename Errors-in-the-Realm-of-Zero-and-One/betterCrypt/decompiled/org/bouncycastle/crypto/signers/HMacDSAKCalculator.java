// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.signers;

import java.security.SecureRandom;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.util.BigIntegers;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.macs.HMac;
import java.math.BigInteger;

public class HMacDSAKCalculator implements DSAKCalculator
{
    private static final BigInteger ZERO;
    private final byte[] K;
    private final byte[] V;
    private final HMac hMac;
    private BigInteger n;
    
    static {
        ZERO = BigInteger.valueOf(0L);
    }
    
    public HMacDSAKCalculator(final Digest digest) {
        this.hMac = new HMac(digest);
        this.V = new byte[this.hMac.getMacSize()];
        this.K = new byte[this.hMac.getMacSize()];
    }
    
    private BigInteger bitsToInt(final byte[] magnitude) {
        BigInteger shiftRight = new BigInteger(1, magnitude);
        if (magnitude.length * 8 > this.n.bitLength()) {
            shiftRight = shiftRight.shiftRight(magnitude.length * 8 - this.n.bitLength());
        }
        return shiftRight;
    }
    
    @Override
    public void init(final BigInteger val, BigInteger bigInteger, final byte[] array) {
        this.n = val;
        Arrays.fill(this.V, (byte)1);
        Arrays.fill(this.K, (byte)0);
        final int unsignedByteLength = BigIntegers.getUnsignedByteLength(val);
        final byte[] array2 = new byte[unsignedByteLength];
        final byte[] unsignedByteArray = BigIntegers.asUnsignedByteArray(bigInteger);
        System.arraycopy(unsignedByteArray, 0, array2, array2.length - unsignedByteArray.length, unsignedByteArray.length);
        final byte[] array3 = new byte[unsignedByteLength];
        final BigInteger bigInteger2 = bigInteger = this.bitsToInt(array);
        if (bigInteger2.compareTo(val) >= 0) {
            bigInteger = bigInteger2.subtract(val);
        }
        final byte[] unsignedByteArray2 = BigIntegers.asUnsignedByteArray(bigInteger);
        System.arraycopy(unsignedByteArray2, 0, array3, array3.length - unsignedByteArray2.length, unsignedByteArray2.length);
        this.hMac.init(new KeyParameter(this.K));
        final HMac hMac = this.hMac;
        final byte[] v = this.V;
        hMac.update(v, 0, v.length);
        this.hMac.update((byte)0);
        this.hMac.update(array2, 0, array2.length);
        this.hMac.update(array3, 0, array3.length);
        this.hMac.doFinal(this.K, 0);
        this.hMac.init(new KeyParameter(this.K));
        final HMac hMac2 = this.hMac;
        final byte[] v2 = this.V;
        hMac2.update(v2, 0, v2.length);
        this.hMac.doFinal(this.V, 0);
        final HMac hMac3 = this.hMac;
        final byte[] v3 = this.V;
        hMac3.update(v3, 0, v3.length);
        this.hMac.update((byte)1);
        this.hMac.update(array2, 0, array2.length);
        this.hMac.update(array3, 0, array3.length);
        this.hMac.doFinal(this.K, 0);
        this.hMac.init(new KeyParameter(this.K));
        final HMac hMac4 = this.hMac;
        final byte[] v4 = this.V;
        hMac4.update(v4, 0, v4.length);
        this.hMac.doFinal(this.V, 0);
    }
    
    @Override
    public void init(final BigInteger bigInteger, final SecureRandom secureRandom) {
        throw new IllegalStateException("Operation not supported");
    }
    
    @Override
    public boolean isDeterministic() {
        return true;
    }
    
    @Override
    public BigInteger nextK() {
        final byte[] array = new byte[BigIntegers.getUnsignedByteLength(this.n)];
        BigInteger bitsToInt;
        while (true) {
            int min;
            for (int i = 0; i < array.length; i += min) {
                final HMac hMac = this.hMac;
                final byte[] v = this.V;
                hMac.update(v, 0, v.length);
                this.hMac.doFinal(this.V, 0);
                min = Math.min(array.length - i, this.V.length);
                System.arraycopy(this.V, 0, array, i, min);
            }
            bitsToInt = this.bitsToInt(array);
            if (bitsToInt.compareTo(HMacDSAKCalculator.ZERO) > 0 && bitsToInt.compareTo(this.n) < 0) {
                break;
            }
            final HMac hMac2 = this.hMac;
            final byte[] v2 = this.V;
            hMac2.update(v2, 0, v2.length);
            this.hMac.update((byte)0);
            this.hMac.doFinal(this.K, 0);
            this.hMac.init(new KeyParameter(this.K));
            final HMac hMac3 = this.hMac;
            final byte[] v3 = this.V;
            hMac3.update(v3, 0, v3.length);
            this.hMac.doFinal(this.V, 0);
        }
        return bitsToInt;
    }
}
