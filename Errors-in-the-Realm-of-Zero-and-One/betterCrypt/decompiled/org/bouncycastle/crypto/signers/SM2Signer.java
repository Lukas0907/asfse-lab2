// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.signers;

import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.crypto.params.ParametersWithID;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.math.ec.FixedPointCombMultiplier;
import org.bouncycastle.math.ec.ECMultiplier;
import org.bouncycastle.math.ec.ECAlgorithms;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import java.math.BigInteger;
import org.bouncycastle.math.ec.ECFieldElement;
import org.bouncycastle.crypto.digests.SM3Digest;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECKeyParameters;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.math.ec.ECConstants;
import org.bouncycastle.crypto.Signer;

public class SM2Signer implements Signer, ECConstants
{
    private final Digest digest;
    private ECKeyParameters ecKey;
    private ECDomainParameters ecParams;
    private final DSAEncoding encoding;
    private final DSAKCalculator kCalculator;
    private ECPoint pubPoint;
    private byte[] z;
    
    public SM2Signer() {
        this(StandardDSAEncoding.INSTANCE, new SM3Digest());
    }
    
    public SM2Signer(final Digest digest) {
        this(StandardDSAEncoding.INSTANCE, digest);
    }
    
    public SM2Signer(final DSAEncoding encoding) {
        this.kCalculator = new RandomDSAKCalculator();
        this.encoding = encoding;
        this.digest = new SM3Digest();
    }
    
    public SM2Signer(final DSAEncoding encoding, final Digest digest) {
        this.kCalculator = new RandomDSAKCalculator();
        this.encoding = encoding;
        this.digest = digest;
    }
    
    private void addFieldElement(final Digest digest, final ECFieldElement ecFieldElement) {
        final byte[] encoded = ecFieldElement.getEncoded();
        digest.update(encoded, 0, encoded.length);
    }
    
    private void addUserID(final Digest digest, final byte[] array) {
        final int n = array.length * 8;
        digest.update((byte)(n >> 8 & 0xFF));
        digest.update((byte)(n & 0xFF));
        digest.update(array, 0, array.length);
    }
    
    private byte[] digestDoFinal() {
        final byte[] array = new byte[this.digest.getDigestSize()];
        this.digest.doFinal(array, 0);
        this.reset();
        return array;
    }
    
    private byte[] getZ(byte[] array) {
        this.digest.reset();
        this.addUserID(this.digest, array);
        this.addFieldElement(this.digest, this.ecParams.getCurve().getA());
        this.addFieldElement(this.digest, this.ecParams.getCurve().getB());
        this.addFieldElement(this.digest, this.ecParams.getG().getAffineXCoord());
        this.addFieldElement(this.digest, this.ecParams.getG().getAffineYCoord());
        this.addFieldElement(this.digest, this.pubPoint.getAffineXCoord());
        this.addFieldElement(this.digest, this.pubPoint.getAffineYCoord());
        array = new byte[this.digest.getDigestSize()];
        this.digest.doFinal(array, 0);
        return array;
    }
    
    private boolean verifySignature(final BigInteger x, final BigInteger val) {
        final BigInteger n = this.ecParams.getN();
        if (x.compareTo(SM2Signer.ONE) >= 0) {
            if (x.compareTo(n) >= 0) {
                return false;
            }
            if (val.compareTo(SM2Signer.ONE) >= 0) {
                if (val.compareTo(n) >= 0) {
                    return false;
                }
                final BigInteger calculateE = this.calculateE(n, this.digestDoFinal());
                final BigInteger mod = x.add(val).mod(n);
                if (mod.equals(SM2Signer.ZERO)) {
                    return false;
                }
                final ECPoint normalize = ECAlgorithms.sumOfTwoMultiplies(this.ecParams.getG(), val, ((ECPublicKeyParameters)this.ecKey).getQ(), mod).normalize();
                return !normalize.isInfinity() && calculateE.add(normalize.getAffineXCoord().toBigInteger()).mod(n).equals(x);
            }
        }
        return false;
    }
    
    protected BigInteger calculateE(final BigInteger bigInteger, final byte[] magnitude) {
        return new BigInteger(1, magnitude);
    }
    
    protected ECMultiplier createBasePointMultiplier() {
        return new FixedPointCombMultiplier();
    }
    
    @Override
    public byte[] generateSignature() throws CryptoException {
        final byte[] digestDoFinal = this.digestDoFinal();
        final BigInteger n = this.ecParams.getN();
        final BigInteger calculateE = this.calculateE(n, digestDoFinal);
        final BigInteger d = ((ECPrivateKeyParameters)this.ecKey).getD();
        final ECMultiplier basePointMultiplier = this.createBasePointMultiplier();
        BigInteger mod;
        BigInteger mod2;
        while (true) {
            final BigInteger nextK = this.kCalculator.nextK();
            mod = calculateE.add(basePointMultiplier.multiply(this.ecParams.getG(), nextK).normalize().getAffineXCoord().toBigInteger()).mod(n);
            if (!mod.equals(SM2Signer.ZERO) && !mod.add(nextK).equals(n)) {
                mod2 = d.add(SM2Signer.ONE).modInverse(n).multiply(nextK.subtract(mod.multiply(d)).mod(n)).mod(n);
                if (!mod2.equals(SM2Signer.ZERO)) {
                    break;
                }
                continue;
            }
        }
        try {
            return this.encoding.encode(this.ecParams.getN(), mod, mod2);
        }
        catch (Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("unable to encode signature: ");
            sb.append(ex.getMessage());
            throw new CryptoException(sb.toString(), ex);
        }
    }
    
    @Override
    public void init(final boolean b, final CipherParameters cipherParameters) {
        CipherParameters parameters;
        byte[] id;
        if (cipherParameters instanceof ParametersWithID) {
            final ParametersWithID parametersWithID = (ParametersWithID)cipherParameters;
            parameters = parametersWithID.getParameters();
            id = parametersWithID.getID();
            if (id.length >= 8192) {
                throw new IllegalArgumentException("SM2 user ID must be less than 2^16 bits long");
            }
        }
        else {
            final byte[] decodeStrict = Hex.decodeStrict("31323334353637383132333435363738");
            parameters = cipherParameters;
            id = decodeStrict;
        }
        ECPoint pubPoint;
        if (b) {
            if (parameters instanceof ParametersWithRandom) {
                final ParametersWithRandom parametersWithRandom = (ParametersWithRandom)parameters;
                this.ecKey = (ECKeyParameters)parametersWithRandom.getParameters();
                this.ecParams = this.ecKey.getParameters();
                this.kCalculator.init(this.ecParams.getN(), parametersWithRandom.getRandom());
            }
            else {
                this.ecKey = (ECKeyParameters)parameters;
                this.ecParams = this.ecKey.getParameters();
                this.kCalculator.init(this.ecParams.getN(), CryptoServicesRegistrar.getSecureRandom());
            }
            pubPoint = this.createBasePointMultiplier().multiply(this.ecParams.getG(), ((ECPrivateKeyParameters)this.ecKey).getD()).normalize();
        }
        else {
            this.ecKey = (ECKeyParameters)parameters;
            this.ecParams = this.ecKey.getParameters();
            pubPoint = ((ECPublicKeyParameters)this.ecKey).getQ();
        }
        this.pubPoint = pubPoint;
        this.z = this.getZ(id);
        final Digest digest = this.digest;
        final byte[] z = this.z;
        digest.update(z, 0, z.length);
    }
    
    @Override
    public void reset() {
        this.digest.reset();
        final byte[] z = this.z;
        if (z != null) {
            this.digest.update(z, 0, z.length);
        }
    }
    
    @Override
    public void update(final byte b) {
        this.digest.update(b);
    }
    
    @Override
    public void update(final byte[] array, final int n, final int n2) {
        this.digest.update(array, n, n2);
    }
    
    @Override
    public boolean verifySignature(final byte[] array) {
        try {
            final BigInteger[] decode = this.encoding.decode(this.ecParams.getN(), array);
            return this.verifySignature(decode[0], decode[1]);
        }
        catch (Exception ex) {
            return false;
        }
    }
}
