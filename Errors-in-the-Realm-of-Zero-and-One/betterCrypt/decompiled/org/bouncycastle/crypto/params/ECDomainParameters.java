// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.math.ec.ECAlgorithms;
import org.bouncycastle.util.Arrays;
import java.math.BigInteger;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.math.ec.ECConstants;

public class ECDomainParameters implements ECConstants
{
    private final ECPoint G;
    private final ECCurve curve;
    private final BigInteger h;
    private BigInteger hInv;
    private final BigInteger n;
    private final byte[] seed;
    
    public ECDomainParameters(final ECCurve ecCurve, final ECPoint ecPoint, final BigInteger bigInteger) {
        this(ecCurve, ecPoint, bigInteger, ECDomainParameters.ONE, null);
    }
    
    public ECDomainParameters(final ECCurve ecCurve, final ECPoint ecPoint, final BigInteger bigInteger, final BigInteger bigInteger2) {
        this(ecCurve, ecPoint, bigInteger, bigInteger2, null);
    }
    
    public ECDomainParameters(final ECCurve curve, final ECPoint ecPoint, final BigInteger n, final BigInteger h, final byte[] array) {
        this.hInv = null;
        if (curve == null) {
            throw new NullPointerException("curve");
        }
        if (n != null) {
            this.curve = curve;
            this.G = validatePublicPoint(curve, ecPoint);
            this.n = n;
            this.h = h;
            this.seed = Arrays.clone(array);
            return;
        }
        throw new NullPointerException("n");
    }
    
    static ECPoint validatePublicPoint(final ECCurve ecCurve, final ECPoint ecPoint) {
        if (ecPoint == null) {
            throw new NullPointerException("Point cannot be null");
        }
        final ECPoint normalize = ECAlgorithms.importPoint(ecCurve, ecPoint).normalize();
        if (normalize.isInfinity()) {
            throw new IllegalArgumentException("Point at infinity");
        }
        if (normalize.isValid()) {
            return normalize;
        }
        throw new IllegalArgumentException("Point not on curve");
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ECDomainParameters)) {
            return false;
        }
        final ECDomainParameters ecDomainParameters = (ECDomainParameters)o;
        return this.curve.equals(ecDomainParameters.curve) && this.G.equals(ecDomainParameters.G) && this.n.equals(ecDomainParameters.n);
    }
    
    public ECCurve getCurve() {
        return this.curve;
    }
    
    public ECPoint getG() {
        return this.G;
    }
    
    public BigInteger getH() {
        return this.h;
    }
    
    public BigInteger getHInv() {
        synchronized (this) {
            if (this.hInv == null) {
                this.hInv = this.h.modInverse(this.n);
            }
            return this.hInv;
        }
    }
    
    public BigInteger getN() {
        return this.n;
    }
    
    public byte[] getSeed() {
        return Arrays.clone(this.seed);
    }
    
    @Override
    public int hashCode() {
        return ((this.curve.hashCode() ^ 0x404) * 257 ^ this.G.hashCode()) * 257 ^ this.n.hashCode();
    }
    
    public BigInteger validatePrivateScalar(final BigInteger bigInteger) {
        if (bigInteger == null) {
            throw new NullPointerException("Scalar cannot be null");
        }
        if (bigInteger.compareTo(ECConstants.ONE) >= 0 && bigInteger.compareTo(this.getN()) < 0) {
            return bigInteger;
        }
        throw new IllegalArgumentException("Scalar is not in the interval [1, n - 1]");
    }
    
    public ECPoint validatePublicPoint(final ECPoint ecPoint) {
        return validatePublicPoint(this.getCurve(), ecPoint);
    }
}
