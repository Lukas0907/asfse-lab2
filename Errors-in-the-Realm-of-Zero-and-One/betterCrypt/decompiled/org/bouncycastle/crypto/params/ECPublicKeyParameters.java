// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.math.ec.ECPoint;

public class ECPublicKeyParameters extends ECKeyParameters
{
    private final ECPoint q;
    
    public ECPublicKeyParameters(final ECPoint ecPoint, final ECDomainParameters ecDomainParameters) {
        super(false, ecDomainParameters);
        this.q = ecDomainParameters.validatePublicPoint(ecPoint);
    }
    
    public ECPoint getQ() {
        return this.q;
    }
}
