// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.util.Arrays;
import java.io.IOException;
import java.io.EOFException;
import org.bouncycastle.util.io.Streams;
import java.io.InputStream;

public final class X448PublicKeyParameters extends AsymmetricKeyParameter
{
    public static final int KEY_SIZE = 56;
    private final byte[] data;
    
    public X448PublicKeyParameters(final InputStream inputStream) throws IOException {
        super(false);
        this.data = new byte[56];
        if (56 == Streams.readFully(inputStream, this.data)) {
            return;
        }
        throw new EOFException("EOF encountered in middle of X448 public key");
    }
    
    public X448PublicKeyParameters(final byte[] array, final int n) {
        super(false);
        System.arraycopy(array, n, this.data = new byte[56], 0, 56);
    }
    
    public void encode(final byte[] array, final int n) {
        System.arraycopy(this.data, 0, array, n, 56);
    }
    
    public byte[] getEncoded() {
        return Arrays.clone(this.data);
    }
}
