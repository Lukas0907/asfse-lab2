// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.util.Properties;
import java.math.BigInteger;

public class RSAKeyParameters extends AsymmetricKeyParameter
{
    private static final BigInteger ONE;
    private static final BigInteger SMALL_PRIMES_PRODUCT;
    private BigInteger exponent;
    private BigInteger modulus;
    
    static {
        SMALL_PRIMES_PRODUCT = new BigInteger("8138e8a0fcf3a4e84a771d40fd305d7f4aa59306d7251de54d98af8fe95729a1f73d893fa424cd2edc8636a6c3285e022b0e3866a565ae8108eed8591cd4fe8d2ce86165a978d719ebf647f362d33fca29cd179fb42401cbaf3df0c614056f9c8f3cfd51e474afb6bc6974f78db8aba8e9e517fded658591ab7502bd41849462f", 16);
        ONE = BigInteger.valueOf(1L);
    }
    
    public RSAKeyParameters(final boolean b, final BigInteger bigInteger, final BigInteger exponent) {
        super(b);
        if (!b && (exponent.intValue() & 0x1) == 0x0) {
            throw new IllegalArgumentException("RSA publicExponent is even");
        }
        this.modulus = this.validate(bigInteger);
        this.exponent = exponent;
    }
    
    private BigInteger validate(final BigInteger bigInteger) {
        if ((bigInteger.intValue() & 0x1) == 0x0) {
            throw new IllegalArgumentException("RSA modulus is even");
        }
        if (Properties.isOverrideSet("org.bouncycastle.rsa.allow_unsafe_mod")) {
            return bigInteger;
        }
        if (bigInteger.gcd(RSAKeyParameters.SMALL_PRIMES_PRODUCT).equals(RSAKeyParameters.ONE)) {
            return bigInteger;
        }
        throw new IllegalArgumentException("RSA modulus has a small prime factor");
    }
    
    public BigInteger getExponent() {
        return this.exponent;
    }
    
    public BigInteger getModulus() {
        return this.modulus;
    }
}
