// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import java.security.SecureRandom;
import org.bouncycastle.crypto.KeyGenerationParameters;

public class X448KeyGenerationParameters extends KeyGenerationParameters
{
    public X448KeyGenerationParameters(final SecureRandom secureRandom) {
        super(secureRandom, 448);
    }
}
