// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.crypto.CipherParameters;

public class XDHUPublicParameters implements CipherParameters
{
    private AsymmetricKeyParameter ephemeralPublicKey;
    private AsymmetricKeyParameter staticPublicKey;
    
    public XDHUPublicParameters(final AsymmetricKeyParameter staticPublicKey, final AsymmetricKeyParameter ephemeralPublicKey) {
        if (staticPublicKey == null) {
            throw new NullPointerException("staticPublicKey cannot be null");
        }
        if (!(staticPublicKey instanceof X448PublicKeyParameters) && !(staticPublicKey instanceof X25519PublicKeyParameters)) {
            throw new IllegalArgumentException("only X25519 and X448 paramaters can be used");
        }
        if (ephemeralPublicKey == null) {
            throw new NullPointerException("ephemeralPublicKey cannot be null");
        }
        if (staticPublicKey.getClass().isAssignableFrom(ephemeralPublicKey.getClass())) {
            this.staticPublicKey = staticPublicKey;
            this.ephemeralPublicKey = ephemeralPublicKey;
            return;
        }
        throw new IllegalArgumentException("static and ephemeral public keys have different domain parameters");
    }
    
    public AsymmetricKeyParameter getEphemeralPublicKey() {
        return this.ephemeralPublicKey;
    }
    
    public AsymmetricKeyParameter getStaticPublicKey() {
        return this.staticPublicKey;
    }
}
