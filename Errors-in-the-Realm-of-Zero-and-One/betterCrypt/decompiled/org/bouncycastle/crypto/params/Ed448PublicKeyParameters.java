// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.util.Arrays;
import java.io.IOException;
import java.io.EOFException;
import org.bouncycastle.util.io.Streams;
import java.io.InputStream;

public final class Ed448PublicKeyParameters extends AsymmetricKeyParameter
{
    public static final int KEY_SIZE = 57;
    private final byte[] data;
    
    public Ed448PublicKeyParameters(final InputStream inputStream) throws IOException {
        super(false);
        this.data = new byte[57];
        if (57 == Streams.readFully(inputStream, this.data)) {
            return;
        }
        throw new EOFException("EOF encountered in middle of Ed448 public key");
    }
    
    public Ed448PublicKeyParameters(final byte[] array, final int n) {
        super(false);
        System.arraycopy(array, n, this.data = new byte[57], 0, 57);
    }
    
    public void encode(final byte[] array, final int n) {
        System.arraycopy(this.data, 0, array, n, 57);
    }
    
    public byte[] getEncoded() {
        return Arrays.clone(this.data);
    }
}
