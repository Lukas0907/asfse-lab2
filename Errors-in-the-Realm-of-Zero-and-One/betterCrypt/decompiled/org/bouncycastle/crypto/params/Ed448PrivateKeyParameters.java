// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.math.ec.rfc8032.Ed448;
import java.security.SecureRandom;
import java.io.IOException;
import java.io.EOFException;
import org.bouncycastle.util.io.Streams;
import java.io.InputStream;

public final class Ed448PrivateKeyParameters extends AsymmetricKeyParameter
{
    public static final int KEY_SIZE = 57;
    public static final int SIGNATURE_SIZE = 114;
    private final byte[] data;
    
    public Ed448PrivateKeyParameters(final InputStream inputStream) throws IOException {
        super(true);
        this.data = new byte[57];
        if (57 == Streams.readFully(inputStream, this.data)) {
            return;
        }
        throw new EOFException("EOF encountered in middle of Ed448 private key");
    }
    
    public Ed448PrivateKeyParameters(final SecureRandom secureRandom) {
        super(true);
        Ed448.generatePrivateKey(secureRandom, this.data = new byte[57]);
    }
    
    public Ed448PrivateKeyParameters(final byte[] array, final int n) {
        super(true);
        System.arraycopy(array, n, this.data = new byte[57], 0, 57);
    }
    
    public void encode(final byte[] array, final int n) {
        System.arraycopy(this.data, 0, array, n, 57);
    }
    
    public Ed448PublicKeyParameters generatePublicKey() {
        final byte[] array = new byte[57];
        Ed448.generatePublicKey(this.data, 0, array, 0);
        return new Ed448PublicKeyParameters(array, 0);
    }
    
    public byte[] getEncoded() {
        return Arrays.clone(this.data);
    }
    
    public void sign(final int n, final Ed448PublicKeyParameters ed448PublicKeyParameters, final byte[] array, final byte[] array2, final int n2, final int n3, final byte[] array3, final int n4) {
        final byte[] array4 = new byte[57];
        if (ed448PublicKeyParameters == null) {
            Ed448.generatePublicKey(this.data, 0, array4, 0);
        }
        else {
            ed448PublicKeyParameters.encode(array4, 0);
        }
        if (n == 0) {
            Ed448.sign(this.data, 0, array4, 0, array, array2, n2, n3, array3, n4);
            return;
        }
        if (n != 1) {
            throw new IllegalArgumentException("algorithm");
        }
        if (64 == n3) {
            Ed448.signPrehash(this.data, 0, array4, 0, array, array2, n2, array3, n4);
            return;
        }
        throw new IllegalArgumentException("msgLen");
    }
}
