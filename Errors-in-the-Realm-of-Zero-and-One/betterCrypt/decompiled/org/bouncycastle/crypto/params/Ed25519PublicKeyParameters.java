// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.util.Arrays;
import java.io.IOException;
import java.io.EOFException;
import org.bouncycastle.util.io.Streams;
import java.io.InputStream;

public final class Ed25519PublicKeyParameters extends AsymmetricKeyParameter
{
    public static final int KEY_SIZE = 32;
    private final byte[] data;
    
    public Ed25519PublicKeyParameters(final InputStream inputStream) throws IOException {
        super(false);
        this.data = new byte[32];
        if (32 == Streams.readFully(inputStream, this.data)) {
            return;
        }
        throw new EOFException("EOF encountered in middle of Ed25519 public key");
    }
    
    public Ed25519PublicKeyParameters(final byte[] array, final int n) {
        super(false);
        System.arraycopy(array, n, this.data = new byte[32], 0, 32);
    }
    
    public void encode(final byte[] array, final int n) {
        System.arraycopy(this.data, 0, array, n, 32);
    }
    
    public byte[] getEncoded() {
        return Arrays.clone(this.data);
    }
}
