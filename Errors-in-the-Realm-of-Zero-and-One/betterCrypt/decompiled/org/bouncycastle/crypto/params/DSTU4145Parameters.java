// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.util.Arrays;

public class DSTU4145Parameters extends ECDomainParameters
{
    private final byte[] dke;
    
    public DSTU4145Parameters(final ECDomainParameters ecDomainParameters, final byte[] array) {
        super(ecDomainParameters.getCurve(), ecDomainParameters.getG(), ecDomainParameters.getN(), ecDomainParameters.getH(), ecDomainParameters.getSeed());
        this.dke = Arrays.clone(array);
    }
    
    public byte[] getDKE() {
        return Arrays.clone(this.dke);
    }
}
