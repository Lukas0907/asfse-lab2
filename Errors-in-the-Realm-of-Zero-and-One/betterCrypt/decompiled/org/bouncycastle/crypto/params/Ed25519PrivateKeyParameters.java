// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.math.ec.rfc8032.Ed25519;
import java.security.SecureRandom;
import java.io.IOException;
import java.io.EOFException;
import org.bouncycastle.util.io.Streams;
import java.io.InputStream;

public final class Ed25519PrivateKeyParameters extends AsymmetricKeyParameter
{
    public static final int KEY_SIZE = 32;
    public static final int SIGNATURE_SIZE = 64;
    private final byte[] data;
    
    public Ed25519PrivateKeyParameters(final InputStream inputStream) throws IOException {
        super(true);
        this.data = new byte[32];
        if (32 == Streams.readFully(inputStream, this.data)) {
            return;
        }
        throw new EOFException("EOF encountered in middle of Ed25519 private key");
    }
    
    public Ed25519PrivateKeyParameters(final SecureRandom secureRandom) {
        super(true);
        Ed25519.generatePrivateKey(secureRandom, this.data = new byte[32]);
    }
    
    public Ed25519PrivateKeyParameters(final byte[] array, final int n) {
        super(true);
        System.arraycopy(array, n, this.data = new byte[32], 0, 32);
    }
    
    public void encode(final byte[] array, final int n) {
        System.arraycopy(this.data, 0, array, n, 32);
    }
    
    public Ed25519PublicKeyParameters generatePublicKey() {
        final byte[] array = new byte[32];
        Ed25519.generatePublicKey(this.data, 0, array, 0);
        return new Ed25519PublicKeyParameters(array, 0);
    }
    
    public byte[] getEncoded() {
        return Arrays.clone(this.data);
    }
    
    public void sign(final int n, final Ed25519PublicKeyParameters ed25519PublicKeyParameters, final byte[] array, final byte[] array2, final int n2, final int n3, final byte[] array3, final int n4) {
        final byte[] array4 = new byte[32];
        if (ed25519PublicKeyParameters == null) {
            Ed25519.generatePublicKey(this.data, 0, array4, 0);
        }
        else {
            ed25519PublicKeyParameters.encode(array4, 0);
        }
        if (n != 0) {
            if (n == 1) {
                Ed25519.sign(this.data, 0, array4, 0, array, array2, n2, n3, array3, n4);
                return;
            }
            if (n != 2) {
                throw new IllegalArgumentException("algorithm");
            }
            if (64 == n3) {
                Ed25519.signPrehash(this.data, 0, array4, 0, array, array2, n2, array3, n4);
                return;
            }
            throw new IllegalArgumentException("msgLen");
        }
        else {
            if (array == null) {
                Ed25519.sign(this.data, 0, array4, 0, array2, n2, n3, array3, n4);
                return;
            }
            throw new IllegalArgumentException("ctx");
        }
    }
}
