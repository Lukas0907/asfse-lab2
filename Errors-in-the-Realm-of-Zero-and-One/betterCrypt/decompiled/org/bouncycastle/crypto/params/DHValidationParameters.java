// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.util.Arrays;

public class DHValidationParameters
{
    private int counter;
    private byte[] seed;
    
    public DHValidationParameters(final byte[] array, final int counter) {
        this.seed = Arrays.clone(array);
        this.counter = counter;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof DHValidationParameters)) {
            return false;
        }
        final DHValidationParameters dhValidationParameters = (DHValidationParameters)o;
        return dhValidationParameters.counter == this.counter && Arrays.areEqual(this.seed, dhValidationParameters.seed);
    }
    
    public int getCounter() {
        return this.counter;
    }
    
    public byte[] getSeed() {
        return Arrays.clone(this.seed);
    }
    
    @Override
    public int hashCode() {
        return this.counter ^ Arrays.hashCode(this.seed);
    }
}
