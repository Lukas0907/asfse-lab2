// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.CipherParameters;

public class IESParameters implements CipherParameters
{
    private byte[] derivation;
    private byte[] encoding;
    private int macKeySize;
    
    public IESParameters(final byte[] array, final byte[] array2, final int macKeySize) {
        this.derivation = Arrays.clone(array);
        this.encoding = Arrays.clone(array2);
        this.macKeySize = macKeySize;
    }
    
    public byte[] getDerivationV() {
        return Arrays.clone(this.derivation);
    }
    
    public byte[] getEncodingV() {
        return Arrays.clone(this.encoding);
    }
    
    public int getMacKeySize() {
        return this.macKeySize;
    }
}
