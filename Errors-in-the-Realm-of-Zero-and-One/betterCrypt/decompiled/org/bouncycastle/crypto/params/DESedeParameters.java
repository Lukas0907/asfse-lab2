// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

public class DESedeParameters extends DESParameters
{
    public static final int DES_EDE_KEY_LENGTH = 24;
    
    public DESedeParameters(final byte[] array) {
        super(array);
        if (!isWeakKey(array, 0, array.length)) {
            return;
        }
        throw new IllegalArgumentException("attempt to create weak DESede key");
    }
    
    public static boolean isReal2Key(final byte[] array, final int n) {
        boolean b = false;
        for (int i = n; i != n + 8; ++i) {
            if (array[i] != array[i + 8]) {
                b = true;
            }
        }
        return b;
    }
    
    public static boolean isReal3Key(final byte[] array, final int n) {
        final boolean b = false;
        int n2 = 0;
        int n4;
        int n3 = n4 = n2;
        int n5 = n;
        while (true) {
            final int n6 = 1;
            if (n5 == n + 8) {
                break;
            }
            final byte b2 = array[n5];
            final int n7 = n5 + 8;
            final boolean b3 = (n2 | ((b2 != array[n7]) ? 1 : 0)) != 0x0;
            final byte b4 = array[n5];
            final int n8 = n5 + 16;
            final boolean b5 = (n4 | ((b4 != array[n8]) ? 1 : 0)) != 0x0;
            int n9;
            if (array[n7] != array[n8]) {
                n9 = n6;
            }
            else {
                n9 = 0;
            }
            n3 |= n9;
            ++n5;
            n2 = (b3 ? 1 : 0);
            n4 = (b5 ? 1 : 0);
        }
        boolean b6 = b;
        if (n2 != 0) {
            b6 = b;
            if (n4 != 0) {
                b6 = b;
                if (n3 != 0) {
                    b6 = true;
                }
            }
        }
        return b6;
    }
    
    public static boolean isRealEDEKey(final byte[] array, final int n) {
        if (array.length == 16) {
            return isReal2Key(array, n);
        }
        return isReal3Key(array, n);
    }
    
    public static boolean isWeakKey(final byte[] array, final int n) {
        return isWeakKey(array, n, array.length - n);
    }
    
    public static boolean isWeakKey(final byte[] array, int i, final int n) {
        while (i < n) {
            if (DESParameters.isWeakKey(array, i)) {
                return true;
            }
            i += 8;
        }
        return false;
    }
}
