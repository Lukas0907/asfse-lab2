// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;

public class ECGOST3410Parameters extends ECNamedDomainParameters
{
    private final ASN1ObjectIdentifier digestParamSet;
    private final ASN1ObjectIdentifier encryptionParamSet;
    private final ASN1ObjectIdentifier publicKeyParamSet;
    
    public ECGOST3410Parameters(final ECDomainParameters ecDomainParameters, final ASN1ObjectIdentifier asn1ObjectIdentifier, final ASN1ObjectIdentifier asn1ObjectIdentifier2) {
        this(ecDomainParameters, asn1ObjectIdentifier, asn1ObjectIdentifier2, null);
    }
    
    public ECGOST3410Parameters(final ECDomainParameters ecDomainParameters, final ASN1ObjectIdentifier publicKeyParamSet, final ASN1ObjectIdentifier digestParamSet, final ASN1ObjectIdentifier encryptionParamSet) {
        super(publicKeyParamSet, ecDomainParameters.getCurve(), ecDomainParameters.getG(), ecDomainParameters.getN(), ecDomainParameters.getH(), ecDomainParameters.getSeed());
        if (ecDomainParameters instanceof ECNamedDomainParameters && !publicKeyParamSet.equals(((ECNamedDomainParameters)ecDomainParameters).getName())) {
            throw new IllegalArgumentException("named parameters do not match publicKeyParamSet value");
        }
        this.publicKeyParamSet = publicKeyParamSet;
        this.digestParamSet = digestParamSet;
        this.encryptionParamSet = encryptionParamSet;
    }
    
    public ASN1ObjectIdentifier getDigestParamSet() {
        return this.digestParamSet;
    }
    
    public ASN1ObjectIdentifier getEncryptionParamSet() {
        return this.encryptionParamSet;
    }
    
    public ASN1ObjectIdentifier getPublicKeyParamSet() {
        return this.publicKeyParamSet;
    }
}
