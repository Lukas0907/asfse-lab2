// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.crypto.CipherParameters;

public class ParametersWithSalt implements CipherParameters
{
    private CipherParameters parameters;
    private byte[] salt;
    
    public ParametersWithSalt(final CipherParameters cipherParameters, final byte[] array) {
        this(cipherParameters, array, 0, array.length);
    }
    
    public ParametersWithSalt(final CipherParameters parameters, final byte[] array, final int n, final int n2) {
        this.salt = new byte[n2];
        this.parameters = parameters;
        System.arraycopy(array, n, this.salt, 0, n2);
    }
    
    public CipherParameters getParameters() {
        return this.parameters;
    }
    
    public byte[] getSalt() {
        return this.salt;
    }
}
