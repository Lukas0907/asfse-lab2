// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.crypto.CipherParameters;

public class DHMQVPrivateParameters implements CipherParameters
{
    private DHPrivateKeyParameters ephemeralPrivateKey;
    private DHPublicKeyParameters ephemeralPublicKey;
    private DHPrivateKeyParameters staticPrivateKey;
    
    public DHMQVPrivateParameters(final DHPrivateKeyParameters dhPrivateKeyParameters, final DHPrivateKeyParameters dhPrivateKeyParameters2) {
        this(dhPrivateKeyParameters, dhPrivateKeyParameters2, null);
    }
    
    public DHMQVPrivateParameters(final DHPrivateKeyParameters staticPrivateKey, final DHPrivateKeyParameters ephemeralPrivateKey, DHPublicKeyParameters ephemeralPublicKey) {
        if (staticPrivateKey == null) {
            throw new NullPointerException("staticPrivateKey cannot be null");
        }
        if (ephemeralPrivateKey == null) {
            throw new NullPointerException("ephemeralPrivateKey cannot be null");
        }
        final DHParameters parameters = staticPrivateKey.getParameters();
        if (parameters.equals(ephemeralPrivateKey.getParameters())) {
            if (ephemeralPublicKey == null) {
                ephemeralPublicKey = new DHPublicKeyParameters(parameters.getG().multiply(ephemeralPrivateKey.getX()), parameters);
            }
            else if (!parameters.equals(ephemeralPublicKey.getParameters())) {
                throw new IllegalArgumentException("Ephemeral public key has different domain parameters");
            }
            this.staticPrivateKey = staticPrivateKey;
            this.ephemeralPrivateKey = ephemeralPrivateKey;
            this.ephemeralPublicKey = ephemeralPublicKey;
            return;
        }
        throw new IllegalArgumentException("Static and ephemeral private keys have different domain parameters");
    }
    
    public DHPrivateKeyParameters getEphemeralPrivateKey() {
        return this.ephemeralPrivateKey;
    }
    
    public DHPublicKeyParameters getEphemeralPublicKey() {
        return this.ephemeralPublicKey;
    }
    
    public DHPrivateKeyParameters getStaticPrivateKey() {
        return this.staticPrivateKey;
    }
}
