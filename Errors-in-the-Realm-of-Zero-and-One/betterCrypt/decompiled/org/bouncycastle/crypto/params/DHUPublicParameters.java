// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.crypto.CipherParameters;

public class DHUPublicParameters implements CipherParameters
{
    private DHPublicKeyParameters ephemeralPublicKey;
    private DHPublicKeyParameters staticPublicKey;
    
    public DHUPublicParameters(final DHPublicKeyParameters staticPublicKey, final DHPublicKeyParameters ephemeralPublicKey) {
        if (staticPublicKey == null) {
            throw new NullPointerException("staticPublicKey cannot be null");
        }
        if (ephemeralPublicKey == null) {
            throw new NullPointerException("ephemeralPublicKey cannot be null");
        }
        if (staticPublicKey.getParameters().equals(ephemeralPublicKey.getParameters())) {
            this.staticPublicKey = staticPublicKey;
            this.ephemeralPublicKey = ephemeralPublicKey;
            return;
        }
        throw new IllegalArgumentException("Static and ephemeral public keys have different domain parameters");
    }
    
    public DHPublicKeyParameters getEphemeralPublicKey() {
        return this.ephemeralPublicKey;
    }
    
    public DHPublicKeyParameters getStaticPublicKey() {
        return this.staticPublicKey;
    }
}
