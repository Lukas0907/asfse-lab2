// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.crypto.CipherParameters;

public class ParametersWithUKM implements CipherParameters
{
    private CipherParameters parameters;
    private byte[] ukm;
    
    public ParametersWithUKM(final CipherParameters cipherParameters, final byte[] array) {
        this(cipherParameters, array, 0, array.length);
    }
    
    public ParametersWithUKM(final CipherParameters parameters, final byte[] array, final int n, final int n2) {
        this.ukm = new byte[n2];
        this.parameters = parameters;
        System.arraycopy(array, n, this.ukm, 0, n2);
    }
    
    public CipherParameters getParameters() {
        return this.parameters;
    }
    
    public byte[] getUKM() {
        return this.ukm;
    }
}
