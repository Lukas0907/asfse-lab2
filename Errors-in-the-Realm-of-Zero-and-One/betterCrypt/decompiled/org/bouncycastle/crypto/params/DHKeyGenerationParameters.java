// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import java.security.SecureRandom;
import org.bouncycastle.crypto.KeyGenerationParameters;

public class DHKeyGenerationParameters extends KeyGenerationParameters
{
    private DHParameters params;
    
    public DHKeyGenerationParameters(final SecureRandom secureRandom, final DHParameters params) {
        super(secureRandom, getStrength(params));
        this.params = params;
    }
    
    static int getStrength(final DHParameters dhParameters) {
        if (dhParameters.getL() != 0) {
            return dhParameters.getL();
        }
        return dhParameters.getP().bitLength();
    }
    
    public DHParameters getParameters() {
        return this.params;
    }
}
