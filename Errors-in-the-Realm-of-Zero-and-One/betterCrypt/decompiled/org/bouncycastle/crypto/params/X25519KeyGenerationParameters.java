// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import java.security.SecureRandom;
import org.bouncycastle.crypto.KeyGenerationParameters;

public class X25519KeyGenerationParameters extends KeyGenerationParameters
{
    public X25519KeyGenerationParameters(final SecureRandom secureRandom) {
        super(secureRandom, 255);
    }
}
