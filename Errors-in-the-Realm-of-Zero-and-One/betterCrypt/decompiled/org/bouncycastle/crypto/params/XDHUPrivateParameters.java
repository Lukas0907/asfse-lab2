// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.crypto.CipherParameters;

public class XDHUPrivateParameters implements CipherParameters
{
    private AsymmetricKeyParameter ephemeralPrivateKey;
    private AsymmetricKeyParameter ephemeralPublicKey;
    private AsymmetricKeyParameter staticPrivateKey;
    
    public XDHUPrivateParameters(final AsymmetricKeyParameter asymmetricKeyParameter, final AsymmetricKeyParameter asymmetricKeyParameter2) {
        this(asymmetricKeyParameter, asymmetricKeyParameter2, null);
    }
    
    public XDHUPrivateParameters(final AsymmetricKeyParameter staticPrivateKey, final AsymmetricKeyParameter ephemeralPrivateKey, final AsymmetricKeyParameter asymmetricKeyParameter) {
        if (staticPrivateKey == null) {
            throw new NullPointerException("staticPrivateKey cannot be null");
        }
        final boolean b = staticPrivateKey instanceof X448PrivateKeyParameters;
        if (!b && !(staticPrivateKey instanceof X25519PrivateKeyParameters)) {
            throw new IllegalArgumentException("only X25519 and X448 paramaters can be used");
        }
        if (ephemeralPrivateKey == null) {
            throw new NullPointerException("ephemeralPrivateKey cannot be null");
        }
        if (staticPrivateKey.getClass().isAssignableFrom(ephemeralPrivateKey.getClass())) {
            AsymmetricKeyParameter ephemeralPublicKey;
            if (asymmetricKeyParameter == null) {
                if (ephemeralPrivateKey instanceof X448PrivateKeyParameters) {
                    ephemeralPublicKey = ((X448PrivateKeyParameters)ephemeralPrivateKey).generatePublicKey();
                }
                else {
                    ephemeralPublicKey = ((X25519PrivateKeyParameters)ephemeralPrivateKey).generatePublicKey();
                }
            }
            else {
                if (asymmetricKeyParameter instanceof X448PublicKeyParameters && !b) {
                    throw new IllegalArgumentException("ephemeral public key has different domain parameters");
                }
                ephemeralPublicKey = asymmetricKeyParameter;
                if (asymmetricKeyParameter instanceof X25519PublicKeyParameters) {
                    if (!(staticPrivateKey instanceof X25519PrivateKeyParameters)) {
                        throw new IllegalArgumentException("ephemeral public key has different domain parameters");
                    }
                    ephemeralPublicKey = asymmetricKeyParameter;
                }
            }
            this.staticPrivateKey = staticPrivateKey;
            this.ephemeralPrivateKey = ephemeralPrivateKey;
            this.ephemeralPublicKey = ephemeralPublicKey;
            return;
        }
        throw new IllegalArgumentException("static and ephemeral private keys have different domain parameters");
    }
    
    public AsymmetricKeyParameter getEphemeralPrivateKey() {
        return this.ephemeralPrivateKey;
    }
    
    public AsymmetricKeyParameter getEphemeralPublicKey() {
        return this.ephemeralPublicKey;
    }
    
    public AsymmetricKeyParameter getStaticPrivateKey() {
        return this.staticPrivateKey;
    }
}
