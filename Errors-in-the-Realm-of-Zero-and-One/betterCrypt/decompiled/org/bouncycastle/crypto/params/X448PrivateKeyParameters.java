// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.math.ec.rfc7748.X448;
import java.security.SecureRandom;
import java.io.IOException;
import java.io.EOFException;
import org.bouncycastle.util.io.Streams;
import java.io.InputStream;

public final class X448PrivateKeyParameters extends AsymmetricKeyParameter
{
    public static final int KEY_SIZE = 56;
    public static final int SECRET_SIZE = 56;
    private final byte[] data;
    
    public X448PrivateKeyParameters(final InputStream inputStream) throws IOException {
        super(true);
        this.data = new byte[56];
        if (56 == Streams.readFully(inputStream, this.data)) {
            return;
        }
        throw new EOFException("EOF encountered in middle of X448 private key");
    }
    
    public X448PrivateKeyParameters(final SecureRandom secureRandom) {
        super(true);
        X448.generatePrivateKey(secureRandom, this.data = new byte[56]);
    }
    
    public X448PrivateKeyParameters(final byte[] array, final int n) {
        super(true);
        System.arraycopy(array, n, this.data = new byte[56], 0, 56);
    }
    
    public void encode(final byte[] array, final int n) {
        System.arraycopy(this.data, 0, array, n, 56);
    }
    
    public X448PublicKeyParameters generatePublicKey() {
        final byte[] array = new byte[56];
        X448.generatePublicKey(this.data, 0, array, 0);
        return new X448PublicKeyParameters(array, 0);
    }
    
    public void generateSecret(final X448PublicKeyParameters x448PublicKeyParameters, final byte[] array, final int n) {
        final byte[] array2 = new byte[56];
        x448PublicKeyParameters.encode(array2, 0);
        if (X448.calculateAgreement(this.data, 0, array2, 0, array, n)) {
            return;
        }
        throw new IllegalStateException("X448 agreement failed");
    }
    
    public byte[] getEncoded() {
        return Arrays.clone(this.data);
    }
}
