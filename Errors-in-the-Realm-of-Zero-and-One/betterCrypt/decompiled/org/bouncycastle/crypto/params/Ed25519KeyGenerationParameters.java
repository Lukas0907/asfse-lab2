// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import java.security.SecureRandom;
import org.bouncycastle.crypto.KeyGenerationParameters;

public class Ed25519KeyGenerationParameters extends KeyGenerationParameters
{
    public Ed25519KeyGenerationParameters(final SecureRandom secureRandom) {
        super(secureRandom, 256);
    }
}
