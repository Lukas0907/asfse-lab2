// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.crypto.CryptoServicesRegistrar;
import java.security.SecureRandom;
import org.bouncycastle.crypto.CipherParameters;

public class ParametersWithRandom implements CipherParameters
{
    private CipherParameters parameters;
    private SecureRandom random;
    
    public ParametersWithRandom(final CipherParameters cipherParameters) {
        this(cipherParameters, CryptoServicesRegistrar.getSecureRandom());
    }
    
    public ParametersWithRandom(final CipherParameters parameters, final SecureRandom random) {
        this.random = random;
        this.parameters = parameters;
    }
    
    public CipherParameters getParameters() {
        return this.parameters;
    }
    
    public SecureRandom getRandom() {
        return this.random;
    }
}
