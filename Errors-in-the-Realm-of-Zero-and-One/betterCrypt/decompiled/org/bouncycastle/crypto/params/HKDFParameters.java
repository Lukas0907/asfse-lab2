// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.DerivationParameters;

public class HKDFParameters implements DerivationParameters
{
    private final byte[] ikm;
    private final byte[] info;
    private final byte[] salt;
    private final boolean skipExpand;
    
    private HKDFParameters(final byte[] array, final boolean skipExpand, final byte[] array2, final byte[] array3) {
        if (array == null) {
            throw new IllegalArgumentException("IKM (input keying material) should not be null");
        }
        this.ikm = Arrays.clone(array);
        this.skipExpand = skipExpand;
        if (array2 != null && array2.length != 0) {
            this.salt = Arrays.clone(array2);
        }
        else {
            this.salt = null;
        }
        if (array3 == null) {
            this.info = new byte[0];
            return;
        }
        this.info = Arrays.clone(array3);
    }
    
    public HKDFParameters(final byte[] array, final byte[] array2, final byte[] array3) {
        this(array, false, array2, array3);
    }
    
    public static HKDFParameters defaultParameters(final byte[] array) {
        return new HKDFParameters(array, false, null, null);
    }
    
    public static HKDFParameters skipExtractParameters(final byte[] array, final byte[] array2) {
        return new HKDFParameters(array, true, null, array2);
    }
    
    public byte[] getIKM() {
        return Arrays.clone(this.ikm);
    }
    
    public byte[] getInfo() {
        return Arrays.clone(this.info);
    }
    
    public byte[] getSalt() {
        return Arrays.clone(this.salt);
    }
    
    public boolean skipExtract() {
        return this.skipExpand;
    }
}
