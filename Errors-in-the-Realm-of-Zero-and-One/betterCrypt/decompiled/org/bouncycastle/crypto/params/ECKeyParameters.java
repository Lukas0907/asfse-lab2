// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

public class ECKeyParameters extends AsymmetricKeyParameter
{
    private final ECDomainParameters parameters;
    
    protected ECKeyParameters(final boolean b, final ECDomainParameters parameters) {
        super(b);
        if (parameters != null) {
            this.parameters = parameters;
            return;
        }
        throw new NullPointerException("'parameters' cannot be null");
    }
    
    public ECDomainParameters getParameters() {
        return this.parameters;
    }
}
