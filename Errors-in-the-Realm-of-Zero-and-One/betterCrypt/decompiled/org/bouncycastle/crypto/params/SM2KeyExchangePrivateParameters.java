// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.math.ec.FixedPointCombMultiplier;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.crypto.CipherParameters;

public class SM2KeyExchangePrivateParameters implements CipherParameters
{
    private final ECPrivateKeyParameters ephemeralPrivateKey;
    private final ECPoint ephemeralPublicPoint;
    private final boolean initiator;
    private final ECPrivateKeyParameters staticPrivateKey;
    private final ECPoint staticPublicPoint;
    
    public SM2KeyExchangePrivateParameters(final boolean initiator, final ECPrivateKeyParameters staticPrivateKey, final ECPrivateKeyParameters ephemeralPrivateKey) {
        if (staticPrivateKey == null) {
            throw new NullPointerException("staticPrivateKey cannot be null");
        }
        if (ephemeralPrivateKey == null) {
            throw new NullPointerException("ephemeralPrivateKey cannot be null");
        }
        final ECDomainParameters parameters = staticPrivateKey.getParameters();
        if (parameters.equals(ephemeralPrivateKey.getParameters())) {
            final FixedPointCombMultiplier fixedPointCombMultiplier = new FixedPointCombMultiplier();
            this.initiator = initiator;
            this.staticPrivateKey = staticPrivateKey;
            this.staticPublicPoint = fixedPointCombMultiplier.multiply(parameters.getG(), staticPrivateKey.getD()).normalize();
            this.ephemeralPrivateKey = ephemeralPrivateKey;
            this.ephemeralPublicPoint = fixedPointCombMultiplier.multiply(parameters.getG(), ephemeralPrivateKey.getD()).normalize();
            return;
        }
        throw new IllegalArgumentException("Static and ephemeral private keys have different domain parameters");
    }
    
    public ECPrivateKeyParameters getEphemeralPrivateKey() {
        return this.ephemeralPrivateKey;
    }
    
    public ECPoint getEphemeralPublicPoint() {
        return this.ephemeralPublicPoint;
    }
    
    public ECPrivateKeyParameters getStaticPrivateKey() {
        return this.staticPrivateKey;
    }
    
    public ECPoint getStaticPublicPoint() {
        return this.staticPublicPoint;
    }
    
    public boolean isInitiator() {
        return this.initiator;
    }
}
