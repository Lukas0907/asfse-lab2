// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import java.math.BigInteger;

public class ECPrivateKeyParameters extends ECKeyParameters
{
    private final BigInteger d;
    
    public ECPrivateKeyParameters(final BigInteger bigInteger, final ECDomainParameters ecDomainParameters) {
        super(true, ecDomainParameters);
        this.d = ecDomainParameters.validatePrivateScalar(bigInteger);
    }
    
    public BigInteger getD() {
        return this.d;
    }
}
