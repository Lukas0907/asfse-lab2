// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import java.math.BigInteger;
import org.bouncycastle.crypto.CipherParameters;

public class RSABlindingParameters implements CipherParameters
{
    private BigInteger blindingFactor;
    private RSAKeyParameters publicKey;
    
    public RSABlindingParameters(final RSAKeyParameters publicKey, final BigInteger blindingFactor) {
        if (!(publicKey instanceof RSAPrivateCrtKeyParameters)) {
            this.publicKey = publicKey;
            this.blindingFactor = blindingFactor;
            return;
        }
        throw new IllegalArgumentException("RSA parameters should be for a public key");
    }
    
    public BigInteger getBlindingFactor() {
        return this.blindingFactor;
    }
    
    public RSAKeyParameters getPublicKey() {
        return this.publicKey;
    }
}
