// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.CipherParameters;

public class AEADParameters implements CipherParameters
{
    private byte[] associatedText;
    private KeyParameter key;
    private int macSize;
    private byte[] nonce;
    
    public AEADParameters(final KeyParameter keyParameter, final int n, final byte[] array) {
        this(keyParameter, n, array, null);
    }
    
    public AEADParameters(final KeyParameter key, final int macSize, final byte[] array, final byte[] array2) {
        this.key = key;
        this.nonce = Arrays.clone(array);
        this.macSize = macSize;
        this.associatedText = Arrays.clone(array2);
    }
    
    public byte[] getAssociatedText() {
        return Arrays.clone(this.associatedText);
    }
    
    public KeyParameter getKey() {
        return this.key;
    }
    
    public int getMacSize() {
        return this.macSize;
    }
    
    public byte[] getNonce() {
        return Arrays.clone(this.nonce);
    }
}
