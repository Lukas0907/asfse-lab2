// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.crypto.PasswordConverter;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.CharToByteConverter;

public class Argon2Parameters
{
    public static final int ARGON2_VERSION_10 = 16;
    public static final int ARGON2_VERSION_13 = 19;
    public static final int ARGON2_d = 0;
    public static final int ARGON2_i = 1;
    public static final int ARGON2_id = 2;
    private static final int DEFAULT_ITERATIONS = 3;
    private static final int DEFAULT_LANES = 1;
    private static final int DEFAULT_MEMORY_COST = 12;
    private static final int DEFAULT_TYPE = 1;
    private static final int DEFAULT_VERSION = 19;
    private final byte[] additional;
    private final CharToByteConverter converter;
    private final int iterations;
    private final int lanes;
    private final int memory;
    private final byte[] salt;
    private final byte[] secret;
    private final int type;
    private final int version;
    
    private Argon2Parameters(final int type, final byte[] array, final byte[] array2, final byte[] array3, final int iterations, final int memory, final int lanes, final int version, final CharToByteConverter converter) {
        this.salt = Arrays.clone(array);
        this.secret = Arrays.clone(array2);
        this.additional = Arrays.clone(array3);
        this.iterations = iterations;
        this.memory = memory;
        this.lanes = lanes;
        this.version = version;
        this.type = type;
        this.converter = converter;
    }
    
    public void clear() {
        Arrays.clear(this.salt);
        Arrays.clear(this.secret);
        Arrays.clear(this.additional);
    }
    
    public byte[] getAdditional() {
        return Arrays.clone(this.additional);
    }
    
    public CharToByteConverter getCharToByteConverter() {
        return this.converter;
    }
    
    public int getIterations() {
        return this.iterations;
    }
    
    public int getLanes() {
        return this.lanes;
    }
    
    public int getMemory() {
        return this.memory;
    }
    
    public byte[] getSalt() {
        return Arrays.clone(this.salt);
    }
    
    public byte[] getSecret() {
        return Arrays.clone(this.secret);
    }
    
    public int getType() {
        return this.type;
    }
    
    public int getVersion() {
        return this.version;
    }
    
    public static class Builder
    {
        private byte[] additional;
        private CharToByteConverter converter;
        private int iterations;
        private int lanes;
        private int memory;
        private byte[] salt;
        private byte[] secret;
        private final int type;
        private int version;
        
        public Builder() {
            this(1);
        }
        
        public Builder(final int type) {
            this.converter = PasswordConverter.UTF8;
            this.type = type;
            this.lanes = 1;
            this.memory = 4096;
            this.iterations = 3;
            this.version = 19;
        }
        
        public Argon2Parameters build() {
            return new Argon2Parameters(this.type, this.salt, this.secret, this.additional, this.iterations, this.memory, this.lanes, this.version, this.converter, null);
        }
        
        public void clear() {
            Arrays.clear(this.salt);
            Arrays.clear(this.secret);
            Arrays.clear(this.additional);
        }
        
        public Builder withAdditional(final byte[] array) {
            this.additional = Arrays.clone(array);
            return this;
        }
        
        public Builder withCharToByteConverter(final CharToByteConverter converter) {
            this.converter = converter;
            return this;
        }
        
        public Builder withIterations(final int iterations) {
            this.iterations = iterations;
            return this;
        }
        
        public Builder withMemoryAsKB(final int memory) {
            this.memory = memory;
            return this;
        }
        
        public Builder withMemoryPowOfTwo(final int n) {
            this.memory = 1 << n;
            return this;
        }
        
        public Builder withParallelism(final int lanes) {
            this.lanes = lanes;
            return this;
        }
        
        public Builder withSalt(final byte[] array) {
            this.salt = Arrays.clone(array);
            return this;
        }
        
        public Builder withSecret(final byte[] array) {
            this.secret = Arrays.clone(array);
            return this;
        }
        
        public Builder withVersion(final int version) {
            this.version = version;
            return this;
        }
    }
}
