// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.params;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.math.ec.rfc7748.X25519;
import java.security.SecureRandom;
import java.io.IOException;
import java.io.EOFException;
import org.bouncycastle.util.io.Streams;
import java.io.InputStream;

public final class X25519PrivateKeyParameters extends AsymmetricKeyParameter
{
    public static final int KEY_SIZE = 32;
    public static final int SECRET_SIZE = 32;
    private final byte[] data;
    
    public X25519PrivateKeyParameters(final InputStream inputStream) throws IOException {
        super(true);
        this.data = new byte[32];
        if (32 == Streams.readFully(inputStream, this.data)) {
            return;
        }
        throw new EOFException("EOF encountered in middle of X25519 private key");
    }
    
    public X25519PrivateKeyParameters(final SecureRandom secureRandom) {
        super(true);
        X25519.generatePrivateKey(secureRandom, this.data = new byte[32]);
    }
    
    public X25519PrivateKeyParameters(final byte[] array, final int n) {
        super(true);
        System.arraycopy(array, n, this.data = new byte[32], 0, 32);
    }
    
    public void encode(final byte[] array, final int n) {
        System.arraycopy(this.data, 0, array, n, 32);
    }
    
    public X25519PublicKeyParameters generatePublicKey() {
        final byte[] array = new byte[32];
        X25519.generatePublicKey(this.data, 0, array, 0);
        return new X25519PublicKeyParameters(array, 0);
    }
    
    public void generateSecret(final X25519PublicKeyParameters x25519PublicKeyParameters, final byte[] array, final int n) {
        final byte[] array2 = new byte[32];
        x25519PublicKeyParameters.encode(array2, 0);
        if (X25519.calculateAgreement(this.data, 0, array2, 0, array, n)) {
            return;
        }
        throw new IllegalStateException("X25519 agreement failed");
    }
    
    public byte[] getEncoded() {
        return Arrays.clone(this.data);
    }
}
