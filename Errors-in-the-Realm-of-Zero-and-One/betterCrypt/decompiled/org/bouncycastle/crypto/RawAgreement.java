// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto;

public interface RawAgreement
{
    void calculateAgreement(final CipherParameters p0, final byte[] p1, final int p2);
    
    int getAgreementSize();
    
    void init(final CipherParameters p0);
}
