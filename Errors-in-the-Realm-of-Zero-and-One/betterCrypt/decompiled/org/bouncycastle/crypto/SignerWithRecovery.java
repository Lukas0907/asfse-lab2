// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto;

public interface SignerWithRecovery extends Signer
{
    byte[] getRecoveredMessage();
    
    boolean hasFullMessage();
    
    void updateWithRecoveredMessage(final byte[] p0) throws InvalidCipherTextException;
}
