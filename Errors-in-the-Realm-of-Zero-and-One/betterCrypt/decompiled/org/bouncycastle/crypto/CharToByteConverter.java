// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto;

public interface CharToByteConverter
{
    byte[] convert(final char[] p0);
    
    String getType();
}
