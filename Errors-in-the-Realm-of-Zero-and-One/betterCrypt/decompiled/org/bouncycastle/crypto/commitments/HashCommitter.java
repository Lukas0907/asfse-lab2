// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.commitments;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.Commitment;
import org.bouncycastle.crypto.ExtendedDigest;
import java.security.SecureRandom;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.Committer;

public class HashCommitter implements Committer
{
    private final int byteLength;
    private final Digest digest;
    private final SecureRandom random;
    
    public HashCommitter(final ExtendedDigest digest, final SecureRandom random) {
        this.digest = digest;
        this.byteLength = digest.getByteLength();
        this.random = random;
    }
    
    private byte[] calculateCommitment(final byte[] array, final byte[] array2) {
        final byte[] array3 = new byte[this.digest.getDigestSize()];
        this.digest.update(array, 0, array.length);
        this.digest.update(array2, 0, array2.length);
        this.digest.doFinal(array3, 0);
        return array3;
    }
    
    @Override
    public Commitment commit(final byte[] array) {
        final int length = array.length;
        final int byteLength = this.byteLength;
        if (length <= byteLength / 2) {
            final byte[] bytes = new byte[byteLength - array.length];
            this.random.nextBytes(bytes);
            return new Commitment(bytes, this.calculateCommitment(bytes, array));
        }
        throw new DataLengthException("Message to be committed to too large for digest.");
    }
    
    @Override
    public boolean isRevealed(final Commitment commitment, byte[] calculateCommitment) {
        if (calculateCommitment.length + commitment.getSecret().length == this.byteLength) {
            calculateCommitment = this.calculateCommitment(commitment.getSecret(), calculateCommitment);
            return Arrays.constantTimeAreEqual(commitment.getCommitment(), calculateCommitment);
        }
        throw new DataLengthException("Message and witness secret lengths do not match.");
    }
}
