// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto;

import java.math.BigInteger;

public interface DSAExt extends DSA
{
    BigInteger getOrder();
}
