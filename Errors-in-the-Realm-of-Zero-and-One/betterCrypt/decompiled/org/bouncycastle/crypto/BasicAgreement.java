// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto;

import java.math.BigInteger;

public interface BasicAgreement
{
    BigInteger calculateAgreement(final CipherParameters p0);
    
    int getFieldSize();
    
    void init(final CipherParameters p0);
}
