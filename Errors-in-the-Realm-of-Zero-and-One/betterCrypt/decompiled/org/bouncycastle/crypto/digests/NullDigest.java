// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.digests;

import org.bouncycastle.util.Arrays;
import java.io.ByteArrayOutputStream;
import org.bouncycastle.crypto.Digest;

public class NullDigest implements Digest
{
    private OpenByteArrayOutputStream bOut;
    
    public NullDigest() {
        this.bOut = new OpenByteArrayOutputStream();
    }
    
    @Override
    public int doFinal(final byte[] array, final int n) {
        final int size = this.bOut.size();
        this.bOut.copy(array, n);
        this.reset();
        return size;
    }
    
    @Override
    public String getAlgorithmName() {
        return "NULL";
    }
    
    @Override
    public int getDigestSize() {
        return this.bOut.size();
    }
    
    @Override
    public void reset() {
        this.bOut.reset();
    }
    
    @Override
    public void update(final byte b) {
        this.bOut.write(b);
    }
    
    @Override
    public void update(final byte[] b, final int off, final int len) {
        this.bOut.write(b, off, len);
    }
    
    private static class OpenByteArrayOutputStream extends ByteArrayOutputStream
    {
        void copy(final byte[] array, final int n) {
            System.arraycopy(this.buf, 0, array, n, this.size());
        }
        
        @Override
        public void reset() {
            super.reset();
            Arrays.clear(this.buf);
        }
    }
}
