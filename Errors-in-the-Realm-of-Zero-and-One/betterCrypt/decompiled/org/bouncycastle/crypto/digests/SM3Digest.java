// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.digests;

import org.bouncycastle.util.Pack;
import org.bouncycastle.util.Memoable;

public class SM3Digest extends GeneralDigest
{
    private static final int BLOCK_SIZE = 16;
    private static final int DIGEST_LENGTH = 32;
    private static final int[] T;
    private int[] V;
    private int[] W;
    private int[] inwords;
    private int xOff;
    
    static {
        T = new int[64];
        int n = 0;
        int i;
        while (true) {
            i = 16;
            if (n >= 16) {
                break;
            }
            SM3Digest.T[n] = (2043430169 >>> 32 - n | 2043430169 << n);
            ++n;
        }
        while (i < 64) {
            final int n2 = i % 32;
            SM3Digest.T[i] = (2055708042 >>> 32 - n2 | 2055708042 << n2);
            ++i;
        }
    }
    
    public SM3Digest() {
        this.V = new int[8];
        this.inwords = new int[16];
        this.W = new int[68];
        this.reset();
    }
    
    public SM3Digest(final SM3Digest sm3Digest) {
        super(sm3Digest);
        this.V = new int[8];
        this.inwords = new int[16];
        this.W = new int[68];
        this.copyIn(sm3Digest);
    }
    
    private int FF0(final int n, final int n2, final int n3) {
        return n ^ n2 ^ n3;
    }
    
    private int FF1(final int n, final int n2, final int n3) {
        return (n & n3) | (n & n2) | (n2 & n3);
    }
    
    private int GG0(final int n, final int n2, final int n3) {
        return n ^ n2 ^ n3;
    }
    
    private int GG1(final int n, final int n2, final int n3) {
        return (n & n3) | (n2 & n);
    }
    
    private int P0(final int n) {
        return n ^ (n << 9 | n >>> 23) ^ (n << 17 | n >>> 15);
    }
    
    private int P1(final int n) {
        return n ^ (n << 15 | n >>> 17) ^ (n << 23 | n >>> 9);
    }
    
    private void copyIn(final SM3Digest sm3Digest) {
        final int[] v = sm3Digest.V;
        final int[] v2 = this.V;
        System.arraycopy(v, 0, v2, 0, v2.length);
        final int[] inwords = sm3Digest.inwords;
        final int[] inwords2 = this.inwords;
        System.arraycopy(inwords, 0, inwords2, 0, inwords2.length);
        this.xOff = sm3Digest.xOff;
    }
    
    @Override
    public Memoable copy() {
        return new SM3Digest(this);
    }
    
    @Override
    public int doFinal(final byte[] array, final int n) {
        this.finish();
        Pack.intToBigEndian(this.V, array, n);
        this.reset();
        return 32;
    }
    
    @Override
    public String getAlgorithmName() {
        return "SM3";
    }
    
    @Override
    public int getDigestSize() {
        return 32;
    }
    
    @Override
    protected void processBlock() {
        for (int i = 0; i < 16; ++i) {
            this.W[i] = this.inwords[i];
        }
        for (int j = 16; j < 68; ++j) {
            final int[] w = this.W;
            final int n = w[j - 3];
            final int n2 = w[j - 13];
            w[j] = (this.P1((n >>> 17 | n << 15) ^ (w[j - 16] ^ w[j - 9])) ^ (n2 >>> 25 | n2 << 7) ^ this.W[j - 6]);
        }
        final int[] v = this.V;
        int n3 = v[0];
        int n4 = v[1];
        int n5 = v[2];
        int n6 = v[3];
        int n7 = v[4];
        int n8 = v[5];
        int n9 = v[6];
        int n10 = v[7];
        int n11;
        int n13;
        int n14;
        int n15;
        int ff0;
        int p0;
        int n16;
        int n17;
        int n18;
        int n19;
        for (int k = 0; k < 16; ++k, n10 = n9, n16 = (n8 << 19 | n8 >>> 13), n17 = p0, n18 = (n4 << 9 | n4 >>> 23), n19 = ff0 + n6 + (n13 ^ n11) + (n14 ^ n15), n4 = n3, n3 = n19, n6 = n5, n5 = n18, n8 = n7, n7 = n17, n9 = n16) {
            n11 = (n3 << 12 | n3 >>> 20);
            final int n12 = n11 + n7 + SM3Digest.T[k];
            n13 = (n12 << 7 | n12 >>> 25);
            final int[] w2 = this.W;
            n14 = w2[k];
            n15 = w2[k + 4];
            ff0 = this.FF0(n3, n4, n5);
            p0 = this.P0(this.GG0(n7, n8, n9) + n10 + n13 + n14);
        }
        final int n20 = n4;
        final int n21 = n3;
        int n22 = n5;
        final int n23 = 16;
        int n24 = n10;
        int n25 = n9;
        int n26 = n8;
        int n27 = n6;
        int n28 = n21;
        int n29 = n20;
        int n30;
        int n32;
        int n33;
        int n34;
        int ff2;
        int p2;
        int n35;
        int n36;
        int n37;
        int n38;
        for (int l = n23; l < 64; ++l, n24 = n25, n35 = (n26 >>> 13 | n26 << 19), n36 = p2, n37 = (n29 >>> 23 | n29 << 9), n38 = ff2 + n27 + (n32 ^ n30) + (n33 ^ n34), n29 = n28, n28 = n38, n27 = n22, n22 = n37, n26 = n7, n7 = n36, n25 = n35) {
            n30 = (n28 << 12 | n28 >>> 20);
            final int n31 = n30 + n7 + SM3Digest.T[l];
            n32 = (n31 << 7 | n31 >>> 25);
            final int[] w3 = this.W;
            n33 = w3[l];
            n34 = w3[l + 4];
            ff2 = this.FF1(n28, n29, n22);
            p2 = this.P0(this.GG1(n7, n26, n25) + n24 + n32 + n33);
        }
        final int[] v2 = this.V;
        v2[0] ^= n28;
        v2[1] ^= n29;
        v2[2] ^= n22;
        v2[3] ^= n27;
        v2[4] ^= n7;
        v2[5] ^= n26;
        v2[6] ^= n25;
        v2[7] ^= n24;
        this.xOff = 0;
    }
    
    @Override
    protected void processLength(final long n) {
        final int xOff = this.xOff;
        if (xOff > 14) {
            this.inwords[xOff] = 0;
            this.xOff = xOff + 1;
            this.processBlock();
        }
        int xOff2;
        while (true) {
            xOff2 = this.xOff;
            if (xOff2 >= 14) {
                break;
            }
            this.inwords[xOff2] = 0;
            this.xOff = xOff2 + 1;
        }
        final int[] inwords = this.inwords;
        this.xOff = xOff2 + 1;
        inwords[xOff2] = (int)(n >>> 32);
        inwords[this.xOff++] = (int)n;
    }
    
    @Override
    protected void processWord(final byte[] array, int n) {
        final byte b = array[n];
        final int n2 = n + 1;
        n = array[n2];
        final int n3 = n2 + 1;
        final byte b2 = array[n3];
        final byte b3 = array[n3 + 1];
        final int[] inwords = this.inwords;
        final int xOff = this.xOff;
        inwords[xOff] = ((b3 & 0xFF) | ((b & 0xFF) << 24 | (n & 0xFF) << 16 | (b2 & 0xFF) << 8));
        this.xOff = xOff + 1;
        if (this.xOff >= 16) {
            this.processBlock();
        }
    }
    
    @Override
    public void reset() {
        super.reset();
        final int[] v = this.V;
        v[0] = 1937774191;
        v[1] = 1226093241;
        v[2] = 388252375;
        v[3] = -628488704;
        v[4] = -1452330820;
        v[5] = 372324522;
        v[6] = -477237683;
        v[7] = -1325724082;
        this.xOff = 0;
    }
    
    @Override
    public void reset(final Memoable memoable) {
        final SM3Digest sm3Digest = (SM3Digest)memoable;
        super.copyIn(sm3Digest);
        this.copyIn(sm3Digest);
    }
}
