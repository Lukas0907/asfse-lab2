// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.digests;

public interface EncodableDigest
{
    byte[] getEncodedState();
}
