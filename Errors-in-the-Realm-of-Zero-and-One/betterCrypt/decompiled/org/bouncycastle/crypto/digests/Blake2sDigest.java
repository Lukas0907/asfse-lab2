// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.digests;

import org.bouncycastle.util.Pack;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.ExtendedDigest;

public class Blake2sDigest implements ExtendedDigest
{
    private static final int BLOCK_LENGTH_BYTES = 64;
    private static final int ROUNDS = 10;
    private static final int[] blake2s_IV;
    private static final byte[][] blake2s_sigma;
    private byte[] buffer;
    private int bufferPos;
    private int[] chainValue;
    private int depth;
    private int digestLength;
    private int f0;
    private int fanout;
    private int innerHashLength;
    private int[] internalState;
    private byte[] key;
    private int keyLength;
    private int leafLength;
    private int nodeDepth;
    private long nodeOffset;
    private byte[] personalization;
    private byte[] salt;
    private int t0;
    private int t1;
    
    static {
        blake2s_IV = new int[] { 1779033703, -1150833019, 1013904242, -1521486534, 1359893119, -1694144372, 528734635, 1541459225 };
        blake2s_sigma = new byte[][] { { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }, { 14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3 }, { 11, 8, 12, 0, 5, 2, 15, 13, 10, 14, 3, 6, 7, 1, 9, 4 }, { 7, 9, 3, 1, 13, 12, 11, 14, 2, 6, 5, 10, 4, 0, 15, 8 }, { 9, 0, 5, 7, 2, 4, 10, 15, 14, 1, 11, 12, 6, 8, 3, 13 }, { 2, 12, 6, 10, 0, 11, 8, 3, 4, 13, 7, 5, 15, 14, 1, 9 }, { 12, 5, 1, 15, 14, 13, 4, 10, 0, 7, 6, 3, 9, 2, 8, 11 }, { 13, 11, 7, 14, 12, 1, 3, 9, 5, 0, 15, 4, 8, 6, 2, 10 }, { 6, 15, 14, 9, 11, 3, 0, 8, 12, 2, 13, 7, 1, 4, 10, 5 }, { 10, 2, 8, 4, 7, 6, 1, 5, 15, 11, 9, 14, 3, 12, 13, 0 } };
    }
    
    public Blake2sDigest() {
        this(256);
    }
    
    public Blake2sDigest(final int n) {
        this.digestLength = 32;
        this.keyLength = 0;
        this.salt = null;
        this.personalization = null;
        this.key = null;
        this.fanout = 1;
        this.depth = 1;
        this.leafLength = 0;
        this.nodeOffset = 0L;
        this.nodeDepth = 0;
        this.innerHashLength = 0;
        this.buffer = null;
        this.bufferPos = 0;
        this.internalState = new int[16];
        this.chainValue = null;
        this.t0 = 0;
        this.t1 = 0;
        this.f0 = 0;
        if (n >= 8 && n <= 256 && n % 8 == 0) {
            this.digestLength = n / 8;
            this.init(null, null, null);
            return;
        }
        throw new IllegalArgumentException("BLAKE2s digest bit length must be a multiple of 8 and not greater than 256");
    }
    
    Blake2sDigest(final int digestLength, final int n, final long nodeOffset) {
        this.digestLength = 32;
        this.keyLength = 0;
        this.salt = null;
        this.personalization = null;
        this.key = null;
        this.fanout = 1;
        this.depth = 1;
        this.leafLength = 0;
        this.nodeOffset = 0L;
        this.nodeDepth = 0;
        this.innerHashLength = 0;
        this.buffer = null;
        this.bufferPos = 0;
        this.internalState = new int[16];
        this.chainValue = null;
        this.t0 = 0;
        this.t1 = 0;
        this.f0 = 0;
        this.digestLength = digestLength;
        this.nodeOffset = nodeOffset;
        this.fanout = 0;
        this.depth = 0;
        this.leafLength = n;
        this.innerHashLength = n;
        this.nodeDepth = 0;
        this.init(null, null, null);
    }
    
    Blake2sDigest(final int digestLength, final byte[] array, final byte[] array2, final byte[] array3, final long nodeOffset) {
        this.digestLength = 32;
        this.keyLength = 0;
        this.salt = null;
        this.personalization = null;
        this.key = null;
        this.fanout = 1;
        this.depth = 1;
        this.leafLength = 0;
        this.nodeOffset = 0L;
        this.nodeDepth = 0;
        this.innerHashLength = 0;
        this.buffer = null;
        this.bufferPos = 0;
        this.internalState = new int[16];
        this.chainValue = null;
        this.t0 = 0;
        this.t1 = 0;
        this.f0 = 0;
        this.digestLength = digestLength;
        this.nodeOffset = nodeOffset;
        this.init(array2, array3, array);
    }
    
    public Blake2sDigest(final Blake2sDigest blake2sDigest) {
        this.digestLength = 32;
        this.keyLength = 0;
        this.salt = null;
        this.personalization = null;
        this.key = null;
        this.fanout = 1;
        this.depth = 1;
        this.leafLength = 0;
        this.nodeOffset = 0L;
        this.nodeDepth = 0;
        this.innerHashLength = 0;
        this.buffer = null;
        this.bufferPos = 0;
        this.internalState = new int[16];
        this.chainValue = null;
        this.t0 = 0;
        this.t1 = 0;
        this.f0 = 0;
        this.bufferPos = blake2sDigest.bufferPos;
        this.buffer = Arrays.clone(blake2sDigest.buffer);
        this.keyLength = blake2sDigest.keyLength;
        this.key = Arrays.clone(blake2sDigest.key);
        this.digestLength = blake2sDigest.digestLength;
        this.internalState = Arrays.clone(this.internalState);
        this.chainValue = Arrays.clone(blake2sDigest.chainValue);
        this.t0 = blake2sDigest.t0;
        this.t1 = blake2sDigest.t1;
        this.f0 = blake2sDigest.f0;
        this.salt = Arrays.clone(blake2sDigest.salt);
        this.personalization = Arrays.clone(blake2sDigest.personalization);
        this.fanout = blake2sDigest.fanout;
        this.depth = blake2sDigest.depth;
        this.leafLength = blake2sDigest.leafLength;
        this.nodeOffset = blake2sDigest.nodeOffset;
        this.nodeDepth = blake2sDigest.nodeDepth;
        this.innerHashLength = blake2sDigest.innerHashLength;
    }
    
    public Blake2sDigest(final byte[] array) {
        this.digestLength = 32;
        this.keyLength = 0;
        this.salt = null;
        this.personalization = null;
        this.key = null;
        this.fanout = 1;
        this.depth = 1;
        this.leafLength = 0;
        this.nodeOffset = 0L;
        this.nodeDepth = 0;
        this.innerHashLength = 0;
        this.buffer = null;
        this.bufferPos = 0;
        this.internalState = new int[16];
        this.chainValue = null;
        this.t0 = 0;
        this.t1 = 0;
        this.f0 = 0;
        this.init(null, null, array);
    }
    
    public Blake2sDigest(final byte[] array, final int digestLength, final byte[] array2, final byte[] array3) {
        this.digestLength = 32;
        this.keyLength = 0;
        this.salt = null;
        this.personalization = null;
        this.key = null;
        this.fanout = 1;
        this.depth = 1;
        this.leafLength = 0;
        this.nodeOffset = 0L;
        this.nodeDepth = 0;
        this.innerHashLength = 0;
        this.buffer = null;
        this.bufferPos = 0;
        this.internalState = new int[16];
        this.chainValue = null;
        this.t0 = 0;
        this.t1 = 0;
        this.f0 = 0;
        if (digestLength >= 1 && digestLength <= 32) {
            this.digestLength = digestLength;
            this.init(array2, array3, array);
            return;
        }
        throw new IllegalArgumentException("Invalid digest length (required: 1 - 32)");
    }
    
    private void G(final int n, final int n2, final int n3, final int n4, final int n5, final int n6) {
        final int[] internalState = this.internalState;
        internalState[n3] = internalState[n3] + internalState[n4] + n;
        internalState[n6] = this.rotr32(internalState[n6] ^ internalState[n3], 16);
        final int[] internalState2 = this.internalState;
        internalState2[n5] += internalState2[n6];
        internalState2[n4] = this.rotr32(internalState2[n4] ^ internalState2[n5], 12);
        final int[] internalState3 = this.internalState;
        internalState3[n3] = internalState3[n3] + internalState3[n4] + n2;
        internalState3[n6] = this.rotr32(internalState3[n6] ^ internalState3[n3], 8);
        final int[] internalState4 = this.internalState;
        internalState4[n5] += internalState4[n6];
        internalState4[n4] = this.rotr32(internalState4[n4] ^ internalState4[n5], 7);
    }
    
    private void compress(final byte[] array, int n) {
        this.initializeInternalState();
        final int[] array2 = new int[16];
        final int n2 = 0;
        for (int i = 0; i < 16; ++i) {
            array2[i] = Pack.littleEndianToInt(array, i * 4 + n);
        }
        int n3 = 0;
        while (true) {
            n = n2;
            if (n3 >= 10) {
                break;
            }
            final byte[][] blake2s_sigma = Blake2sDigest.blake2s_sigma;
            this.G(array2[blake2s_sigma[n3][0]], array2[blake2s_sigma[n3][1]], 0, 4, 8, 12);
            final byte[][] blake2s_sigma2 = Blake2sDigest.blake2s_sigma;
            this.G(array2[blake2s_sigma2[n3][2]], array2[blake2s_sigma2[n3][3]], 1, 5, 9, 13);
            final byte[][] blake2s_sigma3 = Blake2sDigest.blake2s_sigma;
            this.G(array2[blake2s_sigma3[n3][4]], array2[blake2s_sigma3[n3][5]], 2, 6, 10, 14);
            final byte[][] blake2s_sigma4 = Blake2sDigest.blake2s_sigma;
            this.G(array2[blake2s_sigma4[n3][6]], array2[blake2s_sigma4[n3][7]], 3, 7, 11, 15);
            final byte[][] blake2s_sigma5 = Blake2sDigest.blake2s_sigma;
            this.G(array2[blake2s_sigma5[n3][8]], array2[blake2s_sigma5[n3][9]], 0, 5, 10, 15);
            final byte[][] blake2s_sigma6 = Blake2sDigest.blake2s_sigma;
            this.G(array2[blake2s_sigma6[n3][10]], array2[blake2s_sigma6[n3][11]], 1, 6, 11, 12);
            final byte[][] blake2s_sigma7 = Blake2sDigest.blake2s_sigma;
            this.G(array2[blake2s_sigma7[n3][12]], array2[blake2s_sigma7[n3][13]], 2, 7, 8, 13);
            final byte[][] blake2s_sigma8 = Blake2sDigest.blake2s_sigma;
            this.G(array2[blake2s_sigma8[n3][14]], array2[blake2s_sigma8[n3][15]], 3, 4, 9, 14);
            ++n3;
        }
        while (true) {
            final int[] chainValue = this.chainValue;
            if (n >= chainValue.length) {
                break;
            }
            final int n4 = chainValue[n];
            final int[] internalState = this.internalState;
            chainValue[n] = (n4 ^ internalState[n] ^ internalState[n + 8]);
            ++n;
        }
    }
    
    private void init(final byte[] array, final byte[] array2, final byte[] array3) {
        this.buffer = new byte[64];
        if (array3 != null && array3.length > 0) {
            if (array3.length > 32) {
                throw new IllegalArgumentException("Keys > 32 bytes are not supported");
            }
            System.arraycopy(array3, 0, this.key = new byte[array3.length], 0, array3.length);
            this.keyLength = array3.length;
            System.arraycopy(array3, 0, this.buffer, 0, array3.length);
            this.bufferPos = 64;
        }
        if (this.chainValue == null) {
            this.chainValue = new int[8];
            final int[] chainValue = this.chainValue;
            final int[] blake2s_IV = Blake2sDigest.blake2s_IV;
            chainValue[0] = (blake2s_IV[0] ^ (this.digestLength | this.keyLength << 8 | (this.fanout << 16 | this.depth << 24)));
            chainValue[1] = (blake2s_IV[1] ^ this.leafLength);
            final long nodeOffset = this.nodeOffset;
            final int n = (int)(nodeOffset >> 32);
            chainValue[2] = ((int)nodeOffset ^ blake2s_IV[2]);
            chainValue[3] = ((n | this.nodeDepth << 16 | this.innerHashLength << 24) ^ blake2s_IV[3]);
            chainValue[4] = blake2s_IV[4];
            chainValue[5] = blake2s_IV[5];
            if (array != null) {
                if (array.length != 8) {
                    throw new IllegalArgumentException("Salt length must be exactly 8 bytes");
                }
                System.arraycopy(array, 0, this.salt = new byte[8], 0, array.length);
                final int[] chainValue2 = this.chainValue;
                chainValue2[4] ^= Pack.littleEndianToInt(array, 0);
                final int[] chainValue3 = this.chainValue;
                chainValue3[5] ^= Pack.littleEndianToInt(array, 4);
            }
            final int[] chainValue4 = this.chainValue;
            final int[] blake2s_IV2 = Blake2sDigest.blake2s_IV;
            chainValue4[6] = blake2s_IV2[6];
            chainValue4[7] = blake2s_IV2[7];
            if (array2 != null) {
                if (array2.length == 8) {
                    System.arraycopy(array2, 0, this.personalization = new byte[8], 0, array2.length);
                    final int[] chainValue5 = this.chainValue;
                    chainValue5[6] ^= Pack.littleEndianToInt(array2, 0);
                    final int[] chainValue6 = this.chainValue;
                    chainValue6[7] ^= Pack.littleEndianToInt(array2, 4);
                    return;
                }
                throw new IllegalArgumentException("Personalization length must be exactly 8 bytes");
            }
        }
    }
    
    private void initializeInternalState() {
        final int[] chainValue = this.chainValue;
        System.arraycopy(chainValue, 0, this.internalState, 0, chainValue.length);
        System.arraycopy(Blake2sDigest.blake2s_IV, 0, this.internalState, this.chainValue.length, 4);
        final int[] internalState = this.internalState;
        final int t0 = this.t0;
        final int[] blake2s_IV = Blake2sDigest.blake2s_IV;
        internalState[12] = (t0 ^ blake2s_IV[4]);
        internalState[13] = (this.t1 ^ blake2s_IV[5]);
        internalState[14] = (this.f0 ^ blake2s_IV[6]);
        internalState[15] = blake2s_IV[7];
    }
    
    private int rotr32(final int n, final int n2) {
        return n << 32 - n2 | n >>> n2;
    }
    
    public void clearKey() {
        final byte[] key = this.key;
        if (key != null) {
            Arrays.fill(key, (byte)0);
            Arrays.fill(this.buffer, (byte)0);
        }
    }
    
    public void clearSalt() {
        final byte[] salt = this.salt;
        if (salt != null) {
            Arrays.fill(salt, (byte)0);
        }
    }
    
    @Override
    public int doFinal(final byte[] array, final int n) {
        this.f0 = -1;
        final int t0 = this.t0;
        final int bufferPos = this.bufferPos;
        this.t0 = t0 + bufferPos;
        final int t2 = this.t0;
        if (t2 < 0 && bufferPos > -t2) {
            ++this.t1;
        }
        this.compress(this.buffer, 0);
        Arrays.fill(this.buffer, (byte)0);
        Arrays.fill(this.internalState, 0);
        int n2 = 0;
        while (true) {
            final int[] chainValue = this.chainValue;
            if (n2 >= chainValue.length) {
                break;
            }
            final int n3 = n2 * 4;
            if (n3 >= this.digestLength) {
                break;
            }
            final byte[] intToLittleEndian = Pack.intToLittleEndian(chainValue[n2]);
            final int digestLength = this.digestLength;
            if (n3 < digestLength - 4) {
                System.arraycopy(intToLittleEndian, 0, array, n3 + n, 4);
            }
            else {
                System.arraycopy(intToLittleEndian, 0, array, n + n3, digestLength - n3);
            }
            ++n2;
        }
        Arrays.fill(this.chainValue, 0);
        this.reset();
        return this.digestLength;
    }
    
    @Override
    public String getAlgorithmName() {
        return "BLAKE2s";
    }
    
    @Override
    public int getByteLength() {
        return 64;
    }
    
    @Override
    public int getDigestSize() {
        return this.digestLength;
    }
    
    @Override
    public void reset() {
        this.bufferPos = 0;
        this.f0 = 0;
        this.t0 = 0;
        this.t1 = 0;
        this.chainValue = null;
        Arrays.fill(this.buffer, (byte)0);
        final byte[] key = this.key;
        if (key != null) {
            System.arraycopy(key, 0, this.buffer, 0, key.length);
            this.bufferPos = 64;
        }
        this.init(this.salt, this.personalization, this.key);
    }
    
    @Override
    public void update(final byte b) {
        final int bufferPos = this.bufferPos;
        if (64 - bufferPos == 0) {
            this.t0 += 64;
            if (this.t0 == 0) {
                ++this.t1;
            }
            this.compress(this.buffer, 0);
            Arrays.fill(this.buffer, (byte)0);
            this.buffer[0] = b;
            this.bufferPos = 1;
            return;
        }
        this.buffer[bufferPos] = b;
        this.bufferPos = bufferPos + 1;
    }
    
    @Override
    public void update(final byte[] array, int i, int n) {
        if (array == null) {
            return;
        }
        if (n == 0) {
            return;
        }
        final int bufferPos = this.bufferPos;
        while (true) {
            int n2 = 0;
            Label_0125: {
                if (bufferPos == 0) {
                    n2 = 0;
                    break Label_0125;
                }
                n2 = 64 - bufferPos;
                if (n2 < n) {
                    System.arraycopy(array, i, this.buffer, bufferPos, n2);
                    this.t0 += 64;
                    if (this.t0 == 0) {
                        ++this.t1;
                    }
                    this.compress(this.buffer, 0);
                    this.bufferPos = 0;
                    Arrays.fill(this.buffer, (byte)0);
                    break Label_0125;
                }
                System.arraycopy(array, i, this.buffer, bufferPos, n);
                this.bufferPos += n;
                return;
            }
            for (n += i, i += n2; i < n - 64; i += 64) {
                this.t0 += 64;
                if (this.t0 == 0) {
                    ++this.t1;
                }
                this.compress(array, i);
            }
            final byte[] buffer = this.buffer;
            n -= i;
            System.arraycopy(array, i, buffer, 0, n);
            continue;
        }
    }
}
