// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.digests;

import org.bouncycastle.crypto.Digest;

public abstract class HarakaBase implements Digest
{
    protected static final int DIGEST_SIZE = 32;
    private static final byte[][] S;
    
    static {
        S = new byte[][] { { 99, 124, 119, 123, -14, 107, 111, -59, 48, 1, 103, 43, -2, -41, -85, 118 }, { -54, -126, -55, 125, -6, 89, 71, -16, -83, -44, -94, -81, -100, -92, 114, -64 }, { -73, -3, -109, 38, 54, 63, -9, -52, 52, -91, -27, -15, 113, -40, 49, 21 }, { 4, -57, 35, -61, 24, -106, 5, -102, 7, 18, -128, -30, -21, 39, -78, 117 }, { 9, -125, 44, 26, 27, 110, 90, -96, 82, 59, -42, -77, 41, -29, 47, -124 }, { 83, -47, 0, -19, 32, -4, -79, 91, 106, -53, -66, 57, 74, 76, 88, -49 }, { -48, -17, -86, -5, 67, 77, 51, -123, 69, -7, 2, 127, 80, 60, -97, -88 }, { 81, -93, 64, -113, -110, -99, 56, -11, -68, -74, -38, 33, 16, -1, -13, -46 }, { -51, 12, 19, -20, 95, -105, 68, 23, -60, -89, 126, 61, 100, 93, 25, 115 }, { 96, -127, 79, -36, 34, 42, -112, -120, 70, -18, -72, 20, -34, 94, 11, -37 }, { -32, 50, 58, 10, 73, 6, 36, 92, -62, -45, -84, 98, -111, -107, -28, 121 }, { -25, -56, 55, 109, -115, -43, 78, -87, 108, 86, -12, -22, 101, 122, -82, 8 }, { -70, 120, 37, 46, 28, -90, -76, -58, -24, -35, 116, 31, 75, -67, -117, -118 }, { 112, 62, -75, 102, 72, 3, -10, 14, 97, 53, 87, -71, -122, -63, 29, -98 }, { -31, -8, -104, 17, 105, -39, -114, -108, -101, 30, -121, -23, -50, 85, 40, -33 }, { -116, -95, -119, 13, -65, -26, 66, 104, 65, -103, 45, 15, -80, 84, -69, 22 } };
    }
    
    static byte[] aesEnc(byte[] mixColumns, final byte[] array) {
        mixColumns = mixColumns(shiftRows(subBytes(mixColumns)));
        xorReverse(mixColumns, array);
        return mixColumns;
    }
    
    private static byte[] mixColumns(final byte[] array) {
        final byte[] array2 = new byte[array.length];
        int i = 0;
        int n = 0;
        while (i < 4) {
            final int n2 = n + 1;
            final int n3 = i * 4;
            final byte xTime = xTime(array[n3]);
            final int n4 = n3 + 1;
            final byte xTime2 = xTime(array[n4]);
            final byte b = array[n4];
            final int n5 = n3 + 2;
            final byte b2 = array[n5];
            final int n6 = n3 + 3;
            array2[n] = (byte)(xTime ^ xTime2 ^ b ^ b2 ^ array[n6]);
            final int n7 = n2 + 1;
            array2[n2] = (byte)(array[n3] ^ xTime(array[n4]) ^ xTime(array[n5]) ^ array[n5] ^ array[n6]);
            final int n8 = n7 + 1;
            array2[n7] = (byte)(array[n3] ^ array[n4] ^ xTime(array[n5]) ^ xTime(array[n6]) ^ array[n6]);
            n = n8 + 1;
            array2[n8] = (byte)(array[n3] ^ xTime(array[n3]) ^ array[n4] ^ array[n5] ^ xTime(array[n6]));
            ++i;
        }
        return array2;
    }
    
    static byte sBox(final byte b) {
        return HarakaBase.S[(b & 0xFF) >>> 4][b & 0xF];
    }
    
    static byte[] shiftRows(final byte[] array) {
        return new byte[] { array[0], array[5], array[10], array[15], array[4], array[9], array[14], array[3], array[8], array[13], array[2], array[7], array[12], array[1], array[6], array[11] };
    }
    
    static byte[] subBytes(final byte[] array) {
        final byte[] array2 = new byte[array.length];
        array2[0] = sBox(array[0]);
        array2[1] = sBox(array[1]);
        array2[2] = sBox(array[2]);
        array2[3] = sBox(array[3]);
        array2[4] = sBox(array[4]);
        array2[5] = sBox(array[5]);
        array2[6] = sBox(array[6]);
        array2[7] = sBox(array[7]);
        array2[8] = sBox(array[8]);
        array2[9] = sBox(array[9]);
        array2[10] = sBox(array[10]);
        array2[11] = sBox(array[11]);
        array2[12] = sBox(array[12]);
        array2[13] = sBox(array[13]);
        array2[14] = sBox(array[14]);
        array2[15] = sBox(array[15]);
        return array2;
    }
    
    static byte xTime(final byte b) {
        int n = b << 1;
        if (b >>> 7 > 0) {
            n ^= 0x1B;
        }
        return (byte)(n & 0xFF);
    }
    
    static byte[] xor(final byte[] array, final byte[] array2, int n) {
        final byte[] array3 = new byte[16];
        for (int i = 0; i < array3.length; ++i, ++n) {
            array3[i] = (byte)(array2[n] ^ array[i]);
        }
        return array3;
    }
    
    static void xorReverse(final byte[] array, final byte[] array2) {
        array[0] ^= array2[15];
        array[1] ^= array2[14];
        array[2] ^= array2[13];
        array[3] ^= array2[12];
        array[4] ^= array2[11];
        array[5] ^= array2[10];
        array[6] ^= array2[9];
        array[7] ^= array2[8];
        array[8] ^= array2[7];
        array[9] ^= array2[6];
        array[10] ^= array2[5];
        array[11] ^= array2[4];
        array[12] ^= array2[3];
        array[13] ^= array2[2];
        array[14] ^= array2[1];
        array[15] ^= array2[0];
    }
    
    @Override
    public int getDigestSize() {
        return 32;
    }
}
