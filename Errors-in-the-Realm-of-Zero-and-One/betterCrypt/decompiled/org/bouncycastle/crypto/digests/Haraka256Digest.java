// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.digests;

import java.lang.reflect.Array;
import org.bouncycastle.util.Arrays;

public class Haraka256Digest extends HarakaBase
{
    private static final byte[][] RC;
    private final byte[] buffer;
    private int off;
    
    static {
        RC = new byte[][] { { 6, -124, 112, 76, -26, 32, -64, 10, -78, -59, -2, -16, 117, -127, 123, -99 }, { -117, 102, -76, -31, -120, -13, -96, 107, 100, 15, 107, -92, 47, 8, -9, 23 }, { 52, 2, -34, 45, 83, -14, -124, -104, -49, 2, -99, 96, -97, 2, -111, 20 }, { 14, -42, -22, -26, 46, 123, 79, 8, -69, -13, -68, -81, -3, 91, 79, 121 }, { -53, -49, -80, -53, 72, 114, 68, -117, 121, -18, -51, 28, -66, 57, 112, 68 }, { 126, -22, -51, -18, 110, -112, 50, -73, -115, 83, 53, -19, 43, -118, 5, 123 }, { 103, -62, -113, 67, 94, 46, 124, -48, -30, 65, 39, 97, -38, 79, -17, 27 }, { 41, 36, -39, -80, -81, -54, -52, 7, 103, 95, -3, -30, 31, -57, 11, 59 }, { -85, 77, 99, -15, -26, -122, 127, -23, -20, -37, -113, -54, -71, -44, 101, -18 }, { 28, 48, -65, -124, -44, -73, -51, 100, 91, 42, 64, 79, -83, 3, 126, 51 }, { -78, -52, 11, -71, -108, 23, 35, -65, 105, 2, -117, 46, -115, -10, -104, 0 }, { -6, 4, 120, -90, -34, 111, 85, 114, 74, -86, -98, -56, 92, -99, 45, -118 }, { -33, -76, -97, 43, 107, 119, 42, 18, 14, -6, 79, 46, 41, 18, -97, -44 }, { 30, -95, 3, 68, -12, 73, -94, 54, 50, -42, 17, -82, -69, 106, 18, -18 }, { -81, 4, 73, -120, 75, 5, 0, -124, 95, -106, 0, -55, -100, -88, -20, -90 }, { 33, 2, 94, -40, -99, 25, -100, 79, 120, -94, -57, -29, 39, -27, -109, -20 }, { -65, 58, -86, -8, -89, 89, -55, -73, -71, 40, 46, -51, -126, -44, 1, 115 }, { 98, 96, 112, 13, 97, -122, -80, 23, 55, -14, -17, -39, 16, 48, 125, 107 }, { 90, -54, 69, -62, 33, 48, 4, 67, -127, -62, -111, 83, -10, -4, -102, -58 }, { -110, 35, -105, 60, 34, 107, 104, -69, 44, -81, -110, -24, 54, -47, -108, 58 } };
    }
    
    public Haraka256Digest() {
        this.buffer = new byte[32];
    }
    
    public Haraka256Digest(final Haraka256Digest haraka256Digest) {
        this.buffer = Arrays.clone(haraka256Digest.buffer);
        this.off = haraka256Digest.off;
    }
    
    private int haraka256256(final byte[] array, final byte[] array2, final int n) {
        final byte[][] array3 = (byte[][])Array.newInstance(Byte.TYPE, 2, 16);
        final byte[][] array4 = (byte[][])Array.newInstance(Byte.TYPE, 2, 16);
        System.arraycopy(array, 0, array3[0], 0, 16);
        System.arraycopy(array, 16, array3[1], 0, 16);
        array3[0] = HarakaBase.aesEnc(array3[0], Haraka256Digest.RC[0]);
        array3[1] = HarakaBase.aesEnc(array3[1], Haraka256Digest.RC[1]);
        array3[0] = HarakaBase.aesEnc(array3[0], Haraka256Digest.RC[2]);
        array3[1] = HarakaBase.aesEnc(array3[1], Haraka256Digest.RC[3]);
        this.mix256(array3, array4);
        array3[0] = HarakaBase.aesEnc(array4[0], Haraka256Digest.RC[4]);
        array3[1] = HarakaBase.aesEnc(array4[1], Haraka256Digest.RC[5]);
        array3[0] = HarakaBase.aesEnc(array3[0], Haraka256Digest.RC[6]);
        array3[1] = HarakaBase.aesEnc(array3[1], Haraka256Digest.RC[7]);
        this.mix256(array3, array4);
        array3[0] = HarakaBase.aesEnc(array4[0], Haraka256Digest.RC[8]);
        array3[1] = HarakaBase.aesEnc(array4[1], Haraka256Digest.RC[9]);
        array3[0] = HarakaBase.aesEnc(array3[0], Haraka256Digest.RC[10]);
        array3[1] = HarakaBase.aesEnc(array3[1], Haraka256Digest.RC[11]);
        this.mix256(array3, array4);
        array3[0] = HarakaBase.aesEnc(array4[0], Haraka256Digest.RC[12]);
        array3[1] = HarakaBase.aesEnc(array4[1], Haraka256Digest.RC[13]);
        array3[0] = HarakaBase.aesEnc(array3[0], Haraka256Digest.RC[14]);
        array3[1] = HarakaBase.aesEnc(array3[1], Haraka256Digest.RC[15]);
        this.mix256(array3, array4);
        array3[0] = HarakaBase.aesEnc(array4[0], Haraka256Digest.RC[16]);
        array3[1] = HarakaBase.aesEnc(array4[1], Haraka256Digest.RC[17]);
        array3[0] = HarakaBase.aesEnc(array3[0], Haraka256Digest.RC[18]);
        array3[1] = HarakaBase.aesEnc(array3[1], Haraka256Digest.RC[19]);
        this.mix256(array3, array4);
        array3[0] = HarakaBase.xor(array4[0], array, 0);
        array3[1] = HarakaBase.xor(array4[1], array, 16);
        System.arraycopy(array3[0], 0, array2, n, 16);
        System.arraycopy(array3[1], 0, array2, n + 16, 16);
        return 32;
    }
    
    private void mix256(final byte[][] array, final byte[][] array2) {
        System.arraycopy(array[0], 0, array2[0], 0, 4);
        System.arraycopy(array[1], 0, array2[0], 4, 4);
        System.arraycopy(array[0], 4, array2[0], 8, 4);
        System.arraycopy(array[1], 4, array2[0], 12, 4);
        System.arraycopy(array[0], 8, array2[1], 0, 4);
        System.arraycopy(array[1], 8, array2[1], 4, 4);
        System.arraycopy(array[0], 12, array2[1], 8, 4);
        System.arraycopy(array[1], 12, array2[1], 12, 4);
    }
    
    @Override
    public int doFinal(final byte[] array, int haraka256256) {
        if (this.off != 32) {
            throw new IllegalStateException("input must be exactly 32 bytes");
        }
        if (array.length - haraka256256 >= 32) {
            haraka256256 = this.haraka256256(this.buffer, array, haraka256256);
            this.reset();
            return haraka256256;
        }
        throw new IllegalArgumentException("output too short to receive digest");
    }
    
    @Override
    public String getAlgorithmName() {
        return "Haraka-256";
    }
    
    @Override
    public void reset() {
        this.off = 0;
        Arrays.clear(this.buffer);
    }
    
    @Override
    public void update(final byte b) {
        final int off = this.off;
        if (off + 1 <= 32) {
            final byte[] buffer = this.buffer;
            this.off = off + 1;
            buffer[off] = b;
            return;
        }
        throw new IllegalArgumentException("total input cannot be more than 32 bytes");
    }
    
    @Override
    public void update(final byte[] array, final int n, final int n2) {
        final int off = this.off;
        if (off + n2 <= 32) {
            System.arraycopy(array, n, this.buffer, off, n2);
            this.off += n2;
            return;
        }
        throw new IllegalArgumentException("total input cannot be more than 32 bytes");
    }
}
