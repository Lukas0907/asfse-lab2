// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.digests;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.Xof;

public class Blake2xsDigest implements Xof
{
    private static final int DIGEST_LENGTH = 32;
    private static final long MAX_NUMBER_BLOCKS = 4294967296L;
    public static final int UNKNOWN_DIGEST_LENGTH = 65535;
    private long blockPos;
    private byte[] buf;
    private int bufPos;
    private int digestLength;
    private int digestPos;
    private byte[] h0;
    private Blake2sDigest hash;
    private long nodeOffset;
    
    public Blake2xsDigest() {
        this(65535);
    }
    
    public Blake2xsDigest(final int n) {
        this(n, null, null, null);
    }
    
    public Blake2xsDigest(final int n, final byte[] array) {
        this(n, array, null, null);
    }
    
    public Blake2xsDigest(final int digestLength, final byte[] array, final byte[] array2, final byte[] array3) {
        this.h0 = null;
        this.buf = new byte[32];
        this.bufPos = 32;
        this.digestPos = 0;
        this.blockPos = 0L;
        if (digestLength >= 1 && digestLength <= 65535) {
            this.digestLength = digestLength;
            this.nodeOffset = this.computeNodeOffset();
            this.hash = new Blake2sDigest(32, array, array2, array3, this.nodeOffset);
            return;
        }
        throw new IllegalArgumentException("BLAKE2xs digest length must be between 1 and 2^16-1");
    }
    
    public Blake2xsDigest(final Blake2xsDigest blake2xsDigest) {
        this.h0 = null;
        this.buf = new byte[32];
        this.bufPos = 32;
        this.digestPos = 0;
        this.blockPos = 0L;
        this.digestLength = blake2xsDigest.digestLength;
        this.hash = new Blake2sDigest(blake2xsDigest.hash);
        this.h0 = Arrays.clone(blake2xsDigest.h0);
        this.buf = Arrays.clone(blake2xsDigest.buf);
        this.bufPos = blake2xsDigest.bufPos;
        this.digestPos = blake2xsDigest.digestPos;
        this.blockPos = blake2xsDigest.blockPos;
        this.nodeOffset = blake2xsDigest.nodeOffset;
    }
    
    private long computeNodeOffset() {
        return this.digestLength * 4294967296L;
    }
    
    private int computeStepLength() {
        final int digestLength = this.digestLength;
        if (digestLength == 65535) {
            return 32;
        }
        return Math.min(32, digestLength - this.digestPos);
    }
    
    @Override
    public int doFinal(final byte[] array, final int n) {
        return this.doFinal(array, n, array.length);
    }
    
    @Override
    public int doFinal(final byte[] array, int doOutput, final int n) {
        doOutput = this.doOutput(array, doOutput, n);
        this.reset();
        return doOutput;
    }
    
    @Override
    public int doOutput(final byte[] array, int i, final int n) {
        if (this.h0 == null) {
            this.h0 = new byte[this.hash.getDigestSize()];
            this.hash.doFinal(this.h0, 0);
        }
        i = this.digestLength;
        if (i != 65535) {
            if (this.digestPos + n > i) {
                throw new IllegalArgumentException("Output length is above the digest length");
            }
        }
        else if (this.blockPos << 5 >= this.getUnknownMaxLength()) {
            throw new IllegalArgumentException("Maximum length is 2^32 blocks of 32 bytes");
        }
        Blake2sDigest blake2sDigest;
        byte[] h0;
        byte[] buf;
        int bufPos;
        for (i = 0; i < n; ++i) {
            if (this.bufPos >= 32) {
                blake2sDigest = new Blake2sDigest(this.computeStepLength(), 32, this.nodeOffset);
                h0 = this.h0;
                blake2sDigest.update(h0, 0, h0.length);
                Arrays.fill(this.buf, (byte)0);
                blake2sDigest.doFinal(this.buf, 0);
                this.bufPos = 0;
                ++this.nodeOffset;
                ++this.blockPos;
            }
            buf = this.buf;
            bufPos = this.bufPos;
            array[i] = buf[bufPos];
            this.bufPos = bufPos + 1;
            ++this.digestPos;
        }
        return n;
    }
    
    @Override
    public String getAlgorithmName() {
        return "BLAKE2xs";
    }
    
    @Override
    public int getByteLength() {
        return this.hash.getByteLength();
    }
    
    @Override
    public int getDigestSize() {
        return this.digestLength;
    }
    
    public long getUnknownMaxLength() {
        return 137438953472L;
    }
    
    @Override
    public void reset() {
        this.hash.reset();
        this.h0 = null;
        this.bufPos = 32;
        this.digestPos = 0;
        this.blockPos = 0L;
        this.nodeOffset = this.computeNodeOffset();
    }
    
    @Override
    public void update(final byte b) {
        this.hash.update(b);
    }
    
    @Override
    public void update(final byte[] array, final int n, final int n2) {
        this.hash.update(array, n, n2);
    }
}
