// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.digests;

import org.bouncycastle.util.Arrays;

public class CSHAKEDigest extends SHAKEDigest
{
    private static final byte[] padding;
    private final byte[] diff;
    
    static {
        padding = new byte[100];
    }
    
    public CSHAKEDigest(final int n, final byte[] array, final byte[] array2) {
        super(n);
        if ((array == null || array.length == 0) && (array2 == null || array2.length == 0)) {
            this.diff = null;
            return;
        }
        this.diff = Arrays.concatenate(leftEncode(this.rate / 8), this.encodeString(array), this.encodeString(array2));
        this.diffPadAndAbsorb();
    }
    
    private void diffPadAndAbsorb() {
        int n = this.rate / 8;
        final byte[] diff = this.diff;
        this.absorb(diff, 0, diff.length);
        int length = this.diff.length % n;
        byte[] padding;
        while (true) {
            n -= length;
            padding = CSHAKEDigest.padding;
            if (n <= padding.length) {
                break;
            }
            this.absorb(padding, 0, padding.length);
            length = CSHAKEDigest.padding.length;
        }
        this.absorb(padding, 0, n);
    }
    
    private byte[] encodeString(final byte[] array) {
        if (array != null && array.length != 0) {
            return Arrays.concatenate(leftEncode(array.length * 8L), array);
        }
        return leftEncode(0L);
    }
    
    private static byte[] leftEncode(final long n) {
        int i = 1;
        long n2 = n;
        byte b = 1;
        while (true) {
            n2 >>= 8;
            if (n2 == 0L) {
                break;
            }
            ++b;
        }
        final byte[] array = new byte[b + 1];
        array[0] = b;
        while (i <= b) {
            array[i] = (byte)(n >> (b - i) * 8);
            ++i;
        }
        return array;
    }
    
    @Override
    public int doOutput(final byte[] array, final int n, final int n2) {
        if (this.diff != null) {
            if (!this.squeezing) {
                this.absorbBits(0, 2);
            }
            this.squeeze(array, n, n2 * 8L);
            return n2;
        }
        return super.doOutput(array, n, n2);
    }
    
    @Override
    public void reset() {
        super.reset();
        if (this.diff != null) {
            this.diffPadAndAbsorb();
        }
    }
}
