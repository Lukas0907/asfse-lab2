// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto.digests;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.util.Pack;
import org.bouncycastle.crypto.ExtendedDigest;

public class KeccakDigest implements ExtendedDigest
{
    private static long[] KeccakRoundConstants;
    protected int bitsInQueue;
    protected byte[] dataQueue;
    protected int fixedOutputLength;
    protected int rate;
    protected boolean squeezing;
    protected long[] state;
    
    static {
        KeccakDigest.KeccakRoundConstants = new long[] { 1L, 32898L, -9223372036854742902L, -9223372034707259392L, 32907L, 2147483649L, -9223372034707259263L, -9223372036854743031L, 138L, 136L, 2147516425L, 2147483658L, 2147516555L, -9223372036854775669L, -9223372036854742903L, -9223372036854743037L, -9223372036854743038L, -9223372036854775680L, 32778L, -9223372034707292150L, -9223372034707259263L, -9223372036854742912L, 2147483649L, -9223372034707259384L };
    }
    
    public KeccakDigest() {
        this(288);
    }
    
    public KeccakDigest(final int n) {
        this.state = new long[25];
        this.dataQueue = new byte[192];
        this.init(n);
    }
    
    public KeccakDigest(final KeccakDigest keccakDigest) {
        this.state = new long[25];
        this.dataQueue = new byte[192];
        final long[] state = keccakDigest.state;
        System.arraycopy(state, 0, this.state, 0, state.length);
        final byte[] dataQueue = keccakDigest.dataQueue;
        System.arraycopy(dataQueue, 0, this.dataQueue, 0, dataQueue.length);
        this.rate = keccakDigest.rate;
        this.bitsInQueue = keccakDigest.bitsInQueue;
        this.fixedOutputLength = keccakDigest.fixedOutputLength;
        this.squeezing = keccakDigest.squeezing;
    }
    
    private void KeccakAbsorb(final byte[] array, int i) {
        final int rate = this.rate;
        final int n = 0;
        int n2 = i;
        long[] state;
        for (i = n; i < rate >> 6; ++i) {
            state = this.state;
            state[i] ^= Pack.littleEndianToLong(array, n2);
            n2 += 8;
        }
        this.KeccakPermutation();
    }
    
    private void KeccakExtract() {
        Pack.longToLittleEndian(this.state, 0, this.rate >> 6, this.dataQueue, 0);
    }
    
    private void KeccakPermutation() {
        final long[] state = this.state;
        long n = state[0];
        long n2 = state[1];
        long n3 = state[2];
        long n4 = state[3];
        long n5 = state[4];
        long n6 = state[5];
        long n7 = state[6];
        long n8 = state[7];
        long n9 = state[8];
        long n10 = state[9];
        long n11 = state[10];
        long n12 = state[11];
        long n13 = state[12];
        long n14 = state[13];
        long n15 = state[14];
        long n16 = state[15];
        long n17 = state[16];
        long n18 = state[17];
        long n19 = state[18];
        long n20 = state[19];
        long n21 = state[20];
        long n22 = state[21];
        long n23 = state[22];
        long n24 = state[23];
        long n25 = state[24];
        long n62;
        long n67;
        long n68;
        long n71;
        long n72;
        long n73;
        long n75;
        long n78;
        long n80;
        long n81;
        long n82;
        for (int i = 0; i < 24; ++i, n18 = (n82 ^ (n81 & n71)), n16 = (n73 ^ (n78 & n82)), n2 = ((n68 & n80) ^ n62), n24 = (n72 ^ (n75 & n67))) {
            final long n26 = n ^ n6 ^ n11 ^ n16 ^ n21;
            final long n27 = n2 ^ n7 ^ n12 ^ n17 ^ n22;
            final long n28 = n3 ^ n8 ^ n13 ^ n18 ^ n23;
            final long n29 = n4 ^ n9 ^ n14 ^ n19 ^ n24;
            final long n30 = n5 ^ n10 ^ n15 ^ n20 ^ n25;
            final long n31 = (n27 << 1 | n27 >>> -1) ^ n30;
            final long n32 = (n28 << 1 | n28 >>> -1) ^ n26;
            final long n33 = (n29 << 1 | n29 >>> -1) ^ n27;
            final long n34 = (n30 << 1 | n30 >>> -1) ^ n28;
            final long n35 = (n26 << 1 | n26 >>> -1) ^ n29;
            final long n36 = n ^ n31;
            final long n37 = n6 ^ n31;
            final long n38 = n11 ^ n31;
            final long n39 = n16 ^ n31;
            final long n40 = n21 ^ n31;
            final long n41 = n2 ^ n32;
            final long n42 = n7 ^ n32;
            final long n43 = n12 ^ n32;
            final long n44 = n17 ^ n32;
            final long n45 = n22 ^ n32;
            final long n46 = n3 ^ n33;
            final long n47 = n8 ^ n33;
            final long n48 = n13 ^ n33;
            final long n49 = n18 ^ n33;
            final long n50 = n23 ^ n33;
            final long n51 = n4 ^ n34;
            final long n52 = n9 ^ n34;
            final long n53 = n14 ^ n34;
            final long n54 = n19 ^ n34;
            final long n55 = n24 ^ n34;
            final long n56 = n5 ^ n35;
            final long n57 = n10 ^ n35;
            final long n58 = n15 ^ n35;
            final long n59 = n20 ^ n35;
            final long n60 = n25 ^ n35;
            final long n61 = n41 << 1 | n41 >>> 63;
            n62 = (n42 << 44 | n42 >>> 20);
            final long n63 = n57 << 20 | n57 >>> 44;
            final long n64 = n50 << 61 | n50 >>> 3;
            final long n65 = n58 << 39 | n58 >>> 25;
            final long n66 = n40 << 18 | n40 >>> 46;
            n67 = (n46 << 62 | n46 >>> 2);
            n68 = (n48 << 43 | n48 >>> 21);
            final long n69 = n53 << 25 | n53 >>> 39;
            final long n70 = n59 << 8 | n59 >>> 56;
            n71 = (n55 << 56 | n55 >>> 8);
            n72 = (n39 << 41 | n39 >>> 23);
            n73 = (n56 << 27 | n56 >>> 37);
            final long n74 = n60 << 14 | n60 >>> 50;
            n75 = (n45 << 2 | n45 >>> 62);
            final long n76 = n52 << 55 | n52 >>> 9;
            final long n77 = n44 << 45 | n44 >>> 19;
            n78 = (n37 << 36 | n37 >>> 28);
            final long n79 = n51 << 28 | n51 >>> 36;
            n80 = (n54 << 21 | n54 >>> 43);
            n81 = (n49 << 15 | n49 >>> 49);
            n82 = (n43 << 10 | n43 >>> 54);
            final long n83 = n47 << 6 | n47 >>> 58;
            final long n84 = n38 << 3 | n38 >>> 61;
            n3 = (n68 ^ (n80 & n74));
            n4 = ((n74 & n36) ^ n80);
            n11 = (n61 ^ (n83 & n69));
            n21 = (n67 ^ (n76 & n65));
            n25 = (n75 ^ (n67 & n76));
            final long n85 = KeccakDigest.KeccakRoundConstants[i];
            n13 = ((n70 & n66) ^ n69);
            n14 = (n70 ^ (n66 & n61));
            n6 = (n79 ^ (n63 & n84));
            n20 = ((n73 & n78) ^ n71);
            n10 = ((n63 & n79) ^ n64);
            n19 = ((n71 & n73) ^ n81);
            n12 = ((n69 & n70) ^ n83);
            n15 = ((n61 & n83) ^ n66);
            n22 = ((n65 & n72) ^ n76);
            n17 = ((n82 & n81) ^ n78);
            n8 = ((n77 & n64) ^ n84);
            n23 = ((n72 & n75) ^ n65);
            n7 = ((n84 & n77) ^ n63);
            n9 = ((n64 & n79) ^ n77);
            n5 = (n74 ^ (n62 & n36));
            n = ((n62 & n68) ^ n36 ^ n85);
        }
        state[0] = n;
        state[1] = n2;
        state[2] = n3;
        state[3] = n4;
        state[4] = n5;
        state[5] = n6;
        state[6] = n7;
        state[7] = n8;
        state[8] = n9;
        state[9] = n10;
        state[10] = n11;
        state[11] = n12;
        state[12] = n13;
        state[13] = n14;
        state[14] = n15;
        state[15] = n16;
        state[16] = n17;
        state[17] = n18;
        state[18] = n19;
        state[19] = n20;
        state[20] = n21;
        state[21] = n22;
        state[22] = n23;
        state[23] = n24;
        state[24] = n25;
    }
    
    private void init(final int n) {
        if (n != 128 && n != 224 && n != 256 && n != 288 && n != 384 && n != 512) {
            throw new IllegalArgumentException("bitLength must be one of 128, 224, 256, 288, 384, or 512.");
        }
        this.initSponge(1600 - (n << 1));
    }
    
    private void initSponge(final int rate) {
        if (rate > 0 && rate < 1600 && rate % 64 == 0) {
            this.rate = rate;
            int n = 0;
            while (true) {
                final long[] state = this.state;
                if (n >= state.length) {
                    break;
                }
                state[n] = 0L;
                ++n;
            }
            Arrays.fill(this.dataQueue, (byte)0);
            this.bitsInQueue = 0;
            this.squeezing = false;
            this.fixedOutputLength = (1600 - rate) / 2;
            return;
        }
        throw new IllegalStateException("invalid rate value");
    }
    
    private void padAndSwitchToSqueezingPhase() {
        final byte[] dataQueue = this.dataQueue;
        final int bitsInQueue = this.bitsInQueue;
        final int n = bitsInQueue >> 3;
        dataQueue[n] |= (byte)(1L << (bitsInQueue & 0x7));
        final int bitsInQueue2 = bitsInQueue + 1;
        this.bitsInQueue = bitsInQueue2;
        final int rate = this.rate;
        int i = 0;
        if (bitsInQueue2 == rate) {
            this.KeccakAbsorb(dataQueue, 0);
            this.bitsInQueue = 0;
        }
        final int bitsInQueue3 = this.bitsInQueue;
        final int n2 = bitsInQueue3 >> 6;
        final int n3 = bitsInQueue3 & 0x3F;
        int n4 = 0;
        while (i < n2) {
            final long[] state = this.state;
            state[i] ^= Pack.littleEndianToLong(this.dataQueue, n4);
            n4 += 8;
            ++i;
        }
        if (n3 > 0) {
            final long[] state2 = this.state;
            state2[n2] ^= (Pack.littleEndianToLong(this.dataQueue, n4) & (1L << n3) - 1L);
        }
        final long[] state3 = this.state;
        final int n5 = this.rate - 1 >> 6;
        state3[n5] ^= Long.MIN_VALUE;
        this.KeccakPermutation();
        this.KeccakExtract();
        this.bitsInQueue = this.rate;
        this.squeezing = true;
    }
    
    protected void absorb(final byte[] array, final int n, final int n2) {
        final int bitsInQueue = this.bitsInQueue;
        if (bitsInQueue % 8 != 0) {
            throw new IllegalStateException("attempt to absorb with odd length queue");
        }
        if (!this.squeezing) {
            final int n3 = this.rate >> 3;
            int n4 = bitsInQueue >> 3;
            int i = 0;
            while (i < n2) {
                if (n4 == 0) {
                    final int n5 = n2 - n3;
                    if (i <= n5) {
                        int n6;
                        do {
                            this.KeccakAbsorb(array, n + i);
                            n6 = i + n3;
                        } while ((i = n6) <= n5);
                        i = n6;
                        continue;
                    }
                }
                final int min = Math.min(n3 - n4, n2 - i);
                System.arraycopy(array, n + i, this.dataQueue, n4, min);
                final int n7 = n4 + min;
                final int n8 = i += min;
                if ((n4 = n7) == n3) {
                    this.KeccakAbsorb(this.dataQueue, 0);
                    n4 = 0;
                    i = n8;
                }
            }
            this.bitsInQueue = n4 << 3;
            return;
        }
        throw new IllegalStateException("attempt to absorb while squeezing");
    }
    
    protected void absorbBits(final int n, final int n2) {
        if (n2 < 1 || n2 > 7) {
            throw new IllegalArgumentException("'bits' must be in the range 1 to 7");
        }
        final int bitsInQueue = this.bitsInQueue;
        if (bitsInQueue % 8 != 0) {
            throw new IllegalStateException("attempt to absorb with odd length queue");
        }
        if (!this.squeezing) {
            this.dataQueue[bitsInQueue >> 3] = (byte)(n & (1 << n2) - 1);
            this.bitsInQueue = bitsInQueue + n2;
            return;
        }
        throw new IllegalStateException("attempt to absorb while squeezing");
    }
    
    @Override
    public int doFinal(final byte[] array, final int n) {
        this.squeeze(array, n, this.fixedOutputLength);
        this.reset();
        return this.getDigestSize();
    }
    
    protected int doFinal(final byte[] array, final int n, final byte b, final int n2) {
        if (n2 > 0) {
            this.absorbBits(b, n2);
        }
        this.squeeze(array, n, this.fixedOutputLength);
        this.reset();
        return this.getDigestSize();
    }
    
    @Override
    public String getAlgorithmName() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Keccak-");
        sb.append(this.fixedOutputLength);
        return sb.toString();
    }
    
    @Override
    public int getByteLength() {
        return this.rate / 8;
    }
    
    @Override
    public int getDigestSize() {
        return this.fixedOutputLength / 8;
    }
    
    @Override
    public void reset() {
        this.init(this.fixedOutputLength);
    }
    
    protected void squeeze(final byte[] array, final int n, final long n2) {
        if (!this.squeezing) {
            this.padAndSwitchToSqueezingPhase();
        }
        long n3 = 0L;
        if (n2 % 8L == 0L) {
            while (n3 < n2) {
                if (this.bitsInQueue == 0) {
                    this.KeccakPermutation();
                    this.KeccakExtract();
                    this.bitsInQueue = this.rate;
                }
                final int n4 = (int)Math.min(this.bitsInQueue, n2 - n3);
                System.arraycopy(this.dataQueue, (this.rate - this.bitsInQueue) / 8, array, (int)(n3 / 8L) + n, n4 / 8);
                this.bitsInQueue -= n4;
                n3 += n4;
            }
            return;
        }
        throw new IllegalStateException("outputLength not a multiple of 8");
    }
    
    @Override
    public void update(final byte b) {
        this.absorb(new byte[] { b }, 0, 1);
    }
    
    @Override
    public void update(final byte[] array, final int n, final int n2) {
        this.absorb(array, n, n2);
    }
}
