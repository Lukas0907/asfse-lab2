// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.crypto;

public class BufferedAsymmetricBlockCipher
{
    protected byte[] buf;
    protected int bufOff;
    private final AsymmetricBlockCipher cipher;
    
    public BufferedAsymmetricBlockCipher(final AsymmetricBlockCipher cipher) {
        this.cipher = cipher;
    }
    
    public byte[] doFinal() throws InvalidCipherTextException {
        final byte[] processBlock = this.cipher.processBlock(this.buf, 0, this.bufOff);
        this.reset();
        return processBlock;
    }
    
    public int getBufferPosition() {
        return this.bufOff;
    }
    
    public int getInputBlockSize() {
        return this.cipher.getInputBlockSize();
    }
    
    public int getOutputBlockSize() {
        return this.cipher.getOutputBlockSize();
    }
    
    public AsymmetricBlockCipher getUnderlyingCipher() {
        return this.cipher;
    }
    
    public void init(final boolean b, final CipherParameters cipherParameters) {
        throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge Z and I\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.useAs(TypeTransformer.java:868)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e2expr(TypeTransformer.java:598)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:716)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e1expr(TypeTransformer.java:539)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:713)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s2stmt(TypeTransformer.java:820)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:843)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
    }
    
    public void processByte(final byte b) {
        final int bufOff = this.bufOff;
        final byte[] buf = this.buf;
        if (bufOff < buf.length) {
            this.bufOff = bufOff + 1;
            buf[bufOff] = b;
            return;
        }
        throw new DataLengthException("attempt to process message too long for cipher");
    }
    
    public void processBytes(final byte[] array, final int n, final int n2) {
        if (n2 == 0) {
            return;
        }
        if (n2 < 0) {
            throw new IllegalArgumentException("Can't have a negative input length!");
        }
        final int bufOff = this.bufOff;
        final byte[] buf = this.buf;
        if (bufOff + n2 <= buf.length) {
            System.arraycopy(array, n, buf, bufOff, n2);
            this.bufOff += n2;
            return;
        }
        throw new DataLengthException("attempt to process message too long for cipher");
    }
    
    public void reset() {
        if (this.buf != null) {
            int n = 0;
            while (true) {
                final byte[] buf = this.buf;
                if (n >= buf.length) {
                    break;
                }
                buf[n] = 0;
                ++n;
            }
        }
        this.bufOff = 0;
    }
}
