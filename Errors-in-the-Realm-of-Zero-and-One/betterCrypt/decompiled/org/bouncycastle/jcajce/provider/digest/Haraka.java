// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.digest;

import org.bouncycastle.jcajce.provider.config.ConfigurableProvider;
import org.bouncycastle.crypto.digests.Haraka512Digest;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.Haraka256Digest;

public class Haraka
{
    private Haraka() {
    }
    
    public static class Digest256 extends BCMessageDigest implements Cloneable
    {
        public Digest256() {
            super(new Haraka256Digest());
        }
        
        @Override
        public Object clone() throws CloneNotSupportedException {
            final Digest256 digest256 = (Digest256)super.clone();
            digest256.digest = new Haraka256Digest((Haraka256Digest)this.digest);
            return digest256;
        }
    }
    
    public static class Digest512 extends BCMessageDigest implements Cloneable
    {
        public Digest512() {
            super(new Haraka512Digest());
        }
        
        @Override
        public Object clone() throws CloneNotSupportedException {
            final Digest512 digest512 = (Digest512)super.clone();
            digest512.digest = new Haraka512Digest((Haraka512Digest)this.digest);
            return digest512;
        }
    }
    
    public static class Mappings extends DigestAlgorithmProvider
    {
        private static final String PREFIX;
        
        static {
            PREFIX = Haraka.class.getName();
        }
        
        @Override
        public void configure(final ConfigurableProvider configurableProvider) {
            final StringBuilder sb = new StringBuilder();
            sb.append(Mappings.PREFIX);
            sb.append("$Digest256");
            configurableProvider.addAlgorithm("MessageDigest.HARAKA-256", sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(Mappings.PREFIX);
            sb2.append("$Digest512");
            configurableProvider.addAlgorithm("MessageDigest.HARAKA-512", sb2.toString());
        }
    }
}
