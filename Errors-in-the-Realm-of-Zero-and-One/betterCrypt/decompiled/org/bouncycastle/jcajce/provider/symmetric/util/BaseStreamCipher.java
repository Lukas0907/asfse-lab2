// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.symmetric.util;

import org.bouncycastle.crypto.DataLengthException;
import javax.crypto.NoSuchPaddingException;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidParameterException;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.jcajce.PKCS12KeyWithParameters;
import org.bouncycastle.jcajce.PKCS12Key;
import javax.crypto.SecretKey;
import java.security.InvalidKeyException;
import java.security.InvalidAlgorithmParameterException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.security.AlgorithmParameters;
import java.security.Key;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.RC5ParameterSpec;
import javax.crypto.spec.RC2ParameterSpec;
import javax.crypto.spec.PBEParameterSpec;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.StreamCipher;

public class BaseStreamCipher extends BaseWrapCipher implements PBE
{
    private Class[] availableSpecs;
    private StreamCipher cipher;
    private int digest;
    private int ivLength;
    private ParametersWithIV ivParam;
    private int keySizeInBits;
    private String pbeAlgorithm;
    private PBEParameterSpec pbeSpec;
    
    protected BaseStreamCipher(final StreamCipher streamCipher, final int n) {
        this(streamCipher, n, -1, -1);
    }
    
    protected BaseStreamCipher(final StreamCipher streamCipher, final int n, final int n2) {
        this(streamCipher, n, n2, -1);
    }
    
    protected BaseStreamCipher(final StreamCipher cipher, final int ivLength, final int keySizeInBits, final int digest) {
        this.availableSpecs = new Class[] { RC2ParameterSpec.class, RC5ParameterSpec.class, IvParameterSpec.class, PBEParameterSpec.class };
        this.ivLength = 0;
        this.pbeSpec = null;
        this.pbeAlgorithm = null;
        this.cipher = cipher;
        this.ivLength = ivLength;
        this.keySizeInBits = keySizeInBits;
        this.digest = digest;
    }
    
    @Override
    protected int engineDoFinal(final byte[] array, final int n, final int n2, final byte[] array2, final int n3) throws ShortBufferException {
        if (n3 + n2 <= array2.length) {
            if (n2 != 0) {
                this.cipher.processBytes(array, n, n2, array2, n3);
            }
            this.cipher.reset();
            return n2;
        }
        throw new ShortBufferException("output buffer too short for input.");
    }
    
    @Override
    protected byte[] engineDoFinal(byte[] engineUpdate, final int n, final int n2) {
        if (n2 != 0) {
            engineUpdate = this.engineUpdate(engineUpdate, n, n2);
            this.cipher.reset();
            return engineUpdate;
        }
        this.cipher.reset();
        return new byte[0];
    }
    
    @Override
    protected int engineGetBlockSize() {
        return 0;
    }
    
    @Override
    protected byte[] engineGetIV() {
        final ParametersWithIV ivParam = this.ivParam;
        if (ivParam != null) {
            return ivParam.getIV();
        }
        return null;
    }
    
    @Override
    protected int engineGetKeySize(final Key key) {
        return key.getEncoded().length * 8;
    }
    
    @Override
    protected int engineGetOutputSize(final int n) {
        return n;
    }
    
    @Override
    protected AlgorithmParameters engineGetParameters() {
        Label_0205: {
            if (this.engineParams != null) {
                break Label_0205;
            }
            Label_0035: {
                if (this.pbeSpec == null) {
                    break Label_0035;
                }
            Label_0033_Outer:
                while (true) {
                    while (true) {
                        try {
                            final AlgorithmParameters parametersInstance = this.createParametersInstance(this.pbeAlgorithm);
                            parametersInstance.init(this.pbeSpec);
                            return parametersInstance;
                            // iftrue(Label_0205:, this.ivParam == null)
                            // iftrue(Label_0159:, !s.startsWith("HC"))
                            // iftrue(Label_0090:, !s.startsWith("ChaCha7539"))
                            // iftrue(Label_0105:, !s.startsWith("Grain"))
                            while (true) {
                                Block_4: {
                                    break Block_4;
                                    final String s2;
                                    final String s = s2.substring(0, s2.indexOf(47));
                                    String string;
                                    int index;
                                    StringBuilder sb;
                                    Block_6_Outer:Block_7_Outer:
                                    while (true) {
                                        Block_8: {
                                            while (true) {
                                                while (true) {
                                                    Label_0075: {
                                                        break Label_0075;
                                                        Label_0105: {
                                                            string = s;
                                                        }
                                                        break Block_8;
                                                        return null;
                                                        while (true) {
                                                            try {
                                                                (this.engineParams = this.createParametersInstance(string)).init(new IvParameterSpec(this.ivParam.getIV()));
                                                            }
                                                            catch (Exception ex) {
                                                                throw new RuntimeException(ex.toString());
                                                            }
                                                            return this.engineParams;
                                                            string = "ChaCha7539";
                                                            continue Block_6_Outer;
                                                            string = "Grainv1";
                                                            continue Block_6_Outer;
                                                        }
                                                        return this.engineParams;
                                                    }
                                                    continue Block_7_Outer;
                                                }
                                                Label_0090: {
                                                    continue;
                                                }
                                            }
                                        }
                                        index = s.indexOf(45);
                                        sb = new StringBuilder();
                                        sb.append(s.substring(0, index));
                                        sb.append(s.substring(index + 1));
                                        string = sb.toString();
                                        continue;
                                    }
                                }
                                String s;
                                final String s2 = s = this.cipher.getAlgorithmName();
                                continue Label_0033_Outer;
                            }
                        }
                        // iftrue(Label_0075:, s2.indexOf(47) < 0)
                        catch (Exception ex2) {}
                        continue;
                    }
                }
            }
        }
    }
    
    @Override
    protected void engineInit(final int n, Key key, final AlgorithmParameters engineParams, final SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        AlgorithmParameterSpec parameterSpec = null;
        final AlgorithmParameterSpec algorithmParameterSpec = null;
        while (true) {
            while (true) {
                int n2 = 0;
                Label_0013: {
                    if (engineParams != null) {
                        n2 = 0;
                        break Label_0013;
                    }
                    Label_0098: {
                        break Label_0098;
                        while (true) {
                            while (true) {
                                try {
                                    final Class[] availableSpecs;
                                    parameterSpec = engineParams.getParameterSpec((Class<AlgorithmParameterSpec>)availableSpecs[n2]);
                                    if (parameterSpec != null) {
                                        this.engineInit(n, key, parameterSpec, secureRandom);
                                        this.engineParams = engineParams;
                                        return;
                                    }
                                    key = (Key)new StringBuilder();
                                    ((StringBuilder)key).append("can't handle parameter ");
                                    ((StringBuilder)key).append(engineParams.toString());
                                    throw new InvalidAlgorithmParameterException(((StringBuilder)key).toString());
                                    ++n2;
                                    break;
                                }
                                catch (Exception ex) {}
                                continue;
                            }
                        }
                    }
                }
                final Class[] availableSpecs = this.availableSpecs;
                parameterSpec = algorithmParameterSpec;
                if (n2 != availableSpecs.length) {
                    continue;
                }
                break;
            }
            continue;
        }
    }
    
    @Override
    protected void engineInit(final int n, final Key key, final SecureRandom secureRandom) throws InvalidKeyException {
        try {
            this.engineInit(n, key, (AlgorithmParameterSpec)null, secureRandom);
        }
        catch (InvalidAlgorithmParameterException ex) {
            throw new InvalidKeyException(ex.getMessage());
        }
    }
    
    @Override
    protected void engineInit(final int i, final Key key, final AlgorithmParameterSpec algorithmParameterSpec, final SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        this.pbeSpec = null;
        this.pbeAlgorithm = null;
        this.engineParams = null;
        while (true) {
            Label_0547: {
                if (!(key instanceof SecretKey)) {
                    break Label_0547;
                }
                Label_0536: {
                    CipherParameters pbeParameters;
                    if (key instanceof PKCS12Key) {
                        final PKCS12Key pkcs12Key = (PKCS12Key)key;
                        this.pbeSpec = (PBEParameterSpec)algorithmParameterSpec;
                        if (pkcs12Key instanceof PKCS12KeyWithParameters && this.pbeSpec == null) {
                            final PKCS12KeyWithParameters pkcs12KeyWithParameters = (PKCS12KeyWithParameters)pkcs12Key;
                            this.pbeSpec = new PBEParameterSpec(pkcs12KeyWithParameters.getSalt(), pkcs12KeyWithParameters.getIterationCount());
                        }
                        pbeParameters = Util.makePBEParameters(pkcs12Key.getEncoded(), 2, this.digest, this.keySizeInBits, this.ivLength * 8, this.pbeSpec, this.cipher.getAlgorithmName());
                    }
                    else if (key instanceof BCPBEKey) {
                        final BCPBEKey bcpbeKey = (BCPBEKey)key;
                        String pbeAlgorithm;
                        if (bcpbeKey.getOID() != null) {
                            pbeAlgorithm = bcpbeKey.getOID().getId();
                        }
                        else {
                            pbeAlgorithm = bcpbeKey.getAlgorithm();
                        }
                        this.pbeAlgorithm = pbeAlgorithm;
                        CipherParameters param;
                        if (bcpbeKey.getParam() != null) {
                            param = bcpbeKey.getParam();
                            this.pbeSpec = new PBEParameterSpec(bcpbeKey.getSalt(), bcpbeKey.getIterationCount());
                        }
                        else {
                            if (!(algorithmParameterSpec instanceof PBEParameterSpec)) {
                                throw new InvalidAlgorithmParameterException("PBE requires PBE parameters to be set.");
                            }
                            final CipherParameters pbeParameters2 = Util.makePBEParameters(bcpbeKey, algorithmParameterSpec, this.cipher.getAlgorithmName());
                            this.pbeSpec = (PBEParameterSpec)algorithmParameterSpec;
                            param = pbeParameters2;
                        }
                        pbeParameters = param;
                        if (bcpbeKey.getIvSize() != 0) {
                            this.ivParam = (ParametersWithIV)param;
                            pbeParameters = param;
                        }
                    }
                    else if (algorithmParameterSpec == null) {
                        if (this.digest > 0) {
                            throw new InvalidKeyException("Algorithm requires a PBE key");
                        }
                        pbeParameters = new KeyParameter(key.getEncoded());
                    }
                    else {
                        if (!(algorithmParameterSpec instanceof IvParameterSpec)) {
                            break Label_0536;
                        }
                        pbeParameters = new ParametersWithIV(new KeyParameter(key.getEncoded()), ((IvParameterSpec)algorithmParameterSpec).getIV());
                        this.ivParam = (ParametersWithIV)pbeParameters;
                    }
                    ParametersWithIV parametersWithIV = (ParametersWithIV)pbeParameters;
                    if (this.ivLength != 0) {
                        parametersWithIV = (ParametersWithIV)pbeParameters;
                        if (!(pbeParameters instanceof ParametersWithIV)) {
                            SecureRandom secureRandom2;
                            if ((secureRandom2 = secureRandom) == null) {
                                secureRandom2 = CryptoServicesRegistrar.getSecureRandom();
                            }
                            if (i != 1 && i != 3) {
                                throw new InvalidAlgorithmParameterException("no IV set when one expected");
                            }
                            final byte[] bytes = new byte[this.ivLength];
                            secureRandom2.nextBytes(bytes);
                            parametersWithIV = new ParametersWithIV(pbeParameters, bytes);
                            this.ivParam = parametersWithIV;
                        }
                    }
                    Label_0512: {
                        if (i == 1) {
                            break Label_0512;
                        }
                        Label_0500: {
                            if (i == 2) {
                                break Label_0500;
                            }
                            if (i == 3) {
                                break Label_0512;
                            }
                            if (i == 4) {
                                break Label_0500;
                            }
                            try {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("unknown opmode ");
                                sb.append(i);
                                sb.append(" passed");
                                throw new InvalidParameterException(sb.toString());
                                final Exception ex;
                                throw new InvalidKeyException(ex.getMessage());
                                this.cipher.init(true, parametersWithIV);
                                return;
                                this.cipher.init(false, parametersWithIV);
                                return;
                                throw new InvalidAlgorithmParameterException("unknown parameter type.");
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("Key for algorithm ");
                                sb2.append(key.getAlgorithm());
                                sb2.append(" not suitable for symmetric enryption.");
                                throw new InvalidKeyException(sb2.toString());
                            }
                            catch (Exception ex2) {}
                        }
                    }
                }
            }
            final Exception ex2;
            final Exception ex = ex2;
            continue;
        }
    }
    
    @Override
    protected void engineSetMode(final String str) throws NoSuchAlgorithmException {
        if (str.equalsIgnoreCase("ECB")) {
            return;
        }
        if (str.equals("NONE")) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("can't support mode ");
        sb.append(str);
        throw new NoSuchAlgorithmException(sb.toString());
    }
    
    @Override
    protected void engineSetPadding(final String str) throws NoSuchPaddingException {
        if (str.equalsIgnoreCase("NoPadding")) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Padding ");
        sb.append(str);
        sb.append(" unknown.");
        throw new NoSuchPaddingException(sb.toString());
    }
    
    @Override
    protected int engineUpdate(final byte[] array, final int n, final int n2, final byte[] array2, final int n3) throws ShortBufferException {
        if (n3 + n2 <= array2.length) {
            try {
                this.cipher.processBytes(array, n, n2, array2, n3);
                return n2;
            }
            catch (DataLengthException ex) {
                throw new IllegalStateException(ex.getMessage());
            }
        }
        throw new ShortBufferException("output buffer too short for input.");
    }
    
    @Override
    protected byte[] engineUpdate(final byte[] array, final int n, final int n2) {
        final byte[] array2 = new byte[n2];
        this.cipher.processBytes(array, n, n2, array2, 0);
        return array2;
    }
}
