// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.symmetric;

import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.jcajce.provider.config.ConfigurableProvider;
import org.bouncycastle.jcajce.provider.util.AlgorithmProvider;
import org.bouncycastle.crypto.CipherKeyGenerator;
import org.bouncycastle.jcajce.provider.symmetric.util.BaseKeyGenerator;
import org.bouncycastle.crypto.modes.AEADCipher;
import org.bouncycastle.crypto.modes.ChaCha20Poly1305;
import org.bouncycastle.jcajce.provider.symmetric.util.BaseBlockCipher;
import org.bouncycastle.crypto.engines.ChaCha7539Engine;
import org.bouncycastle.crypto.StreamCipher;
import org.bouncycastle.crypto.engines.ChaChaEngine;
import org.bouncycastle.jcajce.provider.symmetric.util.BaseStreamCipher;
import org.bouncycastle.jcajce.provider.symmetric.util.IvAlgorithmParameters;

public final class ChaCha
{
    private ChaCha() {
    }
    
    public static class AlgParams extends IvAlgorithmParameters
    {
        @Override
        protected String engineToString() {
            return "ChaCha7539 IV";
        }
    }
    
    public static class AlgParamsCC1305 extends IvAlgorithmParameters
    {
        @Override
        protected String engineToString() {
            return "ChaCha20-Poly1305 IV";
        }
    }
    
    public static class Base extends BaseStreamCipher
    {
        public Base() {
            super(new ChaChaEngine(), 8);
        }
    }
    
    public static class Base7539 extends BaseStreamCipher
    {
        public Base7539() {
            super(new ChaCha7539Engine(), 12);
        }
    }
    
    public static class BaseCC20P1305 extends BaseBlockCipher
    {
        public BaseCC20P1305() {
            super(new ChaCha20Poly1305(), true, 12);
        }
    }
    
    public static class KeyGen extends BaseKeyGenerator
    {
        public KeyGen() {
            super("ChaCha", 128, new CipherKeyGenerator());
        }
    }
    
    public static class KeyGen7539 extends BaseKeyGenerator
    {
        public KeyGen7539() {
            super("ChaCha7539", 256, new CipherKeyGenerator());
        }
    }
    
    public static class Mappings extends AlgorithmProvider
    {
        private static final String PREFIX;
        
        static {
            PREFIX = ChaCha.class.getName();
        }
        
        @Override
        public void configure(final ConfigurableProvider configurableProvider) {
            final StringBuilder sb = new StringBuilder();
            sb.append(Mappings.PREFIX);
            sb.append("$Base");
            configurableProvider.addAlgorithm("Cipher.CHACHA", sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(Mappings.PREFIX);
            sb2.append("$KeyGen");
            configurableProvider.addAlgorithm("KeyGenerator.CHACHA", sb2.toString());
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(Mappings.PREFIX);
            sb3.append("$Base7539");
            configurableProvider.addAlgorithm("Cipher.CHACHA7539", sb3.toString());
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(Mappings.PREFIX);
            sb4.append("$KeyGen7539");
            configurableProvider.addAlgorithm("KeyGenerator.CHACHA7539", sb4.toString());
            final StringBuilder sb5 = new StringBuilder();
            sb5.append(Mappings.PREFIX);
            sb5.append("$AlgParams");
            configurableProvider.addAlgorithm("AlgorithmParameters.CHACHA7539", sb5.toString());
            configurableProvider.addAlgorithm("Alg.Alias.Cipher.CHACHA20", "CHACHA7539");
            configurableProvider.addAlgorithm("Alg.Alias.KeyGenerator.CHACHA20", "CHACHA7539");
            configurableProvider.addAlgorithm("Alg.Alias.AlgorithmParameters.CHACHA20", "CHACHA7539");
            configurableProvider.addAlgorithm("Alg.Alias.KeyGenerator.CHACHA20-POLY1305", "CHACHA7539");
            final StringBuilder sb6 = new StringBuilder();
            sb6.append("Alg.Alias.KeyGenerator.");
            sb6.append(PKCSObjectIdentifiers.id_alg_AEADChaCha20Poly1305);
            configurableProvider.addAlgorithm(sb6.toString(), "CHACHA7539");
            final StringBuilder sb7 = new StringBuilder();
            sb7.append(Mappings.PREFIX);
            sb7.append("$BaseCC20P1305");
            configurableProvider.addAlgorithm("Cipher.CHACHA20-POLY1305", sb7.toString());
            final StringBuilder sb8 = new StringBuilder();
            sb8.append(Mappings.PREFIX);
            sb8.append("$AlgParamsCC1305");
            configurableProvider.addAlgorithm("AlgorithmParameters.CHACHA20-POLY1305", sb8.toString());
            final StringBuilder sb9 = new StringBuilder();
            sb9.append("Alg.Alias.Cipher.");
            sb9.append(PKCSObjectIdentifiers.id_alg_AEADChaCha20Poly1305);
            configurableProvider.addAlgorithm(sb9.toString(), "CHACHA20-POLY1305");
            final StringBuilder sb10 = new StringBuilder();
            sb10.append("Alg.Alias.AlgorithmParameters.");
            sb10.append(PKCSObjectIdentifiers.id_alg_AEADChaCha20Poly1305);
            configurableProvider.addAlgorithm(sb10.toString(), "CHACHA20-POLY1305");
            final StringBuilder sb11 = new StringBuilder();
            sb11.append("Alg.Alias.Cipher.OID.");
            sb11.append(PKCSObjectIdentifiers.id_alg_AEADChaCha20Poly1305);
            configurableProvider.addAlgorithm(sb11.toString(), "CHACHA20-POLY1305");
            final StringBuilder sb12 = new StringBuilder();
            sb12.append("Alg.Alias.AlgorithmParameters.OID.");
            sb12.append(PKCSObjectIdentifiers.id_alg_AEADChaCha20Poly1305);
            configurableProvider.addAlgorithm(sb12.toString(), "CHACHA20-POLY1305");
        }
    }
}
