// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.symmetric.util;

import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.InvalidCipherTextException;
import java.lang.reflect.Constructor;
import org.bouncycastle.util.Arrays;
import java.nio.ByteBuffer;
import org.bouncycastle.crypto.paddings.ISO10126d2Padding;
import org.bouncycastle.crypto.paddings.X923Padding;
import org.bouncycastle.crypto.paddings.ISO7816d4Padding;
import org.bouncycastle.crypto.paddings.TBCPadding;
import org.bouncycastle.crypto.paddings.BlockCipherPadding;
import org.bouncycastle.crypto.paddings.ZeroBytePadding;
import javax.crypto.NoSuchPaddingException;
import org.bouncycastle.crypto.modes.GCMBlockCipher;
import org.bouncycastle.crypto.modes.KGCMBlockCipher;
import org.bouncycastle.crypto.modes.EAXBlockCipher;
import java.security.NoSuchAlgorithmException;
import org.bouncycastle.crypto.modes.OCBBlockCipher;
import org.bouncycastle.crypto.modes.CCMBlockCipher;
import org.bouncycastle.crypto.modes.KCCMBlockCipher;
import org.bouncycastle.crypto.modes.CTSBlockCipher;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.modes.GCFBBlockCipher;
import org.bouncycastle.crypto.modes.GOFBBlockCipher;
import org.bouncycastle.crypto.modes.KCTRBlockCipher;
import org.bouncycastle.crypto.engines.DSTU7624Engine;
import org.bouncycastle.crypto.modes.SICBlockCipher;
import org.bouncycastle.crypto.modes.OpenPGPCFBBlockCipher;
import org.bouncycastle.crypto.modes.PGPCFBBlockCipher;
import org.bouncycastle.crypto.modes.CFBBlockCipher;
import org.bouncycastle.crypto.modes.OFBBlockCipher;
import org.bouncycastle.util.Strings;
import java.security.InvalidKeyException;
import java.security.InvalidAlgorithmParameterException;
import java.security.SecureRandom;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.cms.GCMParameters;
import java.security.AlgorithmParameters;
import java.security.Key;
import javax.crypto.BadPaddingException;
import javax.crypto.ShortBufferException;
import org.bouncycastle.crypto.OutputLengthException;
import org.bouncycastle.crypto.DataLengthException;
import javax.crypto.IllegalBlockSizeException;
import org.bouncycastle.crypto.params.ParametersWithSBox;
import org.bouncycastle.crypto.CipherParameters;
import java.security.spec.AlgorithmParameterSpec;
import org.bouncycastle.crypto.modes.AEADCipher;
import org.bouncycastle.crypto.modes.AEADBlockCipher;
import org.bouncycastle.crypto.BufferedBlockCipher;
import javax.crypto.spec.IvParameterSpec;
import org.bouncycastle.jcajce.spec.GOST28147ParameterSpec;
import javax.crypto.spec.RC5ParameterSpec;
import javax.crypto.spec.RC2ParameterSpec;
import javax.crypto.spec.PBEParameterSpec;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.BlockCipher;
import org.bouncycastle.crypto.params.AEADParameters;

public class BaseBlockCipher extends BaseWrapCipher implements PBE
{
    private static final int BUF_SIZE = 512;
    private static final Class gcmSpecClass;
    private AEADParameters aeadParams;
    private Class[] availableSpecs;
    private BlockCipher baseEngine;
    private GenericBlockCipher cipher;
    private int digest;
    private BlockCipherProvider engineProvider;
    private boolean fixedIv;
    private int ivLength;
    private ParametersWithIV ivParam;
    private int keySizeInBits;
    private String modeName;
    private boolean padded;
    private String pbeAlgorithm;
    private PBEParameterSpec pbeSpec;
    private int scheme;
    
    static {
        gcmSpecClass = ClassUtil.loadClass(BaseBlockCipher.class, "javax.crypto.spec.GCMParameterSpec");
    }
    
    protected BaseBlockCipher(final BlockCipher baseEngine) {
        this.availableSpecs = new Class[] { RC2ParameterSpec.class, RC5ParameterSpec.class, BaseBlockCipher.gcmSpecClass, GOST28147ParameterSpec.class, IvParameterSpec.class, PBEParameterSpec.class };
        this.scheme = -1;
        this.ivLength = 0;
        this.fixedIv = true;
        this.pbeSpec = null;
        this.pbeAlgorithm = null;
        this.modeName = null;
        this.baseEngine = baseEngine;
        this.cipher = (GenericBlockCipher)new BufferedGenericBlockCipher(baseEngine);
    }
    
    protected BaseBlockCipher(final BlockCipher blockCipher, final int n) {
        this(blockCipher, true, n);
    }
    
    protected BaseBlockCipher(final BlockCipher baseEngine, final int scheme, final int digest, final int keySizeInBits, final int ivLength) {
        this.availableSpecs = new Class[] { RC2ParameterSpec.class, RC5ParameterSpec.class, BaseBlockCipher.gcmSpecClass, GOST28147ParameterSpec.class, IvParameterSpec.class, PBEParameterSpec.class };
        this.scheme = -1;
        this.ivLength = 0;
        this.fixedIv = true;
        this.pbeSpec = null;
        this.pbeAlgorithm = null;
        this.modeName = null;
        this.baseEngine = baseEngine;
        this.scheme = scheme;
        this.digest = digest;
        this.keySizeInBits = keySizeInBits;
        this.ivLength = ivLength;
        this.cipher = (GenericBlockCipher)new BufferedGenericBlockCipher(baseEngine);
    }
    
    protected BaseBlockCipher(final BlockCipher baseEngine, final boolean fixedIv, final int n) {
        this.availableSpecs = new Class[] { RC2ParameterSpec.class, RC5ParameterSpec.class, BaseBlockCipher.gcmSpecClass, GOST28147ParameterSpec.class, IvParameterSpec.class, PBEParameterSpec.class };
        this.scheme = -1;
        this.ivLength = 0;
        this.fixedIv = true;
        this.pbeSpec = null;
        this.pbeAlgorithm = null;
        this.modeName = null;
        this.baseEngine = baseEngine;
        this.fixedIv = fixedIv;
        this.cipher = (GenericBlockCipher)new BufferedGenericBlockCipher(baseEngine);
        this.ivLength = n / 8;
    }
    
    protected BaseBlockCipher(final BufferedBlockCipher bufferedBlockCipher, final int n) {
        this(bufferedBlockCipher, true, n);
    }
    
    protected BaseBlockCipher(final BufferedBlockCipher bufferedBlockCipher, final boolean fixedIv, final int n) {
        this.availableSpecs = new Class[] { RC2ParameterSpec.class, RC5ParameterSpec.class, BaseBlockCipher.gcmSpecClass, GOST28147ParameterSpec.class, IvParameterSpec.class, PBEParameterSpec.class };
        this.scheme = -1;
        this.ivLength = 0;
        this.fixedIv = true;
        this.pbeSpec = null;
        this.pbeAlgorithm = null;
        this.modeName = null;
        this.baseEngine = bufferedBlockCipher.getUnderlyingCipher();
        this.cipher = (GenericBlockCipher)new BufferedGenericBlockCipher(bufferedBlockCipher);
        this.fixedIv = fixedIv;
        this.ivLength = n / 8;
    }
    
    protected BaseBlockCipher(final AEADBlockCipher aeadBlockCipher) {
        this.availableSpecs = new Class[] { RC2ParameterSpec.class, RC5ParameterSpec.class, BaseBlockCipher.gcmSpecClass, GOST28147ParameterSpec.class, IvParameterSpec.class, PBEParameterSpec.class };
        this.scheme = -1;
        this.ivLength = 0;
        this.fixedIv = true;
        this.pbeSpec = null;
        this.pbeAlgorithm = null;
        this.modeName = null;
        this.baseEngine = aeadBlockCipher.getUnderlyingCipher();
        this.ivLength = this.baseEngine.getBlockSize();
        this.cipher = (GenericBlockCipher)new AEADGenericBlockCipher(aeadBlockCipher);
    }
    
    protected BaseBlockCipher(final AEADBlockCipher aeadBlockCipher, final boolean fixedIv, final int ivLength) {
        this.availableSpecs = new Class[] { RC2ParameterSpec.class, RC5ParameterSpec.class, BaseBlockCipher.gcmSpecClass, GOST28147ParameterSpec.class, IvParameterSpec.class, PBEParameterSpec.class };
        this.scheme = -1;
        this.ivLength = 0;
        this.fixedIv = true;
        this.pbeSpec = null;
        this.pbeAlgorithm = null;
        this.modeName = null;
        this.baseEngine = aeadBlockCipher.getUnderlyingCipher();
        this.fixedIv = fixedIv;
        this.ivLength = ivLength;
        this.cipher = (GenericBlockCipher)new AEADGenericBlockCipher(aeadBlockCipher);
    }
    
    protected BaseBlockCipher(final AEADCipher aeadCipher, final boolean fixedIv, final int ivLength) {
        this.availableSpecs = new Class[] { RC2ParameterSpec.class, RC5ParameterSpec.class, BaseBlockCipher.gcmSpecClass, GOST28147ParameterSpec.class, IvParameterSpec.class, PBEParameterSpec.class };
        this.scheme = -1;
        this.ivLength = 0;
        this.fixedIv = true;
        this.pbeSpec = null;
        this.pbeAlgorithm = null;
        this.modeName = null;
        this.baseEngine = null;
        this.fixedIv = fixedIv;
        this.ivLength = ivLength;
        this.cipher = (GenericBlockCipher)new AEADGenericBlockCipher(aeadCipher);
    }
    
    protected BaseBlockCipher(final BlockCipherProvider engineProvider) {
        this.availableSpecs = new Class[] { RC2ParameterSpec.class, RC5ParameterSpec.class, BaseBlockCipher.gcmSpecClass, GOST28147ParameterSpec.class, IvParameterSpec.class, PBEParameterSpec.class };
        this.scheme = -1;
        this.ivLength = 0;
        this.fixedIv = true;
        this.pbeSpec = null;
        this.pbeAlgorithm = null;
        this.modeName = null;
        this.baseEngine = engineProvider.get();
        this.engineProvider = engineProvider;
        this.cipher = (GenericBlockCipher)new BufferedGenericBlockCipher(engineProvider.get());
    }
    
    private CipherParameters adjustParameters(final AlgorithmParameterSpec algorithmParameterSpec, final CipherParameters cipherParameters) {
        if (cipherParameters instanceof ParametersWithIV) {
            final CipherParameters parameters = ((ParametersWithIV)cipherParameters).getParameters();
            if (algorithmParameterSpec instanceof IvParameterSpec) {
                this.ivParam = new ParametersWithIV(parameters, ((IvParameterSpec)algorithmParameterSpec).getIV());
            }
            else {
                final CipherParameters cipherParameters2 = cipherParameters;
                if (!(algorithmParameterSpec instanceof GOST28147ParameterSpec)) {
                    return cipherParameters2;
                }
                final GOST28147ParameterSpec gost28147ParameterSpec = (GOST28147ParameterSpec)algorithmParameterSpec;
                final ParametersWithSBox parametersWithSBox = new ParametersWithSBox(cipherParameters, gost28147ParameterSpec.getSbox());
                if (gost28147ParameterSpec.getIV() != null && this.ivLength != 0) {
                    return this.ivParam = new ParametersWithIV(parameters, gost28147ParameterSpec.getIV());
                }
                return parametersWithSBox;
            }
        }
        else if (algorithmParameterSpec instanceof IvParameterSpec) {
            this.ivParam = new ParametersWithIV(cipherParameters, ((IvParameterSpec)algorithmParameterSpec).getIV());
        }
        else {
            CipherParameters cipherParameters2 = cipherParameters;
            if (!(algorithmParameterSpec instanceof GOST28147ParameterSpec)) {
                return cipherParameters2;
            }
            final GOST28147ParameterSpec gost28147ParameterSpec2 = (GOST28147ParameterSpec)algorithmParameterSpec;
            cipherParameters2 = new ParametersWithSBox(cipherParameters, gost28147ParameterSpec2.getSbox());
            if (gost28147ParameterSpec2.getIV() != null && this.ivLength != 0) {
                return new ParametersWithIV(cipherParameters2, gost28147ParameterSpec2.getIV());
            }
            return cipherParameters2;
        }
        return this.ivParam;
    }
    
    private boolean isAEADModeName(final String s) {
        return "CCM".equals(s) || "EAX".equals(s) || "GCM".equals(s) || "OCB".equals(s);
    }
    
    @Override
    protected int engineDoFinal(final byte[] array, int processBytes, int doFinal, final byte[] array2, final int n) throws IllegalBlockSizeException, BadPaddingException, ShortBufferException {
        if (this.engineGetOutputSize(doFinal) + n <= array2.length) {
            Label_0038: {
                if (doFinal == 0) {
                    processBytes = 0;
                    break Label_0038;
                }
                try {
                    processBytes = this.cipher.processBytes(array, processBytes, doFinal, array2, n);
                    doFinal = this.cipher.doFinal(array2, n + processBytes);
                    return processBytes + doFinal;
                }
                catch (DataLengthException ex) {
                    throw new IllegalBlockSizeException(ex.getMessage());
                }
                catch (OutputLengthException ex2) {
                    throw new IllegalBlockSizeException(ex2.getMessage());
                }
            }
        }
        throw new ShortBufferException("output buffer too short for input.");
    }
    
    @Override
    protected byte[] engineDoFinal(byte[] array, int processBytes, int doFinal) throws IllegalBlockSizeException, BadPaddingException {
        final byte[] array2 = new byte[this.engineGetOutputSize(doFinal)];
        if (doFinal != 0) {
            processBytes = this.cipher.processBytes(array, processBytes, doFinal, array2, 0);
        }
        else {
            processBytes = 0;
        }
        try {
            doFinal = this.cipher.doFinal(array2, processBytes);
            processBytes += doFinal;
            if (processBytes == array2.length) {
                return array2;
            }
            array = new byte[processBytes];
            System.arraycopy(array2, 0, array, 0, processBytes);
            return array;
        }
        catch (DataLengthException ex) {
            throw new IllegalBlockSizeException(ex.getMessage());
        }
    }
    
    @Override
    protected int engineGetBlockSize() {
        final BlockCipher baseEngine = this.baseEngine;
        if (baseEngine == null) {
            return -1;
        }
        return baseEngine.getBlockSize();
    }
    
    @Override
    protected byte[] engineGetIV() {
        final AEADParameters aeadParams = this.aeadParams;
        if (aeadParams != null) {
            return aeadParams.getNonce();
        }
        final ParametersWithIV ivParam = this.ivParam;
        if (ivParam != null) {
            return ivParam.getIV();
        }
        return null;
    }
    
    @Override
    protected int engineGetKeySize(final Key key) {
        return key.getEncoded().length * 8;
    }
    
    @Override
    protected int engineGetOutputSize(final int n) {
        return this.cipher.getOutputSize(n);
    }
    
    @Override
    protected AlgorithmParameters engineGetParameters() {
        Label_0261: {
            if (this.engineParams != null) {
                break Label_0261;
            }
            Label_0042: {
                if (this.pbeSpec == null) {
                    break Label_0042;
                }
                String s;
                String s2;
                Block_7_Outer:Label_0040_Outer:
                while (true) {
                    while (true) {
                        try {
                            (this.engineParams = this.createParametersInstance(this.pbeAlgorithm)).init(this.pbeSpec);
                            return this.engineParams;
                            // iftrue(Label_0215:, s2.indexOf(47) < 0)
                            // iftrue(Label_0110:, this.baseEngine != null)
                            // iftrue(Label_0261:, this.ivParam == null)
                            while (true) {
                                while (true) {
                                Label_0170:
                                    while (true) {
                                        try {
                                            Label_0215: {
                                                (this.engineParams = this.createParametersInstance(s)).init(new IvParameterSpec(this.ivParam.getIV()));
                                            }
                                        }
                                        catch (Exception ex) {
                                            throw new RuntimeException(ex.toString());
                                        }
                                        return this.engineParams;
                                        s2 = (s = this.cipher.getUnderlyingCipher().getAlgorithmName());
                                        Block_8: {
                                            break Block_8;
                                            while (true) {
                                                try {
                                                    Label_0110: {
                                                        (this.engineParams = this.createParametersInstance("GCM")).init(new GCMParameters(this.aeadParams.getNonce(), this.aeadParams.getMacSize() / 8).getEncoded());
                                                    }
                                                    return this.engineParams;
                                                }
                                                catch (Exception ex2) {
                                                    throw new RuntimeException(ex2.toString());
                                                }
                                                break Label_0170;
                                                return null;
                                                try {
                                                    (this.engineParams = this.createParametersInstance(PKCSObjectIdentifiers.id_alg_AEADChaCha20Poly1305.getId())).init(new DEROctetString(this.aeadParams.getNonce()).getEncoded());
                                                    return this.engineParams;
                                                }
                                                catch (Exception ex3) {
                                                    throw new RuntimeException(ex3.toString());
                                                }
                                                continue Label_0040_Outer;
                                            }
                                        }
                                        s = s2.substring(0, s2.indexOf(47));
                                        continue Block_7_Outer;
                                    }
                                    continue Label_0040_Outer;
                                }
                                continue;
                            }
                        }
                        // iftrue(Label_0170:, this.aeadParams == null)
                        catch (Exception ex4) {}
                        continue;
                    }
                }
            }
        }
    }
    
    @Override
    protected void engineInit(final int n, Key key, final AlgorithmParameters engineParams, final SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        AlgorithmParameterSpec parameterSpec = null;
        final AlgorithmParameterSpec algorithmParameterSpec = null;
        Label_0110: {
            if (engineParams == null) {
                break Label_0110;
            }
            int n2 = 0;
        Label_0056_Outer:
            while (true) {
                final Class[] availableSpecs = this.availableSpecs;
                parameterSpec = algorithmParameterSpec;
                Label_0065: {
                    if (n2 == availableSpecs.length) {
                        break Label_0065;
                    }
                    while (true) {
                        if (availableSpecs[n2] == null) {
                            break Label_0056;
                        }
                        try {
                            parameterSpec = engineParams.getParameterSpec((Class<AlgorithmParameterSpec>)availableSpecs[n2]);
                            if (parameterSpec != null) {
                                this.engineInit(n, key, parameterSpec, secureRandom);
                                this.engineParams = engineParams;
                                return;
                            }
                            key = (Key)new StringBuilder();
                            ((StringBuilder)key).append("can't handle parameter ");
                            ((StringBuilder)key).append(engineParams.toString());
                            throw new InvalidAlgorithmParameterException(((StringBuilder)key).toString());
                            ++n2;
                            continue Label_0056_Outer;
                        }
                        catch (Exception ex) {
                            continue;
                        }
                        break;
                    }
                }
                break;
            }
        }
    }
    
    @Override
    protected void engineInit(final int n, final Key key, final SecureRandom secureRandom) throws InvalidKeyException {
        try {
            this.engineInit(n, key, (AlgorithmParameterSpec)null, secureRandom);
        }
        catch (InvalidAlgorithmParameterException ex) {
            throw new InvalidKeyException(ex.getMessage());
        }
    }
    
    @Override
    protected void engineInit(final int p0, final Key p1, final AlgorithmParameterSpec p2, final SecureRandom p3) throws InvalidKeyException, InvalidAlgorithmParameterException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          8
        //     3: aconst_null    
        //     4: astore          7
        //     6: aload_0        
        //     7: aconst_null    
        //     8: putfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;
        //    11: aload_0        
        //    12: aconst_null    
        //    13: putfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.pbeAlgorithm:Ljava/lang/String;
        //    16: aload_0        
        //    17: aconst_null    
        //    18: putfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.engineParams:Ljava/security/AlgorithmParameters;
        //    21: aload_0        
        //    22: aconst_null    
        //    23: putfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.aeadParams:Lorg/bouncycastle/crypto/params/AEADParameters;
        //    26: aload_2        
        //    27: instanceof      Ljavax/crypto/SecretKey;
        //    30: ifne            94
        //    33: new             Ljava/lang/StringBuilder;
        //    36: dup            
        //    37: invokespecial   java/lang/StringBuilder.<init>:()V
        //    40: astore          4
        //    42: aload           4
        //    44: ldc_w           "Key for algorithm "
        //    47: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    50: pop            
        //    51: aload           7
        //    53: astore_3       
        //    54: aload_2        
        //    55: ifnull          65
        //    58: aload_2        
        //    59: invokeinterface java/security/Key.getAlgorithm:()Ljava/lang/String;
        //    64: astore_3       
        //    65: aload           4
        //    67: aload_3        
        //    68: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    71: pop            
        //    72: aload           4
        //    74: ldc_w           " not suitable for symmetric enryption."
        //    77: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    80: pop            
        //    81: new             Ljava/security/InvalidKeyException;
        //    84: dup            
        //    85: aload           4
        //    87: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    90: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //    93: athrow         
        //    94: aload_3        
        //    95: ifnonnull       139
        //    98: aload_0        
        //    99: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.baseEngine:Lorg/bouncycastle/crypto/BlockCipher;
        //   102: astore          7
        //   104: aload           7
        //   106: ifnull          139
        //   109: aload           7
        //   111: invokeinterface org/bouncycastle/crypto/BlockCipher.getAlgorithmName:()Ljava/lang/String;
        //   116: ldc_w           "RC5-64"
        //   119: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   122: ifne            128
        //   125: goto            139
        //   128: new             Ljava/security/InvalidAlgorithmParameterException;
        //   131: dup            
        //   132: ldc_w           "RC5 requires an RC5ParametersSpec to be passed in."
        //   135: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;)V
        //   138: athrow         
        //   139: aload_0        
        //   140: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.scheme:I
        //   143: istore          5
        //   145: iload           5
        //   147: iconst_2       
        //   148: if_icmpeq       623
        //   151: aload_2        
        //   152: instanceof      Lorg/bouncycastle/jcajce/PKCS12Key;
        //   155: ifeq            161
        //   158: goto            623
        //   161: aload_2        
        //   162: instanceof      Lorg/bouncycastle/jcajce/PBKDF1Key;
        //   165: ifeq            299
        //   168: aload_2        
        //   169: checkcast       Lorg/bouncycastle/jcajce/PBKDF1Key;
        //   172: astore          7
        //   174: aload_3        
        //   175: instanceof      Ljavax/crypto/spec/PBEParameterSpec;
        //   178: ifeq            189
        //   181: aload_0        
        //   182: aload_3        
        //   183: checkcast       Ljavax/crypto/spec/PBEParameterSpec;
        //   186: putfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;
        //   189: aload           7
        //   191: instanceof      Lorg/bouncycastle/jcajce/PBKDF1KeyWithParameters;
        //   194: ifeq            232
        //   197: aload_0        
        //   198: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;
        //   201: ifnonnull       232
        //   204: aload           7
        //   206: checkcast       Lorg/bouncycastle/jcajce/PBKDF1KeyWithParameters;
        //   209: astore          8
        //   211: aload_0        
        //   212: new             Ljavax/crypto/spec/PBEParameterSpec;
        //   215: dup            
        //   216: aload           8
        //   218: invokevirtual   org/bouncycastle/jcajce/PBKDF1KeyWithParameters.getSalt:()[B
        //   221: aload           8
        //   223: invokevirtual   org/bouncycastle/jcajce/PBKDF1KeyWithParameters.getIterationCount:()I
        //   226: invokespecial   javax/crypto/spec/PBEParameterSpec.<init>:([BI)V
        //   229: putfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;
        //   232: aload           7
        //   234: invokevirtual   org/bouncycastle/jcajce/PBKDF1Key.getEncoded:()[B
        //   237: iconst_0       
        //   238: aload_0        
        //   239: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.digest:I
        //   242: aload_0        
        //   243: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.keySizeInBits:I
        //   246: aload_0        
        //   247: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.ivLength:I
        //   250: bipush          8
        //   252: imul           
        //   253: aload_0        
        //   254: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;
        //   257: aload_0        
        //   258: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.cipher:Lorg/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher;
        //   261: invokeinterface org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher.getAlgorithmName:()Ljava/lang/String;
        //   266: invokestatic    org/bouncycastle/jcajce/provider/symmetric/util/PBE$Util.makePBEParameters:([BIIIILjava/security/spec/AlgorithmParameterSpec;Ljava/lang/String;)Lorg/bouncycastle/crypto/CipherParameters;
        //   269: astore          8
        //   271: aload           8
        //   273: astore          7
        //   275: aload           8
        //   277: instanceof      Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //   280: ifeq            896
        //   283: aload           8
        //   285: astore          7
        //   287: aload_0        
        //   288: aload           7
        //   290: checkcast       Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //   293: putfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.ivParam:Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //   296: goto            896
        //   299: aload_2        
        //   300: instanceof      Lorg/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;
        //   303: ifeq            436
        //   306: aload_2        
        //   307: checkcast       Lorg/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;
        //   310: astore          8
        //   312: aload           8
        //   314: invokevirtual   org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey.getOID:()Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //   317: ifnull          333
        //   320: aload           8
        //   322: invokevirtual   org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey.getOID:()Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //   325: invokevirtual   org/bouncycastle/asn1/ASN1ObjectIdentifier.getId:()Ljava/lang/String;
        //   328: astore          7
        //   330: goto            340
        //   333: aload           8
        //   335: invokevirtual   org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey.getAlgorithm:()Ljava/lang/String;
        //   338: astore          7
        //   340: aload_0        
        //   341: aload           7
        //   343: putfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.pbeAlgorithm:Ljava/lang/String;
        //   346: aload           8
        //   348: invokevirtual   org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey.getParam:()Lorg/bouncycastle/crypto/CipherParameters;
        //   351: ifnull          369
        //   354: aload_0        
        //   355: aload_3        
        //   356: aload           8
        //   358: invokevirtual   org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey.getParam:()Lorg/bouncycastle/crypto/CipherParameters;
        //   361: invokespecial   org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.adjustParameters:(Ljava/security/spec/AlgorithmParameterSpec;Lorg/bouncycastle/crypto/CipherParameters;)Lorg/bouncycastle/crypto/CipherParameters;
        //   364: astore          8
        //   366: goto            406
        //   369: aload_3        
        //   370: instanceof      Ljavax/crypto/spec/PBEParameterSpec;
        //   373: ifeq            425
        //   376: aload_0        
        //   377: aload_3        
        //   378: checkcast       Ljavax/crypto/spec/PBEParameterSpec;
        //   381: putfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;
        //   384: aload           8
        //   386: aload_3        
        //   387: aload_0        
        //   388: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.cipher:Lorg/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher;
        //   391: invokeinterface org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher.getUnderlyingCipher:()Lorg/bouncycastle/crypto/BlockCipher;
        //   396: invokeinterface org/bouncycastle/crypto/BlockCipher.getAlgorithmName:()Ljava/lang/String;
        //   401: invokestatic    org/bouncycastle/jcajce/provider/symmetric/util/PBE$Util.makePBEParameters:(Lorg/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;Ljava/security/spec/AlgorithmParameterSpec;Ljava/lang/String;)Lorg/bouncycastle/crypto/CipherParameters;
        //   404: astore          8
        //   406: aload           8
        //   408: astore          7
        //   410: aload           8
        //   412: instanceof      Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //   415: ifeq            896
        //   418: aload           8
        //   420: astore          7
        //   422: goto            287
        //   425: new             Ljava/security/InvalidAlgorithmParameterException;
        //   428: dup            
        //   429: ldc_w           "PBE requires PBE parameters to be set."
        //   432: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;)V
        //   435: athrow         
        //   436: aload_2        
        //   437: instanceof      Ljavax/crypto/interfaces/PBEKey;
        //   440: ifeq            560
        //   443: aload_2        
        //   444: checkcast       Ljavax/crypto/interfaces/PBEKey;
        //   447: astore          7
        //   449: aload_0        
        //   450: aload_3        
        //   451: checkcast       Ljavax/crypto/spec/PBEParameterSpec;
        //   454: putfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;
        //   457: aload           7
        //   459: instanceof      Lorg/bouncycastle/jcajce/PKCS12KeyWithParameters;
        //   462: ifeq            497
        //   465: aload_0        
        //   466: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;
        //   469: ifnonnull       497
        //   472: aload_0        
        //   473: new             Ljavax/crypto/spec/PBEParameterSpec;
        //   476: dup            
        //   477: aload           7
        //   479: invokeinterface javax/crypto/interfaces/PBEKey.getSalt:()[B
        //   484: aload           7
        //   486: invokeinterface javax/crypto/interfaces/PBEKey.getIterationCount:()I
        //   491: invokespecial   javax/crypto/spec/PBEParameterSpec.<init>:([BI)V
        //   494: putfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;
        //   497: aload           7
        //   499: invokeinterface javax/crypto/interfaces/PBEKey.getEncoded:()[B
        //   504: aload_0        
        //   505: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.scheme:I
        //   508: aload_0        
        //   509: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.digest:I
        //   512: aload_0        
        //   513: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.keySizeInBits:I
        //   516: aload_0        
        //   517: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.ivLength:I
        //   520: bipush          8
        //   522: imul           
        //   523: aload_0        
        //   524: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;
        //   527: aload_0        
        //   528: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.cipher:Lorg/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher;
        //   531: invokeinterface org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher.getAlgorithmName:()Ljava/lang/String;
        //   536: invokestatic    org/bouncycastle/jcajce/provider/symmetric/util/PBE$Util.makePBEParameters:([BIIIILjava/security/spec/AlgorithmParameterSpec;Ljava/lang/String;)Lorg/bouncycastle/crypto/CipherParameters;
        //   539: astore          8
        //   541: aload           8
        //   543: astore          7
        //   545: aload           8
        //   547: instanceof      Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //   550: ifeq            896
        //   553: aload           8
        //   555: astore          7
        //   557: goto            287
        //   560: aload           8
        //   562: astore          7
        //   564: aload_2        
        //   565: instanceof      Lorg/bouncycastle/jcajce/spec/RepeatedSecretKeySpec;
        //   568: ifne            896
        //   571: iload           5
        //   573: ifeq            612
        //   576: iload           5
        //   578: iconst_4       
        //   579: if_icmpeq       612
        //   582: iload           5
        //   584: iconst_1       
        //   585: if_icmpeq       612
        //   588: iload           5
        //   590: iconst_5       
        //   591: if_icmpeq       612
        //   594: new             Lorg/bouncycastle/crypto/params/KeyParameter;
        //   597: dup            
        //   598: aload_2        
        //   599: invokeinterface java/security/Key.getEncoded:()[B
        //   604: invokespecial   org/bouncycastle/crypto/params/KeyParameter.<init>:([B)V
        //   607: astore          7
        //   609: goto            896
        //   612: new             Ljava/security/InvalidKeyException;
        //   615: dup            
        //   616: ldc_w           "Algorithm requires a PBE key"
        //   619: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   622: athrow         
        //   623: aload_2        
        //   624: checkcast       Ljavax/crypto/SecretKey;
        //   627: astore          8
        //   629: aload_3        
        //   630: instanceof      Ljavax/crypto/spec/PBEParameterSpec;
        //   633: ifeq            644
        //   636: aload_0        
        //   637: aload_3        
        //   638: checkcast       Ljavax/crypto/spec/PBEParameterSpec;
        //   641: putfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;
        //   644: aload           8
        //   646: instanceof      Ljavax/crypto/interfaces/PBEKey;
        //   649: istore          6
        //   651: iload           6
        //   653: ifeq            719
        //   656: aload_0        
        //   657: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;
        //   660: ifnonnull       719
        //   663: aload           8
        //   665: checkcast       Ljavax/crypto/interfaces/PBEKey;
        //   668: astore          7
        //   670: aload           7
        //   672: invokeinterface javax/crypto/interfaces/PBEKey.getSalt:()[B
        //   677: ifnull          708
        //   680: aload_0        
        //   681: new             Ljavax/crypto/spec/PBEParameterSpec;
        //   684: dup            
        //   685: aload           7
        //   687: invokeinterface javax/crypto/interfaces/PBEKey.getSalt:()[B
        //   692: aload           7
        //   694: invokeinterface javax/crypto/interfaces/PBEKey.getIterationCount:()I
        //   699: invokespecial   javax/crypto/spec/PBEParameterSpec.<init>:([BI)V
        //   702: putfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;
        //   705: goto            719
        //   708: new             Ljava/security/InvalidAlgorithmParameterException;
        //   711: dup            
        //   712: ldc_w           "PBEKey requires parameters to specify salt"
        //   715: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;)V
        //   718: athrow         
        //   719: aload_0        
        //   720: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;
        //   723: ifnonnull       745
        //   726: iload           6
        //   728: ifeq            734
        //   731: goto            745
        //   734: new             Ljava/security/InvalidKeyException;
        //   737: dup            
        //   738: ldc_w           "Algorithm requires a PBE key"
        //   741: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   744: athrow         
        //   745: aload_2        
        //   746: instanceof      Lorg/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;
        //   749: ifeq            836
        //   752: aload_2        
        //   753: checkcast       Lorg/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;
        //   756: invokevirtual   org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey.getParam:()Lorg/bouncycastle/crypto/CipherParameters;
        //   759: astore          7
        //   761: aload           7
        //   763: instanceof      Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //   766: ifeq            772
        //   769: goto            818
        //   772: aload           7
        //   774: ifnonnull       825
        //   777: aload           8
        //   779: invokeinterface javax/crypto/SecretKey.getEncoded:()[B
        //   784: iconst_2       
        //   785: aload_0        
        //   786: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.digest:I
        //   789: aload_0        
        //   790: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.keySizeInBits:I
        //   793: aload_0        
        //   794: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.ivLength:I
        //   797: bipush          8
        //   799: imul           
        //   800: aload_0        
        //   801: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;
        //   804: aload_0        
        //   805: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.cipher:Lorg/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher;
        //   808: invokeinterface org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher.getAlgorithmName:()Ljava/lang/String;
        //   813: invokestatic    org/bouncycastle/jcajce/provider/symmetric/util/PBE$Util.makePBEParameters:([BIIIILjava/security/spec/AlgorithmParameterSpec;Ljava/lang/String;)Lorg/bouncycastle/crypto/CipherParameters;
        //   816: astore          7
        //   818: aload           7
        //   820: astore          8
        //   822: goto            877
        //   825: new             Ljava/security/InvalidKeyException;
        //   828: dup            
        //   829: ldc_w           "Algorithm requires a PBE key suitable for PKCS12"
        //   832: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   835: athrow         
        //   836: aload           8
        //   838: invokeinterface javax/crypto/SecretKey.getEncoded:()[B
        //   843: iconst_2       
        //   844: aload_0        
        //   845: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.digest:I
        //   848: aload_0        
        //   849: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.keySizeInBits:I
        //   852: aload_0        
        //   853: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.ivLength:I
        //   856: bipush          8
        //   858: imul           
        //   859: aload_0        
        //   860: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.pbeSpec:Ljavax/crypto/spec/PBEParameterSpec;
        //   863: aload_0        
        //   864: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.cipher:Lorg/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher;
        //   867: invokeinterface org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher.getAlgorithmName:()Ljava/lang/String;
        //   872: invokestatic    org/bouncycastle/jcajce/provider/symmetric/util/PBE$Util.makePBEParameters:([BIIIILjava/security/spec/AlgorithmParameterSpec;Ljava/lang/String;)Lorg/bouncycastle/crypto/CipherParameters;
        //   875: astore          8
        //   877: aload           8
        //   879: astore          7
        //   881: aload           8
        //   883: instanceof      Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //   886: ifeq            896
        //   889: aload           8
        //   891: astore          7
        //   893: goto            287
        //   896: aload_3        
        //   897: instanceof      Lorg/bouncycastle/jcajce/spec/AEADParameterSpec;
        //   900: ifeq            1001
        //   903: aload_0        
        //   904: aload_0        
        //   905: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.modeName:Ljava/lang/String;
        //   908: invokespecial   org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.isAEADModeName:(Ljava/lang/String;)Z
        //   911: ifne            938
        //   914: aload_0        
        //   915: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.cipher:Lorg/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher;
        //   918: instanceof      Lorg/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$AEADGenericBlockCipher;
        //   921: ifeq            927
        //   924: goto            938
        //   927: new             Ljava/security/InvalidAlgorithmParameterException;
        //   930: dup            
        //   931: ldc_w           "AEADParameterSpec can only be used with AEAD modes."
        //   934: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;)V
        //   937: athrow         
        //   938: aload_3        
        //   939: checkcast       Lorg/bouncycastle/jcajce/spec/AEADParameterSpec;
        //   942: astore_3       
        //   943: aload           7
        //   945: instanceof      Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //   948: ifeq            966
        //   951: aload           7
        //   953: checkcast       Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //   956: invokevirtual   org/bouncycastle/crypto/params/ParametersWithIV.getParameters:()Lorg/bouncycastle/crypto/CipherParameters;
        //   959: checkcast       Lorg/bouncycastle/crypto/params/KeyParameter;
        //   962: astore_2       
        //   963: goto            972
        //   966: aload           7
        //   968: checkcast       Lorg/bouncycastle/crypto/params/KeyParameter;
        //   971: astore_2       
        //   972: new             Lorg/bouncycastle/crypto/params/AEADParameters;
        //   975: dup            
        //   976: aload_2        
        //   977: aload_3        
        //   978: invokevirtual   org/bouncycastle/jcajce/spec/AEADParameterSpec.getMacSizeInBits:()I
        //   981: aload_3        
        //   982: invokevirtual   org/bouncycastle/jcajce/spec/AEADParameterSpec.getNonce:()[B
        //   985: aload_3        
        //   986: invokevirtual   org/bouncycastle/jcajce/spec/AEADParameterSpec.getAssociatedData:()[B
        //   989: invokespecial   org/bouncycastle/crypto/params/AEADParameters.<init>:(Lorg/bouncycastle/crypto/params/KeyParameter;I[B[B)V
        //   992: astore_2       
        //   993: aload_0        
        //   994: aload_2        
        //   995: putfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.aeadParams:Lorg/bouncycastle/crypto/params/AEADParameters;
        //   998: goto            1879
        //  1001: aload_3        
        //  1002: instanceof      Ljavax/crypto/spec/IvParameterSpec;
        //  1005: ifeq            1192
        //  1008: aload_0        
        //  1009: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.ivLength:I
        //  1012: ifeq            1153
        //  1015: aload_3        
        //  1016: checkcast       Ljavax/crypto/spec/IvParameterSpec;
        //  1019: astore_2       
        //  1020: aload_2        
        //  1021: invokevirtual   javax/crypto/spec/IvParameterSpec.getIV:()[B
        //  1024: arraylength    
        //  1025: aload_0        
        //  1026: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.ivLength:I
        //  1029: if_icmpeq       1097
        //  1032: aload_0        
        //  1033: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.cipher:Lorg/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher;
        //  1036: instanceof      Lorg/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$AEADGenericBlockCipher;
        //  1039: ifne            1097
        //  1042: aload_0        
        //  1043: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.fixedIv:Z
        //  1046: ifne            1052
        //  1049: goto            1097
        //  1052: new             Ljava/lang/StringBuilder;
        //  1055: dup            
        //  1056: invokespecial   java/lang/StringBuilder.<init>:()V
        //  1059: astore_2       
        //  1060: aload_2        
        //  1061: ldc_w           "IV must be "
        //  1064: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1067: pop            
        //  1068: aload_2        
        //  1069: aload_0        
        //  1070: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.ivLength:I
        //  1073: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //  1076: pop            
        //  1077: aload_2        
        //  1078: ldc_w           " bytes long."
        //  1081: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1084: pop            
        //  1085: new             Ljava/security/InvalidAlgorithmParameterException;
        //  1088: dup            
        //  1089: aload_2        
        //  1090: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1093: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;)V
        //  1096: athrow         
        //  1097: aload           7
        //  1099: instanceof      Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1102: ifeq            1128
        //  1105: new             Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1108: dup            
        //  1109: aload           7
        //  1111: checkcast       Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1114: invokevirtual   org/bouncycastle/crypto/params/ParametersWithIV.getParameters:()Lorg/bouncycastle/crypto/CipherParameters;
        //  1117: aload_2        
        //  1118: invokevirtual   javax/crypto/spec/IvParameterSpec.getIV:()[B
        //  1121: invokespecial   org/bouncycastle/crypto/params/ParametersWithIV.<init>:(Lorg/bouncycastle/crypto/CipherParameters;[B)V
        //  1124: astore_2       
        //  1125: goto            1142
        //  1128: new             Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1131: dup            
        //  1132: aload           7
        //  1134: aload_2        
        //  1135: invokevirtual   javax/crypto/spec/IvParameterSpec.getIV:()[B
        //  1138: invokespecial   org/bouncycastle/crypto/params/ParametersWithIV.<init>:(Lorg/bouncycastle/crypto/CipherParameters;[B)V
        //  1141: astore_2       
        //  1142: aload_0        
        //  1143: aload_2        
        //  1144: checkcast       Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1147: putfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.ivParam:Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1150: goto            1879
        //  1153: aload_0        
        //  1154: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.modeName:Ljava/lang/String;
        //  1157: astore_3       
        //  1158: aload           7
        //  1160: astore_2       
        //  1161: aload_3        
        //  1162: ifnull          1879
        //  1165: aload_3        
        //  1166: ldc_w           "ECB"
        //  1169: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1172: ifne            1181
        //  1175: aload           7
        //  1177: astore_2       
        //  1178: goto            1879
        //  1181: new             Ljava/security/InvalidAlgorithmParameterException;
        //  1184: dup            
        //  1185: ldc_w           "ECB mode does not use an IV"
        //  1188: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;)V
        //  1191: athrow         
        //  1192: aload_3        
        //  1193: instanceof      Lorg/bouncycastle/jcajce/spec/GOST28147ParameterSpec;
        //  1196: ifeq            1305
        //  1199: aload_3        
        //  1200: checkcast       Lorg/bouncycastle/jcajce/spec/GOST28147ParameterSpec;
        //  1203: astore          7
        //  1205: new             Lorg/bouncycastle/crypto/params/ParametersWithSBox;
        //  1208: dup            
        //  1209: new             Lorg/bouncycastle/crypto/params/KeyParameter;
        //  1212: dup            
        //  1213: aload_2        
        //  1214: invokeinterface java/security/Key.getEncoded:()[B
        //  1219: invokespecial   org/bouncycastle/crypto/params/KeyParameter.<init>:([B)V
        //  1222: aload           7
        //  1224: invokevirtual   org/bouncycastle/jcajce/spec/GOST28147ParameterSpec.getSbox:()[B
        //  1227: invokespecial   org/bouncycastle/crypto/params/ParametersWithSBox.<init>:(Lorg/bouncycastle/crypto/CipherParameters;[B)V
        //  1230: astore_3       
        //  1231: aload_3        
        //  1232: astore_2       
        //  1233: aload           7
        //  1235: invokevirtual   org/bouncycastle/jcajce/spec/GOST28147ParameterSpec.getIV:()[B
        //  1238: ifnull          1879
        //  1241: aload_3        
        //  1242: astore_2       
        //  1243: aload_0        
        //  1244: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.ivLength:I
        //  1247: ifeq            1879
        //  1250: aload_3        
        //  1251: instanceof      Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1254: ifeq            1280
        //  1257: new             Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1260: dup            
        //  1261: aload_3        
        //  1262: checkcast       Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1265: invokevirtual   org/bouncycastle/crypto/params/ParametersWithIV.getParameters:()Lorg/bouncycastle/crypto/CipherParameters;
        //  1268: aload           7
        //  1270: invokevirtual   org/bouncycastle/jcajce/spec/GOST28147ParameterSpec.getIV:()[B
        //  1273: invokespecial   org/bouncycastle/crypto/params/ParametersWithIV.<init>:(Lorg/bouncycastle/crypto/CipherParameters;[B)V
        //  1276: astore_2       
        //  1277: goto            1294
        //  1280: new             Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1283: dup            
        //  1284: aload_3        
        //  1285: aload           7
        //  1287: invokevirtual   org/bouncycastle/jcajce/spec/GOST28147ParameterSpec.getIV:()[B
        //  1290: invokespecial   org/bouncycastle/crypto/params/ParametersWithIV.<init>:(Lorg/bouncycastle/crypto/CipherParameters;[B)V
        //  1293: astore_2       
        //  1294: aload_0        
        //  1295: aload_2        
        //  1296: checkcast       Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1299: putfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.ivParam:Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1302: goto            1879
        //  1305: aload_3        
        //  1306: instanceof      Ljavax/crypto/spec/RC2ParameterSpec;
        //  1309: ifeq            1403
        //  1312: aload_3        
        //  1313: checkcast       Ljavax/crypto/spec/RC2ParameterSpec;
        //  1316: astore          7
        //  1318: new             Lorg/bouncycastle/crypto/params/RC2Parameters;
        //  1321: dup            
        //  1322: aload_2        
        //  1323: invokeinterface java/security/Key.getEncoded:()[B
        //  1328: aload           7
        //  1330: invokevirtual   javax/crypto/spec/RC2ParameterSpec.getEffectiveKeyBits:()I
        //  1333: invokespecial   org/bouncycastle/crypto/params/RC2Parameters.<init>:([BI)V
        //  1336: astore_3       
        //  1337: aload_3        
        //  1338: astore_2       
        //  1339: aload           7
        //  1341: invokevirtual   javax/crypto/spec/RC2ParameterSpec.getIV:()[B
        //  1344: ifnull          1879
        //  1347: aload_3        
        //  1348: astore_2       
        //  1349: aload_0        
        //  1350: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.ivLength:I
        //  1353: ifeq            1879
        //  1356: aload_3        
        //  1357: instanceof      Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1360: ifeq            1386
        //  1363: new             Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1366: dup            
        //  1367: aload_3        
        //  1368: checkcast       Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1371: invokevirtual   org/bouncycastle/crypto/params/ParametersWithIV.getParameters:()Lorg/bouncycastle/crypto/CipherParameters;
        //  1374: aload           7
        //  1376: invokevirtual   javax/crypto/spec/RC2ParameterSpec.getIV:()[B
        //  1379: invokespecial   org/bouncycastle/crypto/params/ParametersWithIV.<init>:(Lorg/bouncycastle/crypto/CipherParameters;[B)V
        //  1382: astore_2       
        //  1383: goto            1294
        //  1386: new             Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1389: dup            
        //  1390: aload_3        
        //  1391: aload           7
        //  1393: invokevirtual   javax/crypto/spec/RC2ParameterSpec.getIV:()[B
        //  1396: invokespecial   org/bouncycastle/crypto/params/ParametersWithIV.<init>:(Lorg/bouncycastle/crypto/CipherParameters;[B)V
        //  1399: astore_2       
        //  1400: goto            1294
        //  1403: aload_3        
        //  1404: instanceof      Ljavax/crypto/spec/RC5ParameterSpec;
        //  1407: ifeq            1684
        //  1410: aload_3        
        //  1411: checkcast       Ljavax/crypto/spec/RC5ParameterSpec;
        //  1414: astore          7
        //  1416: new             Lorg/bouncycastle/crypto/params/RC5Parameters;
        //  1419: dup            
        //  1420: aload_2        
        //  1421: invokeinterface java/security/Key.getEncoded:()[B
        //  1426: aload           7
        //  1428: invokevirtual   javax/crypto/spec/RC5ParameterSpec.getRounds:()I
        //  1431: invokespecial   org/bouncycastle/crypto/params/RC5Parameters.<init>:([BI)V
        //  1434: astore_3       
        //  1435: aload_0        
        //  1436: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.baseEngine:Lorg/bouncycastle/crypto/BlockCipher;
        //  1439: invokeinterface org/bouncycastle/crypto/BlockCipher.getAlgorithmName:()Ljava/lang/String;
        //  1444: ldc_w           "RC5"
        //  1447: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //  1450: ifeq            1673
        //  1453: aload_0        
        //  1454: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.baseEngine:Lorg/bouncycastle/crypto/BlockCipher;
        //  1457: invokeinterface org/bouncycastle/crypto/BlockCipher.getAlgorithmName:()Ljava/lang/String;
        //  1462: ldc_w           "RC5-32"
        //  1465: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1468: ifeq            1530
        //  1471: aload           7
        //  1473: invokevirtual   javax/crypto/spec/RC5ParameterSpec.getWordSize:()I
        //  1476: bipush          32
        //  1478: if_icmpne       1484
        //  1481: goto            1607
        //  1484: new             Ljava/lang/StringBuilder;
        //  1487: dup            
        //  1488: invokespecial   java/lang/StringBuilder.<init>:()V
        //  1491: astore_2       
        //  1492: aload_2        
        //  1493: ldc_w           "RC5 already set up for a word size of 32 not "
        //  1496: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1499: pop            
        //  1500: aload_2        
        //  1501: aload           7
        //  1503: invokevirtual   javax/crypto/spec/RC5ParameterSpec.getWordSize:()I
        //  1506: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //  1509: pop            
        //  1510: aload_2        
        //  1511: ldc_w           "."
        //  1514: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1517: pop            
        //  1518: new             Ljava/security/InvalidAlgorithmParameterException;
        //  1521: dup            
        //  1522: aload_2        
        //  1523: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1526: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;)V
        //  1529: athrow         
        //  1530: aload_0        
        //  1531: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.baseEngine:Lorg/bouncycastle/crypto/BlockCipher;
        //  1534: invokeinterface org/bouncycastle/crypto/BlockCipher.getAlgorithmName:()Ljava/lang/String;
        //  1539: ldc_w           "RC5-64"
        //  1542: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1545: ifeq            1607
        //  1548: aload           7
        //  1550: invokevirtual   javax/crypto/spec/RC5ParameterSpec.getWordSize:()I
        //  1553: bipush          64
        //  1555: if_icmpne       1561
        //  1558: goto            1607
        //  1561: new             Ljava/lang/StringBuilder;
        //  1564: dup            
        //  1565: invokespecial   java/lang/StringBuilder.<init>:()V
        //  1568: astore_2       
        //  1569: aload_2        
        //  1570: ldc_w           "RC5 already set up for a word size of 64 not "
        //  1573: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1576: pop            
        //  1577: aload_2        
        //  1578: aload           7
        //  1580: invokevirtual   javax/crypto/spec/RC5ParameterSpec.getWordSize:()I
        //  1583: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //  1586: pop            
        //  1587: aload_2        
        //  1588: ldc_w           "."
        //  1591: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1594: pop            
        //  1595: new             Ljava/security/InvalidAlgorithmParameterException;
        //  1598: dup            
        //  1599: aload_2        
        //  1600: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1603: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;)V
        //  1606: athrow         
        //  1607: aload_3        
        //  1608: astore_2       
        //  1609: aload           7
        //  1611: invokevirtual   javax/crypto/spec/RC5ParameterSpec.getIV:()[B
        //  1614: ifnull          1879
        //  1617: aload_3        
        //  1618: astore_2       
        //  1619: aload_0        
        //  1620: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.ivLength:I
        //  1623: ifeq            1879
        //  1626: aload_3        
        //  1627: instanceof      Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1630: ifeq            1656
        //  1633: new             Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1636: dup            
        //  1637: aload_3        
        //  1638: checkcast       Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1641: invokevirtual   org/bouncycastle/crypto/params/ParametersWithIV.getParameters:()Lorg/bouncycastle/crypto/CipherParameters;
        //  1644: aload           7
        //  1646: invokevirtual   javax/crypto/spec/RC5ParameterSpec.getIV:()[B
        //  1649: invokespecial   org/bouncycastle/crypto/params/ParametersWithIV.<init>:(Lorg/bouncycastle/crypto/CipherParameters;[B)V
        //  1652: astore_2       
        //  1653: goto            1294
        //  1656: new             Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1659: dup            
        //  1660: aload_3        
        //  1661: aload           7
        //  1663: invokevirtual   javax/crypto/spec/RC5ParameterSpec.getIV:()[B
        //  1666: invokespecial   org/bouncycastle/crypto/params/ParametersWithIV.<init>:(Lorg/bouncycastle/crypto/CipherParameters;[B)V
        //  1669: astore_2       
        //  1670: goto            1294
        //  1673: new             Ljava/security/InvalidAlgorithmParameterException;
        //  1676: dup            
        //  1677: ldc_w           "RC5 parameters passed to a cipher that is not RC5."
        //  1680: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;)V
        //  1683: athrow         
        //  1684: getstatic       org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.gcmSpecClass:Ljava/lang/Class;
        //  1687: astore_2       
        //  1688: aload_2        
        //  1689: ifnull          1848
        //  1692: aload_2        
        //  1693: aload_3        
        //  1694: invokevirtual   java/lang/Class.isInstance:(Ljava/lang/Object;)Z
        //  1697: ifeq            1848
        //  1700: aload_0        
        //  1701: aload_0        
        //  1702: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.modeName:Ljava/lang/String;
        //  1705: invokespecial   org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.isAEADModeName:(Ljava/lang/String;)Z
        //  1708: ifne            1735
        //  1711: aload_0        
        //  1712: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.cipher:Lorg/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher;
        //  1715: instanceof      Lorg/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$AEADGenericBlockCipher;
        //  1718: ifeq            1724
        //  1721: goto            1735
        //  1724: new             Ljava/security/InvalidAlgorithmParameterException;
        //  1727: dup            
        //  1728: ldc_w           "GCMParameterSpec can only be used with AEAD modes."
        //  1731: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;)V
        //  1734: athrow         
        //  1735: getstatic       org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.gcmSpecClass:Ljava/lang/Class;
        //  1738: ldc_w           "getTLen"
        //  1741: iconst_0       
        //  1742: anewarray       Ljava/lang/Class;
        //  1745: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //  1748: astore          8
        //  1750: getstatic       org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.gcmSpecClass:Ljava/lang/Class;
        //  1753: ldc_w           "getIV"
        //  1756: iconst_0       
        //  1757: anewarray       Ljava/lang/Class;
        //  1760: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //  1763: astore          9
        //  1765: aload           7
        //  1767: astore_2       
        //  1768: aload           7
        //  1770: instanceof      Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1773: ifeq            1785
        //  1776: aload           7
        //  1778: checkcast       Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1781: invokevirtual   org/bouncycastle/crypto/params/ParametersWithIV.getParameters:()Lorg/bouncycastle/crypto/CipherParameters;
        //  1784: astore_2       
        //  1785: new             Lorg/bouncycastle/crypto/params/AEADParameters;
        //  1788: dup            
        //  1789: aload_2        
        //  1790: checkcast       Lorg/bouncycastle/crypto/params/KeyParameter;
        //  1793: aload           8
        //  1795: aload_3        
        //  1796: iconst_0       
        //  1797: anewarray       Ljava/lang/Object;
        //  1800: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //  1803: checkcast       Ljava/lang/Integer;
        //  1806: invokevirtual   java/lang/Integer.intValue:()I
        //  1809: aload           9
        //  1811: aload_3        
        //  1812: iconst_0       
        //  1813: anewarray       Ljava/lang/Object;
        //  1816: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //  1819: checkcast       [B
        //  1822: checkcast       [B
        //  1825: invokespecial   org/bouncycastle/crypto/params/AEADParameters.<init>:(Lorg/bouncycastle/crypto/params/KeyParameter;I[B)V
        //  1828: astore_2       
        //  1829: aload_0        
        //  1830: aload_2        
        //  1831: putfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.aeadParams:Lorg/bouncycastle/crypto/params/AEADParameters;
        //  1834: goto            1879
        //  1837: new             Ljava/security/InvalidAlgorithmParameterException;
        //  1840: dup            
        //  1841: ldc_w           "Cannot process GCMParameterSpec."
        //  1844: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;)V
        //  1847: athrow         
        //  1848: aload           7
        //  1850: astore_2       
        //  1851: aload_3        
        //  1852: ifnull          1879
        //  1855: aload_3        
        //  1856: instanceof      Ljavax/crypto/spec/PBEParameterSpec;
        //  1859: ifeq            1868
        //  1862: aload           7
        //  1864: astore_2       
        //  1865: goto            1879
        //  1868: new             Ljava/security/InvalidAlgorithmParameterException;
        //  1871: dup            
        //  1872: ldc_w           "unknown parameter type."
        //  1875: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;)V
        //  1878: athrow         
        //  1879: aload_0        
        //  1880: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.ivLength:I
        //  1883: ifeq            2001
        //  1886: aload_2        
        //  1887: instanceof      Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1890: ifne            2001
        //  1893: aload_2        
        //  1894: instanceof      Lorg/bouncycastle/crypto/params/AEADParameters;
        //  1897: ifne            2001
        //  1900: aload           4
        //  1902: ifnonnull       1912
        //  1905: invokestatic    org/bouncycastle/crypto/CryptoServicesRegistrar.getSecureRandom:()Ljava/security/SecureRandom;
        //  1908: astore_3       
        //  1909: goto            1915
        //  1912: aload           4
        //  1914: astore_3       
        //  1915: iload_1        
        //  1916: iconst_1       
        //  1917: if_icmpeq       1965
        //  1920: iload_1        
        //  1921: iconst_3       
        //  1922: if_icmpne       1928
        //  1925: goto            1965
        //  1928: aload_0        
        //  1929: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.cipher:Lorg/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher;
        //  1932: invokeinterface org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher.getUnderlyingCipher:()Lorg/bouncycastle/crypto/BlockCipher;
        //  1937: invokeinterface org/bouncycastle/crypto/BlockCipher.getAlgorithmName:()Ljava/lang/String;
        //  1942: ldc_w           "PGPCFB"
        //  1945: invokevirtual   java/lang/String.indexOf:(Ljava/lang/String;)I
        //  1948: iflt            1954
        //  1951: goto            2001
        //  1954: new             Ljava/security/InvalidAlgorithmParameterException;
        //  1957: dup            
        //  1958: ldc_w           "no IV set when one expected"
        //  1961: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;)V
        //  1964: athrow         
        //  1965: aload_0        
        //  1966: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.ivLength:I
        //  1969: newarray        B
        //  1971: astore          7
        //  1973: aload_3        
        //  1974: aload           7
        //  1976: invokevirtual   java/security/SecureRandom.nextBytes:([B)V
        //  1979: new             Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1982: dup            
        //  1983: aload_2        
        //  1984: aload           7
        //  1986: invokespecial   org/bouncycastle/crypto/params/ParametersWithIV.<init>:(Lorg/bouncycastle/crypto/CipherParameters;[B)V
        //  1989: astore_2       
        //  1990: aload_0        
        //  1991: aload_2        
        //  1992: checkcast       Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1995: putfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.ivParam:Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  1998: goto            2001
        //  2001: aload_2        
        //  2002: astore_3       
        //  2003: aload           4
        //  2005: ifnull          2028
        //  2008: aload_2        
        //  2009: astore_3       
        //  2010: aload_0        
        //  2011: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.padded:Z
        //  2014: ifeq            2028
        //  2017: new             Lorg/bouncycastle/crypto/params/ParametersWithRandom;
        //  2020: dup            
        //  2021: aload_2        
        //  2022: aload           4
        //  2024: invokespecial   org/bouncycastle/crypto/params/ParametersWithRandom.<init>:(Lorg/bouncycastle/crypto/CipherParameters;Ljava/security/SecureRandom;)V
        //  2027: astore_3       
        //  2028: iload_1        
        //  2029: iconst_1       
        //  2030: if_icmpeq       2107
        //  2033: iload_1        
        //  2034: iconst_2       
        //  2035: if_icmpeq       2093
        //  2038: iload_1        
        //  2039: iconst_3       
        //  2040: if_icmpeq       2107
        //  2043: iload_1        
        //  2044: iconst_4       
        //  2045: if_icmpne       2051
        //  2048: goto            2093
        //  2051: new             Ljava/lang/StringBuilder;
        //  2054: dup            
        //  2055: invokespecial   java/lang/StringBuilder.<init>:()V
        //  2058: astore_2       
        //  2059: aload_2        
        //  2060: ldc_w           "unknown opmode "
        //  2063: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  2066: pop            
        //  2067: aload_2        
        //  2068: iload_1        
        //  2069: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //  2072: pop            
        //  2073: aload_2        
        //  2074: ldc_w           " passed"
        //  2077: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  2080: pop            
        //  2081: new             Ljava/security/InvalidParameterException;
        //  2084: dup            
        //  2085: aload_2        
        //  2086: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  2089: invokespecial   java/security/InvalidParameterException.<init>:(Ljava/lang/String;)V
        //  2092: athrow         
        //  2093: aload_0        
        //  2094: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.cipher:Lorg/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher;
        //  2097: iconst_0       
        //  2098: aload_3        
        //  2099: invokeinterface org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher.init:(ZLorg/bouncycastle/crypto/CipherParameters;)V
        //  2104: goto            2118
        //  2107: aload_0        
        //  2108: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.cipher:Lorg/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher;
        //  2111: iconst_1       
        //  2112: aload_3        
        //  2113: invokeinterface org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher.init:(ZLorg/bouncycastle/crypto/CipherParameters;)V
        //  2118: aload_0        
        //  2119: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.cipher:Lorg/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher;
        //  2122: instanceof      Lorg/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$AEADGenericBlockCipher;
        //  2125: ifeq            2184
        //  2128: aload_0        
        //  2129: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.aeadParams:Lorg/bouncycastle/crypto/params/AEADParameters;
        //  2132: ifnonnull       2184
        //  2135: aload_0        
        //  2136: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.cipher:Lorg/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$GenericBlockCipher;
        //  2139: checkcast       Lorg/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$AEADGenericBlockCipher;
        //  2142: invokestatic    org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$AEADGenericBlockCipher.access$000:(Lorg/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher$AEADGenericBlockCipher;)Lorg/bouncycastle/crypto/modes/AEADCipher;
        //  2145: astore_2       
        //  2146: aload_0        
        //  2147: new             Lorg/bouncycastle/crypto/params/AEADParameters;
        //  2150: dup            
        //  2151: aload_0        
        //  2152: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.ivParam:Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  2155: invokevirtual   org/bouncycastle/crypto/params/ParametersWithIV.getParameters:()Lorg/bouncycastle/crypto/CipherParameters;
        //  2158: checkcast       Lorg/bouncycastle/crypto/params/KeyParameter;
        //  2161: aload_2        
        //  2162: invokeinterface org/bouncycastle/crypto/modes/AEADCipher.getMac:()[B
        //  2167: arraylength    
        //  2168: bipush          8
        //  2170: imul           
        //  2171: aload_0        
        //  2172: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.ivParam:Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //  2175: invokevirtual   org/bouncycastle/crypto/params/ParametersWithIV.getIV:()[B
        //  2178: invokespecial   org/bouncycastle/crypto/params/AEADParameters.<init>:(Lorg/bouncycastle/crypto/params/KeyParameter;I[B)V
        //  2181: putfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseBlockCipher.aeadParams:Lorg/bouncycastle/crypto/params/AEADParameters;
        //  2184: return         
        //  2185: new             Lorg/bouncycastle/jcajce/provider/symmetric/util/BaseWrapCipher$InvalidKeyOrParametersException;
        //  2188: dup            
        //  2189: aload_2        
        //  2190: invokevirtual   java/lang/Exception.getMessage:()Ljava/lang/String;
        //  2193: aload_2        
        //  2194: invokespecial   org/bouncycastle/jcajce/provider/symmetric/util/BaseWrapCipher$InvalidKeyOrParametersException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //  2197: athrow         
        //  2198: new             Ljava/security/InvalidAlgorithmParameterException;
        //  2201: dup            
        //  2202: aload_2        
        //  2203: invokevirtual   java/lang/IllegalArgumentException.getMessage:()Ljava/lang/String;
        //  2206: aload_2        
        //  2207: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //  2210: athrow         
        //  2211: new             Ljava/security/InvalidKeyException;
        //  2214: dup            
        //  2215: ldc_w           "PKCS12 requires a SecretKey/PBEKey"
        //  2218: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //  2221: athrow         
        //  2222: astore_2       
        //  2223: goto            2211
        //  2226: astore_2       
        //  2227: goto            1837
        //  2230: astore_2       
        //  2231: goto            2185
        //  2234: astore_2       
        //  2235: goto            2198
        //    Exceptions:
        //  throws java.security.InvalidKeyException
        //  throws java.security.InvalidAlgorithmParameterException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  623    629    2222   2226   Ljava/lang/Exception;
        //  1735   1765   2226   1848   Ljava/lang/Exception;
        //  1768   1785   2226   1848   Ljava/lang/Exception;
        //  1785   1834   2226   1848   Ljava/lang/Exception;
        //  2051   2093   2234   2211   Ljava/lang/IllegalArgumentException;
        //  2051   2093   2230   2198   Ljava/lang/Exception;
        //  2093   2104   2234   2211   Ljava/lang/IllegalArgumentException;
        //  2093   2104   2230   2198   Ljava/lang/Exception;
        //  2107   2118   2234   2211   Ljava/lang/IllegalArgumentException;
        //  2107   2118   2230   2198   Ljava/lang/Exception;
        //  2118   2184   2234   2211   Ljava/lang/IllegalArgumentException;
        //  2118   2184   2230   2198   Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 1009 out of bounds for length 1009
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:373)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:426)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    protected void engineSetMode(final String s) throws NoSuchAlgorithmException {
        if (this.baseEngine != null) {
            this.modeName = Strings.toUpperCase(s);
            GenericBlockCipher cipher = null;
            Label_0045: {
                if (this.modeName.equals("ECB")) {
                    this.ivLength = 0;
                    cipher = new BufferedGenericBlockCipher(this.baseEngine);
                }
                else {
                    if (!this.modeName.equals("CBC")) {
                        GenericBlockCipher cipher2;
                        if (this.modeName.startsWith("OFB")) {
                            this.ivLength = this.baseEngine.getBlockSize();
                            if (this.modeName.length() == 3) {
                                final BlockCipher baseEngine = this.baseEngine;
                                cipher = new BufferedGenericBlockCipher(new OFBBlockCipher(baseEngine, baseEngine.getBlockSize() * 8));
                                break Label_0045;
                            }
                            cipher2 = new BufferedGenericBlockCipher(new OFBBlockCipher(this.baseEngine, Integer.parseInt(this.modeName.substring(3))));
                        }
                        else if (this.modeName.startsWith("CFB")) {
                            this.ivLength = this.baseEngine.getBlockSize();
                            if (this.modeName.length() == 3) {
                                final BlockCipher baseEngine2 = this.baseEngine;
                                cipher = new BufferedGenericBlockCipher(new CFBBlockCipher(baseEngine2, baseEngine2.getBlockSize() * 8));
                                break Label_0045;
                            }
                            cipher2 = new BufferedGenericBlockCipher(new CFBBlockCipher(this.baseEngine, Integer.parseInt(this.modeName.substring(3))));
                        }
                        else if (this.modeName.startsWith("PGP")) {
                            final boolean equalsIgnoreCase = this.modeName.equalsIgnoreCase("PGPCFBwithIV");
                            this.ivLength = this.baseEngine.getBlockSize();
                            cipher2 = new BufferedGenericBlockCipher(new PGPCFBBlockCipher(this.baseEngine, equalsIgnoreCase));
                        }
                        else {
                            if (this.modeName.equalsIgnoreCase("OpenPGPCFB")) {
                                this.ivLength = 0;
                                cipher = new BufferedGenericBlockCipher(new OpenPGPCFBBlockCipher(this.baseEngine));
                                break Label_0045;
                            }
                            if (this.modeName.startsWith("SIC")) {
                                this.ivLength = this.baseEngine.getBlockSize();
                                if (this.ivLength >= 16) {
                                    this.fixedIv = false;
                                    cipher = new BufferedGenericBlockCipher(new BufferedBlockCipher(new SICBlockCipher(this.baseEngine)));
                                    break Label_0045;
                                }
                                throw new IllegalArgumentException("Warning: SIC-Mode can become a twotime-pad if the blocksize of the cipher is too small. Use a cipher with a block size of at least 128 bits (e.g. AES)");
                            }
                            else if (this.modeName.startsWith("CTR")) {
                                this.ivLength = this.baseEngine.getBlockSize();
                                this.fixedIv = false;
                                final BlockCipher baseEngine3 = this.baseEngine;
                                if (baseEngine3 instanceof DSTU7624Engine) {
                                    cipher2 = new BufferedGenericBlockCipher(new BufferedBlockCipher(new KCTRBlockCipher(baseEngine3)));
                                }
                                else {
                                    cipher2 = new BufferedGenericBlockCipher(new BufferedBlockCipher(new SICBlockCipher(baseEngine3)));
                                }
                            }
                            else {
                                if (this.modeName.startsWith("GOFB")) {
                                    this.ivLength = this.baseEngine.getBlockSize();
                                    cipher = new BufferedGenericBlockCipher(new BufferedBlockCipher(new GOFBBlockCipher(this.baseEngine)));
                                    break Label_0045;
                                }
                                if (this.modeName.startsWith("GCFB")) {
                                    this.ivLength = this.baseEngine.getBlockSize();
                                    cipher = new BufferedGenericBlockCipher(new BufferedBlockCipher(new GCFBBlockCipher(this.baseEngine)));
                                    break Label_0045;
                                }
                                if (this.modeName.startsWith("CTS")) {
                                    this.ivLength = this.baseEngine.getBlockSize();
                                    cipher = new BufferedGenericBlockCipher(new CTSBlockCipher(new CBCBlockCipher(this.baseEngine)));
                                    break Label_0045;
                                }
                                if (this.modeName.startsWith("CCM")) {
                                    this.ivLength = 12;
                                    final BlockCipher baseEngine4 = this.baseEngine;
                                    if (baseEngine4 instanceof DSTU7624Engine) {
                                        cipher2 = new AEADGenericBlockCipher(new KCCMBlockCipher(baseEngine4));
                                    }
                                    else {
                                        cipher2 = new AEADGenericBlockCipher(new CCMBlockCipher(baseEngine4));
                                    }
                                }
                                else if (this.modeName.startsWith("OCB")) {
                                    final BlockCipherProvider engineProvider = this.engineProvider;
                                    if (engineProvider != null) {
                                        this.ivLength = 15;
                                        cipher = new AEADGenericBlockCipher(new OCBBlockCipher(this.baseEngine, engineProvider.get()));
                                        break Label_0045;
                                    }
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append("can't support mode ");
                                    sb.append(s);
                                    throw new NoSuchAlgorithmException(sb.toString());
                                }
                                else {
                                    if (this.modeName.startsWith("EAX")) {
                                        this.ivLength = this.baseEngine.getBlockSize();
                                        cipher = new AEADGenericBlockCipher(new EAXBlockCipher(this.baseEngine));
                                        break Label_0045;
                                    }
                                    if (!this.modeName.startsWith("GCM")) {
                                        final StringBuilder sb2 = new StringBuilder();
                                        sb2.append("can't support mode ");
                                        sb2.append(s);
                                        throw new NoSuchAlgorithmException(sb2.toString());
                                    }
                                    this.ivLength = this.baseEngine.getBlockSize();
                                    final BlockCipher baseEngine5 = this.baseEngine;
                                    if (baseEngine5 instanceof DSTU7624Engine) {
                                        cipher2 = new AEADGenericBlockCipher(new KGCMBlockCipher(baseEngine5));
                                    }
                                    else {
                                        cipher2 = new AEADGenericBlockCipher(new GCMBlockCipher(baseEngine5));
                                    }
                                }
                            }
                        }
                        this.cipher = cipher2;
                        return;
                    }
                    this.ivLength = this.baseEngine.getBlockSize();
                    cipher = new BufferedGenericBlockCipher(new CBCBlockCipher(this.baseEngine));
                }
            }
            this.cipher = cipher;
            return;
        }
        throw new NoSuchAlgorithmException("no mode supported for this algorithm");
    }
    
    @Override
    protected void engineSetPadding(final String str) throws NoSuchPaddingException {
        if (this.baseEngine != null) {
            final String upperCase = Strings.toUpperCase(str);
            BufferedGenericBlockCipher cipher;
            if (upperCase.equals("NOPADDING")) {
                if (!this.cipher.wrapOnNoPadding()) {
                    return;
                }
                cipher = new BufferedGenericBlockCipher(new BufferedBlockCipher(this.cipher.getUnderlyingCipher()));
            }
            else if (!upperCase.equals("WITHCTS") && !upperCase.equals("CTSPADDING") && !upperCase.equals("CS3PADDING")) {
                this.padded = true;
                if (this.isAEADModeName(this.modeName)) {
                    throw new NoSuchPaddingException("Only NoPadding can be used with AEAD modes.");
                }
                if (!upperCase.equals("PKCS5PADDING") && !upperCase.equals("PKCS7PADDING")) {
                    if (upperCase.equals("ZEROBYTEPADDING")) {
                        cipher = new BufferedGenericBlockCipher(this.cipher.getUnderlyingCipher(), new ZeroBytePadding());
                    }
                    else if (!upperCase.equals("ISO10126PADDING") && !upperCase.equals("ISO10126-2PADDING")) {
                        if (!upperCase.equals("X9.23PADDING") && !upperCase.equals("X923PADDING")) {
                            if (!upperCase.equals("ISO7816-4PADDING") && !upperCase.equals("ISO9797-1PADDING")) {
                                if (!upperCase.equals("TBCPADDING")) {
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append("Padding ");
                                    sb.append(str);
                                    sb.append(" unknown.");
                                    throw new NoSuchPaddingException(sb.toString());
                                }
                                cipher = new BufferedGenericBlockCipher(this.cipher.getUnderlyingCipher(), new TBCPadding());
                            }
                            else {
                                cipher = new BufferedGenericBlockCipher(this.cipher.getUnderlyingCipher(), new ISO7816d4Padding());
                            }
                        }
                        else {
                            cipher = new BufferedGenericBlockCipher(this.cipher.getUnderlyingCipher(), new X923Padding());
                        }
                    }
                    else {
                        cipher = new BufferedGenericBlockCipher(this.cipher.getUnderlyingCipher(), new ISO10126d2Padding());
                    }
                }
                else {
                    cipher = new BufferedGenericBlockCipher(this.cipher.getUnderlyingCipher());
                }
            }
            else {
                cipher = new BufferedGenericBlockCipher(new CTSBlockCipher(this.cipher.getUnderlyingCipher()));
            }
            this.cipher = (GenericBlockCipher)cipher;
            return;
        }
        throw new NoSuchPaddingException("no padding supported for this algorithm");
    }
    
    @Override
    protected int engineUpdate(final byte[] array, int processBytes, final int n, final byte[] array2, final int n2) throws ShortBufferException {
        if (this.cipher.getUpdateOutputSize(n) + n2 <= array2.length) {
            try {
                processBytes = this.cipher.processBytes(array, processBytes, n, array2, n2);
                return processBytes;
            }
            catch (DataLengthException ex) {
                throw new IllegalStateException(ex.toString());
            }
        }
        throw new ShortBufferException("output buffer too short for input.");
    }
    
    @Override
    protected byte[] engineUpdate(byte[] array, int processBytes, final int n) {
        final int updateOutputSize = this.cipher.getUpdateOutputSize(n);
        if (updateOutputSize <= 0) {
            this.cipher.processBytes(array, processBytes, n, null, 0);
            return null;
        }
        final byte[] array2 = new byte[updateOutputSize];
        processBytes = this.cipher.processBytes(array, processBytes, n, array2, 0);
        if (processBytes == 0) {
            return null;
        }
        if (processBytes != array2.length) {
            array = new byte[processBytes];
            System.arraycopy(array2, 0, array, 0, processBytes);
            return array;
        }
        return array2;
    }
    
    @Override
    protected void engineUpdateAAD(final ByteBuffer byteBuffer) {
        int remaining = byteBuffer.remaining();
        if (remaining < 1) {
            return;
        }
        if (byteBuffer.hasArray()) {
            this.engineUpdateAAD(byteBuffer.array(), byteBuffer.arrayOffset() + byteBuffer.position(), remaining);
            byteBuffer.position(byteBuffer.limit());
            return;
        }
        if (remaining <= 512) {
            final byte[] dst = new byte[remaining];
            byteBuffer.get(dst);
            this.engineUpdateAAD(dst, 0, dst.length);
            Arrays.fill(dst, (byte)0);
            return;
        }
        final byte[] dst2 = new byte[512];
        int min;
        do {
            min = Math.min(dst2.length, remaining);
            byteBuffer.get(dst2, 0, min);
            this.engineUpdateAAD(dst2, 0, min);
        } while ((remaining -= min) > 0);
        Arrays.fill(dst2, (byte)0);
    }
    
    @Override
    protected void engineUpdateAAD(final byte[] array, final int n, final int n2) {
        this.cipher.updateAAD(array, n, n2);
    }
    
    private static class AEADGenericBlockCipher implements GenericBlockCipher
    {
        private static final Constructor aeadBadTagConstructor;
        private AEADCipher cipher;
        
        static {
            final Class loadClass = ClassUtil.loadClass(BaseBlockCipher.class, "javax.crypto.AEADBadTagException");
            Constructor exceptionConstructor;
            if (loadClass != null) {
                exceptionConstructor = findExceptionConstructor(loadClass);
            }
            else {
                exceptionConstructor = null;
            }
            aeadBadTagConstructor = exceptionConstructor;
        }
        
        AEADGenericBlockCipher(final AEADCipher cipher) {
            this.cipher = cipher;
        }
        
        private static Constructor findExceptionConstructor(final Class clazz) {
            try {
                return clazz.getConstructor(String.class);
            }
            catch (Exception ex) {
                return null;
            }
        }
        
        @Override
        public int doFinal(final byte[] array, int doFinal) throws IllegalStateException, BadPaddingException {
            try {
                doFinal = this.cipher.doFinal(array, doFinal);
                return doFinal;
            }
            catch (InvalidCipherTextException ex2) {
                final Constructor aeadBadTagConstructor = AEADGenericBlockCipher.aeadBadTagConstructor;
                Label_0053: {
                    if (aeadBadTagConstructor == null) {
                        break Label_0053;
                    }
                Label_0045_Outer:
                    while (true) {
                        while (true) {
                            try {
                                BadPaddingException ex = aeadBadTagConstructor.newInstance(ex2.getMessage());
                                while (true) {
                                    if (ex != null) {
                                        throw ex;
                                    }
                                    throw new BadPaddingException(ex2.getMessage());
                                    ex = null;
                                    continue Label_0045_Outer;
                                }
                            }
                            catch (Exception ex3) {}
                            continue;
                        }
                    }
                }
            }
        }
        
        @Override
        public String getAlgorithmName() {
            final AEADCipher cipher = this.cipher;
            if (cipher instanceof AEADBlockCipher) {
                return ((AEADBlockCipher)cipher).getUnderlyingCipher().getAlgorithmName();
            }
            return cipher.getAlgorithmName();
        }
        
        @Override
        public int getOutputSize(final int n) {
            return this.cipher.getOutputSize(n);
        }
        
        @Override
        public BlockCipher getUnderlyingCipher() {
            final AEADCipher cipher = this.cipher;
            if (cipher instanceof AEADBlockCipher) {
                return ((AEADBlockCipher)cipher).getUnderlyingCipher();
            }
            return null;
        }
        
        @Override
        public int getUpdateOutputSize(final int n) {
            return this.cipher.getUpdateOutputSize(n);
        }
        
        @Override
        public void init(final boolean b, final CipherParameters cipherParameters) throws IllegalArgumentException {
            this.cipher.init(b, cipherParameters);
        }
        
        @Override
        public int processByte(final byte b, final byte[] array, final int n) throws DataLengthException {
            return this.cipher.processByte(b, array, n);
        }
        
        @Override
        public int processBytes(final byte[] array, final int n, final int n2, final byte[] array2, final int n3) throws DataLengthException {
            return this.cipher.processBytes(array, n, n2, array2, n3);
        }
        
        @Override
        public void updateAAD(final byte[] array, final int n, final int n2) {
            this.cipher.processAADBytes(array, n, n2);
        }
        
        @Override
        public boolean wrapOnNoPadding() {
            return false;
        }
    }
    
    private static class BufferedGenericBlockCipher implements GenericBlockCipher
    {
        private BufferedBlockCipher cipher;
        
        BufferedGenericBlockCipher(final BlockCipher blockCipher) {
            this.cipher = new PaddedBufferedBlockCipher(blockCipher);
        }
        
        BufferedGenericBlockCipher(final BlockCipher blockCipher, final BlockCipherPadding blockCipherPadding) {
            this.cipher = new PaddedBufferedBlockCipher(blockCipher, blockCipherPadding);
        }
        
        BufferedGenericBlockCipher(final BufferedBlockCipher cipher) {
            this.cipher = cipher;
        }
        
        @Override
        public int doFinal(final byte[] array, int doFinal) throws IllegalStateException, BadPaddingException {
            try {
                doFinal = this.cipher.doFinal(array, doFinal);
                return doFinal;
            }
            catch (InvalidCipherTextException ex) {
                throw new BadPaddingException(ex.getMessage());
            }
        }
        
        @Override
        public String getAlgorithmName() {
            return this.cipher.getUnderlyingCipher().getAlgorithmName();
        }
        
        @Override
        public int getOutputSize(final int n) {
            return this.cipher.getOutputSize(n);
        }
        
        @Override
        public BlockCipher getUnderlyingCipher() {
            return this.cipher.getUnderlyingCipher();
        }
        
        @Override
        public int getUpdateOutputSize(final int n) {
            return this.cipher.getUpdateOutputSize(n);
        }
        
        @Override
        public void init(final boolean b, final CipherParameters cipherParameters) throws IllegalArgumentException {
            this.cipher.init(b, cipherParameters);
        }
        
        @Override
        public int processByte(final byte b, final byte[] array, final int n) throws DataLengthException {
            return this.cipher.processByte(b, array, n);
        }
        
        @Override
        public int processBytes(final byte[] array, final int n, final int n2, final byte[] array2, final int n3) throws DataLengthException {
            return this.cipher.processBytes(array, n, n2, array2, n3);
        }
        
        @Override
        public void updateAAD(final byte[] array, final int n, final int n2) {
            throw new UnsupportedOperationException("AAD is not supported in the current mode.");
        }
        
        @Override
        public boolean wrapOnNoPadding() {
            return this.cipher instanceof CTSBlockCipher ^ true;
        }
    }
    
    private interface GenericBlockCipher
    {
        int doFinal(final byte[] p0, final int p1) throws IllegalStateException, BadPaddingException;
        
        String getAlgorithmName();
        
        int getOutputSize(final int p0);
        
        BlockCipher getUnderlyingCipher();
        
        int getUpdateOutputSize(final int p0);
        
        void init(final boolean p0, final CipherParameters p1) throws IllegalArgumentException;
        
        int processByte(final byte p0, final byte[] p1, final int p2) throws DataLengthException;
        
        int processBytes(final byte[] p0, final int p1, final int p2, final byte[] p3, final int p4) throws DataLengthException;
        
        void updateAAD(final byte[] p0, final int p1, final int p2);
        
        boolean wrapOnNoPadding();
    }
}
