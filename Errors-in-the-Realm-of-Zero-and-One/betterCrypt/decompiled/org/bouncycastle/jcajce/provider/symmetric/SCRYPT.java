// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.symmetric;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.jcajce.provider.config.ConfigurableProvider;
import org.bouncycastle.jcajce.provider.util.AlgorithmProvider;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.jcajce.provider.symmetric.util.BCPBEKey;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.generators.SCrypt;
import org.bouncycastle.crypto.PasswordConverter;
import java.security.spec.InvalidKeySpecException;
import org.bouncycastle.jcajce.spec.ScryptKeySpec;
import javax.crypto.SecretKey;
import java.security.spec.KeySpec;
import org.bouncycastle.asn1.misc.MiscObjectIdentifiers;
import org.bouncycastle.jcajce.provider.symmetric.util.BaseSecretKeyFactory;

public class SCRYPT
{
    private SCRYPT() {
    }
    
    public static class BasePBKDF2 extends BaseSecretKeyFactory
    {
        private int scheme;
        
        public BasePBKDF2(final String s, final int scheme) {
            super(s, MiscObjectIdentifiers.id_scrypt);
            this.scheme = scheme;
        }
        
        @Override
        protected SecretKey engineGenerateSecret(final KeySpec keySpec) throws InvalidKeySpecException {
            if (!(keySpec instanceof ScryptKeySpec)) {
                throw new InvalidKeySpecException("Invalid KeySpec");
            }
            final ScryptKeySpec scryptKeySpec = (ScryptKeySpec)keySpec;
            if (scryptKeySpec.getSalt() == null) {
                throw new IllegalArgumentException("Salt S must be provided.");
            }
            if (scryptKeySpec.getCostParameter() <= 1) {
                throw new IllegalArgumentException("Cost parameter N must be > 1.");
            }
            if (scryptKeySpec.getKeyLength() <= 0) {
                final StringBuilder sb = new StringBuilder();
                sb.append("positive key length required: ");
                sb.append(scryptKeySpec.getKeyLength());
                throw new InvalidKeySpecException(sb.toString());
            }
            if (scryptKeySpec.getPassword().length != 0) {
                return new BCPBEKey(this.algName, scryptKeySpec, new KeyParameter(SCrypt.generate(PasswordConverter.UTF8.convert(scryptKeySpec.getPassword()), scryptKeySpec.getSalt(), scryptKeySpec.getCostParameter(), scryptKeySpec.getBlockSize(), scryptKeySpec.getParallelizationParameter(), scryptKeySpec.getKeyLength() / 8)));
            }
            throw new IllegalArgumentException("password empty");
        }
    }
    
    public static class Mappings extends AlgorithmProvider
    {
        private static final String PREFIX;
        
        static {
            PREFIX = SCRYPT.class.getName();
        }
        
        @Override
        public void configure(final ConfigurableProvider configurableProvider) {
            final StringBuilder sb = new StringBuilder();
            sb.append(Mappings.PREFIX);
            sb.append("$ScryptWithUTF8");
            configurableProvider.addAlgorithm("SecretKeyFactory.SCRYPT", sb.toString());
            final ASN1ObjectIdentifier id_scrypt = MiscObjectIdentifiers.id_scrypt;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(Mappings.PREFIX);
            sb2.append("$ScryptWithUTF8");
            configurableProvider.addAlgorithm("SecretKeyFactory", id_scrypt, sb2.toString());
        }
    }
    
    public static class ScryptWithUTF8 extends BasePBKDF2
    {
        public ScryptWithUTF8() {
            super("SCRYPT", 5);
        }
    }
}
