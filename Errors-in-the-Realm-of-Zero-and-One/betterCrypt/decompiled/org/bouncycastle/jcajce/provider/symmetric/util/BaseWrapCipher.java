// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.symmetric.util;

import java.io.ByteArrayOutputStream;
import javax.crypto.NoSuchPaddingException;
import org.bouncycastle.crypto.CipherParameters;
import java.security.InvalidParameterException;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.crypto.params.ParametersWithUKM;
import org.bouncycastle.crypto.params.ParametersWithSBox;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.params.KeyParameter;
import java.security.InvalidKeyException;
import java.security.InvalidAlgorithmParameterException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.security.Key;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.InvalidCipherTextException;
import javax.crypto.BadPaddingException;
import javax.crypto.ShortBufferException;
import javax.crypto.IllegalBlockSizeException;
import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import org.bouncycastle.jcajce.util.BCJcaJceHelper;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.RC5ParameterSpec;
import javax.crypto.spec.RC2ParameterSpec;
import javax.crypto.spec.PBEParameterSpec;
import org.bouncycastle.jcajce.spec.GOST28147WrapParameterSpec;
import org.bouncycastle.crypto.Wrapper;
import org.bouncycastle.jcajce.util.JcaJceHelper;
import java.security.AlgorithmParameters;
import javax.crypto.CipherSpi;

public abstract class BaseWrapCipher extends CipherSpi implements PBE
{
    private Class[] availableSpecs;
    protected AlgorithmParameters engineParams;
    private boolean forWrapping;
    private final JcaJceHelper helper;
    private byte[] iv;
    private int ivSize;
    protected int pbeHash;
    protected int pbeIvSize;
    protected int pbeKeySize;
    protected int pbeType;
    protected Wrapper wrapEngine;
    private ErasableOutputStream wrapStream;
    
    protected BaseWrapCipher() {
        this.availableSpecs = new Class[] { GOST28147WrapParameterSpec.class, PBEParameterSpec.class, RC2ParameterSpec.class, RC5ParameterSpec.class, IvParameterSpec.class };
        this.pbeType = 2;
        this.pbeHash = 1;
        this.engineParams = null;
        this.wrapEngine = null;
        this.wrapStream = null;
        this.helper = new BCJcaJceHelper();
    }
    
    protected BaseWrapCipher(final Wrapper wrapper) {
        this(wrapper, 0);
    }
    
    protected BaseWrapCipher(final Wrapper wrapEngine, final int ivSize) {
        this.availableSpecs = new Class[] { GOST28147WrapParameterSpec.class, PBEParameterSpec.class, RC2ParameterSpec.class, RC5ParameterSpec.class, IvParameterSpec.class };
        this.pbeType = 2;
        this.pbeHash = 1;
        this.engineParams = null;
        this.wrapEngine = null;
        this.wrapStream = null;
        this.helper = new BCJcaJceHelper();
        this.wrapEngine = wrapEngine;
        this.ivSize = ivSize;
    }
    
    protected final AlgorithmParameters createParametersInstance(final String s) throws NoSuchAlgorithmException, NoSuchProviderException {
        return this.helper.createAlgorithmParameters(s);
    }
    
    @Override
    protected int engineDoFinal(byte[] b, int length, final int len, final byte[] array, final int n) throws IllegalBlockSizeException, BadPaddingException, ShortBufferException {
        final ErasableOutputStream wrapStream = this.wrapStream;
        if (wrapStream != null) {
            wrapStream.write(b, length, len);
            try {
                Label_0096: {
                    if (this.forWrapping) {
                        try {
                            b = this.wrapEngine.wrap(this.wrapStream.getBuf(), 0, this.wrapStream.size());
                            break Label_0096;
                        }
                        catch (Exception ex) {
                            throw new IllegalBlockSizeException(ex.getMessage());
                        }
                    }
                    try {
                        b = this.wrapEngine.unwrap(this.wrapStream.getBuf(), 0, this.wrapStream.size());
                        if (b.length + n <= array.length) {
                            System.arraycopy(b, 0, array, n, b.length);
                            length = b.length;
                            return length;
                        }
                        throw new ShortBufferException("output buffer too short for input.");
                    }
                    catch (InvalidCipherTextException ex2) {
                        throw new BadPaddingException(ex2.getMessage());
                    }
                }
            }
            finally {
                this.wrapStream.erase();
            }
        }
        throw new IllegalStateException("not supported in a wrapping mode");
    }
    
    @Override
    protected byte[] engineDoFinal(byte[] b, final int off, final int len) throws IllegalBlockSizeException, BadPaddingException {
        final ErasableOutputStream wrapStream = this.wrapStream;
        if (wrapStream != null) {
            if (b != null) {
                wrapStream.write(b, off, len);
            }
            try {
                Label_0081: {
                    if (!this.forWrapping) {
                        break Label_0081;
                    }
                    try {
                        b = this.wrapEngine.wrap(this.wrapStream.getBuf(), 0, this.wrapStream.size());
                        return b;
                    }
                    catch (Exception ex) {
                        throw new IllegalBlockSizeException(ex.getMessage());
                    }
                    try {
                        b = this.wrapEngine.unwrap(this.wrapStream.getBuf(), 0, this.wrapStream.size());
                    }
                    catch (InvalidCipherTextException ex2) {
                        throw new BadPaddingException(ex2.getMessage());
                    }
                }
            }
            finally {
                this.wrapStream.erase();
            }
        }
        throw new IllegalStateException("not supported in a wrapping mode");
    }
    
    @Override
    protected int engineGetBlockSize() {
        return 0;
    }
    
    @Override
    protected byte[] engineGetIV() {
        return Arrays.clone(this.iv);
    }
    
    @Override
    protected int engineGetKeySize(final Key key) {
        return key.getEncoded().length * 8;
    }
    
    @Override
    protected int engineGetOutputSize(final int n) {
        return -1;
    }
    
    @Override
    protected AlgorithmParameters engineGetParameters() {
        if (this.engineParams == null && this.iv != null) {
            String s2;
            final String s = s2 = this.wrapEngine.getAlgorithmName();
            if (s.indexOf(47) >= 0) {
                s2 = s.substring(0, s.indexOf(47));
            }
            try {
                (this.engineParams = this.createParametersInstance(s2)).init(new IvParameterSpec(this.iv));
            }
            catch (Exception ex) {
                throw new RuntimeException(ex.toString());
            }
        }
        return this.engineParams;
    }
    
    @Override
    protected void engineInit(final int n, Key key, final AlgorithmParameters engineParams, final SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        AlgorithmParameterSpec parameterSpec = null;
        final AlgorithmParameterSpec algorithmParameterSpec = null;
        while (true) {
            while (true) {
                int n2 = 0;
                Label_0013: {
                    if (engineParams != null) {
                        n2 = 0;
                        break Label_0013;
                    }
                    Label_0098: {
                        break Label_0098;
                        while (true) {
                            while (true) {
                                try {
                                    final Class[] availableSpecs;
                                    parameterSpec = engineParams.getParameterSpec((Class<AlgorithmParameterSpec>)availableSpecs[n2]);
                                    if (parameterSpec != null) {
                                        this.engineParams = engineParams;
                                        this.engineInit(n, key, parameterSpec, secureRandom);
                                        return;
                                    }
                                    key = (Key)new StringBuilder();
                                    ((StringBuilder)key).append("can't handle parameter ");
                                    ((StringBuilder)key).append(engineParams.toString());
                                    throw new InvalidAlgorithmParameterException(((StringBuilder)key).toString());
                                    ++n2;
                                    break;
                                }
                                catch (Exception ex) {}
                                continue;
                            }
                        }
                    }
                }
                final Class[] availableSpecs = this.availableSpecs;
                parameterSpec = algorithmParameterSpec;
                if (n2 != availableSpecs.length) {
                    continue;
                }
                break;
            }
            continue;
        }
    }
    
    @Override
    protected void engineInit(final int n, final Key key, final SecureRandom secureRandom) throws InvalidKeyException {
        try {
            this.engineInit(n, key, (AlgorithmParameterSpec)null, secureRandom);
        }
        catch (InvalidAlgorithmParameterException ex) {
            throw new InvalidKeyOrParametersException(ex.getMessage(), ex);
        }
    }
    
    @Override
    protected void engineInit(final int n, final Key key, final AlgorithmParameterSpec algorithmParameterSpec, final SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        CipherParameters cipherParameters;
        if (key instanceof BCPBEKey) {
            final BCPBEKey bcpbeKey = (BCPBEKey)key;
            if (algorithmParameterSpec instanceof PBEParameterSpec) {
                cipherParameters = Util.makePBEParameters(bcpbeKey, algorithmParameterSpec, this.wrapEngine.getAlgorithmName());
            }
            else {
                if (bcpbeKey.getParam() == null) {
                    throw new InvalidAlgorithmParameterException("PBE requires PBE parameters to be set.");
                }
                cipherParameters = bcpbeKey.getParam();
            }
        }
        else {
            cipherParameters = new KeyParameter(key.getEncoded());
        }
        CipherParameters cipherParameters2 = cipherParameters;
        if (algorithmParameterSpec instanceof IvParameterSpec) {
            this.iv = ((IvParameterSpec)algorithmParameterSpec).getIV();
            cipherParameters2 = new ParametersWithIV(cipherParameters, this.iv);
        }
        CipherParameters cipherParameters3 = cipherParameters2;
        if (algorithmParameterSpec instanceof GOST28147WrapParameterSpec) {
            final GOST28147WrapParameterSpec gost28147WrapParameterSpec = (GOST28147WrapParameterSpec)algorithmParameterSpec;
            final byte[] sBox = gost28147WrapParameterSpec.getSBox();
            CipherParameters cipherParameters4 = cipherParameters2;
            if (sBox != null) {
                cipherParameters4 = new ParametersWithSBox(cipherParameters2, sBox);
            }
            cipherParameters3 = new ParametersWithUKM(cipherParameters4, gost28147WrapParameterSpec.getUKM());
        }
        CipherParameters cipherParameters5 = cipherParameters3;
        Label_0237: {
            if (cipherParameters3 instanceof KeyParameter) {
                cipherParameters5 = cipherParameters3;
                if (this.ivSize != 0) {
                    if (n != 3) {
                        cipherParameters5 = cipherParameters3;
                        if (n != 1) {
                            break Label_0237;
                        }
                    }
                    secureRandom.nextBytes(this.iv = new byte[this.ivSize]);
                    cipherParameters5 = new ParametersWithIV(cipherParameters3, this.iv);
                }
            }
        }
        CipherParameters cipherParameters6 = cipherParameters5;
        if (secureRandom != null) {
            cipherParameters6 = new ParametersWithRandom(cipherParameters5, secureRandom);
        }
        Label_0355: {
            if (n == 1) {
                break Label_0355;
            }
            Label_0330: {
                if (n == 2) {
                    break Label_0330;
                }
                Label_0308: {
                    if (n == 3) {
                        break Label_0308;
                    }
                    Label_0297: {
                        if (n != 4) {
                            break Label_0297;
                        }
                        try {
                            this.wrapEngine.init(false, cipherParameters6);
                            this.wrapStream = null;
                            while (true) {
                                this.forWrapping = false;
                                return;
                                throw new InvalidParameterException("Unknown mode parameter passed to init.");
                                this.wrapEngine.init(true, cipherParameters6);
                                this.wrapStream = null;
                                Label_0324: {
                                    break Label_0324;
                                    this.wrapEngine.init(true, cipherParameters6);
                                    this.wrapStream = new ErasableOutputStream();
                                }
                                this.forWrapping = true;
                                return;
                                this.wrapEngine.init(false, cipherParameters6);
                                this.wrapStream = new ErasableOutputStream();
                                continue;
                            }
                        }
                        catch (Exception ex) {
                            throw new InvalidKeyOrParametersException(ex.getMessage(), ex);
                        }
                    }
                }
            }
        }
    }
    
    @Override
    protected void engineSetMode(final String str) throws NoSuchAlgorithmException {
        final StringBuilder sb = new StringBuilder();
        sb.append("can't support mode ");
        sb.append(str);
        throw new NoSuchAlgorithmException(sb.toString());
    }
    
    @Override
    protected void engineSetPadding(final String str) throws NoSuchPaddingException {
        final StringBuilder sb = new StringBuilder();
        sb.append("Padding ");
        sb.append(str);
        sb.append(" unknown.");
        throw new NoSuchPaddingException(sb.toString());
    }
    
    @Override
    protected Key engineUnwrap(final byte[] p0, final String p1, final int p2) throws InvalidKeyException, NoSuchAlgorithmException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseWrapCipher.wrapEngine:Lorg/bouncycastle/crypto/Wrapper;
        //     4: ifnonnull       19
        //     7: aload_0        
        //     8: aload_1        
        //     9: iconst_0       
        //    10: aload_1        
        //    11: arraylength    
        //    12: invokevirtual   org/bouncycastle/jcajce/provider/symmetric/util/BaseWrapCipher.engineDoFinal:([BII)[B
        //    15: astore_1       
        //    16: goto            33
        //    19: aload_0        
        //    20: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseWrapCipher.wrapEngine:Lorg/bouncycastle/crypto/Wrapper;
        //    23: aload_1        
        //    24: iconst_0       
        //    25: aload_1        
        //    26: arraylength    
        //    27: invokeinterface org/bouncycastle/crypto/Wrapper.unwrap:([BII)[B
        //    32: astore_1       
        //    33: iload_3        
        //    34: iconst_3       
        //    35: if_icmpne       48
        //    38: new             Ljavax/crypto/spec/SecretKeySpec;
        //    41: dup            
        //    42: aload_1        
        //    43: aload_2        
        //    44: invokespecial   javax/crypto/spec/SecretKeySpec.<init>:([BLjava/lang/String;)V
        //    47: areturn        
        //    48: aload_2        
        //    49: ldc_w           ""
        //    52: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    55: ifeq            138
        //    58: iload_3        
        //    59: iconst_2       
        //    60: if_icmpne       138
        //    63: aload_1        
        //    64: invokestatic    org/bouncycastle/asn1/pkcs/PrivateKeyInfo.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/pkcs/PrivateKeyInfo;
        //    67: astore_1       
        //    68: aload_1        
        //    69: invokestatic    org/bouncycastle/jce/provider/BouncyCastleProvider.getPrivateKey:(Lorg/bouncycastle/asn1/pkcs/PrivateKeyInfo;)Ljava/security/PrivateKey;
        //    72: astore_2       
        //    73: aload_2        
        //    74: ifnull          79
        //    77: aload_2        
        //    78: areturn        
        //    79: new             Ljava/lang/StringBuilder;
        //    82: dup            
        //    83: invokespecial   java/lang/StringBuilder.<init>:()V
        //    86: astore_2       
        //    87: aload_2        
        //    88: ldc_w           "algorithm "
        //    91: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    94: pop            
        //    95: aload_2        
        //    96: aload_1        
        //    97: invokevirtual   org/bouncycastle/asn1/pkcs/PrivateKeyInfo.getPrivateKeyAlgorithm:()Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //   100: invokevirtual   org/bouncycastle/asn1/x509/AlgorithmIdentifier.getAlgorithm:()Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //   103: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   106: pop            
        //   107: aload_2        
        //   108: ldc_w           " not supported"
        //   111: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   114: pop            
        //   115: new             Ljava/security/InvalidKeyException;
        //   118: dup            
        //   119: aload_2        
        //   120: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   123: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   126: athrow         
        //   127: new             Ljava/security/InvalidKeyException;
        //   130: dup            
        //   131: ldc_w           "Invalid key encoding."
        //   134: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   137: athrow         
        //   138: aload_0        
        //   139: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseWrapCipher.helper:Lorg/bouncycastle/jcajce/util/JcaJceHelper;
        //   142: aload_2        
        //   143: invokeinterface org/bouncycastle/jcajce/util/JcaJceHelper.createKeyFactory:(Ljava/lang/String;)Ljava/security/KeyFactory;
        //   148: astore_2       
        //   149: iload_3        
        //   150: iconst_1       
        //   151: if_icmpne       167
        //   154: aload_2        
        //   155: new             Ljava/security/spec/X509EncodedKeySpec;
        //   158: dup            
        //   159: aload_1        
        //   160: invokespecial   java/security/spec/X509EncodedKeySpec.<init>:([B)V
        //   163: invokevirtual   java/security/KeyFactory.generatePublic:(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
        //   166: areturn        
        //   167: iload_3        
        //   168: iconst_2       
        //   169: if_icmpne       187
        //   172: aload_2        
        //   173: new             Ljava/security/spec/PKCS8EncodedKeySpec;
        //   176: dup            
        //   177: aload_1        
        //   178: invokespecial   java/security/spec/PKCS8EncodedKeySpec.<init>:([B)V
        //   181: invokevirtual   java/security/KeyFactory.generatePrivate:(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;
        //   184: astore_1       
        //   185: aload_1        
        //   186: areturn        
        //   187: new             Ljava/lang/StringBuilder;
        //   190: dup            
        //   191: invokespecial   java/lang/StringBuilder.<init>:()V
        //   194: astore_1       
        //   195: aload_1        
        //   196: ldc_w           "Unknown key type "
        //   199: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   202: pop            
        //   203: aload_1        
        //   204: iload_3        
        //   205: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   208: pop            
        //   209: new             Ljava/security/InvalidKeyException;
        //   212: dup            
        //   213: aload_1        
        //   214: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   217: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   220: athrow         
        //   221: astore_1       
        //   222: new             Ljava/lang/StringBuilder;
        //   225: dup            
        //   226: invokespecial   java/lang/StringBuilder.<init>:()V
        //   229: astore_2       
        //   230: aload_2        
        //   231: ldc_w           "Unknown key type "
        //   234: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   237: pop            
        //   238: aload_2        
        //   239: aload_1        
        //   240: invokevirtual   java/security/spec/InvalidKeySpecException.getMessage:()Ljava/lang/String;
        //   243: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   246: pop            
        //   247: new             Ljava/security/InvalidKeyException;
        //   250: dup            
        //   251: aload_2        
        //   252: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   255: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   258: athrow         
        //   259: astore_1       
        //   260: new             Ljava/lang/StringBuilder;
        //   263: dup            
        //   264: invokespecial   java/lang/StringBuilder.<init>:()V
        //   267: astore_2       
        //   268: aload_2        
        //   269: ldc_w           "Unknown key type "
        //   272: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   275: pop            
        //   276: aload_2        
        //   277: aload_1        
        //   278: invokevirtual   java/security/NoSuchProviderException.getMessage:()Ljava/lang/String;
        //   281: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   284: pop            
        //   285: new             Ljava/security/InvalidKeyException;
        //   288: dup            
        //   289: aload_2        
        //   290: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   293: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   296: athrow         
        //   297: astore_1       
        //   298: new             Ljava/security/InvalidKeyException;
        //   301: dup            
        //   302: aload_1        
        //   303: invokevirtual   javax/crypto/IllegalBlockSizeException.getMessage:()Ljava/lang/String;
        //   306: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   309: athrow         
        //   310: astore_1       
        //   311: new             Ljava/security/InvalidKeyException;
        //   314: dup            
        //   315: aload_1        
        //   316: invokevirtual   javax/crypto/BadPaddingException.getMessage:()Ljava/lang/String;
        //   319: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   322: athrow         
        //   323: astore_1       
        //   324: new             Ljava/security/InvalidKeyException;
        //   327: dup            
        //   328: aload_1        
        //   329: invokevirtual   org/bouncycastle/crypto/InvalidCipherTextException.getMessage:()Ljava/lang/String;
        //   332: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   335: athrow         
        //   336: astore_1       
        //   337: goto            127
        //    Exceptions:
        //  throws java.security.InvalidKeyException
        //  throws java.security.NoSuchAlgorithmException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                                
        //  -----  -----  -----  -----  ----------------------------------------------------
        //  0      16     323    336    Lorg/bouncycastle/crypto/InvalidCipherTextException;
        //  0      16     310    323    Ljavax/crypto/BadPaddingException;
        //  0      16     297    310    Ljavax/crypto/IllegalBlockSizeException;
        //  19     33     323    336    Lorg/bouncycastle/crypto/InvalidCipherTextException;
        //  19     33     310    323    Ljavax/crypto/BadPaddingException;
        //  19     33     297    310    Ljavax/crypto/IllegalBlockSizeException;
        //  63     73     336    138    Ljava/lang/Exception;
        //  79     127    336    138    Ljava/lang/Exception;
        //  138    149    259    297    Ljava/security/NoSuchProviderException;
        //  138    149    221    259    Ljava/security/spec/InvalidKeySpecException;
        //  154    167    259    297    Ljava/security/NoSuchProviderException;
        //  154    167    221    259    Ljava/security/spec/InvalidKeySpecException;
        //  172    185    259    297    Ljava/security/NoSuchProviderException;
        //  172    185    221    259    Ljava/security/spec/InvalidKeySpecException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0079:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    protected int engineUpdate(final byte[] b, final int off, final int len, final byte[] array, final int n) throws ShortBufferException {
        final ErasableOutputStream wrapStream = this.wrapStream;
        if (wrapStream != null) {
            wrapStream.write(b, off, len);
            return 0;
        }
        throw new IllegalStateException("not supported in a wrapping mode");
    }
    
    @Override
    protected byte[] engineUpdate(final byte[] b, final int off, final int len) {
        final ErasableOutputStream wrapStream = this.wrapStream;
        if (wrapStream != null) {
            wrapStream.write(b, off, len);
            return null;
        }
        throw new IllegalStateException("not supported in a wrapping mode");
    }
    
    @Override
    protected byte[] engineWrap(final Key key) throws IllegalBlockSizeException, InvalidKeyException {
        final byte[] encoded = key.getEncoded();
        if (encoded != null) {
            try {
                if (this.wrapEngine == null) {
                    return this.engineDoFinal(encoded, 0, encoded.length);
                }
                return this.wrapEngine.wrap(encoded, 0, encoded.length);
            }
            catch (BadPaddingException ex) {
                throw new IllegalBlockSizeException(ex.getMessage());
            }
        }
        throw new InvalidKeyException("Cannot wrap key, null encoding.");
    }
    
    protected static final class ErasableOutputStream extends ByteArrayOutputStream
    {
        public ErasableOutputStream() {
        }
        
        public void erase() {
            Arrays.fill(this.buf, (byte)0);
            this.reset();
        }
        
        public byte[] getBuf() {
            return this.buf;
        }
    }
    
    protected static class InvalidKeyOrParametersException extends InvalidKeyException
    {
        private final Throwable cause;
        
        InvalidKeyOrParametersException(final String msg, final Throwable cause) {
            super(msg);
            this.cause = cause;
        }
        
        @Override
        public Throwable getCause() {
            return this.cause;
        }
    }
}
