// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.symmetric;

import org.bouncycastle.crypto.macs.Zuc256Mac;
import org.bouncycastle.crypto.Mac;
import org.bouncycastle.crypto.macs.Zuc128Mac;
import org.bouncycastle.jcajce.provider.symmetric.util.BaseMac;
import org.bouncycastle.crypto.engines.Zuc256Engine;
import org.bouncycastle.crypto.StreamCipher;
import org.bouncycastle.crypto.engines.Zuc128Engine;
import org.bouncycastle.jcajce.provider.symmetric.util.BaseStreamCipher;
import org.bouncycastle.jcajce.provider.config.ConfigurableProvider;
import org.bouncycastle.crypto.CipherKeyGenerator;
import org.bouncycastle.jcajce.provider.symmetric.util.BaseKeyGenerator;
import org.bouncycastle.jcajce.provider.symmetric.util.IvAlgorithmParameters;

public class Zuc
{
    private Zuc() {
    }
    
    public static class AlgParams extends IvAlgorithmParameters
    {
        @Override
        protected String engineToString() {
            return "Zuc IV";
        }
    }
    
    public static class KeyGen128 extends BaseKeyGenerator
    {
        public KeyGen128() {
            super("ZUC128", 128, new CipherKeyGenerator());
        }
    }
    
    public static class KeyGen256 extends BaseKeyGenerator
    {
        public KeyGen256() {
            super("ZUC256", 256, new CipherKeyGenerator());
        }
    }
    
    public static class Mappings extends SymmetricAlgorithmProvider
    {
        private static final String PREFIX;
        
        static {
            PREFIX = Zuc.class.getName();
        }
        
        @Override
        public void configure(final ConfigurableProvider configurableProvider) {
            final StringBuilder sb = new StringBuilder();
            sb.append(Mappings.PREFIX);
            sb.append("$Zuc128");
            configurableProvider.addAlgorithm("Cipher.ZUC-128", sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(Mappings.PREFIX);
            sb2.append("$KeyGen128");
            configurableProvider.addAlgorithm("KeyGenerator.ZUC-128", sb2.toString());
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(Mappings.PREFIX);
            sb3.append("$AlgParams");
            configurableProvider.addAlgorithm("AlgorithmParameters.ZUC-128", sb3.toString());
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(Mappings.PREFIX);
            sb4.append("$Zuc256");
            configurableProvider.addAlgorithm("Cipher.ZUC-256", sb4.toString());
            final StringBuilder sb5 = new StringBuilder();
            sb5.append(Mappings.PREFIX);
            sb5.append("$KeyGen256");
            configurableProvider.addAlgorithm("KeyGenerator.ZUC-256", sb5.toString());
            final StringBuilder sb6 = new StringBuilder();
            sb6.append(Mappings.PREFIX);
            sb6.append("$AlgParams");
            configurableProvider.addAlgorithm("AlgorithmParameters.ZUC-256", sb6.toString());
            final StringBuilder sb7 = new StringBuilder();
            sb7.append(Mappings.PREFIX);
            sb7.append("$ZucMac128");
            configurableProvider.addAlgorithm("Mac.ZUC-128", sb7.toString());
            final StringBuilder sb8 = new StringBuilder();
            sb8.append(Mappings.PREFIX);
            sb8.append("$ZucMac256");
            configurableProvider.addAlgorithm("Mac.ZUC-256", sb8.toString());
            configurableProvider.addAlgorithm("Alg.Alias.Mac.ZUC-256-128", "ZUC-256");
            final StringBuilder sb9 = new StringBuilder();
            sb9.append(Mappings.PREFIX);
            sb9.append("$ZucMac256_64");
            configurableProvider.addAlgorithm("Mac.ZUC-256-64", sb9.toString());
            final StringBuilder sb10 = new StringBuilder();
            sb10.append(Mappings.PREFIX);
            sb10.append("$ZucMac256_32");
            configurableProvider.addAlgorithm("Mac.ZUC-256-32", sb10.toString());
        }
    }
    
    public static class Zuc128 extends BaseStreamCipher
    {
        public Zuc128() {
            super(new Zuc128Engine(), 16, 128);
        }
    }
    
    public static class Zuc256 extends BaseStreamCipher
    {
        public Zuc256() {
            super(new Zuc256Engine(), 25, 256);
        }
    }
    
    public static class ZucMac128 extends BaseMac
    {
        public ZucMac128() {
            super(new Zuc128Mac());
        }
    }
    
    public static class ZucMac256 extends BaseMac
    {
        public ZucMac256() {
            super(new Zuc256Mac(128));
        }
    }
    
    public static class ZucMac256_32 extends BaseMac
    {
        public ZucMac256_32() {
            super(new Zuc256Mac(32));
        }
    }
    
    public static class ZucMac256_64 extends BaseMac
    {
        public ZucMac256_64() {
            super(new Zuc256Mac(64));
        }
    }
}
