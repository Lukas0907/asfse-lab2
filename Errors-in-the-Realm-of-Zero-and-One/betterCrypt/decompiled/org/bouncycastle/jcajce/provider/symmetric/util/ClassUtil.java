// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.symmetric.util;

import java.security.AccessController;
import java.security.PrivilegedAction;

public class ClassUtil
{
    public static Class loadClass(Class clazz, final String name) {
        try {
            final ClassLoader classLoader = clazz.getClassLoader();
            if (classLoader != null) {
                return classLoader.loadClass(name);
            }
            clazz = AccessController.doPrivileged((PrivilegedAction<Class>)new PrivilegedAction() {
                @Override
                public Object run() {
                    try {
                        return Class.forName(name);
                    }
                    catch (Exception ex) {
                        return null;
                    }
                }
            });
            return clazz;
        }
        catch (ClassNotFoundException ex) {
            return null;
        }
    }
}
