// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.symmetric.util;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.Key;
import java.util.Iterator;
import java.util.Hashtable;
import java.util.Map;
import org.bouncycastle.crypto.Mac;
import javax.crypto.MacSpi;

public class BaseMac extends MacSpi implements PBE
{
    private static final Class gcmSpecClass;
    private int keySize;
    private Mac macEngine;
    private int pbeHash;
    private int scheme;
    
    static {
        gcmSpecClass = ClassUtil.loadClass(BaseMac.class, "javax.crypto.spec.GCMParameterSpec");
    }
    
    protected BaseMac(final Mac macEngine) {
        this.scheme = 2;
        this.pbeHash = 1;
        this.keySize = 160;
        this.macEngine = macEngine;
    }
    
    protected BaseMac(final Mac macEngine, final int scheme, final int pbeHash, final int keySize) {
        this.scheme = 2;
        this.pbeHash = 1;
        this.keySize = 160;
        this.macEngine = macEngine;
        this.scheme = scheme;
        this.pbeHash = pbeHash;
        this.keySize = keySize;
    }
    
    private static Hashtable copyMap(final Map map) {
        final Hashtable<Object, Object> hashtable = new Hashtable<Object, Object>();
        for (final Object next : map.keySet()) {
            hashtable.put(next, map.get(next));
        }
        return hashtable;
    }
    
    @Override
    protected byte[] engineDoFinal() {
        final byte[] array = new byte[this.engineGetMacLength()];
        this.macEngine.doFinal(array, 0);
        return array;
    }
    
    @Override
    protected int engineGetMacLength() {
        return this.macEngine.getMacSize();
    }
    
    @Override
    protected void engineInit(final Key p0, final AlgorithmParameterSpec p1) throws InvalidKeyException, InvalidAlgorithmParameterException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: ifnull          877
        //     4: aload_1        
        //     5: instanceof      Lorg/bouncycastle/jcajce/PKCS12Key;
        //     8: ifeq            354
        //    11: aload_1        
        //    12: checkcast       Ljavax/crypto/SecretKey;
        //    15: astore          9
        //    17: aload_2        
        //    18: checkcast       Ljavax/crypto/spec/PBEParameterSpec;
        //    21: astore          8
        //    23: aload           8
        //    25: astore          7
        //    27: aload           9
        //    29: instanceof      Ljavax/crypto/interfaces/PBEKey;
        //    32: ifeq            74
        //    35: aload           8
        //    37: astore          7
        //    39: aload           8
        //    41: ifnonnull       74
        //    44: aload           9
        //    46: checkcast       Ljavax/crypto/interfaces/PBEKey;
        //    49: astore          7
        //    51: new             Ljavax/crypto/spec/PBEParameterSpec;
        //    54: dup            
        //    55: aload           7
        //    57: invokeinterface javax/crypto/interfaces/PBEKey.getSalt:()[B
        //    62: aload           7
        //    64: invokeinterface javax/crypto/interfaces/PBEKey.getIterationCount:()I
        //    69: invokespecial   javax/crypto/spec/PBEParameterSpec.<init>:([BI)V
        //    72: astore          7
        //    74: iconst_1       
        //    75: istore          5
        //    77: aload_0        
        //    78: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseMac.macEngine:Lorg/bouncycastle/crypto/Mac;
        //    81: invokeinterface org/bouncycastle/crypto/Mac.getAlgorithmName:()Ljava/lang/String;
        //    86: ldc             "GOST"
        //    88: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //    91: istore          6
        //    93: sipush          256
        //    96: istore          4
        //    98: iload           6
        //   100: ifeq            109
        //   103: bipush          6
        //   105: istore_3       
        //   106: goto            318
        //   109: aload_0        
        //   110: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseMac.macEngine:Lorg/bouncycastle/crypto/Mac;
        //   113: astore          8
        //   115: iload           5
        //   117: istore_3       
        //   118: aload           8
        //   120: instanceof      Lorg/bouncycastle/crypto/macs/HMac;
        //   123: ifeq            313
        //   126: iload           5
        //   128: istore_3       
        //   129: aload           8
        //   131: invokeinterface org/bouncycastle/crypto/Mac.getAlgorithmName:()Ljava/lang/String;
        //   136: ldc             "SHA-1"
        //   138: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   141: ifne            313
        //   144: aload_0        
        //   145: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseMac.macEngine:Lorg/bouncycastle/crypto/Mac;
        //   148: invokeinterface org/bouncycastle/crypto/Mac.getAlgorithmName:()Ljava/lang/String;
        //   153: ldc             "SHA-224"
        //   155: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   158: ifeq            172
        //   161: bipush          7
        //   163: istore_3       
        //   164: sipush          224
        //   167: istore          4
        //   169: goto            318
        //   172: aload_0        
        //   173: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseMac.macEngine:Lorg/bouncycastle/crypto/Mac;
        //   176: invokeinterface org/bouncycastle/crypto/Mac.getAlgorithmName:()Ljava/lang/String;
        //   181: ldc             "SHA-256"
        //   183: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   186: ifeq            194
        //   189: iconst_4       
        //   190: istore_3       
        //   191: goto            318
        //   194: aload_0        
        //   195: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseMac.macEngine:Lorg/bouncycastle/crypto/Mac;
        //   198: invokeinterface org/bouncycastle/crypto/Mac.getAlgorithmName:()Ljava/lang/String;
        //   203: ldc             "SHA-384"
        //   205: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   208: ifeq            222
        //   211: bipush          8
        //   213: istore_3       
        //   214: sipush          384
        //   217: istore          4
        //   219: goto            318
        //   222: aload_0        
        //   223: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseMac.macEngine:Lorg/bouncycastle/crypto/Mac;
        //   226: invokeinterface org/bouncycastle/crypto/Mac.getAlgorithmName:()Ljava/lang/String;
        //   231: ldc             "SHA-512"
        //   233: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   236: ifeq            250
        //   239: bipush          9
        //   241: istore_3       
        //   242: sipush          512
        //   245: istore          4
        //   247: goto            318
        //   250: aload_0        
        //   251: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseMac.macEngine:Lorg/bouncycastle/crypto/Mac;
        //   254: invokeinterface org/bouncycastle/crypto/Mac.getAlgorithmName:()Ljava/lang/String;
        //   259: ldc             "RIPEMD160"
        //   261: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   264: ifeq            272
        //   267: iconst_2       
        //   268: istore_3       
        //   269: goto            313
        //   272: new             Ljava/lang/StringBuilder;
        //   275: dup            
        //   276: invokespecial   java/lang/StringBuilder.<init>:()V
        //   279: astore_1       
        //   280: aload_1        
        //   281: ldc             "no PKCS12 mapping for HMAC: "
        //   283: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   286: pop            
        //   287: aload_1        
        //   288: aload_0        
        //   289: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseMac.macEngine:Lorg/bouncycastle/crypto/Mac;
        //   292: invokeinterface org/bouncycastle/crypto/Mac.getAlgorithmName:()Ljava/lang/String;
        //   297: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   300: pop            
        //   301: new             Ljava/security/InvalidAlgorithmParameterException;
        //   304: dup            
        //   305: aload_1        
        //   306: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   309: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;)V
        //   312: athrow         
        //   313: sipush          160
        //   316: istore          4
        //   318: aload           9
        //   320: iconst_2       
        //   321: iload_3        
        //   322: iload           4
        //   324: aload           7
        //   326: invokestatic    org/bouncycastle/jcajce/provider/symmetric/util/PBE$Util.makePBEMacParameters:(Ljavax/crypto/SecretKey;IIILjavax/crypto/spec/PBEParameterSpec;)Lorg/bouncycastle/crypto/CipherParameters;
        //   329: astore          7
        //   331: goto            435
        //   334: new             Ljava/security/InvalidAlgorithmParameterException;
        //   337: dup            
        //   338: ldc             "PKCS12 requires a PBEParameterSpec"
        //   340: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;)V
        //   343: athrow         
        //   344: new             Ljava/security/InvalidKeyException;
        //   347: dup            
        //   348: ldc             "PKCS12 requires a SecretKey/PBEKey"
        //   350: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   353: athrow         
        //   354: aload_1        
        //   355: instanceof      Lorg/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;
        //   358: ifeq            413
        //   361: aload_1        
        //   362: checkcast       Lorg/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;
        //   365: astore          7
        //   367: aload           7
        //   369: invokevirtual   org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey.getParam:()Lorg/bouncycastle/crypto/CipherParameters;
        //   372: ifnull          385
        //   375: aload           7
        //   377: invokevirtual   org/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey.getParam:()Lorg/bouncycastle/crypto/CipherParameters;
        //   380: astore          7
        //   382: goto            435
        //   385: aload_2        
        //   386: instanceof      Ljavax/crypto/spec/PBEParameterSpec;
        //   389: ifeq            403
        //   392: aload           7
        //   394: aload_2        
        //   395: invokestatic    org/bouncycastle/jcajce/provider/symmetric/util/PBE$Util.makePBEMacParameters:(Lorg/bouncycastle/jcajce/provider/symmetric/util/BCPBEKey;Ljava/security/spec/AlgorithmParameterSpec;)Lorg/bouncycastle/crypto/CipherParameters;
        //   398: astore          7
        //   400: goto            435
        //   403: new             Ljava/security/InvalidAlgorithmParameterException;
        //   406: dup            
        //   407: ldc             "PBE requires PBE parameters to be set."
        //   409: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;)V
        //   412: athrow         
        //   413: aload_2        
        //   414: instanceof      Ljavax/crypto/spec/PBEParameterSpec;
        //   417: ifne            837
        //   420: new             Lorg/bouncycastle/crypto/params/KeyParameter;
        //   423: dup            
        //   424: aload_1        
        //   425: invokeinterface java/security/Key.getEncoded:()[B
        //   430: invokespecial   org/bouncycastle/crypto/params/KeyParameter.<init>:([B)V
        //   433: astore          7
        //   435: aload           7
        //   437: instanceof      Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //   440: ifeq            459
        //   443: aload           7
        //   445: checkcast       Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //   448: invokevirtual   org/bouncycastle/crypto/params/ParametersWithIV.getParameters:()Lorg/bouncycastle/crypto/CipherParameters;
        //   451: checkcast       Lorg/bouncycastle/crypto/params/KeyParameter;
        //   454: astore          8
        //   456: goto            466
        //   459: aload           7
        //   461: checkcast       Lorg/bouncycastle/crypto/params/KeyParameter;
        //   464: astore          8
        //   466: aload_2        
        //   467: instanceof      Lorg/bouncycastle/jcajce/spec/AEADParameterSpec;
        //   470: ifeq            503
        //   473: aload_2        
        //   474: checkcast       Lorg/bouncycastle/jcajce/spec/AEADParameterSpec;
        //   477: astore_1       
        //   478: new             Lorg/bouncycastle/crypto/params/AEADParameters;
        //   481: dup            
        //   482: aload           8
        //   484: aload_1        
        //   485: invokevirtual   org/bouncycastle/jcajce/spec/AEADParameterSpec.getMacSizeInBits:()I
        //   488: aload_1        
        //   489: invokevirtual   org/bouncycastle/jcajce/spec/AEADParameterSpec.getNonce:()[B
        //   492: aload_1        
        //   493: invokevirtual   org/bouncycastle/jcajce/spec/AEADParameterSpec.getAssociatedData:()[B
        //   496: invokespecial   org/bouncycastle/crypto/params/AEADParameters.<init>:(Lorg/bouncycastle/crypto/params/KeyParameter;I[B[B)V
        //   499: astore_1       
        //   500: goto            748
        //   503: aload_2        
        //   504: instanceof      Ljavax/crypto/spec/IvParameterSpec;
        //   507: ifeq            530
        //   510: new             Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //   513: dup            
        //   514: aload           8
        //   516: aload_2        
        //   517: checkcast       Ljavax/crypto/spec/IvParameterSpec;
        //   520: invokevirtual   javax/crypto/spec/IvParameterSpec.getIV:()[B
        //   523: invokespecial   org/bouncycastle/crypto/params/ParametersWithIV.<init>:(Lorg/bouncycastle/crypto/CipherParameters;[B)V
        //   526: astore_1       
        //   527: goto            748
        //   530: aload_2        
        //   531: instanceof      Ljavax/crypto/spec/RC2ParameterSpec;
        //   534: ifeq            575
        //   537: aload           8
        //   539: invokevirtual   org/bouncycastle/crypto/params/KeyParameter.getKey:()[B
        //   542: astore_1       
        //   543: aload_2        
        //   544: checkcast       Ljavax/crypto/spec/RC2ParameterSpec;
        //   547: astore_2       
        //   548: new             Lorg/bouncycastle/crypto/params/ParametersWithIV;
        //   551: dup            
        //   552: new             Lorg/bouncycastle/crypto/params/RC2Parameters;
        //   555: dup            
        //   556: aload_1        
        //   557: aload_2        
        //   558: invokevirtual   javax/crypto/spec/RC2ParameterSpec.getEffectiveKeyBits:()I
        //   561: invokespecial   org/bouncycastle/crypto/params/RC2Parameters.<init>:([BI)V
        //   564: aload_2        
        //   565: invokevirtual   javax/crypto/spec/RC2ParameterSpec.getIV:()[B
        //   568: invokespecial   org/bouncycastle/crypto/params/ParametersWithIV.<init>:(Lorg/bouncycastle/crypto/CipherParameters;[B)V
        //   571: astore_1       
        //   572: goto            748
        //   575: aload_2        
        //   576: instanceof      Lorg/bouncycastle/jcajce/spec/SkeinParameterSpec;
        //   579: ifeq            614
        //   582: new             Lorg/bouncycastle/crypto/params/SkeinParameters$Builder;
        //   585: dup            
        //   586: aload_2        
        //   587: checkcast       Lorg/bouncycastle/jcajce/spec/SkeinParameterSpec;
        //   590: invokevirtual   org/bouncycastle/jcajce/spec/SkeinParameterSpec.getParameters:()Ljava/util/Map;
        //   593: invokestatic    org/bouncycastle/jcajce/provider/symmetric/util/BaseMac.copyMap:(Ljava/util/Map;)Ljava/util/Hashtable;
        //   596: invokespecial   org/bouncycastle/crypto/params/SkeinParameters$Builder.<init>:(Ljava/util/Hashtable;)V
        //   599: aload           8
        //   601: invokevirtual   org/bouncycastle/crypto/params/KeyParameter.getKey:()[B
        //   604: invokevirtual   org/bouncycastle/crypto/params/SkeinParameters$Builder.setKey:([B)Lorg/bouncycastle/crypto/params/SkeinParameters$Builder;
        //   607: invokevirtual   org/bouncycastle/crypto/params/SkeinParameters$Builder.build:()Lorg/bouncycastle/crypto/params/SkeinParameters;
        //   610: astore_1       
        //   611: goto            748
        //   614: aload_2        
        //   615: ifnonnull       635
        //   618: new             Lorg/bouncycastle/crypto/params/KeyParameter;
        //   621: dup            
        //   622: aload_1        
        //   623: invokeinterface java/security/Key.getEncoded:()[B
        //   628: invokespecial   org/bouncycastle/crypto/params/KeyParameter.<init>:([B)V
        //   631: astore_1       
        //   632: goto            748
        //   635: getstatic       org/bouncycastle/jcajce/provider/symmetric/util/BaseMac.gcmSpecClass:Ljava/lang/Class;
        //   638: astore_1       
        //   639: aload_1        
        //   640: ifnull          738
        //   643: aload_1        
        //   644: aload_2        
        //   645: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //   648: invokevirtual   java/lang/Class.isAssignableFrom:(Ljava/lang/Class;)Z
        //   651: ifeq            738
        //   654: getstatic       org/bouncycastle/jcajce/provider/symmetric/util/BaseMac.gcmSpecClass:Ljava/lang/Class;
        //   657: ldc_w           "getTLen"
        //   660: iconst_0       
        //   661: anewarray       Ljava/lang/Class;
        //   664: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //   667: astore_1       
        //   668: getstatic       org/bouncycastle/jcajce/provider/symmetric/util/BaseMac.gcmSpecClass:Ljava/lang/Class;
        //   671: ldc_w           "getIV"
        //   674: iconst_0       
        //   675: anewarray       Ljava/lang/Class;
        //   678: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //   681: astore          7
        //   683: new             Lorg/bouncycastle/crypto/params/AEADParameters;
        //   686: dup            
        //   687: aload           8
        //   689: aload_1        
        //   690: aload_2        
        //   691: iconst_0       
        //   692: anewarray       Ljava/lang/Object;
        //   695: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //   698: checkcast       Ljava/lang/Integer;
        //   701: invokevirtual   java/lang/Integer.intValue:()I
        //   704: aload           7
        //   706: aload_2        
        //   707: iconst_0       
        //   708: anewarray       Ljava/lang/Object;
        //   711: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //   714: checkcast       [B
        //   717: checkcast       [B
        //   720: invokespecial   org/bouncycastle/crypto/params/AEADParameters.<init>:(Lorg/bouncycastle/crypto/params/KeyParameter;I[B)V
        //   723: astore_1       
        //   724: goto            748
        //   727: new             Ljava/security/InvalidAlgorithmParameterException;
        //   730: dup            
        //   731: ldc_w           "Cannot process GCMParameterSpec."
        //   734: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;)V
        //   737: athrow         
        //   738: aload_2        
        //   739: instanceof      Ljavax/crypto/spec/PBEParameterSpec;
        //   742: ifeq            797
        //   745: aload           7
        //   747: astore_1       
        //   748: aload_0        
        //   749: getfield        org/bouncycastle/jcajce/provider/symmetric/util/BaseMac.macEngine:Lorg/bouncycastle/crypto/Mac;
        //   752: aload_1        
        //   753: invokeinterface org/bouncycastle/crypto/Mac.init:(Lorg/bouncycastle/crypto/CipherParameters;)V
        //   758: return         
        //   759: astore_1       
        //   760: new             Ljava/lang/StringBuilder;
        //   763: dup            
        //   764: invokespecial   java/lang/StringBuilder.<init>:()V
        //   767: astore_2       
        //   768: aload_2        
        //   769: ldc_w           "cannot initialize MAC: "
        //   772: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   775: pop            
        //   776: aload_2        
        //   777: aload_1        
        //   778: invokevirtual   java/lang/Exception.getMessage:()Ljava/lang/String;
        //   781: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   784: pop            
        //   785: new             Ljava/security/InvalidAlgorithmParameterException;
        //   788: dup            
        //   789: aload_2        
        //   790: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   793: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;)V
        //   796: athrow         
        //   797: new             Ljava/lang/StringBuilder;
        //   800: dup            
        //   801: invokespecial   java/lang/StringBuilder.<init>:()V
        //   804: astore_1       
        //   805: aload_1        
        //   806: ldc_w           "unknown parameter type: "
        //   809: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   812: pop            
        //   813: aload_1        
        //   814: aload_2        
        //   815: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //   818: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   821: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   824: pop            
        //   825: new             Ljava/security/InvalidAlgorithmParameterException;
        //   828: dup            
        //   829: aload_1        
        //   830: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   833: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;)V
        //   836: athrow         
        //   837: new             Ljava/lang/StringBuilder;
        //   840: dup            
        //   841: invokespecial   java/lang/StringBuilder.<init>:()V
        //   844: astore_1       
        //   845: aload_1        
        //   846: ldc_w           "inappropriate parameter type: "
        //   849: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   852: pop            
        //   853: aload_1        
        //   854: aload_2        
        //   855: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //   858: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   861: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   864: pop            
        //   865: new             Ljava/security/InvalidAlgorithmParameterException;
        //   868: dup            
        //   869: aload_1        
        //   870: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   873: invokespecial   java/security/InvalidAlgorithmParameterException.<init>:(Ljava/lang/String;)V
        //   876: athrow         
        //   877: new             Ljava/security/InvalidKeyException;
        //   880: dup            
        //   881: ldc_w           "key is null"
        //   884: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   887: athrow         
        //   888: astore_1       
        //   889: goto            344
        //   892: astore_1       
        //   893: goto            334
        //   896: astore_1       
        //   897: goto            727
        //    Exceptions:
        //  throws java.security.InvalidKeyException
        //  throws java.security.InvalidAlgorithmParameterException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  11     17     888    354    Ljava/lang/Exception;
        //  17     23     892    344    Ljava/lang/Exception;
        //  654    724    896    738    Ljava/lang/Exception;
        //  748    758    759    797    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 403 out of bounds for length 403
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:373)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:426)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    protected void engineReset() {
        this.macEngine.reset();
    }
    
    @Override
    protected void engineUpdate(final byte b) {
        this.macEngine.update(b);
    }
    
    @Override
    protected void engineUpdate(final byte[] array, final int n, final int n2) {
        this.macEngine.update(array, n, n2);
    }
}
