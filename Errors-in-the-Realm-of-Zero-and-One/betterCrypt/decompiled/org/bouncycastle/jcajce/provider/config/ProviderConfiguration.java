// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.config;

import org.bouncycastle.jce.spec.ECParameterSpec;
import java.security.spec.DSAParameterSpec;
import javax.crypto.spec.DHParameterSpec;
import java.util.Map;
import java.util.Set;

public interface ProviderConfiguration
{
    Set getAcceptableNamedCurves();
    
    Map getAdditionalECParameters();
    
    DHParameterSpec getDHDefaultParameters(final int p0);
    
    DSAParameterSpec getDSADefaultParameters(final int p0);
    
    ECParameterSpec getEcImplicitlyCa();
}
