// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.keystore.pkcs12;

import org.bouncycastle.util.Strings;
import java.util.Collections;
import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import org.bouncycastle.asn1.ntt.NTTObjectIdentifiers;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.util.Integers;
import java.util.HashMap;
import java.util.Map;
import org.bouncycastle.jcajce.util.DefaultJcaJceHelper;
import java.security.cert.CertificateException;
import org.bouncycastle.jce.provider.JDKPKCS12StoreParameter;
import org.bouncycastle.jcajce.PKCS12StoreParameter;
import java.security.KeyStore;
import java.io.PrintStream;
import java.io.ByteArrayInputStream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.util.ASN1Dump;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.util.Arrays;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.security.UnrecoverableKeyException;
import java.util.Date;
import java.security.Principal;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.x509.Extension;
import java.security.cert.X509Certificate;
import java.util.Vector;
import java.security.KeyStoreException;
import org.bouncycastle.util.Properties;
import java.math.BigInteger;
import java.util.HashSet;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.util.DigestFactory;
import java.util.Set;
import org.bouncycastle.asn1.x509.DigestInfo;
import org.bouncycastle.asn1.pkcs.AuthenticatedSafe;
import org.bouncycastle.asn1.pkcs.EncryptedData;
import org.bouncycastle.asn1.pkcs.EncryptedPrivateKeyInfo;
import org.bouncycastle.asn1.pkcs.PKCS12PBEParams;
import java.security.PrivateKey;
import org.bouncycastle.asn1.BEROctetString;
import org.bouncycastle.asn1.BERSequence;
import org.bouncycastle.asn1.pkcs.MacData;
import org.bouncycastle.asn1.pkcs.Pfx;
import org.bouncycastle.asn1.pkcs.ContentInfo;
import java.io.IOException;
import java.io.OutputStream;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import java.security.cert.CertificateEncodingException;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.DERBMPString;
import org.bouncycastle.jce.interfaces.PKCS12BagAttributeCarrier;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.pkcs.CertBag;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.pkcs.SafeBag;
import java.security.cert.Certificate;
import java.security.NoSuchProviderException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import javax.crypto.NoSuchPaddingException;
import java.security.spec.InvalidKeySpecException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import org.bouncycastle.jcajce.spec.GOST28147ParameterSpec;
import org.bouncycastle.asn1.cryptopro.GOST28147Parameters;
import javax.crypto.spec.IvParameterSpec;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.jcajce.spec.PBKDF2KeySpec;
import java.security.spec.KeySpec;
import javax.crypto.spec.PBEKeySpec;
import org.bouncycastle.asn1.pkcs.PBKDF2Params;
import org.bouncycastle.asn1.pkcs.PBES2Parameters;
import javax.crypto.Cipher;
import javax.crypto.Mac;
import java.security.spec.AlgorithmParameterSpec;
import java.security.Key;
import org.bouncycastle.jcajce.PKCS12Key;
import javax.crypto.spec.PBEParameterSpec;
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;
import java.security.PublicKey;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import org.bouncycastle.jcajce.util.BCJcaJceHelper;
import java.security.SecureRandom;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.jcajce.util.JcaJceHelper;
import java.util.Hashtable;
import java.security.cert.CertificateFactory;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.jce.interfaces.BCKeyStore;
import org.bouncycastle.asn1.x509.X509ObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import java.security.KeyStoreSpi;

public class PKCS12KeyStoreSpi extends KeyStoreSpi implements PKCSObjectIdentifiers, X509ObjectIdentifiers, BCKeyStore
{
    static final int CERTIFICATE = 1;
    static final int KEY = 2;
    static final int KEY_PRIVATE = 0;
    static final int KEY_PUBLIC = 1;
    static final int KEY_SECRET = 2;
    private static final int MIN_ITERATIONS = 51200;
    static final int NULL = 0;
    static final String PKCS12_MAX_IT_COUNT_PROPERTY = "org.bouncycastle.pkcs12.max_it_count";
    private static final int SALT_SIZE = 20;
    static final int SEALED = 4;
    static final int SECRET = 3;
    private static final DefaultSecretKeyProvider keySizeProvider;
    private ASN1ObjectIdentifier certAlgorithm;
    private CertificateFactory certFact;
    private IgnoresCaseHashtable certs;
    private Hashtable chainCerts;
    private final JcaJceHelper helper;
    private int itCount;
    private ASN1ObjectIdentifier keyAlgorithm;
    private Hashtable keyCerts;
    private IgnoresCaseHashtable keys;
    private Hashtable localIds;
    private AlgorithmIdentifier macAlgorithm;
    protected SecureRandom random;
    private int saltLength;
    
    static {
        keySizeProvider = new DefaultSecretKeyProvider();
    }
    
    public PKCS12KeyStoreSpi(final JcaJceHelper jcaJceHelper, final ASN1ObjectIdentifier keyAlgorithm, final ASN1ObjectIdentifier certAlgorithm) {
        this.helper = new BCJcaJceHelper();
        this.keys = new IgnoresCaseHashtable();
        this.localIds = new Hashtable();
        this.certs = new IgnoresCaseHashtable();
        this.chainCerts = new Hashtable();
        this.keyCerts = new Hashtable();
        this.random = CryptoServicesRegistrar.getSecureRandom();
        this.macAlgorithm = new AlgorithmIdentifier(OIWObjectIdentifiers.idSHA1, DERNull.INSTANCE);
        this.itCount = 102400;
        this.saltLength = 20;
        this.keyAlgorithm = keyAlgorithm;
        this.certAlgorithm = certAlgorithm;
        try {
            this.certFact = jcaJceHelper.createCertificateFactory("X.509");
        }
        catch (Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("can't create cert factory - ");
            sb.append(ex.toString());
            throw new IllegalArgumentException(sb.toString());
        }
    }
    
    private byte[] calculatePbeMac(final ASN1ObjectIdentifier asn1ObjectIdentifier, final byte[] salt, final int iterationCount, final char[] array, final boolean b, final byte[] input) throws Exception {
        final PBEParameterSpec params = new PBEParameterSpec(salt, iterationCount);
        final Mac mac = this.helper.createMac(asn1ObjectIdentifier.getId());
        mac.init(new PKCS12Key(array, b), params);
        mac.update(input);
        return mac.doFinal();
    }
    
    private Cipher createCipher(final int opmode, final char[] password, final AlgorithmIdentifier algorithmIdentifier) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, NoSuchProviderException {
        final PBES2Parameters instance = PBES2Parameters.getInstance(algorithmIdentifier.getParameters());
        final PBKDF2Params instance2 = PBKDF2Params.getInstance(instance.getKeyDerivationFunc().getParameters());
        final AlgorithmIdentifier instance3 = AlgorithmIdentifier.getInstance(instance.getEncryptionScheme());
        final SecretKeyFactory secretKeyFactory = this.helper.createSecretKeyFactory(instance.getKeyDerivationFunc().getAlgorithm().getId());
        SecretKey key;
        if (instance2.isDefaultPrf()) {
            key = secretKeyFactory.generateSecret(new PBEKeySpec(password, instance2.getSalt(), this.validateIterationCount(instance2.getIterationCount()), PKCS12KeyStoreSpi.keySizeProvider.getKeySize(instance3)));
        }
        else {
            key = secretKeyFactory.generateSecret(new PBKDF2KeySpec(password, instance2.getSalt(), this.validateIterationCount(instance2.getIterationCount()), PKCS12KeyStoreSpi.keySizeProvider.getKeySize(instance3), instance2.getPrf()));
        }
        final Cipher instance4 = Cipher.getInstance(instance.getEncryptionScheme().getAlgorithm().getId());
        final ASN1Encodable parameters = instance.getEncryptionScheme().getParameters();
        AlgorithmParameterSpec params;
        if (parameters instanceof ASN1OctetString) {
            params = new IvParameterSpec(ASN1OctetString.getInstance(parameters).getOctets());
        }
        else {
            final GOST28147Parameters instance5 = GOST28147Parameters.getInstance(parameters);
            params = new GOST28147ParameterSpec(instance5.getEncryptionParamSet(), instance5.getIV());
        }
        instance4.init(opmode, key, params);
        return instance4;
    }
    
    private SafeBag createSafeBag(final String anObject, final Certificate certificate) throws CertificateEncodingException {
        final CertBag certBag = new CertBag(PKCS12KeyStoreSpi.x509Certificate, new DEROctetString(certificate.getEncoded()));
        final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
        final boolean b = certificate instanceof PKCS12BagAttributeCarrier;
        int n = 0;
        int n2 = 0;
        if (b) {
            final PKCS12BagAttributeCarrier pkcs12BagAttributeCarrier = (PKCS12BagAttributeCarrier)certificate;
            final DERBMPString derbmpString = (DERBMPString)pkcs12BagAttributeCarrier.getBagAttribute(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName);
            if ((derbmpString == null || !derbmpString.getString().equals(anObject)) && anObject != null) {
                pkcs12BagAttributeCarrier.setBagAttribute(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName, new DERBMPString(anObject));
            }
            final Enumeration bagAttributeKeys = pkcs12BagAttributeCarrier.getBagAttributeKeys();
            while (true) {
                n = n2;
                if (!bagAttributeKeys.hasMoreElements()) {
                    break;
                }
                final ASN1ObjectIdentifier asn1ObjectIdentifier = bagAttributeKeys.nextElement();
                if (asn1ObjectIdentifier.equals(PKCSObjectIdentifiers.pkcs_9_at_localKeyId)) {
                    continue;
                }
                final ASN1EncodableVector asn1EncodableVector2 = new ASN1EncodableVector();
                asn1EncodableVector2.add(asn1ObjectIdentifier);
                asn1EncodableVector2.add(new DERSet(pkcs12BagAttributeCarrier.getBagAttribute(asn1ObjectIdentifier)));
                asn1EncodableVector.add(new DERSequence(asn1EncodableVector2));
                n2 = 1;
            }
        }
        if (n == 0) {
            final ASN1EncodableVector asn1EncodableVector3 = new ASN1EncodableVector();
            asn1EncodableVector3.add(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName);
            asn1EncodableVector3.add(new DERSet(new DERBMPString(anObject)));
            asn1EncodableVector.add(new DERSequence(asn1EncodableVector3));
        }
        return new SafeBag(PKCS12KeyStoreSpi.certBag, certBag.toASN1Primitive(), new DERSet(asn1EncodableVector));
    }
    
    private SubjectKeyIdentifier createSubjectKeyId(final PublicKey publicKey) {
        try {
            return new SubjectKeyIdentifier(getDigest(SubjectPublicKeyInfo.getInstance(publicKey.getEncoded())));
        }
        catch (Exception ex) {
            throw new RuntimeException("error creating key");
        }
    }
    
    private void doStore(final OutputStream outputStream, final char[] array, final boolean b) throws IOException {
        final int size = this.keys.size();
        String s = "BER";
        final String s2 = "DER";
        if (size == 0) {
            if (array == null) {
                final Enumeration keys = this.certs.keys();
                final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
                while (keys.hasMoreElements()) {
                    try {
                        final String s3 = keys.nextElement();
                        asn1EncodableVector.add(this.createSafeBag(s3, (Certificate)this.certs.get(s3)));
                        continue;
                    }
                    catch (CertificateEncodingException ex) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Error encoding certificate: ");
                        sb.append(ex.toString());
                        throw new IOException(sb.toString());
                    }
                    break;
                }
                final ASN1ObjectIdentifier data = PKCSObjectIdentifiers.data;
                if (b) {
                    new Pfx(new ContentInfo(PKCSObjectIdentifiers.data, new DEROctetString(new DERSequence(new ContentInfo(data, new DEROctetString(new DERSequence(asn1EncodableVector).getEncoded()))).getEncoded())), null).encodeTo(outputStream, "DER");
                    return;
                }
                new Pfx(new ContentInfo(PKCSObjectIdentifiers.data, new BEROctetString(new BERSequence(new ContentInfo(data, new BEROctetString(new BERSequence(asn1EncodableVector).getEncoded()))).getEncoded())), null).encodeTo(outputStream, "BER");
                return;
            }
        }
        else if (array == null) {
            throw new NullPointerException("no password supplied for PKCS#12 KeyStore");
        }
        final ASN1EncodableVector asn1EncodableVector2 = new ASN1EncodableVector();
        final Enumeration keys2 = this.keys.keys();
        while (keys2.hasMoreElements()) {
            final byte[] bytes = new byte[20];
            this.random.nextBytes(bytes);
            final String anObject = keys2.nextElement();
            final PrivateKey privateKey = (PrivateKey)this.keys.get(anObject);
            final PKCS12PBEParams pkcs12PBEParams = new PKCS12PBEParams(bytes, 51200);
            final EncryptedPrivateKeyInfo encryptedPrivateKeyInfo = new EncryptedPrivateKeyInfo(new AlgorithmIdentifier(this.keyAlgorithm, pkcs12PBEParams.toASN1Primitive()), this.wrapKey(this.keyAlgorithm.getId(), privateKey, pkcs12PBEParams, array));
            final ASN1EncodableVector asn1EncodableVector3 = new ASN1EncodableVector();
            boolean b2;
            if (privateKey instanceof PKCS12BagAttributeCarrier) {
                final PKCS12BagAttributeCarrier pkcs12BagAttributeCarrier = (PKCS12BagAttributeCarrier)privateKey;
                final DERBMPString derbmpString = (DERBMPString)pkcs12BagAttributeCarrier.getBagAttribute(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName);
                if (derbmpString == null || !derbmpString.getString().equals(anObject)) {
                    pkcs12BagAttributeCarrier.setBagAttribute(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName, new DERBMPString(anObject));
                }
                if (pkcs12BagAttributeCarrier.getBagAttribute(PKCS12KeyStoreSpi.pkcs_9_at_localKeyId) == null) {
                    pkcs12BagAttributeCarrier.setBagAttribute(PKCS12KeyStoreSpi.pkcs_9_at_localKeyId, this.createSubjectKeyId(this.engineGetCertificate(anObject).getPublicKey()));
                }
                final Enumeration bagAttributeKeys = pkcs12BagAttributeCarrier.getBagAttributeKeys();
                b2 = false;
                while (bagAttributeKeys.hasMoreElements()) {
                    final ASN1ObjectIdentifier asn1ObjectIdentifier = bagAttributeKeys.nextElement();
                    final ASN1EncodableVector asn1EncodableVector4 = new ASN1EncodableVector();
                    asn1EncodableVector4.add(asn1ObjectIdentifier);
                    asn1EncodableVector4.add(new DERSet(pkcs12BagAttributeCarrier.getBagAttribute(asn1ObjectIdentifier)));
                    asn1EncodableVector3.add(new DERSequence(asn1EncodableVector4));
                    b2 = true;
                }
            }
            else {
                b2 = false;
            }
            if (!b2) {
                final ASN1EncodableVector asn1EncodableVector5 = new ASN1EncodableVector();
                final Certificate engineGetCertificate = this.engineGetCertificate(anObject);
                asn1EncodableVector5.add(PKCS12KeyStoreSpi.pkcs_9_at_localKeyId);
                asn1EncodableVector5.add(new DERSet(this.createSubjectKeyId(engineGetCertificate.getPublicKey())));
                asn1EncodableVector3.add(new DERSequence(asn1EncodableVector5));
                final ASN1EncodableVector asn1EncodableVector6 = new ASN1EncodableVector();
                asn1EncodableVector6.add(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName);
                asn1EncodableVector6.add(new DERSet(new DERBMPString(anObject)));
                asn1EncodableVector3.add(new DERSequence(asn1EncodableVector6));
            }
            asn1EncodableVector2.add(new SafeBag(PKCS12KeyStoreSpi.pkcs8ShroudedKeyBag, encryptedPrivateKeyInfo.toASN1Primitive(), new DERSet(asn1EncodableVector3)));
        }
        final BEROctetString berOctetString = new BEROctetString(new DERSequence(asn1EncodableVector2).getEncoded("DER"));
        final byte[] bytes2 = new byte[20];
        this.random.nextBytes(bytes2);
        final ASN1EncodableVector asn1EncodableVector7 = new ASN1EncodableVector();
        final AlgorithmIdentifier algorithmIdentifier = new AlgorithmIdentifier(this.certAlgorithm, new PKCS12PBEParams(bytes2, 51200).toASN1Primitive());
        final Hashtable<Certificate, Certificate> hashtable = new Hashtable<Certificate, Certificate>();
        final Enumeration keys3 = this.keys.keys();
    Label_1312:
        while (keys3.hasMoreElements()) {
            while (true) {
                while (true) {
                    Label_2049: {
                        Label_2046: {
                            try {
                                final String anObject2 = keys3.nextElement();
                                final Certificate engineGetCertificate2 = this.engineGetCertificate(anObject2);
                                final CertBag certBag = new CertBag(PKCS12KeyStoreSpi.x509Certificate, new DEROctetString(engineGetCertificate2.getEncoded()));
                                final ASN1EncodableVector asn1EncodableVector8 = new ASN1EncodableVector();
                                if (engineGetCertificate2 instanceof PKCS12BagAttributeCarrier) {
                                    final PKCS12BagAttributeCarrier pkcs12BagAttributeCarrier2 = (PKCS12BagAttributeCarrier)engineGetCertificate2;
                                    final DERBMPString derbmpString2 = (DERBMPString)pkcs12BagAttributeCarrier2.getBagAttribute(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName);
                                    if (derbmpString2 == null || !derbmpString2.getString().equals(anObject2)) {
                                        pkcs12BagAttributeCarrier2.setBagAttribute(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName, new DERBMPString(anObject2));
                                    }
                                    if (pkcs12BagAttributeCarrier2.getBagAttribute(PKCS12KeyStoreSpi.pkcs_9_at_localKeyId) == null) {
                                        pkcs12BagAttributeCarrier2.setBagAttribute(PKCS12KeyStoreSpi.pkcs_9_at_localKeyId, this.createSubjectKeyId(engineGetCertificate2.getPublicKey()));
                                    }
                                    final Enumeration bagAttributeKeys2 = pkcs12BagAttributeCarrier2.getBagAttributeKeys();
                                    int n = 0;
                                    while (bagAttributeKeys2.hasMoreElements()) {
                                        final ASN1ObjectIdentifier asn1ObjectIdentifier2 = bagAttributeKeys2.nextElement();
                                        final ASN1EncodableVector asn1EncodableVector9 = new ASN1EncodableVector();
                                        asn1EncodableVector9.add(asn1ObjectIdentifier2);
                                        asn1EncodableVector9.add(new DERSet(pkcs12BagAttributeCarrier2.getBagAttribute(asn1ObjectIdentifier2)));
                                        asn1EncodableVector8.add(new DERSequence(asn1EncodableVector9));
                                        n = 1;
                                    }
                                    break Label_2046;
                                }
                                break Label_2049;
                                // iftrue(Label_1232:, n != 0)
                                while (true) {
                                    Block_23: {
                                        break Block_23;
                                        asn1EncodableVector7.add(new SafeBag(PKCS12KeyStoreSpi.certBag, certBag.toASN1Primitive(), new DERSet(asn1EncodableVector8)));
                                        hashtable.put(engineGetCertificate2, engineGetCertificate2);
                                        break;
                                    }
                                    final ASN1EncodableVector asn1EncodableVector10 = new ASN1EncodableVector();
                                    asn1EncodableVector10.add(PKCS12KeyStoreSpi.pkcs_9_at_localKeyId);
                                    asn1EncodableVector10.add(new DERSet(this.createSubjectKeyId(engineGetCertificate2.getPublicKey())));
                                    asn1EncodableVector8.add(new DERSequence(asn1EncodableVector10));
                                    final ASN1EncodableVector asn1EncodableVector11 = new ASN1EncodableVector();
                                    asn1EncodableVector11.add(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName);
                                    asn1EncodableVector11.add(new DERSet(new DERBMPString(anObject2)));
                                    asn1EncodableVector8.add(new DERSequence(asn1EncodableVector11));
                                    continue;
                                }
                            }
                            catch (CertificateEncodingException ex2) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("Error encoding certificate: ");
                                sb2.append(ex2.toString());
                                throw new IOException(sb2.toString());
                            }
                            break Label_1312;
                        }
                        continue;
                    }
                    int n = 0;
                    continue;
                }
            }
        }
        final Enumeration keys4 = this.certs.keys();
        while (keys4.hasMoreElements()) {
            try {
                final String s4 = keys4.nextElement();
                final Certificate certificate = (Certificate)this.certs.get(s4);
                if (this.keys.get(s4) != null) {
                    continue;
                }
                asn1EncodableVector7.add(this.createSafeBag(s4, certificate));
                hashtable.put(certificate, certificate);
                continue;
            }
            catch (CertificateEncodingException ex3) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Error encoding certificate: ");
                sb3.append(ex3.toString());
                throw new IOException(sb3.toString());
            }
            break;
        }
        final Set usedCertificateSet = this.getUsedCertificateSet();
        final Enumeration<CertId> keys5 = (Enumeration<CertId>)this.chainCerts.keys();
        Hashtable<Certificate, Certificate> hashtable2 = hashtable;
        while (keys5.hasMoreElements()) {
            try {
                final Certificate key = this.chainCerts.get(keys5.nextElement());
                if (!usedCertificateSet.contains(key)) {
                    continue;
                }
                if (hashtable2.get(key) != null) {
                    continue;
                }
                final CertBag certBag2 = new CertBag(PKCS12KeyStoreSpi.x509Certificate, new DEROctetString(key.getEncoded()));
                final ASN1EncodableVector asn1EncodableVector12 = new ASN1EncodableVector();
                Hashtable<Certificate, Certificate> hashtable3 = hashtable2;
                if (key instanceof PKCS12BagAttributeCarrier) {
                    final PKCS12BagAttributeCarrier pkcs12BagAttributeCarrier3 = (PKCS12BagAttributeCarrier)key;
                    final Enumeration bagAttributeKeys3 = pkcs12BagAttributeCarrier3.getBagAttributeKeys();
                    while (true) {
                        hashtable3 = hashtable2;
                        if (!bagAttributeKeys3.hasMoreElements()) {
                            break;
                        }
                        final ASN1ObjectIdentifier asn1ObjectIdentifier3 = bagAttributeKeys3.nextElement();
                        if (asn1ObjectIdentifier3.equals(PKCSObjectIdentifiers.pkcs_9_at_localKeyId)) {
                            continue;
                        }
                        final ASN1EncodableVector asn1EncodableVector13 = new ASN1EncodableVector();
                        asn1EncodableVector13.add(asn1ObjectIdentifier3);
                        asn1EncodableVector13.add(new DERSet(pkcs12BagAttributeCarrier3.getBagAttribute(asn1ObjectIdentifier3)));
                        asn1EncodableVector12.add(new DERSequence(asn1EncodableVector13));
                    }
                }
                asn1EncodableVector7.add(new SafeBag(PKCS12KeyStoreSpi.certBag, certBag2.toASN1Primitive(), new DERSet(asn1EncodableVector12)));
                hashtable2 = hashtable3;
                continue;
            }
            catch (CertificateEncodingException ex4) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("Error encoding certificate: ");
                sb4.append(ex4.toString());
                throw new IOException(sb4.toString());
            }
            break;
        }
        final AuthenticatedSafe authenticatedSafe = new AuthenticatedSafe(new ContentInfo[] { new ContentInfo(PKCS12KeyStoreSpi.data, berOctetString), new ContentInfo(PKCS12KeyStoreSpi.encryptedData, new EncryptedData(PKCS12KeyStoreSpi.data, algorithmIdentifier, new BEROctetString(this.cryptData(true, algorithmIdentifier, array, false, new DERSequence(asn1EncodableVector7).getEncoded("DER")))).toASN1Primitive()) });
        String s5;
        if (b) {
            s5 = "DER";
        }
        else {
            s5 = s;
        }
        final ContentInfo contentInfo = new ContentInfo(PKCS12KeyStoreSpi.data, new BEROctetString(authenticatedSafe.getEncoded(s5)));
        final byte[] bytes3 = new byte[this.saltLength];
        this.random.nextBytes(bytes3);
        final byte[] octets = ((ASN1OctetString)contentInfo.getContent()).getOctets();
        try {
            final Pfx pfx = new Pfx(contentInfo, new MacData(new DigestInfo(this.macAlgorithm, this.calculatePbeMac(this.macAlgorithm.getAlgorithm(), bytes3, this.itCount, array, false, octets)), bytes3, this.itCount));
            if (b) {
                s = s2;
            }
            pfx.encodeTo(outputStream, s);
            return;
        }
        catch (Exception ex5) {
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("error constructing MAC: ");
            sb5.append(ex5.toString());
            throw new IOException(sb5.toString());
        }
        throw new NullPointerException("no password supplied for PKCS#12 KeyStore");
    }
    
    private static byte[] getDigest(final SubjectPublicKeyInfo subjectPublicKeyInfo) {
        final Digest sha1 = DigestFactory.createSHA1();
        final byte[] array = new byte[sha1.getDigestSize()];
        final byte[] bytes = subjectPublicKeyInfo.getPublicKeyData().getBytes();
        sha1.update(bytes, 0, bytes.length);
        sha1.doFinal(array, 0);
        return array;
    }
    
    private Set getUsedCertificateSet() {
        final HashSet<Certificate> set = new HashSet<Certificate>();
        final Enumeration keys = this.keys.keys();
        while (keys.hasMoreElements()) {
            final Certificate[] engineGetCertificateChain = this.engineGetCertificateChain(keys.nextElement());
            for (int i = 0; i != engineGetCertificateChain.length; ++i) {
                set.add(engineGetCertificateChain[i]);
            }
        }
        final Enumeration keys2 = this.certs.keys();
        while (keys2.hasMoreElements()) {
            set.add(this.engineGetCertificate(keys2.nextElement()));
        }
        return set;
    }
    
    private int validateIterationCount(BigInteger bigInteger) {
        final int intValue = bigInteger.intValue();
        if (intValue < 0) {
            throw new IllegalStateException("negative iteration count found");
        }
        bigInteger = Properties.asBigInteger("org.bouncycastle.pkcs12.max_it_count");
        if (bigInteger == null) {
            return intValue;
        }
        if (bigInteger.intValue() >= intValue) {
            return intValue;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("iteration count ");
        sb.append(intValue);
        sb.append(" greater than ");
        sb.append(bigInteger.intValue());
        throw new IllegalStateException(sb.toString());
    }
    
    protected byte[] cryptData(final boolean b, final AlgorithmIdentifier algorithmIdentifier, final char[] array, final boolean b2, final byte[] array2) throws IOException {
        final ASN1ObjectIdentifier algorithm = algorithmIdentifier.getAlgorithm();
        int opmode;
        if (b) {
            opmode = 1;
        }
        else {
            opmode = 2;
        }
        if (algorithm.on(PKCSObjectIdentifiers.pkcs_12PbeIds)) {
            final PKCS12PBEParams instance = PKCS12PBEParams.getInstance(algorithmIdentifier.getParameters());
            try {
                final PBEParameterSpec params = new PBEParameterSpec(instance.getIV(), instance.getIterations().intValue());
                final PKCS12Key key = new PKCS12Key(array, b2);
                final Cipher cipher = this.helper.createCipher(algorithm.getId());
                cipher.init(opmode, key, params);
                return cipher.doFinal(array2);
            }
            catch (Exception ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("exception decrypting data - ");
                sb.append(ex.toString());
                throw new IOException(sb.toString());
            }
        }
        if (algorithm.equals(PKCSObjectIdentifiers.id_PBES2)) {
            try {
                return this.createCipher(opmode, array, algorithmIdentifier).doFinal(array2);
            }
            catch (Exception ex2) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("exception decrypting data - ");
                sb2.append(ex2.toString());
                throw new IOException(sb2.toString());
            }
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("unknown PBE algorithm: ");
        sb3.append(algorithm);
        throw new IOException(sb3.toString());
    }
    
    @Override
    public Enumeration engineAliases() {
        final Hashtable<String, String> hashtable = new Hashtable<String, String>();
        final Enumeration keys = this.certs.keys();
        while (keys.hasMoreElements()) {
            hashtable.put(keys.nextElement(), "cert");
        }
        final Enumeration keys2 = this.keys.keys();
        while (keys2.hasMoreElements()) {
            final String s = keys2.nextElement();
            if (hashtable.get(s) == null) {
                hashtable.put(s, "key");
            }
        }
        return hashtable.keys();
    }
    
    @Override
    public boolean engineContainsAlias(final String s) {
        return this.certs.get(s) != null || this.keys.get(s) != null;
    }
    
    @Override
    public void engineDeleteEntry(final String key) throws KeyStoreException {
        final Key key2 = (Key)this.keys.remove(key);
        final Certificate certificate = (Certificate)this.certs.remove(key);
        if (certificate != null) {
            this.chainCerts.remove(new CertId(certificate.getPublicKey()));
        }
        if (key2 != null) {
            final String key3 = this.localIds.remove(key);
            Certificate certificate2 = certificate;
            if (key3 != null) {
                certificate2 = this.keyCerts.remove(key3);
            }
            if (certificate2 != null) {
                this.chainCerts.remove(new CertId(certificate2.getPublicKey()));
            }
        }
    }
    
    @Override
    public Certificate engineGetCertificate(final String s) {
        if (s != null) {
            Certificate certificate;
            if ((certificate = (Certificate)this.certs.get(s)) == null) {
                final String key = this.localIds.get(s);
                Object o;
                if (key != null) {
                    o = this.keyCerts.get(key);
                }
                else {
                    o = this.keyCerts.get(s);
                }
                certificate = (Certificate)o;
            }
            return certificate;
        }
        throw new IllegalArgumentException("null alias passed to getCertificate.");
    }
    
    @Override
    public String engineGetCertificateAlias(final Certificate certificate) {
        final Enumeration elements = this.certs.elements();
        final Enumeration keys = this.certs.keys();
        while (elements.hasMoreElements()) {
            final Certificate certificate2 = elements.nextElement();
            final String s = keys.nextElement();
            if (certificate2.equals(certificate)) {
                return s;
            }
        }
        final Enumeration<Certificate> elements2 = (Enumeration<Certificate>)this.keyCerts.elements();
        final Enumeration<String> keys2 = this.keyCerts.keys();
        while (elements2.hasMoreElements()) {
            final Certificate certificate3 = elements2.nextElement();
            final String s2 = keys2.nextElement();
            if (certificate3.equals(certificate)) {
                return s2;
            }
        }
        return null;
    }
    
    @Override
    public Certificate[] engineGetCertificateChain(String engineGetCertificate) {
        Label_0308: {
            if (engineGetCertificate == null) {
                break Label_0308;
            }
            if (!this.engineIsKeyEntry(engineGetCertificate)) {
                return null;
            }
            engineGetCertificate = (String)this.engineGetCertificate(engineGetCertificate);
        Label_0179_Outer:
            while (true) {
                if (engineGetCertificate != null) {
                    final Vector<String> vector = new Vector<String>();
                    break Label_0033;
                }
                Label_0306: {
                    break Label_0306;
                    while (true) {
                        Label_0236: {
                            Certificate certificate;
                            final Principal issuerDN;
                            do {
                                final Certificate certificate2;
                                certificate = certificate2;
                                final Enumeration<Object> keys;
                                if (!keys.hasMoreElements()) {
                                    break Label_0236;
                                }
                                certificate = (X509Certificate)this.chainCerts.get(keys.nextElement());
                            } while (!((X509Certificate)certificate).getSubjectDN().equals(issuerDN));
                            Label_0268: {
                                final X509Certificate x509Certificate;
                                Block_9: {
                                    break Block_9;
                                    if (engineGetCertificate == null) {
                                        break Label_0268;
                                    }
                                    x509Certificate = (X509Certificate)engineGetCertificate;
                                    final byte[] extensionValue = x509Certificate.getExtensionValue(Extension.authorityKeyIdentifier.getId());
                                    Certificate certificate2 = null;
                                    Label_0138: {
                                        if (extensionValue != null) {
                                            try {
                                                final AuthorityKeyIdentifier instance = AuthorityKeyIdentifier.getInstance(new ASN1InputStream(ASN1OctetString.getInstance(new ASN1InputStream(extensionValue).readObject()).getOctets()).readObject());
                                                if (instance.getKeyIdentifier() != null) {
                                                    certificate2 = (Certificate)this.chainCerts.get(new CertId(instance.getKeyIdentifier()));
                                                    break Label_0138;
                                                }
                                            }
                                            catch (IOException ex) {
                                                throw new RuntimeException(ex.toString());
                                            }
                                        }
                                        certificate2 = null;
                                    }
                                    certificate = certificate2;
                                    if (certificate2 != null) {
                                        break Label_0236;
                                    }
                                    issuerDN = x509Certificate.getIssuerDN();
                                    certificate = certificate2;
                                    if (!issuerDN.equals(x509Certificate.getSubjectDN())) {
                                        final Enumeration<Object> keys = this.chainCerts.keys();
                                        continue;
                                    }
                                    break Label_0236;
                                }
                                try {
                                    x509Certificate.verify(certificate.getPublicKey());
                                    final Vector<String> vector;
                                    if (!vector.contains(engineGetCertificate)) {
                                        vector.addElement(engineGetCertificate);
                                        if (certificate != engineGetCertificate) {
                                            engineGetCertificate = (String)certificate;
                                            continue Label_0179_Outer;
                                        }
                                    }
                                    engineGetCertificate = null;
                                    continue Label_0179_Outer;
                                    engineGetCertificate = (String)(Object)new Certificate[vector.size()];
                                    int index = 0;
                                    // iftrue(Label_0304:, index == engineGetCertificate.length)
                                    while (true) {
                                        break Label_0279;
                                        throw new IllegalArgumentException("null alias passed to getCertificateChain.");
                                        Label_0304: {
                                            return (Certificate[])(Object)engineGetCertificate;
                                        }
                                        engineGetCertificate[index] = (Certificate)vector.elementAt(index);
                                        ++index;
                                        continue;
                                    }
                                    return null;
                                }
                                catch (Exception ex2) {
                                    continue;
                                }
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
    }
    
    @Override
    public Date engineGetCreationDate(final String s) {
        if (s == null) {
            throw new NullPointerException("alias == null");
        }
        if (this.keys.get(s) == null && this.certs.get(s) == null) {
            return null;
        }
        return new Date();
    }
    
    @Override
    public Key engineGetKey(final String s, final char[] array) throws NoSuchAlgorithmException, UnrecoverableKeyException {
        if (s != null) {
            return (Key)this.keys.get(s);
        }
        throw new IllegalArgumentException("null alias passed to getKey.");
    }
    
    @Override
    public boolean engineIsCertificateEntry(final String s) {
        return this.certs.get(s) != null && this.keys.get(s) == null;
    }
    
    @Override
    public boolean engineIsKeyEntry(final String s) {
        return this.keys.get(s) != null;
    }
    
    @Override
    public void engineLoad(final InputStream in, final char[] array) throws IOException {
        if (in == null) {
            return;
        }
        final BufferedInputStream bufferedInputStream = new BufferedInputStream(in);
        bufferedInputStream.mark(10);
        if (bufferedInputStream.read() == 48) {
            bufferedInputStream.reset();
            final ASN1InputStream asn1InputStream = new ASN1InputStream(bufferedInputStream);
            try {
                final Pfx instance = Pfx.getInstance(asn1InputStream.readObject());
                final ContentInfo authSafe = instance.getAuthSafe();
                final Vector<SafeBag> vector = new Vector<SafeBag>();
                boolean b = false;
                Label_0326: {
                    Label_0323: {
                        if (instance.getMacData() != null) {
                            if (array != null) {
                                final MacData macData = instance.getMacData();
                                final DigestInfo mac = macData.getMac();
                                this.macAlgorithm = mac.getAlgorithmId();
                                final byte[] salt = macData.getSalt();
                                this.itCount = this.validateIterationCount(macData.getIterationCount());
                                this.saltLength = salt.length;
                                final byte[] octets = ((ASN1OctetString)authSafe.getContent()).getOctets();
                                try {
                                    final byte[] calculatePbeMac = this.calculatePbeMac(this.macAlgorithm.getAlgorithm(), salt, this.itCount, array, false, octets);
                                    final byte[] digest = mac.getDigest();
                                    if (Arrays.constantTimeAreEqual(calculatePbeMac, digest)) {
                                        break Label_0323;
                                    }
                                    if (array.length > 0) {
                                        throw new IOException("PKCS12 key store mac invalid - wrong password or corrupted file.");
                                    }
                                    if (Arrays.constantTimeAreEqual(this.calculatePbeMac(this.macAlgorithm.getAlgorithm(), salt, this.itCount, array, true, octets), digest)) {
                                        b = true;
                                        break Label_0326;
                                    }
                                    throw new IOException("PKCS12 key store mac invalid - wrong password or corrupted file.");
                                }
                                catch (Exception ex) {
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append("error constructing MAC: ");
                                    sb.append(ex.toString());
                                    throw new IOException(sb.toString());
                                }
                                catch (IOException ex2) {
                                    throw ex2;
                                }
                            }
                            throw new NullPointerException("no password supplied when one expected");
                        }
                        if (!Properties.isOverrideSet("org.bouncycastle.pkcs12.ignore_useless_passwd")) {
                            if (array != null) {
                                throw new IOException("password supplied for keystore that does not require one");
                            }
                        }
                    }
                    b = false;
                }
                this.keys = new IgnoresCaseHashtable();
                this.localIds = new Hashtable();
                int n3;
                if (authSafe.getContentType().equals(PKCS12KeyStoreSpi.data)) {
                    final ContentInfo[] contentInfo = AuthenticatedSafe.getInstance(new ASN1InputStream(((ASN1OctetString)authSafe.getContent()).getOctets()).readObject()).getContentInfo();
                    int n2;
                    int n = n2 = 0;
                    while (true) {
                        n3 = n2;
                        if (n == contentInfo.length) {
                            break;
                        }
                        if (contentInfo[n].getContentType().equals(PKCS12KeyStoreSpi.data)) {
                            final ASN1Sequence instance2 = ASN1Sequence.getInstance(new ASN1InputStream(ASN1OctetString.getInstance(contentInfo[n].getContent()).getOctets()).readObject());
                            for (int i = 0; i != instance2.size(); ++i) {
                                final SafeBag instance3 = SafeBag.getInstance(instance2.getObjectAt(i));
                                if (instance3.getBagId().equals(PKCS12KeyStoreSpi.pkcs8ShroudedKeyBag)) {
                                    final EncryptedPrivateKeyInfo instance4 = EncryptedPrivateKeyInfo.getInstance(instance3.getBagValue());
                                    final PrivateKey unwrapKey = this.unwrapKey(instance4.getEncryptionAlgorithm(), instance4.getEncryptedData(), array, b);
                                    Object key;
                                    ASN1OctetString asn1OctetString2;
                                    if (instance3.getBagAttributes() != null) {
                                        final Enumeration objects = instance3.getBagAttributes().getObjects();
                                        ASN1OctetString asn1OctetString;
                                        Object o = asn1OctetString = null;
                                        while (true) {
                                            key = o;
                                            asn1OctetString2 = asn1OctetString;
                                            if (!objects.hasMoreElements()) {
                                                break;
                                            }
                                            final ASN1Sequence asn1Sequence = objects.nextElement();
                                            final ASN1ObjectIdentifier asn1ObjectIdentifier = (ASN1ObjectIdentifier)asn1Sequence.getObjectAt(0);
                                            final ASN1Set set = (ASN1Set)asn1Sequence.getObjectAt(1);
                                            ASN1Primitive asn1Primitive2;
                                            if (set.size() > 0) {
                                                final ASN1Primitive asn1Primitive = asn1Primitive2 = (ASN1Primitive)set.getObjectAt(0);
                                                if (unwrapKey instanceof PKCS12BagAttributeCarrier) {
                                                    final PKCS12BagAttributeCarrier pkcs12BagAttributeCarrier = (PKCS12BagAttributeCarrier)unwrapKey;
                                                    final ASN1Encodable bagAttribute = pkcs12BagAttributeCarrier.getBagAttribute(asn1ObjectIdentifier);
                                                    if (bagAttribute != null) {
                                                        if (!bagAttribute.toASN1Primitive().equals(asn1Primitive)) {
                                                            throw new IOException("attempt to add existing attribute with different value");
                                                        }
                                                        asn1Primitive2 = asn1Primitive;
                                                    }
                                                    else {
                                                        pkcs12BagAttributeCarrier.setBagAttribute(asn1ObjectIdentifier, asn1Primitive);
                                                        asn1Primitive2 = asn1Primitive;
                                                    }
                                                }
                                            }
                                            else {
                                                asn1Primitive2 = null;
                                            }
                                            Object string;
                                            if (asn1ObjectIdentifier.equals(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName)) {
                                                string = ((DERBMPString)asn1Primitive2).getString();
                                                this.keys.put((String)string, unwrapKey);
                                            }
                                            else {
                                                string = o;
                                                if (asn1ObjectIdentifier.equals(PKCS12KeyStoreSpi.pkcs_9_at_localKeyId)) {
                                                    asn1OctetString = (ASN1OctetString)asn1Primitive2;
                                                    string = o;
                                                }
                                            }
                                            o = string;
                                        }
                                    }
                                    else {
                                        key = null;
                                        asn1OctetString2 = null;
                                    }
                                    if (asn1OctetString2 != null) {
                                        final String value = new String(Hex.encode(asn1OctetString2.getOctets()));
                                        if (key == null) {
                                            this.keys.put(value, unwrapKey);
                                        }
                                        else {
                                            this.localIds.put(key, value);
                                        }
                                    }
                                    else {
                                        this.keys.put("unmarked", unwrapKey);
                                        n2 = 1;
                                    }
                                }
                                else if (instance3.getBagId().equals(PKCS12KeyStoreSpi.certBag)) {
                                    vector.addElement(instance3);
                                }
                                else {
                                    final PrintStream out = System.out;
                                    final StringBuilder sb2 = new StringBuilder();
                                    sb2.append("extra in data ");
                                    sb2.append(instance3.getBagId());
                                    out.println(sb2.toString());
                                    System.out.println(ASN1Dump.dumpAsString(instance3));
                                }
                            }
                        }
                        else if (contentInfo[n].getContentType().equals(PKCS12KeyStoreSpi.encryptedData)) {
                            final EncryptedData instance5 = EncryptedData.getInstance(contentInfo[n].getContent());
                            final ASN1Sequence asn1Sequence2 = (ASN1Sequence)ASN1Primitive.fromByteArray(this.cryptData(false, instance5.getEncryptionAlgorithm(), array, b, instance5.getContent().getOctets()));
                            for (int j = 0; j != asn1Sequence2.size(); ++j) {
                                final SafeBag instance6 = SafeBag.getInstance(asn1Sequence2.getObjectAt(j));
                                if (instance6.getBagId().equals(PKCS12KeyStoreSpi.certBag)) {
                                    vector.addElement(instance6);
                                }
                                else if (instance6.getBagId().equals(PKCS12KeyStoreSpi.pkcs8ShroudedKeyBag)) {
                                    final EncryptedPrivateKeyInfo instance7 = EncryptedPrivateKeyInfo.getInstance(instance6.getBagValue());
                                    final PrivateKey unwrapKey2 = this.unwrapKey(instance7.getEncryptionAlgorithm(), instance7.getEncryptedData(), array, b);
                                    final PKCS12BagAttributeCarrier pkcs12BagAttributeCarrier2 = (PKCS12BagAttributeCarrier)unwrapKey2;
                                    final Enumeration objects2 = instance6.getBagAttributes().getObjects();
                                    ASN1OctetString asn1OctetString3 = null;
                                    String key2 = null;
                                    while (objects2.hasMoreElements()) {
                                        final ASN1Sequence asn1Sequence3 = objects2.nextElement();
                                        final ASN1ObjectIdentifier asn1ObjectIdentifier2 = (ASN1ObjectIdentifier)asn1Sequence3.getObjectAt(0);
                                        final ASN1Set set2 = (ASN1Set)asn1Sequence3.getObjectAt(1);
                                        ASN1Primitive asn1Primitive3;
                                        if (set2.size() > 0) {
                                            asn1Primitive3 = (ASN1Primitive)set2.getObjectAt(0);
                                            final ASN1Encodable bagAttribute2 = pkcs12BagAttributeCarrier2.getBagAttribute(asn1ObjectIdentifier2);
                                            if (bagAttribute2 != null) {
                                                if (!bagAttribute2.toASN1Primitive().equals(asn1Primitive3)) {
                                                    throw new IOException("attempt to add existing attribute with different value");
                                                }
                                            }
                                            else {
                                                pkcs12BagAttributeCarrier2.setBagAttribute(asn1ObjectIdentifier2, asn1Primitive3);
                                            }
                                        }
                                        else {
                                            asn1Primitive3 = null;
                                        }
                                        String string2;
                                        if (asn1ObjectIdentifier2.equals(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName)) {
                                            string2 = ((DERBMPString)asn1Primitive3).getString();
                                            this.keys.put(string2, unwrapKey2);
                                        }
                                        else {
                                            string2 = key2;
                                            if (asn1ObjectIdentifier2.equals(PKCS12KeyStoreSpi.pkcs_9_at_localKeyId)) {
                                                asn1OctetString3 = (ASN1OctetString)asn1Primitive3;
                                                string2 = key2;
                                            }
                                        }
                                        key2 = string2;
                                    }
                                    final String value2 = new String(Hex.encode(asn1OctetString3.getOctets()));
                                    if (key2 == null) {
                                        this.keys.put(value2, unwrapKey2);
                                    }
                                    else {
                                        this.localIds.put(key2, value2);
                                    }
                                }
                                else if (instance6.getBagId().equals(PKCS12KeyStoreSpi.keyBag)) {
                                    final PrivateKey privateKey = BouncyCastleProvider.getPrivateKey(PrivateKeyInfo.getInstance(instance6.getBagValue()));
                                    final PKCS12BagAttributeCarrier pkcs12BagAttributeCarrier3 = (PKCS12BagAttributeCarrier)privateKey;
                                    final Enumeration objects3 = instance6.getBagAttributes().getObjects();
                                    ASN1OctetString asn1OctetString4 = null;
                                    String string3 = null;
                                    while (objects3.hasMoreElements()) {
                                        final ASN1Sequence instance8 = ASN1Sequence.getInstance(objects3.nextElement());
                                        final ASN1ObjectIdentifier instance9 = ASN1ObjectIdentifier.getInstance(instance8.getObjectAt(0));
                                        final ASN1Set instance10 = ASN1Set.getInstance(instance8.getObjectAt(1));
                                        if (instance10.size() > 0) {
                                            final ASN1Primitive asn1Primitive4 = (ASN1Primitive)instance10.getObjectAt(0);
                                            final ASN1Encodable bagAttribute3 = pkcs12BagAttributeCarrier3.getBagAttribute(instance9);
                                            if (bagAttribute3 != null) {
                                                if (!bagAttribute3.toASN1Primitive().equals(asn1Primitive4)) {
                                                    throw new IOException("attempt to add existing attribute with different value");
                                                }
                                            }
                                            else {
                                                pkcs12BagAttributeCarrier3.setBagAttribute(instance9, asn1Primitive4);
                                            }
                                            if (instance9.equals(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName)) {
                                                string3 = ((DERBMPString)asn1Primitive4).getString();
                                                this.keys.put(string3, privateKey);
                                            }
                                            else {
                                                if (!instance9.equals(PKCS12KeyStoreSpi.pkcs_9_at_localKeyId)) {
                                                    continue;
                                                }
                                                asn1OctetString4 = (ASN1OctetString)asn1Primitive4;
                                            }
                                        }
                                    }
                                    final String value3 = new String(Hex.encode(asn1OctetString4.getOctets()));
                                    if (string3 == null) {
                                        this.keys.put(value3, privateKey);
                                    }
                                    else {
                                        this.localIds.put(string3, value3);
                                    }
                                }
                                else {
                                    final PrintStream out2 = System.out;
                                    final StringBuilder sb3 = new StringBuilder();
                                    sb3.append("extra in encryptedData ");
                                    sb3.append(instance6.getBagId());
                                    out2.println(sb3.toString());
                                    System.out.println(ASN1Dump.dumpAsString(instance6));
                                }
                            }
                        }
                        else {
                            final int n4 = n;
                            final PrintStream out3 = System.out;
                            final StringBuilder sb4 = new StringBuilder();
                            sb4.append("extra ");
                            sb4.append(contentInfo[n4].getContentType().getId());
                            out3.println(sb4.toString());
                            final PrintStream out4 = System.out;
                            final StringBuilder sb5 = new StringBuilder();
                            sb5.append("extra ");
                            sb5.append(ASN1Dump.dumpAsString(contentInfo[n4].getContent()));
                            out4.println(sb5.toString());
                        }
                        ++n;
                    }
                }
                else {
                    n3 = 0;
                }
                this.certs = new IgnoresCaseHashtable();
                this.chainCerts = new Hashtable();
                this.keyCerts = new Hashtable();
                int k = 0;
                while (k != vector.size()) {
                    final SafeBag safeBag = vector.elementAt(k);
                    final CertBag instance11 = CertBag.getInstance(safeBag.getBagValue());
                    if (instance11.getCertId().equals(PKCS12KeyStoreSpi.x509Certificate)) {
                        try {
                            final Certificate generateCertificate = this.certFact.generateCertificate(new ByteArrayInputStream(((ASN1OctetString)instance11.getCertValue()).getOctets()));
                            String string4;
                            Object o2;
                            if (safeBag.getBagAttributes() != null) {
                                final Enumeration objects4 = safeBag.getBagAttributes().getObjects();
                                o2 = (string4 = null);
                                while (objects4.hasMoreElements()) {
                                    final ASN1Sequence instance12 = ASN1Sequence.getInstance(objects4.nextElement());
                                    final ASN1ObjectIdentifier instance13 = ASN1ObjectIdentifier.getInstance(instance12.getObjectAt(0));
                                    final ASN1Set instance14 = ASN1Set.getInstance(instance12.getObjectAt(1));
                                    if (instance14.size() > 0) {
                                        final ASN1Primitive asn1Primitive5 = (ASN1Primitive)instance14.getObjectAt(0);
                                        if (generateCertificate instanceof PKCS12BagAttributeCarrier) {
                                            final PKCS12BagAttributeCarrier pkcs12BagAttributeCarrier4 = (PKCS12BagAttributeCarrier)generateCertificate;
                                            final ASN1Encodable bagAttribute4 = pkcs12BagAttributeCarrier4.getBagAttribute(instance13);
                                            if (bagAttribute4 != null) {
                                                if (!bagAttribute4.toASN1Primitive().equals(asn1Primitive5)) {
                                                    throw new IOException("attempt to add existing attribute with different value");
                                                }
                                            }
                                            else {
                                                pkcs12BagAttributeCarrier4.setBagAttribute(instance13, asn1Primitive5);
                                            }
                                        }
                                        if (instance13.equals(PKCS12KeyStoreSpi.pkcs_9_at_friendlyName)) {
                                            string4 = ((DERBMPString)asn1Primitive5).getString();
                                        }
                                        else {
                                            if (!instance13.equals(PKCS12KeyStoreSpi.pkcs_9_at_localKeyId)) {
                                                continue;
                                            }
                                            o2 = asn1Primitive5;
                                        }
                                    }
                                }
                            }
                            else {
                                o2 = (string4 = null);
                            }
                            this.chainCerts.put(new CertId(generateCertificate.getPublicKey()), generateCertificate);
                            if (n3 != 0) {
                                if (this.keyCerts.isEmpty()) {
                                    final String key3 = new String(Hex.encode(this.createSubjectKeyId(generateCertificate.getPublicKey()).getKeyIdentifier()));
                                    this.keyCerts.put(key3, generateCertificate);
                                    final IgnoresCaseHashtable keys = this.keys;
                                    keys.put(key3, keys.remove("unmarked"));
                                }
                            }
                            else {
                                if (o2 != null) {
                                    this.keyCerts.put(new String(Hex.encode(((ASN1OctetString)o2).getOctets())), generateCertificate);
                                }
                                if (string4 != null) {
                                    this.certs.put(string4, generateCertificate);
                                }
                            }
                            ++k;
                            continue;
                        }
                        catch (Exception ex3) {
                            throw new RuntimeException(ex3.toString());
                        }
                    }
                    final StringBuilder sb6 = new StringBuilder();
                    sb6.append("Unsupported certificate type: ");
                    sb6.append(instance11.getCertId());
                    throw new RuntimeException(sb6.toString());
                }
                return;
            }
            catch (Exception ex4) {
                throw new IOException(ex4.getMessage());
            }
        }
        throw new IOException("stream does not represent a PKCS12 key store");
    }
    
    @Override
    public void engineSetCertificateEntry(final String str, final Certificate value) throws KeyStoreException {
        if (this.keys.get(str) == null) {
            this.certs.put(str, value);
            this.chainCerts.put(new CertId(value.getPublicKey()), value);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("There is a key entry with the name ");
        sb.append(str);
        sb.append(".");
        throw new KeyStoreException(sb.toString());
    }
    
    @Override
    public void engineSetKeyEntry(final String s, final Key key, final char[] array, final Certificate[] array2) throws KeyStoreException {
        final boolean b = key instanceof PrivateKey;
        if (!b) {
            throw new KeyStoreException("PKCS12 does not support non-PrivateKeys");
        }
        if (b && array2 == null) {
            throw new KeyStoreException("no certificate chain for private key");
        }
        if (this.keys.get(s) != null) {
            this.engineDeleteEntry(s);
        }
        this.keys.put(s, key);
        if (array2 != null) {
            final IgnoresCaseHashtable certs = this.certs;
            int i = 0;
            certs.put(s, array2[0]);
            while (i != array2.length) {
                this.chainCerts.put(new CertId(array2[i].getPublicKey()), array2[i]);
                ++i;
            }
        }
    }
    
    @Override
    public void engineSetKeyEntry(final String s, final byte[] array, final Certificate[] array2) throws KeyStoreException {
        throw new RuntimeException("operation not supported");
    }
    
    @Override
    public int engineSize() {
        final Hashtable<String, String> hashtable = new Hashtable<String, String>();
        final Enumeration keys = this.certs.keys();
        while (keys.hasMoreElements()) {
            hashtable.put(keys.nextElement(), "cert");
        }
        final Enumeration keys2 = this.keys.keys();
        while (keys2.hasMoreElements()) {
            final String s = keys2.nextElement();
            if (hashtable.get(s) == null) {
                hashtable.put(s, "key");
            }
        }
        return hashtable.size();
    }
    
    @Override
    public void engineStore(final OutputStream outputStream, final char[] array) throws IOException {
        this.doStore(outputStream, array, false);
    }
    
    @Override
    public void engineStore(final KeyStore.LoadStoreParameter loadStoreParameter) throws IOException, NoSuchAlgorithmException, CertificateException {
        if (loadStoreParameter == null) {
            throw new IllegalArgumentException("'param' arg cannot be null");
        }
        final boolean b = loadStoreParameter instanceof PKCS12StoreParameter;
        if (!b && !(loadStoreParameter instanceof JDKPKCS12StoreParameter)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("No support for 'param' of type ");
            sb.append(loadStoreParameter.getClass().getName());
            throw new IllegalArgumentException(sb.toString());
        }
        PKCS12StoreParameter pkcs12StoreParameter;
        if (b) {
            pkcs12StoreParameter = (PKCS12StoreParameter)loadStoreParameter;
        }
        else {
            final JDKPKCS12StoreParameter jdkpkcs12StoreParameter = (JDKPKCS12StoreParameter)loadStoreParameter;
            pkcs12StoreParameter = new PKCS12StoreParameter(jdkpkcs12StoreParameter.getOutputStream(), loadStoreParameter.getProtectionParameter(), jdkpkcs12StoreParameter.isUseDEREncoding());
        }
        final KeyStore.ProtectionParameter protectionParameter = loadStoreParameter.getProtectionParameter();
        char[] password;
        if (protectionParameter == null) {
            password = null;
        }
        else {
            if (!(protectionParameter instanceof KeyStore.PasswordProtection)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("No support for protection parameter of type ");
                sb2.append(((KeyStore.PasswordProtection)protectionParameter).getClass().getName());
                throw new IllegalArgumentException(sb2.toString());
            }
            password = ((KeyStore.PasswordProtection)protectionParameter).getPassword();
        }
        this.doStore(pkcs12StoreParameter.getOutputStream(), password, pkcs12StoreParameter.isForDEREncoding());
    }
    
    @Override
    public void setRandom(final SecureRandom random) {
        this.random = random;
    }
    
    protected PrivateKey unwrapKey(final AlgorithmIdentifier algorithmIdentifier, final byte[] array, final char[] array2, final boolean b) throws IOException {
        final ASN1ObjectIdentifier algorithm = algorithmIdentifier.getAlgorithm();
        try {
            if (algorithm.on(PKCSObjectIdentifiers.pkcs_12PbeIds)) {
                final PKCS12PBEParams instance = PKCS12PBEParams.getInstance(algorithmIdentifier.getParameters());
                final PBEParameterSpec params = new PBEParameterSpec(instance.getIV(), this.validateIterationCount(instance.getIterations()));
                final Cipher cipher = this.helper.createCipher(algorithm.getId());
                cipher.init(4, new PKCS12Key(array2, b), params);
                return (PrivateKey)cipher.unwrap(array, "", 2);
            }
            if (algorithm.equals(PKCSObjectIdentifiers.id_PBES2)) {
                return (PrivateKey)this.createCipher(4, array2, algorithmIdentifier).unwrap(array, "", 2);
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("exception unwrapping private key - cannot recognise: ");
            sb.append(algorithm);
            throw new IOException(sb.toString());
        }
        catch (Exception ex) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("exception unwrapping private key - ");
            sb2.append(ex.toString());
            throw new IOException(sb2.toString());
        }
    }
    
    protected byte[] wrapKey(final String s, final Key key, final PKCS12PBEParams pkcs12PBEParams, final char[] password) throws IOException {
        final PBEKeySpec keySpec = new PBEKeySpec(password);
        try {
            final SecretKeyFactory secretKeyFactory = this.helper.createSecretKeyFactory(s);
            final PBEParameterSpec params = new PBEParameterSpec(pkcs12PBEParams.getIV(), pkcs12PBEParams.getIterations().intValue());
            final Cipher cipher = this.helper.createCipher(s);
            cipher.init(3, secretKeyFactory.generateSecret(keySpec), params);
            return cipher.wrap(key);
        }
        catch (Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("exception encrypting data - ");
            sb.append(ex.toString());
            throw new IOException(sb.toString());
        }
    }
    
    public static class BCPKCS12KeyStore extends PKCS12KeyStoreSpi
    {
        public BCPKCS12KeyStore() {
            super(new BCJcaJceHelper(), BCPKCS12KeyStore.pbeWithSHAAnd3_KeyTripleDES_CBC, BCPKCS12KeyStore.pbeWithSHAAnd40BitRC2_CBC);
        }
    }
    
    public static class BCPKCS12KeyStore3DES extends PKCS12KeyStoreSpi
    {
        public BCPKCS12KeyStore3DES() {
            super(new BCJcaJceHelper(), BCPKCS12KeyStore3DES.pbeWithSHAAnd3_KeyTripleDES_CBC, BCPKCS12KeyStore3DES.pbeWithSHAAnd3_KeyTripleDES_CBC);
        }
    }
    
    private class CertId
    {
        byte[] id;
        
        CertId(final PublicKey publicKey) {
            this.id = PKCS12KeyStoreSpi.this.createSubjectKeyId(publicKey).getKeyIdentifier();
        }
        
        CertId(final byte[] id) {
            this.id = id;
        }
        
        @Override
        public boolean equals(final Object o) {
            return o == this || (o instanceof CertId && Arrays.areEqual(this.id, ((CertId)o).id));
        }
        
        @Override
        public int hashCode() {
            return Arrays.hashCode(this.id);
        }
    }
    
    public static class DefPKCS12KeyStore extends PKCS12KeyStoreSpi
    {
        public DefPKCS12KeyStore() {
            super(new DefaultJcaJceHelper(), DefPKCS12KeyStore.pbeWithSHAAnd3_KeyTripleDES_CBC, DefPKCS12KeyStore.pbeWithSHAAnd40BitRC2_CBC);
        }
    }
    
    public static class DefPKCS12KeyStore3DES extends PKCS12KeyStoreSpi
    {
        public DefPKCS12KeyStore3DES() {
            super(new DefaultJcaJceHelper(), DefPKCS12KeyStore3DES.pbeWithSHAAnd3_KeyTripleDES_CBC, DefPKCS12KeyStore3DES.pbeWithSHAAnd3_KeyTripleDES_CBC);
        }
    }
    
    private static class DefaultSecretKeyProvider
    {
        private final Map KEY_SIZES;
        
        DefaultSecretKeyProvider() {
            final HashMap<ASN1ObjectIdentifier, Integer> m = new HashMap<ASN1ObjectIdentifier, Integer>();
            m.put(new ASN1ObjectIdentifier("1.2.840.113533.7.66.10"), Integers.valueOf(128));
            m.put(PKCSObjectIdentifiers.des_EDE3_CBC, Integers.valueOf(192));
            m.put(NISTObjectIdentifiers.id_aes128_CBC, Integers.valueOf(128));
            m.put(NISTObjectIdentifiers.id_aes192_CBC, Integers.valueOf(192));
            m.put(NISTObjectIdentifiers.id_aes256_CBC, Integers.valueOf(256));
            m.put(NTTObjectIdentifiers.id_camellia128_cbc, Integers.valueOf(128));
            m.put(NTTObjectIdentifiers.id_camellia192_cbc, Integers.valueOf(192));
            m.put(NTTObjectIdentifiers.id_camellia256_cbc, Integers.valueOf(256));
            m.put(CryptoProObjectIdentifiers.gostR28147_gcfb, Integers.valueOf(256));
            this.KEY_SIZES = Collections.unmodifiableMap((Map<?, ?>)m);
        }
        
        public int getKeySize(final AlgorithmIdentifier algorithmIdentifier) {
            final Integer n = this.KEY_SIZES.get(algorithmIdentifier.getAlgorithm());
            if (n != null) {
                return n;
            }
            return -1;
        }
    }
    
    private static class IgnoresCaseHashtable
    {
        private Hashtable keys;
        private Hashtable orig;
        
        private IgnoresCaseHashtable() {
            this.orig = new Hashtable();
            this.keys = new Hashtable();
        }
        
        public Enumeration elements() {
            return this.orig.elements();
        }
        
        public Object get(String lowerCase) {
            final Hashtable keys = this.keys;
            if (lowerCase == null) {
                lowerCase = null;
            }
            else {
                lowerCase = Strings.toLowerCase(lowerCase);
            }
            lowerCase = keys.get(lowerCase);
            if (lowerCase == null) {
                return null;
            }
            return this.orig.get(lowerCase);
        }
        
        public Enumeration keys() {
            return this.orig.keys();
        }
        
        public void put(final String s, final Object value) {
            String lowerCase;
            if (s == null) {
                lowerCase = null;
            }
            else {
                lowerCase = Strings.toLowerCase(s);
            }
            final String key = this.keys.get(lowerCase);
            if (key != null) {
                this.orig.remove(key);
            }
            this.keys.put(lowerCase, s);
            this.orig.put(s, value);
        }
        
        public Object remove(String lowerCase) {
            final Hashtable keys = this.keys;
            if (lowerCase == null) {
                lowerCase = null;
            }
            else {
                lowerCase = Strings.toLowerCase(lowerCase);
            }
            lowerCase = keys.remove(lowerCase);
            if (lowerCase == null) {
                return null;
            }
            return this.orig.remove(lowerCase);
        }
        
        public int size() {
            return this.orig.size();
        }
    }
}
