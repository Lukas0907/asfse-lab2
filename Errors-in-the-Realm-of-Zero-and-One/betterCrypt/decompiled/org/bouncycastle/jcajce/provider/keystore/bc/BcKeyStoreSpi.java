// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.keystore.bc;

import java.io.ByteArrayOutputStream;
import org.bouncycastle.crypto.io.DigestOutputStream;
import org.bouncycastle.jcajce.io.CipherOutputStream;
import org.bouncycastle.util.io.Streams;
import org.bouncycastle.crypto.io.DigestInputStream;
import org.bouncycastle.jcajce.io.CipherInputStream;
import javax.crypto.SecretKeyFactory;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.Cipher;
import org.bouncycastle.util.io.TeeOutputStream;
import org.bouncycastle.crypto.io.MacOutputStream;
import java.io.OutputStream;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.Mac;
import org.bouncycastle.crypto.io.MacInputStream;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.generators.PKCS12ParametersGenerator;
import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.digests.SHA1Digest;
import java.security.UnrecoverableKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.security.KeyStoreException;
import java.util.Enumeration;
import java.security.PublicKey;
import java.security.PrivateKey;
import java.security.cert.CertificateEncodingException;
import java.security.spec.EncodedKeySpec;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.security.cert.Certificate;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.DataOutputStream;
import java.security.Key;
import org.bouncycastle.jcajce.util.BCJcaJceHelper;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import java.util.Hashtable;
import java.security.SecureRandom;
import org.bouncycastle.jcajce.util.JcaJceHelper;
import org.bouncycastle.jce.interfaces.BCKeyStore;
import java.security.KeyStoreSpi;

public class BcKeyStoreSpi extends KeyStoreSpi implements BCKeyStore
{
    static final int CERTIFICATE = 1;
    static final int KEY = 2;
    private static final String KEY_CIPHER = "PBEWithSHAAnd3-KeyTripleDES-CBC";
    static final int KEY_PRIVATE = 0;
    static final int KEY_PUBLIC = 1;
    private static final int KEY_SALT_SIZE = 20;
    static final int KEY_SECRET = 2;
    private static final int MIN_ITERATIONS = 1024;
    static final int NULL = 0;
    static final int SEALED = 4;
    static final int SECRET = 3;
    private static final String STORE_CIPHER = "PBEWithSHAAndTwofish-CBC";
    private static final int STORE_SALT_SIZE = 20;
    private static final int STORE_VERSION = 2;
    private final JcaJceHelper helper;
    protected SecureRandom random;
    protected Hashtable table;
    protected int version;
    
    public BcKeyStoreSpi(final int version) {
        this.table = new Hashtable();
        this.random = CryptoServicesRegistrar.getSecureRandom();
        this.helper = new BCJcaJceHelper();
        this.version = version;
    }
    
    private Certificate decodeCertificate(final DataInputStream dataInputStream) throws IOException {
        final String utf = dataInputStream.readUTF();
        final byte[] array = new byte[dataInputStream.readInt()];
        dataInputStream.readFully(array);
        try {
            return this.helper.createCertificateFactory(utf).generateCertificate(new ByteArrayInputStream(array));
        }
        catch (CertificateException ex) {
            throw new IOException(ex.toString());
        }
        catch (NoSuchProviderException ex2) {
            throw new IOException(ex2.toString());
        }
    }
    
    private Key decodeKey(final DataInputStream dataInputStream) throws IOException {
        final int read = dataInputStream.read();
        final String utf = dataInputStream.readUTF();
        final String utf2 = dataInputStream.readUTF();
        final byte[] array = new byte[dataInputStream.readInt()];
        dataInputStream.readFully(array);
        EncodedKeySpec keySpec;
        if (!utf.equals("PKCS#8") && !utf.equals("PKCS8")) {
            if (!utf.equals("X.509") && !utf.equals("X509")) {
                if (utf.equals("RAW")) {
                    return new SecretKeySpec(array, utf2);
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Key format ");
                sb.append(utf);
                sb.append(" not recognised!");
                throw new IOException(sb.toString());
            }
            else {
                keySpec = new X509EncodedKeySpec(array);
            }
        }
        else {
            keySpec = new PKCS8EncodedKeySpec(array);
        }
        Label_0239: {
            if (read == 0) {
                break Label_0239;
            }
            Label_0230: {
                if (read == 1) {
                    break Label_0230;
                }
                Label_0190: {
                    if (read != 2) {
                        break Label_0190;
                    }
                    while (true) {
                        try {
                            return this.helper.createSecretKeyFactory(utf2).generateSecret(keySpec);
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Key type ");
                            sb2.append(read);
                            sb2.append(" not recognised!");
                            throw new IOException(sb2.toString());
                            final StringBuilder sb3 = new StringBuilder();
                            sb3.append("Exception creating key: ");
                            final Exception ex;
                            sb3.append(ex.toString());
                            throw new IOException(sb3.toString());
                            return BouncyCastleProvider.getPrivateKey(PrivateKeyInfo.getInstance(array));
                            return BouncyCastleProvider.getPublicKey(SubjectPublicKeyInfo.getInstance(array));
                        }
                        catch (Exception ex) {
                            continue;
                        }
                        break;
                    }
                }
            }
        }
    }
    
    private void encodeCertificate(final Certificate certificate, final DataOutputStream dataOutputStream) throws IOException {
        try {
            final byte[] encoded = certificate.getEncoded();
            dataOutputStream.writeUTF(certificate.getType());
            dataOutputStream.writeInt(encoded.length);
            dataOutputStream.write(encoded);
        }
        catch (CertificateEncodingException ex) {
            throw new IOException(ex.toString());
        }
    }
    
    private void encodeKey(final Key key, final DataOutputStream dataOutputStream) throws IOException {
        final byte[] encoded = key.getEncoded();
        int b;
        if (key instanceof PrivateKey) {
            b = 0;
        }
        else if (key instanceof PublicKey) {
            b = 1;
        }
        else {
            b = 2;
        }
        dataOutputStream.write(b);
        dataOutputStream.writeUTF(key.getFormat());
        dataOutputStream.writeUTF(key.getAlgorithm());
        dataOutputStream.writeInt(encoded.length);
        dataOutputStream.write(encoded);
    }
    
    @Override
    public Enumeration engineAliases() {
        return this.table.keys();
    }
    
    @Override
    public boolean engineContainsAlias(final String key) {
        return this.table.get(key) != null;
    }
    
    @Override
    public void engineDeleteEntry(final String s) throws KeyStoreException {
        if (this.table.get(s) == null) {
            return;
        }
        this.table.remove(s);
    }
    
    @Override
    public Certificate engineGetCertificate(final String key) {
        final StoreEntry storeEntry = this.table.get(key);
        if (storeEntry != null) {
            if (storeEntry.getType() == 1) {
                return (Certificate)storeEntry.getObject();
            }
            final Certificate[] certificateChain = storeEntry.getCertificateChain();
            if (certificateChain != null) {
                return certificateChain[0];
            }
        }
        return null;
    }
    
    @Override
    public String engineGetCertificateAlias(final Certificate certificate) {
        final Enumeration<StoreEntry> elements = this.table.elements();
        while (elements.hasMoreElements()) {
            final StoreEntry storeEntry = elements.nextElement();
            if (storeEntry.getObject() instanceof Certificate) {
                if (((Certificate)storeEntry.getObject()).equals(certificate)) {
                    return storeEntry.getAlias();
                }
                continue;
            }
            else {
                final Certificate[] certificateChain = storeEntry.getCertificateChain();
                if (certificateChain != null && certificateChain[0].equals(certificate)) {
                    return storeEntry.getAlias();
                }
                continue;
            }
        }
        return null;
    }
    
    @Override
    public Certificate[] engineGetCertificateChain(final String key) {
        final StoreEntry storeEntry = this.table.get(key);
        if (storeEntry != null) {
            return storeEntry.getCertificateChain();
        }
        return null;
    }
    
    @Override
    public Date engineGetCreationDate(final String key) {
        final StoreEntry storeEntry = this.table.get(key);
        if (storeEntry != null) {
            return storeEntry.getDate();
        }
        return null;
    }
    
    @Override
    public Key engineGetKey(final String key, final char[] array) throws NoSuchAlgorithmException, UnrecoverableKeyException {
        final StoreEntry storeEntry = this.table.get(key);
        if (storeEntry != null && storeEntry.getType() != 1) {
            return (Key)storeEntry.getObject(array);
        }
        return null;
    }
    
    @Override
    public boolean engineIsCertificateEntry(final String key) {
        final StoreEntry storeEntry = this.table.get(key);
        return storeEntry != null && storeEntry.getType() == 1;
    }
    
    @Override
    public boolean engineIsKeyEntry(final String key) {
        final StoreEntry storeEntry = this.table.get(key);
        return storeEntry != null && storeEntry.getType() != 1;
    }
    
    @Override
    public void engineLoad(final InputStream in, final char[] array) throws IOException {
        this.table.clear();
        if (in == null) {
            return;
        }
        final DataInputStream dataInputStream = new DataInputStream(in);
        final int int1 = dataInputStream.readInt();
        if (int1 != 2 && int1 != 0 && int1 != 1) {
            throw new IOException("Wrong version of key store.");
        }
        final int int2 = dataInputStream.readInt();
        if (int2 <= 0) {
            throw new IOException("Invalid salt detected");
        }
        final byte[] b = new byte[int2];
        dataInputStream.readFully(b);
        final int int3 = dataInputStream.readInt();
        final HMac hMac = new HMac(new SHA1Digest());
        if (array == null || array.length == 0) {
            this.loadStore(dataInputStream);
            dataInputStream.readFully(new byte[hMac.getMacSize()]);
            return;
        }
        final byte[] pkcs12PasswordToBytes = PBEParametersGenerator.PKCS12PasswordToBytes(array);
        final PKCS12ParametersGenerator pkcs12ParametersGenerator = new PKCS12ParametersGenerator(new SHA1Digest());
        pkcs12ParametersGenerator.init(pkcs12PasswordToBytes, b, int3);
        int macSize;
        if (int1 != 2) {
            macSize = hMac.getMacSize();
        }
        else {
            macSize = hMac.getMacSize() * 8;
        }
        final CipherParameters generateDerivedMacParameters = pkcs12ParametersGenerator.generateDerivedMacParameters(macSize);
        Arrays.fill(pkcs12PasswordToBytes, (byte)0);
        hMac.init(generateDerivedMacParameters);
        this.loadStore(new MacInputStream(dataInputStream, hMac));
        final byte[] array2 = new byte[hMac.getMacSize()];
        hMac.doFinal(array2, 0);
        final byte[] b2 = new byte[hMac.getMacSize()];
        dataInputStream.readFully(b2);
        if (Arrays.constantTimeAreEqual(array2, b2)) {
            return;
        }
        this.table.clear();
        throw new IOException("KeyStore integrity check failed.");
    }
    
    @Override
    public void engineSetCertificateEntry(final String key, final Certificate certificate) throws KeyStoreException {
        final StoreEntry storeEntry = this.table.get(key);
        if (storeEntry != null && storeEntry.getType() != 1) {
            final StringBuilder sb = new StringBuilder();
            sb.append("key store already has a key entry with alias ");
            sb.append(key);
            throw new KeyStoreException(sb.toString());
        }
        this.table.put(key, new StoreEntry(key, certificate));
    }
    
    @Override
    public void engineSetKeyEntry(final String key, final Key key2, final char[] array, final Certificate[] array2) throws KeyStoreException {
        if (key2 instanceof PrivateKey) {
            if (array2 == null) {
                throw new KeyStoreException("no certificate chain for private key");
            }
        }
        try {
            this.table.put(key, new StoreEntry(key, key2, array, array2));
        }
        catch (Exception ex) {
            throw new KeyStoreException(ex.toString());
        }
    }
    
    @Override
    public void engineSetKeyEntry(final String key, final byte[] array, final Certificate[] array2) throws KeyStoreException {
        this.table.put(key, new StoreEntry(key, array, array2));
    }
    
    @Override
    public int engineSize() {
        return this.table.size();
    }
    
    @Override
    public void engineStore(final OutputStream out, final char[] array) throws IOException {
        final DataOutputStream dataOutputStream = new DataOutputStream(out);
        final byte[] array2 = new byte[20];
        final int v = (this.random.nextInt() & 0x3FF) + 1024;
        this.random.nextBytes(array2);
        dataOutputStream.writeInt(this.version);
        dataOutputStream.writeInt(array2.length);
        dataOutputStream.write(array2);
        dataOutputStream.writeInt(v);
        final HMac hMac = new HMac(new SHA1Digest());
        final MacOutputStream macOutputStream = new MacOutputStream(hMac);
        final PKCS12ParametersGenerator pkcs12ParametersGenerator = new PKCS12ParametersGenerator(new SHA1Digest());
        final byte[] pkcs12PasswordToBytes = PBEParametersGenerator.PKCS12PasswordToBytes(array);
        pkcs12ParametersGenerator.init(pkcs12PasswordToBytes, array2, v);
        int macSize;
        if (this.version < 2) {
            macSize = hMac.getMacSize();
        }
        else {
            macSize = hMac.getMacSize() * 8;
        }
        hMac.init(pkcs12ParametersGenerator.generateDerivedMacParameters(macSize));
        for (int i = 0; i != pkcs12PasswordToBytes.length; ++i) {
            pkcs12PasswordToBytes[i] = 0;
        }
        this.saveStore(new TeeOutputStream(dataOutputStream, macOutputStream));
        final byte[] b = new byte[hMac.getMacSize()];
        hMac.doFinal(b, 0);
        dataOutputStream.write(b);
        dataOutputStream.close();
    }
    
    protected void loadStore(final InputStream in) throws IOException {
        final DataInputStream dataInputStream = new DataInputStream(in);
        for (int i = dataInputStream.read(); i > 0; i = dataInputStream.read()) {
            final String utf = dataInputStream.readUTF();
            final Date date = new Date(dataInputStream.readLong());
            final int int1 = dataInputStream.readInt();
            Certificate[] array = null;
            if (int1 != 0) {
                final Certificate[] array2 = new Certificate[int1];
                int n = 0;
                while (true) {
                    array = array2;
                    if (n == int1) {
                        break;
                    }
                    array2[n] = this.decodeCertificate(dataInputStream);
                    ++n;
                }
            }
            Hashtable table2;
            StoreEntry value;
            if (i != 1) {
                if (i != 2) {
                    if (i != 3 && i != 4) {
                        throw new IOException("Unknown object type in store.");
                    }
                    final byte[] b = new byte[dataInputStream.readInt()];
                    dataInputStream.readFully(b);
                    this.table.put(utf, new StoreEntry(utf, date, i, b, array));
                    continue;
                }
                else {
                    final Key decodeKey = this.decodeKey(dataInputStream);
                    final Hashtable table = this.table;
                    final StoreEntry storeEntry = new StoreEntry(utf, date, 2, decodeKey, array);
                    table2 = table;
                    value = storeEntry;
                }
            }
            else {
                final Certificate decodeCertificate = this.decodeCertificate(dataInputStream);
                table2 = this.table;
                value = new StoreEntry(utf, date, 1, decodeCertificate);
            }
            table2.put(utf, value);
        }
    }
    
    protected Cipher makePBECipher(final String s, final int opmode, final char[] password, final byte[] salt, final int iterationCount) throws IOException {
        try {
            final PBEKeySpec keySpec = new PBEKeySpec(password);
            final SecretKeyFactory secretKeyFactory = this.helper.createSecretKeyFactory(s);
            final PBEParameterSpec params = new PBEParameterSpec(salt, iterationCount);
            final Cipher cipher = this.helper.createCipher(s);
            cipher.init(opmode, secretKeyFactory.generateSecret(keySpec), params);
            return cipher;
        }
        catch (Exception obj) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Error initialising store of key store: ");
            sb.append(obj);
            throw new IOException(sb.toString());
        }
    }
    
    protected void saveStore(final OutputStream out) throws IOException {
        final Enumeration elements = this.table.elements();
        final DataOutputStream dataOutputStream = new DataOutputStream(out);
        while (true) {
            final boolean hasMoreElements = elements.hasMoreElements();
            int i = 0;
            if (!hasMoreElements) {
                dataOutputStream.write(0);
                return;
            }
            final StoreEntry storeEntry = elements.nextElement();
            dataOutputStream.write(storeEntry.getType());
            dataOutputStream.writeUTF(storeEntry.getAlias());
            dataOutputStream.writeLong(storeEntry.getDate().getTime());
            final Certificate[] certificateChain = storeEntry.getCertificateChain();
            if (certificateChain == null) {
                dataOutputStream.writeInt(0);
            }
            else {
                dataOutputStream.writeInt(certificateChain.length);
                while (i != certificateChain.length) {
                    this.encodeCertificate(certificateChain[i], dataOutputStream);
                    ++i;
                }
            }
            final int type = storeEntry.getType();
            if (type != 1) {
                if (type != 2) {
                    if (type != 3 && type != 4) {
                        throw new IOException("Unknown object type in store.");
                    }
                    final byte[] b = (byte[])storeEntry.getObject();
                    dataOutputStream.writeInt(b.length);
                    dataOutputStream.write(b);
                }
                else {
                    this.encodeKey((Key)storeEntry.getObject(), dataOutputStream);
                }
            }
            else {
                this.encodeCertificate((Certificate)storeEntry.getObject(), dataOutputStream);
            }
        }
    }
    
    @Override
    public void setRandom(final SecureRandom random) {
        this.random = random;
    }
    
    public static class BouncyCastleStore extends BcKeyStoreSpi
    {
        public BouncyCastleStore() {
            super(1);
        }
        
        @Override
        public void engineLoad(final InputStream in, final char[] array) throws IOException {
            this.table.clear();
            if (in == null) {
                return;
            }
            final DataInputStream dataInputStream = new DataInputStream(in);
            final int int1 = dataInputStream.readInt();
            if (int1 != 2 && int1 != 0 && int1 != 1) {
                throw new IOException("Wrong version of key store.");
            }
            final byte[] b = new byte[dataInputStream.readInt()];
            if (b.length != 20) {
                throw new IOException("Key store corrupted.");
            }
            dataInputStream.readFully(b);
            final int int2 = dataInputStream.readInt();
            if (int2 < 0 || int2 > 65536) {
                throw new IOException("Key store corrupted.");
            }
            String s;
            if (int1 == 0) {
                s = "OldPBEWithSHAAndTwofish-CBC";
            }
            else {
                s = "PBEWithSHAAndTwofish-CBC";
            }
            final CipherInputStream cipherInputStream = new CipherInputStream(dataInputStream, this.makePBECipher(s, 2, array, b, int2));
            final SHA1Digest sha1Digest = new SHA1Digest();
            this.loadStore(new DigestInputStream(cipherInputStream, sha1Digest));
            final byte[] array2 = new byte[sha1Digest.getDigestSize()];
            sha1Digest.doFinal(array2, 0);
            final byte[] array3 = new byte[sha1Digest.getDigestSize()];
            Streams.readFully(cipherInputStream, array3);
            if (Arrays.constantTimeAreEqual(array2, array3)) {
                return;
            }
            this.table.clear();
            throw new IOException("KeyStore integrity check failed.");
        }
        
        @Override
        public void engineStore(final OutputStream out, final char[] array) throws IOException {
            final DataOutputStream dataOutputStream = new DataOutputStream(out);
            final byte[] array2 = new byte[20];
            final int v = (this.random.nextInt() & 0x3FF) + 1024;
            this.random.nextBytes(array2);
            dataOutputStream.writeInt(this.version);
            dataOutputStream.writeInt(array2.length);
            dataOutputStream.write(array2);
            dataOutputStream.writeInt(v);
            final CipherOutputStream cipherOutputStream = new CipherOutputStream(dataOutputStream, this.makePBECipher("PBEWithSHAAndTwofish-CBC", 1, array, array2, v));
            final DigestOutputStream digestOutputStream = new DigestOutputStream(new SHA1Digest());
            this.saveStore(new TeeOutputStream(cipherOutputStream, digestOutputStream));
            cipherOutputStream.write(digestOutputStream.getDigest());
            cipherOutputStream.close();
        }
    }
    
    public static class Std extends BcKeyStoreSpi
    {
        public Std() {
            super(2);
        }
    }
    
    private class StoreEntry
    {
        String alias;
        Certificate[] certChain;
        Date date;
        Object obj;
        int type;
        
        StoreEntry(final String alias, final Key key, final char[] array, final Certificate[] certChain) throws Exception {
            this.date = new Date();
            this.type = 4;
            this.alias = alias;
            this.certChain = certChain;
            final byte[] array2 = new byte[20];
            BcKeyStoreSpi.this.random.setSeed(System.currentTimeMillis());
            BcKeyStoreSpi.this.random.nextBytes(array2);
            final int v = (BcKeyStoreSpi.this.random.nextInt() & 0x3FF) + 1024;
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            final DataOutputStream dataOutputStream = new DataOutputStream(out);
            dataOutputStream.writeInt(array2.length);
            dataOutputStream.write(array2);
            dataOutputStream.writeInt(v);
            final DataOutputStream dataOutputStream2 = new DataOutputStream(new CipherOutputStream(dataOutputStream, BcKeyStoreSpi.this.makePBECipher("PBEWithSHAAnd3-KeyTripleDES-CBC", 1, array, array2, v)));
            BcKeyStoreSpi.this.encodeKey(key, dataOutputStream2);
            dataOutputStream2.close();
            this.obj = out.toByteArray();
        }
        
        StoreEntry(final String alias, final Certificate obj) {
            this.date = new Date();
            this.type = 1;
            this.alias = alias;
            this.obj = obj;
            this.certChain = null;
        }
        
        StoreEntry(final String alias, final Date date, final int type, final Object obj) {
            this.date = new Date();
            this.alias = alias;
            this.date = date;
            this.type = type;
            this.obj = obj;
        }
        
        StoreEntry(final String alias, final Date date, final int type, final Object obj, final Certificate[] certChain) {
            this.date = new Date();
            this.alias = alias;
            this.date = date;
            this.type = type;
            this.obj = obj;
            this.certChain = certChain;
        }
        
        StoreEntry(final String alias, final byte[] obj, final Certificate[] certChain) {
            this.date = new Date();
            this.type = 3;
            this.alias = alias;
            this.obj = obj;
            this.certChain = certChain;
        }
        
        String getAlias() {
            return this.alias;
        }
        
        Certificate[] getCertificateChain() {
            return this.certChain;
        }
        
        Date getDate() {
            return this.date;
        }
        
        Object getObject() {
            return this.obj;
        }
        
        Object getObject(final char[] p0) throws NoSuchAlgorithmException, UnrecoverableKeyException {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     4: aload_1        
            //     5: arraylength    
            //     6: ifne            23
            //     9: aload_0        
            //    10: getfield        org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi$StoreEntry.obj:Ljava/lang/Object;
            //    13: astore_3       
            //    14: aload_3        
            //    15: instanceof      Ljava/security/Key;
            //    18: ifeq            23
            //    21: aload_3        
            //    22: areturn        
            //    23: aload_0        
            //    24: getfield        org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi$StoreEntry.type:I
            //    27: iconst_4       
            //    28: if_icmpne       408
            //    31: new             Ljava/io/DataInputStream;
            //    34: dup            
            //    35: new             Ljava/io/ByteArrayInputStream;
            //    38: dup            
            //    39: aload_0        
            //    40: getfield        org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi$StoreEntry.obj:Ljava/lang/Object;
            //    43: checkcast       [B
            //    46: checkcast       [B
            //    49: invokespecial   java/io/ByteArrayInputStream.<init>:([B)V
            //    52: invokespecial   java/io/DataInputStream.<init>:(Ljava/io/InputStream;)V
            //    55: astore_3       
            //    56: aload_3        
            //    57: invokevirtual   java/io/DataInputStream.readInt:()I
            //    60: newarray        B
            //    62: astore          4
            //    64: aload_3        
            //    65: aload           4
            //    67: invokevirtual   java/io/DataInputStream.readFully:([B)V
            //    70: aload_3        
            //    71: invokevirtual   java/io/DataInputStream.readInt:()I
            //    74: istore_2       
            //    75: new             Lorg/bouncycastle/jcajce/io/CipherInputStream;
            //    78: dup            
            //    79: aload_3        
            //    80: aload_0        
            //    81: getfield        org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi$StoreEntry.this$0:Lorg/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi;
            //    84: ldc             "PBEWithSHAAnd3-KeyTripleDES-CBC"
            //    86: iconst_2       
            //    87: aload_1        
            //    88: aload           4
            //    90: iload_2        
            //    91: invokevirtual   org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi.makePBECipher:(Ljava/lang/String;I[C[BI)Ljavax/crypto/Cipher;
            //    94: invokespecial   org/bouncycastle/jcajce/io/CipherInputStream.<init>:(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V
            //    97: astore_3       
            //    98: aload_0        
            //    99: getfield        org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi$StoreEntry.this$0:Lorg/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi;
            //   102: new             Ljava/io/DataInputStream;
            //   105: dup            
            //   106: aload_3        
            //   107: invokespecial   java/io/DataInputStream.<init>:(Ljava/io/InputStream;)V
            //   110: invokestatic    org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi.access$100:(Lorg/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi;Ljava/io/DataInputStream;)Ljava/security/Key;
            //   113: astore_3       
            //   114: aload_3        
            //   115: areturn        
            //   116: new             Ljava/io/DataInputStream;
            //   119: dup            
            //   120: new             Ljava/io/ByteArrayInputStream;
            //   123: dup            
            //   124: aload_0        
            //   125: getfield        org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi$StoreEntry.obj:Ljava/lang/Object;
            //   128: checkcast       [B
            //   131: checkcast       [B
            //   134: invokespecial   java/io/ByteArrayInputStream.<init>:([B)V
            //   137: invokespecial   java/io/DataInputStream.<init>:(Ljava/io/InputStream;)V
            //   140: astore_3       
            //   141: aload_3        
            //   142: invokevirtual   java/io/DataInputStream.readInt:()I
            //   145: newarray        B
            //   147: astore          4
            //   149: aload_3        
            //   150: aload           4
            //   152: invokevirtual   java/io/DataInputStream.readFully:([B)V
            //   155: aload_3        
            //   156: invokevirtual   java/io/DataInputStream.readInt:()I
            //   159: istore_2       
            //   160: new             Lorg/bouncycastle/jcajce/io/CipherInputStream;
            //   163: dup            
            //   164: aload_3        
            //   165: aload_0        
            //   166: getfield        org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi$StoreEntry.this$0:Lorg/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi;
            //   169: ldc             "BrokenPBEWithSHAAnd3-KeyTripleDES-CBC"
            //   171: iconst_2       
            //   172: aload_1        
            //   173: aload           4
            //   175: iload_2        
            //   176: invokevirtual   org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi.makePBECipher:(Ljava/lang/String;I[C[BI)Ljavax/crypto/Cipher;
            //   179: invokespecial   org/bouncycastle/jcajce/io/CipherInputStream.<init>:(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V
            //   182: astore_3       
            //   183: aload_0        
            //   184: getfield        org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi$StoreEntry.this$0:Lorg/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi;
            //   187: new             Ljava/io/DataInputStream;
            //   190: dup            
            //   191: aload_3        
            //   192: invokespecial   java/io/DataInputStream.<init>:(Ljava/io/InputStream;)V
            //   195: invokestatic    org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi.access$100:(Lorg/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi;Ljava/io/DataInputStream;)Ljava/security/Key;
            //   198: astore_3       
            //   199: goto            288
            //   202: new             Ljava/io/DataInputStream;
            //   205: dup            
            //   206: new             Ljava/io/ByteArrayInputStream;
            //   209: dup            
            //   210: aload_0        
            //   211: getfield        org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi$StoreEntry.obj:Ljava/lang/Object;
            //   214: checkcast       [B
            //   217: checkcast       [B
            //   220: invokespecial   java/io/ByteArrayInputStream.<init>:([B)V
            //   223: invokespecial   java/io/DataInputStream.<init>:(Ljava/io/InputStream;)V
            //   226: astore_3       
            //   227: aload_3        
            //   228: invokevirtual   java/io/DataInputStream.readInt:()I
            //   231: newarray        B
            //   233: astore          4
            //   235: aload_3        
            //   236: aload           4
            //   238: invokevirtual   java/io/DataInputStream.readFully:([B)V
            //   241: aload_3        
            //   242: invokevirtual   java/io/DataInputStream.readInt:()I
            //   245: istore_2       
            //   246: new             Lorg/bouncycastle/jcajce/io/CipherInputStream;
            //   249: dup            
            //   250: aload_3        
            //   251: aload_0        
            //   252: getfield        org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi$StoreEntry.this$0:Lorg/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi;
            //   255: ldc             "OldPBEWithSHAAnd3-KeyTripleDES-CBC"
            //   257: iconst_2       
            //   258: aload_1        
            //   259: aload           4
            //   261: iload_2        
            //   262: invokevirtual   org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi.makePBECipher:(Ljava/lang/String;I[C[BI)Ljavax/crypto/Cipher;
            //   265: invokespecial   org/bouncycastle/jcajce/io/CipherInputStream.<init>:(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V
            //   268: astore_3       
            //   269: aload_0        
            //   270: getfield        org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi$StoreEntry.this$0:Lorg/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi;
            //   273: new             Ljava/io/DataInputStream;
            //   276: dup            
            //   277: aload_3        
            //   278: invokespecial   java/io/DataInputStream.<init>:(Ljava/io/InputStream;)V
            //   281: invokestatic    org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi.access$100:(Lorg/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi;Ljava/io/DataInputStream;)Ljava/security/Key;
            //   284: astore_3       
            //   285: goto            199
            //   288: aload_3        
            //   289: ifnull          388
            //   292: new             Ljava/io/ByteArrayOutputStream;
            //   295: dup            
            //   296: invokespecial   java/io/ByteArrayOutputStream.<init>:()V
            //   299: astore          5
            //   301: new             Ljava/io/DataOutputStream;
            //   304: dup            
            //   305: aload           5
            //   307: invokespecial   java/io/DataOutputStream.<init>:(Ljava/io/OutputStream;)V
            //   310: astore          6
            //   312: aload           6
            //   314: aload           4
            //   316: arraylength    
            //   317: invokevirtual   java/io/DataOutputStream.writeInt:(I)V
            //   320: aload           6
            //   322: aload           4
            //   324: invokevirtual   java/io/DataOutputStream.write:([B)V
            //   327: aload           6
            //   329: iload_2        
            //   330: invokevirtual   java/io/DataOutputStream.writeInt:(I)V
            //   333: new             Ljava/io/DataOutputStream;
            //   336: dup            
            //   337: new             Lorg/bouncycastle/jcajce/io/CipherOutputStream;
            //   340: dup            
            //   341: aload           6
            //   343: aload_0        
            //   344: getfield        org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi$StoreEntry.this$0:Lorg/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi;
            //   347: ldc             "PBEWithSHAAnd3-KeyTripleDES-CBC"
            //   349: iconst_1       
            //   350: aload_1        
            //   351: aload           4
            //   353: iload_2        
            //   354: invokevirtual   org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi.makePBECipher:(Ljava/lang/String;I[C[BI)Ljavax/crypto/Cipher;
            //   357: invokespecial   org/bouncycastle/jcajce/io/CipherOutputStream.<init>:(Ljava/io/OutputStream;Ljavax/crypto/Cipher;)V
            //   360: invokespecial   java/io/DataOutputStream.<init>:(Ljava/io/OutputStream;)V
            //   363: astore_1       
            //   364: aload_0        
            //   365: getfield        org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi$StoreEntry.this$0:Lorg/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi;
            //   368: aload_3        
            //   369: aload_1        
            //   370: invokestatic    org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi.access$000:(Lorg/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi;Ljava/security/Key;Ljava/io/DataOutputStream;)V
            //   373: aload_1        
            //   374: invokevirtual   java/io/DataOutputStream.close:()V
            //   377: aload_0        
            //   378: aload           5
            //   380: invokevirtual   java/io/ByteArrayOutputStream.toByteArray:()[B
            //   383: putfield        org/bouncycastle/jcajce/provider/keystore/bc/BcKeyStoreSpi$StoreEntry.obj:Ljava/lang/Object;
            //   386: aload_3        
            //   387: areturn        
            //   388: new             Ljava/security/UnrecoverableKeyException;
            //   391: dup            
            //   392: ldc             "no match"
            //   394: invokespecial   java/security/UnrecoverableKeyException.<init>:(Ljava/lang/String;)V
            //   397: athrow         
            //   398: new             Ljava/security/UnrecoverableKeyException;
            //   401: dup            
            //   402: ldc             "no match"
            //   404: invokespecial   java/security/UnrecoverableKeyException.<init>:(Ljava/lang/String;)V
            //   407: athrow         
            //   408: new             Ljava/lang/RuntimeException;
            //   411: dup            
            //   412: ldc             "forget something!"
            //   414: invokespecial   java/lang/RuntimeException.<init>:(Ljava/lang/String;)V
            //   417: athrow         
            //   418: astore_1       
            //   419: goto            398
            //   422: astore_3       
            //   423: goto            116
            //   426: astore_3       
            //   427: goto            202
            //    Exceptions:
            //  throws java.security.NoSuchAlgorithmException
            //  throws java.security.UnrecoverableKeyException
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                 
            //  -----  -----  -----  -----  ---------------------
            //  56     98     418    408    Ljava/lang/Exception;
            //  98     114    422    430    Ljava/lang/Exception;
            //  116    183    418    408    Ljava/lang/Exception;
            //  183    199    426    288    Ljava/lang/Exception;
            //  202    285    418    408    Ljava/lang/Exception;
            //  292    386    418    408    Ljava/lang/Exception;
            //  388    398    418    408    Ljava/lang/Exception;
            // 
            // The error that occurred was:
            // 
            // java.lang.IndexOutOfBoundsException: Index 218 out of bounds for length 218
            //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
            //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
            //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
            //     at java.base/java.util.Objects.checkIndex(Objects.java:373)
            //     at java.base/java.util.ArrayList.get(ArrayList.java:426)
            //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
            //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:576)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        int getType() {
            return this.type;
        }
    }
    
    public static class Version1 extends BcKeyStoreSpi
    {
        public Version1() {
            super(1);
        }
    }
}
