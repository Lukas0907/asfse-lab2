// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.keystore.bcfks;

import org.bouncycastle.jcajce.util.BCJcaJceHelper;
import org.bouncycastle.asn1.x509.X509ObjectIdentifiers;
import org.bouncycastle.jcajce.util.DefaultJcaJceHelper;
import org.bouncycastle.jcajce.BCFKSStoreParameter;
import org.bouncycastle.asn1.bc.ObjectStore;
import org.bouncycastle.asn1.bc.ObjectStoreIntegrityCheck;
import java.io.OutputStream;
import org.bouncycastle.util.Strings;
import javax.crypto.SecretKey;
import org.bouncycastle.jcajce.BCLoadStoreParameter;
import java.security.cert.CertificateException;
import java.io.InputStream;
import java.security.spec.PKCS8EncodedKeySpec;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import java.security.spec.KeySpec;
import org.bouncycastle.asn1.bc.SecretKeyData;
import org.bouncycastle.asn1.bc.EncryptedSecretKeyData;
import java.security.UnrecoverableKeyException;
import java.security.cert.X509Certificate;
import java.security.KeyStoreException;
import java.util.Iterator;
import java.util.Collection;
import java.util.HashSet;
import java.util.Enumeration;
import java.security.GeneralSecurityException;
import java.security.Signature;
import org.bouncycastle.asn1.bc.SignatureCheck;
import org.bouncycastle.asn1.bc.PbkdMacIntegrityCheck;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import org.bouncycastle.asn1.bc.ObjectStoreData;
import org.bouncycastle.asn1.bc.ObjectDataSequence;
import org.bouncycastle.asn1.bc.EncryptedObjectStoreData;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import java.security.SecureRandom;
import java.security.interfaces.RSAKey;
import java.security.interfaces.DSAKey;
import org.bouncycastle.jce.interfaces.ECKey;
import org.bouncycastle.crypto.util.PBKDF2Config;
import org.bouncycastle.crypto.util.ScryptConfig;
import org.bouncycastle.crypto.util.PBKDFConfig;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.crypto.digests.SHA3Digest;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator;
import org.bouncycastle.crypto.digests.SHA512Digest;
import org.bouncycastle.asn1.pkcs.PBKDF2Params;
import org.bouncycastle.crypto.generators.SCrypt;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.asn1.misc.ScryptParams;
import org.bouncycastle.asn1.misc.MiscObjectIdentifiers;
import org.bouncycastle.crypto.PBEParametersGenerator;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.PasswordCallback;
import java.security.KeyStore;
import java.text.ParseException;
import java.security.AlgorithmParameters;
import org.bouncycastle.asn1.pkcs.EncryptionScheme;
import org.bouncycastle.asn1.cms.CCMParameters;
import org.bouncycastle.asn1.pkcs.PBES2Parameters;
import org.bouncycastle.asn1.ASN1Primitive;
import java.security.cert.CertificateEncodingException;
import org.bouncycastle.asn1.bc.EncryptedPrivateKeyData;
import java.security.cert.Certificate;
import org.bouncycastle.asn1.pkcs.EncryptedPrivateKeyInfo;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.Cipher;
import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import java.security.InvalidKeyException;
import java.io.IOException;
import java.security.Key;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.asn1.nsri.NSRIObjectIdentifiers;
import org.bouncycastle.asn1.ntt.NTTObjectIdentifiers;
import org.bouncycastle.asn1.kisa.KISAObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import java.util.HashMap;
import java.security.PublicKey;
import org.bouncycastle.jcajce.BCFKSLoadStoreParameter;
import java.security.PrivateKey;
import org.bouncycastle.asn1.pkcs.KeyDerivationFunc;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.jcajce.util.JcaJceHelper;
import org.bouncycastle.asn1.bc.ObjectData;
import java.util.Date;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.util.Map;
import java.math.BigInteger;
import java.security.KeyStoreSpi;

class BcFKSKeyStoreSpi extends KeyStoreSpi
{
    private static final BigInteger CERTIFICATE;
    private static final BigInteger PRIVATE_KEY;
    private static final BigInteger PROTECTED_PRIVATE_KEY;
    private static final BigInteger PROTECTED_SECRET_KEY;
    private static final BigInteger SECRET_KEY;
    private static final Map<String, ASN1ObjectIdentifier> oidMap;
    private static final Map<ASN1ObjectIdentifier, String> publicAlgMap;
    private Date creationDate;
    private final Map<String, ObjectData> entries;
    private final JcaJceHelper helper;
    private AlgorithmIdentifier hmacAlgorithm;
    private KeyDerivationFunc hmacPkbdAlgorithm;
    private Date lastModifiedDate;
    private final Map<String, PrivateKey> privateKeyCache;
    private AlgorithmIdentifier signatureAlgorithm;
    private ASN1ObjectIdentifier storeEncryptionAlgorithm;
    private BCFKSLoadStoreParameter.CertChainValidator validator;
    private PublicKey verificationKey;
    
    static {
        oidMap = new HashMap<String, ASN1ObjectIdentifier>();
        publicAlgMap = new HashMap<ASN1ObjectIdentifier, String>();
        BcFKSKeyStoreSpi.oidMap.put("DESEDE", OIWObjectIdentifiers.desEDE);
        BcFKSKeyStoreSpi.oidMap.put("TRIPLEDES", OIWObjectIdentifiers.desEDE);
        BcFKSKeyStoreSpi.oidMap.put("TDEA", OIWObjectIdentifiers.desEDE);
        BcFKSKeyStoreSpi.oidMap.put("HMACSHA1", PKCSObjectIdentifiers.id_hmacWithSHA1);
        BcFKSKeyStoreSpi.oidMap.put("HMACSHA224", PKCSObjectIdentifiers.id_hmacWithSHA224);
        BcFKSKeyStoreSpi.oidMap.put("HMACSHA256", PKCSObjectIdentifiers.id_hmacWithSHA256);
        BcFKSKeyStoreSpi.oidMap.put("HMACSHA384", PKCSObjectIdentifiers.id_hmacWithSHA384);
        BcFKSKeyStoreSpi.oidMap.put("HMACSHA512", PKCSObjectIdentifiers.id_hmacWithSHA512);
        BcFKSKeyStoreSpi.oidMap.put("SEED", KISAObjectIdentifiers.id_seedCBC);
        BcFKSKeyStoreSpi.oidMap.put("CAMELLIA.128", NTTObjectIdentifiers.id_camellia128_cbc);
        BcFKSKeyStoreSpi.oidMap.put("CAMELLIA.192", NTTObjectIdentifiers.id_camellia192_cbc);
        BcFKSKeyStoreSpi.oidMap.put("CAMELLIA.256", NTTObjectIdentifiers.id_camellia256_cbc);
        BcFKSKeyStoreSpi.oidMap.put("ARIA.128", NSRIObjectIdentifiers.id_aria128_cbc);
        BcFKSKeyStoreSpi.oidMap.put("ARIA.192", NSRIObjectIdentifiers.id_aria192_cbc);
        BcFKSKeyStoreSpi.oidMap.put("ARIA.256", NSRIObjectIdentifiers.id_aria256_cbc);
        BcFKSKeyStoreSpi.publicAlgMap.put(PKCSObjectIdentifiers.rsaEncryption, "RSA");
        BcFKSKeyStoreSpi.publicAlgMap.put(X9ObjectIdentifiers.id_ecPublicKey, "EC");
        BcFKSKeyStoreSpi.publicAlgMap.put(OIWObjectIdentifiers.elGamalAlgorithm, "DH");
        BcFKSKeyStoreSpi.publicAlgMap.put(PKCSObjectIdentifiers.dhKeyAgreement, "DH");
        BcFKSKeyStoreSpi.publicAlgMap.put(X9ObjectIdentifiers.id_dsa, "DSA");
        CERTIFICATE = BigInteger.valueOf(0L);
        PRIVATE_KEY = BigInteger.valueOf(1L);
        SECRET_KEY = BigInteger.valueOf(2L);
        PROTECTED_PRIVATE_KEY = BigInteger.valueOf(3L);
        PROTECTED_SECRET_KEY = BigInteger.valueOf(4L);
    }
    
    BcFKSKeyStoreSpi(final JcaJceHelper helper) {
        this.entries = new HashMap<String, ObjectData>();
        this.privateKeyCache = new HashMap<String, PrivateKey>();
        this.storeEncryptionAlgorithm = NISTObjectIdentifiers.id_aes256_CCM;
        this.helper = helper;
    }
    
    private byte[] calculateMac(final byte[] input, final AlgorithmIdentifier algorithmIdentifier, final KeyDerivationFunc keyDerivationFunc, char[] array) throws NoSuchAlgorithmException, IOException, NoSuchProviderException {
        final String id = algorithmIdentifier.getAlgorithm().getId();
        final Mac mac = this.helper.createMac(id);
        Label_0033: {
            if (array != null) {
                break Label_0033;
            }
            try {
                array = new char[0];
                mac.init(new SecretKeySpec(this.generateKey(keyDerivationFunc, "INTEGRITY_CHECK", array, -1), id));
                return mac.doFinal(input);
            }
            catch (InvalidKeyException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Cannot set up MAC calculation: ");
                sb.append(ex.getMessage());
                throw new IOException(sb.toString());
            }
        }
    }
    
    private Cipher createCipher(final String s, final byte[] key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, NoSuchProviderException {
        final Cipher cipher = this.helper.createCipher(s);
        cipher.init(1, new SecretKeySpec(key, "AES"));
        return cipher;
    }
    
    private EncryptedPrivateKeyData createPrivateKeySequence(final EncryptedPrivateKeyInfo encryptedPrivateKeyInfo, final Certificate[] array) throws CertificateEncodingException {
        final org.bouncycastle.asn1.x509.Certificate[] array2 = new org.bouncycastle.asn1.x509.Certificate[array.length];
        for (int i = 0; i != array.length; ++i) {
            array2[i] = org.bouncycastle.asn1.x509.Certificate.getInstance(array[i].getEncoded());
        }
        return new EncryptedPrivateKeyData(encryptedPrivateKeyInfo, array2);
    }
    
    private Certificate decodeCertificate(final Object p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.helper:Lorg/bouncycastle/jcajce/util/JcaJceHelper;
        //     4: astore_2       
        //     5: aload_2        
        //     6: ifnull          38
        //     9: aload_2        
        //    10: ldc_w           "X.509"
        //    13: invokeinterface org/bouncycastle/jcajce/util/JcaJceHelper.createCertificateFactory:(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;
        //    18: new             Ljava/io/ByteArrayInputStream;
        //    21: dup            
        //    22: aload_1        
        //    23: invokestatic    org/bouncycastle/asn1/x509/Certificate.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/Certificate;
        //    26: invokevirtual   org/bouncycastle/asn1/x509/Certificate.getEncoded:()[B
        //    29: invokespecial   java/io/ByteArrayInputStream.<init>:([B)V
        //    32: invokevirtual   java/security/cert/CertificateFactory.generateCertificate:(Ljava/io/InputStream;)Ljava/security/cert/Certificate;
        //    35: astore_1       
        //    36: aload_1        
        //    37: areturn        
        //    38: ldc_w           "X.509"
        //    41: invokestatic    java/security/cert/CertificateFactory.getInstance:(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;
        //    44: new             Ljava/io/ByteArrayInputStream;
        //    47: dup            
        //    48: aload_1        
        //    49: invokestatic    org/bouncycastle/asn1/x509/Certificate.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/Certificate;
        //    52: invokevirtual   org/bouncycastle/asn1/x509/Certificate.getEncoded:()[B
        //    55: invokespecial   java/io/ByteArrayInputStream.<init>:([B)V
        //    58: invokevirtual   java/security/cert/CertificateFactory.generateCertificate:(Ljava/io/InputStream;)Ljava/security/cert/Certificate;
        //    61: astore_1       
        //    62: aload_1        
        //    63: areturn        
        //    64: astore_1       
        //    65: aconst_null    
        //    66: areturn        
        //    67: astore_1       
        //    68: aconst_null    
        //    69: areturn        
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  9      36     64     67     Ljava/lang/Exception;
        //  38     62     67     70     Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0038:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private byte[] decryptData(final String s, final AlgorithmIdentifier algorithmIdentifier, char[] array, final byte[] input) throws IOException {
        if (algorithmIdentifier.getAlgorithm().equals(PKCSObjectIdentifiers.id_PBES2)) {
            final PBES2Parameters instance = PBES2Parameters.getInstance(algorithmIdentifier.getParameters());
            final EncryptionScheme encryptionScheme = instance.getEncryptionScheme();
            try {
                Cipher cipher;
                AlgorithmParameters algorithmParameters;
                if (encryptionScheme.getAlgorithm().equals(NISTObjectIdentifiers.id_aes256_CCM)) {
                    cipher = this.helper.createCipher("AES/CCM/NoPadding");
                    algorithmParameters = this.helper.createAlgorithmParameters("CCM");
                    algorithmParameters.init(CCMParameters.getInstance(encryptionScheme.getParameters()).getEncoded());
                }
                else {
                    if (!encryptionScheme.getAlgorithm().equals(NISTObjectIdentifiers.id_aes256_wrap_pad)) {
                        throw new IOException("BCFKS KeyStore cannot recognize protection encryption algorithm.");
                    }
                    cipher = this.helper.createCipher("AESKWP");
                    algorithmParameters = null;
                }
                final KeyDerivationFunc keyDerivationFunc = instance.getKeyDerivationFunc();
                if (array == null) {
                    array = new char[0];
                }
                cipher.init(2, new SecretKeySpec(this.generateKey(keyDerivationFunc, s, array, 32), "AES"), algorithmParameters);
                return cipher.doFinal(input);
            }
            catch (Exception ex) {
                throw new IOException(ex.toString());
            }
            catch (IOException ex2) {
                throw ex2;
            }
        }
        throw new IOException("BCFKS KeyStore cannot recognize protection algorithm.");
    }
    
    private Date extractCreationDate(final ObjectData objectData, final Date date) {
        try {
            return objectData.getCreationDate().getDate();
        }
        catch (ParseException ex) {
            return date;
        }
    }
    
    private char[] extractPassword(final KeyStore.LoadStoreParameter loadStoreParameter) throws IOException {
        final KeyStore.ProtectionParameter protectionParameter = loadStoreParameter.getProtectionParameter();
        if (protectionParameter == null) {
            return null;
        }
        if (protectionParameter instanceof KeyStore.PasswordProtection) {
            return ((KeyStore.PasswordProtection)protectionParameter).getPassword();
        }
        if (protectionParameter instanceof KeyStore.CallbackHandlerProtection) {
            final CallbackHandler callbackHandler = ((KeyStore.CallbackHandlerProtection)protectionParameter).getCallbackHandler();
            final PasswordCallback passwordCallback = new PasswordCallback("password: ", false);
            try {
                callbackHandler.handle(new Callback[] { passwordCallback });
                return passwordCallback.getPassword();
            }
            catch (UnsupportedCallbackException cause) {
                final StringBuilder sb = new StringBuilder();
                sb.append("PasswordCallback not recognised: ");
                sb.append(cause.getMessage());
                throw new IllegalArgumentException(sb.toString(), cause);
            }
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("no support for protection parameter of type ");
        sb2.append(((KeyStore.CallbackHandlerProtection)protectionParameter).getClass().getName());
        throw new IllegalArgumentException(sb2.toString());
    }
    
    private byte[] generateKey(final KeyDerivationFunc keyDerivationFunc, final String s, final char[] array, int n) throws IOException {
        final byte[] pkcs12PasswordToBytes = PBEParametersGenerator.PKCS12PasswordToBytes(array);
        final byte[] pkcs12PasswordToBytes2 = PBEParametersGenerator.PKCS12PasswordToBytes(s.toCharArray());
        if (MiscObjectIdentifiers.id_scrypt.equals(keyDerivationFunc.getAlgorithm())) {
            final ScryptParams instance = ScryptParams.getInstance(keyDerivationFunc.getParameters());
            if (instance.getKeyLength() != null) {
                n = instance.getKeyLength().intValue();
            }
            else if (n == -1) {
                throw new IOException("no keyLength found in ScryptParams");
            }
            return SCrypt.generate(Arrays.concatenate(pkcs12PasswordToBytes, pkcs12PasswordToBytes2), instance.getSalt(), instance.getCostParameter().intValue(), instance.getBlockSize().intValue(), instance.getBlockSize().intValue(), n);
        }
        if (!keyDerivationFunc.getAlgorithm().equals(PKCSObjectIdentifiers.id_PBKDF2)) {
            throw new IOException("BCFKS KeyStore: unrecognized MAC PBKD.");
        }
        final PBKDF2Params instance2 = PBKDF2Params.getInstance(keyDerivationFunc.getParameters());
        if (instance2.getKeyLength() != null) {
            n = instance2.getKeyLength().intValue();
        }
        else if (n == -1) {
            throw new IOException("no keyLength found in PBKDF2Params");
        }
        if (instance2.getPrf().getAlgorithm().equals(PKCSObjectIdentifiers.id_hmacWithSHA512)) {
            final PKCS5S2ParametersGenerator pkcs5S2ParametersGenerator = new PKCS5S2ParametersGenerator(new SHA512Digest());
            pkcs5S2ParametersGenerator.init(Arrays.concatenate(pkcs12PasswordToBytes, pkcs12PasswordToBytes2), instance2.getSalt(), instance2.getIterationCount().intValue());
            return ((KeyParameter)pkcs5S2ParametersGenerator.generateDerivedParameters(n * 8)).getKey();
        }
        if (instance2.getPrf().getAlgorithm().equals(NISTObjectIdentifiers.id_hmacWithSHA3_512)) {
            final PKCS5S2ParametersGenerator pkcs5S2ParametersGenerator2 = new PKCS5S2ParametersGenerator(new SHA3Digest(512));
            pkcs5S2ParametersGenerator2.init(Arrays.concatenate(pkcs12PasswordToBytes, pkcs12PasswordToBytes2), instance2.getSalt(), instance2.getIterationCount().intValue());
            return ((KeyParameter)pkcs5S2ParametersGenerator2.generateDerivedParameters(n * 8)).getKey();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("BCFKS KeyStore: unrecognized MAC PBKD PRF: ");
        sb.append(instance2.getPrf().getAlgorithm());
        throw new IOException(sb.toString());
    }
    
    private KeyDerivationFunc generatePkbdAlgorithmIdentifier(final ASN1ObjectIdentifier obj, final int n) {
        final byte[] bytes = new byte[64];
        this.getDefaultSecureRandom().nextBytes(bytes);
        if (PKCSObjectIdentifiers.id_PBKDF2.equals(obj)) {
            return new KeyDerivationFunc(PKCSObjectIdentifiers.id_PBKDF2, new PBKDF2Params(bytes, 51200, n, new AlgorithmIdentifier(PKCSObjectIdentifiers.id_hmacWithSHA512, DERNull.INSTANCE)));
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown derivation algorithm: ");
        sb.append(obj);
        throw new IllegalStateException(sb.toString());
    }
    
    private KeyDerivationFunc generatePkbdAlgorithmIdentifier(final KeyDerivationFunc keyDerivationFunc, final int n) {
        final boolean equals = MiscObjectIdentifiers.id_scrypt.equals(keyDerivationFunc.getAlgorithm());
        final ASN1Encodable parameters = keyDerivationFunc.getParameters();
        if (equals) {
            final ScryptParams instance = ScryptParams.getInstance(parameters);
            final byte[] bytes = new byte[instance.getSalt().length];
            this.getDefaultSecureRandom().nextBytes(bytes);
            return new KeyDerivationFunc(MiscObjectIdentifiers.id_scrypt, new ScryptParams(bytes, instance.getCostParameter(), instance.getBlockSize(), instance.getParallelizationParameter(), BigInteger.valueOf(n)));
        }
        final PBKDF2Params instance2 = PBKDF2Params.getInstance(parameters);
        final byte[] bytes2 = new byte[instance2.getSalt().length];
        this.getDefaultSecureRandom().nextBytes(bytes2);
        return new KeyDerivationFunc(PKCSObjectIdentifiers.id_PBKDF2, new PBKDF2Params(bytes2, instance2.getIterationCount().intValue(), n, instance2.getPrf()));
    }
    
    private KeyDerivationFunc generatePkbdAlgorithmIdentifier(final PBKDFConfig pbkdfConfig, final int n) {
        if (MiscObjectIdentifiers.id_scrypt.equals(pbkdfConfig.getAlgorithm())) {
            final ScryptConfig scryptConfig = (ScryptConfig)pbkdfConfig;
            final byte[] bytes = new byte[scryptConfig.getSaltLength()];
            this.getDefaultSecureRandom().nextBytes(bytes);
            return new KeyDerivationFunc(MiscObjectIdentifiers.id_scrypt, new ScryptParams(bytes, scryptConfig.getCostParameter(), scryptConfig.getBlockSize(), scryptConfig.getParallelizationParameter(), n));
        }
        final PBKDF2Config pbkdf2Config = (PBKDF2Config)pbkdfConfig;
        final byte[] bytes2 = new byte[pbkdf2Config.getSaltLength()];
        this.getDefaultSecureRandom().nextBytes(bytes2);
        return new KeyDerivationFunc(PKCSObjectIdentifiers.id_PBKDF2, new PBKDF2Params(bytes2, pbkdf2Config.getIterationCount(), n, pbkdf2Config.getPRF()));
    }
    
    private AlgorithmIdentifier generateSignatureAlgId(final Key key, final BCFKSLoadStoreParameter.SignatureAlgorithm signatureAlgorithm) throws IOException {
        if (key == null) {
            return null;
        }
        if (key instanceof ECKey) {
            if (signatureAlgorithm == BCFKSLoadStoreParameter.SignatureAlgorithm.SHA512withECDSA) {
                return new AlgorithmIdentifier(X9ObjectIdentifiers.ecdsa_with_SHA512);
            }
            if (signatureAlgorithm == BCFKSLoadStoreParameter.SignatureAlgorithm.SHA3_512withECDSA) {
                return new AlgorithmIdentifier(NISTObjectIdentifiers.id_ecdsa_with_sha3_512);
            }
        }
        if (key instanceof DSAKey) {
            if (signatureAlgorithm == BCFKSLoadStoreParameter.SignatureAlgorithm.SHA512withDSA) {
                return new AlgorithmIdentifier(NISTObjectIdentifiers.dsa_with_sha512);
            }
            if (signatureAlgorithm == BCFKSLoadStoreParameter.SignatureAlgorithm.SHA3_512withDSA) {
                return new AlgorithmIdentifier(NISTObjectIdentifiers.id_dsa_with_sha3_512);
            }
        }
        if (key instanceof RSAKey) {
            if (signatureAlgorithm == BCFKSLoadStoreParameter.SignatureAlgorithm.SHA512withRSA) {
                return new AlgorithmIdentifier(PKCSObjectIdentifiers.sha512WithRSAEncryption, DERNull.INSTANCE);
            }
            if (signatureAlgorithm == BCFKSLoadStoreParameter.SignatureAlgorithm.SHA3_512withRSA) {
                return new AlgorithmIdentifier(NISTObjectIdentifiers.id_rsassa_pkcs1_v1_5_with_sha3_512, DERNull.INSTANCE);
            }
        }
        throw new IOException("unknown signature algorithm");
    }
    
    private SecureRandom getDefaultSecureRandom() {
        return CryptoServicesRegistrar.getSecureRandom();
    }
    
    private EncryptedObjectStoreData getEncryptedObjectStoreData(final AlgorithmIdentifier algorithmIdentifier, char[] array) throws IOException, NoSuchAlgorithmException {
        final ObjectData[] array2 = this.entries.values().toArray(new ObjectData[this.entries.size()]);
        final KeyDerivationFunc generatePkbdAlgorithmIdentifier = this.generatePkbdAlgorithmIdentifier(this.hmacPkbdAlgorithm, 32);
        if (array == null) {
            array = new char[0];
        }
        final byte[] generateKey = this.generateKey(generatePkbdAlgorithmIdentifier, "STORE_ENCRYPTION", array, 32);
        final ObjectStoreData objectStoreData = new ObjectStoreData(algorithmIdentifier, this.creationDate, this.lastModifiedDate, new ObjectDataSequence(array2), null);
        try {
            if (this.storeEncryptionAlgorithm.equals(NISTObjectIdentifiers.id_aes256_CCM)) {
                final Cipher cipher = this.createCipher("AES/CCM/NoPadding", generateKey);
                return new EncryptedObjectStoreData(new AlgorithmIdentifier(PKCSObjectIdentifiers.id_PBES2, new PBES2Parameters(generatePkbdAlgorithmIdentifier, new EncryptionScheme(NISTObjectIdentifiers.id_aes256_CCM, CCMParameters.getInstance(cipher.getParameters().getEncoded())))), cipher.doFinal(objectStoreData.getEncoded()));
            }
            return new EncryptedObjectStoreData(new AlgorithmIdentifier(PKCSObjectIdentifiers.id_PBES2, new PBES2Parameters(generatePkbdAlgorithmIdentifier, new EncryptionScheme(NISTObjectIdentifiers.id_aes256_wrap_pad))), this.createCipher("AESKWP", generateKey).doFinal(objectStoreData.getEncoded()));
        }
        catch (NoSuchProviderException ex) {
            throw new IOException(ex.toString());
        }
        catch (InvalidKeyException ex2) {
            throw new IOException(ex2.toString());
        }
        catch (IllegalBlockSizeException ex3) {
            throw new IOException(ex3.toString());
        }
        catch (BadPaddingException ex4) {
            throw new IOException(ex4.toString());
        }
        catch (NoSuchPaddingException ex5) {
            throw new NoSuchAlgorithmException(ex5.toString());
        }
    }
    
    private static String getPublicKeyAlg(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        final String s = BcFKSKeyStoreSpi.publicAlgMap.get(asn1ObjectIdentifier);
        if (s != null) {
            return s;
        }
        return asn1ObjectIdentifier.getId();
    }
    
    private boolean isSimilarHmacPbkd(final PBKDFConfig pbkdfConfig, final KeyDerivationFunc keyDerivationFunc) {
        if (!pbkdfConfig.getAlgorithm().equals(keyDerivationFunc.getAlgorithm())) {
            return false;
        }
        if (MiscObjectIdentifiers.id_scrypt.equals(keyDerivationFunc.getAlgorithm())) {
            if (!(pbkdfConfig instanceof ScryptConfig)) {
                return false;
            }
            final ScryptConfig scryptConfig = (ScryptConfig)pbkdfConfig;
            final ScryptParams instance = ScryptParams.getInstance(keyDerivationFunc.getParameters());
            if (scryptConfig.getSaltLength() != instance.getSalt().length || scryptConfig.getBlockSize() != instance.getBlockSize().intValue() || scryptConfig.getCostParameter() != instance.getCostParameter().intValue() || scryptConfig.getParallelizationParameter() != instance.getParallelizationParameter().intValue()) {
                return false;
            }
        }
        else {
            if (!(pbkdfConfig instanceof PBKDF2Config)) {
                return false;
            }
            final PBKDF2Config pbkdf2Config = (PBKDF2Config)pbkdfConfig;
            final PBKDF2Params instance2 = PBKDF2Params.getInstance(keyDerivationFunc.getParameters());
            if (pbkdf2Config.getSaltLength() != instance2.getSalt().length) {
                return false;
            }
            if (pbkdf2Config.getIterationCount() != instance2.getIterationCount().intValue()) {
                return false;
            }
        }
        return true;
    }
    
    private void verifyMac(final byte[] array, final PbkdMacIntegrityCheck pbkdMacIntegrityCheck, final char[] array2) throws NoSuchAlgorithmException, IOException, NoSuchProviderException {
        if (Arrays.constantTimeAreEqual(this.calculateMac(array, pbkdMacIntegrityCheck.getMacAlgorithm(), pbkdMacIntegrityCheck.getPbkdAlgorithm(), array2), pbkdMacIntegrityCheck.getMac())) {
            return;
        }
        throw new IOException("BCFKS KeyStore corrupted: MAC calculation failed");
    }
    
    private void verifySig(final ASN1Encodable asn1Encodable, final SignatureCheck signatureCheck, final PublicKey publicKey) throws GeneralSecurityException, IOException {
        final Signature signature = this.helper.createSignature(signatureCheck.getSignatureAlgorithm().getAlgorithm().getId());
        signature.initVerify(publicKey);
        signature.update(asn1Encodable.toASN1Primitive().getEncoded("DER"));
        if (signature.verify(signatureCheck.getSignature().getOctets())) {
            return;
        }
        throw new IOException("BCFKS KeyStore corrupted: signature calculation failed");
    }
    
    @Override
    public Enumeration<String> engineAliases() {
        return (Enumeration<String>)new Enumeration() {
            final /* synthetic */ Iterator val$it = new HashSet(BcFKSKeyStoreSpi.this.entries.keySet()).iterator();
            
            @Override
            public boolean hasMoreElements() {
                return this.val$it.hasNext();
            }
            
            @Override
            public Object nextElement() {
                return this.val$it.next();
            }
        };
    }
    
    @Override
    public boolean engineContainsAlias(final String s) {
        if (s != null) {
            return this.entries.containsKey(s);
        }
        throw new NullPointerException("alias value is null");
    }
    
    @Override
    public void engineDeleteEntry(final String s) throws KeyStoreException {
        if (this.entries.get(s) == null) {
            return;
        }
        this.privateKeyCache.remove(s);
        this.entries.remove(s);
        this.lastModifiedDate = new Date();
    }
    
    @Override
    public Certificate engineGetCertificate(final String s) {
        final ObjectData objectData = this.entries.get(s);
        if (objectData != null) {
            if (objectData.getType().equals(BcFKSKeyStoreSpi.PRIVATE_KEY) || objectData.getType().equals(BcFKSKeyStoreSpi.PROTECTED_PRIVATE_KEY)) {
                return this.decodeCertificate(EncryptedPrivateKeyData.getInstance(objectData.getData()).getCertificateChain()[0]);
            }
            if (objectData.getType().equals(BcFKSKeyStoreSpi.CERTIFICATE)) {
                return this.decodeCertificate(objectData.getData());
            }
        }
        return null;
    }
    
    @Override
    public String engineGetCertificateAlias(final Certificate p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: ifnonnull       6
        //     4: aconst_null    
        //     5: areturn        
        //     6: aload_1        
        //     7: invokevirtual   java/security/cert/Certificate.getEncoded:()[B
        //    10: astore_1       
        //    11: aload_0        
        //    12: getfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.entries:Ljava/util/Map;
        //    15: invokeinterface java/util/Map.keySet:()Ljava/util/Set;
        //    20: invokeinterface java/util/Set.iterator:()Ljava/util/Iterator;
        //    25: astore_3       
        //    26: aload_3        
        //    27: invokeinterface java/util/Iterator.hasNext:()Z
        //    32: ifeq            150
        //    35: aload_3        
        //    36: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //    41: checkcast       Ljava/lang/String;
        //    44: astore          4
        //    46: aload_0        
        //    47: getfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.entries:Ljava/util/Map;
        //    50: aload           4
        //    52: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    57: checkcast       Lorg/bouncycastle/asn1/bc/ObjectData;
        //    60: astore          5
        //    62: aload           5
        //    64: invokevirtual   org/bouncycastle/asn1/bc/ObjectData.getType:()Ljava/math/BigInteger;
        //    67: getstatic       org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.CERTIFICATE:Ljava/math/BigInteger;
        //    70: invokevirtual   java/math/BigInteger.equals:(Ljava/lang/Object;)Z
        //    73: ifeq            91
        //    76: aload           5
        //    78: invokevirtual   org/bouncycastle/asn1/bc/ObjectData.getData:()[B
        //    81: aload_1        
        //    82: invokestatic    org/bouncycastle/util/Arrays.areEqual:([B[B)Z
        //    85: ifeq            26
        //    88: aload           4
        //    90: areturn        
        //    91: aload           5
        //    93: invokevirtual   org/bouncycastle/asn1/bc/ObjectData.getType:()Ljava/math/BigInteger;
        //    96: getstatic       org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.PRIVATE_KEY:Ljava/math/BigInteger;
        //    99: invokevirtual   java/math/BigInteger.equals:(Ljava/lang/Object;)Z
        //   102: ifne            119
        //   105: aload           5
        //   107: invokevirtual   org/bouncycastle/asn1/bc/ObjectData.getType:()Ljava/math/BigInteger;
        //   110: getstatic       org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.PROTECTED_PRIVATE_KEY:Ljava/math/BigInteger;
        //   113: invokevirtual   java/math/BigInteger.equals:(Ljava/lang/Object;)Z
        //   116: ifeq            26
        //   119: aload           5
        //   121: invokevirtual   org/bouncycastle/asn1/bc/ObjectData.getData:()[B
        //   124: invokestatic    org/bouncycastle/asn1/bc/EncryptedPrivateKeyData.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/bc/EncryptedPrivateKeyData;
        //   127: invokevirtual   org/bouncycastle/asn1/bc/EncryptedPrivateKeyData.getCertificateChain:()[Lorg/bouncycastle/asn1/x509/Certificate;
        //   130: iconst_0       
        //   131: aaload         
        //   132: invokevirtual   org/bouncycastle/asn1/x509/Certificate.toASN1Primitive:()Lorg/bouncycastle/asn1/ASN1Primitive;
        //   135: invokevirtual   org/bouncycastle/asn1/ASN1Primitive.getEncoded:()[B
        //   138: aload_1        
        //   139: invokestatic    org/bouncycastle/util/Arrays.areEqual:([B[B)Z
        //   142: istore_2       
        //   143: iload_2        
        //   144: ifeq            26
        //   147: aload           4
        //   149: areturn        
        //   150: aconst_null    
        //   151: areturn        
        //   152: astore_1       
        //   153: aconst_null    
        //   154: areturn        
        //   155: astore          4
        //   157: goto            26
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                             
        //  -----  -----  -----  -----  -------------------------------------------------
        //  6      11     152    155    Ljava/security/cert/CertificateEncodingException;
        //  119    143    155    160    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0119:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public Certificate[] engineGetCertificateChain(final String s) {
        final ObjectData objectData = this.entries.get(s);
        if (objectData != null && (objectData.getType().equals(BcFKSKeyStoreSpi.PRIVATE_KEY) || objectData.getType().equals(BcFKSKeyStoreSpi.PROTECTED_PRIVATE_KEY))) {
            final org.bouncycastle.asn1.x509.Certificate[] certificateChain = EncryptedPrivateKeyData.getInstance(objectData.getData()).getCertificateChain();
            final X509Certificate[] array = new X509Certificate[certificateChain.length];
            for (int i = 0; i != array.length; ++i) {
                array[i] = (X509Certificate)this.decodeCertificate(certificateChain[i]);
            }
            return array;
        }
        return null;
    }
    
    @Override
    public Date engineGetCreationDate(final String s) {
        final ObjectData objectData = this.entries.get(s);
        Label_0036: {
            if (objectData == null) {
                break Label_0036;
            }
            while (true) {
                while (true) {
                    try {
                        return objectData.getLastModifiedDate().getDate();
                        return new Date();
                        return null;
                    }
                    catch (ParseException ex) {}
                    continue;
                }
            }
        }
    }
    
    @Override
    public Key engineGetKey(final String str, final char[] array) throws NoSuchAlgorithmException, UnrecoverableKeyException {
        final ObjectData objectData = this.entries.get(str);
        if (objectData != null) {
            if (!objectData.getType().equals(BcFKSKeyStoreSpi.PRIVATE_KEY)) {
                if (!objectData.getType().equals(BcFKSKeyStoreSpi.PROTECTED_PRIVATE_KEY)) {
                    if (!objectData.getType().equals(BcFKSKeyStoreSpi.SECRET_KEY) && !objectData.getType().equals(BcFKSKeyStoreSpi.PROTECTED_SECRET_KEY)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("BCFKS KeyStore unable to recover secret key (");
                        sb.append(str);
                        sb.append("): type not recognized");
                        throw new UnrecoverableKeyException(sb.toString());
                    }
                    final EncryptedSecretKeyData instance = EncryptedSecretKeyData.getInstance(objectData.getData());
                    try {
                        final SecretKeyData instance2 = SecretKeyData.getInstance(this.decryptData("SECRET_KEY_ENCRYPTION", instance.getKeyEncryptionAlgorithm(), array, instance.getEncryptedKeyData()));
                        return this.helper.createSecretKeyFactory(instance2.getKeyAlgorithm().getId()).generateSecret(new SecretKeySpec(instance2.getKeyBytes(), instance2.getKeyAlgorithm().getId()));
                    }
                    catch (Exception ex) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("BCFKS KeyStore unable to recover secret key (");
                        sb2.append(str);
                        sb2.append("): ");
                        sb2.append(ex.getMessage());
                        throw new UnrecoverableKeyException(sb2.toString());
                    }
                }
            }
            final PrivateKey privateKey = this.privateKeyCache.get(str);
            if (privateKey != null) {
                return privateKey;
            }
            final EncryptedPrivateKeyInfo instance3 = EncryptedPrivateKeyInfo.getInstance(EncryptedPrivateKeyData.getInstance(objectData.getData()).getEncryptedPrivateKeyInfo());
            try {
                final PrivateKeyInfo instance4 = PrivateKeyInfo.getInstance(this.decryptData("PRIVATE_KEY_ENCRYPTION", instance3.getEncryptionAlgorithm(), array, instance3.getEncryptedData()));
                final PrivateKey generatePrivate = this.helper.createKeyFactory(getPublicKeyAlg(instance4.getPrivateKeyAlgorithm().getAlgorithm())).generatePrivate(new PKCS8EncodedKeySpec(instance4.getEncoded()));
                this.privateKeyCache.put(str, generatePrivate);
                return generatePrivate;
            }
            catch (Exception ex2) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("BCFKS KeyStore unable to recover private key (");
                sb3.append(str);
                sb3.append("): ");
                sb3.append(ex2.getMessage());
                throw new UnrecoverableKeyException(sb3.toString());
            }
        }
        return null;
    }
    
    @Override
    public boolean engineIsCertificateEntry(final String s) {
        final ObjectData objectData = this.entries.get(s);
        return objectData != null && objectData.getType().equals(BcFKSKeyStoreSpi.CERTIFICATE);
    }
    
    @Override
    public boolean engineIsKeyEntry(final String s) {
        final ObjectData objectData = this.entries.get(s);
        boolean b = false;
        if (objectData != null) {
            final BigInteger type = objectData.getType();
            if (!type.equals(BcFKSKeyStoreSpi.PRIVATE_KEY) && !type.equals(BcFKSKeyStoreSpi.SECRET_KEY) && !type.equals(BcFKSKeyStoreSpi.PROTECTED_PRIVATE_KEY)) {
                b = b;
                if (!type.equals(BcFKSKeyStoreSpi.PROTECTED_SECRET_KEY)) {
                    return b;
                }
            }
            b = true;
        }
        return b;
    }
    
    @Override
    public void engineLoad(final InputStream p0, final char[] p1) throws IOException, NoSuchAlgorithmException, CertificateException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.entries:Ljava/util/Map;
        //     4: invokeinterface java/util/Map.clear:()V
        //     9: aload_0        
        //    10: getfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.privateKeyCache:Ljava/util/Map;
        //    13: invokeinterface java/util/Map.clear:()V
        //    18: aload_0        
        //    19: aconst_null    
        //    20: putfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.creationDate:Ljava/util/Date;
        //    23: aload_0        
        //    24: aconst_null    
        //    25: putfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.lastModifiedDate:Ljava/util/Date;
        //    28: aload_0        
        //    29: aconst_null    
        //    30: putfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.hmacAlgorithm:Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //    33: aload_1        
        //    34: ifnonnull       96
        //    37: new             Ljava/util/Date;
        //    40: dup            
        //    41: invokespecial   java/util/Date.<init>:()V
        //    44: astore_1       
        //    45: aload_0        
        //    46: aload_1        
        //    47: putfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.creationDate:Ljava/util/Date;
        //    50: aload_0        
        //    51: aload_1        
        //    52: putfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.lastModifiedDate:Ljava/util/Date;
        //    55: aload_0        
        //    56: aconst_null    
        //    57: putfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.verificationKey:Ljava/security/PublicKey;
        //    60: aload_0        
        //    61: aconst_null    
        //    62: putfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.validator:Lorg/bouncycastle/jcajce/BCFKSLoadStoreParameter$CertChainValidator;
        //    65: aload_0        
        //    66: new             Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //    69: dup            
        //    70: getstatic       org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers.id_hmacWithSHA512:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //    73: getstatic       org/bouncycastle/asn1/DERNull.INSTANCE:Lorg/bouncycastle/asn1/DERNull;
        //    76: invokespecial   org/bouncycastle/asn1/x509/AlgorithmIdentifier.<init>:(Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;Lorg/bouncycastle/asn1/ASN1Encodable;)V
        //    79: putfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.hmacAlgorithm:Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //    82: aload_0        
        //    83: aload_0        
        //    84: getstatic       org/bouncycastle/asn1/pkcs/PKCSObjectIdentifiers.id_PBKDF2:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //    87: bipush          64
        //    89: invokespecial   org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.generatePkbdAlgorithmIdentifier:(Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;I)Lorg/bouncycastle/asn1/pkcs/KeyDerivationFunc;
        //    92: putfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.hmacPkbdAlgorithm:Lorg/bouncycastle/asn1/pkcs/KeyDerivationFunc;
        //    95: return         
        //    96: new             Lorg/bouncycastle/asn1/ASN1InputStream;
        //    99: dup            
        //   100: aload_1        
        //   101: invokespecial   org/bouncycastle/asn1/ASN1InputStream.<init>:(Ljava/io/InputStream;)V
        //   104: astore_1       
        //   105: aload_1        
        //   106: invokevirtual   org/bouncycastle/asn1/ASN1InputStream.readObject:()Lorg/bouncycastle/asn1/ASN1Primitive;
        //   109: invokestatic    org/bouncycastle/asn1/bc/ObjectStore.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/bc/ObjectStore;
        //   112: astore          6
        //   114: aload           6
        //   116: invokevirtual   org/bouncycastle/asn1/bc/ObjectStore.getIntegrityCheck:()Lorg/bouncycastle/asn1/bc/ObjectStoreIntegrityCheck;
        //   119: astore_1       
        //   120: aload_1        
        //   121: invokevirtual   org/bouncycastle/asn1/bc/ObjectStoreIntegrityCheck.getType:()I
        //   124: ifne            195
        //   127: aload_1        
        //   128: invokevirtual   org/bouncycastle/asn1/bc/ObjectStoreIntegrityCheck.getIntegrityCheck:()Lorg/bouncycastle/asn1/ASN1Object;
        //   131: invokestatic    org/bouncycastle/asn1/bc/PbkdMacIntegrityCheck.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/bc/PbkdMacIntegrityCheck;
        //   134: astore          4
        //   136: aload_0        
        //   137: aload           4
        //   139: invokevirtual   org/bouncycastle/asn1/bc/PbkdMacIntegrityCheck.getMacAlgorithm:()Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //   142: putfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.hmacAlgorithm:Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //   145: aload_0        
        //   146: aload           4
        //   148: invokevirtual   org/bouncycastle/asn1/bc/PbkdMacIntegrityCheck.getPbkdAlgorithm:()Lorg/bouncycastle/asn1/pkcs/KeyDerivationFunc;
        //   151: putfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.hmacPkbdAlgorithm:Lorg/bouncycastle/asn1/pkcs/KeyDerivationFunc;
        //   154: aload_0        
        //   155: getfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.hmacAlgorithm:Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //   158: astore_1       
        //   159: aload_0        
        //   160: aload           6
        //   162: invokevirtual   org/bouncycastle/asn1/bc/ObjectStore.getStoreData:()Lorg/bouncycastle/asn1/ASN1Encodable;
        //   165: invokeinterface org/bouncycastle/asn1/ASN1Encodable.toASN1Primitive:()Lorg/bouncycastle/asn1/ASN1Primitive;
        //   170: invokevirtual   org/bouncycastle/asn1/ASN1Primitive.getEncoded:()[B
        //   173: aload           4
        //   175: aload_2        
        //   176: invokespecial   org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.verifyMac:([BLorg/bouncycastle/asn1/bc/PbkdMacIntegrityCheck;[C)V
        //   179: goto            379
        //   182: astore_1       
        //   183: new             Ljava/io/IOException;
        //   186: dup            
        //   187: aload_1        
        //   188: invokevirtual   java/security/NoSuchProviderException.getMessage:()Ljava/lang/String;
        //   191: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   194: athrow         
        //   195: aload_1        
        //   196: invokevirtual   org/bouncycastle/asn1/bc/ObjectStoreIntegrityCheck.getType:()I
        //   199: iconst_1       
        //   200: if_icmpne       575
        //   203: aload_1        
        //   204: invokevirtual   org/bouncycastle/asn1/bc/ObjectStoreIntegrityCheck.getIntegrityCheck:()Lorg/bouncycastle/asn1/ASN1Object;
        //   207: invokestatic    org/bouncycastle/asn1/bc/SignatureCheck.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/bc/SignatureCheck;
        //   210: astore          7
        //   212: aload           7
        //   214: invokevirtual   org/bouncycastle/asn1/bc/SignatureCheck.getSignatureAlgorithm:()Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //   217: astore          5
        //   219: aload           7
        //   221: invokevirtual   org/bouncycastle/asn1/bc/SignatureCheck.getCertificates:()[Lorg/bouncycastle/asn1/x509/Certificate;
        //   224: astore_1       
        //   225: aload_0        
        //   226: getfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.validator:Lorg/bouncycastle/jcajce/BCFKSLoadStoreParameter$CertChainValidator;
        //   229: ifnull          364
        //   232: aload_1        
        //   233: ifnull          353
        //   236: aload_0        
        //   237: getfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.helper:Lorg/bouncycastle/jcajce/util/JcaJceHelper;
        //   240: ldc_w           "X.509"
        //   243: invokeinterface org/bouncycastle/jcajce/util/JcaJceHelper.createCertificateFactory:(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;
        //   248: astore          8
        //   250: aload_1        
        //   251: arraylength    
        //   252: anewarray       Ljava/security/cert/X509Certificate;
        //   255: astore          4
        //   257: iconst_0       
        //   258: istore_3       
        //   259: iload_3        
        //   260: aload           4
        //   262: arraylength    
        //   263: if_icmpeq       298
        //   266: aload           4
        //   268: iload_3        
        //   269: aload           8
        //   271: new             Ljava/io/ByteArrayInputStream;
        //   274: dup            
        //   275: aload_1        
        //   276: iload_3        
        //   277: aaload         
        //   278: invokevirtual   org/bouncycastle/asn1/x509/Certificate.getEncoded:()[B
        //   281: invokespecial   java/io/ByteArrayInputStream.<init>:([B)V
        //   284: invokevirtual   java/security/cert/CertificateFactory.generateCertificate:(Ljava/io/InputStream;)Ljava/security/cert/Certificate;
        //   287: checkcast       Ljava/security/cert/X509Certificate;
        //   290: aastore        
        //   291: iload_3        
        //   292: iconst_1       
        //   293: iadd           
        //   294: istore_3       
        //   295: goto            259
        //   298: aload_0        
        //   299: getfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.validator:Lorg/bouncycastle/jcajce/BCFKSLoadStoreParameter$CertChainValidator;
        //   302: aload           4
        //   304: invokeinterface org/bouncycastle/jcajce/BCFKSLoadStoreParameter$CertChainValidator.isValid:([Ljava/security/cert/X509Certificate;)Z
        //   309: ifeq            342
        //   312: aload           6
        //   314: invokevirtual   org/bouncycastle/asn1/bc/ObjectStore.getStoreData:()Lorg/bouncycastle/asn1/ASN1Encodable;
        //   317: astore_1       
        //   318: aload           4
        //   320: iconst_0       
        //   321: aaload         
        //   322: invokevirtual   java/security/cert/X509Certificate.getPublicKey:()Ljava/security/PublicKey;
        //   325: astore          4
        //   327: aload_0        
        //   328: aload_1        
        //   329: aload           7
        //   331: aload           4
        //   333: invokespecial   org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.verifySig:(Lorg/bouncycastle/asn1/ASN1Encodable;Lorg/bouncycastle/asn1/bc/SignatureCheck;Ljava/security/PublicKey;)V
        //   336: aload           5
        //   338: astore_1       
        //   339: goto            379
        //   342: new             Ljava/io/IOException;
        //   345: dup            
        //   346: ldc_w           "certificate chain in key store signature not valid"
        //   349: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   352: athrow         
        //   353: new             Ljava/io/IOException;
        //   356: dup            
        //   357: ldc_w           "validator specified but no certifcates in store"
        //   360: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   363: athrow         
        //   364: aload           6
        //   366: invokevirtual   org/bouncycastle/asn1/bc/ObjectStore.getStoreData:()Lorg/bouncycastle/asn1/ASN1Encodable;
        //   369: astore_1       
        //   370: aload_0        
        //   371: getfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.verificationKey:Ljava/security/PublicKey;
        //   374: astore          4
        //   376: goto            327
        //   379: aload           6
        //   381: invokevirtual   org/bouncycastle/asn1/bc/ObjectStore.getStoreData:()Lorg/bouncycastle/asn1/ASN1Encodable;
        //   384: astore          4
        //   386: aload           4
        //   388: instanceof      Lorg/bouncycastle/asn1/bc/EncryptedObjectStoreData;
        //   391: ifeq            429
        //   394: aload           4
        //   396: checkcast       Lorg/bouncycastle/asn1/bc/EncryptedObjectStoreData;
        //   399: astore          4
        //   401: aload_0        
        //   402: ldc_w           "STORE_ENCRYPTION"
        //   405: aload           4
        //   407: invokevirtual   org/bouncycastle/asn1/bc/EncryptedObjectStoreData.getEncryptionAlgorithm:()Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //   410: aload_2        
        //   411: aload           4
        //   413: invokevirtual   org/bouncycastle/asn1/bc/EncryptedObjectStoreData.getEncryptedContent:()Lorg/bouncycastle/asn1/ASN1OctetString;
        //   416: invokevirtual   org/bouncycastle/asn1/ASN1OctetString.getOctets:()[B
        //   419: invokespecial   org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.decryptData:(Ljava/lang/String;Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;[C[B)[B
        //   422: invokestatic    org/bouncycastle/asn1/bc/ObjectStoreData.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/bc/ObjectStoreData;
        //   425: astore_2       
        //   426: goto            435
        //   429: aload           4
        //   431: invokestatic    org/bouncycastle/asn1/bc/ObjectStoreData.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/bc/ObjectStoreData;
        //   434: astore_2       
        //   435: aload_0        
        //   436: aload_2        
        //   437: invokevirtual   org/bouncycastle/asn1/bc/ObjectStoreData.getCreationDate:()Lorg/bouncycastle/asn1/ASN1GeneralizedTime;
        //   440: invokevirtual   org/bouncycastle/asn1/ASN1GeneralizedTime.getDate:()Ljava/util/Date;
        //   443: putfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.creationDate:Ljava/util/Date;
        //   446: aload_0        
        //   447: aload_2        
        //   448: invokevirtual   org/bouncycastle/asn1/bc/ObjectStoreData.getLastModifiedDate:()Lorg/bouncycastle/asn1/ASN1GeneralizedTime;
        //   451: invokevirtual   org/bouncycastle/asn1/ASN1GeneralizedTime.getDate:()Ljava/util/Date;
        //   454: putfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.lastModifiedDate:Ljava/util/Date;
        //   457: aload_2        
        //   458: invokevirtual   org/bouncycastle/asn1/bc/ObjectStoreData.getIntegrityAlgorithm:()Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //   461: aload_1        
        //   462: invokevirtual   org/bouncycastle/asn1/x509/AlgorithmIdentifier.equals:(Ljava/lang/Object;)Z
        //   465: ifeq            514
        //   468: aload_2        
        //   469: invokevirtual   org/bouncycastle/asn1/bc/ObjectStoreData.getObjectDataSequence:()Lorg/bouncycastle/asn1/bc/ObjectDataSequence;
        //   472: invokevirtual   org/bouncycastle/asn1/bc/ObjectDataSequence.iterator:()Ljava/util/Iterator;
        //   475: astore_1       
        //   476: aload_1        
        //   477: invokeinterface java/util/Iterator.hasNext:()Z
        //   482: ifeq            513
        //   485: aload_1        
        //   486: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   491: invokestatic    org/bouncycastle/asn1/bc/ObjectData.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/bc/ObjectData;
        //   494: astore_2       
        //   495: aload_0        
        //   496: getfield        org/bouncycastle/jcajce/provider/keystore/bcfks/BcFKSKeyStoreSpi.entries:Ljava/util/Map;
        //   499: aload_2        
        //   500: invokevirtual   org/bouncycastle/asn1/bc/ObjectData.getIdentifier:()Ljava/lang/String;
        //   503: aload_2        
        //   504: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   509: pop            
        //   510: goto            476
        //   513: return         
        //   514: new             Ljava/io/IOException;
        //   517: dup            
        //   518: ldc_w           "BCFKS KeyStore storeData integrity algorithm does not match store integrity algorithm."
        //   521: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   524: athrow         
        //   525: new             Ljava/io/IOException;
        //   528: dup            
        //   529: ldc_w           "BCFKS KeyStore unable to parse store data information."
        //   532: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   535: athrow         
        //   536: astore_1       
        //   537: new             Ljava/lang/StringBuilder;
        //   540: dup            
        //   541: invokespecial   java/lang/StringBuilder.<init>:()V
        //   544: astore_2       
        //   545: aload_2        
        //   546: ldc_w           "error verifying signature: "
        //   549: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   552: pop            
        //   553: aload_2        
        //   554: aload_1        
        //   555: invokevirtual   java/security/GeneralSecurityException.getMessage:()Ljava/lang/String;
        //   558: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   561: pop            
        //   562: new             Ljava/io/IOException;
        //   565: dup            
        //   566: aload_2        
        //   567: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   570: aload_1        
        //   571: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   574: athrow         
        //   575: new             Ljava/io/IOException;
        //   578: dup            
        //   579: ldc_w           "BCFKS KeyStore unable to recognize integrity check."
        //   582: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   585: athrow         
        //   586: astore_1       
        //   587: new             Ljava/io/IOException;
        //   590: dup            
        //   591: aload_1        
        //   592: invokevirtual   java/lang/Exception.getMessage:()Ljava/lang/String;
        //   595: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   598: athrow         
        //   599: astore_1       
        //   600: goto            525
        //    Exceptions:
        //  throws java.io.IOException
        //  throws java.security.NoSuchAlgorithmException
        //  throws java.security.cert.CertificateException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                    
        //  -----  -----  -----  -----  ----------------------------------------
        //  105    114    586    599    Ljava/lang/Exception;
        //  159    179    182    195    Ljava/security/NoSuchProviderException;
        //  219    232    536    575    Ljava/security/GeneralSecurityException;
        //  236    257    536    575    Ljava/security/GeneralSecurityException;
        //  259    291    536    575    Ljava/security/GeneralSecurityException;
        //  298    327    536    575    Ljava/security/GeneralSecurityException;
        //  327    336    536    575    Ljava/security/GeneralSecurityException;
        //  342    353    536    575    Ljava/security/GeneralSecurityException;
        //  353    364    536    575    Ljava/security/GeneralSecurityException;
        //  364    376    536    575    Ljava/security/GeneralSecurityException;
        //  435    457    599    536    Ljava/text/ParseException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0435:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public void engineLoad(final KeyStore.LoadStoreParameter loadStoreParameter) throws CertificateException, NoSuchAlgorithmException, IOException {
        if (loadStoreParameter != null) {
            if (loadStoreParameter instanceof BCFKSLoadStoreParameter) {
                final BCFKSLoadStoreParameter bcfksLoadStoreParameter = (BCFKSLoadStoreParameter)loadStoreParameter;
                final char[] password = this.extractPassword(bcfksLoadStoreParameter);
                this.hmacPkbdAlgorithm = this.generatePkbdAlgorithmIdentifier(bcfksLoadStoreParameter.getStorePBKDFConfig(), 64);
                ASN1ObjectIdentifier storeEncryptionAlgorithm;
                if (bcfksLoadStoreParameter.getStoreEncryptionAlgorithm() == BCFKSLoadStoreParameter.EncryptionAlgorithm.AES256_CCM) {
                    storeEncryptionAlgorithm = NISTObjectIdentifiers.id_aes256_CCM;
                }
                else {
                    storeEncryptionAlgorithm = NISTObjectIdentifiers.id_aes256_wrap_pad;
                }
                this.storeEncryptionAlgorithm = storeEncryptionAlgorithm;
                AlgorithmIdentifier hmacAlgorithm;
                if (bcfksLoadStoreParameter.getStoreMacAlgorithm() == BCFKSLoadStoreParameter.MacAlgorithm.HmacSHA512) {
                    hmacAlgorithm = new AlgorithmIdentifier(PKCSObjectIdentifiers.id_hmacWithSHA512, DERNull.INSTANCE);
                }
                else {
                    hmacAlgorithm = new AlgorithmIdentifier(NISTObjectIdentifiers.id_hmacWithSHA3_512, DERNull.INSTANCE);
                }
                this.hmacAlgorithm = hmacAlgorithm;
                this.verificationKey = (PublicKey)bcfksLoadStoreParameter.getStoreSignatureKey();
                this.validator = bcfksLoadStoreParameter.getCertChainValidator();
                this.signatureAlgorithm = this.generateSignatureAlgId(this.verificationKey, bcfksLoadStoreParameter.getStoreSignatureAlgorithm());
                final AlgorithmIdentifier hmacAlgorithm2 = this.hmacAlgorithm;
                final ASN1ObjectIdentifier storeEncryptionAlgorithm2 = this.storeEncryptionAlgorithm;
                final InputStream inputStream = bcfksLoadStoreParameter.getInputStream();
                this.engineLoad(inputStream, password);
                if (inputStream != null) {
                    if (this.isSimilarHmacPbkd(bcfksLoadStoreParameter.getStorePBKDFConfig(), this.hmacPkbdAlgorithm) && storeEncryptionAlgorithm2.equals(this.storeEncryptionAlgorithm)) {
                        return;
                    }
                    throw new IOException("configuration parameters do not match existing store");
                }
            }
            else {
                if (!(loadStoreParameter instanceof BCLoadStoreParameter)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("no support for 'parameter' of type ");
                    sb.append(loadStoreParameter.getClass().getName());
                    throw new IllegalArgumentException(sb.toString());
                }
                this.engineLoad(((BCLoadStoreParameter)loadStoreParameter).getInputStream(), this.extractPassword(loadStoreParameter));
            }
            return;
        }
        throw new IllegalArgumentException("'parameter' arg cannot be null");
    }
    
    @Override
    public void engineSetCertificateEntry(final String str, final Certificate certificate) throws KeyStoreException {
        final ObjectData objectData = this.entries.get(str);
        final Date lastModifiedDate = new Date();
        Date creationDate;
        if (objectData != null) {
            if (!objectData.getType().equals(BcFKSKeyStoreSpi.CERTIFICATE)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("BCFKS KeyStore already has a key entry with alias ");
                sb.append(str);
                throw new KeyStoreException(sb.toString());
            }
            creationDate = this.extractCreationDate(objectData, lastModifiedDate);
        }
        else {
            creationDate = lastModifiedDate;
        }
        try {
            this.entries.put(str, new ObjectData(BcFKSKeyStoreSpi.CERTIFICATE, str, creationDate, lastModifiedDate, certificate.getEncoded(), null));
            this.lastModifiedDate = lastModifiedDate;
        }
        catch (CertificateEncodingException ex) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("BCFKS KeyStore unable to handle certificate: ");
            sb2.append(ex.getMessage());
            throw new ExtKeyStoreException(sb2.toString(), ex);
        }
    }
    
    @Override
    public void engineSetKeyEntry(final String s, final Key key, char[] array, final Certificate[] array2) throws KeyStoreException {
        final Date lastModifiedDate = new Date();
        final ObjectData objectData = this.entries.get(s);
        Date creationDate;
        if (objectData != null) {
            creationDate = this.extractCreationDate(objectData, lastModifiedDate);
        }
        else {
            creationDate = lastModifiedDate;
        }
        this.privateKeyCache.remove(s);
        Label_0718: {
            if (key instanceof PrivateKey) {
                if (array2 != null) {
                    try {
                        final byte[] encoded = key.getEncoded();
                        final KeyDerivationFunc generatePkbdAlgorithmIdentifier = this.generatePkbdAlgorithmIdentifier(PKCSObjectIdentifiers.id_PBKDF2, 32);
                        if (array == null) {
                            array = new char[0];
                        }
                        final byte[] generateKey = this.generateKey(generatePkbdAlgorithmIdentifier, "PRIVATE_KEY_ENCRYPTION", array, 32);
                        EncryptedPrivateKeyInfo encryptedPrivateKeyInfo;
                        if (this.storeEncryptionAlgorithm.equals(NISTObjectIdentifiers.id_aes256_CCM)) {
                            final Cipher cipher = this.createCipher("AES/CCM/NoPadding", generateKey);
                            encryptedPrivateKeyInfo = new EncryptedPrivateKeyInfo(new AlgorithmIdentifier(PKCSObjectIdentifiers.id_PBES2, new PBES2Parameters(generatePkbdAlgorithmIdentifier, new EncryptionScheme(NISTObjectIdentifiers.id_aes256_CCM, CCMParameters.getInstance(cipher.getParameters().getEncoded())))), cipher.doFinal(encoded));
                        }
                        else {
                            encryptedPrivateKeyInfo = new EncryptedPrivateKeyInfo(new AlgorithmIdentifier(PKCSObjectIdentifiers.id_PBES2, new PBES2Parameters(generatePkbdAlgorithmIdentifier, new EncryptionScheme(NISTObjectIdentifiers.id_aes256_wrap_pad))), this.createCipher("AESKWP", generateKey).doFinal(encoded));
                        }
                        this.entries.put(s, new ObjectData(BcFKSKeyStoreSpi.PRIVATE_KEY, s, creationDate, lastModifiedDate, this.createPrivateKeySequence(encryptedPrivateKeyInfo, array2).getEncoded(), null));
                        break Label_0718;
                    }
                    catch (Exception ex) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("BCFKS KeyStore exception storing private key: ");
                        sb.append(ex.toString());
                        throw new ExtKeyStoreException(sb.toString(), ex);
                    }
                }
                throw new KeyStoreException("BCFKS KeyStore requires a certificate chain for private key storage.");
            }
            if (!(key instanceof SecretKey)) {
                throw new KeyStoreException("BCFKS KeyStore unable to recognize key.");
            }
            if (array2 != null) {
                throw new KeyStoreException("BCFKS KeyStore cannot store certificate chain with secret key.");
            }
            try {
                final byte[] encoded2 = key.getEncoded();
                final KeyDerivationFunc generatePkbdAlgorithmIdentifier2 = this.generatePkbdAlgorithmIdentifier(PKCSObjectIdentifiers.id_PBKDF2, 32);
                if (array == null) {
                    array = new char[0];
                }
                final byte[] generateKey2 = this.generateKey(generatePkbdAlgorithmIdentifier2, "SECRET_KEY_ENCRYPTION", array, 32);
                final String upperCase = Strings.toUpperCase(key.getAlgorithm());
                SecretKeyData secretKeyData;
                if (upperCase.indexOf("AES") > -1) {
                    secretKeyData = new SecretKeyData(NISTObjectIdentifiers.aes, encoded2);
                }
                else {
                    final ASN1ObjectIdentifier asn1ObjectIdentifier = BcFKSKeyStoreSpi.oidMap.get(upperCase);
                    if (asn1ObjectIdentifier != null) {
                        secretKeyData = new SecretKeyData(asn1ObjectIdentifier, encoded2);
                    }
                    else {
                        final Map<String, ASN1ObjectIdentifier> oidMap = BcFKSKeyStoreSpi.oidMap;
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append(upperCase);
                        sb2.append(".");
                        sb2.append(encoded2.length * 8);
                        final ASN1ObjectIdentifier asn1ObjectIdentifier2 = oidMap.get(sb2.toString());
                        if (asn1ObjectIdentifier2 == null) {
                            final StringBuilder sb3 = new StringBuilder();
                            sb3.append("BCFKS KeyStore cannot recognize secret key (");
                            sb3.append(upperCase);
                            sb3.append(") for storage.");
                            throw new KeyStoreException(sb3.toString());
                        }
                        secretKeyData = new SecretKeyData(asn1ObjectIdentifier2, encoded2);
                    }
                }
                EncryptedSecretKeyData encryptedSecretKeyData;
                if (this.storeEncryptionAlgorithm.equals(NISTObjectIdentifiers.id_aes256_CCM)) {
                    final Cipher cipher2 = this.createCipher("AES/CCM/NoPadding", generateKey2);
                    encryptedSecretKeyData = new EncryptedSecretKeyData(new AlgorithmIdentifier(PKCSObjectIdentifiers.id_PBES2, new PBES2Parameters(generatePkbdAlgorithmIdentifier2, new EncryptionScheme(NISTObjectIdentifiers.id_aes256_CCM, CCMParameters.getInstance(cipher2.getParameters().getEncoded())))), cipher2.doFinal(secretKeyData.getEncoded()));
                }
                else {
                    encryptedSecretKeyData = new EncryptedSecretKeyData(new AlgorithmIdentifier(PKCSObjectIdentifiers.id_PBES2, new PBES2Parameters(generatePkbdAlgorithmIdentifier2, new EncryptionScheme(NISTObjectIdentifiers.id_aes256_wrap_pad))), this.createCipher("AESKWP", generateKey2).doFinal(secretKeyData.getEncoded()));
                }
                this.entries.put(s, new ObjectData(BcFKSKeyStoreSpi.SECRET_KEY, s, creationDate, lastModifiedDate, encryptedSecretKeyData.getEncoded(), null));
                this.lastModifiedDate = lastModifiedDate;
                return;
            }
            catch (Exception ex2) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("BCFKS KeyStore exception storing private key: ");
                sb4.append(ex2.toString());
                throw new ExtKeyStoreException(sb4.toString(), ex2);
            }
        }
        throw new KeyStoreException("BCFKS KeyStore cannot store certificate chain with secret key.");
    }
    
    @Override
    public void engineSetKeyEntry(final String s, final byte[] array, final Certificate[] array2) throws KeyStoreException {
        final Date lastModifiedDate = new Date();
        final ObjectData objectData = this.entries.get(s);
        Date creationDate;
        if (objectData != null) {
            creationDate = this.extractCreationDate(objectData, lastModifiedDate);
        }
        else {
            creationDate = lastModifiedDate;
        }
        if (array2 != null) {
            try {
                final EncryptedPrivateKeyInfo instance = EncryptedPrivateKeyInfo.getInstance(array);
                try {
                    this.privateKeyCache.remove(s);
                    this.entries.put(s, new ObjectData(BcFKSKeyStoreSpi.PROTECTED_PRIVATE_KEY, s, creationDate, lastModifiedDate, this.createPrivateKeySequence(instance, array2).getEncoded(), null));
                }
                catch (Exception ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("BCFKS KeyStore exception storing protected private key: ");
                    sb.append(ex.toString());
                    throw new ExtKeyStoreException(sb.toString(), ex);
                }
            }
            catch (Exception ex2) {
                throw new ExtKeyStoreException("BCFKS KeyStore private key encoding must be an EncryptedPrivateKeyInfo.", ex2);
            }
        }
        try {
            this.entries.put(s, new ObjectData(BcFKSKeyStoreSpi.PROTECTED_SECRET_KEY, s, creationDate, lastModifiedDate, array, null));
            this.lastModifiedDate = lastModifiedDate;
        }
        catch (Exception ex3) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("BCFKS KeyStore exception storing protected private key: ");
            sb2.append(ex3.toString());
            throw new ExtKeyStoreException(sb2.toString(), ex3);
        }
    }
    
    @Override
    public int engineSize() {
        return this.entries.size();
    }
    
    @Override
    public void engineStore(final OutputStream outputStream, final char[] array) throws IOException, NoSuchAlgorithmException, CertificateException {
        if (this.creationDate != null) {
            final EncryptedObjectStoreData encryptedObjectStoreData = this.getEncryptedObjectStoreData(this.hmacAlgorithm, array);
            KeyDerivationFunc keyDerivationFunc;
            BigInteger bigInteger;
            if (MiscObjectIdentifiers.id_scrypt.equals(this.hmacPkbdAlgorithm.getAlgorithm())) {
                final ScryptParams instance = ScryptParams.getInstance(this.hmacPkbdAlgorithm.getParameters());
                keyDerivationFunc = this.hmacPkbdAlgorithm;
                bigInteger = instance.getKeyLength();
            }
            else {
                final PBKDF2Params instance2 = PBKDF2Params.getInstance(this.hmacPkbdAlgorithm.getParameters());
                keyDerivationFunc = this.hmacPkbdAlgorithm;
                bigInteger = instance2.getKeyLength();
            }
            this.hmacPkbdAlgorithm = this.generatePkbdAlgorithmIdentifier(keyDerivationFunc, bigInteger.intValue());
            try {
                outputStream.write(new ObjectStore(encryptedObjectStoreData, new ObjectStoreIntegrityCheck(new PbkdMacIntegrityCheck(this.hmacAlgorithm, this.hmacPkbdAlgorithm, this.calculateMac(encryptedObjectStoreData.getEncoded(), this.hmacAlgorithm, this.hmacPkbdAlgorithm, array)))).getEncoded());
                outputStream.flush();
                return;
            }
            catch (NoSuchProviderException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("cannot calculate mac: ");
                sb.append(ex.getMessage());
                throw new IOException(sb.toString());
            }
        }
        throw new IOException("KeyStore not initialized");
    }
    
    @Override
    public void engineStore(final KeyStore.LoadStoreParameter loadStoreParameter) throws CertificateException, NoSuchAlgorithmException, IOException {
        if (loadStoreParameter == null) {
            throw new IllegalArgumentException("'parameter' arg cannot be null");
        }
        if (loadStoreParameter instanceof BCFKSStoreParameter) {
            final BCFKSStoreParameter bcfksStoreParameter = (BCFKSStoreParameter)loadStoreParameter;
            final char[] password = this.extractPassword(loadStoreParameter);
            this.hmacPkbdAlgorithm = this.generatePkbdAlgorithmIdentifier(bcfksStoreParameter.getStorePBKDFConfig(), 64);
            this.engineStore(bcfksStoreParameter.getOutputStream(), password);
            return;
        }
        if (loadStoreParameter instanceof BCFKSLoadStoreParameter) {
            final BCFKSLoadStoreParameter bcfksLoadStoreParameter = (BCFKSLoadStoreParameter)loadStoreParameter;
            if (bcfksLoadStoreParameter.getStoreSignatureKey() != null) {
                this.signatureAlgorithm = this.generateSignatureAlgId(bcfksLoadStoreParameter.getStoreSignatureKey(), bcfksLoadStoreParameter.getStoreSignatureAlgorithm());
                this.hmacPkbdAlgorithm = this.generatePkbdAlgorithmIdentifier(bcfksLoadStoreParameter.getStorePBKDFConfig(), 64);
                ASN1ObjectIdentifier storeEncryptionAlgorithm;
                if (bcfksLoadStoreParameter.getStoreEncryptionAlgorithm() == BCFKSLoadStoreParameter.EncryptionAlgorithm.AES256_CCM) {
                    storeEncryptionAlgorithm = NISTObjectIdentifiers.id_aes256_CCM;
                }
                else {
                    storeEncryptionAlgorithm = NISTObjectIdentifiers.id_aes256_wrap_pad;
                }
                this.storeEncryptionAlgorithm = storeEncryptionAlgorithm;
                AlgorithmIdentifier hmacAlgorithm;
                if (bcfksLoadStoreParameter.getStoreMacAlgorithm() == BCFKSLoadStoreParameter.MacAlgorithm.HmacSHA512) {
                    hmacAlgorithm = new AlgorithmIdentifier(PKCSObjectIdentifiers.id_hmacWithSHA512, DERNull.INSTANCE);
                }
                else {
                    hmacAlgorithm = new AlgorithmIdentifier(NISTObjectIdentifiers.id_hmacWithSHA3_512, DERNull.INSTANCE);
                }
                this.hmacAlgorithm = hmacAlgorithm;
                final EncryptedObjectStoreData encryptedObjectStoreData = this.getEncryptedObjectStoreData(this.signatureAlgorithm, this.extractPassword(bcfksLoadStoreParameter));
                try {
                    final Signature signature = this.helper.createSignature(this.signatureAlgorithm.getAlgorithm().getId());
                    signature.initSign((PrivateKey)bcfksLoadStoreParameter.getStoreSignatureKey());
                    signature.update(encryptedObjectStoreData.getEncoded());
                    final X509Certificate[] storeCertificates = bcfksLoadStoreParameter.getStoreCertificates();
                    SignatureCheck signatureCheck;
                    if (storeCertificates != null) {
                        final org.bouncycastle.asn1.x509.Certificate[] array = new org.bouncycastle.asn1.x509.Certificate[storeCertificates.length];
                        for (int i = 0; i != array.length; ++i) {
                            array[i] = org.bouncycastle.asn1.x509.Certificate.getInstance(storeCertificates[i].getEncoded());
                        }
                        signatureCheck = new SignatureCheck(this.signatureAlgorithm, array, signature.sign());
                    }
                    else {
                        signatureCheck = new SignatureCheck(this.signatureAlgorithm, signature.sign());
                    }
                    bcfksLoadStoreParameter.getOutputStream().write(new ObjectStore(encryptedObjectStoreData, new ObjectStoreIntegrityCheck(signatureCheck)).getEncoded());
                    bcfksLoadStoreParameter.getOutputStream().flush();
                    return;
                }
                catch (GeneralSecurityException cause) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("error creating signature: ");
                    sb.append(cause.getMessage());
                    throw new IOException(sb.toString(), cause);
                }
            }
            final char[] password2 = this.extractPassword(bcfksLoadStoreParameter);
            this.hmacPkbdAlgorithm = this.generatePkbdAlgorithmIdentifier(bcfksLoadStoreParameter.getStorePBKDFConfig(), 64);
            ASN1ObjectIdentifier storeEncryptionAlgorithm2;
            if (bcfksLoadStoreParameter.getStoreEncryptionAlgorithm() == BCFKSLoadStoreParameter.EncryptionAlgorithm.AES256_CCM) {
                storeEncryptionAlgorithm2 = NISTObjectIdentifiers.id_aes256_CCM;
            }
            else {
                storeEncryptionAlgorithm2 = NISTObjectIdentifiers.id_aes256_wrap_pad;
            }
            this.storeEncryptionAlgorithm = storeEncryptionAlgorithm2;
            AlgorithmIdentifier hmacAlgorithm2;
            if (bcfksLoadStoreParameter.getStoreMacAlgorithm() == BCFKSLoadStoreParameter.MacAlgorithm.HmacSHA512) {
                hmacAlgorithm2 = new AlgorithmIdentifier(PKCSObjectIdentifiers.id_hmacWithSHA512, DERNull.INSTANCE);
            }
            else {
                hmacAlgorithm2 = new AlgorithmIdentifier(NISTObjectIdentifiers.id_hmacWithSHA3_512, DERNull.INSTANCE);
            }
            this.hmacAlgorithm = hmacAlgorithm2;
            this.engineStore(bcfksLoadStoreParameter.getOutputStream(), password2);
            return;
        }
        if (loadStoreParameter instanceof BCLoadStoreParameter) {
            this.engineStore(((BCLoadStoreParameter)loadStoreParameter).getOutputStream(), this.extractPassword(loadStoreParameter));
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("no support for 'parameter' of type ");
        sb2.append(loadStoreParameter.getClass().getName());
        throw new IllegalArgumentException(sb2.toString());
    }
    
    public static class Def extends BcFKSKeyStoreSpi
    {
        public Def() {
            super(new DefaultJcaJceHelper());
        }
    }
    
    public static class DefShared extends SharedKeyStoreSpi
    {
        public DefShared() {
            super(new DefaultJcaJceHelper());
        }
    }
    
    private static class ExtKeyStoreException extends KeyStoreException
    {
        private final Throwable cause;
        
        ExtKeyStoreException(final String msg, final Throwable cause) {
            super(msg);
            this.cause = cause;
        }
        
        @Override
        public Throwable getCause() {
            return this.cause;
        }
    }
    
    private static class SharedKeyStoreSpi extends BcFKSKeyStoreSpi implements PKCSObjectIdentifiers, X509ObjectIdentifiers
    {
        private final Map<String, byte[]> cache;
        private final byte[] seedKey;
        
        public SharedKeyStoreSpi(final JcaJceHelper jcaJceHelper) {
            super(jcaJceHelper);
            try {
                this.seedKey = new byte[32];
                jcaJceHelper.createSecureRandom("DEFAULT").nextBytes(this.seedKey);
                this.cache = new HashMap<String, byte[]>();
            }
            catch (GeneralSecurityException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("can't create random - ");
                sb.append(ex.toString());
                throw new IllegalArgumentException(sb.toString());
            }
        }
        
        private byte[] calculateMac(final String s, final char[] array) throws NoSuchAlgorithmException, InvalidKeyException {
            byte[] array2;
            if (array != null) {
                array2 = Arrays.concatenate(Strings.toUTF8ByteArray(array), Strings.toUTF8ByteArray(s));
            }
            else {
                array2 = Arrays.concatenate(this.seedKey, Strings.toUTF8ByteArray(s));
            }
            return SCrypt.generate(array2, this.seedKey, 16384, 8, 1, 32);
        }
        
        @Override
        public void engineDeleteEntry(final String s) throws KeyStoreException {
            throw new KeyStoreException("delete operation not supported in shared mode");
        }
        
        @Override
        public Key engineGetKey(final String s, final char[] array) throws NoSuchAlgorithmException, UnrecoverableKeyException {
            try {
                final byte[] calculateMac = this.calculateMac(s, array);
                if (this.cache.containsKey(s) && !Arrays.constantTimeAreEqual(this.cache.get(s), calculateMac)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("unable to recover key (");
                    sb.append(s);
                    sb.append(")");
                    throw new UnrecoverableKeyException(sb.toString());
                }
                final Key engineGetKey = super.engineGetKey(s, array);
                if (engineGetKey != null && !this.cache.containsKey(s)) {
                    this.cache.put(s, calculateMac);
                }
                return engineGetKey;
            }
            catch (InvalidKeyException ex) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("unable to recover key (");
                sb2.append(s);
                sb2.append("): ");
                sb2.append(ex.getMessage());
                throw new UnrecoverableKeyException(sb2.toString());
            }
        }
        
        @Override
        public void engineSetCertificateEntry(final String s, final Certificate certificate) throws KeyStoreException {
            throw new KeyStoreException("set operation not supported in shared mode");
        }
        
        @Override
        public void engineSetKeyEntry(final String s, final Key key, final char[] array, final Certificate[] array2) throws KeyStoreException {
            throw new KeyStoreException("set operation not supported in shared mode");
        }
        
        @Override
        public void engineSetKeyEntry(final String s, final byte[] array, final Certificate[] array2) throws KeyStoreException {
            throw new KeyStoreException("set operation not supported in shared mode");
        }
    }
    
    public static class Std extends BcFKSKeyStoreSpi
    {
        public Std() {
            super(new BCJcaJceHelper());
        }
    }
    
    public static class StdShared extends SharedKeyStoreSpi
    {
        public StdShared() {
            super(new BCJcaJceHelper());
        }
    }
}
