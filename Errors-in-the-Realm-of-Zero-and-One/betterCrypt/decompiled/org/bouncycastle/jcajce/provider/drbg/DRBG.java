// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.drbg;

import java.io.IOException;
import java.io.InputStream;
import org.bouncycastle.jcajce.provider.config.ConfigurableProvider;
import org.bouncycastle.jcajce.provider.util.AsymmetricAlgorithmProvider;
import org.bouncycastle.util.Properties;
import java.util.concurrent.atomic.AtomicReference;
import org.bouncycastle.crypto.Mac;
import org.bouncycastle.crypto.macs.HMac;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import org.bouncycastle.crypto.prng.SP800SecureRandom;
import java.security.Provider;
import java.security.SecureRandomSpi;
import org.bouncycastle.util.Pack;
import org.bouncycastle.util.Strings;
import java.security.AccessController;
import org.bouncycastle.jcajce.provider.symmetric.util.ClassUtil;
import java.security.PrivilegedAction;
import java.net.URL;
import java.security.Security;
import org.bouncycastle.crypto.prng.EntropySource;
import org.bouncycastle.crypto.prng.EntropySourceProvider;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.digests.SHA512Digest;
import org.bouncycastle.crypto.prng.SP800SecureRandomBuilder;
import java.security.SecureRandom;

public class DRBG
{
    private static final String PREFIX;
    private static final String[][] initialEntropySourceNames;
    
    static {
        PREFIX = DRBG.class.getName();
        initialEntropySourceNames = new String[][] { { "sun.security.provider.Sun", "sun.security.provider.SecureRandom" }, { "org.apache.harmony.security.provider.crypto.CryptoProvider", "org.apache.harmony.security.provider.crypto.SHA1PRNG_SecureRandomImpl" }, { "com.android.org.conscrypt.OpenSSLProvider", "com.android.org.conscrypt.OpenSSLRandom" }, { "org.conscrypt.OpenSSLProvider", "org.conscrypt.OpenSSLRandom" } };
    }
    
    private static SecureRandom createBaseRandom(final boolean b) {
        if (System.getProperty("org.bouncycastle.drbg.entropysource") != null) {
            final EntropySourceProvider entropySource = createEntropySource();
            final EntropySource value = entropySource.get(128);
            final byte[] entropy = value.getEntropy();
            byte[] personalizationString;
            if (b) {
                personalizationString = generateDefaultPersonalizationString(entropy);
            }
            else {
                personalizationString = generateNonceIVPersonalizationString(entropy);
            }
            return new SP800SecureRandomBuilder(entropySource).setPersonalizationString(personalizationString).buildHash(new SHA512Digest(), Arrays.concatenate(value.getEntropy(), value.getEntropy()), b);
        }
        final HybridSecureRandom hybridSecureRandom = new HybridSecureRandom();
        final byte[] generateSeed = hybridSecureRandom.generateSeed(16);
        byte[] personalizationString2;
        if (b) {
            personalizationString2 = generateDefaultPersonalizationString(generateSeed);
        }
        else {
            personalizationString2 = generateNonceIVPersonalizationString(generateSeed);
        }
        return new SP800SecureRandomBuilder(hybridSecureRandom, true).setPersonalizationString(personalizationString2).buildHash(new SHA512Digest(), hybridSecureRandom.generateSeed(32), b);
    }
    
    private static SecureRandom createCoreSecureRandom() {
        if (Security.getProperty("securerandom.source") == null) {
            return new CoreSecureRandom(findSource());
        }
        try {
            return new URLSeededSecureRandom(new URL(Security.getProperty("securerandom.source")));
        }
        catch (Exception ex) {
            return new SecureRandom();
        }
    }
    
    private static EntropySourceProvider createEntropySource() {
        return AccessController.doPrivileged((PrivilegedAction<EntropySourceProvider>)new PrivilegedAction<EntropySourceProvider>() {
            final /* synthetic */ String val$sourceClass = System.getProperty("org.bouncycastle.drbg.entropysource");
            
            @Override
            public EntropySourceProvider run() {
                try {
                    return ClassUtil.loadClass(DRBG.class, this.val$sourceClass).newInstance();
                }
                catch (Exception cause) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("entropy source ");
                    sb.append(this.val$sourceClass);
                    sb.append(" not created: ");
                    sb.append(cause.getMessage());
                    throw new IllegalStateException(sb.toString(), cause);
                }
            }
        });
    }
    
    private static SecureRandom createInitialEntropySource() {
        if (AccessController.doPrivileged((PrivilegedAction<Boolean>)new PrivilegedAction<Boolean>() {
            @Override
            public Boolean run() {
                while (true) {
                    while (true) {
                        Label_0033: {
                            try {
                                if (SecureRandom.class.getMethod("getInstanceStrong", (Class<?>[])new Class[0]) != null) {
                                    return true;
                                }
                                break Label_0033;
                            }
                            catch (Exception ex) {
                                return false;
                            }
                        }
                        final boolean b = false;
                        continue;
                    }
                }
            }
        })) {
            return AccessController.doPrivileged((PrivilegedAction<SecureRandom>)new PrivilegedAction<SecureRandom>() {
                @Override
                public SecureRandom run() {
                    try {
                        return (SecureRandom)SecureRandom.class.getMethod("getInstanceStrong", (Class<?>[])new Class[0]).invoke(null, new Object[0]);
                    }
                    catch (Exception ex) {
                        return createCoreSecureRandom();
                    }
                }
            });
        }
        return createCoreSecureRandom();
    }
    
    private static final Object[] findSource() {
        int n = 0;
    Label_0056_Outer:
        while (true) {
            final String[][] initialEntropySourceNames = DRBG.initialEntropySourceNames;
            while (true) {
                if (n < initialEntropySourceNames.length) {
                    final String[] array = initialEntropySourceNames[n];
                    while (true) {
                        try {
                            return new Object[] { Class.forName(array[0]).newInstance(), Class.forName(array[1]).newInstance() };
                            return null;
                            ++n;
                            continue Label_0056_Outer;
                        }
                        finally {}
                        continue;
                    }
                }
                continue;
            }
        }
    }
    
    private static byte[] generateDefaultPersonalizationString(final byte[] array) {
        return Arrays.concatenate(Strings.toByteArray("Default"), array, Pack.longToBigEndian(Thread.currentThread().getId()), Pack.longToBigEndian(System.currentTimeMillis()));
    }
    
    private static byte[] generateNonceIVPersonalizationString(final byte[] array) {
        return Arrays.concatenate(Strings.toByteArray("Nonce"), array, Pack.longToLittleEndian(Thread.currentThread().getId()), Pack.longToLittleEndian(System.currentTimeMillis()));
    }
    
    private static class CoreSecureRandom extends SecureRandom
    {
        CoreSecureRandom(final Object[] array) {
            super((SecureRandomSpi)array[1], (Provider)array[0]);
        }
    }
    
    public static class Default extends SecureRandomSpi
    {
        private static final SecureRandom random;
        
        static {
            random = createBaseRandom(true);
        }
        
        @Override
        protected byte[] engineGenerateSeed(final int numBytes) {
            return Default.random.generateSeed(numBytes);
        }
        
        @Override
        protected void engineNextBytes(final byte[] bytes) {
            Default.random.nextBytes(bytes);
        }
        
        @Override
        protected void engineSetSeed(final byte[] seed) {
            Default.random.setSeed(seed);
        }
    }
    
    private static class HybridRandomProvider extends Provider
    {
        protected HybridRandomProvider() {
            super("BCHEP", 1.0, "Bouncy Castle Hybrid Entropy Provider");
        }
    }
    
    private static class HybridSecureRandom extends SecureRandom
    {
        private final SecureRandom baseRandom;
        private final SP800SecureRandom drbg;
        private final AtomicInteger samples;
        private final AtomicBoolean seedAvailable;
        
        HybridSecureRandom() {
            super(null, new HybridRandomProvider());
            this.seedAvailable = new AtomicBoolean(false);
            this.samples = new AtomicInteger(0);
            this.baseRandom = createInitialEntropySource();
            this.drbg = new SP800SecureRandomBuilder(new EntropySourceProvider() {
                @Override
                public EntropySource get(final int n) {
                    return new SignallingEntropySource(n);
                }
            }).setPersonalizationString(Strings.toByteArray("Bouncy Castle Hybrid Entropy Source")).buildHMAC(new HMac(new SHA512Digest()), this.baseRandom.generateSeed(32), false);
        }
        
        @Override
        public byte[] generateSeed(final int n) {
            final byte[] array = new byte[n];
            if (this.samples.getAndIncrement() > 20 && this.seedAvailable.getAndSet(false)) {
                this.samples.set(0);
                this.drbg.reseed((byte[])null);
            }
            this.drbg.nextBytes(array);
            return array;
        }
        
        @Override
        public void setSeed(final long seed) {
            final SP800SecureRandom drbg = this.drbg;
            if (drbg != null) {
                drbg.setSeed(seed);
            }
        }
        
        @Override
        public void setSeed(final byte[] seed) {
            final SP800SecureRandom drbg = this.drbg;
            if (drbg != null) {
                drbg.setSeed(seed);
            }
        }
        
        private class SignallingEntropySource implements EntropySource
        {
            private final int byteLength;
            private final AtomicReference entropy;
            private final AtomicBoolean scheduled;
            
            SignallingEntropySource(final int n) {
                this.entropy = new AtomicReference();
                this.scheduled = new AtomicBoolean(false);
                this.byteLength = (n + 7) / 8;
            }
            
            @Override
            public int entropySize() {
                return this.byteLength * 8;
            }
            
            @Override
            public byte[] getEntropy() {
                byte[] generateSeed = this.entropy.getAndSet(null);
                if (generateSeed != null && generateSeed.length == this.byteLength) {
                    this.scheduled.set(false);
                }
                else {
                    generateSeed = HybridSecureRandom.this.baseRandom.generateSeed(this.byteLength);
                }
                if (!this.scheduled.getAndSet(true)) {
                    new Thread(new EntropyGatherer(this.byteLength)).start();
                }
                return generateSeed;
            }
            
            @Override
            public boolean isPredictionResistant() {
                return true;
            }
            
            private class EntropyGatherer implements Runnable
            {
                private final int numBytes;
                
                EntropyGatherer(final int numBytes) {
                    this.numBytes = numBytes;
                }
                
                private void sleep(final long n) {
                    while (true) {
                        try {
                            Thread.sleep(n);
                            return;
                            Thread.currentThread().interrupt();
                        }
                        catch (InterruptedException ex) {
                            continue;
                        }
                        break;
                    }
                }
                
                @Override
                public void run() {
                    final String propertyValue = Properties.getPropertyValue("org.bouncycastle.drbg.gather_pause_secs");
                    long n2;
                    final long n = n2 = 5000L;
                    while (true) {
                        if (propertyValue == null) {
                            break Label_0032;
                        }
                        try {
                            n2 = Long.parseLong(propertyValue) * 1000L;
                            final byte[] newValue = new byte[this.numBytes];
                            for (int i = 0; i < SignallingEntropySource.this.byteLength / 8; ++i) {
                                this.sleep(n2);
                                final byte[] generateSeed = HybridSecureRandom.this.baseRandom.generateSeed(8);
                                System.arraycopy(generateSeed, 0, newValue, i * 8, generateSeed.length);
                            }
                            final int numBytes = SignallingEntropySource.this.byteLength - SignallingEntropySource.this.byteLength / 8 * 8;
                            if (numBytes != 0) {
                                this.sleep(n2);
                                final byte[] generateSeed2 = HybridSecureRandom.this.baseRandom.generateSeed(numBytes);
                                System.arraycopy(generateSeed2, 0, newValue, newValue.length - generateSeed2.length, generateSeed2.length);
                            }
                            SignallingEntropySource.this.entropy.set(newValue);
                            HybridSecureRandom.this.seedAvailable.set(true);
                        }
                        catch (Exception ex) {
                            n2 = n;
                            continue;
                        }
                        break;
                    }
                }
            }
        }
    }
    
    public static class Mappings extends AsymmetricAlgorithmProvider
    {
        @Override
        public void configure(final ConfigurableProvider configurableProvider) {
            final StringBuilder sb = new StringBuilder();
            sb.append(DRBG.PREFIX);
            sb.append("$Default");
            configurableProvider.addAlgorithm("SecureRandom.DEFAULT", sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(DRBG.PREFIX);
            sb2.append("$NonceAndIV");
            configurableProvider.addAlgorithm("SecureRandom.NONCEANDIV", sb2.toString());
        }
    }
    
    public static class NonceAndIV extends SecureRandomSpi
    {
        private static final SecureRandom random;
        
        static {
            random = createBaseRandom(false);
        }
        
        @Override
        protected byte[] engineGenerateSeed(final int numBytes) {
            return NonceAndIV.random.generateSeed(numBytes);
        }
        
        @Override
        protected void engineNextBytes(final byte[] bytes) {
            NonceAndIV.random.nextBytes(bytes);
        }
        
        @Override
        protected void engineSetSeed(final byte[] seed) {
            NonceAndIV.random.setSeed(seed);
        }
    }
    
    private static class URLSeededSecureRandom extends SecureRandom
    {
        private final InputStream seedStream;
        
        URLSeededSecureRandom(final URL url) {
            super(null, new HybridRandomProvider());
            this.seedStream = AccessController.doPrivileged((PrivilegedAction<InputStream>)new PrivilegedAction<InputStream>() {
                @Override
                public InputStream run() {
                    try {
                        return url.openStream();
                    }
                    catch (IOException ex) {
                        throw new InternalError("unable to open random source");
                    }
                }
            });
        }
        
        private int privilegedRead(final byte[] array, final int n, final int n2) {
            return AccessController.doPrivileged((PrivilegedAction<Integer>)new PrivilegedAction<Integer>() {
                @Override
                public Integer run() {
                    try {
                        return URLSeededSecureRandom.this.seedStream.read(array, n, n2);
                    }
                    catch (IOException ex) {
                        throw new InternalError("unable to read random source");
                    }
                }
            });
        }
        
        @Override
        public byte[] generateSeed(int i) {
            synchronized (this) {
                byte[] array;
                int privilegedRead;
                for (array = new byte[i], i = 0; i != array.length; i += privilegedRead) {
                    privilegedRead = this.privilegedRead(array, i, array.length - i);
                    if (privilegedRead <= -1) {
                        break;
                    }
                }
                if (i == array.length) {
                    return array;
                }
                throw new InternalError("unable to fully read random source");
            }
        }
        
        @Override
        public void setSeed(final long n) {
        }
        
        @Override
        public void setSeed(final byte[] array) {
        }
    }
}
