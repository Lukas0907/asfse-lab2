// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.ec;

import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.asn1.x9.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECNamedCurveSpec;
import java.math.BigInteger;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.AlgorithmParameterSpec;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.asn1.x9.X9ECPoint;
import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;
import org.bouncycastle.asn1.ASN1Null;
import org.bouncycastle.asn1.x9.X962Parameters;
import org.bouncycastle.asn1.DERNull;
import java.io.IOException;
import java.security.spec.ECParameterSpec;

public class AlgorithmParametersSpi extends java.security.AlgorithmParametersSpi
{
    private String curveName;
    private ECParameterSpec ecParameterSpec;
    
    @Override
    protected byte[] engineGetEncoded() throws IOException {
        return this.engineGetEncoded("ASN.1");
    }
    
    @Override
    protected byte[] engineGetEncoded(final String str) throws IOException {
        if (this.isASN1FormatString(str)) {
            final ECParameterSpec ecParameterSpec = this.ecParameterSpec;
            X962Parameters x962Parameters;
            if (ecParameterSpec == null) {
                x962Parameters = new X962Parameters(DERNull.INSTANCE);
            }
            else {
                final String curveName = this.curveName;
                if (curveName != null) {
                    x962Parameters = new X962Parameters(ECUtil.getNamedCurveOid(curveName));
                }
                else {
                    final org.bouncycastle.jce.spec.ECParameterSpec convertSpec = EC5Util.convertSpec(ecParameterSpec);
                    x962Parameters = new X962Parameters(new X9ECParameters(convertSpec.getCurve(), new X9ECPoint(convertSpec.getG(), false), convertSpec.getN(), convertSpec.getH(), convertSpec.getSeed()));
                }
            }
            return x962Parameters.getEncoded();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown parameters format in AlgorithmParameters object: ");
        sb.append(str);
        throw new IOException(sb.toString());
    }
    
    @Override
    protected <T extends AlgorithmParameterSpec> T engineGetParameterSpec(final Class<T> clazz) throws InvalidParameterSpecException {
        if (!ECParameterSpec.class.isAssignableFrom(clazz) && clazz != AlgorithmParameterSpec.class) {
            if (ECGenParameterSpec.class.isAssignableFrom(clazz)) {
                final String curveName = this.curveName;
                if (curveName != null) {
                    final ASN1ObjectIdentifier namedCurveOid = ECUtil.getNamedCurveOid(curveName);
                    if (namedCurveOid != null) {
                        return (T)new ECGenParameterSpec(namedCurveOid.getId());
                    }
                    return (T)new ECGenParameterSpec(this.curveName);
                }
                else {
                    final ASN1ObjectIdentifier namedCurveOid2 = ECUtil.getNamedCurveOid(EC5Util.convertSpec(this.ecParameterSpec));
                    if (namedCurveOid2 != null) {
                        return (T)new ECGenParameterSpec(namedCurveOid2.getId());
                    }
                }
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("EC AlgorithmParameters cannot convert to ");
            sb.append(clazz.getName());
            throw new InvalidParameterSpecException(sb.toString());
        }
        return (T)this.ecParameterSpec;
    }
    
    @Override
    protected void engineInit(final AlgorithmParameterSpec algorithmParameterSpec) throws InvalidParameterSpecException {
        if (algorithmParameterSpec instanceof ECGenParameterSpec) {
            final ECGenParameterSpec ecGenParameterSpec = (ECGenParameterSpec)algorithmParameterSpec;
            final X9ECParameters domainParametersFromGenSpec = ECUtils.getDomainParametersFromGenSpec(ecGenParameterSpec);
            if (domainParametersFromGenSpec != null) {
                this.curveName = ecGenParameterSpec.getName();
                final ECParameterSpec convertToSpec = EC5Util.convertToSpec(domainParametersFromGenSpec);
                this.ecParameterSpec = new ECNamedCurveSpec(this.curveName, convertToSpec.getCurve(), convertToSpec.getGenerator(), convertToSpec.getOrder(), BigInteger.valueOf(convertToSpec.getCofactor()));
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("EC curve name not recognized: ");
            sb.append(ecGenParameterSpec.getName());
            throw new InvalidParameterSpecException(sb.toString());
        }
        else {
            if (algorithmParameterSpec instanceof ECParameterSpec) {
                String name;
                if (algorithmParameterSpec instanceof ECNamedCurveSpec) {
                    name = ((ECNamedCurveSpec)algorithmParameterSpec).getName();
                }
                else {
                    name = null;
                }
                this.curveName = name;
                this.ecParameterSpec = (ECParameterSpec)algorithmParameterSpec;
                return;
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("AlgorithmParameterSpec class not recognized: ");
            sb2.append(algorithmParameterSpec.getClass().getName());
            throw new InvalidParameterSpecException(sb2.toString());
        }
    }
    
    @Override
    protected void engineInit(final byte[] array) throws IOException {
        this.engineInit(array, "ASN.1");
    }
    
    @Override
    protected void engineInit(final byte[] array, final String str) throws IOException {
        if (this.isASN1FormatString(str)) {
            final X962Parameters instance = X962Parameters.getInstance(array);
            final ECCurve curve = EC5Util.getCurve(BouncyCastleProvider.CONFIGURATION, instance);
            if (instance.isNamedCurve()) {
                final ASN1ObjectIdentifier instance2 = ASN1ObjectIdentifier.getInstance(instance.getParameters());
                this.curveName = ECNamedCurveTable.getName(instance2);
                if (this.curveName == null) {
                    this.curveName = instance2.getId();
                }
            }
            this.ecParameterSpec = EC5Util.convertToSpec(instance, curve);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown encoded parameters format in AlgorithmParameters object: ");
        sb.append(str);
        throw new IOException(sb.toString());
    }
    
    @Override
    protected String engineToString() {
        return "EC Parameters";
    }
    
    protected boolean isASN1FormatString(final String s) {
        return s == null || s.equals("ASN.1");
    }
}
