// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.x509;

import java.security.cert.CertificateEncodingException;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateExpiredException;
import java.util.Date;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.x509.BasicConstraints;
import java.security.cert.CertificateParsingException;
import org.bouncycastle.jcajce.provider.asymmetric.util.PKCS12BagAttributeCarrierImpl;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.jcajce.util.JcaJceHelper;
import java.security.PublicKey;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.jce.interfaces.PKCS12BagAttributeCarrier;

class X509CertificateObject extends X509CertificateImpl implements PKCS12BagAttributeCarrier
{
    private PKCS12BagAttributeCarrier attrCarrier;
    private final Object cacheLock;
    private volatile int hashValue;
    private volatile boolean hashValueSet;
    private X509CertificateInternal internalCertificateValue;
    private X500Principal issuerValue;
    private PublicKey publicKeyValue;
    private X500Principal subjectValue;
    private long[] validityValues;
    
    X509CertificateObject(final JcaJceHelper jcaJceHelper, final org.bouncycastle.asn1.x509.Certificate certificate) throws CertificateParsingException {
        super(jcaJceHelper, certificate, createBasicConstraints(certificate), createKeyUsage(certificate));
        this.cacheLock = new Object();
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
    }
    
    private static BasicConstraints createBasicConstraints(final org.bouncycastle.asn1.x509.Certificate certificate) throws CertificateParsingException {
        try {
            final byte[] extensionOctets = X509CertificateImpl.getExtensionOctets(certificate, "2.5.29.19");
            if (extensionOctets == null) {
                return null;
            }
            return BasicConstraints.getInstance(ASN1Primitive.fromByteArray(extensionOctets));
        }
        catch (Exception obj) {
            final StringBuilder sb = new StringBuilder();
            sb.append("cannot construct BasicConstraints: ");
            sb.append(obj);
            throw new CertificateParsingException(sb.toString());
        }
    }
    
    private static boolean[] createKeyUsage(final org.bouncycastle.asn1.x509.Certificate certificate) throws CertificateParsingException {
    Label_0087_Outer:
        while (true) {
            while (true) {
            Label_0145:
                while (true) {
                    int n;
                    try {
                        final byte[] extensionOctets = X509CertificateImpl.getExtensionOctets(certificate, "2.5.29.15");
                        if (extensionOctets == null) {
                            return null;
                        }
                        final DERBitString instance = DERBitString.getInstance(ASN1Primitive.fromByteArray(extensionOctets));
                        final byte[] bytes = instance.getBytes();
                        n = bytes.length * 8 - instance.getPadBits();
                        final int n2 = 9;
                        if (n < 9) {
                            final boolean[] array = new boolean[n2];
                            for (int i = 0; i != n; ++i) {
                                if ((bytes[i / 8] & 128 >>> i % 8) == 0x0) {
                                    break Label_0145;
                                }
                                final boolean b = true;
                                array[i] = b;
                            }
                            return array;
                        }
                    }
                    catch (Exception obj) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("cannot construct KeyUsage: ");
                        sb.append(obj);
                        throw new CertificateParsingException(sb.toString());
                    }
                    final int n2 = n;
                    continue Label_0087_Outer;
                }
                final boolean b = false;
                continue;
            }
        }
    }
    
    private X509CertificateInternal getInternalCertificate() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CertificateObject.cacheLock:Ljava/lang/Object;
        //     4: astore_1       
        //     5: aload_1        
        //     6: monitorenter   
        //     7: aload_0        
        //     8: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CertificateObject.internalCertificateValue:Lorg/bouncycastle/jcajce/provider/asymmetric/x509/X509CertificateInternal;
        //    11: ifnull          23
        //    14: aload_0        
        //    15: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CertificateObject.internalCertificateValue:Lorg/bouncycastle/jcajce/provider/asymmetric/x509/X509CertificateInternal;
        //    18: astore_2       
        //    19: aload_1        
        //    20: monitorexit    
        //    21: aload_2        
        //    22: areturn        
        //    23: aload_1        
        //    24: monitorexit    
        //    25: aload_0        
        //    26: invokevirtual   org/bouncycastle/jcajce/provider/asymmetric/x509/X509CertificateObject.getEncoded:()[B
        //    29: astore_1       
        //    30: goto            38
        //    33: aconst_null    
        //    34: astore_1       
        //    35: goto            30
        //    38: new             Lorg/bouncycastle/jcajce/provider/asymmetric/x509/X509CertificateInternal;
        //    41: dup            
        //    42: aload_0        
        //    43: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CertificateObject.bcHelper:Lorg/bouncycastle/jcajce/util/JcaJceHelper;
        //    46: aload_0        
        //    47: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CertificateObject.c:Lorg/bouncycastle/asn1/x509/Certificate;
        //    50: aload_0        
        //    51: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CertificateObject.basicConstraints:Lorg/bouncycastle/asn1/x509/BasicConstraints;
        //    54: aload_0        
        //    55: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CertificateObject.keyUsage:[Z
        //    58: aload_1        
        //    59: invokespecial   org/bouncycastle/jcajce/provider/asymmetric/x509/X509CertificateInternal.<init>:(Lorg/bouncycastle/jcajce/util/JcaJceHelper;Lorg/bouncycastle/asn1/x509/Certificate;Lorg/bouncycastle/asn1/x509/BasicConstraints;[Z[B)V
        //    62: astore_2       
        //    63: aload_0        
        //    64: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CertificateObject.cacheLock:Ljava/lang/Object;
        //    67: astore_1       
        //    68: aload_1        
        //    69: monitorenter   
        //    70: aload_0        
        //    71: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CertificateObject.internalCertificateValue:Lorg/bouncycastle/jcajce/provider/asymmetric/x509/X509CertificateInternal;
        //    74: ifnonnull       82
        //    77: aload_0        
        //    78: aload_2        
        //    79: putfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CertificateObject.internalCertificateValue:Lorg/bouncycastle/jcajce/provider/asymmetric/x509/X509CertificateInternal;
        //    82: aload_0        
        //    83: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CertificateObject.internalCertificateValue:Lorg/bouncycastle/jcajce/provider/asymmetric/x509/X509CertificateInternal;
        //    86: astore_2       
        //    87: aload_1        
        //    88: monitorexit    
        //    89: aload_2        
        //    90: areturn        
        //    91: astore_2       
        //    92: aload_1        
        //    93: monitorexit    
        //    94: aload_2        
        //    95: athrow         
        //    96: astore_2       
        //    97: aload_1        
        //    98: monitorexit    
        //    99: aload_2        
        //   100: athrow         
        //   101: astore_1       
        //   102: goto            33
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                             
        //  -----  -----  -----  -----  -------------------------------------------------
        //  7      21     96     101    Any
        //  23     25     96     101    Any
        //  25     30     101    38     Ljava/security/cert/CertificateEncodingException;
        //  70     82     91     96     Any
        //  82     89     91     96     Any
        //  92     94     91     96     Any
        //  97     99     96     101    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0030:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public void checkValidity(final Date date) throws CertificateExpiredException, CertificateNotYetValidException {
        final long time = date.getTime();
        final long[] validityValues = this.getValidityValues();
        if (time > validityValues[1]) {
            final StringBuilder sb = new StringBuilder();
            sb.append("certificate expired on ");
            sb.append(this.c.getEndDate().getTime());
            throw new CertificateExpiredException(sb.toString());
        }
        if (time >= validityValues[0]) {
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("certificate not valid till ");
        sb2.append(this.c.getStartDate().getTime());
        throw new CertificateNotYetValidException(sb2.toString());
    }
    
    @Override
    public boolean equals(final Object other) {
        if (other == this) {
            return true;
        }
        if (other instanceof X509CertificateObject) {
            final X509CertificateObject x509CertificateObject = (X509CertificateObject)other;
            if (this.hashValueSet && x509CertificateObject.hashValueSet) {
                if (this.hashValue != x509CertificateObject.hashValue) {
                    return false;
                }
            }
            else if (this.internalCertificateValue == null || x509CertificateObject.internalCertificateValue == null) {
                final DERBitString signature = this.c.getSignature();
                if (signature != null && !signature.equals(x509CertificateObject.c.getSignature())) {
                    return false;
                }
            }
        }
        return this.getInternalCertificate().equals(other);
    }
    
    @Override
    public ASN1Encodable getBagAttribute(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        return this.attrCarrier.getBagAttribute(asn1ObjectIdentifier);
    }
    
    @Override
    public Enumeration getBagAttributeKeys() {
        return this.attrCarrier.getBagAttributeKeys();
    }
    
    @Override
    public X500Principal getIssuerX500Principal() {
        synchronized (this.cacheLock) {
            if (this.issuerValue != null) {
                return this.issuerValue;
            }
            // monitorexit(this.cacheLock)
            final X500Principal issuerX500Principal = super.getIssuerX500Principal();
            final Object cacheLock = this.cacheLock;
            synchronized (this.cacheLock) {
                if (this.issuerValue == null) {
                    this.issuerValue = issuerX500Principal;
                }
                return this.issuerValue;
            }
        }
    }
    
    @Override
    public PublicKey getPublicKey() {
        synchronized (this.cacheLock) {
            if (this.publicKeyValue != null) {
                return this.publicKeyValue;
            }
            // monitorexit(this.cacheLock)
            final PublicKey publicKey = super.getPublicKey();
            if (publicKey == null) {
                return null;
            }
            final Object cacheLock = this.cacheLock;
            synchronized (this.cacheLock) {
                if (this.publicKeyValue == null) {
                    this.publicKeyValue = publicKey;
                }
                return this.publicKeyValue;
            }
        }
    }
    
    @Override
    public X500Principal getSubjectX500Principal() {
        synchronized (this.cacheLock) {
            if (this.subjectValue != null) {
                return this.subjectValue;
            }
            // monitorexit(this.cacheLock)
            final X500Principal subjectX500Principal = super.getSubjectX500Principal();
            final Object cacheLock = this.cacheLock;
            synchronized (this.cacheLock) {
                if (this.subjectValue == null) {
                    this.subjectValue = subjectX500Principal;
                }
                return this.subjectValue;
            }
        }
    }
    
    public long[] getValidityValues() {
        synchronized (this.cacheLock) {
            if (this.validityValues != null) {
                return this.validityValues;
            }
            // monitorexit(this.cacheLock)
            final long time = super.getNotBefore().getTime();
            final long time2 = super.getNotAfter().getTime();
            final Object cacheLock = this.cacheLock;
            synchronized (this.cacheLock) {
                if (this.validityValues == null) {
                    this.validityValues = new long[] { time, time2 };
                }
                return this.validityValues;
            }
        }
    }
    
    @Override
    public int hashCode() {
        if (!this.hashValueSet) {
            this.hashValue = this.getInternalCertificate().hashCode();
            this.hashValueSet = true;
        }
        return this.hashValue;
    }
    
    public int originalHashCode() {
        try {
            final byte[] encoded = this.getInternalCertificate().getEncoded();
            int i = 1;
            int n = 0;
            while (i < encoded.length) {
                n += encoded[i] * i;
                ++i;
            }
            return n;
        }
        catch (CertificateEncodingException ex) {
            return 0;
        }
    }
    
    @Override
    public void setBagAttribute(final ASN1ObjectIdentifier asn1ObjectIdentifier, final ASN1Encodable asn1Encodable) {
        this.attrCarrier.setBagAttribute(asn1ObjectIdentifier, asn1Encodable);
    }
}
