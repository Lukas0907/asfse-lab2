// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.edec;

import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import org.bouncycastle.crypto.params.X25519PublicKeyParameters;
import org.bouncycastle.crypto.params.Ed448PublicKeyParameters;
import org.bouncycastle.crypto.params.X448PublicKeyParameters;
import org.bouncycastle.util.Strings;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.util.Fingerprint;

class Utils
{
    private static String generateKeyFingerprint(final byte[] array) {
        return new Fingerprint(array).toString();
    }
    
    static boolean isValidPrefix(final byte[] array, final byte[] array2) {
        if (array2.length < array.length) {
            return isValidPrefix(array, array) ^ true;
        }
        boolean b = false;
        int n;
        for (int i = n = 0; i != array.length; ++i) {
            n |= (array[i] ^ array2[i]);
        }
        if (n == 0) {
            b = true;
        }
        return b;
    }
    
    static String keyToString(final String str, final String str2, final AsymmetricKeyParameter asymmetricKeyParameter) {
        final StringBuffer sb = new StringBuffer();
        final String lineSeparator = Strings.lineSeparator();
        byte[] array;
        if (asymmetricKeyParameter instanceof X448PublicKeyParameters) {
            array = ((X448PublicKeyParameters)asymmetricKeyParameter).getEncoded();
        }
        else if (asymmetricKeyParameter instanceof Ed448PublicKeyParameters) {
            array = ((Ed448PublicKeyParameters)asymmetricKeyParameter).getEncoded();
        }
        else if (asymmetricKeyParameter instanceof X25519PublicKeyParameters) {
            array = ((X25519PublicKeyParameters)asymmetricKeyParameter).getEncoded();
        }
        else {
            array = ((Ed25519PublicKeyParameters)asymmetricKeyParameter).getEncoded();
        }
        sb.append(str2);
        sb.append(" ");
        sb.append(str);
        sb.append(" [");
        sb.append(generateKeyFingerprint(array));
        sb.append("]");
        sb.append(lineSeparator);
        sb.append("    public data: ");
        sb.append(Hex.toHexString(array));
        sb.append(lineSeparator);
        return sb.toString();
    }
}
