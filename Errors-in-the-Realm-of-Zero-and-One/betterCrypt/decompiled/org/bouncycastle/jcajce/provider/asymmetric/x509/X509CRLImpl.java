// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.x509;

import java.security.Provider;
import java.security.NoSuchProviderException;
import java.util.Iterator;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.util.ASN1Dump;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.IssuingDistributionPoint;
import org.bouncycastle.asn1.x509.CRLNumber;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.util.Strings;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.security.cert.Certificate;
import org.bouncycastle.util.Arrays;
import java.util.Collections;
import java.security.cert.X509CRLEntry;
import java.math.BigInteger;
import java.util.Date;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.jce.X509Principal;
import java.security.Principal;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.TBSCertList;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.ASN1OctetString;
import java.util.Enumeration;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.util.HashSet;
import java.util.Set;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CRLException;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import org.bouncycastle.jcajce.io.OutputStreamFactory;
import java.io.IOException;
import java.security.SignatureException;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Primitive;
import java.security.Signature;
import java.security.PublicKey;
import org.bouncycastle.asn1.x509.CertificateList;
import org.bouncycastle.jcajce.util.JcaJceHelper;
import java.security.cert.X509CRL;

abstract class X509CRLImpl extends X509CRL
{
    protected JcaJceHelper bcHelper;
    protected CertificateList c;
    protected boolean isIndirect;
    protected String sigAlgName;
    protected byte[] sigAlgParams;
    
    X509CRLImpl(final JcaJceHelper bcHelper, final CertificateList c, final String sigAlgName, final byte[] sigAlgParams, final boolean isIndirect) {
        this.bcHelper = bcHelper;
        this.c = c;
        this.sigAlgName = sigAlgName;
        this.sigAlgParams = sigAlgParams;
        this.isIndirect = isIndirect;
    }
    
    private void doVerify(final PublicKey publicKey, final Signature signature) throws CRLException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        if (this.c.getSignatureAlgorithm().equals(this.c.getTBSCertList().getSignature())) {
            final byte[] sigAlgParams = this.sigAlgParams;
            if (sigAlgParams != null) {
                try {
                    X509SignatureUtil.setSignatureParameters(signature, ASN1Primitive.fromByteArray(sigAlgParams));
                }
                catch (IOException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("cannot decode signature parameters: ");
                    sb.append(ex.getMessage());
                    throw new SignatureException(sb.toString());
                }
            }
            signature.initVerify(publicKey);
            try {
                final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(OutputStreamFactory.createStream(signature), 512);
                this.c.getTBSCertList().encodeTo(bufferedOutputStream, "DER");
                bufferedOutputStream.close();
                if (signature.verify(this.getSignature())) {
                    return;
                }
                throw new SignatureException("CRL does not verify with supplied public key.");
            }
            catch (IOException ex2) {
                throw new CRLException(ex2.toString());
            }
        }
        throw new CRLException("Signature algorithm on CertificateList does not match TBSCertList.");
    }
    
    private Set getExtensionOIDs(final boolean b) {
        if (this.getVersion() == 2) {
            final Extensions extensions = this.c.getTBSCertList().getExtensions();
            if (extensions != null) {
                final HashSet<String> set = new HashSet<String>();
                final Enumeration oids = extensions.oids();
                while (oids.hasMoreElements()) {
                    final ASN1ObjectIdentifier asn1ObjectIdentifier = oids.nextElement();
                    if (b == extensions.getExtension(asn1ObjectIdentifier).isCritical()) {
                        set.add(asn1ObjectIdentifier.getId());
                    }
                }
                return set;
            }
        }
        return null;
    }
    
    protected static byte[] getExtensionOctets(final CertificateList list, final String s) {
        final ASN1OctetString extensionValue = getExtensionValue(list, s);
        if (extensionValue != null) {
            return extensionValue.getOctets();
        }
        return null;
    }
    
    protected static ASN1OctetString getExtensionValue(final CertificateList list, final String s) {
        final Extensions extensions = list.getTBSCertList().getExtensions();
        if (extensions != null) {
            final Extension extension = extensions.getExtension(new ASN1ObjectIdentifier(s));
            if (extension != null) {
                return extension.getExtnValue();
            }
        }
        return null;
    }
    
    private Set loadCRLEntries() {
        final HashSet<X509CRLEntryObject> set = new HashSet<X509CRLEntryObject>();
        final Enumeration revokedCertificateEnumeration = this.c.getRevokedCertificateEnumeration();
        X500Name instance = null;
        while (revokedCertificateEnumeration.hasMoreElements()) {
            final TBSCertList.CRLEntry crlEntry = revokedCertificateEnumeration.nextElement();
            set.add(new X509CRLEntryObject(crlEntry, this.isIndirect, instance));
            if (this.isIndirect && crlEntry.hasExtensions()) {
                final Extension extension = crlEntry.getExtensions().getExtension(Extension.certificateIssuer);
                if (extension == null) {
                    continue;
                }
                instance = X500Name.getInstance(GeneralNames.getInstance(extension.getParsedValue()).getNames()[0].getName());
            }
        }
        return set;
    }
    
    @Override
    public Set getCriticalExtensionOIDs() {
        return this.getExtensionOIDs(true);
    }
    
    @Override
    public byte[] getEncoded() throws CRLException {
        try {
            return this.c.getEncoded("DER");
        }
        catch (IOException ex) {
            throw new CRLException(ex.toString());
        }
    }
    
    @Override
    public byte[] getExtensionValue(final String s) {
        final ASN1OctetString extensionValue = getExtensionValue(this.c, s);
        if (extensionValue != null) {
            try {
                return extensionValue.getEncoded();
            }
            catch (Exception ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("error parsing ");
                sb.append(ex.toString());
                throw new IllegalStateException(sb.toString());
            }
        }
        return null;
    }
    
    @Override
    public Principal getIssuerDN() {
        return new X509Principal(X500Name.getInstance(this.c.getIssuer().toASN1Primitive()));
    }
    
    @Override
    public X500Principal getIssuerX500Principal() {
        try {
            return new X500Principal(this.c.getIssuer().getEncoded());
        }
        catch (IOException ex) {
            throw new IllegalStateException("can't encode issuer DN");
        }
    }
    
    @Override
    public Date getNextUpdate() {
        if (this.c.getNextUpdate() != null) {
            return this.c.getNextUpdate().getDate();
        }
        return null;
    }
    
    @Override
    public Set getNonCriticalExtensionOIDs() {
        return this.getExtensionOIDs(false);
    }
    
    @Override
    public X509CRLEntry getRevokedCertificate(final BigInteger bigInteger) {
        final Enumeration revokedCertificateEnumeration = this.c.getRevokedCertificateEnumeration();
        X500Name instance = null;
        while (revokedCertificateEnumeration.hasMoreElements()) {
            final TBSCertList.CRLEntry crlEntry = revokedCertificateEnumeration.nextElement();
            if (crlEntry.getUserCertificate().hasValue(bigInteger)) {
                return new X509CRLEntryObject(crlEntry, this.isIndirect, instance);
            }
            if (!this.isIndirect || !crlEntry.hasExtensions()) {
                continue;
            }
            final Extension extension = crlEntry.getExtensions().getExtension(Extension.certificateIssuer);
            if (extension == null) {
                continue;
            }
            instance = X500Name.getInstance(GeneralNames.getInstance(extension.getParsedValue()).getNames()[0].getName());
        }
        return null;
    }
    
    @Override
    public Set getRevokedCertificates() {
        final Set loadCRLEntries = this.loadCRLEntries();
        if (!loadCRLEntries.isEmpty()) {
            return Collections.unmodifiableSet((Set<?>)loadCRLEntries);
        }
        return null;
    }
    
    @Override
    public String getSigAlgName() {
        return this.sigAlgName;
    }
    
    @Override
    public String getSigAlgOID() {
        return this.c.getSignatureAlgorithm().getAlgorithm().getId();
    }
    
    @Override
    public byte[] getSigAlgParams() {
        return Arrays.clone(this.sigAlgParams);
    }
    
    @Override
    public byte[] getSignature() {
        return this.c.getSignature().getOctets();
    }
    
    @Override
    public byte[] getTBSCertList() throws CRLException {
        try {
            return this.c.getTBSCertList().getEncoded("DER");
        }
        catch (IOException ex) {
            throw new CRLException(ex.toString());
        }
    }
    
    @Override
    public Date getThisUpdate() {
        return this.c.getThisUpdate().getDate();
    }
    
    @Override
    public int getVersion() {
        return this.c.getVersionNumber();
    }
    
    @Override
    public boolean hasUnsupportedCriticalExtension() {
        final Set criticalExtensionOIDs = this.getCriticalExtensionOIDs();
        if (criticalExtensionOIDs == null) {
            return false;
        }
        criticalExtensionOIDs.remove(Extension.issuingDistributionPoint.getId());
        criticalExtensionOIDs.remove(Extension.deltaCRLIndicator.getId());
        return criticalExtensionOIDs.isEmpty() ^ true;
    }
    
    @Override
    public boolean isRevoked(final Certificate certificate) {
        if (certificate.getType().equals("X.509")) {
            final Enumeration revokedCertificateEnumeration = this.c.getRevokedCertificateEnumeration();
            X500Name issuer = this.c.getIssuer();
            if (revokedCertificateEnumeration.hasMoreElements()) {
                final X509Certificate x509Certificate = (X509Certificate)certificate;
                final BigInteger serialNumber = x509Certificate.getSerialNumber();
                while (revokedCertificateEnumeration.hasMoreElements()) {
                    final TBSCertList.CRLEntry instance = TBSCertList.CRLEntry.getInstance(revokedCertificateEnumeration.nextElement());
                    X500Name instance2 = issuer;
                    if (this.isIndirect) {
                        instance2 = issuer;
                        if (instance.hasExtensions()) {
                            final Extension extension = instance.getExtensions().getExtension(Extension.certificateIssuer);
                            instance2 = issuer;
                            if (extension != null) {
                                instance2 = X500Name.getInstance(GeneralNames.getInstance(extension.getParsedValue()).getNames()[0].getName());
                            }
                        }
                    }
                    issuer = instance2;
                    if (instance.getUserCertificate().hasValue(serialNumber)) {
                        Label_0182: {
                            if (certificate instanceof X509Certificate) {
                                final X500Name x500Name = X500Name.getInstance(x509Certificate.getIssuerX500Principal().getEncoded());
                                break Label_0182;
                            }
                            try {
                                final X500Name x500Name = org.bouncycastle.asn1.x509.Certificate.getInstance(certificate.getEncoded()).getIssuer();
                                return instance2.equals(x500Name);
                            }
                            catch (CertificateEncodingException ex) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Cannot process certificate: ");
                                sb.append(ex.getMessage());
                                throw new IllegalArgumentException(sb.toString());
                            }
                        }
                        break;
                    }
                }
            }
            return false;
        }
        throw new IllegalArgumentException("X.509 CRL used with non X.509 Cert");
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        final String lineSeparator = Strings.lineSeparator();
        sb.append("              Version: ");
        sb.append(this.getVersion());
        sb.append(lineSeparator);
        sb.append("             IssuerDN: ");
        sb.append(this.getIssuerDN());
        sb.append(lineSeparator);
        sb.append("          This update: ");
        sb.append(this.getThisUpdate());
        sb.append(lineSeparator);
        sb.append("          Next update: ");
        sb.append(this.getNextUpdate());
        sb.append(lineSeparator);
        sb.append("  Signature Algorithm: ");
        sb.append(this.getSigAlgName());
        sb.append(lineSeparator);
        final byte[] signature = this.getSignature();
        sb.append("            Signature: ");
        sb.append(new String(Hex.encode(signature, 0, 20)));
        sb.append(lineSeparator);
        for (int i = 20; i < signature.length; i += 20) {
            final int length = signature.length;
            sb.append("                       ");
            String str;
            if (i < length - 20) {
                str = new String(Hex.encode(signature, i, 20));
            }
            else {
                str = new String(Hex.encode(signature, i, signature.length - i));
            }
            sb.append(str);
            sb.append(lineSeparator);
        }
        final Extensions extensions = this.c.getTBSCertList().getExtensions();
        while (true) {
            Label_0661: {
                if (extensions == null) {
                    break Label_0661;
                }
                final Enumeration oids = extensions.oids();
            Label_0323_Outer:
                while (true) {
                    if (!oids.hasMoreElements()) {
                        break Label_0338;
                    }
                    String str2 = "           Extensions: ";
                Label_0330_Outer:
                    while (true) {
                        sb.append(str2);
                        ASN1ObjectIdentifier asn1ObjectIdentifier;
                        Extension extension;
                        while (true) {
                            sb.append(lineSeparator);
                            if (!oids.hasMoreElements()) {
                                break Label_0661;
                            }
                            asn1ObjectIdentifier = oids.nextElement();
                            extension = extensions.getExtension(asn1ObjectIdentifier);
                            if (extension.getExtnValue() == null) {
                                continue;
                            }
                            break;
                        }
                        final ASN1InputStream asn1InputStream = new ASN1InputStream(extension.getExtnValue().getOctets());
                        sb.append("                       critical(");
                        sb.append(extension.isCritical());
                        sb.append(") ");
                        try {
                            Label_0458: {
                                ASN1Object obj = null;
                                Label_0451: {
                                    if (!asn1ObjectIdentifier.equals(Extension.cRLNumber)) {
                                        String str3;
                                        if (asn1ObjectIdentifier.equals(Extension.deltaCRLIndicator)) {
                                            final StringBuilder sb2 = new StringBuilder();
                                            sb2.append("Base CRL: ");
                                            sb2.append(new CRLNumber(ASN1Integer.getInstance(asn1InputStream.readObject()).getPositiveValue()));
                                            str3 = sb2.toString();
                                        }
                                        else {
                                            if (asn1ObjectIdentifier.equals(Extension.issuingDistributionPoint)) {
                                                obj = IssuingDistributionPoint.getInstance(asn1InputStream.readObject());
                                                break Label_0451;
                                            }
                                            if (asn1ObjectIdentifier.equals(Extension.cRLDistributionPoints)) {
                                                obj = CRLDistPoint.getInstance(asn1InputStream.readObject());
                                                break Label_0451;
                                            }
                                            if (asn1ObjectIdentifier.equals(Extension.freshestCRL)) {
                                                obj = CRLDistPoint.getInstance(asn1InputStream.readObject());
                                                break Label_0451;
                                            }
                                            sb.append(asn1ObjectIdentifier.getId());
                                            sb.append(" value = ");
                                            str3 = ASN1Dump.dumpAsString(asn1InputStream.readObject());
                                        }
                                        sb.append(str3);
                                        break Label_0458;
                                    }
                                    obj = new CRLNumber(ASN1Integer.getInstance(asn1InputStream.readObject()).getPositiveValue());
                                }
                                sb.append(obj);
                            }
                            sb.append(lineSeparator);
                            continue Label_0323_Outer;
                            // iftrue(Label_0709:, !iterator.hasNext())
                            // iftrue(Label_0709:, revokedCertificates == null)
                            while (true) {
                                final Iterator<Object> iterator;
                                Block_15: {
                                    break Block_15;
                                    final Set revokedCertificates = this.getRevokedCertificates();
                                    iterator = revokedCertificates.iterator();
                                    continue;
                                    sb.append(asn1ObjectIdentifier.getId());
                                    sb.append(" value = ");
                                    str2 = "*****";
                                    continue Label_0330_Outer;
                                }
                                sb.append(iterator.next());
                                sb.append(lineSeparator);
                                continue;
                            }
                            Label_0709: {
                                return sb.toString();
                            }
                        }
                        catch (Exception ex) {}
                        break;
                    }
                    break;
                }
            }
            continue;
        }
    }
    
    @Override
    public void verify(final PublicKey publicKey) throws CRLException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException, SignatureException {
    Label_0017_Outer:
        while (true) {
            while (true) {
                try {
                    Signature signature = this.bcHelper.createSignature(this.getSigAlgName());
                    while (true) {
                        this.doVerify(publicKey, signature);
                        return;
                        signature = Signature.getInstance(this.getSigAlgName());
                        continue Label_0017_Outer;
                    }
                }
                catch (Exception ex) {}
                continue;
            }
        }
    }
    
    @Override
    public void verify(final PublicKey publicKey, final String provider) throws CRLException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException, SignatureException {
        Signature signature;
        if (provider != null) {
            signature = Signature.getInstance(this.getSigAlgName(), provider);
        }
        else {
            signature = Signature.getInstance(this.getSigAlgName());
        }
        this.doVerify(publicKey, signature);
    }
    
    @Override
    public void verify(final PublicKey publicKey, final Provider provider) throws CRLException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        Signature signature;
        if (provider != null) {
            signature = Signature.getInstance(this.getSigAlgName(), provider);
        }
        else {
            signature = Signature.getInstance(this.getSigAlgName());
        }
        this.doVerify(publicKey, signature);
    }
}
