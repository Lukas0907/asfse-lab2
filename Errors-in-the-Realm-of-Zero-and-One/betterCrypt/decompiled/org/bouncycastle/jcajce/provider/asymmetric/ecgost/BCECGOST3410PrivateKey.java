// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.ecgost;

import java.util.Enumeration;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.asn1.x9.X9ECPoint;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x9.X962Parameters;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.jce.spec.ECNamedCurveSpec;
import org.bouncycastle.jce.ECGOST3410NamedCurveTable;
import org.bouncycastle.asn1.cryptopro.ECGOST3410NamedCurves;
import org.bouncycastle.asn1.cryptopro.GOST3410PublicKeyAlgParameters;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.ASN1Primitive;
import java.io.IOException;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import java.security.spec.ECPrivateKeySpec;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.ASN1Encodable;
import java.security.spec.ECParameterSpec;
import java.math.BigInteger;
import org.bouncycastle.jcajce.provider.asymmetric.util.PKCS12BagAttributeCarrierImpl;
import org.bouncycastle.jce.interfaces.ECPointEncoder;
import org.bouncycastle.jce.interfaces.PKCS12BagAttributeCarrier;
import java.security.interfaces.ECPrivateKey;

public class BCECGOST3410PrivateKey implements ECPrivateKey, org.bouncycastle.jce.interfaces.ECPrivateKey, PKCS12BagAttributeCarrier, ECPointEncoder
{
    static final long serialVersionUID = 7245981689601667138L;
    private String algorithm;
    private transient PKCS12BagAttributeCarrierImpl attrCarrier;
    private transient BigInteger d;
    private transient ECParameterSpec ecSpec;
    private transient ASN1Encodable gostParams;
    private transient DERBitString publicKey;
    private boolean withCompression;
    
    protected BCECGOST3410PrivateKey() {
        this.algorithm = "ECGOST3410";
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
    }
    
    public BCECGOST3410PrivateKey(final String algorithm, final ECPrivateKeyParameters ecPrivateKeyParameters) {
        this.algorithm = "ECGOST3410";
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
        this.algorithm = algorithm;
        this.d = ecPrivateKeyParameters.getD();
        this.ecSpec = null;
    }
    
    public BCECGOST3410PrivateKey(final String algorithm, final ECPrivateKeyParameters ecPrivateKeyParameters, final BCECGOST3410PublicKey bcecgost3410PublicKey, final ECParameterSpec ecParameterSpec) {
        this.algorithm = "ECGOST3410";
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
        this.algorithm = algorithm;
        this.d = ecPrivateKeyParameters.getD();
        ECParameterSpec ecSpec = ecParameterSpec;
        if (ecParameterSpec == null) {
            final ECDomainParameters parameters = ecPrivateKeyParameters.getParameters();
            ecSpec = new ECParameterSpec(EC5Util.convertCurve(parameters.getCurve(), parameters.getSeed()), EC5Util.convertPoint(parameters.getG()), parameters.getN(), parameters.getH().intValue());
        }
        this.ecSpec = ecSpec;
        this.gostParams = bcecgost3410PublicKey.getGostParams();
        this.publicKey = this.getPublicKeyDetails(bcecgost3410PublicKey);
    }
    
    public BCECGOST3410PrivateKey(final String algorithm, final ECPrivateKeyParameters ecPrivateKeyParameters, final BCECGOST3410PublicKey bcecgost3410PublicKey, final org.bouncycastle.jce.spec.ECParameterSpec ecParameterSpec) {
        this.algorithm = "ECGOST3410";
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
        this.algorithm = algorithm;
        this.d = ecPrivateKeyParameters.getD();
        if (ecParameterSpec == null) {
            final ECDomainParameters parameters = ecPrivateKeyParameters.getParameters();
            this.ecSpec = new ECParameterSpec(EC5Util.convertCurve(parameters.getCurve(), parameters.getSeed()), EC5Util.convertPoint(parameters.getG()), parameters.getN(), parameters.getH().intValue());
        }
        else {
            this.ecSpec = new ECParameterSpec(EC5Util.convertCurve(ecParameterSpec.getCurve(), ecParameterSpec.getSeed()), EC5Util.convertPoint(ecParameterSpec.getG()), ecParameterSpec.getN(), ecParameterSpec.getH().intValue());
        }
        this.gostParams = bcecgost3410PublicKey.getGostParams();
        this.publicKey = this.getPublicKeyDetails(bcecgost3410PublicKey);
    }
    
    public BCECGOST3410PrivateKey(final ECPrivateKey ecPrivateKey) {
        this.algorithm = "ECGOST3410";
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
        this.d = ecPrivateKey.getS();
        this.algorithm = ecPrivateKey.getAlgorithm();
        this.ecSpec = ecPrivateKey.getParams();
    }
    
    public BCECGOST3410PrivateKey(final ECPrivateKeySpec ecPrivateKeySpec) {
        this.algorithm = "ECGOST3410";
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
        this.d = ecPrivateKeySpec.getS();
        this.ecSpec = ecPrivateKeySpec.getParams();
    }
    
    BCECGOST3410PrivateKey(final PrivateKeyInfo privateKeyInfo) throws IOException {
        this.algorithm = "ECGOST3410";
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
        this.populateFromPrivKeyInfo(privateKeyInfo);
    }
    
    public BCECGOST3410PrivateKey(final BCECGOST3410PrivateKey bcecgost3410PrivateKey) {
        this.algorithm = "ECGOST3410";
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
        this.d = bcecgost3410PrivateKey.d;
        this.ecSpec = bcecgost3410PrivateKey.ecSpec;
        this.withCompression = bcecgost3410PrivateKey.withCompression;
        this.attrCarrier = bcecgost3410PrivateKey.attrCarrier;
        this.publicKey = bcecgost3410PrivateKey.publicKey;
        this.gostParams = bcecgost3410PrivateKey.gostParams;
    }
    
    public BCECGOST3410PrivateKey(final org.bouncycastle.jce.spec.ECPrivateKeySpec ecPrivateKeySpec) {
        this.algorithm = "ECGOST3410";
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
        this.d = ecPrivateKeySpec.getD();
        ECParameterSpec convertSpec;
        if (ecPrivateKeySpec.getParams() != null) {
            convertSpec = EC5Util.convertSpec(EC5Util.convertCurve(ecPrivateKeySpec.getParams().getCurve(), ecPrivateKeySpec.getParams().getSeed()), ecPrivateKeySpec.getParams());
        }
        else {
            convertSpec = null;
        }
        this.ecSpec = convertSpec;
    }
    
    private void extractBytes(final byte[] array, final int n, final BigInteger bigInteger) {
        final byte[] byteArray = bigInteger.toByteArray();
        final int length = byteArray.length;
        int i;
        final int n2 = i = 0;
        byte[] array2 = byteArray;
        if (length < 32) {
            array2 = new byte[32];
            System.arraycopy(byteArray, 0, array2, array2.length - byteArray.length, byteArray.length);
            i = n2;
        }
        while (i != 32) {
            array[n + i] = array2[array2.length - 1 - i];
            ++i;
        }
    }
    
    private DERBitString getPublicKeyDetails(final BCECGOST3410PublicKey bcecgost3410PublicKey) {
        try {
            return SubjectPublicKeyInfo.getInstance(ASN1Primitive.fromByteArray(bcecgost3410PublicKey.getEncoded())).getPublicKeyData();
        }
        catch (IOException ex) {
            return null;
        }
    }
    
    private void populateFromPrivKeyInfo(final PrivateKeyInfo privateKeyInfo) throws IOException {
        final ASN1Encodable parameters = privateKeyInfo.getPrivateKeyAlgorithm().getParameters();
        final ASN1Primitive asn1Primitive = parameters.toASN1Primitive();
        final boolean b = asn1Primitive instanceof ASN1Sequence;
        int i = 0;
        BigInteger d;
        if (b && (ASN1Sequence.getInstance(asn1Primitive).size() == 2 || ASN1Sequence.getInstance(asn1Primitive).size() == 3)) {
            final GOST3410PublicKeyAlgParameters instance = GOST3410PublicKeyAlgParameters.getInstance(parameters);
            this.gostParams = instance;
            final ECNamedCurveParameterSpec parameterSpec = ECGOST3410NamedCurveTable.getParameterSpec(ECGOST3410NamedCurves.getName(instance.getPublicKeyParamSet()));
            this.ecSpec = new ECNamedCurveSpec(ECGOST3410NamedCurves.getName(instance.getPublicKeyParamSet()), EC5Util.convertCurve(parameterSpec.getCurve(), parameterSpec.getSeed()), EC5Util.convertPoint(parameterSpec.getG()), parameterSpec.getN(), parameterSpec.getH());
            final ASN1Encodable privateKey = privateKeyInfo.parsePrivateKey();
            if (privateKey instanceof ASN1Integer) {
                d = ASN1Integer.getInstance(privateKey).getPositiveValue();
            }
            else {
                final byte[] octets = ASN1OctetString.getInstance(privateKey).getOctets();
                final byte[] magnitude = new byte[octets.length];
                while (i != octets.length) {
                    magnitude[i] = octets[octets.length - 1 - i];
                    ++i;
                }
                d = new BigInteger(1, magnitude);
            }
        }
        else {
            final X962Parameters instance2 = X962Parameters.getInstance(parameters);
            Label_0433: {
                ECParameterSpec ecSpec;
                if (instance2.isNamedCurve()) {
                    final ASN1ObjectIdentifier instance3 = ASN1ObjectIdentifier.getInstance(instance2.getParameters());
                    X9ECParameters namedCurveByOid = ECUtil.getNamedCurveByOid(instance3);
                    String s;
                    if (namedCurveByOid == null) {
                        final ECDomainParameters byOID = ECGOST3410NamedCurves.getByOID(instance3);
                        namedCurveByOid = new X9ECParameters(byOID.getCurve(), new X9ECPoint(byOID.getG(), false), byOID.getN(), byOID.getH(), byOID.getSeed());
                        s = ECGOST3410NamedCurves.getName(instance3);
                    }
                    else {
                        s = ECUtil.getCurveName(instance3);
                    }
                    ecSpec = new ECNamedCurveSpec(s, EC5Util.convertCurve(namedCurveByOid.getCurve(), namedCurveByOid.getSeed()), EC5Util.convertPoint(namedCurveByOid.getG()), namedCurveByOid.getN(), namedCurveByOid.getH());
                }
                else {
                    if (!instance2.isImplicitlyCA()) {
                        final X9ECParameters instance4 = X9ECParameters.getInstance(instance2.getParameters());
                        this.ecSpec = new ECParameterSpec(EC5Util.convertCurve(instance4.getCurve(), instance4.getSeed()), EC5Util.convertPoint(instance4.getG()), instance4.getN(), instance4.getH().intValue());
                        break Label_0433;
                    }
                    ecSpec = null;
                }
                this.ecSpec = ecSpec;
            }
            final ASN1Encodable privateKey2 = privateKeyInfo.parsePrivateKey();
            if (!(privateKey2 instanceof ASN1Integer)) {
                final org.bouncycastle.asn1.sec.ECPrivateKey instance5 = org.bouncycastle.asn1.sec.ECPrivateKey.getInstance(privateKey2);
                this.d = instance5.getKey();
                this.publicKey = instance5.getPublicKey();
                return;
            }
            d = ASN1Integer.getInstance(privateKey2).getValue();
        }
        this.d = d;
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.populateFromPrivKeyInfo(PrivateKeyInfo.getInstance(ASN1Primitive.fromByteArray((byte[])objectInputStream.readObject())));
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.getEncoded());
    }
    
    org.bouncycastle.jce.spec.ECParameterSpec engineGetSpec() {
        final ECParameterSpec ecSpec = this.ecSpec;
        if (ecSpec != null) {
            return EC5Util.convertSpec(ecSpec);
        }
        return BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa();
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof BCECGOST3410PrivateKey;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final BCECGOST3410PrivateKey bcecgost3410PrivateKey = (BCECGOST3410PrivateKey)o;
        boolean b3 = b2;
        if (this.getD().equals(bcecgost3410PrivateKey.getD())) {
            b3 = b2;
            if (this.engineGetSpec().equals(bcecgost3410PrivateKey.engineGetSpec())) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public String getAlgorithm() {
        return this.algorithm;
    }
    
    @Override
    public ASN1Encodable getBagAttribute(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        return this.attrCarrier.getBagAttribute(asn1ObjectIdentifier);
    }
    
    @Override
    public Enumeration getBagAttributeKeys() {
        return this.attrCarrier.getBagAttributeKeys();
    }
    
    @Override
    public BigInteger getD() {
        return this.d;
    }
    
    @Override
    public byte[] getEncoded() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost/BCECGOST3410PrivateKey.gostParams:Lorg/bouncycastle/asn1/ASN1Encodable;
        //     4: ifnull          60
        //     7: bipush          32
        //     9: newarray        B
        //    11: astore_2       
        //    12: aload_0        
        //    13: aload_2        
        //    14: iconst_0       
        //    15: aload_0        
        //    16: invokevirtual   org/bouncycastle/jcajce/provider/asymmetric/ecgost/BCECGOST3410PrivateKey.getS:()Ljava/math/BigInteger;
        //    19: invokespecial   org/bouncycastle/jcajce/provider/asymmetric/ecgost/BCECGOST3410PrivateKey.extractBytes:([BILjava/math/BigInteger;)V
        //    22: new             Lorg/bouncycastle/asn1/pkcs/PrivateKeyInfo;
        //    25: dup            
        //    26: new             Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //    29: dup            
        //    30: getstatic       org/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers.gostR3410_2001:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //    33: aload_0        
        //    34: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost/BCECGOST3410PrivateKey.gostParams:Lorg/bouncycastle/asn1/ASN1Encodable;
        //    37: invokespecial   org/bouncycastle/asn1/x509/AlgorithmIdentifier.<init>:(Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;Lorg/bouncycastle/asn1/ASN1Encodable;)V
        //    40: new             Lorg/bouncycastle/asn1/DEROctetString;
        //    43: dup            
        //    44: aload_2        
        //    45: invokespecial   org/bouncycastle/asn1/DEROctetString.<init>:([B)V
        //    48: invokespecial   org/bouncycastle/asn1/pkcs/PrivateKeyInfo.<init>:(Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;Lorg/bouncycastle/asn1/ASN1Encodable;)V
        //    51: ldc_w           "DER"
        //    54: invokevirtual   org/bouncycastle/asn1/pkcs/PrivateKeyInfo.getEncoded:(Ljava/lang/String;)[B
        //    57: astore_2       
        //    58: aload_2        
        //    59: areturn        
        //    60: aload_0        
        //    61: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost/BCECGOST3410PrivateKey.ecSpec:Ljava/security/spec/ECParameterSpec;
        //    64: astore_2       
        //    65: aload_2        
        //    66: instanceof      Lorg/bouncycastle/jce/spec/ECNamedCurveSpec;
        //    69: ifeq            119
        //    72: aload_2        
        //    73: checkcast       Lorg/bouncycastle/jce/spec/ECNamedCurveSpec;
        //    76: invokevirtual   org/bouncycastle/jce/spec/ECNamedCurveSpec.getName:()Ljava/lang/String;
        //    79: invokestatic    org/bouncycastle/jcajce/provider/asymmetric/util/ECUtil.getNamedCurveOid:(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //    82: astore_3       
        //    83: aload_3        
        //    84: astore_2       
        //    85: aload_3        
        //    86: ifnonnull       107
        //    89: new             Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //    92: dup            
        //    93: aload_0        
        //    94: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost/BCECGOST3410PrivateKey.ecSpec:Ljava/security/spec/ECParameterSpec;
        //    97: checkcast       Lorg/bouncycastle/jce/spec/ECNamedCurveSpec;
        //   100: invokevirtual   org/bouncycastle/jce/spec/ECNamedCurveSpec.getName:()Ljava/lang/String;
        //   103: invokespecial   org/bouncycastle/asn1/ASN1ObjectIdentifier.<init>:(Ljava/lang/String;)V
        //   106: astore_2       
        //   107: new             Lorg/bouncycastle/asn1/x9/X962Parameters;
        //   110: dup            
        //   111: aload_2        
        //   112: invokespecial   org/bouncycastle/asn1/x9/X962Parameters.<init>:(Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;)V
        //   115: astore_2       
        //   116: goto            223
        //   119: aload_2        
        //   120: ifnonnull       149
        //   123: new             Lorg/bouncycastle/asn1/x9/X962Parameters;
        //   126: dup            
        //   127: getstatic       org/bouncycastle/asn1/DERNull.INSTANCE:Lorg/bouncycastle/asn1/DERNull;
        //   130: invokespecial   org/bouncycastle/asn1/x9/X962Parameters.<init>:(Lorg/bouncycastle/asn1/ASN1Null;)V
        //   133: astore_2       
        //   134: getstatic       org/bouncycastle/jce/provider/BouncyCastleProvider.CONFIGURATION:Lorg/bouncycastle/jcajce/provider/config/ProviderConfiguration;
        //   137: aconst_null    
        //   138: aload_0        
        //   139: invokevirtual   org/bouncycastle/jcajce/provider/asymmetric/ecgost/BCECGOST3410PrivateKey.getS:()Ljava/math/BigInteger;
        //   142: invokestatic    org/bouncycastle/jcajce/provider/asymmetric/util/ECUtil.getOrderBitLength:(Lorg/bouncycastle/jcajce/provider/config/ProviderConfiguration;Ljava/math/BigInteger;Ljava/math/BigInteger;)I
        //   145: istore_1       
        //   146: goto            241
        //   149: aload_2        
        //   150: invokevirtual   java/security/spec/ECParameterSpec.getCurve:()Ljava/security/spec/EllipticCurve;
        //   153: invokestatic    org/bouncycastle/jcajce/provider/asymmetric/util/EC5Util.convertCurve:(Ljava/security/spec/EllipticCurve;)Lorg/bouncycastle/math/ec/ECCurve;
        //   156: astore_2       
        //   157: new             Lorg/bouncycastle/asn1/x9/X962Parameters;
        //   160: dup            
        //   161: new             Lorg/bouncycastle/asn1/x9/X9ECParameters;
        //   164: dup            
        //   165: aload_2        
        //   166: new             Lorg/bouncycastle/asn1/x9/X9ECPoint;
        //   169: dup            
        //   170: aload_2        
        //   171: aload_0        
        //   172: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost/BCECGOST3410PrivateKey.ecSpec:Ljava/security/spec/ECParameterSpec;
        //   175: invokevirtual   java/security/spec/ECParameterSpec.getGenerator:()Ljava/security/spec/ECPoint;
        //   178: invokestatic    org/bouncycastle/jcajce/provider/asymmetric/util/EC5Util.convertPoint:(Lorg/bouncycastle/math/ec/ECCurve;Ljava/security/spec/ECPoint;)Lorg/bouncycastle/math/ec/ECPoint;
        //   181: aload_0        
        //   182: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost/BCECGOST3410PrivateKey.withCompression:Z
        //   185: invokespecial   org/bouncycastle/asn1/x9/X9ECPoint.<init>:(Lorg/bouncycastle/math/ec/ECPoint;Z)V
        //   188: aload_0        
        //   189: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost/BCECGOST3410PrivateKey.ecSpec:Ljava/security/spec/ECParameterSpec;
        //   192: invokevirtual   java/security/spec/ECParameterSpec.getOrder:()Ljava/math/BigInteger;
        //   195: aload_0        
        //   196: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost/BCECGOST3410PrivateKey.ecSpec:Ljava/security/spec/ECParameterSpec;
        //   199: invokevirtual   java/security/spec/ECParameterSpec.getCofactor:()I
        //   202: i2l            
        //   203: invokestatic    java/math/BigInteger.valueOf:(J)Ljava/math/BigInteger;
        //   206: aload_0        
        //   207: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost/BCECGOST3410PrivateKey.ecSpec:Ljava/security/spec/ECParameterSpec;
        //   210: invokevirtual   java/security/spec/ECParameterSpec.getCurve:()Ljava/security/spec/EllipticCurve;
        //   213: invokevirtual   java/security/spec/EllipticCurve.getSeed:()[B
        //   216: invokespecial   org/bouncycastle/asn1/x9/X9ECParameters.<init>:(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/asn1/x9/X9ECPoint;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V
        //   219: invokespecial   org/bouncycastle/asn1/x9/X962Parameters.<init>:(Lorg/bouncycastle/asn1/x9/X9ECParameters;)V
        //   222: astore_2       
        //   223: getstatic       org/bouncycastle/jce/provider/BouncyCastleProvider.CONFIGURATION:Lorg/bouncycastle/jcajce/provider/config/ProviderConfiguration;
        //   226: aload_0        
        //   227: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost/BCECGOST3410PrivateKey.ecSpec:Ljava/security/spec/ECParameterSpec;
        //   230: invokevirtual   java/security/spec/ECParameterSpec.getOrder:()Ljava/math/BigInteger;
        //   233: aload_0        
        //   234: invokevirtual   org/bouncycastle/jcajce/provider/asymmetric/ecgost/BCECGOST3410PrivateKey.getS:()Ljava/math/BigInteger;
        //   237: invokestatic    org/bouncycastle/jcajce/provider/asymmetric/util/ECUtil.getOrderBitLength:(Lorg/bouncycastle/jcajce/provider/config/ProviderConfiguration;Ljava/math/BigInteger;Ljava/math/BigInteger;)I
        //   240: istore_1       
        //   241: aload_0        
        //   242: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost/BCECGOST3410PrivateKey.publicKey:Lorg/bouncycastle/asn1/DERBitString;
        //   245: ifnull          269
        //   248: new             Lorg/bouncycastle/asn1/sec/ECPrivateKey;
        //   251: dup            
        //   252: iload_1        
        //   253: aload_0        
        //   254: invokevirtual   org/bouncycastle/jcajce/provider/asymmetric/ecgost/BCECGOST3410PrivateKey.getS:()Ljava/math/BigInteger;
        //   257: aload_0        
        //   258: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost/BCECGOST3410PrivateKey.publicKey:Lorg/bouncycastle/asn1/DERBitString;
        //   261: aload_2        
        //   262: invokespecial   org/bouncycastle/asn1/sec/ECPrivateKey.<init>:(ILjava/math/BigInteger;Lorg/bouncycastle/asn1/DERBitString;Lorg/bouncycastle/asn1/ASN1Encodable;)V
        //   265: astore_3       
        //   266: goto            283
        //   269: new             Lorg/bouncycastle/asn1/sec/ECPrivateKey;
        //   272: dup            
        //   273: iload_1        
        //   274: aload_0        
        //   275: invokevirtual   org/bouncycastle/jcajce/provider/asymmetric/ecgost/BCECGOST3410PrivateKey.getS:()Ljava/math/BigInteger;
        //   278: aload_2        
        //   279: invokespecial   org/bouncycastle/asn1/sec/ECPrivateKey.<init>:(ILjava/math/BigInteger;Lorg/bouncycastle/asn1/ASN1Encodable;)V
        //   282: astore_3       
        //   283: new             Lorg/bouncycastle/asn1/pkcs/PrivateKeyInfo;
        //   286: dup            
        //   287: new             Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //   290: dup            
        //   291: getstatic       org/bouncycastle/asn1/cryptopro/CryptoProObjectIdentifiers.gostR3410_2001:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //   294: aload_2        
        //   295: invokevirtual   org/bouncycastle/asn1/x9/X962Parameters.toASN1Primitive:()Lorg/bouncycastle/asn1/ASN1Primitive;
        //   298: invokespecial   org/bouncycastle/asn1/x509/AlgorithmIdentifier.<init>:(Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;Lorg/bouncycastle/asn1/ASN1Encodable;)V
        //   301: aload_3        
        //   302: invokevirtual   org/bouncycastle/asn1/sec/ECPrivateKey.toASN1Primitive:()Lorg/bouncycastle/asn1/ASN1Primitive;
        //   305: invokespecial   org/bouncycastle/asn1/pkcs/PrivateKeyInfo.<init>:(Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;Lorg/bouncycastle/asn1/ASN1Encodable;)V
        //   308: ldc_w           "DER"
        //   311: invokevirtual   org/bouncycastle/asn1/pkcs/PrivateKeyInfo.getEncoded:(Ljava/lang/String;)[B
        //   314: astore_2       
        //   315: aload_2        
        //   316: areturn        
        //   317: astore_2       
        //   318: aconst_null    
        //   319: areturn        
        //   320: astore_2       
        //   321: aconst_null    
        //   322: areturn        
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  22     58     317    320    Ljava/io/IOException;
        //  283    315    320    323    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0283:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public String getFormat() {
        return "PKCS#8";
    }
    
    @Override
    public org.bouncycastle.jce.spec.ECParameterSpec getParameters() {
        final ECParameterSpec ecSpec = this.ecSpec;
        if (ecSpec == null) {
            return null;
        }
        return EC5Util.convertSpec(ecSpec);
    }
    
    @Override
    public ECParameterSpec getParams() {
        return this.ecSpec;
    }
    
    @Override
    public BigInteger getS() {
        return this.d;
    }
    
    @Override
    public int hashCode() {
        return this.getD().hashCode() ^ this.engineGetSpec().hashCode();
    }
    
    @Override
    public void setBagAttribute(final ASN1ObjectIdentifier asn1ObjectIdentifier, final ASN1Encodable asn1Encodable) {
        this.attrCarrier.setBagAttribute(asn1ObjectIdentifier, asn1Encodable);
    }
    
    @Override
    public void setPointFormat(final String anotherString) {
        this.withCompression = ("UNCOMPRESSED".equalsIgnoreCase(anotherString) ^ true);
    }
    
    @Override
    public String toString() {
        return ECUtil.privateKeyToString(this.algorithm, this.d, this.engineGetSpec());
    }
}
