// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.gost;

import org.bouncycastle.util.Strings;
import org.bouncycastle.util.Fingerprint;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.params.GOST3410Parameters;
import java.math.BigInteger;

class GOSTUtil
{
    private static String generateKeyFingerprint(final BigInteger bigInteger, final GOST3410Parameters gost3410Parameters) {
        return new Fingerprint(Arrays.concatenate(bigInteger.toByteArray(), gost3410Parameters.getP().toByteArray(), gost3410Parameters.getA().toByteArray())).toString();
    }
    
    static String privateKeyToString(final String str, BigInteger modPow, final GOST3410Parameters gost3410Parameters) {
        final StringBuffer sb = new StringBuffer();
        final String lineSeparator = Strings.lineSeparator();
        modPow = gost3410Parameters.getA().modPow(modPow, gost3410Parameters.getP());
        sb.append(str);
        sb.append(" Private Key [");
        sb.append(generateKeyFingerprint(modPow, gost3410Parameters));
        sb.append("]");
        sb.append(lineSeparator);
        sb.append("                  Y: ");
        sb.append(modPow.toString(16));
        sb.append(lineSeparator);
        return sb.toString();
    }
    
    static String publicKeyToString(final String str, final BigInteger bigInteger, final GOST3410Parameters gost3410Parameters) {
        final StringBuffer sb = new StringBuffer();
        final String lineSeparator = Strings.lineSeparator();
        sb.append(str);
        sb.append(" Public Key [");
        sb.append(generateKeyFingerprint(bigInteger, gost3410Parameters));
        sb.append("]");
        sb.append(lineSeparator);
        sb.append("                 Y: ");
        sb.append(bigInteger.toString(16));
        sb.append(lineSeparator);
        return sb.toString();
    }
}
