// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.x509;

import java.security.cert.CRLException;
import org.bouncycastle.asn1.x509.CertificateList;
import org.bouncycastle.jcajce.util.JcaJceHelper;

class X509CRLInternal extends X509CRLImpl
{
    private final byte[] encoding;
    
    X509CRLInternal(final JcaJceHelper jcaJceHelper, final CertificateList list, final String s, final byte[] array, final boolean b, final byte[] encoding) {
        super(jcaJceHelper, list, s, array, b);
        this.encoding = encoding;
    }
    
    @Override
    public byte[] getEncoded() throws CRLException {
        final byte[] encoding = this.encoding;
        if (encoding != null) {
            return encoding;
        }
        throw new CRLException();
    }
}
