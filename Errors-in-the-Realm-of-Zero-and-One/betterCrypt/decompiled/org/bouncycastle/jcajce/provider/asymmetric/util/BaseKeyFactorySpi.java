// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.util;

import java.security.Key;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import java.security.spec.X509EncodedKeySpec;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.PrivateKey;
import java.security.spec.KeySpec;
import org.bouncycastle.jcajce.provider.util.AsymmetricKeyInfoConverter;
import java.security.KeyFactorySpi;

public abstract class BaseKeyFactorySpi extends KeyFactorySpi implements AsymmetricKeyInfoConverter
{
    @Override
    protected PrivateKey engineGeneratePrivate(final KeySpec keySpec) throws InvalidKeySpecException {
        if (keySpec instanceof PKCS8EncodedKeySpec) {
            try {
                return this.generatePrivate(PrivateKeyInfo.getInstance(((PKCS8EncodedKeySpec)keySpec).getEncoded()));
            }
            catch (Exception ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("encoded key spec not recognized: ");
                sb.append(ex.getMessage());
                throw new InvalidKeySpecException(sb.toString());
            }
        }
        throw new InvalidKeySpecException("key spec not recognized");
    }
    
    @Override
    protected PublicKey engineGeneratePublic(final KeySpec keySpec) throws InvalidKeySpecException {
        if (keySpec instanceof X509EncodedKeySpec) {
            try {
                return this.generatePublic(SubjectPublicKeyInfo.getInstance(((X509EncodedKeySpec)keySpec).getEncoded()));
            }
            catch (Exception ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("encoded key spec not recognized: ");
                sb.append(ex.getMessage());
                throw new InvalidKeySpecException(sb.toString());
            }
        }
        throw new InvalidKeySpecException("key spec not recognized");
    }
    
    @Override
    protected KeySpec engineGetKeySpec(final Key obj, final Class obj2) throws InvalidKeySpecException {
        if (obj2.isAssignableFrom(PKCS8EncodedKeySpec.class) && obj.getFormat().equals("PKCS#8")) {
            return new PKCS8EncodedKeySpec(obj.getEncoded());
        }
        if (obj2.isAssignableFrom(X509EncodedKeySpec.class) && obj.getFormat().equals("X.509")) {
            return new X509EncodedKeySpec(obj.getEncoded());
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("not implemented yet ");
        sb.append(obj);
        sb.append(" ");
        sb.append(obj2);
        throw new InvalidKeySpecException(sb.toString());
    }
}
