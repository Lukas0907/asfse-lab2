// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.util;

import org.bouncycastle.util.Arrays;
import java.io.ByteArrayOutputStream;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.security.InvalidKeyException;
import javax.crypto.NoSuchPaddingException;
import java.security.Key;
import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import org.bouncycastle.jcajce.util.BCJcaJceHelper;
import javax.crypto.spec.RC5ParameterSpec;
import javax.crypto.spec.RC2ParameterSpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import org.bouncycastle.crypto.Wrapper;
import org.bouncycastle.jcajce.util.JcaJceHelper;
import java.security.AlgorithmParameters;
import javax.crypto.CipherSpi;

public abstract class BaseCipherSpi extends CipherSpi
{
    private Class[] availableSpecs;
    protected AlgorithmParameters engineParams;
    private final JcaJceHelper helper;
    private byte[] iv;
    private int ivSize;
    protected Wrapper wrapEngine;
    
    protected BaseCipherSpi() {
        this.availableSpecs = new Class[] { IvParameterSpec.class, PBEParameterSpec.class, RC2ParameterSpec.class, RC5ParameterSpec.class };
        this.helper = new BCJcaJceHelper();
        this.engineParams = null;
        this.wrapEngine = null;
    }
    
    protected final AlgorithmParameters createParametersInstance(final String s) throws NoSuchAlgorithmException, NoSuchProviderException {
        return this.helper.createAlgorithmParameters(s);
    }
    
    @Override
    protected int engineGetBlockSize() {
        return 0;
    }
    
    @Override
    protected byte[] engineGetIV() {
        return null;
    }
    
    @Override
    protected int engineGetKeySize(final Key key) {
        return key.getEncoded().length;
    }
    
    @Override
    protected int engineGetOutputSize(final int n) {
        return -1;
    }
    
    @Override
    protected AlgorithmParameters engineGetParameters() {
        return null;
    }
    
    @Override
    protected void engineSetMode(final String str) throws NoSuchAlgorithmException {
        final StringBuilder sb = new StringBuilder();
        sb.append("can't support mode ");
        sb.append(str);
        throw new NoSuchAlgorithmException(sb.toString());
    }
    
    @Override
    protected void engineSetPadding(final String str) throws NoSuchPaddingException {
        final StringBuilder sb = new StringBuilder();
        sb.append("Padding ");
        sb.append(str);
        sb.append(" unknown.");
        throw new NoSuchPaddingException(sb.toString());
    }
    
    @Override
    protected Key engineUnwrap(final byte[] p0, final String p1, final int p2) throws InvalidKeyException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        org/bouncycastle/jcajce/provider/asymmetric/util/BaseCipherSpi.wrapEngine:Lorg/bouncycastle/crypto/Wrapper;
        //     4: ifnonnull       19
        //     7: aload_0        
        //     8: aload_1        
        //     9: iconst_0       
        //    10: aload_1        
        //    11: arraylength    
        //    12: invokevirtual   org/bouncycastle/jcajce/provider/asymmetric/util/BaseCipherSpi.engineDoFinal:([BII)[B
        //    15: astore_1       
        //    16: goto            33
        //    19: aload_0        
        //    20: getfield        org/bouncycastle/jcajce/provider/asymmetric/util/BaseCipherSpi.wrapEngine:Lorg/bouncycastle/crypto/Wrapper;
        //    23: aload_1        
        //    24: iconst_0       
        //    25: aload_1        
        //    26: arraylength    
        //    27: invokeinterface org/bouncycastle/crypto/Wrapper.unwrap:([BII)[B
        //    32: astore_1       
        //    33: iload_3        
        //    34: iconst_3       
        //    35: if_icmpne       48
        //    38: new             Ljavax/crypto/spec/SecretKeySpec;
        //    41: dup            
        //    42: aload_1        
        //    43: aload_2        
        //    44: invokespecial   javax/crypto/spec/SecretKeySpec.<init>:([BLjava/lang/String;)V
        //    47: areturn        
        //    48: aload_2        
        //    49: ldc             ""
        //    51: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    54: ifeq            134
        //    57: iload_3        
        //    58: iconst_2       
        //    59: if_icmpne       134
        //    62: aload_1        
        //    63: invokestatic    org/bouncycastle/asn1/pkcs/PrivateKeyInfo.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/pkcs/PrivateKeyInfo;
        //    66: astore_1       
        //    67: aload_1        
        //    68: invokestatic    org/bouncycastle/jce/provider/BouncyCastleProvider.getPrivateKey:(Lorg/bouncycastle/asn1/pkcs/PrivateKeyInfo;)Ljava/security/PrivateKey;
        //    71: astore_2       
        //    72: aload_2        
        //    73: ifnull          78
        //    76: aload_2        
        //    77: areturn        
        //    78: new             Ljava/lang/StringBuilder;
        //    81: dup            
        //    82: invokespecial   java/lang/StringBuilder.<init>:()V
        //    85: astore_2       
        //    86: aload_2        
        //    87: ldc             "algorithm "
        //    89: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    92: pop            
        //    93: aload_2        
        //    94: aload_1        
        //    95: invokevirtual   org/bouncycastle/asn1/pkcs/PrivateKeyInfo.getPrivateKeyAlgorithm:()Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //    98: invokevirtual   org/bouncycastle/asn1/x509/AlgorithmIdentifier.getAlgorithm:()Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //   101: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   104: pop            
        //   105: aload_2        
        //   106: ldc             " not supported"
        //   108: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   111: pop            
        //   112: new             Ljava/security/InvalidKeyException;
        //   115: dup            
        //   116: aload_2        
        //   117: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   120: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   123: athrow         
        //   124: new             Ljava/security/InvalidKeyException;
        //   127: dup            
        //   128: ldc             "Invalid key encoding."
        //   130: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   133: athrow         
        //   134: aload_0        
        //   135: getfield        org/bouncycastle/jcajce/provider/asymmetric/util/BaseCipherSpi.helper:Lorg/bouncycastle/jcajce/util/JcaJceHelper;
        //   138: aload_2        
        //   139: invokeinterface org/bouncycastle/jcajce/util/JcaJceHelper.createKeyFactory:(Ljava/lang/String;)Ljava/security/KeyFactory;
        //   144: astore_2       
        //   145: iload_3        
        //   146: iconst_1       
        //   147: if_icmpne       163
        //   150: aload_2        
        //   151: new             Ljava/security/spec/X509EncodedKeySpec;
        //   154: dup            
        //   155: aload_1        
        //   156: invokespecial   java/security/spec/X509EncodedKeySpec.<init>:([B)V
        //   159: invokevirtual   java/security/KeyFactory.generatePublic:(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
        //   162: areturn        
        //   163: iload_3        
        //   164: iconst_2       
        //   165: if_icmpne       183
        //   168: aload_2        
        //   169: new             Ljava/security/spec/PKCS8EncodedKeySpec;
        //   172: dup            
        //   173: aload_1        
        //   174: invokespecial   java/security/spec/PKCS8EncodedKeySpec.<init>:([B)V
        //   177: invokevirtual   java/security/KeyFactory.generatePrivate:(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;
        //   180: astore_1       
        //   181: aload_1        
        //   182: areturn        
        //   183: new             Ljava/lang/StringBuilder;
        //   186: dup            
        //   187: invokespecial   java/lang/StringBuilder.<init>:()V
        //   190: astore_1       
        //   191: aload_1        
        //   192: ldc             "Unknown key type "
        //   194: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   197: pop            
        //   198: aload_1        
        //   199: iload_3        
        //   200: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   203: pop            
        //   204: new             Ljava/security/InvalidKeyException;
        //   207: dup            
        //   208: aload_1        
        //   209: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   212: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   215: athrow         
        //   216: astore_1       
        //   217: new             Ljava/lang/StringBuilder;
        //   220: dup            
        //   221: invokespecial   java/lang/StringBuilder.<init>:()V
        //   224: astore_2       
        //   225: aload_2        
        //   226: ldc             "Unknown key type "
        //   228: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   231: pop            
        //   232: aload_2        
        //   233: aload_1        
        //   234: invokevirtual   java/security/NoSuchProviderException.getMessage:()Ljava/lang/String;
        //   237: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   240: pop            
        //   241: new             Ljava/security/InvalidKeyException;
        //   244: dup            
        //   245: aload_2        
        //   246: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   249: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   252: athrow         
        //   253: astore_1       
        //   254: new             Ljava/lang/StringBuilder;
        //   257: dup            
        //   258: invokespecial   java/lang/StringBuilder.<init>:()V
        //   261: astore_2       
        //   262: aload_2        
        //   263: ldc             "Unknown key type "
        //   265: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   268: pop            
        //   269: aload_2        
        //   270: aload_1        
        //   271: invokevirtual   java/security/spec/InvalidKeySpecException.getMessage:()Ljava/lang/String;
        //   274: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   277: pop            
        //   278: new             Ljava/security/InvalidKeyException;
        //   281: dup            
        //   282: aload_2        
        //   283: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   286: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   289: athrow         
        //   290: astore_1       
        //   291: new             Ljava/lang/StringBuilder;
        //   294: dup            
        //   295: invokespecial   java/lang/StringBuilder.<init>:()V
        //   298: astore_2       
        //   299: aload_2        
        //   300: ldc             "Unknown key type "
        //   302: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   305: pop            
        //   306: aload_2        
        //   307: aload_1        
        //   308: invokevirtual   java/security/NoSuchAlgorithmException.getMessage:()Ljava/lang/String;
        //   311: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   314: pop            
        //   315: new             Ljava/security/InvalidKeyException;
        //   318: dup            
        //   319: aload_2        
        //   320: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   323: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   326: athrow         
        //   327: astore_1       
        //   328: new             Ljava/security/InvalidKeyException;
        //   331: dup            
        //   332: aload_1        
        //   333: invokevirtual   javax/crypto/IllegalBlockSizeException.getMessage:()Ljava/lang/String;
        //   336: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   339: athrow         
        //   340: astore_1       
        //   341: new             Lorg/bouncycastle/jcajce/provider/asymmetric/util/BaseCipherSpi$1;
        //   344: dup            
        //   345: aload_0        
        //   346: ldc             "unable to unwrap"
        //   348: aload_1        
        //   349: invokespecial   org/bouncycastle/jcajce/provider/asymmetric/util/BaseCipherSpi$1.<init>:(Lorg/bouncycastle/jcajce/provider/asymmetric/util/BaseCipherSpi;Ljava/lang/String;Ljavax/crypto/BadPaddingException;)V
        //   352: athrow         
        //   353: astore_1       
        //   354: new             Ljava/security/InvalidKeyException;
        //   357: dup            
        //   358: aload_1        
        //   359: invokevirtual   org/bouncycastle/crypto/InvalidCipherTextException.getMessage:()Ljava/lang/String;
        //   362: invokespecial   java/security/InvalidKeyException.<init>:(Ljava/lang/String;)V
        //   365: athrow         
        //   366: astore_1       
        //   367: goto            124
        //    Exceptions:
        //  throws java.security.InvalidKeyException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                                
        //  -----  -----  -----  -----  ----------------------------------------------------
        //  0      16     353    366    Lorg/bouncycastle/crypto/InvalidCipherTextException;
        //  0      16     340    353    Ljavax/crypto/BadPaddingException;
        //  0      16     327    340    Ljavax/crypto/IllegalBlockSizeException;
        //  19     33     353    366    Lorg/bouncycastle/crypto/InvalidCipherTextException;
        //  19     33     340    353    Ljavax/crypto/BadPaddingException;
        //  19     33     327    340    Ljavax/crypto/IllegalBlockSizeException;
        //  62     72     366    134    Ljava/lang/Exception;
        //  78     124    366    134    Ljava/lang/Exception;
        //  134    145    290    327    Ljava/security/NoSuchAlgorithmException;
        //  134    145    253    290    Ljava/security/spec/InvalidKeySpecException;
        //  134    145    216    253    Ljava/security/NoSuchProviderException;
        //  150    163    290    327    Ljava/security/NoSuchAlgorithmException;
        //  150    163    253    290    Ljava/security/spec/InvalidKeySpecException;
        //  150    163    216    253    Ljava/security/NoSuchProviderException;
        //  168    181    290    327    Ljava/security/NoSuchAlgorithmException;
        //  168    181    253    290    Ljava/security/spec/InvalidKeySpecException;
        //  168    181    216    253    Ljava/security/NoSuchProviderException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0078:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    protected byte[] engineWrap(final Key key) throws IllegalBlockSizeException, InvalidKeyException {
        final byte[] encoded = key.getEncoded();
        if (encoded != null) {
            try {
                if (this.wrapEngine == null) {
                    return this.engineDoFinal(encoded, 0, encoded.length);
                }
                return this.wrapEngine.wrap(encoded, 0, encoded.length);
            }
            catch (BadPaddingException ex) {
                throw new IllegalBlockSizeException(ex.getMessage());
            }
        }
        throw new InvalidKeyException("Cannot wrap key, null encoding.");
    }
    
    protected static final class ErasableOutputStream extends ByteArrayOutputStream
    {
        public ErasableOutputStream() {
        }
        
        public void erase() {
            Arrays.fill(this.buf, (byte)0);
            this.reset();
        }
        
        public byte[] getBuf() {
            return this.buf;
        }
    }
}
