// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.dh;

import java.security.InvalidAlgorithmParameterException;
import java.security.spec.AlgorithmParameterSpec;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import org.bouncycastle.crypto.params.DHPrivateKeyParameters;
import org.bouncycastle.crypto.params.DHPublicKeyParameters;
import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.jcajce.provider.asymmetric.util.PrimeCertaintyCalculator;
import org.bouncycastle.crypto.generators.DHParametersGenerator;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.Integers;
import java.security.KeyPair;
import java.math.BigInteger;
import org.bouncycastle.crypto.params.DHParameters;
import org.bouncycastle.jcajce.spec.DHDomainParameterSpec;
import javax.crypto.spec.DHParameterSpec;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import java.security.SecureRandom;
import org.bouncycastle.crypto.params.DHKeyGenerationParameters;
import org.bouncycastle.crypto.generators.DHBasicKeyPairGenerator;
import java.util.Hashtable;
import java.security.KeyPairGenerator;

public class KeyPairGeneratorSpi extends KeyPairGenerator
{
    private static Object lock;
    private static Hashtable params;
    DHBasicKeyPairGenerator engine;
    boolean initialised;
    DHKeyGenerationParameters param;
    SecureRandom random;
    int strength;
    
    static {
        KeyPairGeneratorSpi.params = new Hashtable();
        KeyPairGeneratorSpi.lock = new Object();
    }
    
    public KeyPairGeneratorSpi() {
        super("DH");
        this.engine = new DHBasicKeyPairGenerator();
        this.strength = 2048;
        this.random = CryptoServicesRegistrar.getSecureRandom();
        this.initialised = false;
    }
    
    private DHKeyGenerationParameters convertParams(final SecureRandom secureRandom, final DHParameterSpec dhParameterSpec) {
        if (dhParameterSpec instanceof DHDomainParameterSpec) {
            return new DHKeyGenerationParameters(secureRandom, ((DHDomainParameterSpec)dhParameterSpec).getDomainParameters());
        }
        return new DHKeyGenerationParameters(secureRandom, new DHParameters(dhParameterSpec.getP(), dhParameterSpec.getG(), null, dhParameterSpec.getL()));
    }
    
    @Override
    public KeyPair generateKeyPair() {
        if (!this.initialised) {
            final Integer value = Integers.valueOf(this.strength);
            Label_0167: {
                Label_0074: {
                    DHKeyGenerationParameters convertParams;
                    if (KeyPairGeneratorSpi.params.containsKey(value)) {
                        convertParams = KeyPairGeneratorSpi.params.get(value);
                    }
                    else {
                        final DHParameterSpec dhDefaultParameters = BouncyCastleProvider.CONFIGURATION.getDHDefaultParameters(this.strength);
                        if (dhDefaultParameters == null) {
                            break Label_0074;
                        }
                        convertParams = this.convertParams(this.random, dhDefaultParameters);
                    }
                    this.param = convertParams;
                    break Label_0167;
                }
                synchronized (KeyPairGeneratorSpi.lock) {
                    if (KeyPairGeneratorSpi.params.containsKey(value)) {
                        this.param = (DHKeyGenerationParameters)KeyPairGeneratorSpi.params.get(value);
                    }
                    else {
                        final DHParametersGenerator dhParametersGenerator = new DHParametersGenerator();
                        dhParametersGenerator.init(this.strength, PrimeCertaintyCalculator.getDefaultCertainty(this.strength), this.random);
                        this.param = new DHKeyGenerationParameters(this.random, dhParametersGenerator.generateParameters());
                        KeyPairGeneratorSpi.params.put(value, this.param);
                    }
                    // monitorexit(KeyPairGeneratorSpi.lock)
                    this.engine.init(this.param);
                    this.initialised = true;
                }
            }
        }
        final AsymmetricCipherKeyPair generateKeyPair = this.engine.generateKeyPair();
        return new KeyPair(new BCDHPublicKey((DHPublicKeyParameters)generateKeyPair.getPublic()), new BCDHPrivateKey((DHPrivateKeyParameters)generateKeyPair.getPrivate()));
    }
    
    @Override
    public void initialize(final int strength, final SecureRandom random) {
        this.strength = strength;
        this.random = random;
        this.initialised = false;
    }
    
    @Override
    public void initialize(final AlgorithmParameterSpec algorithmParameterSpec, final SecureRandom secureRandom) throws InvalidAlgorithmParameterException {
        if (algorithmParameterSpec instanceof DHParameterSpec) {
            final DHParameterSpec dhParameterSpec = (DHParameterSpec)algorithmParameterSpec;
            try {
                this.param = this.convertParams(secureRandom, dhParameterSpec);
                this.engine.init(this.param);
                this.initialised = true;
                return;
            }
            catch (IllegalArgumentException cause) {
                throw new InvalidAlgorithmParameterException(cause.getMessage(), cause);
            }
        }
        throw new InvalidAlgorithmParameterException("parameter object not a DHParameterSpec");
    }
}
