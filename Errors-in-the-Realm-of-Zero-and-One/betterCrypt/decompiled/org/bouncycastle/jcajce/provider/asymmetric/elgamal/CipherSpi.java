// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.elgamal;

import org.bouncycastle.crypto.encodings.ISO9796d1Encoding;
import org.bouncycastle.crypto.encodings.PKCS1Encoding;
import java.security.NoSuchAlgorithmException;
import org.bouncycastle.util.Strings;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import java.security.InvalidParameterException;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import java.security.PrivateKey;
import javax.crypto.interfaces.DHPrivateKey;
import java.security.PublicKey;
import javax.crypto.interfaces.DHPublicKey;
import java.security.InvalidKeyException;
import java.security.InvalidAlgorithmParameterException;
import java.security.SecureRandom;
import java.math.BigInteger;
import javax.crypto.interfaces.DHKey;
import org.bouncycastle.jce.interfaces.ElGamalKey;
import java.security.Key;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.ShortBufferException;
import org.bouncycastle.crypto.Digest;
import javax.crypto.NoSuchPaddingException;
import org.bouncycastle.crypto.encodings.OAEPEncoding;
import javax.crypto.spec.PSource;
import org.bouncycastle.crypto.engines.ElGamalEngine;
import org.bouncycastle.jcajce.provider.util.DigestFactory;
import java.security.spec.MGF1ParameterSpec;
import javax.crypto.spec.OAEPParameterSpec;
import javax.crypto.BadPaddingException;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.jcajce.provider.util.BadBlockException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.AlgorithmParameters;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.jcajce.provider.asymmetric.util.BaseCipherSpi;

public class CipherSpi extends BaseCipherSpi
{
    private ErasableOutputStream bOut;
    private AsymmetricBlockCipher cipher;
    private AlgorithmParameters engineParams;
    private AlgorithmParameterSpec paramSpec;
    
    public CipherSpi(final AsymmetricBlockCipher cipher) {
        this.bOut = new ErasableOutputStream();
        this.cipher = cipher;
    }
    
    private byte[] getOutput() throws BadPaddingException {
        try {
            try {
                final byte[] processBlock = this.cipher.processBlock(this.bOut.getBuf(), 0, this.bOut.size());
                this.bOut.erase();
                return processBlock;
            }
            finally {}
        }
        catch (ArrayIndexOutOfBoundsException ex) {
            throw new BadBlockException("unable to decrypt block", ex);
        }
        catch (InvalidCipherTextException ex2) {
            throw new BadBlockException("unable to decrypt block", ex2);
        }
        this.bOut.erase();
    }
    
    private void initFromSpec(final OAEPParameterSpec paramSpec) throws NoSuchPaddingException {
        final MGF1ParameterSpec mgf1ParameterSpec = (MGF1ParameterSpec)paramSpec.getMGFParameters();
        final Digest digest = DigestFactory.getDigest(mgf1ParameterSpec.getDigestAlgorithm());
        if (digest != null) {
            this.cipher = new OAEPEncoding(new ElGamalEngine(), digest, ((PSource.PSpecified)paramSpec.getPSource()).getValue());
            this.paramSpec = paramSpec;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("no match on OAEP constructor for digest algorithm: ");
        sb.append(mgf1ParameterSpec.getDigestAlgorithm());
        throw new NoSuchPaddingException(sb.toString());
    }
    
    @Override
    protected int engineDoFinal(byte[] output, int i, final int len, final byte[] array, final int n) throws IllegalBlockSizeException, BadPaddingException, ShortBufferException {
        if (this.engineGetOutputSize(len) + n <= array.length) {
            if (output != null) {
                this.bOut.write(output, i, len);
            }
            if (this.cipher instanceof ElGamalEngine) {
                if (this.bOut.size() > this.cipher.getInputBlockSize() + 1) {
                    throw new ArrayIndexOutOfBoundsException("too much data for ElGamal block");
                }
            }
            else if (this.bOut.size() > this.cipher.getInputBlockSize()) {
                throw new ArrayIndexOutOfBoundsException("too much data for ElGamal block");
            }
            for (output = this.getOutput(), i = 0; i != output.length; ++i) {
                array[n + i] = output[i];
            }
            return output.length;
        }
        throw new ShortBufferException("output buffer too short for input.");
    }
    
    @Override
    protected byte[] engineDoFinal(final byte[] b, final int off, final int len) throws IllegalBlockSizeException, BadPaddingException {
        if (b != null) {
            this.bOut.write(b, off, len);
        }
        if (this.cipher instanceof ElGamalEngine) {
            if (this.bOut.size() > this.cipher.getInputBlockSize() + 1) {
                throw new ArrayIndexOutOfBoundsException("too much data for ElGamal block");
            }
        }
        else if (this.bOut.size() > this.cipher.getInputBlockSize()) {
            throw new ArrayIndexOutOfBoundsException("too much data for ElGamal block");
        }
        return this.getOutput();
    }
    
    @Override
    protected int engineGetBlockSize() {
        return this.cipher.getInputBlockSize();
    }
    
    @Override
    protected int engineGetKeySize(final Key key) {
        BigInteger bigInteger;
        if (key instanceof ElGamalKey) {
            bigInteger = ((ElGamalKey)key).getParameters().getP();
        }
        else {
            if (!(key instanceof DHKey)) {
                throw new IllegalArgumentException("not an ElGamal key!");
            }
            bigInteger = ((DHKey)key).getParams().getP();
        }
        return bigInteger.bitLength();
    }
    
    @Override
    protected int engineGetOutputSize(final int n) {
        return this.cipher.getOutputBlockSize();
    }
    
    @Override
    protected AlgorithmParameters engineGetParameters() {
        if (this.engineParams == null && this.paramSpec != null) {
            try {
                (this.engineParams = this.createParametersInstance("OAEP")).init(this.paramSpec);
            }
            catch (Exception ex) {
                throw new RuntimeException(ex.toString());
            }
        }
        return this.engineParams;
    }
    
    @Override
    protected void engineInit(final int n, final Key key, final AlgorithmParameters algorithmParameters, final SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        throw new InvalidAlgorithmParameterException("can't handle parameters in ElGamal");
    }
    
    @Override
    protected void engineInit(final int n, final Key key, final SecureRandom secureRandom) throws InvalidKeyException {
        try {
            this.engineInit(n, key, (AlgorithmParameterSpec)null, secureRandom);
        }
        catch (InvalidAlgorithmParameterException cause) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Eeeek! ");
            sb.append(cause.toString());
            throw new InvalidKeyException(sb.toString(), cause);
        }
    }
    
    @Override
    protected void engineInit(final int i, final Key key, final AlgorithmParameterSpec paramSpec, final SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        AsymmetricKeyParameter asymmetricKeyParameter;
        if (key instanceof DHPublicKey) {
            asymmetricKeyParameter = ElGamalUtil.generatePublicKeyParameter((PublicKey)key);
        }
        else {
            if (!(key instanceof DHPrivateKey)) {
                throw new InvalidKeyException("unknown key type passed to ElGamal");
            }
            asymmetricKeyParameter = ElGamalUtil.generatePrivateKeyParameter((PrivateKey)key);
        }
        if (paramSpec instanceof OAEPParameterSpec) {
            final OAEPParameterSpec oaepParameterSpec = (OAEPParameterSpec)paramSpec;
            this.paramSpec = paramSpec;
            if (!oaepParameterSpec.getMGFAlgorithm().equalsIgnoreCase("MGF1") && !oaepParameterSpec.getMGFAlgorithm().equals(PKCSObjectIdentifiers.id_mgf1.getId())) {
                throw new InvalidAlgorithmParameterException("unknown mask generation function specified");
            }
            if (!(oaepParameterSpec.getMGFParameters() instanceof MGF1ParameterSpec)) {
                throw new InvalidAlgorithmParameterException("unkown MGF parameters");
            }
            final Digest digest = DigestFactory.getDigest(oaepParameterSpec.getDigestAlgorithm());
            if (digest == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("no match on digest algorithm: ");
                sb.append(oaepParameterSpec.getDigestAlgorithm());
                throw new InvalidAlgorithmParameterException(sb.toString());
            }
            final MGF1ParameterSpec mgf1ParameterSpec = (MGF1ParameterSpec)oaepParameterSpec.getMGFParameters();
            final Digest digest2 = DigestFactory.getDigest(mgf1ParameterSpec.getDigestAlgorithm());
            if (digest2 == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("no match on MGF digest algorithm: ");
                sb2.append(mgf1ParameterSpec.getDigestAlgorithm());
                throw new InvalidAlgorithmParameterException(sb2.toString());
            }
            this.cipher = new OAEPEncoding(new ElGamalEngine(), digest, digest2, ((PSource.PSpecified)oaepParameterSpec.getPSource()).getValue());
        }
        else if (paramSpec != null) {
            throw new InvalidAlgorithmParameterException("unknown parameter type.");
        }
        CipherParameters cipherParameters = asymmetricKeyParameter;
        if (secureRandom != null) {
            cipherParameters = new ParametersWithRandom(asymmetricKeyParameter, secureRandom);
        }
        boolean b = true;
        AsymmetricBlockCipher asymmetricBlockCipher = null;
        Label_0372: {
            Label_0367: {
                if (i != 1) {
                    if (i != 2) {
                        if (i == 3) {
                            break Label_0367;
                        }
                        if (i != 4) {
                            final StringBuilder sb3 = new StringBuilder();
                            sb3.append("unknown opmode ");
                            sb3.append(i);
                            sb3.append(" passed to ElGamal");
                            throw new InvalidParameterException(sb3.toString());
                        }
                    }
                    asymmetricBlockCipher = this.cipher;
                    b = false;
                    break Label_0372;
                }
            }
            asymmetricBlockCipher = this.cipher;
        }
        asymmetricBlockCipher.init(b, cipherParameters);
    }
    
    @Override
    protected void engineSetMode(final String str) throws NoSuchAlgorithmException {
        final String upperCase = Strings.toUpperCase(str);
        if (upperCase.equals("NONE")) {
            return;
        }
        if (upperCase.equals("ECB")) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("can't support mode ");
        sb.append(str);
        throw new NoSuchAlgorithmException(sb.toString());
    }
    
    @Override
    protected void engineSetPadding(final String str) throws NoSuchPaddingException {
        final String upperCase = Strings.toUpperCase(str);
        AsymmetricBlockCipher cipher;
        if (upperCase.equals("NOPADDING")) {
            cipher = new ElGamalEngine();
        }
        else if (upperCase.equals("PKCS1PADDING")) {
            cipher = new PKCS1Encoding(new ElGamalEngine());
        }
        else {
            if (!upperCase.equals("ISO9796-1PADDING")) {
                OAEPParameterSpec default1 = null;
                Label_0099: {
                    if (!upperCase.equals("OAEPPADDING")) {
                        if (upperCase.equals("OAEPWITHMD5ANDMGF1PADDING")) {
                            default1 = new OAEPParameterSpec("MD5", "MGF1", new MGF1ParameterSpec("MD5"), PSource.PSpecified.DEFAULT);
                            break Label_0099;
                        }
                        if (!upperCase.equals("OAEPWITHSHA1ANDMGF1PADDING")) {
                            if (upperCase.equals("OAEPWITHSHA224ANDMGF1PADDING")) {
                                default1 = new OAEPParameterSpec("SHA-224", "MGF1", new MGF1ParameterSpec("SHA-224"), PSource.PSpecified.DEFAULT);
                                break Label_0099;
                            }
                            if (upperCase.equals("OAEPWITHSHA256ANDMGF1PADDING")) {
                                default1 = new OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA256, PSource.PSpecified.DEFAULT);
                                break Label_0099;
                            }
                            if (upperCase.equals("OAEPWITHSHA384ANDMGF1PADDING")) {
                                default1 = new OAEPParameterSpec("SHA-384", "MGF1", MGF1ParameterSpec.SHA384, PSource.PSpecified.DEFAULT);
                                break Label_0099;
                            }
                            if (upperCase.equals("OAEPWITHSHA512ANDMGF1PADDING")) {
                                default1 = new OAEPParameterSpec("SHA-512", "MGF1", MGF1ParameterSpec.SHA512, PSource.PSpecified.DEFAULT);
                                break Label_0099;
                            }
                            if (upperCase.equals("OAEPWITHSHA3-224ANDMGF1PADDING")) {
                                default1 = new OAEPParameterSpec("SHA3-224", "MGF1", new MGF1ParameterSpec("SHA3-224"), PSource.PSpecified.DEFAULT);
                                break Label_0099;
                            }
                            if (upperCase.equals("OAEPWITHSHA3-256ANDMGF1PADDING")) {
                                default1 = new OAEPParameterSpec("SHA3-256", "MGF1", new MGF1ParameterSpec("SHA3-256"), PSource.PSpecified.DEFAULT);
                                break Label_0099;
                            }
                            if (upperCase.equals("OAEPWITHSHA3-384ANDMGF1PADDING")) {
                                default1 = new OAEPParameterSpec("SHA3-384", "MGF1", new MGF1ParameterSpec("SHA3-384"), PSource.PSpecified.DEFAULT);
                                break Label_0099;
                            }
                            if (upperCase.equals("OAEPWITHSHA3-512ANDMGF1PADDING")) {
                                default1 = new OAEPParameterSpec("SHA3-512", "MGF1", new MGF1ParameterSpec("SHA3-512"), PSource.PSpecified.DEFAULT);
                                break Label_0099;
                            }
                            final StringBuilder sb = new StringBuilder();
                            sb.append(str);
                            sb.append(" unavailable with ElGamal.");
                            throw new NoSuchPaddingException(sb.toString());
                        }
                    }
                    default1 = OAEPParameterSpec.DEFAULT;
                }
                this.initFromSpec(default1);
                return;
            }
            cipher = new ISO9796d1Encoding(new ElGamalEngine());
        }
        this.cipher = cipher;
    }
    
    @Override
    protected int engineUpdate(final byte[] b, final int off, final int len, final byte[] array, final int n) {
        this.bOut.write(b, off, len);
        return 0;
    }
    
    @Override
    protected byte[] engineUpdate(final byte[] b, final int off, final int len) {
        this.bOut.write(b, off, len);
        return null;
    }
    
    public static class NoPadding extends CipherSpi
    {
        public NoPadding() {
            super(new ElGamalEngine());
        }
    }
    
    public static class PKCS1v1_5Padding extends CipherSpi
    {
        public PKCS1v1_5Padding() {
            super(new PKCS1Encoding(new ElGamalEngine()));
        }
    }
}
