// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.util;

import java.util.Set;
import org.bouncycastle.jcajce.provider.config.ProviderConfiguration;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x9.X962Parameters;
import org.bouncycastle.jce.spec.ECNamedCurveSpec;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import java.security.spec.ECParameterSpec;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.math.field.Polynomial;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.math.field.PolynomialExtensionField;
import org.bouncycastle.math.ec.ECAlgorithms;
import org.bouncycastle.math.field.FiniteField;
import java.math.BigInteger;
import java.security.spec.ECField;
import java.security.spec.ECFieldF2m;
import java.security.spec.ECFieldFp;
import java.security.spec.EllipticCurve;
import org.bouncycastle.asn1.x9.X9ECParameters;
import java.util.Enumeration;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.asn1.x9.ECNamedCurveTable;
import org.bouncycastle.crypto.ec.CustomNamedCurves;
import java.util.HashMap;
import java.util.Map;

public class EC5Util
{
    private static Map customCurves;
    
    static {
        EC5Util.customCurves = new HashMap();
        final Enumeration names = CustomNamedCurves.getNames();
        while (names.hasMoreElements()) {
            final String s = names.nextElement();
            final X9ECParameters byName = ECNamedCurveTable.getByName(s);
            if (byName != null) {
                EC5Util.customCurves.put(byName.getCurve(), CustomNamedCurves.getByName(s).getCurve());
            }
        }
        final ECCurve curve = CustomNamedCurves.getByName("Curve25519").getCurve();
        EC5Util.customCurves.put(new ECCurve.Fp(curve.getField().getCharacteristic(), curve.getA().toBigInteger(), curve.getB().toBigInteger(), curve.getOrder(), curve.getCofactor()), curve);
    }
    
    public static EllipticCurve convertCurve(final ECCurve ecCurve, final byte[] array) {
        return new EllipticCurve(convertField(ecCurve.getField()), ecCurve.getA().toBigInteger(), ecCurve.getB().toBigInteger(), null);
    }
    
    public static ECCurve convertCurve(final EllipticCurve ellipticCurve) {
        final ECField field = ellipticCurve.getField();
        final BigInteger a = ellipticCurve.getA();
        final BigInteger b = ellipticCurve.getB();
        if (field instanceof ECFieldFp) {
            ECCurve ecCurve;
            final ECCurve.Fp fp = (ECCurve.Fp)(ecCurve = new ECCurve.Fp(((ECFieldFp)field).getP(), a, b));
            if (EC5Util.customCurves.containsKey(fp)) {
                ecCurve = (ECCurve)EC5Util.customCurves.get(fp);
            }
            return ecCurve;
        }
        final ECFieldF2m ecFieldF2m = (ECFieldF2m)field;
        final int m = ecFieldF2m.getM();
        final int[] convertMidTerms = ECUtil.convertMidTerms(ecFieldF2m.getMidTermsOfReductionPolynomial());
        return new ECCurve.F2m(m, convertMidTerms[0], convertMidTerms[1], convertMidTerms[2], a, b);
    }
    
    public static ECField convertField(final FiniteField finiteField) {
        if (ECAlgorithms.isFpField(finiteField)) {
            return new ECFieldFp(finiteField.getCharacteristic());
        }
        final Polynomial minimalPolynomial = ((PolynomialExtensionField)finiteField).getMinimalPolynomial();
        final int[] exponentsPresent = minimalPolynomial.getExponentsPresent();
        return new ECFieldF2m(minimalPolynomial.getDegree(), Arrays.reverse(Arrays.copyOfRange(exponentsPresent, 1, exponentsPresent.length - 1)));
    }
    
    public static java.security.spec.ECPoint convertPoint(ECPoint normalize) {
        normalize = normalize.normalize();
        return new java.security.spec.ECPoint(normalize.getAffineXCoord().toBigInteger(), normalize.getAffineYCoord().toBigInteger());
    }
    
    public static ECPoint convertPoint(final ECParameterSpec ecParameterSpec, final java.security.spec.ECPoint ecPoint) {
        return convertPoint(convertCurve(ecParameterSpec.getCurve()), ecPoint);
    }
    
    public static ECPoint convertPoint(final ECCurve ecCurve, final java.security.spec.ECPoint ecPoint) {
        return ecCurve.createPoint(ecPoint.getAffineX(), ecPoint.getAffineY());
    }
    
    public static ECParameterSpec convertSpec(final EllipticCurve curve, final org.bouncycastle.jce.spec.ECParameterSpec ecParameterSpec) {
        final java.security.spec.ECPoint convertPoint = convertPoint(ecParameterSpec.getG());
        if (ecParameterSpec instanceof ECNamedCurveParameterSpec) {
            return new ECNamedCurveSpec(((ECNamedCurveParameterSpec)ecParameterSpec).getName(), curve, convertPoint, ecParameterSpec.getN(), ecParameterSpec.getH());
        }
        return new ECParameterSpec(curve, convertPoint, ecParameterSpec.getN(), ecParameterSpec.getH().intValue());
    }
    
    public static org.bouncycastle.jce.spec.ECParameterSpec convertSpec(final ECParameterSpec ecParameterSpec) {
        final ECCurve convertCurve = convertCurve(ecParameterSpec.getCurve());
        final ECPoint convertPoint = convertPoint(convertCurve, ecParameterSpec.getGenerator());
        final BigInteger order = ecParameterSpec.getOrder();
        final BigInteger value = BigInteger.valueOf(ecParameterSpec.getCofactor());
        final byte[] seed = ecParameterSpec.getCurve().getSeed();
        if (ecParameterSpec instanceof ECNamedCurveSpec) {
            return new ECNamedCurveParameterSpec(((ECNamedCurveSpec)ecParameterSpec).getName(), convertCurve, convertPoint, order, value, seed);
        }
        return new org.bouncycastle.jce.spec.ECParameterSpec(convertCurve, convertPoint, order, value, seed);
    }
    
    public static ECParameterSpec convertToSpec(final X962Parameters x962Parameters, final ECCurve ecCurve) {
        if (x962Parameters.isNamedCurve()) {
            final ASN1ObjectIdentifier asn1ObjectIdentifier = (ASN1ObjectIdentifier)x962Parameters.getParameters();
            final X9ECParameters namedCurveByOid = ECUtil.getNamedCurveByOid(asn1ObjectIdentifier);
            X9ECParameters x9ECParameters;
            if ((x9ECParameters = namedCurveByOid) == null) {
                final Map additionalECParameters = BouncyCastleProvider.CONFIGURATION.getAdditionalECParameters();
                x9ECParameters = namedCurveByOid;
                if (!additionalECParameters.isEmpty()) {
                    x9ECParameters = additionalECParameters.get(asn1ObjectIdentifier);
                }
            }
            return new ECNamedCurveSpec(ECUtil.getCurveName(asn1ObjectIdentifier), convertCurve(ecCurve, x9ECParameters.getSeed()), convertPoint(x9ECParameters.getG()), x9ECParameters.getN(), x9ECParameters.getH());
        }
        if (x962Parameters.isImplicitlyCA()) {
            return null;
        }
        final X9ECParameters instance = X9ECParameters.getInstance(x962Parameters.getParameters());
        final EllipticCurve convertCurve = convertCurve(ecCurve, instance.getSeed());
        ECParameterSpec ecParameterSpec;
        if (instance.getH() != null) {
            ecParameterSpec = new ECParameterSpec(convertCurve, convertPoint(instance.getG()), instance.getN(), instance.getH().intValue());
        }
        else {
            ecParameterSpec = new ECParameterSpec(convertCurve, convertPoint(instance.getG()), instance.getN(), 1);
        }
        return ecParameterSpec;
    }
    
    public static ECParameterSpec convertToSpec(final X9ECParameters x9ECParameters) {
        return new ECParameterSpec(convertCurve(x9ECParameters.getCurve(), null), convertPoint(x9ECParameters.getG()), x9ECParameters.getN(), x9ECParameters.getH().intValue());
    }
    
    public static ECParameterSpec convertToSpec(final ECDomainParameters ecDomainParameters) {
        return new ECParameterSpec(convertCurve(ecDomainParameters.getCurve(), null), convertPoint(ecDomainParameters.getG()), ecDomainParameters.getN(), ecDomainParameters.getH().intValue());
    }
    
    public static ECCurve getCurve(final ProviderConfiguration providerConfiguration, final X962Parameters x962Parameters) {
        final Set acceptableNamedCurves = providerConfiguration.getAcceptableNamedCurves();
        if (x962Parameters.isNamedCurve()) {
            final ASN1ObjectIdentifier instance = ASN1ObjectIdentifier.getInstance(x962Parameters.getParameters());
            if (!acceptableNamedCurves.isEmpty() && !acceptableNamedCurves.contains(instance)) {
                throw new IllegalStateException("named curve not acceptable");
            }
            X9ECParameters namedCurveByOid;
            if ((namedCurveByOid = ECUtil.getNamedCurveByOid(instance)) == null) {
                namedCurveByOid = providerConfiguration.getAdditionalECParameters().get(instance);
            }
            return namedCurveByOid.getCurve();
        }
        else {
            if (x962Parameters.isImplicitlyCA()) {
                return providerConfiguration.getEcImplicitlyCa().getCurve();
            }
            if (acceptableNamedCurves.isEmpty()) {
                return X9ECParameters.getInstance(x962Parameters.getParameters()).getCurve();
            }
            throw new IllegalStateException("encoded parameters not acceptable");
        }
    }
    
    public static ECDomainParameters getDomainParameters(final ProviderConfiguration providerConfiguration, final ECParameterSpec ecParameterSpec) {
        if (ecParameterSpec == null) {
            final org.bouncycastle.jce.spec.ECParameterSpec ecImplicitlyCa = providerConfiguration.getEcImplicitlyCa();
            return new ECDomainParameters(ecImplicitlyCa.getCurve(), ecImplicitlyCa.getG(), ecImplicitlyCa.getN(), ecImplicitlyCa.getH(), ecImplicitlyCa.getSeed());
        }
        return ECUtil.getDomainParameters(providerConfiguration, convertSpec(ecParameterSpec));
    }
}
