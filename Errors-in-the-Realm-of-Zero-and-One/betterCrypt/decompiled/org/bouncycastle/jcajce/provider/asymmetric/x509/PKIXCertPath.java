// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.x509;

import java.util.ListIterator;
import org.bouncycastle.util.io.pem.PemObjectGenerator;
import org.bouncycastle.util.io.pem.PemObject;
import java.io.Writer;
import org.bouncycastle.util.io.pem.PemWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.ByteArrayOutputStream;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.pkcs.SignedData;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.pkcs.ContentInfo;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.ASN1EncodableVector;
import java.util.Iterator;
import java.security.cert.CertificateEncodingException;
import javax.security.auth.x500.X500Principal;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1Primitive;
import java.io.IOException;
import java.security.NoSuchProviderException;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import org.bouncycastle.asn1.ASN1Encodable;
import java.security.cert.CertificateException;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.jcajce.util.BCJcaJceHelper;
import java.io.InputStream;
import java.util.Collections;
import java.util.ArrayList;
import org.bouncycastle.jcajce.util.JcaJceHelper;
import java.util.List;
import java.security.cert.CertPath;

public class PKIXCertPath extends CertPath
{
    static final List certPathEncodings;
    private List certificates;
    private final JcaJceHelper helper;
    
    static {
        final ArrayList<String> list = new ArrayList<String>();
        list.add("PkiPath");
        list.add("PEM");
        list.add("PKCS7");
        certPathEncodings = Collections.unmodifiableList((List<?>)list);
    }
    
    PKIXCertPath(final InputStream in, final String str) throws CertificateException {
        super("X.509");
        this.helper = new BCJcaJceHelper();
        try {
            if (str.equalsIgnoreCase("PkiPath")) {
                final ASN1Primitive object = new ASN1InputStream(in).readObject();
                if (!(object instanceof ASN1Sequence)) {
                    throw new CertificateException("input stream does not contain a ASN1 SEQUENCE while reading PkiPath encoded data to load CertPath");
                }
                final Enumeration objects = ((ASN1Sequence)object).getObjects();
                this.certificates = new ArrayList();
                final CertificateFactory certificateFactory = this.helper.createCertificateFactory("X.509");
                while (objects.hasMoreElements()) {
                    this.certificates.add(0, certificateFactory.generateCertificate(new ByteArrayInputStream(objects.nextElement().toASN1Primitive().getEncoded("DER"))));
                }
            }
            else {
                if (!str.equalsIgnoreCase("PKCS7") && !str.equalsIgnoreCase("PEM")) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("unsupported encoding: ");
                    sb.append(str);
                    throw new CertificateException(sb.toString());
                }
                final BufferedInputStream inStream = new BufferedInputStream(in);
                this.certificates = new ArrayList();
                final CertificateFactory certificateFactory2 = this.helper.createCertificateFactory("X.509");
                while (true) {
                    final Certificate generateCertificate = certificateFactory2.generateCertificate(inStream);
                    if (generateCertificate == null) {
                        break;
                    }
                    this.certificates.add(generateCertificate);
                }
            }
            this.certificates = this.sortCerts(this.certificates);
        }
        catch (NoSuchProviderException ex) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("BouncyCastle provider not found while trying to get a CertificateFactory:\n");
            sb2.append(ex.toString());
            throw new CertificateException(sb2.toString());
        }
        catch (IOException ex2) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("IOException throw while decoding CertPath:\n");
            sb3.append(ex2.toString());
            throw new CertificateException(sb3.toString());
        }
    }
    
    PKIXCertPath(final List c) {
        super("X.509");
        this.helper = new BCJcaJceHelper();
        this.certificates = this.sortCerts(new ArrayList(c));
    }
    
    private List sortCerts(final List c) {
        if (c.size() < 2) {
            return c;
        }
        X500Principal x500Principal = c.get(0).getIssuerX500Principal();
        int i = 1;
        while (true) {
            while (i != c.size()) {
                if (x500Principal.equals(c.get(i).getSubjectX500Principal())) {
                    x500Principal = c.get(i).getIssuerX500Principal();
                    ++i;
                }
                else {
                    final boolean b = false;
                    if (b) {
                        return c;
                    }
                    final ArrayList<X509Certificate> list = new ArrayList<X509Certificate>(c.size());
                    final ArrayList list2 = new ArrayList(c);
                    int j = 0;
                Label_0122:
                    while (j < c.size()) {
                        final X509Certificate x509Certificate = c.get(j);
                        final X500Principal subjectX500Principal = x509Certificate.getSubjectX500Principal();
                        while (true) {
                            for (int k = 0; k != c.size(); ++k) {
                                if (c.get(k).getIssuerX500Principal().equals(subjectX500Principal)) {
                                    final boolean b2 = true;
                                    if (!b2) {
                                        list.add(x509Certificate);
                                        c.remove(j);
                                    }
                                    ++j;
                                    continue Label_0122;
                                }
                            }
                            final boolean b2 = false;
                            continue;
                        }
                    }
                    if (list.size() > 1) {
                        return list2;
                    }
                    for (int l = 0; l != list.size(); ++l) {
                        final X500Principal issuerX500Principal = list.get(l).getIssuerX500Principal();
                        for (int n = 0; n < c.size(); ++n) {
                            final X509Certificate x509Certificate2 = c.get(n);
                            if (issuerX500Principal.equals(x509Certificate2.getSubjectX500Principal())) {
                                list.add(x509Certificate2);
                                c.remove(n);
                                break;
                            }
                        }
                    }
                    if (c.size() > 0) {
                        return list2;
                    }
                    return list;
                }
            }
            final boolean b = true;
            continue;
        }
    }
    
    private ASN1Primitive toASN1Object(final X509Certificate x509Certificate) throws CertificateEncodingException {
        try {
            return new ASN1InputStream(x509Certificate.getEncoded()).readObject();
        }
        catch (Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Exception while encoding certificate: ");
            sb.append(ex.toString());
            throw new CertificateEncodingException(sb.toString());
        }
    }
    
    private byte[] toDEREncoded(final ASN1Encodable asn1Encodable) throws CertificateEncodingException {
        try {
            return asn1Encodable.toASN1Primitive().getEncoded("DER");
        }
        catch (IOException obj) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Exception thrown: ");
            sb.append(obj);
            throw new CertificateEncodingException(sb.toString());
        }
    }
    
    @Override
    public List getCertificates() {
        return Collections.unmodifiableList((List<?>)new ArrayList<Object>(this.certificates));
    }
    
    @Override
    public byte[] getEncoded() throws CertificateEncodingException {
        final Iterator encodings = this.getEncodings();
        if (encodings.hasNext()) {
            final String next = encodings.next();
            if (next instanceof String) {
                return this.getEncoded(next);
            }
        }
        return null;
    }
    
    @Override
    public byte[] getEncoded(final String str) throws CertificateEncodingException {
        if (str.equalsIgnoreCase("PkiPath")) {
            final ASN1EncodableVector asn1EncodableVector = new ASN1EncodableVector();
            final List certificates = this.certificates;
            final ListIterator<X509Certificate> listIterator = certificates.listIterator(certificates.size());
            while (listIterator.hasPrevious()) {
                asn1EncodableVector.add(this.toASN1Object(listIterator.previous()));
            }
            return this.toDEREncoded(new DERSequence(asn1EncodableVector));
        }
        final boolean equalsIgnoreCase = str.equalsIgnoreCase("PKCS7");
        final int n = 0;
        int i = 0;
        if (equalsIgnoreCase) {
            final ContentInfo contentInfo = new ContentInfo(PKCSObjectIdentifiers.data, null);
            final ASN1EncodableVector asn1EncodableVector2 = new ASN1EncodableVector();
            while (i != this.certificates.size()) {
                asn1EncodableVector2.add(this.toASN1Object(this.certificates.get(i)));
                ++i;
            }
            return this.toDEREncoded(new ContentInfo(PKCSObjectIdentifiers.signedData, new SignedData(new ASN1Integer(1L), new DERSet(), contentInfo, new DERSet(asn1EncodableVector2), null, new DERSet())));
        }
        Label_0328: {
            if (!str.equalsIgnoreCase("PEM")) {
                break Label_0328;
            }
            final ByteArrayOutputStream out = new ByteArrayOutputStream();
            final PemWriter pemWriter = new PemWriter(new OutputStreamWriter(out));
            int j = n;
            while (true) {
                while (true) {
                    try {
                        while (j != this.certificates.size()) {
                            pemWriter.writeObject(new PemObject("CERTIFICATE", ((X509Certificate)this.certificates.get(j)).getEncoded()));
                            ++j;
                        }
                        pemWriter.close();
                        return out.toByteArray();
                        throw new CertificateEncodingException("can't encode certificate for PEM encoded path");
                        final StringBuilder sb = new StringBuilder();
                        sb.append("unsupported encoding: ");
                        sb.append(str);
                        throw new CertificateEncodingException(sb.toString());
                    }
                    catch (Exception ex) {}
                    continue;
                }
            }
        }
    }
    
    @Override
    public Iterator getEncodings() {
        return PKIXCertPath.certPathEncodings.iterator();
    }
}
