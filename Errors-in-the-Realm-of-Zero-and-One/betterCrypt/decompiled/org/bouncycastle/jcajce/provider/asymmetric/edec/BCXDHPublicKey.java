// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.edec;

import org.bouncycastle.util.Arrays;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.edec.EdECObjectIdentifiers;
import org.bouncycastle.crypto.params.X25519PublicKeyParameters;
import java.security.spec.InvalidKeySpecException;
import org.bouncycastle.crypto.params.X448PublicKeyParameters;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import java.security.PublicKey;
import org.bouncycastle.jcajce.interfaces.XDHKey;

public class BCXDHPublicKey implements XDHKey, PublicKey
{
    static final long serialVersionUID = 1L;
    private transient AsymmetricKeyParameter xdhPublicKey;
    
    BCXDHPublicKey(final SubjectPublicKeyInfo subjectPublicKeyInfo) {
        this.populateFromPubKeyInfo(subjectPublicKeyInfo);
    }
    
    BCXDHPublicKey(final AsymmetricKeyParameter xdhPublicKey) {
        this.xdhPublicKey = xdhPublicKey;
    }
    
    BCXDHPublicKey(final byte[] array, final byte[] array2) throws InvalidKeySpecException {
        final int length = array.length;
        if (Utils.isValidPrefix(array, array2)) {
            AsymmetricKeyParameter xdhPublicKey;
            if (array2.length - length == 56) {
                xdhPublicKey = new X448PublicKeyParameters(array2, length);
            }
            else {
                if (array2.length - length != 32) {
                    throw new InvalidKeySpecException("raw key data not recognised");
                }
                xdhPublicKey = new X25519PublicKeyParameters(array2, length);
            }
            this.xdhPublicKey = xdhPublicKey;
            return;
        }
        throw new InvalidKeySpecException("raw key data not recognised");
    }
    
    private void populateFromPubKeyInfo(final SubjectPublicKeyInfo subjectPublicKeyInfo) {
        AsymmetricKeyParameter xdhPublicKey;
        if (EdECObjectIdentifiers.id_X448.equals(subjectPublicKeyInfo.getAlgorithm().getAlgorithm())) {
            xdhPublicKey = new X448PublicKeyParameters(subjectPublicKeyInfo.getPublicKeyData().getOctets(), 0);
        }
        else {
            xdhPublicKey = new X25519PublicKeyParameters(subjectPublicKeyInfo.getPublicKeyData().getOctets(), 0);
        }
        this.xdhPublicKey = xdhPublicKey;
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.populateFromPubKeyInfo(SubjectPublicKeyInfo.getInstance(objectInputStream.readObject()));
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.getEncoded());
    }
    
    AsymmetricKeyParameter engineGetKeyParameters() {
        return this.xdhPublicKey;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof BCXDHPublicKey && Arrays.areEqual(((BCXDHPublicKey)o).getEncoded(), this.getEncoded()));
    }
    
    @Override
    public String getAlgorithm() {
        if (this.xdhPublicKey instanceof X448PublicKeyParameters) {
            return "X448";
        }
        return "X25519";
    }
    
    @Override
    public byte[] getEncoded() {
        if (this.xdhPublicKey instanceof X448PublicKeyParameters) {
            final byte[] array = new byte[KeyFactorySpi.x448Prefix.length + 56];
            System.arraycopy(KeyFactorySpi.x448Prefix, 0, array, 0, KeyFactorySpi.x448Prefix.length);
            ((X448PublicKeyParameters)this.xdhPublicKey).encode(array, KeyFactorySpi.x448Prefix.length);
            return array;
        }
        final byte[] array2 = new byte[KeyFactorySpi.x25519Prefix.length + 32];
        System.arraycopy(KeyFactorySpi.x25519Prefix, 0, array2, 0, KeyFactorySpi.x25519Prefix.length);
        ((X25519PublicKeyParameters)this.xdhPublicKey).encode(array2, KeyFactorySpi.x25519Prefix.length);
        return array2;
    }
    
    @Override
    public String getFormat() {
        return "X.509";
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.getEncoded());
    }
    
    @Override
    public String toString() {
        return Utils.keyToString("Public Key", this.getAlgorithm(), this.xdhPublicKey);
    }
}
