// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.x509;

import java.security.Provider;
import java.security.NoSuchProviderException;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.misc.VerisignCzagExtension;
import org.bouncycastle.asn1.util.ASN1Dump;
import org.bouncycastle.asn1.misc.NetscapeRevocationURL;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.misc.NetscapeCertType;
import org.bouncycastle.asn1.misc.MiscObjectIdentifiers;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.util.Strings;
import org.bouncycastle.asn1.x509.TBSCertificate;
import java.math.BigInteger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.Arrays;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.jce.X509Principal;
import java.security.Principal;
import java.util.Collections;
import java.util.ArrayList;
import org.bouncycastle.asn1.ASN1Sequence;
import java.util.List;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateExpiredException;
import java.util.Date;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1OctetString;
import java.security.cert.CertificateParsingException;
import java.util.Collection;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.io.IOException;
import java.security.cert.CertificateEncodingException;
import java.security.SignatureException;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import org.bouncycastle.jcajce.io.OutputStreamFactory;
import java.security.Signature;
import java.security.PublicKey;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.jcajce.util.JcaJceHelper;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.jcajce.interfaces.BCX509Certificate;
import java.security.cert.X509Certificate;

abstract class X509CertificateImpl extends X509Certificate implements BCX509Certificate
{
    protected BasicConstraints basicConstraints;
    protected JcaJceHelper bcHelper;
    protected org.bouncycastle.asn1.x509.Certificate c;
    protected boolean[] keyUsage;
    
    X509CertificateImpl(final JcaJceHelper bcHelper, final org.bouncycastle.asn1.x509.Certificate c, final BasicConstraints basicConstraints, final boolean[] keyUsage) {
        this.bcHelper = bcHelper;
        this.c = c;
        this.basicConstraints = basicConstraints;
        this.keyUsage = keyUsage;
    }
    
    private void checkSignature(final PublicKey publicKey, final Signature signature) throws CertificateException, NoSuchAlgorithmException, SignatureException, InvalidKeyException {
        if (this.isAlgIdEqual(this.c.getSignatureAlgorithm(), this.c.getTBSCertificate().getSignature())) {
            X509SignatureUtil.setSignatureParameters(signature, this.c.getSignatureAlgorithm().getParameters());
            signature.initVerify(publicKey);
            try {
                final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(OutputStreamFactory.createStream(signature), 512);
                this.c.getTBSCertificate().encodeTo(bufferedOutputStream, "DER");
                bufferedOutputStream.close();
                if (signature.verify(this.getSignature())) {
                    return;
                }
                throw new SignatureException("certificate does not verify with supplied key");
            }
            catch (IOException ex) {
                throw new CertificateEncodingException(ex.toString());
            }
        }
        throw new CertificateException("signature algorithm in TBS cert not same as outer cert");
    }
    
    private static Collection getAlternativeNames(final org.bouncycastle.asn1.x509.Certificate p0, final String p1) throws CertificateParsingException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: aload_1        
        //     2: invokestatic    org/bouncycastle/jcajce/provider/asymmetric/x509/X509CertificateImpl.getExtensionOctets:(Lorg/bouncycastle/asn1/x509/Certificate;Ljava/lang/String;)[B
        //     5: astore_0       
        //     6: aload_0        
        //     7: ifnonnull       12
        //    10: aconst_null    
        //    11: areturn        
        //    12: new             Ljava/util/ArrayList;
        //    15: dup            
        //    16: invokespecial   java/util/ArrayList.<init>:()V
        //    19: astore_1       
        //    20: aload_0        
        //    21: invokestatic    org/bouncycastle/asn1/ASN1Sequence.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/ASN1Sequence;
        //    24: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.getObjects:()Ljava/util/Enumeration;
        //    27: astore_2       
        //    28: aload_2        
        //    29: invokeinterface java/util/Enumeration.hasMoreElements:()Z
        //    34: ifeq            265
        //    37: aload_2        
        //    38: invokeinterface java/util/Enumeration.nextElement:()Ljava/lang/Object;
        //    43: invokestatic    org/bouncycastle/asn1/x509/GeneralName.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/GeneralName;
        //    46: astore_0       
        //    47: new             Ljava/util/ArrayList;
        //    50: dup            
        //    51: invokespecial   java/util/ArrayList.<init>:()V
        //    54: astore_3       
        //    55: aload_3        
        //    56: aload_0        
        //    57: invokevirtual   org/bouncycastle/asn1/x509/GeneralName.getTagNo:()I
        //    60: invokestatic    org/bouncycastle/util/Integers.valueOf:(I)Ljava/lang/Integer;
        //    63: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //    68: pop            
        //    69: aload_0        
        //    70: invokevirtual   org/bouncycastle/asn1/x509/GeneralName.getTagNo:()I
        //    73: tableswitch {
        //                0: 204
        //                1: 188
        //                2: 188
        //                3: 204
        //                4: 171
        //                5: 204
        //                6: 188
        //                7: 149
        //                8: 127
        //          default: 300
        //        }
        //   124: goto            229
        //   127: aload_0        
        //   128: invokevirtual   org/bouncycastle/asn1/x509/GeneralName.getName:()Lorg/bouncycastle/asn1/ASN1Encodable;
        //   131: invokestatic    org/bouncycastle/asn1/ASN1ObjectIdentifier.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //   134: invokevirtual   org/bouncycastle/asn1/ASN1ObjectIdentifier.getId:()Ljava/lang/String;
        //   137: astore_0       
        //   138: aload_3        
        //   139: aload_0        
        //   140: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   145: pop            
        //   146: goto            215
        //   149: aload_0        
        //   150: invokevirtual   org/bouncycastle/asn1/x509/GeneralName.getName:()Lorg/bouncycastle/asn1/ASN1Encodable;
        //   153: invokestatic    org/bouncycastle/asn1/DEROctetString.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/ASN1OctetString;
        //   156: invokevirtual   org/bouncycastle/asn1/ASN1OctetString.getOctets:()[B
        //   159: astore_0       
        //   160: aload_0        
        //   161: invokestatic    java/net/InetAddress.getByAddress:([B)Ljava/net/InetAddress;
        //   164: invokevirtual   java/net/InetAddress.getHostAddress:()Ljava/lang/String;
        //   167: astore_0       
        //   168: goto            138
        //   171: getstatic       org/bouncycastle/asn1/x500/style/RFC4519Style.INSTANCE:Lorg/bouncycastle/asn1/x500/X500NameStyle;
        //   174: aload_0        
        //   175: invokevirtual   org/bouncycastle/asn1/x509/GeneralName.getName:()Lorg/bouncycastle/asn1/ASN1Encodable;
        //   178: invokestatic    org/bouncycastle/asn1/x500/X500Name.getInstance:(Lorg/bouncycastle/asn1/x500/X500NameStyle;Ljava/lang/Object;)Lorg/bouncycastle/asn1/x500/X500Name;
        //   181: invokevirtual   org/bouncycastle/asn1/x500/X500Name.toString:()Ljava/lang/String;
        //   184: astore_0       
        //   185: goto            138
        //   188: aload_0        
        //   189: invokevirtual   org/bouncycastle/asn1/x509/GeneralName.getName:()Lorg/bouncycastle/asn1/ASN1Encodable;
        //   192: checkcast       Lorg/bouncycastle/asn1/ASN1String;
        //   195: invokeinterface org/bouncycastle/asn1/ASN1String.getString:()Ljava/lang/String;
        //   200: astore_0       
        //   201: goto            138
        //   204: aload_3        
        //   205: aload_0        
        //   206: invokevirtual   org/bouncycastle/asn1/x509/GeneralName.getEncoded:()[B
        //   209: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   214: pop            
        //   215: aload_1        
        //   216: aload_3        
        //   217: invokestatic    java/util/Collections.unmodifiableList:(Ljava/util/List;)Ljava/util/List;
        //   220: invokeinterface java/util/Collection.add:(Ljava/lang/Object;)Z
        //   225: pop            
        //   226: goto            28
        //   229: new             Ljava/lang/StringBuilder;
        //   232: dup            
        //   233: invokespecial   java/lang/StringBuilder.<init>:()V
        //   236: astore_1       
        //   237: aload_1        
        //   238: ldc             "Bad tag number: "
        //   240: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   243: pop            
        //   244: aload_1        
        //   245: aload_0        
        //   246: invokevirtual   org/bouncycastle/asn1/x509/GeneralName.getTagNo:()I
        //   249: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   252: pop            
        //   253: new             Ljava/io/IOException;
        //   256: dup            
        //   257: aload_1        
        //   258: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   261: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
        //   264: athrow         
        //   265: aload_1        
        //   266: invokeinterface java/util/Collection.size:()I
        //   271: ifne            276
        //   274: aconst_null    
        //   275: areturn        
        //   276: aload_1        
        //   277: invokestatic    java/util/Collections.unmodifiableCollection:(Ljava/util/Collection;)Ljava/util/Collection;
        //   280: astore_0       
        //   281: aload_0        
        //   282: areturn        
        //   283: astore_0       
        //   284: new             Ljava/security/cert/CertificateParsingException;
        //   287: dup            
        //   288: aload_0        
        //   289: invokevirtual   java/lang/Exception.getMessage:()Ljava/lang/String;
        //   292: invokespecial   java/security/cert/CertificateParsingException.<init>:(Ljava/lang/String;)V
        //   295: athrow         
        //   296: astore_0       
        //   297: goto            28
        //   300: goto            124
        //    Exceptions:
        //  throws java.security.cert.CertificateParsingException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                           
        //  -----  -----  -----  -----  -------------------------------
        //  12     28     283    296    Ljava/lang/Exception;
        //  28     124    283    296    Ljava/lang/Exception;
        //  127    138    283    296    Ljava/lang/Exception;
        //  138    146    283    296    Ljava/lang/Exception;
        //  149    160    283    296    Ljava/lang/Exception;
        //  160    168    296    300    Ljava/net/UnknownHostException;
        //  160    168    283    296    Ljava/lang/Exception;
        //  171    185    283    296    Ljava/lang/Exception;
        //  188    201    283    296    Ljava/lang/Exception;
        //  204    215    283    296    Ljava/lang/Exception;
        //  215    226    283    296    Ljava/lang/Exception;
        //  229    265    283    296    Ljava/lang/Exception;
        //  265    274    283    296    Ljava/lang/Exception;
        //  276    281    283    296    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0171:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    protected static byte[] getExtensionOctets(final org.bouncycastle.asn1.x509.Certificate certificate, final String s) {
        final ASN1OctetString extensionValue = getExtensionValue(certificate, s);
        if (extensionValue != null) {
            return extensionValue.getOctets();
        }
        return null;
    }
    
    protected static ASN1OctetString getExtensionValue(final org.bouncycastle.asn1.x509.Certificate certificate, final String s) {
        final Extensions extensions = certificate.getTBSCertificate().getExtensions();
        if (extensions != null) {
            final Extension extension = extensions.getExtension(new ASN1ObjectIdentifier(s));
            if (extension != null) {
                return extension.getExtnValue();
            }
        }
        return null;
    }
    
    private boolean isAlgIdEqual(final AlgorithmIdentifier algorithmIdentifier, final AlgorithmIdentifier algorithmIdentifier2) {
        if (!algorithmIdentifier.getAlgorithm().equals(algorithmIdentifier2.getAlgorithm())) {
            return false;
        }
        if (algorithmIdentifier.getParameters() == null) {
            return algorithmIdentifier2.getParameters() == null || algorithmIdentifier2.getParameters().equals(DERNull.INSTANCE);
        }
        if (algorithmIdentifier2.getParameters() == null) {
            return algorithmIdentifier.getParameters() == null || algorithmIdentifier.getParameters().equals(DERNull.INSTANCE);
        }
        return algorithmIdentifier.getParameters().equals(algorithmIdentifier2.getParameters());
    }
    
    @Override
    public void checkValidity() throws CertificateExpiredException, CertificateNotYetValidException {
        this.checkValidity(new Date());
    }
    
    @Override
    public void checkValidity(final Date date) throws CertificateExpiredException, CertificateNotYetValidException {
        if (date.getTime() > this.getNotAfter().getTime()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("certificate expired on ");
            sb.append(this.c.getEndDate().getTime());
            throw new CertificateExpiredException(sb.toString());
        }
        if (date.getTime() >= this.getNotBefore().getTime()) {
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("certificate not valid till ");
        sb2.append(this.c.getStartDate().getTime());
        throw new CertificateNotYetValidException(sb2.toString());
    }
    
    @Override
    public int getBasicConstraints() {
        final BasicConstraints basicConstraints = this.basicConstraints;
        if (basicConstraints == null || !basicConstraints.isCA()) {
            return -1;
        }
        if (this.basicConstraints.getPathLenConstraint() == null) {
            return Integer.MAX_VALUE;
        }
        return this.basicConstraints.getPathLenConstraint().intValue();
    }
    
    @Override
    public Set getCriticalExtensionOIDs() {
        if (this.getVersion() == 3) {
            final HashSet<String> set = new HashSet<String>();
            final Extensions extensions = this.c.getTBSCertificate().getExtensions();
            if (extensions != null) {
                final Enumeration oids = extensions.oids();
                while (oids.hasMoreElements()) {
                    final ASN1ObjectIdentifier asn1ObjectIdentifier = oids.nextElement();
                    if (extensions.getExtension(asn1ObjectIdentifier).isCritical()) {
                        set.add(asn1ObjectIdentifier.getId());
                    }
                }
                return set;
            }
        }
        return null;
    }
    
    @Override
    public byte[] getEncoded() throws CertificateEncodingException {
        try {
            return this.c.getEncoded("DER");
        }
        catch (IOException ex) {
            throw new CertificateEncodingException(ex.toString());
        }
    }
    
    @Override
    public List getExtendedKeyUsage() throws CertificateParsingException {
        final byte[] extensionOctets = getExtensionOctets(this.c, "2.5.29.37");
        if (extensionOctets == null) {
            return null;
        }
        try {
            final ASN1Sequence instance = ASN1Sequence.getInstance(ASN1Primitive.fromByteArray(extensionOctets));
            final ArrayList<String> list = new ArrayList<String>();
            for (int i = 0; i != instance.size(); ++i) {
                list.add(((ASN1ObjectIdentifier)instance.getObjectAt(i)).getId());
            }
            return Collections.unmodifiableList((List<?>)list);
        }
        catch (Exception ex) {
            throw new CertificateParsingException("error processing extended key usage extension");
        }
    }
    
    @Override
    public byte[] getExtensionValue(final String s) {
        final ASN1OctetString extensionValue = getExtensionValue(this.c, s);
        if (extensionValue != null) {
            try {
                return extensionValue.getEncoded();
            }
            catch (Exception ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("error parsing ");
                sb.append(ex.toString());
                throw new IllegalStateException(sb.toString());
            }
        }
        return null;
    }
    
    @Override
    public Collection getIssuerAlternativeNames() throws CertificateParsingException {
        return getAlternativeNames(this.c, Extension.issuerAlternativeName.getId());
    }
    
    @Override
    public Principal getIssuerDN() {
        return new X509Principal(this.c.getIssuer());
    }
    
    @Override
    public boolean[] getIssuerUniqueID() {
        final DERBitString issuerUniqueId = this.c.getTBSCertificate().getIssuerUniqueId();
        if (issuerUniqueId != null) {
            final byte[] bytes = issuerUniqueId.getBytes();
            final boolean[] array = new boolean[bytes.length * 8 - issuerUniqueId.getPadBits()];
            for (int i = 0; i != array.length; ++i) {
                array[i] = ((bytes[i / 8] & 128 >>> i % 8) != 0x0);
            }
            return array;
        }
        return null;
    }
    
    @Override
    public X500Name getIssuerX500Name() {
        return this.c.getIssuer();
    }
    
    @Override
    public X500Principal getIssuerX500Principal() {
        try {
            return new X500Principal(this.c.getIssuer().getEncoded("DER"));
        }
        catch (IOException ex) {
            throw new IllegalStateException("can't encode issuer DN");
        }
    }
    
    @Override
    public boolean[] getKeyUsage() {
        return Arrays.clone(this.keyUsage);
    }
    
    @Override
    public Set getNonCriticalExtensionOIDs() {
        if (this.getVersion() == 3) {
            final HashSet<String> set = new HashSet<String>();
            final Extensions extensions = this.c.getTBSCertificate().getExtensions();
            if (extensions != null) {
                final Enumeration oids = extensions.oids();
                while (oids.hasMoreElements()) {
                    final ASN1ObjectIdentifier asn1ObjectIdentifier = oids.nextElement();
                    if (!extensions.getExtension(asn1ObjectIdentifier).isCritical()) {
                        set.add(asn1ObjectIdentifier.getId());
                    }
                }
                return set;
            }
        }
        return null;
    }
    
    @Override
    public Date getNotAfter() {
        return this.c.getEndDate().getDate();
    }
    
    @Override
    public Date getNotBefore() {
        return this.c.getStartDate().getDate();
    }
    
    @Override
    public PublicKey getPublicKey() {
        try {
            return BouncyCastleProvider.getPublicKey(this.c.getSubjectPublicKeyInfo());
        }
        catch (IOException ex) {
            return null;
        }
    }
    
    @Override
    public BigInteger getSerialNumber() {
        return this.c.getSerialNumber().getValue();
    }
    
    @Override
    public String getSigAlgName() {
        return X509SignatureUtil.getSignatureName(this.c.getSignatureAlgorithm());
    }
    
    @Override
    public String getSigAlgOID() {
        return this.c.getSignatureAlgorithm().getAlgorithm().getId();
    }
    
    @Override
    public byte[] getSigAlgParams() {
        Label_0036: {
            if (this.c.getSignatureAlgorithm().getParameters() == null) {
                break Label_0036;
            }
            try {
                return this.c.getSignatureAlgorithm().getParameters().toASN1Primitive().getEncoded("DER");
                return null;
            }
            catch (IOException ex) {
                return null;
            }
        }
    }
    
    @Override
    public byte[] getSignature() {
        return this.c.getSignature().getOctets();
    }
    
    @Override
    public Collection getSubjectAlternativeNames() throws CertificateParsingException {
        return getAlternativeNames(this.c, Extension.subjectAlternativeName.getId());
    }
    
    @Override
    public Principal getSubjectDN() {
        return new X509Principal(this.c.getSubject());
    }
    
    @Override
    public boolean[] getSubjectUniqueID() {
        final DERBitString subjectUniqueId = this.c.getTBSCertificate().getSubjectUniqueId();
        if (subjectUniqueId != null) {
            final byte[] bytes = subjectUniqueId.getBytes();
            final boolean[] array = new boolean[bytes.length * 8 - subjectUniqueId.getPadBits()];
            for (int i = 0; i != array.length; ++i) {
                array[i] = ((bytes[i / 8] & 128 >>> i % 8) != 0x0);
            }
            return array;
        }
        return null;
    }
    
    @Override
    public X500Name getSubjectX500Name() {
        return this.c.getSubject();
    }
    
    @Override
    public X500Principal getSubjectX500Principal() {
        try {
            return new X500Principal(this.c.getSubject().getEncoded("DER"));
        }
        catch (IOException ex) {
            throw new IllegalStateException("can't encode subject DN");
        }
    }
    
    @Override
    public byte[] getTBSCertificate() throws CertificateEncodingException {
        try {
            return this.c.getTBSCertificate().getEncoded("DER");
        }
        catch (IOException ex) {
            throw new CertificateEncodingException(ex.toString());
        }
    }
    
    @Override
    public TBSCertificate getTBSCertificateNative() {
        return this.c.getTBSCertificate();
    }
    
    @Override
    public int getVersion() {
        return this.c.getVersionNumber();
    }
    
    @Override
    public boolean hasUnsupportedCriticalExtension() {
        if (this.getVersion() == 3) {
            final Extensions extensions = this.c.getTBSCertificate().getExtensions();
            if (extensions != null) {
                final Enumeration oids = extensions.oids();
                while (oids.hasMoreElements()) {
                    final ASN1ObjectIdentifier asn1ObjectIdentifier = oids.nextElement();
                    if (!asn1ObjectIdentifier.equals(Extension.keyUsage) && !asn1ObjectIdentifier.equals(Extension.certificatePolicies) && !asn1ObjectIdentifier.equals(Extension.policyMappings) && !asn1ObjectIdentifier.equals(Extension.inhibitAnyPolicy) && !asn1ObjectIdentifier.equals(Extension.cRLDistributionPoints) && !asn1ObjectIdentifier.equals(Extension.issuingDistributionPoint) && !asn1ObjectIdentifier.equals(Extension.deltaCRLIndicator) && !asn1ObjectIdentifier.equals(Extension.policyConstraints) && !asn1ObjectIdentifier.equals(Extension.basicConstraints) && !asn1ObjectIdentifier.equals(Extension.subjectAlternativeName)) {
                        if (asn1ObjectIdentifier.equals(Extension.nameConstraints)) {
                            continue;
                        }
                        if (extensions.getExtension(asn1ObjectIdentifier).isCritical()) {
                            return true;
                        }
                        continue;
                    }
                }
            }
        }
        return false;
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        final String lineSeparator = Strings.lineSeparator();
        sb.append("  [0]         Version: ");
        sb.append(this.getVersion());
        sb.append(lineSeparator);
        sb.append("         SerialNumber: ");
        sb.append(this.getSerialNumber());
        sb.append(lineSeparator);
        sb.append("             IssuerDN: ");
        sb.append(this.getIssuerDN());
        sb.append(lineSeparator);
        sb.append("           Start Date: ");
        sb.append(this.getNotBefore());
        sb.append(lineSeparator);
        sb.append("           Final Date: ");
        sb.append(this.getNotAfter());
        sb.append(lineSeparator);
        sb.append("            SubjectDN: ");
        sb.append(this.getSubjectDN());
        sb.append(lineSeparator);
        sb.append("           Public Key: ");
        sb.append(this.getPublicKey());
        sb.append(lineSeparator);
        sb.append("  Signature Algorithm: ");
        sb.append(this.getSigAlgName());
        sb.append(lineSeparator);
        final byte[] signature = this.getSignature();
        sb.append("            Signature: ");
        sb.append(new String(Hex.encode(signature, 0, 20)));
        sb.append(lineSeparator);
        for (int i = 20; i < signature.length; i += 20) {
            final int length = signature.length;
            sb.append("                       ");
            String str;
            if (i < length - 20) {
                str = new String(Hex.encode(signature, i, 20));
            }
            else {
                str = new String(Hex.encode(signature, i, signature.length - i));
            }
            sb.append(str);
            sb.append(lineSeparator);
        }
        final Extensions extensions = this.c.getTBSCertificate().getExtensions();
        while (true) {
            Label_0718: {
                if (extensions == null) {
                    break Label_0718;
                }
                final Enumeration oids = extensions.oids();
                if (oids.hasMoreElements()) {
                    sb.append("       Extensions: \n");
                }
                while (true) {
                    if (!oids.hasMoreElements()) {
                        break Label_0718;
                    }
                    final ASN1ObjectIdentifier asn1ObjectIdentifier = oids.nextElement();
                    final Extension extension = extensions.getExtension(asn1ObjectIdentifier);
                    Label_0707: {
                        if (extension.getExtnValue() == null) {
                            break Label_0707;
                        }
                        final ASN1InputStream asn1InputStream = new ASN1InputStream(extension.getExtnValue().getOctets());
                        sb.append("                       critical(");
                        sb.append(extension.isCritical());
                        sb.append(") ");
                        try {
                            Label_0517: {
                                ASN1Object obj;
                                if (asn1ObjectIdentifier.equals(Extension.basicConstraints)) {
                                    obj = BasicConstraints.getInstance(asn1InputStream.readObject());
                                }
                                else if (asn1ObjectIdentifier.equals(Extension.keyUsage)) {
                                    obj = KeyUsage.getInstance(asn1InputStream.readObject());
                                }
                                else if (asn1ObjectIdentifier.equals(MiscObjectIdentifiers.netscapeCertType)) {
                                    obj = new NetscapeCertType(DERBitString.getInstance(asn1InputStream.readObject()));
                                }
                                else if (asn1ObjectIdentifier.equals(MiscObjectIdentifiers.netscapeRevocationURL)) {
                                    obj = new NetscapeRevocationURL(DERIA5String.getInstance(asn1InputStream.readObject()));
                                }
                                else {
                                    if (!asn1ObjectIdentifier.equals(MiscObjectIdentifiers.verisignCzagExtension)) {
                                        sb.append(asn1ObjectIdentifier.getId());
                                        sb.append(" value = ");
                                        sb.append(ASN1Dump.dumpAsString(asn1InputStream.readObject()));
                                        break Label_0517;
                                    }
                                    obj = new VerisignCzagExtension(DERIA5String.getInstance(asn1InputStream.readObject()));
                                }
                                sb.append(obj);
                            }
                            sb.append(lineSeparator);
                            continue;
                            sb.append(asn1ObjectIdentifier.getId());
                            sb.append(" value = ");
                            sb.append("*****");
                            sb.append(lineSeparator);
                            continue;
                            return sb.toString();
                        }
                        catch (Exception ex) {}
                    }
                    break;
                }
            }
            continue;
        }
    }
    
    @Override
    public final void verify(final PublicKey publicKey) throws CertificateException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException, SignatureException {
        final String signatureName = X509SignatureUtil.getSignatureName(this.c.getSignatureAlgorithm());
        while (true) {
            try {
                Signature signature = this.bcHelper.createSignature(signatureName);
                while (true) {
                    this.checkSignature(publicKey, signature);
                    return;
                    signature = Signature.getInstance(signatureName);
                    continue;
                }
            }
            catch (Exception ex) {}
            continue;
        }
    }
    
    @Override
    public final void verify(final PublicKey publicKey, final String provider) throws CertificateException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException, SignatureException {
        final String signatureName = X509SignatureUtil.getSignatureName(this.c.getSignatureAlgorithm());
        Signature signature;
        if (provider != null) {
            signature = Signature.getInstance(signatureName, provider);
        }
        else {
            signature = Signature.getInstance(signatureName);
        }
        this.checkSignature(publicKey, signature);
    }
    
    @Override
    public final void verify(final PublicKey publicKey, final Provider provider) throws CertificateException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        final String signatureName = X509SignatureUtil.getSignatureName(this.c.getSignatureAlgorithm());
        Signature signature;
        if (provider != null) {
            signature = Signature.getInstance(signatureName, provider);
        }
        else {
            signature = Signature.getInstance(signatureName);
        }
        this.checkSignature(publicKey, signature);
    }
}
