// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.dsa;

import org.bouncycastle.util.Strings;
import org.bouncycastle.jcajce.provider.asymmetric.util.KeyUtil;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.ASN1Encodable;
import java.io.IOException;
import org.bouncycastle.asn1.x509.DSAParameter;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import java.security.spec.DSAParameterSpec;
import java.security.spec.DSAPublicKeySpec;
import org.bouncycastle.crypto.params.DSAPublicKeyParameters;
import java.security.interfaces.DSAParams;
import java.math.BigInteger;
import java.security.interfaces.DSAPublicKey;

public class BCDSAPublicKey implements DSAPublicKey
{
    private static BigInteger ZERO;
    private static final long serialVersionUID = 1752452449903495175L;
    private transient DSAParams dsaSpec;
    private transient DSAPublicKeyParameters lwKeyParams;
    private BigInteger y;
    
    static {
        BCDSAPublicKey.ZERO = BigInteger.valueOf(0L);
    }
    
    BCDSAPublicKey(final DSAPublicKey dsaPublicKey) {
        this.y = dsaPublicKey.getY();
        this.dsaSpec = dsaPublicKey.getParams();
        this.lwKeyParams = new DSAPublicKeyParameters(this.y, DSAUtil.toDSAParameters(this.dsaSpec));
    }
    
    BCDSAPublicKey(final DSAPublicKeySpec dsaPublicKeySpec) {
        this.y = dsaPublicKeySpec.getY();
        this.dsaSpec = new DSAParameterSpec(dsaPublicKeySpec.getP(), dsaPublicKeySpec.getQ(), dsaPublicKeySpec.getG());
        this.lwKeyParams = new DSAPublicKeyParameters(this.y, DSAUtil.toDSAParameters(this.dsaSpec));
    }
    
    public BCDSAPublicKey(final SubjectPublicKeyInfo subjectPublicKeyInfo) {
        try {
            this.y = ((ASN1Integer)subjectPublicKeyInfo.parsePublicKey()).getValue();
            if (this.isNotNull(subjectPublicKeyInfo.getAlgorithm().getParameters())) {
                final DSAParameter instance = DSAParameter.getInstance(subjectPublicKeyInfo.getAlgorithm().getParameters());
                this.dsaSpec = new DSAParameterSpec(instance.getP(), instance.getQ(), instance.getG());
            }
            else {
                this.dsaSpec = null;
            }
            this.lwKeyParams = new DSAPublicKeyParameters(this.y, DSAUtil.toDSAParameters(this.dsaSpec));
        }
        catch (IOException ex) {
            throw new IllegalArgumentException("invalid info structure in DSA public key");
        }
    }
    
    BCDSAPublicKey(final DSAPublicKeyParameters lwKeyParams) {
        this.y = lwKeyParams.getY();
        DSAParameterSpec dsaSpec;
        if (lwKeyParams != null) {
            dsaSpec = new DSAParameterSpec(lwKeyParams.getParameters().getP(), lwKeyParams.getParameters().getQ(), lwKeyParams.getParameters().getG());
        }
        else {
            dsaSpec = null;
        }
        this.dsaSpec = dsaSpec;
        this.lwKeyParams = lwKeyParams;
    }
    
    private boolean isNotNull(final ASN1Encodable asn1Encodable) {
        return asn1Encodable != null && !DERNull.INSTANCE.equals(asn1Encodable.toASN1Primitive());
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        final BigInteger p = (BigInteger)objectInputStream.readObject();
        if (p.equals(BCDSAPublicKey.ZERO)) {
            this.dsaSpec = null;
        }
        else {
            this.dsaSpec = new DSAParameterSpec(p, (BigInteger)objectInputStream.readObject(), (BigInteger)objectInputStream.readObject());
        }
        this.lwKeyParams = new DSAPublicKeyParameters(this.y, DSAUtil.toDSAParameters(this.dsaSpec));
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        final DSAParams dsaSpec = this.dsaSpec;
        BigInteger obj;
        if (dsaSpec == null) {
            obj = BCDSAPublicKey.ZERO;
        }
        else {
            objectOutputStream.writeObject(dsaSpec.getP());
            objectOutputStream.writeObject(this.dsaSpec.getQ());
            obj = this.dsaSpec.getG();
        }
        objectOutputStream.writeObject(obj);
    }
    
    DSAPublicKeyParameters engineGetKeyParameters() {
        return this.lwKeyParams;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof DSAPublicKey;
        final boolean b2 = false;
        final boolean b3 = false;
        if (!b) {
            return false;
        }
        final DSAPublicKey dsaPublicKey = (DSAPublicKey)o;
        if (this.dsaSpec != null) {
            boolean b4 = b3;
            if (this.getY().equals(dsaPublicKey.getY())) {
                b4 = b3;
                if (dsaPublicKey.getParams() != null) {
                    b4 = b3;
                    if (this.getParams().getG().equals(dsaPublicKey.getParams().getG())) {
                        b4 = b3;
                        if (this.getParams().getP().equals(dsaPublicKey.getParams().getP())) {
                            b4 = b3;
                            if (this.getParams().getQ().equals(dsaPublicKey.getParams().getQ())) {
                                b4 = true;
                            }
                        }
                    }
                }
            }
            return b4;
        }
        boolean b5 = b2;
        if (this.getY().equals(dsaPublicKey.getY())) {
            b5 = b2;
            if (dsaPublicKey.getParams() == null) {
                b5 = true;
            }
        }
        return b5;
    }
    
    @Override
    public String getAlgorithm() {
        return "DSA";
    }
    
    @Override
    public byte[] getEncoded() {
        AlgorithmIdentifier algorithmIdentifier;
        ASN1Integer asn1Integer;
        if (this.dsaSpec == null) {
            algorithmIdentifier = new AlgorithmIdentifier(X9ObjectIdentifiers.id_dsa);
            asn1Integer = new ASN1Integer(this.y);
        }
        else {
            algorithmIdentifier = new AlgorithmIdentifier(X9ObjectIdentifiers.id_dsa, new DSAParameter(this.dsaSpec.getP(), this.dsaSpec.getQ(), this.dsaSpec.getG()).toASN1Primitive());
            asn1Integer = new ASN1Integer(this.y);
        }
        return KeyUtil.getEncodedSubjectPublicKeyInfo(algorithmIdentifier, asn1Integer);
    }
    
    @Override
    public String getFormat() {
        return "X.509";
    }
    
    @Override
    public DSAParams getParams() {
        return this.dsaSpec;
    }
    
    @Override
    public BigInteger getY() {
        return this.y;
    }
    
    @Override
    public int hashCode() {
        if (this.dsaSpec != null) {
            return this.getY().hashCode() ^ this.getParams().getG().hashCode() ^ this.getParams().getP().hashCode() ^ this.getParams().getQ().hashCode();
        }
        return this.getY().hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        final String lineSeparator = Strings.lineSeparator();
        sb.append("DSA Public Key [");
        sb.append(DSAUtil.generateKeyFingerprint(this.y, this.getParams()));
        sb.append("]");
        sb.append(lineSeparator);
        sb.append("            Y: ");
        sb.append(this.getY().toString(16));
        sb.append(lineSeparator);
        return sb.toString();
    }
}
