// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.rsa;

import org.bouncycastle.asn1.ASN1Primitive;
import java.security.interfaces.RSAPublicKey;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;
import java.security.interfaces.RSAPrivateCrtKey;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import java.security.interfaces.RSAPrivateKey;
import org.bouncycastle.util.Fingerprint;
import java.math.BigInteger;
import org.bouncycastle.asn1.x509.X509ObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;

public class RSAUtil
{
    public static final ASN1ObjectIdentifier[] rsaOids;
    
    static {
        rsaOids = new ASN1ObjectIdentifier[] { PKCSObjectIdentifiers.rsaEncryption, X509ObjectIdentifiers.id_ea_rsa, PKCSObjectIdentifiers.id_RSAES_OAEP, PKCSObjectIdentifiers.id_RSASSA_PSS };
    }
    
    static String generateExponentFingerprint(final BigInteger bigInteger) {
        return new Fingerprint(bigInteger.toByteArray(), 32).toString();
    }
    
    static String generateKeyFingerprint(final BigInteger bigInteger) {
        return new Fingerprint(bigInteger.toByteArray()).toString();
    }
    
    static RSAKeyParameters generatePrivateKeyParameter(final RSAPrivateKey rsaPrivateKey) {
        if (rsaPrivateKey instanceof BCRSAPrivateKey) {
            return ((BCRSAPrivateKey)rsaPrivateKey).engineGetKeyParameters();
        }
        if (rsaPrivateKey instanceof RSAPrivateCrtKey) {
            final RSAPrivateCrtKey rsaPrivateCrtKey = (RSAPrivateCrtKey)rsaPrivateKey;
            return new RSAPrivateCrtKeyParameters(rsaPrivateCrtKey.getModulus(), rsaPrivateCrtKey.getPublicExponent(), rsaPrivateCrtKey.getPrivateExponent(), rsaPrivateCrtKey.getPrimeP(), rsaPrivateCrtKey.getPrimeQ(), rsaPrivateCrtKey.getPrimeExponentP(), rsaPrivateCrtKey.getPrimeExponentQ(), rsaPrivateCrtKey.getCrtCoefficient());
        }
        return new RSAKeyParameters(true, rsaPrivateKey.getModulus(), rsaPrivateKey.getPrivateExponent());
    }
    
    static RSAKeyParameters generatePublicKeyParameter(final RSAPublicKey rsaPublicKey) {
        if (rsaPublicKey instanceof BCRSAPublicKey) {
            return ((BCRSAPublicKey)rsaPublicKey).engineGetKeyParameters();
        }
        return new RSAKeyParameters(false, rsaPublicKey.getModulus(), rsaPublicKey.getPublicExponent());
    }
    
    public static boolean isRsaOid(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        int n = 0;
        while (true) {
            final ASN1ObjectIdentifier[] rsaOids = RSAUtil.rsaOids;
            if (n == rsaOids.length) {
                return false;
            }
            if (asn1ObjectIdentifier.equals(rsaOids[n])) {
                return true;
            }
            ++n;
        }
    }
}
