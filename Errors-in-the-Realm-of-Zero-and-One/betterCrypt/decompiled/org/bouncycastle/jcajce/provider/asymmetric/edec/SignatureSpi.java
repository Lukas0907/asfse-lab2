// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.edec;

import org.bouncycastle.crypto.CryptoException;
import java.security.SignatureException;
import org.bouncycastle.crypto.params.Ed448PublicKeyParameters;
import java.security.PublicKey;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.Ed448PrivateKeyParameters;
import java.security.PrivateKey;
import java.security.InvalidParameterException;
import org.bouncycastle.crypto.signers.Ed25519Signer;
import org.bouncycastle.crypto.signers.Ed448Signer;
import java.security.InvalidKeyException;
import org.bouncycastle.crypto.Signer;

public class SignatureSpi extends java.security.SignatureSpi
{
    private static final byte[] EMPTY_CONTEXT;
    private final String algorithm;
    private Signer signer;
    
    static {
        EMPTY_CONTEXT = new byte[0];
    }
    
    SignatureSpi(final String algorithm) {
        this.algorithm = algorithm;
    }
    
    private Signer getSigner(final String s) throws InvalidKeyException {
        final String algorithm = this.algorithm;
        if (algorithm != null && !s.equals(algorithm)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("inappropriate key for ");
            sb.append(this.algorithm);
            throw new InvalidKeyException(sb.toString());
        }
        if (s.equals("Ed448")) {
            return new Ed448Signer(SignatureSpi.EMPTY_CONTEXT);
        }
        return new Ed25519Signer();
    }
    
    @Override
    protected Object engineGetParameter(final String s) throws InvalidParameterException {
        throw new UnsupportedOperationException("engineGetParameter unsupported");
    }
    
    @Override
    protected void engineInitSign(final PrivateKey privateKey) throws InvalidKeyException {
        if (privateKey instanceof BCEdDSAPrivateKey) {
            final AsymmetricKeyParameter engineGetKeyParameters = ((BCEdDSAPrivateKey)privateKey).engineGetKeyParameters();
            String s;
            if (engineGetKeyParameters instanceof Ed448PrivateKeyParameters) {
                s = "Ed448";
            }
            else {
                s = "Ed25519";
            }
            (this.signer = this.getSigner(s)).init(true, engineGetKeyParameters);
            return;
        }
        throw new InvalidKeyException("cannot identify EdDSA private key");
    }
    
    @Override
    protected void engineInitVerify(final PublicKey publicKey) throws InvalidKeyException {
        if (publicKey instanceof BCEdDSAPublicKey) {
            final AsymmetricKeyParameter engineGetKeyParameters = ((BCEdDSAPublicKey)publicKey).engineGetKeyParameters();
            String s;
            if (engineGetKeyParameters instanceof Ed448PublicKeyParameters) {
                s = "Ed448";
            }
            else {
                s = "Ed25519";
            }
            (this.signer = this.getSigner(s)).init(false, engineGetKeyParameters);
            return;
        }
        throw new InvalidKeyException("cannot identify EdDSA public key");
    }
    
    @Override
    protected void engineSetParameter(final String s, final Object o) throws InvalidParameterException {
        throw new UnsupportedOperationException("engineSetParameter unsupported");
    }
    
    @Override
    protected byte[] engineSign() throws SignatureException {
        try {
            return this.signer.generateSignature();
        }
        catch (CryptoException ex) {
            throw new SignatureException(ex.getMessage());
        }
    }
    
    @Override
    protected void engineUpdate(final byte b) throws SignatureException {
        this.signer.update(b);
    }
    
    @Override
    protected void engineUpdate(final byte[] array, final int n, final int n2) throws SignatureException {
        this.signer.update(array, n, n2);
    }
    
    @Override
    protected boolean engineVerify(final byte[] array) throws SignatureException {
        return this.signer.verifySignature(array);
    }
    
    public static final class Ed25519 extends SignatureSpi
    {
        public Ed25519() {
            super("Ed25519");
        }
    }
    
    public static final class Ed448 extends SignatureSpi
    {
        public Ed448() {
            super("Ed448");
        }
    }
    
    public static final class EdDSA extends SignatureSpi
    {
        public EdDSA() {
            super(null);
        }
    }
}
