// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.dsa;

import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import java.security.InvalidKeyException;
import java.io.IOException;
import org.bouncycastle.crypto.params.DSAParameters;
import java.security.interfaces.DSAPrivateKey;
import java.security.interfaces.DSAPublicKey;
import java.security.Key;
import org.bouncycastle.crypto.params.DSAPublicKeyParameters;
import org.bouncycastle.crypto.util.OpenSSHPublicKeyUtil;
import org.bouncycastle.jcajce.spec.OpenSSHPublicKeySpec;
import java.security.spec.DSAPublicKeySpec;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.DSAPrivateKeyParameters;
import org.bouncycastle.crypto.util.OpenSSHPrivateKeyUtil;
import org.bouncycastle.jcajce.spec.OpenSSHPrivateKeySpec;
import java.security.spec.DSAPrivateKeySpec;
import java.security.PrivateKey;
import java.security.spec.KeySpec;
import org.bouncycastle.jcajce.provider.asymmetric.util.BaseKeyFactorySpi;

public class KeyFactorySpi extends BaseKeyFactorySpi
{
    @Override
    protected PrivateKey engineGeneratePrivate(final KeySpec keySpec) throws InvalidKeySpecException {
        if (keySpec instanceof DSAPrivateKeySpec) {
            return new BCDSAPrivateKey((DSAPrivateKeySpec)keySpec);
        }
        if (!(keySpec instanceof OpenSSHPrivateKeySpec)) {
            return super.engineGeneratePrivate(keySpec);
        }
        final AsymmetricKeyParameter privateKeyBlob = OpenSSHPrivateKeyUtil.parsePrivateKeyBlob(((OpenSSHPrivateKeySpec)keySpec).getEncoded());
        if (privateKeyBlob instanceof DSAPrivateKeyParameters) {
            final DSAPrivateKeyParameters dsaPrivateKeyParameters = (DSAPrivateKeyParameters)privateKeyBlob;
            return this.engineGeneratePrivate(new DSAPrivateKeySpec(dsaPrivateKeyParameters.getX(), dsaPrivateKeyParameters.getParameters().getP(), dsaPrivateKeyParameters.getParameters().getQ(), dsaPrivateKeyParameters.getParameters().getG()));
        }
        throw new IllegalArgumentException("openssh private key is not dsa privare key");
    }
    
    @Override
    protected PublicKey engineGeneratePublic(final KeySpec keySpec) throws InvalidKeySpecException {
        if (keySpec instanceof DSAPublicKeySpec) {
            try {
                return new BCDSAPublicKey((DSAPublicKeySpec)keySpec);
            }
            catch (Exception ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("invalid KeySpec: ");
                sb.append(ex.getMessage());
                throw new InvalidKeySpecException(sb.toString()) {
                    @Override
                    public Throwable getCause() {
                        return ex;
                    }
                };
            }
        }
        if (!(keySpec instanceof OpenSSHPublicKeySpec)) {
            return super.engineGeneratePublic(keySpec);
        }
        final AsymmetricKeyParameter publicKey = OpenSSHPublicKeyUtil.parsePublicKey(((OpenSSHPublicKeySpec)keySpec).getEncoded());
        if (publicKey instanceof DSAPublicKeyParameters) {
            final DSAPublicKeyParameters dsaPublicKeyParameters = (DSAPublicKeyParameters)publicKey;
            return this.engineGeneratePublic(new DSAPublicKeySpec(dsaPublicKeyParameters.getY(), dsaPublicKeyParameters.getParameters().getP(), dsaPublicKeyParameters.getParameters().getQ(), dsaPublicKeyParameters.getParameters().getG()));
        }
        throw new IllegalArgumentException("openssh public key is not dsa public key");
    }
    
    @Override
    protected KeySpec engineGetKeySpec(final Key key, final Class clazz) throws InvalidKeySpecException {
        if (clazz.isAssignableFrom(DSAPublicKeySpec.class) && key instanceof DSAPublicKey) {
            final DSAPublicKey dsaPublicKey = (DSAPublicKey)key;
            return new DSAPublicKeySpec(dsaPublicKey.getY(), dsaPublicKey.getParams().getP(), dsaPublicKey.getParams().getQ(), dsaPublicKey.getParams().getG());
        }
        if (clazz.isAssignableFrom(DSAPrivateKeySpec.class) && key instanceof DSAPrivateKey) {
            final DSAPrivateKey dsaPrivateKey = (DSAPrivateKey)key;
            return new DSAPrivateKeySpec(dsaPrivateKey.getX(), dsaPrivateKey.getParams().getP(), dsaPrivateKey.getParams().getQ(), dsaPrivateKey.getParams().getG());
        }
        if (clazz.isAssignableFrom(OpenSSHPublicKeySpec.class) && key instanceof DSAPublicKey) {
            final DSAPublicKey dsaPublicKey2 = (DSAPublicKey)key;
            try {
                return new OpenSSHPublicKeySpec(OpenSSHPublicKeyUtil.encodePublicKey(new DSAPublicKeyParameters(dsaPublicKey2.getY(), new DSAParameters(dsaPublicKey2.getParams().getP(), dsaPublicKey2.getParams().getQ(), dsaPublicKey2.getParams().getG()))));
            }
            catch (IOException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("unable to produce encoding: ");
                sb.append(ex.getMessage());
                throw new IllegalArgumentException(sb.toString());
            }
        }
        if (clazz.isAssignableFrom(OpenSSHPrivateKeySpec.class) && key instanceof DSAPrivateKey) {
            final DSAPrivateKey dsaPrivateKey2 = (DSAPrivateKey)key;
            try {
                return new OpenSSHPrivateKeySpec(OpenSSHPrivateKeyUtil.encodePrivateKey(new DSAPrivateKeyParameters(dsaPrivateKey2.getX(), new DSAParameters(dsaPrivateKey2.getParams().getP(), dsaPrivateKey2.getParams().getQ(), dsaPrivateKey2.getParams().getG()))));
            }
            catch (IOException ex2) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("unable to produce encoding: ");
                sb2.append(ex2.getMessage());
                throw new IllegalArgumentException(sb2.toString());
            }
        }
        if (clazz.isAssignableFrom(org.bouncycastle.jce.spec.OpenSSHPublicKeySpec.class) && key instanceof DSAPublicKey) {
            final DSAPublicKey dsaPublicKey3 = (DSAPublicKey)key;
            try {
                return new org.bouncycastle.jce.spec.OpenSSHPublicKeySpec(OpenSSHPublicKeyUtil.encodePublicKey(new DSAPublicKeyParameters(dsaPublicKey3.getY(), new DSAParameters(dsaPublicKey3.getParams().getP(), dsaPublicKey3.getParams().getQ(), dsaPublicKey3.getParams().getG()))));
            }
            catch (IOException ex3) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("unable to produce encoding: ");
                sb3.append(ex3.getMessage());
                throw new IllegalArgumentException(sb3.toString());
            }
        }
        if (clazz.isAssignableFrom(org.bouncycastle.jce.spec.OpenSSHPrivateKeySpec.class) && key instanceof DSAPrivateKey) {
            final DSAPrivateKey dsaPrivateKey3 = (DSAPrivateKey)key;
            try {
                return new org.bouncycastle.jce.spec.OpenSSHPrivateKeySpec(OpenSSHPrivateKeyUtil.encodePrivateKey(new DSAPrivateKeyParameters(dsaPrivateKey3.getX(), new DSAParameters(dsaPrivateKey3.getParams().getP(), dsaPrivateKey3.getParams().getQ(), dsaPrivateKey3.getParams().getG()))));
            }
            catch (IOException ex4) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("unable to produce encoding: ");
                sb4.append(ex4.getMessage());
                throw new IllegalArgumentException(sb4.toString());
            }
        }
        return super.engineGetKeySpec(key, clazz);
    }
    
    @Override
    protected Key engineTranslateKey(final Key key) throws InvalidKeyException {
        if (key instanceof DSAPublicKey) {
            return new BCDSAPublicKey((DSAPublicKey)key);
        }
        if (key instanceof DSAPrivateKey) {
            return new BCDSAPrivateKey((DSAPrivateKey)key);
        }
        throw new InvalidKeyException("key type unknown");
    }
    
    @Override
    public PrivateKey generatePrivate(final PrivateKeyInfo privateKeyInfo) throws IOException {
        final ASN1ObjectIdentifier algorithm = privateKeyInfo.getPrivateKeyAlgorithm().getAlgorithm();
        if (DSAUtil.isDsaOid(algorithm)) {
            return new BCDSAPrivateKey(privateKeyInfo);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("algorithm identifier ");
        sb.append(algorithm);
        sb.append(" in key not recognised");
        throw new IOException(sb.toString());
    }
    
    @Override
    public PublicKey generatePublic(final SubjectPublicKeyInfo subjectPublicKeyInfo) throws IOException {
        final ASN1ObjectIdentifier algorithm = subjectPublicKeyInfo.getAlgorithm().getAlgorithm();
        if (DSAUtil.isDsaOid(algorithm)) {
            return new BCDSAPublicKey(subjectPublicKeyInfo);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("algorithm identifier ");
        sb.append(algorithm);
        sb.append(" in key not recognised");
        throw new IOException(sb.toString());
    }
}
