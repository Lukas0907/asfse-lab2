// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.ec;

import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;
import org.bouncycastle.jce.spec.ECNamedCurveGenParameterSpec;
import java.security.InvalidParameterException;
import java.security.spec.AlgorithmParameterSpec;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import java.security.KeyPair;
import java.security.InvalidAlgorithmParameterException;
import org.bouncycastle.asn1.x9.ECNamedCurveTable;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.asn1.x9.X9ECParameters;
import java.math.BigInteger;
import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util;
import org.bouncycastle.jce.spec.ECNamedCurveSpec;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.crypto.CryptoServicesRegistrar;
import java.security.spec.ECGenParameterSpec;
import org.bouncycastle.util.Integers;
import java.security.SecureRandom;
import org.bouncycastle.crypto.params.ECKeyGenerationParameters;
import org.bouncycastle.crypto.generators.ECKeyPairGenerator;
import org.bouncycastle.jcajce.provider.config.ProviderConfiguration;
import java.util.Hashtable;
import java.security.KeyPairGenerator;

public abstract class KeyPairGeneratorSpi extends KeyPairGenerator
{
    public KeyPairGeneratorSpi(final String algorithm) {
        super(algorithm);
    }
    
    public static class EC extends KeyPairGeneratorSpi
    {
        private static Hashtable ecParameters;
        String algorithm;
        ProviderConfiguration configuration;
        Object ecParams;
        ECKeyPairGenerator engine;
        boolean initialised;
        ECKeyGenerationParameters param;
        SecureRandom random;
        int strength;
        
        static {
            (EC.ecParameters = new Hashtable()).put(Integers.valueOf(192), new ECGenParameterSpec("prime192v1"));
            EC.ecParameters.put(Integers.valueOf(239), new ECGenParameterSpec("prime239v1"));
            EC.ecParameters.put(Integers.valueOf(256), new ECGenParameterSpec("prime256v1"));
            EC.ecParameters.put(Integers.valueOf(224), new ECGenParameterSpec("P-224"));
            EC.ecParameters.put(Integers.valueOf(384), new ECGenParameterSpec("P-384"));
            EC.ecParameters.put(Integers.valueOf(521), new ECGenParameterSpec("P-521"));
        }
        
        public EC() {
            super("EC");
            this.engine = new ECKeyPairGenerator();
            this.ecParams = null;
            this.strength = 239;
            this.random = CryptoServicesRegistrar.getSecureRandom();
            this.initialised = false;
            this.algorithm = "EC";
            this.configuration = BouncyCastleProvider.CONFIGURATION;
        }
        
        public EC(final String algorithm, final ProviderConfiguration configuration) {
            super(algorithm);
            this.engine = new ECKeyPairGenerator();
            this.ecParams = null;
            this.strength = 239;
            this.random = CryptoServicesRegistrar.getSecureRandom();
            this.initialised = false;
            this.algorithm = algorithm;
            this.configuration = configuration;
        }
        
        protected ECKeyGenerationParameters createKeyGenParamsBC(final ECParameterSpec ecParameterSpec, final SecureRandom secureRandom) {
            return new ECKeyGenerationParameters(new ECDomainParameters(ecParameterSpec.getCurve(), ecParameterSpec.getG(), ecParameterSpec.getN(), ecParameterSpec.getH()), secureRandom);
        }
        
        protected ECKeyGenerationParameters createKeyGenParamsJCE(final java.security.spec.ECParameterSpec ecParameterSpec, final SecureRandom secureRandom) {
            if (ecParameterSpec instanceof ECNamedCurveSpec) {
                final X9ECParameters domainParametersFromName = ECUtils.getDomainParametersFromName(((ECNamedCurveSpec)ecParameterSpec).getName());
                if (domainParametersFromName != null) {
                    return new ECKeyGenerationParameters(new ECDomainParameters(domainParametersFromName.getCurve(), domainParametersFromName.getG(), domainParametersFromName.getN(), domainParametersFromName.getH()), secureRandom);
                }
            }
            final ECCurve convertCurve = EC5Util.convertCurve(ecParameterSpec.getCurve());
            return new ECKeyGenerationParameters(new ECDomainParameters(convertCurve, EC5Util.convertPoint(convertCurve, ecParameterSpec.getGenerator()), ecParameterSpec.getOrder(), BigInteger.valueOf(ecParameterSpec.getCofactor())), secureRandom);
        }
        
        protected ECNamedCurveSpec createNamedCurveSpec(final String s) throws InvalidAlgorithmParameterException {
            Label_0128: {
                X9ECParameters x9ECParameters;
                if ((x9ECParameters = ECUtils.getDomainParametersFromName(s)) != null) {
                    break Label_0128;
                }
                while (true) {
                    while (true) {
                        try {
                            if ((x9ECParameters = ECNamedCurveTable.getByOID(new ASN1ObjectIdentifier(s))) == null) {
                                x9ECParameters = this.configuration.getAdditionalECParameters().get(new ASN1ObjectIdentifier(s));
                                if (x9ECParameters == null) {
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append("unknown curve OID: ");
                                    sb.append(s);
                                    throw new InvalidAlgorithmParameterException(sb.toString());
                                }
                            }
                            return new ECNamedCurveSpec(s, x9ECParameters.getCurve(), x9ECParameters.getG(), x9ECParameters.getN(), x9ECParameters.getH(), null);
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("unknown curve name: ");
                            sb2.append(s);
                            throw new InvalidAlgorithmParameterException(sb2.toString());
                        }
                        catch (IllegalArgumentException ex) {}
                        continue;
                    }
                }
            }
        }
        
        @Override
        public KeyPair generateKeyPair() {
            if (!this.initialised) {
                this.initialize(this.strength, new SecureRandom());
            }
            final AsymmetricCipherKeyPair generateKeyPair = this.engine.generateKeyPair();
            final ECPublicKeyParameters ecPublicKeyParameters = (ECPublicKeyParameters)generateKeyPair.getPublic();
            final ECPrivateKeyParameters ecPrivateKeyParameters = (ECPrivateKeyParameters)generateKeyPair.getPrivate();
            final Object ecParams = this.ecParams;
            if (ecParams instanceof ECParameterSpec) {
                final ECParameterSpec ecParameterSpec = (ECParameterSpec)ecParams;
                final BCECPublicKey publicKey = new BCECPublicKey(this.algorithm, ecPublicKeyParameters, ecParameterSpec, this.configuration);
                return new KeyPair(publicKey, new BCECPrivateKey(this.algorithm, ecPrivateKeyParameters, publicKey, ecParameterSpec, this.configuration));
            }
            if (ecParams == null) {
                return new KeyPair(new BCECPublicKey(this.algorithm, ecPublicKeyParameters, this.configuration), new BCECPrivateKey(this.algorithm, ecPrivateKeyParameters, this.configuration));
            }
            final java.security.spec.ECParameterSpec ecParameterSpec2 = (java.security.spec.ECParameterSpec)ecParams;
            final BCECPublicKey publicKey2 = new BCECPublicKey(this.algorithm, ecPublicKeyParameters, ecParameterSpec2, this.configuration);
            return new KeyPair(publicKey2, new BCECPrivateKey(this.algorithm, ecPrivateKeyParameters, publicKey2, ecParameterSpec2, this.configuration));
        }
        
        @Override
        public void initialize(final int strength, final SecureRandom random) {
            this.strength = strength;
            this.random = random;
            final ECGenParameterSpec ecGenParameterSpec = EC.ecParameters.get(Integers.valueOf(strength));
            Label_0046: {
                if (ecGenParameterSpec == null) {
                    break Label_0046;
                }
                while (true) {
                    while (true) {
                        try {
                            this.initialize(ecGenParameterSpec, random);
                            return;
                            throw new InvalidParameterException("unknown key size.");
                            throw new InvalidParameterException("key size not configurable.");
                        }
                        catch (InvalidAlgorithmParameterException ex) {}
                        continue;
                    }
                }
            }
        }
        
        @Override
        public void initialize(final AlgorithmParameterSpec obj, final SecureRandom secureRandom) throws InvalidAlgorithmParameterException {
            Label_0151: {
                ECKeyGenerationParameters param = null;
                Label_0061: {
                    ECParameterSpec ecImplicitlyCa;
                    if (obj == null) {
                        ecImplicitlyCa = this.configuration.getEcImplicitlyCa();
                        if (ecImplicitlyCa == null) {
                            throw new InvalidAlgorithmParameterException("null parameter passed but no implicitCA set");
                        }
                        this.ecParams = null;
                    }
                    else if (obj instanceof ECParameterSpec) {
                        this.ecParams = obj;
                        ecImplicitlyCa = (ECParameterSpec)obj;
                    }
                    else {
                        if (obj instanceof java.security.spec.ECParameterSpec) {
                            this.ecParams = obj;
                            param = this.createKeyGenParamsJCE((java.security.spec.ECParameterSpec)obj, secureRandom);
                            break Label_0061;
                        }
                        String s;
                        if (obj instanceof ECGenParameterSpec) {
                            s = ((ECGenParameterSpec)obj).getName();
                        }
                        else if (obj instanceof ECNamedCurveGenParameterSpec) {
                            s = ((ECNamedCurveGenParameterSpec)obj).getName();
                        }
                        else {
                            final String name = ECUtil.getNameFrom(obj);
                            if (name != null) {
                                this.initializeNamedCurve(name, secureRandom);
                                break Label_0151;
                            }
                            final StringBuilder sb = new StringBuilder();
                            sb.append("invalid parameterSpec: ");
                            sb.append(obj);
                            throw new InvalidAlgorithmParameterException(sb.toString());
                        }
                        this.initializeNamedCurve(s, secureRandom);
                        break Label_0151;
                    }
                    param = this.createKeyGenParamsBC(ecImplicitlyCa, secureRandom);
                }
                this.param = param;
            }
            this.engine.init(this.param);
            this.initialised = true;
        }
        
        protected void initializeNamedCurve(final String s, final SecureRandom secureRandom) throws InvalidAlgorithmParameterException {
            final ECNamedCurveSpec namedCurveSpec = this.createNamedCurveSpec(s);
            this.ecParams = namedCurveSpec;
            this.param = this.createKeyGenParamsJCE(namedCurveSpec, secureRandom);
        }
    }
    
    public static class ECDH extends EC
    {
        public ECDH() {
            super("ECDH", BouncyCastleProvider.CONFIGURATION);
        }
    }
    
    public static class ECDHC extends EC
    {
        public ECDHC() {
            super("ECDHC", BouncyCastleProvider.CONFIGURATION);
        }
    }
    
    public static class ECDSA extends EC
    {
        public ECDSA() {
            super("ECDSA", BouncyCastleProvider.CONFIGURATION);
        }
    }
    
    public static class ECMQV extends EC
    {
        public ECMQV() {
            super("ECMQV", BouncyCastleProvider.CONFIGURATION);
        }
    }
}
