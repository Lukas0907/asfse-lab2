// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.x509;

import java.security.cert.CertificateEncodingException;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.jcajce.util.JcaJceHelper;

class X509CertificateInternal extends X509CertificateImpl
{
    private final byte[] encoding;
    
    X509CertificateInternal(final JcaJceHelper jcaJceHelper, final org.bouncycastle.asn1.x509.Certificate certificate, final BasicConstraints basicConstraints, final boolean[] array, final byte[] encoding) {
        super(jcaJceHelper, certificate, basicConstraints, array);
        this.encoding = encoding;
    }
    
    @Override
    public byte[] getEncoded() throws CertificateEncodingException {
        final byte[] encoding = this.encoding;
        if (encoding != null) {
            return encoding;
        }
        throw new CertificateEncodingException();
    }
}
