// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.util;

import org.bouncycastle.crypto.params.DHPublicKeyParameters;
import javax.crypto.interfaces.DHPublicKey;
import org.bouncycastle.jcajce.provider.asymmetric.dh.BCDHPublicKey;
import java.security.PublicKey;
import java.security.InvalidKeyException;
import org.bouncycastle.crypto.params.DHPrivateKeyParameters;
import java.math.BigInteger;
import org.bouncycastle.crypto.params.DHParameters;
import javax.crypto.interfaces.DHPrivateKey;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import java.security.PrivateKey;

public class DHUtil
{
    public static AsymmetricKeyParameter generatePrivateKeyParameter(final PrivateKey privateKey) throws InvalidKeyException {
        if (privateKey instanceof DHPrivateKey) {
            final DHPrivateKey dhPrivateKey = (DHPrivateKey)privateKey;
            return new DHPrivateKeyParameters(dhPrivateKey.getX(), new DHParameters(dhPrivateKey.getParams().getP(), dhPrivateKey.getParams().getG(), null, dhPrivateKey.getParams().getL()));
        }
        throw new InvalidKeyException("can't identify DH private key.");
    }
    
    public static AsymmetricKeyParameter generatePublicKeyParameter(final PublicKey publicKey) throws InvalidKeyException {
        if (publicKey instanceof BCDHPublicKey) {
            return ((BCDHPublicKey)publicKey).engineGetKeyParameters();
        }
        if (publicKey instanceof DHPublicKey) {
            final DHPublicKey dhPublicKey = (DHPublicKey)publicKey;
            return new DHPublicKeyParameters(dhPublicKey.getY(), new DHParameters(dhPublicKey.getParams().getP(), dhPublicKey.getParams().getG(), null, dhPublicKey.getParams().getL()));
        }
        throw new InvalidKeyException("can't identify DH public key.");
    }
}
