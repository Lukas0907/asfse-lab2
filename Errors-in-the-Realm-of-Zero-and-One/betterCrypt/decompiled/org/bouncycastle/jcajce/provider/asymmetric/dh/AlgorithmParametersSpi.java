// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.dh;

import java.security.spec.InvalidParameterSpecException;
import java.security.spec.AlgorithmParameterSpec;
import java.io.IOException;
import org.bouncycastle.asn1.pkcs.DHParameter;
import javax.crypto.spec.DHParameterSpec;

public class AlgorithmParametersSpi extends java.security.AlgorithmParametersSpi
{
    DHParameterSpec currentSpec;
    
    @Override
    protected byte[] engineGetEncoded() {
        final DHParameter dhParameter = new DHParameter(this.currentSpec.getP(), this.currentSpec.getG(), this.currentSpec.getL());
        try {
            return dhParameter.getEncoded("DER");
        }
        catch (IOException ex) {
            throw new RuntimeException("Error encoding DHParameters");
        }
    }
    
    @Override
    protected byte[] engineGetEncoded(final String s) {
        if (this.isASN1FormatString(s)) {
            return this.engineGetEncoded();
        }
        return null;
    }
    
    @Override
    protected AlgorithmParameterSpec engineGetParameterSpec(final Class clazz) throws InvalidParameterSpecException {
        if (clazz != null) {
            return this.localEngineGetParameterSpec(clazz);
        }
        throw new NullPointerException("argument to getParameterSpec must not be null");
    }
    
    @Override
    protected void engineInit(final AlgorithmParameterSpec algorithmParameterSpec) throws InvalidParameterSpecException {
        if (algorithmParameterSpec instanceof DHParameterSpec) {
            this.currentSpec = (DHParameterSpec)algorithmParameterSpec;
            return;
        }
        throw new InvalidParameterSpecException("DHParameterSpec required to initialise a Diffie-Hellman algorithm parameters object");
    }
    
    @Override
    protected void engineInit(final byte[] array) throws IOException {
        while (true) {
            while (true) {
                try {
                    final DHParameter instance = DHParameter.getInstance(array);
                    DHParameterSpec currentSpec;
                    if (instance.getL() != null) {
                        currentSpec = new DHParameterSpec(instance.getP(), instance.getG(), instance.getL().intValue());
                    }
                    else {
                        currentSpec = new DHParameterSpec(instance.getP(), instance.getG());
                    }
                    this.currentSpec = currentSpec;
                    return;
                    throw new IOException("Not a valid DH Parameter encoding.");
                }
                catch (ClassCastException ex) {
                    throw new IOException("Not a valid DH Parameter encoding.");
                }
                catch (ArrayIndexOutOfBoundsException ex2) {}
                continue;
            }
        }
    }
    
    @Override
    protected void engineInit(final byte[] array, final String str) throws IOException {
        if (this.isASN1FormatString(str)) {
            this.engineInit(array);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown parameter format ");
        sb.append(str);
        throw new IOException(sb.toString());
    }
    
    @Override
    protected String engineToString() {
        return "Diffie-Hellman Parameters";
    }
    
    protected boolean isASN1FormatString(final String s) {
        return s == null || s.equals("ASN.1");
    }
    
    protected AlgorithmParameterSpec localEngineGetParameterSpec(final Class clazz) throws InvalidParameterSpecException {
        if (clazz != DHParameterSpec.class && clazz != AlgorithmParameterSpec.class) {
            throw new InvalidParameterSpecException("unknown parameter spec passed to DH parameters object.");
        }
        return this.currentSpec;
    }
}
