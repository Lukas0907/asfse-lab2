// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.ec;

import org.bouncycastle.math.ec.ECCurve;
import java.math.BigInteger;
import org.bouncycastle.asn1.x9.X9ECPoint;
import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util;
import org.bouncycastle.asn1.ASN1Null;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.jce.spec.ECNamedCurveSpec;
import org.bouncycastle.asn1.x9.X962Parameters;
import java.security.spec.ECParameterSpec;
import org.bouncycastle.asn1.x9.X9ECParameters;
import java.security.spec.ECGenParameterSpec;
import java.security.InvalidKeyException;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import java.security.PublicKey;

class ECUtils
{
    static AsymmetricKeyParameter generatePublicKeyParameter(final PublicKey publicKey) throws InvalidKeyException {
        if (publicKey instanceof BCECPublicKey) {
            return ((BCECPublicKey)publicKey).engineGetKeyParameters();
        }
        return ECUtil.generatePublicKeyParameter(publicKey);
    }
    
    static X9ECParameters getDomainParametersFromGenSpec(final ECGenParameterSpec ecGenParameterSpec) {
        return getDomainParametersFromName(ecGenParameterSpec.getName());
    }
    
    static X962Parameters getDomainParametersFromName(final ECParameterSpec ecParameterSpec, final boolean b) {
        if (ecParameterSpec instanceof ECNamedCurveSpec) {
            final ECNamedCurveSpec ecNamedCurveSpec = (ECNamedCurveSpec)ecParameterSpec;
            ASN1ObjectIdentifier namedCurveOid;
            if ((namedCurveOid = ECUtil.getNamedCurveOid(ecNamedCurveSpec.getName())) == null) {
                namedCurveOid = new ASN1ObjectIdentifier(ecNamedCurveSpec.getName());
            }
            return new X962Parameters(namedCurveOid);
        }
        if (ecParameterSpec == null) {
            return new X962Parameters(DERNull.INSTANCE);
        }
        final ECCurve convertCurve = EC5Util.convertCurve(ecParameterSpec.getCurve());
        return new X962Parameters(new X9ECParameters(convertCurve, new X9ECPoint(EC5Util.convertPoint(convertCurve, ecParameterSpec.getGenerator()), b), ecParameterSpec.getOrder(), BigInteger.valueOf(ecParameterSpec.getCofactor()), ecParameterSpec.getCurve().getSeed()));
    }
    
    static X9ECParameters getDomainParametersFromName(final String s) {
        String s2 = s;
        try {
            if (s.charAt(0) >= '0') {
                s2 = s;
                if (s.charAt(0) <= '2') {
                    s2 = s;
                    return ECUtil.getNamedCurveByOid(new ASN1ObjectIdentifier(s));
                }
            }
            String substring = s;
            s2 = s;
            if (s.indexOf(32) > 0) {
                s2 = s;
                substring = s.substring(s.indexOf(32) + 1);
            }
            s2 = substring;
            return ECUtil.getNamedCurveByName(substring);
        }
        catch (IllegalArgumentException ex) {
            return ECUtil.getNamedCurveByName(s2);
        }
    }
}
