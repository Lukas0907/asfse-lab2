// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.rsa;

import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import java.security.InvalidKeyException;
import org.bouncycastle.crypto.util.OpenSSHPrivateKeyUtil;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;
import org.bouncycastle.jcajce.spec.OpenSSHPrivateKeySpec;
import java.io.IOException;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.RSAPrivateKeySpec;
import java.security.interfaces.RSAPublicKey;
import java.security.Key;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.util.OpenSSHPublicKeyUtil;
import org.bouncycastle.jcajce.spec.OpenSSHPublicKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.PrivateKey;
import java.security.spec.KeySpec;
import org.bouncycastle.jcajce.provider.asymmetric.util.BaseKeyFactorySpi;

public class KeyFactorySpi extends BaseKeyFactorySpi
{
    @Override
    protected PrivateKey engineGeneratePrivate(final KeySpec p0) throws InvalidKeySpecException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: instanceof      Ljava/security/spec/PKCS8EncodedKeySpec;
        //     4: ifeq            82
        //     7: aload_0        
        //     8: aload_1        
        //     9: checkcast       Ljava/security/spec/PKCS8EncodedKeySpec;
        //    12: invokevirtual   java/security/spec/PKCS8EncodedKeySpec.getEncoded:()[B
        //    15: invokestatic    org/bouncycastle/asn1/pkcs/PrivateKeyInfo.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/pkcs/PrivateKeyInfo;
        //    18: invokevirtual   org/bouncycastle/jcajce/provider/asymmetric/rsa/KeyFactorySpi.generatePrivate:(Lorg/bouncycastle/asn1/pkcs/PrivateKeyInfo;)Ljava/security/PrivateKey;
        //    21: astore_2       
        //    22: aload_2        
        //    23: areturn        
        //    24: astore_2       
        //    25: new             Lorg/bouncycastle/jcajce/provider/asymmetric/rsa/BCRSAPrivateCrtKey;
        //    28: dup            
        //    29: aload_1        
        //    30: checkcast       Ljava/security/spec/PKCS8EncodedKeySpec;
        //    33: invokevirtual   java/security/spec/PKCS8EncodedKeySpec.getEncoded:()[B
        //    36: invokestatic    org/bouncycastle/asn1/pkcs/RSAPrivateKey.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/pkcs/RSAPrivateKey;
        //    39: invokespecial   org/bouncycastle/jcajce/provider/asymmetric/rsa/BCRSAPrivateCrtKey.<init>:(Lorg/bouncycastle/asn1/pkcs/RSAPrivateKey;)V
        //    42: astore_1       
        //    43: aload_1        
        //    44: areturn        
        //    45: new             Ljava/lang/StringBuilder;
        //    48: dup            
        //    49: invokespecial   java/lang/StringBuilder.<init>:()V
        //    52: astore_1       
        //    53: aload_1        
        //    54: ldc             "unable to process key spec: "
        //    56: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    59: pop            
        //    60: aload_1        
        //    61: aload_2        
        //    62: invokevirtual   java/lang/Exception.toString:()Ljava/lang/String;
        //    65: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    68: pop            
        //    69: new             Lorg/bouncycastle/jcajce/provider/asymmetric/util/ExtendedInvalidKeySpecException;
        //    72: dup            
        //    73: aload_1        
        //    74: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    77: aload_2        
        //    78: invokespecial   org/bouncycastle/jcajce/provider/asymmetric/util/ExtendedInvalidKeySpecException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //    81: athrow         
        //    82: aload_1        
        //    83: instanceof      Ljava/security/spec/RSAPrivateCrtKeySpec;
        //    86: ifeq            101
        //    89: new             Lorg/bouncycastle/jcajce/provider/asymmetric/rsa/BCRSAPrivateCrtKey;
        //    92: dup            
        //    93: aload_1        
        //    94: checkcast       Ljava/security/spec/RSAPrivateCrtKeySpec;
        //    97: invokespecial   org/bouncycastle/jcajce/provider/asymmetric/rsa/BCRSAPrivateCrtKey.<init>:(Ljava/security/spec/RSAPrivateCrtKeySpec;)V
        //   100: areturn        
        //   101: aload_1        
        //   102: instanceof      Ljava/security/spec/RSAPrivateKeySpec;
        //   105: ifeq            120
        //   108: new             Lorg/bouncycastle/jcajce/provider/asymmetric/rsa/BCRSAPrivateKey;
        //   111: dup            
        //   112: aload_1        
        //   113: checkcast       Ljava/security/spec/RSAPrivateKeySpec;
        //   116: invokespecial   org/bouncycastle/jcajce/provider/asymmetric/rsa/BCRSAPrivateKey.<init>:(Ljava/security/spec/RSAPrivateKeySpec;)V
        //   119: areturn        
        //   120: aload_1        
        //   121: instanceof      Lorg/bouncycastle/jcajce/spec/OpenSSHPrivateKeySpec;
        //   124: ifeq            167
        //   127: aload_1        
        //   128: checkcast       Lorg/bouncycastle/jcajce/spec/OpenSSHPrivateKeySpec;
        //   131: invokevirtual   org/bouncycastle/jcajce/spec/OpenSSHPrivateKeySpec.getEncoded:()[B
        //   134: invokestatic    org/bouncycastle/crypto/util/OpenSSHPrivateKeyUtil.parsePrivateKeyBlob:([B)Lorg/bouncycastle/crypto/params/AsymmetricKeyParameter;
        //   137: astore_1       
        //   138: aload_1        
        //   139: instanceof      Lorg/bouncycastle/crypto/params/RSAPrivateCrtKeyParameters;
        //   142: ifeq            157
        //   145: new             Lorg/bouncycastle/jcajce/provider/asymmetric/rsa/BCRSAPrivateCrtKey;
        //   148: dup            
        //   149: aload_1        
        //   150: checkcast       Lorg/bouncycastle/crypto/params/RSAPrivateCrtKeyParameters;
        //   153: invokespecial   org/bouncycastle/jcajce/provider/asymmetric/rsa/BCRSAPrivateCrtKey.<init>:(Lorg/bouncycastle/crypto/params/RSAPrivateCrtKeyParameters;)V
        //   156: areturn        
        //   157: new             Ljava/security/spec/InvalidKeySpecException;
        //   160: dup            
        //   161: ldc             "open SSH public key is not RSA private key"
        //   163: invokespecial   java/security/spec/InvalidKeySpecException.<init>:(Ljava/lang/String;)V
        //   166: athrow         
        //   167: new             Ljava/lang/StringBuilder;
        //   170: dup            
        //   171: invokespecial   java/lang/StringBuilder.<init>:()V
        //   174: astore_2       
        //   175: aload_2        
        //   176: ldc             "unknown KeySpec type: "
        //   178: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   181: pop            
        //   182: aload_2        
        //   183: aload_1        
        //   184: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //   187: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   190: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   193: pop            
        //   194: new             Ljava/security/spec/InvalidKeySpecException;
        //   197: dup            
        //   198: aload_2        
        //   199: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   202: invokespecial   java/security/spec/InvalidKeySpecException.<init>:(Ljava/lang/String;)V
        //   205: athrow         
        //   206: astore_1       
        //   207: goto            45
        //    Exceptions:
        //  throws java.security.spec.InvalidKeySpecException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  7      22     24     82     Ljava/lang/Exception;
        //  25     43     206    82     Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0045:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    protected PublicKey engineGeneratePublic(final KeySpec keySpec) throws InvalidKeySpecException {
        if (keySpec instanceof RSAPublicKeySpec) {
            return new BCRSAPublicKey((RSAPublicKeySpec)keySpec);
        }
        if (!(keySpec instanceof OpenSSHPublicKeySpec)) {
            return super.engineGeneratePublic(keySpec);
        }
        final AsymmetricKeyParameter publicKey = OpenSSHPublicKeyUtil.parsePublicKey(((OpenSSHPublicKeySpec)keySpec).getEncoded());
        if (publicKey instanceof RSAKeyParameters) {
            return new BCRSAPublicKey((RSAKeyParameters)publicKey);
        }
        throw new InvalidKeySpecException("Open SSH public key is not RSA public key");
    }
    
    @Override
    protected KeySpec engineGetKeySpec(final Key key, final Class clazz) throws InvalidKeySpecException {
        if (clazz.isAssignableFrom(RSAPublicKeySpec.class) && key instanceof RSAPublicKey) {
            final RSAPublicKey rsaPublicKey = (RSAPublicKey)key;
            return new RSAPublicKeySpec(rsaPublicKey.getModulus(), rsaPublicKey.getPublicExponent());
        }
        if (clazz.isAssignableFrom(RSAPrivateKeySpec.class) && key instanceof RSAPrivateKey) {
            final RSAPrivateKey rsaPrivateKey = (RSAPrivateKey)key;
            return new RSAPrivateKeySpec(rsaPrivateKey.getModulus(), rsaPrivateKey.getPrivateExponent());
        }
        if (clazz.isAssignableFrom(RSAPrivateCrtKeySpec.class) && key instanceof RSAPrivateCrtKey) {
            final RSAPrivateCrtKey rsaPrivateCrtKey = (RSAPrivateCrtKey)key;
            return new RSAPrivateCrtKeySpec(rsaPrivateCrtKey.getModulus(), rsaPrivateCrtKey.getPublicExponent(), rsaPrivateCrtKey.getPrivateExponent(), rsaPrivateCrtKey.getPrimeP(), rsaPrivateCrtKey.getPrimeQ(), rsaPrivateCrtKey.getPrimeExponentP(), rsaPrivateCrtKey.getPrimeExponentQ(), rsaPrivateCrtKey.getCrtCoefficient());
        }
        if (clazz.isAssignableFrom(OpenSSHPublicKeySpec.class) && key instanceof RSAPublicKey) {
            try {
                return new OpenSSHPublicKeySpec(OpenSSHPublicKeyUtil.encodePublicKey(new RSAKeyParameters(false, ((RSAPublicKey)key).getModulus(), ((RSAPublicKey)key).getPublicExponent())));
            }
            catch (IOException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("unable to produce encoding: ");
                sb.append(ex.getMessage());
                throw new IllegalArgumentException(sb.toString());
            }
        }
        if (clazz.isAssignableFrom(OpenSSHPrivateKeySpec.class) && key instanceof RSAPrivateCrtKey) {
            try {
                return new OpenSSHPrivateKeySpec(OpenSSHPrivateKeyUtil.encodePrivateKey(new RSAPrivateCrtKeyParameters(((RSAPrivateCrtKey)key).getModulus(), ((RSAPrivateCrtKey)key).getPublicExponent(), ((RSAPrivateCrtKey)key).getPrivateExponent(), ((RSAPrivateCrtKey)key).getPrimeP(), ((RSAPrivateCrtKey)key).getPrimeQ(), ((RSAPrivateCrtKey)key).getPrimeExponentP(), ((RSAPrivateCrtKey)key).getPrimeExponentQ(), ((RSAPrivateCrtKey)key).getCrtCoefficient())));
            }
            catch (IOException ex2) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("unable to produce encoding: ");
                sb2.append(ex2.getMessage());
                throw new IllegalArgumentException(sb2.toString());
            }
        }
        if (clazz.isAssignableFrom(org.bouncycastle.jce.spec.OpenSSHPublicKeySpec.class) && key instanceof RSAPublicKey) {
            try {
                return new org.bouncycastle.jce.spec.OpenSSHPublicKeySpec(OpenSSHPublicKeyUtil.encodePublicKey(new RSAKeyParameters(false, ((RSAPublicKey)key).getModulus(), ((RSAPublicKey)key).getPublicExponent())));
            }
            catch (IOException ex3) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("unable to produce encoding: ");
                sb3.append(ex3.getMessage());
                throw new IllegalArgumentException(sb3.toString());
            }
        }
        if (clazz.isAssignableFrom(org.bouncycastle.jce.spec.OpenSSHPrivateKeySpec.class) && key instanceof RSAPrivateCrtKey) {
            try {
                return new org.bouncycastle.jce.spec.OpenSSHPrivateKeySpec(OpenSSHPrivateKeyUtil.encodePrivateKey(new RSAPrivateCrtKeyParameters(((RSAPrivateCrtKey)key).getModulus(), ((RSAPrivateCrtKey)key).getPublicExponent(), ((RSAPrivateCrtKey)key).getPrivateExponent(), ((RSAPrivateCrtKey)key).getPrimeP(), ((RSAPrivateCrtKey)key).getPrimeQ(), ((RSAPrivateCrtKey)key).getPrimeExponentP(), ((RSAPrivateCrtKey)key).getPrimeExponentQ(), ((RSAPrivateCrtKey)key).getCrtCoefficient())));
            }
            catch (IOException ex4) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("unable to produce encoding: ");
                sb4.append(ex4.getMessage());
                throw new IllegalArgumentException(sb4.toString());
            }
        }
        return super.engineGetKeySpec(key, clazz);
    }
    
    @Override
    protected Key engineTranslateKey(final Key key) throws InvalidKeyException {
        if (key instanceof RSAPublicKey) {
            return new BCRSAPublicKey((RSAPublicKey)key);
        }
        if (key instanceof RSAPrivateCrtKey) {
            return new BCRSAPrivateCrtKey((RSAPrivateCrtKey)key);
        }
        if (key instanceof RSAPrivateKey) {
            return new BCRSAPrivateKey((RSAPrivateKey)key);
        }
        throw new InvalidKeyException("key type unknown");
    }
    
    @Override
    public PrivateKey generatePrivate(final PrivateKeyInfo privateKeyInfo) throws IOException {
        final ASN1ObjectIdentifier algorithm = privateKeyInfo.getPrivateKeyAlgorithm().getAlgorithm();
        if (!RSAUtil.isRsaOid(algorithm)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("algorithm identifier ");
            sb.append(algorithm);
            sb.append(" in key not recognised");
            throw new IOException(sb.toString());
        }
        final org.bouncycastle.asn1.pkcs.RSAPrivateKey instance = org.bouncycastle.asn1.pkcs.RSAPrivateKey.getInstance(privateKeyInfo.parsePrivateKey());
        if (instance.getCoefficient().intValue() == 0) {
            return new BCRSAPrivateKey(instance);
        }
        return new BCRSAPrivateCrtKey(privateKeyInfo);
    }
    
    @Override
    public PublicKey generatePublic(final SubjectPublicKeyInfo subjectPublicKeyInfo) throws IOException {
        final ASN1ObjectIdentifier algorithm = subjectPublicKeyInfo.getAlgorithm().getAlgorithm();
        if (RSAUtil.isRsaOid(algorithm)) {
            return new BCRSAPublicKey(subjectPublicKeyInfo);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("algorithm identifier ");
        sb.append(algorithm);
        sb.append(" in key not recognised");
        throw new IOException(sb.toString());
    }
}
