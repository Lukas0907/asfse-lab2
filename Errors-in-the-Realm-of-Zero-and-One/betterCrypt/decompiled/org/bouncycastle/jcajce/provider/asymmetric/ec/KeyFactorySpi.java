// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.ec;

import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Primitive;
import java.security.InvalidKeyException;
import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import java.security.interfaces.ECPublicKey;
import java.security.Key;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.util.OpenSSHPublicKeyUtil;
import org.bouncycastle.jcajce.spec.OpenSSHPublicKeySpec;
import org.bouncycastle.jce.spec.ECPublicKeySpec;
import java.security.PublicKey;
import java.io.IOException;
import java.security.spec.InvalidKeySpecException;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.asn1.sec.ECPrivateKey;
import org.bouncycastle.jcajce.spec.OpenSSHPrivateKeySpec;
import org.bouncycastle.jce.spec.ECPrivateKeySpec;
import java.security.PrivateKey;
import java.security.spec.KeySpec;
import org.bouncycastle.jcajce.provider.config.ProviderConfiguration;
import org.bouncycastle.jcajce.provider.util.AsymmetricKeyInfoConverter;
import org.bouncycastle.jcajce.provider.asymmetric.util.BaseKeyFactorySpi;

public class KeyFactorySpi extends BaseKeyFactorySpi implements AsymmetricKeyInfoConverter
{
    String algorithm;
    ProviderConfiguration configuration;
    
    KeyFactorySpi(final String algorithm, final ProviderConfiguration configuration) {
        this.algorithm = algorithm;
        this.configuration = configuration;
    }
    
    @Override
    protected PrivateKey engineGeneratePrivate(final KeySpec keySpec) throws InvalidKeySpecException {
        if (keySpec instanceof ECPrivateKeySpec) {
            return new BCECPrivateKey(this.algorithm, (ECPrivateKeySpec)keySpec, this.configuration);
        }
        if (keySpec instanceof java.security.spec.ECPrivateKeySpec) {
            return new BCECPrivateKey(this.algorithm, (java.security.spec.ECPrivateKeySpec)keySpec, this.configuration);
        }
        if (keySpec instanceof OpenSSHPrivateKeySpec) {
            final ECPrivateKey instance = ECPrivateKey.getInstance(((OpenSSHPrivateKeySpec)keySpec).getEncoded());
            try {
                return new BCECPrivateKey(this.algorithm, new PrivateKeyInfo(new AlgorithmIdentifier(X9ObjectIdentifiers.id_ecPublicKey, instance.getParameters()), instance), this.configuration);
            }
            catch (IOException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("bad encoding: ");
                sb.append(ex.getMessage());
                throw new InvalidKeySpecException(sb.toString());
            }
        }
        return super.engineGeneratePrivate(keySpec);
    }
    
    @Override
    protected PublicKey engineGeneratePublic(final KeySpec keySpec) throws InvalidKeySpecException {
        try {
            if (keySpec instanceof ECPublicKeySpec) {
                return new BCECPublicKey(this.algorithm, (ECPublicKeySpec)keySpec, this.configuration);
            }
            if (keySpec instanceof java.security.spec.ECPublicKeySpec) {
                return new BCECPublicKey(this.algorithm, (java.security.spec.ECPublicKeySpec)keySpec, this.configuration);
            }
            if (!(keySpec instanceof OpenSSHPublicKeySpec)) {
                return super.engineGeneratePublic(keySpec);
            }
            final AsymmetricKeyParameter publicKey = OpenSSHPublicKeyUtil.parsePublicKey(((OpenSSHPublicKeySpec)keySpec).getEncoded());
            if (publicKey instanceof ECPublicKeyParameters) {
                final ECDomainParameters parameters = ((ECPublicKeyParameters)publicKey).getParameters();
                return this.engineGeneratePublic(new ECPublicKeySpec(((ECPublicKeyParameters)publicKey).getQ(), new ECParameterSpec(parameters.getCurve(), parameters.getG(), parameters.getN(), parameters.getH(), parameters.getSeed())));
            }
            throw new IllegalArgumentException("openssh key is not ec public key");
        }
        catch (Exception cause) {
            final StringBuilder sb = new StringBuilder();
            sb.append("invalid KeySpec: ");
            sb.append(cause.getMessage());
            throw new InvalidKeySpecException(sb.toString(), cause);
        }
    }
    
    @Override
    protected KeySpec engineGetKeySpec(final Key key, final Class clazz) throws InvalidKeySpecException {
        if (clazz.isAssignableFrom(java.security.spec.ECPublicKeySpec.class) && key instanceof ECPublicKey) {
            final ECPublicKey ecPublicKey = (ECPublicKey)key;
            if (ecPublicKey.getParams() != null) {
                return new java.security.spec.ECPublicKeySpec(ecPublicKey.getW(), ecPublicKey.getParams());
            }
            final ECParameterSpec ecImplicitlyCa = BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa();
            return new java.security.spec.ECPublicKeySpec(ecPublicKey.getW(), EC5Util.convertSpec(EC5Util.convertCurve(ecImplicitlyCa.getCurve(), ecImplicitlyCa.getSeed()), ecImplicitlyCa));
        }
        else if (clazz.isAssignableFrom(java.security.spec.ECPrivateKeySpec.class) && key instanceof java.security.interfaces.ECPrivateKey) {
            final java.security.interfaces.ECPrivateKey ecPrivateKey = (java.security.interfaces.ECPrivateKey)key;
            if (ecPrivateKey.getParams() != null) {
                return new java.security.spec.ECPrivateKeySpec(ecPrivateKey.getS(), ecPrivateKey.getParams());
            }
            final ECParameterSpec ecImplicitlyCa2 = BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa();
            return new java.security.spec.ECPrivateKeySpec(ecPrivateKey.getS(), EC5Util.convertSpec(EC5Util.convertCurve(ecImplicitlyCa2.getCurve(), ecImplicitlyCa2.getSeed()), ecImplicitlyCa2));
        }
        else if (clazz.isAssignableFrom(ECPublicKeySpec.class) && key instanceof ECPublicKey) {
            final ECPublicKey ecPublicKey2 = (ECPublicKey)key;
            if (ecPublicKey2.getParams() != null) {
                return new ECPublicKeySpec(EC5Util.convertPoint(ecPublicKey2.getParams(), ecPublicKey2.getW()), EC5Util.convertSpec(ecPublicKey2.getParams()));
            }
            return new ECPublicKeySpec(EC5Util.convertPoint(ecPublicKey2.getParams(), ecPublicKey2.getW()), BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa());
        }
        else if (clazz.isAssignableFrom(ECPrivateKeySpec.class) && key instanceof java.security.interfaces.ECPrivateKey) {
            final java.security.interfaces.ECPrivateKey ecPrivateKey2 = (java.security.interfaces.ECPrivateKey)key;
            if (ecPrivateKey2.getParams() != null) {
                return new ECPrivateKeySpec(ecPrivateKey2.getS(), EC5Util.convertSpec(ecPrivateKey2.getParams()));
            }
            return new ECPrivateKeySpec(ecPrivateKey2.getS(), BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa());
        }
        else {
            if (clazz.isAssignableFrom(OpenSSHPublicKeySpec.class) && key instanceof ECPublicKey) {
                if (key instanceof BCECPublicKey) {
                    final BCECPublicKey bcecPublicKey = (BCECPublicKey)key;
                    final ECParameterSpec parameters = bcecPublicKey.getParameters();
                    try {
                        return new OpenSSHPublicKeySpec(OpenSSHPublicKeyUtil.encodePublicKey(new ECPublicKeyParameters(bcecPublicKey.getQ(), new ECDomainParameters(parameters.getCurve(), parameters.getG(), parameters.getN(), parameters.getH(), parameters.getSeed()))));
                    }
                    catch (IOException ex) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("unable to produce encoding: ");
                        sb.append(ex.getMessage());
                        throw new IllegalArgumentException(sb.toString());
                    }
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("invalid key type: ");
                sb2.append(key.getClass().getName());
                throw new IllegalArgumentException(sb2.toString());
            }
            if (clazz.isAssignableFrom(OpenSSHPrivateKeySpec.class) && key instanceof java.security.interfaces.ECPrivateKey) {
                if (key instanceof BCECPrivateKey) {
                    try {
                        return new OpenSSHPrivateKeySpec(PrivateKeyInfo.getInstance(key.getEncoded()).parsePrivateKey().toASN1Primitive().getEncoded());
                    }
                    catch (IOException ex2) {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("cannot encoded key: ");
                        sb3.append(ex2.getMessage());
                        throw new IllegalArgumentException(sb3.toString());
                    }
                }
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("invalid key type: ");
                sb4.append(key.getClass().getName());
                throw new IllegalArgumentException(sb4.toString());
            }
            if (clazz.isAssignableFrom(org.bouncycastle.jce.spec.OpenSSHPublicKeySpec.class) && key instanceof ECPublicKey) {
                if (key instanceof BCECPublicKey) {
                    final BCECPublicKey bcecPublicKey2 = (BCECPublicKey)key;
                    final ECParameterSpec parameters2 = bcecPublicKey2.getParameters();
                    try {
                        return new org.bouncycastle.jce.spec.OpenSSHPublicKeySpec(OpenSSHPublicKeyUtil.encodePublicKey(new ECPublicKeyParameters(bcecPublicKey2.getQ(), new ECDomainParameters(parameters2.getCurve(), parameters2.getG(), parameters2.getN(), parameters2.getH(), parameters2.getSeed()))));
                    }
                    catch (IOException ex3) {
                        final StringBuilder sb5 = new StringBuilder();
                        sb5.append("unable to produce encoding: ");
                        sb5.append(ex3.getMessage());
                        throw new IllegalArgumentException(sb5.toString());
                    }
                }
                final StringBuilder sb6 = new StringBuilder();
                sb6.append("invalid key type: ");
                sb6.append(key.getClass().getName());
                throw new IllegalArgumentException(sb6.toString());
            }
            if (clazz.isAssignableFrom(org.bouncycastle.jce.spec.OpenSSHPrivateKeySpec.class) && key instanceof java.security.interfaces.ECPrivateKey) {
                if (key instanceof BCECPrivateKey) {
                    try {
                        return new org.bouncycastle.jce.spec.OpenSSHPrivateKeySpec(PrivateKeyInfo.getInstance(key.getEncoded()).parsePrivateKey().toASN1Primitive().getEncoded());
                    }
                    catch (IOException ex4) {
                        final StringBuilder sb7 = new StringBuilder();
                        sb7.append("cannot encoded key: ");
                        sb7.append(ex4.getMessage());
                        throw new IllegalArgumentException(sb7.toString());
                    }
                }
                final StringBuilder sb8 = new StringBuilder();
                sb8.append("invalid key type: ");
                sb8.append(key.getClass().getName());
                throw new IllegalArgumentException(sb8.toString());
            }
            return super.engineGetKeySpec(key, clazz);
        }
    }
    
    @Override
    protected Key engineTranslateKey(final Key key) throws InvalidKeyException {
        if (key instanceof ECPublicKey) {
            return new BCECPublicKey((ECPublicKey)key, this.configuration);
        }
        if (key instanceof java.security.interfaces.ECPrivateKey) {
            return new BCECPrivateKey((java.security.interfaces.ECPrivateKey)key, this.configuration);
        }
        throw new InvalidKeyException("key type unknown");
    }
    
    @Override
    public PrivateKey generatePrivate(final PrivateKeyInfo privateKeyInfo) throws IOException {
        final ASN1ObjectIdentifier algorithm = privateKeyInfo.getPrivateKeyAlgorithm().getAlgorithm();
        if (algorithm.equals(X9ObjectIdentifiers.id_ecPublicKey)) {
            return new BCECPrivateKey(this.algorithm, privateKeyInfo, this.configuration);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("algorithm identifier ");
        sb.append(algorithm);
        sb.append(" in key not recognised");
        throw new IOException(sb.toString());
    }
    
    @Override
    public PublicKey generatePublic(final SubjectPublicKeyInfo subjectPublicKeyInfo) throws IOException {
        final ASN1ObjectIdentifier algorithm = subjectPublicKeyInfo.getAlgorithm().getAlgorithm();
        if (algorithm.equals(X9ObjectIdentifiers.id_ecPublicKey)) {
            return new BCECPublicKey(this.algorithm, subjectPublicKeyInfo, this.configuration);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("algorithm identifier ");
        sb.append(algorithm);
        sb.append(" in key not recognised");
        throw new IOException(sb.toString());
    }
    
    public static class EC extends KeyFactorySpi
    {
        public EC() {
            super("EC", BouncyCastleProvider.CONFIGURATION);
        }
    }
    
    public static class ECDH extends KeyFactorySpi
    {
        public ECDH() {
            super("ECDH", BouncyCastleProvider.CONFIGURATION);
        }
    }
    
    public static class ECDHC extends KeyFactorySpi
    {
        public ECDHC() {
            super("ECDHC", BouncyCastleProvider.CONFIGURATION);
        }
    }
    
    public static class ECDSA extends KeyFactorySpi
    {
        public ECDSA() {
            super("ECDSA", BouncyCastleProvider.CONFIGURATION);
        }
    }
    
    public static class ECGOST3410 extends KeyFactorySpi
    {
        public ECGOST3410() {
            super("ECGOST3410", BouncyCastleProvider.CONFIGURATION);
        }
    }
    
    public static class ECGOST3410_2012 extends KeyFactorySpi
    {
        public ECGOST3410_2012() {
            super("ECGOST3410-2012", BouncyCastleProvider.CONFIGURATION);
        }
    }
    
    public static class ECMQV extends KeyFactorySpi
    {
        public ECMQV() {
            super("ECMQV", BouncyCastleProvider.CONFIGURATION);
        }
    }
}
