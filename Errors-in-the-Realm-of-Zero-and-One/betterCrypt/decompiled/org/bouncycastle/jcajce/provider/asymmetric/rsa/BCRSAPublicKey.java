// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.rsa;

import org.bouncycastle.util.Strings;
import org.bouncycastle.jcajce.provider.asymmetric.util.KeyUtil;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import java.security.spec.RSAPublicKeySpec;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import java.math.BigInteger;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import java.security.interfaces.RSAPublicKey;

public class BCRSAPublicKey implements RSAPublicKey
{
    private static final AlgorithmIdentifier DEFAULT_ALGORITHM_IDENTIFIER;
    static final long serialVersionUID = 2675817738516720772L;
    private transient AlgorithmIdentifier algorithmIdentifier;
    private BigInteger modulus;
    private BigInteger publicExponent;
    private transient RSAKeyParameters rsaPublicKey;
    
    static {
        DEFAULT_ALGORITHM_IDENTIFIER = new AlgorithmIdentifier(PKCSObjectIdentifiers.rsaEncryption, DERNull.INSTANCE);
    }
    
    BCRSAPublicKey(final RSAPublicKey rsaPublicKey) {
        this.algorithmIdentifier = BCRSAPublicKey.DEFAULT_ALGORITHM_IDENTIFIER;
        this.modulus = rsaPublicKey.getModulus();
        this.publicExponent = rsaPublicKey.getPublicExponent();
        this.rsaPublicKey = new RSAKeyParameters(false, this.modulus, this.publicExponent);
    }
    
    BCRSAPublicKey(final RSAPublicKeySpec rsaPublicKeySpec) {
        this.algorithmIdentifier = BCRSAPublicKey.DEFAULT_ALGORITHM_IDENTIFIER;
        this.modulus = rsaPublicKeySpec.getModulus();
        this.publicExponent = rsaPublicKeySpec.getPublicExponent();
        this.rsaPublicKey = new RSAKeyParameters(false, this.modulus, this.publicExponent);
    }
    
    BCRSAPublicKey(final SubjectPublicKeyInfo subjectPublicKeyInfo) {
        this.populateFromPublicKeyInfo(subjectPublicKeyInfo);
    }
    
    BCRSAPublicKey(final RSAKeyParameters rsaPublicKey) {
        this.algorithmIdentifier = BCRSAPublicKey.DEFAULT_ALGORITHM_IDENTIFIER;
        this.modulus = rsaPublicKey.getModulus();
        this.publicExponent = rsaPublicKey.getExponent();
        this.rsaPublicKey = rsaPublicKey;
    }
    
    private void populateFromPublicKeyInfo(final SubjectPublicKeyInfo subjectPublicKeyInfo) {
        try {
            final org.bouncycastle.asn1.pkcs.RSAPublicKey instance = org.bouncycastle.asn1.pkcs.RSAPublicKey.getInstance(subjectPublicKeyInfo.parsePublicKey());
            this.algorithmIdentifier = subjectPublicKeyInfo.getAlgorithm();
            this.modulus = instance.getModulus();
            this.publicExponent = instance.getPublicExponent();
            this.rsaPublicKey = new RSAKeyParameters(false, this.modulus, this.publicExponent);
        }
        catch (IOException ex) {
            throw new IllegalArgumentException("invalid info structure in RSA public key");
        }
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        while (true) {
            try {
                this.algorithmIdentifier = AlgorithmIdentifier.getInstance(objectInputStream.readObject());
                while (true) {
                    this.rsaPublicKey = new RSAKeyParameters(false, this.modulus, this.publicExponent);
                    return;
                    this.algorithmIdentifier = BCRSAPublicKey.DEFAULT_ALGORITHM_IDENTIFIER;
                    continue;
                }
            }
            catch (Exception ex) {}
            continue;
        }
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        if (!this.algorithmIdentifier.equals(BCRSAPublicKey.DEFAULT_ALGORITHM_IDENTIFIER)) {
            objectOutputStream.writeObject(this.algorithmIdentifier.getEncoded());
        }
    }
    
    RSAKeyParameters engineGetKeyParameters() {
        return this.rsaPublicKey;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof RSAPublicKey)) {
            return false;
        }
        final RSAPublicKey rsaPublicKey = (RSAPublicKey)o;
        return this.getModulus().equals(rsaPublicKey.getModulus()) && this.getPublicExponent().equals(rsaPublicKey.getPublicExponent());
    }
    
    @Override
    public String getAlgorithm() {
        return "RSA";
    }
    
    @Override
    public byte[] getEncoded() {
        return KeyUtil.getEncodedSubjectPublicKeyInfo(this.algorithmIdentifier, new org.bouncycastle.asn1.pkcs.RSAPublicKey(this.getModulus(), this.getPublicExponent()));
    }
    
    @Override
    public String getFormat() {
        return "X.509";
    }
    
    @Override
    public BigInteger getModulus() {
        return this.modulus;
    }
    
    @Override
    public BigInteger getPublicExponent() {
        return this.publicExponent;
    }
    
    @Override
    public int hashCode() {
        return this.getModulus().hashCode() ^ this.getPublicExponent().hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        final String lineSeparator = Strings.lineSeparator();
        sb.append("RSA Public Key [");
        sb.append(RSAUtil.generateKeyFingerprint(this.getModulus()));
        sb.append("]");
        sb.append(",[");
        sb.append(RSAUtil.generateExponentFingerprint(this.getPublicExponent()));
        sb.append("]");
        sb.append(lineSeparator);
        sb.append("        modulus: ");
        sb.append(this.getModulus().toString(16));
        sb.append(lineSeparator);
        sb.append("public exponent: ");
        sb.append(this.getPublicExponent().toString(16));
        sb.append(lineSeparator);
        return sb.toString();
    }
}
