// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.edec;

import org.bouncycastle.crypto.generators.KDF2BytesGenerator;
import org.bouncycastle.crypto.agreement.kdf.ConcatenationKDFGenerator;
import org.bouncycastle.crypto.util.DigestFactory;
import org.bouncycastle.jcajce.spec.UserKeyingMaterialSpec;
import org.bouncycastle.crypto.params.XDHUPrivateParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.spec.AlgorithmParameterSpec;
import org.bouncycastle.crypto.params.X448PrivateKeyParameters;
import java.security.SecureRandom;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.XDHUPublicParameters;
import java.security.Key;
import org.bouncycastle.crypto.agreement.X25519Agreement;
import org.bouncycastle.crypto.agreement.XDHUnifiedAgreement;
import org.bouncycastle.crypto.agreement.X448Agreement;
import java.security.InvalidKeyException;
import org.bouncycastle.crypto.DerivationFunction;
import org.bouncycastle.jcajce.spec.DHUParameterSpec;
import org.bouncycastle.crypto.RawAgreement;
import org.bouncycastle.jcajce.provider.asymmetric.util.BaseAgreementSpi;

public class KeyAgreementSpi extends BaseAgreementSpi
{
    private RawAgreement agreement;
    private DHUParameterSpec dhuSpec;
    private byte[] result;
    
    KeyAgreementSpi(final String s) {
        super(s, null);
    }
    
    KeyAgreementSpi(final String s, final DerivationFunction derivationFunction) {
        super(s, derivationFunction);
    }
    
    private RawAgreement getAgreement(final String prefix) throws InvalidKeyException {
        if (!this.kaAlgorithm.equals("XDH") && !this.kaAlgorithm.startsWith(prefix)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("inappropriate key for ");
            sb.append(this.kaAlgorithm);
            throw new InvalidKeyException(sb.toString());
        }
        final int index = this.kaAlgorithm.indexOf(85);
        final boolean startsWith = prefix.startsWith("X448");
        if (index > 0) {
            if (startsWith) {
                return new XDHUnifiedAgreement(new X448Agreement());
            }
            return new XDHUnifiedAgreement(new X25519Agreement());
        }
        else {
            if (startsWith) {
                return new X448Agreement();
            }
            return new X25519Agreement();
        }
    }
    
    @Override
    protected byte[] calcSecret() {
        return this.result;
    }
    
    @Override
    protected Key engineDoPhase(final Key key, final boolean b) throws InvalidKeyException, IllegalStateException {
        if (this.agreement == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.kaAlgorithm);
            sb.append(" not initialised.");
            throw new IllegalStateException(sb.toString());
        }
        if (!b) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(this.kaAlgorithm);
            sb2.append(" can only be between two parties.");
            throw new IllegalStateException(sb2.toString());
        }
        if (key instanceof BCXDHPublicKey) {
            final AsymmetricKeyParameter engineGetKeyParameters = ((BCXDHPublicKey)key).engineGetKeyParameters();
            this.result = new byte[this.agreement.getAgreementSize()];
            final DHUParameterSpec dhuSpec = this.dhuSpec;
            if (dhuSpec != null) {
                this.agreement.calculateAgreement(new XDHUPublicParameters(engineGetKeyParameters, ((BCXDHPublicKey)dhuSpec.getOtherPartyEphemeralKey()).engineGetKeyParameters()), this.result, 0);
            }
            else {
                this.agreement.calculateAgreement(engineGetKeyParameters, this.result, 0);
            }
            return null;
        }
        throw new InvalidKeyException("cannot identify XDH private key");
    }
    
    @Override
    protected void engineInit(final Key key, final SecureRandom secureRandom) throws InvalidKeyException {
        if (!(key instanceof BCXDHPrivateKey)) {
            throw new InvalidKeyException("cannot identify XDH private key");
        }
        final AsymmetricKeyParameter engineGetKeyParameters = ((BCXDHPrivateKey)key).engineGetKeyParameters();
        String s;
        if (engineGetKeyParameters instanceof X448PrivateKeyParameters) {
            s = "X448";
        }
        else {
            s = "X25519";
        }
        (this.agreement = this.getAgreement(s)).init(engineGetKeyParameters);
        if (this.kdf != null) {
            this.ukmParameters = new byte[0];
            return;
        }
        this.ukmParameters = null;
    }
    
    @Override
    protected void engineInit(final Key key, final AlgorithmParameterSpec algorithmParameterSpec, final SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        if (key instanceof BCXDHPrivateKey) {
            final AsymmetricKeyParameter engineGetKeyParameters = ((BCXDHPrivateKey)key).engineGetKeyParameters();
            String s;
            if (engineGetKeyParameters instanceof X448PrivateKeyParameters) {
                s = "X448";
            }
            else {
                s = "X25519";
            }
            this.agreement = this.getAgreement(s);
            this.ukmParameters = null;
            if (algorithmParameterSpec instanceof DHUParameterSpec) {
                if (this.kaAlgorithm.indexOf(85) < 0) {
                    throw new InvalidAlgorithmParameterException("agreement algorithm not DHU based");
                }
                this.dhuSpec = (DHUParameterSpec)algorithmParameterSpec;
                this.ukmParameters = this.dhuSpec.getUserKeyingMaterial();
                this.agreement.init(new XDHUPrivateParameters(engineGetKeyParameters, ((BCXDHPrivateKey)this.dhuSpec.getEphemeralPrivateKey()).engineGetKeyParameters(), ((BCXDHPublicKey)this.dhuSpec.getEphemeralPublicKey()).engineGetKeyParameters()));
            }
            else {
                this.agreement.init(engineGetKeyParameters);
                if (!(algorithmParameterSpec instanceof UserKeyingMaterialSpec)) {
                    throw new InvalidAlgorithmParameterException("unknown ParameterSpec");
                }
                if (this.kdf == null) {
                    throw new InvalidAlgorithmParameterException("no KDF specified for UserKeyingMaterialSpec");
                }
                this.ukmParameters = ((UserKeyingMaterialSpec)algorithmParameterSpec).getUserKeyingMaterial();
            }
            if (this.kdf != null && this.ukmParameters == null) {
                this.ukmParameters = new byte[0];
            }
            return;
        }
        throw new InvalidKeyException("cannot identify XDH private key");
    }
    
    public static final class X25519 extends KeyAgreementSpi
    {
        public X25519() {
            super("X25519");
        }
    }
    
    public static class X25519UwithSHA256CKDF extends KeyAgreementSpi
    {
        public X25519UwithSHA256CKDF() {
            super("X25519UwithSHA256CKDF", new ConcatenationKDFGenerator(DigestFactory.createSHA256()));
        }
    }
    
    public static class X25519UwithSHA256KDF extends KeyAgreementSpi
    {
        public X25519UwithSHA256KDF() {
            super("X25519UwithSHA256KDF", new KDF2BytesGenerator(DigestFactory.createSHA256()));
        }
    }
    
    public static final class X25519withSHA256CKDF extends KeyAgreementSpi
    {
        public X25519withSHA256CKDF() {
            super("X25519withSHA256CKDF", new ConcatenationKDFGenerator(DigestFactory.createSHA256()));
        }
    }
    
    public static final class X25519withSHA256KDF extends KeyAgreementSpi
    {
        public X25519withSHA256KDF() {
            super("X25519withSHA256KDF", new KDF2BytesGenerator(DigestFactory.createSHA256()));
        }
    }
    
    public static class X25519withSHA384CKDF extends KeyAgreementSpi
    {
        public X25519withSHA384CKDF() {
            super("X25519withSHA384CKDF", new ConcatenationKDFGenerator(DigestFactory.createSHA384()));
        }
    }
    
    public static class X25519withSHA512CKDF extends KeyAgreementSpi
    {
        public X25519withSHA512CKDF() {
            super("X25519withSHA512CKDF", new ConcatenationKDFGenerator(DigestFactory.createSHA512()));
        }
    }
    
    public static final class X448 extends KeyAgreementSpi
    {
        public X448() {
            super("X448");
        }
    }
    
    public static class X448UwithSHA512CKDF extends KeyAgreementSpi
    {
        public X448UwithSHA512CKDF() {
            super("X448UwithSHA512CKDF", new ConcatenationKDFGenerator(DigestFactory.createSHA512()));
        }
    }
    
    public static class X448UwithSHA512KDF extends KeyAgreementSpi
    {
        public X448UwithSHA512KDF() {
            super("X448UwithSHA512KDF", new KDF2BytesGenerator(DigestFactory.createSHA512()));
        }
    }
    
    public static final class X448withSHA256CKDF extends KeyAgreementSpi
    {
        public X448withSHA256CKDF() {
            super("X448withSHA256CKDF", new ConcatenationKDFGenerator(DigestFactory.createSHA256()));
        }
    }
    
    public static class X448withSHA384CKDF extends KeyAgreementSpi
    {
        public X448withSHA384CKDF() {
            super("X448withSHA384CKDF", new ConcatenationKDFGenerator(DigestFactory.createSHA384()));
        }
    }
    
    public static final class X448withSHA512CKDF extends KeyAgreementSpi
    {
        public X448withSHA512CKDF() {
            super("X448withSHA512CKDF", new ConcatenationKDFGenerator(DigestFactory.createSHA512()));
        }
    }
    
    public static final class X448withSHA512KDF extends KeyAgreementSpi
    {
        public X448withSHA512KDF() {
            super("X448withSHA512KDF", new KDF2BytesGenerator(DigestFactory.createSHA512()));
        }
    }
    
    public static final class XDH extends KeyAgreementSpi
    {
        public XDH() {
            super("XDH");
        }
    }
}
