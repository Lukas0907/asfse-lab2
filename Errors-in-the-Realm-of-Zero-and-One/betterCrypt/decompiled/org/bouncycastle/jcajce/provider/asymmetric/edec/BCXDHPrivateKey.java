// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.edec;

import org.bouncycastle.crypto.util.PrivateKeyInfoFactory;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.util.Arrays;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.crypto.params.X25519PrivateKeyParameters;
import org.bouncycastle.crypto.params.X448PrivateKeyParameters;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.edec.EdECObjectIdentifiers;
import java.io.IOException;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import java.security.PrivateKey;
import org.bouncycastle.jcajce.interfaces.XDHKey;

public class BCXDHPrivateKey implements XDHKey, PrivateKey
{
    static final long serialVersionUID = 1L;
    private final byte[] attributes;
    private final boolean hasPublicKey;
    private transient AsymmetricKeyParameter xdhPrivateKey;
    
    BCXDHPrivateKey(final PrivateKeyInfo privateKeyInfo) throws IOException {
        this.hasPublicKey = privateKeyInfo.hasPublicKey();
        byte[] encoded;
        if (privateKeyInfo.getAttributes() != null) {
            encoded = privateKeyInfo.getAttributes().getEncoded();
        }
        else {
            encoded = null;
        }
        this.attributes = encoded;
        this.populateFromPrivateKeyInfo(privateKeyInfo);
    }
    
    BCXDHPrivateKey(final AsymmetricKeyParameter xdhPrivateKey) {
        this.hasPublicKey = true;
        this.attributes = null;
        this.xdhPrivateKey = xdhPrivateKey;
    }
    
    private void populateFromPrivateKeyInfo(final PrivateKeyInfo privateKeyInfo) throws IOException {
        final ASN1Encodable privateKey = privateKeyInfo.parsePrivateKey();
        AsymmetricKeyParameter xdhPrivateKey;
        if (EdECObjectIdentifiers.id_X448.equals(privateKeyInfo.getPrivateKeyAlgorithm().getAlgorithm())) {
            xdhPrivateKey = new X448PrivateKeyParameters(ASN1OctetString.getInstance(privateKey).getOctets(), 0);
        }
        else {
            xdhPrivateKey = new X25519PrivateKeyParameters(ASN1OctetString.getInstance(privateKey).getOctets(), 0);
        }
        this.xdhPrivateKey = xdhPrivateKey;
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.populateFromPrivateKeyInfo(PrivateKeyInfo.getInstance(objectInputStream.readObject()));
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.getEncoded());
    }
    
    AsymmetricKeyParameter engineGetKeyParameters() {
        return this.xdhPrivateKey;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof BCXDHPrivateKey && Arrays.areEqual(((BCXDHPrivateKey)o).getEncoded(), this.getEncoded()));
    }
    
    @Override
    public String getAlgorithm() {
        if (this.xdhPrivateKey instanceof X448PrivateKeyParameters) {
            return "X448";
        }
        return "X25519";
    }
    
    @Override
    public byte[] getEncoded() {
        try {
            final ASN1Set instance = ASN1Set.getInstance(this.attributes);
            final PrivateKeyInfo privateKeyInfo = PrivateKeyInfoFactory.createPrivateKeyInfo(this.xdhPrivateKey, instance);
            if (this.hasPublicKey) {
                return privateKeyInfo.getEncoded();
            }
            return new PrivateKeyInfo(privateKeyInfo.getPrivateKeyAlgorithm(), privateKeyInfo.parsePrivateKey(), instance).getEncoded();
        }
        catch (IOException ex) {
            return null;
        }
    }
    
    @Override
    public String getFormat() {
        return "PKCS#8";
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.getEncoded());
    }
    
    @Override
    public String toString() {
        final AsymmetricKeyParameter xdhPrivateKey = this.xdhPrivateKey;
        AsymmetricKeyParameter asymmetricKeyParameter;
        if (xdhPrivateKey instanceof X448PrivateKeyParameters) {
            asymmetricKeyParameter = ((X448PrivateKeyParameters)xdhPrivateKey).generatePublicKey();
        }
        else {
            asymmetricKeyParameter = ((X25519PrivateKeyParameters)xdhPrivateKey).generatePublicKey();
        }
        return Utils.keyToString("Private Key", this.getAlgorithm(), asymmetricKeyParameter);
    }
}
