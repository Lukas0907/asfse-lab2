// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.edec;

import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.edec.EdECObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import java.security.InvalidKeyException;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DEROctetString;
import java.security.Key;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import org.bouncycastle.crypto.util.OpenSSHPublicKeyUtil;
import org.bouncycastle.jcajce.spec.OpenSSHPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.util.OpenSSHPrivateKeyUtil;
import org.bouncycastle.jcajce.spec.OpenSSHPrivateKeySpec;
import java.security.PrivateKey;
import java.security.spec.KeySpec;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.jcajce.provider.util.AsymmetricKeyInfoConverter;
import org.bouncycastle.jcajce.provider.asymmetric.util.BaseKeyFactorySpi;

public class KeyFactorySpi extends BaseKeyFactorySpi implements AsymmetricKeyInfoConverter
{
    static final byte[] Ed25519Prefix;
    private static final byte Ed25519_type = 112;
    static final byte[] Ed448Prefix;
    private static final byte Ed448_type = 113;
    static final byte[] x25519Prefix;
    private static final byte x25519_type = 110;
    static final byte[] x448Prefix;
    private static final byte x448_type = 111;
    String algorithm;
    private final boolean isXdh;
    private final int specificBase;
    
    static {
        x448Prefix = Hex.decode("3042300506032b656f033900");
        x25519Prefix = Hex.decode("302a300506032b656e032100");
        Ed448Prefix = Hex.decode("3043300506032b6571033a00");
        Ed25519Prefix = Hex.decode("302a300506032b6570032100");
    }
    
    public KeyFactorySpi(final String algorithm, final boolean isXdh, final int specificBase) {
        this.algorithm = algorithm;
        this.isXdh = isXdh;
        this.specificBase = specificBase;
    }
    
    @Override
    protected PrivateKey engineGeneratePrivate(final KeySpec keySpec) throws InvalidKeySpecException {
        if (!(keySpec instanceof OpenSSHPrivateKeySpec)) {
            return super.engineGeneratePrivate(keySpec);
        }
        final AsymmetricKeyParameter privateKeyBlob = OpenSSHPrivateKeyUtil.parsePrivateKeyBlob(((OpenSSHPrivateKeySpec)keySpec).getEncoded());
        if (privateKeyBlob instanceof Ed25519PrivateKeyParameters) {
            return new BCEdDSAPrivateKey(privateKeyBlob);
        }
        throw new IllegalStateException("openssh private key not Ed25519 private key");
    }
    
    @Override
    protected PublicKey engineGeneratePublic(final KeySpec keySpec) throws InvalidKeySpecException {
        if (keySpec instanceof X509EncodedKeySpec) {
            final byte[] encoded = ((X509EncodedKeySpec)keySpec).getEncoded();
            final int specificBase = this.specificBase;
            if (specificBase == 0 || specificBase == encoded[8]) {
                switch (encoded[8]) {
                    default: {
                        return super.engineGeneratePublic(keySpec);
                    }
                    case 113: {
                        return new BCEdDSAPublicKey(KeyFactorySpi.Ed448Prefix, encoded);
                    }
                    case 112: {
                        return new BCEdDSAPublicKey(KeyFactorySpi.Ed25519Prefix, encoded);
                    }
                    case 111: {
                        return new BCXDHPublicKey(KeyFactorySpi.x448Prefix, encoded);
                    }
                    case 110: {
                        return new BCXDHPublicKey(KeyFactorySpi.x25519Prefix, encoded);
                    }
                }
            }
        }
        else if (keySpec instanceof OpenSSHPublicKeySpec) {
            final AsymmetricKeyParameter publicKey = OpenSSHPublicKeyUtil.parsePublicKey(((OpenSSHPublicKeySpec)keySpec).getEncoded());
            if (publicKey instanceof Ed25519PublicKeyParameters) {
                return new BCEdDSAPublicKey(new byte[0], ((Ed25519PublicKeyParameters)publicKey).getEncoded());
            }
            throw new IllegalStateException("openssh public key not Ed25519 public key");
        }
        return super.engineGeneratePublic(keySpec);
    }
    
    @Override
    protected KeySpec engineGetKeySpec(final Key key, final Class clazz) throws InvalidKeySpecException {
        if (clazz.isAssignableFrom(OpenSSHPrivateKeySpec.class) && key instanceof BCEdDSAPrivateKey) {
            try {
                return new OpenSSHPrivateKeySpec(OpenSSHPrivateKeyUtil.encodePrivateKey(new Ed25519PrivateKeyParameters(ASN1OctetString.getInstance(new ASN1InputStream(((DEROctetString)ASN1Sequence.getInstance(key.getEncoded()).getObjectAt(2)).getOctets()).readObject()).getOctets(), 0)));
            }
            catch (IOException ex) {
                throw new InvalidKeySpecException(ex.getMessage(), ex.getCause());
            }
        }
        if (clazz.isAssignableFrom(OpenSSHPublicKeySpec.class) && key instanceof BCEdDSAPublicKey) {
            try {
                return new OpenSSHPublicKeySpec(OpenSSHPublicKeyUtil.encodePublicKey(new Ed25519PublicKeyParameters(key.getEncoded(), KeyFactorySpi.Ed25519Prefix.length)));
            }
            catch (IOException ex2) {
                throw new InvalidKeySpecException(ex2.getMessage(), ex2.getCause());
            }
        }
        if (clazz.isAssignableFrom(org.bouncycastle.jce.spec.OpenSSHPrivateKeySpec.class) && key instanceof BCEdDSAPrivateKey) {
            try {
                return new org.bouncycastle.jce.spec.OpenSSHPrivateKeySpec(OpenSSHPrivateKeyUtil.encodePrivateKey(new Ed25519PrivateKeyParameters(ASN1OctetString.getInstance(new ASN1InputStream(((DEROctetString)ASN1Sequence.getInstance(key.getEncoded()).getObjectAt(2)).getOctets()).readObject()).getOctets(), 0)));
            }
            catch (IOException ex3) {
                throw new InvalidKeySpecException(ex3.getMessage(), ex3.getCause());
            }
        }
        if (clazz.isAssignableFrom(org.bouncycastle.jce.spec.OpenSSHPublicKeySpec.class) && key instanceof BCEdDSAPublicKey) {
            try {
                return new org.bouncycastle.jce.spec.OpenSSHPublicKeySpec(OpenSSHPublicKeyUtil.encodePublicKey(new Ed25519PublicKeyParameters(key.getEncoded(), KeyFactorySpi.Ed25519Prefix.length)));
            }
            catch (IOException ex4) {
                throw new InvalidKeySpecException(ex4.getMessage(), ex4.getCause());
            }
        }
        return super.engineGetKeySpec(key, clazz);
    }
    
    @Override
    protected Key engineTranslateKey(final Key key) throws InvalidKeyException {
        throw new InvalidKeyException("key type unknown");
    }
    
    @Override
    public PrivateKey generatePrivate(final PrivateKeyInfo privateKeyInfo) throws IOException {
        final ASN1ObjectIdentifier algorithm = privateKeyInfo.getPrivateKeyAlgorithm().getAlgorithm();
        if (this.isXdh) {
            final int specificBase = this.specificBase;
            if ((specificBase == 0 || specificBase == 111) && algorithm.equals(EdECObjectIdentifiers.id_X448)) {
                return new BCXDHPrivateKey(privateKeyInfo);
            }
            final int specificBase2 = this.specificBase;
            if ((specificBase2 == 0 || specificBase2 == 110) && algorithm.equals(EdECObjectIdentifiers.id_X25519)) {
                return new BCXDHPrivateKey(privateKeyInfo);
            }
        }
        else if (algorithm.equals(EdECObjectIdentifiers.id_Ed448) || algorithm.equals(EdECObjectIdentifiers.id_Ed25519)) {
            final int specificBase3 = this.specificBase;
            if ((specificBase3 == 0 || specificBase3 == 113) && algorithm.equals(EdECObjectIdentifiers.id_Ed448)) {
                return new BCEdDSAPrivateKey(privateKeyInfo);
            }
            final int specificBase4 = this.specificBase;
            if ((specificBase4 == 0 || specificBase4 == 112) && algorithm.equals(EdECObjectIdentifiers.id_Ed25519)) {
                return new BCEdDSAPrivateKey(privateKeyInfo);
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("algorithm identifier ");
        sb.append(algorithm);
        sb.append(" in key not recognized");
        throw new IOException(sb.toString());
    }
    
    @Override
    public PublicKey generatePublic(final SubjectPublicKeyInfo subjectPublicKeyInfo) throws IOException {
        final ASN1ObjectIdentifier algorithm = subjectPublicKeyInfo.getAlgorithm().getAlgorithm();
        if (this.isXdh) {
            final int specificBase = this.specificBase;
            if ((specificBase == 0 || specificBase == 111) && algorithm.equals(EdECObjectIdentifiers.id_X448)) {
                return new BCXDHPublicKey(subjectPublicKeyInfo);
            }
            final int specificBase2 = this.specificBase;
            if ((specificBase2 == 0 || specificBase2 == 110) && algorithm.equals(EdECObjectIdentifiers.id_X25519)) {
                return new BCXDHPublicKey(subjectPublicKeyInfo);
            }
        }
        else if (algorithm.equals(EdECObjectIdentifiers.id_Ed448) || algorithm.equals(EdECObjectIdentifiers.id_Ed25519)) {
            final int specificBase3 = this.specificBase;
            if ((specificBase3 == 0 || specificBase3 == 113) && algorithm.equals(EdECObjectIdentifiers.id_Ed448)) {
                return new BCEdDSAPublicKey(subjectPublicKeyInfo);
            }
            final int specificBase4 = this.specificBase;
            if ((specificBase4 == 0 || specificBase4 == 112) && algorithm.equals(EdECObjectIdentifiers.id_Ed25519)) {
                return new BCEdDSAPublicKey(subjectPublicKeyInfo);
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("algorithm identifier ");
        sb.append(algorithm);
        sb.append(" in key not recognized");
        throw new IOException(sb.toString());
    }
    
    public static class ED25519 extends KeyFactorySpi
    {
        public ED25519() {
            super("Ed25519", false, 112);
        }
    }
    
    public static class ED448 extends KeyFactorySpi
    {
        public ED448() {
            super("Ed448", false, 113);
        }
    }
    
    public static class EDDSA extends KeyFactorySpi
    {
        public EDDSA() {
            super("EdDSA", false, 0);
        }
    }
    
    public static class X25519 extends KeyFactorySpi
    {
        public X25519() {
            super("X25519", true, 110);
        }
    }
    
    public static class X448 extends KeyFactorySpi
    {
        public X448() {
            super("X448", true, 111);
        }
    }
    
    public static class XDH extends KeyFactorySpi
    {
        public XDH() {
            super("XDH", true, 0);
        }
    }
}
