// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.x509;

import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.asn1.ASN1Sequence;
import java.io.IOException;
import java.io.InputStream;

class PEMUtil
{
    private final Boundaries[] _supportedBoundaries;
    
    PEMUtil(final String str) {
        final Boundaries boundaries = new Boundaries(str);
        final StringBuilder sb = new StringBuilder();
        sb.append("X509 ");
        sb.append(str);
        this._supportedBoundaries = new Boundaries[] { boundaries, new Boundaries(sb.toString()), new Boundaries("PKCS7") };
    }
    
    private Boundaries getBoundaries(final String s) {
        int n = 0;
        while (true) {
            final Boundaries[] supportedBoundaries = this._supportedBoundaries;
            if (n == supportedBoundaries.length) {
                return null;
            }
            final Boundaries boundaries = supportedBoundaries[n];
            if (boundaries.isTheExpectedHeader(s)) {
                return boundaries;
            }
            if (boundaries.isTheExpectedFooter(s)) {
                return boundaries;
            }
            ++n;
        }
    }
    
    private String readLine(final InputStream inputStream) throws IOException {
        final StringBuffer sb = new StringBuffer();
        int read;
        while (true) {
            read = inputStream.read();
            if (read != 13 && read != 10 && read >= 0) {
                sb.append((char)read);
            }
            else {
                if (read < 0 || sb.length() != 0) {
                    break;
                }
                continue;
            }
        }
        if (read >= 0) {
            if (read == 13) {
                inputStream.mark(1);
                final int read2 = inputStream.read();
                if (read2 == 10) {
                    inputStream.mark(1);
                }
                if (read2 > 0) {
                    inputStream.reset();
                }
            }
            return sb.toString();
        }
        if (sb.length() == 0) {
            return null;
        }
        return sb.toString();
    }
    
    ASN1Sequence readPEMObject(final InputStream inputStream) throws IOException {
        final StringBuffer sb = new StringBuffer();
        Boundaries boundaries = null;
        while (boundaries == null) {
            final String line = this.readLine(inputStream);
            if (line == null) {
                break;
            }
            final Boundaries boundaries2 = this.getBoundaries(line);
            if ((boundaries = boundaries2) == null) {
                continue;
            }
            if (!boundaries2.isTheExpectedHeader(line)) {
                throw new IOException("malformed PEM data: found footer where header was expected");
            }
            boundaries = boundaries2;
        }
        Label_0178: {
            if (boundaries == null) {
                break Label_0178;
            }
            Boundaries boundaries3 = null;
            while (boundaries3 == null) {
                final String line2 = this.readLine(inputStream);
                if (line2 == null) {
                    break;
                }
                boundaries3 = this.getBoundaries(line2);
                if (boundaries3 != null) {
                    if (boundaries.isTheExpectedFooter(line2)) {
                        continue;
                    }
                    throw new IOException("malformed PEM data: header/footer mismatch");
                }
                else {
                    sb.append(line2);
                }
            }
            Label_0168: {
                if (boundaries3 == null) {
                    break Label_0168;
                }
                Label_0166: {
                    if (sb.length() == 0) {
                        break Label_0166;
                    }
                    while (true) {
                        while (true) {
                            try {
                                return ASN1Sequence.getInstance(Base64.decode(sb.toString()));
                                return null;
                                throw new IOException("malformed PEM data: no footer found");
                                throw new IOException("malformed PEM data: no header found");
                                throw new IOException("malformed PEM data encountered");
                            }
                            catch (Exception ex) {}
                            continue;
                        }
                    }
                }
            }
        }
    }
    
    private class Boundaries
    {
        private final String _footer;
        private final String _header;
        
        private Boundaries(final String s) {
            final StringBuilder sb = new StringBuilder();
            sb.append("-----BEGIN ");
            sb.append(s);
            sb.append("-----");
            this._header = sb.toString();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("-----END ");
            sb2.append(s);
            sb2.append("-----");
            this._footer = sb2.toString();
        }
        
        public boolean isTheExpectedFooter(final String s) {
            return s.startsWith(this._footer);
        }
        
        public boolean isTheExpectedHeader(final String s) {
            return s.startsWith(this._header);
        }
    }
}
