// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.edec;

import org.bouncycastle.util.Arrays;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.edec.EdECObjectIdentifiers;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import java.security.spec.InvalidKeySpecException;
import org.bouncycastle.crypto.params.Ed448PublicKeyParameters;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import java.security.PublicKey;
import org.bouncycastle.jcajce.interfaces.EdDSAKey;

public class BCEdDSAPublicKey implements EdDSAKey, PublicKey
{
    static final long serialVersionUID = 1L;
    private transient AsymmetricKeyParameter eddsaPublicKey;
    
    BCEdDSAPublicKey(final SubjectPublicKeyInfo subjectPublicKeyInfo) {
        this.populateFromPubKeyInfo(subjectPublicKeyInfo);
    }
    
    BCEdDSAPublicKey(final AsymmetricKeyParameter eddsaPublicKey) {
        this.eddsaPublicKey = eddsaPublicKey;
    }
    
    BCEdDSAPublicKey(final byte[] array, final byte[] array2) throws InvalidKeySpecException {
        final int length = array.length;
        if (Utils.isValidPrefix(array, array2)) {
            AsymmetricKeyParameter eddsaPublicKey;
            if (array2.length - length == 57) {
                eddsaPublicKey = new Ed448PublicKeyParameters(array2, length);
            }
            else {
                if (array2.length - length != 32) {
                    throw new InvalidKeySpecException("raw key data not recognised");
                }
                eddsaPublicKey = new Ed25519PublicKeyParameters(array2, length);
            }
            this.eddsaPublicKey = eddsaPublicKey;
            return;
        }
        throw new InvalidKeySpecException("raw key data not recognised");
    }
    
    private void populateFromPubKeyInfo(final SubjectPublicKeyInfo subjectPublicKeyInfo) {
        AsymmetricKeyParameter eddsaPublicKey;
        if (EdECObjectIdentifiers.id_Ed448.equals(subjectPublicKeyInfo.getAlgorithm().getAlgorithm())) {
            eddsaPublicKey = new Ed448PublicKeyParameters(subjectPublicKeyInfo.getPublicKeyData().getOctets(), 0);
        }
        else {
            eddsaPublicKey = new Ed25519PublicKeyParameters(subjectPublicKeyInfo.getPublicKeyData().getOctets(), 0);
        }
        this.eddsaPublicKey = eddsaPublicKey;
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.populateFromPubKeyInfo(SubjectPublicKeyInfo.getInstance(objectInputStream.readObject()));
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.getEncoded());
    }
    
    AsymmetricKeyParameter engineGetKeyParameters() {
        return this.eddsaPublicKey;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof BCEdDSAPublicKey && Arrays.areEqual(((BCEdDSAPublicKey)o).getEncoded(), this.getEncoded()));
    }
    
    @Override
    public String getAlgorithm() {
        if (this.eddsaPublicKey instanceof Ed448PublicKeyParameters) {
            return "Ed448";
        }
        return "Ed25519";
    }
    
    @Override
    public byte[] getEncoded() {
        if (this.eddsaPublicKey instanceof Ed448PublicKeyParameters) {
            final byte[] array = new byte[KeyFactorySpi.Ed448Prefix.length + 57];
            System.arraycopy(KeyFactorySpi.Ed448Prefix, 0, array, 0, KeyFactorySpi.Ed448Prefix.length);
            ((Ed448PublicKeyParameters)this.eddsaPublicKey).encode(array, KeyFactorySpi.Ed448Prefix.length);
            return array;
        }
        final byte[] array2 = new byte[KeyFactorySpi.Ed25519Prefix.length + 32];
        System.arraycopy(KeyFactorySpi.Ed25519Prefix, 0, array2, 0, KeyFactorySpi.Ed25519Prefix.length);
        ((Ed25519PublicKeyParameters)this.eddsaPublicKey).encode(array2, KeyFactorySpi.Ed25519Prefix.length);
        return array2;
    }
    
    @Override
    public String getFormat() {
        return "X.509";
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.getEncoded());
    }
    
    @Override
    public String toString() {
        return Utils.keyToString("Public Key", this.getAlgorithm(), this.eddsaPublicKey);
    }
}
