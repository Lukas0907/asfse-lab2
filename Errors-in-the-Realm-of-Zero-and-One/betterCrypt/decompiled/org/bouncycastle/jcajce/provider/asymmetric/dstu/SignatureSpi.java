// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.dstu;

import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1OctetString;
import java.math.BigInteger;
import java.security.SignatureException;
import org.bouncycastle.asn1.DEROctetString;
import java.security.spec.AlgorithmParameterSpec;
import java.security.PublicKey;
import java.security.InvalidKeyException;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.jce.interfaces.ECKey;
import org.bouncycastle.crypto.digests.GOST3411Digest;
import org.bouncycastle.asn1.ua.DSTU4145Params;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;
import java.security.PrivateKey;
import org.bouncycastle.crypto.signers.DSTU4145Signer;
import org.bouncycastle.crypto.DSAExt;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.asn1.x509.X509ObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;

public class SignatureSpi extends java.security.SignatureSpi implements PKCSObjectIdentifiers, X509ObjectIdentifiers
{
    private Digest digest;
    private DSAExt signer;
    
    public SignatureSpi() {
        this.signer = new DSTU4145Signer();
    }
    
    @Override
    protected Object engineGetParameter(final String s) {
        throw new UnsupportedOperationException("engineSetParameter unsupported");
    }
    
    @Override
    protected void engineInitSign(final PrivateKey privateKey) throws InvalidKeyException {
        AsymmetricKeyParameter asymmetricKeyParameter = null;
        Label_0067: {
            GOST3411Digest digest;
            if (privateKey instanceof BCDSTU4145PrivateKey) {
                asymmetricKeyParameter = ECUtil.generatePrivateKeyParameter(privateKey);
                digest = new GOST3411Digest(this.expandSbox(DSTU4145Params.getDefaultDKE()));
            }
            else {
                if (!(privateKey instanceof ECKey)) {
                    asymmetricKeyParameter = null;
                    break Label_0067;
                }
                asymmetricKeyParameter = ECUtil.generatePrivateKeyParameter(privateKey);
                digest = new GOST3411Digest(this.expandSbox(DSTU4145Params.getDefaultDKE()));
            }
            this.digest = digest;
        }
        if (this.appRandom != null) {
            this.signer.init(true, new ParametersWithRandom(asymmetricKeyParameter, this.appRandom));
            return;
        }
        this.signer.init(true, asymmetricKeyParameter);
    }
    
    @Override
    protected void engineInitVerify(final PublicKey publicKey) throws InvalidKeyException {
        AsymmetricKeyParameter asymmetricKeyParameter;
        if (publicKey instanceof BCDSTU4145PublicKey) {
            final BCDSTU4145PublicKey bcdstu4145PublicKey = (BCDSTU4145PublicKey)publicKey;
            asymmetricKeyParameter = bcdstu4145PublicKey.engineGetKeyParameters();
            this.digest = new GOST3411Digest(this.expandSbox(bcdstu4145PublicKey.getSbox()));
        }
        else {
            asymmetricKeyParameter = ECUtil.generatePublicKeyParameter(publicKey);
            this.digest = new GOST3411Digest(this.expandSbox(DSTU4145Params.getDefaultDKE()));
        }
        this.signer.init(false, asymmetricKeyParameter);
    }
    
    @Override
    protected void engineSetParameter(final String s, final Object o) {
        throw new UnsupportedOperationException("engineSetParameter unsupported");
    }
    
    @Override
    protected void engineSetParameter(final AlgorithmParameterSpec algorithmParameterSpec) {
        throw new UnsupportedOperationException("engineSetParameter unsupported");
    }
    
    @Override
    protected byte[] engineSign() throws SignatureException {
        while (true) {
            final byte[] array = new byte[this.digest.getDigestSize()];
            this.digest.doFinal(array, 0);
            while (true) {
                Label_0135: {
                    try {
                        final BigInteger[] generateSignature = this.signer.generateSignature(array);
                        final byte[] byteArray = generateSignature[0].toByteArray();
                        final byte[] byteArray2 = generateSignature[1].toByteArray();
                        if (byteArray.length > byteArray2.length) {
                            final int n = byteArray.length;
                            break Label_0135;
                        }
                        final int n = byteArray2.length;
                        break Label_0135;
                        final byte[] array2 = new byte[n * 2];
                        System.arraycopy(byteArray2, 0, array2, array2.length / 2 - byteArray2.length, byteArray2.length);
                        System.arraycopy(byteArray, 0, array2, array2.length - byteArray.length, byteArray.length);
                        return new DEROctetString(array2).getEncoded();
                    }
                    catch (Exception ex) {
                        throw new SignatureException(ex.toString());
                    }
                }
                continue;
            }
        }
    }
    
    @Override
    protected void engineUpdate(final byte b) throws SignatureException {
        this.digest.update(b);
    }
    
    @Override
    protected void engineUpdate(final byte[] array, final int n, final int n2) throws SignatureException {
        this.digest.update(array, n, n2);
    }
    
    @Override
    protected boolean engineVerify(byte[] magnitude) throws SignatureException {
        final byte[] array = new byte[this.digest.getDigestSize()];
        this.digest.doFinal(array, 0);
        try {
            final byte[] octets = ((ASN1OctetString)ASN1Primitive.fromByteArray(magnitude)).getOctets();
            magnitude = new byte[octets.length / 2];
            final byte[] magnitude2 = new byte[octets.length / 2];
            System.arraycopy(octets, 0, magnitude2, 0, octets.length / 2);
            System.arraycopy(octets, octets.length / 2, magnitude, 0, octets.length / 2);
            final BigInteger[] array2 = { new BigInteger(1, magnitude), new BigInteger(1, magnitude2) };
            return this.signer.verifySignature(array, array2[0], array2[1]);
        }
        catch (Exception ex) {
            throw new SignatureException("error decoding signature bytes.");
        }
    }
    
    byte[] expandSbox(final byte[] array) {
        final byte[] array2 = new byte[128];
        for (int i = 0; i < array.length; ++i) {
            final int n = i * 2;
            array2[n] = (byte)(array[i] >> 4 & 0xF);
            array2[n + 1] = (byte)(array[i] & 0xF);
        }
        return array2;
    }
}
