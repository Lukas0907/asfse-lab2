// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.ecgost12;

import java.util.Enumeration;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x9.X962Parameters;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.jce.spec.ECNamedCurveSpec;
import org.bouncycastle.jce.ECGOST3410NamedCurveTable;
import org.bouncycastle.asn1.cryptopro.ECGOST3410NamedCurves;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import java.io.IOException;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import java.security.spec.ECPrivateKeySpec;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.cryptopro.GOST3410PublicKeyAlgParameters;
import java.security.spec.ECParameterSpec;
import java.math.BigInteger;
import org.bouncycastle.jcajce.provider.asymmetric.util.PKCS12BagAttributeCarrierImpl;
import org.bouncycastle.jce.interfaces.ECPointEncoder;
import org.bouncycastle.jce.interfaces.PKCS12BagAttributeCarrier;
import java.security.interfaces.ECPrivateKey;

public class BCECGOST3410_2012PrivateKey implements ECPrivateKey, org.bouncycastle.jce.interfaces.ECPrivateKey, PKCS12BagAttributeCarrier, ECPointEncoder
{
    static final long serialVersionUID = 7245981689601667138L;
    private String algorithm;
    private transient PKCS12BagAttributeCarrierImpl attrCarrier;
    private transient BigInteger d;
    private transient ECParameterSpec ecSpec;
    private transient GOST3410PublicKeyAlgParameters gostParams;
    private transient DERBitString publicKey;
    private boolean withCompression;
    
    protected BCECGOST3410_2012PrivateKey() {
        this.algorithm = "ECGOST3410-2012";
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
    }
    
    public BCECGOST3410_2012PrivateKey(final String algorithm, final ECPrivateKeyParameters ecPrivateKeyParameters) {
        this.algorithm = "ECGOST3410-2012";
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
        this.algorithm = algorithm;
        this.d = ecPrivateKeyParameters.getD();
        this.ecSpec = null;
    }
    
    public BCECGOST3410_2012PrivateKey(final String algorithm, final ECPrivateKeyParameters ecPrivateKeyParameters, final BCECGOST3410_2012PublicKey bcecgost3410_2012PublicKey, final ECParameterSpec ecSpec) {
        this.algorithm = "ECGOST3410-2012";
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
        final ECDomainParameters parameters = ecPrivateKeyParameters.getParameters();
        this.algorithm = algorithm;
        this.d = ecPrivateKeyParameters.getD();
        if (ecSpec == null) {
            this.ecSpec = new ECParameterSpec(EC5Util.convertCurve(parameters.getCurve(), parameters.getSeed()), EC5Util.convertPoint(parameters.getG()), parameters.getN(), parameters.getH().intValue());
        }
        else {
            this.ecSpec = ecSpec;
        }
        this.gostParams = bcecgost3410_2012PublicKey.getGostParams();
        this.publicKey = this.getPublicKeyDetails(bcecgost3410_2012PublicKey);
    }
    
    public BCECGOST3410_2012PrivateKey(final String algorithm, final ECPrivateKeyParameters ecPrivateKeyParameters, final BCECGOST3410_2012PublicKey bcecgost3410_2012PublicKey, final org.bouncycastle.jce.spec.ECParameterSpec ecParameterSpec) {
        this.algorithm = "ECGOST3410-2012";
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
        final ECDomainParameters parameters = ecPrivateKeyParameters.getParameters();
        this.algorithm = algorithm;
        this.d = ecPrivateKeyParameters.getD();
        ECParameterSpec ecSpec;
        if (ecParameterSpec == null) {
            ecSpec = new ECParameterSpec(EC5Util.convertCurve(parameters.getCurve(), parameters.getSeed()), EC5Util.convertPoint(parameters.getG()), parameters.getN(), parameters.getH().intValue());
        }
        else {
            ecSpec = new ECParameterSpec(EC5Util.convertCurve(ecParameterSpec.getCurve(), ecParameterSpec.getSeed()), EC5Util.convertPoint(ecParameterSpec.getG()), ecParameterSpec.getN(), ecParameterSpec.getH().intValue());
        }
        this.ecSpec = ecSpec;
        this.gostParams = bcecgost3410_2012PublicKey.getGostParams();
        this.publicKey = this.getPublicKeyDetails(bcecgost3410_2012PublicKey);
    }
    
    public BCECGOST3410_2012PrivateKey(final ECPrivateKey ecPrivateKey) {
        this.algorithm = "ECGOST3410-2012";
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
        this.d = ecPrivateKey.getS();
        this.algorithm = ecPrivateKey.getAlgorithm();
        this.ecSpec = ecPrivateKey.getParams();
    }
    
    public BCECGOST3410_2012PrivateKey(final ECPrivateKeySpec ecPrivateKeySpec) {
        this.algorithm = "ECGOST3410-2012";
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
        this.d = ecPrivateKeySpec.getS();
        this.ecSpec = ecPrivateKeySpec.getParams();
    }
    
    BCECGOST3410_2012PrivateKey(final PrivateKeyInfo privateKeyInfo) throws IOException {
        this.algorithm = "ECGOST3410-2012";
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
        this.populateFromPrivKeyInfo(privateKeyInfo);
    }
    
    public BCECGOST3410_2012PrivateKey(final BCECGOST3410_2012PrivateKey bcecgost3410_2012PrivateKey) {
        this.algorithm = "ECGOST3410-2012";
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
        this.d = bcecgost3410_2012PrivateKey.d;
        this.ecSpec = bcecgost3410_2012PrivateKey.ecSpec;
        this.withCompression = bcecgost3410_2012PrivateKey.withCompression;
        this.attrCarrier = bcecgost3410_2012PrivateKey.attrCarrier;
        this.publicKey = bcecgost3410_2012PrivateKey.publicKey;
        this.gostParams = bcecgost3410_2012PrivateKey.gostParams;
    }
    
    public BCECGOST3410_2012PrivateKey(final org.bouncycastle.jce.spec.ECPrivateKeySpec ecPrivateKeySpec) {
        this.algorithm = "ECGOST3410-2012";
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
        this.d = ecPrivateKeySpec.getD();
        ECParameterSpec convertSpec;
        if (ecPrivateKeySpec.getParams() != null) {
            convertSpec = EC5Util.convertSpec(EC5Util.convertCurve(ecPrivateKeySpec.getParams().getCurve(), ecPrivateKeySpec.getParams().getSeed()), ecPrivateKeySpec.getParams());
        }
        else {
            convertSpec = null;
        }
        this.ecSpec = convertSpec;
    }
    
    private void extractBytes(final byte[] array, final int n, final int n2, final BigInteger bigInteger) {
        final byte[] byteArray = bigInteger.toByteArray();
        final int length = byteArray.length;
        int i;
        final int n3 = i = 0;
        byte[] array2 = byteArray;
        if (length < n) {
            array2 = new byte[n];
            System.arraycopy(byteArray, 0, array2, array2.length - byteArray.length, byteArray.length);
            i = n3;
        }
        while (i != n) {
            array[n2 + i] = array2[array2.length - 1 - i];
            ++i;
        }
    }
    
    private DERBitString getPublicKeyDetails(final BCECGOST3410_2012PublicKey bcecgost3410_2012PublicKey) {
        return SubjectPublicKeyInfo.getInstance(bcecgost3410_2012PublicKey.getEncoded()).getPublicKeyData();
    }
    
    private void populateFromPrivKeyInfo(final PrivateKeyInfo privateKeyInfo) throws IOException {
        final ASN1Primitive asn1Primitive = privateKeyInfo.getPrivateKeyAlgorithm().getParameters().toASN1Primitive();
        BigInteger d;
        if (asn1Primitive instanceof ASN1Sequence && (ASN1Sequence.getInstance(asn1Primitive).size() == 2 || ASN1Sequence.getInstance(asn1Primitive).size() == 3)) {
            this.gostParams = GOST3410PublicKeyAlgParameters.getInstance(privateKeyInfo.getPrivateKeyAlgorithm().getParameters());
            final ECNamedCurveParameterSpec parameterSpec = ECGOST3410NamedCurveTable.getParameterSpec(ECGOST3410NamedCurves.getName(this.gostParams.getPublicKeyParamSet()));
            this.ecSpec = new ECNamedCurveSpec(ECGOST3410NamedCurves.getName(this.gostParams.getPublicKeyParamSet()), EC5Util.convertCurve(parameterSpec.getCurve(), parameterSpec.getSeed()), EC5Util.convertPoint(parameterSpec.getG()), parameterSpec.getN(), parameterSpec.getH());
            final ASN1Encodable privateKey = privateKeyInfo.parsePrivateKey();
            if (privateKey instanceof ASN1Integer) {
                d = ASN1Integer.getInstance(privateKey).getPositiveValue();
            }
            else {
                final byte[] octets = ASN1OctetString.getInstance(privateKey).getOctets();
                final byte[] magnitude = new byte[octets.length];
                for (int i = 0; i != octets.length; ++i) {
                    magnitude[i] = octets[octets.length - 1 - i];
                }
                d = new BigInteger(1, magnitude);
            }
        }
        else {
            final X962Parameters instance = X962Parameters.getInstance(privateKeyInfo.getPrivateKeyAlgorithm().getParameters());
            Label_0404: {
                ECParameterSpec ecSpec;
                if (instance.isNamedCurve()) {
                    final ASN1ObjectIdentifier instance2 = ASN1ObjectIdentifier.getInstance(instance.getParameters());
                    final X9ECParameters namedCurveByOid = ECUtil.getNamedCurveByOid(instance2);
                    if (namedCurveByOid == null) {
                        final ECDomainParameters byOID = ECGOST3410NamedCurves.getByOID(instance2);
                        ecSpec = new ECNamedCurveSpec(ECGOST3410NamedCurves.getName(instance2), EC5Util.convertCurve(byOID.getCurve(), byOID.getSeed()), EC5Util.convertPoint(byOID.getG()), byOID.getN(), byOID.getH());
                    }
                    else {
                        ecSpec = new ECNamedCurveSpec(ECUtil.getCurveName(instance2), EC5Util.convertCurve(namedCurveByOid.getCurve(), namedCurveByOid.getSeed()), EC5Util.convertPoint(namedCurveByOid.getG()), namedCurveByOid.getN(), namedCurveByOid.getH());
                    }
                }
                else {
                    if (instance.isImplicitlyCA()) {
                        this.ecSpec = null;
                        break Label_0404;
                    }
                    final X9ECParameters instance3 = X9ECParameters.getInstance(instance.getParameters());
                    ecSpec = new ECParameterSpec(EC5Util.convertCurve(instance3.getCurve(), instance3.getSeed()), EC5Util.convertPoint(instance3.getG()), instance3.getN(), instance3.getH().intValue());
                }
                this.ecSpec = ecSpec;
            }
            final ASN1Encodable privateKey2 = privateKeyInfo.parsePrivateKey();
            if (!(privateKey2 instanceof ASN1Integer)) {
                final org.bouncycastle.asn1.sec.ECPrivateKey instance4 = org.bouncycastle.asn1.sec.ECPrivateKey.getInstance(privateKey2);
                this.d = instance4.getKey();
                this.publicKey = instance4.getPublicKey();
                return;
            }
            d = ASN1Integer.getInstance(privateKey2).getValue();
        }
        this.d = d;
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.populateFromPrivKeyInfo(PrivateKeyInfo.getInstance(ASN1Primitive.fromByteArray((byte[])objectInputStream.readObject())));
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.getEncoded());
    }
    
    org.bouncycastle.jce.spec.ECParameterSpec engineGetSpec() {
        final ECParameterSpec ecSpec = this.ecSpec;
        if (ecSpec != null) {
            return EC5Util.convertSpec(ecSpec);
        }
        return BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa();
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof BCECGOST3410_2012PrivateKey;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final BCECGOST3410_2012PrivateKey bcecgost3410_2012PrivateKey = (BCECGOST3410_2012PrivateKey)o;
        boolean b3 = b2;
        if (this.getD().equals(bcecgost3410_2012PrivateKey.getD())) {
            b3 = b2;
            if (this.engineGetSpec().equals(bcecgost3410_2012PrivateKey.engineGetSpec())) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public String getAlgorithm() {
        return this.algorithm;
    }
    
    @Override
    public ASN1Encodable getBagAttribute(final ASN1ObjectIdentifier asn1ObjectIdentifier) {
        return this.attrCarrier.getBagAttribute(asn1ObjectIdentifier);
    }
    
    @Override
    public Enumeration getBagAttributeKeys() {
        return this.attrCarrier.getBagAttributeKeys();
    }
    
    @Override
    public BigInteger getD() {
        return this.d;
    }
    
    @Override
    public byte[] getEncoded() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost12/BCECGOST3410_2012PrivateKey.d:Ljava/math/BigInteger;
        //     4: invokevirtual   java/math/BigInteger.bitLength:()I
        //     7: sipush          256
        //    10: if_icmple       18
        //    13: iconst_1       
        //    14: istore_1       
        //    15: goto            20
        //    18: iconst_0       
        //    19: istore_1       
        //    20: iload_1        
        //    21: ifeq            31
        //    24: getstatic       org/bouncycastle/asn1/rosstandart/RosstandartObjectIdentifiers.id_tc26_gost_3410_12_512:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //    27: astore_3       
        //    28: goto            35
        //    31: getstatic       org/bouncycastle/asn1/rosstandart/RosstandartObjectIdentifiers.id_tc26_gost_3410_12_256:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //    34: astore_3       
        //    35: iload_1        
        //    36: ifeq            45
        //    39: bipush          64
        //    41: istore_1       
        //    42: goto            48
        //    45: bipush          32
        //    47: istore_1       
        //    48: aload_0        
        //    49: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost12/BCECGOST3410_2012PrivateKey.gostParams:Lorg/bouncycastle/asn1/cryptopro/GOST3410PublicKeyAlgParameters;
        //    52: ifnull          106
        //    55: iload_1        
        //    56: newarray        B
        //    58: astore_2       
        //    59: aload_0        
        //    60: aload_2        
        //    61: iload_1        
        //    62: iconst_0       
        //    63: aload_0        
        //    64: invokevirtual   org/bouncycastle/jcajce/provider/asymmetric/ecgost12/BCECGOST3410_2012PrivateKey.getS:()Ljava/math/BigInteger;
        //    67: invokespecial   org/bouncycastle/jcajce/provider/asymmetric/ecgost12/BCECGOST3410_2012PrivateKey.extractBytes:([BIILjava/math/BigInteger;)V
        //    70: new             Lorg/bouncycastle/asn1/pkcs/PrivateKeyInfo;
        //    73: dup            
        //    74: new             Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //    77: dup            
        //    78: aload_3        
        //    79: aload_0        
        //    80: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost12/BCECGOST3410_2012PrivateKey.gostParams:Lorg/bouncycastle/asn1/cryptopro/GOST3410PublicKeyAlgParameters;
        //    83: invokespecial   org/bouncycastle/asn1/x509/AlgorithmIdentifier.<init>:(Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;Lorg/bouncycastle/asn1/ASN1Encodable;)V
        //    86: new             Lorg/bouncycastle/asn1/DEROctetString;
        //    89: dup            
        //    90: aload_2        
        //    91: invokespecial   org/bouncycastle/asn1/DEROctetString.<init>:([B)V
        //    94: invokespecial   org/bouncycastle/asn1/pkcs/PrivateKeyInfo.<init>:(Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;Lorg/bouncycastle/asn1/ASN1Encodable;)V
        //    97: ldc_w           "DER"
        //   100: invokevirtual   org/bouncycastle/asn1/pkcs/PrivateKeyInfo.getEncoded:(Ljava/lang/String;)[B
        //   103: astore_2       
        //   104: aload_2        
        //   105: areturn        
        //   106: aload_0        
        //   107: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost12/BCECGOST3410_2012PrivateKey.ecSpec:Ljava/security/spec/ECParameterSpec;
        //   110: astore_2       
        //   111: aload_2        
        //   112: instanceof      Lorg/bouncycastle/jce/spec/ECNamedCurveSpec;
        //   115: ifeq            168
        //   118: aload_2        
        //   119: checkcast       Lorg/bouncycastle/jce/spec/ECNamedCurveSpec;
        //   122: invokevirtual   org/bouncycastle/jce/spec/ECNamedCurveSpec.getName:()Ljava/lang/String;
        //   125: invokestatic    org/bouncycastle/jcajce/provider/asymmetric/util/ECUtil.getNamedCurveOid:(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //   128: astore          4
        //   130: aload           4
        //   132: astore_2       
        //   133: aload           4
        //   135: ifnonnull       156
        //   138: new             Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //   141: dup            
        //   142: aload_0        
        //   143: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost12/BCECGOST3410_2012PrivateKey.ecSpec:Ljava/security/spec/ECParameterSpec;
        //   146: checkcast       Lorg/bouncycastle/jce/spec/ECNamedCurveSpec;
        //   149: invokevirtual   org/bouncycastle/jce/spec/ECNamedCurveSpec.getName:()Ljava/lang/String;
        //   152: invokespecial   org/bouncycastle/asn1/ASN1ObjectIdentifier.<init>:(Ljava/lang/String;)V
        //   155: astore_2       
        //   156: new             Lorg/bouncycastle/asn1/x9/X962Parameters;
        //   159: dup            
        //   160: aload_2        
        //   161: invokespecial   org/bouncycastle/asn1/x9/X962Parameters.<init>:(Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;)V
        //   164: astore_2       
        //   165: goto            272
        //   168: aload_2        
        //   169: ifnonnull       198
        //   172: new             Lorg/bouncycastle/asn1/x9/X962Parameters;
        //   175: dup            
        //   176: getstatic       org/bouncycastle/asn1/DERNull.INSTANCE:Lorg/bouncycastle/asn1/DERNull;
        //   179: invokespecial   org/bouncycastle/asn1/x9/X962Parameters.<init>:(Lorg/bouncycastle/asn1/ASN1Null;)V
        //   182: astore_2       
        //   183: getstatic       org/bouncycastle/jce/provider/BouncyCastleProvider.CONFIGURATION:Lorg/bouncycastle/jcajce/provider/config/ProviderConfiguration;
        //   186: aconst_null    
        //   187: aload_0        
        //   188: invokevirtual   org/bouncycastle/jcajce/provider/asymmetric/ecgost12/BCECGOST3410_2012PrivateKey.getS:()Ljava/math/BigInteger;
        //   191: invokestatic    org/bouncycastle/jcajce/provider/asymmetric/util/ECUtil.getOrderBitLength:(Lorg/bouncycastle/jcajce/provider/config/ProviderConfiguration;Ljava/math/BigInteger;Ljava/math/BigInteger;)I
        //   194: istore_1       
        //   195: goto            290
        //   198: aload_2        
        //   199: invokevirtual   java/security/spec/ECParameterSpec.getCurve:()Ljava/security/spec/EllipticCurve;
        //   202: invokestatic    org/bouncycastle/jcajce/provider/asymmetric/util/EC5Util.convertCurve:(Ljava/security/spec/EllipticCurve;)Lorg/bouncycastle/math/ec/ECCurve;
        //   205: astore_2       
        //   206: new             Lorg/bouncycastle/asn1/x9/X962Parameters;
        //   209: dup            
        //   210: new             Lorg/bouncycastle/asn1/x9/X9ECParameters;
        //   213: dup            
        //   214: aload_2        
        //   215: new             Lorg/bouncycastle/asn1/x9/X9ECPoint;
        //   218: dup            
        //   219: aload_2        
        //   220: aload_0        
        //   221: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost12/BCECGOST3410_2012PrivateKey.ecSpec:Ljava/security/spec/ECParameterSpec;
        //   224: invokevirtual   java/security/spec/ECParameterSpec.getGenerator:()Ljava/security/spec/ECPoint;
        //   227: invokestatic    org/bouncycastle/jcajce/provider/asymmetric/util/EC5Util.convertPoint:(Lorg/bouncycastle/math/ec/ECCurve;Ljava/security/spec/ECPoint;)Lorg/bouncycastle/math/ec/ECPoint;
        //   230: aload_0        
        //   231: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost12/BCECGOST3410_2012PrivateKey.withCompression:Z
        //   234: invokespecial   org/bouncycastle/asn1/x9/X9ECPoint.<init>:(Lorg/bouncycastle/math/ec/ECPoint;Z)V
        //   237: aload_0        
        //   238: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost12/BCECGOST3410_2012PrivateKey.ecSpec:Ljava/security/spec/ECParameterSpec;
        //   241: invokevirtual   java/security/spec/ECParameterSpec.getOrder:()Ljava/math/BigInteger;
        //   244: aload_0        
        //   245: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost12/BCECGOST3410_2012PrivateKey.ecSpec:Ljava/security/spec/ECParameterSpec;
        //   248: invokevirtual   java/security/spec/ECParameterSpec.getCofactor:()I
        //   251: i2l            
        //   252: invokestatic    java/math/BigInteger.valueOf:(J)Ljava/math/BigInteger;
        //   255: aload_0        
        //   256: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost12/BCECGOST3410_2012PrivateKey.ecSpec:Ljava/security/spec/ECParameterSpec;
        //   259: invokevirtual   java/security/spec/ECParameterSpec.getCurve:()Ljava/security/spec/EllipticCurve;
        //   262: invokevirtual   java/security/spec/EllipticCurve.getSeed:()[B
        //   265: invokespecial   org/bouncycastle/asn1/x9/X9ECParameters.<init>:(Lorg/bouncycastle/math/ec/ECCurve;Lorg/bouncycastle/asn1/x9/X9ECPoint;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)V
        //   268: invokespecial   org/bouncycastle/asn1/x9/X962Parameters.<init>:(Lorg/bouncycastle/asn1/x9/X9ECParameters;)V
        //   271: astore_2       
        //   272: getstatic       org/bouncycastle/jce/provider/BouncyCastleProvider.CONFIGURATION:Lorg/bouncycastle/jcajce/provider/config/ProviderConfiguration;
        //   275: aload_0        
        //   276: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost12/BCECGOST3410_2012PrivateKey.ecSpec:Ljava/security/spec/ECParameterSpec;
        //   279: invokevirtual   java/security/spec/ECParameterSpec.getOrder:()Ljava/math/BigInteger;
        //   282: aload_0        
        //   283: invokevirtual   org/bouncycastle/jcajce/provider/asymmetric/ecgost12/BCECGOST3410_2012PrivateKey.getS:()Ljava/math/BigInteger;
        //   286: invokestatic    org/bouncycastle/jcajce/provider/asymmetric/util/ECUtil.getOrderBitLength:(Lorg/bouncycastle/jcajce/provider/config/ProviderConfiguration;Ljava/math/BigInteger;Ljava/math/BigInteger;)I
        //   289: istore_1       
        //   290: aload_0        
        //   291: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost12/BCECGOST3410_2012PrivateKey.publicKey:Lorg/bouncycastle/asn1/DERBitString;
        //   294: ifnull          319
        //   297: new             Lorg/bouncycastle/asn1/sec/ECPrivateKey;
        //   300: dup            
        //   301: iload_1        
        //   302: aload_0        
        //   303: invokevirtual   org/bouncycastle/jcajce/provider/asymmetric/ecgost12/BCECGOST3410_2012PrivateKey.getS:()Ljava/math/BigInteger;
        //   306: aload_0        
        //   307: getfield        org/bouncycastle/jcajce/provider/asymmetric/ecgost12/BCECGOST3410_2012PrivateKey.publicKey:Lorg/bouncycastle/asn1/DERBitString;
        //   310: aload_2        
        //   311: invokespecial   org/bouncycastle/asn1/sec/ECPrivateKey.<init>:(ILjava/math/BigInteger;Lorg/bouncycastle/asn1/DERBitString;Lorg/bouncycastle/asn1/ASN1Encodable;)V
        //   314: astore          4
        //   316: goto            334
        //   319: new             Lorg/bouncycastle/asn1/sec/ECPrivateKey;
        //   322: dup            
        //   323: iload_1        
        //   324: aload_0        
        //   325: invokevirtual   org/bouncycastle/jcajce/provider/asymmetric/ecgost12/BCECGOST3410_2012PrivateKey.getS:()Ljava/math/BigInteger;
        //   328: aload_2        
        //   329: invokespecial   org/bouncycastle/asn1/sec/ECPrivateKey.<init>:(ILjava/math/BigInteger;Lorg/bouncycastle/asn1/ASN1Encodable;)V
        //   332: astore          4
        //   334: new             Lorg/bouncycastle/asn1/pkcs/PrivateKeyInfo;
        //   337: dup            
        //   338: new             Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //   341: dup            
        //   342: aload_3        
        //   343: aload_2        
        //   344: invokevirtual   org/bouncycastle/asn1/x9/X962Parameters.toASN1Primitive:()Lorg/bouncycastle/asn1/ASN1Primitive;
        //   347: invokespecial   org/bouncycastle/asn1/x509/AlgorithmIdentifier.<init>:(Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;Lorg/bouncycastle/asn1/ASN1Encodable;)V
        //   350: aload           4
        //   352: invokevirtual   org/bouncycastle/asn1/sec/ECPrivateKey.toASN1Primitive:()Lorg/bouncycastle/asn1/ASN1Primitive;
        //   355: invokespecial   org/bouncycastle/asn1/pkcs/PrivateKeyInfo.<init>:(Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;Lorg/bouncycastle/asn1/ASN1Encodable;)V
        //   358: ldc_w           "DER"
        //   361: invokevirtual   org/bouncycastle/asn1/pkcs/PrivateKeyInfo.getEncoded:(Ljava/lang/String;)[B
        //   364: astore_2       
        //   365: aload_2        
        //   366: areturn        
        //   367: astore_2       
        //   368: aconst_null    
        //   369: areturn        
        //   370: astore_2       
        //   371: aconst_null    
        //   372: areturn        
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  70     104    367    370    Ljava/io/IOException;
        //  334    365    370    373    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0334:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public String getFormat() {
        return "PKCS#8";
    }
    
    @Override
    public org.bouncycastle.jce.spec.ECParameterSpec getParameters() {
        final ECParameterSpec ecSpec = this.ecSpec;
        if (ecSpec == null) {
            return null;
        }
        return EC5Util.convertSpec(ecSpec);
    }
    
    @Override
    public ECParameterSpec getParams() {
        return this.ecSpec;
    }
    
    @Override
    public BigInteger getS() {
        return this.d;
    }
    
    @Override
    public int hashCode() {
        return this.getD().hashCode() ^ this.engineGetSpec().hashCode();
    }
    
    @Override
    public void setBagAttribute(final ASN1ObjectIdentifier asn1ObjectIdentifier, final ASN1Encodable asn1Encodable) {
        this.attrCarrier.setBagAttribute(asn1ObjectIdentifier, asn1Encodable);
    }
    
    @Override
    public void setPointFormat(final String anotherString) {
        this.withCompression = ("UNCOMPRESSED".equalsIgnoreCase(anotherString) ^ true);
    }
    
    @Override
    public String toString() {
        return ECUtil.privateKeyToString(this.algorithm, this.d, this.engineGetSpec());
    }
}
