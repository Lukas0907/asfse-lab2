// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.ec;

import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.jcajce.provider.asymmetric.util.KeyUtil;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import java.io.ObjectOutputStream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import java.io.ObjectInputStream;
import org.bouncycastle.math.ec.ECCurve;
import java.io.IOException;
import org.bouncycastle.asn1.x9.X9ECPoint;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.x9.X9IntegerConverter;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.x9.X962Parameters;
import java.security.spec.EllipticCurve;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util;
import java.security.spec.ECPublicKeySpec;
import java.security.spec.ECParameterSpec;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.jcajce.provider.config.ProviderConfiguration;
import org.bouncycastle.jce.interfaces.ECPointEncoder;
import java.security.interfaces.ECPublicKey;

public class BCECPublicKey implements ECPublicKey, org.bouncycastle.jce.interfaces.ECPublicKey, ECPointEncoder
{
    static final long serialVersionUID = 2422789860422731812L;
    private String algorithm;
    private transient ProviderConfiguration configuration;
    private transient ECPublicKeyParameters ecPublicKey;
    private transient ECParameterSpec ecSpec;
    private boolean withCompression;
    
    public BCECPublicKey(final String algorithm, final ECPublicKeySpec ecPublicKeySpec, final ProviderConfiguration configuration) {
        this.algorithm = "EC";
        this.algorithm = algorithm;
        this.ecSpec = ecPublicKeySpec.getParams();
        this.ecPublicKey = new ECPublicKeyParameters(EC5Util.convertPoint(this.ecSpec, ecPublicKeySpec.getW()), EC5Util.getDomainParameters(configuration, ecPublicKeySpec.getParams()));
        this.configuration = configuration;
    }
    
    BCECPublicKey(final String algorithm, final SubjectPublicKeyInfo subjectPublicKeyInfo, final ProviderConfiguration configuration) {
        this.algorithm = "EC";
        this.algorithm = algorithm;
        this.configuration = configuration;
        this.populateFromPubKeyInfo(subjectPublicKeyInfo);
    }
    
    public BCECPublicKey(final String algorithm, final ECPublicKeyParameters ecPublicKey, final ECParameterSpec ecSpec, final ProviderConfiguration configuration) {
        this.algorithm = "EC";
        final ECDomainParameters parameters = ecPublicKey.getParameters();
        this.algorithm = algorithm;
        this.ecPublicKey = ecPublicKey;
        if (ecSpec == null) {
            this.ecSpec = this.createSpec(EC5Util.convertCurve(parameters.getCurve(), parameters.getSeed()), parameters);
        }
        else {
            this.ecSpec = ecSpec;
        }
        this.configuration = configuration;
    }
    
    public BCECPublicKey(final String algorithm, final ECPublicKeyParameters ecPublicKey, final ProviderConfiguration configuration) {
        this.algorithm = "EC";
        this.algorithm = algorithm;
        this.ecPublicKey = ecPublicKey;
        this.ecSpec = null;
        this.configuration = configuration;
    }
    
    public BCECPublicKey(final String algorithm, final ECPublicKeyParameters ecPublicKey, final org.bouncycastle.jce.spec.ECParameterSpec ecParameterSpec, final ProviderConfiguration configuration) {
        this.algorithm = "EC";
        final ECDomainParameters parameters = ecPublicKey.getParameters();
        this.algorithm = algorithm;
        ECParameterSpec ecSpec;
        if (ecParameterSpec == null) {
            ecSpec = this.createSpec(EC5Util.convertCurve(parameters.getCurve(), parameters.getSeed()), parameters);
        }
        else {
            ecSpec = EC5Util.convertSpec(EC5Util.convertCurve(ecParameterSpec.getCurve(), ecParameterSpec.getSeed()), ecParameterSpec);
        }
        this.ecSpec = ecSpec;
        this.ecPublicKey = ecPublicKey;
        this.configuration = configuration;
    }
    
    public BCECPublicKey(final String algorithm, final BCECPublicKey bcecPublicKey) {
        this.algorithm = "EC";
        this.algorithm = algorithm;
        this.ecPublicKey = bcecPublicKey.ecPublicKey;
        this.ecSpec = bcecPublicKey.ecSpec;
        this.withCompression = bcecPublicKey.withCompression;
        this.configuration = bcecPublicKey.configuration;
    }
    
    public BCECPublicKey(final String algorithm, final org.bouncycastle.jce.spec.ECPublicKeySpec ecPublicKeySpec, final ProviderConfiguration configuration) {
        this.algorithm = "EC";
        this.algorithm = algorithm;
        if (ecPublicKeySpec.getParams() != null) {
            final EllipticCurve convertCurve = EC5Util.convertCurve(ecPublicKeySpec.getParams().getCurve(), ecPublicKeySpec.getParams().getSeed());
            this.ecPublicKey = new ECPublicKeyParameters(ecPublicKeySpec.getQ(), ECUtil.getDomainParameters(configuration, ecPublicKeySpec.getParams()));
            this.ecSpec = EC5Util.convertSpec(convertCurve, ecPublicKeySpec.getParams());
        }
        else {
            this.ecPublicKey = new ECPublicKeyParameters(configuration.getEcImplicitlyCa().getCurve().createPoint(ecPublicKeySpec.getQ().getAffineXCoord().toBigInteger(), ecPublicKeySpec.getQ().getAffineYCoord().toBigInteger()), EC5Util.getDomainParameters(configuration, null));
            this.ecSpec = null;
        }
        this.configuration = configuration;
    }
    
    public BCECPublicKey(final ECPublicKey ecPublicKey, final ProviderConfiguration configuration) {
        this.algorithm = "EC";
        this.algorithm = ecPublicKey.getAlgorithm();
        this.ecSpec = ecPublicKey.getParams();
        this.ecPublicKey = new ECPublicKeyParameters(EC5Util.convertPoint(this.ecSpec, ecPublicKey.getW()), EC5Util.getDomainParameters(configuration, ecPublicKey.getParams()));
        this.configuration = configuration;
    }
    
    private ECParameterSpec createSpec(final EllipticCurve curve, final ECDomainParameters ecDomainParameters) {
        return new ECParameterSpec(curve, EC5Util.convertPoint(ecDomainParameters.getG()), ecDomainParameters.getN(), ecDomainParameters.getH().intValue());
    }
    
    private void populateFromPubKeyInfo(final SubjectPublicKeyInfo subjectPublicKeyInfo) {
        final X962Parameters instance = X962Parameters.getInstance(subjectPublicKeyInfo.getAlgorithm().getParameters());
        final ECCurve curve = EC5Util.getCurve(this.configuration, instance);
        this.ecSpec = EC5Util.convertToSpec(instance, curve);
        final byte[] bytes = subjectPublicKeyInfo.getPublicKeyData().getBytes();
        ASN1OctetString asn1OctetString;
        final DEROctetString derOctetString = (DEROctetString)(asn1OctetString = new DEROctetString(bytes));
        Label_0136: {
            if (bytes[0] != 4) {
                break Label_0136;
            }
            asn1OctetString = derOctetString;
            if (bytes[1] != bytes.length - 2) {
                break Label_0136;
            }
            if (bytes[2] != 2) {
                asn1OctetString = derOctetString;
                if (bytes[2] != 3) {
                    break Label_0136;
                }
            }
            asn1OctetString = derOctetString;
            if (new X9IntegerConverter().getByteLength(curve) < bytes.length - 3) {
                break Label_0136;
            }
            while (true) {
                while (true) {
                    try {
                        asn1OctetString = (ASN1OctetString)ASN1Primitive.fromByteArray(bytes);
                        this.ecPublicKey = new ECPublicKeyParameters(new X9ECPoint(curve, asn1OctetString).getPoint(), ECUtil.getDomainParameters(this.configuration, instance));
                        return;
                        throw new IllegalArgumentException("error recovering public key");
                    }
                    catch (IOException ex) {}
                    continue;
                }
            }
        }
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        final byte[] array = (byte[])objectInputStream.readObject();
        this.configuration = BouncyCastleProvider.CONFIGURATION;
        this.populateFromPubKeyInfo(SubjectPublicKeyInfo.getInstance(ASN1Primitive.fromByteArray(array)));
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.getEncoded());
    }
    
    ECPublicKeyParameters engineGetKeyParameters() {
        return this.ecPublicKey;
    }
    
    org.bouncycastle.jce.spec.ECParameterSpec engineGetSpec() {
        final ECParameterSpec ecSpec = this.ecSpec;
        if (ecSpec != null) {
            return EC5Util.convertSpec(ecSpec);
        }
        return this.configuration.getEcImplicitlyCa();
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof BCECPublicKey;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final BCECPublicKey bcecPublicKey = (BCECPublicKey)o;
        boolean b3 = b2;
        if (this.ecPublicKey.getQ().equals(bcecPublicKey.ecPublicKey.getQ())) {
            b3 = b2;
            if (this.engineGetSpec().equals(bcecPublicKey.engineGetSpec())) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public String getAlgorithm() {
        return this.algorithm;
    }
    
    @Override
    public byte[] getEncoded() {
        return KeyUtil.getEncodedSubjectPublicKeyInfo(new AlgorithmIdentifier(X9ObjectIdentifiers.id_ecPublicKey, ECUtils.getDomainParametersFromName(this.ecSpec, this.withCompression)), this.ecPublicKey.getQ().getEncoded(this.withCompression));
    }
    
    @Override
    public String getFormat() {
        return "X.509";
    }
    
    @Override
    public org.bouncycastle.jce.spec.ECParameterSpec getParameters() {
        final ECParameterSpec ecSpec = this.ecSpec;
        if (ecSpec == null) {
            return null;
        }
        return EC5Util.convertSpec(ecSpec);
    }
    
    @Override
    public ECParameterSpec getParams() {
        return this.ecSpec;
    }
    
    @Override
    public ECPoint getQ() {
        ECPoint ecPoint = this.ecPublicKey.getQ();
        if (this.ecSpec == null) {
            ecPoint = ecPoint.getDetachedPoint();
        }
        return ecPoint;
    }
    
    @Override
    public java.security.spec.ECPoint getW() {
        return EC5Util.convertPoint(this.ecPublicKey.getQ());
    }
    
    @Override
    public int hashCode() {
        return this.ecPublicKey.getQ().hashCode() ^ this.engineGetSpec().hashCode();
    }
    
    @Override
    public void setPointFormat(final String anotherString) {
        this.withCompression = ("UNCOMPRESSED".equalsIgnoreCase(anotherString) ^ true);
    }
    
    @Override
    public String toString() {
        return ECUtil.publicKeyToString("EC", this.ecPublicKey.getQ(), this.engineGetSpec());
    }
}
