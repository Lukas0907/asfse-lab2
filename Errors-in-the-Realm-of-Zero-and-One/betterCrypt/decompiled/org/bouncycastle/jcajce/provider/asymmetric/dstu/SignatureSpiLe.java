// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.dstu;

import java.io.IOException;
import org.bouncycastle.asn1.ASN1Primitive;
import java.security.SignatureException;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ASN1OctetString;

public class SignatureSpiLe extends SignatureSpi
{
    @Override
    protected byte[] engineSign() throws SignatureException {
        final byte[] octets = ASN1OctetString.getInstance(super.engineSign()).getOctets();
        this.reverseBytes(octets);
        try {
            return new DEROctetString(octets).getEncoded();
        }
        catch (Exception ex) {
            throw new SignatureException(ex.toString());
        }
    }
    
    @Override
    protected boolean engineVerify(byte[] octets) throws SignatureException {
        try {
            octets = ((ASN1OctetString)ASN1Primitive.fromByteArray(octets)).getOctets();
            this.reverseBytes(octets);
            try {
                return super.engineVerify(new DEROctetString(octets).getEncoded());
            }
            catch (Exception ex) {
                throw new SignatureException(ex.toString());
            }
            catch (SignatureException ex2) {
                throw ex2;
            }
            throw new SignatureException("error decoding signature bytes.");
        }
        catch (IOException ex3) {
            throw new SignatureException("error decoding signature bytes.");
        }
    }
    
    void reverseBytes(final byte[] array) {
        for (int i = 0; i < array.length / 2; ++i) {
            final byte b = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = b;
        }
    }
}
