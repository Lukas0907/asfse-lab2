// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.ecgost12;

import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECNamedCurveGenParameterSpec;
import java.math.BigInteger;
import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util;
import java.security.InvalidParameterException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.ECGenParameterSpec;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import java.security.KeyPair;
import java.security.InvalidAlgorithmParameterException;
import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECGOST3410Parameters;
import org.bouncycastle.crypto.params.ECNamedDomainParameters;
import org.bouncycastle.jce.spec.ECNamedCurveSpec;
import org.bouncycastle.asn1.cryptopro.ECGOST3410NamedCurves;
import org.bouncycastle.jcajce.spec.GOST3410ParameterSpec;
import java.security.SecureRandom;
import org.bouncycastle.crypto.params.ECKeyGenerationParameters;
import org.bouncycastle.crypto.generators.ECKeyPairGenerator;
import java.security.KeyPairGenerator;

public class KeyPairGeneratorSpi extends KeyPairGenerator
{
    String algorithm;
    Object ecParams;
    ECKeyPairGenerator engine;
    boolean initialised;
    ECKeyGenerationParameters param;
    SecureRandom random;
    int strength;
    
    public KeyPairGeneratorSpi() {
        super("ECGOST3410-2012");
        this.ecParams = null;
        this.engine = new ECKeyPairGenerator();
        this.algorithm = "ECGOST3410-2012";
        this.strength = 239;
        this.random = null;
        this.initialised = false;
    }
    
    private void init(final GOST3410ParameterSpec gost3410ParameterSpec, final SecureRandom secureRandom) throws InvalidAlgorithmParameterException {
        final ECDomainParameters byOID = ECGOST3410NamedCurves.getByOID(gost3410ParameterSpec.getPublicKeyParamSet());
        if (byOID != null) {
            this.ecParams = new ECNamedCurveSpec(ECGOST3410NamedCurves.getName(gost3410ParameterSpec.getPublicKeyParamSet()), byOID.getCurve(), byOID.getG(), byOID.getN(), byOID.getH(), byOID.getSeed());
            this.param = new ECKeyGenerationParameters(new ECGOST3410Parameters(new ECNamedDomainParameters(gost3410ParameterSpec.getPublicKeyParamSet(), byOID), gost3410ParameterSpec.getPublicKeyParamSet(), gost3410ParameterSpec.getDigestParamSet(), gost3410ParameterSpec.getEncryptionParamSet()), secureRandom);
            this.engine.init(this.param);
            this.initialised = true;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("unknown curve: ");
        sb.append(gost3410ParameterSpec.getPublicKeyParamSet());
        throw new InvalidAlgorithmParameterException(sb.toString());
    }
    
    @Override
    public KeyPair generateKeyPair() {
        if (!this.initialised) {
            throw new IllegalStateException("EC Key Pair Generator not initialised");
        }
        final AsymmetricCipherKeyPair generateKeyPair = this.engine.generateKeyPair();
        final ECPublicKeyParameters ecPublicKeyParameters = (ECPublicKeyParameters)generateKeyPair.getPublic();
        final ECPrivateKeyParameters ecPrivateKeyParameters = (ECPrivateKeyParameters)generateKeyPair.getPrivate();
        final Object ecParams = this.ecParams;
        if (ecParams instanceof ECParameterSpec) {
            final ECParameterSpec ecParameterSpec = (ECParameterSpec)ecParams;
            final BCECGOST3410_2012PublicKey publicKey = new BCECGOST3410_2012PublicKey(this.algorithm, ecPublicKeyParameters, ecParameterSpec);
            return new KeyPair(publicKey, new BCECGOST3410_2012PrivateKey(this.algorithm, ecPrivateKeyParameters, publicKey, ecParameterSpec));
        }
        if (ecParams == null) {
            return new KeyPair(new BCECGOST3410_2012PublicKey(this.algorithm, ecPublicKeyParameters), new BCECGOST3410_2012PrivateKey(this.algorithm, ecPrivateKeyParameters));
        }
        final java.security.spec.ECParameterSpec ecParameterSpec2 = (java.security.spec.ECParameterSpec)ecParams;
        final BCECGOST3410_2012PublicKey publicKey2 = new BCECGOST3410_2012PublicKey(this.algorithm, ecPublicKeyParameters, ecParameterSpec2);
        return new KeyPair(publicKey2, new BCECGOST3410_2012PrivateKey(this.algorithm, ecPrivateKeyParameters, publicKey2, ecParameterSpec2));
    }
    
    @Override
    public void initialize(final int strength, final SecureRandom random) {
        this.strength = strength;
        this.random = random;
        final Object ecParams = this.ecParams;
        Label_0039: {
            if (ecParams == null) {
                break Label_0039;
            }
            while (true) {
                while (true) {
                    try {
                        this.initialize((AlgorithmParameterSpec)ecParams, random);
                        return;
                        throw new InvalidParameterException("unknown key size.");
                        throw new InvalidParameterException("key size not configurable.");
                    }
                    catch (InvalidAlgorithmParameterException ex) {}
                    continue;
                }
            }
        }
    }
    
    @Override
    public void initialize(final AlgorithmParameterSpec ecParams, final SecureRandom secureRandom) throws InvalidAlgorithmParameterException {
        if (ecParams instanceof GOST3410ParameterSpec) {
            this.init((GOST3410ParameterSpec)ecParams, secureRandom);
            return;
        }
        Label_0076: {
            ECKeyGenerationParameters param;
            if (ecParams instanceof ECParameterSpec) {
                final ECParameterSpec ecParameterSpec = (ECParameterSpec)ecParams;
                this.ecParams = ecParams;
                param = new ECKeyGenerationParameters(new ECDomainParameters(ecParameterSpec.getCurve(), ecParameterSpec.getG(), ecParameterSpec.getN(), ecParameterSpec.getH()), secureRandom);
            }
            else {
                if (ecParams instanceof java.security.spec.ECParameterSpec) {
                    final java.security.spec.ECParameterSpec ecParameterSpec2 = (java.security.spec.ECParameterSpec)ecParams;
                    this.ecParams = ecParams;
                    final ECCurve convertCurve = EC5Util.convertCurve(ecParameterSpec2.getCurve());
                    this.param = new ECKeyGenerationParameters(new ECDomainParameters(convertCurve, EC5Util.convertPoint(convertCurve, ecParameterSpec2.getGenerator()), ecParameterSpec2.getOrder(), BigInteger.valueOf(ecParameterSpec2.getCofactor())), secureRandom);
                    break Label_0076;
                }
                final boolean b = ecParams instanceof ECGenParameterSpec;
                if (b || ecParams instanceof ECNamedCurveGenParameterSpec) {
                    String s;
                    if (b) {
                        s = ((ECGenParameterSpec)ecParams).getName();
                    }
                    else {
                        s = ((ECNamedCurveGenParameterSpec)ecParams).getName();
                    }
                    this.init(new GOST3410ParameterSpec(s), secureRandom);
                    return;
                }
                if (ecParams == null && BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa() != null) {
                    final ECParameterSpec ecImplicitlyCa = BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa();
                    this.ecParams = ecParams;
                    param = new ECKeyGenerationParameters(new ECDomainParameters(ecImplicitlyCa.getCurve(), ecImplicitlyCa.getG(), ecImplicitlyCa.getN(), ecImplicitlyCa.getH()), secureRandom);
                }
                else {
                    if (ecParams == null && BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa() == null) {
                        throw new InvalidAlgorithmParameterException("null parameter passed but no implicitCA set");
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("parameter object not a ECParameterSpec: ");
                    sb.append(ecParams.getClass().getName());
                    throw new InvalidAlgorithmParameterException(sb.toString());
                }
            }
            this.param = param;
        }
        this.engine.init(this.param);
        this.initialised = true;
    }
}
