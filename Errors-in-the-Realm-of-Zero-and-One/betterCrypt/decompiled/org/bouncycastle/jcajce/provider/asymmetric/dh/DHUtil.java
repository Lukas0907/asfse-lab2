// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.dh;

import org.bouncycastle.util.Strings;
import org.bouncycastle.util.Fingerprint;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.params.DHParameters;
import java.math.BigInteger;

class DHUtil
{
    private static String generateKeyFingerprint(final BigInteger bigInteger, final DHParameters dhParameters) {
        return new Fingerprint(Arrays.concatenate(bigInteger.toByteArray(), dhParameters.getP().toByteArray(), dhParameters.getG().toByteArray())).toString();
    }
    
    static String privateKeyToString(final String str, BigInteger modPow, final DHParameters dhParameters) {
        final StringBuffer sb = new StringBuffer();
        final String lineSeparator = Strings.lineSeparator();
        modPow = dhParameters.getG().modPow(modPow, dhParameters.getP());
        sb.append(str);
        sb.append(" Private Key [");
        sb.append(generateKeyFingerprint(modPow, dhParameters));
        sb.append("]");
        sb.append(lineSeparator);
        sb.append("              Y: ");
        sb.append(modPow.toString(16));
        sb.append(lineSeparator);
        return sb.toString();
    }
    
    static String publicKeyToString(final String str, final BigInteger bigInteger, final DHParameters dhParameters) {
        final StringBuffer sb = new StringBuffer();
        final String lineSeparator = Strings.lineSeparator();
        sb.append(str);
        sb.append(" Public Key [");
        sb.append(generateKeyFingerprint(bigInteger, dhParameters));
        sb.append("]");
        sb.append(lineSeparator);
        sb.append("             Y: ");
        sb.append(bigInteger.toString(16));
        sb.append(lineSeparator);
        return sb.toString();
    }
}
