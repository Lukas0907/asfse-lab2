// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.ec;

import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.CryptoException;
import java.security.SignatureException;
import java.security.InvalidAlgorithmParameterException;
import java.security.PublicKey;
import java.security.InvalidKeyException;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.params.ParametersWithID;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.params.ParametersWithRandom;
import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;
import java.security.PrivateKey;
import java.security.spec.AlgorithmParameterSpec;
import org.bouncycastle.jcajce.util.BCJcaJceHelper;
import org.bouncycastle.crypto.signers.SM2Signer;
import org.bouncycastle.jcajce.spec.SM2ParameterSpec;
import org.bouncycastle.jcajce.util.JcaJceHelper;
import java.security.AlgorithmParameters;
import java.security.SignatureSpi;

public class GMSignatureSpi extends SignatureSpi
{
    private AlgorithmParameters engineParams;
    private final JcaJceHelper helper;
    private SM2ParameterSpec paramSpec;
    private final SM2Signer signer;
    
    GMSignatureSpi(final SM2Signer signer) {
        this.helper = new BCJcaJceHelper();
        this.signer = signer;
    }
    
    @Override
    protected Object engineGetParameter(final String s) {
        throw new UnsupportedOperationException("engineGetParameter unsupported");
    }
    
    @Override
    protected AlgorithmParameters engineGetParameters() {
        if (this.engineParams == null && this.paramSpec != null) {
            try {
                (this.engineParams = this.helper.createAlgorithmParameters("PSS")).init(this.paramSpec);
            }
            catch (Exception ex) {
                throw new RuntimeException(ex.toString());
            }
        }
        return this.engineParams;
    }
    
    @Override
    protected void engineInitSign(final PrivateKey privateKey) throws InvalidKeyException {
        CipherParameters generatePrivateKeyParameter;
        final AsymmetricKeyParameter asymmetricKeyParameter = (AsymmetricKeyParameter)(generatePrivateKeyParameter = ECUtil.generatePrivateKeyParameter(privateKey));
        if (this.appRandom != null) {
            generatePrivateKeyParameter = new ParametersWithRandom(asymmetricKeyParameter, this.appRandom);
        }
        final SM2ParameterSpec paramSpec = this.paramSpec;
        if (paramSpec != null) {
            this.signer.init(true, new ParametersWithID(generatePrivateKeyParameter, paramSpec.getID()));
            return;
        }
        this.signer.init(true, generatePrivateKeyParameter);
    }
    
    @Override
    protected void engineInitVerify(final PublicKey publicKey) throws InvalidKeyException {
        final AsymmetricKeyParameter generatePublicKeyParameter = ECUtils.generatePublicKeyParameter(publicKey);
        final SM2ParameterSpec paramSpec = this.paramSpec;
        CipherParameters cipherParameters = generatePublicKeyParameter;
        if (paramSpec != null) {
            cipherParameters = new ParametersWithID(generatePublicKeyParameter, paramSpec.getID());
        }
        this.signer.init(false, cipherParameters);
    }
    
    @Override
    protected void engineSetParameter(final String s, final Object o) {
        throw new UnsupportedOperationException("engineSetParameter unsupported");
    }
    
    @Override
    protected void engineSetParameter(final AlgorithmParameterSpec algorithmParameterSpec) throws InvalidAlgorithmParameterException {
        if (algorithmParameterSpec instanceof SM2ParameterSpec) {
            this.paramSpec = (SM2ParameterSpec)algorithmParameterSpec;
            return;
        }
        throw new InvalidAlgorithmParameterException("only SM2ParameterSpec supported");
    }
    
    @Override
    protected byte[] engineSign() throws SignatureException {
        try {
            return this.signer.generateSignature();
        }
        catch (CryptoException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("unable to create signature: ");
            sb.append(ex.getMessage());
            throw new SignatureException(sb.toString());
        }
    }
    
    @Override
    protected void engineUpdate(final byte b) throws SignatureException {
        this.signer.update(b);
    }
    
    @Override
    protected void engineUpdate(final byte[] array, final int n, final int n2) throws SignatureException {
        this.signer.update(array, n, n2);
    }
    
    @Override
    protected boolean engineVerify(final byte[] array) throws SignatureException {
        return this.signer.verifySignature(array);
    }
    
    public static class sha256WithSM2 extends GMSignatureSpi
    {
        public sha256WithSM2() {
            super(new SM2Signer(new SHA256Digest()));
        }
    }
    
    public static class sm3WithSM2 extends GMSignatureSpi
    {
        public sm3WithSM2() {
            super(new SM2Signer());
        }
    }
}
