// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.edec;

import org.bouncycastle.jcajce.provider.asymmetric.util.ECUtil;
import org.bouncycastle.jcajce.spec.XDHParameterSpec;
import org.bouncycastle.jcajce.spec.EdDSAParameterSpec;
import org.bouncycastle.jce.spec.ECNamedCurveGenParameterSpec;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.AlgorithmParameterSpec;
import java.security.InvalidParameterException;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.KeyPair;
import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.params.X25519KeyGenerationParameters;
import org.bouncycastle.crypto.params.Ed25519KeyGenerationParameters;
import org.bouncycastle.crypto.params.X448KeyGenerationParameters;
import org.bouncycastle.crypto.params.Ed448KeyGenerationParameters;
import org.bouncycastle.crypto.generators.Ed448KeyPairGenerator;
import org.bouncycastle.crypto.generators.Ed25519KeyPairGenerator;
import org.bouncycastle.crypto.generators.X448KeyPairGenerator;
import org.bouncycastle.crypto.generators.X25519KeyPairGenerator;
import org.bouncycastle.asn1.edec.EdECObjectIdentifiers;
import java.security.InvalidAlgorithmParameterException;
import java.security.SecureRandom;
import org.bouncycastle.crypto.AsymmetricCipherKeyPairGenerator;

public class KeyPairGeneratorSpi extends java.security.KeyPairGeneratorSpi
{
    private static final int Ed25519 = 1;
    private static final int Ed448 = 0;
    private static final int EdDSA = -1;
    private static final int X25519 = 3;
    private static final int X448 = 2;
    private static final int XDH = -2;
    private int algorithm;
    private AsymmetricCipherKeyPairGenerator generator;
    private boolean initialised;
    private SecureRandom secureRandom;
    
    KeyPairGeneratorSpi(final int algorithm, final AsymmetricCipherKeyPairGenerator generator) {
        this.algorithm = algorithm;
        this.generator = generator;
    }
    
    private void algorithmCheck(final int algorithm) throws InvalidAlgorithmParameterException {
        final int algorithm2 = this.algorithm;
        if (algorithm2 == algorithm) {
            return;
        }
        if (algorithm2 == 1 || algorithm2 == 0) {
            throw new InvalidAlgorithmParameterException("parameterSpec for wrong curve type");
        }
        if (algorithm2 == -1 && algorithm != 1 && algorithm != 0) {
            throw new InvalidAlgorithmParameterException("parameterSpec for wrong curve type");
        }
        final int algorithm3 = this.algorithm;
        if (algorithm3 == 3 || algorithm3 == 2) {
            throw new InvalidAlgorithmParameterException("parameterSpec for wrong curve type");
        }
        if (algorithm3 == -2 && algorithm != 3 && algorithm != 2) {
            throw new InvalidAlgorithmParameterException("parameterSpec for wrong curve type");
        }
        this.algorithm = algorithm;
    }
    
    private void initializeGenerator(final String s) throws InvalidAlgorithmParameterException {
        int n;
        AsymmetricCipherKeyPairGenerator generator;
        if (!s.equalsIgnoreCase("Ed448") && !s.equals(EdECObjectIdentifiers.id_Ed448.getId())) {
            if (!s.equalsIgnoreCase("Ed25519") && !s.equals(EdECObjectIdentifiers.id_Ed25519.getId())) {
                if (!s.equalsIgnoreCase("X448") && !s.equals(EdECObjectIdentifiers.id_X448.getId())) {
                    if (!s.equalsIgnoreCase("X25519") && !s.equals(EdECObjectIdentifiers.id_X25519.getId())) {
                        return;
                    }
                    n = 3;
                    this.algorithmCheck(3);
                    generator = new X25519KeyPairGenerator();
                }
                else {
                    n = 2;
                    this.algorithmCheck(2);
                    generator = new X448KeyPairGenerator();
                }
            }
            else {
                n = 1;
                this.algorithmCheck(1);
                generator = new Ed25519KeyPairGenerator();
            }
        }
        else {
            n = 0;
            this.algorithmCheck(0);
            generator = new Ed448KeyPairGenerator();
        }
        this.generator = generator;
        this.setupGenerator(n);
    }
    
    private void setupGenerator(final int n) {
        this.initialised = true;
        if (this.secureRandom == null) {
            this.secureRandom = new SecureRandom();
        }
        AsymmetricCipherKeyPairGenerator asymmetricCipherKeyPairGenerator = null;
        KeyGenerationParameters keyGenerationParameters = null;
        Label_0131: {
            Label_0114: {
                if (n != -2) {
                    if (n != -1) {
                        if (n == 0) {
                            asymmetricCipherKeyPairGenerator = this.generator;
                            keyGenerationParameters = new Ed448KeyGenerationParameters(this.secureRandom);
                            break Label_0131;
                        }
                        if (n != 1) {
                            if (n == 2) {
                                asymmetricCipherKeyPairGenerator = this.generator;
                                keyGenerationParameters = new X448KeyGenerationParameters(this.secureRandom);
                                break Label_0131;
                            }
                            if (n != 3) {
                                return;
                            }
                            break Label_0114;
                        }
                    }
                    asymmetricCipherKeyPairGenerator = this.generator;
                    keyGenerationParameters = new Ed25519KeyGenerationParameters(this.secureRandom);
                    break Label_0131;
                }
            }
            asymmetricCipherKeyPairGenerator = this.generator;
            keyGenerationParameters = new X25519KeyGenerationParameters(this.secureRandom);
        }
        asymmetricCipherKeyPairGenerator.init(keyGenerationParameters);
    }
    
    @Override
    public KeyPair generateKeyPair() {
        if (this.generator == null) {
            throw new IllegalStateException("generator not correctly initialized");
        }
        if (!this.initialised) {
            this.setupGenerator(this.algorithm);
        }
        final AsymmetricCipherKeyPair generateKeyPair = this.generator.generateKeyPair();
        final int algorithm = this.algorithm;
        if (algorithm == 0) {
            return new KeyPair(new BCEdDSAPublicKey(generateKeyPair.getPublic()), new BCEdDSAPrivateKey(generateKeyPair.getPrivate()));
        }
        if (algorithm == 1) {
            return new KeyPair(new BCEdDSAPublicKey(generateKeyPair.getPublic()), new BCEdDSAPrivateKey(generateKeyPair.getPrivate()));
        }
        if (algorithm == 2) {
            return new KeyPair(new BCXDHPublicKey(generateKeyPair.getPublic()), new BCXDHPrivateKey(generateKeyPair.getPrivate()));
        }
        if (algorithm == 3) {
            return new KeyPair(new BCXDHPublicKey(generateKeyPair.getPublic()), new BCXDHPrivateKey(generateKeyPair.getPrivate()));
        }
        throw new IllegalStateException("generator not correctly initialized");
    }
    
    @Override
    public void initialize(int n, final SecureRandom secureRandom) {
        this.secureRandom = secureRandom;
        if (n != 255 && n != 256) {
            if (n != 448) {
                throw new InvalidParameterException("unknown key size");
            }
            final int algorithm = this.algorithm;
            final int n2 = n = 2;
            if (algorithm != -2) {
                if (algorithm == -1 || algorithm == 0) {
                    this.setupGenerator(0);
                    return;
                }
                if (algorithm != 2) {
                    throw new InvalidParameterException("key size not configurable");
                }
                n = n2;
            }
        }
        else {
            final int algorithm2 = this.algorithm;
            final int n3 = n = 3;
            if (algorithm2 != -2) {
                if (algorithm2 == -1 || algorithm2 == 1) {
                    this.setupGenerator(1);
                    return;
                }
                if (algorithm2 != 3) {
                    throw new InvalidParameterException("key size not configurable");
                }
                n = n3;
            }
        }
        this.setupGenerator(n);
    }
    
    @Override
    public void initialize(final AlgorithmParameterSpec obj, final SecureRandom secureRandom) throws InvalidAlgorithmParameterException {
        this.secureRandom = secureRandom;
        String s;
        if (obj instanceof ECGenParameterSpec) {
            s = ((ECGenParameterSpec)obj).getName();
        }
        else if (obj instanceof ECNamedCurveGenParameterSpec) {
            s = ((ECNamedCurveGenParameterSpec)obj).getName();
        }
        else if (obj instanceof EdDSAParameterSpec) {
            s = ((EdDSAParameterSpec)obj).getCurveName();
        }
        else if (obj instanceof XDHParameterSpec) {
            s = ((XDHParameterSpec)obj).getCurveName();
        }
        else {
            final String name = ECUtil.getNameFrom(obj);
            if (name != null) {
                this.initializeGenerator(name);
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("invalid parameterSpec: ");
            sb.append(obj);
            throw new InvalidAlgorithmParameterException(sb.toString());
        }
        this.initializeGenerator(s);
    }
    
    public static final class Ed25519 extends KeyPairGeneratorSpi
    {
        public Ed25519() {
            super(1, new Ed25519KeyPairGenerator());
        }
    }
    
    public static final class Ed448 extends KeyPairGeneratorSpi
    {
        public Ed448() {
            super(0, new Ed448KeyPairGenerator());
        }
    }
    
    public static final class EdDSA extends KeyPairGeneratorSpi
    {
        public EdDSA() {
            super(-1, null);
        }
    }
    
    public static final class X25519 extends KeyPairGeneratorSpi
    {
        public X25519() {
            super(3, new X25519KeyPairGenerator());
        }
    }
    
    public static final class X448 extends KeyPairGeneratorSpi
    {
        public X448() {
            super(2, new X448KeyPairGenerator());
        }
    }
    
    public static final class XDH extends KeyPairGeneratorSpi
    {
        public XDH() {
            super(-2, null);
        }
    }
}
