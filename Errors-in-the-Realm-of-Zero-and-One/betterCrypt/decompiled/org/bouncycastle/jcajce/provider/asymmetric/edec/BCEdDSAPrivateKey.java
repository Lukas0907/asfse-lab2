// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.edec;

import org.bouncycastle.crypto.util.PrivateKeyInfoFactory;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.util.Arrays;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.params.Ed448PrivateKeyParameters;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.edec.EdECObjectIdentifiers;
import java.io.IOException;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import java.security.PrivateKey;
import org.bouncycastle.jcajce.interfaces.EdDSAKey;

public class BCEdDSAPrivateKey implements EdDSAKey, PrivateKey
{
    static final long serialVersionUID = 1L;
    private final byte[] attributes;
    private transient AsymmetricKeyParameter eddsaPrivateKey;
    private final boolean hasPublicKey;
    
    BCEdDSAPrivateKey(final PrivateKeyInfo privateKeyInfo) throws IOException {
        this.hasPublicKey = privateKeyInfo.hasPublicKey();
        byte[] encoded;
        if (privateKeyInfo.getAttributes() != null) {
            encoded = privateKeyInfo.getAttributes().getEncoded();
        }
        else {
            encoded = null;
        }
        this.attributes = encoded;
        this.populateFromPrivateKeyInfo(privateKeyInfo);
    }
    
    BCEdDSAPrivateKey(final AsymmetricKeyParameter eddsaPrivateKey) {
        this.hasPublicKey = true;
        this.attributes = null;
        this.eddsaPrivateKey = eddsaPrivateKey;
    }
    
    private void populateFromPrivateKeyInfo(final PrivateKeyInfo privateKeyInfo) throws IOException {
        final ASN1Encodable privateKey = privateKeyInfo.parsePrivateKey();
        AsymmetricKeyParameter eddsaPrivateKey;
        if (EdECObjectIdentifiers.id_Ed448.equals(privateKeyInfo.getPrivateKeyAlgorithm().getAlgorithm())) {
            eddsaPrivateKey = new Ed448PrivateKeyParameters(ASN1OctetString.getInstance(privateKey).getOctets(), 0);
        }
        else {
            eddsaPrivateKey = new Ed25519PrivateKeyParameters(ASN1OctetString.getInstance(privateKey).getOctets(), 0);
        }
        this.eddsaPrivateKey = eddsaPrivateKey;
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.populateFromPrivateKeyInfo(PrivateKeyInfo.getInstance(objectInputStream.readObject()));
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(this.getEncoded());
    }
    
    AsymmetricKeyParameter engineGetKeyParameters() {
        return this.eddsaPrivateKey;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o == this || (o instanceof BCEdDSAPrivateKey && Arrays.areEqual(((BCEdDSAPrivateKey)o).getEncoded(), this.getEncoded()));
    }
    
    @Override
    public String getAlgorithm() {
        if (this.eddsaPrivateKey instanceof Ed448PrivateKeyParameters) {
            return "Ed448";
        }
        return "Ed25519";
    }
    
    @Override
    public byte[] getEncoded() {
        try {
            final ASN1Set instance = ASN1Set.getInstance(this.attributes);
            final PrivateKeyInfo privateKeyInfo = PrivateKeyInfoFactory.createPrivateKeyInfo(this.eddsaPrivateKey, instance);
            if (this.hasPublicKey) {
                return privateKeyInfo.getEncoded();
            }
            return new PrivateKeyInfo(privateKeyInfo.getPrivateKeyAlgorithm(), privateKeyInfo.parsePrivateKey(), instance).getEncoded();
        }
        catch (IOException ex) {
            return null;
        }
    }
    
    @Override
    public String getFormat() {
        return "PKCS#8";
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.getEncoded());
    }
    
    @Override
    public String toString() {
        final AsymmetricKeyParameter eddsaPrivateKey = this.eddsaPrivateKey;
        AsymmetricKeyParameter asymmetricKeyParameter;
        if (eddsaPrivateKey instanceof Ed448PrivateKeyParameters) {
            asymmetricKeyParameter = ((Ed448PrivateKeyParameters)eddsaPrivateKey).generatePublicKey();
        }
        else {
            asymmetricKeyParameter = ((Ed25519PrivateKeyParameters)eddsaPrivateKey).generatePublicKey();
        }
        return Utils.keyToString("Private Key", this.getAlgorithm(), asymmetricKeyParameter);
    }
}
