// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.rsa;

import org.bouncycastle.util.Strings;
import org.bouncycastle.jcajce.provider.asymmetric.util.KeyUtil;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import java.io.ObjectOutputStream;
import org.bouncycastle.jcajce.provider.asymmetric.util.PKCS12BagAttributeCarrierImpl;
import java.io.ObjectInputStream;
import java.io.IOException;
import org.bouncycastle.asn1.pkcs.RSAPrivateKey;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import java.security.spec.RSAPrivateCrtKeySpec;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;
import java.math.BigInteger;
import java.security.interfaces.RSAPrivateCrtKey;

public class BCRSAPrivateCrtKey extends BCRSAPrivateKey implements RSAPrivateCrtKey
{
    static final long serialVersionUID = 7834723820638524718L;
    private BigInteger crtCoefficient;
    private BigInteger primeExponentP;
    private BigInteger primeExponentQ;
    private BigInteger primeP;
    private BigInteger primeQ;
    private BigInteger publicExponent;
    
    BCRSAPrivateCrtKey(final RSAPrivateCrtKey rsaPrivateCrtKey) {
        super(new RSAPrivateCrtKeyParameters(rsaPrivateCrtKey.getModulus(), rsaPrivateCrtKey.getPublicExponent(), rsaPrivateCrtKey.getPrivateExponent(), rsaPrivateCrtKey.getPrimeP(), rsaPrivateCrtKey.getPrimeQ(), rsaPrivateCrtKey.getPrimeExponentP(), rsaPrivateCrtKey.getPrimeExponentQ(), rsaPrivateCrtKey.getCrtCoefficient()));
        this.modulus = rsaPrivateCrtKey.getModulus();
        this.publicExponent = rsaPrivateCrtKey.getPublicExponent();
        this.privateExponent = rsaPrivateCrtKey.getPrivateExponent();
        this.primeP = rsaPrivateCrtKey.getPrimeP();
        this.primeQ = rsaPrivateCrtKey.getPrimeQ();
        this.primeExponentP = rsaPrivateCrtKey.getPrimeExponentP();
        this.primeExponentQ = rsaPrivateCrtKey.getPrimeExponentQ();
        this.crtCoefficient = rsaPrivateCrtKey.getCrtCoefficient();
    }
    
    BCRSAPrivateCrtKey(final RSAPrivateCrtKeySpec rsaPrivateCrtKeySpec) {
        super(new RSAPrivateCrtKeyParameters(rsaPrivateCrtKeySpec.getModulus(), rsaPrivateCrtKeySpec.getPublicExponent(), rsaPrivateCrtKeySpec.getPrivateExponent(), rsaPrivateCrtKeySpec.getPrimeP(), rsaPrivateCrtKeySpec.getPrimeQ(), rsaPrivateCrtKeySpec.getPrimeExponentP(), rsaPrivateCrtKeySpec.getPrimeExponentQ(), rsaPrivateCrtKeySpec.getCrtCoefficient()));
        this.modulus = rsaPrivateCrtKeySpec.getModulus();
        this.publicExponent = rsaPrivateCrtKeySpec.getPublicExponent();
        this.privateExponent = rsaPrivateCrtKeySpec.getPrivateExponent();
        this.primeP = rsaPrivateCrtKeySpec.getPrimeP();
        this.primeQ = rsaPrivateCrtKeySpec.getPrimeQ();
        this.primeExponentP = rsaPrivateCrtKeySpec.getPrimeExponentP();
        this.primeExponentQ = rsaPrivateCrtKeySpec.getPrimeExponentQ();
        this.crtCoefficient = rsaPrivateCrtKeySpec.getCrtCoefficient();
    }
    
    BCRSAPrivateCrtKey(final PrivateKeyInfo privateKeyInfo) throws IOException {
        this(org.bouncycastle.asn1.pkcs.RSAPrivateKey.getInstance(privateKeyInfo.parsePrivateKey()));
    }
    
    BCRSAPrivateCrtKey(final org.bouncycastle.asn1.pkcs.RSAPrivateKey rsaPrivateKey) {
        super(new RSAPrivateCrtKeyParameters(rsaPrivateKey.getModulus(), rsaPrivateKey.getPublicExponent(), rsaPrivateKey.getPrivateExponent(), rsaPrivateKey.getPrime1(), rsaPrivateKey.getPrime2(), rsaPrivateKey.getExponent1(), rsaPrivateKey.getExponent2(), rsaPrivateKey.getCoefficient()));
        this.modulus = rsaPrivateKey.getModulus();
        this.publicExponent = rsaPrivateKey.getPublicExponent();
        this.privateExponent = rsaPrivateKey.getPrivateExponent();
        this.primeP = rsaPrivateKey.getPrime1();
        this.primeQ = rsaPrivateKey.getPrime2();
        this.primeExponentP = rsaPrivateKey.getExponent1();
        this.primeExponentQ = rsaPrivateKey.getExponent2();
        this.crtCoefficient = rsaPrivateKey.getCoefficient();
    }
    
    BCRSAPrivateCrtKey(final RSAPrivateCrtKeyParameters rsaPrivateCrtKeyParameters) {
        super(rsaPrivateCrtKeyParameters);
        this.publicExponent = rsaPrivateCrtKeyParameters.getPublicExponent();
        this.primeP = rsaPrivateCrtKeyParameters.getP();
        this.primeQ = rsaPrivateCrtKeyParameters.getQ();
        this.primeExponentP = rsaPrivateCrtKeyParameters.getDP();
        this.primeExponentQ = rsaPrivateCrtKeyParameters.getDQ();
        this.crtCoefficient = rsaPrivateCrtKeyParameters.getQInv();
    }
    
    private void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        this.attrCarrier = new PKCS12BagAttributeCarrierImpl();
        this.rsaPrivateKey = new RSAPrivateCrtKeyParameters(this.getModulus(), this.getPublicExponent(), this.getPrivateExponent(), this.getPrimeP(), this.getPrimeQ(), this.getPrimeExponentP(), this.getPrimeExponentQ(), this.getCrtCoefficient());
    }
    
    private void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof RSAPrivateCrtKey)) {
            return false;
        }
        final RSAPrivateCrtKey rsaPrivateCrtKey = (RSAPrivateCrtKey)o;
        return this.getModulus().equals(rsaPrivateCrtKey.getModulus()) && this.getPublicExponent().equals(rsaPrivateCrtKey.getPublicExponent()) && this.getPrivateExponent().equals(rsaPrivateCrtKey.getPrivateExponent()) && this.getPrimeP().equals(rsaPrivateCrtKey.getPrimeP()) && this.getPrimeQ().equals(rsaPrivateCrtKey.getPrimeQ()) && this.getPrimeExponentP().equals(rsaPrivateCrtKey.getPrimeExponentP()) && this.getPrimeExponentQ().equals(rsaPrivateCrtKey.getPrimeExponentQ()) && this.getCrtCoefficient().equals(rsaPrivateCrtKey.getCrtCoefficient());
    }
    
    @Override
    public BigInteger getCrtCoefficient() {
        return this.crtCoefficient;
    }
    
    @Override
    public byte[] getEncoded() {
        return KeyUtil.getEncodedPrivateKeyInfo(new AlgorithmIdentifier(PKCSObjectIdentifiers.rsaEncryption, DERNull.INSTANCE), new org.bouncycastle.asn1.pkcs.RSAPrivateKey(this.getModulus(), this.getPublicExponent(), this.getPrivateExponent(), this.getPrimeP(), this.getPrimeQ(), this.getPrimeExponentP(), this.getPrimeExponentQ(), this.getCrtCoefficient()));
    }
    
    @Override
    public String getFormat() {
        return "PKCS#8";
    }
    
    @Override
    public BigInteger getPrimeExponentP() {
        return this.primeExponentP;
    }
    
    @Override
    public BigInteger getPrimeExponentQ() {
        return this.primeExponentQ;
    }
    
    @Override
    public BigInteger getPrimeP() {
        return this.primeP;
    }
    
    @Override
    public BigInteger getPrimeQ() {
        return this.primeQ;
    }
    
    @Override
    public BigInteger getPublicExponent() {
        return this.publicExponent;
    }
    
    @Override
    public int hashCode() {
        return this.getModulus().hashCode() ^ this.getPublicExponent().hashCode() ^ this.getPrivateExponent().hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        final String lineSeparator = Strings.lineSeparator();
        sb.append("RSA Private CRT Key [");
        sb.append(RSAUtil.generateKeyFingerprint(this.getModulus()));
        sb.append("]");
        sb.append(",[");
        sb.append(RSAUtil.generateExponentFingerprint(this.getPublicExponent()));
        sb.append("]");
        sb.append(lineSeparator);
        sb.append("             modulus: ");
        sb.append(this.getModulus().toString(16));
        sb.append(lineSeparator);
        sb.append("     public exponent: ");
        sb.append(this.getPublicExponent().toString(16));
        sb.append(lineSeparator);
        return sb.toString();
    }
}
