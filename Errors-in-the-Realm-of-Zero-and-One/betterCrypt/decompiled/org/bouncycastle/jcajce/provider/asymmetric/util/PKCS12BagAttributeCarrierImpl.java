// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.util;

import org.bouncycastle.asn1.ASN1Primitive;
import java.io.OutputStream;
import org.bouncycastle.asn1.ASN1OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1InputStream;
import java.io.ObjectInputStream;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.util.Vector;
import java.util.Hashtable;
import org.bouncycastle.jce.interfaces.PKCS12BagAttributeCarrier;

public class PKCS12BagAttributeCarrierImpl implements PKCS12BagAttributeCarrier
{
    private Hashtable pkcs12Attributes;
    private Vector pkcs12Ordering;
    
    public PKCS12BagAttributeCarrierImpl() {
        this(new Hashtable(), new Vector());
    }
    
    PKCS12BagAttributeCarrierImpl(final Hashtable pkcs12Attributes, final Vector pkcs12Ordering) {
        this.pkcs12Attributes = pkcs12Attributes;
        this.pkcs12Ordering = pkcs12Ordering;
    }
    
    Hashtable getAttributes() {
        return this.pkcs12Attributes;
    }
    
    @Override
    public ASN1Encodable getBagAttribute(final ASN1ObjectIdentifier key) {
        return this.pkcs12Attributes.get(key);
    }
    
    @Override
    public Enumeration getBagAttributeKeys() {
        return this.pkcs12Ordering.elements();
    }
    
    Vector getOrdering() {
        return this.pkcs12Ordering;
    }
    
    public void readObject(final ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        final Object object = objectInputStream.readObject();
        if (object instanceof Hashtable) {
            this.pkcs12Attributes = (Hashtable)object;
            this.pkcs12Ordering = (Vector)objectInputStream.readObject();
            return;
        }
        final ASN1InputStream asn1InputStream = new ASN1InputStream((byte[])object);
        while (true) {
            final ASN1ObjectIdentifier asn1ObjectIdentifier = (ASN1ObjectIdentifier)asn1InputStream.readObject();
            if (asn1ObjectIdentifier == null) {
                break;
            }
            this.setBagAttribute(asn1ObjectIdentifier, asn1InputStream.readObject());
        }
    }
    
    @Override
    public void setBagAttribute(final ASN1ObjectIdentifier asn1ObjectIdentifier, final ASN1Encodable asn1Encodable) {
        if (this.pkcs12Attributes.containsKey(asn1ObjectIdentifier)) {
            this.pkcs12Attributes.put(asn1ObjectIdentifier, asn1Encodable);
            return;
        }
        this.pkcs12Attributes.put(asn1ObjectIdentifier, asn1Encodable);
        this.pkcs12Ordering.addElement(asn1ObjectIdentifier);
    }
    
    int size() {
        return this.pkcs12Ordering.size();
    }
    
    public void writeObject(final ObjectOutputStream objectOutputStream) throws IOException {
        if (this.pkcs12Ordering.size() == 0) {
            objectOutputStream.writeObject(new Hashtable());
            objectOutputStream.writeObject(new Vector());
            return;
        }
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final ASN1OutputStream create = ASN1OutputStream.create(byteArrayOutputStream);
        final Enumeration bagAttributeKeys = this.getBagAttributeKeys();
        while (bagAttributeKeys.hasMoreElements()) {
            final ASN1ObjectIdentifier instance = ASN1ObjectIdentifier.getInstance(bagAttributeKeys.nextElement());
            create.writeObject(instance);
            create.writeObject((ASN1Encodable)this.pkcs12Attributes.get(instance));
        }
        objectOutputStream.writeObject(byteArrayOutputStream.toByteArray());
    }
}
