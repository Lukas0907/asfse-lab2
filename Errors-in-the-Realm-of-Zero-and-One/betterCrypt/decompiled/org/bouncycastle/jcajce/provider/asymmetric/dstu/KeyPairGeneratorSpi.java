// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.dstu;

import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.jce.spec.ECNamedCurveSpec;
import org.bouncycastle.asn1.ua.DSTU4145NamedCurves;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECNamedCurveGenParameterSpec;
import org.bouncycastle.crypto.params.DSTU4145Parameters;
import org.bouncycastle.crypto.params.ECDomainParameters;
import java.math.BigInteger;
import org.bouncycastle.jcajce.spec.DSTU4145ParameterSpec;
import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidParameterException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.ECGenParameterSpec;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import java.security.KeyPair;
import org.bouncycastle.crypto.generators.DSTU4145KeyPairGenerator;
import java.security.SecureRandom;
import org.bouncycastle.crypto.params.ECKeyGenerationParameters;
import org.bouncycastle.crypto.generators.ECKeyPairGenerator;
import java.security.KeyPairGenerator;

public class KeyPairGeneratorSpi extends KeyPairGenerator
{
    String algorithm;
    Object ecParams;
    ECKeyPairGenerator engine;
    boolean initialised;
    ECKeyGenerationParameters param;
    SecureRandom random;
    
    public KeyPairGeneratorSpi() {
        super("DSTU4145");
        this.ecParams = null;
        this.engine = new DSTU4145KeyPairGenerator();
        this.algorithm = "DSTU4145";
        this.random = null;
        this.initialised = false;
    }
    
    @Override
    public KeyPair generateKeyPair() {
        if (!this.initialised) {
            throw new IllegalStateException("DSTU Key Pair Generator not initialised");
        }
        final AsymmetricCipherKeyPair generateKeyPair = this.engine.generateKeyPair();
        final ECPublicKeyParameters ecPublicKeyParameters = (ECPublicKeyParameters)generateKeyPair.getPublic();
        final ECPrivateKeyParameters ecPrivateKeyParameters = (ECPrivateKeyParameters)generateKeyPair.getPrivate();
        final Object ecParams = this.ecParams;
        if (ecParams instanceof ECParameterSpec) {
            final ECParameterSpec ecParameterSpec = (ECParameterSpec)ecParams;
            final BCDSTU4145PublicKey publicKey = new BCDSTU4145PublicKey(this.algorithm, ecPublicKeyParameters, ecParameterSpec);
            return new KeyPair(publicKey, new BCDSTU4145PrivateKey(this.algorithm, ecPrivateKeyParameters, publicKey, ecParameterSpec));
        }
        if (ecParams == null) {
            return new KeyPair(new BCDSTU4145PublicKey(this.algorithm, ecPublicKeyParameters), new BCDSTU4145PrivateKey(this.algorithm, ecPrivateKeyParameters));
        }
        final java.security.spec.ECParameterSpec ecParameterSpec2 = (java.security.spec.ECParameterSpec)ecParams;
        final BCDSTU4145PublicKey publicKey2 = new BCDSTU4145PublicKey(this.algorithm, ecPublicKeyParameters, ecParameterSpec2);
        return new KeyPair(publicKey2, new BCDSTU4145PrivateKey(this.algorithm, ecPrivateKeyParameters, publicKey2, ecParameterSpec2));
    }
    
    @Override
    public void initialize(final int n, final SecureRandom random) {
        this.random = random;
        final Object ecParams = this.ecParams;
        Label_0034: {
            if (ecParams == null) {
                break Label_0034;
            }
            while (true) {
                while (true) {
                    try {
                        this.initialize((AlgorithmParameterSpec)ecParams, random);
                        return;
                        throw new InvalidParameterException("key size not configurable.");
                        throw new InvalidParameterException("unknown key size.");
                    }
                    catch (InvalidAlgorithmParameterException ex) {}
                    continue;
                }
            }
        }
    }
    
    @Override
    public void initialize(final AlgorithmParameterSpec ecParams, final SecureRandom secureRandom) throws InvalidAlgorithmParameterException {
        Label_0059: {
            ECKeyGenerationParameters param2 = null;
            Label_0054: {
                if (!(ecParams instanceof ECParameterSpec)) {
                    ECKeyGenerationParameters param;
                    if (ecParams instanceof java.security.spec.ECParameterSpec) {
                        final java.security.spec.ECParameterSpec ecParameterSpec = (java.security.spec.ECParameterSpec)ecParams;
                        this.ecParams = ecParams;
                        final ECCurve convertCurve = EC5Util.convertCurve(ecParameterSpec.getCurve());
                        final ECPoint convertPoint = EC5Util.convertPoint(convertCurve, ecParameterSpec.getGenerator());
                        if (ecParameterSpec instanceof DSTU4145ParameterSpec) {
                            this.param = new ECKeyGenerationParameters(new DSTU4145Parameters(new ECDomainParameters(convertCurve, convertPoint, ecParameterSpec.getOrder(), BigInteger.valueOf(ecParameterSpec.getCofactor())), ((DSTU4145ParameterSpec)ecParameterSpec).getDKE()), secureRandom);
                            break Label_0059;
                        }
                        param = new ECKeyGenerationParameters(new ECDomainParameters(convertCurve, convertPoint, ecParameterSpec.getOrder(), BigInteger.valueOf(ecParameterSpec.getCofactor())), secureRandom);
                    }
                    else {
                        final boolean b = ecParams instanceof ECGenParameterSpec;
                        if (!b && !(ecParams instanceof ECNamedCurveGenParameterSpec)) {
                            if (ecParams == null && BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa() != null) {
                                final ECParameterSpec ecImplicitlyCa = BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa();
                                this.ecParams = ecParams;
                                param2 = new ECKeyGenerationParameters(new ECDomainParameters(ecImplicitlyCa.getCurve(), ecImplicitlyCa.getG(), ecImplicitlyCa.getN(), ecImplicitlyCa.getH()), secureRandom);
                                break Label_0054;
                            }
                            if (ecParams == null && BouncyCastleProvider.CONFIGURATION.getEcImplicitlyCa() == null) {
                                throw new InvalidAlgorithmParameterException("null parameter passed but no implicitCA set");
                            }
                            final StringBuilder sb = new StringBuilder();
                            sb.append("parameter object not a ECParameterSpec: ");
                            sb.append(ecParams.getClass().getName());
                            throw new InvalidAlgorithmParameterException(sb.toString());
                        }
                        else {
                            String str;
                            if (b) {
                                str = ((ECGenParameterSpec)ecParams).getName();
                            }
                            else {
                                str = ((ECNamedCurveGenParameterSpec)ecParams).getName();
                            }
                            final ECDomainParameters byOID = DSTU4145NamedCurves.getByOID(new ASN1ObjectIdentifier(str));
                            if (byOID == null) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("unknown curve name: ");
                                sb2.append(str);
                                throw new InvalidAlgorithmParameterException(sb2.toString());
                            }
                            this.ecParams = new ECNamedCurveSpec(str, byOID.getCurve(), byOID.getG(), byOID.getN(), byOID.getH(), byOID.getSeed());
                            final java.security.spec.ECParameterSpec ecParameterSpec2 = (java.security.spec.ECParameterSpec)this.ecParams;
                            final ECCurve convertCurve2 = EC5Util.convertCurve(ecParameterSpec2.getCurve());
                            param = new ECKeyGenerationParameters(new ECDomainParameters(convertCurve2, EC5Util.convertPoint(convertCurve2, ecParameterSpec2.getGenerator()), ecParameterSpec2.getOrder(), BigInteger.valueOf(ecParameterSpec2.getCofactor())), secureRandom);
                        }
                    }
                    this.param = param;
                    break Label_0059;
                }
                final ECParameterSpec ecParameterSpec3 = (ECParameterSpec)ecParams;
                this.ecParams = ecParams;
                param2 = new ECKeyGenerationParameters(new ECDomainParameters(ecParameterSpec3.getCurve(), ecParameterSpec3.getG(), ecParameterSpec3.getN(), ecParameterSpec3.getH()), secureRandom);
            }
            this.param = param2;
        }
        this.engine.init(this.param);
        this.initialised = true;
    }
}
