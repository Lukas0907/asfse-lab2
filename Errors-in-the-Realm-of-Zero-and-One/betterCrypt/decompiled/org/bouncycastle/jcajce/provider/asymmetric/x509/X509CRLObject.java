// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.asymmetric.x509;

import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.x509.IssuingDistributionPoint;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.ASN1Encodable;
import java.security.cert.CRLException;
import org.bouncycastle.asn1.x509.CertificateList;
import org.bouncycastle.jcajce.util.JcaJceHelper;

class X509CRLObject extends X509CRLImpl
{
    private final Object cacheLock;
    private volatile int hashValue;
    private volatile boolean hashValueSet;
    private X509CRLInternal internalCRLValue;
    
    X509CRLObject(final JcaJceHelper jcaJceHelper, final CertificateList list) throws CRLException {
        super(jcaJceHelper, list, createSigAlgName(list), createSigAlgParams(list), isIndirectCRL(list));
        this.cacheLock = new Object();
    }
    
    private static String createSigAlgName(final CertificateList list) throws CRLException {
        try {
            return X509SignatureUtil.getSignatureName(list.getSignatureAlgorithm());
        }
        catch (Exception obj) {
            final StringBuilder sb = new StringBuilder();
            sb.append("CRL contents invalid: ");
            sb.append(obj);
            throw new CRLException(sb.toString());
        }
    }
    
    private static byte[] createSigAlgParams(final CertificateList list) throws CRLException {
        try {
            final ASN1Encodable parameters = list.getSignatureAlgorithm().getParameters();
            if (parameters == null) {
                return null;
            }
            return parameters.toASN1Primitive().getEncoded("DER");
        }
        catch (Exception obj) {
            final StringBuilder sb = new StringBuilder();
            sb.append("CRL contents invalid: ");
            sb.append(obj);
            throw new CRLException(sb.toString());
        }
    }
    
    private X509CRLInternal getInternalCRL() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CRLObject.cacheLock:Ljava/lang/Object;
        //     4: astore_1       
        //     5: aload_1        
        //     6: monitorenter   
        //     7: aload_0        
        //     8: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CRLObject.internalCRLValue:Lorg/bouncycastle/jcajce/provider/asymmetric/x509/X509CRLInternal;
        //    11: ifnull          23
        //    14: aload_0        
        //    15: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CRLObject.internalCRLValue:Lorg/bouncycastle/jcajce/provider/asymmetric/x509/X509CRLInternal;
        //    18: astore_2       
        //    19: aload_1        
        //    20: monitorexit    
        //    21: aload_2        
        //    22: areturn        
        //    23: aload_1        
        //    24: monitorexit    
        //    25: aload_0        
        //    26: invokevirtual   org/bouncycastle/jcajce/provider/asymmetric/x509/X509CRLObject.getEncoded:()[B
        //    29: astore_1       
        //    30: goto            38
        //    33: aconst_null    
        //    34: astore_1       
        //    35: goto            30
        //    38: new             Lorg/bouncycastle/jcajce/provider/asymmetric/x509/X509CRLInternal;
        //    41: dup            
        //    42: aload_0        
        //    43: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CRLObject.bcHelper:Lorg/bouncycastle/jcajce/util/JcaJceHelper;
        //    46: aload_0        
        //    47: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CRLObject.c:Lorg/bouncycastle/asn1/x509/CertificateList;
        //    50: aload_0        
        //    51: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CRLObject.sigAlgName:Ljava/lang/String;
        //    54: aload_0        
        //    55: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CRLObject.sigAlgParams:[B
        //    58: aload_0        
        //    59: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CRLObject.isIndirect:Z
        //    62: aload_1        
        //    63: invokespecial   org/bouncycastle/jcajce/provider/asymmetric/x509/X509CRLInternal.<init>:(Lorg/bouncycastle/jcajce/util/JcaJceHelper;Lorg/bouncycastle/asn1/x509/CertificateList;Ljava/lang/String;[BZ[B)V
        //    66: astore_2       
        //    67: aload_0        
        //    68: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CRLObject.cacheLock:Ljava/lang/Object;
        //    71: astore_1       
        //    72: aload_1        
        //    73: monitorenter   
        //    74: aload_0        
        //    75: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CRLObject.internalCRLValue:Lorg/bouncycastle/jcajce/provider/asymmetric/x509/X509CRLInternal;
        //    78: ifnonnull       86
        //    81: aload_0        
        //    82: aload_2        
        //    83: putfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CRLObject.internalCRLValue:Lorg/bouncycastle/jcajce/provider/asymmetric/x509/X509CRLInternal;
        //    86: aload_0        
        //    87: getfield        org/bouncycastle/jcajce/provider/asymmetric/x509/X509CRLObject.internalCRLValue:Lorg/bouncycastle/jcajce/provider/asymmetric/x509/X509CRLInternal;
        //    90: astore_2       
        //    91: aload_1        
        //    92: monitorexit    
        //    93: aload_2        
        //    94: areturn        
        //    95: astore_2       
        //    96: aload_1        
        //    97: monitorexit    
        //    98: aload_2        
        //    99: athrow         
        //   100: astore_2       
        //   101: aload_1        
        //   102: monitorexit    
        //   103: aload_2        
        //   104: athrow         
        //   105: astore_1       
        //   106: goto            33
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                             
        //  -----  -----  -----  -----  ---------------------------------
        //  7      21     100    105    Any
        //  23     25     100    105    Any
        //  25     30     105    38     Ljava/security/cert/CRLException;
        //  74     86     95     100    Any
        //  86     93     95     100    Any
        //  96     98     95     100    Any
        //  101    103    100    105    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0030:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static boolean isIndirectCRL(final CertificateList list) throws CRLException {
        try {
            final byte[] extensionOctets = X509CRLImpl.getExtensionOctets(list, Extension.issuingDistributionPoint.getId());
            return extensionOctets != null && IssuingDistributionPoint.getInstance(extensionOctets).isIndirectCRL();
        }
        catch (Exception ex) {
            throw new ExtCRLException("Exception reading IssuingDistributionPoint", ex);
        }
    }
    
    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (other instanceof X509CRLObject) {
            final X509CRLObject x509CRLObject = (X509CRLObject)other;
            if (this.hashValueSet && x509CRLObject.hashValueSet) {
                if (this.hashValue != x509CRLObject.hashValue) {
                    return false;
                }
            }
            else if (this.internalCRLValue == null || x509CRLObject.internalCRLValue == null) {
                final DERBitString signature = this.c.getSignature();
                if (signature != null && !signature.equals(x509CRLObject.c.getSignature())) {
                    return false;
                }
            }
        }
        return this.getInternalCRL().equals(other);
    }
    
    @Override
    public int hashCode() {
        if (!this.hashValueSet) {
            this.hashValue = this.getInternalCRL().hashCode();
            this.hashValueSet = true;
        }
        return this.hashValue;
    }
}
