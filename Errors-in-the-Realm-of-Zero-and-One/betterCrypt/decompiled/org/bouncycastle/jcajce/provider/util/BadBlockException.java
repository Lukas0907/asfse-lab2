// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.provider.util;

import javax.crypto.BadPaddingException;

public class BadBlockException extends BadPaddingException
{
    private final Throwable cause;
    
    public BadBlockException(final String msg, final Throwable cause) {
        super(msg);
        this.cause = cause;
    }
    
    @Override
    public Throwable getCause() {
        return this.cause;
    }
}
