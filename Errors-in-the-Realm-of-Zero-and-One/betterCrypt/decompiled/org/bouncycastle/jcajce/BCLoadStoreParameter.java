// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce;

import java.io.OutputStream;
import java.io.InputStream;
import java.security.KeyStore;

public class BCLoadStoreParameter implements LoadStoreParameter
{
    private final InputStream in;
    private final OutputStream out;
    private final ProtectionParameter protectionParameter;
    
    BCLoadStoreParameter(final InputStream in, final OutputStream out, final ProtectionParameter protectionParameter) {
        this.in = in;
        this.out = out;
        this.protectionParameter = protectionParameter;
    }
    
    public BCLoadStoreParameter(final InputStream inputStream, final ProtectionParameter protectionParameter) {
        this(inputStream, null, protectionParameter);
    }
    
    public BCLoadStoreParameter(final InputStream inputStream, final char[] password) {
        this(inputStream, (ProtectionParameter)new KeyStore.PasswordProtection(password));
    }
    
    public BCLoadStoreParameter(final OutputStream outputStream, final ProtectionParameter protectionParameter) {
        this(null, outputStream, protectionParameter);
    }
    
    public BCLoadStoreParameter(final OutputStream outputStream, final char[] password) {
        this(outputStream, (ProtectionParameter)new KeyStore.PasswordProtection(password));
    }
    
    public InputStream getInputStream() {
        if (this.out == null) {
            return this.in;
        }
        throw new UnsupportedOperationException("parameter configured for storage OutputStream present");
    }
    
    public OutputStream getOutputStream() {
        final OutputStream out = this.out;
        if (out != null) {
            return out;
        }
        throw new UnsupportedOperationException("parameter not configured for storage - no OutputStream");
    }
    
    @Override
    public ProtectionParameter getProtectionParameter() {
        return this.protectionParameter;
    }
}
