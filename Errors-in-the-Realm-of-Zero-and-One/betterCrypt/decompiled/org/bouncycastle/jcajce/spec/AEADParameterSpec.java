// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.spec;

import org.bouncycastle.util.Arrays;
import javax.crypto.spec.IvParameterSpec;

public class AEADParameterSpec extends IvParameterSpec
{
    private final byte[] associatedData;
    private final int macSizeInBits;
    
    public AEADParameterSpec(final byte[] array, final int n) {
        this(array, n, null);
    }
    
    public AEADParameterSpec(final byte[] iv, final int macSizeInBits, final byte[] array) {
        super(iv);
        this.macSizeInBits = macSizeInBits;
        this.associatedData = Arrays.clone(array);
    }
    
    public byte[] getAssociatedData() {
        return Arrays.clone(this.associatedData);
    }
    
    public int getMacSizeInBits() {
        return this.macSizeInBits;
    }
    
    public byte[] getNonce() {
        return this.getIV();
    }
}
