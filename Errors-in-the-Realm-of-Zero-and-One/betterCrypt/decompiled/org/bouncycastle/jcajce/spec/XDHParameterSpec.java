// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.spec;

import org.bouncycastle.asn1.edec.EdECObjectIdentifiers;
import java.security.spec.AlgorithmParameterSpec;

public class XDHParameterSpec implements AlgorithmParameterSpec
{
    public static final String X25519 = "X25519";
    public static final String X448 = "X448";
    private final String curveName;
    
    public XDHParameterSpec(final String str) {
        Label_0013: {
            if (!str.equalsIgnoreCase("X25519")) {
                if (!str.equalsIgnoreCase("X448")) {
                    if (str.equals(EdECObjectIdentifiers.id_X25519.getId())) {
                        break Label_0013;
                    }
                    if (!str.equals(EdECObjectIdentifiers.id_X448.getId())) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("unrecognized curve name: ");
                        sb.append(str);
                        throw new IllegalArgumentException(sb.toString());
                    }
                }
                this.curveName = "X448";
                return;
            }
        }
        this.curveName = "X25519";
    }
    
    public String getCurveName() {
        return this.curveName;
    }
}
