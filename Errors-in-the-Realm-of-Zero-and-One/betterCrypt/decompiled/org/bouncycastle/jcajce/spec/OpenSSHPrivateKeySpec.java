// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.spec;

import java.security.spec.EncodedKeySpec;

public class OpenSSHPrivateKeySpec extends EncodedKeySpec
{
    private final String format;
    
    public OpenSSHPrivateKeySpec(final byte[] encodedKey) {
        super(encodedKey);
        String format;
        if (encodedKey[0] == 48) {
            format = "ASN.1";
        }
        else {
            if (encodedKey[0] != 111) {
                throw new IllegalArgumentException("unknown byte encoding");
            }
            format = "OpenSSH";
        }
        this.format = format;
    }
    
    @Override
    public String getFormat() {
        return this.format;
    }
}
