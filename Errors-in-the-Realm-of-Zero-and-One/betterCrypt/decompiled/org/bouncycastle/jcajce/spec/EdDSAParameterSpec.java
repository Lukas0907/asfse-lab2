// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.spec;

import org.bouncycastle.asn1.edec.EdECObjectIdentifiers;
import java.security.spec.AlgorithmParameterSpec;

public class EdDSAParameterSpec implements AlgorithmParameterSpec
{
    public static final String Ed25519 = "Ed25519";
    public static final String Ed448 = "Ed448";
    private final String curveName;
    
    public EdDSAParameterSpec(final String str) {
        Label_0013: {
            if (!str.equalsIgnoreCase("Ed25519")) {
                if (!str.equalsIgnoreCase("Ed448")) {
                    if (str.equals(EdECObjectIdentifiers.id_Ed25519.getId())) {
                        break Label_0013;
                    }
                    if (!str.equals(EdECObjectIdentifiers.id_Ed448.getId())) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("unrecognized curve name: ");
                        sb.append(str);
                        throw new IllegalArgumentException(sb.toString());
                    }
                }
                this.curveName = "Ed448";
                return;
            }
        }
        this.curveName = "Ed25519";
    }
    
    public String getCurveName() {
        return this.curveName;
    }
}
