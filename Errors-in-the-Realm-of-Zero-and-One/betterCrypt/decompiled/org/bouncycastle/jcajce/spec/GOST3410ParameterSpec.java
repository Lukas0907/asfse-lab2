// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.spec;

import org.bouncycastle.asn1.cryptopro.ECGOST3410NamedCurves;
import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import org.bouncycastle.asn1.rosstandart.RosstandartObjectIdentifiers;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.security.spec.AlgorithmParameterSpec;

public class GOST3410ParameterSpec implements AlgorithmParameterSpec
{
    private final ASN1ObjectIdentifier digestParamSet;
    private final ASN1ObjectIdentifier encryptionParamSet;
    private final ASN1ObjectIdentifier publicKeyParamSet;
    
    public GOST3410ParameterSpec(final String s) {
        this(getOid(s), getDigestOid(s), null);
    }
    
    public GOST3410ParameterSpec(final ASN1ObjectIdentifier asn1ObjectIdentifier, final ASN1ObjectIdentifier asn1ObjectIdentifier2) {
        this(asn1ObjectIdentifier, asn1ObjectIdentifier2, null);
    }
    
    public GOST3410ParameterSpec(final ASN1ObjectIdentifier publicKeyParamSet, final ASN1ObjectIdentifier digestParamSet, final ASN1ObjectIdentifier encryptionParamSet) {
        this.publicKeyParamSet = publicKeyParamSet;
        this.digestParamSet = digestParamSet;
        this.encryptionParamSet = encryptionParamSet;
    }
    
    private static ASN1ObjectIdentifier getDigestOid(final String s) {
        if (s.indexOf("12-512") > 0) {
            return RosstandartObjectIdentifiers.id_tc26_gost_3411_12_512;
        }
        if (s.indexOf("12-256") > 0) {
            return RosstandartObjectIdentifiers.id_tc26_gost_3411_12_256;
        }
        return CryptoProObjectIdentifiers.gostR3411_94_CryptoProParamSet;
    }
    
    private static ASN1ObjectIdentifier getOid(final String s) {
        return ECGOST3410NamedCurves.getOID(s);
    }
    
    public ASN1ObjectIdentifier getDigestParamSet() {
        return this.digestParamSet;
    }
    
    public ASN1ObjectIdentifier getEncryptionParamSet() {
        return this.encryptionParamSet;
    }
    
    public ASN1ObjectIdentifier getPublicKeyParamSet() {
        return this.publicKeyParamSet;
    }
    
    public String getPublicKeyParamSetName() {
        return ECGOST3410NamedCurves.getName(this.getPublicKeyParamSet());
    }
}
