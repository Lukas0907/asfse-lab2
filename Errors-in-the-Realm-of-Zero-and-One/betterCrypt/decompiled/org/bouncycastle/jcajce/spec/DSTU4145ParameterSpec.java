// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.spec;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.asn1.ua.DSTU4145Params;
import org.bouncycastle.jcajce.provider.asymmetric.util.EC5Util;
import org.bouncycastle.crypto.params.ECDomainParameters;
import java.security.spec.ECParameterSpec;

public class DSTU4145ParameterSpec extends ECParameterSpec
{
    private final byte[] dke;
    private final ECDomainParameters parameters;
    
    public DSTU4145ParameterSpec(final ECDomainParameters ecDomainParameters) {
        this(ecDomainParameters, EC5Util.convertToSpec(ecDomainParameters), DSTU4145Params.getDefaultDKE());
    }
    
    private DSTU4145ParameterSpec(final ECDomainParameters parameters, final ECParameterSpec ecParameterSpec, final byte[] array) {
        super(ecParameterSpec.getCurve(), ecParameterSpec.getGenerator(), ecParameterSpec.getOrder(), ecParameterSpec.getCofactor());
        this.parameters = parameters;
        this.dke = Arrays.clone(array);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof DSTU4145ParameterSpec && this.parameters.equals(((DSTU4145ParameterSpec)o).parameters);
    }
    
    public byte[] getDKE() {
        return Arrays.clone(this.dke);
    }
    
    @Override
    public int hashCode() {
        return this.parameters.hashCode();
    }
}
