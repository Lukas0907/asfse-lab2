// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.spec;

import org.bouncycastle.crypto.params.DHParameters;
import org.bouncycastle.crypto.params.DHValidationParameters;
import java.math.BigInteger;
import javax.crypto.spec.DHParameterSpec;

public class DHDomainParameterSpec extends DHParameterSpec
{
    private final BigInteger j;
    private final int m;
    private final BigInteger q;
    private DHValidationParameters validationParameters;
    
    public DHDomainParameterSpec(final BigInteger bigInteger, final BigInteger bigInteger2, final BigInteger bigInteger3) {
        this(bigInteger, bigInteger2, bigInteger3, null, 0);
    }
    
    public DHDomainParameterSpec(final BigInteger bigInteger, final BigInteger bigInteger2, final BigInteger bigInteger3, final int n) {
        this(bigInteger, bigInteger2, bigInteger3, null, n);
    }
    
    public DHDomainParameterSpec(final BigInteger bigInteger, final BigInteger bigInteger2, final BigInteger bigInteger3, final BigInteger bigInteger4, final int n) {
        this(bigInteger, bigInteger2, bigInteger3, bigInteger4, 0, n);
    }
    
    public DHDomainParameterSpec(final BigInteger p6, final BigInteger q, final BigInteger g, final BigInteger j, final int m, final int l) {
        super(p6, g, l);
        this.q = q;
        this.j = j;
        this.m = m;
    }
    
    public DHDomainParameterSpec(final DHParameters dhParameters) {
        this(dhParameters.getP(), dhParameters.getQ(), dhParameters.getG(), dhParameters.getJ(), dhParameters.getM(), dhParameters.getL());
        this.validationParameters = dhParameters.getValidationParameters();
    }
    
    public DHParameters getDomainParameters() {
        return new DHParameters(this.getP(), this.getG(), this.q, this.m, this.getL(), this.j, this.validationParameters);
    }
    
    public BigInteger getJ() {
        return this.j;
    }
    
    public int getM() {
        return this.m;
    }
    
    public BigInteger getQ() {
        return this.q;
    }
}
