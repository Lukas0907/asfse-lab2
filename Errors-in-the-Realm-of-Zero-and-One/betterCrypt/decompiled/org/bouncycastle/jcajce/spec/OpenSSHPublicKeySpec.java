// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.spec;

import org.bouncycastle.util.Strings;
import org.bouncycastle.util.Arrays;
import java.security.spec.EncodedKeySpec;

public class OpenSSHPublicKeySpec extends EncodedKeySpec
{
    private static final String[] allowedTypes;
    private final String type;
    
    static {
        allowedTypes = new String[] { "ssh-rsa", "ssh-ed25519", "ssh-dss" };
    }
    
    public OpenSSHPublicKeySpec(final byte[] encodedKey) {
        super(encodedKey);
        int n = 0;
        final int n2 = ((encodedKey[0] & 0xFF) << 24 | (encodedKey[1] & 0xFF) << 16 | (encodedKey[2] & 0xFF) << 8 | (encodedKey[3] & 0xFF)) + 4;
        if (n2 >= encodedKey.length) {
            throw new IllegalArgumentException("invalid public key blob: type field longer than blob");
        }
        this.type = Strings.fromByteArray(Arrays.copyOfRange(encodedKey, 4, n2));
        if (this.type.startsWith("ecdsa")) {
            return;
        }
        while (true) {
            final String[] allowedTypes = OpenSSHPublicKeySpec.allowedTypes;
            if (n >= allowedTypes.length) {
                final StringBuilder sb = new StringBuilder();
                sb.append("unrecognised public key type ");
                sb.append(this.type);
                throw new IllegalArgumentException(sb.toString());
            }
            if (allowedTypes[n].equals(this.type)) {
                return;
            }
            ++n;
        }
    }
    
    @Override
    public String getFormat() {
        return "OpenSSH";
    }
    
    public String getType() {
        return this.type;
    }
}
