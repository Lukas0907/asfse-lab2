// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce;

import org.bouncycastle.crypto.util.PBKDFConfig;
import java.io.OutputStream;
import java.security.KeyStore;

public class BCFKSStoreParameter implements LoadStoreParameter
{
    private OutputStream out;
    private final ProtectionParameter protectionParameter;
    private final PBKDFConfig storeConfig;
    
    public BCFKSStoreParameter(final OutputStream out, final PBKDFConfig storeConfig, final ProtectionParameter protectionParameter) {
        this.out = out;
        this.storeConfig = storeConfig;
        this.protectionParameter = protectionParameter;
    }
    
    public BCFKSStoreParameter(final OutputStream outputStream, final PBKDFConfig pbkdfConfig, final char[] password) {
        this(outputStream, pbkdfConfig, (ProtectionParameter)new KeyStore.PasswordProtection(password));
    }
    
    public OutputStream getOutputStream() {
        return this.out;
    }
    
    @Override
    public ProtectionParameter getProtectionParameter() {
        return this.protectionParameter;
    }
    
    public PBKDFConfig getStorePBKDFConfig() {
        return this.storeConfig;
    }
}
