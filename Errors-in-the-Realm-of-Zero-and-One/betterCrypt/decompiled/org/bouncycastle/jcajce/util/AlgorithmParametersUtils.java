// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.util;

import java.io.IOException;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Encodable;
import java.security.AlgorithmParameters;

public class AlgorithmParametersUtils
{
    private AlgorithmParametersUtils() {
    }
    
    public static ASN1Encodable extractParameters(final AlgorithmParameters algorithmParameters) throws IOException {
        try {
            return ASN1Primitive.fromByteArray(algorithmParameters.getEncoded("ASN.1"));
        }
        catch (Exception ex) {
            return ASN1Primitive.fromByteArray(algorithmParameters.getEncoded());
        }
    }
    
    public static void loadParameters(final AlgorithmParameters algorithmParameters, final ASN1Encodable asn1Encodable) throws IOException {
        while (true) {
            try {
                algorithmParameters.init(asn1Encodable.toASN1Primitive().getEncoded(), "ASN.1");
                return;
                algorithmParameters.init(asn1Encodable.toASN1Primitive().getEncoded());
            }
            catch (Exception ex) {
                continue;
            }
            break;
        }
    }
}
