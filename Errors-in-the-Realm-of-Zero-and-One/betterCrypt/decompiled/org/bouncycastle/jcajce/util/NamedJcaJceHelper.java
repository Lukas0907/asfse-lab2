// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.util;

import java.security.Signature;
import java.security.SecureRandom;
import javax.crypto.SecretKeyFactory;
import javax.crypto.Mac;
import java.security.KeyStoreException;
import java.security.KeyStore;
import java.security.KeyPairGenerator;
import javax.crypto.KeyGenerator;
import java.security.KeyFactory;
import javax.crypto.KeyAgreement;
import javax.crypto.ExemptionMechanism;
import java.security.MessageDigest;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.Cipher;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.InvalidAlgorithmParameterException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreParameters;
import java.security.cert.CertPathValidator;
import java.security.cert.CertPathBuilder;
import java.security.AlgorithmParameters;
import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import java.security.AlgorithmParameterGenerator;

public class NamedJcaJceHelper implements JcaJceHelper
{
    protected final String providerName;
    
    public NamedJcaJceHelper(final String providerName) {
        this.providerName = providerName;
    }
    
    @Override
    public AlgorithmParameterGenerator createAlgorithmParameterGenerator(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return AlgorithmParameterGenerator.getInstance(algorithm, this.providerName);
    }
    
    @Override
    public AlgorithmParameters createAlgorithmParameters(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return AlgorithmParameters.getInstance(algorithm, this.providerName);
    }
    
    @Override
    public CertPathBuilder createCertPathBuilder(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return CertPathBuilder.getInstance(algorithm, this.providerName);
    }
    
    @Override
    public CertPathValidator createCertPathValidator(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return CertPathValidator.getInstance(algorithm, this.providerName);
    }
    
    @Override
    public CertStore createCertStore(final String type, final CertStoreParameters params) throws NoSuchAlgorithmException, InvalidAlgorithmParameterException, NoSuchProviderException {
        return CertStore.getInstance(type, params, this.providerName);
    }
    
    @Override
    public CertificateFactory createCertificateFactory(final String type) throws CertificateException, NoSuchProviderException {
        return CertificateFactory.getInstance(type, this.providerName);
    }
    
    @Override
    public Cipher createCipher(final String transformation) throws NoSuchAlgorithmException, NoSuchPaddingException, NoSuchProviderException {
        return Cipher.getInstance(transformation, this.providerName);
    }
    
    @Override
    public MessageDigest createDigest(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return MessageDigest.getInstance(algorithm, this.providerName);
    }
    
    @Override
    public ExemptionMechanism createExemptionMechanism(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return ExemptionMechanism.getInstance(algorithm, this.providerName);
    }
    
    @Override
    public KeyAgreement createKeyAgreement(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return KeyAgreement.getInstance(algorithm, this.providerName);
    }
    
    @Override
    public KeyFactory createKeyFactory(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return KeyFactory.getInstance(algorithm, this.providerName);
    }
    
    @Override
    public KeyGenerator createKeyGenerator(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return KeyGenerator.getInstance(algorithm, this.providerName);
    }
    
    @Override
    public KeyPairGenerator createKeyPairGenerator(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return KeyPairGenerator.getInstance(algorithm, this.providerName);
    }
    
    @Override
    public KeyStore createKeyStore(final String type) throws KeyStoreException, NoSuchProviderException {
        return KeyStore.getInstance(type, this.providerName);
    }
    
    @Override
    public Mac createMac(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return Mac.getInstance(algorithm, this.providerName);
    }
    
    @Override
    public MessageDigest createMessageDigest(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return MessageDigest.getInstance(algorithm, this.providerName);
    }
    
    @Override
    public SecretKeyFactory createSecretKeyFactory(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return SecretKeyFactory.getInstance(algorithm, this.providerName);
    }
    
    @Override
    public SecureRandom createSecureRandom(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return SecureRandom.getInstance(algorithm, this.providerName);
    }
    
    @Override
    public Signature createSignature(final String algorithm) throws NoSuchAlgorithmException, NoSuchProviderException {
        return Signature.getInstance(algorithm, this.providerName);
    }
}
