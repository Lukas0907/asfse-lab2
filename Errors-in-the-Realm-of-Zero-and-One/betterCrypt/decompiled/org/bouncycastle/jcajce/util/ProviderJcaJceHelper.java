// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.util;

import java.security.Signature;
import java.security.SecureRandom;
import javax.crypto.SecretKeyFactory;
import javax.crypto.Mac;
import java.security.KeyStoreException;
import java.security.KeyStore;
import java.security.KeyPairGenerator;
import javax.crypto.KeyGenerator;
import java.security.KeyFactory;
import javax.crypto.KeyAgreement;
import javax.crypto.ExemptionMechanism;
import java.security.MessageDigest;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.Cipher;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.InvalidAlgorithmParameterException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreParameters;
import java.security.cert.CertPathValidator;
import java.security.cert.CertPathBuilder;
import java.security.AlgorithmParameters;
import java.security.NoSuchAlgorithmException;
import java.security.AlgorithmParameterGenerator;
import java.security.Provider;

public class ProviderJcaJceHelper implements JcaJceHelper
{
    protected final Provider provider;
    
    public ProviderJcaJceHelper(final Provider provider) {
        this.provider = provider;
    }
    
    @Override
    public AlgorithmParameterGenerator createAlgorithmParameterGenerator(final String algorithm) throws NoSuchAlgorithmException {
        return AlgorithmParameterGenerator.getInstance(algorithm, this.provider);
    }
    
    @Override
    public AlgorithmParameters createAlgorithmParameters(final String algorithm) throws NoSuchAlgorithmException {
        return AlgorithmParameters.getInstance(algorithm, this.provider);
    }
    
    @Override
    public CertPathBuilder createCertPathBuilder(final String algorithm) throws NoSuchAlgorithmException {
        return CertPathBuilder.getInstance(algorithm, this.provider);
    }
    
    @Override
    public CertPathValidator createCertPathValidator(final String algorithm) throws NoSuchAlgorithmException {
        return CertPathValidator.getInstance(algorithm, this.provider);
    }
    
    @Override
    public CertStore createCertStore(final String type, final CertStoreParameters params) throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        return CertStore.getInstance(type, params, this.provider);
    }
    
    @Override
    public CertificateFactory createCertificateFactory(final String type) throws CertificateException {
        return CertificateFactory.getInstance(type, this.provider);
    }
    
    @Override
    public Cipher createCipher(final String transformation) throws NoSuchAlgorithmException, NoSuchPaddingException {
        return Cipher.getInstance(transformation, this.provider);
    }
    
    @Override
    public MessageDigest createDigest(final String algorithm) throws NoSuchAlgorithmException {
        return MessageDigest.getInstance(algorithm, this.provider);
    }
    
    @Override
    public ExemptionMechanism createExemptionMechanism(final String algorithm) throws NoSuchAlgorithmException {
        return ExemptionMechanism.getInstance(algorithm, this.provider);
    }
    
    @Override
    public KeyAgreement createKeyAgreement(final String algorithm) throws NoSuchAlgorithmException {
        return KeyAgreement.getInstance(algorithm, this.provider);
    }
    
    @Override
    public KeyFactory createKeyFactory(final String algorithm) throws NoSuchAlgorithmException {
        return KeyFactory.getInstance(algorithm, this.provider);
    }
    
    @Override
    public KeyGenerator createKeyGenerator(final String algorithm) throws NoSuchAlgorithmException {
        return KeyGenerator.getInstance(algorithm, this.provider);
    }
    
    @Override
    public KeyPairGenerator createKeyPairGenerator(final String algorithm) throws NoSuchAlgorithmException {
        return KeyPairGenerator.getInstance(algorithm, this.provider);
    }
    
    @Override
    public KeyStore createKeyStore(final String type) throws KeyStoreException {
        return KeyStore.getInstance(type, this.provider);
    }
    
    @Override
    public Mac createMac(final String algorithm) throws NoSuchAlgorithmException {
        return Mac.getInstance(algorithm, this.provider);
    }
    
    @Override
    public MessageDigest createMessageDigest(final String algorithm) throws NoSuchAlgorithmException {
        return MessageDigest.getInstance(algorithm, this.provider);
    }
    
    @Override
    public SecretKeyFactory createSecretKeyFactory(final String algorithm) throws NoSuchAlgorithmException {
        return SecretKeyFactory.getInstance(algorithm, this.provider);
    }
    
    @Override
    public SecureRandom createSecureRandom(final String algorithm) throws NoSuchAlgorithmException {
        return SecureRandom.getInstance(algorithm, this.provider);
    }
    
    @Override
    public Signature createSignature(final String algorithm) throws NoSuchAlgorithmException {
        return Signature.getInstance(algorithm, this.provider);
    }
}
