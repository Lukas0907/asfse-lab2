// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.util;

import java.security.Signature;
import java.security.SecureRandom;
import javax.crypto.SecretKeyFactory;
import javax.crypto.Mac;
import java.security.KeyStoreException;
import java.security.KeyStore;
import java.security.KeyPairGenerator;
import javax.crypto.KeyGenerator;
import java.security.KeyFactory;
import javax.crypto.KeyAgreement;
import javax.crypto.ExemptionMechanism;
import java.security.MessageDigest;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.Cipher;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.InvalidAlgorithmParameterException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreParameters;
import java.security.cert.CertPathValidator;
import java.security.cert.CertPathBuilder;
import java.security.AlgorithmParameters;
import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import java.security.AlgorithmParameterGenerator;

public interface JcaJceHelper
{
    AlgorithmParameterGenerator createAlgorithmParameterGenerator(final String p0) throws NoSuchAlgorithmException, NoSuchProviderException;
    
    AlgorithmParameters createAlgorithmParameters(final String p0) throws NoSuchAlgorithmException, NoSuchProviderException;
    
    CertPathBuilder createCertPathBuilder(final String p0) throws NoSuchAlgorithmException, NoSuchProviderException;
    
    CertPathValidator createCertPathValidator(final String p0) throws NoSuchAlgorithmException, NoSuchProviderException;
    
    CertStore createCertStore(final String p0, final CertStoreParameters p1) throws NoSuchAlgorithmException, InvalidAlgorithmParameterException, NoSuchProviderException;
    
    CertificateFactory createCertificateFactory(final String p0) throws NoSuchProviderException, CertificateException;
    
    Cipher createCipher(final String p0) throws NoSuchAlgorithmException, NoSuchPaddingException, NoSuchProviderException;
    
    MessageDigest createDigest(final String p0) throws NoSuchAlgorithmException, NoSuchProviderException;
    
    ExemptionMechanism createExemptionMechanism(final String p0) throws NoSuchAlgorithmException, NoSuchProviderException;
    
    KeyAgreement createKeyAgreement(final String p0) throws NoSuchAlgorithmException, NoSuchProviderException;
    
    KeyFactory createKeyFactory(final String p0) throws NoSuchAlgorithmException, NoSuchProviderException;
    
    KeyGenerator createKeyGenerator(final String p0) throws NoSuchAlgorithmException, NoSuchProviderException;
    
    KeyPairGenerator createKeyPairGenerator(final String p0) throws NoSuchAlgorithmException, NoSuchProviderException;
    
    KeyStore createKeyStore(final String p0) throws KeyStoreException, NoSuchProviderException;
    
    Mac createMac(final String p0) throws NoSuchAlgorithmException, NoSuchProviderException;
    
    MessageDigest createMessageDigest(final String p0) throws NoSuchAlgorithmException, NoSuchProviderException;
    
    SecretKeyFactory createSecretKeyFactory(final String p0) throws NoSuchAlgorithmException, NoSuchProviderException;
    
    SecureRandom createSecureRandom(final String p0) throws NoSuchAlgorithmException, NoSuchProviderException;
    
    Signature createSignature(final String p0) throws NoSuchAlgorithmException, NoSuchProviderException;
}
