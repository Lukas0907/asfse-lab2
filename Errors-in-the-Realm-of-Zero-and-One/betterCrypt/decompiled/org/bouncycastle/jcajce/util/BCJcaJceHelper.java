// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.util;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import java.security.Security;
import java.security.Provider;

public class BCJcaJceHelper extends ProviderJcaJceHelper
{
    private static volatile Provider bcProvider;
    
    public BCJcaJceHelper() {
        super(getBouncyCastleProvider());
    }
    
    private static Provider getBouncyCastleProvider() {
        synchronized (BCJcaJceHelper.class) {
            if (Security.getProvider("BC") != null) {
                return Security.getProvider("BC");
            }
            if (BCJcaJceHelper.bcProvider != null) {
                return BCJcaJceHelper.bcProvider;
            }
            return BCJcaJceHelper.bcProvider = new BouncyCastleProvider();
        }
    }
}
