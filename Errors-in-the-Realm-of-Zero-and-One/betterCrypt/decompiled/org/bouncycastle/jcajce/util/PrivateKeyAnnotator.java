// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.security.PrivateKey;

public class PrivateKeyAnnotator
{
    public static AnnotatedPrivateKey annotate(final PrivateKey privateKey, final String s) {
        return new AnnotatedPrivateKey(privateKey, s);
    }
    
    public static AnnotatedPrivateKey annotate(final PrivateKey privateKey, final Map<String, Object> m) {
        return new AnnotatedPrivateKey(privateKey, Collections.unmodifiableMap((Map<? extends String, ?>)new HashMap<String, Object>(m)));
    }
}
