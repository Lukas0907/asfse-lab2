// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.util;

import java.util.HashMap;
import java.util.Collections;
import java.util.Map;
import java.security.PrivateKey;

public class AnnotatedPrivateKey implements PrivateKey
{
    public static final String LABEL = "label";
    private final Map<String, Object> annotations;
    private final PrivateKey key;
    
    AnnotatedPrivateKey(final PrivateKey key, final String value) {
        this.key = key;
        this.annotations = (Map<String, Object>)Collections.singletonMap("label", value);
    }
    
    AnnotatedPrivateKey(final PrivateKey key, final Map<String, Object> annotations) {
        this.key = key;
        this.annotations = annotations;
    }
    
    public AnnotatedPrivateKey addAnnotation(final String s, final Object o) {
        final HashMap<String, Object> m = new HashMap<String, Object>(this.annotations);
        m.put(s, o);
        return new AnnotatedPrivateKey(this.key, (Map<String, Object>)Collections.unmodifiableMap((Map<?, ?>)m));
    }
    
    @Override
    public boolean equals(Object key) {
        PrivateKey privateKey;
        if (key instanceof AnnotatedPrivateKey) {
            privateKey = this.key;
            key = ((AnnotatedPrivateKey)key).key;
        }
        else {
            privateKey = this.key;
        }
        return privateKey.equals(key);
    }
    
    @Override
    public String getAlgorithm() {
        return this.key.getAlgorithm();
    }
    
    public Object getAnnotation(final String s) {
        return this.annotations.get(s);
    }
    
    public Map<String, Object> getAnnotations() {
        return this.annotations;
    }
    
    @Override
    public byte[] getEncoded() {
        return this.key.getEncoded();
    }
    
    @Override
    public String getFormat() {
        return this.key.getFormat();
    }
    
    public PrivateKey getKey() {
        return this.key;
    }
    
    @Override
    public int hashCode() {
        return this.key.hashCode();
    }
    
    public AnnotatedPrivateKey removeAnnotation(final String s) {
        final HashMap<Object, Object> m = new HashMap<Object, Object>(this.annotations);
        m.remove(s);
        return new AnnotatedPrivateKey(this.key, Collections.unmodifiableMap((Map<? extends String, ?>)m));
    }
    
    @Override
    public String toString() {
        Object o;
        if (this.annotations.containsKey("label")) {
            o = this.annotations.get("label");
        }
        else {
            o = this.key;
        }
        return o.toString();
    }
}
