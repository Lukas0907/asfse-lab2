// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce;

import java.security.PrivateKey;
import java.security.PublicKey;
import org.bouncycastle.crypto.util.PBKDF2Config;
import java.security.KeyStore;
import java.io.OutputStream;
import java.io.InputStream;
import org.bouncycastle.crypto.util.PBKDFConfig;
import java.security.Key;
import java.security.cert.X509Certificate;

public class BCFKSLoadStoreParameter extends BCLoadStoreParameter
{
    private final X509Certificate[] certificates;
    private final EncryptionAlgorithm encAlg;
    private final MacAlgorithm macAlg;
    private final SignatureAlgorithm sigAlg;
    private final Key sigKey;
    private final PBKDFConfig storeConfig;
    private final CertChainValidator validator;
    
    private BCFKSLoadStoreParameter(final Builder builder) {
        super(builder.in, builder.out, builder.protectionParameter);
        this.storeConfig = builder.storeConfig;
        this.encAlg = builder.encAlg;
        this.macAlg = builder.macAlg;
        this.sigAlg = builder.sigAlg;
        this.sigKey = builder.sigKey;
        this.certificates = builder.certs;
        this.validator = builder.validator;
    }
    
    public CertChainValidator getCertChainValidator() {
        return this.validator;
    }
    
    public X509Certificate[] getStoreCertificates() {
        return this.certificates;
    }
    
    public EncryptionAlgorithm getStoreEncryptionAlgorithm() {
        return this.encAlg;
    }
    
    public MacAlgorithm getStoreMacAlgorithm() {
        return this.macAlg;
    }
    
    public PBKDFConfig getStorePBKDFConfig() {
        return this.storeConfig;
    }
    
    public SignatureAlgorithm getStoreSignatureAlgorithm() {
        return this.sigAlg;
    }
    
    public Key getStoreSignatureKey() {
        return this.sigKey;
    }
    
    public static class Builder
    {
        private X509Certificate[] certs;
        private EncryptionAlgorithm encAlg;
        private final InputStream in;
        private MacAlgorithm macAlg;
        private final OutputStream out;
        private final ProtectionParameter protectionParameter;
        private SignatureAlgorithm sigAlg;
        private final Key sigKey;
        private PBKDFConfig storeConfig;
        private CertChainValidator validator;
        
        public Builder() {
            this((OutputStream)null, (ProtectionParameter)null);
        }
        
        public Builder(final InputStream in, final ProtectionParameter protectionParameter) {
            this.storeConfig = new PBKDF2Config.Builder().withIterationCount(16384).withSaltLength(64).withPRF(PBKDF2Config.PRF_SHA512).build();
            this.encAlg = EncryptionAlgorithm.AES256_CCM;
            this.macAlg = MacAlgorithm.HmacSHA512;
            this.sigAlg = SignatureAlgorithm.SHA512withECDSA;
            this.certs = null;
            this.in = in;
            this.out = null;
            this.protectionParameter = protectionParameter;
            this.sigKey = null;
        }
        
        public Builder(final InputStream in, final PublicKey sigKey) {
            this.storeConfig = new PBKDF2Config.Builder().withIterationCount(16384).withSaltLength(64).withPRF(PBKDF2Config.PRF_SHA512).build();
            this.encAlg = EncryptionAlgorithm.AES256_CCM;
            this.macAlg = MacAlgorithm.HmacSHA512;
            this.sigAlg = SignatureAlgorithm.SHA512withECDSA;
            this.certs = null;
            this.in = in;
            this.out = null;
            this.protectionParameter = null;
            this.sigKey = sigKey;
        }
        
        public Builder(final InputStream in, final CertChainValidator validator) {
            this.storeConfig = new PBKDF2Config.Builder().withIterationCount(16384).withSaltLength(64).withPRF(PBKDF2Config.PRF_SHA512).build();
            this.encAlg = EncryptionAlgorithm.AES256_CCM;
            this.macAlg = MacAlgorithm.HmacSHA512;
            this.sigAlg = SignatureAlgorithm.SHA512withECDSA;
            this.certs = null;
            this.in = in;
            this.out = null;
            this.protectionParameter = null;
            this.validator = validator;
            this.sigKey = null;
        }
        
        public Builder(final InputStream inputStream, final char[] password) {
            this(inputStream, (ProtectionParameter)new KeyStore.PasswordProtection(password));
        }
        
        public Builder(final OutputStream out, final ProtectionParameter protectionParameter) {
            this.storeConfig = new PBKDF2Config.Builder().withIterationCount(16384).withSaltLength(64).withPRF(PBKDF2Config.PRF_SHA512).build();
            this.encAlg = EncryptionAlgorithm.AES256_CCM;
            this.macAlg = MacAlgorithm.HmacSHA512;
            this.sigAlg = SignatureAlgorithm.SHA512withECDSA;
            this.certs = null;
            this.in = null;
            this.out = out;
            this.protectionParameter = protectionParameter;
            this.sigKey = null;
        }
        
        public Builder(final OutputStream out, final PrivateKey sigKey) {
            this.storeConfig = new PBKDF2Config.Builder().withIterationCount(16384).withSaltLength(64).withPRF(PBKDF2Config.PRF_SHA512).build();
            this.encAlg = EncryptionAlgorithm.AES256_CCM;
            this.macAlg = MacAlgorithm.HmacSHA512;
            this.sigAlg = SignatureAlgorithm.SHA512withECDSA;
            this.certs = null;
            this.in = null;
            this.out = out;
            this.protectionParameter = null;
            this.sigKey = sigKey;
        }
        
        public Builder(final OutputStream outputStream, final char[] password) {
            this(outputStream, (ProtectionParameter)new KeyStore.PasswordProtection(password));
        }
        
        public BCFKSLoadStoreParameter build() {
            return new BCFKSLoadStoreParameter(this, null);
        }
        
        public Builder withCertificates(final X509Certificate[] array) {
            final X509Certificate[] certs = new X509Certificate[array.length];
            System.arraycopy(array, 0, certs, 0, certs.length);
            this.certs = certs;
            return this;
        }
        
        public Builder withStoreEncryptionAlgorithm(final EncryptionAlgorithm encAlg) {
            this.encAlg = encAlg;
            return this;
        }
        
        public Builder withStoreMacAlgorithm(final MacAlgorithm macAlg) {
            this.macAlg = macAlg;
            return this;
        }
        
        public Builder withStorePBKDFConfig(final PBKDFConfig storeConfig) {
            this.storeConfig = storeConfig;
            return this;
        }
        
        public Builder withStoreSignatureAlgorithm(final SignatureAlgorithm sigAlg) {
            this.sigAlg = sigAlg;
            return this;
        }
    }
    
    public interface CertChainValidator
    {
        boolean isValid(final X509Certificate[] p0);
    }
    
    public enum EncryptionAlgorithm
    {
        AES256_CCM, 
        AES256_KWP;
    }
    
    public enum MacAlgorithm
    {
        HmacSHA3_512, 
        HmacSHA512;
    }
    
    public enum SignatureAlgorithm
    {
        SHA3_512withDSA, 
        SHA3_512withECDSA, 
        SHA3_512withRSA, 
        SHA512withDSA, 
        SHA512withECDSA, 
        SHA512withRSA;
    }
}
