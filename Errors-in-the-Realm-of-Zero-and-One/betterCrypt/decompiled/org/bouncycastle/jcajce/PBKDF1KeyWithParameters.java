// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce;

import org.bouncycastle.util.Arrays;
import org.bouncycastle.crypto.CharToByteConverter;
import javax.crypto.interfaces.PBEKey;

public class PBKDF1KeyWithParameters extends PBKDF1Key implements PBEKey
{
    private final int iterationCount;
    private final byte[] salt;
    
    public PBKDF1KeyWithParameters(final char[] array, final CharToByteConverter charToByteConverter, final byte[] array2, final int iterationCount) {
        super(array, charToByteConverter);
        this.salt = Arrays.clone(array2);
        this.iterationCount = iterationCount;
    }
    
    @Override
    public int getIterationCount() {
        return this.iterationCount;
    }
    
    @Override
    public byte[] getSalt() {
        return this.salt;
    }
}
