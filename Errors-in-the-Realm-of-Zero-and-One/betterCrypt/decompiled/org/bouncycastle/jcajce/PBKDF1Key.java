// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce;

import org.bouncycastle.crypto.CharToByteConverter;

public class PBKDF1Key implements PBKDFKey
{
    private final CharToByteConverter converter;
    private final char[] password;
    
    public PBKDF1Key(final char[] array, final CharToByteConverter converter) {
        this.password = new char[array.length];
        this.converter = converter;
        System.arraycopy(array, 0, this.password, 0, array.length);
    }
    
    @Override
    public String getAlgorithm() {
        return "PBKDF1";
    }
    
    @Override
    public byte[] getEncoded() {
        return this.converter.convert(this.password);
    }
    
    @Override
    public String getFormat() {
        return this.converter.getType();
    }
    
    public char[] getPassword() {
        return this.password;
    }
}
