// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.io;

import java.security.GeneralSecurityException;
import org.bouncycastle.crypto.io.InvalidCipherTextIOException;
import java.io.IOException;
import java.io.OutputStream;
import javax.crypto.Cipher;
import java.io.FilterOutputStream;

public class CipherOutputStream extends FilterOutputStream
{
    private final Cipher cipher;
    private final byte[] oneByte;
    
    public CipherOutputStream(final OutputStream out, final Cipher cipher) {
        super(out);
        this.oneByte = new byte[1];
        this.cipher = cipher;
    }
    
    @Override
    public void close() throws IOException {
        IOException ex;
        try {
            final byte[] doFinal = this.cipher.doFinal();
            if (doFinal != null) {
                this.out.write(doFinal);
            }
            ex = null;
        }
        catch (Exception obj) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Error closing stream: ");
            sb.append(obj);
            ex = new IOException(sb.toString());
        }
        catch (GeneralSecurityException ex2) {
            ex = new InvalidCipherTextIOException("Error during cipher finalisation", ex2);
        }
        try {
            this.flush();
            this.out.close();
        }
        catch (IOException ex3) {
            if (ex == null) {
                ex = ex3;
            }
        }
        if (ex == null) {
            return;
        }
        throw ex;
    }
    
    @Override
    public void flush() throws IOException {
        this.out.flush();
    }
    
    @Override
    public void write(final int n) throws IOException {
        final byte[] oneByte = this.oneByte;
        oneByte[0] = (byte)n;
        this.write(oneByte, 0, 1);
    }
    
    @Override
    public void write(byte[] update, final int inputOffset, final int inputLen) throws IOException {
        update = this.cipher.update(update, inputOffset, inputLen);
        if (update != null) {
            this.out.write(update);
        }
    }
}
