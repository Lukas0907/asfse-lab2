// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.io;

import java.io.IOException;
import javax.crypto.Mac;
import java.io.OutputStream;

class MacUpdatingOutputStream extends OutputStream
{
    private Mac mac;
    
    MacUpdatingOutputStream(final Mac mac) {
        this.mac = mac;
    }
    
    @Override
    public void write(final int n) throws IOException {
        this.mac.update((byte)n);
    }
    
    @Override
    public void write(final byte[] input) throws IOException {
        this.mac.update(input);
    }
    
    @Override
    public void write(final byte[] input, final int offset, final int len) throws IOException {
        this.mac.update(input, offset, len);
    }
}
