// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.io;

import java.io.IOException;
import java.security.MessageDigest;
import java.io.OutputStream;

class DigestUpdatingOutputStream extends OutputStream
{
    private MessageDigest digest;
    
    DigestUpdatingOutputStream(final MessageDigest digest) {
        this.digest = digest;
    }
    
    @Override
    public void write(final int n) throws IOException {
        this.digest.update((byte)n);
    }
    
    @Override
    public void write(final byte[] input) throws IOException {
        this.digest.update(input);
    }
    
    @Override
    public void write(final byte[] input, final int offset, final int len) throws IOException {
        this.digest.update(input, offset, len);
    }
}
