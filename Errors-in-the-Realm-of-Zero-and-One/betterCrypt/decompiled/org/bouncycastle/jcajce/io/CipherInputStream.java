// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.io;

import java.io.IOException;
import java.security.GeneralSecurityException;
import org.bouncycastle.crypto.io.InvalidCipherTextIOException;
import java.io.InputStream;
import javax.crypto.Cipher;
import java.io.FilterInputStream;

public class CipherInputStream extends FilterInputStream
{
    private byte[] buf;
    private int bufOff;
    private final Cipher cipher;
    private boolean finalized;
    private final byte[] inputBuffer;
    private int maxBuf;
    
    public CipherInputStream(final InputStream in, final Cipher cipher) {
        super(in);
        this.inputBuffer = new byte[512];
        this.finalized = false;
        this.cipher = cipher;
    }
    
    private byte[] finaliseCipher() throws InvalidCipherTextIOException {
        try {
            if (!this.finalized) {
                this.finalized = true;
                return this.cipher.doFinal();
            }
            return null;
        }
        catch (GeneralSecurityException ex) {
            throw new InvalidCipherTextIOException("Error finalising cipher", ex);
        }
    }
    
    private int nextChunk() throws IOException {
        if (this.finalized) {
            return -1;
        }
        this.bufOff = 0;
        this.maxBuf = 0;
        while (true) {
            final int maxBuf = this.maxBuf;
            if (maxBuf != 0) {
                return maxBuf;
            }
            final int read = this.in.read(this.inputBuffer);
            if (read == -1) {
                this.buf = this.finaliseCipher();
                final byte[] buf = this.buf;
                if (buf == null) {
                    return -1;
                }
                if (buf.length == 0) {
                    return -1;
                }
                return this.maxBuf = buf.length;
            }
            else {
                this.buf = this.cipher.update(this.inputBuffer, 0, read);
                final byte[] buf2 = this.buf;
                if (buf2 == null) {
                    continue;
                }
                this.maxBuf = buf2.length;
            }
        }
    }
    
    @Override
    public int available() throws IOException {
        return this.maxBuf - this.bufOff;
    }
    
    @Override
    public void close() throws IOException {
        try {
            this.in.close();
            if (!this.finalized) {
                this.finaliseCipher();
            }
            this.bufOff = 0;
            this.maxBuf = 0;
        }
        finally {
            if (!this.finalized) {
                this.finaliseCipher();
            }
        }
    }
    
    @Override
    public void mark(final int n) {
    }
    
    @Override
    public boolean markSupported() {
        return false;
    }
    
    @Override
    public int read() throws IOException {
        if (this.bufOff >= this.maxBuf && this.nextChunk() < 0) {
            return -1;
        }
        return this.buf[this.bufOff++] & 0xFF;
    }
    
    @Override
    public int read(final byte[] array, final int n, int min) throws IOException {
        if (this.bufOff >= this.maxBuf && this.nextChunk() < 0) {
            return -1;
        }
        min = Math.min(min, this.available());
        System.arraycopy(this.buf, this.bufOff, array, n, min);
        this.bufOff += min;
        return min;
    }
    
    @Override
    public void reset() throws IOException {
    }
    
    @Override
    public long skip(final long a) throws IOException {
        if (a <= 0L) {
            return 0L;
        }
        final int n = (int)Math.min(a, this.available());
        this.bufOff += n;
        return n;
    }
}
