// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.io;

import java.security.SignatureException;
import java.io.IOException;
import java.security.Signature;
import java.io.OutputStream;

class SignatureUpdatingOutputStream extends OutputStream
{
    private Signature sig;
    
    SignatureUpdatingOutputStream(final Signature sig) {
        this.sig = sig;
    }
    
    @Override
    public void write(final int n) throws IOException {
        try {
            this.sig.update((byte)n);
        }
        catch (SignatureException ex) {
            throw new IOException(ex.getMessage());
        }
    }
    
    @Override
    public void write(final byte[] data) throws IOException {
        try {
            this.sig.update(data);
        }
        catch (SignatureException ex) {
            throw new IOException(ex.getMessage());
        }
    }
    
    @Override
    public void write(final byte[] data, final int off, final int len) throws IOException {
        try {
            this.sig.update(data, off, len);
        }
        catch (SignatureException ex) {
            throw new IOException(ex.getMessage());
        }
    }
}
