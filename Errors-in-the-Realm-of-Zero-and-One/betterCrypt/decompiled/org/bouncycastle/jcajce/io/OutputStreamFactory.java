// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.io;

import javax.crypto.Mac;
import java.security.Signature;
import java.io.OutputStream;
import java.security.MessageDigest;

public class OutputStreamFactory
{
    public static OutputStream createStream(final MessageDigest messageDigest) {
        return new DigestUpdatingOutputStream(messageDigest);
    }
    
    public static OutputStream createStream(final Signature signature) {
        return new SignatureUpdatingOutputStream(signature);
    }
    
    public static OutputStream createStream(final Mac mac) {
        return new MacUpdatingOutputStream(mac);
    }
}
