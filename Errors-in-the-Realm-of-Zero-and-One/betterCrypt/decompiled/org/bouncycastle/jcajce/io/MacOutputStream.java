// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.io;

import java.io.IOException;
import javax.crypto.Mac;
import java.io.OutputStream;

public final class MacOutputStream extends OutputStream
{
    private Mac mac;
    
    public MacOutputStream(final Mac mac) {
        this.mac = mac;
    }
    
    public byte[] getMac() {
        return this.mac.doFinal();
    }
    
    @Override
    public void write(final int n) throws IOException {
        this.mac.update((byte)n);
    }
    
    @Override
    public void write(final byte[] input, final int offset, final int len) throws IOException {
        this.mac.update(input, offset, len);
    }
}
