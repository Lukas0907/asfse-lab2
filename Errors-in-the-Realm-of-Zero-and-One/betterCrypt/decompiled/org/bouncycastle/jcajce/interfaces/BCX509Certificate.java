// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.jcajce.interfaces;

import org.bouncycastle.asn1.x509.TBSCertificate;
import org.bouncycastle.asn1.x500.X500Name;

public interface BCX509Certificate
{
    X500Name getIssuerX500Name();
    
    X500Name getSubjectX500Name();
    
    TBSCertificate getTBSCertificateNative();
}
