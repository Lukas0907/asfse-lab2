// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.x509;

import java.security.GeneralSecurityException;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.security.cert.PolicyNode;
import java.util.ArrayList;
import org.bouncycastle.asn1.x509.PolicyInformation;
import org.bouncycastle.jce.provider.PKIXPolicyNode;
import java.util.Map;
import org.bouncycastle.asn1.x509.IssuingDistributionPoint;
import java.security.cert.PKIXParameters;
import java.math.BigInteger;
import java.util.Enumeration;
import org.bouncycastle.asn1.ASN1Encodable;
import java.io.OutputStream;
import org.bouncycastle.asn1.ASN1OutputStream;
import java.io.ByteArrayOutputStream;
import java.security.cert.PolicyQualifierInfo;
import java.util.Set;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1OctetString;
import java.security.interfaces.DSAParams;
import java.security.spec.KeySpec;
import java.security.KeyFactory;
import java.security.spec.DSAPublicKeySpec;
import java.security.interfaces.DSAPublicKey;
import org.bouncycastle.asn1.ASN1Primitive;
import java.security.cert.X509Certificate;
import javax.security.auth.x500.X500Principal;
import java.security.cert.X509CRLEntry;
import java.security.cert.CRLException;
import org.bouncycastle.asn1.ASN1Enumerated;
import org.bouncycastle.asn1.x509.X509Extension;
import java.security.cert.X509CRL;
import java.util.Date;
import java.security.cert.CertPathValidatorException;
import org.bouncycastle.jce.exception.ExtCertPathValidatorException;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import java.security.PublicKey;
import java.security.cert.CertSelector;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import org.bouncycastle.util.Encodable;
import org.bouncycastle.jcajce.provider.asymmetric.x509.CertificateFactory;
import java.util.Iterator;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertStore;
import org.bouncycastle.util.StoreException;
import org.bouncycastle.jce.provider.AnnotatedException;
import org.bouncycastle.util.Selector;
import org.bouncycastle.util.Store;
import java.util.HashSet;
import java.util.Collection;
import java.util.List;
import org.bouncycastle.jcajce.PKIXCertStoreSelector;
import org.bouncycastle.asn1.x509.Extension;

class CertPathValidatorUtilities
{
    protected static final String ANY_POLICY = "2.5.29.32.0";
    protected static final String AUTHORITY_KEY_IDENTIFIER;
    protected static final String BASIC_CONSTRAINTS;
    protected static final String CERTIFICATE_POLICIES;
    protected static final String CRL_DISTRIBUTION_POINTS;
    protected static final String CRL_NUMBER;
    protected static final int CRL_SIGN = 6;
    protected static final PKIXCRLUtil CRL_UTIL;
    protected static final String DELTA_CRL_INDICATOR;
    protected static final String FRESHEST_CRL;
    protected static final String INHIBIT_ANY_POLICY;
    protected static final String ISSUING_DISTRIBUTION_POINT;
    protected static final int KEY_CERT_SIGN = 5;
    protected static final String KEY_USAGE;
    protected static final String NAME_CONSTRAINTS;
    protected static final String POLICY_CONSTRAINTS;
    protected static final String POLICY_MAPPINGS;
    protected static final String SUBJECT_ALTERNATIVE_NAME;
    protected static final String[] crlReasons;
    
    static {
        CRL_UTIL = new PKIXCRLUtil();
        CERTIFICATE_POLICIES = Extension.certificatePolicies.getId();
        BASIC_CONSTRAINTS = Extension.basicConstraints.getId();
        POLICY_MAPPINGS = Extension.policyMappings.getId();
        SUBJECT_ALTERNATIVE_NAME = Extension.subjectAlternativeName.getId();
        NAME_CONSTRAINTS = Extension.nameConstraints.getId();
        KEY_USAGE = Extension.keyUsage.getId();
        INHIBIT_ANY_POLICY = Extension.inhibitAnyPolicy.getId();
        ISSUING_DISTRIBUTION_POINT = Extension.issuingDistributionPoint.getId();
        DELTA_CRL_INDICATOR = Extension.deltaCRLIndicator.getId();
        POLICY_CONSTRAINTS = Extension.policyConstraints.getId();
        FRESHEST_CRL = Extension.freshestCRL.getId();
        CRL_DISTRIBUTION_POINTS = Extension.cRLDistributionPoints.getId();
        AUTHORITY_KEY_IDENTIFIER = Extension.authorityKeyIdentifier.getId();
        CRL_NUMBER = Extension.cRLNumber.getId();
        crlReasons = new String[] { "unspecified", "keyCompromise", "cACompromise", "affiliationChanged", "superseded", "cessationOfOperation", "certificateHold", "unknown", "removeFromCRL", "privilegeWithdrawn", "aACompromise" };
    }
    
    protected static Collection findCertificates(final PKIXCertStoreSelector pkixCertStoreSelector, final List list) throws AnnotatedException {
        final HashSet set = new HashSet();
        for (final Store<? extends E> next : list) {
            if (next instanceof Store) {
                final Store<? extends E> store = next;
                try {
                    set.addAll(store.getMatches(pkixCertStoreSelector));
                    continue;
                }
                catch (StoreException ex) {
                    throw new AnnotatedException("Problem while picking certificates from X.509 store.", ex);
                }
            }
            final CertStore certStore = (CertStore)next;
            try {
                set.addAll(PKIXCertStoreSelector.getCertificates(pkixCertStoreSelector, certStore));
                continue;
            }
            catch (CertStoreException ex2) {
                throw new AnnotatedException("Problem while picking certificates from certificate store.", ex2);
            }
            break;
        }
        return set;
    }
    
    protected static Collection findCertificates(final X509AttributeCertStoreSelector x509AttributeCertStoreSelector, final List list) throws AnnotatedException {
        final HashSet set = new HashSet();
        for (final X509Store next : list) {
            if (next instanceof X509Store) {
                final X509Store x509Store = next;
                try {
                    set.addAll(x509Store.getMatches(x509AttributeCertStoreSelector));
                    continue;
                }
                catch (StoreException ex) {
                    throw new AnnotatedException("Problem while picking certificates from X.509 store.", ex);
                }
                break;
            }
        }
        return set;
    }
    
    protected static Collection findCertificates(final X509CertStoreSelector selector, final List list) throws AnnotatedException {
        final HashSet<Object> set = new HashSet<Object>();
        final Iterator<Store<Object>> iterator = list.iterator();
        final CertificateFactory certificateFactory = new CertificateFactory();
        while (iterator.hasNext()) {
            final Store<Object> next = iterator.next();
            if (next instanceof Store) {
                final Store<Object> store = next;
                try {
                    for (Object o : store.getMatches((Selector<Encodable>)selector)) {
                        if (o instanceof Encodable) {
                            o = certificateFactory.engineGenerateCertificate(new ByteArrayInputStream(((Encodable)o).getEncoded()));
                        }
                        else if (!(o instanceof Certificate)) {
                            throw new AnnotatedException("Unknown object found in certificate store.");
                        }
                        set.add(o);
                    }
                    continue;
                }
                catch (CertificateException ex) {
                    throw new AnnotatedException("Problem while extracting certificates from X.509 store.", ex);
                }
                catch (IOException ex2) {
                    throw new AnnotatedException("Problem while extracting certificates from X.509 store.", ex2);
                }
                catch (StoreException ex3) {
                    throw new AnnotatedException("Problem while picking certificates from X.509 store.", ex3);
                }
            }
            final CertStore certStore = (CertStore)next;
            try {
                set.addAll(certStore.getCertificates(selector));
                continue;
            }
            catch (CertStoreException ex4) {
                throw new AnnotatedException("Problem while picking certificates from certificate store.", ex4);
            }
            break;
        }
        return set;
    }
    
    protected static AlgorithmIdentifier getAlgorithmIdentifier(final PublicKey publicKey) throws CertPathValidatorException {
        try {
            return SubjectPublicKeyInfo.getInstance(new ASN1InputStream(publicKey.getEncoded()).readObject()).getAlgorithmId();
        }
        catch (Exception ex) {
            throw new ExtCertPathValidatorException("Subject public key cannot be decoded.", ex);
        }
    }
    
    protected static void getCertStatus(final Date date, final X509CRL x509CRL, final Object o, final CertStatus certStatus) throws AnnotatedException {
        try {
            X509CRLEntry revokedCertificate2;
            if (isIndirectCRL(x509CRL)) {
                final X509CRLEntry revokedCertificate = x509CRL.getRevokedCertificate(getSerialNumber(o));
                if (revokedCertificate == null) {
                    return;
                }
                X500Principal o2;
                if ((o2 = revokedCertificate.getCertificateIssuer()) == null) {
                    o2 = getIssuerPrincipal(x509CRL);
                }
                revokedCertificate2 = revokedCertificate;
                if (!getEncodedIssuerPrincipal(o).equals(o2)) {
                    return;
                }
            }
            else {
                if (!getEncodedIssuerPrincipal(o).equals(getIssuerPrincipal(x509CRL))) {
                    return;
                }
                if ((revokedCertificate2 = x509CRL.getRevokedCertificate(getSerialNumber(o))) == null) {
                    return;
                }
            }
            ASN1Enumerated instance = null;
            if (revokedCertificate2.hasExtensions()) {
                try {
                    instance = ASN1Enumerated.getInstance(getExtensionValue(revokedCertificate2, X509Extension.reasonCode.getId()));
                }
                catch (Exception ex) {
                    throw new AnnotatedException("Reason code CRL entry extension could not be decoded.", ex);
                }
            }
            int intValueExact;
            if (instance == null) {
                intValueExact = 0;
            }
            else {
                intValueExact = instance.intValueExact();
            }
            if (date.getTime() >= revokedCertificate2.getRevocationDate().getTime() || intValueExact == 0 || intValueExact == 1 || intValueExact == 2 || intValueExact == 10) {
                certStatus.setCertStatus(intValueExact);
                certStatus.setRevocationDate(revokedCertificate2.getRevocationDate());
            }
        }
        catch (CRLException ex2) {
            throw new AnnotatedException("Failed check for indirect CRL.", ex2);
        }
    }
    
    protected static X500Principal getEncodedIssuerPrincipal(final Object o) {
        if (o instanceof X509Certificate) {
            return ((X509Certificate)o).getIssuerX500Principal();
        }
        return (X500Principal)((X509AttributeCertificate)o).getIssuer().getPrincipals()[0];
    }
    
    protected static ASN1Primitive getExtensionValue(final java.security.cert.X509Extension x509Extension, final String s) throws AnnotatedException {
        final byte[] extensionValue = x509Extension.getExtensionValue(s);
        if (extensionValue == null) {
            return null;
        }
        return getObject(s, extensionValue);
    }
    
    protected static X500Principal getIssuerPrincipal(final X509CRL x509CRL) {
        return x509CRL.getIssuerX500Principal();
    }
    
    protected static PublicKey getNextWorkingKey(final List list, int n) throws CertPathValidatorException {
        final PublicKey publicKey = list.get(n).getPublicKey();
        if (!(publicKey instanceof DSAPublicKey)) {
            return publicKey;
        }
        final DSAPublicKey dsaPublicKey = (DSAPublicKey)publicKey;
        if (dsaPublicKey.getParams() != null) {
            return dsaPublicKey;
        }
        while (true) {
            ++n;
            if (n >= list.size()) {
                throw new CertPathValidatorException("DSA parameters cannot be inherited from previous certificate.");
            }
            final PublicKey publicKey2 = list.get(n).getPublicKey();
            if (!(publicKey2 instanceof DSAPublicKey)) {
                break;
            }
            final DSAPublicKey dsaPublicKey2 = (DSAPublicKey)publicKey2;
            if (dsaPublicKey2.getParams() == null) {
                continue;
            }
            final DSAParams params = dsaPublicKey2.getParams();
            final DSAPublicKeySpec keySpec = new DSAPublicKeySpec(dsaPublicKey.getY(), params.getP(), params.getQ(), params.getG());
            try {
                return KeyFactory.getInstance("DSA", "BC").generatePublic(keySpec);
            }
            catch (Exception ex) {
                throw new RuntimeException(ex.getMessage());
            }
            break;
        }
        throw new CertPathValidatorException("DSA parameters cannot be inherited from previous certificate.");
    }
    
    private static ASN1Primitive getObject(final String str, final byte[] array) throws AnnotatedException {
        try {
            return new ASN1InputStream(((ASN1OctetString)new ASN1InputStream(array).readObject()).getOctets()).readObject();
        }
        catch (Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("exception processing extension ");
            sb.append(str);
            throw new AnnotatedException(sb.toString(), ex);
        }
    }
    
    protected static final Set getQualifierSet(final ASN1Sequence asn1Sequence) throws CertPathValidatorException {
        final HashSet<PolicyQualifierInfo> set = new HashSet<PolicyQualifierInfo>();
        if (asn1Sequence == null) {
            return set;
        }
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final ASN1OutputStream create = ASN1OutputStream.create(byteArrayOutputStream);
        final Enumeration objects = asn1Sequence.getObjects();
        while (objects.hasMoreElements()) {
            try {
                create.writeObject(objects.nextElement());
                set.add(new PolicyQualifierInfo(byteArrayOutputStream.toByteArray()));
                byteArrayOutputStream.reset();
                continue;
            }
            catch (IOException ex) {
                throw new ExtCertPathValidatorException("Policy qualifier info cannot be decoded.", ex);
            }
            break;
        }
        return set;
    }
    
    private static BigInteger getSerialNumber(final Object o) {
        if (o instanceof X509Certificate) {
            return ((X509Certificate)o).getSerialNumber();
        }
        return ((X509AttributeCertificate)o).getSerialNumber();
    }
    
    protected static X500Principal getSubjectPrincipal(final X509Certificate x509Certificate) {
        return x509Certificate.getSubjectX500Principal();
    }
    
    protected static Date getValidDate(final PKIXParameters pkixParameters) {
        Date date;
        if ((date = pkixParameters.getDate()) == null) {
            date = new Date();
        }
        return date;
    }
    
    protected static boolean isAnyPolicy(final Set set) {
        return set == null || set.contains("2.5.29.32.0") || set.isEmpty();
    }
    
    static boolean isIndirectCRL(final X509CRL x509CRL) throws CRLException {
        try {
            final byte[] extensionValue = x509CRL.getExtensionValue(Extension.issuingDistributionPoint.getId());
            return extensionValue != null && IssuingDistributionPoint.getInstance(ASN1OctetString.getInstance(extensionValue).getOctets()).isIndirectCRL();
        }
        catch (Exception obj) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Exception reading IssuingDistributionPoint: ");
            sb.append(obj);
            throw new CRLException(sb.toString());
        }
    }
    
    protected static boolean isSelfIssued(final X509Certificate x509Certificate) {
        return x509Certificate.getSubjectDN().equals(x509Certificate.getIssuerDN());
    }
    
    protected static void prepareNextCertB1(final int n, final List[] array, final String anObject, final Map map, final X509Certificate x509Certificate) throws AnnotatedException, CertPathValidatorException {
        final Iterator iterator = array[n].iterator();
        while (true) {
            PKIXPolicyNode pkixPolicyNode;
            do {
                final boolean hasNext = iterator.hasNext();
                boolean contains = false;
                if (!hasNext) {
                    final boolean b = false;
                    if (!b) {
                        for (final PKIXPolicyNode pkixPolicyNode2 : array[n]) {
                            if ("2.5.29.32.0".equals(pkixPolicyNode2.getValidPolicy())) {
                                final Set set = null;
                                try {
                                    final Enumeration objects = ASN1Sequence.getInstance(getExtensionValue(x509Certificate, CertPathValidatorUtilities.CERTIFICATE_POLICIES)).getObjects();
                                    Set qualifierSet;
                                    while (true) {
                                        qualifierSet = set;
                                        if (objects.hasMoreElements()) {
                                            try {
                                                final PolicyInformation instance = PolicyInformation.getInstance(objects.nextElement());
                                                if (!"2.5.29.32.0".equals(instance.getPolicyIdentifier().getId())) {
                                                    continue;
                                                }
                                                try {
                                                    qualifierSet = getQualifierSet(instance.getPolicyQualifiers());
                                                }
                                                catch (CertPathValidatorException ex) {
                                                    throw new ExtCertPathValidatorException("Policy qualifier info set could not be built.", ex);
                                                }
                                            }
                                            catch (Exception ex2) {
                                                throw new AnnotatedException("Policy information cannot be decoded.", ex2);
                                            }
                                            break;
                                        }
                                        break;
                                    }
                                    if (x509Certificate.getCriticalExtensionOIDs() != null) {
                                        contains = x509Certificate.getCriticalExtensionOIDs().contains(CertPathValidatorUtilities.CERTIFICATE_POLICIES);
                                    }
                                    final PKIXPolicyNode pkixPolicyNode3 = (PKIXPolicyNode)pkixPolicyNode2.getParent();
                                    if ("2.5.29.32.0".equals(pkixPolicyNode3.getValidPolicy())) {
                                        final PKIXPolicyNode pkixPolicyNode4 = new PKIXPolicyNode(new ArrayList(), n, map.get(anObject), pkixPolicyNode3, qualifierSet, anObject, contains);
                                        pkixPolicyNode3.addChild(pkixPolicyNode4);
                                        array[n].add(pkixPolicyNode4);
                                        return;
                                    }
                                }
                                catch (Exception ex3) {
                                    throw new AnnotatedException("Certificate policies cannot be decoded.", ex3);
                                }
                                break;
                            }
                        }
                    }
                    return;
                }
                pkixPolicyNode = iterator.next();
            } while (!pkixPolicyNode.getValidPolicy().equals(anObject));
            final boolean b = true;
            pkixPolicyNode.setExpectedPolicies(map.get(anObject));
            continue;
        }
    }
    
    protected static PKIXPolicyNode prepareNextCertB2(final int n, final List[] array, final String anObject, PKIXPolicyNode removePolicyNode) {
        final Iterator iterator = array[n].iterator();
        PKIXPolicyNode pkixPolicyNode = removePolicyNode;
        while (iterator.hasNext()) {
            removePolicyNode = iterator.next();
            if (removePolicyNode.getValidPolicy().equals(anObject)) {
                ((PKIXPolicyNode)removePolicyNode.getParent()).removeChild(removePolicyNode);
                iterator.remove();
                int n2 = n - 1;
                removePolicyNode = pkixPolicyNode;
                while (true) {
                    pkixPolicyNode = removePolicyNode;
                    if (n2 < 0) {
                        break;
                    }
                    final List list = array[n2];
                    int n3 = 0;
                    PKIXPolicyNode pkixPolicyNode2;
                    while (true) {
                        pkixPolicyNode2 = removePolicyNode;
                        if (n3 >= list.size()) {
                            break;
                        }
                        final PKIXPolicyNode pkixPolicyNode3 = list.get(n3);
                        PKIXPolicyNode pkixPolicyNode4 = removePolicyNode;
                        if (!pkixPolicyNode3.hasChildren()) {
                            removePolicyNode = removePolicyNode(removePolicyNode, array, pkixPolicyNode3);
                            if ((pkixPolicyNode4 = removePolicyNode) == null) {
                                pkixPolicyNode2 = removePolicyNode;
                                break;
                            }
                        }
                        ++n3;
                        removePolicyNode = pkixPolicyNode4;
                    }
                    --n2;
                    removePolicyNode = pkixPolicyNode2;
                }
            }
        }
        return pkixPolicyNode;
    }
    
    protected static boolean processCertD1i(final int n, final List[] array, final ASN1ObjectIdentifier asn1ObjectIdentifier, final Set set) {
        final List list = array[n - 1];
        for (int i = 0; i < list.size(); ++i) {
            final PKIXPolicyNode pkixPolicyNode = list.get(i);
            if (pkixPolicyNode.getExpectedPolicies().contains(asn1ObjectIdentifier.getId())) {
                final HashSet<String> set2 = new HashSet<String>();
                set2.add(asn1ObjectIdentifier.getId());
                final PKIXPolicyNode pkixPolicyNode2 = new PKIXPolicyNode(new ArrayList(), n, set2, pkixPolicyNode, set, asn1ObjectIdentifier.getId(), false);
                pkixPolicyNode.addChild(pkixPolicyNode2);
                array[n].add(pkixPolicyNode2);
                return true;
            }
        }
        return false;
    }
    
    protected static void processCertD1ii(final int n, final List[] array, final ASN1ObjectIdentifier asn1ObjectIdentifier, final Set set) {
        final List list = array[n - 1];
        for (int i = 0; i < list.size(); ++i) {
            final PKIXPolicyNode pkixPolicyNode = list.get(i);
            if ("2.5.29.32.0".equals(pkixPolicyNode.getValidPolicy())) {
                final HashSet<String> set2 = new HashSet<String>();
                set2.add(asn1ObjectIdentifier.getId());
                final PKIXPolicyNode pkixPolicyNode2 = new PKIXPolicyNode(new ArrayList(), n, set2, pkixPolicyNode, set, asn1ObjectIdentifier.getId(), false);
                pkixPolicyNode.addChild(pkixPolicyNode2);
                array[n].add(pkixPolicyNode2);
                return;
            }
        }
    }
    
    protected static PKIXPolicyNode removePolicyNode(final PKIXPolicyNode pkixPolicyNode, final List[] array, final PKIXPolicyNode pkixPolicyNode2) {
        final PKIXPolicyNode pkixPolicyNode3 = (PKIXPolicyNode)pkixPolicyNode2.getParent();
        if (pkixPolicyNode == null) {
            return null;
        }
        if (pkixPolicyNode3 == null) {
            for (int i = 0; i < array.length; ++i) {
                array[i] = new ArrayList();
            }
            return null;
        }
        pkixPolicyNode3.removeChild(pkixPolicyNode2);
        removePolicyNodeRecurse(array, pkixPolicyNode2);
        return pkixPolicyNode;
    }
    
    private static void removePolicyNodeRecurse(final List[] array, final PKIXPolicyNode pkixPolicyNode) {
        array[pkixPolicyNode.getDepth()].remove(pkixPolicyNode);
        if (pkixPolicyNode.hasChildren()) {
            final Iterator children = pkixPolicyNode.getChildren();
            while (children.hasNext()) {
                removePolicyNodeRecurse(array, children.next());
            }
        }
    }
    
    protected static void verifyX509Certificate(final X509Certificate x509Certificate, final PublicKey publicKey, final String s) throws GeneralSecurityException {
        if (s == null) {
            x509Certificate.verify(publicKey);
            return;
        }
        x509Certificate.verify(publicKey, s);
    }
}
