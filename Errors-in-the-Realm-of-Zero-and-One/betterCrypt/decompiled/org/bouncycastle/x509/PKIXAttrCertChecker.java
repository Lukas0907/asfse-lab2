// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.x509;

import java.util.Set;
import java.security.cert.CertPathValidatorException;
import java.util.Collection;
import java.security.cert.CertPath;

public abstract class PKIXAttrCertChecker implements Cloneable
{
    public abstract void check(final X509AttributeCertificate p0, final CertPath p1, final CertPath p2, final Collection p3) throws CertPathValidatorException;
    
    public abstract Object clone();
    
    public abstract Set getSupportedExtensions();
}
