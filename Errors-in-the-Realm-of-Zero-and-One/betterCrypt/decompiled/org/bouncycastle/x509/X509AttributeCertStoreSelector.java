// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.x509;

import java.util.Collections;
import java.io.IOException;
import java.util.Iterator;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.x509.GeneralName;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.math.BigInteger;
import java.util.Date;
import org.bouncycastle.util.Selector;

public class X509AttributeCertStoreSelector implements Selector
{
    private X509AttributeCertificate attributeCert;
    private Date attributeCertificateValid;
    private AttributeCertificateHolder holder;
    private AttributeCertificateIssuer issuer;
    private BigInteger serialNumber;
    private Collection targetGroups;
    private Collection targetNames;
    
    public X509AttributeCertStoreSelector() {
        this.targetNames = new HashSet();
        this.targetGroups = new HashSet();
    }
    
    private Set extractGeneralNames(final Collection collection) throws IOException {
        if (collection != null && !collection.isEmpty()) {
            final HashSet<byte[]> set = new HashSet<byte[]>();
            for (Object o : collection) {
                if (!(o instanceof GeneralName)) {
                    o = GeneralName.getInstance(ASN1Primitive.fromByteArray((byte[])o));
                }
                set.add((byte[])o);
            }
            return set;
        }
        return new HashSet();
    }
    
    public void addTargetGroup(final GeneralName generalName) {
        this.targetGroups.add(generalName);
    }
    
    public void addTargetGroup(final byte[] array) throws IOException {
        this.addTargetGroup(GeneralName.getInstance(ASN1Primitive.fromByteArray(array)));
    }
    
    public void addTargetName(final GeneralName generalName) {
        this.targetNames.add(generalName);
    }
    
    public void addTargetName(final byte[] array) throws IOException {
        this.addTargetName(GeneralName.getInstance(ASN1Primitive.fromByteArray(array)));
    }
    
    @Override
    public Object clone() {
        final X509AttributeCertStoreSelector x509AttributeCertStoreSelector = new X509AttributeCertStoreSelector();
        x509AttributeCertStoreSelector.attributeCert = this.attributeCert;
        x509AttributeCertStoreSelector.attributeCertificateValid = this.getAttributeCertificateValid();
        x509AttributeCertStoreSelector.holder = this.holder;
        x509AttributeCertStoreSelector.issuer = this.issuer;
        x509AttributeCertStoreSelector.serialNumber = this.serialNumber;
        x509AttributeCertStoreSelector.targetGroups = this.getTargetGroups();
        x509AttributeCertStoreSelector.targetNames = this.getTargetNames();
        return x509AttributeCertStoreSelector;
    }
    
    public X509AttributeCertificate getAttributeCert() {
        return this.attributeCert;
    }
    
    public Date getAttributeCertificateValid() {
        final Date attributeCertificateValid = this.attributeCertificateValid;
        if (attributeCertificateValid != null) {
            return new Date(attributeCertificateValid.getTime());
        }
        return null;
    }
    
    public AttributeCertificateHolder getHolder() {
        return this.holder;
    }
    
    public AttributeCertificateIssuer getIssuer() {
        return this.issuer;
    }
    
    public BigInteger getSerialNumber() {
        return this.serialNumber;
    }
    
    public Collection getTargetGroups() {
        return Collections.unmodifiableCollection((Collection<?>)this.targetGroups);
    }
    
    public Collection getTargetNames() {
        return Collections.unmodifiableCollection((Collection<?>)this.targetNames);
    }
    
    @Override
    public boolean match(final Object p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: instanceof      Lorg/bouncycastle/x509/X509AttributeCertificate;
        //     4: ifne            9
        //     7: iconst_0       
        //     8: ireturn        
        //     9: aload_1        
        //    10: checkcast       Lorg/bouncycastle/x509/X509AttributeCertificate;
        //    13: astore_1       
        //    14: aload_0        
        //    15: getfield        org/bouncycastle/x509/X509AttributeCertStoreSelector.attributeCert:Lorg/bouncycastle/x509/X509AttributeCertificate;
        //    18: astore          6
        //    20: aload           6
        //    22: ifnull          36
        //    25: aload           6
        //    27: aload_1        
        //    28: invokevirtual   java/lang/Object.equals:(Ljava/lang/Object;)Z
        //    31: ifne            36
        //    34: iconst_0       
        //    35: ireturn        
        //    36: aload_0        
        //    37: getfield        org/bouncycastle/x509/X509AttributeCertStoreSelector.serialNumber:Ljava/math/BigInteger;
        //    40: ifnull          61
        //    43: aload_1        
        //    44: invokeinterface org/bouncycastle/x509/X509AttributeCertificate.getSerialNumber:()Ljava/math/BigInteger;
        //    49: aload_0        
        //    50: getfield        org/bouncycastle/x509/X509AttributeCertStoreSelector.serialNumber:Ljava/math/BigInteger;
        //    53: invokevirtual   java/math/BigInteger.equals:(Ljava/lang/Object;)Z
        //    56: ifne            61
        //    59: iconst_0       
        //    60: ireturn        
        //    61: aload_0        
        //    62: getfield        org/bouncycastle/x509/X509AttributeCertStoreSelector.holder:Lorg/bouncycastle/x509/AttributeCertificateHolder;
        //    65: ifnull          86
        //    68: aload_1        
        //    69: invokeinterface org/bouncycastle/x509/X509AttributeCertificate.getHolder:()Lorg/bouncycastle/x509/AttributeCertificateHolder;
        //    74: aload_0        
        //    75: getfield        org/bouncycastle/x509/X509AttributeCertStoreSelector.holder:Lorg/bouncycastle/x509/AttributeCertificateHolder;
        //    78: invokevirtual   org/bouncycastle/x509/AttributeCertificateHolder.equals:(Ljava/lang/Object;)Z
        //    81: ifne            86
        //    84: iconst_0       
        //    85: ireturn        
        //    86: aload_0        
        //    87: getfield        org/bouncycastle/x509/X509AttributeCertStoreSelector.issuer:Lorg/bouncycastle/x509/AttributeCertificateIssuer;
        //    90: ifnull          111
        //    93: aload_1        
        //    94: invokeinterface org/bouncycastle/x509/X509AttributeCertificate.getIssuer:()Lorg/bouncycastle/x509/AttributeCertificateIssuer;
        //    99: aload_0        
        //   100: getfield        org/bouncycastle/x509/X509AttributeCertStoreSelector.issuer:Lorg/bouncycastle/x509/AttributeCertificateIssuer;
        //   103: invokevirtual   org/bouncycastle/x509/AttributeCertificateIssuer.equals:(Ljava/lang/Object;)Z
        //   106: ifne            111
        //   109: iconst_0       
        //   110: ireturn        
        //   111: aload_0        
        //   112: getfield        org/bouncycastle/x509/X509AttributeCertStoreSelector.attributeCertificateValid:Ljava/util/Date;
        //   115: astore          6
        //   117: aload           6
        //   119: ifnull          133
        //   122: aload_1        
        //   123: aload           6
        //   125: invokeinterface org/bouncycastle/x509/X509AttributeCertificate.checkValidity:(Ljava/util/Date;)V
        //   130: goto            133
        //   133: aload_0        
        //   134: getfield        org/bouncycastle/x509/X509AttributeCertStoreSelector.targetNames:Ljava/util/Collection;
        //   137: invokeinterface java/util/Collection.isEmpty:()Z
        //   142: ifeq            157
        //   145: aload_0        
        //   146: getfield        org/bouncycastle/x509/X509AttributeCertStoreSelector.targetGroups:Ljava/util/Collection;
        //   149: invokeinterface java/util/Collection.isEmpty:()Z
        //   154: ifne            399
        //   157: aload_1        
        //   158: getstatic       org/bouncycastle/asn1/x509/Extension.targetInformation:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //   161: invokevirtual   org/bouncycastle/asn1/ASN1ObjectIdentifier.getId:()Ljava/lang/String;
        //   164: invokeinterface org/bouncycastle/x509/X509AttributeCertificate.getExtensionValue:(Ljava/lang/String;)[B
        //   169: astore_1       
        //   170: aload_1        
        //   171: ifnull          399
        //   174: new             Lorg/bouncycastle/asn1/ASN1InputStream;
        //   177: dup            
        //   178: aload_1        
        //   179: invokestatic    org/bouncycastle/asn1/DEROctetString.fromByteArray:([B)Lorg/bouncycastle/asn1/ASN1Primitive;
        //   182: checkcast       Lorg/bouncycastle/asn1/DEROctetString;
        //   185: invokevirtual   org/bouncycastle/asn1/DEROctetString.getOctets:()[B
        //   188: invokespecial   org/bouncycastle/asn1/ASN1InputStream.<init>:([B)V
        //   191: invokevirtual   org/bouncycastle/asn1/ASN1InputStream.readObject:()Lorg/bouncycastle/asn1/ASN1Primitive;
        //   194: invokestatic    org/bouncycastle/asn1/x509/TargetInformation.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/TargetInformation;
        //   197: astore_1       
        //   198: aload_1        
        //   199: invokevirtual   org/bouncycastle/asn1/x509/TargetInformation.getTargetsObjects:()[Lorg/bouncycastle/asn1/x509/Targets;
        //   202: astore_1       
        //   203: aload_0        
        //   204: getfield        org/bouncycastle/x509/X509AttributeCertStoreSelector.targetNames:Ljava/util/Collection;
        //   207: invokeinterface java/util/Collection.isEmpty:()Z
        //   212: ifne            301
        //   215: iconst_0       
        //   216: istore_2       
        //   217: iload_2        
        //   218: istore_3       
        //   219: iload_2        
        //   220: aload_1        
        //   221: arraylength    
        //   222: if_icmpge       295
        //   225: aload_1        
        //   226: iload_2        
        //   227: aaload         
        //   228: invokevirtual   org/bouncycastle/asn1/x509/Targets.getTargets:()[Lorg/bouncycastle/asn1/x509/Target;
        //   231: astore          6
        //   233: iconst_0       
        //   234: istore          5
        //   236: iload_3        
        //   237: istore          4
        //   239: iload           5
        //   241: aload           6
        //   243: arraylength    
        //   244: if_icmpge       285
        //   247: aload_0        
        //   248: getfield        org/bouncycastle/x509/X509AttributeCertStoreSelector.targetNames:Ljava/util/Collection;
        //   251: aload           6
        //   253: iload           5
        //   255: aaload         
        //   256: invokevirtual   org/bouncycastle/asn1/x509/Target.getTargetName:()Lorg/bouncycastle/asn1/x509/GeneralName;
        //   259: invokestatic    org/bouncycastle/asn1/x509/GeneralName.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/GeneralName;
        //   262: invokeinterface java/util/Collection.contains:(Ljava/lang/Object;)Z
        //   267: ifeq            276
        //   270: iconst_1       
        //   271: istore          4
        //   273: goto            285
        //   276: iload           5
        //   278: iconst_1       
        //   279: iadd           
        //   280: istore          5
        //   282: goto            236
        //   285: iload_2        
        //   286: iconst_1       
        //   287: iadd           
        //   288: istore_2       
        //   289: iload           4
        //   291: istore_3       
        //   292: goto            219
        //   295: iload_3        
        //   296: ifne            301
        //   299: iconst_0       
        //   300: ireturn        
        //   301: aload_0        
        //   302: getfield        org/bouncycastle/x509/X509AttributeCertStoreSelector.targetGroups:Ljava/util/Collection;
        //   305: invokeinterface java/util/Collection.isEmpty:()Z
        //   310: ifne            399
        //   313: iconst_0       
        //   314: istore_2       
        //   315: iload_2        
        //   316: istore_3       
        //   317: iload_2        
        //   318: aload_1        
        //   319: arraylength    
        //   320: if_icmpge       393
        //   323: aload_1        
        //   324: iload_2        
        //   325: aaload         
        //   326: invokevirtual   org/bouncycastle/asn1/x509/Targets.getTargets:()[Lorg/bouncycastle/asn1/x509/Target;
        //   329: astore          6
        //   331: iconst_0       
        //   332: istore          5
        //   334: iload_3        
        //   335: istore          4
        //   337: iload           5
        //   339: aload           6
        //   341: arraylength    
        //   342: if_icmpge       383
        //   345: aload_0        
        //   346: getfield        org/bouncycastle/x509/X509AttributeCertStoreSelector.targetGroups:Ljava/util/Collection;
        //   349: aload           6
        //   351: iload           5
        //   353: aaload         
        //   354: invokevirtual   org/bouncycastle/asn1/x509/Target.getTargetGroup:()Lorg/bouncycastle/asn1/x509/GeneralName;
        //   357: invokestatic    org/bouncycastle/asn1/x509/GeneralName.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/GeneralName;
        //   360: invokeinterface java/util/Collection.contains:(Ljava/lang/Object;)Z
        //   365: ifeq            374
        //   368: iconst_1       
        //   369: istore          4
        //   371: goto            383
        //   374: iload           5
        //   376: iconst_1       
        //   377: iadd           
        //   378: istore          5
        //   380: goto            334
        //   383: iload_2        
        //   384: iconst_1       
        //   385: iadd           
        //   386: istore_2       
        //   387: iload           4
        //   389: istore_3       
        //   390: goto            317
        //   393: iload_3        
        //   394: ifne            399
        //   397: iconst_0       
        //   398: ireturn        
        //   399: iconst_1       
        //   400: ireturn        
        //   401: astore_1       
        //   402: iconst_0       
        //   403: ireturn        
        //   404: astore_1       
        //   405: iconst_0       
        //   406: ireturn        
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                                
        //  -----  -----  -----  -----  ----------------------------------------------------
        //  122    130    401    404    Ljava/security/cert/CertificateExpiredException;
        //  122    130    401    404    Ljava/security/cert/CertificateNotYetValidException;
        //  174    198    404    407    Ljava/io/IOException;
        //  174    198    404    407    Ljava/lang/IllegalArgumentException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0219:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void setAttributeCert(final X509AttributeCertificate attributeCert) {
        this.attributeCert = attributeCert;
    }
    
    public void setAttributeCertificateValid(final Date date) {
        if (date != null) {
            this.attributeCertificateValid = new Date(date.getTime());
            return;
        }
        this.attributeCertificateValid = null;
    }
    
    public void setHolder(final AttributeCertificateHolder holder) {
        this.holder = holder;
    }
    
    public void setIssuer(final AttributeCertificateIssuer issuer) {
        this.issuer = issuer;
    }
    
    public void setSerialNumber(final BigInteger serialNumber) {
        this.serialNumber = serialNumber;
    }
    
    public void setTargetGroups(final Collection collection) throws IOException {
        this.targetGroups = this.extractGeneralNames(collection);
    }
    
    public void setTargetNames(final Collection collection) throws IOException {
        this.targetNames = this.extractGeneralNames(collection);
    }
}
