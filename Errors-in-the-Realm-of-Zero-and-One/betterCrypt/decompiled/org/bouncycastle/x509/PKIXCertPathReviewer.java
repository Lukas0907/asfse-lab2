// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.x509;

import javax.security.auth.x500.X500Principal;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.ASN1OctetString;
import java.security.cert.X509CertSelector;
import org.bouncycastle.asn1.x509.AccessDescription;
import org.bouncycastle.asn1.x509.AuthorityInformationAccess;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import java.util.ArrayList;
import java.util.Vector;
import org.bouncycastle.i18n.filter.TrustedInput;
import org.bouncycastle.asn1.x509.qualified.MonetaryValue;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.x509.qualified.QCStatement;
import java.security.cert.CertificateFactory;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.X509CRL;
import java.math.BigInteger;
import org.bouncycastle.util.Integers;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.GeneralSubtree;
import org.bouncycastle.asn1.x509.NameConstraints;
import java.io.IOException;
import org.bouncycastle.jce.provider.AnnotatedException;
import org.bouncycastle.jce.provider.PKIXNameConstraintValidatorException;
import org.bouncycastle.i18n.filter.UntrustedInput;
import org.bouncycastle.asn1.x509.GeneralName;
import java.security.cert.X509Extension;
import org.bouncycastle.asn1.ASN1Sequence;
import java.io.InputStream;
import org.bouncycastle.asn1.ASN1InputStream;
import java.io.ByteArrayInputStream;
import org.bouncycastle.jce.provider.PKIXNameConstraintValidator;
import java.util.Set;
import java.util.Iterator;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import java.security.cert.CertPathValidatorException;
import org.bouncycastle.i18n.ErrorBundle;
import java.util.Collection;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.security.cert.PKIXCertPathChecker;
import java.net.InetAddress;
import org.bouncycastle.asn1.x509.Extension;
import java.util.Date;
import java.security.cert.TrustAnchor;
import java.security.PublicKey;
import java.security.cert.PolicyNode;
import java.security.cert.PKIXParameters;
import java.util.List;
import java.security.cert.CertPath;

public class PKIXCertPathReviewer extends CertPathValidatorUtilities
{
    private static final String AUTH_INFO_ACCESS;
    private static final String CRL_DIST_POINTS;
    private static final String QC_STATEMENT;
    private static final String RESOURCE_NAME = "org.bouncycastle.x509.CertPathReviewerMessages";
    protected CertPath certPath;
    protected List certs;
    protected List[] errors;
    private boolean initialized;
    protected int n;
    protected List[] notifications;
    protected PKIXParameters pkixParams;
    protected PolicyNode policyTree;
    protected PublicKey subjectPublicKey;
    protected TrustAnchor trustAnchor;
    protected Date validDate;
    
    static {
        QC_STATEMENT = Extension.qCStatements.getId();
        CRL_DIST_POINTS = Extension.cRLDistributionPoints.getId();
        AUTH_INFO_ACCESS = Extension.authorityInfoAccess.getId();
    }
    
    public PKIXCertPathReviewer() {
    }
    
    public PKIXCertPathReviewer(final CertPath certPath, final PKIXParameters pkixParameters) throws CertPathReviewerException {
        this.init(certPath, pkixParameters);
    }
    
    private String IPtoString(final byte[] addr) {
        while (true) {
            try {
                return InetAddress.getByAddress(addr).getHostAddress();
                final StringBuffer sb = new StringBuffer();
                int n = 0;
                // iftrue(Label_0055:, n == addr.length)
                while (true) {
                    sb.append(Integer.toHexString(addr[n] & 0xFF));
                    sb.append(' ');
                    ++n;
                    continue;
                }
                Label_0055: {
                    return sb.toString();
                }
            }
            catch (Exception ex) {
                continue;
            }
            break;
        }
    }
    
    private void checkCriticalExtensions() {
        final List<PKIXCertPathChecker> certPathCheckers = this.pkixParams.getCertPathCheckers();
        final Iterator<PKIXCertPathChecker> iterator = certPathCheckers.iterator();
    Label_0455:
        while (true) {
            try {
            Label_0055_Outer:
                while (true) {
                    while (true) {
                        int n = 0;
                        Label_0468: {
                            try {
                                while (iterator.hasNext()) {
                                    iterator.next().init(false);
                                }
                                n = this.certs.size() - 1;
                                if (n < 0) {
                                    break;
                                }
                                final X509Certificate x509Certificate = this.certs.get(n);
                                final Set<String> criticalExtensionOIDs = x509Certificate.getCriticalExtensionOIDs();
                                if (criticalExtensionOIDs == null) {
                                    break Label_0468;
                                }
                                if (criticalExtensionOIDs.isEmpty()) {
                                    break Label_0468;
                                }
                                criticalExtensionOIDs.remove(PKIXCertPathReviewer.KEY_USAGE);
                                criticalExtensionOIDs.remove(PKIXCertPathReviewer.CERTIFICATE_POLICIES);
                                criticalExtensionOIDs.remove(PKIXCertPathReviewer.POLICY_MAPPINGS);
                                criticalExtensionOIDs.remove(PKIXCertPathReviewer.INHIBIT_ANY_POLICY);
                                criticalExtensionOIDs.remove(PKIXCertPathReviewer.ISSUING_DISTRIBUTION_POINT);
                                criticalExtensionOIDs.remove(PKIXCertPathReviewer.DELTA_CRL_INDICATOR);
                                criticalExtensionOIDs.remove(PKIXCertPathReviewer.POLICY_CONSTRAINTS);
                                criticalExtensionOIDs.remove(PKIXCertPathReviewer.BASIC_CONSTRAINTS);
                                criticalExtensionOIDs.remove(PKIXCertPathReviewer.SUBJECT_ALTERNATIVE_NAME);
                                criticalExtensionOIDs.remove(PKIXCertPathReviewer.NAME_CONSTRAINTS);
                                if (criticalExtensionOIDs.contains(PKIXCertPathReviewer.QC_STATEMENT) && this.processQcStatements(x509Certificate, n)) {
                                    criticalExtensionOIDs.remove(PKIXCertPathReviewer.QC_STATEMENT);
                                }
                                final Iterator<PKIXCertPathChecker> iterator2 = certPathCheckers.iterator();
                                while (iterator2.hasNext()) {
                                    try {
                                        iterator2.next().check(x509Certificate, criticalExtensionOIDs);
                                        continue Label_0055_Outer;
                                    }
                                    catch (CertPathValidatorException ex) {
                                        throw new CertPathReviewerException(new ErrorBundle("org.bouncycastle.x509.CertPathReviewerMessages", "CertPathReviewer.criticalExtensionError", new Object[] { ex.getMessage(), ex, ex.getClass().getName() }), ex.getCause(), this.certPath, n);
                                    }
                                    break;
                                }
                                if (!criticalExtensionOIDs.isEmpty()) {
                                    final Iterator<String> iterator3 = criticalExtensionOIDs.iterator();
                                    while (iterator3.hasNext()) {
                                        this.addError(new ErrorBundle("org.bouncycastle.x509.CertPathReviewerMessages", "CertPathReviewer.unknownCriticalExt", new Object[] { new ASN1ObjectIdentifier(iterator3.next()) }), n);
                                    }
                                }
                                break Label_0468;
                            }
                            catch (CertPathValidatorException ex2) {
                                throw new CertPathReviewerException(new ErrorBundle("org.bouncycastle.x509.CertPathReviewerMessages", "CertPathReviewer.certPathCheckerError", new Object[] { ex2.getMessage(), ex2, ex2.getClass().getName() }), ex2);
                            }
                            break Label_0455;
                        }
                        --n;
                        continue;
                    }
                    final CertPathReviewerException ex3;
                    this.addError(ex3.getErrorMessage(), ex3.getIndex());
                    break;
                }
                return;
            }
            catch (CertPathReviewerException ex4) {}
            final CertPathReviewerException ex4;
            final CertPathReviewerException ex3 = ex4;
            continue Label_0455;
        }
    }
    
    private void checkNameConstraints() {
        while (true) {
            final PKIXNameConstraintValidator pkixNameConstraintValidator = new PKIXNameConstraintValidator();
            while (true) {
                int n = 0;
                Label_0514: {
                    try {
                        n = this.certs.size() - 1;
                        if (n > 0) {
                            final int n2 = this.n;
                            final X509Certificate x509Certificate = this.certs.get(n);
                            final boolean selfIssued = CertPathValidatorUtilities.isSelfIssued(x509Certificate);
                            final int n3 = 0;
                            Label_0387: {
                                if (selfIssued) {
                                    break Label_0387;
                                }
                                Object o = CertPathValidatorUtilities.getSubjectPrincipal(x509Certificate);
                                final ASN1InputStream asn1InputStream = new ASN1InputStream(new ByteArrayInputStream(((X500Principal)o).getEncoded()));
                                try {
                                    final ASN1Sequence asn1Sequence = (ASN1Sequence)asn1InputStream.readObject();
                                    try {
                                        pkixNameConstraintValidator.checkPermittedDN(asn1Sequence);
                                        try {
                                            pkixNameConstraintValidator.checkExcludedDN(asn1Sequence);
                                            try {
                                                final ASN1Sequence asn1Sequence2 = (ASN1Sequence)CertPathValidatorUtilities.getExtensionValue(x509Certificate, PKIXCertPathReviewer.SUBJECT_ALTERNATIVE_NAME);
                                                if (asn1Sequence2 != null) {
                                                    int i = 0;
                                                    while (i < asn1Sequence2.size()) {
                                                        o = GeneralName.getInstance(asn1Sequence2.getObjectAt(i));
                                                        try {
                                                            pkixNameConstraintValidator.checkPermitted((GeneralName)o);
                                                            pkixNameConstraintValidator.checkExcluded((GeneralName)o);
                                                            ++i;
                                                        }
                                                        catch (PKIXNameConstraintValidatorException ex) {
                                                            throw new CertPathReviewerException(new ErrorBundle("org.bouncycastle.x509.CertPathReviewerMessages", "CertPathReviewer.notPermittedEmail", new Object[] { new UntrustedInput(o) }), ex, this.certPath, n);
                                                        }
                                                    }
                                                    break Label_0387;
                                                }
                                                break Label_0387;
                                            }
                                            catch (AnnotatedException ex3) {
                                                final String s = "org.bouncycastle.x509.CertPathReviewerMessages";
                                                final String s2 = "CertPathReviewer.subjAltNameExtError";
                                                final ErrorBundle errorBundle = new ErrorBundle(s, s2);
                                                final AnnotatedException ex2 = ex3;
                                                final PKIXCertPathReviewer pkixCertPathReviewer = this;
                                                final CertPath certPath = pkixCertPathReviewer.certPath;
                                                final int n4 = n;
                                                throw new CertPathReviewerException(errorBundle, ex2, certPath, n4);
                                            }
                                        }
                                        catch (PKIXNameConstraintValidatorException ex6) {
                                            final String s3 = "org.bouncycastle.x509.CertPathReviewerMessages";
                                            final String s4 = "CertPathReviewer.excludedDN";
                                            final int n5 = 1;
                                            final Object[] array = new Object[n5];
                                            final int n6 = 0;
                                            final GeneralName generalName = (GeneralName)o;
                                            final String s5 = ((X500Principal)generalName).getName();
                                            final UntrustedInput untrustedInput = new UntrustedInput(s5);
                                            array[n6] = untrustedInput;
                                            final ErrorBundle errorBundle2 = new ErrorBundle(s3, s4, array);
                                            final PKIXNameConstraintValidatorException ex5 = ex6;
                                            final PKIXCertPathReviewer pkixCertPathReviewer2 = this;
                                            final CertPath certPath2 = pkixCertPathReviewer2.certPath;
                                            final int n7 = n;
                                            throw new CertPathReviewerException(errorBundle2, ex5, certPath2, n7);
                                        }
                                    }
                                    catch (PKIXNameConstraintValidatorException ex9) {
                                        final String s6 = "org.bouncycastle.x509.CertPathReviewerMessages";
                                        final String s7 = "CertPathReviewer.notPermittedDN";
                                        final int n8 = 1;
                                        final Object[] array2 = new Object[n8];
                                        final int n9 = 0;
                                        final GeneralName generalName2 = (GeneralName)o;
                                        final String s8 = ((X500Principal)generalName2).getName();
                                        final UntrustedInput untrustedInput2 = new UntrustedInput(s8);
                                        array2[n9] = untrustedInput2;
                                        final ErrorBundle errorBundle3 = new ErrorBundle(s6, s7, array2);
                                        final PKIXNameConstraintValidatorException ex8 = ex9;
                                        final PKIXCertPathReviewer pkixCertPathReviewer3 = this;
                                        final CertPath certPath3 = pkixCertPathReviewer3.certPath;
                                        final int n10 = n;
                                        throw new CertPathReviewerException(errorBundle3, ex8, certPath3, n10);
                                    }
                                }
                                catch (IOException ex12) {
                                    final String s9 = "org.bouncycastle.x509.CertPathReviewerMessages";
                                    final String s10 = "CertPathReviewer.ncSubjectNameError";
                                    final int n11 = 1;
                                    final Object[] array3 = new Object[n11];
                                    final int n12 = 0;
                                    final GeneralName generalName3 = (GeneralName)o;
                                    final UntrustedInput untrustedInput3 = new UntrustedInput(generalName3);
                                    array3[n12] = untrustedInput3;
                                    final ErrorBundle errorBundle4 = new ErrorBundle(s9, s10, array3);
                                    final IOException ex11 = ex12;
                                    final PKIXCertPathReviewer pkixCertPathReviewer4 = this;
                                    final CertPath certPath4 = pkixCertPathReviewer4.certPath;
                                    final int n13 = n;
                                    throw new CertPathReviewerException(errorBundle4, ex11, certPath4, n13);
                                }
                                try {
                                    final String s = "org.bouncycastle.x509.CertPathReviewerMessages";
                                    final String s2 = "CertPathReviewer.subjAltNameExtError";
                                    final ErrorBundle errorBundle = new ErrorBundle(s, s2);
                                    final AnnotatedException ex3;
                                    final AnnotatedException ex2 = ex3;
                                    final PKIXCertPathReviewer pkixCertPathReviewer = this;
                                    final CertPath certPath = pkixCertPathReviewer.certPath;
                                    final int n4 = n;
                                    throw new CertPathReviewerException(errorBundle, ex2, certPath, n4);
                                    try {
                                        final String s3 = "org.bouncycastle.x509.CertPathReviewerMessages";
                                        final String s4 = "CertPathReviewer.excludedDN";
                                        final int n5 = 1;
                                        final Object[] array = new Object[n5];
                                        final int n6 = 0;
                                        final GeneralName generalName = (GeneralName)o;
                                        final String s5 = ((X500Principal)generalName).getName();
                                        final UntrustedInput untrustedInput = new UntrustedInput(s5);
                                        array[n6] = untrustedInput;
                                        final ErrorBundle errorBundle2 = new ErrorBundle(s3, s4, array);
                                        final PKIXNameConstraintValidatorException ex6;
                                        final PKIXNameConstraintValidatorException ex5 = ex6;
                                        final PKIXCertPathReviewer pkixCertPathReviewer2 = this;
                                        final CertPath certPath2 = pkixCertPathReviewer2.certPath;
                                        final int n7 = n;
                                        throw new CertPathReviewerException(errorBundle2, ex5, certPath2, n7);
                                        try {
                                            final String s6 = "org.bouncycastle.x509.CertPathReviewerMessages";
                                            final String s7 = "CertPathReviewer.notPermittedDN";
                                            final int n8 = 1;
                                            final Object[] array2 = new Object[n8];
                                            final int n9 = 0;
                                            final GeneralName generalName2 = (GeneralName)o;
                                            final String s8 = ((X500Principal)generalName2).getName();
                                            final UntrustedInput untrustedInput2 = new UntrustedInput(s8);
                                            array2[n9] = untrustedInput2;
                                            final ErrorBundle errorBundle3 = new ErrorBundle(s6, s7, array2);
                                            final PKIXNameConstraintValidatorException ex9;
                                            final PKIXNameConstraintValidatorException ex8 = ex9;
                                            final PKIXCertPathReviewer pkixCertPathReviewer3 = this;
                                            final CertPath certPath3 = pkixCertPathReviewer3.certPath;
                                            final int n10 = n;
                                            throw new CertPathReviewerException(errorBundle3, ex8, certPath3, n10);
                                            try {
                                                final String s9 = "org.bouncycastle.x509.CertPathReviewerMessages";
                                                final String s10 = "CertPathReviewer.ncSubjectNameError";
                                                final int n11 = 1;
                                                final Object[] array3 = new Object[n11];
                                                final int n12 = 0;
                                                final GeneralName generalName3 = (GeneralName)o;
                                                final UntrustedInput untrustedInput3 = new UntrustedInput(generalName3);
                                                array3[n12] = untrustedInput3;
                                                final ErrorBundle errorBundle4 = new ErrorBundle(s9, s10, array3);
                                                final IOException ex12;
                                                final IOException ex11 = ex12;
                                                final PKIXCertPathReviewer pkixCertPathReviewer4 = this;
                                                final CertPath certPath4 = pkixCertPathReviewer4.certPath;
                                                final int n13 = n;
                                                throw new CertPathReviewerException(errorBundle4, ex11, certPath4, n13);
                                                try {
                                                    final ASN1Sequence asn1Sequence3 = (ASN1Sequence)CertPathValidatorUtilities.getExtensionValue(x509Certificate, PKIXCertPathReviewer.NAME_CONSTRAINTS);
                                                    if (asn1Sequence3 == null) {
                                                        break Label_0514;
                                                    }
                                                    final NameConstraints instance = NameConstraints.getInstance(asn1Sequence3);
                                                    final GeneralSubtree[] permittedSubtrees = instance.getPermittedSubtrees();
                                                    if (permittedSubtrees != null) {
                                                        pkixNameConstraintValidator.intersectPermittedSubtree(permittedSubtrees);
                                                    }
                                                    final GeneralSubtree[] excludedSubtrees = instance.getExcludedSubtrees();
                                                    if (excludedSubtrees != null) {
                                                        for (int j = n3; j != excludedSubtrees.length; ++j) {
                                                            pkixNameConstraintValidator.addExcludedSubtree(excludedSubtrees[j]);
                                                        }
                                                    }
                                                    break Label_0514;
                                                }
                                                catch (AnnotatedException ex14) {
                                                    throw new CertPathReviewerException(new ErrorBundle("org.bouncycastle.x509.CertPathReviewerMessages", "CertPathReviewer.ncExtError"), ex14, this.certPath, n);
                                                }
                                            }
                                            catch (CertPathReviewerException ex15) {
                                                this.addError(ex15.getErrorMessage(), ex15.getIndex());
                                            }
                                        }
                                        catch (CertPathReviewerException ex16) {}
                                    }
                                    catch (CertPathReviewerException ex17) {}
                                }
                                catch (CertPathReviewerException ex18) {}
                            }
                        }
                    }
                    catch (CertPathReviewerException ex19) {}
                    break;
                }
                --n;
                continue;
            }
        }
    }
    
    private void checkPathLength() {
        int n = this.n;
        int n2 = this.certs.size() - 1;
        int n3 = 0;
        while (true) {
            Label_0174: {
                if (n2 <= 0) {
                    break Label_0174;
                }
                final int n4 = this.n;
                final X509Certificate x509Certificate = this.certs.get(n2);
                int n5 = n3;
                int n6 = n;
                if (!CertPathValidatorUtilities.isSelfIssued(x509Certificate)) {
                    if (n <= 0) {
                        this.addError(new ErrorBundle("org.bouncycastle.x509.CertPathReviewerMessages", "CertPathReviewer.pathLengthExtended"));
                    }
                    n6 = n - 1;
                    n5 = n3 + 1;
                }
            Label_0104_Outer:
                while (true) {
                    while (true) {
                        try {
                            BasicConstraints instance = BasicConstraints.getInstance(CertPathValidatorUtilities.getExtensionValue(x509Certificate, PKIXCertPathReviewer.BASIC_CONSTRAINTS));
                            while (true) {
                                n = n6;
                                if (instance != null) {
                                    final BigInteger pathLenConstraint = instance.getPathLenConstraint();
                                    n = n6;
                                    if (pathLenConstraint != null) {
                                        final int intValue = pathLenConstraint.intValue();
                                        if (intValue < (n = n6)) {
                                            n = intValue;
                                        }
                                    }
                                }
                                --n2;
                                n3 = n5;
                                break;
                                this.addError(new ErrorBundle("org.bouncycastle.x509.CertPathReviewerMessages", "CertPathReviewer.processLengthConstError"), n2);
                                instance = null;
                                continue Label_0104_Outer;
                            }
                            this.addNotification(new ErrorBundle("org.bouncycastle.x509.CertPathReviewerMessages", "CertPathReviewer.totalPathLength", new Object[] { Integers.valueOf(n3) }));
                            return;
                        }
                        catch (AnnotatedException ex) {}
                        continue;
                    }
                }
            }
        }
    }
    
    private void checkPolicy() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: astore          11
        //     5: aload_0        
        //     6: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.pkixParams:Ljava/security/cert/PKIXParameters;
        //     9: invokevirtual   java/security/cert/PKIXParameters.getInitialPolicies:()Ljava/util/Set;
        //    12: astore          12
        //    14: aload_0        
        //    15: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.n:I
        //    18: iconst_1       
        //    19: iadd           
        //    20: anewarray       Ljava/util/ArrayList;
        //    23: astore          18
        //    25: iconst_0       
        //    26: istore_1       
        //    27: iload_1        
        //    28: aload           18
        //    30: arraylength    
        //    31: if_icmpge       52
        //    34: aload           18
        //    36: iload_1        
        //    37: new             Ljava/util/ArrayList;
        //    40: dup            
        //    41: invokespecial   java/util/ArrayList.<init>:()V
        //    44: aastore        
        //    45: iload_1        
        //    46: iconst_1       
        //    47: iadd           
        //    48: istore_1       
        //    49: goto            27
        //    52: new             Ljava/util/HashSet;
        //    55: dup            
        //    56: invokespecial   java/util/HashSet.<init>:()V
        //    59: astore          10
        //    61: aload           10
        //    63: ldc_w           "2.5.29.32.0"
        //    66: invokeinterface java/util/Set.add:(Ljava/lang/Object;)Z
        //    71: pop            
        //    72: new             Lorg/bouncycastle/jce/provider/PKIXPolicyNode;
        //    75: dup            
        //    76: new             Ljava/util/ArrayList;
        //    79: dup            
        //    80: invokespecial   java/util/ArrayList.<init>:()V
        //    83: iconst_0       
        //    84: aload           10
        //    86: aconst_null    
        //    87: new             Ljava/util/HashSet;
        //    90: dup            
        //    91: invokespecial   java/util/HashSet.<init>:()V
        //    94: ldc_w           "2.5.29.32.0"
        //    97: iconst_0       
        //    98: invokespecial   org/bouncycastle/jce/provider/PKIXPolicyNode.<init>:(Ljava/util/List;ILjava/util/Set;Ljava/security/cert/PolicyNode;Ljava/util/Set;Ljava/lang/String;Z)V
        //   101: astore          10
        //   103: aload           18
        //   105: iconst_0       
        //   106: aaload         
        //   107: aload           10
        //   109: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   114: pop            
        //   115: aload_0        
        //   116: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.pkixParams:Ljava/security/cert/PKIXParameters;
        //   119: invokevirtual   java/security/cert/PKIXParameters.isExplicitPolicyRequired:()Z
        //   122: ifeq            130
        //   125: iconst_0       
        //   126: istore_1       
        //   127: goto            137
        //   130: aload_0        
        //   131: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.n:I
        //   134: iconst_1       
        //   135: iadd           
        //   136: istore_1       
        //   137: aload_0        
        //   138: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.pkixParams:Ljava/security/cert/PKIXParameters;
        //   141: invokevirtual   java/security/cert/PKIXParameters.isAnyPolicyInhibited:()Z
        //   144: ifeq            152
        //   147: iconst_0       
        //   148: istore_2       
        //   149: goto            159
        //   152: aload_0        
        //   153: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.n:I
        //   156: iconst_1       
        //   157: iadd           
        //   158: istore_2       
        //   159: aload_0        
        //   160: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.pkixParams:Ljava/security/cert/PKIXParameters;
        //   163: invokevirtual   java/security/cert/PKIXParameters.isPolicyMappingInhibited:()Z
        //   166: ifeq            174
        //   169: iconst_0       
        //   170: istore_3       
        //   171: goto            181
        //   174: aload_0        
        //   175: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.n:I
        //   178: iconst_1       
        //   179: iadd           
        //   180: istore_3       
        //   181: aload_0        
        //   182: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.certs:Ljava/util/List;
        //   185: invokeinterface java/util/List.size:()I
        //   190: istore          4
        //   192: iload           4
        //   194: iconst_1       
        //   195: isub           
        //   196: istore          6
        //   198: aconst_null    
        //   199: astore          13
        //   201: aconst_null    
        //   202: astore          14
        //   204: iload           6
        //   206: iflt            1838
        //   209: aload_0        
        //   210: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.n:I
        //   213: iload           6
        //   215: isub           
        //   216: istore          8
        //   218: aload_0        
        //   219: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.certs:Ljava/util/List;
        //   222: iload           6
        //   224: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   229: checkcast       Ljava/security/cert/X509Certificate;
        //   232: astore          17
        //   234: aload           17
        //   236: getstatic       org/bouncycastle/x509/PKIXCertPathReviewer.CERTIFICATE_POLICIES:Ljava/lang/String;
        //   239: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getExtensionValue:(Ljava/security/cert/X509Extension;Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1Primitive;
        //   242: checkcast       Lorg/bouncycastle/asn1/ASN1Sequence;
        //   245: astore          19
        //   247: aload           19
        //   249: ifnull          2701
        //   252: aload           10
        //   254: ifnull          2701
        //   257: aload           19
        //   259: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.getObjects:()Ljava/util/Enumeration;
        //   262: astore          14
        //   264: new             Ljava/util/HashSet;
        //   267: dup            
        //   268: invokespecial   java/util/HashSet.<init>:()V
        //   271: astore          16
        //   273: aload           14
        //   275: invokeinterface java/util/Enumeration.hasMoreElements:()Z
        //   280: ifeq            401
        //   283: aload           14
        //   285: invokeinterface java/util/Enumeration.nextElement:()Ljava/lang/Object;
        //   290: invokestatic    org/bouncycastle/asn1/x509/PolicyInformation.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/PolicyInformation;
        //   293: astore          20
        //   295: aload           20
        //   297: invokevirtual   org/bouncycastle/asn1/x509/PolicyInformation.getPolicyIdentifier:()Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //   300: astore          15
        //   302: aload           16
        //   304: aload           15
        //   306: invokevirtual   org/bouncycastle/asn1/ASN1ObjectIdentifier.getId:()Ljava/lang/String;
        //   309: invokeinterface java/util/Set.add:(Ljava/lang/Object;)Z
        //   314: pop            
        //   315: ldc_w           "2.5.29.32.0"
        //   318: aload           15
        //   320: invokevirtual   org/bouncycastle/asn1/ASN1ObjectIdentifier.getId:()Ljava/lang/String;
        //   323: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   326: istore          9
        //   328: iload           9
        //   330: ifne            2593
        //   333: aload           20
        //   335: invokevirtual   org/bouncycastle/asn1/x509/PolicyInformation.getPolicyQualifiers:()Lorg/bouncycastle/asn1/ASN1Sequence;
        //   338: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getQualifierSet:(Lorg/bouncycastle/asn1/ASN1Sequence;)Ljava/util/Set;
        //   341: astore          20
        //   343: iload           8
        //   345: aload           18
        //   347: aload           15
        //   349: aload           20
        //   351: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.processCertD1i:(I[Ljava/util/List;Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/util/Set;)Z
        //   354: ifne            2593
        //   357: iload           8
        //   359: aload           18
        //   361: aload           15
        //   363: aload           20
        //   365: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.processCertD1ii:(I[Ljava/util/List;Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;Ljava/util/Set;)V
        //   368: goto            2593
        //   371: astore          10
        //   373: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //   376: dup            
        //   377: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   380: dup            
        //   381: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   383: ldc_w           "CertPathReviewer.policyQualifierError"
        //   386: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   389: aload           10
        //   391: aload_0        
        //   392: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.certPath:Ljava/security/cert/CertPath;
        //   395: iload           6
        //   397: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/lang/Throwable;Ljava/security/cert/CertPath;I)V
        //   400: athrow         
        //   401: aload           11
        //   403: astore          14
        //   405: aload           10
        //   407: astore          15
        //   409: aload           16
        //   411: astore          11
        //   413: aload           13
        //   415: ifnull          500
        //   418: aload           13
        //   420: ldc_w           "2.5.29.32.0"
        //   423: invokeinterface java/util/Set.contains:(Ljava/lang/Object;)Z
        //   428: ifeq            438
        //   431: aload           16
        //   433: astore          11
        //   435: goto            500
        //   438: aload           13
        //   440: invokeinterface java/util/Set.iterator:()Ljava/util/Iterator;
        //   445: astore          10
        //   447: new             Ljava/util/HashSet;
        //   450: dup            
        //   451: invokespecial   java/util/HashSet.<init>:()V
        //   454: astore          11
        //   456: aload           10
        //   458: invokeinterface java/util/Iterator.hasNext:()Z
        //   463: ifeq            2596
        //   466: aload           10
        //   468: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   473: astore          13
        //   475: aload           16
        //   477: aload           13
        //   479: invokeinterface java/util/Set.contains:(Ljava/lang/Object;)Z
        //   484: ifeq            456
        //   487: aload           11
        //   489: aload           13
        //   491: invokeinterface java/util/Set.add:(Ljava/lang/Object;)Z
        //   496: pop            
        //   497: goto            456
        //   500: iload_2        
        //   501: ifgt            524
        //   504: iload           8
        //   506: aload_0        
        //   507: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.n:I
        //   510: if_icmpge       2599
        //   513: aload           17
        //   515: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.isSelfIssued:(Ljava/security/cert/X509Certificate;)Z
        //   518: ifeq            2599
        //   521: goto            524
        //   524: aload           19
        //   526: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.getObjects:()Ljava/util/Enumeration;
        //   529: astore          10
        //   531: aload           10
        //   533: invokeinterface java/util/Enumeration.hasMoreElements:()Z
        //   538: ifeq            2599
        //   541: aload           10
        //   543: invokeinterface java/util/Enumeration.nextElement:()Ljava/lang/Object;
        //   548: invokestatic    org/bouncycastle/asn1/x509/PolicyInformation.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/PolicyInformation;
        //   551: astore          13
        //   553: ldc_w           "2.5.29.32.0"
        //   556: aload           13
        //   558: invokevirtual   org/bouncycastle/asn1/x509/PolicyInformation.getPolicyIdentifier:()Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //   561: invokevirtual   org/bouncycastle/asn1/ASN1ObjectIdentifier.getId:()Ljava/lang/String;
        //   564: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   567: istore          9
        //   569: iload           9
        //   571: ifeq            531
        //   574: aload           13
        //   576: invokevirtual   org/bouncycastle/asn1/x509/PolicyInformation.getPolicyQualifiers:()Lorg/bouncycastle/asn1/ASN1Sequence;
        //   579: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getQualifierSet:(Lorg/bouncycastle/asn1/ASN1Sequence;)Ljava/util/Set;
        //   582: astore          20
        //   584: aload           18
        //   586: iload           8
        //   588: iconst_1       
        //   589: isub           
        //   590: aaload         
        //   591: astore          10
        //   593: iconst_0       
        //   594: istore          7
        //   596: iload_2        
        //   597: istore          5
        //   599: iload_3        
        //   600: istore          4
        //   602: iload           7
        //   604: aload           10
        //   606: invokeinterface java/util/List.size:()I
        //   611: if_icmpge       2605
        //   614: aload           10
        //   616: iload           7
        //   618: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   623: checkcast       Lorg/bouncycastle/jce/provider/PKIXPolicyNode;
        //   626: astore          21
        //   628: aload           21
        //   630: invokevirtual   org/bouncycastle/jce/provider/PKIXPolicyNode.getExpectedPolicies:()Ljava/util/Set;
        //   633: invokeinterface java/util/Set.iterator:()Ljava/util/Iterator;
        //   638: astore          22
        //   640: aload           22
        //   642: invokeinterface java/util/Iterator.hasNext:()Z
        //   647: ifeq            2623
        //   650: aload           22
        //   652: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   657: astore          13
        //   659: aload           13
        //   661: instanceof      Ljava/lang/String;
        //   664: ifeq            677
        //   667: aload           13
        //   669: checkcast       Ljava/lang/String;
        //   672: astore          13
        //   674: goto            695
        //   677: aload           13
        //   679: instanceof      Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //   682: ifeq            2620
        //   685: aload           13
        //   687: checkcast       Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //   690: invokevirtual   org/bouncycastle/asn1/ASN1ObjectIdentifier.getId:()Ljava/lang/String;
        //   693: astore          13
        //   695: aload           21
        //   697: invokevirtual   org/bouncycastle/jce/provider/PKIXPolicyNode.getChildren:()Ljava/util/Iterator;
        //   700: astore          16
        //   702: iconst_0       
        //   703: istore          4
        //   705: aload           16
        //   707: invokeinterface java/util/Iterator.hasNext:()Z
        //   712: ifeq            742
        //   715: aload           13
        //   717: aload           16
        //   719: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   724: checkcast       Lorg/bouncycastle/jce/provider/PKIXPolicyNode;
        //   727: invokevirtual   org/bouncycastle/jce/provider/PKIXPolicyNode.getValidPolicy:()Ljava/lang/String;
        //   730: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   733: ifeq            2614
        //   736: iconst_1       
        //   737: istore          4
        //   739: goto            2614
        //   742: iload           4
        //   744: ifne            2617
        //   747: new             Ljava/util/HashSet;
        //   750: dup            
        //   751: invokespecial   java/util/HashSet.<init>:()V
        //   754: astore          16
        //   756: aload           16
        //   758: aload           13
        //   760: invokeinterface java/util/Set.add:(Ljava/lang/Object;)Z
        //   765: pop            
        //   766: new             Lorg/bouncycastle/jce/provider/PKIXPolicyNode;
        //   769: dup            
        //   770: new             Ljava/util/ArrayList;
        //   773: dup            
        //   774: invokespecial   java/util/ArrayList.<init>:()V
        //   777: iload           8
        //   779: aload           16
        //   781: aload           21
        //   783: aload           20
        //   785: aload           13
        //   787: iconst_0       
        //   788: invokespecial   org/bouncycastle/jce/provider/PKIXPolicyNode.<init>:(Ljava/util/List;ILjava/util/Set;Ljava/security/cert/PolicyNode;Ljava/util/Set;Ljava/lang/String;Z)V
        //   791: astore          13
        //   793: aload           21
        //   795: aload           13
        //   797: invokevirtual   org/bouncycastle/jce/provider/PKIXPolicyNode.addChild:(Lorg/bouncycastle/jce/provider/PKIXPolicyNode;)V
        //   800: aload           18
        //   802: iload           8
        //   804: aaload         
        //   805: aload           13
        //   807: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   812: pop            
        //   813: goto            2617
        //   816: astore          10
        //   818: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //   821: dup            
        //   822: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   825: dup            
        //   826: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   828: ldc_w           "CertPathReviewer.policyQualifierError"
        //   831: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   834: aload           10
        //   836: aload_0        
        //   837: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.certPath:Ljava/security/cert/CertPath;
        //   840: iload           6
        //   842: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/lang/Throwable;Ljava/security/cert/CertPath;I)V
        //   845: athrow         
        //   846: aload           10
        //   848: astore          13
        //   850: iload           5
        //   852: aload           15
        //   854: invokeinterface java/util/List.size:()I
        //   859: if_icmpge       2673
        //   862: aload           15
        //   864: iload           5
        //   866: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   871: checkcast       Lorg/bouncycastle/jce/provider/PKIXPolicyNode;
        //   874: astore          16
        //   876: aload           10
        //   878: astore          13
        //   880: aload           16
        //   882: invokevirtual   org/bouncycastle/jce/provider/PKIXPolicyNode.hasChildren:()Z
        //   885: ifne            2660
        //   888: aload           10
        //   890: aload           18
        //   892: aload           16
        //   894: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.removePolicyNode:(Lorg/bouncycastle/jce/provider/PKIXPolicyNode;[Ljava/util/List;Lorg/bouncycastle/jce/provider/PKIXPolicyNode;)Lorg/bouncycastle/jce/provider/PKIXPolicyNode;
        //   897: astore          10
        //   899: aload           10
        //   901: astore          13
        //   903: aload           10
        //   905: ifnonnull       2660
        //   908: aload           10
        //   910: astore          13
        //   912: goto            2673
        //   915: aload           17
        //   917: invokevirtual   java/security/cert/X509Certificate.getCriticalExtensionOIDs:()Ljava/util/Set;
        //   920: astore          13
        //   922: aload           13
        //   924: ifnull          2686
        //   927: aload           13
        //   929: getstatic       org/bouncycastle/x509/PKIXCertPathReviewer.CERTIFICATE_POLICIES:Ljava/lang/String;
        //   932: invokeinterface java/util/Set.contains:(Ljava/lang/Object;)Z
        //   937: istore          9
        //   939: aload           18
        //   941: iload           8
        //   943: aaload         
        //   944: astore          13
        //   946: iconst_0       
        //   947: istore          4
        //   949: iload           4
        //   951: aload           13
        //   953: invokeinterface java/util/List.size:()I
        //   958: if_icmpge       2686
        //   961: aload           13
        //   963: iload           4
        //   965: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   970: checkcast       Lorg/bouncycastle/jce/provider/PKIXPolicyNode;
        //   973: iload           9
        //   975: invokevirtual   org/bouncycastle/jce/provider/PKIXPolicyNode.setCritical:(Z)V
        //   978: iload           4
        //   980: iconst_1       
        //   981: iadd           
        //   982: istore          4
        //   984: goto            949
        //   987: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //   990: dup            
        //   991: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   994: dup            
        //   995: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   997: ldc_w           "CertPathReviewer.noValidPolicyTree"
        //  1000: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1003: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //  1006: athrow         
        //  1007: aload_0        
        //  1008: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.n:I
        //  1011: istore          4
        //  1013: iload           8
        //  1015: iload           4
        //  1017: if_icmpeq       2755
        //  1020: aload           17
        //  1022: getstatic       org/bouncycastle/x509/PKIXCertPathReviewer.POLICY_MAPPINGS:Ljava/lang/String;
        //  1025: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getExtensionValue:(Ljava/security/cert/X509Extension;Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1Primitive;
        //  1028: astore          14
        //  1030: aload           14
        //  1032: ifnull          1182
        //  1035: aload           14
        //  1037: checkcast       Lorg/bouncycastle/asn1/ASN1Sequence;
        //  1040: astore          15
        //  1042: iconst_0       
        //  1043: istore          4
        //  1045: iload           4
        //  1047: aload           15
        //  1049: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.size:()I
        //  1052: if_icmpge       1182
        //  1055: aload           15
        //  1057: iload           4
        //  1059: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.getObjectAt:(I)Lorg/bouncycastle/asn1/ASN1Encodable;
        //  1062: checkcast       Lorg/bouncycastle/asn1/ASN1Sequence;
        //  1065: astore          19
        //  1067: aload           19
        //  1069: iconst_0       
        //  1070: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.getObjectAt:(I)Lorg/bouncycastle/asn1/ASN1Encodable;
        //  1073: checkcast       Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //  1076: astore          16
        //  1078: aload           19
        //  1080: iconst_1       
        //  1081: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.getObjectAt:(I)Lorg/bouncycastle/asn1/ASN1Encodable;
        //  1084: checkcast       Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //  1087: astore          19
        //  1089: ldc_w           "2.5.29.32.0"
        //  1092: aload           16
        //  1094: invokevirtual   org/bouncycastle/asn1/ASN1ObjectIdentifier.getId:()Ljava/lang/String;
        //  1097: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1100: istore          9
        //  1102: iload           9
        //  1104: ifne            1156
        //  1107: ldc_w           "2.5.29.32.0"
        //  1110: aload           19
        //  1112: invokevirtual   org/bouncycastle/asn1/ASN1ObjectIdentifier.getId:()Ljava/lang/String;
        //  1115: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1118: ifne            1130
        //  1121: iload           4
        //  1123: iconst_1       
        //  1124: iadd           
        //  1125: istore          4
        //  1127: goto            1045
        //  1130: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1133: dup            
        //  1134: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1137: dup            
        //  1138: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1140: ldc_w           "CertPathReviewer.invalidPolicyMapping"
        //  1143: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1146: aload_0        
        //  1147: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.certPath:Ljava/security/cert/CertPath;
        //  1150: iload           6
        //  1152: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/security/cert/CertPath;I)V
        //  1155: athrow         
        //  1156: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1159: dup            
        //  1160: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1163: dup            
        //  1164: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1166: ldc_w           "CertPathReviewer.invalidPolicyMapping"
        //  1169: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1172: aload_0        
        //  1173: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.certPath:Ljava/security/cert/CertPath;
        //  1176: iload           6
        //  1178: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/security/cert/CertPath;I)V
        //  1181: athrow         
        //  1182: aload           14
        //  1184: ifnull          2752
        //  1187: aload           14
        //  1189: checkcast       Lorg/bouncycastle/asn1/ASN1Sequence;
        //  1192: astore          14
        //  1194: new             Ljava/util/HashMap;
        //  1197: dup            
        //  1198: invokespecial   java/util/HashMap.<init>:()V
        //  1201: astore          15
        //  1203: new             Ljava/util/HashSet;
        //  1206: dup            
        //  1207: invokespecial   java/util/HashSet.<init>:()V
        //  1210: astore          16
        //  1212: iconst_0       
        //  1213: istore          4
        //  1215: iload           4
        //  1217: aload           14
        //  1219: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.size:()I
        //  1222: if_icmpge       1344
        //  1225: aload           14
        //  1227: iload           4
        //  1229: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.getObjectAt:(I)Lorg/bouncycastle/asn1/ASN1Encodable;
        //  1232: checkcast       Lorg/bouncycastle/asn1/ASN1Sequence;
        //  1235: astore          20
        //  1237: aload           20
        //  1239: iconst_0       
        //  1240: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.getObjectAt:(I)Lorg/bouncycastle/asn1/ASN1Encodable;
        //  1243: checkcast       Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //  1246: invokevirtual   org/bouncycastle/asn1/ASN1ObjectIdentifier.getId:()Ljava/lang/String;
        //  1249: astore          19
        //  1251: aload           20
        //  1253: iconst_1       
        //  1254: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.getObjectAt:(I)Lorg/bouncycastle/asn1/ASN1Encodable;
        //  1257: checkcast       Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //  1260: invokevirtual   org/bouncycastle/asn1/ASN1ObjectIdentifier.getId:()Ljava/lang/String;
        //  1263: astore          20
        //  1265: aload           15
        //  1267: aload           19
        //  1269: invokeinterface java/util/Map.containsKey:(Ljava/lang/Object;)Z
        //  1274: ifne            1321
        //  1277: new             Ljava/util/HashSet;
        //  1280: dup            
        //  1281: invokespecial   java/util/HashSet.<init>:()V
        //  1284: astore          21
        //  1286: aload           21
        //  1288: aload           20
        //  1290: invokeinterface java/util/Set.add:(Ljava/lang/Object;)Z
        //  1295: pop            
        //  1296: aload           15
        //  1298: aload           19
        //  1300: aload           21
        //  1302: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //  1307: pop            
        //  1308: aload           16
        //  1310: aload           19
        //  1312: invokeinterface java/util/Set.add:(Ljava/lang/Object;)Z
        //  1317: pop            
        //  1318: goto            2733
        //  1321: aload           15
        //  1323: aload           19
        //  1325: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //  1330: checkcast       Ljava/util/Set;
        //  1333: aload           20
        //  1335: invokeinterface java/util/Set.add:(Ljava/lang/Object;)Z
        //  1340: pop            
        //  1341: goto            2733
        //  1344: aload           16
        //  1346: invokeinterface java/util/Set.iterator:()Ljava/util/Iterator;
        //  1351: astore          16
        //  1353: aload           16
        //  1355: invokeinterface java/util/Iterator.hasNext:()Z
        //  1360: ifeq            2749
        //  1363: aload           16
        //  1365: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //  1370: checkcast       Ljava/lang/String;
        //  1373: astore          19
        //  1375: iload_3        
        //  1376: ifle            1458
        //  1379: iload           8
        //  1381: aload           18
        //  1383: aload           19
        //  1385: aload           15
        //  1387: aload           17
        //  1389: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.prepareNextCertB1:(I[Ljava/util/List;Ljava/lang/String;Ljava/util/Map;Ljava/security/cert/X509Certificate;)V
        //  1392: aload           11
        //  1394: astore          14
        //  1396: goto            2742
        //  1399: astore          10
        //  1401: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1404: dup            
        //  1405: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1408: dup            
        //  1409: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1411: ldc_w           "CertPathReviewer.policyQualifierError"
        //  1414: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1417: aload           10
        //  1419: aload_0        
        //  1420: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.certPath:Ljava/security/cert/CertPath;
        //  1423: iload           6
        //  1425: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/lang/Throwable;Ljava/security/cert/CertPath;I)V
        //  1428: athrow         
        //  1429: astore          11
        //  1431: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1434: dup            
        //  1435: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1438: dup            
        //  1439: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1441: aload           10
        //  1443: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1446: aload           11
        //  1448: aload_0        
        //  1449: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.certPath:Ljava/security/cert/CertPath;
        //  1452: iload           6
        //  1454: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/lang/Throwable;Ljava/security/cert/CertPath;I)V
        //  1457: athrow         
        //  1458: aload           11
        //  1460: astore          14
        //  1462: iload_3        
        //  1463: ifgt            2742
        //  1466: iload           8
        //  1468: aload           18
        //  1470: aload           19
        //  1472: aload           11
        //  1474: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.prepareNextCertB2:(I[Ljava/util/List;Ljava/lang/String;Lorg/bouncycastle/jce/provider/PKIXPolicyNode;)Lorg/bouncycastle/jce/provider/PKIXPolicyNode;
        //  1477: astore          14
        //  1479: goto            2742
        //  1482: aload           17
        //  1484: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.isSelfIssued:(Ljava/security/cert/X509Certificate;)Z
        //  1487: istore          9
        //  1489: iload           9
        //  1491: ifne            1539
        //  1494: iload_1        
        //  1495: istore          4
        //  1497: iload_1        
        //  1498: ifeq            1506
        //  1501: iload_1        
        //  1502: iconst_1       
        //  1503: isub           
        //  1504: istore          4
        //  1506: iload_3        
        //  1507: ifeq            1517
        //  1510: iload_3        
        //  1511: iconst_1       
        //  1512: isub           
        //  1513: istore_1       
        //  1514: goto            1519
        //  1517: iload_3        
        //  1518: istore_1       
        //  1519: iload_2        
        //  1520: ifeq            1533
        //  1523: iload_2        
        //  1524: iconst_1       
        //  1525: isub           
        //  1526: istore_2       
        //  1527: iload           4
        //  1529: istore_3       
        //  1530: goto            1547
        //  1533: iload           4
        //  1535: istore_3       
        //  1536: goto            1547
        //  1539: iload_3        
        //  1540: istore          4
        //  1542: iload_1        
        //  1543: istore_3       
        //  1544: iload           4
        //  1546: istore_1       
        //  1547: aload           17
        //  1549: getstatic       org/bouncycastle/x509/PKIXCertPathReviewer.POLICY_CONSTRAINTS:Ljava/lang/String;
        //  1552: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getExtensionValue:(Ljava/security/cert/X509Extension;Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1Primitive;
        //  1555: checkcast       Lorg/bouncycastle/asn1/ASN1Sequence;
        //  1558: astore          14
        //  1560: iload_1        
        //  1561: istore          5
        //  1563: iload_3        
        //  1564: istore          4
        //  1566: aload           14
        //  1568: ifnull          1673
        //  1571: aload           14
        //  1573: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.getObjects:()Ljava/util/Enumeration;
        //  1576: astore          14
        //  1578: iload_1        
        //  1579: istore          5
        //  1581: iload_3        
        //  1582: istore          4
        //  1584: aload           14
        //  1586: invokeinterface java/util/Enumeration.hasMoreElements:()Z
        //  1591: ifeq            1673
        //  1594: aload           14
        //  1596: invokeinterface java/util/Enumeration.nextElement:()Ljava/lang/Object;
        //  1601: checkcast       Lorg/bouncycastle/asn1/ASN1TaggedObject;
        //  1604: astore          15
        //  1606: aload           15
        //  1608: invokevirtual   org/bouncycastle/asn1/ASN1TaggedObject.getTagNo:()I
        //  1611: istore          4
        //  1613: iload           4
        //  1615: ifeq            1650
        //  1618: iload           4
        //  1620: iconst_1       
        //  1621: if_icmpeq       1627
        //  1624: goto            1578
        //  1627: aload           15
        //  1629: iconst_0       
        //  1630: invokestatic    org/bouncycastle/asn1/ASN1Integer.getInstance:(Lorg/bouncycastle/asn1/ASN1TaggedObject;Z)Lorg/bouncycastle/asn1/ASN1Integer;
        //  1633: invokevirtual   org/bouncycastle/asn1/ASN1Integer.intValueExact:()I
        //  1636: istore          4
        //  1638: iload           4
        //  1640: iload_1        
        //  1641: if_icmpge       1578
        //  1644: iload           4
        //  1646: istore_1       
        //  1647: goto            1578
        //  1650: aload           15
        //  1652: iconst_0       
        //  1653: invokestatic    org/bouncycastle/asn1/ASN1Integer.getInstance:(Lorg/bouncycastle/asn1/ASN1TaggedObject;Z)Lorg/bouncycastle/asn1/ASN1Integer;
        //  1656: invokevirtual   org/bouncycastle/asn1/ASN1Integer.intValueExact:()I
        //  1659: istore          4
        //  1661: iload           4
        //  1663: iload_3        
        //  1664: if_icmpge       1578
        //  1667: iload           4
        //  1669: istore_3       
        //  1670: goto            1578
        //  1673: aload           17
        //  1675: getstatic       org/bouncycastle/x509/PKIXCertPathReviewer.INHIBIT_ANY_POLICY:Ljava/lang/String;
        //  1678: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getExtensionValue:(Ljava/security/cert/X509Extension;Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1Primitive;
        //  1681: checkcast       Lorg/bouncycastle/asn1/ASN1Integer;
        //  1684: astore          14
        //  1686: aload           14
        //  1688: ifnull          1705
        //  1691: aload           14
        //  1693: invokevirtual   org/bouncycastle/asn1/ASN1Integer.intValueExact:()I
        //  1696: istore_1       
        //  1697: iload_1        
        //  1698: iload_2        
        //  1699: if_icmpge       1705
        //  1702: goto            1707
        //  1705: iload_2        
        //  1706: istore_1       
        //  1707: iload_1        
        //  1708: istore_2       
        //  1709: aload           11
        //  1711: astore          14
        //  1713: iload           4
        //  1715: istore_1       
        //  1716: aload           10
        //  1718: astore          11
        //  1720: aload           14
        //  1722: astore          10
        //  1724: goto            2770
        //  1727: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1730: dup            
        //  1731: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1734: dup            
        //  1735: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1737: ldc_w           "CertPathReviewer.policyInhibitExtError"
        //  1740: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1743: aload_0        
        //  1744: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.certPath:Ljava/security/cert/CertPath;
        //  1747: iload           6
        //  1749: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/security/cert/CertPath;I)V
        //  1752: athrow         
        //  1753: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1756: dup            
        //  1757: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1760: dup            
        //  1761: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1763: ldc_w           "CertPathReviewer.policyConstExtError"
        //  1766: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1769: aload_0        
        //  1770: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.certPath:Ljava/security/cert/CertPath;
        //  1773: iload           6
        //  1775: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/security/cert/CertPath;I)V
        //  1778: athrow         
        //  1779: astore          10
        //  1781: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1784: dup            
        //  1785: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1788: dup            
        //  1789: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1791: ldc_w           "CertPathReviewer.policyMapExtError"
        //  1794: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1797: aload           10
        //  1799: aload_0        
        //  1800: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.certPath:Ljava/security/cert/CertPath;
        //  1803: iload           6
        //  1805: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/lang/Throwable;Ljava/security/cert/CertPath;I)V
        //  1808: athrow         
        //  1809: astore          10
        //  1811: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1814: dup            
        //  1815: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1818: dup            
        //  1819: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1821: aload           11
        //  1823: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1826: aload           10
        //  1828: aload_0        
        //  1829: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.certPath:Ljava/security/cert/CertPath;
        //  1832: iload           6
        //  1834: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/lang/Throwable;Ljava/security/cert/CertPath;I)V
        //  1837: athrow         
        //  1838: aload           14
        //  1840: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.isSelfIssued:(Ljava/security/cert/X509Certificate;)Z
        //  1843: istore          9
        //  1845: iload_1        
        //  1846: istore_2       
        //  1847: iload           9
        //  1849: ifne            1862
        //  1852: iload_1        
        //  1853: istore_2       
        //  1854: iload_1        
        //  1855: ifle            1862
        //  1858: iload_1        
        //  1859: iconst_1       
        //  1860: isub           
        //  1861: istore_2       
        //  1862: aload           14
        //  1864: getstatic       org/bouncycastle/x509/PKIXCertPathReviewer.POLICY_CONSTRAINTS:Ljava/lang/String;
        //  1867: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getExtensionValue:(Ljava/security/cert/X509Extension;Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1Primitive;
        //  1870: checkcast       Lorg/bouncycastle/asn1/ASN1Sequence;
        //  1873: astore          11
        //  1875: aload           11
        //  1877: ifnull          1944
        //  1880: aload           11
        //  1882: invokevirtual   org/bouncycastle/asn1/ASN1Sequence.getObjects:()Ljava/util/Enumeration;
        //  1885: astore          11
        //  1887: iload_2        
        //  1888: istore_1       
        //  1889: aload           11
        //  1891: invokeinterface java/util/Enumeration.hasMoreElements:()Z
        //  1896: ifeq            1941
        //  1899: aload           11
        //  1901: invokeinterface java/util/Enumeration.nextElement:()Ljava/lang/Object;
        //  1906: checkcast       Lorg/bouncycastle/asn1/ASN1TaggedObject;
        //  1909: astore          14
        //  1911: aload           14
        //  1913: invokevirtual   org/bouncycastle/asn1/ASN1TaggedObject.getTagNo:()I
        //  1916: ifeq            1922
        //  1919: goto            1889
        //  1922: aload           14
        //  1924: iconst_0       
        //  1925: invokestatic    org/bouncycastle/asn1/ASN1Integer.getInstance:(Lorg/bouncycastle/asn1/ASN1TaggedObject;Z)Lorg/bouncycastle/asn1/ASN1Integer;
        //  1928: invokevirtual   org/bouncycastle/asn1/ASN1Integer.intValueExact:()I
        //  1931: istore_2       
        //  1932: iload_2        
        //  1933: ifne            1889
        //  1936: iconst_0       
        //  1937: istore_1       
        //  1938: goto            1889
        //  1941: goto            1946
        //  1944: iload_2        
        //  1945: istore_1       
        //  1946: aload           10
        //  1948: ifnonnull       1993
        //  1951: aload_0        
        //  1952: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.pkixParams:Ljava/security/cert/PKIXParameters;
        //  1955: invokevirtual   java/security/cert/PKIXParameters.isExplicitPolicyRequired:()Z
        //  1958: ifne            1967
        //  1961: aconst_null    
        //  1962: astore          10
        //  1964: goto            2906
        //  1967: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1970: dup            
        //  1971: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1974: dup            
        //  1975: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1977: ldc_w           "CertPathReviewer.explicitPolicy"
        //  1980: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1983: aload_0        
        //  1984: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.certPath:Ljava/security/cert/CertPath;
        //  1987: iload           6
        //  1989: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/security/cert/CertPath;I)V
        //  1992: athrow         
        //  1993: aload           12
        //  1995: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.isAnyPolicy:(Ljava/util/Set;)Z
        //  1998: ifeq            2262
        //  2001: aload           10
        //  2003: astore          11
        //  2005: aload_0        
        //  2006: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.pkixParams:Ljava/security/cert/PKIXParameters;
        //  2009: invokevirtual   java/security/cert/PKIXParameters.isExplicitPolicyRequired:()Z
        //  2012: ifeq            2837
        //  2015: aload           13
        //  2017: invokeinterface java/util/Set.isEmpty:()Z
        //  2022: ifne            2236
        //  2025: new             Ljava/util/HashSet;
        //  2028: dup            
        //  2029: invokespecial   java/util/HashSet.<init>:()V
        //  2032: astore          11
        //  2034: iconst_0       
        //  2035: istore_2       
        //  2036: iload_2        
        //  2037: aload           18
        //  2039: arraylength    
        //  2040: if_icmpge       2124
        //  2043: aload           18
        //  2045: iload_2        
        //  2046: aaload         
        //  2047: astore          12
        //  2049: iconst_0       
        //  2050: istore_3       
        //  2051: iload_3        
        //  2052: aload           12
        //  2054: invokeinterface java/util/List.size:()I
        //  2059: if_icmpge       2793
        //  2062: aload           12
        //  2064: iload_3        
        //  2065: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //  2070: checkcast       Lorg/bouncycastle/jce/provider/PKIXPolicyNode;
        //  2073: astore          14
        //  2075: ldc_w           "2.5.29.32.0"
        //  2078: aload           14
        //  2080: invokevirtual   org/bouncycastle/jce/provider/PKIXPolicyNode.getValidPolicy:()Ljava/lang/String;
        //  2083: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  2086: ifeq            2786
        //  2089: aload           14
        //  2091: invokevirtual   org/bouncycastle/jce/provider/PKIXPolicyNode.getChildren:()Ljava/util/Iterator;
        //  2094: astore          14
        //  2096: aload           14
        //  2098: invokeinterface java/util/Iterator.hasNext:()Z
        //  2103: ifeq            2786
        //  2106: aload           11
        //  2108: aload           14
        //  2110: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //  2115: invokeinterface java/util/Set.add:(Ljava/lang/Object;)Z
        //  2120: pop            
        //  2121: goto            2096
        //  2124: aload           11
        //  2126: invokeinterface java/util/Set.iterator:()Ljava/util/Iterator;
        //  2131: astore          11
        //  2133: aload           11
        //  2135: invokeinterface java/util/Iterator.hasNext:()Z
        //  2140: ifeq            2167
        //  2143: aload           13
        //  2145: aload           11
        //  2147: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //  2152: checkcast       Lorg/bouncycastle/jce/provider/PKIXPolicyNode;
        //  2155: invokevirtual   org/bouncycastle/jce/provider/PKIXPolicyNode.getValidPolicy:()Ljava/lang/String;
        //  2158: invokeinterface java/util/Set.contains:(Ljava/lang/Object;)Z
        //  2163: pop            
        //  2164: goto            2133
        //  2167: aload           10
        //  2169: astore          11
        //  2171: aload           10
        //  2173: ifnull          2837
        //  2176: aload_0        
        //  2177: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.n:I
        //  2180: iconst_1       
        //  2181: isub           
        //  2182: istore_2       
        //  2183: goto            2800
        //  2186: iload_3        
        //  2187: aload           12
        //  2189: invokeinterface java/util/List.size:()I
        //  2194: if_icmpge       2830
        //  2197: aload           12
        //  2199: iload_3        
        //  2200: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //  2205: checkcast       Lorg/bouncycastle/jce/provider/PKIXPolicyNode;
        //  2208: astore          13
        //  2210: aload           10
        //  2212: astore          11
        //  2214: aload           13
        //  2216: invokevirtual   org/bouncycastle/jce/provider/PKIXPolicyNode.hasChildren:()Z
        //  2219: ifne            2819
        //  2222: aload           10
        //  2224: aload           18
        //  2226: aload           13
        //  2228: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.removePolicyNode:(Lorg/bouncycastle/jce/provider/PKIXPolicyNode;[Ljava/util/List;Lorg/bouncycastle/jce/provider/PKIXPolicyNode;)Lorg/bouncycastle/jce/provider/PKIXPolicyNode;
        //  2231: astore          11
        //  2233: goto            2819
        //  2236: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2239: dup            
        //  2240: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  2243: dup            
        //  2244: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  2246: ldc_w           "CertPathReviewer.explicitPolicy"
        //  2249: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  2252: aload_0        
        //  2253: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.certPath:Ljava/security/cert/CertPath;
        //  2256: iload           6
        //  2258: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/security/cert/CertPath;I)V
        //  2261: athrow         
        //  2262: new             Ljava/util/HashSet;
        //  2265: dup            
        //  2266: invokespecial   java/util/HashSet.<init>:()V
        //  2269: astore          11
        //  2271: iconst_0       
        //  2272: istore_2       
        //  2273: iload_2        
        //  2274: aload           18
        //  2276: arraylength    
        //  2277: if_icmpge       2382
        //  2280: aload           18
        //  2282: iload_2        
        //  2283: aaload         
        //  2284: astore          13
        //  2286: iconst_0       
        //  2287: istore_3       
        //  2288: iload_3        
        //  2289: aload           13
        //  2291: invokeinterface java/util/List.size:()I
        //  2296: if_icmpge       2851
        //  2299: aload           13
        //  2301: iload_3        
        //  2302: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //  2307: checkcast       Lorg/bouncycastle/jce/provider/PKIXPolicyNode;
        //  2310: astore          14
        //  2312: ldc_w           "2.5.29.32.0"
        //  2315: aload           14
        //  2317: invokevirtual   org/bouncycastle/jce/provider/PKIXPolicyNode.getValidPolicy:()Ljava/lang/String;
        //  2320: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  2323: ifeq            2844
        //  2326: aload           14
        //  2328: invokevirtual   org/bouncycastle/jce/provider/PKIXPolicyNode.getChildren:()Ljava/util/Iterator;
        //  2331: astore          14
        //  2333: aload           14
        //  2335: invokeinterface java/util/Iterator.hasNext:()Z
        //  2340: ifeq            2844
        //  2343: aload           14
        //  2345: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //  2350: checkcast       Lorg/bouncycastle/jce/provider/PKIXPolicyNode;
        //  2353: astore          15
        //  2355: ldc_w           "2.5.29.32.0"
        //  2358: aload           15
        //  2360: invokevirtual   org/bouncycastle/jce/provider/PKIXPolicyNode.getValidPolicy:()Ljava/lang/String;
        //  2363: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  2366: ifne            2333
        //  2369: aload           11
        //  2371: aload           15
        //  2373: invokeinterface java/util/Set.add:(Ljava/lang/Object;)Z
        //  2378: pop            
        //  2379: goto            2333
        //  2382: aload           11
        //  2384: invokeinterface java/util/Set.iterator:()Ljava/util/Iterator;
        //  2389: astore          13
        //  2391: aload           13
        //  2393: invokeinterface java/util/Iterator.hasNext:()Z
        //  2398: ifeq            2446
        //  2401: aload           13
        //  2403: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //  2408: checkcast       Lorg/bouncycastle/jce/provider/PKIXPolicyNode;
        //  2411: astore          14
        //  2413: aload           10
        //  2415: astore          11
        //  2417: aload           12
        //  2419: aload           14
        //  2421: invokevirtual   org/bouncycastle/jce/provider/PKIXPolicyNode.getValidPolicy:()Ljava/lang/String;
        //  2424: invokeinterface java/util/Set.contains:(Ljava/lang/Object;)Z
        //  2429: ifne            2858
        //  2432: aload           10
        //  2434: aload           18
        //  2436: aload           14
        //  2438: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.removePolicyNode:(Lorg/bouncycastle/jce/provider/PKIXPolicyNode;[Ljava/util/List;Lorg/bouncycastle/jce/provider/PKIXPolicyNode;)Lorg/bouncycastle/jce/provider/PKIXPolicyNode;
        //  2441: astore          11
        //  2443: goto            2858
        //  2446: aload           10
        //  2448: astore          11
        //  2450: aload           10
        //  2452: ifnull          2902
        //  2455: aload_0        
        //  2456: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.n:I
        //  2459: iconst_1       
        //  2460: isub           
        //  2461: istore_2       
        //  2462: goto            2865
        //  2465: iload_3        
        //  2466: aload           12
        //  2468: invokeinterface java/util/List.size:()I
        //  2473: if_icmpge       2895
        //  2476: aload           12
        //  2478: iload_3        
        //  2479: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //  2484: checkcast       Lorg/bouncycastle/jce/provider/PKIXPolicyNode;
        //  2487: astore          13
        //  2489: aload           10
        //  2491: astore          11
        //  2493: aload           13
        //  2495: invokevirtual   org/bouncycastle/jce/provider/PKIXPolicyNode.hasChildren:()Z
        //  2498: ifne            2884
        //  2501: aload           10
        //  2503: aload           18
        //  2505: aload           13
        //  2507: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.removePolicyNode:(Lorg/bouncycastle/jce/provider/PKIXPolicyNode;[Ljava/util/List;Lorg/bouncycastle/jce/provider/PKIXPolicyNode;)Lorg/bouncycastle/jce/provider/PKIXPolicyNode;
        //  2510: astore          11
        //  2512: goto            2884
        //  2515: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2518: dup            
        //  2519: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  2522: dup            
        //  2523: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  2525: ldc_w           "CertPathReviewer.invalidPolicy"
        //  2528: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  2531: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //  2534: athrow         
        //  2535: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2538: dup            
        //  2539: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  2542: dup            
        //  2543: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  2545: ldc_w           "CertPathReviewer.policyConstExtError"
        //  2548: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  2551: aload_0        
        //  2552: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.certPath:Ljava/security/cert/CertPath;
        //  2555: iload           6
        //  2557: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/security/cert/CertPath;I)V
        //  2560: athrow         
        //  2561: astore          10
        //  2563: aload_0        
        //  2564: aload           10
        //  2566: invokevirtual   org/bouncycastle/x509/CertPathReviewerException.getErrorMessage:()Lorg/bouncycastle/i18n/ErrorBundle;
        //  2569: aload           10
        //  2571: invokevirtual   org/bouncycastle/x509/CertPathReviewerException.getIndex:()I
        //  2574: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //  2577: return         
        //  2578: astore          10
        //  2580: goto            1753
        //  2583: astore          10
        //  2585: goto            1727
        //  2588: astore          10
        //  2590: goto            2535
        //  2593: goto            273
        //  2596: goto            500
        //  2599: iload_3        
        //  2600: istore          4
        //  2602: iload_2        
        //  2603: istore          5
        //  2605: iload           5
        //  2607: istore_2       
        //  2608: iload           4
        //  2610: istore_3       
        //  2611: goto            2632
        //  2614: goto            705
        //  2617: goto            640
        //  2620: goto            640
        //  2623: iload           7
        //  2625: iconst_1       
        //  2626: iadd           
        //  2627: istore          7
        //  2629: goto            596
        //  2632: iload           8
        //  2634: iconst_1       
        //  2635: isub           
        //  2636: istore          4
        //  2638: aload           15
        //  2640: astore          10
        //  2642: iload           4
        //  2644: iflt            915
        //  2647: aload           18
        //  2649: iload           4
        //  2651: aaload         
        //  2652: astore          15
        //  2654: iconst_0       
        //  2655: istore          5
        //  2657: goto            846
        //  2660: iload           5
        //  2662: iconst_1       
        //  2663: iadd           
        //  2664: istore          5
        //  2666: aload           13
        //  2668: astore          10
        //  2670: goto            846
        //  2673: aload           13
        //  2675: astore          10
        //  2677: iload           4
        //  2679: iconst_1       
        //  2680: isub           
        //  2681: istore          4
        //  2683: goto            2642
        //  2686: aload           11
        //  2688: astore          13
        //  2690: aload           10
        //  2692: astore          11
        //  2694: aload           14
        //  2696: astore          10
        //  2698: goto            2713
        //  2701: aload           10
        //  2703: astore          14
        //  2705: aload           11
        //  2707: astore          10
        //  2709: aload           14
        //  2711: astore          11
        //  2713: aload           19
        //  2715: ifnonnull       2721
        //  2718: aconst_null    
        //  2719: astore          11
        //  2721: iload_1        
        //  2722: ifgt            1007
        //  2725: aload           11
        //  2727: ifnull          987
        //  2730: goto            1007
        //  2733: iload           4
        //  2735: iconst_1       
        //  2736: iadd           
        //  2737: istore          4
        //  2739: goto            1215
        //  2742: aload           14
        //  2744: astore          11
        //  2746: goto            1353
        //  2749: goto            1482
        //  2752: goto            1482
        //  2755: aload           10
        //  2757: astore          14
        //  2759: aload           11
        //  2761: astore          10
        //  2763: aload           14
        //  2765: astore          11
        //  2767: iload_3        
        //  2768: istore          5
        //  2770: iload           6
        //  2772: iconst_1       
        //  2773: isub           
        //  2774: istore          6
        //  2776: aload           17
        //  2778: astore          14
        //  2780: iload           5
        //  2782: istore_3       
        //  2783: goto            204
        //  2786: iload_3        
        //  2787: iconst_1       
        //  2788: iadd           
        //  2789: istore_3       
        //  2790: goto            2051
        //  2793: iload_2        
        //  2794: iconst_1       
        //  2795: iadd           
        //  2796: istore_2       
        //  2797: goto            2036
        //  2800: aload           10
        //  2802: astore          11
        //  2804: iload_2        
        //  2805: iflt            2837
        //  2808: aload           18
        //  2810: iload_2        
        //  2811: aaload         
        //  2812: astore          12
        //  2814: iconst_0       
        //  2815: istore_3       
        //  2816: goto            2186
        //  2819: iload_3        
        //  2820: iconst_1       
        //  2821: iadd           
        //  2822: istore_3       
        //  2823: aload           11
        //  2825: astore          10
        //  2827: goto            2186
        //  2830: iload_2        
        //  2831: iconst_1       
        //  2832: isub           
        //  2833: istore_2       
        //  2834: goto            2800
        //  2837: aload           11
        //  2839: astore          10
        //  2841: goto            2906
        //  2844: iload_3        
        //  2845: iconst_1       
        //  2846: iadd           
        //  2847: istore_3       
        //  2848: goto            2288
        //  2851: iload_2        
        //  2852: iconst_1       
        //  2853: iadd           
        //  2854: istore_2       
        //  2855: goto            2273
        //  2858: aload           11
        //  2860: astore          10
        //  2862: goto            2391
        //  2865: aload           10
        //  2867: astore          11
        //  2869: iload_2        
        //  2870: iflt            2902
        //  2873: aload           18
        //  2875: iload_2        
        //  2876: aaload         
        //  2877: astore          12
        //  2879: iconst_0       
        //  2880: istore_3       
        //  2881: goto            2465
        //  2884: iload_3        
        //  2885: iconst_1       
        //  2886: iadd           
        //  2887: istore_3       
        //  2888: aload           11
        //  2890: astore          10
        //  2892: goto            2465
        //  2895: iload_2        
        //  2896: iconst_1       
        //  2897: isub           
        //  2898: istore_2       
        //  2899: goto            2865
        //  2902: aload           11
        //  2904: astore          10
        //  2906: iload_1        
        //  2907: ifgt            2577
        //  2910: aload           10
        //  2912: ifnull          2515
        //  2915: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                              
        //  -----  -----  -----  -----  --------------------------------------------------
        //  181    192    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  209    234    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  234    247    1809   1838   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  234    247    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  257    273    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  273    328    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  333    343    371    401    Ljava/security/cert/CertPathValidatorException;
        //  333    343    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  343    368    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  373    401    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  418    431    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  438    456    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  456    497    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  504    521    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  524    531    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  531    569    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  574    584    816    846    Ljava/security/cert/CertPathValidatorException;
        //  574    584    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  602    640    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  640    674    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  677    695    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  695    702    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  705    715    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  715    736    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  747    813    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  818    846    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  850    876    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  880    899    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  915    922    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  927    939    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  949    978    2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  987    1007   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1007   1013   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1020   1030   1779   1809   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1020   1030   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1035   1042   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1045   1102   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1107   1121   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1130   1156   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1156   1182   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1187   1212   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1215   1318   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1321   1341   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1344   1353   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1353   1375   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1379   1392   1429   1458   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1379   1392   1399   1429   Ljava/security/cert/CertPathValidatorException;
        //  1379   1392   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1401   1429   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1431   1458   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1466   1479   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1482   1489   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1547   1560   2578   1779   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1547   1560   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1571   1578   2578   1779   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1571   1578   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1584   1613   2578   1779   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1584   1613   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1627   1638   2578   1779   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1627   1638   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1650   1661   2578   1779   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1650   1661   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1673   1686   2583   1753   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1673   1686   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1691   1697   2583   1753   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1691   1697   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1727   1753   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1753   1779   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1781   1809   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1811   1838   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1838   1845   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1862   1875   2588   2561   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1862   1875   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1880   1887   2588   2561   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1880   1887   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1889   1919   2588   2561   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1889   1919   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1922   1932   2588   2561   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1922   1932   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1951   1961   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1967   1993   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1993   2001   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2005   2034   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2036   2043   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2051   2096   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2096   2121   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2124   2133   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2133   2164   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2176   2183   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2186   2210   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2214   2233   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2236   2262   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2262   2271   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2273   2280   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2288   2333   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2333   2379   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2382   2391   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2391   2413   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2417   2443   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2455   2462   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2465   2489   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2493   2512   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2515   2535   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  2535   2561   2561   2577   Lorg/bouncycastle/x509/CertPathReviewerException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 1307 out of bounds for length 1307
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:373)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:426)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void checkSignatures() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: istore_2       
        //     2: new             Lorg/bouncycastle/i18n/filter/TrustedInput;
        //     5: dup            
        //     6: aload_0        
        //     7: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.validDate:Ljava/util/Date;
        //    10: invokespecial   org/bouncycastle/i18n/filter/TrustedInput.<init>:(Ljava/lang/Object;)V
        //    13: astore          5
        //    15: new             Lorg/bouncycastle/i18n/filter/TrustedInput;
        //    18: dup            
        //    19: new             Ljava/util/Date;
        //    22: dup            
        //    23: invokespecial   java/util/Date.<init>:()V
        //    26: invokespecial   org/bouncycastle/i18n/filter/TrustedInput.<init>:(Ljava/lang/Object;)V
        //    29: astore          6
        //    31: iconst_1       
        //    32: istore_3       
        //    33: aload_0        
        //    34: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //    37: dup            
        //    38: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //    40: ldc_w           "CertPathReviewer.certPathValidDate"
        //    43: iconst_2       
        //    44: anewarray       Ljava/lang/Object;
        //    47: dup            
        //    48: iconst_0       
        //    49: aload           5
        //    51: aastore        
        //    52: dup            
        //    53: iconst_1       
        //    54: aload           6
        //    56: aastore        
        //    57: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //    60: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addNotification:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //    63: aload_0        
        //    64: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.certs:Ljava/util/List;
        //    67: aload_0        
        //    68: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.certs:Ljava/util/List;
        //    71: invokeinterface java/util/List.size:()I
        //    76: iconst_1       
        //    77: isub           
        //    78: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //    83: checkcast       Ljava/security/cert/X509Certificate;
        //    86: astore          7
        //    88: aload_0        
        //    89: aload           7
        //    91: aload_0        
        //    92: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.pkixParams:Ljava/security/cert/PKIXParameters;
        //    95: invokevirtual   java/security/cert/PKIXParameters.getTrustAnchors:()Ljava/util/Set;
        //    98: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.getTrustAnchors:(Ljava/security/cert/X509Certificate;Ljava/util/Set;)Ljava/util/Collection;
        //   101: astore          5
        //   103: aload           5
        //   105: invokeinterface java/util/Collection.size:()I
        //   110: iconst_1       
        //   111: if_icmple       165
        //   114: aload_0        
        //   115: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   118: dup            
        //   119: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   121: ldc_w           "CertPathReviewer.conflictingTrustAnchors"
        //   124: iconst_2       
        //   125: anewarray       Ljava/lang/Object;
        //   128: dup            
        //   129: iconst_0       
        //   130: aload           5
        //   132: invokeinterface java/util/Collection.size:()I
        //   137: invokestatic    org/bouncycastle/util/Integers.valueOf:(I)Ljava/lang/Integer;
        //   140: aastore        
        //   141: dup            
        //   142: iconst_1       
        //   143: new             Lorg/bouncycastle/i18n/filter/UntrustedInput;
        //   146: dup            
        //   147: aload           7
        //   149: invokevirtual   java/security/cert/X509Certificate.getIssuerX500Principal:()Ljavax/security/auth/x500/X500Principal;
        //   152: invokespecial   org/bouncycastle/i18n/filter/UntrustedInput.<init>:(Ljava/lang/Object;)V
        //   155: aastore        
        //   156: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //   159: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //   162: goto            1774
        //   165: aload           5
        //   167: invokeinterface java/util/Collection.isEmpty:()Z
        //   172: ifeq            231
        //   175: aload_0        
        //   176: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   179: dup            
        //   180: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   182: ldc_w           "CertPathReviewer.noTrustAnchorFound"
        //   185: iconst_2       
        //   186: anewarray       Ljava/lang/Object;
        //   189: dup            
        //   190: iconst_0       
        //   191: new             Lorg/bouncycastle/i18n/filter/UntrustedInput;
        //   194: dup            
        //   195: aload           7
        //   197: invokevirtual   java/security/cert/X509Certificate.getIssuerX500Principal:()Ljavax/security/auth/x500/X500Principal;
        //   200: invokespecial   org/bouncycastle/i18n/filter/UntrustedInput.<init>:(Ljava/lang/Object;)V
        //   203: aastore        
        //   204: dup            
        //   205: iconst_1       
        //   206: aload_0        
        //   207: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.pkixParams:Ljava/security/cert/PKIXParameters;
        //   210: invokevirtual   java/security/cert/PKIXParameters.getTrustAnchors:()Ljava/util/Set;
        //   213: invokeinterface java/util/Set.size:()I
        //   218: invokestatic    org/bouncycastle/util/Integers.valueOf:(I)Ljava/lang/Integer;
        //   221: aastore        
        //   222: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //   225: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //   228: goto            1774
        //   231: aload           5
        //   233: invokeinterface java/util/Collection.iterator:()Ljava/util/Iterator;
        //   238: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   243: checkcast       Ljava/security/cert/TrustAnchor;
        //   246: astore          5
        //   248: aload           5
        //   250: invokevirtual   java/security/cert/TrustAnchor.getTrustedCert:()Ljava/security/cert/X509Certificate;
        //   253: ifnull          269
        //   256: aload           5
        //   258: invokevirtual   java/security/cert/TrustAnchor.getTrustedCert:()Ljava/security/cert/X509Certificate;
        //   261: invokevirtual   java/security/cert/X509Certificate.getPublicKey:()Ljava/security/PublicKey;
        //   264: astore          6
        //   266: goto            276
        //   269: aload           5
        //   271: invokevirtual   java/security/cert/TrustAnchor.getCAPublicKey:()Ljava/security/PublicKey;
        //   274: astore          6
        //   276: aload           7
        //   278: aload           6
        //   280: aload_0        
        //   281: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.pkixParams:Ljava/security/cert/PKIXParameters;
        //   284: invokevirtual   java/security/cert/PKIXParameters.getSigProvider:()Ljava/lang/String;
        //   287: invokestatic    org/bouncycastle/x509/CertPathValidatorUtilities.verifyX509Certificate:(Ljava/security/cert/X509Certificate;Ljava/security/PublicKey;Ljava/lang/String;)V
        //   290: goto            391
        //   293: aload_0        
        //   294: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   297: dup            
        //   298: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   300: ldc_w           "CertPathReviewer.trustButInvalidCert"
        //   303: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   306: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //   309: goto            391
        //   312: astore          6
        //   314: goto            327
        //   317: astore          6
        //   319: goto            382
        //   322: astore          6
        //   324: aconst_null    
        //   325: astore          5
        //   327: aload_0        
        //   328: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   331: dup            
        //   332: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   334: ldc_w           "CertPathReviewer.unknown"
        //   337: iconst_2       
        //   338: anewarray       Ljava/lang/Object;
        //   341: dup            
        //   342: iconst_0       
        //   343: new             Lorg/bouncycastle/i18n/filter/UntrustedInput;
        //   346: dup            
        //   347: aload           6
        //   349: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   352: invokespecial   org/bouncycastle/i18n/filter/UntrustedInput.<init>:(Ljava/lang/Object;)V
        //   355: aastore        
        //   356: dup            
        //   357: iconst_1       
        //   358: new             Lorg/bouncycastle/i18n/filter/UntrustedInput;
        //   361: dup            
        //   362: aload           6
        //   364: invokespecial   org/bouncycastle/i18n/filter/UntrustedInput.<init>:(Ljava/lang/Object;)V
        //   367: aastore        
        //   368: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //   371: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //   374: goto            391
        //   377: astore          6
        //   379: aconst_null    
        //   380: astore          5
        //   382: aload_0        
        //   383: aload           6
        //   385: invokevirtual   org/bouncycastle/x509/CertPathReviewerException.getErrorMessage:()Lorg/bouncycastle/i18n/ErrorBundle;
        //   388: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //   391: aload           5
        //   393: astore          8
        //   395: aload           8
        //   397: ifnull          536
        //   400: aload           8
        //   402: invokevirtual   java/security/cert/TrustAnchor.getTrustedCert:()Ljava/security/cert/X509Certificate;
        //   405: astore          7
        //   407: aload           7
        //   409: ifnull          422
        //   412: aload           7
        //   414: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getSubjectPrincipal:(Ljava/security/cert/X509Certificate;)Ljavax/security/auth/x500/X500Principal;
        //   417: astore          5
        //   419: goto            477
        //   422: new             Ljavax/security/auth/x500/X500Principal;
        //   425: dup            
        //   426: aload           8
        //   428: invokevirtual   java/security/cert/TrustAnchor.getCAName:()Ljava/lang/String;
        //   431: invokespecial   javax/security/auth/x500/X500Principal.<init>:(Ljava/lang/String;)V
        //   434: astore          5
        //   436: goto            477
        //   439: aload_0        
        //   440: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   443: dup            
        //   444: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   446: ldc_w           "CertPathReviewer.trustDNInvalid"
        //   449: iconst_1       
        //   450: anewarray       Ljava/lang/Object;
        //   453: dup            
        //   454: iconst_0       
        //   455: new             Lorg/bouncycastle/i18n/filter/UntrustedInput;
        //   458: dup            
        //   459: aload           8
        //   461: invokevirtual   java/security/cert/TrustAnchor.getCAName:()Ljava/lang/String;
        //   464: invokespecial   org/bouncycastle/i18n/filter/UntrustedInput.<init>:(Ljava/lang/Object;)V
        //   467: aastore        
        //   468: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //   471: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //   474: aconst_null    
        //   475: astore          5
        //   477: aload           5
        //   479: astore          6
        //   481: aload           7
        //   483: ifnull          539
        //   486: aload           7
        //   488: invokevirtual   java/security/cert/X509Certificate.getKeyUsage:()[Z
        //   491: astore          7
        //   493: aload           5
        //   495: astore          6
        //   497: aload           7
        //   499: ifnull          539
        //   502: aload           5
        //   504: astore          6
        //   506: aload           7
        //   508: iconst_5       
        //   509: baload         
        //   510: ifne            539
        //   513: aload_0        
        //   514: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   517: dup            
        //   518: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   520: ldc_w           "CertPathReviewer.trustKeyUsage"
        //   523: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   526: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addNotification:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //   529: aload           5
        //   531: astore          6
        //   533: goto            539
        //   536: aconst_null    
        //   537: astore          6
        //   539: aload           8
        //   541: ifnull          614
        //   544: aload           8
        //   546: invokevirtual   java/security/cert/TrustAnchor.getTrustedCert:()Ljava/security/cert/X509Certificate;
        //   549: astore          7
        //   551: aload           7
        //   553: ifnull          566
        //   556: aload           7
        //   558: invokevirtual   java/security/cert/X509Certificate.getPublicKey:()Ljava/security/PublicKey;
        //   561: astore          5
        //   563: goto            573
        //   566: aload           8
        //   568: invokevirtual   java/security/cert/TrustAnchor.getCAPublicKey:()Ljava/security/PublicKey;
        //   571: astore          5
        //   573: aload           5
        //   575: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getAlgorithmIdentifier:(Ljava/security/PublicKey;)Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //   578: astore          9
        //   580: aload           9
        //   582: invokevirtual   org/bouncycastle/asn1/x509/AlgorithmIdentifier.getAlgorithm:()Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //   585: pop            
        //   586: aload           9
        //   588: invokevirtual   org/bouncycastle/asn1/x509/AlgorithmIdentifier.getParameters:()Lorg/bouncycastle/asn1/ASN1Encodable;
        //   591: pop            
        //   592: goto            620
        //   595: aload_0        
        //   596: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   599: dup            
        //   600: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   602: ldc_w           "CertPathReviewer.trustPubKeyError"
        //   605: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   608: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //   611: goto            620
        //   614: aconst_null    
        //   615: astore          7
        //   617: aconst_null    
        //   618: astore          5
        //   620: aload_0        
        //   621: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.certs:Ljava/util/List;
        //   624: invokeinterface java/util/List.size:()I
        //   629: istore_1       
        //   630: aload           7
        //   632: astore          9
        //   634: aload           6
        //   636: astore          7
        //   638: iload_1        
        //   639: iconst_1       
        //   640: isub           
        //   641: istore_1       
        //   642: aload           5
        //   644: astore          6
        //   646: iload_1        
        //   647: iflt            1702
        //   650: aload_0        
        //   651: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.n:I
        //   654: iload_1        
        //   655: isub           
        //   656: istore          4
        //   658: aload_0        
        //   659: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.certs:Ljava/util/List;
        //   662: iload_1        
        //   663: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   668: checkcast       Ljava/security/cert/X509Certificate;
        //   671: astore          5
        //   673: aload           6
        //   675: ifnull          756
        //   678: aload           5
        //   680: aload           6
        //   682: aload_0        
        //   683: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.pkixParams:Ljava/security/cert/PKIXParameters;
        //   686: invokevirtual   java/security/cert/PKIXParameters.getSigProvider:()Ljava/lang/String;
        //   689: invokestatic    org/bouncycastle/x509/CertPathValidatorUtilities.verifyX509Certificate:(Ljava/security/cert/X509Certificate;Ljava/security/PublicKey;Ljava/lang/String;)V
        //   692: goto            1009
        //   695: astore          10
        //   697: iconst_3       
        //   698: anewarray       Ljava/lang/Object;
        //   701: astore          11
        //   703: aload           11
        //   705: iconst_0       
        //   706: aload           10
        //   708: invokevirtual   java/security/GeneralSecurityException.getMessage:()Ljava/lang/String;
        //   711: aastore        
        //   712: aload           11
        //   714: iload_3        
        //   715: aload           10
        //   717: aastore        
        //   718: aload           11
        //   720: iload_2        
        //   721: aload           10
        //   723: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //   726: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   729: aastore        
        //   730: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   733: dup            
        //   734: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   736: ldc_w           "CertPathReviewer.signatureNotVerified"
        //   739: aload           11
        //   741: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //   744: astore          10
        //   746: aload_0        
        //   747: aload           10
        //   749: iload_1        
        //   750: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //   753: goto            1009
        //   756: aload           5
        //   758: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.isSelfIssued:(Ljava/security/cert/X509Certificate;)Z
        //   761: ifeq            855
        //   764: aload           5
        //   766: aload           5
        //   768: invokevirtual   java/security/cert/X509Certificate.getPublicKey:()Ljava/security/PublicKey;
        //   771: aload_0        
        //   772: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.pkixParams:Ljava/security/cert/PKIXParameters;
        //   775: invokevirtual   java/security/cert/PKIXParameters.getSigProvider:()Ljava/lang/String;
        //   778: invokestatic    org/bouncycastle/x509/CertPathValidatorUtilities.verifyX509Certificate:(Ljava/security/cert/X509Certificate;Ljava/security/PublicKey;Ljava/lang/String;)V
        //   781: aload_0        
        //   782: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   785: dup            
        //   786: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   788: ldc_w           "CertPathReviewer.rootKeyIsValidButNotATrustAnchor"
        //   791: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   794: iload_1        
        //   795: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //   798: goto            1009
        //   801: astore          10
        //   803: iconst_3       
        //   804: anewarray       Ljava/lang/Object;
        //   807: astore          11
        //   809: aload           11
        //   811: iconst_0       
        //   812: aload           10
        //   814: invokevirtual   java/security/GeneralSecurityException.getMessage:()Ljava/lang/String;
        //   817: aastore        
        //   818: aload           11
        //   820: iload_3        
        //   821: aload           10
        //   823: aastore        
        //   824: aload           11
        //   826: iload_2        
        //   827: aload           10
        //   829: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //   832: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   835: aastore        
        //   836: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   839: dup            
        //   840: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   842: ldc_w           "CertPathReviewer.signatureNotVerified"
        //   845: aload           11
        //   847: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //   850: astore          10
        //   852: goto            746
        //   855: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   858: dup            
        //   859: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   861: ldc_w           "CertPathReviewer.NoIssuerPublicKey"
        //   864: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   867: astore          10
        //   869: aload           5
        //   871: getstatic       org/bouncycastle/asn1/x509/Extension.authorityKeyIdentifier:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //   874: invokevirtual   org/bouncycastle/asn1/ASN1ObjectIdentifier.getId:()Ljava/lang/String;
        //   877: invokevirtual   java/security/cert/X509Certificate.getExtensionValue:(Ljava/lang/String;)[B
        //   880: astore          11
        //   882: aload           11
        //   884: ifnull          1002
        //   887: aload           11
        //   889: invokestatic    org/bouncycastle/asn1/DEROctetString.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/ASN1OctetString;
        //   892: invokevirtual   org/bouncycastle/asn1/ASN1OctetString.getOctets:()[B
        //   895: invokestatic    org/bouncycastle/asn1/x509/AuthorityKeyIdentifier.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/AuthorityKeyIdentifier;
        //   898: astore          11
        //   900: aload           11
        //   902: invokevirtual   org/bouncycastle/asn1/x509/AuthorityKeyIdentifier.getAuthorityCertIssuer:()Lorg/bouncycastle/asn1/x509/GeneralNames;
        //   905: astore          12
        //   907: aload           12
        //   909: ifnull          1002
        //   912: aload           12
        //   914: invokevirtual   org/bouncycastle/asn1/x509/GeneralNames.getNames:()[Lorg/bouncycastle/asn1/x509/GeneralName;
        //   917: iconst_0       
        //   918: aaload         
        //   919: astore          12
        //   921: aload           11
        //   923: invokevirtual   org/bouncycastle/asn1/x509/AuthorityKeyIdentifier.getAuthorityCertSerialNumber:()Ljava/math/BigInteger;
        //   926: astore          11
        //   928: aload           11
        //   930: ifnull          1002
        //   933: aload           10
        //   935: bipush          7
        //   937: anewarray       Ljava/lang/Object;
        //   940: dup            
        //   941: iconst_0       
        //   942: new             Lorg/bouncycastle/i18n/LocaleString;
        //   945: dup            
        //   946: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   948: ldc_w           "missingIssuer"
        //   951: invokespecial   org/bouncycastle/i18n/LocaleString.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   954: aastore        
        //   955: dup            
        //   956: iconst_1       
        //   957: ldc_w           " \""
        //   960: aastore        
        //   961: dup            
        //   962: iconst_2       
        //   963: aload           12
        //   965: aastore        
        //   966: dup            
        //   967: iconst_3       
        //   968: ldc_w           "\" "
        //   971: aastore        
        //   972: dup            
        //   973: iconst_4       
        //   974: new             Lorg/bouncycastle/i18n/LocaleString;
        //   977: dup            
        //   978: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   980: ldc_w           "missingSerial"
        //   983: invokespecial   org/bouncycastle/i18n/LocaleString.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   986: aastore        
        //   987: dup            
        //   988: iconst_5       
        //   989: ldc_w           " "
        //   992: aastore        
        //   993: dup            
        //   994: bipush          6
        //   996: aload           11
        //   998: aastore        
        //   999: invokevirtual   org/bouncycastle/i18n/ErrorBundle.setExtraArguments:([Ljava/lang/Object;)V
        //  1002: aload_0        
        //  1003: aload           10
        //  1005: iload_1        
        //  1006: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //  1009: aload           5
        //  1011: aload_0        
        //  1012: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.validDate:Ljava/util/Date;
        //  1015: invokevirtual   java/security/cert/X509Certificate.checkValidity:(Ljava/util/Date;)V
        //  1018: goto            1097
        //  1021: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1024: dup            
        //  1025: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1027: ldc_w           "CertPathReviewer.certificateExpired"
        //  1030: iconst_1       
        //  1031: anewarray       Ljava/lang/Object;
        //  1034: dup            
        //  1035: iconst_0       
        //  1036: new             Lorg/bouncycastle/i18n/filter/TrustedInput;
        //  1039: dup            
        //  1040: aload           5
        //  1042: invokevirtual   java/security/cert/X509Certificate.getNotAfter:()Ljava/util/Date;
        //  1045: invokespecial   org/bouncycastle/i18n/filter/TrustedInput.<init>:(Ljava/lang/Object;)V
        //  1048: aastore        
        //  1049: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //  1052: astore          10
        //  1054: goto            1090
        //  1057: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1060: dup            
        //  1061: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1063: ldc_w           "CertPathReviewer.certificateNotYetValid"
        //  1066: iconst_1       
        //  1067: anewarray       Ljava/lang/Object;
        //  1070: dup            
        //  1071: iconst_0       
        //  1072: new             Lorg/bouncycastle/i18n/filter/TrustedInput;
        //  1075: dup            
        //  1076: aload           5
        //  1078: invokevirtual   java/security/cert/X509Certificate.getNotBefore:()Ljava/util/Date;
        //  1081: invokespecial   org/bouncycastle/i18n/filter/TrustedInput.<init>:(Ljava/lang/Object;)V
        //  1084: aastore        
        //  1085: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //  1088: astore          10
        //  1090: aload_0        
        //  1091: aload           10
        //  1093: iload_1        
        //  1094: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //  1097: aload_0        
        //  1098: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.pkixParams:Ljava/security/cert/PKIXParameters;
        //  1101: invokevirtual   java/security/cert/PKIXParameters.isRevocationEnabled:()Z
        //  1104: ifeq            1397
        //  1107: aload           5
        //  1109: getstatic       org/bouncycastle/x509/PKIXCertPathReviewer.CRL_DIST_POINTS:Ljava/lang/String;
        //  1112: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getExtensionValue:(Ljava/security/cert/X509Extension;Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1Primitive;
        //  1115: astore          10
        //  1117: aload           10
        //  1119: ifnull          1132
        //  1122: aload           10
        //  1124: invokestatic    org/bouncycastle/asn1/x509/CRLDistPoint.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/CRLDistPoint;
        //  1127: astore          10
        //  1129: goto            1158
        //  1132: aconst_null    
        //  1133: astore          10
        //  1135: goto            1158
        //  1138: aload_0        
        //  1139: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1142: dup            
        //  1143: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1145: ldc_w           "CertPathReviewer.crlDistPtExtError"
        //  1148: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1151: iload_1        
        //  1152: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //  1155: goto            1132
        //  1158: aload           5
        //  1160: getstatic       org/bouncycastle/x509/PKIXCertPathReviewer.AUTH_INFO_ACCESS:Ljava/lang/String;
        //  1163: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getExtensionValue:(Ljava/security/cert/X509Extension;Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1Primitive;
        //  1166: astore          11
        //  1168: aload           11
        //  1170: ifnull          1200
        //  1173: aload           11
        //  1175: invokestatic    org/bouncycastle/asn1/x509/AuthorityInformationAccess.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/AuthorityInformationAccess;
        //  1178: astore          11
        //  1180: goto            1203
        //  1183: aload_0        
        //  1184: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1187: dup            
        //  1188: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1190: ldc_w           "CertPathReviewer.crlAuthInfoAccError"
        //  1193: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1196: iload_1        
        //  1197: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //  1200: aconst_null    
        //  1201: astore          11
        //  1203: aload_0        
        //  1204: aload           10
        //  1206: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.getCRLDistUrls:(Lorg/bouncycastle/asn1/x509/CRLDistPoint;)Ljava/util/Vector;
        //  1209: astore          12
        //  1211: aload_0        
        //  1212: aload           11
        //  1214: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.getOCSPUrls:(Lorg/bouncycastle/asn1/x509/AuthorityInformationAccess;)Ljava/util/Vector;
        //  1217: astore          11
        //  1219: aload           12
        //  1221: invokevirtual   java/util/Vector.iterator:()Ljava/util/Iterator;
        //  1224: astore          10
        //  1226: aload           10
        //  1228: invokeinterface java/util/Iterator.hasNext:()Z
        //  1233: ifeq            1277
        //  1236: aload_0        
        //  1237: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1240: dup            
        //  1241: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1243: ldc_w           "CertPathReviewer.crlDistPoint"
        //  1246: iconst_1       
        //  1247: anewarray       Ljava/lang/Object;
        //  1250: dup            
        //  1251: iconst_0       
        //  1252: new             Lorg/bouncycastle/i18n/filter/UntrustedUrlInput;
        //  1255: dup            
        //  1256: aload           10
        //  1258: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //  1263: invokespecial   org/bouncycastle/i18n/filter/UntrustedUrlInput.<init>:(Ljava/lang/Object;)V
        //  1266: aastore        
        //  1267: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //  1270: iload_1        
        //  1271: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addNotification:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //  1274: goto            1226
        //  1277: aload           11
        //  1279: invokevirtual   java/util/Vector.iterator:()Ljava/util/Iterator;
        //  1282: astore          10
        //  1284: aload           10
        //  1286: invokeinterface java/util/Iterator.hasNext:()Z
        //  1291: ifeq            1335
        //  1294: aload_0        
        //  1295: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1298: dup            
        //  1299: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1301: ldc_w           "CertPathReviewer.ocspLocation"
        //  1304: iconst_1       
        //  1305: anewarray       Ljava/lang/Object;
        //  1308: dup            
        //  1309: iconst_0       
        //  1310: new             Lorg/bouncycastle/i18n/filter/UntrustedUrlInput;
        //  1313: dup            
        //  1314: aload           10
        //  1316: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //  1321: invokespecial   org/bouncycastle/i18n/filter/UntrustedUrlInput.<init>:(Ljava/lang/Object;)V
        //  1324: aastore        
        //  1325: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //  1328: iload_1        
        //  1329: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addNotification:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //  1332: goto            1284
        //  1335: aload_0        
        //  1336: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.pkixParams:Ljava/security/cert/PKIXParameters;
        //  1339: astore          13
        //  1341: aload_0        
        //  1342: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.validDate:Ljava/util/Date;
        //  1345: astore          14
        //  1347: aload           5
        //  1349: astore          10
        //  1351: aload_0        
        //  1352: aload           13
        //  1354: aload           10
        //  1356: aload           14
        //  1358: aload           9
        //  1360: aload           6
        //  1362: aload           12
        //  1364: aload           11
        //  1366: iload_1        
        //  1367: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.checkRevocation:(Ljava/security/cert/PKIXParameters;Ljava/security/cert/X509Certificate;Ljava/util/Date;Ljava/security/cert/X509Certificate;Ljava/security/PublicKey;Ljava/util/Vector;Ljava/util/Vector;I)V
        //  1370: aload           10
        //  1372: astore          5
        //  1374: goto            1397
        //  1377: astore          9
        //  1379: goto            1384
        //  1382: astore          9
        //  1384: aload_0        
        //  1385: aload           9
        //  1387: invokevirtual   org/bouncycastle/x509/CertPathReviewerException.getErrorMessage:()Lorg/bouncycastle/i18n/ErrorBundle;
        //  1390: iload_1        
        //  1391: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //  1394: goto            1397
        //  1397: aload           7
        //  1399: ifnull          1458
        //  1402: aload           5
        //  1404: invokevirtual   java/security/cert/X509Certificate.getIssuerX500Principal:()Ljavax/security/auth/x500/X500Principal;
        //  1407: aload           7
        //  1409: invokevirtual   javax/security/auth/x500/X500Principal.equals:(Ljava/lang/Object;)Z
        //  1412: ifne            1458
        //  1415: aload_0        
        //  1416: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1419: dup            
        //  1420: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1422: ldc_w           "CertPathReviewer.certWrongIssuer"
        //  1425: iconst_2       
        //  1426: anewarray       Ljava/lang/Object;
        //  1429: dup            
        //  1430: iconst_0       
        //  1431: aload           7
        //  1433: invokevirtual   javax/security/auth/x500/X500Principal.getName:()Ljava/lang/String;
        //  1436: aastore        
        //  1437: dup            
        //  1438: iconst_1       
        //  1439: aload           5
        //  1441: invokevirtual   java/security/cert/X509Certificate.getIssuerX500Principal:()Ljavax/security/auth/x500/X500Principal;
        //  1444: invokevirtual   javax/security/auth/x500/X500Principal.getName:()Ljava/lang/String;
        //  1447: aastore        
        //  1448: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //  1451: iload_1        
        //  1452: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //  1455: goto            1458
        //  1458: iload           4
        //  1460: aload_0        
        //  1461: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.n:I
        //  1464: if_icmpeq       1623
        //  1467: aload           5
        //  1469: ifnull          1501
        //  1472: aload           5
        //  1474: invokevirtual   java/security/cert/X509Certificate.getVersion:()I
        //  1477: iconst_1       
        //  1478: if_icmpne       1501
        //  1481: aload_0        
        //  1482: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1485: dup            
        //  1486: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1488: ldc_w           "CertPathReviewer.noCACert"
        //  1491: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1494: iload_1        
        //  1495: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //  1498: goto            1501
        //  1501: aload           5
        //  1503: getstatic       org/bouncycastle/x509/PKIXCertPathReviewer.BASIC_CONSTRAINTS:Ljava/lang/String;
        //  1506: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getExtensionValue:(Ljava/security/cert/X509Extension;Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1Primitive;
        //  1509: invokestatic    org/bouncycastle/asn1/x509/BasicConstraints.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/BasicConstraints;
        //  1512: astore          7
        //  1514: aload           7
        //  1516: ifnull          1547
        //  1519: aload           7
        //  1521: invokevirtual   org/bouncycastle/asn1/x509/BasicConstraints.isCA:()Z
        //  1524: ifne            1584
        //  1527: aload_0        
        //  1528: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1531: dup            
        //  1532: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1534: ldc_w           "CertPathReviewer.noCACert"
        //  1537: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1540: iload_1        
        //  1541: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //  1544: goto            1584
        //  1547: aload_0        
        //  1548: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1551: dup            
        //  1552: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1554: ldc_w           "CertPathReviewer.noBasicConstraints"
        //  1557: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1560: iload_1        
        //  1561: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //  1564: goto            1584
        //  1567: aload_0        
        //  1568: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1571: dup            
        //  1572: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1574: ldc_w           "CertPathReviewer.errorProcesingBC"
        //  1577: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1580: iload_1        
        //  1581: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //  1584: aload           5
        //  1586: invokevirtual   java/security/cert/X509Certificate.getKeyUsage:()[Z
        //  1589: astore          7
        //  1591: aload           7
        //  1593: ifnull          1623
        //  1596: aload           7
        //  1598: iconst_5       
        //  1599: baload         
        //  1600: ifne            1623
        //  1603: aload_0        
        //  1604: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1607: dup            
        //  1608: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1610: ldc_w           "CertPathReviewer.noCertSign"
        //  1613: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1616: iload_1        
        //  1617: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //  1620: goto            1623
        //  1623: aload           5
        //  1625: invokevirtual   java/security/cert/X509Certificate.getSubjectX500Principal:()Ljavax/security/auth/x500/X500Principal;
        //  1628: astore          10
        //  1630: aload_0        
        //  1631: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.certs:Ljava/util/List;
        //  1634: iload_1        
        //  1635: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getNextWorkingKey:(Ljava/util/List;I)Ljava/security/PublicKey;
        //  1638: astore          7
        //  1640: aload           7
        //  1642: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getAlgorithmIdentifier:(Ljava/security/PublicKey;)Lorg/bouncycastle/asn1/x509/AlgorithmIdentifier;
        //  1645: astore          6
        //  1647: aload           6
        //  1649: invokevirtual   org/bouncycastle/asn1/x509/AlgorithmIdentifier.getAlgorithm:()Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //  1652: pop            
        //  1653: aload           6
        //  1655: invokevirtual   org/bouncycastle/asn1/x509/AlgorithmIdentifier.getParameters:()Lorg/bouncycastle/asn1/ASN1Encodable;
        //  1658: pop            
        //  1659: aload           7
        //  1661: astore          6
        //  1663: goto            1683
        //  1666: aload_0        
        //  1667: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1670: dup            
        //  1671: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1673: ldc_w           "CertPathReviewer.pubKeyError"
        //  1676: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1679: iload_1        
        //  1680: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //  1683: iload_1        
        //  1684: iconst_1       
        //  1685: isub           
        //  1686: istore_1       
        //  1687: iconst_2       
        //  1688: istore_2       
        //  1689: iconst_1       
        //  1690: istore_3       
        //  1691: aload           5
        //  1693: astore          9
        //  1695: aload           10
        //  1697: astore          7
        //  1699: goto            646
        //  1702: aload_0        
        //  1703: aload           8
        //  1705: putfield        org/bouncycastle/x509/PKIXCertPathReviewer.trustAnchor:Ljava/security/cert/TrustAnchor;
        //  1708: aload_0        
        //  1709: aload           6
        //  1711: putfield        org/bouncycastle/x509/PKIXCertPathReviewer.subjectPublicKey:Ljava/security/PublicKey;
        //  1714: return         
        //  1715: astore          6
        //  1717: goto            293
        //  1720: astore          6
        //  1722: goto            391
        //  1725: astore          5
        //  1727: goto            439
        //  1730: astore          9
        //  1732: goto            595
        //  1735: astore          10
        //  1737: goto            1057
        //  1740: astore          10
        //  1742: goto            1021
        //  1745: astore          10
        //  1747: goto            1138
        //  1750: astore          11
        //  1752: goto            1183
        //  1755: astore          7
        //  1757: goto            1567
        //  1760: astore          7
        //  1762: goto            1666
        //  1765: astore          6
        //  1767: aload           7
        //  1769: astore          6
        //  1771: goto            1666
        //  1774: aconst_null    
        //  1775: astore          5
        //  1777: goto            391
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                                
        //  -----  -----  -----  -----  ----------------------------------------------------
        //  63     162    377    382    Lorg/bouncycastle/x509/CertPathReviewerException;
        //  63     162    322    327    Any
        //  165    228    377    382    Lorg/bouncycastle/x509/CertPathReviewerException;
        //  165    228    322    327    Any
        //  231    248    377    382    Lorg/bouncycastle/x509/CertPathReviewerException;
        //  231    248    322    327    Any
        //  248    266    317    322    Lorg/bouncycastle/x509/CertPathReviewerException;
        //  248    266    312    317    Any
        //  269    276    317    322    Lorg/bouncycastle/x509/CertPathReviewerException;
        //  269    276    312    317    Any
        //  276    290    1715   312    Ljava/security/SignatureException;
        //  276    290    1720   1725   Ljava/lang/Exception;
        //  276    290    317    322    Lorg/bouncycastle/x509/CertPathReviewerException;
        //  276    290    312    317    Any
        //  293    309    317    322    Lorg/bouncycastle/x509/CertPathReviewerException;
        //  293    309    312    317    Any
        //  412    419    1725   477    Ljava/lang/IllegalArgumentException;
        //  422    436    1725   477    Ljava/lang/IllegalArgumentException;
        //  573    592    1730   614    Ljava/security/cert/CertPathValidatorException;
        //  678    692    695    746    Ljava/security/GeneralSecurityException;
        //  764    798    801    855    Ljava/security/GeneralSecurityException;
        //  1009   1018   1735   1090   Ljava/security/cert/CertificateNotYetValidException;
        //  1009   1018   1740   1057   Ljava/security/cert/CertificateExpiredException;
        //  1107   1117   1745   1158   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1122   1129   1745   1158   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1158   1168   1750   1200   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1173   1180   1750   1200   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1335   1347   1382   1384   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1351   1370   1377   1382   Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1501   1514   1755   1584   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1519   1544   1755   1584   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1547   1564   1755   1584   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1630   1640   1760   1765   Ljava/security/cert/CertPathValidatorException;
        //  1640   1659   1765   1774   Ljava/security/cert/CertPathValidatorException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 825 out of bounds for length 825
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:373)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:426)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private X509CRL getCRL(final String spec) throws CertPathReviewerException {
        try {
            final URL url = new URL(spec);
            if (url.getProtocol().equals("http") || url.getProtocol().equals("https")) {
                final HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setUseCaches(false);
                httpURLConnection.setDoInput(true);
                httpURLConnection.connect();
                if (httpURLConnection.getResponseCode() == 200) {
                    return (X509CRL)CertificateFactory.getInstance("X.509", "BC").generateCRL(httpURLConnection.getInputStream());
                }
                throw new Exception(httpURLConnection.getResponseMessage());
            }
        }
        catch (Exception ex) {
            throw new CertPathReviewerException(new ErrorBundle("org.bouncycastle.x509.CertPathReviewerMessages", "CertPathReviewer.loadCrlDistPointError", new Object[] { new UntrustedInput(spec), ex.getMessage(), ex, ex.getClass().getName() }));
        }
        return null;
    }
    
    private boolean processQcStatements(final X509Certificate x509Certificate, final int n) {
        while (true) {
            try {
                final ASN1Sequence asn1Sequence = (ASN1Sequence)CertPathValidatorUtilities.getExtensionValue(x509Certificate, PKIXCertPathReviewer.QC_STATEMENT);
                boolean b;
                for (int i = (b = false) ? 1 : 0; i < asn1Sequence.size(); ++i) {
                    final QCStatement instance = QCStatement.getInstance(asn1Sequence.getObjectAt(i));
                    ErrorBundle errorBundle;
                    if (QCStatement.id_etsi_qcs_QcCompliance.equals(instance.getStatementId())) {
                        errorBundle = new ErrorBundle("org.bouncycastle.x509.CertPathReviewerMessages", "CertPathReviewer.QcEuCompliance");
                    }
                    else {
                        if (QCStatement.id_qcs_pkixQCSyntax_v1.equals(instance.getStatementId())) {
                            continue;
                        }
                        if (QCStatement.id_etsi_qcs_QcSSCD.equals(instance.getStatementId())) {
                            errorBundle = new ErrorBundle("org.bouncycastle.x509.CertPathReviewerMessages", "CertPathReviewer.QcSSCD");
                        }
                        else {
                            if (QCStatement.id_etsi_qcs_LimiteValue.equals(instance.getStatementId())) {
                                final MonetaryValue instance2 = MonetaryValue.getInstance(instance.getStatementInfo());
                                instance2.getCurrency();
                                final double n2 = instance2.getAmount().doubleValue() * Math.pow(10.0, instance2.getExponent().doubleValue());
                                ErrorBundle errorBundle2;
                                if (instance2.getCurrency().isAlphabetic()) {
                                    errorBundle2 = new ErrorBundle("org.bouncycastle.x509.CertPathReviewerMessages", "CertPathReviewer.QcLimitValueAlpha", new Object[] { instance2.getCurrency().getAlphabetic(), new TrustedInput(new Double(n2)), instance2 });
                                }
                                else {
                                    errorBundle2 = new ErrorBundle("org.bouncycastle.x509.CertPathReviewerMessages", "CertPathReviewer.QcLimitValueNum", new Object[] { Integers.valueOf(instance2.getCurrency().getNumeric()), new TrustedInput(new Double(n2)), instance2 });
                                }
                                this.addNotification(errorBundle2, n);
                                continue;
                            }
                            this.addNotification(new ErrorBundle("org.bouncycastle.x509.CertPathReviewerMessages", "CertPathReviewer.QcUnknownStatement", new Object[] { instance.getStatementId(), new UntrustedInput(instance) }), n);
                            b = true;
                            continue;
                        }
                    }
                    this.addNotification(errorBundle, n);
                }
                return true ^ b;
                this.addError(new ErrorBundle("org.bouncycastle.x509.CertPathReviewerMessages", "CertPathReviewer.QcStatementExtError"), n);
                return false;
            }
            catch (AnnotatedException ex) {
                continue;
            }
            break;
        }
    }
    
    protected void addError(final ErrorBundle errorBundle) {
        this.errors[0].add(errorBundle);
    }
    
    protected void addError(final ErrorBundle errorBundle, final int n) {
        if (n >= -1 && n < this.n) {
            this.errors[n + 1].add(errorBundle);
            return;
        }
        throw new IndexOutOfBoundsException();
    }
    
    protected void addNotification(final ErrorBundle errorBundle) {
        this.notifications[0].add(errorBundle);
    }
    
    protected void addNotification(final ErrorBundle errorBundle, final int n) {
        if (n >= -1 && n < this.n) {
            this.notifications[n + 1].add(errorBundle);
            return;
        }
        throw new IndexOutOfBoundsException();
    }
    
    protected void checkCRLs(final PKIXParameters p0, final X509Certificate p1, final Date p2, final X509Certificate p3, final PublicKey p4, final Vector p5, final int p6) throws CertPathReviewerException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   org/bouncycastle/x509/X509CRLStoreSelector.<init>:()V
        //     7: astore          10
        //     9: aload           10
        //    11: aload_2        
        //    12: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getEncodedIssuerPrincipal:(Ljava/lang/Object;)Ljavax/security/auth/x500/X500Principal;
        //    15: invokevirtual   javax/security/auth/x500/X500Principal.getEncoded:()[B
        //    18: invokevirtual   org/bouncycastle/x509/X509CRLStoreSelector.addIssuerName:([B)V
        //    21: aload           10
        //    23: aload_2        
        //    24: invokevirtual   org/bouncycastle/x509/X509CRLStoreSelector.setCertificateChecking:(Ljava/security/cert/X509Certificate;)V
        //    27: getstatic       org/bouncycastle/x509/PKIXCertPathReviewer.CRL_UTIL:Lorg/bouncycastle/x509/PKIXCRLUtil;
        //    30: aload           10
        //    32: aload_1        
        //    33: invokevirtual   org/bouncycastle/x509/PKIXCRLUtil.findCRLs:(Lorg/bouncycastle/x509/X509CRLStoreSelector;Ljava/security/cert/PKIXParameters;)Ljava/util/Set;
        //    36: astore          12
        //    38: aload           12
        //    40: invokeinterface java/util/Collection.iterator:()Ljava/util/Iterator;
        //    45: astore          11
        //    47: aload           12
        //    49: invokeinterface java/util/Collection.isEmpty:()Z
        //    54: ifeq            190
        //    57: getstatic       org/bouncycastle/x509/PKIXCertPathReviewer.CRL_UTIL:Lorg/bouncycastle/x509/PKIXCRLUtil;
        //    60: new             Lorg/bouncycastle/x509/X509CRLStoreSelector;
        //    63: dup            
        //    64: invokespecial   org/bouncycastle/x509/X509CRLStoreSelector.<init>:()V
        //    67: aload_1        
        //    68: invokevirtual   org/bouncycastle/x509/PKIXCRLUtil.findCRLs:(Lorg/bouncycastle/x509/X509CRLStoreSelector;Ljava/security/cert/PKIXParameters;)Ljava/util/Set;
        //    71: invokeinterface java/util/Collection.iterator:()Ljava/util/Iterator;
        //    76: astore          12
        //    78: new             Ljava/util/ArrayList;
        //    81: dup            
        //    82: invokespecial   java/util/ArrayList.<init>:()V
        //    85: astore          13
        //    87: aload           12
        //    89: invokeinterface java/util/Iterator.hasNext:()Z
        //    94: ifeq            121
        //    97: aload           13
        //    99: aload           12
        //   101: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   106: checkcast       Ljava/security/cert/X509CRL;
        //   109: invokevirtual   java/security/cert/X509CRL.getIssuerX500Principal:()Ljavax/security/auth/x500/X500Principal;
        //   112: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   117: pop            
        //   118: goto            87
        //   121: aload           13
        //   123: invokeinterface java/util/List.size:()I
        //   128: istore          8
        //   130: aload_0        
        //   131: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   134: dup            
        //   135: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   137: ldc_w           "CertPathReviewer.noCrlInCertstore"
        //   140: iconst_3       
        //   141: anewarray       Ljava/lang/Object;
        //   144: dup            
        //   145: iconst_0       
        //   146: new             Lorg/bouncycastle/i18n/filter/UntrustedInput;
        //   149: dup            
        //   150: aload           10
        //   152: invokevirtual   org/bouncycastle/x509/X509CRLStoreSelector.getIssuerNames:()Ljava/util/Collection;
        //   155: invokespecial   org/bouncycastle/i18n/filter/UntrustedInput.<init>:(Ljava/lang/Object;)V
        //   158: aastore        
        //   159: dup            
        //   160: iconst_1       
        //   161: new             Lorg/bouncycastle/i18n/filter/UntrustedInput;
        //   164: dup            
        //   165: aload           13
        //   167: invokespecial   org/bouncycastle/i18n/filter/UntrustedInput.<init>:(Ljava/lang/Object;)V
        //   170: aastore        
        //   171: dup            
        //   172: iconst_2       
        //   173: iload           8
        //   175: invokestatic    org/bouncycastle/util/Integers.valueOf:(I)Ljava/lang/Integer;
        //   178: aastore        
        //   179: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //   182: iload           7
        //   184: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addNotification:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //   187: goto            190
        //   190: goto            262
        //   193: astore          10
        //   195: aload_0        
        //   196: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   199: dup            
        //   200: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   202: ldc_w           "CertPathReviewer.crlExtractionError"
        //   205: iconst_3       
        //   206: anewarray       Ljava/lang/Object;
        //   209: dup            
        //   210: iconst_0       
        //   211: aload           10
        //   213: invokevirtual   org/bouncycastle/jce/provider/AnnotatedException.getCause:()Ljava/lang/Throwable;
        //   216: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   219: aastore        
        //   220: dup            
        //   221: iconst_1       
        //   222: aload           10
        //   224: invokevirtual   org/bouncycastle/jce/provider/AnnotatedException.getCause:()Ljava/lang/Throwable;
        //   227: aastore        
        //   228: dup            
        //   229: iconst_2       
        //   230: aload           10
        //   232: invokevirtual   org/bouncycastle/jce/provider/AnnotatedException.getCause:()Ljava/lang/Throwable;
        //   235: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //   238: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   241: aastore        
        //   242: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //   245: iload           7
        //   247: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addError:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //   250: new             Ljava/util/ArrayList;
        //   253: dup            
        //   254: invokespecial   java/util/ArrayList.<init>:()V
        //   257: invokevirtual   java/util/ArrayList.iterator:()Ljava/util/Iterator;
        //   260: astore          11
        //   262: aconst_null    
        //   263: astore          10
        //   265: aload           11
        //   267: invokeinterface java/util/Iterator.hasNext:()Z
        //   272: ifeq            426
        //   275: aload           11
        //   277: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   282: checkcast       Ljava/security/cert/X509CRL;
        //   285: astore          10
        //   287: aload           10
        //   289: invokevirtual   java/security/cert/X509CRL.getNextUpdate:()Ljava/util/Date;
        //   292: ifnull          368
        //   295: aload_1        
        //   296: invokevirtual   java/security/cert/PKIXParameters.getDate:()Ljava/util/Date;
        //   299: aload           10
        //   301: invokevirtual   java/security/cert/X509CRL.getNextUpdate:()Ljava/util/Date;
        //   304: invokevirtual   java/util/Date.before:(Ljava/util/Date;)Z
        //   307: ifeq            313
        //   310: goto            368
        //   313: aload_0        
        //   314: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   317: dup            
        //   318: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   320: ldc_w           "CertPathReviewer.localInvalidCRL"
        //   323: iconst_2       
        //   324: anewarray       Ljava/lang/Object;
        //   327: dup            
        //   328: iconst_0       
        //   329: new             Lorg/bouncycastle/i18n/filter/TrustedInput;
        //   332: dup            
        //   333: aload           10
        //   335: invokevirtual   java/security/cert/X509CRL.getThisUpdate:()Ljava/util/Date;
        //   338: invokespecial   org/bouncycastle/i18n/filter/TrustedInput.<init>:(Ljava/lang/Object;)V
        //   341: aastore        
        //   342: dup            
        //   343: iconst_1       
        //   344: new             Lorg/bouncycastle/i18n/filter/TrustedInput;
        //   347: dup            
        //   348: aload           10
        //   350: invokevirtual   java/security/cert/X509CRL.getNextUpdate:()Ljava/util/Date;
        //   353: invokespecial   org/bouncycastle/i18n/filter/TrustedInput.<init>:(Ljava/lang/Object;)V
        //   356: aastore        
        //   357: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //   360: iload           7
        //   362: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addNotification:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //   365: goto            265
        //   368: aload_0        
        //   369: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   372: dup            
        //   373: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   375: ldc_w           "CertPathReviewer.localValidCRL"
        //   378: iconst_2       
        //   379: anewarray       Ljava/lang/Object;
        //   382: dup            
        //   383: iconst_0       
        //   384: new             Lorg/bouncycastle/i18n/filter/TrustedInput;
        //   387: dup            
        //   388: aload           10
        //   390: invokevirtual   java/security/cert/X509CRL.getThisUpdate:()Ljava/util/Date;
        //   393: invokespecial   org/bouncycastle/i18n/filter/TrustedInput.<init>:(Ljava/lang/Object;)V
        //   396: aastore        
        //   397: dup            
        //   398: iconst_1       
        //   399: new             Lorg/bouncycastle/i18n/filter/TrustedInput;
        //   402: dup            
        //   403: aload           10
        //   405: invokevirtual   java/security/cert/X509CRL.getNextUpdate:()Ljava/util/Date;
        //   408: invokespecial   org/bouncycastle/i18n/filter/TrustedInput.<init>:(Ljava/lang/Object;)V
        //   411: aastore        
        //   412: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //   415: iload           7
        //   417: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addNotification:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //   420: iconst_1       
        //   421: istore          8
        //   423: goto            429
        //   426: iconst_0       
        //   427: istore          8
        //   429: iload           8
        //   431: ifne            793
        //   434: aload           6
        //   436: invokevirtual   java/util/Vector.iterator:()Ljava/util/Iterator;
        //   439: astore          11
        //   441: aload           10
        //   443: astore          6
        //   445: aload           11
        //   447: invokeinterface java/util/Iterator.hasNext:()Z
        //   452: ifeq            790
        //   455: aload           11
        //   457: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //   462: checkcast       Ljava/lang/String;
        //   465: astore          12
        //   467: aload_0        
        //   468: aload           12
        //   470: invokespecial   org/bouncycastle/x509/PKIXCertPathReviewer.getCRL:(Ljava/lang/String;)Ljava/security/cert/X509CRL;
        //   473: astore          10
        //   475: aload           10
        //   477: ifnull          768
        //   480: aload_2        
        //   481: invokevirtual   java/security/cert/X509Certificate.getIssuerX500Principal:()Ljavax/security/auth/x500/X500Principal;
        //   484: aload           10
        //   486: invokevirtual   java/security/cert/X509CRL.getIssuerX500Principal:()Ljavax/security/auth/x500/X500Principal;
        //   489: invokevirtual   javax/security/auth/x500/X500Principal.equals:(Ljava/lang/Object;)Z
        //   492: istore          9
        //   494: iload           9
        //   496: ifne            580
        //   499: aload_0        
        //   500: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   503: dup            
        //   504: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   506: ldc_w           "CertPathReviewer.onlineCRLWrongCA"
        //   509: iconst_3       
        //   510: anewarray       Ljava/lang/Object;
        //   513: dup            
        //   514: iconst_0       
        //   515: new             Lorg/bouncycastle/i18n/filter/UntrustedInput;
        //   518: dup            
        //   519: aload           10
        //   521: invokevirtual   java/security/cert/X509CRL.getIssuerX500Principal:()Ljavax/security/auth/x500/X500Principal;
        //   524: invokevirtual   javax/security/auth/x500/X500Principal.getName:()Ljava/lang/String;
        //   527: invokespecial   org/bouncycastle/i18n/filter/UntrustedInput.<init>:(Ljava/lang/Object;)V
        //   530: aastore        
        //   531: dup            
        //   532: iconst_1       
        //   533: new             Lorg/bouncycastle/i18n/filter/UntrustedInput;
        //   536: dup            
        //   537: aload_2        
        //   538: invokevirtual   java/security/cert/X509Certificate.getIssuerX500Principal:()Ljavax/security/auth/x500/X500Principal;
        //   541: invokevirtual   javax/security/auth/x500/X500Principal.getName:()Ljava/lang/String;
        //   544: invokespecial   org/bouncycastle/i18n/filter/UntrustedInput.<init>:(Ljava/lang/Object;)V
        //   547: aastore        
        //   548: dup            
        //   549: iconst_2       
        //   550: new             Lorg/bouncycastle/i18n/filter/UntrustedUrlInput;
        //   553: dup            
        //   554: aload           12
        //   556: invokespecial   org/bouncycastle/i18n/filter/UntrustedUrlInput.<init>:(Ljava/lang/Object;)V
        //   559: aastore        
        //   560: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //   563: iload           7
        //   565: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addNotification:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //   568: goto            771
        //   571: astore          10
        //   573: goto            776
        //   576: astore_1       
        //   577: goto            776
        //   580: aload           10
        //   582: invokevirtual   java/security/cert/X509CRL.getNextUpdate:()Ljava/util/Date;
        //   585: ifnull          681
        //   588: aload_0        
        //   589: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.pkixParams:Ljava/security/cert/PKIXParameters;
        //   592: invokevirtual   java/security/cert/PKIXParameters.getDate:()Ljava/util/Date;
        //   595: aload           10
        //   597: invokevirtual   java/security/cert/X509CRL.getNextUpdate:()Ljava/util/Date;
        //   600: invokevirtual   java/util/Date.before:(Ljava/util/Date;)Z
        //   603: ifeq            609
        //   606: goto            681
        //   609: aload_0        
        //   610: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   613: dup            
        //   614: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   616: ldc_w           "CertPathReviewer.onlineInvalidCRL"
        //   619: iconst_3       
        //   620: anewarray       Ljava/lang/Object;
        //   623: dup            
        //   624: iconst_0       
        //   625: new             Lorg/bouncycastle/i18n/filter/TrustedInput;
        //   628: dup            
        //   629: aload           10
        //   631: invokevirtual   java/security/cert/X509CRL.getThisUpdate:()Ljava/util/Date;
        //   634: invokespecial   org/bouncycastle/i18n/filter/TrustedInput.<init>:(Ljava/lang/Object;)V
        //   637: aastore        
        //   638: dup            
        //   639: iconst_1       
        //   640: new             Lorg/bouncycastle/i18n/filter/TrustedInput;
        //   643: dup            
        //   644: aload           10
        //   646: invokevirtual   java/security/cert/X509CRL.getNextUpdate:()Ljava/util/Date;
        //   649: invokespecial   org/bouncycastle/i18n/filter/TrustedInput.<init>:(Ljava/lang/Object;)V
        //   652: aastore        
        //   653: dup            
        //   654: iconst_2       
        //   655: new             Lorg/bouncycastle/i18n/filter/UntrustedUrlInput;
        //   658: dup            
        //   659: aload           12
        //   661: invokespecial   org/bouncycastle/i18n/filter/UntrustedUrlInput.<init>:(Ljava/lang/Object;)V
        //   664: aastore        
        //   665: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //   668: iload           7
        //   670: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addNotification:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //   673: goto            771
        //   676: astore          10
        //   678: goto            776
        //   681: aload_0        
        //   682: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   685: dup            
        //   686: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   688: ldc_w           "CertPathReviewer.onlineValidCRL"
        //   691: iconst_3       
        //   692: anewarray       Ljava/lang/Object;
        //   695: dup            
        //   696: iconst_0       
        //   697: new             Lorg/bouncycastle/i18n/filter/TrustedInput;
        //   700: dup            
        //   701: aload           10
        //   703: invokevirtual   java/security/cert/X509CRL.getThisUpdate:()Ljava/util/Date;
        //   706: invokespecial   org/bouncycastle/i18n/filter/TrustedInput.<init>:(Ljava/lang/Object;)V
        //   709: aastore        
        //   710: dup            
        //   711: iconst_1       
        //   712: new             Lorg/bouncycastle/i18n/filter/TrustedInput;
        //   715: dup            
        //   716: aload           10
        //   718: invokevirtual   java/security/cert/X509CRL.getNextUpdate:()Ljava/util/Date;
        //   721: invokespecial   org/bouncycastle/i18n/filter/TrustedInput.<init>:(Ljava/lang/Object;)V
        //   724: aastore        
        //   725: dup            
        //   726: iconst_2       
        //   727: new             Lorg/bouncycastle/i18n/filter/UntrustedUrlInput;
        //   730: dup            
        //   731: aload           12
        //   733: invokespecial   org/bouncycastle/i18n/filter/UntrustedUrlInput.<init>:(Ljava/lang/Object;)V
        //   736: aastore        
        //   737: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //   740: iload           7
        //   742: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addNotification:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //   745: iconst_1       
        //   746: istore          8
        //   748: aload           10
        //   750: astore          6
        //   752: goto            797
        //   755: astore          10
        //   757: iconst_1       
        //   758: istore          8
        //   760: goto            776
        //   763: astore          10
        //   765: goto            776
        //   768: goto            568
        //   771: goto            445
        //   774: astore          10
        //   776: aload_0        
        //   777: aload           10
        //   779: invokevirtual   org/bouncycastle/x509/CertPathReviewerException.getErrorMessage:()Lorg/bouncycastle/i18n/ErrorBundle;
        //   782: iload           7
        //   784: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addNotification:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //   787: goto            445
        //   790: goto            797
        //   793: aload           10
        //   795: astore          6
        //   797: aload           6
        //   799: ifnull          1684
        //   802: aload           4
        //   804: ifnull          858
        //   807: aload           4
        //   809: invokevirtual   java/security/cert/X509Certificate.getKeyUsage:()[Z
        //   812: astore          4
        //   814: aload           4
        //   816: ifnull          858
        //   819: aload           4
        //   821: arraylength    
        //   822: bipush          7
        //   824: if_icmplt       838
        //   827: aload           4
        //   829: bipush          6
        //   831: baload         
        //   832: ifeq            838
        //   835: goto            858
        //   838: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //   841: dup            
        //   842: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   845: dup            
        //   846: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   848: ldc_w           "CertPathReviewer.noCrlSigningPermited"
        //   851: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   854: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //   857: athrow         
        //   858: aload           5
        //   860: ifnull          1664
        //   863: aload           6
        //   865: aload           5
        //   867: ldc_w           "BC"
        //   870: invokevirtual   java/security/cert/X509CRL.verify:(Ljava/security/PublicKey;Ljava/lang/String;)V
        //   873: aload           6
        //   875: aload_2        
        //   876: invokevirtual   java/security/cert/X509Certificate.getSerialNumber:()Ljava/math/BigInteger;
        //   879: invokevirtual   java/security/cert/X509CRL.getRevokedCertificate:(Ljava/math/BigInteger;)Ljava/security/cert/X509CRLEntry;
        //   882: astore          10
        //   884: aload           10
        //   886: ifnull          1088
        //   889: aload           10
        //   891: invokevirtual   java/security/cert/X509CRLEntry.hasExtensions:()Z
        //   894: ifeq            954
        //   897: aload           10
        //   899: getstatic       org/bouncycastle/asn1/x509/Extension.reasonCode:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;
        //   902: invokevirtual   org/bouncycastle/asn1/ASN1ObjectIdentifier.getId:()Ljava/lang/String;
        //   905: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getExtensionValue:(Ljava/security/cert/X509Extension;Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1Primitive;
        //   908: invokestatic    org/bouncycastle/asn1/ASN1Enumerated.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/ASN1Enumerated;
        //   911: astore          4
        //   913: aload           4
        //   915: ifnull          954
        //   918: getstatic       org/bouncycastle/x509/PKIXCertPathReviewer.crlReasons:[Ljava/lang/String;
        //   921: aload           4
        //   923: invokevirtual   org/bouncycastle/asn1/ASN1Enumerated.intValueExact:()I
        //   926: aaload         
        //   927: astore          4
        //   929: goto            957
        //   932: astore_1       
        //   933: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //   936: dup            
        //   937: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //   940: dup            
        //   941: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   943: ldc_w           "CertPathReviewer.crlReasonExtError"
        //   946: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   949: aload_1        
        //   950: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/lang/Throwable;)V
        //   953: athrow         
        //   954: aconst_null    
        //   955: astore          4
        //   957: aload           4
        //   959: astore          5
        //   961: aload           4
        //   963: ifnonnull       974
        //   966: getstatic       org/bouncycastle/x509/PKIXCertPathReviewer.crlReasons:[Ljava/lang/String;
        //   969: bipush          7
        //   971: aaload         
        //   972: astore          5
        //   974: new             Lorg/bouncycastle/i18n/LocaleString;
        //   977: dup            
        //   978: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //   980: aload           5
        //   982: invokespecial   org/bouncycastle/i18n/LocaleString.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   985: astore          4
        //   987: aload_3        
        //   988: aload           10
        //   990: invokevirtual   java/security/cert/X509CRLEntry.getRevocationDate:()Ljava/util/Date;
        //   993: invokevirtual   java/util/Date.before:(Ljava/util/Date;)Z
        //   996: ifeq            1044
        //   999: aload_0        
        //  1000: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1003: dup            
        //  1004: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1006: ldc_w           "CertPathReviewer.revokedAfterValidation"
        //  1009: iconst_2       
        //  1010: anewarray       Ljava/lang/Object;
        //  1013: dup            
        //  1014: iconst_0       
        //  1015: new             Lorg/bouncycastle/i18n/filter/TrustedInput;
        //  1018: dup            
        //  1019: aload           10
        //  1021: invokevirtual   java/security/cert/X509CRLEntry.getRevocationDate:()Ljava/util/Date;
        //  1024: invokespecial   org/bouncycastle/i18n/filter/TrustedInput.<init>:(Ljava/lang/Object;)V
        //  1027: aastore        
        //  1028: dup            
        //  1029: iconst_1       
        //  1030: aload           4
        //  1032: aastore        
        //  1033: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //  1036: iload           7
        //  1038: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addNotification:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //  1041: goto            1106
        //  1044: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1047: dup            
        //  1048: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1051: dup            
        //  1052: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1054: ldc_w           "CertPathReviewer.certRevoked"
        //  1057: iconst_2       
        //  1058: anewarray       Ljava/lang/Object;
        //  1061: dup            
        //  1062: iconst_0       
        //  1063: new             Lorg/bouncycastle/i18n/filter/TrustedInput;
        //  1066: dup            
        //  1067: aload           10
        //  1069: invokevirtual   java/security/cert/X509CRLEntry.getRevocationDate:()Ljava/util/Date;
        //  1072: invokespecial   org/bouncycastle/i18n/filter/TrustedInput.<init>:(Ljava/lang/Object;)V
        //  1075: aastore        
        //  1076: dup            
        //  1077: iconst_1       
        //  1078: aload           4
        //  1080: aastore        
        //  1081: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //  1084: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //  1087: athrow         
        //  1088: aload_0        
        //  1089: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1092: dup            
        //  1093: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1095: ldc_w           "CertPathReviewer.notRevoked"
        //  1098: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1101: iload           7
        //  1103: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addNotification:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //  1106: aload           6
        //  1108: invokevirtual   java/security/cert/X509CRL.getNextUpdate:()Ljava/util/Date;
        //  1111: ifnull          1172
        //  1114: aload           6
        //  1116: invokevirtual   java/security/cert/X509CRL.getNextUpdate:()Ljava/util/Date;
        //  1119: aload_0        
        //  1120: getfield        org/bouncycastle/x509/PKIXCertPathReviewer.pkixParams:Ljava/security/cert/PKIXParameters;
        //  1123: invokevirtual   java/security/cert/PKIXParameters.getDate:()Ljava/util/Date;
        //  1126: invokevirtual   java/util/Date.before:(Ljava/util/Date;)Z
        //  1129: ifeq            1172
        //  1132: aload_0        
        //  1133: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1136: dup            
        //  1137: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1139: ldc_w           "CertPathReviewer.crlUpdateAvailable"
        //  1142: iconst_1       
        //  1143: anewarray       Ljava/lang/Object;
        //  1146: dup            
        //  1147: iconst_0       
        //  1148: new             Lorg/bouncycastle/i18n/filter/TrustedInput;
        //  1151: dup            
        //  1152: aload           6
        //  1154: invokevirtual   java/security/cert/X509CRL.getNextUpdate:()Ljava/util/Date;
        //  1157: invokespecial   org/bouncycastle/i18n/filter/TrustedInput.<init>:(Ljava/lang/Object;)V
        //  1160: aastore        
        //  1161: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
        //  1164: iload           7
        //  1166: invokevirtual   org/bouncycastle/x509/PKIXCertPathReviewer.addNotification:(Lorg/bouncycastle/i18n/ErrorBundle;I)V
        //  1169: goto            1172
        //  1172: iconst_1       
        //  1173: istore          7
        //  1175: aload           6
        //  1177: getstatic       org/bouncycastle/x509/PKIXCertPathReviewer.ISSUING_DISTRIBUTION_POINT:Ljava/lang/String;
        //  1180: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getExtensionValue:(Ljava/security/cert/X509Extension;Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1Primitive;
        //  1183: astore_3       
        //  1184: aload           6
        //  1186: getstatic       org/bouncycastle/x509/PKIXCertPathReviewer.DELTA_CRL_INDICATOR:Ljava/lang/String;
        //  1189: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getExtensionValue:(Ljava/security/cert/X509Extension;Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1Primitive;
        //  1192: astore          4
        //  1194: aload           4
        //  1196: ifnull          1448
        //  1199: new             Lorg/bouncycastle/x509/X509CRLStoreSelector;
        //  1202: dup            
        //  1203: invokespecial   org/bouncycastle/x509/X509CRLStoreSelector.<init>:()V
        //  1206: astore          5
        //  1208: aload           5
        //  1210: aload           6
        //  1212: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getIssuerPrincipal:(Ljava/security/cert/X509CRL;)Ljavax/security/auth/x500/X500Principal;
        //  1215: invokevirtual   javax/security/auth/x500/X500Principal.getEncoded:()[B
        //  1218: invokevirtual   org/bouncycastle/x509/X509CRLStoreSelector.addIssuerName:([B)V
        //  1221: aload           5
        //  1223: aload           4
        //  1225: checkcast       Lorg/bouncycastle/asn1/ASN1Integer;
        //  1228: invokevirtual   org/bouncycastle/asn1/ASN1Integer.getPositiveValue:()Ljava/math/BigInteger;
        //  1231: invokevirtual   org/bouncycastle/x509/X509CRLStoreSelector.setMinCRLNumber:(Ljava/math/BigInteger;)V
        //  1234: aload           5
        //  1236: aload           6
        //  1238: getstatic       org/bouncycastle/x509/PKIXCertPathReviewer.CRL_NUMBER:Ljava/lang/String;
        //  1241: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getExtensionValue:(Ljava/security/cert/X509Extension;Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1Primitive;
        //  1244: checkcast       Lorg/bouncycastle/asn1/ASN1Integer;
        //  1247: invokevirtual   org/bouncycastle/asn1/ASN1Integer.getPositiveValue:()Ljava/math/BigInteger;
        //  1250: lconst_1       
        //  1251: invokestatic    java/math/BigInteger.valueOf:(J)Ljava/math/BigInteger;
        //  1254: invokevirtual   java/math/BigInteger.subtract:(Ljava/math/BigInteger;)Ljava/math/BigInteger;
        //  1257: invokevirtual   org/bouncycastle/x509/X509CRLStoreSelector.setMaxCRLNumber:(Ljava/math/BigInteger;)V
        //  1260: getstatic       org/bouncycastle/x509/PKIXCertPathReviewer.CRL_UTIL:Lorg/bouncycastle/x509/PKIXCRLUtil;
        //  1263: aload           5
        //  1265: aload_1        
        //  1266: invokevirtual   org/bouncycastle/x509/PKIXCRLUtil.findCRLs:(Lorg/bouncycastle/x509/X509CRLStoreSelector;Ljava/security/cert/PKIXParameters;)Ljava/util/Set;
        //  1269: invokeinterface java/util/Set.iterator:()Ljava/util/Iterator;
        //  1274: astore_1       
        //  1275: aload_1        
        //  1276: invokeinterface java/util/Iterator.hasNext:()Z
        //  1281: ifeq            1351
        //  1284: aload_1        
        //  1285: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //  1290: checkcast       Ljava/security/cert/X509CRL;
        //  1293: astore          4
        //  1295: aload           4
        //  1297: getstatic       org/bouncycastle/x509/PKIXCertPathReviewer.ISSUING_DISTRIBUTION_POINT:Ljava/lang/String;
        //  1300: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getExtensionValue:(Ljava/security/cert/X509Extension;Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1Primitive;
        //  1303: astore          4
        //  1305: aload_3        
        //  1306: ifnonnull       1317
        //  1309: aload           4
        //  1311: ifnonnull       1275
        //  1314: goto            1354
        //  1317: aload_3        
        //  1318: aload           4
        //  1320: invokevirtual   org/bouncycastle/asn1/ASN1Primitive.equals:(Lorg/bouncycastle/asn1/ASN1Primitive;)Z
        //  1323: ifeq            1275
        //  1326: goto            1354
        //  1329: astore_1       
        //  1330: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1333: dup            
        //  1334: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1337: dup            
        //  1338: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1340: ldc_w           "CertPathReviewer.distrPtExtError"
        //  1343: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1346: aload_1        
        //  1347: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/lang/Throwable;)V
        //  1350: athrow         
        //  1351: iconst_0       
        //  1352: istore          7
        //  1354: iload           7
        //  1356: ifeq            1362
        //  1359: goto            1448
        //  1362: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1365: dup            
        //  1366: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1369: dup            
        //  1370: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1372: ldc_w           "CertPathReviewer.noBaseCRL"
        //  1375: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1378: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //  1381: athrow         
        //  1382: astore_1       
        //  1383: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1386: dup            
        //  1387: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1390: dup            
        //  1391: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1393: ldc_w           "CertPathReviewer.crlExtractionError"
        //  1396: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1399: aload_1        
        //  1400: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/lang/Throwable;)V
        //  1403: athrow         
        //  1404: astore_1       
        //  1405: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1408: dup            
        //  1409: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1412: dup            
        //  1413: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1415: ldc_w           "CertPathReviewer.crlNbrExtError"
        //  1418: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1421: aload_1        
        //  1422: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/lang/Throwable;)V
        //  1425: athrow         
        //  1426: astore_1       
        //  1427: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1430: dup            
        //  1431: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1434: dup            
        //  1435: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1437: ldc_w           "CertPathReviewer.crlIssuerException"
        //  1440: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1443: aload_1        
        //  1444: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/lang/Throwable;)V
        //  1447: athrow         
        //  1448: aload_3        
        //  1449: ifnull          1684
        //  1452: aload_3        
        //  1453: invokestatic    org/bouncycastle/asn1/x509/IssuingDistributionPoint.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/IssuingDistributionPoint;
        //  1456: astore_1       
        //  1457: aload_2        
        //  1458: getstatic       org/bouncycastle/x509/PKIXCertPathReviewer.BASIC_CONSTRAINTS:Ljava/lang/String;
        //  1461: invokestatic    org/bouncycastle/x509/PKIXCertPathReviewer.getExtensionValue:(Ljava/security/cert/X509Extension;Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1Primitive;
        //  1464: invokestatic    org/bouncycastle/asn1/x509/BasicConstraints.getInstance:(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x509/BasicConstraints;
        //  1467: astore_2       
        //  1468: aload_1        
        //  1469: invokevirtual   org/bouncycastle/asn1/x509/IssuingDistributionPoint.onlyContainsUserCerts:()Z
        //  1472: ifeq            1509
        //  1475: aload_2        
        //  1476: ifnull          1509
        //  1479: aload_2        
        //  1480: invokevirtual   org/bouncycastle/asn1/x509/BasicConstraints.isCA:()Z
        //  1483: ifne            1489
        //  1486: goto            1509
        //  1489: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1492: dup            
        //  1493: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1496: dup            
        //  1497: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1499: ldc_w           "CertPathReviewer.crlOnlyUserCert"
        //  1502: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1505: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //  1508: athrow         
        //  1509: aload_1        
        //  1510: invokevirtual   org/bouncycastle/asn1/x509/IssuingDistributionPoint.onlyContainsCACerts:()Z
        //  1513: ifeq            1550
        //  1516: aload_2        
        //  1517: ifnull          1530
        //  1520: aload_2        
        //  1521: invokevirtual   org/bouncycastle/asn1/x509/BasicConstraints.isCA:()Z
        //  1524: ifeq            1530
        //  1527: goto            1550
        //  1530: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1533: dup            
        //  1534: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1537: dup            
        //  1538: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1540: ldc_w           "CertPathReviewer.crlOnlyCaCert"
        //  1543: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1546: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //  1549: athrow         
        //  1550: aload_1        
        //  1551: invokevirtual   org/bouncycastle/asn1/x509/IssuingDistributionPoint.onlyContainsAttributeCerts:()Z
        //  1554: ifne            1560
        //  1557: goto            1684
        //  1560: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1563: dup            
        //  1564: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1567: dup            
        //  1568: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1570: ldc_w           "CertPathReviewer.crlOnlyAttrCert"
        //  1573: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1576: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //  1579: athrow         
        //  1580: astore_1       
        //  1581: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1584: dup            
        //  1585: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1588: dup            
        //  1589: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1591: ldc_w           "CertPathReviewer.crlBCExtError"
        //  1594: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1597: aload_1        
        //  1598: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/lang/Throwable;)V
        //  1601: athrow         
        //  1602: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1605: dup            
        //  1606: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1609: dup            
        //  1610: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1612: ldc_w           "CertPathReviewer.deltaCrlExtError"
        //  1615: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1618: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //  1621: athrow         
        //  1622: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1625: dup            
        //  1626: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1629: dup            
        //  1630: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1632: ldc_w           "CertPathReviewer.distrPtExtError"
        //  1635: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1638: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //  1641: athrow         
        //  1642: astore_1       
        //  1643: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1646: dup            
        //  1647: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1650: dup            
        //  1651: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1653: ldc_w           "CertPathReviewer.crlVerifyFailed"
        //  1656: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1659: aload_1        
        //  1660: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/lang/Throwable;)V
        //  1663: athrow         
        //  1664: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1667: dup            
        //  1668: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1671: dup            
        //  1672: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1674: ldc_w           "CertPathReviewer.crlNoIssuerPublicKey"
        //  1677: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1680: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //  1683: athrow         
        //  1684: iload           8
        //  1686: ifeq            1690
        //  1689: return         
        //  1690: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1693: dup            
        //  1694: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1697: dup            
        //  1698: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1700: ldc_w           "CertPathReviewer.noValidCrlFound"
        //  1703: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1706: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;)V
        //  1709: athrow         
        //  1710: astore_1       
        //  1711: new             Lorg/bouncycastle/x509/CertPathReviewerException;
        //  1714: dup            
        //  1715: new             Lorg/bouncycastle/i18n/ErrorBundle;
        //  1718: dup            
        //  1719: ldc             "org.bouncycastle.x509.CertPathReviewerMessages"
        //  1721: ldc_w           "CertPathReviewer.crlIssuerException"
        //  1724: invokespecial   org/bouncycastle/i18n/ErrorBundle.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //  1727: aload_1        
        //  1728: invokespecial   org/bouncycastle/x509/CertPathReviewerException.<init>:(Lorg/bouncycastle/i18n/ErrorBundle;Ljava/lang/Throwable;)V
        //  1731: athrow         
        //  1732: astore_1       
        //  1733: goto            1622
        //  1736: astore_1       
        //  1737: goto            1602
        //    Exceptions:
        //  throws org.bouncycastle.x509.CertPathReviewerException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                              
        //  -----  -----  -----  -----  --------------------------------------------------
        //  9      21     1710   1732   Ljava/io/IOException;
        //  27     87     193    262    Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  87     118    193    262    Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  121    187    193    262    Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  455    475    774    776    Lorg/bouncycastle/x509/CertPathReviewerException;
        //  480    494    774    776    Lorg/bouncycastle/x509/CertPathReviewerException;
        //  499    568    571    576    Lorg/bouncycastle/x509/CertPathReviewerException;
        //  580    606    763    768    Lorg/bouncycastle/x509/CertPathReviewerException;
        //  609    673    676    681    Lorg/bouncycastle/x509/CertPathReviewerException;
        //  681    745    755    763    Lorg/bouncycastle/x509/CertPathReviewerException;
        //  863    873    1642   1664   Ljava/lang/Exception;
        //  897    913    932    954    Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1175   1184   1732   1642   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1184   1194   1736   1622   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1208   1221   1426   1448   Ljava/io/IOException;
        //  1234   1260   1404   1426   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1260   1275   1382   1404   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1295   1305   1329   1351   Lorg/bouncycastle/jce/provider/AnnotatedException;
        //  1457   1468   1580   1602   Lorg/bouncycastle/jce/provider/AnnotatedException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 789 out of bounds for length 789
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:373)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:426)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    protected void checkRevocation(final PKIXParameters pkixParameters, final X509Certificate x509Certificate, final Date date, final X509Certificate x509Certificate2, final PublicKey publicKey, final Vector vector, final Vector vector2, final int n) throws CertPathReviewerException {
        this.checkCRLs(pkixParameters, x509Certificate, date, x509Certificate2, publicKey, vector, n);
    }
    
    protected void doChecks() {
        if (this.initialized) {
            if (this.notifications == null) {
                final int n = this.n;
                this.notifications = new List[n + 1];
                this.errors = new List[n + 1];
                int n2 = 0;
                while (true) {
                    final List[] notifications = this.notifications;
                    if (n2 >= notifications.length) {
                        break;
                    }
                    notifications[n2] = new ArrayList();
                    this.errors[n2] = new ArrayList();
                    ++n2;
                }
                this.checkSignatures();
                this.checkNameConstraints();
                this.checkPathLength();
                this.checkPolicy();
                this.checkCriticalExtensions();
            }
            return;
        }
        throw new IllegalStateException("Object not initialized. Call init() first.");
    }
    
    protected Vector getCRLDistUrls(final CRLDistPoint crlDistPoint) {
        final Vector<String> vector = new Vector<String>();
        if (crlDistPoint != null) {
            final DistributionPoint[] distributionPoints = crlDistPoint.getDistributionPoints();
            for (int i = 0; i < distributionPoints.length; ++i) {
                final DistributionPointName distributionPoint = distributionPoints[i].getDistributionPoint();
                if (distributionPoint.getType() == 0) {
                    final GeneralName[] names = GeneralNames.getInstance(distributionPoint.getName()).getNames();
                    for (int j = 0; j < names.length; ++j) {
                        if (names[j].getTagNo() == 6) {
                            vector.add(((DERIA5String)names[j].getName()).getString());
                        }
                    }
                }
            }
        }
        return vector;
    }
    
    public CertPath getCertPath() {
        return this.certPath;
    }
    
    public int getCertPathSize() {
        return this.n;
    }
    
    public List getErrors(final int n) {
        this.doChecks();
        return this.errors[n + 1];
    }
    
    public List[] getErrors() {
        this.doChecks();
        return this.errors;
    }
    
    public List getNotifications(final int n) {
        this.doChecks();
        return this.notifications[n + 1];
    }
    
    public List[] getNotifications() {
        this.doChecks();
        return this.notifications;
    }
    
    protected Vector getOCSPUrls(final AuthorityInformationAccess authorityInformationAccess) {
        final Vector<String> vector = new Vector<String>();
        if (authorityInformationAccess != null) {
            final AccessDescription[] accessDescriptions = authorityInformationAccess.getAccessDescriptions();
            for (int i = 0; i < accessDescriptions.length; ++i) {
                if (accessDescriptions[i].getAccessMethod().equals(AccessDescription.id_ad_ocsp)) {
                    final GeneralName accessLocation = accessDescriptions[i].getAccessLocation();
                    if (accessLocation.getTagNo() == 6) {
                        vector.add(((DERIA5String)accessLocation.getName()).getString());
                    }
                }
            }
        }
        return vector;
    }
    
    public PolicyNode getPolicyTree() {
        this.doChecks();
        return this.policyTree;
    }
    
    public PublicKey getSubjectPublicKey() {
        this.doChecks();
        return this.subjectPublicKey;
    }
    
    public TrustAnchor getTrustAnchor() {
        this.doChecks();
        return this.trustAnchor;
    }
    
    protected Collection getTrustAnchors(final X509Certificate x509Certificate, final Set set) throws CertPathReviewerException {
        final ArrayList<TrustAnchor> list = new ArrayList<TrustAnchor>();
        final Iterator<TrustAnchor> iterator = set.iterator();
        final X509CertSelector x509CertSelector = new X509CertSelector();
        try {
            x509CertSelector.setSubject(CertPathValidatorUtilities.getEncodedIssuerPrincipal(x509Certificate).getEncoded());
            final byte[] extensionValue = x509Certificate.getExtensionValue(Extension.authorityKeyIdentifier.getId());
            if (extensionValue != null) {
                final AuthorityKeyIdentifier instance = AuthorityKeyIdentifier.getInstance(ASN1Primitive.fromByteArray(((ASN1OctetString)ASN1Primitive.fromByteArray(extensionValue)).getOctets()));
                x509CertSelector.setSerialNumber(instance.getAuthorityCertSerialNumber());
                final byte[] keyIdentifier = instance.getKeyIdentifier();
                if (keyIdentifier != null) {
                    x509CertSelector.setSubjectKeyIdentifier(new DEROctetString(keyIdentifier).getEncoded());
                }
            }
            while (iterator.hasNext()) {
                final TrustAnchor trustAnchor = iterator.next();
                if (trustAnchor.getTrustedCert() != null) {
                    if (!x509CertSelector.match(trustAnchor.getTrustedCert())) {
                        continue;
                    }
                }
                else if (trustAnchor.getCAName() == null || trustAnchor.getCAPublicKey() == null || !CertPathValidatorUtilities.getEncodedIssuerPrincipal(x509Certificate).equals(new X500Principal(trustAnchor.getCAName()))) {
                    continue;
                }
                list.add(trustAnchor);
            }
            return list;
        }
        catch (IOException ex) {
            throw new CertPathReviewerException(new ErrorBundle("org.bouncycastle.x509.CertPathReviewerMessages", "CertPathReviewer.trustAnchorIssuerError"));
        }
    }
    
    public void init(final CertPath certPath, final PKIXParameters pkixParameters) throws CertPathReviewerException {
        if (this.initialized) {
            throw new IllegalStateException("object is already initialized!");
        }
        this.initialized = true;
        if (certPath == null) {
            throw new NullPointerException("certPath was null");
        }
        this.certPath = certPath;
        this.certs = certPath.getCertificates();
        this.n = this.certs.size();
        if (!this.certs.isEmpty()) {
            this.pkixParams = (PKIXParameters)pkixParameters.clone();
            this.validDate = CertPathValidatorUtilities.getValidDate(this.pkixParams);
            this.notifications = null;
            this.errors = null;
            this.trustAnchor = null;
            this.subjectPublicKey = null;
            this.policyTree = null;
            return;
        }
        throw new CertPathReviewerException(new ErrorBundle("org.bouncycastle.x509.CertPathReviewerMessages", "CertPathReviewer.emptyCertPath"));
    }
    
    public boolean isValidCertPath() {
        this.doChecks();
        int n = 0;
        while (true) {
            final List[] errors = this.errors;
            if (n >= errors.length) {
                return true;
            }
            if (!errors[n].isEmpty()) {
                return false;
            }
            ++n;
        }
    }
}
