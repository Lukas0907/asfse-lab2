// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.x509;

import java.security.SignatureException;
import java.security.NoSuchProviderException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.PublicKey;
import java.math.BigInteger;
import java.io.IOException;
import java.util.Date;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.X509Extension;

public interface X509AttributeCertificate extends X509Extension
{
    void checkValidity() throws CertificateExpiredException, CertificateNotYetValidException;
    
    void checkValidity(final Date p0) throws CertificateExpiredException, CertificateNotYetValidException;
    
    X509Attribute[] getAttributes();
    
    X509Attribute[] getAttributes(final String p0);
    
    byte[] getEncoded() throws IOException;
    
    AttributeCertificateHolder getHolder();
    
    AttributeCertificateIssuer getIssuer();
    
    boolean[] getIssuerUniqueID();
    
    Date getNotAfter();
    
    Date getNotBefore();
    
    BigInteger getSerialNumber();
    
    byte[] getSignature();
    
    int getVersion();
    
    void verify(final PublicKey p0, final String p1) throws CertificateException, NoSuchAlgorithmException, InvalidKeyException, NoSuchProviderException, SignatureException;
}
