// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.x509.extension;

import org.bouncycastle.asn1.x509.Extension;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.List;
import java.security.cert.CertificateParsingException;
import org.bouncycastle.asn1.ASN1String;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.util.Integers;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.ASN1Sequence;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Collection;
import java.io.IOException;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Primitive;

public class X509ExtensionUtil
{
    public static ASN1Primitive fromExtensionValue(final byte[] array) throws IOException {
        return ASN1Primitive.fromByteArray(((ASN1OctetString)ASN1Primitive.fromByteArray(array)).getOctets());
    }
    
    private static Collection getAlternativeNames(final byte[] array) throws CertificateParsingException {
        if (array == null) {
            return Collections.EMPTY_LIST;
        }
        while (true) {
            while (true) {
                Label_0279: {
                    try {
                        final ArrayList<List<byte[]>> c = (ArrayList<List<byte[]>>)new ArrayList<ArrayList<byte[]>>();
                        final Enumeration objects = ASN1Sequence.getInstance(fromExtensionValue(array)).getObjects();
                        while (objects.hasMoreElements()) {
                            final GeneralName instance = GeneralName.getInstance(objects.nextElement());
                            final ArrayList<byte[]> list = new ArrayList<byte[]>();
                            list.add(Integers.valueOf(instance.getTagNo()));
                            Label_0212: {
                                Object o = null;
                                switch (instance.getTagNo()) {
                                    case 8: {
                                        o = ASN1ObjectIdentifier.getInstance(instance.getName()).getId();
                                        break;
                                    }
                                    case 7: {
                                        list.add(ASN1OctetString.getInstance(instance.getName()).getOctets());
                                        break Label_0212;
                                    }
                                    case 4: {
                                        o = X500Name.getInstance(instance.getName()).toString();
                                        break;
                                    }
                                    case 1:
                                    case 2:
                                    case 6: {
                                        o = ((ASN1String)instance.getName()).getString();
                                        break;
                                    }
                                    case 0:
                                    case 3:
                                    case 5: {
                                        o = instance.getName().toASN1Primitive();
                                        break;
                                    }
                                    default: {
                                        break Label_0279;
                                    }
                                }
                                list.add((byte[])o);
                            }
                            c.add(list);
                        }
                        return Collections.unmodifiableCollection((Collection<?>)c);
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Bad tag number: ");
                        GeneralName instance = null;
                        sb.append(instance.getTagNo());
                        throw new IOException(sb.toString());
                    }
                    catch (Exception ex) {
                        throw new CertificateParsingException(ex.getMessage());
                    }
                }
                continue;
            }
        }
    }
    
    public static Collection getIssuerAlternativeNames(final X509Certificate x509Certificate) throws CertificateParsingException {
        return getAlternativeNames(x509Certificate.getExtensionValue(Extension.issuerAlternativeName.getId()));
    }
    
    public static Collection getSubjectAlternativeNames(final X509Certificate x509Certificate) throws CertificateParsingException {
        return getAlternativeNames(x509Certificate.getExtensionValue(Extension.subjectAlternativeName.getId()));
    }
}
