// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.x509.extension;

import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.X509Name;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.jce.PrincipalUtil;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import java.io.IOException;
import org.bouncycastle.asn1.x509.X509Extension;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x509.Extension;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.security.InvalidKeyException;
import java.security.PublicKey;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;

public class AuthorityKeyIdentifierStructure extends AuthorityKeyIdentifier
{
    public AuthorityKeyIdentifierStructure(final PublicKey publicKey) throws InvalidKeyException {
        super(fromKey(publicKey));
    }
    
    public AuthorityKeyIdentifierStructure(final X509Certificate x509Certificate) throws CertificateParsingException {
        super(fromCertificate(x509Certificate));
    }
    
    public AuthorityKeyIdentifierStructure(final Extension extension) {
        super((ASN1Sequence)extension.getParsedValue());
    }
    
    public AuthorityKeyIdentifierStructure(final X509Extension x509Extension) {
        super((ASN1Sequence)x509Extension.getParsedValue());
    }
    
    public AuthorityKeyIdentifierStructure(final byte[] array) throws IOException {
        super((ASN1Sequence)X509ExtensionUtil.fromExtensionValue(array));
    }
    
    private static ASN1Sequence fromCertificate(final X509Certificate x509Certificate) throws CertificateParsingException {
        try {
            if (x509Certificate.getVersion() != 3) {
                return (ASN1Sequence)new AuthorityKeyIdentifier(SubjectPublicKeyInfo.getInstance(x509Certificate.getPublicKey().getEncoded()), new GeneralNames(new GeneralName(PrincipalUtil.getIssuerX509Principal(x509Certificate))), x509Certificate.getSerialNumber()).toASN1Primitive();
            }
            final GeneralName generalName = new GeneralName(PrincipalUtil.getIssuerX509Principal(x509Certificate));
            final byte[] extensionValue = x509Certificate.getExtensionValue(Extension.subjectKeyIdentifier.getId());
            if (extensionValue != null) {
                return (ASN1Sequence)new AuthorityKeyIdentifier(((ASN1OctetString)X509ExtensionUtil.fromExtensionValue(extensionValue)).getOctets(), new GeneralNames(generalName), x509Certificate.getSerialNumber()).toASN1Primitive();
            }
            return (ASN1Sequence)new AuthorityKeyIdentifier(SubjectPublicKeyInfo.getInstance(x509Certificate.getPublicKey().getEncoded()), new GeneralNames(generalName), x509Certificate.getSerialNumber()).toASN1Primitive();
        }
        catch (Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Exception extracting certificate details: ");
            sb.append(ex.toString());
            throw new CertificateParsingException(sb.toString());
        }
    }
    
    private static ASN1Sequence fromKey(final PublicKey publicKey) throws InvalidKeyException {
        try {
            return (ASN1Sequence)new AuthorityKeyIdentifier(SubjectPublicKeyInfo.getInstance(publicKey.getEncoded())).toASN1Primitive();
        }
        catch (Exception obj) {
            final StringBuilder sb = new StringBuilder();
            sb.append("can't process key: ");
            sb.append(obj);
            throw new InvalidKeyException(sb.toString());
        }
    }
}
