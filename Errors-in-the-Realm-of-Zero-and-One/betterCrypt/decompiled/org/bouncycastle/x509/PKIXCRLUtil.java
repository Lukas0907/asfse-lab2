// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.x509;

import java.security.cert.X509Certificate;
import java.security.cert.X509CRL;
import java.util.Date;
import java.util.Set;
import java.security.cert.PKIXParameters;
import java.util.Iterator;
import org.bouncycastle.util.StoreException;
import org.bouncycastle.util.Selector;
import java.security.cert.CertStoreException;
import org.bouncycastle.jce.provider.AnnotatedException;
import java.security.cert.CRLSelector;
import java.security.cert.CertStore;
import java.util.HashSet;
import java.util.Collection;
import java.util.List;

class PKIXCRLUtil
{
    private final Collection findCRLs(final X509CRLStoreSelector selector, final List list) throws AnnotatedException {
        final HashSet set = new HashSet();
        final Iterator<X509Store> iterator = list.iterator();
        Object o = null;
        boolean b = false;
        while (iterator.hasNext()) {
            final X509Store next = iterator.next();
            Label_0069: {
                if (!(next instanceof X509Store)) {
                    final CertStore certStore = (CertStore)next;
                    try {
                        set.addAll(certStore.getCRLs(selector));
                        break Label_0069;
                    }
                    catch (CertStoreException ex) {
                        o = new AnnotatedException("Exception searching in X.509 CRL store.", ex);
                        continue;
                    }
                    break;
                }
                final X509Store x509Store = next;
                try {
                    set.addAll(x509Store.getMatches(selector));
                    b = true;
                }
                catch (StoreException ex2) {
                    o = new AnnotatedException("Exception searching in X.509 CRL store.", ex2);
                }
            }
        }
        if (b) {
            return set;
        }
        if (o == null) {
            return set;
        }
        throw o;
    }
    
    public Set findCRLs(final X509CRLStoreSelector x509CRLStoreSelector, final PKIXParameters pkixParameters) throws AnnotatedException {
        final HashSet set = new HashSet();
        try {
            set.addAll(this.findCRLs(x509CRLStoreSelector, pkixParameters.getCertStores()));
            return set;
        }
        catch (AnnotatedException ex) {
            throw new AnnotatedException("Exception obtaining complete CRLs.", ex);
        }
    }
    
    public Set findCRLs(final X509CRLStoreSelector x509CRLStoreSelector, final ExtendedPKIXParameters extendedPKIXParameters, Date date) throws AnnotatedException {
        final HashSet<X509CRL> set = new HashSet<X509CRL>();
        try {
            set.addAll((Collection<?>)this.findCRLs(x509CRLStoreSelector, extendedPKIXParameters.getAdditionalStores()));
            set.addAll((Collection<?>)this.findCRLs(x509CRLStoreSelector, extendedPKIXParameters.getStores()));
            set.addAll((Collection<?>)this.findCRLs(x509CRLStoreSelector, extendedPKIXParameters.getCertStores()));
            final HashSet<X509CRL> set2 = new HashSet<X509CRL>();
            if (extendedPKIXParameters.getDate() != null) {
                date = extendedPKIXParameters.getDate();
            }
            for (final X509CRL x509CRL : set) {
                if (x509CRL.getNextUpdate().after(date)) {
                    final X509Certificate certificateChecking = x509CRLStoreSelector.getCertificateChecking();
                    if (certificateChecking != null && !x509CRL.getThisUpdate().before(certificateChecking.getNotAfter())) {
                        continue;
                    }
                    set2.add(x509CRL);
                }
            }
            return set2;
        }
        catch (AnnotatedException ex) {
            throw new AnnotatedException("Exception obtaining complete CRLs.", ex);
        }
    }
}
