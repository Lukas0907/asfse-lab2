// 
// Decompiled by Procyon v0.5.36
// 

package org.bouncycastle.x509.util;

import java.io.IOException;
import java.security.cert.CertificateParsingException;
import org.bouncycastle.asn1.x509.CertificatePair;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.jce.provider.X509CertPairParser;
import org.bouncycastle.x509.X509CertificatePair;
import org.bouncycastle.x509.X509CertPairStoreSelector;
import org.bouncycastle.jce.provider.X509CertParser;
import java.security.cert.X509Certificate;
import org.bouncycastle.jce.provider.X509CRLParser;
import java.security.cert.X509CRL;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import org.bouncycastle.jce.provider.X509AttrCertParser;
import org.bouncycastle.x509.X509AttributeCertificate;
import java.util.Set;
import javax.naming.NamingException;
import java.util.Hashtable;
import javax.naming.directory.InitialDirContext;
import java.util.Properties;
import javax.naming.directory.DirContext;
import org.bouncycastle.x509.X509CertStoreSelector;
import org.bouncycastle.x509.X509CRLStoreSelector;
import org.bouncycastle.util.StoreException;
import java.security.Principal;
import org.bouncycastle.x509.AttributeCertificateHolder;
import java.util.Collection;
import javax.security.auth.x500.X500Principal;
import java.util.HashSet;
import org.bouncycastle.x509.X509AttributeCertStoreSelector;
import java.util.Iterator;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;
import java.util.HashMap;
import org.bouncycastle.jce.X509LDAPCertStoreParameters;
import java.util.Map;

public class LDAPStoreHelper
{
    private static String LDAP_PROVIDER = "com.sun.jndi.ldap.LdapCtxFactory";
    private static String REFERRALS_IGNORE = "ignore";
    private static final String SEARCH_SECURITY_LEVEL = "none";
    private static final String URL_CONTEXT_PREFIX = "com.sun.jndi.url";
    private static int cacheSize = 32;
    private static long lifeTime = 60000L;
    private Map cacheMap;
    private X509LDAPCertStoreParameters params;
    
    public LDAPStoreHelper(final X509LDAPCertStoreParameters params) {
        this.cacheMap = new HashMap(LDAPStoreHelper.cacheSize);
        this.params = params;
    }
    
    private void addToCache(final String s, final List list) {
        synchronized (this) {
            final Date date = new Date(System.currentTimeMillis());
            final ArrayList<Date> list2 = new ArrayList<Date>();
            list2.add(date);
            list2.add((Date)list);
            Map map;
            if (this.cacheMap.containsKey(s)) {
                map = this.cacheMap;
            }
            else {
                if (this.cacheMap.size() >= LDAPStoreHelper.cacheSize) {
                    final Iterator<Map.Entry<K, List>> iterator = this.cacheMap.entrySet().iterator();
                    long time = date.getTime();
                    Object key = null;
                    while (iterator.hasNext()) {
                        final Map.Entry<K, List> entry = iterator.next();
                        final long time2 = entry.getValue().get(0).getTime();
                        if (time2 < time) {
                            key = entry.getKey();
                            time = time2;
                        }
                    }
                    this.cacheMap.remove(key);
                }
                map = this.cacheMap;
            }
            map.put(s, list2);
        }
    }
    
    private List attrCertSubjectSerialSearch(final X509AttributeCertStoreSelector x509AttributeCertStoreSelector, final String[] array, final String[] array2, final String[] array3) throws StoreException {
        final ArrayList list = new ArrayList();
        final HashSet<String> set = new HashSet<String>();
        final AttributeCertificateHolder holder = x509AttributeCertStoreSelector.getHolder();
        final String s = null;
        Principal[] array4 = null;
        Label_0085: {
            if (holder != null) {
                if (x509AttributeCertStoreSelector.getHolder().getSerialNumber() != null) {
                    set.add(x509AttributeCertStoreSelector.getHolder().getSerialNumber().toString());
                }
                if (x509AttributeCertStoreSelector.getHolder().getEntityNames() != null) {
                    array4 = x509AttributeCertStoreSelector.getHolder().getEntityNames();
                    break Label_0085;
                }
            }
            array4 = null;
        }
        Principal[] array5 = array4;
        if (x509AttributeCertStoreSelector.getAttributeCert() != null) {
            if (x509AttributeCertStoreSelector.getAttributeCert().getHolder().getEntityNames() != null) {
                array4 = x509AttributeCertStoreSelector.getAttributeCert().getHolder().getEntityNames();
            }
            set.add(x509AttributeCertStoreSelector.getAttributeCert().getSerialNumber().toString());
            array5 = array4;
        }
        int i = 0;
        String s2 = s;
        if (array5 != null) {
            if (array5[0] instanceof X500Principal) {
                s2 = ((X500Principal)array5[0]).getName("RFC1779");
            }
            else {
                s2 = array5[0].getName();
            }
        }
        if (x509AttributeCertStoreSelector.getSerialNumber() != null) {
            set.add(x509AttributeCertStoreSelector.getSerialNumber().toString());
        }
        if (s2 != null) {
            while (i < array3.length) {
                final String dn = this.parseDN(s2, array3[i]);
                final StringBuilder sb = new StringBuilder();
                sb.append("*");
                sb.append(dn);
                sb.append("*");
                list.addAll(this.search(array2, sb.toString(), array));
                ++i;
            }
        }
        if (set.size() > 0 && this.params.getSearchForSerialNumberIn() != null) {
            final Iterator<Object> iterator = set.iterator();
            while (iterator.hasNext()) {
                list.addAll(this.search(this.splitString(this.params.getSearchForSerialNumberIn()), iterator.next(), array));
            }
        }
        if (set.size() == 0 && s2 == null) {
            list.addAll(this.search(array2, "*", array));
        }
        return list;
    }
    
    private List cRLIssuerSearch(final X509CRLStoreSelector x509CRLStoreSelector, final String[] array, final String[] array2, final String[] array3) throws StoreException {
        final ArrayList list = new ArrayList();
        final HashSet<Principal> set = new HashSet<Principal>();
        if (x509CRLStoreSelector.getIssuers() != null) {
            set.addAll((Collection<?>)x509CRLStoreSelector.getIssuers());
        }
        if (x509CRLStoreSelector.getCertificateChecking() != null) {
            set.add(this.getCertificateIssuer(x509CRLStoreSelector.getCertificateChecking()));
        }
        if (x509CRLStoreSelector.getAttrCertificateChecking() != null) {
            final Principal[] principals = x509CRLStoreSelector.getAttrCertificateChecking().getIssuer().getPrincipals();
            for (int i = 0; i < principals.length; ++i) {
                if (principals[i] instanceof X500Principal) {
                    set.add(principals[i]);
                }
            }
        }
        final Iterator<X500Principal> iterator = set.iterator();
        String s = null;
        while (iterator.hasNext()) {
            final String name = iterator.next().getName("RFC1779");
            int n = 0;
            while (true) {
                s = name;
                if (n >= array3.length) {
                    break;
                }
                final String dn = this.parseDN(name, array3[n]);
                final StringBuilder sb = new StringBuilder();
                sb.append("*");
                sb.append(dn);
                sb.append("*");
                list.addAll(this.search(array2, sb.toString(), array));
                ++n;
            }
        }
        if (s == null) {
            list.addAll(this.search(array2, "*", array));
        }
        return list;
    }
    
    private List certSubjectSerialSearch(final X509CertStoreSelector x509CertStoreSelector, final String[] array, final String[] array2, final String[] array3) throws StoreException {
        final ArrayList list = new ArrayList();
        String s = this.getSubjectAsString(x509CertStoreSelector);
        String s2;
        if (x509CertStoreSelector.getSerialNumber() != null) {
            s2 = x509CertStoreSelector.getSerialNumber().toString();
        }
        else {
            s2 = null;
        }
        if (x509CertStoreSelector.getCertificate() != null) {
            s = x509CertStoreSelector.getCertificate().getSubjectX500Principal().getName("RFC1779");
            s2 = x509CertStoreSelector.getCertificate().getSerialNumber().toString();
        }
        if (s != null) {
            for (int i = 0; i < array3.length; ++i) {
                final String dn = this.parseDN(s, array3[i]);
                final StringBuilder sb = new StringBuilder();
                sb.append("*");
                sb.append(dn);
                sb.append("*");
                list.addAll(this.search(array2, sb.toString(), array));
            }
        }
        if (s2 != null && this.params.getSearchForSerialNumberIn() != null) {
            list.addAll(this.search(this.splitString(this.params.getSearchForSerialNumberIn()), s2, array));
        }
        if (s2 == null && s == null) {
            list.addAll(this.search(array2, "*", array));
        }
        return list;
    }
    
    private DirContext connectLDAP() throws NamingException {
        final Properties environment = new Properties();
        environment.setProperty("java.naming.factory.initial", LDAPStoreHelper.LDAP_PROVIDER);
        environment.setProperty("java.naming.batchsize", "0");
        environment.setProperty("java.naming.provider.url", this.params.getLdapURL());
        environment.setProperty("java.naming.factory.url.pkgs", "com.sun.jndi.url");
        environment.setProperty("java.naming.referral", LDAPStoreHelper.REFERRALS_IGNORE);
        environment.setProperty("java.naming.security.authentication", "none");
        return new InitialDirContext(environment);
    }
    
    private Set createAttributeCertificates(List iterator, final X509AttributeCertStoreSelector x509AttributeCertStoreSelector) throws StoreException {
        final HashSet<X509AttributeCertificate> set = new HashSet<X509AttributeCertificate>();
        iterator = ((List)iterator).iterator();
        final X509AttrCertParser x509AttrCertParser = new X509AttrCertParser();
        while (true) {
            if (!iterator.hasNext()) {
                return set;
            }
            try {
                x509AttrCertParser.engineInit(new ByteArrayInputStream(iterator.next()));
                final X509AttributeCertificate x509AttributeCertificate = (X509AttributeCertificate)x509AttrCertParser.engineRead();
                if (x509AttributeCertStoreSelector.match(x509AttributeCertificate)) {
                    set.add(x509AttributeCertificate);
                    continue;
                }
                continue;
            }
            catch (StreamParsingException ex) {}
        }
    }
    
    private Set createCRLs(List iterator, final X509CRLStoreSelector x509CRLStoreSelector) throws StoreException {
        final HashSet<X509CRL> set = new HashSet<X509CRL>();
        final X509CRLParser x509CRLParser = new X509CRLParser();
        iterator = ((List)iterator).iterator();
        while (true) {
            if (!iterator.hasNext()) {
                return set;
            }
            try {
                x509CRLParser.engineInit(new ByteArrayInputStream(iterator.next()));
                final X509CRL x509CRL = (X509CRL)x509CRLParser.engineRead();
                if (x509CRLStoreSelector.match((Object)x509CRL)) {
                    set.add(x509CRL);
                    continue;
                }
                continue;
            }
            catch (StreamParsingException ex) {}
        }
    }
    
    private Set createCerts(List iterator, final X509CertStoreSelector x509CertStoreSelector) throws StoreException {
        final HashSet<X509Certificate> set = new HashSet<X509Certificate>();
        iterator = ((List)iterator).iterator();
        final X509CertParser x509CertParser = new X509CertParser();
        while (true) {
            if (!iterator.hasNext()) {
                return set;
            }
            try {
                x509CertParser.engineInit(new ByteArrayInputStream(iterator.next()));
                final X509Certificate x509Certificate = (X509Certificate)x509CertParser.engineRead();
                if (x509CertStoreSelector.match((Object)x509Certificate)) {
                    set.add(x509Certificate);
                    continue;
                }
                continue;
            }
            catch (Exception ex) {}
        }
    }
    
    private Set createCrossCertificatePairs(final List list, final X509CertPairStoreSelector x509CertPairStoreSelector) throws StoreException {
        final HashSet<X509CertificatePair> set = new HashSet<X509CertificatePair>();
        int n = 0;
    Label_0202_Outer:
        while (true) {
            if (n >= list.size()) {
                return set;
            }
            int n2 = n;
            while (true) {
                try {
                    while (true) {
                        while (true) {
                            try {
                                final X509CertPairParser x509CertPairParser = new X509CertPairParser();
                                n2 = n;
                                x509CertPairParser.engineInit(new ByteArrayInputStream(list.get(n)));
                                n2 = n;
                                X509CertificatePair x509CertificatePair = (X509CertificatePair)x509CertPairParser.engineRead();
                                while (true) {
                                    int n3 = n;
                                    n2 = n;
                                    if (x509CertPairStoreSelector.match(x509CertificatePair)) {
                                        n2 = n;
                                        set.add(x509CertificatePair);
                                        n3 = n;
                                    }
                                    n = n3 + 1;
                                    break;
                                    n2 = n;
                                    final byte[] array = list.get(n);
                                    final int n4 = n + 1;
                                    n2 = n;
                                    final byte[] array2 = list.get(n4);
                                    n2 = n;
                                    x509CertificatePair = new X509CertificatePair(new CertificatePair(Certificate.getInstance(new ASN1InputStream(array).readObject()), Certificate.getInstance(new ASN1InputStream(array2).readObject())));
                                    n = n4;
                                    continue Label_0202_Outer;
                                }
                            }
                            catch (StreamParsingException ex) {}
                            continue;
                        }
                    }
                }
                catch (CertificateParsingException | IOException ex2) {
                    final int n3 = n2;
                    continue;
                }
                break;
            }
        }
    }
    
    private List crossCertificatePairSubjectSearch(final X509CertPairStoreSelector x509CertPairStoreSelector, final String[] array, final String[] array2, final String[] array3) throws StoreException {
        final ArrayList list = new ArrayList();
        String subjectAsString;
        if (x509CertPairStoreSelector.getForwardSelector() != null) {
            subjectAsString = this.getSubjectAsString(x509CertPairStoreSelector.getForwardSelector());
        }
        else {
            subjectAsString = null;
        }
        String name = subjectAsString;
        if (x509CertPairStoreSelector.getCertPair() != null) {
            name = subjectAsString;
            if (x509CertPairStoreSelector.getCertPair().getForward() != null) {
                name = x509CertPairStoreSelector.getCertPair().getForward().getSubjectX500Principal().getName("RFC1779");
            }
        }
        if (name != null) {
            for (int i = 0; i < array3.length; ++i) {
                final String dn = this.parseDN(name, array3[i]);
                final StringBuilder sb = new StringBuilder();
                sb.append("*");
                sb.append(dn);
                sb.append("*");
                list.addAll(this.search(array2, sb.toString(), array));
            }
        }
        if (name == null) {
            list.addAll(this.search(array2, "*", array));
        }
        return list;
    }
    
    private X500Principal getCertificateIssuer(final X509Certificate x509Certificate) {
        return x509Certificate.getIssuerX500Principal();
    }
    
    private List getFromCache(final String s) {
        final List<Date> list = this.cacheMap.get(s);
        final long currentTimeMillis = System.currentTimeMillis();
        if (list == null) {
            return null;
        }
        if (list.get(0).getTime() < currentTimeMillis - LDAPStoreHelper.lifeTime) {
            return null;
        }
        return (List)list.get(1);
    }
    
    private String getSubjectAsString(final X509CertStoreSelector x509CertStoreSelector) {
        try {
            final byte[] subjectAsBytes = x509CertStoreSelector.getSubjectAsBytes();
            if (subjectAsBytes != null) {
                return new X500Principal(subjectAsBytes).getName("RFC1779");
            }
            return null;
        }
        catch (IOException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("exception processing name: ");
            sb.append(ex.getMessage());
            throw new StoreException(sb.toString(), ex);
        }
    }
    
    private String parseDN(String s, String substring) {
        final String lowerCase = s.toLowerCase();
        final StringBuilder sb = new StringBuilder();
        sb.append(substring.toLowerCase());
        sb.append("=");
        final int index = lowerCase.indexOf(sb.toString());
        if (index == -1) {
            return "";
        }
        s = s.substring(index + substring.length());
        while (true) {
            Label_0082: {
                int endIndex;
                if ((endIndex = s.indexOf(44)) == -1) {
                    break Label_0082;
                }
                while (s.charAt(endIndex - 1) == '\\') {
                    if ((endIndex = s.indexOf(44, endIndex + 1)) == -1) {
                        break Label_0082;
                    }
                }
                s = s.substring(0, endIndex);
                substring = (s = s.substring(s.indexOf(61) + 1));
                if (substring.charAt(0) == ' ') {
                    s = substring.substring(1);
                }
                substring = s;
                if (s.startsWith("\"")) {
                    substring = s.substring(1);
                }
                s = substring;
                if (substring.endsWith("\"")) {
                    s = substring.substring(0, substring.length() - 1);
                }
                return s;
            }
            int endIndex = s.length();
            continue;
        }
    }
    
    private List search(final String[] p0, final String p1, final String[] p2) throws StoreException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: istore          5
        //     3: aconst_null    
        //     4: astore          8
        //     6: aconst_null    
        //     7: astore          7
        //     9: aload_1        
        //    10: ifnonnull       18
        //    13: aconst_null    
        //    14: astore_1       
        //    15: goto            160
        //    18: aload_2        
        //    19: astore          6
        //    21: aload_2        
        //    22: ldc_w           "**"
        //    25: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    28: ifeq            35
        //    31: ldc             "*"
        //    33: astore          6
        //    35: iconst_0       
        //    36: istore          4
        //    38: ldc_w           ""
        //    41: astore_2       
        //    42: iload           4
        //    44: aload_1        
        //    45: arraylength    
        //    46: if_icmpge       125
        //    49: new             Ljava/lang/StringBuilder;
        //    52: dup            
        //    53: invokespecial   java/lang/StringBuilder.<init>:()V
        //    56: astore          9
        //    58: aload           9
        //    60: aload_2        
        //    61: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    64: pop            
        //    65: aload           9
        //    67: ldc_w           "("
        //    70: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    73: pop            
        //    74: aload           9
        //    76: aload_1        
        //    77: iload           4
        //    79: aaload         
        //    80: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    83: pop            
        //    84: aload           9
        //    86: ldc_w           "="
        //    89: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    92: pop            
        //    93: aload           9
        //    95: aload           6
        //    97: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   100: pop            
        //   101: aload           9
        //   103: ldc_w           ")"
        //   106: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   109: pop            
        //   110: aload           9
        //   112: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   115: astore_2       
        //   116: iload           4
        //   118: iconst_1       
        //   119: iadd           
        //   120: istore          4
        //   122: goto            42
        //   125: new             Ljava/lang/StringBuilder;
        //   128: dup            
        //   129: invokespecial   java/lang/StringBuilder.<init>:()V
        //   132: astore_1       
        //   133: aload_1        
        //   134: ldc_w           "(|"
        //   137: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   140: pop            
        //   141: aload_1        
        //   142: aload_2        
        //   143: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   146: pop            
        //   147: aload_1        
        //   148: ldc_w           ")"
        //   151: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   154: pop            
        //   155: aload_1        
        //   156: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   159: astore_1       
        //   160: ldc_w           ""
        //   163: astore_2       
        //   164: iload           5
        //   166: istore          4
        //   168: iload           4
        //   170: aload_3        
        //   171: arraylength    
        //   172: if_icmpge       234
        //   175: new             Ljava/lang/StringBuilder;
        //   178: dup            
        //   179: invokespecial   java/lang/StringBuilder.<init>:()V
        //   182: astore          6
        //   184: aload           6
        //   186: aload_2        
        //   187: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   190: pop            
        //   191: aload           6
        //   193: ldc_w           "("
        //   196: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   199: pop            
        //   200: aload           6
        //   202: aload_3        
        //   203: iload           4
        //   205: aaload         
        //   206: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   209: pop            
        //   210: aload           6
        //   212: ldc_w           "=*)"
        //   215: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   218: pop            
        //   219: aload           6
        //   221: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   224: astore_2       
        //   225: iload           4
        //   227: iconst_1       
        //   228: iadd           
        //   229: istore          4
        //   231: goto            168
        //   234: new             Ljava/lang/StringBuilder;
        //   237: dup            
        //   238: invokespecial   java/lang/StringBuilder.<init>:()V
        //   241: astore          6
        //   243: aload           6
        //   245: ldc_w           "(|"
        //   248: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   251: pop            
        //   252: aload           6
        //   254: aload_2        
        //   255: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   258: pop            
        //   259: aload           6
        //   261: ldc_w           ")"
        //   264: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   267: pop            
        //   268: aload           6
        //   270: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   273: astore          6
        //   275: new             Ljava/lang/StringBuilder;
        //   278: dup            
        //   279: invokespecial   java/lang/StringBuilder.<init>:()V
        //   282: astore_2       
        //   283: aload_2        
        //   284: ldc_w           "(&"
        //   287: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   290: pop            
        //   291: aload_2        
        //   292: aload_1        
        //   293: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   296: pop            
        //   297: aload_2        
        //   298: ldc_w           ""
        //   301: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   304: pop            
        //   305: aload_2        
        //   306: aload           6
        //   308: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   311: pop            
        //   312: aload_2        
        //   313: ldc_w           ")"
        //   316: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   319: pop            
        //   320: aload_2        
        //   321: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   324: astore_2       
        //   325: aload_1        
        //   326: ifnonnull       332
        //   329: goto            335
        //   332: aload_2        
        //   333: astore          6
        //   335: aload_0        
        //   336: aload           6
        //   338: invokespecial   org/bouncycastle/x509/util/LDAPStoreHelper.getFromCache:(Ljava/lang/String;)Ljava/util/List;
        //   341: astore_1       
        //   342: aload_1        
        //   343: ifnull          348
        //   346: aload_1        
        //   347: areturn        
        //   348: new             Ljava/util/ArrayList;
        //   351: dup            
        //   352: invokespecial   java/util/ArrayList.<init>:()V
        //   355: astore          9
        //   357: aload           8
        //   359: astore_1       
        //   360: aload_0        
        //   361: invokespecial   org/bouncycastle/x509/util/LDAPStoreHelper.connectLDAP:()Ljavax/naming/directory/DirContext;
        //   364: astore_2       
        //   365: aload_2        
        //   366: astore          7
        //   368: aload_2        
        //   369: astore_1       
        //   370: new             Ljavax/naming/directory/SearchControls;
        //   373: dup            
        //   374: invokespecial   javax/naming/directory/SearchControls.<init>:()V
        //   377: astore          8
        //   379: aload_2        
        //   380: astore          7
        //   382: aload_2        
        //   383: astore_1       
        //   384: aload           8
        //   386: iconst_2       
        //   387: invokevirtual   javax/naming/directory/SearchControls.setSearchScope:(I)V
        //   390: aload_2        
        //   391: astore          7
        //   393: aload_2        
        //   394: astore_1       
        //   395: aload           8
        //   397: lconst_0       
        //   398: invokevirtual   javax/naming/directory/SearchControls.setCountLimit:(J)V
        //   401: aload_2        
        //   402: astore          7
        //   404: aload_2        
        //   405: astore_1       
        //   406: aload           8
        //   408: aload_3        
        //   409: invokevirtual   javax/naming/directory/SearchControls.setReturningAttributes:([Ljava/lang/String;)V
        //   412: aload_2        
        //   413: astore          7
        //   415: aload_2        
        //   416: astore_1       
        //   417: aload_2        
        //   418: aload_0        
        //   419: getfield        org/bouncycastle/x509/util/LDAPStoreHelper.params:Lorg/bouncycastle/jce/X509LDAPCertStoreParameters;
        //   422: invokevirtual   org/bouncycastle/jce/X509LDAPCertStoreParameters.getBaseDN:()Ljava/lang/String;
        //   425: aload           6
        //   427: aload           8
        //   429: invokeinterface javax/naming/directory/DirContext.search:(Ljava/lang/String;Ljava/lang/String;Ljavax/naming/directory/SearchControls;)Ljavax/naming/NamingEnumeration;
        //   434: astore_3       
        //   435: aload_2        
        //   436: astore          7
        //   438: aload_2        
        //   439: astore_1       
        //   440: aload_3        
        //   441: invokeinterface javax/naming/NamingEnumeration.hasMoreElements:()Z
        //   446: ifeq            524
        //   449: aload_2        
        //   450: astore          7
        //   452: aload_2        
        //   453: astore_1       
        //   454: aload_3        
        //   455: invokeinterface javax/naming/NamingEnumeration.next:()Ljava/lang/Object;
        //   460: checkcast       Ljavax/naming/directory/SearchResult;
        //   463: invokevirtual   javax/naming/directory/SearchResult.getAttributes:()Ljavax/naming/directory/Attributes;
        //   466: invokeinterface javax/naming/directory/Attributes.getAll:()Ljavax/naming/NamingEnumeration;
        //   471: invokeinterface javax/naming/NamingEnumeration.next:()Ljava/lang/Object;
        //   476: checkcast       Ljavax/naming/directory/Attribute;
        //   479: invokeinterface javax/naming/directory/Attribute.getAll:()Ljavax/naming/NamingEnumeration;
        //   484: astore          8
        //   486: aload_2        
        //   487: astore          7
        //   489: aload_2        
        //   490: astore_1       
        //   491: aload           8
        //   493: invokeinterface javax/naming/NamingEnumeration.hasMore:()Z
        //   498: ifeq            435
        //   501: aload_2        
        //   502: astore          7
        //   504: aload_2        
        //   505: astore_1       
        //   506: aload           9
        //   508: aload           8
        //   510: invokeinterface javax/naming/NamingEnumeration.next:()Ljava/lang/Object;
        //   515: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   520: pop            
        //   521: goto            486
        //   524: aload_2        
        //   525: astore          7
        //   527: aload_2        
        //   528: astore_1       
        //   529: aload_0        
        //   530: aload           6
        //   532: aload           9
        //   534: invokespecial   org/bouncycastle/x509/util/LDAPStoreHelper.addToCache:(Ljava/lang/String;Ljava/util/List;)V
        //   537: aload_2        
        //   538: ifnull          574
        //   541: aload_2        
        //   542: astore_1       
        //   543: aload_1        
        //   544: invokeinterface javax/naming/directory/DirContext.close:()V
        //   549: aload           9
        //   551: areturn        
        //   552: astore_1       
        //   553: aload           7
        //   555: ifnull          565
        //   558: aload           7
        //   560: invokeinterface javax/naming/directory/DirContext.close:()V
        //   565: aload_1        
        //   566: athrow         
        //   567: aload_1        
        //   568: ifnull          574
        //   571: goto            543
        //   574: aload           9
        //   576: areturn        
        //   577: astore_2       
        //   578: goto            567
        //   581: astore_1       
        //   582: aload           9
        //   584: areturn        
        //   585: astore_2       
        //   586: goto            565
        //    Exceptions:
        //  throws org.bouncycastle.util.StoreException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                          
        //  -----  -----  -----  -----  ------------------------------
        //  360    365    577    574    Ljavax/naming/NamingException;
        //  360    365    552    567    Any
        //  370    379    577    574    Ljavax/naming/NamingException;
        //  370    379    552    567    Any
        //  384    390    577    574    Ljavax/naming/NamingException;
        //  384    390    552    567    Any
        //  395    401    577    574    Ljavax/naming/NamingException;
        //  395    401    552    567    Any
        //  406    412    577    574    Ljavax/naming/NamingException;
        //  406    412    552    567    Any
        //  417    435    577    574    Ljavax/naming/NamingException;
        //  417    435    552    567    Any
        //  440    449    577    574    Ljavax/naming/NamingException;
        //  440    449    552    567    Any
        //  454    486    577    574    Ljavax/naming/NamingException;
        //  454    486    552    567    Any
        //  491    501    577    574    Ljavax/naming/NamingException;
        //  491    501    552    567    Any
        //  506    521    577    574    Ljavax/naming/NamingException;
        //  506    521    552    567    Any
        //  529    537    577    574    Ljavax/naming/NamingException;
        //  529    537    552    567    Any
        //  543    549    581    585    Ljava/lang/Exception;
        //  558    565    585    589    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 303 out of bounds for length 303
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:373)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:426)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private String[] splitString(final String s) {
        return s.split("\\s+");
    }
    
    public Collection getAACertificates(final X509AttributeCertStoreSelector x509AttributeCertStoreSelector) throws StoreException {
        final String[] splitString = this.splitString(this.params.getAACertificateAttribute());
        final String[] splitString2 = this.splitString(this.params.getLdapAACertificateAttributeName());
        final String[] splitString3 = this.splitString(this.params.getAACertificateSubjectAttributeName());
        final Set attributeCertificates = this.createAttributeCertificates(this.attrCertSubjectSerialSearch(x509AttributeCertStoreSelector, splitString, splitString2, splitString3), x509AttributeCertStoreSelector);
        if (attributeCertificates.size() == 0) {
            attributeCertificates.addAll(this.createAttributeCertificates(this.attrCertSubjectSerialSearch(new X509AttributeCertStoreSelector(), splitString, splitString2, splitString3), x509AttributeCertStoreSelector));
        }
        return attributeCertificates;
    }
    
    public Collection getAttributeAuthorityRevocationLists(final X509CRLStoreSelector x509CRLStoreSelector) throws StoreException {
        final String[] splitString = this.splitString(this.params.getAttributeAuthorityRevocationListAttribute());
        final String[] splitString2 = this.splitString(this.params.getLdapAttributeAuthorityRevocationListAttributeName());
        final String[] splitString3 = this.splitString(this.params.getAttributeAuthorityRevocationListIssuerAttributeName());
        final Set crLs = this.createCRLs(this.cRLIssuerSearch(x509CRLStoreSelector, splitString, splitString2, splitString3), x509CRLStoreSelector);
        if (crLs.size() == 0) {
            crLs.addAll(this.createCRLs(this.cRLIssuerSearch(new X509CRLStoreSelector(), splitString, splitString2, splitString3), x509CRLStoreSelector));
        }
        return crLs;
    }
    
    public Collection getAttributeCertificateAttributes(final X509AttributeCertStoreSelector x509AttributeCertStoreSelector) throws StoreException {
        final String[] splitString = this.splitString(this.params.getAttributeCertificateAttributeAttribute());
        final String[] splitString2 = this.splitString(this.params.getLdapAttributeCertificateAttributeAttributeName());
        final String[] splitString3 = this.splitString(this.params.getAttributeCertificateAttributeSubjectAttributeName());
        final Set attributeCertificates = this.createAttributeCertificates(this.attrCertSubjectSerialSearch(x509AttributeCertStoreSelector, splitString, splitString2, splitString3), x509AttributeCertStoreSelector);
        if (attributeCertificates.size() == 0) {
            attributeCertificates.addAll(this.createAttributeCertificates(this.attrCertSubjectSerialSearch(new X509AttributeCertStoreSelector(), splitString, splitString2, splitString3), x509AttributeCertStoreSelector));
        }
        return attributeCertificates;
    }
    
    public Collection getAttributeCertificateRevocationLists(final X509CRLStoreSelector x509CRLStoreSelector) throws StoreException {
        final String[] splitString = this.splitString(this.params.getAttributeCertificateRevocationListAttribute());
        final String[] splitString2 = this.splitString(this.params.getLdapAttributeCertificateRevocationListAttributeName());
        final String[] splitString3 = this.splitString(this.params.getAttributeCertificateRevocationListIssuerAttributeName());
        final Set crLs = this.createCRLs(this.cRLIssuerSearch(x509CRLStoreSelector, splitString, splitString2, splitString3), x509CRLStoreSelector);
        if (crLs.size() == 0) {
            crLs.addAll(this.createCRLs(this.cRLIssuerSearch(new X509CRLStoreSelector(), splitString, splitString2, splitString3), x509CRLStoreSelector));
        }
        return crLs;
    }
    
    public Collection getAttributeDescriptorCertificates(final X509AttributeCertStoreSelector x509AttributeCertStoreSelector) throws StoreException {
        final String[] splitString = this.splitString(this.params.getAttributeDescriptorCertificateAttribute());
        final String[] splitString2 = this.splitString(this.params.getLdapAttributeDescriptorCertificateAttributeName());
        final String[] splitString3 = this.splitString(this.params.getAttributeDescriptorCertificateSubjectAttributeName());
        final Set attributeCertificates = this.createAttributeCertificates(this.attrCertSubjectSerialSearch(x509AttributeCertStoreSelector, splitString, splitString2, splitString3), x509AttributeCertStoreSelector);
        if (attributeCertificates.size() == 0) {
            attributeCertificates.addAll(this.createAttributeCertificates(this.attrCertSubjectSerialSearch(new X509AttributeCertStoreSelector(), splitString, splitString2, splitString3), x509AttributeCertStoreSelector));
        }
        return attributeCertificates;
    }
    
    public Collection getAuthorityRevocationLists(final X509CRLStoreSelector x509CRLStoreSelector) throws StoreException {
        final String[] splitString = this.splitString(this.params.getAuthorityRevocationListAttribute());
        final String[] splitString2 = this.splitString(this.params.getLdapAuthorityRevocationListAttributeName());
        final String[] splitString3 = this.splitString(this.params.getAuthorityRevocationListIssuerAttributeName());
        final Set crLs = this.createCRLs(this.cRLIssuerSearch(x509CRLStoreSelector, splitString, splitString2, splitString3), x509CRLStoreSelector);
        if (crLs.size() == 0) {
            crLs.addAll(this.createCRLs(this.cRLIssuerSearch(new X509CRLStoreSelector(), splitString, splitString2, splitString3), x509CRLStoreSelector));
        }
        return crLs;
    }
    
    public Collection getCACertificates(final X509CertStoreSelector x509CertStoreSelector) throws StoreException {
        final String[] splitString = this.splitString(this.params.getCACertificateAttribute());
        final String[] splitString2 = this.splitString(this.params.getLdapCACertificateAttributeName());
        final String[] splitString3 = this.splitString(this.params.getCACertificateSubjectAttributeName());
        final Set certs = this.createCerts(this.certSubjectSerialSearch(x509CertStoreSelector, splitString, splitString2, splitString3), x509CertStoreSelector);
        if (certs.size() == 0) {
            certs.addAll(this.createCerts(this.certSubjectSerialSearch(new X509CertStoreSelector(), splitString, splitString2, splitString3), x509CertStoreSelector));
        }
        return certs;
    }
    
    public Collection getCertificateRevocationLists(final X509CRLStoreSelector x509CRLStoreSelector) throws StoreException {
        final String[] splitString = this.splitString(this.params.getCertificateRevocationListAttribute());
        final String[] splitString2 = this.splitString(this.params.getLdapCertificateRevocationListAttributeName());
        final String[] splitString3 = this.splitString(this.params.getCertificateRevocationListIssuerAttributeName());
        final Set crLs = this.createCRLs(this.cRLIssuerSearch(x509CRLStoreSelector, splitString, splitString2, splitString3), x509CRLStoreSelector);
        if (crLs.size() == 0) {
            crLs.addAll(this.createCRLs(this.cRLIssuerSearch(new X509CRLStoreSelector(), splitString, splitString2, splitString3), x509CRLStoreSelector));
        }
        return crLs;
    }
    
    public Collection getCrossCertificatePairs(final X509CertPairStoreSelector x509CertPairStoreSelector) throws StoreException {
        final String[] splitString = this.splitString(this.params.getCrossCertificateAttribute());
        final String[] splitString2 = this.splitString(this.params.getLdapCrossCertificateAttributeName());
        final String[] splitString3 = this.splitString(this.params.getCrossCertificateSubjectAttributeName());
        final Set crossCertificatePairs = this.createCrossCertificatePairs(this.crossCertificatePairSubjectSearch(x509CertPairStoreSelector, splitString, splitString2, splitString3), x509CertPairStoreSelector);
        if (crossCertificatePairs.size() == 0) {
            final X509CertStoreSelector x509CertStoreSelector = new X509CertStoreSelector();
            final X509CertPairStoreSelector x509CertPairStoreSelector2 = new X509CertPairStoreSelector();
            x509CertPairStoreSelector2.setForwardSelector(x509CertStoreSelector);
            x509CertPairStoreSelector2.setReverseSelector(x509CertStoreSelector);
            crossCertificatePairs.addAll(this.createCrossCertificatePairs(this.crossCertificatePairSubjectSearch(x509CertPairStoreSelector2, splitString, splitString2, splitString3), x509CertPairStoreSelector));
        }
        return crossCertificatePairs;
    }
    
    public Collection getDeltaCertificateRevocationLists(final X509CRLStoreSelector x509CRLStoreSelector) throws StoreException {
        final String[] splitString = this.splitString(this.params.getDeltaRevocationListAttribute());
        final String[] splitString2 = this.splitString(this.params.getLdapDeltaRevocationListAttributeName());
        final String[] splitString3 = this.splitString(this.params.getDeltaRevocationListIssuerAttributeName());
        final Set crLs = this.createCRLs(this.cRLIssuerSearch(x509CRLStoreSelector, splitString, splitString2, splitString3), x509CRLStoreSelector);
        if (crLs.size() == 0) {
            crLs.addAll(this.createCRLs(this.cRLIssuerSearch(new X509CRLStoreSelector(), splitString, splitString2, splitString3), x509CRLStoreSelector));
        }
        return crLs;
    }
    
    public Collection getUserCertificates(final X509CertStoreSelector x509CertStoreSelector) throws StoreException {
        final String[] splitString = this.splitString(this.params.getUserCertificateAttribute());
        final String[] splitString2 = this.splitString(this.params.getLdapUserCertificateAttributeName());
        final String[] splitString3 = this.splitString(this.params.getUserCertificateSubjectAttributeName());
        final Set certs = this.createCerts(this.certSubjectSerialSearch(x509CertStoreSelector, splitString, splitString2, splitString3), x509CertStoreSelector);
        if (certs.size() == 0) {
            certs.addAll(this.createCerts(this.certSubjectSerialSearch(new X509CertStoreSelector(), splitString, splitString2, splitString3), x509CertStoreSelector));
        }
        return certs;
    }
}
