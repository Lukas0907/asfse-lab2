// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.io;

import kotlin.internal.PlatformImplementationsKt;
import kotlin.jvm.internal.InlineMarker;
import kotlin.sequences.Sequence;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.io.PrintWriter;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import java.io.Writer;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.BufferedWriter;
import java.io.Reader;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import kotlin.text.Charsets;
import java.nio.charset.Charset;
import java.io.Closeable;
import kotlin.Unit;
import java.io.FileOutputStream;
import kotlin.jvm.internal.Intrinsics;
import java.io.File;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000z\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\u001c\u0010\u0005\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t\u001a!\u0010\n\u001a\u00020\u000b*\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\f\u001a\u00020\rH\u0087\b\u001a!\u0010\u000e\u001a\u00020\u000f*\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\f\u001a\u00020\rH\u0087\b\u001aB\u0010\u0010\u001a\u00020\u0001*\u00020\u000226\u0010\u0011\u001a2\u0012\u0013\u0012\u00110\u0004¢\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\u0015\u0012\u0013\u0012\u00110\r¢\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\u0016\u0012\u0004\u0012\u00020\u00010\u0012\u001aJ\u0010\u0010\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0017\u001a\u00020\r26\u0010\u0011\u001a2\u0012\u0013\u0012\u00110\u0004¢\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\u0015\u0012\u0013\u0012\u00110\r¢\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\u0016\u0012\u0004\u0012\u00020\u00010\u0012\u001a7\u0010\u0018\u001a\u00020\u0001*\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\t2!\u0010\u0011\u001a\u001d\u0012\u0013\u0012\u00110\u0007¢\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\u001a\u0012\u0004\u0012\u00020\u00010\u0019\u001a\r\u0010\u001b\u001a\u00020\u001c*\u00020\u0002H\u0087\b\u001a\r\u0010\u001d\u001a\u00020\u001e*\u00020\u0002H\u0087\b\u001a\u0017\u0010\u001f\u001a\u00020 *\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\tH\u0087\b\u001a\n\u0010!\u001a\u00020\u0004*\u00020\u0002\u001a\u001a\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00070#*\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\t\u001a\u0014\u0010$\u001a\u00020\u0007*\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\t\u001a\u0017\u0010%\u001a\u00020&*\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\tH\u0087\b\u001a?\u0010'\u001a\u0002H(\"\u0004\b\u0000\u0010(*\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\t2\u0018\u0010)\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070*\u0012\u0004\u0012\u0002H(0\u0019H\u0086\b\u00f8\u0001\u0000¢\u0006\u0002\u0010,\u001a\u0012\u0010-\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\u001c\u0010.\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t\u001a\u0017\u0010/\u001a\u000200*\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\tH\u0087\b\u0082\u0002\b\n\u0006\b\u0011(+0\u0001¨\u00061" }, d2 = { "appendBytes", "", "Ljava/io/File;", "array", "", "appendText", "text", "", "charset", "Ljava/nio/charset/Charset;", "bufferedReader", "Ljava/io/BufferedReader;", "bufferSize", "", "bufferedWriter", "Ljava/io/BufferedWriter;", "forEachBlock", "action", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "buffer", "bytesRead", "blockSize", "forEachLine", "Lkotlin/Function1;", "line", "inputStream", "Ljava/io/FileInputStream;", "outputStream", "Ljava/io/FileOutputStream;", "printWriter", "Ljava/io/PrintWriter;", "readBytes", "readLines", "", "readText", "reader", "Ljava/io/InputStreamReader;", "useLines", "T", "block", "Lkotlin/sequences/Sequence;", "Requires newer compiler version to be inlined correctly.", "(Ljava/io/File;Ljava/nio/charset/Charset;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "writeBytes", "writeText", "writer", "Ljava/io/OutputStreamWriter;", "kotlin-stdlib" }, k = 5, mv = { 1, 1, 15 }, xi = 1, xs = "kotlin/io/FilesKt")
class FilesKt__FileReadWriteKt extends FilesKt__FilePathComponentsKt
{
    public FilesKt__FileReadWriteKt() {
    }
    
    public static final void appendBytes(File file, final byte[] b) {
        Intrinsics.checkParameterIsNotNull(file, "$this$appendBytes");
        Intrinsics.checkParameterIsNotNull(b, "array");
        file = (File)new FileOutputStream(file, true);
        final Throwable t = null;
        try {
            ((FileOutputStream)file).write(b);
            final Unit instance = Unit.INSTANCE;
            CloseableKt.closeFinally((Closeable)file, t);
        }
        finally {
            try {}
            finally {
                CloseableKt.closeFinally((Closeable)file, (Throwable)(Object)b);
            }
        }
    }
    
    public static final void appendText(final File file, final String s, final Charset charset) {
        Intrinsics.checkParameterIsNotNull(file, "$this$appendText");
        Intrinsics.checkParameterIsNotNull(s, "text");
        Intrinsics.checkParameterIsNotNull(charset, "charset");
        final byte[] bytes = s.getBytes(charset);
        Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
        appendBytes(file, bytes);
    }
    
    private static final BufferedReader bufferedReader(final File file, final Charset cs, final int sz) {
        final InputStreamReader in = new InputStreamReader(new FileInputStream(file), cs);
        if (in instanceof BufferedReader) {
            return (BufferedReader)in;
        }
        return new BufferedReader(in, sz);
    }
    
    private static final BufferedWriter bufferedWriter(final File file, final Charset cs, final int sz) {
        final OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(file), cs);
        if (out instanceof BufferedWriter) {
            return (BufferedWriter)out;
        }
        return new BufferedWriter(out, sz);
    }
    
    public static final void forEachBlock(File file, int read, final Function2<? super byte[], ? super Integer, Unit> function2) {
        Intrinsics.checkParameterIsNotNull(file, "$this$forEachBlock");
        Intrinsics.checkParameterIsNotNull(function2, "action");
        final byte[] b = new byte[RangesKt___RangesKt.coerceAtLeast(read, 512)];
        file = (File)new FileInputStream(file);
        final Throwable t = null;
        try {
            final FileInputStream fileInputStream = (FileInputStream)file;
            while (true) {
                read = fileInputStream.read(b);
                if (read <= 0) {
                    break;
                }
                function2.invoke(b, read);
            }
            final Unit instance = Unit.INSTANCE;
            CloseableKt.closeFinally((Closeable)file, t);
        }
        finally {
            try {}
            finally {
                CloseableKt.closeFinally((Closeable)file, (Throwable)function2);
            }
        }
    }
    
    public static final void forEachBlock(final File file, final Function2<? super byte[], ? super Integer, Unit> function2) {
        Intrinsics.checkParameterIsNotNull(file, "$this$forEachBlock");
        Intrinsics.checkParameterIsNotNull(function2, "action");
        forEachBlock(file, 4096, function2);
    }
    
    public static final void forEachLine(final File file, final Charset cs, final Function1<? super String, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(file, "$this$forEachLine");
        Intrinsics.checkParameterIsNotNull(cs, "charset");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        TextStreamsKt.forEachLine(new BufferedReader(new InputStreamReader(new FileInputStream(file), cs)), function1);
    }
    
    private static final FileInputStream inputStream(final File file) {
        return new FileInputStream(file);
    }
    
    private static final FileOutputStream outputStream(final File file) {
        return new FileOutputStream(file);
    }
    
    private static final PrintWriter printWriter(final File file, final Charset cs) {
        final OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(file), cs);
        BufferedWriter bufferedWriter;
        if (out instanceof BufferedWriter) {
            bufferedWriter = (BufferedWriter)out;
        }
        else {
            bufferedWriter = new BufferedWriter(out, 8192);
        }
        return new PrintWriter(bufferedWriter);
    }
    
    public static final byte[] readBytes(File copy) {
    Label_0077_Outer:
        while (true) {
            Intrinsics.checkParameterIsNotNull(copy, "$this$readBytes");
            final FileInputStream fileInputStream = new FileInputStream(copy);
            final Throwable t = null;
        Label_0089_Outer:
            while (true) {
                while (true) {
                    int len = 0;
                Label_0190:
                    while (true) {
                        int n = 0;
                        int read = 0;
                        Label_0179: {
                            try {
                                final FileInputStream fileInputStream2 = fileInputStream;
                                n = 0;
                                final long length = copy.length();
                                if (length > Integer.MAX_VALUE) {
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append("File ");
                                    sb.append(copy);
                                    sb.append(" is too big (");
                                    sb.append(length);
                                    sb.append(" bytes) to fit in memory.");
                                    throw new OutOfMemoryError(sb.toString());
                                }
                                len = (int)length;
                                copy = (File)(Object)new byte[len];
                                if (len <= 0) {
                                    break Label_0190;
                                }
                                read = fileInputStream2.read((byte[])(Object)copy, n, len);
                                if (read < 0) {
                                    break Label_0190;
                                }
                                break Label_0179;
                                copy = (File)(Object)Arrays.copyOf((byte[])(Object)copy, n);
                                Intrinsics.checkExpressionValueIsNotNull(copy, "java.util.Arrays.copyOf(this, newSize)");
                                final Throwable t2;
                                CloseableKt.closeFinally(fileInputStream, t2);
                                return (byte[])(Object)copy;
                            }
                            finally {
                                try {}
                                finally {
                                    CloseableKt.closeFinally(fileInputStream, (Throwable)copy);
                                }
                            }
                        }
                        len -= read;
                        n += read;
                        continue Label_0077_Outer;
                    }
                    if (len == 0) {
                        continue;
                    }
                    break;
                }
                continue Label_0089_Outer;
            }
        }
    }
    
    public static final List<String> readLines(final File file, final Charset charset) {
        Intrinsics.checkParameterIsNotNull(file, "$this$readLines");
        Intrinsics.checkParameterIsNotNull(charset, "charset");
        final ArrayList<String> list = new ArrayList<String>();
        forEachLine(file, charset, (Function1<? super String, Unit>)new FilesKt__FileReadWriteKt$readLines.FilesKt__FileReadWriteKt$readLines$1((ArrayList)list));
        return list;
    }
    
    public static final String readText(final File file, final Charset charset) {
        Intrinsics.checkParameterIsNotNull(file, "$this$readText");
        Intrinsics.checkParameterIsNotNull(charset, "charset");
        return new String(readBytes(file), charset);
    }
    
    private static final InputStreamReader reader(final File file, final Charset cs) {
        return new InputStreamReader(new FileInputStream(file), cs);
    }
    
    public static final <T> T useLines(File file, final Charset cs, final Function1<? super Sequence<String>, ? extends T> function1) {
        Intrinsics.checkParameterIsNotNull(file, "$this$useLines");
        Intrinsics.checkParameterIsNotNull(cs, "charset");
        Intrinsics.checkParameterIsNotNull(function1, "block");
        final InputStreamReader in = new InputStreamReader(new FileInputStream(file), cs);
        BufferedReader bufferedReader;
        if (in instanceof BufferedReader) {
            bufferedReader = (BufferedReader)in;
        }
        else {
            bufferedReader = new BufferedReader(in, 8192);
        }
        file = (File)bufferedReader;
        final Throwable t = null;
        try {
            final T invoke = function1.invoke(TextStreamsKt.lineSequence((BufferedReader)file));
            InlineMarker.finallyStart(1);
            if (PlatformImplementationsKt.apiVersionIsAtLeast(1, 1, 0)) {
                CloseableKt.closeFinally((Closeable)file, t);
            }
            else {
                ((Closeable)file).close();
            }
            InlineMarker.finallyEnd(1);
            return invoke;
        }
        finally {
            try {}
            finally {
                InlineMarker.finallyStart(1);
                Label_0153: {
                    if (PlatformImplementationsKt.apiVersionIsAtLeast(1, 1, 0)) {
                        break Label_0153;
                    }
                    try {
                        ((Closeable)file).close();
                        InlineMarker.finallyEnd(1);
                        break Label_0153;
                        CloseableKt.closeFinally((Closeable)file, (Throwable)function1);
                    }
                    finally {}
                }
            }
        }
    }
    
    public static final void writeBytes(File file, final byte[] b) {
        Intrinsics.checkParameterIsNotNull(file, "$this$writeBytes");
        Intrinsics.checkParameterIsNotNull(b, "array");
        file = (File)new FileOutputStream(file);
        final Throwable t = null;
        try {
            ((FileOutputStream)file).write(b);
            final Unit instance = Unit.INSTANCE;
            CloseableKt.closeFinally((Closeable)file, t);
        }
        finally {
            try {}
            finally {
                CloseableKt.closeFinally((Closeable)file, (Throwable)(Object)b);
            }
        }
    }
    
    public static final void writeText(final File file, final String s, final Charset charset) {
        Intrinsics.checkParameterIsNotNull(file, "$this$writeText");
        Intrinsics.checkParameterIsNotNull(s, "text");
        Intrinsics.checkParameterIsNotNull(charset, "charset");
        final byte[] bytes = s.getBytes(charset);
        Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
        writeBytes(file, bytes);
    }
    
    private static final OutputStreamWriter writer(final File file, final Charset cs) {
        return new OutputStreamWriter(new FileOutputStream(file), cs);
    }
}
