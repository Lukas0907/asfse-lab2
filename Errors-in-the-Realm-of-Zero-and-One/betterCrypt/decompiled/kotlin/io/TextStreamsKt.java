// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.io;

import kotlin.internal.PlatformImplementationsKt;
import kotlin.jvm.internal.InlineMarker;
import java.io.StringReader;
import kotlin.text.Charsets;
import java.nio.charset.Charset;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.io.InputStream;
import java.net.URL;
import kotlin.sequences.Sequence;
import java.util.Iterator;
import java.io.Closeable;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import java.io.BufferedWriter;
import java.io.Writer;
import java.io.BufferedReader;
import java.io.Reader;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000X\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u001a\u0017\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\b\b\u0002\u0010\u0003\u001a\u00020\u0004H\u0087\b\u001a\u0017\u0010\u0000\u001a\u00020\u0005*\u00020\u00062\b\b\u0002\u0010\u0003\u001a\u00020\u0004H\u0087\b\u001a\u001c\u0010\u0007\u001a\u00020\b*\u00020\u00022\u0006\u0010\t\u001a\u00020\u00062\b\b\u0002\u0010\u0003\u001a\u00020\u0004\u001a\u001e\u0010\n\u001a\u00020\u000b*\u00020\u00022\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000b0\r\u001a\u0010\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0010*\u00020\u0001\u001a\n\u0010\u0011\u001a\u00020\u0012*\u00020\u0013\u001a\u0010\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0015*\u00020\u0002\u001a\n\u0010\u0016\u001a\u00020\u000e*\u00020\u0002\u001a\u0017\u0010\u0016\u001a\u00020\u000e*\u00020\u00132\b\b\u0002\u0010\u0017\u001a\u00020\u0018H\u0087\b\u001a\r\u0010\u0019\u001a\u00020\u001a*\u00020\u000eH\u0087\b\u001a5\u0010\u001b\u001a\u0002H\u001c\"\u0004\b\u0000\u0010\u001c*\u00020\u00022\u0018\u0010\u001d\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\u0010\u0012\u0004\u0012\u0002H\u001c0\rH\u0086\b\u00f8\u0001\u0000¢\u0006\u0002\u0010\u001f\u0082\u0002\b\n\u0006\b\u0011(\u001e0\u0001¨\u0006 " }, d2 = { "buffered", "Ljava/io/BufferedReader;", "Ljava/io/Reader;", "bufferSize", "", "Ljava/io/BufferedWriter;", "Ljava/io/Writer;", "copyTo", "", "out", "forEachLine", "", "action", "Lkotlin/Function1;", "", "lineSequence", "Lkotlin/sequences/Sequence;", "readBytes", "", "Ljava/net/URL;", "readLines", "", "readText", "charset", "Ljava/nio/charset/Charset;", "reader", "Ljava/io/StringReader;", "useLines", "T", "block", "Requires newer compiler version to be inlined correctly.", "(Ljava/io/Reader;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "kotlin-stdlib" }, k = 2, mv = { 1, 1, 15 })
public final class TextStreamsKt
{
    private static final BufferedReader buffered(final Reader in, final int sz) {
        if (in instanceof BufferedReader) {
            return (BufferedReader)in;
        }
        return new BufferedReader(in, sz);
    }
    
    private static final BufferedWriter buffered(final Writer out, final int sz) {
        if (out instanceof BufferedWriter) {
            return (BufferedWriter)out;
        }
        return new BufferedWriter(out, sz);
    }
    
    public static final long copyTo(final Reader reader, final Writer writer, int i) {
        Intrinsics.checkParameterIsNotNull(reader, "$this$copyTo");
        Intrinsics.checkParameterIsNotNull(writer, "out");
        final char[] array = new char[i];
        i = reader.read(array);
        long n = 0L;
        while (i >= 0) {
            writer.write(array, 0, i);
            n += i;
            i = reader.read(array);
        }
        return n;
    }
    
    public static /* synthetic */ long copyTo$default(final Reader reader, final Writer writer, int n, final int n2, final Object o) {
        if ((n2 & 0x2) != 0x0) {
            n = 8192;
        }
        return copyTo(reader, writer, n);
    }
    
    public static final void forEachLine(Reader in, final Function1<? super String, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(in, "$this$forEachLine");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        BufferedReader bufferedReader;
        if (in instanceof BufferedReader) {
            bufferedReader = (BufferedReader)in;
        }
        else {
            bufferedReader = new BufferedReader(in, 8192);
        }
        in = bufferedReader;
        final Throwable t = null;
        try {
            final Iterator<String> iterator = lineSequence((BufferedReader)in).iterator();
            while (iterator.hasNext()) {
                function1.invoke(iterator.next());
            }
            final Unit instance = Unit.INSTANCE;
            CloseableKt.closeFinally(in, t);
        }
        finally {
            try {}
            finally {
                CloseableKt.closeFinally(in, (Throwable)function1);
            }
        }
    }
    
    public static final Sequence<String> lineSequence(final BufferedReader bufferedReader) {
        Intrinsics.checkParameterIsNotNull(bufferedReader, "$this$lineSequence");
        return SequencesKt__SequencesKt.constrainOnce((Sequence<? extends String>)new LinesSequence(bufferedReader));
    }
    
    public static final byte[] readBytes(URL url) {
        Intrinsics.checkParameterIsNotNull(url, "$this$readBytes");
        url = (URL)url.openStream();
        final Throwable t = null;
        try {
            final InputStream inputStream = (InputStream)url;
            Intrinsics.checkExpressionValueIsNotNull(inputStream, "it");
            final byte[] bytes = ByteStreamsKt.readBytes(inputStream);
            CloseableKt.closeFinally((Closeable)url, t);
            return bytes;
        }
        finally {
            try {}
            finally {
                final Throwable t2;
                CloseableKt.closeFinally((Closeable)url, t2);
            }
        }
    }
    
    public static final List<String> readLines(final Reader reader) {
        Intrinsics.checkParameterIsNotNull(reader, "$this$readLines");
        final ArrayList<String> list = new ArrayList<String>();
        forEachLine(reader, (Function1<? super String, Unit>)new TextStreamsKt$readLines.TextStreamsKt$readLines$1((ArrayList)list));
        return list;
    }
    
    public static final String readText(final Reader reader) {
        Intrinsics.checkParameterIsNotNull(reader, "$this$readText");
        final StringWriter stringWriter = new StringWriter();
        copyTo$default(reader, stringWriter, 0, 2, null);
        final String string = stringWriter.toString();
        Intrinsics.checkExpressionValueIsNotNull(string, "buffer.toString()");
        return string;
    }
    
    private static final String readText(final URL url, final Charset charset) {
        return new String(readBytes(url), charset);
    }
    
    private static final StringReader reader(final String s) {
        return new StringReader(s);
    }
    
    public static final <T> T useLines(Reader in, final Function1<? super Sequence<String>, ? extends T> function1) {
        Intrinsics.checkParameterIsNotNull(in, "$this$useLines");
        Intrinsics.checkParameterIsNotNull(function1, "block");
        BufferedReader bufferedReader;
        if (in instanceof BufferedReader) {
            bufferedReader = (BufferedReader)in;
        }
        else {
            bufferedReader = new BufferedReader(in, 8192);
        }
        in = bufferedReader;
        final Throwable t = null;
        try {
            final T invoke = function1.invoke(lineSequence((BufferedReader)in));
            InlineMarker.finallyStart(1);
            if (PlatformImplementationsKt.apiVersionIsAtLeast(1, 1, 0)) {
                CloseableKt.closeFinally(in, t);
            }
            else {
                in.close();
            }
            InlineMarker.finallyEnd(1);
            return invoke;
        }
        finally {
            try {}
            finally {
                InlineMarker.finallyStart(1);
                Label_0122: {
                    if (PlatformImplementationsKt.apiVersionIsAtLeast(1, 1, 0)) {
                        break Label_0122;
                    }
                    try {
                        in.close();
                        InlineMarker.finallyEnd(1);
                        break Label_0122;
                        final Throwable t2;
                        CloseableKt.closeFinally(in, t2);
                    }
                    finally {}
                }
            }
        }
    }
}
