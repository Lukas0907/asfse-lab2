// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.io;

import kotlin.ReplaceWith;
import kotlin.Deprecated;
import java.io.ByteArrayOutputStream;
import kotlin.collections.ByteIterator;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import java.io.ByteArrayInputStream;
import java.io.Writer;
import java.io.OutputStreamWriter;
import java.io.BufferedWriter;
import kotlin.text.Charsets;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.nio.charset.Charset;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.BufferedInputStream;
import java.io.InputStream;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000Z\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u0012\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0017\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\b\b\u0002\u0010\u0003\u001a\u00020\u0004H\u0087\b\u001a\u0017\u0010\u0000\u001a\u00020\u0005*\u00020\u00062\b\b\u0002\u0010\u0003\u001a\u00020\u0004H\u0087\b\u001a\u0017\u0010\u0007\u001a\u00020\b*\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\nH\u0087\b\u001a\u0017\u0010\u000b\u001a\u00020\f*\u00020\u00062\b\b\u0002\u0010\t\u001a\u00020\nH\u0087\b\u001a\u0017\u0010\r\u001a\u00020\u000e*\u00020\u000f2\b\b\u0002\u0010\t\u001a\u00020\nH\u0087\b\u001a\u001c\u0010\u0010\u001a\u00020\u0011*\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00062\b\b\u0002\u0010\u0003\u001a\u00020\u0004\u001a\r\u0010\u0013\u001a\u00020\u000e*\u00020\u0014H\u0087\b\u001a\u001d\u0010\u0013\u001a\u00020\u000e*\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u0004H\u0087\b\u001a\r\u0010\u0017\u001a\u00020\u0018*\u00020\u0001H\u0086\u0002\u001a\f\u0010\u0019\u001a\u00020\u0014*\u00020\u0002H\u0007\u001a\u0016\u0010\u0019\u001a\u00020\u0014*\u00020\u00022\b\b\u0002\u0010\u001a\u001a\u00020\u0004H\u0007\u001a\u0017\u0010\u001b\u001a\u00020\u001c*\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\nH\u0087\b\u001a\u0017\u0010\u001d\u001a\u00020\u001e*\u00020\u00062\b\b\u0002\u0010\t\u001a\u00020\nH\u0087\b¨\u0006\u001f" }, d2 = { "buffered", "Ljava/io/BufferedInputStream;", "Ljava/io/InputStream;", "bufferSize", "", "Ljava/io/BufferedOutputStream;", "Ljava/io/OutputStream;", "bufferedReader", "Ljava/io/BufferedReader;", "charset", "Ljava/nio/charset/Charset;", "bufferedWriter", "Ljava/io/BufferedWriter;", "byteInputStream", "Ljava/io/ByteArrayInputStream;", "", "copyTo", "", "out", "inputStream", "", "offset", "length", "iterator", "Lkotlin/collections/ByteIterator;", "readBytes", "estimatedSize", "reader", "Ljava/io/InputStreamReader;", "writer", "Ljava/io/OutputStreamWriter;", "kotlin-stdlib" }, k = 2, mv = { 1, 1, 15 })
public final class ByteStreamsKt
{
    private static final BufferedInputStream buffered(final InputStream in, final int size) {
        if (in instanceof BufferedInputStream) {
            return (BufferedInputStream)in;
        }
        return new BufferedInputStream(in, size);
    }
    
    private static final BufferedOutputStream buffered(final OutputStream out, final int size) {
        if (out instanceof BufferedOutputStream) {
            return (BufferedOutputStream)out;
        }
        return new BufferedOutputStream(out, size);
    }
    
    private static final BufferedReader bufferedReader(final InputStream in, final Charset cs) {
        final InputStreamReader in2 = new InputStreamReader(in, cs);
        if (in2 instanceof BufferedReader) {
            return (BufferedReader)in2;
        }
        return new BufferedReader(in2, 8192);
    }
    
    private static final BufferedWriter bufferedWriter(final OutputStream out, final Charset cs) {
        final OutputStreamWriter out2 = new OutputStreamWriter(out, cs);
        if (out2 instanceof BufferedWriter) {
            return (BufferedWriter)out2;
        }
        return new BufferedWriter(out2, 8192);
    }
    
    private static final ByteArrayInputStream byteInputStream(final String s, final Charset charset) {
        if (s != null) {
            final byte[] bytes = s.getBytes(charset);
            Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
            return new ByteArrayInputStream(bytes);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    public static final long copyTo(final InputStream inputStream, final OutputStream outputStream, int i) {
        Intrinsics.checkParameterIsNotNull(inputStream, "$this$copyTo");
        Intrinsics.checkParameterIsNotNull(outputStream, "out");
        final byte[] b = new byte[i];
        i = inputStream.read(b);
        long n = 0L;
        while (i >= 0) {
            outputStream.write(b, 0, i);
            n += i;
            i = inputStream.read(b);
        }
        return n;
    }
    
    public static /* synthetic */ long copyTo$default(final InputStream inputStream, final OutputStream outputStream, int n, final int n2, final Object o) {
        if ((n2 & 0x2) != 0x0) {
            n = 8192;
        }
        return copyTo(inputStream, outputStream, n);
    }
    
    private static final ByteArrayInputStream inputStream(final byte[] buf) {
        return new ByteArrayInputStream(buf);
    }
    
    private static final ByteArrayInputStream inputStream(final byte[] buf, final int offset, final int length) {
        return new ByteArrayInputStream(buf, offset, length);
    }
    
    public static final ByteIterator iterator(final BufferedInputStream bufferedInputStream) {
        Intrinsics.checkParameterIsNotNull(bufferedInputStream, "$this$iterator");
        return (ByteIterator)new ByteStreamsKt$iterator.ByteStreamsKt$iterator$1(bufferedInputStream);
    }
    
    public static final byte[] readBytes(final InputStream inputStream) {
        Intrinsics.checkParameterIsNotNull(inputStream, "$this$readBytes");
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(Math.max(8192, inputStream.available()));
        copyTo$default(inputStream, byteArrayOutputStream, 0, 2, null);
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        Intrinsics.checkExpressionValueIsNotNull(byteArray, "buffer.toByteArray()");
        return byteArray;
    }
    
    @Deprecated(message = "Use readBytes() overload without estimatedSize parameter", replaceWith = @ReplaceWith(expression = "readBytes()", imports = {}))
    public static final byte[] readBytes(final InputStream inputStream, final int a) {
        Intrinsics.checkParameterIsNotNull(inputStream, "$this$readBytes");
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(Math.max(a, inputStream.available()));
        copyTo$default(inputStream, byteArrayOutputStream, 0, 2, null);
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        Intrinsics.checkExpressionValueIsNotNull(byteArray, "buffer.toByteArray()");
        return byteArray;
    }
    
    private static final InputStreamReader reader(final InputStream in, final Charset cs) {
        return new InputStreamReader(in, cs);
    }
    
    private static final OutputStreamWriter writer(final OutputStream out, final Charset cs) {
        return new OutputStreamWriter(out, cs);
    }
}
