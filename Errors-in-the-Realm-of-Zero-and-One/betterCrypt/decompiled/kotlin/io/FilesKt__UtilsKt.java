// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.io;

import java.util.ArrayList;
import kotlin.jvm.functions.Function1;
import java.util.List;
import java.io.Closeable;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.util.Iterator;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import java.io.IOException;
import kotlin.jvm.functions.Function2;
import java.io.File;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000<\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\u001a(\u0010\t\u001a\u00020\u00022\b\b\u0002\u0010\n\u001a\u00020\u00012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0002\u001a(\u0010\r\u001a\u00020\u00022\b\b\u0002\u0010\n\u001a\u00020\u00012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0002\u001a8\u0010\u000e\u001a\u00020\u000f*\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00022\b\b\u0002\u0010\u0011\u001a\u00020\u000f2\u001a\b\u0002\u0010\u0012\u001a\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u00150\u0013\u001a&\u0010\u0016\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00022\b\b\u0002\u0010\u0011\u001a\u00020\u000f2\b\b\u0002\u0010\u0017\u001a\u00020\u0018\u001a\n\u0010\u0019\u001a\u00020\u000f*\u00020\u0002\u001a\u0012\u0010\u001a\u001a\u00020\u000f*\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u0002\u001a\u0012\u0010\u001a\u001a\u00020\u000f*\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u0001\u001a\n\u0010\u001c\u001a\u00020\u0002*\u00020\u0002\u001a\u001d\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00020\u001d*\b\u0012\u0004\u0012\u00020\u00020\u001dH\u0002¢\u0006\u0002\b\u001e\u001a\u0011\u0010\u001c\u001a\u00020\u001f*\u00020\u001fH\u0002¢\u0006\u0002\b\u001e\u001a\u0012\u0010 \u001a\u00020\u0002*\u00020\u00022\u0006\u0010!\u001a\u00020\u0002\u001a\u0014\u0010\"\u001a\u0004\u0018\u00010\u0002*\u00020\u00022\u0006\u0010!\u001a\u00020\u0002\u001a\u0012\u0010#\u001a\u00020\u0002*\u00020\u00022\u0006\u0010!\u001a\u00020\u0002\u001a\u0012\u0010$\u001a\u00020\u0002*\u00020\u00022\u0006\u0010%\u001a\u00020\u0002\u001a\u0012\u0010$\u001a\u00020\u0002*\u00020\u00022\u0006\u0010%\u001a\u00020\u0001\u001a\u0012\u0010&\u001a\u00020\u0002*\u00020\u00022\u0006\u0010%\u001a\u00020\u0002\u001a\u0012\u0010&\u001a\u00020\u0002*\u00020\u00022\u0006\u0010%\u001a\u00020\u0001\u001a\u0012\u0010'\u001a\u00020\u000f*\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u0002\u001a\u0012\u0010'\u001a\u00020\u000f*\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u0001\u001a\u0012\u0010(\u001a\u00020\u0001*\u00020\u00022\u0006\u0010!\u001a\u00020\u0002\u001a\u001b\u0010)\u001a\u0004\u0018\u00010\u0001*\u00020\u00022\u0006\u0010!\u001a\u00020\u0002H\u0002¢\u0006\u0002\b*\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004\"\u0015\u0010\u0005\u001a\u00020\u0001*\u00020\u00028F¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0004\"\u0015\u0010\u0007\u001a\u00020\u0001*\u00020\u00028F¢\u0006\u0006\u001a\u0004\b\b\u0010\u0004¨\u0006+" }, d2 = { "extension", "", "Ljava/io/File;", "getExtension", "(Ljava/io/File;)Ljava/lang/String;", "invariantSeparatorsPath", "getInvariantSeparatorsPath", "nameWithoutExtension", "getNameWithoutExtension", "createTempDir", "prefix", "suffix", "directory", "createTempFile", "copyRecursively", "", "target", "overwrite", "onError", "Lkotlin/Function2;", "Ljava/io/IOException;", "Lkotlin/io/OnErrorAction;", "copyTo", "bufferSize", "", "deleteRecursively", "endsWith", "other", "normalize", "", "normalize$FilesKt__UtilsKt", "Lkotlin/io/FilePathComponents;", "relativeTo", "base", "relativeToOrNull", "relativeToOrSelf", "resolve", "relative", "resolveSibling", "startsWith", "toRelativeString", "toRelativeStringOrNull", "toRelativeStringOrNull$FilesKt__UtilsKt", "kotlin-stdlib" }, k = 5, mv = { 1, 1, 15 }, xi = 1, xs = "kotlin/io/FilesKt")
class FilesKt__UtilsKt extends FilesKt__FileTreeWalkKt
{
    public FilesKt__UtilsKt() {
    }
    
    public static final boolean copyRecursively(final File file, final File parent, final boolean b, final Function2<? super File, ? super IOException, ? extends OnErrorAction> function2) {
        Intrinsics.checkParameterIsNotNull(file, "$this$copyRecursively");
        Intrinsics.checkParameterIsNotNull(parent, "target");
        Intrinsics.checkParameterIsNotNull(function2, "onError");
        if (!file.exists()) {
            return (OnErrorAction)function2.invoke(file, new NoSuchFileException(file, null, "The source file doesn't exist.", 2, null)) != OnErrorAction.TERMINATE;
        }
    Label_0217_Outer:
        while (true) {
        Label_0187_Outer:
            while (true) {
            Label_0339:
                while (true) {
                    Label_0335: {
                        try {
                        Label_0081:
                            for (final File file2 : FilesKt__FileTreeWalkKt.walkTopDown(file).onFail((Function2<? super File, ? super IOException, Unit>)new FilesKt__UtilsKt$copyRecursively.FilesKt__UtilsKt$copyRecursively$2((Function2)function2))) {
                                if (!file2.exists()) {
                                    if ((OnErrorAction)function2.invoke(file2, new NoSuchFileException(file2, null, "The source file doesn't exist.", 2, null)) == OnErrorAction.TERMINATE) {
                                        return false;
                                    }
                                    continue Label_0217_Outer;
                                }
                                else {
                                    final File file3 = new File(parent, toRelativeString(file2, file));
                                    if (file3.exists()) {
                                        if (!file2.isDirectory()) {
                                            break Label_0335;
                                        }
                                        if (!file3.isDirectory()) {
                                            break Label_0335;
                                        }
                                    }
                                    Label_0254: {
                                        if (file2.isDirectory()) {
                                            file3.mkdirs();
                                        }
                                        else {
                                            if (copyTo$default(file2, file3, b, 0, 4, null).length() != file2.length() && (OnErrorAction)function2.invoke(file2, new IOException("Source file wasn't copied completely, length of destination file differs.")) == OnErrorAction.TERMINATE) {
                                                return false;
                                            }
                                            continue Label_0217_Outer;
                                        }
                                    }
                                }
                            }
                            return true;
                            // iftrue(Label_0254:, n == 0)
                            // iftrue(Label_0081:, (OnErrorAction)function2.invoke(file3, (NoSuchFileException)new FileAlreadyExistsException(file2, file3, "The destination file already exists.")) != OnErrorAction.TERMINATE)
                            return false;
                            // iftrue(Label_0345:, file3.isDirectory() ? deleteRecursively(file3) : file3.delete())
                            break Label_0339;
                        }
                        catch (TerminateException ex) {
                            return false;
                        }
                    }
                    if (b) {
                        continue;
                    }
                    break;
                }
                int n = 1;
                continue Label_0187_Outer;
                Label_0345: {
                    n = 0;
                }
                continue Label_0187_Outer;
            }
        }
    }
    
    public static final File copyTo(File file, final File file2, final boolean b, final int n) {
        Intrinsics.checkParameterIsNotNull(file, "$this$copyTo");
        Intrinsics.checkParameterIsNotNull(file2, "target");
        if (file.exists()) {
            if (file2.exists()) {
                boolean b2 = true;
                if (b) {
                    if (file2.delete()) {
                        b2 = false;
                    }
                }
                if (b2) {
                    throw new FileAlreadyExistsException(file, file2, "The destination file already exists.");
                }
            }
            if (file.isDirectory()) {
                if (file2.mkdirs()) {
                    return file2;
                }
                throw new FileSystemException(file, file2, "Failed to create target directory.");
            }
            else {
                final File parentFile = file2.getParentFile();
                if (parentFile != null) {
                    parentFile.mkdirs();
                }
                file = (File)new FileInputStream(file);
                final Throwable t = null;
                try {
                    final FileInputStream fileInputStream = (FileInputStream)file;
                    final FileOutputStream fileOutputStream = new FileOutputStream(file2);
                    final Throwable t2 = null;
                    try {
                        ByteStreamsKt.copyTo(fileInputStream, fileOutputStream, n);
                        CloseableKt.closeFinally(fileOutputStream, t2);
                        CloseableKt.closeFinally((Closeable)file, t);
                        return file2;
                    }
                    finally {
                        try {}
                        finally {
                            CloseableKt.closeFinally(fileOutputStream, (Throwable)file2);
                        }
                    }
                }
                finally {
                    try {}
                    finally {
                        CloseableKt.closeFinally((Closeable)file, (Throwable)file2);
                    }
                }
            }
        }
        throw new NoSuchFileException(file, null, "The source file doesn't exist.", 2, null);
    }
    
    public static /* synthetic */ File copyTo$default(final File file, final File file2, boolean b, int n, final int n2, final Object o) {
        if ((n2 & 0x2) != 0x0) {
            b = false;
        }
        if ((n2 & 0x4) != 0x0) {
            n = 8192;
        }
        return copyTo(file, file2, b, n);
    }
    
    public static final File createTempDir(final String prefix, final String suffix, final File directory) {
        Intrinsics.checkParameterIsNotNull(prefix, "prefix");
        final File tempFile = File.createTempFile(prefix, suffix, directory);
        tempFile.delete();
        if (tempFile.mkdir()) {
            Intrinsics.checkExpressionValueIsNotNull(tempFile, "dir");
            return tempFile;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unable to create temporary directory ");
        sb.append(tempFile);
        sb.append('.');
        throw new IOException(sb.toString());
    }
    
    public static final File createTempFile(final String prefix, final String suffix, final File directory) {
        Intrinsics.checkParameterIsNotNull(prefix, "prefix");
        final File tempFile = File.createTempFile(prefix, suffix, directory);
        Intrinsics.checkExpressionValueIsNotNull(tempFile, "File.createTempFile(prefix, suffix, directory)");
        return tempFile;
    }
    
    public static final boolean deleteRecursively(final File file) {
        Intrinsics.checkParameterIsNotNull(file, "$this$deleteRecursively");
        final Iterator<File> iterator = FilesKt__FileTreeWalkKt.walkBottomUp(file).iterator();
        boolean b = false;
    Label_0020:
        while (true) {
            b = true;
            while (iterator.hasNext()) {
                final File file2 = iterator.next();
                if ((file2.delete() || !file2.exists()) && b) {
                    continue Label_0020;
                }
                b = false;
            }
            break;
        }
        return b;
    }
    
    public static final boolean endsWith(final File file, final File file2) {
        Intrinsics.checkParameterIsNotNull(file, "$this$endsWith");
        Intrinsics.checkParameterIsNotNull(file2, "other");
        final FilePathComponents components = FilesKt__FilePathComponentsKt.toComponents(file);
        final FilePathComponents components2 = FilesKt__FilePathComponentsKt.toComponents(file2);
        if (components2.isRooted()) {
            return Intrinsics.areEqual(file, file2);
        }
        final int n = components.getSize() - components2.getSize();
        return n >= 0 && components.getSegments().subList(n, components.getSize()).equals(components2.getSegments());
    }
    
    public static final boolean endsWith(final File file, final String pathname) {
        Intrinsics.checkParameterIsNotNull(file, "$this$endsWith");
        Intrinsics.checkParameterIsNotNull(pathname, "other");
        return endsWith(file, new File(pathname));
    }
    
    public static final String getExtension(final File file) {
        Intrinsics.checkParameterIsNotNull(file, "$this$extension");
        final String name = file.getName();
        Intrinsics.checkExpressionValueIsNotNull(name, "name");
        return StringsKt__StringsKt.substringAfterLast(name, '.', "");
    }
    
    public static final String getInvariantSeparatorsPath(final File file) {
        Intrinsics.checkParameterIsNotNull(file, "$this$invariantSeparatorsPath");
        if (File.separatorChar != '/') {
            final String path = file.getPath();
            Intrinsics.checkExpressionValueIsNotNull(path, "path");
            return StringsKt__StringsJVMKt.replace$default(path, File.separatorChar, '/', false, 4, null);
        }
        final String path2 = file.getPath();
        Intrinsics.checkExpressionValueIsNotNull(path2, "path");
        return path2;
    }
    
    public static final String getNameWithoutExtension(final File file) {
        Intrinsics.checkParameterIsNotNull(file, "$this$nameWithoutExtension");
        final String name = file.getName();
        Intrinsics.checkExpressionValueIsNotNull(name, "name");
        return StringsKt__StringsKt.substringBeforeLast$default(name, ".", null, 2, null);
    }
    
    public static final File normalize(File root) {
        Intrinsics.checkParameterIsNotNull(root, "$this$normalize");
        final FilePathComponents components = FilesKt__FilePathComponentsKt.toComponents(root);
        root = components.getRoot();
        final List<File> list = normalize$FilesKt__UtilsKt(components.getSegments());
        final String separator = File.separator;
        Intrinsics.checkExpressionValueIsNotNull(separator, "File.separator");
        return resolve(root, CollectionsKt___CollectionsKt.joinToString$default(list, separator, null, null, 0, null, null, 62, null));
    }
    
    private static final List<File> normalize$FilesKt__UtilsKt(final List<? extends File> list) {
        final ArrayList<Object> list2 = new ArrayList<Object>(list.size());
        for (final File file : list) {
            final String name = file.getName();
            if (name != null) {
                final int hashCode = name.hashCode();
                if (hashCode != 46) {
                    if (hashCode == 1472) {
                        if (name.equals("..")) {
                            if (!list2.isEmpty() && (Intrinsics.areEqual(((File)CollectionsKt___CollectionsKt.last((List<?>)list2)).getName(), "..") ^ true)) {
                                list2.remove(list2.size() - 1);
                                continue;
                            }
                            list2.add(file);
                            continue;
                        }
                    }
                }
                else if (name.equals(".")) {
                    continue;
                }
            }
            list2.add(file);
        }
        return (List<File>)list2;
    }
    
    private static final FilePathComponents normalize$FilesKt__UtilsKt(final FilePathComponents filePathComponents) {
        return new FilePathComponents(filePathComponents.getRoot(), normalize$FilesKt__UtilsKt(filePathComponents.getSegments()));
    }
    
    public static final File relativeTo(final File file, final File file2) {
        Intrinsics.checkParameterIsNotNull(file, "$this$relativeTo");
        Intrinsics.checkParameterIsNotNull(file2, "base");
        return new File(toRelativeString(file, file2));
    }
    
    public static final File relativeToOrNull(final File file, final File file2) {
        Intrinsics.checkParameterIsNotNull(file, "$this$relativeToOrNull");
        Intrinsics.checkParameterIsNotNull(file2, "base");
        final String relativeStringOrNull$FilesKt__UtilsKt = toRelativeStringOrNull$FilesKt__UtilsKt(file, file2);
        if (relativeStringOrNull$FilesKt__UtilsKt != null) {
            return new File(relativeStringOrNull$FilesKt__UtilsKt);
        }
        return null;
    }
    
    public static final File relativeToOrSelf(File file, final File file2) {
        Intrinsics.checkParameterIsNotNull(file, "$this$relativeToOrSelf");
        Intrinsics.checkParameterIsNotNull(file2, "base");
        final String relativeStringOrNull$FilesKt__UtilsKt = toRelativeStringOrNull$FilesKt__UtilsKt(file, file2);
        if (relativeStringOrNull$FilesKt__UtilsKt != null) {
            file = new File(relativeStringOrNull$FilesKt__UtilsKt);
        }
        return file;
    }
    
    public static final File resolve(final File file, final File file2) {
        Intrinsics.checkParameterIsNotNull(file, "$this$resolve");
        Intrinsics.checkParameterIsNotNull(file2, "relative");
        if (FilesKt__FilePathComponentsKt.isRooted(file2)) {
            return file2;
        }
        final String string = file.toString();
        Intrinsics.checkExpressionValueIsNotNull(string, "this.toString()");
        final String s = string;
        if (s.length() != 0 && !StringsKt__StringsKt.endsWith$default(s, File.separatorChar, false, 2, null)) {
            final StringBuilder sb = new StringBuilder();
            sb.append(string);
            sb.append(File.separatorChar);
            sb.append(file2);
            return new File(sb.toString());
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(string);
        sb2.append(file2);
        return new File(sb2.toString());
    }
    
    public static final File resolve(final File file, final String pathname) {
        Intrinsics.checkParameterIsNotNull(file, "$this$resolve");
        Intrinsics.checkParameterIsNotNull(pathname, "relative");
        return resolve(file, new File(pathname));
    }
    
    public static final File resolveSibling(File subPath, final File file) {
        Intrinsics.checkParameterIsNotNull(subPath, "$this$resolveSibling");
        Intrinsics.checkParameterIsNotNull(file, "relative");
        final FilePathComponents components = FilesKt__FilePathComponentsKt.toComponents(subPath);
        if (components.getSize() == 0) {
            subPath = new File("..");
        }
        else {
            subPath = components.subPath(0, components.getSize() - 1);
        }
        return resolve(resolve(components.getRoot(), subPath), file);
    }
    
    public static final File resolveSibling(final File file, final String pathname) {
        Intrinsics.checkParameterIsNotNull(file, "$this$resolveSibling");
        Intrinsics.checkParameterIsNotNull(pathname, "relative");
        return resolveSibling(file, new File(pathname));
    }
    
    public static final boolean startsWith(final File file, final File file2) {
        Intrinsics.checkParameterIsNotNull(file, "$this$startsWith");
        Intrinsics.checkParameterIsNotNull(file2, "other");
        final FilePathComponents components = FilesKt__FilePathComponentsKt.toComponents(file);
        final FilePathComponents components2 = FilesKt__FilePathComponentsKt.toComponents(file2);
        return !(Intrinsics.areEqual(components.getRoot(), components2.getRoot()) ^ true) && components.getSize() >= components2.getSize() && components.getSegments().subList(0, components2.getSize()).equals(components2.getSegments());
    }
    
    public static final boolean startsWith(final File file, final String pathname) {
        Intrinsics.checkParameterIsNotNull(file, "$this$startsWith");
        Intrinsics.checkParameterIsNotNull(pathname, "other");
        return startsWith(file, new File(pathname));
    }
    
    public static final String toRelativeString(final File obj, final File obj2) {
        Intrinsics.checkParameterIsNotNull(obj, "$this$toRelativeString");
        Intrinsics.checkParameterIsNotNull(obj2, "base");
        final String relativeStringOrNull$FilesKt__UtilsKt = toRelativeStringOrNull$FilesKt__UtilsKt(obj, obj2);
        if (relativeStringOrNull$FilesKt__UtilsKt != null) {
            return relativeStringOrNull$FilesKt__UtilsKt;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("this and base files have different roots: ");
        sb.append(obj);
        sb.append(" and ");
        sb.append(obj2);
        sb.append('.');
        throw new IllegalArgumentException(sb.toString());
    }
    
    private static final String toRelativeStringOrNull$FilesKt__UtilsKt(final File file, final File file2) {
        final FilePathComponents normalize$FilesKt__UtilsKt = normalize$FilesKt__UtilsKt(FilesKt__FilePathComponentsKt.toComponents(file));
        final FilePathComponents normalize$FilesKt__UtilsKt2 = normalize$FilesKt__UtilsKt(FilesKt__FilePathComponentsKt.toComponents(file2));
        if (Intrinsics.areEqual(normalize$FilesKt__UtilsKt.getRoot(), normalize$FilesKt__UtilsKt2.getRoot()) ^ true) {
            return null;
        }
        int size;
        int size2;
        int n;
        for (size = normalize$FilesKt__UtilsKt2.getSize(), size2 = normalize$FilesKt__UtilsKt.getSize(), n = 0; n < Math.min(size2, size) && Intrinsics.areEqual(normalize$FilesKt__UtilsKt.getSegments().get(n), normalize$FilesKt__UtilsKt2.getSegments().get(n)); ++n) {}
        final StringBuilder sb = new StringBuilder();
        int n2 = size - 1;
        Label_0182: {
            if (n2 >= n) {
                while (!Intrinsics.areEqual(normalize$FilesKt__UtilsKt2.getSegments().get(n2).getName(), "..")) {
                    sb.append("..");
                    if (n2 != n) {
                        sb.append(File.separatorChar);
                    }
                    if (n2 == n) {
                        break Label_0182;
                    }
                    --n2;
                }
                return null;
            }
        }
        if (n < size2) {
            if (n < size) {
                sb.append(File.separatorChar);
            }
            final List<Object> list = CollectionsKt___CollectionsKt.drop((Iterable<?>)normalize$FilesKt__UtilsKt.getSegments(), n);
            final StringBuilder sb2 = sb;
            final String separator = File.separator;
            Intrinsics.checkExpressionValueIsNotNull(separator, "File.separator");
            CollectionsKt___CollectionsKt.joinTo$default(list, sb2, separator, null, null, 0, null, null, 124, null);
        }
        return sb.toString();
    }
}
