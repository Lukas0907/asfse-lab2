// 
// Decompiled by Procyon v0.5.36
// 

package kotlin;

import java.math.MathContext;
import java.math.BigDecimal;
import kotlin.jvm.internal.Intrinsics;
import java.math.BigInteger;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000(\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\u001a\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0087\f\u001a\r\u0010\u0003\u001a\u00020\u0001*\u00020\u0001H\u0087\n\u001a\u0015\u0010\u0004\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0087\n\u001a\r\u0010\u0005\u001a\u00020\u0001*\u00020\u0001H\u0087\n\u001a\r\u0010\u0006\u001a\u00020\u0001*\u00020\u0001H\u0087\b\u001a\u0015\u0010\u0007\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0087\n\u001a\u0015\u0010\b\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0087\f\u001a\u0015\u0010\t\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0087\n\u001a\u0015\u0010\n\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0087\n\u001a\u0015\u0010\u000b\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\f\u001a\u00020\rH\u0087\f\u001a\u0015\u0010\u000e\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\f\u001a\u00020\rH\u0087\f\u001a\u0015\u0010\u000f\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0087\n\u001a\r\u0010\u0010\u001a\u00020\u0011*\u00020\u0001H\u0087\b\u001a!\u0010\u0010\u001a\u00020\u0011*\u00020\u00012\b\b\u0002\u0010\u0012\u001a\u00020\r2\b\b\u0002\u0010\u0013\u001a\u00020\u0014H\u0087\b\u001a\r\u0010\u0015\u001a\u00020\u0001*\u00020\rH\u0087\b\u001a\r\u0010\u0015\u001a\u00020\u0001*\u00020\u0016H\u0087\b\u001a\r\u0010\u0017\u001a\u00020\u0001*\u00020\u0001H\u0087\n\u001a\u0015\u0010\u0018\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0087\f¨\u0006\u0019" }, d2 = { "and", "Ljava/math/BigInteger;", "other", "dec", "div", "inc", "inv", "minus", "or", "plus", "rem", "shl", "n", "", "shr", "times", "toBigDecimal", "Ljava/math/BigDecimal;", "scale", "mathContext", "Ljava/math/MathContext;", "toBigInteger", "", "unaryMinus", "xor", "kotlin-stdlib" }, k = 5, mv = { 1, 1, 15 }, xi = 1, xs = "kotlin/NumbersKt")
class NumbersKt__BigIntegersKt extends NumbersKt__BigDecimalsKt
{
    public NumbersKt__BigIntegersKt() {
    }
    
    private static final BigInteger and(BigInteger and, final BigInteger val) {
        and = and.and(val);
        Intrinsics.checkExpressionValueIsNotNull(and, "this.and(other)");
        return and;
    }
    
    private static final BigInteger dec(BigInteger subtract) {
        Intrinsics.checkParameterIsNotNull(subtract, "$this$dec");
        subtract = subtract.subtract(BigInteger.ONE);
        Intrinsics.checkExpressionValueIsNotNull(subtract, "this.subtract(BigInteger.ONE)");
        return subtract;
    }
    
    private static final BigInteger div(BigInteger divide, final BigInteger val) {
        Intrinsics.checkParameterIsNotNull(divide, "$this$div");
        divide = divide.divide(val);
        Intrinsics.checkExpressionValueIsNotNull(divide, "this.divide(other)");
        return divide;
    }
    
    private static final BigInteger inc(BigInteger add) {
        Intrinsics.checkParameterIsNotNull(add, "$this$inc");
        add = add.add(BigInteger.ONE);
        Intrinsics.checkExpressionValueIsNotNull(add, "this.add(BigInteger.ONE)");
        return add;
    }
    
    private static final BigInteger inv(BigInteger not) {
        not = not.not();
        Intrinsics.checkExpressionValueIsNotNull(not, "this.not()");
        return not;
    }
    
    private static final BigInteger minus(BigInteger subtract, final BigInteger val) {
        Intrinsics.checkParameterIsNotNull(subtract, "$this$minus");
        subtract = subtract.subtract(val);
        Intrinsics.checkExpressionValueIsNotNull(subtract, "this.subtract(other)");
        return subtract;
    }
    
    private static final BigInteger or(BigInteger or, final BigInteger val) {
        or = or.or(val);
        Intrinsics.checkExpressionValueIsNotNull(or, "this.or(other)");
        return or;
    }
    
    private static final BigInteger plus(BigInteger add, final BigInteger val) {
        Intrinsics.checkParameterIsNotNull(add, "$this$plus");
        add = add.add(val);
        Intrinsics.checkExpressionValueIsNotNull(add, "this.add(other)");
        return add;
    }
    
    private static final BigInteger rem(BigInteger remainder, final BigInteger val) {
        Intrinsics.checkParameterIsNotNull(remainder, "$this$rem");
        remainder = remainder.remainder(val);
        Intrinsics.checkExpressionValueIsNotNull(remainder, "this.remainder(other)");
        return remainder;
    }
    
    private static final BigInteger shl(BigInteger shiftLeft, final int n) {
        shiftLeft = shiftLeft.shiftLeft(n);
        Intrinsics.checkExpressionValueIsNotNull(shiftLeft, "this.shiftLeft(n)");
        return shiftLeft;
    }
    
    private static final BigInteger shr(BigInteger shiftRight, final int n) {
        shiftRight = shiftRight.shiftRight(n);
        Intrinsics.checkExpressionValueIsNotNull(shiftRight, "this.shiftRight(n)");
        return shiftRight;
    }
    
    private static final BigInteger times(BigInteger multiply, final BigInteger val) {
        Intrinsics.checkParameterIsNotNull(multiply, "$this$times");
        multiply = multiply.multiply(val);
        Intrinsics.checkExpressionValueIsNotNull(multiply, "this.multiply(other)");
        return multiply;
    }
    
    private static final BigDecimal toBigDecimal(final BigInteger val) {
        return new BigDecimal(val);
    }
    
    private static final BigDecimal toBigDecimal(final BigInteger unscaledVal, final int scale, final MathContext mc) {
        return new BigDecimal(unscaledVal, scale, mc);
    }
    
    private static final BigInteger toBigInteger(final int n) {
        final BigInteger value = BigInteger.valueOf(n);
        Intrinsics.checkExpressionValueIsNotNull(value, "BigInteger.valueOf(this.toLong())");
        return value;
    }
    
    private static final BigInteger toBigInteger(final long val) {
        final BigInteger value = BigInteger.valueOf(val);
        Intrinsics.checkExpressionValueIsNotNull(value, "BigInteger.valueOf(this)");
        return value;
    }
    
    private static final BigInteger unaryMinus(BigInteger negate) {
        Intrinsics.checkParameterIsNotNull(negate, "$this$unaryMinus");
        negate = negate.negate();
        Intrinsics.checkExpressionValueIsNotNull(negate, "this.negate()");
        return negate;
    }
    
    private static final BigInteger xor(BigInteger xor, final BigInteger val) {
        xor = xor.xor(val);
        Intrinsics.checkExpressionValueIsNotNull(xor, "this.xor(other)");
        return xor;
    }
}
