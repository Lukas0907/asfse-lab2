// 
// Decompiled by Procyon v0.5.36
// 

package kotlin;

import kotlin.jvm.internal.FloatCompanionObject;
import kotlin.jvm.internal.DoubleCompanionObject;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000*\n\u0000\n\u0002\u0010\b\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\u001a\r\u0010\u0000\u001a\u00020\u0001*\u00020\u0001H\u0087\b\u001a\r\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0087\b\u001a\r\u0010\u0003\u001a\u00020\u0001*\u00020\u0001H\u0087\b\u001a\r\u0010\u0003\u001a\u00020\u0001*\u00020\u0002H\u0087\b\u001a\r\u0010\u0004\u001a\u00020\u0001*\u00020\u0001H\u0087\b\u001a\r\u0010\u0004\u001a\u00020\u0001*\u00020\u0002H\u0087\b\u001a\u0015\u0010\u0005\u001a\u00020\u0006*\u00020\u00072\u0006\u0010\b\u001a\u00020\u0002H\u0087\b\u001a\u0015\u0010\u0005\u001a\u00020\t*\u00020\n2\u0006\u0010\b\u001a\u00020\u0001H\u0087\b\u001a\r\u0010\u000b\u001a\u00020\f*\u00020\u0006H\u0087\b\u001a\r\u0010\u000b\u001a\u00020\f*\u00020\tH\u0087\b\u001a\r\u0010\r\u001a\u00020\f*\u00020\u0006H\u0087\b\u001a\r\u0010\r\u001a\u00020\f*\u00020\tH\u0087\b\u001a\r\u0010\u000e\u001a\u00020\f*\u00020\u0006H\u0087\b\u001a\r\u0010\u000e\u001a\u00020\f*\u00020\tH\u0087\b\u001a\u0015\u0010\u000f\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0010\u001a\u00020\u0001H\u0087\b\u001a\u0015\u0010\u000f\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u0001H\u0087\b\u001a\u0015\u0010\u0011\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0010\u001a\u00020\u0001H\u0087\b\u001a\u0015\u0010\u0011\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u0001H\u0087\b\u001a\r\u0010\u0012\u001a\u00020\u0001*\u00020\u0001H\u0087\b\u001a\r\u0010\u0012\u001a\u00020\u0002*\u00020\u0002H\u0087\b\u001a\r\u0010\u0013\u001a\u00020\u0001*\u00020\u0001H\u0087\b\u001a\r\u0010\u0013\u001a\u00020\u0002*\u00020\u0002H\u0087\b\u001a\r\u0010\u0014\u001a\u00020\u0002*\u00020\u0006H\u0087\b\u001a\r\u0010\u0014\u001a\u00020\u0001*\u00020\tH\u0087\b\u001a\r\u0010\u0015\u001a\u00020\u0002*\u00020\u0006H\u0087\b\u001a\r\u0010\u0015\u001a\u00020\u0001*\u00020\tH\u0087\b¨\u0006\u0016" }, d2 = { "countLeadingZeroBits", "", "", "countOneBits", "countTrailingZeroBits", "fromBits", "", "Lkotlin/Double$Companion;", "bits", "", "Lkotlin/Float$Companion;", "isFinite", "", "isInfinite", "isNaN", "rotateLeft", "bitCount", "rotateRight", "takeHighestOneBit", "takeLowestOneBit", "toBits", "toRawBits", "kotlin-stdlib" }, k = 5, mv = { 1, 1, 15 }, xi = 1, xs = "kotlin/NumbersKt")
class NumbersKt__NumbersJVMKt extends NumbersKt__BigIntegersKt
{
    public NumbersKt__NumbersJVMKt() {
    }
    
    private static final int countLeadingZeroBits(final int i) {
        return Integer.numberOfLeadingZeros(i);
    }
    
    private static final int countLeadingZeroBits(final long i) {
        return Long.numberOfLeadingZeros(i);
    }
    
    private static final int countOneBits(final int i) {
        return Integer.bitCount(i);
    }
    
    private static final int countOneBits(final long i) {
        return Long.bitCount(i);
    }
    
    private static final int countTrailingZeroBits(final int i) {
        return Integer.numberOfTrailingZeros(i);
    }
    
    private static final int countTrailingZeroBits(final long i) {
        return Long.numberOfTrailingZeros(i);
    }
    
    private static final double fromBits(final DoubleCompanionObject doubleCompanionObject, final long n) {
        return Double.longBitsToDouble(n);
    }
    
    private static final float fromBits(final FloatCompanionObject floatCompanionObject, final int n) {
        return Float.intBitsToFloat(n);
    }
    
    private static final boolean isFinite(final double n) {
        return !Double.isInfinite(n) && !Double.isNaN(n);
    }
    
    private static final boolean isFinite(final float n) {
        return !Float.isInfinite(n) && !Float.isNaN(n);
    }
    
    private static final boolean isInfinite(final double v) {
        return Double.isInfinite(v);
    }
    
    private static final boolean isInfinite(final float v) {
        return Float.isInfinite(v);
    }
    
    private static final boolean isNaN(final double v) {
        return Double.isNaN(v);
    }
    
    private static final boolean isNaN(final float v) {
        return Float.isNaN(v);
    }
    
    private static final int rotateLeft(final int i, final int distance) {
        return Integer.rotateLeft(i, distance);
    }
    
    private static final long rotateLeft(final long i, final int distance) {
        return Long.rotateLeft(i, distance);
    }
    
    private static final int rotateRight(final int i, final int distance) {
        return Integer.rotateRight(i, distance);
    }
    
    private static final long rotateRight(final long i, final int distance) {
        return Long.rotateRight(i, distance);
    }
    
    private static final int takeHighestOneBit(final int i) {
        return Integer.highestOneBit(i);
    }
    
    private static final long takeHighestOneBit(final long i) {
        return Long.highestOneBit(i);
    }
    
    private static final int takeLowestOneBit(final int i) {
        return Integer.lowestOneBit(i);
    }
    
    private static final long takeLowestOneBit(final long i) {
        return Long.lowestOneBit(i);
    }
    
    private static final int toBits(final float value) {
        return Float.floatToIntBits(value);
    }
    
    private static final long toBits(final double value) {
        return Double.doubleToLongBits(value);
    }
    
    private static final int toRawBits(final float n) {
        return Float.floatToRawIntBits(n);
    }
    
    private static final long toRawBits(final double n) {
        return Double.doubleToRawLongBits(n);
    }
}
