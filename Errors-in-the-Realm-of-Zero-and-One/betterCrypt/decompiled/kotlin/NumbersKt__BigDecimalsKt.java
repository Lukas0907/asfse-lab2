// 
// Decompiled by Procyon v0.5.36
// 

package kotlin;

import java.math.MathContext;
import java.math.RoundingMode;
import kotlin.jvm.internal.Intrinsics;
import java.math.BigDecimal;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0006\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0007\n\u0002\u0010\b\n\u0002\u0010\t\n\u0002\b\u0002\u001a\r\u0010\u0000\u001a\u00020\u0001*\u00020\u0001H\u0087\n\u001a\u0015\u0010\u0002\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0001H\u0087\n\u001a\r\u0010\u0004\u001a\u00020\u0001*\u00020\u0001H\u0087\n\u001a\u0015\u0010\u0005\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0001H\u0087\n\u001a\u0015\u0010\u0006\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0001H\u0087\n\u001a\u0015\u0010\u0007\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0001H\u0087\n\u001a\u0015\u0010\b\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0001H\u0087\n\u001a\u0015\u0010\t\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0001H\u0087\n\u001a\r\u0010\n\u001a\u00020\u0001*\u00020\u000bH\u0087\b\u001a\u0015\u0010\n\u001a\u00020\u0001*\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0087\b\u001a\r\u0010\n\u001a\u00020\u0001*\u00020\u000eH\u0087\b\u001a\u0015\u0010\n\u001a\u00020\u0001*\u00020\u000e2\u0006\u0010\f\u001a\u00020\rH\u0087\b\u001a\r\u0010\n\u001a\u00020\u0001*\u00020\u000fH\u0087\b\u001a\u0015\u0010\n\u001a\u00020\u0001*\u00020\u000f2\u0006\u0010\f\u001a\u00020\rH\u0087\b\u001a\r\u0010\n\u001a\u00020\u0001*\u00020\u0010H\u0087\b\u001a\u0015\u0010\n\u001a\u00020\u0001*\u00020\u00102\u0006\u0010\f\u001a\u00020\rH\u0087\b\u001a\r\u0010\u0011\u001a\u00020\u0001*\u00020\u0001H\u0087\n¨\u0006\u0012" }, d2 = { "dec", "Ljava/math/BigDecimal;", "div", "other", "inc", "minus", "mod", "plus", "rem", "times", "toBigDecimal", "", "mathContext", "Ljava/math/MathContext;", "", "", "", "unaryMinus", "kotlin-stdlib" }, k = 5, mv = { 1, 1, 15 }, xi = 1, xs = "kotlin/NumbersKt")
class NumbersKt__BigDecimalsKt
{
    public NumbersKt__BigDecimalsKt() {
    }
    
    private static final BigDecimal dec(BigDecimal subtract) {
        Intrinsics.checkParameterIsNotNull(subtract, "$this$dec");
        subtract = subtract.subtract(BigDecimal.ONE);
        Intrinsics.checkExpressionValueIsNotNull(subtract, "this.subtract(BigDecimal.ONE)");
        return subtract;
    }
    
    private static final BigDecimal div(BigDecimal divide, final BigDecimal divisor) {
        Intrinsics.checkParameterIsNotNull(divide, "$this$div");
        divide = divide.divide(divisor, RoundingMode.HALF_EVEN);
        Intrinsics.checkExpressionValueIsNotNull(divide, "this.divide(other, RoundingMode.HALF_EVEN)");
        return divide;
    }
    
    private static final BigDecimal inc(BigDecimal add) {
        Intrinsics.checkParameterIsNotNull(add, "$this$inc");
        add = add.add(BigDecimal.ONE);
        Intrinsics.checkExpressionValueIsNotNull(add, "this.add(BigDecimal.ONE)");
        return add;
    }
    
    private static final BigDecimal minus(BigDecimal subtract, final BigDecimal subtrahend) {
        Intrinsics.checkParameterIsNotNull(subtract, "$this$minus");
        subtract = subtract.subtract(subtrahend);
        Intrinsics.checkExpressionValueIsNotNull(subtract, "this.subtract(other)");
        return subtract;
    }
    
    @Deprecated(level = DeprecationLevel.ERROR, message = "Use rem(other) instead", replaceWith = @ReplaceWith(expression = "rem(other)", imports = {}))
    private static final BigDecimal mod(BigDecimal remainder, final BigDecimal divisor) {
        Intrinsics.checkParameterIsNotNull(remainder, "$this$mod");
        remainder = remainder.remainder(divisor);
        Intrinsics.checkExpressionValueIsNotNull(remainder, "this.remainder(other)");
        return remainder;
    }
    
    private static final BigDecimal plus(BigDecimal add, final BigDecimal augend) {
        Intrinsics.checkParameterIsNotNull(add, "$this$plus");
        add = add.add(augend);
        Intrinsics.checkExpressionValueIsNotNull(add, "this.add(other)");
        return add;
    }
    
    private static final BigDecimal rem(BigDecimal remainder, final BigDecimal divisor) {
        Intrinsics.checkParameterIsNotNull(remainder, "$this$rem");
        remainder = remainder.remainder(divisor);
        Intrinsics.checkExpressionValueIsNotNull(remainder, "this.remainder(other)");
        return remainder;
    }
    
    private static final BigDecimal times(BigDecimal multiply, final BigDecimal multiplicand) {
        Intrinsics.checkParameterIsNotNull(multiply, "$this$times");
        multiply = multiply.multiply(multiplicand);
        Intrinsics.checkExpressionValueIsNotNull(multiply, "this.multiply(other)");
        return multiply;
    }
    
    private static final BigDecimal toBigDecimal(final double d) {
        return new BigDecimal(String.valueOf(d));
    }
    
    private static final BigDecimal toBigDecimal(final double d, final MathContext mc) {
        return new BigDecimal(String.valueOf(d), mc);
    }
    
    private static final BigDecimal toBigDecimal(final float f) {
        return new BigDecimal(String.valueOf(f));
    }
    
    private static final BigDecimal toBigDecimal(final float f, final MathContext mc) {
        return new BigDecimal(String.valueOf(f), mc);
    }
    
    private static final BigDecimal toBigDecimal(final int n) {
        final BigDecimal value = BigDecimal.valueOf(n);
        Intrinsics.checkExpressionValueIsNotNull(value, "BigDecimal.valueOf(this.toLong())");
        return value;
    }
    
    private static final BigDecimal toBigDecimal(final int val, final MathContext mc) {
        return new BigDecimal(val, mc);
    }
    
    private static final BigDecimal toBigDecimal(final long val) {
        final BigDecimal value = BigDecimal.valueOf(val);
        Intrinsics.checkExpressionValueIsNotNull(value, "BigDecimal.valueOf(this)");
        return value;
    }
    
    private static final BigDecimal toBigDecimal(final long val, final MathContext mc) {
        return new BigDecimal(val, mc);
    }
    
    private static final BigDecimal unaryMinus(BigDecimal negate) {
        Intrinsics.checkParameterIsNotNull(negate, "$this$unaryMinus");
        negate = negate.negate();
        Intrinsics.checkExpressionValueIsNotNull(negate, "this.negate()");
        return negate;
    }
}
