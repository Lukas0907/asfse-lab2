// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.sequences;

import kotlin.UShort;
import kotlin.ULong;
import java.util.Iterator;
import kotlin.UInt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.UByte;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u001c\u0010\u0000\u001a\u00020\u0001*\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005\u001a\u001c\u0010\u0000\u001a\u00020\u0001*\b\u0012\u0004\u0012\u00020\u00010\u0002H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0006\u0010\u0005\u001a\u001c\u0010\u0000\u001a\u00020\u0007*\b\u0012\u0004\u0012\u00020\u00070\u0002H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\b\u0010\t\u001a\u001c\u0010\u0000\u001a\u00020\u0001*\b\u0012\u0004\u0012\u00020\n0\u0002H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u000b\u0010\u0005\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\f" }, d2 = { "sum", "Lkotlin/UInt;", "Lkotlin/sequences/Sequence;", "Lkotlin/UByte;", "sumOfUByte", "(Lkotlin/sequences/Sequence;)I", "sumOfUInt", "Lkotlin/ULong;", "sumOfULong", "(Lkotlin/sequences/Sequence;)J", "Lkotlin/UShort;", "sumOfUShort", "kotlin-stdlib" }, k = 5, mv = { 1, 1, 15 }, xi = 1, xs = "kotlin/sequences/USequencesKt")
class USequencesKt___USequencesKt
{
    public USequencesKt___USequencesKt() {
    }
    
    public static final int sumOfUByte(final Sequence<UByte> sequence) {
        Intrinsics.checkParameterIsNotNull(sequence, "$this$sum");
        final Iterator<UByte> iterator = sequence.iterator();
        int constructor-impl = 0;
        while (iterator.hasNext()) {
            constructor-impl = UInt.constructor-impl(constructor-impl + UInt.constructor-impl(iterator.next().unbox-impl() & 0xFF));
        }
        return constructor-impl;
    }
    
    public static final int sumOfUInt(final Sequence<UInt> sequence) {
        Intrinsics.checkParameterIsNotNull(sequence, "$this$sum");
        final Iterator<UInt> iterator = sequence.iterator();
        int constructor-impl = 0;
        while (iterator.hasNext()) {
            constructor-impl = UInt.constructor-impl(constructor-impl + iterator.next().unbox-impl());
        }
        return constructor-impl;
    }
    
    public static final long sumOfULong(final Sequence<ULong> sequence) {
        Intrinsics.checkParameterIsNotNull(sequence, "$this$sum");
        final Iterator<ULong> iterator = sequence.iterator();
        long constructor-impl = 0L;
        while (iterator.hasNext()) {
            constructor-impl = ULong.constructor-impl(constructor-impl + iterator.next().unbox-impl());
        }
        return constructor-impl;
    }
    
    public static final int sumOfUShort(final Sequence<UShort> sequence) {
        Intrinsics.checkParameterIsNotNull(sequence, "$this$sum");
        final Iterator<UShort> iterator = sequence.iterator();
        int constructor-impl = 0;
        while (iterator.hasNext()) {
            constructor-impl = UInt.constructor-impl(constructor-impl + UInt.constructor-impl(iterator.next().unbox-impl() & 0xFFFF));
        }
        return constructor-impl;
    }
}
