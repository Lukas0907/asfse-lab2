// 
// Decompiled by Procyon v0.5.36
// 

package kotlin;

import java.util.NoSuchElementException;
import kotlin.jvm.internal.CollectionToArray;
import kotlin.collections.UShortIterator;
import java.util.Arrays;
import java.util.Iterator;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.markers.KMappedMarker;
import java.util.Collection;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0017\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\u0000\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0087@\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001-B\u0014\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00f8\u0001\u0000¢\u0006\u0004\b\u0005\u0010\u0006B\u0014\b\u0001\u0012\u0006\u0010\u0007\u001a\u00020\b\u00f8\u0001\u0000¢\u0006\u0004\b\u0005\u0010\tJ\u001b\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0002H\u0096\u0002\u00f8\u0001\u0000¢\u0006\u0004\b\u0011\u0010\u0012J \u0010\u0013\u001a\u00020\u000f2\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001H\u0016\u00f8\u0001\u0000¢\u0006\u0004\b\u0015\u0010\u0016J\u0013\u0010\u0017\u001a\u00020\u000f2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00d6\u0003J\u001b\u0010\u001a\u001a\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u0004H\u0086\u0002\u00f8\u0001\u0000¢\u0006\u0004\b\u001c\u0010\u001dJ\t\u0010\u001e\u001a\u00020\u0004H\u00d6\u0001J\u000f\u0010\u001f\u001a\u00020\u000fH\u0016¢\u0006\u0004\b \u0010!J\u0010\u0010\"\u001a\u00020#H\u0096\u0002¢\u0006\u0004\b$\u0010%J#\u0010&\u001a\u00020'2\u0006\u0010\u001b\u001a\u00020\u00042\u0006\u0010(\u001a\u00020\u0002H\u0086\u0002\u00f8\u0001\u0000¢\u0006\u0004\b)\u0010*J\t\u0010+\u001a\u00020,H\u00d6\u0001R\u0014\u0010\u0003\u001a\u00020\u00048VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u0016\u0010\u0007\u001a\u00020\b8\u0000X\u0081\u0004¢\u0006\b\n\u0000\u0012\u0004\b\f\u0010\r\u00f8\u0001\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006." }, d2 = { "Lkotlin/UShortArray;", "", "Lkotlin/UShort;", "size", "", "constructor-impl", "(I)[S", "storage", "", "([S)[S", "getSize-impl", "([S)I", "storage$annotations", "()V", "contains", "", "element", "contains-xj2QHRw", "([SS)Z", "containsAll", "elements", "containsAll-impl", "([SLjava/util/Collection;)Z", "equals", "other", "", "get", "index", "get-impl", "([SI)S", "hashCode", "isEmpty", "isEmpty-impl", "([S)Z", "iterator", "Lkotlin/collections/UShortIterator;", "iterator-impl", "([S)Lkotlin/collections/UShortIterator;", "set", "", "value", "set-01HTLdE", "([SIS)V", "toString", "", "Iterator", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
public final class UShortArray implements Collection<UShort>, KMappedMarker
{
    private final short[] storage = storage;
    
    public static short[] constructor-impl(final int n) {
        return constructor-impl(new short[n]);
    }
    
    public static short[] constructor-impl(final short[] array) {
        Intrinsics.checkParameterIsNotNull(array, "storage");
        return array;
    }
    
    public static boolean contains-xj2QHRw(final short[] array, final short n) {
        return ArraysKt___ArraysKt.contains(array, n);
    }
    
    public static boolean containsAll-impl(final short[] array, final Collection<UShort> collection) {
        Intrinsics.checkParameterIsNotNull(collection, "elements");
        final Collection<UShort> collection2 = collection;
        final boolean empty = ((Collection<Object>)collection2).isEmpty();
        final boolean b = true;
        if (empty) {
            return true;
        }
        final java.util.Iterator<Object> iterator = collection2.iterator();
        UShort next;
        do {
            final boolean b2 = b;
            if (!iterator.hasNext()) {
                return b2;
            }
            next = iterator.next();
        } while (next instanceof UShort && ArraysKt___ArraysKt.contains(array, next.unbox-impl()));
        return false;
    }
    
    public static boolean equals-impl(final short[] array, final Object o) {
        return o instanceof UShortArray && Intrinsics.areEqual(array, ((UShortArray)o).unbox-impl());
    }
    
    public static final boolean equals-impl0(final short[] array, final short[] array2) {
        Intrinsics.checkParameterIsNotNull(array, "p1");
        Intrinsics.checkParameterIsNotNull(array2, "p2");
        throw null;
    }
    
    public static final short get-impl(final short[] array, final int n) {
        return UShort.constructor-impl(array[n]);
    }
    
    public static int getSize-impl(final short[] array) {
        return array.length;
    }
    
    public static int hashCode-impl(final short[] a) {
        if (a != null) {
            return Arrays.hashCode(a);
        }
        return 0;
    }
    
    public static boolean isEmpty-impl(final short[] array) {
        return array.length == 0;
    }
    
    public static UShortIterator iterator-impl(final short[] array) {
        return new Iterator(array);
    }
    
    public static final void set-01HTLdE(final short[] array, final int n, final short n2) {
        array[n] = n2;
    }
    
    public static String toString-impl(final short[] a) {
        final StringBuilder sb = new StringBuilder();
        sb.append("UShortArray(storage=");
        sb.append(Arrays.toString(a));
        sb.append(")");
        return sb.toString();
    }
    
    public boolean add-xj2QHRw(final short n) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    @Override
    public boolean addAll(final Collection<? extends UShort> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    @Override
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    @Override
    public final /* bridge */ boolean contains(final Object o) {
        return o instanceof UShort && this.contains-xj2QHRw(((UShort)o).unbox-impl());
    }
    
    public boolean contains-xj2QHRw(final short n) {
        return contains-xj2QHRw(this.storage, n);
    }
    
    @Override
    public boolean containsAll(final Collection<?> collection) {
        return containsAll-impl(this.storage, (Collection<UShort>)collection);
    }
    
    @Override
    public boolean equals(final Object o) {
        return equals-impl(this.storage, o);
    }
    
    public int getSize() {
        return getSize-impl(this.storage);
    }
    
    @Override
    public int hashCode() {
        return hashCode-impl(this.storage);
    }
    
    @Override
    public boolean isEmpty() {
        return isEmpty-impl(this.storage);
    }
    
    @Override
    public UShortIterator iterator() {
        return iterator-impl(this.storage);
    }
    
    @Override
    public boolean remove(final Object o) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    @Override
    public boolean removeAll(final Collection<?> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    @Override
    public boolean retainAll(final Collection<?> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
    
    @Override
    public final /* bridge */ int size() {
        return this.getSize();
    }
    
    @Override
    public Object[] toArray() {
        return CollectionToArray.toArray(this);
    }
    
    @Override
    public <T> T[] toArray(final T[] array) {
        return (T[])CollectionToArray.toArray(this, array);
    }
    
    @Override
    public String toString() {
        return toString-impl(this.storage);
    }
    
    public final /* synthetic */ short[] unbox-impl() {
        return this.storage;
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0017\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\bH\u0096\u0002J\u0010\u0010\t\u001a\u00020\nH\u0016\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e¢\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\f" }, d2 = { "Lkotlin/UShortArray$Iterator;", "Lkotlin/collections/UShortIterator;", "array", "", "([S)V", "index", "", "hasNext", "", "nextUShort", "Lkotlin/UShort;", "()S", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
    private static final class Iterator extends UShortIterator
    {
        private final short[] array;
        private int index;
        
        public Iterator(final short[] array) {
            Intrinsics.checkParameterIsNotNull(array, "array");
            this.array = array;
        }
        
        @Override
        public boolean hasNext() {
            return this.index < this.array.length;
        }
        
        @Override
        public short nextUShort() {
            final int index = this.index;
            final short[] array = this.array;
            if (index < array.length) {
                this.index = index + 1;
                return UShort.constructor-impl(array[index]);
            }
            throw new NoSuchElementException(String.valueOf(index));
        }
    }
}
