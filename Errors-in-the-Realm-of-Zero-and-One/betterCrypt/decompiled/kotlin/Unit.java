// 
// Decompiled by Procyon v0.5.36
// 

package kotlin;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0010\n\u0002\u0010\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0002\b\u0002J\b\u0010\u0002\u001a\u00020\u0003H\u0016¨\u0006\u0004" }, d2 = { "", "", "toString", "", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
public final class Unit
{
    public static final Unit INSTANCE;
    
    static {
        INSTANCE = new Unit();
    }
    
    private Unit() {
    }
    
    @Override
    public String toString() {
        return "kotlin.Unit";
    }
}
