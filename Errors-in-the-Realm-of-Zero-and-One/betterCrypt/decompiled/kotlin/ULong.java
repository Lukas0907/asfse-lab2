// 
// Decompiled by Procyon v0.5.36
// 

package kotlin;

import kotlin.ranges.ULongRange;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000j\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0000\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u000b\n\u0002\u0010\u0000\n\u0002\b\u0012\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\u0005\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\b\n\u0002\u0010\n\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u000e\b\u0087@\u0018\u0000 m2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001mB\u0014\b\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00f8\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005J\u001b\u0010\b\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0000H\u0087\f\u00f8\u0001\u0000¢\u0006\u0004\b\n\u0010\u000bJ\u001b\u0010\f\u001a\u00020\r2\u0006\u0010\t\u001a\u00020\u000eH\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b\u000f\u0010\u0010J\u001b\u0010\f\u001a\u00020\r2\u0006\u0010\t\u001a\u00020\u0011H\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b\u0012\u0010\u0013J\u001b\u0010\f\u001a\u00020\r2\u0006\u0010\t\u001a\u00020\u0000H\u0097\n\u00f8\u0001\u0000¢\u0006\u0004\b\u0014\u0010\u0015J\u001b\u0010\f\u001a\u00020\r2\u0006\u0010\t\u001a\u00020\u0016H\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b\u0017\u0010\u0018J\u0013\u0010\u0019\u001a\u00020\u0000H\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b\u001a\u0010\u0005J\u001b\u0010\u001b\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u000eH\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b\u001c\u0010\u001dJ\u001b\u0010\u001b\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0011H\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b\u001e\u0010\u001fJ\u001b\u0010\u001b\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0000H\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b \u0010\u000bJ\u001b\u0010\u001b\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0016H\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b!\u0010\"J\u0013\u0010#\u001a\u00020$2\b\u0010\t\u001a\u0004\u0018\u00010%H\u00d6\u0003J\t\u0010&\u001a\u00020\rH\u00d6\u0001J\u0013\u0010'\u001a\u00020\u0000H\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b(\u0010\u0005J\u0013\u0010)\u001a\u00020\u0000H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b*\u0010\u0005J\u001b\u0010+\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u000eH\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b,\u0010\u001dJ\u001b\u0010+\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0011H\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b-\u0010\u001fJ\u001b\u0010+\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0000H\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b.\u0010\u000bJ\u001b\u0010+\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0016H\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b/\u0010\"J\u001b\u00100\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0000H\u0087\f\u00f8\u0001\u0000¢\u0006\u0004\b1\u0010\u000bJ\u001b\u00102\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u000eH\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b3\u0010\u001dJ\u001b\u00102\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0011H\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b4\u0010\u001fJ\u001b\u00102\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0000H\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b5\u0010\u000bJ\u001b\u00102\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0016H\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b6\u0010\"J\u001b\u00107\u001a\u0002082\u0006\u0010\t\u001a\u00020\u0000H\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b9\u0010:J\u001b\u0010;\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u000eH\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b<\u0010\u001dJ\u001b\u0010;\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0011H\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b=\u0010\u001fJ\u001b\u0010;\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0000H\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b>\u0010\u000bJ\u001b\u0010;\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0016H\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\b?\u0010\"J\u001b\u0010@\u001a\u00020\u00002\u0006\u0010A\u001a\u00020\rH\u0087\f\u00f8\u0001\u0000¢\u0006\u0004\bB\u0010\u001fJ\u001b\u0010C\u001a\u00020\u00002\u0006\u0010A\u001a\u00020\rH\u0087\f\u00f8\u0001\u0000¢\u0006\u0004\bD\u0010\u001fJ\u001b\u0010E\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u000eH\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\bF\u0010\u001dJ\u001b\u0010E\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0011H\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\bG\u0010\u001fJ\u001b\u0010E\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0000H\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\bH\u0010\u000bJ\u001b\u0010E\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0016H\u0087\n\u00f8\u0001\u0000¢\u0006\u0004\bI\u0010\"J\u0010\u0010J\u001a\u00020KH\u0087\b¢\u0006\u0004\bL\u0010MJ\u0010\u0010N\u001a\u00020OH\u0087\b¢\u0006\u0004\bP\u0010QJ\u0010\u0010R\u001a\u00020SH\u0087\b¢\u0006\u0004\bT\u0010UJ\u0010\u0010V\u001a\u00020\rH\u0087\b¢\u0006\u0004\bW\u0010XJ\u0010\u0010Y\u001a\u00020\u0003H\u0087\b¢\u0006\u0004\bZ\u0010\u0005J\u0010\u0010[\u001a\u00020\\H\u0087\b¢\u0006\u0004\b]\u0010^J\u000f\u0010_\u001a\u00020`H\u0016¢\u0006\u0004\ba\u0010bJ\u0013\u0010c\u001a\u00020\u000eH\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\bd\u0010MJ\u0013\u0010e\u001a\u00020\u0011H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\bf\u0010XJ\u0013\u0010g\u001a\u00020\u0000H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\bh\u0010\u0005J\u0013\u0010i\u001a\u00020\u0016H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\bj\u0010^J\u001b\u0010k\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0000H\u0087\f\u00f8\u0001\u0000¢\u0006\u0004\bl\u0010\u000bR\u0016\u0010\u0002\u001a\u00020\u00038\u0000X\u0081\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u0006\u0010\u0007\u00f8\u0001\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006n" }, d2 = { "Lkotlin/ULong;", "", "data", "", "constructor-impl", "(J)J", "data$annotations", "()V", "and", "other", "and-VKZWuLQ", "(JJ)J", "compareTo", "", "Lkotlin/UByte;", "compareTo-7apg3OU", "(JB)I", "Lkotlin/UInt;", "compareTo-WZ4Q5Ns", "(JI)I", "compareTo-VKZWuLQ", "(JJ)I", "Lkotlin/UShort;", "compareTo-xj2QHRw", "(JS)I", "dec", "dec-impl", "div", "div-7apg3OU", "(JB)J", "div-WZ4Q5Ns", "(JI)J", "div-VKZWuLQ", "div-xj2QHRw", "(JS)J", "equals", "", "", "hashCode", "inc", "inc-impl", "inv", "inv-impl", "minus", "minus-7apg3OU", "minus-WZ4Q5Ns", "minus-VKZWuLQ", "minus-xj2QHRw", "or", "or-VKZWuLQ", "plus", "plus-7apg3OU", "plus-WZ4Q5Ns", "plus-VKZWuLQ", "plus-xj2QHRw", "rangeTo", "Lkotlin/ranges/ULongRange;", "rangeTo-VKZWuLQ", "(JJ)Lkotlin/ranges/ULongRange;", "rem", "rem-7apg3OU", "rem-WZ4Q5Ns", "rem-VKZWuLQ", "rem-xj2QHRw", "shl", "bitCount", "shl-impl", "shr", "shr-impl", "times", "times-7apg3OU", "times-WZ4Q5Ns", "times-VKZWuLQ", "times-xj2QHRw", "toByte", "", "toByte-impl", "(J)B", "toDouble", "", "toDouble-impl", "(J)D", "toFloat", "", "toFloat-impl", "(J)F", "toInt", "toInt-impl", "(J)I", "toLong", "toLong-impl", "toShort", "", "toShort-impl", "(J)S", "toString", "", "toString-impl", "(J)Ljava/lang/String;", "toUByte", "toUByte-impl", "toUInt", "toUInt-impl", "toULong", "toULong-impl", "toUShort", "toUShort-impl", "xor", "xor-VKZWuLQ", "Companion", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
public final class ULong implements Comparable<ULong>
{
    public static final Companion Companion;
    public static final long MAX_VALUE = -1L;
    public static final long MIN_VALUE = 0L;
    public static final int SIZE_BITS = 64;
    public static final int SIZE_BYTES = 8;
    private final long data = data;
    
    static {
        Companion = new Companion(null);
    }
    
    private static final long and-VKZWuLQ(final long n, final long n2) {
        return constructor-impl(n & n2);
    }
    
    private static final int compareTo-7apg3OU(final long n, final byte b) {
        return UnsignedKt.ulongCompare(n, constructor-impl((long)b & 0xFFL));
    }
    
    private int compareTo-VKZWuLQ(final long n) {
        return compareTo-VKZWuLQ(this.data, n);
    }
    
    private static int compareTo-VKZWuLQ(final long n, final long n2) {
        return UnsignedKt.ulongCompare(n, n2);
    }
    
    private static final int compareTo-WZ4Q5Ns(final long n, final int n2) {
        return UnsignedKt.ulongCompare(n, constructor-impl((long)n2 & 0xFFFFFFFFL));
    }
    
    private static final int compareTo-xj2QHRw(final long n, final short n2) {
        return UnsignedKt.ulongCompare(n, constructor-impl((long)n2 & 0xFFFFL));
    }
    
    public static long constructor-impl(final long n) {
        return n;
    }
    
    private static final long dec-impl(final long n) {
        return constructor-impl(n - 1L);
    }
    
    private static final long div-7apg3OU(final long n, final byte b) {
        return UnsignedKt.ulongDivide-eb3DHEI(n, constructor-impl((long)b & 0xFFL));
    }
    
    private static final long div-VKZWuLQ(final long n, final long n2) {
        return UnsignedKt.ulongDivide-eb3DHEI(n, n2);
    }
    
    private static final long div-WZ4Q5Ns(final long n, final int n2) {
        return UnsignedKt.ulongDivide-eb3DHEI(n, constructor-impl((long)n2 & 0xFFFFFFFFL));
    }
    
    private static final long div-xj2QHRw(final long n, final short n2) {
        return UnsignedKt.ulongDivide-eb3DHEI(n, constructor-impl((long)n2 & 0xFFFFL));
    }
    
    public static boolean equals-impl(final long n, final Object o) {
        return o instanceof ULong && n == ((ULong)o).unbox-impl();
    }
    
    public static final boolean equals-impl0(final long n, final long n2) {
        throw null;
    }
    
    public static int hashCode-impl(final long n) {
        return (int)(n ^ n >>> 32);
    }
    
    private static final long inc-impl(final long n) {
        return constructor-impl(n + 1L);
    }
    
    private static final long inv-impl(final long n) {
        return constructor-impl(n);
    }
    
    private static final long minus-7apg3OU(final long n, final byte b) {
        return constructor-impl(n - constructor-impl((long)b & 0xFFL));
    }
    
    private static final long minus-VKZWuLQ(final long n, final long n2) {
        return constructor-impl(n - n2);
    }
    
    private static final long minus-WZ4Q5Ns(final long n, final int n2) {
        return constructor-impl(n - constructor-impl((long)n2 & 0xFFFFFFFFL));
    }
    
    private static final long minus-xj2QHRw(final long n, final short n2) {
        return constructor-impl(n - constructor-impl((long)n2 & 0xFFFFL));
    }
    
    private static final long or-VKZWuLQ(final long n, final long n2) {
        return constructor-impl(n | n2);
    }
    
    private static final long plus-7apg3OU(final long n, final byte b) {
        return constructor-impl(n + constructor-impl((long)b & 0xFFL));
    }
    
    private static final long plus-VKZWuLQ(final long n, final long n2) {
        return constructor-impl(n + n2);
    }
    
    private static final long plus-WZ4Q5Ns(final long n, final int n2) {
        return constructor-impl(n + constructor-impl((long)n2 & 0xFFFFFFFFL));
    }
    
    private static final long plus-xj2QHRw(final long n, final short n2) {
        return constructor-impl(n + constructor-impl((long)n2 & 0xFFFFL));
    }
    
    private static final ULongRange rangeTo-VKZWuLQ(final long n, final long n2) {
        return new ULongRange(n, n2, null);
    }
    
    private static final long rem-7apg3OU(final long n, final byte b) {
        return UnsignedKt.ulongRemainder-eb3DHEI(n, constructor-impl((long)b & 0xFFL));
    }
    
    private static final long rem-VKZWuLQ(final long n, final long n2) {
        return UnsignedKt.ulongRemainder-eb3DHEI(n, n2);
    }
    
    private static final long rem-WZ4Q5Ns(final long n, final int n2) {
        return UnsignedKt.ulongRemainder-eb3DHEI(n, constructor-impl((long)n2 & 0xFFFFFFFFL));
    }
    
    private static final long rem-xj2QHRw(final long n, final short n2) {
        return UnsignedKt.ulongRemainder-eb3DHEI(n, constructor-impl((long)n2 & 0xFFFFL));
    }
    
    private static final long shl-impl(final long n, final int n2) {
        return constructor-impl(n << n2);
    }
    
    private static final long shr-impl(final long n, final int n2) {
        return constructor-impl(n >>> n2);
    }
    
    private static final long times-7apg3OU(final long n, final byte b) {
        return constructor-impl(n * constructor-impl((long)b & 0xFFL));
    }
    
    private static final long times-VKZWuLQ(final long n, final long n2) {
        return constructor-impl(n * n2);
    }
    
    private static final long times-WZ4Q5Ns(final long n, final int n2) {
        return constructor-impl(n * constructor-impl((long)n2 & 0xFFFFFFFFL));
    }
    
    private static final long times-xj2QHRw(final long n, final short n2) {
        return constructor-impl(n * constructor-impl((long)n2 & 0xFFFFL));
    }
    
    private static final byte toByte-impl(final long n) {
        return (byte)n;
    }
    
    private static final double toDouble-impl(final long n) {
        return UnsignedKt.ulongToDouble(n);
    }
    
    private static final float toFloat-impl(final long n) {
        return (float)UnsignedKt.ulongToDouble(n);
    }
    
    private static final int toInt-impl(final long n) {
        return (int)n;
    }
    
    private static final long toLong-impl(final long n) {
        return n;
    }
    
    private static final short toShort-impl(final long n) {
        return (short)n;
    }
    
    public static String toString-impl(final long n) {
        return UnsignedKt.ulongToString(n);
    }
    
    private static final byte toUByte-impl(final long n) {
        return UByte.constructor-impl((byte)n);
    }
    
    private static final int toUInt-impl(final long n) {
        return UInt.constructor-impl((int)n);
    }
    
    private static final long toULong-impl(final long n) {
        return n;
    }
    
    private static final short toUShort-impl(final long n) {
        return UShort.constructor-impl((short)n);
    }
    
    private static final long xor-VKZWuLQ(final long n, final long n2) {
        return constructor-impl(n ^ n2);
    }
    
    @Override
    public boolean equals(final Object o) {
        return equals-impl(this.data, o);
    }
    
    @Override
    public int hashCode() {
        return hashCode-impl(this.data);
    }
    
    @Override
    public String toString() {
        return toString-impl(this.data);
    }
    
    public final /* synthetic */ long unbox-impl() {
        return this.data;
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0013\u0010\u0003\u001a\u00020\u0004X\u0086T\u00f8\u0001\u0000¢\u0006\u0004\n\u0002\u0010\u0005R\u0013\u0010\u0006\u001a\u00020\u0004X\u0086T\u00f8\u0001\u0000¢\u0006\u0004\n\u0002\u0010\u0005R\u000e\u0010\u0007\u001a\u00020\bX\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\bX\u0086T¢\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\n" }, d2 = { "Lkotlin/ULong$Companion;", "", "()V", "MAX_VALUE", "Lkotlin/ULong;", "J", "MIN_VALUE", "SIZE_BITS", "", "SIZE_BYTES", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
    public static final class Companion
    {
        private Companion() {
        }
    }
}
