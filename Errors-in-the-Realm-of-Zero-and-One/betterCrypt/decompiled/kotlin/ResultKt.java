// 
// Decompiled by Procyon v0.5.36
// 

package kotlin;

import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000:\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0001\u001a+\u0010\u0004\u001a\b\u0012\u0004\u0012\u0002H\u00060\u0005\"\u0004\b\u0000\u0010\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u0002H\u00060\bH\u0087\b\u00f8\u0001\u0000¢\u0006\u0002\u0010\t\u001a\u0084\u0001\u0010\n\u001a\u0002H\u0006\"\u0004\b\u0000\u0010\u0006\"\u0004\b\u0001\u0010\u000b*\b\u0012\u0004\u0012\u0002H\u000b0\u00052!\u0010\f\u001a\u001d\u0012\u0013\u0012\u0011H\u000b¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0010\u0012\u0004\u0012\u0002H\u00060\r2!\u0010\u0011\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0002\u0012\u0004\u0012\u0002H\u00060\rH\u0087\b\u00f8\u0001\u0000\u0082\u0002\u0014\n\b\b\u0001\u0012\u0002\u0010\u0001 \u0000\n\b\b\u0001\u0012\u0002\u0010\u0002 \u0000¢\u0006\u0002\u0010\u0012\u001a3\u0010\u0013\u001a\u0002H\u0006\"\u0004\b\u0000\u0010\u0006\"\b\b\u0001\u0010\u000b*\u0002H\u0006*\b\u0012\u0004\u0012\u0002H\u000b0\u00052\u0006\u0010\u0014\u001a\u0002H\u0006H\u0087\b\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0015\u001a[\u0010\u0016\u001a\u0002H\u0006\"\u0004\b\u0000\u0010\u0006\"\b\b\u0001\u0010\u000b*\u0002H\u0006*\b\u0012\u0004\u0012\u0002H\u000b0\u00052!\u0010\u0011\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0002\u0012\u0004\u0012\u0002H\u00060\rH\u0087\b\u00f8\u0001\u0000\u0082\u0002\n\n\b\b\u0001\u0012\u0002\u0010\u0001 \u0000¢\u0006\u0002\u0010\u0017\u001a!\u0010\u0018\u001a\u0002H\u000b\"\u0004\b\u0000\u0010\u000b*\b\u0012\u0004\u0012\u0002H\u000b0\u0005H\u0087\b\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0019\u001a]\u0010\u001a\u001a\b\u0012\u0004\u0012\u0002H\u00060\u0005\"\u0004\b\u0000\u0010\u0006\"\u0004\b\u0001\u0010\u000b*\b\u0012\u0004\u0012\u0002H\u000b0\u00052!\u0010\u001b\u001a\u001d\u0012\u0013\u0012\u0011H\u000b¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0010\u0012\u0004\u0012\u0002H\u00060\rH\u0087\b\u00f8\u0001\u0000\u0082\u0002\n\n\b\b\u0001\u0012\u0002\u0010\u0001 \u0000¢\u0006\u0002\u0010\u0017\u001aP\u0010\u001c\u001a\b\u0012\u0004\u0012\u0002H\u00060\u0005\"\u0004\b\u0000\u0010\u0006\"\u0004\b\u0001\u0010\u000b*\b\u0012\u0004\u0012\u0002H\u000b0\u00052!\u0010\u001b\u001a\u001d\u0012\u0013\u0012\u0011H\u000b¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0010\u0012\u0004\u0012\u0002H\u00060\rH\u0087\b\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0017\u001aW\u0010\u0011\u001a\b\u0012\u0004\u0012\u0002H\u000b0\u0005\"\u0004\b\u0000\u0010\u000b*\b\u0012\u0004\u0012\u0002H\u000b0\u00052!\u0010\u001d\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0002\u0012\u0004\u0012\u00020\u001e0\rH\u0087\b\u00f8\u0001\u0000\u0082\u0002\n\n\b\b\u0001\u0012\u0002\u0010\u0001 \u0000¢\u0006\u0002\u0010\u0017\u001aW\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\u000b0\u0005\"\u0004\b\u0000\u0010\u000b*\b\u0012\u0004\u0012\u0002H\u000b0\u00052!\u0010\u001d\u001a\u001d\u0012\u0013\u0012\u0011H\u000b¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0010\u0012\u0004\u0012\u00020\u001e0\rH\u0087\b\u00f8\u0001\u0000\u0082\u0002\n\n\b\b\u0001\u0012\u0002\u0010\u0001 \u0000¢\u0006\u0002\u0010\u0017\u001aa\u0010\u001f\u001a\b\u0012\u0004\u0012\u0002H\u00060\u0005\"\u0004\b\u0000\u0010\u0006\"\b\b\u0001\u0010\u000b*\u0002H\u0006*\b\u0012\u0004\u0012\u0002H\u000b0\u00052!\u0010\u001b\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0002\u0012\u0004\u0012\u0002H\u00060\rH\u0087\b\u00f8\u0001\u0000\u0082\u0002\n\n\b\b\u0001\u0012\u0002\u0010\u0001 \u0000¢\u0006\u0002\u0010\u0017\u001aT\u0010 \u001a\b\u0012\u0004\u0012\u0002H\u00060\u0005\"\u0004\b\u0000\u0010\u0006\"\b\b\u0001\u0010\u000b*\u0002H\u0006*\b\u0012\u0004\u0012\u0002H\u000b0\u00052!\u0010\u001b\u001a\u001d\u0012\u0013\u0012\u00110\u0003¢\u0006\f\b\u000e\u0012\b\b\u000f\u0012\u0004\b\b(\u0002\u0012\u0004\u0012\u0002H\u00060\rH\u0087\b\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0017\u001a@\u0010\u0004\u001a\b\u0012\u0004\u0012\u0002H\u00060\u0005\"\u0004\b\u0000\u0010\u000b\"\u0004\b\u0001\u0010\u0006*\u0002H\u000b2\u0017\u0010\u0007\u001a\u0013\u0012\u0004\u0012\u0002H\u000b\u0012\u0004\u0012\u0002H\u00060\r¢\u0006\u0002\b!H\u0087\b\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0017\u001a\u0018\u0010\"\u001a\u00020\u001e*\u0006\u0012\u0002\b\u00030\u0005H\u0001\u00f8\u0001\u0000¢\u0006\u0002\u0010#\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006$" }, d2 = { "createFailure", "", "exception", "", "runCatching", "Lkotlin/Result;", "R", "block", "Lkotlin/Function0;", "(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;", "fold", "T", "onSuccess", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "value", "onFailure", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "getOrDefault", "defaultValue", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", "getOrElse", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "getOrThrow", "(Ljava/lang/Object;)Ljava/lang/Object;", "map", "transform", "mapCatching", "action", "", "recover", "recoverCatching", "Lkotlin/ExtensionFunctionType;", "throwOnFailure", "(Ljava/lang/Object;)V", "kotlin-stdlib" }, k = 2, mv = { 1, 1, 15 })
public final class ResultKt
{
    public static final Object createFailure(final Throwable t) {
        Intrinsics.checkParameterIsNotNull(t, "exception");
        return new Result.Failure(t);
    }
    
    private static final <R, T> R fold(final Object o, final Function1<? super T, ? extends R> function1, final Function1<? super Throwable, ? extends R> function2) {
        final Throwable exceptionOrNull-impl = Result.exceptionOrNull-impl(o);
        if (exceptionOrNull-impl == null) {
            return (R)function1.invoke((Object)o);
        }
        return (R)function2.invoke(exceptionOrNull-impl);
    }
    
    private static final <R, T extends R> R getOrDefault(final Object o, final R r) {
        if (Result.isFailure-impl(o)) {
            return r;
        }
        return (R)o;
    }
    
    private static final <R, T extends R> R getOrElse(final Object o, final Function1<? super Throwable, ? extends R> function1) {
        final Throwable exceptionOrNull-impl = Result.exceptionOrNull-impl(o);
        if (exceptionOrNull-impl == null) {
            return (R)o;
        }
        return (R)function1.invoke(exceptionOrNull-impl);
    }
    
    private static final <T> T getOrThrow(final Object o) {
        throwOnFailure(o);
        return (T)o;
    }
    
    private static final <R, T> Object map(final Object o, final Function1<? super T, ? extends R> function1) {
        if (Result.isSuccess-impl(o)) {
            final Result.Companion companion = Result.Companion;
            return Result.constructor-impl(function1.invoke((Object)o));
        }
        return Result.constructor-impl(o);
    }
    
    private static final <R, T> Object mapCatching(Object constructor-impl, final Function1<? super T, ? extends R> function1) {
        if (Result.isSuccess-impl(constructor-impl)) {
            try {
                final Result.Companion companion = Result.Companion;
                constructor-impl = Result.constructor-impl(function1.invoke((Object)constructor-impl));
                return constructor-impl;
            }
            finally {
                final Result.Companion companion2 = Result.Companion;
                final Throwable t;
                return Result.constructor-impl(createFailure(t));
            }
        }
        return Result.constructor-impl(constructor-impl);
    }
    
    private static final <T> Object onFailure(final Object o, final Function1<? super Throwable, Unit> function1) {
        final Throwable exceptionOrNull-impl = Result.exceptionOrNull-impl(o);
        if (exceptionOrNull-impl != null) {
            function1.invoke(exceptionOrNull-impl);
        }
        return o;
    }
    
    private static final <T> Object onSuccess(final Object o, final Function1<? super T, Unit> function1) {
        if (Result.isSuccess-impl(o)) {
            function1.invoke((Object)o);
        }
        return o;
    }
    
    private static final <R, T extends R> Object recover(final Object o, final Function1<? super Throwable, ? extends R> function1) {
        final Throwable exceptionOrNull-impl = Result.exceptionOrNull-impl(o);
        if (exceptionOrNull-impl == null) {
            return o;
        }
        final Result.Companion companion = Result.Companion;
        return Result.constructor-impl(function1.invoke(exceptionOrNull-impl));
    }
    
    private static final <R, T extends R> Object recoverCatching(Object constructor-impl, final Function1<? super Throwable, ? extends R> function1) {
        final Throwable exceptionOrNull-impl = Result.exceptionOrNull-impl(constructor-impl);
        if (exceptionOrNull-impl == null) {
            return constructor-impl;
        }
        try {
            final Result.Companion companion = Result.Companion;
            constructor-impl = Result.constructor-impl(function1.invoke(exceptionOrNull-impl));
            return constructor-impl;
        }
        finally {
            final Result.Companion companion2 = Result.Companion;
            final Throwable t;
            return Result.constructor-impl(createFailure(t));
        }
    }
    
    private static final <T, R> Object runCatching(final T t, final Function1<? super T, ? extends R> function1) {
        try {
            final Result.Companion companion = Result.Companion;
            return Result.constructor-impl(function1.invoke(t));
        }
        finally {
            final Result.Companion companion2 = Result.Companion;
            final Throwable t2;
            return Result.constructor-impl(createFailure(t2));
        }
    }
    
    private static final <R> Object runCatching(final Function0<? extends R> function0) {
        try {
            final Result.Companion companion = Result.Companion;
            return Result.constructor-impl(function0.invoke());
        }
        finally {
            final Result.Companion companion2 = Result.Companion;
            final Throwable t;
            return Result.constructor-impl(createFailure(t));
        }
    }
    
    public static final void throwOnFailure(final Object o) {
        if (!(o instanceof Result.Failure)) {
            return;
        }
        throw ((Result.Failure)o).exception;
    }
}
