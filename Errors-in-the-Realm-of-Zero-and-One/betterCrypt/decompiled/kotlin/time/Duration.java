// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.time;

import kotlin.jvm.functions.Function5;
import kotlin.jvm.functions.Function4;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function2;
import java.util.concurrent.TimeUnit;
import kotlin.jvm.internal.DoubleCompanionObject;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0000\n\u0002\u0010\u0006\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b&\n\u0002\u0010\u000b\n\u0002\u0010\u0000\n\u0002\b\u0015\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0012\b\u0087@\u0018\u0000 s2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001sB\u0014\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00f8\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005J\u001b\u0010%\u001a\u00020\t2\u0006\u0010&\u001a\u00020\u0000H\u0096\u0002\u00f8\u0001\u0000¢\u0006\u0004\b'\u0010(J\u001b\u0010)\u001a\u00020\u00002\u0006\u0010*\u001a\u00020\u0003H\u0086\u0002\u00f8\u0001\u0000¢\u0006\u0004\b+\u0010,J\u001b\u0010)\u001a\u00020\u00002\u0006\u0010*\u001a\u00020\tH\u0086\u0002\u00f8\u0001\u0000¢\u0006\u0004\b+\u0010-J\u001b\u0010)\u001a\u00020\u00032\u0006\u0010&\u001a\u00020\u0000H\u0086\u0002\u00f8\u0001\u0000¢\u0006\u0004\b.\u0010,J\u0013\u0010/\u001a\u0002002\b\u0010&\u001a\u0004\u0018\u000101H\u00d6\u0003J\t\u00102\u001a\u00020\tH\u00d6\u0001J\r\u00103\u001a\u000200¢\u0006\u0004\b4\u00105J\r\u00106\u001a\u000200¢\u0006\u0004\b7\u00105J\r\u00108\u001a\u000200¢\u0006\u0004\b9\u00105J\r\u0010:\u001a\u000200¢\u0006\u0004\b;\u00105J\u001b\u0010<\u001a\u00020\u00002\u0006\u0010&\u001a\u00020\u0000H\u0086\u0002\u00f8\u0001\u0000¢\u0006\u0004\b=\u0010,J\u001b\u0010>\u001a\u00020\u00002\u0006\u0010&\u001a\u00020\u0000H\u0086\u0002\u00f8\u0001\u0000¢\u0006\u0004\b?\u0010,J\u0017\u0010@\u001a\u00020\t2\u0006\u0010\u0002\u001a\u00020\u0003H\u0002¢\u0006\u0004\bA\u0010(J\u001b\u0010B\u001a\u00020\u00002\u0006\u0010*\u001a\u00020\u0003H\u0086\u0002\u00f8\u0001\u0000¢\u0006\u0004\bC\u0010,J\u001b\u0010B\u001a\u00020\u00002\u0006\u0010*\u001a\u00020\tH\u0086\u0002\u00f8\u0001\u0000¢\u0006\u0004\bC\u0010-J\u008d\u0001\u0010D\u001a\u0002HE\"\u0004\b\u0000\u0010E2u\u0010F\u001aq\u0012\u0013\u0012\u00110\t¢\u0006\f\bH\u0012\b\bI\u0012\u0004\b\b(J\u0012\u0013\u0012\u00110\t¢\u0006\f\bH\u0012\b\bI\u0012\u0004\b\b(K\u0012\u0013\u0012\u00110\t¢\u0006\f\bH\u0012\b\bI\u0012\u0004\b\b(L\u0012\u0013\u0012\u00110\t¢\u0006\f\bH\u0012\b\bI\u0012\u0004\b\b(M\u0012\u0013\u0012\u00110\t¢\u0006\f\bH\u0012\b\bI\u0012\u0004\b\b(N\u0012\u0004\u0012\u0002HE0GH\u0086\b¢\u0006\u0004\bO\u0010PJx\u0010D\u001a\u0002HE\"\u0004\b\u0000\u0010E2`\u0010F\u001a\\\u0012\u0013\u0012\u00110\t¢\u0006\f\bH\u0012\b\bI\u0012\u0004\b\b(K\u0012\u0013\u0012\u00110\t¢\u0006\f\bH\u0012\b\bI\u0012\u0004\b\b(L\u0012\u0013\u0012\u00110\t¢\u0006\f\bH\u0012\b\bI\u0012\u0004\b\b(M\u0012\u0013\u0012\u00110\t¢\u0006\f\bH\u0012\b\bI\u0012\u0004\b\b(N\u0012\u0004\u0012\u0002HE0QH\u0086\b¢\u0006\u0004\bO\u0010RJc\u0010D\u001a\u0002HE\"\u0004\b\u0000\u0010E2K\u0010F\u001aG\u0012\u0013\u0012\u00110\t¢\u0006\f\bH\u0012\b\bI\u0012\u0004\b\b(L\u0012\u0013\u0012\u00110\t¢\u0006\f\bH\u0012\b\bI\u0012\u0004\b\b(M\u0012\u0013\u0012\u00110\t¢\u0006\f\bH\u0012\b\bI\u0012\u0004\b\b(N\u0012\u0004\u0012\u0002HE0SH\u0086\b¢\u0006\u0004\bO\u0010TJN\u0010D\u001a\u0002HE\"\u0004\b\u0000\u0010E26\u0010F\u001a2\u0012\u0013\u0012\u00110V¢\u0006\f\bH\u0012\b\bI\u0012\u0004\b\b(M\u0012\u0013\u0012\u00110\t¢\u0006\f\bH\u0012\b\bI\u0012\u0004\b\b(N\u0012\u0004\u0012\u0002HE0UH\u0086\b¢\u0006\u0004\bO\u0010WJ\u0019\u0010X\u001a\u00020\u00032\n\u0010Y\u001a\u00060Zj\u0002`[¢\u0006\u0004\b\\\u0010]J\u0019\u0010^\u001a\u00020\t2\n\u0010Y\u001a\u00060Zj\u0002`[¢\u0006\u0004\b_\u0010`J\r\u0010a\u001a\u00020b¢\u0006\u0004\bc\u0010dJ\u0019\u0010e\u001a\u00020V2\n\u0010Y\u001a\u00060Zj\u0002`[¢\u0006\u0004\bf\u0010gJ\r\u0010h\u001a\u00020V¢\u0006\u0004\bi\u0010jJ\r\u0010k\u001a\u00020V¢\u0006\u0004\bl\u0010jJ\u000f\u0010m\u001a\u00020bH\u0016¢\u0006\u0004\bn\u0010dJ#\u0010m\u001a\u00020b2\n\u0010Y\u001a\u00060Zj\u0002`[2\b\b\u0002\u0010o\u001a\u00020\t¢\u0006\u0004\bn\u0010pJ\u0013\u0010q\u001a\u00020\u0000H\u0086\u0002\u00f8\u0001\u0000¢\u0006\u0004\br\u0010\u0005R\u0014\u0010\u0006\u001a\u00020\u00008F\u00f8\u0001\u0000¢\u0006\u0006\u001a\u0004\b\u0007\u0010\u0005R\u001a\u0010\b\u001a\u00020\t8@X\u0081\u0004¢\u0006\f\u0012\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\rR\u0011\u0010\u000e\u001a\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0005R\u0011\u0010\u0010\u001a\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0005R\u0011\u0010\u0012\u001a\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0005R\u0011\u0010\u0014\u001a\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\u0015\u0010\u0005R\u0011\u0010\u0016\u001a\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\u0017\u0010\u0005R\u0011\u0010\u0018\u001a\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u0005R\u0011\u0010\u001a\u001a\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\u001b\u0010\u0005R\u001a\u0010\u001c\u001a\u00020\t8@X\u0081\u0004¢\u0006\f\u0012\u0004\b\u001d\u0010\u000b\u001a\u0004\b\u001e\u0010\rR\u001a\u0010\u001f\u001a\u00020\t8@X\u0081\u0004¢\u0006\f\u0012\u0004\b \u0010\u000b\u001a\u0004\b!\u0010\rR\u001a\u0010\"\u001a\u00020\t8@X\u0081\u0004¢\u0006\f\u0012\u0004\b#\u0010\u000b\u001a\u0004\b$\u0010\rR\u000e\u0010\u0002\u001a\u00020\u0003X\u0080\u0004¢\u0006\u0002\n\u0000\u00f8\u0001\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006t" }, d2 = { "Lkotlin/time/Duration;", "", "value", "", "constructor-impl", "(D)D", "absoluteValue", "getAbsoluteValue-impl", "hoursComponent", "", "hoursComponent$annotations", "()V", "getHoursComponent-impl", "(D)I", "inDays", "getInDays-impl", "inHours", "getInHours-impl", "inMicroseconds", "getInMicroseconds-impl", "inMilliseconds", "getInMilliseconds-impl", "inMinutes", "getInMinutes-impl", "inNanoseconds", "getInNanoseconds-impl", "inSeconds", "getInSeconds-impl", "minutesComponent", "minutesComponent$annotations", "getMinutesComponent-impl", "nanosecondsComponent", "nanosecondsComponent$annotations", "getNanosecondsComponent-impl", "secondsComponent", "secondsComponent$annotations", "getSecondsComponent-impl", "compareTo", "other", "compareTo-LRDsOJo", "(DD)I", "div", "scale", "div-impl", "(DD)D", "(DI)D", "div-LRDsOJo", "equals", "", "", "hashCode", "isFinite", "isFinite-impl", "(D)Z", "isInfinite", "isInfinite-impl", "isNegative", "isNegative-impl", "isPositive", "isPositive-impl", "minus", "minus-LRDsOJo", "plus", "plus-LRDsOJo", "precision", "precision-impl", "times", "times-impl", "toComponents", "T", "action", "Lkotlin/Function5;", "Lkotlin/ParameterName;", "name", "days", "hours", "minutes", "seconds", "nanoseconds", "toComponents-impl", "(DLkotlin/jvm/functions/Function5;)Ljava/lang/Object;", "Lkotlin/Function4;", "(DLkotlin/jvm/functions/Function4;)Ljava/lang/Object;", "Lkotlin/Function3;", "(DLkotlin/jvm/functions/Function3;)Ljava/lang/Object;", "Lkotlin/Function2;", "", "(DLkotlin/jvm/functions/Function2;)Ljava/lang/Object;", "toDouble", "unit", "Ljava/util/concurrent/TimeUnit;", "Lkotlin/time/DurationUnit;", "toDouble-impl", "(DLjava/util/concurrent/TimeUnit;)D", "toInt", "toInt-impl", "(DLjava/util/concurrent/TimeUnit;)I", "toIsoString", "", "toIsoString-impl", "(D)Ljava/lang/String;", "toLong", "toLong-impl", "(DLjava/util/concurrent/TimeUnit;)J", "toLongMilliseconds", "toLongMilliseconds-impl", "(D)J", "toLongNanoseconds", "toLongNanoseconds-impl", "toString", "toString-impl", "decimals", "(DLjava/util/concurrent/TimeUnit;I)Ljava/lang/String;", "unaryMinus", "unaryMinus-impl", "Companion", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
public final class Duration implements Comparable<Duration>
{
    public static final Companion Companion;
    private static final double INFINITE;
    private static final double ZERO;
    private final double value = value;
    
    static {
        Companion = new Companion(null);
        ZERO = constructor-impl(0.0);
        INFINITE = constructor-impl(DoubleCompanionObject.INSTANCE.getPOSITIVE_INFINITY());
    }
    
    public static final /* synthetic */ double access$getINFINITE$cp() {
        return Duration.INFINITE;
    }
    
    public static final /* synthetic */ double access$getZERO$cp() {
        return Duration.ZERO;
    }
    
    public static int compareTo-LRDsOJo(final double d1, final double d2) {
        return Double.compare(d1, d2);
    }
    
    public static double constructor-impl(final double n) {
        return n;
    }
    
    public static final double div-LRDsOJo(final double n, final double n2) {
        return n / n2;
    }
    
    public static final double div-impl(final double n, final double n2) {
        return constructor-impl(n / n2);
    }
    
    public static final double div-impl(final double n, final int n2) {
        return constructor-impl(n / n2);
    }
    
    public static boolean equals-impl(final double d1, final Object o) {
        return o instanceof Duration && Double.compare(d1, ((Duration)o).unbox-impl()) == 0;
    }
    
    public static final boolean equals-impl0(final double n, final double n2) {
        throw null;
    }
    
    public static final double getAbsoluteValue-impl(final double n) {
        double unaryMinus-impl = n;
        if (isNegative-impl(n)) {
            unaryMinus-impl = unaryMinus-impl(n);
        }
        return unaryMinus-impl;
    }
    
    public static final int getHoursComponent-impl(final double n) {
        return (int)(getInHours-impl(n) % 24);
    }
    
    public static final double getInDays-impl(final double n) {
        return toDouble-impl(n, TimeUnit.DAYS);
    }
    
    public static final double getInHours-impl(final double n) {
        return toDouble-impl(n, TimeUnit.HOURS);
    }
    
    public static final double getInMicroseconds-impl(final double n) {
        return toDouble-impl(n, TimeUnit.MICROSECONDS);
    }
    
    public static final double getInMilliseconds-impl(final double n) {
        return toDouble-impl(n, TimeUnit.MILLISECONDS);
    }
    
    public static final double getInMinutes-impl(final double n) {
        return toDouble-impl(n, TimeUnit.MINUTES);
    }
    
    public static final double getInNanoseconds-impl(final double n) {
        return toDouble-impl(n, TimeUnit.NANOSECONDS);
    }
    
    public static final double getInSeconds-impl(final double n) {
        return toDouble-impl(n, TimeUnit.SECONDS);
    }
    
    public static final int getMinutesComponent-impl(final double n) {
        return (int)(getInMinutes-impl(n) % 60);
    }
    
    public static final int getNanosecondsComponent-impl(final double n) {
        return (int)(getInNanoseconds-impl(n) % 1.0E9);
    }
    
    public static final int getSecondsComponent-impl(final double n) {
        return (int)(getInSeconds-impl(n) % 60);
    }
    
    public static int hashCode-impl(final double value) {
        final long doubleToLongBits = Double.doubleToLongBits(value);
        return (int)(doubleToLongBits ^ doubleToLongBits >>> 32);
    }
    
    public static final boolean isFinite-impl(final double n) {
        return !Double.isInfinite(n) && !Double.isNaN(n);
    }
    
    public static final boolean isInfinite-impl(final double v) {
        return Double.isInfinite(v);
    }
    
    public static final boolean isNegative-impl(final double n) {
        boolean b = false;
        if (n < 0) {
            b = true;
        }
        return b;
    }
    
    public static final boolean isPositive-impl(final double n) {
        boolean b = false;
        if (n > 0) {
            b = true;
        }
        return b;
    }
    
    public static final double minus-LRDsOJo(final double n, final double n2) {
        return constructor-impl(n - n2);
    }
    
    public static final double plus-LRDsOJo(final double n, final double n2) {
        return constructor-impl(n + n2);
    }
    
    private static final int precision-impl(final double n, final double n2) {
        if (n2 < 1) {
            return 3;
        }
        if (n2 < 10) {
            return 2;
        }
        if (n2 < 100) {
            return 1;
        }
        return 0;
    }
    
    public static final double times-impl(final double n, final double n2) {
        return constructor-impl(n * n2);
    }
    
    public static final double times-impl(final double n, final int n2) {
        return constructor-impl(n * n2);
    }
    
    public static final <T> T toComponents-impl(final double n, final Function2<? super Long, ? super Integer, ? extends T> function2) {
        Intrinsics.checkParameterIsNotNull(function2, "action");
        return (T)function2.invoke((long)getInSeconds-impl(n), getNanosecondsComponent-impl(n));
    }
    
    public static final <T> T toComponents-impl(final double n, final Function3<? super Integer, ? super Integer, ? super Integer, ? extends T> function3) {
        Intrinsics.checkParameterIsNotNull(function3, "action");
        return (T)function3.invoke((int)getInMinutes-impl(n), getSecondsComponent-impl(n), getNanosecondsComponent-impl(n));
    }
    
    public static final <T> T toComponents-impl(final double n, final Function4<? super Integer, ? super Integer, ? super Integer, ? super Integer, ? extends T> function4) {
        Intrinsics.checkParameterIsNotNull(function4, "action");
        return (T)function4.invoke((int)getInHours-impl(n), getMinutesComponent-impl(n), getSecondsComponent-impl(n), getNanosecondsComponent-impl(n));
    }
    
    public static final <T> T toComponents-impl(final double n, final Function5<? super Integer, ? super Integer, ? super Integer, ? super Integer, ? super Integer, ? extends T> function5) {
        Intrinsics.checkParameterIsNotNull(function5, "action");
        return (T)function5.invoke((int)getInDays-impl(n), getHoursComponent-impl(n), getMinutesComponent-impl(n), getSecondsComponent-impl(n), getNanosecondsComponent-impl(n));
    }
    
    public static final double toDouble-impl(final double n, final TimeUnit timeUnit) {
        Intrinsics.checkParameterIsNotNull(timeUnit, "unit");
        return DurationUnitKt__DurationUnitJvmKt.convertDurationUnit(n, DurationKt.access$getStorageUnit$p(), timeUnit);
    }
    
    public static final int toInt-impl(final double n, final TimeUnit timeUnit) {
        Intrinsics.checkParameterIsNotNull(timeUnit, "unit");
        return (int)toDouble-impl(n, timeUnit);
    }
    
    public static final String toIsoString-impl(double absoluteValue-impl) {
        final StringBuilder sb = new StringBuilder();
        if (isNegative-impl(absoluteValue-impl)) {
            sb.append('-');
        }
        sb.append("PT");
        absoluteValue-impl = getAbsoluteValue-impl(absoluteValue-impl);
        final int i = (int)getInHours-impl(absoluteValue-impl);
        final int minutesComponent-impl = getMinutesComponent-impl(absoluteValue-impl);
        final int secondsComponent-impl = getSecondsComponent-impl(absoluteValue-impl);
        final int nanosecondsComponent-impl = getNanosecondsComponent-impl(absoluteValue-impl);
        final int n = 1;
        final boolean b = i != 0;
        final boolean b2 = secondsComponent-impl != 0 || nanosecondsComponent-impl != 0;
        int n2 = n;
        if (minutesComponent-impl == 0) {
            if (b2 && b) {
                n2 = n;
            }
            else {
                n2 = 0;
            }
        }
        if (b) {
            sb.append(i);
            sb.append('H');
        }
        if (n2 != 0) {
            sb.append(minutesComponent-impl);
            sb.append('M');
        }
        if (b2 || (!b && n2 == 0)) {
            sb.append(secondsComponent-impl);
            if (nanosecondsComponent-impl != 0) {
                sb.append('.');
                final String padStart = StringsKt__StringsKt.padStart(String.valueOf(nanosecondsComponent-impl), 9, '0');
                if (nanosecondsComponent-impl % 1000000 == 0) {
                    sb.append(padStart, 0, 3);
                }
                else if (nanosecondsComponent-impl % 1000 == 0) {
                    sb.append(padStart, 0, 6);
                }
                else {
                    sb.append(padStart);
                }
            }
            sb.append('S');
        }
        final String string = sb.toString();
        Intrinsics.checkExpressionValueIsNotNull(string, "StringBuilder().apply(builderAction).toString()");
        return string;
    }
    
    public static final long toLong-impl(final double n, final TimeUnit timeUnit) {
        Intrinsics.checkParameterIsNotNull(timeUnit, "unit");
        return (long)toDouble-impl(n, timeUnit);
    }
    
    public static final long toLongMilliseconds-impl(final double n) {
        return toLong-impl(n, TimeUnit.MILLISECONDS);
    }
    
    public static final long toLongNanoseconds-impl(final double n) {
        return toLong-impl(n, TimeUnit.NANOSECONDS);
    }
    
    public static String toString-impl(final double d) {
        if (isInfinite-impl(d)) {
            return String.valueOf(d);
        }
        if (d == 0.0) {
            return "0s";
        }
        final double inNanoseconds-impl = getInNanoseconds-impl(getAbsoluteValue-impl(d));
        boolean b = false;
        TimeUnit timeUnit = null;
        int n = 0;
        Label_0197: {
            Label_0046: {
                if (inNanoseconds-impl < 1.0E-6) {
                    timeUnit = TimeUnit.SECONDS;
                }
                else {
                    if (inNanoseconds-impl < 1) {
                        timeUnit = TimeUnit.NANOSECONDS;
                        n = 7;
                        break Label_0197;
                    }
                    if (inNanoseconds-impl < 1000.0) {
                        timeUnit = TimeUnit.NANOSECONDS;
                    }
                    else if (inNanoseconds-impl < 1000000.0) {
                        timeUnit = TimeUnit.MICROSECONDS;
                    }
                    else if (inNanoseconds-impl < 1.0E9) {
                        timeUnit = TimeUnit.MILLISECONDS;
                    }
                    else if (inNanoseconds-impl < 1.0E12) {
                        timeUnit = TimeUnit.SECONDS;
                    }
                    else if (inNanoseconds-impl < 6.0E13) {
                        timeUnit = TimeUnit.MINUTES;
                    }
                    else if (inNanoseconds-impl < 3.6E15) {
                        timeUnit = TimeUnit.HOURS;
                    }
                    else {
                        if (inNanoseconds-impl >= 8.64E20) {
                            timeUnit = TimeUnit.DAYS;
                            break Label_0046;
                        }
                        timeUnit = TimeUnit.DAYS;
                    }
                    n = 0;
                    break Label_0197;
                }
            }
            n = 0;
            b = true;
        }
        final double double-impl = toDouble-impl(d, timeUnit);
        final StringBuilder sb = new StringBuilder();
        String str;
        if (b) {
            str = FormatToDecimalsKt.formatScientific(double-impl);
        }
        else if (n > 0) {
            str = FormatToDecimalsKt.formatUpToDecimals(double-impl, n);
        }
        else {
            str = FormatToDecimalsKt.formatToExactDecimals(double-impl, precision-impl(d, Math.abs(double-impl)));
        }
        sb.append(str);
        sb.append(DurationUnitKt__DurationUnitKt.shortName(timeUnit));
        return sb.toString();
    }
    
    public static final String toString-impl(double double-impl, final TimeUnit timeUnit, final int i) {
        Intrinsics.checkParameterIsNotNull(timeUnit, "unit");
        if (i < 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("decimals must be not negative, but was ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString().toString());
        }
        if (isInfinite-impl(double-impl)) {
            return String.valueOf(double-impl);
        }
        double-impl = toDouble-impl(double-impl, timeUnit);
        final StringBuilder sb2 = new StringBuilder();
        String str;
        if (Math.abs(double-impl) < 1.0E14) {
            str = FormatToDecimalsKt.formatToExactDecimals(double-impl, RangesKt___RangesKt.coerceAtMost(i, 12));
        }
        else {
            str = FormatToDecimalsKt.formatScientific(double-impl);
        }
        sb2.append(str);
        sb2.append(DurationUnitKt__DurationUnitKt.shortName(timeUnit));
        return sb2.toString();
    }
    
    public static final double unaryMinus-impl(final double n) {
        return constructor-impl(-n);
    }
    
    public int compareTo-LRDsOJo(final double n) {
        return compareTo-LRDsOJo(this.value, n);
    }
    
    @Override
    public boolean equals(final Object o) {
        return equals-impl(this.value, o);
    }
    
    @Override
    public int hashCode() {
        return hashCode-impl(this.value);
    }
    
    @Override
    public String toString() {
        return toString-impl(this.value);
    }
    
    public final /* synthetic */ double unbox-impl() {
        return this.value;
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0006\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J&\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u000b2\n\u0010\r\u001a\u00060\u000ej\u0002`\u000f2\n\u0010\u0010\u001a\u00060\u000ej\u0002`\u000fR\u0016\u0010\u0003\u001a\u00020\u0004\u00f8\u0001\u0000¢\u0006\n\n\u0002\u0010\u0007\u001a\u0004\b\u0005\u0010\u0006R\u0016\u0010\b\u001a\u00020\u0004\u00f8\u0001\u0000¢\u0006\n\n\u0002\u0010\u0007\u001a\u0004\b\t\u0010\u0006\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0011" }, d2 = { "Lkotlin/time/Duration$Companion;", "", "()V", "INFINITE", "Lkotlin/time/Duration;", "getINFINITE", "()D", "D", "ZERO", "getZERO", "convert", "", "value", "sourceUnit", "Ljava/util/concurrent/TimeUnit;", "Lkotlin/time/DurationUnit;", "targetUnit", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
    public static final class Companion
    {
        private Companion() {
        }
        
        public final double convert(final double n, final TimeUnit timeUnit, final TimeUnit timeUnit2) {
            Intrinsics.checkParameterIsNotNull(timeUnit, "sourceUnit");
            Intrinsics.checkParameterIsNotNull(timeUnit2, "targetUnit");
            return DurationUnitKt__DurationUnitJvmKt.convertDurationUnit(n, timeUnit, timeUnit2);
        }
        
        public final double getINFINITE() {
            return Duration.access$getINFINITE$cp();
        }
        
        public final double getZERO() {
            return Duration.access$getZERO$cp();
        }
    }
}
