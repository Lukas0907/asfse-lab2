// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.time;

import kotlin.jvm.internal.Intrinsics;
import java.math.RoundingMode;
import java.util.Locale;
import java.text.DecimalFormatSymbols;
import java.text.DecimalFormat;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000.\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0006\n\u0002\b\u0003\u001a\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000bH\u0002\u001a\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0000\u001a\u0018\u0010\u0010\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\n\u001a\u00020\u000bH\u0000\u001a\u0018\u0010\u0011\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\n\u001a\u00020\u000bH\u0000\"\u001c\u0010\u0000\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u0001X\u0082\u0004¢\u0006\u0004\n\u0002\u0010\u0004\"\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0006X\u0082\u0004¢\u0006\u0002\n\u0000\"\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012" }, d2 = { "precisionFormats", "", "Ljava/lang/ThreadLocal;", "Ljava/text/DecimalFormat;", "[Ljava/lang/ThreadLocal;", "rootNegativeExpFormatSymbols", "Ljava/text/DecimalFormatSymbols;", "rootPositiveExpFormatSymbols", "scientificFormat", "createFormatForDecimals", "decimals", "", "formatScientific", "", "value", "", "formatToExactDecimals", "formatUpToDecimals", "kotlin-stdlib" }, k = 2, mv = { 1, 1, 15 })
public final class FormatToDecimalsKt
{
    private static final ThreadLocal<DecimalFormat>[] precisionFormats;
    private static final DecimalFormatSymbols rootNegativeExpFormatSymbols;
    private static final DecimalFormatSymbols rootPositiveExpFormatSymbols;
    private static final ThreadLocal<DecimalFormat> scientificFormat;
    
    static {
        final DecimalFormatSymbols rootNegativeExpFormatSymbols2 = new DecimalFormatSymbols(Locale.ROOT);
        rootNegativeExpFormatSymbols2.setExponentSeparator("e");
        rootNegativeExpFormatSymbols = rootNegativeExpFormatSymbols2;
        final DecimalFormatSymbols rootPositiveExpFormatSymbols2 = new DecimalFormatSymbols(Locale.ROOT);
        rootPositiveExpFormatSymbols2.setExponentSeparator("e+");
        rootPositiveExpFormatSymbols = rootPositiveExpFormatSymbols2;
        final ThreadLocal[] precisionFormats2 = new ThreadLocal[4];
        for (int i = 0; i < 4; ++i) {
            precisionFormats2[i] = new ThreadLocal<DecimalFormat>();
        }
        precisionFormats = precisionFormats2;
        scientificFormat = new ThreadLocal<DecimalFormat>();
    }
    
    private static final DecimalFormat createFormatForDecimals(final int minimumFractionDigits) {
        final DecimalFormat decimalFormat = new DecimalFormat("0", FormatToDecimalsKt.rootNegativeExpFormatSymbols);
        if (minimumFractionDigits > 0) {
            decimalFormat.setMinimumFractionDigits(minimumFractionDigits);
        }
        decimalFormat.setRoundingMode(RoundingMode.HALF_UP);
        return decimalFormat;
    }
    
    public static final String formatScientific(final double number) {
        final ThreadLocal<DecimalFormat> scientificFormat = FormatToDecimalsKt.scientificFormat;
        DecimalFormat value = scientificFormat.get();
        if (value == null) {
            value = new DecimalFormat("0E0", FormatToDecimalsKt.rootNegativeExpFormatSymbols);
            value.setMinimumFractionDigits(2);
            scientificFormat.set(value);
        }
        final DecimalFormat decimalFormat = value;
        DecimalFormatSymbols decimalFormatSymbols;
        if (number < 1 && number > -1) {
            decimalFormatSymbols = FormatToDecimalsKt.rootNegativeExpFormatSymbols;
        }
        else {
            decimalFormatSymbols = FormatToDecimalsKt.rootPositiveExpFormatSymbols;
        }
        decimalFormat.setDecimalFormatSymbols(decimalFormatSymbols);
        final String format = decimalFormat.format(number);
        Intrinsics.checkExpressionValueIsNotNull(format, "scientificFormat.getOrSe\u2026 }\n        .format(value)");
        return format;
    }
    
    public static final String formatToExactDecimals(final double number, final int n) {
        final ThreadLocal<DecimalFormat>[] precisionFormats = FormatToDecimalsKt.precisionFormats;
        DecimalFormat formatForDecimals;
        if (n < precisionFormats.length) {
            final ThreadLocal<DecimalFormat> threadLocal = precisionFormats[n];
            DecimalFormat value = threadLocal.get();
            if (value == null) {
                value = createFormatForDecimals(n);
                threadLocal.set(value);
            }
            formatForDecimals = value;
        }
        else {
            formatForDecimals = createFormatForDecimals(n);
        }
        final String format = formatForDecimals.format(number);
        Intrinsics.checkExpressionValueIsNotNull(format, "format.format(value)");
        return format;
    }
    
    public static final String formatUpToDecimals(final double number, final int maximumFractionDigits) {
        final DecimalFormat formatForDecimals = createFormatForDecimals(0);
        formatForDecimals.setMaximumFractionDigits(maximumFractionDigits);
        final String format = formatForDecimals.format(number);
        Intrinsics.checkExpressionValueIsNotNull(format, "createFormatForDecimals(\u2026 }\n        .format(value)");
        return format;
    }
}
