// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.time;

import java.util.concurrent.TimeUnit;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0007\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002\u00f8\u0001\u0000¢\u0006\u0004\b\t\u0010\nJ\u001b\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0086\u0002\u00f8\u0001\u0000¢\u0006\u0004\b\f\u0010\nJ\b\u0010\r\u001a\u00020\u0004H\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e¢\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u000e" }, d2 = { "Lkotlin/time/TestClock;", "Lkotlin/time/AbstractLongClock;", "()V", "reading", "", "overflow", "", "duration", "Lkotlin/time/Duration;", "overflow-LRDsOJo", "(D)V", "plusAssign", "plusAssign-LRDsOJo", "read", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
public final class TestClock extends AbstractLongClock
{
    private long reading;
    
    public TestClock() {
        super(TimeUnit.NANOSECONDS);
    }
    
    private final void overflow-LRDsOJo(final double n) {
        final StringBuilder sb = new StringBuilder();
        sb.append("TestClock will overflow if its reading ");
        sb.append(this.reading);
        sb.append("ns is advanced by ");
        sb.append(Duration.toString-impl(n));
        sb.append('.');
        throw new IllegalStateException(sb.toString());
    }
    
    public final void plusAssign-LRDsOJo(final double n) {
        final double double-impl = Duration.toDouble-impl(n, this.getUnit());
        final long n2 = (long)double-impl;
        long reading2;
        if (n2 != Long.MIN_VALUE && n2 != Long.MAX_VALUE) {
            final long reading = this.reading;
            final long n3 = reading2 = reading + n2;
            if ((n2 ^ reading) >= 0L) {
                reading2 = n3;
                if ((reading ^ n3) < 0L) {
                    this.overflow-LRDsOJo(n);
                    reading2 = n3;
                }
            }
        }
        else {
            final double n4 = this.reading + double-impl;
            if (n4 > Long.MAX_VALUE || n4 < Long.MIN_VALUE) {
                this.overflow-LRDsOJo(n);
            }
            reading2 = (long)n4;
        }
        this.reading = reading2;
    }
    
    @Override
    protected long read() {
        return this.reading;
    }
}
