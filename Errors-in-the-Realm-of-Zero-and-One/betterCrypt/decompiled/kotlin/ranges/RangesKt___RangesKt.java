// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.ranges;

import kotlin.random.RandomKt;
import java.util.NoSuchElementException;
import kotlin.random.Random;
import kotlin.Deprecated;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000n\n\u0002\b\u0002\n\u0002\u0010\u000f\n\u0002\b\u0002\n\u0002\u0010\u0005\n\u0002\u0010\u0006\n\u0002\u0010\u0007\n\u0002\u0010\b\n\u0002\u0010\t\n\u0002\u0010\n\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\f\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0016\u001a'\u0010\u0000\u001a\u0002H\u0001\"\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u0002*\u0002H\u00012\u0006\u0010\u0003\u001a\u0002H\u0001¢\u0006\u0002\u0010\u0004\u001a\u0012\u0010\u0000\u001a\u00020\u0005*\u00020\u00052\u0006\u0010\u0003\u001a\u00020\u0005\u001a\u0012\u0010\u0000\u001a\u00020\u0006*\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u0006\u001a\u0012\u0010\u0000\u001a\u00020\u0007*\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u0007\u001a\u0012\u0010\u0000\u001a\u00020\b*\u00020\b2\u0006\u0010\u0003\u001a\u00020\b\u001a\u0012\u0010\u0000\u001a\u00020\t*\u00020\t2\u0006\u0010\u0003\u001a\u00020\t\u001a\u0012\u0010\u0000\u001a\u00020\n*\u00020\n2\u0006\u0010\u0003\u001a\u00020\n\u001a'\u0010\u000b\u001a\u0002H\u0001\"\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u0002*\u0002H\u00012\u0006\u0010\f\u001a\u0002H\u0001¢\u0006\u0002\u0010\u0004\u001a\u0012\u0010\u000b\u001a\u00020\u0005*\u00020\u00052\u0006\u0010\f\u001a\u00020\u0005\u001a\u0012\u0010\u000b\u001a\u00020\u0006*\u00020\u00062\u0006\u0010\f\u001a\u00020\u0006\u001a\u0012\u0010\u000b\u001a\u00020\u0007*\u00020\u00072\u0006\u0010\f\u001a\u00020\u0007\u001a\u0012\u0010\u000b\u001a\u00020\b*\u00020\b2\u0006\u0010\f\u001a\u00020\b\u001a\u0012\u0010\u000b\u001a\u00020\t*\u00020\t2\u0006\u0010\f\u001a\u00020\t\u001a\u0012\u0010\u000b\u001a\u00020\n*\u00020\n2\u0006\u0010\f\u001a\u00020\n\u001a3\u0010\r\u001a\u0002H\u0001\"\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u0002*\u0002H\u00012\b\u0010\u0003\u001a\u0004\u0018\u0001H\u00012\b\u0010\f\u001a\u0004\u0018\u0001H\u0001¢\u0006\u0002\u0010\u000e\u001a/\u0010\r\u001a\u0002H\u0001\"\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u0002*\u0002H\u00012\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\u00010\u0010H\u0007¢\u0006\u0002\u0010\u0011\u001a-\u0010\r\u001a\u0002H\u0001\"\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u0002*\u0002H\u00012\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\u00010\u0012¢\u0006\u0002\u0010\u0013\u001a\u001a\u0010\r\u001a\u00020\u0005*\u00020\u00052\u0006\u0010\u0003\u001a\u00020\u00052\u0006\u0010\f\u001a\u00020\u0005\u001a\u001a\u0010\r\u001a\u00020\u0006*\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\u0006\u001a\u001a\u0010\r\u001a\u00020\u0007*\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00072\u0006\u0010\f\u001a\u00020\u0007\u001a\u001a\u0010\r\u001a\u00020\b*\u00020\b2\u0006\u0010\u0003\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\b\u001a\u0018\u0010\r\u001a\u00020\b*\u00020\b2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\b0\u0012\u001a\u001a\u0010\r\u001a\u00020\t*\u00020\t2\u0006\u0010\u0003\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\t\u001a\u0018\u0010\r\u001a\u00020\t*\u00020\t2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\t0\u0012\u001a\u001a\u0010\r\u001a\u00020\n*\u00020\n2\u0006\u0010\u0003\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\n\u001a\u001c\u0010\u0014\u001a\u00020\u0015*\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0087\n¢\u0006\u0002\u0010\u0019\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00050\u00122\u0006\u0010\u001a\u001a\u00020\u0006H\u0087\u0002¢\u0006\u0002\b\u001b\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00050\u00122\u0006\u0010\u001a\u001a\u00020\u0007H\u0087\u0002¢\u0006\u0002\b\u001b\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00050\u00122\u0006\u0010\u001a\u001a\u00020\bH\u0087\u0002¢\u0006\u0002\b\u001b\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00050\u00122\u0006\u0010\u001a\u001a\u00020\tH\u0087\u0002¢\u0006\u0002\b\u001b\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00050\u00122\u0006\u0010\u001a\u001a\u00020\nH\u0087\u0002¢\u0006\u0002\b\u001b\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00060\u00122\u0006\u0010\u001a\u001a\u00020\u0005H\u0087\u0002¢\u0006\u0002\b\u001c\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00060\u00122\u0006\u0010\u001a\u001a\u00020\u0007H\u0087\u0002¢\u0006\u0002\b\u001c\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00060\u00122\u0006\u0010\u001a\u001a\u00020\bH\u0087\u0002¢\u0006\u0002\b\u001c\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00060\u00122\u0006\u0010\u001a\u001a\u00020\tH\u0087\u0002¢\u0006\u0002\b\u001c\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00060\u00122\u0006\u0010\u001a\u001a\u00020\nH\u0087\u0002¢\u0006\u0002\b\u001c\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00070\u00122\u0006\u0010\u001a\u001a\u00020\u0005H\u0087\u0002¢\u0006\u0002\b\u001d\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00070\u00122\u0006\u0010\u001a\u001a\u00020\u0006H\u0087\u0002¢\u0006\u0002\b\u001d\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00070\u00122\u0006\u0010\u001a\u001a\u00020\bH\u0087\u0002¢\u0006\u0002\b\u001d\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00070\u00122\u0006\u0010\u001a\u001a\u00020\tH\u0087\u0002¢\u0006\u0002\b\u001d\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\u00070\u00122\u0006\u0010\u001a\u001a\u00020\nH\u0087\u0002¢\u0006\u0002\b\u001d\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\b0\u00122\u0006\u0010\u001a\u001a\u00020\u0005H\u0087\u0002¢\u0006\u0002\b\u001e\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\b0\u00122\u0006\u0010\u001a\u001a\u00020\u0006H\u0087\u0002¢\u0006\u0002\b\u001e\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\b0\u00122\u0006\u0010\u001a\u001a\u00020\u0007H\u0087\u0002¢\u0006\u0002\b\u001e\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\b0\u00122\u0006\u0010\u001a\u001a\u00020\tH\u0087\u0002¢\u0006\u0002\b\u001e\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\b0\u00122\u0006\u0010\u001a\u001a\u00020\nH\u0087\u0002¢\u0006\u0002\b\u001e\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\t0\u00122\u0006\u0010\u001a\u001a\u00020\u0005H\u0087\u0002¢\u0006\u0002\b\u001f\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\t0\u00122\u0006\u0010\u001a\u001a\u00020\u0006H\u0087\u0002¢\u0006\u0002\b\u001f\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\t0\u00122\u0006\u0010\u001a\u001a\u00020\u0007H\u0087\u0002¢\u0006\u0002\b\u001f\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\t0\u00122\u0006\u0010\u001a\u001a\u00020\bH\u0087\u0002¢\u0006\u0002\b\u001f\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\t0\u00122\u0006\u0010\u001a\u001a\u00020\nH\u0087\u0002¢\u0006\u0002\b\u001f\u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\n0\u00122\u0006\u0010\u001a\u001a\u00020\u0005H\u0087\u0002¢\u0006\u0002\b \u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\n0\u00122\u0006\u0010\u001a\u001a\u00020\u0006H\u0087\u0002¢\u0006\u0002\b \u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\n0\u00122\u0006\u0010\u001a\u001a\u00020\u0007H\u0087\u0002¢\u0006\u0002\b \u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\n0\u00122\u0006\u0010\u001a\u001a\u00020\bH\u0087\u0002¢\u0006\u0002\b \u001a \u0010\u0014\u001a\u00020\u0015*\b\u0012\u0004\u0012\u00020\n0\u00122\u0006\u0010\u001a\u001a\u00020\tH\u0087\u0002¢\u0006\u0002\b \u001a\u001c\u0010\u0014\u001a\u00020\u0015*\u00020!2\b\u0010\u0017\u001a\u0004\u0018\u00010\bH\u0087\n¢\u0006\u0002\u0010\"\u001a\u001c\u0010\u0014\u001a\u00020\u0015*\u00020#2\b\u0010\u0017\u001a\u0004\u0018\u00010\tH\u0087\n¢\u0006\u0002\u0010$\u001a\u0015\u0010%\u001a\u00020&*\u00020\u00052\u0006\u0010'\u001a\u00020\u0005H\u0086\u0004\u001a\u0015\u0010%\u001a\u00020&*\u00020\u00052\u0006\u0010'\u001a\u00020\bH\u0086\u0004\u001a\u0015\u0010%\u001a\u00020(*\u00020\u00052\u0006\u0010'\u001a\u00020\tH\u0086\u0004\u001a\u0015\u0010%\u001a\u00020&*\u00020\u00052\u0006\u0010'\u001a\u00020\nH\u0086\u0004\u001a\u0015\u0010%\u001a\u00020)*\u00020\u00182\u0006\u0010'\u001a\u00020\u0018H\u0086\u0004\u001a\u0015\u0010%\u001a\u00020&*\u00020\b2\u0006\u0010'\u001a\u00020\u0005H\u0086\u0004\u001a\u0015\u0010%\u001a\u00020&*\u00020\b2\u0006\u0010'\u001a\u00020\bH\u0086\u0004\u001a\u0015\u0010%\u001a\u00020(*\u00020\b2\u0006\u0010'\u001a\u00020\tH\u0086\u0004\u001a\u0015\u0010%\u001a\u00020&*\u00020\b2\u0006\u0010'\u001a\u00020\nH\u0086\u0004\u001a\u0015\u0010%\u001a\u00020(*\u00020\t2\u0006\u0010'\u001a\u00020\u0005H\u0086\u0004\u001a\u0015\u0010%\u001a\u00020(*\u00020\t2\u0006\u0010'\u001a\u00020\bH\u0086\u0004\u001a\u0015\u0010%\u001a\u00020(*\u00020\t2\u0006\u0010'\u001a\u00020\tH\u0086\u0004\u001a\u0015\u0010%\u001a\u00020(*\u00020\t2\u0006\u0010'\u001a\u00020\nH\u0086\u0004\u001a\u0015\u0010%\u001a\u00020&*\u00020\n2\u0006\u0010'\u001a\u00020\u0005H\u0086\u0004\u001a\u0015\u0010%\u001a\u00020&*\u00020\n2\u0006\u0010'\u001a\u00020\bH\u0086\u0004\u001a\u0015\u0010%\u001a\u00020(*\u00020\n2\u0006\u0010'\u001a\u00020\tH\u0086\u0004\u001a\u0015\u0010%\u001a\u00020&*\u00020\n2\u0006\u0010'\u001a\u00020\nH\u0086\u0004\u001a\r\u0010*\u001a\u00020\u0018*\u00020\u0016H\u0087\b\u001a\u0014\u0010*\u001a\u00020\u0018*\u00020\u00162\u0006\u0010*\u001a\u00020+H\u0007\u001a\r\u0010*\u001a\u00020\b*\u00020!H\u0087\b\u001a\u0014\u0010*\u001a\u00020\b*\u00020!2\u0006\u0010*\u001a\u00020+H\u0007\u001a\r\u0010*\u001a\u00020\t*\u00020#H\u0087\b\u001a\u0014\u0010*\u001a\u00020\t*\u00020#2\u0006\u0010*\u001a\u00020+H\u0007\u001a\n\u0010,\u001a\u00020)*\u00020)\u001a\n\u0010,\u001a\u00020&*\u00020&\u001a\n\u0010,\u001a\u00020(*\u00020(\u001a\u0015\u0010-\u001a\u00020)*\u00020)2\u0006\u0010-\u001a\u00020\bH\u0086\u0004\u001a\u0015\u0010-\u001a\u00020&*\u00020&2\u0006\u0010-\u001a\u00020\bH\u0086\u0004\u001a\u0015\u0010-\u001a\u00020(*\u00020(2\u0006\u0010-\u001a\u00020\tH\u0086\u0004\u001a\u0013\u0010.\u001a\u0004\u0018\u00010\u0005*\u00020\u0006H\u0000¢\u0006\u0002\u0010/\u001a\u0013\u0010.\u001a\u0004\u0018\u00010\u0005*\u00020\u0007H\u0000¢\u0006\u0002\u00100\u001a\u0013\u0010.\u001a\u0004\u0018\u00010\u0005*\u00020\bH\u0000¢\u0006\u0002\u00101\u001a\u0013\u0010.\u001a\u0004\u0018\u00010\u0005*\u00020\tH\u0000¢\u0006\u0002\u00102\u001a\u0013\u0010.\u001a\u0004\u0018\u00010\u0005*\u00020\nH\u0000¢\u0006\u0002\u00103\u001a\u0013\u00104\u001a\u0004\u0018\u00010\b*\u00020\u0006H\u0000¢\u0006\u0002\u00105\u001a\u0013\u00104\u001a\u0004\u0018\u00010\b*\u00020\u0007H\u0000¢\u0006\u0002\u00106\u001a\u0013\u00104\u001a\u0004\u0018\u00010\b*\u00020\tH\u0000¢\u0006\u0002\u00107\u001a\u0013\u00108\u001a\u0004\u0018\u00010\t*\u00020\u0006H\u0000¢\u0006\u0002\u00109\u001a\u0013\u00108\u001a\u0004\u0018\u00010\t*\u00020\u0007H\u0000¢\u0006\u0002\u0010:\u001a\u0013\u0010;\u001a\u0004\u0018\u00010\n*\u00020\u0006H\u0000¢\u0006\u0002\u0010<\u001a\u0013\u0010;\u001a\u0004\u0018\u00010\n*\u00020\u0007H\u0000¢\u0006\u0002\u0010=\u001a\u0013\u0010;\u001a\u0004\u0018\u00010\n*\u00020\bH\u0000¢\u0006\u0002\u0010>\u001a\u0013\u0010;\u001a\u0004\u0018\u00010\n*\u00020\tH\u0000¢\u0006\u0002\u0010?\u001a\u0015\u0010@\u001a\u00020!*\u00020\u00052\u0006\u0010'\u001a\u00020\u0005H\u0086\u0004\u001a\u0015\u0010@\u001a\u00020!*\u00020\u00052\u0006\u0010'\u001a\u00020\bH\u0086\u0004\u001a\u0015\u0010@\u001a\u00020#*\u00020\u00052\u0006\u0010'\u001a\u00020\tH\u0086\u0004\u001a\u0015\u0010@\u001a\u00020!*\u00020\u00052\u0006\u0010'\u001a\u00020\nH\u0086\u0004\u001a\u0015\u0010@\u001a\u00020\u0016*\u00020\u00182\u0006\u0010'\u001a\u00020\u0018H\u0086\u0004\u001a\u0015\u0010@\u001a\u00020!*\u00020\b2\u0006\u0010'\u001a\u00020\u0005H\u0086\u0004\u001a\u0015\u0010@\u001a\u00020!*\u00020\b2\u0006\u0010'\u001a\u00020\bH\u0086\u0004\u001a\u0015\u0010@\u001a\u00020#*\u00020\b2\u0006\u0010'\u001a\u00020\tH\u0086\u0004\u001a\u0015\u0010@\u001a\u00020!*\u00020\b2\u0006\u0010'\u001a\u00020\nH\u0086\u0004\u001a\u0015\u0010@\u001a\u00020#*\u00020\t2\u0006\u0010'\u001a\u00020\u0005H\u0086\u0004\u001a\u0015\u0010@\u001a\u00020#*\u00020\t2\u0006\u0010'\u001a\u00020\bH\u0086\u0004\u001a\u0015\u0010@\u001a\u00020#*\u00020\t2\u0006\u0010'\u001a\u00020\tH\u0086\u0004\u001a\u0015\u0010@\u001a\u00020#*\u00020\t2\u0006\u0010'\u001a\u00020\nH\u0086\u0004\u001a\u0015\u0010@\u001a\u00020!*\u00020\n2\u0006\u0010'\u001a\u00020\u0005H\u0086\u0004\u001a\u0015\u0010@\u001a\u00020!*\u00020\n2\u0006\u0010'\u001a\u00020\bH\u0086\u0004\u001a\u0015\u0010@\u001a\u00020#*\u00020\n2\u0006\u0010'\u001a\u00020\tH\u0086\u0004\u001a\u0015\u0010@\u001a\u00020!*\u00020\n2\u0006\u0010'\u001a\u00020\nH\u0086\u0004¨\u0006A" }, d2 = { "coerceAtLeast", "T", "", "minimumValue", "(Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;", "", "", "", "", "", "", "coerceAtMost", "maximumValue", "coerceIn", "(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;)Ljava/lang/Comparable;", "range", "Lkotlin/ranges/ClosedFloatingPointRange;", "(Ljava/lang/Comparable;Lkotlin/ranges/ClosedFloatingPointRange;)Ljava/lang/Comparable;", "Lkotlin/ranges/ClosedRange;", "(Ljava/lang/Comparable;Lkotlin/ranges/ClosedRange;)Ljava/lang/Comparable;", "contains", "", "Lkotlin/ranges/CharRange;", "element", "", "(Lkotlin/ranges/CharRange;Ljava/lang/Character;)Z", "value", "byteRangeContains", "doubleRangeContains", "floatRangeContains", "intRangeContains", "longRangeContains", "shortRangeContains", "Lkotlin/ranges/IntRange;", "(Lkotlin/ranges/IntRange;Ljava/lang/Integer;)Z", "Lkotlin/ranges/LongRange;", "(Lkotlin/ranges/LongRange;Ljava/lang/Long;)Z", "downTo", "Lkotlin/ranges/IntProgression;", "to", "Lkotlin/ranges/LongProgression;", "Lkotlin/ranges/CharProgression;", "random", "Lkotlin/random/Random;", "reversed", "step", "toByteExactOrNull", "(D)Ljava/lang/Byte;", "(F)Ljava/lang/Byte;", "(I)Ljava/lang/Byte;", "(J)Ljava/lang/Byte;", "(S)Ljava/lang/Byte;", "toIntExactOrNull", "(D)Ljava/lang/Integer;", "(F)Ljava/lang/Integer;", "(J)Ljava/lang/Integer;", "toLongExactOrNull", "(D)Ljava/lang/Long;", "(F)Ljava/lang/Long;", "toShortExactOrNull", "(D)Ljava/lang/Short;", "(F)Ljava/lang/Short;", "(I)Ljava/lang/Short;", "(J)Ljava/lang/Short;", "until", "kotlin-stdlib" }, k = 5, mv = { 1, 1, 15 }, xi = 1, xs = "kotlin/ranges/RangesKt")
class RangesKt___RangesKt extends RangesKt__RangesKt
{
    public RangesKt___RangesKt() {
    }
    
    @Deprecated(message = "This `contains` operation mixing integer and floating point arguments has ambiguous semantics and is going to be removed.")
    public static final boolean byteRangeContains(final ClosedRange<Byte> closedRange, final double n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        final Byte byteExactOrNull = toByteExactOrNull(n);
        return byteExactOrNull != null && closedRange.contains(byteExactOrNull);
    }
    
    @Deprecated(message = "This `contains` operation mixing integer and floating point arguments has ambiguous semantics and is going to be removed.")
    public static final boolean byteRangeContains(final ClosedRange<Byte> closedRange, final float n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        final Byte byteExactOrNull = toByteExactOrNull(n);
        return byteExactOrNull != null && closedRange.contains(byteExactOrNull);
    }
    
    public static final boolean byteRangeContains(final ClosedRange<Byte> closedRange, final int n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        final Byte byteExactOrNull = toByteExactOrNull(n);
        return byteExactOrNull != null && closedRange.contains(byteExactOrNull);
    }
    
    public static final boolean byteRangeContains(final ClosedRange<Byte> closedRange, final long n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        final Byte byteExactOrNull = toByteExactOrNull(n);
        return byteExactOrNull != null && closedRange.contains(byteExactOrNull);
    }
    
    public static final boolean byteRangeContains(final ClosedRange<Byte> closedRange, final short n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        final Byte byteExactOrNull = toByteExactOrNull(n);
        return byteExactOrNull != null && closedRange.contains(byteExactOrNull);
    }
    
    public static final byte coerceAtLeast(final byte b, final byte b2) {
        byte b3 = b;
        if (b < b2) {
            b3 = b2;
        }
        return b3;
    }
    
    public static final double coerceAtLeast(final double n, final double n2) {
        double n3 = n;
        if (n < n2) {
            n3 = n2;
        }
        return n3;
    }
    
    public static final float coerceAtLeast(final float n, final float n2) {
        float n3 = n;
        if (n < n2) {
            n3 = n2;
        }
        return n3;
    }
    
    public static final int coerceAtLeast(final int n, final int n2) {
        int n3 = n;
        if (n < n2) {
            n3 = n2;
        }
        return n3;
    }
    
    public static final long coerceAtLeast(final long n, final long n2) {
        long n3 = n;
        if (n < n2) {
            n3 = n2;
        }
        return n3;
    }
    
    public static final <T extends Comparable<? super T>> T coerceAtLeast(final T t, final T t2) {
        Intrinsics.checkParameterIsNotNull(t, "$this$coerceAtLeast");
        Intrinsics.checkParameterIsNotNull(t2, "minimumValue");
        Comparable<? super T> comparable = t;
        if (t.compareTo((Object)t2) < 0) {
            comparable = t2;
        }
        return (T)comparable;
    }
    
    public static final short coerceAtLeast(final short n, final short n2) {
        short n3 = n;
        if (n < n2) {
            n3 = n2;
        }
        return n3;
    }
    
    public static final byte coerceAtMost(final byte b, final byte b2) {
        byte b3 = b;
        if (b > b2) {
            b3 = b2;
        }
        return b3;
    }
    
    public static final double coerceAtMost(final double n, final double n2) {
        double n3 = n;
        if (n > n2) {
            n3 = n2;
        }
        return n3;
    }
    
    public static final float coerceAtMost(final float n, final float n2) {
        float n3 = n;
        if (n > n2) {
            n3 = n2;
        }
        return n3;
    }
    
    public static final int coerceAtMost(final int n, final int n2) {
        int n3 = n;
        if (n > n2) {
            n3 = n2;
        }
        return n3;
    }
    
    public static final long coerceAtMost(final long n, final long n2) {
        long n3 = n;
        if (n > n2) {
            n3 = n2;
        }
        return n3;
    }
    
    public static final <T extends Comparable<? super T>> T coerceAtMost(final T t, final T t2) {
        Intrinsics.checkParameterIsNotNull(t, "$this$coerceAtMost");
        Intrinsics.checkParameterIsNotNull(t2, "maximumValue");
        Comparable<? super T> comparable = t;
        if (t.compareTo((Object)t2) > 0) {
            comparable = t2;
        }
        return (T)comparable;
    }
    
    public static final short coerceAtMost(final short n, final short n2) {
        short n3 = n;
        if (n > n2) {
            n3 = n2;
        }
        return n3;
    }
    
    public static final byte coerceIn(final byte b, final byte i, final byte j) {
        if (i > j) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot coerce value to an empty range: maximum ");
            sb.append(j);
            sb.append(" is less than minimum ");
            sb.append(i);
            sb.append('.');
            throw new IllegalArgumentException(sb.toString());
        }
        if (b < i) {
            return i;
        }
        if (b > j) {
            return j;
        }
        return b;
    }
    
    public static final double coerceIn(final double n, final double d, final double d2) {
        if (d > d2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot coerce value to an empty range: maximum ");
            sb.append(d2);
            sb.append(" is less than minimum ");
            sb.append(d);
            sb.append('.');
            throw new IllegalArgumentException(sb.toString());
        }
        if (n < d) {
            return d;
        }
        if (n > d2) {
            return d2;
        }
        return n;
    }
    
    public static final float coerceIn(final float n, final float f, final float f2) {
        if (f > f2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot coerce value to an empty range: maximum ");
            sb.append(f2);
            sb.append(" is less than minimum ");
            sb.append(f);
            sb.append('.');
            throw new IllegalArgumentException(sb.toString());
        }
        if (n < f) {
            return f;
        }
        if (n > f2) {
            return f2;
        }
        return n;
    }
    
    public static final int coerceIn(final int n, final int i, final int j) {
        if (i > j) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot coerce value to an empty range: maximum ");
            sb.append(j);
            sb.append(" is less than minimum ");
            sb.append(i);
            sb.append('.');
            throw new IllegalArgumentException(sb.toString());
        }
        if (n < i) {
            return i;
        }
        if (n > j) {
            return j;
        }
        return n;
    }
    
    public static final int coerceIn(final int i, final ClosedRange<Integer> obj) {
        Intrinsics.checkParameterIsNotNull(obj, "range");
        if (obj instanceof ClosedFloatingPointRange) {
            return coerceIn(Integer.valueOf(i), (ClosedFloatingPointRange<Integer>)obj).intValue();
        }
        if (obj.isEmpty()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot coerce value to an empty range: ");
            sb.append(obj);
            sb.append('.');
            throw new IllegalArgumentException(sb.toString());
        }
        if (i < obj.getStart().intValue()) {
            return obj.getStart().intValue();
        }
        int intValue;
        if ((intValue = i) > obj.getEndInclusive().intValue()) {
            intValue = obj.getEndInclusive().intValue();
        }
        return intValue;
    }
    
    public static final long coerceIn(final long n, final long lng, final long lng2) {
        if (lng > lng2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot coerce value to an empty range: maximum ");
            sb.append(lng2);
            sb.append(" is less than minimum ");
            sb.append(lng);
            sb.append('.');
            throw new IllegalArgumentException(sb.toString());
        }
        if (n < lng) {
            return lng;
        }
        if (n > lng2) {
            return lng2;
        }
        return n;
    }
    
    public static final long coerceIn(final long l, final ClosedRange<Long> obj) {
        Intrinsics.checkParameterIsNotNull(obj, "range");
        if (obj instanceof ClosedFloatingPointRange) {
            return coerceIn(Long.valueOf(l), (ClosedFloatingPointRange<Long>)obj).longValue();
        }
        if (obj.isEmpty()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot coerce value to an empty range: ");
            sb.append(obj);
            sb.append('.');
            throw new IllegalArgumentException(sb.toString());
        }
        if (l < obj.getStart().longValue()) {
            return obj.getStart().longValue();
        }
        long longValue = l;
        if (l > obj.getEndInclusive().longValue()) {
            longValue = obj.getEndInclusive().longValue();
        }
        return longValue;
    }
    
    public static final <T extends Comparable<? super T>> T coerceIn(final T t, final T obj, final T obj2) {
        Intrinsics.checkParameterIsNotNull(t, "$this$coerceIn");
        if (obj != null && obj2 != null) {
            if (obj.compareTo((Object)obj2) > 0) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Cannot coerce value to an empty range: maximum ");
                sb.append(obj2);
                sb.append(" is less than minimum ");
                sb.append(obj);
                sb.append('.');
                throw new IllegalArgumentException(sb.toString());
            }
            if (t.compareTo((Object)obj) < 0) {
                return obj;
            }
            if (t.compareTo((Object)obj2) > 0) {
                return obj2;
            }
        }
        else {
            if (obj != null && t.compareTo((Object)obj) < 0) {
                return obj;
            }
            if (obj2 != null && t.compareTo((Object)obj2) > 0) {
                return obj2;
            }
        }
        return t;
    }
    
    public static final <T extends Comparable<? super T>> T coerceIn(final T t, final ClosedFloatingPointRange<T> obj) {
        Intrinsics.checkParameterIsNotNull(t, "$this$coerceIn");
        Intrinsics.checkParameterIsNotNull(obj, "range");
        if (obj.isEmpty()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot coerce value to an empty range: ");
            sb.append(obj);
            sb.append('.');
            throw new IllegalArgumentException(sb.toString());
        }
        if (obj.lessThanOrEquals((Comparable<? super T>)t, obj.getStart()) && !obj.lessThanOrEquals(obj.getStart(), t)) {
            return obj.getStart();
        }
        Comparable<? super T> endInclusive = t;
        if (obj.lessThanOrEquals(obj.getEndInclusive(), (Comparable<? super T>)t)) {
            endInclusive = t;
            if (!obj.lessThanOrEquals((Comparable<? super T>)t, obj.getEndInclusive())) {
                endInclusive = obj.getEndInclusive();
            }
        }
        return (T)endInclusive;
    }
    
    public static final <T extends Comparable<? super T>> T coerceIn(final T t, final ClosedRange<T> obj) {
        Intrinsics.checkParameterIsNotNull(t, "$this$coerceIn");
        Intrinsics.checkParameterIsNotNull(obj, "range");
        if (obj instanceof ClosedFloatingPointRange) {
            return coerceIn(t, (ClosedFloatingPointRange<T>)obj);
        }
        if (obj.isEmpty()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot coerce value to an empty range: ");
            sb.append(obj);
            sb.append('.');
            throw new IllegalArgumentException(sb.toString());
        }
        if (t.compareTo((Object)obj.getStart()) < 0) {
            return obj.getStart();
        }
        Comparable<? super T> endInclusive = t;
        if (t.compareTo((Object)obj.getEndInclusive()) > 0) {
            endInclusive = obj.getEndInclusive();
        }
        return (T)endInclusive;
    }
    
    public static final short coerceIn(final short n, final short i, final short j) {
        if (i > j) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot coerce value to an empty range: maximum ");
            sb.append(j);
            sb.append(" is less than minimum ");
            sb.append(i);
            sb.append('.');
            throw new IllegalArgumentException(sb.toString());
        }
        if (n < i) {
            return i;
        }
        if (n > j) {
            return j;
        }
        return n;
    }
    
    private static final boolean contains(final CharRange charRange, final Character c) {
        Intrinsics.checkParameterIsNotNull(charRange, "$this$contains");
        return c != null && charRange.contains((char)c);
    }
    
    private static final boolean contains(final IntRange intRange, final Integer n) {
        Intrinsics.checkParameterIsNotNull(intRange, "$this$contains");
        return n != null && intRange.contains((int)n);
    }
    
    private static final boolean contains(final LongRange longRange, final Long n) {
        Intrinsics.checkParameterIsNotNull(longRange, "$this$contains");
        return n != null && longRange.contains((long)n);
    }
    
    @Deprecated(message = "This `contains` operation mixing integer and floating point arguments has ambiguous semantics and is going to be removed.")
    public static final boolean doubleRangeContains(final ClosedRange<Double> closedRange, final byte b) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        return closedRange.contains((double)b);
    }
    
    public static final boolean doubleRangeContains(final ClosedRange<Double> closedRange, final float n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        return closedRange.contains((double)n);
    }
    
    @Deprecated(message = "This `contains` operation mixing integer and floating point arguments has ambiguous semantics and is going to be removed.")
    public static final boolean doubleRangeContains(final ClosedRange<Double> closedRange, final int n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        return closedRange.contains((double)n);
    }
    
    @Deprecated(message = "This `contains` operation mixing integer and floating point arguments has ambiguous semantics and is going to be removed.")
    public static final boolean doubleRangeContains(final ClosedRange<Double> closedRange, final long n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        return closedRange.contains(Double.valueOf(n));
    }
    
    @Deprecated(message = "This `contains` operation mixing integer and floating point arguments has ambiguous semantics and is going to be removed.")
    public static final boolean doubleRangeContains(final ClosedRange<Double> closedRange, final short n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        return closedRange.contains((double)n);
    }
    
    public static final CharProgression downTo(final char c, final char c2) {
        return CharProgression.Companion.fromClosedRange(c, c2, -1);
    }
    
    public static final IntProgression downTo(final byte b, final byte b2) {
        return IntProgression.Companion.fromClosedRange(b, b2, -1);
    }
    
    public static final IntProgression downTo(final byte b, final int n) {
        return IntProgression.Companion.fromClosedRange(b, n, -1);
    }
    
    public static final IntProgression downTo(final byte b, final short n) {
        return IntProgression.Companion.fromClosedRange(b, n, -1);
    }
    
    public static final IntProgression downTo(final int n, final byte b) {
        return IntProgression.Companion.fromClosedRange(n, b, -1);
    }
    
    public static final IntProgression downTo(final int n, final int n2) {
        return IntProgression.Companion.fromClosedRange(n, n2, -1);
    }
    
    public static final IntProgression downTo(final int n, final short n2) {
        return IntProgression.Companion.fromClosedRange(n, n2, -1);
    }
    
    public static final IntProgression downTo(final short n, final byte b) {
        return IntProgression.Companion.fromClosedRange(n, b, -1);
    }
    
    public static final IntProgression downTo(final short n, final int n2) {
        return IntProgression.Companion.fromClosedRange(n, n2, -1);
    }
    
    public static final IntProgression downTo(final short n, final short n2) {
        return IntProgression.Companion.fromClosedRange(n, n2, -1);
    }
    
    public static final LongProgression downTo(final byte b, final long n) {
        return LongProgression.Companion.fromClosedRange(b, n, -1L);
    }
    
    public static final LongProgression downTo(final int n, final long n2) {
        return LongProgression.Companion.fromClosedRange(n, n2, -1L);
    }
    
    public static final LongProgression downTo(final long n, final byte b) {
        return LongProgression.Companion.fromClosedRange(n, b, -1L);
    }
    
    public static final LongProgression downTo(final long n, final int n2) {
        return LongProgression.Companion.fromClosedRange(n, n2, -1L);
    }
    
    public static final LongProgression downTo(final long n, final long n2) {
        return LongProgression.Companion.fromClosedRange(n, n2, -1L);
    }
    
    public static final LongProgression downTo(final long n, final short n2) {
        return LongProgression.Companion.fromClosedRange(n, n2, -1L);
    }
    
    public static final LongProgression downTo(final short n, final long n2) {
        return LongProgression.Companion.fromClosedRange(n, n2, -1L);
    }
    
    @Deprecated(message = "This `contains` operation mixing integer and floating point arguments has ambiguous semantics and is going to be removed.")
    public static final boolean floatRangeContains(final ClosedRange<Float> closedRange, final byte b) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        return closedRange.contains((float)b);
    }
    
    public static final boolean floatRangeContains(final ClosedRange<Float> closedRange, final double n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        return closedRange.contains((float)n);
    }
    
    @Deprecated(message = "This `contains` operation mixing integer and floating point arguments has ambiguous semantics and is going to be removed.")
    public static final boolean floatRangeContains(final ClosedRange<Float> closedRange, final int n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        return closedRange.contains(Float.valueOf(n));
    }
    
    @Deprecated(message = "This `contains` operation mixing integer and floating point arguments has ambiguous semantics and is going to be removed.")
    public static final boolean floatRangeContains(final ClosedRange<Float> closedRange, final long n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        return closedRange.contains(Float.valueOf(n));
    }
    
    @Deprecated(message = "This `contains` operation mixing integer and floating point arguments has ambiguous semantics and is going to be removed.")
    public static final boolean floatRangeContains(final ClosedRange<Float> closedRange, final short n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        return closedRange.contains((float)n);
    }
    
    public static final boolean intRangeContains(final ClosedRange<Integer> closedRange, final byte i) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        return closedRange.contains((int)i);
    }
    
    @Deprecated(message = "This `contains` operation mixing integer and floating point arguments has ambiguous semantics and is going to be removed.")
    public static final boolean intRangeContains(final ClosedRange<Integer> closedRange, final double n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        final Integer intExactOrNull = toIntExactOrNull(n);
        return intExactOrNull != null && closedRange.contains(intExactOrNull);
    }
    
    @Deprecated(message = "This `contains` operation mixing integer and floating point arguments has ambiguous semantics and is going to be removed.")
    public static final boolean intRangeContains(final ClosedRange<Integer> closedRange, final float n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        final Integer intExactOrNull = toIntExactOrNull(n);
        return intExactOrNull != null && closedRange.contains(intExactOrNull);
    }
    
    public static final boolean intRangeContains(final ClosedRange<Integer> closedRange, final long n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        final Integer intExactOrNull = toIntExactOrNull(n);
        return intExactOrNull != null && closedRange.contains(intExactOrNull);
    }
    
    public static final boolean intRangeContains(final ClosedRange<Integer> closedRange, final short i) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        return closedRange.contains((int)i);
    }
    
    public static final boolean longRangeContains(final ClosedRange<Long> closedRange, final byte b) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        return closedRange.contains((long)b);
    }
    
    @Deprecated(message = "This `contains` operation mixing integer and floating point arguments has ambiguous semantics and is going to be removed.")
    public static final boolean longRangeContains(final ClosedRange<Long> closedRange, final double n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        final Long longExactOrNull = toLongExactOrNull(n);
        return longExactOrNull != null && closedRange.contains(longExactOrNull);
    }
    
    @Deprecated(message = "This `contains` operation mixing integer and floating point arguments has ambiguous semantics and is going to be removed.")
    public static final boolean longRangeContains(final ClosedRange<Long> closedRange, final float n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        final Long longExactOrNull = toLongExactOrNull(n);
        return longExactOrNull != null && closedRange.contains(longExactOrNull);
    }
    
    public static final boolean longRangeContains(final ClosedRange<Long> closedRange, final int n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        return closedRange.contains((long)n);
    }
    
    public static final boolean longRangeContains(final ClosedRange<Long> closedRange, final short n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        return closedRange.contains((long)n);
    }
    
    private static final char random(final CharRange charRange) {
        return random(charRange, Random.Default);
    }
    
    public static final char random(final CharRange charRange, final Random random) {
        Intrinsics.checkParameterIsNotNull(charRange, "$this$random");
        Intrinsics.checkParameterIsNotNull(random, "random");
        try {
            return (char)random.nextInt(charRange.getFirst(), charRange.getLast() + '\u0001');
        }
        catch (IllegalArgumentException ex) {
            throw new NoSuchElementException(ex.getMessage());
        }
    }
    
    private static final int random(final IntRange intRange) {
        return random(intRange, Random.Default);
    }
    
    public static final int random(final IntRange intRange, final Random random) {
        Intrinsics.checkParameterIsNotNull(intRange, "$this$random");
        Intrinsics.checkParameterIsNotNull(random, "random");
        try {
            return RandomKt.nextInt(random, intRange);
        }
        catch (IllegalArgumentException ex) {
            throw new NoSuchElementException(ex.getMessage());
        }
    }
    
    private static final long random(final LongRange longRange) {
        return random(longRange, Random.Default);
    }
    
    public static final long random(final LongRange longRange, final Random random) {
        Intrinsics.checkParameterIsNotNull(longRange, "$this$random");
        Intrinsics.checkParameterIsNotNull(random, "random");
        try {
            return RandomKt.nextLong(random, longRange);
        }
        catch (IllegalArgumentException ex) {
            throw new NoSuchElementException(ex.getMessage());
        }
    }
    
    public static final CharProgression reversed(final CharProgression charProgression) {
        Intrinsics.checkParameterIsNotNull(charProgression, "$this$reversed");
        return CharProgression.Companion.fromClosedRange(charProgression.getLast(), charProgression.getFirst(), -charProgression.getStep());
    }
    
    public static final IntProgression reversed(final IntProgression intProgression) {
        Intrinsics.checkParameterIsNotNull(intProgression, "$this$reversed");
        return IntProgression.Companion.fromClosedRange(intProgression.getLast(), intProgression.getFirst(), -intProgression.getStep());
    }
    
    public static final LongProgression reversed(final LongProgression longProgression) {
        Intrinsics.checkParameterIsNotNull(longProgression, "$this$reversed");
        return LongProgression.Companion.fromClosedRange(longProgression.getLast(), longProgression.getFirst(), -longProgression.getStep());
    }
    
    public static final boolean shortRangeContains(final ClosedRange<Short> closedRange, final byte b) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        return closedRange.contains((short)b);
    }
    
    @Deprecated(message = "This `contains` operation mixing integer and floating point arguments has ambiguous semantics and is going to be removed.")
    public static final boolean shortRangeContains(final ClosedRange<Short> closedRange, final double n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        final Short shortExactOrNull = toShortExactOrNull(n);
        return shortExactOrNull != null && closedRange.contains(shortExactOrNull);
    }
    
    @Deprecated(message = "This `contains` operation mixing integer and floating point arguments has ambiguous semantics and is going to be removed.")
    public static final boolean shortRangeContains(final ClosedRange<Short> closedRange, final float n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        final Short shortExactOrNull = toShortExactOrNull(n);
        return shortExactOrNull != null && closedRange.contains(shortExactOrNull);
    }
    
    public static final boolean shortRangeContains(final ClosedRange<Short> closedRange, final int n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        final Short shortExactOrNull = toShortExactOrNull(n);
        return shortExactOrNull != null && closedRange.contains(shortExactOrNull);
    }
    
    public static final boolean shortRangeContains(final ClosedRange<Short> closedRange, final long n) {
        Intrinsics.checkParameterIsNotNull(closedRange, "$this$contains");
        final Short shortExactOrNull = toShortExactOrNull(n);
        return shortExactOrNull != null && closedRange.contains(shortExactOrNull);
    }
    
    public static final CharProgression step(final CharProgression charProgression, int i) {
        Intrinsics.checkParameterIsNotNull(charProgression, "$this$step");
        RangesKt__RangesKt.checkStepIsPositive(i > 0, i);
        final CharProgression.Companion companion = CharProgression.Companion;
        final char first = charProgression.getFirst();
        final char last = charProgression.getLast();
        if (charProgression.getStep() <= 0) {
            i = -i;
        }
        return companion.fromClosedRange(first, last, i);
    }
    
    public static final IntProgression step(final IntProgression intProgression, int i) {
        Intrinsics.checkParameterIsNotNull(intProgression, "$this$step");
        RangesKt__RangesKt.checkStepIsPositive(i > 0, i);
        final IntProgression.Companion companion = IntProgression.Companion;
        final int first = intProgression.getFirst();
        final int last = intProgression.getLast();
        if (intProgression.getStep() <= 0) {
            i = -i;
        }
        return companion.fromClosedRange(first, last, i);
    }
    
    public static final LongProgression step(final LongProgression longProgression, long l) {
        Intrinsics.checkParameterIsNotNull(longProgression, "$this$step");
        RangesKt__RangesKt.checkStepIsPositive(l > 0L, l);
        final LongProgression.Companion companion = LongProgression.Companion;
        final long first = longProgression.getFirst();
        final long last = longProgression.getLast();
        if (longProgression.getStep() <= 0L) {
            l = -l;
        }
        return companion.fromClosedRange(first, last, l);
    }
    
    public static final Byte toByteExactOrNull(final double n) {
        final double n2 = -128;
        final double n3 = 127;
        if (n >= n2 && n <= n3) {
            return (byte)n;
        }
        return null;
    }
    
    public static final Byte toByteExactOrNull(final float n) {
        final float n2 = -128;
        final float n3 = 127;
        if (n >= n2 && n <= n3) {
            return (byte)n;
        }
        return null;
    }
    
    public static final Byte toByteExactOrNull(final int n) {
        if (-128 <= n) {
            if (127 >= n) {
                return (byte)n;
            }
        }
        return null;
    }
    
    public static final Byte toByteExactOrNull(final long n) {
        final long n2 = -128;
        final long n3 = 127;
        if (n2 <= n) {
            if (n3 >= n) {
                return (byte)n;
            }
        }
        return null;
    }
    
    public static final Byte toByteExactOrNull(final short n) {
        final short n2 = -128;
        final short n3 = 127;
        if (n2 <= n) {
            if (n3 >= n) {
                return (byte)n;
            }
        }
        return null;
    }
    
    public static final Integer toIntExactOrNull(final double n) {
        final double n2 = Integer.MIN_VALUE;
        final double n3 = Integer.MAX_VALUE;
        if (n >= n2 && n <= n3) {
            return (int)n;
        }
        return null;
    }
    
    public static final Integer toIntExactOrNull(final float n) {
        final float n2 = Integer.MIN_VALUE;
        final float n3 = Integer.MAX_VALUE;
        if (n >= n2 && n <= n3) {
            return (int)n;
        }
        return null;
    }
    
    public static final Integer toIntExactOrNull(final long n) {
        final long n2 = Integer.MIN_VALUE;
        final long n3 = Integer.MAX_VALUE;
        if (n2 <= n) {
            if (n3 >= n) {
                return (int)n;
            }
        }
        return null;
    }
    
    public static final Long toLongExactOrNull(final double n) {
        final double n2 = Long.MIN_VALUE;
        final double n3 = Long.MAX_VALUE;
        if (n >= n2 && n <= n3) {
            return (long)n;
        }
        return null;
    }
    
    public static final Long toLongExactOrNull(final float n) {
        final float n2 = Long.MIN_VALUE;
        final float n3 = Long.MAX_VALUE;
        if (n >= n2 && n <= n3) {
            return (long)n;
        }
        return null;
    }
    
    public static final Short toShortExactOrNull(final double n) {
        final double n2 = -32768;
        final double n3 = 32767;
        if (n >= n2 && n <= n3) {
            return (short)n;
        }
        return null;
    }
    
    public static final Short toShortExactOrNull(final float n) {
        final float n2 = -32768;
        final float n3 = 32767;
        if (n >= n2 && n <= n3) {
            return (short)n;
        }
        return null;
    }
    
    public static final Short toShortExactOrNull(final int n) {
        if (-32768 <= n) {
            if (32767 >= n) {
                return (short)n;
            }
        }
        return null;
    }
    
    public static final Short toShortExactOrNull(final long n) {
        final long n2 = -32768;
        final long n3 = 32767;
        if (n2 <= n) {
            if (n3 >= n) {
                return (short)n;
            }
        }
        return null;
    }
    
    public static final CharRange until(final char c, final char c2) {
        if (c2 <= '\0') {
            return CharRange.Companion.getEMPTY();
        }
        return new CharRange(c, (char)(c2 - '\u0001'));
    }
    
    public static final IntRange until(final byte b, final byte b2) {
        return new IntRange(b, b2 - 1);
    }
    
    public static final IntRange until(final byte b, final int n) {
        if (n <= Integer.MIN_VALUE) {
            return IntRange.Companion.getEMPTY();
        }
        return new IntRange(b, n - 1);
    }
    
    public static final IntRange until(final byte b, final short n) {
        return new IntRange(b, n - 1);
    }
    
    public static final IntRange until(final int n, final byte b) {
        return new IntRange(n, b - 1);
    }
    
    public static final IntRange until(final int n, final int n2) {
        if (n2 <= Integer.MIN_VALUE) {
            return IntRange.Companion.getEMPTY();
        }
        return new IntRange(n, n2 - 1);
    }
    
    public static final IntRange until(final int n, final short n2) {
        return new IntRange(n, n2 - 1);
    }
    
    public static final IntRange until(final short n, final byte b) {
        return new IntRange(n, b - 1);
    }
    
    public static final IntRange until(final short n, final int n2) {
        if (n2 <= Integer.MIN_VALUE) {
            return IntRange.Companion.getEMPTY();
        }
        return new IntRange(n, n2 - 1);
    }
    
    public static final IntRange until(final short n, final short n2) {
        return new IntRange(n, n2 - 1);
    }
    
    public static final LongRange until(final byte b, final long n) {
        if (n <= Long.MIN_VALUE) {
            return LongRange.Companion.getEMPTY();
        }
        return new LongRange(b, n - 1L);
    }
    
    public static final LongRange until(final int n, final long n2) {
        if (n2 <= Long.MIN_VALUE) {
            return LongRange.Companion.getEMPTY();
        }
        return new LongRange(n, n2 - 1L);
    }
    
    public static final LongRange until(final long n, final byte b) {
        return new LongRange(n, b - 1L);
    }
    
    public static final LongRange until(final long n, final int n2) {
        return new LongRange(n, n2 - 1L);
    }
    
    public static final LongRange until(final long n, final long n2) {
        if (n2 <= Long.MIN_VALUE) {
            return LongRange.Companion.getEMPTY();
        }
        return new LongRange(n, n2 - 1L);
    }
    
    public static final LongRange until(final long n, final short n2) {
        return new LongRange(n, n2 - 1L);
    }
    
    public static final LongRange until(final short n, final long n2) {
        if (n2 <= Long.MIN_VALUE) {
            return LongRange.Companion.getEMPTY();
        }
        return new LongRange(n, n2 - 1L);
    }
}
