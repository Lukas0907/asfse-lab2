// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.ranges;

import java.util.NoSuchElementException;
import kotlin.random.URandomKt;
import kotlin.random.Random;
import kotlin.UByte;
import kotlin.UInt;
import kotlin.UShort;
import kotlin.ULong;
import kotlin.UnsignedKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\u0010\t\n\u0002\b\n\u001a\u001e\u0010\u0000\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0003\u0010\u0004\u001a\u001e\u0010\u0000\u001a\u00020\u0005*\u00020\u00052\u0006\u0010\u0002\u001a\u00020\u0005H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0006\u0010\u0007\u001a\u001e\u0010\u0000\u001a\u00020\b*\u00020\b2\u0006\u0010\u0002\u001a\u00020\bH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\t\u0010\n\u001a\u001e\u0010\u0000\u001a\u00020\u000b*\u00020\u000b2\u0006\u0010\u0002\u001a\u00020\u000bH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\f\u0010\r\u001a\u001e\u0010\u000e\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u000f\u001a\u00020\u0001H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0010\u0010\u0004\u001a\u001e\u0010\u000e\u001a\u00020\u0005*\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u0005H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0011\u0010\u0007\u001a\u001e\u0010\u000e\u001a\u00020\b*\u00020\b2\u0006\u0010\u000f\u001a\u00020\bH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0012\u0010\n\u001a\u001e\u0010\u000e\u001a\u00020\u000b*\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u000bH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0013\u0010\r\u001a&\u0010\u0014\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u000f\u001a\u00020\u0001H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0015\u0010\u0016\u001a&\u0010\u0014\u001a\u00020\u0005*\u00020\u00052\u0006\u0010\u0002\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u0005H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0017\u0010\u0018\u001a$\u0010\u0014\u001a\u00020\u0005*\u00020\u00052\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00050\u001aH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u001b\u0010\u001c\u001a&\u0010\u0014\u001a\u00020\b*\u00020\b2\u0006\u0010\u0002\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\bH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u001d\u0010\u001e\u001a$\u0010\u0014\u001a\u00020\b*\u00020\b2\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\b0\u001aH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u001f\u0010 \u001a&\u0010\u0014\u001a\u00020\u000b*\u00020\u000b2\u0006\u0010\u0002\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u000bH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b!\u0010\"\u001a\u001f\u0010#\u001a\u00020$*\u00020%2\u0006\u0010&\u001a\u00020\u0001H\u0087\u0002\u00f8\u0001\u0000¢\u0006\u0004\b'\u0010(\u001a\u001f\u0010#\u001a\u00020$*\u00020%2\b\u0010)\u001a\u0004\u0018\u00010\u0005H\u0087\n\u00f8\u0001\u0000¢\u0006\u0002\b*\u001a\u001f\u0010#\u001a\u00020$*\u00020%2\u0006\u0010&\u001a\u00020\bH\u0087\u0002\u00f8\u0001\u0000¢\u0006\u0004\b+\u0010,\u001a\u001f\u0010#\u001a\u00020$*\u00020%2\u0006\u0010&\u001a\u00020\u000bH\u0087\u0002\u00f8\u0001\u0000¢\u0006\u0004\b-\u0010.\u001a\u001f\u0010#\u001a\u00020$*\u00020/2\u0006\u0010&\u001a\u00020\u0001H\u0087\u0002\u00f8\u0001\u0000¢\u0006\u0004\b0\u00101\u001a\u001f\u0010#\u001a\u00020$*\u00020/2\u0006\u0010&\u001a\u00020\u0005H\u0087\u0002\u00f8\u0001\u0000¢\u0006\u0004\b2\u00103\u001a\u001f\u0010#\u001a\u00020$*\u00020/2\b\u0010)\u001a\u0004\u0018\u00010\bH\u0087\n\u00f8\u0001\u0000¢\u0006\u0002\b4\u001a\u001f\u0010#\u001a\u00020$*\u00020/2\u0006\u0010&\u001a\u00020\u000bH\u0087\u0002\u00f8\u0001\u0000¢\u0006\u0004\b5\u00106\u001a\u001f\u00107\u001a\u000208*\u00020\u00012\u0006\u00109\u001a\u00020\u0001H\u0087\u0004\u00f8\u0001\u0000¢\u0006\u0004\b:\u0010;\u001a\u001f\u00107\u001a\u000208*\u00020\u00052\u0006\u00109\u001a\u00020\u0005H\u0087\u0004\u00f8\u0001\u0000¢\u0006\u0004\b<\u0010=\u001a\u001f\u00107\u001a\u00020>*\u00020\b2\u0006\u00109\u001a\u00020\bH\u0087\u0004\u00f8\u0001\u0000¢\u0006\u0004\b?\u0010@\u001a\u001f\u00107\u001a\u000208*\u00020\u000b2\u0006\u00109\u001a\u00020\u000bH\u0087\u0004\u00f8\u0001\u0000¢\u0006\u0004\bA\u0010B\u001a\u0015\u0010C\u001a\u00020\u0005*\u00020%H\u0087\b\u00f8\u0001\u0000¢\u0006\u0002\u0010D\u001a\u001c\u0010C\u001a\u00020\u0005*\u00020%2\u0006\u0010C\u001a\u00020EH\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010F\u001a\u0015\u0010C\u001a\u00020\b*\u00020/H\u0087\b\u00f8\u0001\u0000¢\u0006\u0002\u0010G\u001a\u001c\u0010C\u001a\u00020\b*\u00020/2\u0006\u0010C\u001a\u00020EH\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010H\u001a\f\u0010I\u001a\u000208*\u000208H\u0007\u001a\f\u0010I\u001a\u00020>*\u00020>H\u0007\u001a\u0015\u0010J\u001a\u000208*\u0002082\u0006\u0010J\u001a\u00020KH\u0087\u0004\u001a\u0015\u0010J\u001a\u00020>*\u00020>2\u0006\u0010J\u001a\u00020LH\u0087\u0004\u001a\u001f\u0010M\u001a\u00020%*\u00020\u00012\u0006\u00109\u001a\u00020\u0001H\u0087\u0004\u00f8\u0001\u0000¢\u0006\u0004\bN\u0010O\u001a\u001f\u0010M\u001a\u00020%*\u00020\u00052\u0006\u00109\u001a\u00020\u0005H\u0087\u0004\u00f8\u0001\u0000¢\u0006\u0004\bP\u0010Q\u001a\u001f\u0010M\u001a\u00020/*\u00020\b2\u0006\u00109\u001a\u00020\bH\u0087\u0004\u00f8\u0001\u0000¢\u0006\u0004\bR\u0010S\u001a\u001f\u0010M\u001a\u00020%*\u00020\u000b2\u0006\u00109\u001a\u00020\u000bH\u0087\u0004\u00f8\u0001\u0000¢\u0006\u0004\bT\u0010U\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006V" }, d2 = { "coerceAtLeast", "Lkotlin/UByte;", "minimumValue", "coerceAtLeast-Kr8caGY", "(BB)B", "Lkotlin/UInt;", "coerceAtLeast-J1ME1BU", "(II)I", "Lkotlin/ULong;", "coerceAtLeast-eb3DHEI", "(JJ)J", "Lkotlin/UShort;", "coerceAtLeast-5PvTz6A", "(SS)S", "coerceAtMost", "maximumValue", "coerceAtMost-Kr8caGY", "coerceAtMost-J1ME1BU", "coerceAtMost-eb3DHEI", "coerceAtMost-5PvTz6A", "coerceIn", "coerceIn-b33U2AM", "(BBB)B", "coerceIn-WZ9TVnA", "(III)I", "range", "Lkotlin/ranges/ClosedRange;", "coerceIn-wuiCnnA", "(ILkotlin/ranges/ClosedRange;)I", "coerceIn-sambcqE", "(JJJ)J", "coerceIn-JPwROB0", "(JLkotlin/ranges/ClosedRange;)J", "coerceIn-VKSA0NQ", "(SSS)S", "contains", "", "Lkotlin/ranges/UIntRange;", "value", "contains-68kG9v0", "(Lkotlin/ranges/UIntRange;B)Z", "element", "contains-biwQdVI", "contains-fz5IDCE", "(Lkotlin/ranges/UIntRange;J)Z", "contains-ZsK3CEQ", "(Lkotlin/ranges/UIntRange;S)Z", "Lkotlin/ranges/ULongRange;", "contains-ULb-yJY", "(Lkotlin/ranges/ULongRange;B)Z", "contains-Gab390E", "(Lkotlin/ranges/ULongRange;I)Z", "contains-GYNo2lE", "contains-uhHAxoY", "(Lkotlin/ranges/ULongRange;S)Z", "downTo", "Lkotlin/ranges/UIntProgression;", "to", "downTo-Kr8caGY", "(BB)Lkotlin/ranges/UIntProgression;", "downTo-J1ME1BU", "(II)Lkotlin/ranges/UIntProgression;", "Lkotlin/ranges/ULongProgression;", "downTo-eb3DHEI", "(JJ)Lkotlin/ranges/ULongProgression;", "downTo-5PvTz6A", "(SS)Lkotlin/ranges/UIntProgression;", "random", "(Lkotlin/ranges/UIntRange;)I", "Lkotlin/random/Random;", "(Lkotlin/ranges/UIntRange;Lkotlin/random/Random;)I", "(Lkotlin/ranges/ULongRange;)J", "(Lkotlin/ranges/ULongRange;Lkotlin/random/Random;)J", "reversed", "step", "", "", "until", "until-Kr8caGY", "(BB)Lkotlin/ranges/UIntRange;", "until-J1ME1BU", "(II)Lkotlin/ranges/UIntRange;", "until-eb3DHEI", "(JJ)Lkotlin/ranges/ULongRange;", "until-5PvTz6A", "(SS)Lkotlin/ranges/UIntRange;", "kotlin-stdlib" }, k = 5, mv = { 1, 1, 15 }, xi = 1, xs = "kotlin/ranges/URangesKt")
class URangesKt___URangesKt
{
    public URangesKt___URangesKt() {
    }
    
    public static final short coerceAtLeast-5PvTz6A(final short n, final short n2) {
        short n3 = n;
        if (Intrinsics.compare(n & 0xFFFF, 0xFFFF & n2) < 0) {
            n3 = n2;
        }
        return n3;
    }
    
    public static final int coerceAtLeast-J1ME1BU(final int n, final int n2) {
        int n3 = n;
        if (UnsignedKt.uintCompare(n, n2) < 0) {
            n3 = n2;
        }
        return n3;
    }
    
    public static final byte coerceAtLeast-Kr8caGY(final byte b, final byte b2) {
        byte b3 = b;
        if (Intrinsics.compare(b & 0xFF, b2 & 0xFF) < 0) {
            b3 = b2;
        }
        return b3;
    }
    
    public static final long coerceAtLeast-eb3DHEI(final long n, final long n2) {
        long n3 = n;
        if (UnsignedKt.ulongCompare(n, n2) < 0) {
            n3 = n2;
        }
        return n3;
    }
    
    public static final short coerceAtMost-5PvTz6A(final short n, final short n2) {
        short n3 = n;
        if (Intrinsics.compare(n & 0xFFFF, 0xFFFF & n2) > 0) {
            n3 = n2;
        }
        return n3;
    }
    
    public static final int coerceAtMost-J1ME1BU(final int n, final int n2) {
        int n3 = n;
        if (UnsignedKt.uintCompare(n, n2) > 0) {
            n3 = n2;
        }
        return n3;
    }
    
    public static final byte coerceAtMost-Kr8caGY(final byte b, final byte b2) {
        byte b3 = b;
        if (Intrinsics.compare(b & 0xFF, b2 & 0xFF) > 0) {
            b3 = b2;
        }
        return b3;
    }
    
    public static final long coerceAtMost-eb3DHEI(final long n, final long n2) {
        long n3 = n;
        if (UnsignedKt.ulongCompare(n, n2) > 0) {
            n3 = n2;
        }
        return n3;
    }
    
    public static final long coerceIn-JPwROB0(final long n, final ClosedRange<ULong> obj) {
        Intrinsics.checkParameterIsNotNull(obj, "range");
        if (obj instanceof ClosedFloatingPointRange) {
            return RangesKt___RangesKt.coerceIn(ULong.box-impl(n), (ClosedFloatingPointRange<ULong>)obj).unbox-impl();
        }
        if (obj.isEmpty()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot coerce value to an empty range: ");
            sb.append(obj);
            sb.append('.');
            throw new IllegalArgumentException(sb.toString());
        }
        if (UnsignedKt.ulongCompare(n, obj.getStart().unbox-impl()) < 0) {
            return obj.getStart().unbox-impl();
        }
        long unbox-impl = n;
        if (UnsignedKt.ulongCompare(n, obj.getEndInclusive().unbox-impl()) > 0) {
            unbox-impl = obj.getEndInclusive().unbox-impl();
        }
        return unbox-impl;
    }
    
    public static final short coerceIn-VKSA0NQ(final short n, final short n2, final short n3) {
        final int n4 = n2 & 0xFFFF;
        final int n5 = n3 & 0xFFFF;
        if (Intrinsics.compare(n4, n5) > 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot coerce value to an empty range: maximum ");
            sb.append(UShort.toString-impl(n3));
            sb.append(" is less than minimum ");
            sb.append(UShort.toString-impl(n2));
            sb.append('.');
            throw new IllegalArgumentException(sb.toString());
        }
        final int n6 = 0xFFFF & n;
        if (Intrinsics.compare(n6, n4) < 0) {
            return n2;
        }
        if (Intrinsics.compare(n6, n5) > 0) {
            return n3;
        }
        return n;
    }
    
    public static final int coerceIn-WZ9TVnA(final int n, final int n2, final int n3) {
        if (UnsignedKt.uintCompare(n2, n3) > 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot coerce value to an empty range: maximum ");
            sb.append(UInt.toString-impl(n3));
            sb.append(" is less than minimum ");
            sb.append(UInt.toString-impl(n2));
            sb.append('.');
            throw new IllegalArgumentException(sb.toString());
        }
        if (UnsignedKt.uintCompare(n, n2) < 0) {
            return n2;
        }
        if (UnsignedKt.uintCompare(n, n3) > 0) {
            return n3;
        }
        return n;
    }
    
    public static final byte coerceIn-b33U2AM(final byte b, final byte b2, final byte b3) {
        final int n = b2 & 0xFF;
        final int n2 = b3 & 0xFF;
        if (Intrinsics.compare(n, n2) > 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot coerce value to an empty range: maximum ");
            sb.append(UByte.toString-impl(b3));
            sb.append(" is less than minimum ");
            sb.append(UByte.toString-impl(b2));
            sb.append('.');
            throw new IllegalArgumentException(sb.toString());
        }
        final int n3 = b & 0xFF;
        if (Intrinsics.compare(n3, n) < 0) {
            return b2;
        }
        if (Intrinsics.compare(n3, n2) > 0) {
            return b3;
        }
        return b;
    }
    
    public static final long coerceIn-sambcqE(final long n, final long n2, final long n3) {
        if (UnsignedKt.ulongCompare(n2, n3) > 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot coerce value to an empty range: maximum ");
            sb.append(ULong.toString-impl(n3));
            sb.append(" is less than minimum ");
            sb.append(ULong.toString-impl(n2));
            sb.append('.');
            throw new IllegalArgumentException(sb.toString());
        }
        if (UnsignedKt.ulongCompare(n, n2) < 0) {
            return n2;
        }
        if (UnsignedKt.ulongCompare(n, n3) > 0) {
            return n3;
        }
        return n;
    }
    
    public static final int coerceIn-wuiCnnA(final int n, final ClosedRange<UInt> obj) {
        Intrinsics.checkParameterIsNotNull(obj, "range");
        if (obj instanceof ClosedFloatingPointRange) {
            return RangesKt___RangesKt.coerceIn(UInt.box-impl(n), (ClosedFloatingPointRange<UInt>)obj).unbox-impl();
        }
        if (obj.isEmpty()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot coerce value to an empty range: ");
            sb.append(obj);
            sb.append('.');
            throw new IllegalArgumentException(sb.toString());
        }
        if (UnsignedKt.uintCompare(n, obj.getStart().unbox-impl()) < 0) {
            return obj.getStart().unbox-impl();
        }
        int unbox-impl = n;
        if (UnsignedKt.uintCompare(n, obj.getEndInclusive().unbox-impl()) > 0) {
            unbox-impl = obj.getEndInclusive().unbox-impl();
        }
        return unbox-impl;
    }
    
    public static final boolean contains-68kG9v0(final UIntRange uIntRange, final byte b) {
        Intrinsics.checkParameterIsNotNull(uIntRange, "$this$contains");
        return uIntRange.contains-WZ4Q5Ns(UInt.constructor-impl(b & 0xFF));
    }
    
    private static final boolean contains-GYNo2lE(final ULongRange uLongRange, final ULong uLong) {
        Intrinsics.checkParameterIsNotNull(uLongRange, "$this$contains");
        return uLong != null && uLongRange.contains-VKZWuLQ(uLong.unbox-impl());
    }
    
    public static final boolean contains-Gab390E(final ULongRange uLongRange, final int n) {
        Intrinsics.checkParameterIsNotNull(uLongRange, "$this$contains");
        return uLongRange.contains-VKZWuLQ(ULong.constructor-impl((long)n & 0xFFFFFFFFL));
    }
    
    public static final boolean contains-ULb-yJY(final ULongRange uLongRange, final byte b) {
        Intrinsics.checkParameterIsNotNull(uLongRange, "$this$contains");
        return uLongRange.contains-VKZWuLQ(ULong.constructor-impl((long)b & 0xFFL));
    }
    
    public static final boolean contains-ZsK3CEQ(final UIntRange uIntRange, final short n) {
        Intrinsics.checkParameterIsNotNull(uIntRange, "$this$contains");
        return uIntRange.contains-WZ4Q5Ns(UInt.constructor-impl(n & 0xFFFF));
    }
    
    private static final boolean contains-biwQdVI(final UIntRange uIntRange, final UInt uInt) {
        Intrinsics.checkParameterIsNotNull(uIntRange, "$this$contains");
        return uInt != null && uIntRange.contains-WZ4Q5Ns(uInt.unbox-impl());
    }
    
    public static final boolean contains-fz5IDCE(final UIntRange uIntRange, final long n) {
        Intrinsics.checkParameterIsNotNull(uIntRange, "$this$contains");
        return ULong.constructor-impl(n >>> 32) == 0L && uIntRange.contains-WZ4Q5Ns(UInt.constructor-impl((int)n));
    }
    
    public static final boolean contains-uhHAxoY(final ULongRange uLongRange, final short n) {
        Intrinsics.checkParameterIsNotNull(uLongRange, "$this$contains");
        return uLongRange.contains-VKZWuLQ(ULong.constructor-impl((long)n & 0xFFFFL));
    }
    
    public static final UIntProgression downTo-5PvTz6A(final short n, final short n2) {
        return UIntProgression.Companion.fromClosedRange-Nkh28Cs(UInt.constructor-impl(n & 0xFFFF), UInt.constructor-impl(n2 & 0xFFFF), -1);
    }
    
    public static final UIntProgression downTo-J1ME1BU(final int n, final int n2) {
        return UIntProgression.Companion.fromClosedRange-Nkh28Cs(n, n2, -1);
    }
    
    public static final UIntProgression downTo-Kr8caGY(final byte b, final byte b2) {
        return UIntProgression.Companion.fromClosedRange-Nkh28Cs(UInt.constructor-impl(b & 0xFF), UInt.constructor-impl(b2 & 0xFF), -1);
    }
    
    public static final ULongProgression downTo-eb3DHEI(final long n, final long n2) {
        return ULongProgression.Companion.fromClosedRange-7ftBX0g(n, n2, -1L);
    }
    
    private static final int random(final UIntRange uIntRange) {
        return random(uIntRange, Random.Default);
    }
    
    public static final int random(final UIntRange uIntRange, final Random random) {
        Intrinsics.checkParameterIsNotNull(uIntRange, "$this$random");
        Intrinsics.checkParameterIsNotNull(random, "random");
        try {
            return URandomKt.nextUInt(random, uIntRange);
        }
        catch (IllegalArgumentException ex) {
            throw new NoSuchElementException(ex.getMessage());
        }
    }
    
    private static final long random(final ULongRange uLongRange) {
        return random(uLongRange, Random.Default);
    }
    
    public static final long random(final ULongRange uLongRange, final Random random) {
        Intrinsics.checkParameterIsNotNull(uLongRange, "$this$random");
        Intrinsics.checkParameterIsNotNull(random, "random");
        try {
            return URandomKt.nextULong(random, uLongRange);
        }
        catch (IllegalArgumentException ex) {
            throw new NoSuchElementException(ex.getMessage());
        }
    }
    
    public static final UIntProgression reversed(final UIntProgression uIntProgression) {
        Intrinsics.checkParameterIsNotNull(uIntProgression, "$this$reversed");
        return UIntProgression.Companion.fromClosedRange-Nkh28Cs(uIntProgression.getLast(), uIntProgression.getFirst(), -uIntProgression.getStep());
    }
    
    public static final ULongProgression reversed(final ULongProgression uLongProgression) {
        Intrinsics.checkParameterIsNotNull(uLongProgression, "$this$reversed");
        return ULongProgression.Companion.fromClosedRange-7ftBX0g(uLongProgression.getLast(), uLongProgression.getFirst(), -uLongProgression.getStep());
    }
    
    public static final UIntProgression step(final UIntProgression uIntProgression, int i) {
        Intrinsics.checkParameterIsNotNull(uIntProgression, "$this$step");
        RangesKt__RangesKt.checkStepIsPositive(i > 0, i);
        final UIntProgression.Companion companion = UIntProgression.Companion;
        final int first = uIntProgression.getFirst();
        final int last = uIntProgression.getLast();
        if (uIntProgression.getStep() <= 0) {
            i = -i;
        }
        return companion.fromClosedRange-Nkh28Cs(first, last, i);
    }
    
    public static final ULongProgression step(final ULongProgression uLongProgression, long l) {
        Intrinsics.checkParameterIsNotNull(uLongProgression, "$this$step");
        RangesKt__RangesKt.checkStepIsPositive(l > 0L, l);
        final ULongProgression.Companion companion = ULongProgression.Companion;
        final long first = uLongProgression.getFirst();
        final long last = uLongProgression.getLast();
        if (uLongProgression.getStep() <= 0L) {
            l = -l;
        }
        return companion.fromClosedRange-7ftBX0g(first, last, l);
    }
    
    public static final UIntRange until-5PvTz6A(final short n, final short n2) {
        final int n3 = n2 & 0xFFFF;
        if (Intrinsics.compare(n3, 0) <= 0) {
            return UIntRange.Companion.getEMPTY();
        }
        return new UIntRange(UInt.constructor-impl(n & 0xFFFF), UInt.constructor-impl(UInt.constructor-impl(n3) - 1), null);
    }
    
    public static final UIntRange until-J1ME1BU(final int n, final int n2) {
        if (UnsignedKt.uintCompare(n2, 0) <= 0) {
            return UIntRange.Companion.getEMPTY();
        }
        return new UIntRange(n, UInt.constructor-impl(n2 - 1), null);
    }
    
    public static final UIntRange until-Kr8caGY(final byte b, final byte b2) {
        final int n = b2 & 0xFF;
        if (Intrinsics.compare(n, 0) <= 0) {
            return UIntRange.Companion.getEMPTY();
        }
        return new UIntRange(UInt.constructor-impl(b & 0xFF), UInt.constructor-impl(UInt.constructor-impl(n) - 1), null);
    }
    
    public static final ULongRange until-eb3DHEI(final long n, final long n2) {
        if (UnsignedKt.ulongCompare(n2, 0L) <= 0) {
            return ULongRange.Companion.getEMPTY();
        }
        return new ULongRange(n, ULong.constructor-impl(n2 - ULong.constructor-impl((long)1 & 0xFFFFFFFFL)), null);
    }
}
