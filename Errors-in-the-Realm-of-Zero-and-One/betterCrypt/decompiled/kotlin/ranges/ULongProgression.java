// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.ranges;

import kotlin.collections.ULongIterator;
import java.util.Iterator;
import kotlin.UnsignedKt;
import kotlin.internal.UProgressionUtilKt;
import kotlin.Metadata;
import kotlin.jvm.internal.markers.KMappedMarker;
import kotlin.ULong;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u001c\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0017\u0018\u0000 \u001a2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001aB\"\b\u0000\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u0004\u001a\u00020\u0002\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0007J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0096\u0002J\b\u0010\u0013\u001a\u00020\u0014H\u0016J\b\u0010\u0015\u001a\u00020\u0010H\u0016J\t\u0010\u0016\u001a\u00020\u0017H\u0096\u0002J\b\u0010\u0018\u001a\u00020\u0019H\u0016R\u0016\u0010\b\u001a\u00020\u0002\u00f8\u0001\u0000¢\u0006\n\n\u0002\u0010\u000b\u001a\u0004\b\t\u0010\nR\u0016\u0010\f\u001a\u00020\u0002\u00f8\u0001\u0000¢\u0006\n\n\u0002\u0010\u000b\u001a\u0004\b\r\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\n\u00f8\u0001\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u001b" }, d2 = { "Lkotlin/ranges/ULongProgression;", "", "Lkotlin/ULong;", "start", "endInclusive", "step", "", "(JJJLkotlin/jvm/internal/DefaultConstructorMarker;)V", "first", "getFirst", "()J", "J", "last", "getLast", "getStep", "equals", "", "other", "", "hashCode", "", "isEmpty", "iterator", "Lkotlin/collections/ULongIterator;", "toString", "", "Companion", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
public class ULongProgression implements Iterable<ULong>, KMappedMarker
{
    public static final Companion Companion;
    private final long first;
    private final long last;
    private final long step;
    
    static {
        Companion = new Companion(null);
    }
    
    private ULongProgression(final long first, final long n, final long step) {
        if (step == 0L) {
            throw new IllegalArgumentException("Step must be non-zero.");
        }
        if (step != Long.MIN_VALUE) {
            this.first = first;
            this.last = UProgressionUtilKt.getProgressionLastElement-7ftBX0g(first, n, step);
            this.step = step;
            return;
        }
        throw new IllegalArgumentException("Step must be greater than Long.MIN_VALUE to avoid overflow on negation.");
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof ULongProgression) {
            if (!this.isEmpty() || !((ULongProgression)o).isEmpty()) {
                final long first = this.first;
                final ULongProgression uLongProgression = (ULongProgression)o;
                if (first != uLongProgression.first || this.last != uLongProgression.last || this.step != uLongProgression.step) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    public final long getFirst() {
        return this.first;
    }
    
    public final long getLast() {
        return this.last;
    }
    
    public final long getStep() {
        return this.step;
    }
    
    @Override
    public int hashCode() {
        if (this.isEmpty()) {
            return -1;
        }
        final long first = this.first;
        final int n = (int)ULong.constructor-impl(first ^ ULong.constructor-impl(first >>> 32));
        final long last = this.last;
        final int n2 = (int)ULong.constructor-impl(last ^ ULong.constructor-impl(last >>> 32));
        final long step = this.step;
        return (n * 31 + n2) * 31 + (int)(step >>> 32 ^ step);
    }
    
    public boolean isEmpty() {
        final long step = this.step;
        final long first = this.first;
        final long last = this.last;
        if (step > 0L) {
            if (UnsignedKt.ulongCompare(first, last) > 0) {
                return true;
            }
        }
        else if (UnsignedKt.ulongCompare(first, last) < 0) {
            return true;
        }
        return false;
    }
    
    @Override
    public ULongIterator iterator() {
        return new ULongProgressionIterator(this.first, this.last, this.step, null);
    }
    
    @Override
    public String toString() {
        StringBuilder sb;
        long step;
        if (this.step > 0L) {
            sb = new StringBuilder();
            sb.append(ULong.toString-impl(this.first));
            sb.append("..");
            sb.append(ULong.toString-impl(this.last));
            sb.append(" step ");
            step = this.step;
        }
        else {
            sb = new StringBuilder();
            sb.append(ULong.toString-impl(this.first));
            sb.append(" downTo ");
            sb.append(ULong.toString-impl(this.last));
            sb.append(" step ");
            step = -this.step;
        }
        sb.append(step);
        return sb.toString();
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J(\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\t\u00f8\u0001\u0000¢\u0006\u0004\b\n\u0010\u000b\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\f" }, d2 = { "Lkotlin/ranges/ULongProgression$Companion;", "", "()V", "fromClosedRange", "Lkotlin/ranges/ULongProgression;", "rangeStart", "Lkotlin/ULong;", "rangeEnd", "step", "", "fromClosedRange-7ftBX0g", "(JJJ)Lkotlin/ranges/ULongProgression;", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
    public static final class Companion
    {
        private Companion() {
        }
        
        public final ULongProgression fromClosedRange-7ftBX0g(final long n, final long n2, final long n3) {
            return new ULongProgression(n, n2, n3, null);
        }
    }
}
