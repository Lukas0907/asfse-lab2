// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.ranges;

import java.util.NoSuchElementException;
import kotlin.UInt;
import kotlin.UnsignedKt;
import kotlin.Metadata;
import kotlin.collections.UIntIterator;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B \u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0007J\t\u0010\n\u001a\u00020\u000bH\u0096\u0002J\u0010\u0010\r\u001a\u00020\u0003H\u0016\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000eR\u0013\u0010\b\u001a\u00020\u0003X\u0082\u0004\u00f8\u0001\u0000¢\u0006\u0004\n\u0002\u0010\tR\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e¢\u0006\u0002\n\u0000R\u0013\u0010\f\u001a\u00020\u0003X\u0082\u000e\u00f8\u0001\u0000¢\u0006\u0004\n\u0002\u0010\tR\u0013\u0010\u0005\u001a\u00020\u0003X\u0082\u0004\u00f8\u0001\u0000¢\u0006\u0004\n\u0002\u0010\t\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u000f" }, d2 = { "Lkotlin/ranges/UIntProgressionIterator;", "Lkotlin/collections/UIntIterator;", "first", "Lkotlin/UInt;", "last", "step", "", "(IIILkotlin/jvm/internal/DefaultConstructorMarker;)V", "finalElement", "I", "hasNext", "", "next", "nextUInt", "()I", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
final class UIntProgressionIterator extends UIntIterator
{
    private final int finalElement;
    private boolean hasNext;
    private int next;
    private final int step;
    
    private UIntProgressionIterator(int finalElement, int uintCompare, final int n) {
        this.finalElement = uintCompare;
        boolean hasNext = true;
        uintCompare = UnsignedKt.uintCompare(finalElement, uintCompare);
        Label_0039: {
            if (n > 0) {
                if (uintCompare <= 0) {
                    break Label_0039;
                }
            }
            else if (uintCompare >= 0) {
                break Label_0039;
            }
            hasNext = false;
        }
        this.hasNext = hasNext;
        this.step = UInt.constructor-impl(n);
        if (!this.hasNext) {
            finalElement = this.finalElement;
        }
        this.next = finalElement;
    }
    
    @Override
    public boolean hasNext() {
        return this.hasNext;
    }
    
    @Override
    public int nextUInt() {
        final int next = this.next;
        if (next != this.finalElement) {
            this.next = UInt.constructor-impl(this.step + next);
            return next;
        }
        if (this.hasNext) {
            this.hasNext = false;
            return next;
        }
        throw new NoSuchElementException();
    }
}
