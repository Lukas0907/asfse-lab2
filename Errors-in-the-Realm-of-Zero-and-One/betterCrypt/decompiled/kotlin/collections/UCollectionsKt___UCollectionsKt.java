// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.collections;

import kotlin.UShortArray;
import kotlin.ULongArray;
import kotlin.UIntArray;
import kotlin.UByteArray;
import java.util.Collection;
import kotlin.UShort;
import kotlin.ULong;
import java.util.Iterator;
import kotlin.UInt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.UByte;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000F\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u001c\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u001c\u0010\u0000\u001a\u00020\u0001*\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005\u001a\u001c\u0010\u0000\u001a\u00020\u0001*\b\u0012\u0004\u0012\u00020\u00010\u0002H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0006\u0010\u0005\u001a\u001c\u0010\u0000\u001a\u00020\u0007*\b\u0012\u0004\u0012\u00020\u00070\u0002H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\b\u0010\t\u001a\u001c\u0010\u0000\u001a\u00020\u0001*\b\u0012\u0004\u0012\u00020\n0\u0002H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u000b\u0010\u0005\u001a\u001a\u0010\f\u001a\u00020\r*\b\u0012\u0004\u0012\u00020\u00030\u000eH\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u000f\u001a\u001a\u0010\u0010\u001a\u00020\u0011*\b\u0012\u0004\u0012\u00020\u00010\u000eH\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0012\u001a\u001a\u0010\u0013\u001a\u00020\u0014*\b\u0012\u0004\u0012\u00020\u00070\u000eH\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0015\u001a\u001a\u0010\u0016\u001a\u00020\u0017*\b\u0012\u0004\u0012\u00020\n0\u000eH\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0018\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0019" }, d2 = { "sum", "Lkotlin/UInt;", "", "Lkotlin/UByte;", "sumOfUByte", "(Ljava/lang/Iterable;)I", "sumOfUInt", "Lkotlin/ULong;", "sumOfULong", "(Ljava/lang/Iterable;)J", "Lkotlin/UShort;", "sumOfUShort", "toUByteArray", "Lkotlin/UByteArray;", "", "(Ljava/util/Collection;)[B", "toUIntArray", "Lkotlin/UIntArray;", "(Ljava/util/Collection;)[I", "toULongArray", "Lkotlin/ULongArray;", "(Ljava/util/Collection;)[J", "toUShortArray", "Lkotlin/UShortArray;", "(Ljava/util/Collection;)[S", "kotlin-stdlib" }, k = 5, mv = { 1, 1, 15 }, xi = 1, xs = "kotlin/collections/UCollectionsKt")
class UCollectionsKt___UCollectionsKt
{
    public UCollectionsKt___UCollectionsKt() {
    }
    
    public static final int sumOfUByte(final Iterable<UByte> iterable) {
        Intrinsics.checkParameterIsNotNull(iterable, "$this$sum");
        final Iterator<UByte> iterator = iterable.iterator();
        int constructor-impl = 0;
        while (iterator.hasNext()) {
            constructor-impl = UInt.constructor-impl(constructor-impl + UInt.constructor-impl(iterator.next().unbox-impl() & 0xFF));
        }
        return constructor-impl;
    }
    
    public static final int sumOfUInt(final Iterable<UInt> iterable) {
        Intrinsics.checkParameterIsNotNull(iterable, "$this$sum");
        final Iterator<UInt> iterator = iterable.iterator();
        int constructor-impl = 0;
        while (iterator.hasNext()) {
            constructor-impl = UInt.constructor-impl(constructor-impl + iterator.next().unbox-impl());
        }
        return constructor-impl;
    }
    
    public static final long sumOfULong(final Iterable<ULong> iterable) {
        Intrinsics.checkParameterIsNotNull(iterable, "$this$sum");
        final Iterator<ULong> iterator = iterable.iterator();
        long constructor-impl = 0L;
        while (iterator.hasNext()) {
            constructor-impl = ULong.constructor-impl(constructor-impl + iterator.next().unbox-impl());
        }
        return constructor-impl;
    }
    
    public static final int sumOfUShort(final Iterable<UShort> iterable) {
        Intrinsics.checkParameterIsNotNull(iterable, "$this$sum");
        final Iterator<UShort> iterator = iterable.iterator();
        int constructor-impl = 0;
        while (iterator.hasNext()) {
            constructor-impl = UInt.constructor-impl(constructor-impl + UInt.constructor-impl(iterator.next().unbox-impl() & 0xFFFF));
        }
        return constructor-impl;
    }
    
    public static final byte[] toUByteArray(final Collection<UByte> collection) {
        Intrinsics.checkParameterIsNotNull(collection, "$this$toUByteArray");
        final byte[] constructor-impl = UByteArray.constructor-impl(collection.size());
        final Iterator<UByte> iterator = collection.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            UByteArray.set-VurrAj0(constructor-impl, n, iterator.next().unbox-impl());
            ++n;
        }
        return constructor-impl;
    }
    
    public static final int[] toUIntArray(final Collection<UInt> collection) {
        Intrinsics.checkParameterIsNotNull(collection, "$this$toUIntArray");
        final int[] constructor-impl = UIntArray.constructor-impl(collection.size());
        final Iterator<UInt> iterator = collection.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            UIntArray.set-VXSXFK8(constructor-impl, n, iterator.next().unbox-impl());
            ++n;
        }
        return constructor-impl;
    }
    
    public static final long[] toULongArray(final Collection<ULong> collection) {
        Intrinsics.checkParameterIsNotNull(collection, "$this$toULongArray");
        final long[] constructor-impl = ULongArray.constructor-impl(collection.size());
        final Iterator<ULong> iterator = collection.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            ULongArray.set-k8EXiF4(constructor-impl, n, iterator.next().unbox-impl());
            ++n;
        }
        return constructor-impl;
    }
    
    public static final short[] toUShortArray(final Collection<UShort> collection) {
        Intrinsics.checkParameterIsNotNull(collection, "$this$toUShortArray");
        final short[] constructor-impl = UShortArray.constructor-impl(collection.size());
        final Iterator<UShort> iterator = collection.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            UShortArray.set-01HTLdE(constructor-impl, n, iterator.next().unbox-impl());
            ++n;
        }
        return constructor-impl;
    }
}
