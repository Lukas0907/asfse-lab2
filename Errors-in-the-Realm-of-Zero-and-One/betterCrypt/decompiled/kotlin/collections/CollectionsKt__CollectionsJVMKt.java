// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.collections;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Collections;
import java.util.List;
import java.util.Arrays;
import kotlin.jvm.internal.Intrinsics;
import kotlin.TypeCastException;
import kotlin.jvm.internal.CollectionToArray;
import java.util.Collection;
import kotlin.internal.PlatformImplementationsKt;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u00002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u001e\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0011\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\u0081\b\u001a\u0011\u0010\u0003\u001a\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0001H\u0081\b\u001a\"\u0010\u0005\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u00062\n\u0010\b\u001a\u0006\u0012\u0002\b\u00030\tH\u0081\b¢\u0006\u0002\u0010\n\u001a4\u0010\u0005\u001a\b\u0012\u0004\u0012\u0002H\u000b0\u0006\"\u0004\b\u0000\u0010\u000b2\n\u0010\b\u001a\u0006\u0012\u0002\b\u00030\t2\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\u000b0\u0006H\u0081\b¢\u0006\u0002\u0010\r\u001a\u001f\u0010\u000e\u001a\b\u0012\u0004\u0012\u0002H\u000b0\u000f\"\u0004\b\u0000\u0010\u000b2\u0006\u0010\u0010\u001a\u0002H\u000b¢\u0006\u0002\u0010\u0011\u001a1\u0010\u0012\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u00070\u0006\"\u0004\b\u0000\u0010\u000b*\n\u0012\u0006\b\u0001\u0012\u0002H\u000b0\u00062\u0006\u0010\u0013\u001a\u00020\u0014H\u0000¢\u0006\u0002\u0010\u0015\u001a\u001f\u0010\u0016\u001a\b\u0012\u0004\u0012\u0002H\u000b0\u000f\"\u0004\b\u0000\u0010\u000b*\b\u0012\u0004\u0012\u0002H\u000b0\u0017H\u0087\b¨\u0006\u0018" }, d2 = { "checkCountOverflow", "", "count", "checkIndexOverflow", "index", "copyToArrayImpl", "", "", "collection", "", "(Ljava/util/Collection;)[Ljava/lang/Object;", "T", "array", "(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;", "listOf", "", "element", "(Ljava/lang/Object;)Ljava/util/List;", "copyToArrayOfAny", "isVarargs", "", "([Ljava/lang/Object;Z)[Ljava/lang/Object;", "toList", "Ljava/util/Enumeration;", "kotlin-stdlib" }, k = 5, mv = { 1, 1, 15 }, xi = 1, xs = "kotlin/collections/CollectionsKt")
class CollectionsKt__CollectionsJVMKt
{
    public CollectionsKt__CollectionsJVMKt() {
    }
    
    private static final int checkCountOverflow(final int n) {
        if (n >= 0) {
            return n;
        }
        if (PlatformImplementationsKt.apiVersionIsAtLeast(1, 3, 0)) {
            CollectionsKt.throwCountOverflow();
            return n;
        }
        throw new ArithmeticException("Count overflow has happened.");
    }
    
    private static final int checkIndexOverflow(final int n) {
        if (n >= 0) {
            return n;
        }
        if (PlatformImplementationsKt.apiVersionIsAtLeast(1, 3, 0)) {
            CollectionsKt.throwIndexOverflow();
            return n;
        }
        throw new ArithmeticException("Index overflow has happened.");
    }
    
    private static final Object[] copyToArrayImpl(final Collection<?> collection) {
        return CollectionToArray.toArray(collection);
    }
    
    private static final <T> T[] copyToArrayImpl(final Collection<?> collection, final T[] array) {
        if (array == null) {
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
        }
        final Object[] array2 = CollectionToArray.toArray(collection, array);
        if (array2 != null) {
            return (T[])array2;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }
    
    public static final <T> Object[] copyToArrayOfAny(final T[] original, final boolean b) {
        Intrinsics.checkParameterIsNotNull(original, "$this$copyToArrayOfAny");
        if (b && Intrinsics.areEqual(original.getClass(), Object[].class)) {
            return original;
        }
        final Object[] copy = Arrays.copyOf(original, original.length, (Class<? extends Object[]>)Object[].class);
        Intrinsics.checkExpressionValueIsNotNull(copy, "java.util.Arrays.copyOf(\u2026 Array<Any?>::class.java)");
        return copy;
    }
    
    public static final <T> List<T> listOf(final T o) {
        final List<T> singletonList = Collections.singletonList(o);
        Intrinsics.checkExpressionValueIsNotNull(singletonList, "java.util.Collections.singletonList(element)");
        return singletonList;
    }
    
    private static final <T> List<T> toList(final Enumeration<T> e) {
        final ArrayList<T> list = Collections.list(e);
        Intrinsics.checkExpressionValueIsNotNull(list, "java.util.Collections.list(this)");
        return list;
    }
}
