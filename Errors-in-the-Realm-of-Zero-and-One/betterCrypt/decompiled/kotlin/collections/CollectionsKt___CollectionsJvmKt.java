// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.collections;

import java.util.Comparator;
import java.util.TreeSet;
import java.util.SortedSet;
import java.util.Collections;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import kotlin.jvm.internal.Intrinsics;
import java.util.List;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000>\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u001f\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a(\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\u0006\u0012\u0002\b\u00030\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0005\u001aA\u0010\u0006\u001a\u0002H\u0007\"\u0010\b\u0000\u0010\u0007*\n\u0012\u0006\b\u0000\u0012\u0002H\u00020\b\"\u0004\b\u0001\u0010\u0002*\u0006\u0012\u0002\b\u00030\u00032\u0006\u0010\t\u001a\u0002H\u00072\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0005¢\u0006\u0002\u0010\n\u001a\u0016\u0010\u000b\u001a\u00020\f\"\u0004\b\u0000\u0010\r*\b\u0012\u0004\u0012\u0002H\r0\u000e\u001a&\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\r0\u0010\"\u000e\b\u0000\u0010\r*\b\u0012\u0004\u0012\u0002H\r0\u0011*\b\u0012\u0004\u0012\u0002H\r0\u0003\u001a8\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\r0\u0010\"\u0004\b\u0000\u0010\r*\b\u0012\u0004\u0012\u0002H\r0\u00032\u001a\u0010\u0012\u001a\u0016\u0012\u0006\b\u0000\u0012\u0002H\r0\u0013j\n\u0012\u0006\b\u0000\u0012\u0002H\r`\u0014¨\u0006\u0015" }, d2 = { "filterIsInstance", "", "R", "", "klass", "Ljava/lang/Class;", "filterIsInstanceTo", "C", "", "destination", "(Ljava/lang/Iterable;Ljava/util/Collection;Ljava/lang/Class;)Ljava/util/Collection;", "reverse", "", "T", "", "toSortedSet", "Ljava/util/SortedSet;", "", "comparator", "Ljava/util/Comparator;", "Lkotlin/Comparator;", "kotlin-stdlib" }, k = 5, mv = { 1, 1, 15 }, xi = 1, xs = "kotlin/collections/CollectionsKt")
class CollectionsKt___CollectionsJvmKt extends CollectionsKt__ReversedViewsKt
{
    public CollectionsKt___CollectionsJvmKt() {
    }
    
    public static final <R> List<R> filterIsInstance(final Iterable<?> iterable, final Class<R> clazz) {
        Intrinsics.checkParameterIsNotNull(iterable, "$this$filterIsInstance");
        Intrinsics.checkParameterIsNotNull(clazz, "klass");
        return filterIsInstanceTo(iterable, new ArrayList<R>(), clazz);
    }
    
    public static final <C extends Collection<? super R>, R> C filterIsInstanceTo(final Iterable<?> iterable, final C c, final Class<R> clazz) {
        Intrinsics.checkParameterIsNotNull(iterable, "$this$filterIsInstanceTo");
        Intrinsics.checkParameterIsNotNull(c, "destination");
        Intrinsics.checkParameterIsNotNull(clazz, "klass");
        for (final Object next : iterable) {
            if (clazz.isInstance(next)) {
                c.add((Object)next);
            }
        }
        return c;
    }
    
    public static final <T> void reverse(final List<T> list) {
        Intrinsics.checkParameterIsNotNull(list, "$this$reverse");
        Collections.reverse(list);
    }
    
    public static final <T extends Comparable<? super T>> SortedSet<T> toSortedSet(final Iterable<? extends T> iterable) {
        Intrinsics.checkParameterIsNotNull(iterable, "$this$toSortedSet");
        return CollectionsKt___CollectionsKt.toCollection((Iterable<?>)iterable, (TreeSet<T>)new TreeSet<T>());
    }
    
    public static final <T> SortedSet<T> toSortedSet(final Iterable<? extends T> iterable, final Comparator<? super T> comparator) {
        Intrinsics.checkParameterIsNotNull(iterable, "$this$toSortedSet");
        Intrinsics.checkParameterIsNotNull(comparator, "comparator");
        return CollectionsKt___CollectionsKt.toCollection((Iterable<?>)iterable, (TreeSet<T>)new TreeSet<T>(comparator));
    }
}
