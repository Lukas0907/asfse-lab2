// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.collections;

import kotlin.jvm.functions.Function1;
import java.util.NoSuchElementException;
import kotlin.jvm.internal.Intrinsics;
import java.util.Map;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001e\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\b\u0002\u001a3\u0010\u0000\u001a\u0002H\u0001\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0001*\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00010\u00032\u0006\u0010\u0004\u001a\u0002H\u0002H\u0001¢\u0006\u0004\b\u0005\u0010\u0006\u001aQ\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00010\u0003\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0001*\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00010\u00032!\u0010\b\u001a\u001d\u0012\u0013\u0012\u0011H\u0002¢\u0006\f\b\n\u0012\b\b\u000b\u0012\u0004\b\b(\u0004\u0012\u0004\u0012\u0002H\u00010\t\u001aX\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00010\f\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0001*\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00010\f2!\u0010\b\u001a\u001d\u0012\u0013\u0012\u0011H\u0002¢\u0006\f\b\n\u0012\b\b\u000b\u0012\u0004\b\b(\u0004\u0012\u0004\u0012\u0002H\u00010\tH\u0007¢\u0006\u0002\b\r¨\u0006\u000e" }, d2 = { "getOrImplicitDefault", "V", "K", "", "key", "getOrImplicitDefaultNullable", "(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;", "withDefault", "defaultValue", "Lkotlin/Function1;", "Lkotlin/ParameterName;", "name", "", "withDefaultMutable", "kotlin-stdlib" }, k = 5, mv = { 1, 1, 15 }, xi = 1, xs = "kotlin/collections/MapsKt")
class MapsKt__MapWithDefaultKt
{
    public MapsKt__MapWithDefaultKt() {
    }
    
    public static final <K, V> V getOrImplicitDefaultNullable(final Map<K, ? extends V> map, final K obj) {
        Intrinsics.checkParameterIsNotNull(map, "$this$getOrImplicitDefault");
        if (map instanceof MapWithDefault) {
            return ((MapWithDefault<K, V>)map).getOrImplicitDefault(obj);
        }
        final Object value = map.get(obj);
        if (value != null) {
            return (V)value;
        }
        if (map.containsKey(obj)) {
            return (V)value;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Key ");
        sb.append(obj);
        sb.append(" is missing in the map.");
        throw new NoSuchElementException(sb.toString());
    }
    
    public static final <K, V> Map<K, V> withDefault(final Map<K, ? extends V> map, final Function1<? super K, ? extends V> function1) {
        Intrinsics.checkParameterIsNotNull(map, "$this$withDefault");
        Intrinsics.checkParameterIsNotNull(function1, "defaultValue");
        if (map instanceof MapWithDefault) {
            return (Map<K, V>)withDefault((Map<Object, ?>)((MapWithDefault<K, ? extends V>)map).getMap(), (Function1<? super Object, ?>)function1);
        }
        return new MapWithDefaultImpl<K, V>(map, function1);
    }
    
    public static final <K, V> Map<K, V> withDefaultMutable(final Map<K, V> map, final Function1<? super K, ? extends V> function1) {
        Intrinsics.checkParameterIsNotNull(map, "$this$withDefault");
        Intrinsics.checkParameterIsNotNull(function1, "defaultValue");
        if (map instanceof MutableMapWithDefault) {
            return (Map<K, V>)withDefaultMutable((Map<Object, Object>)((MutableMapWithDefault<K, V>)map).getMap(), (Function1<? super Object, ?>)function1);
        }
        return new MutableMapWithDefaultImpl<K, V>(map, function1);
    }
}
