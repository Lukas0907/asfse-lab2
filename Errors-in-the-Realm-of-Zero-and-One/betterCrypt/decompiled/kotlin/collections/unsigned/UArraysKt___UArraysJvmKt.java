// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.collections.unsigned;

import kotlin.UByteArray;
import kotlin.ULongArray;
import kotlin.UShortArray;
import kotlin.UnsignedKt;
import kotlin.UIntArray;
import kotlin.collections.AbstractList;
import kotlin.UShort;
import kotlin.ULong;
import kotlin.UByte;
import kotlin.jvm.internal.Intrinsics;
import kotlin.UInt;
import java.util.List;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000>\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0016\u001a\u001c\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u0003H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005\u001a\u001c\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00060\u0001*\u00020\u0007H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\b\u0010\t\u001a\u001c\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\n0\u0001*\u00020\u000bH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\f\u0010\r\u001a\u001c\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0001*\u00020\u000fH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0010\u0010\u0011\u001a2\u0010\u0012\u001a\u00020\u0013*\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00022\b\b\u0002\u0010\u0015\u001a\u00020\u00132\b\b\u0002\u0010\u0016\u001a\u00020\u0013H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0017\u0010\u0018\u001a2\u0010\u0012\u001a\u00020\u0013*\u00020\u00072\u0006\u0010\u0014\u001a\u00020\u00062\b\b\u0002\u0010\u0015\u001a\u00020\u00132\b\b\u0002\u0010\u0016\u001a\u00020\u0013H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0019\u0010\u001a\u001a2\u0010\u0012\u001a\u00020\u0013*\u00020\u000b2\u0006\u0010\u0014\u001a\u00020\n2\b\b\u0002\u0010\u0015\u001a\u00020\u00132\b\b\u0002\u0010\u0016\u001a\u00020\u0013H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u001b\u0010\u001c\u001a2\u0010\u0012\u001a\u00020\u0013*\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u000e2\b\b\u0002\u0010\u0015\u001a\u00020\u00132\b\b\u0002\u0010\u0016\u001a\u00020\u0013H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u001d\u0010\u001e\u001a\u001f\u0010\u001f\u001a\u00020\u0002*\u00020\u00032\u0006\u0010 \u001a\u00020\u0013H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b!\u0010\"\u001a\u001f\u0010\u001f\u001a\u00020\u0006*\u00020\u00072\u0006\u0010 \u001a\u00020\u0013H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b#\u0010$\u001a\u001f\u0010\u001f\u001a\u00020\n*\u00020\u000b2\u0006\u0010 \u001a\u00020\u0013H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b%\u0010&\u001a\u001f\u0010\u001f\u001a\u00020\u000e*\u00020\u000f2\u0006\u0010 \u001a\u00020\u0013H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b'\u0010(\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006)" }, d2 = { "asList", "", "Lkotlin/UByte;", "Lkotlin/UByteArray;", "asList-GBYM_sE", "([B)Ljava/util/List;", "Lkotlin/UInt;", "Lkotlin/UIntArray;", "asList--ajY-9A", "([I)Ljava/util/List;", "Lkotlin/ULong;", "Lkotlin/ULongArray;", "asList-QwZRm1k", "([J)Ljava/util/List;", "Lkotlin/UShort;", "Lkotlin/UShortArray;", "asList-rL5Bavg", "([S)Ljava/util/List;", "binarySearch", "", "element", "fromIndex", "toIndex", "binarySearch-WpHrYlw", "([BBII)I", "binarySearch-2fe2U9s", "([IIII)I", "binarySearch-K6DWlUc", "([JJII)I", "binarySearch-EtDCXyQ", "([SSII)I", "elementAt", "index", "elementAt-PpDY95g", "([BI)B", "elementAt-qFRl0hI", "([II)I", "elementAt-r7IrZao", "([JI)J", "elementAt-nggk6HY", "([SI)S", "kotlin-stdlib" }, k = 5, mv = { 1, 1, 15 }, pn = "kotlin.collections", xi = 1, xs = "kotlin/collections/unsigned/UArraysKt")
class UArraysKt___UArraysJvmKt
{
    public UArraysKt___UArraysJvmKt() {
    }
    
    public static final List<UInt> asList--ajY-9A(final int[] array) {
        Intrinsics.checkParameterIsNotNull(array, "$this$asList");
        return (List<UInt>)new UArraysKt___UArraysJvmKt$asList.UArraysKt___UArraysJvmKt$asList$1(array);
    }
    
    public static final List<UByte> asList-GBYM_sE(final byte[] array) {
        Intrinsics.checkParameterIsNotNull(array, "$this$asList");
        return (List<UByte>)new UArraysKt___UArraysJvmKt$asList.UArraysKt___UArraysJvmKt$asList$3(array);
    }
    
    public static final List<ULong> asList-QwZRm1k(final long[] array) {
        Intrinsics.checkParameterIsNotNull(array, "$this$asList");
        return (List<ULong>)new UArraysKt___UArraysJvmKt$asList.UArraysKt___UArraysJvmKt$asList$2(array);
    }
    
    public static final List<UShort> asList-rL5Bavg(final short[] array) {
        Intrinsics.checkParameterIsNotNull(array, "$this$asList");
        return (List<UShort>)new UArraysKt___UArraysJvmKt$asList.UArraysKt___UArraysJvmKt$asList$4(array);
    }
    
    public static final int binarySearch-2fe2U9s(final int[] array, final int n, int i, int n2) {
        Intrinsics.checkParameterIsNotNull(array, "$this$binarySearch");
        AbstractList.Companion.checkRangeIndexes$kotlin_stdlib(i, n2, UIntArray.getSize-impl(array));
        --n2;
        while (i <= n2) {
            final int n3 = i + n2 >>> 1;
            final int uintCompare = UnsignedKt.uintCompare(array[n3], n);
            if (uintCompare < 0) {
                i = n3 + 1;
            }
            else {
                if (uintCompare <= 0) {
                    return n3;
                }
                n2 = n3 - 1;
            }
        }
        return -(i + 1);
    }
    
    public static final int binarySearch-EtDCXyQ(final short[] array, final short n, int i, int n2) {
        Intrinsics.checkParameterIsNotNull(array, "$this$binarySearch");
        AbstractList.Companion.checkRangeIndexes$kotlin_stdlib(i, n2, UShortArray.getSize-impl(array));
        --n2;
        while (i <= n2) {
            final int n3 = i + n2 >>> 1;
            final int uintCompare = UnsignedKt.uintCompare(array[n3], n & 0xFFFF);
            if (uintCompare < 0) {
                i = n3 + 1;
            }
            else {
                if (uintCompare <= 0) {
                    return n3;
                }
                n2 = n3 - 1;
            }
        }
        return -(i + 1);
    }
    
    public static final int binarySearch-K6DWlUc(final long[] array, final long n, int i, int n2) {
        Intrinsics.checkParameterIsNotNull(array, "$this$binarySearch");
        AbstractList.Companion.checkRangeIndexes$kotlin_stdlib(i, n2, ULongArray.getSize-impl(array));
        --n2;
        while (i <= n2) {
            final int n3 = i + n2 >>> 1;
            final int ulongCompare = UnsignedKt.ulongCompare(array[n3], n);
            if (ulongCompare < 0) {
                i = n3 + 1;
            }
            else {
                if (ulongCompare <= 0) {
                    return n3;
                }
                n2 = n3 - 1;
            }
        }
        return -(i + 1);
    }
    
    public static final int binarySearch-WpHrYlw(final byte[] array, final byte b, int i, int n) {
        Intrinsics.checkParameterIsNotNull(array, "$this$binarySearch");
        AbstractList.Companion.checkRangeIndexes$kotlin_stdlib(i, n, UByteArray.getSize-impl(array));
        --n;
        while (i <= n) {
            final int n2 = i + n >>> 1;
            final int uintCompare = UnsignedKt.uintCompare(array[n2], b & 0xFF);
            if (uintCompare < 0) {
                i = n2 + 1;
            }
            else {
                if (uintCompare <= 0) {
                    return n2;
                }
                n = n2 - 1;
            }
        }
        return -(i + 1);
    }
    
    private static final byte elementAt-PpDY95g(final byte[] array, final int n) {
        return UByteArray.get-impl(array, n);
    }
    
    private static final short elementAt-nggk6HY(final short[] array, final int n) {
        return UShortArray.get-impl(array, n);
    }
    
    private static final int elementAt-qFRl0hI(final int[] array, final int n) {
        return UIntArray.get-impl(array, n);
    }
    
    private static final long elementAt-r7IrZao(final long[] array, final int n) {
        return ULongArray.get-impl(array, n);
    }
}
