// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.collections;

import kotlin.TuplesKt;
import kotlin.Pair;
import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.Set;
import java.util.Collection;
import kotlin.jvm.internal.Intrinsics;
import java.util.Iterator;
import kotlin.jvm.functions.Function0;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000:\n\u0000\n\u0002\u0010\u001c\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010(\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u001e\n\u0002\b\u0003\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a+\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u00022\u0014\b\u0004\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u00050\u0004H\u0087\b\u001a \u0010\u0006\u001a\u00020\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\u0006\u0010\b\u001a\u00020\u0007H\u0001\u001a\u001f\u0010\t\u001a\u0004\u0018\u00010\u0007\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0001H\u0001¢\u0006\u0002\u0010\n\u001a\u001e\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\u00020\f\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0001H\u0000\u001a,\u0010\r\u001a\b\u0012\u0004\u0012\u0002H\u00020\f\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u00012\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001H\u0000\u001a\"\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0010\"\u0004\b\u0000\u0010\u0002*\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u00010\u0001\u001a\u001d\u0010\u0011\u001a\u00020\u0012\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\fH\u0002¢\u0006\u0002\b\u0013\u001a@\u0010\u0014\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00020\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00160\u00100\u0015\"\u0004\b\u0000\u0010\u0002\"\u0004\b\u0001\u0010\u0016*\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00160\u00150\u0001¨\u0006\u0017" }, d2 = { "Iterable", "", "T", "iterator", "Lkotlin/Function0;", "", "collectionSizeOrDefault", "", "default", "collectionSizeOrNull", "(Ljava/lang/Iterable;)Ljava/lang/Integer;", "convertToSetForSetOperation", "", "convertToSetForSetOperationWith", "source", "flatten", "", "safeToConvertToSet", "", "safeToConvertToSet$CollectionsKt__IterablesKt", "unzip", "Lkotlin/Pair;", "R", "kotlin-stdlib" }, k = 5, mv = { 1, 1, 15 }, xi = 1, xs = "kotlin/collections/CollectionsKt")
class CollectionsKt__IterablesKt extends CollectionsKt__CollectionsKt
{
    public CollectionsKt__IterablesKt() {
    }
    
    private static final <T> Iterable<T> Iterable(final Function0<? extends Iterator<? extends T>> function0) {
        return (Iterable<T>)new CollectionsKt__IterablesKt$Iterable.CollectionsKt__IterablesKt$Iterable$1((Function0)function0);
    }
    
    public static final <T> int collectionSizeOrDefault(final Iterable<? extends T> iterable, int size) {
        Intrinsics.checkParameterIsNotNull(iterable, "$this$collectionSizeOrDefault");
        if (iterable instanceof Collection) {
            size = ((Collection)iterable).size();
        }
        return size;
    }
    
    public static final <T> Integer collectionSizeOrNull(final Iterable<? extends T> iterable) {
        Intrinsics.checkParameterIsNotNull(iterable, "$this$collectionSizeOrNull");
        if (iterable instanceof Collection) {
            return ((Collection)iterable).size();
        }
        return null;
    }
    
    public static final <T> Collection<T> convertToSetForSetOperation(final Iterable<? extends T> iterable) {
        Intrinsics.checkParameterIsNotNull(iterable, "$this$convertToSetForSetOperation");
        if (iterable instanceof Set) {
            return (Collection<T>)iterable;
        }
        if (!(iterable instanceof Collection)) {
            return (Collection<T>)CollectionsKt___CollectionsKt.toHashSet((Iterable<?>)iterable);
        }
        final Collection<? extends T> collection = (Collection<? extends T>)iterable;
        if (safeToConvertToSet$CollectionsKt__IterablesKt((Collection<?>)collection)) {
            return (Collection<T>)CollectionsKt___CollectionsKt.toHashSet((Iterable<?>)iterable);
        }
        return (Collection<T>)collection;
    }
    
    public static final <T> Collection<T> convertToSetForSetOperationWith(final Iterable<? extends T> iterable, final Iterable<? extends T> iterable2) {
        Intrinsics.checkParameterIsNotNull(iterable, "$this$convertToSetForSetOperationWith");
        Intrinsics.checkParameterIsNotNull(iterable2, "source");
        if (iterable instanceof Set) {
            return (Collection<T>)iterable;
        }
        if (!(iterable instanceof Collection)) {
            return (Collection<T>)CollectionsKt___CollectionsKt.toHashSet((Iterable<?>)iterable);
        }
        if (iterable2 instanceof Collection && ((Collection)iterable2).size() < 2) {
            return (Collection<T>)iterable;
        }
        final Collection<? extends T> collection = (Collection<? extends T>)iterable;
        if (safeToConvertToSet$CollectionsKt__IterablesKt((Collection<?>)collection)) {
            return (Collection<T>)CollectionsKt___CollectionsKt.toHashSet((Iterable<?>)iterable);
        }
        return (Collection<T>)collection;
    }
    
    public static final <T> List<T> flatten(final Iterable<? extends Iterable<? extends T>> iterable) {
        Intrinsics.checkParameterIsNotNull(iterable, "$this$flatten");
        final ArrayList<T> list = new ArrayList<T>();
        final Iterator<? extends Iterable<? extends T>> iterator = iterable.iterator();
        while (iterator.hasNext()) {
            CollectionsKt__MutableCollectionsKt.addAll((Collection<? super Object>)list, (Iterable<?>)iterator.next());
        }
        return list;
    }
    
    private static final <T> boolean safeToConvertToSet$CollectionsKt__IterablesKt(final Collection<? extends T> collection) {
        return collection.size() > 2 && collection instanceof ArrayList;
    }
    
    public static final <T, R> Pair<List<T>, List<R>> unzip(final Iterable<? extends Pair<? extends T, ? extends R>> iterable) {
        Intrinsics.checkParameterIsNotNull(iterable, "$this$unzip");
        final int collectionSizeOrDefault = collectionSizeOrDefault((Iterable<?>)iterable, 10);
        final ArrayList list = new ArrayList<T>(collectionSizeOrDefault);
        final ArrayList list2 = new ArrayList<R>(collectionSizeOrDefault);
        for (final Pair<Object, B> pair : iterable) {
            list.add((T)pair.getFirst());
            list2.add((R)pair.getSecond());
        }
        return (Pair<List<T>, List<R>>)TuplesKt.to(list, list2);
    }
}
