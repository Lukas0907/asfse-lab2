// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.collections;

import kotlin.UShort;
import kotlin.ULong;
import kotlin.UByte;
import kotlin.UInt;
import java.util.NoSuchElementException;
import kotlin.random.Random;
import kotlin.UShortArray;
import kotlin.ULongArray;
import kotlin.UByteArray;
import kotlin.jvm.functions.Function1;
import kotlin.UIntArray;
import kotlin.jvm.JvmStatic;
import java.util.Arrays;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;
import kotlin.DeprecationLevel;
import kotlin.Deprecated;

@Deprecated(level = DeprecationLevel.HIDDEN, message = "Provided for binary compatibility")
@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000l\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\b\t\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001f\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0005H\u0087\u0004\u00f8\u0001\u0000¢\u0006\u0004\b\u0007\u0010\bJ\u001f\u0010\u0003\u001a\u00020\u0004*\u00020\t2\u0006\u0010\u0006\u001a\u00020\tH\u0087\u0004\u00f8\u0001\u0000¢\u0006\u0004\b\n\u0010\u000bJ\u001f\u0010\u0003\u001a\u00020\u0004*\u00020\f2\u0006\u0010\u0006\u001a\u00020\fH\u0087\u0004\u00f8\u0001\u0000¢\u0006\u0004\b\r\u0010\u000eJ\u001f\u0010\u0003\u001a\u00020\u0004*\u00020\u000f2\u0006\u0010\u0006\u001a\u00020\u000fH\u0087\u0004\u00f8\u0001\u0000¢\u0006\u0004\b\u0010\u0010\u0011J\u0016\u0010\u0012\u001a\u00020\u0013*\u00020\u0005H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0014\u0010\u0015J\u0016\u0010\u0012\u001a\u00020\u0013*\u00020\tH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0016\u0010\u0017J\u0016\u0010\u0012\u001a\u00020\u0013*\u00020\fH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0018\u0010\u0019J\u0016\u0010\u0012\u001a\u00020\u0013*\u00020\u000fH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u001a\u0010\u001bJ\u0016\u0010\u001c\u001a\u00020\u001d*\u00020\u0005H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u001e\u0010\u001fJ\u0016\u0010\u001c\u001a\u00020\u001d*\u00020\tH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b \u0010!J\u0016\u0010\u001c\u001a\u00020\u001d*\u00020\fH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\"\u0010#J\u0016\u0010\u001c\u001a\u00020\u001d*\u00020\u000fH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b$\u0010%J\u001e\u0010&\u001a\u00020'*\u00020\u00052\u0006\u0010&\u001a\u00020(H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b)\u0010*J\u001e\u0010&\u001a\u00020+*\u00020\t2\u0006\u0010&\u001a\u00020(H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b,\u0010-J\u001e\u0010&\u001a\u00020.*\u00020\f2\u0006\u0010&\u001a\u00020(H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b/\u00100J\u001e\u0010&\u001a\u000201*\u00020\u000f2\u0006\u0010&\u001a\u00020(H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b2\u00103J\u001c\u00104\u001a\b\u0012\u0004\u0012\u00020'05*\u00020\u0005H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b6\u00107J\u001c\u00104\u001a\b\u0012\u0004\u0012\u00020+05*\u00020\tH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b8\u00109J\u001c\u00104\u001a\b\u0012\u0004\u0012\u00020.05*\u00020\fH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b:\u0010;J\u001c\u00104\u001a\b\u0012\u0004\u0012\u00020105*\u00020\u000fH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b<\u0010=\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006>" }, d2 = { "Lkotlin/collections/UArraysKt;", "", "()V", "contentEquals", "", "Lkotlin/UByteArray;", "other", "contentEquals-kdPth3s", "([B[B)Z", "Lkotlin/UIntArray;", "contentEquals-ctEhBpI", "([I[I)Z", "Lkotlin/ULongArray;", "contentEquals-us8wMrg", "([J[J)Z", "Lkotlin/UShortArray;", "contentEquals-mazbYpA", "([S[S)Z", "contentHashCode", "", "contentHashCode-GBYM_sE", "([B)I", "contentHashCode--ajY-9A", "([I)I", "contentHashCode-QwZRm1k", "([J)I", "contentHashCode-rL5Bavg", "([S)I", "contentToString", "", "contentToString-GBYM_sE", "([B)Ljava/lang/String;", "contentToString--ajY-9A", "([I)Ljava/lang/String;", "contentToString-QwZRm1k", "([J)Ljava/lang/String;", "contentToString-rL5Bavg", "([S)Ljava/lang/String;", "random", "Lkotlin/UByte;", "Lkotlin/random/Random;", "random-oSF2wD8", "([BLkotlin/random/Random;)B", "Lkotlin/UInt;", "random-2D5oskM", "([ILkotlin/random/Random;)I", "Lkotlin/ULong;", "random-JzugnMA", "([JLkotlin/random/Random;)J", "Lkotlin/UShort;", "random-s5X_as8", "([SLkotlin/random/Random;)S", "toTypedArray", "", "toTypedArray-GBYM_sE", "([B)[Lkotlin/UByte;", "toTypedArray--ajY-9A", "([I)[Lkotlin/UInt;", "toTypedArray-QwZRm1k", "([J)[Lkotlin/ULong;", "toTypedArray-rL5Bavg", "([S)[Lkotlin/UShort;", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
public final class UArraysKt
{
    public static final UArraysKt INSTANCE;
    
    static {
        INSTANCE = new UArraysKt();
    }
    
    private UArraysKt() {
    }
    
    @JvmStatic
    public static final boolean contentEquals-ctEhBpI(final int[] a, final int[] a2) {
        Intrinsics.checkParameterIsNotNull(a, "$this$contentEquals");
        Intrinsics.checkParameterIsNotNull(a2, "other");
        return Arrays.equals(a, a2);
    }
    
    @JvmStatic
    public static final boolean contentEquals-kdPth3s(final byte[] a, final byte[] a2) {
        Intrinsics.checkParameterIsNotNull(a, "$this$contentEquals");
        Intrinsics.checkParameterIsNotNull(a2, "other");
        return Arrays.equals(a, a2);
    }
    
    @JvmStatic
    public static final boolean contentEquals-mazbYpA(final short[] a, final short[] a2) {
        Intrinsics.checkParameterIsNotNull(a, "$this$contentEquals");
        Intrinsics.checkParameterIsNotNull(a2, "other");
        return Arrays.equals(a, a2);
    }
    
    @JvmStatic
    public static final boolean contentEquals-us8wMrg(final long[] a, final long[] a2) {
        Intrinsics.checkParameterIsNotNull(a, "$this$contentEquals");
        Intrinsics.checkParameterIsNotNull(a2, "other");
        return Arrays.equals(a, a2);
    }
    
    @JvmStatic
    public static final int contentHashCode--ajY-9A(final int[] a) {
        Intrinsics.checkParameterIsNotNull(a, "$this$contentHashCode");
        return Arrays.hashCode(a);
    }
    
    @JvmStatic
    public static final int contentHashCode-GBYM_sE(final byte[] a) {
        Intrinsics.checkParameterIsNotNull(a, "$this$contentHashCode");
        return Arrays.hashCode(a);
    }
    
    @JvmStatic
    public static final int contentHashCode-QwZRm1k(final long[] a) {
        Intrinsics.checkParameterIsNotNull(a, "$this$contentHashCode");
        return Arrays.hashCode(a);
    }
    
    @JvmStatic
    public static final int contentHashCode-rL5Bavg(final short[] a) {
        Intrinsics.checkParameterIsNotNull(a, "$this$contentHashCode");
        return Arrays.hashCode(a);
    }
    
    @JvmStatic
    public static final String contentToString--ajY-9A(final int[] array) {
        Intrinsics.checkParameterIsNotNull(array, "$this$contentToString");
        return CollectionsKt___CollectionsKt.joinToString$default(UIntArray.box-impl(array), ", ", "[", "]", 0, null, null, 56, null);
    }
    
    @JvmStatic
    public static final String contentToString-GBYM_sE(final byte[] array) {
        Intrinsics.checkParameterIsNotNull(array, "$this$contentToString");
        return CollectionsKt___CollectionsKt.joinToString$default(UByteArray.box-impl(array), ", ", "[", "]", 0, null, null, 56, null);
    }
    
    @JvmStatic
    public static final String contentToString-QwZRm1k(final long[] array) {
        Intrinsics.checkParameterIsNotNull(array, "$this$contentToString");
        return CollectionsKt___CollectionsKt.joinToString$default(ULongArray.box-impl(array), ", ", "[", "]", 0, null, null, 56, null);
    }
    
    @JvmStatic
    public static final String contentToString-rL5Bavg(final short[] array) {
        Intrinsics.checkParameterIsNotNull(array, "$this$contentToString");
        return CollectionsKt___CollectionsKt.joinToString$default(UShortArray.box-impl(array), ", ", "[", "]", 0, null, null, 56, null);
    }
    
    @JvmStatic
    public static final int random-2D5oskM(final int[] array, final Random random) {
        Intrinsics.checkParameterIsNotNull(array, "$this$random");
        Intrinsics.checkParameterIsNotNull(random, "random");
        if (!UIntArray.isEmpty-impl(array)) {
            return UIntArray.get-impl(array, random.nextInt(UIntArray.getSize-impl(array)));
        }
        throw new NoSuchElementException("Array is empty.");
    }
    
    @JvmStatic
    public static final long random-JzugnMA(final long[] array, final Random random) {
        Intrinsics.checkParameterIsNotNull(array, "$this$random");
        Intrinsics.checkParameterIsNotNull(random, "random");
        if (!ULongArray.isEmpty-impl(array)) {
            return ULongArray.get-impl(array, random.nextInt(ULongArray.getSize-impl(array)));
        }
        throw new NoSuchElementException("Array is empty.");
    }
    
    @JvmStatic
    public static final byte random-oSF2wD8(final byte[] array, final Random random) {
        Intrinsics.checkParameterIsNotNull(array, "$this$random");
        Intrinsics.checkParameterIsNotNull(random, "random");
        if (!UByteArray.isEmpty-impl(array)) {
            return UByteArray.get-impl(array, random.nextInt(UByteArray.getSize-impl(array)));
        }
        throw new NoSuchElementException("Array is empty.");
    }
    
    @JvmStatic
    public static final short random-s5X_as8(final short[] array, final Random random) {
        Intrinsics.checkParameterIsNotNull(array, "$this$random");
        Intrinsics.checkParameterIsNotNull(random, "random");
        if (!UShortArray.isEmpty-impl(array)) {
            return UShortArray.get-impl(array, random.nextInt(UShortArray.getSize-impl(array)));
        }
        throw new NoSuchElementException("Array is empty.");
    }
    
    @JvmStatic
    public static final UInt[] toTypedArray--ajY-9A(final int[] array) {
        Intrinsics.checkParameterIsNotNull(array, "$this$toTypedArray");
        final int size-impl = UIntArray.getSize-impl(array);
        final UInt[] array2 = new UInt[size-impl];
        for (int i = 0; i < size-impl; ++i) {
            array2[i] = UInt.box-impl(UIntArray.get-impl(array, i));
        }
        return array2;
    }
    
    @JvmStatic
    public static final UByte[] toTypedArray-GBYM_sE(final byte[] array) {
        Intrinsics.checkParameterIsNotNull(array, "$this$toTypedArray");
        final int size-impl = UByteArray.getSize-impl(array);
        final UByte[] array2 = new UByte[size-impl];
        for (int i = 0; i < size-impl; ++i) {
            array2[i] = UByte.box-impl(UByteArray.get-impl(array, i));
        }
        return array2;
    }
    
    @JvmStatic
    public static final ULong[] toTypedArray-QwZRm1k(final long[] array) {
        Intrinsics.checkParameterIsNotNull(array, "$this$toTypedArray");
        final int size-impl = ULongArray.getSize-impl(array);
        final ULong[] array2 = new ULong[size-impl];
        for (int i = 0; i < size-impl; ++i) {
            array2[i] = ULong.box-impl(ULongArray.get-impl(array, i));
        }
        return array2;
    }
    
    @JvmStatic
    public static final UShort[] toTypedArray-rL5Bavg(final short[] array) {
        Intrinsics.checkParameterIsNotNull(array, "$this$toTypedArray");
        final int size-impl = UShortArray.getSize-impl(array);
        final UShort[] array2 = new UShort[size-impl];
        for (int i = 0; i < size-impl; ++i) {
            array2[i] = UShort.box-impl(UShortArray.get-impl(array, i));
        }
        return array2;
    }
}
