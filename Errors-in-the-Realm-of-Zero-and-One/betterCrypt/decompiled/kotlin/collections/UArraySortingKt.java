// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.collections;

import kotlin.UIntArray;
import kotlin.UShortArray;
import kotlin.jvm.internal.Intrinsics;
import kotlin.UByteArray;
import kotlin.UnsignedKt;
import kotlin.ULongArray;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u00000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0012\u001a*\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0001H\u0003\u00f8\u0001\u0000¢\u0006\u0004\b\u0006\u0010\u0007\u001a*\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\b2\u0006\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0001H\u0003\u00f8\u0001\u0000¢\u0006\u0004\b\t\u0010\n\u001a*\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u000b2\u0006\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0001H\u0003\u00f8\u0001\u0000¢\u0006\u0004\b\f\u0010\r\u001a*\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u000e2\u0006\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0001H\u0003\u00f8\u0001\u0000¢\u0006\u0004\b\u000f\u0010\u0010\u001a*\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0001H\u0003\u00f8\u0001\u0000¢\u0006\u0004\b\u0013\u0010\u0014\u001a*\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0002\u001a\u00020\b2\u0006\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0001H\u0003\u00f8\u0001\u0000¢\u0006\u0004\b\u0015\u0010\u0016\u001a*\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0002\u001a\u00020\u000b2\u0006\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0001H\u0003\u00f8\u0001\u0000¢\u0006\u0004\b\u0017\u0010\u0018\u001a*\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0002\u001a\u00020\u000e2\u0006\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0001H\u0003\u00f8\u0001\u0000¢\u0006\u0004\b\u0019\u0010\u001a\u001a\u001a\u0010\u001b\u001a\u00020\u00122\u0006\u0010\u0002\u001a\u00020\u0003H\u0001\u00f8\u0001\u0000¢\u0006\u0004\b\u001c\u0010\u001d\u001a\u001a\u0010\u001b\u001a\u00020\u00122\u0006\u0010\u0002\u001a\u00020\bH\u0001\u00f8\u0001\u0000¢\u0006\u0004\b\u001e\u0010\u001f\u001a\u001a\u0010\u001b\u001a\u00020\u00122\u0006\u0010\u0002\u001a\u00020\u000bH\u0001\u00f8\u0001\u0000¢\u0006\u0004\b \u0010!\u001a\u001a\u0010\u001b\u001a\u00020\u00122\u0006\u0010\u0002\u001a\u00020\u000eH\u0001\u00f8\u0001\u0000¢\u0006\u0004\b\"\u0010#\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006$" }, d2 = { "partition", "", "array", "Lkotlin/UByteArray;", "left", "right", "partition-4UcCI2c", "([BII)I", "Lkotlin/UIntArray;", "partition-oBK06Vg", "([III)I", "Lkotlin/ULongArray;", "partition--nroSd4", "([JII)I", "Lkotlin/UShortArray;", "partition-Aa5vz7o", "([SII)I", "quickSort", "", "quickSort-4UcCI2c", "([BII)V", "quickSort-oBK06Vg", "([III)V", "quickSort--nroSd4", "([JII)V", "quickSort-Aa5vz7o", "([SII)V", "sortArray", "sortArray-GBYM_sE", "([B)V", "sortArray--ajY-9A", "([I)V", "sortArray-QwZRm1k", "([J)V", "sortArray-rL5Bavg", "([S)V", "kotlin-stdlib" }, k = 2, mv = { 1, 1, 15 })
public final class UArraySortingKt
{
    private static final int partition--nroSd4(final long[] array, int i, int n) {
        final long get-impl = ULongArray.get-impl(array, (i + n) / 2);
        while (i <= n) {
            int n2 = i;
            int n3;
            while (true) {
                n3 = n;
                if (UnsignedKt.ulongCompare(ULongArray.get-impl(array, n2), get-impl) >= 0) {
                    break;
                }
                ++n2;
            }
            while (UnsignedKt.ulongCompare(ULongArray.get-impl(array, n3), get-impl) > 0) {
                --n3;
            }
            if ((i = n2) <= (n = n3)) {
                final long get-impl2 = ULongArray.get-impl(array, n2);
                ULongArray.set-k8EXiF4(array, n2, ULongArray.get-impl(array, n3));
                ULongArray.set-k8EXiF4(array, n3, get-impl2);
                i = n2 + 1;
                n = n3 - 1;
            }
        }
        return i;
    }
    
    private static final int partition-4UcCI2c(final byte[] array, int i, int n) {
        final byte get-impl = UByteArray.get-impl(array, (i + n) / 2);
        while (i <= n) {
            int n2 = i;
            int n3;
            while (true) {
                final byte get-impl2 = UByteArray.get-impl(array, n2);
                i = (get-impl & 0xFF);
                n3 = n;
                if (Intrinsics.compare(get-impl2 & 0xFF, i) >= 0) {
                    break;
                }
                ++n2;
            }
            while (Intrinsics.compare(UByteArray.get-impl(array, n3) & 0xFF, i) > 0) {
                --n3;
            }
            if ((i = n2) <= (n = n3)) {
                final byte get-impl3 = UByteArray.get-impl(array, n2);
                UByteArray.set-VurrAj0(array, n2, UByteArray.get-impl(array, n3));
                UByteArray.set-VurrAj0(array, n3, get-impl3);
                i = n2 + 1;
                n = n3 - 1;
            }
        }
        return i;
    }
    
    private static final int partition-Aa5vz7o(final short[] array, int i, int n) {
        final short get-impl = UShortArray.get-impl(array, (i + n) / 2);
        while (i <= n) {
            int n2 = i;
            int n3;
            while (true) {
                final short get-impl2 = UShortArray.get-impl(array, n2);
                i = (get-impl & 0xFFFF);
                n3 = n;
                if (Intrinsics.compare(get-impl2 & 0xFFFF, i) >= 0) {
                    break;
                }
                ++n2;
            }
            while (Intrinsics.compare(UShortArray.get-impl(array, n3) & 0xFFFF, i) > 0) {
                --n3;
            }
            if ((i = n2) <= (n = n3)) {
                final short get-impl3 = UShortArray.get-impl(array, n2);
                UShortArray.set-01HTLdE(array, n2, UShortArray.get-impl(array, n3));
                UShortArray.set-01HTLdE(array, n3, get-impl3);
                i = n2 + 1;
                n = n3 - 1;
            }
        }
        return i;
    }
    
    private static final int partition-oBK06Vg(final int[] array, int i, int n) {
        final int get-impl = UIntArray.get-impl(array, (i + n) / 2);
        while (i <= n) {
            int n2 = i;
            int n3;
            while (true) {
                n3 = n;
                if (UnsignedKt.uintCompare(UIntArray.get-impl(array, n2), get-impl) >= 0) {
                    break;
                }
                ++n2;
            }
            while (UnsignedKt.uintCompare(UIntArray.get-impl(array, n3), get-impl) > 0) {
                --n3;
            }
            if ((i = n2) <= (n = n3)) {
                i = UIntArray.get-impl(array, n2);
                UIntArray.set-VXSXFK8(array, n2, UIntArray.get-impl(array, n3));
                UIntArray.set-VXSXFK8(array, n3, i);
                i = n2 + 1;
                n = n3 - 1;
            }
        }
        return i;
    }
    
    private static final void quickSort--nroSd4(final long[] array, final int n, final int n2) {
        final int partition--nroSd4 = partition--nroSd4(array, n, n2);
        final int n3 = partition--nroSd4 - 1;
        if (n < n3) {
            quickSort--nroSd4(array, n, n3);
        }
        if (partition--nroSd4 < n2) {
            quickSort--nroSd4(array, partition--nroSd4, n2);
        }
    }
    
    private static final void quickSort-4UcCI2c(final byte[] array, final int n, final int n2) {
        final int partition-4UcCI2c = partition-4UcCI2c(array, n, n2);
        final int n3 = partition-4UcCI2c - 1;
        if (n < n3) {
            quickSort-4UcCI2c(array, n, n3);
        }
        if (partition-4UcCI2c < n2) {
            quickSort-4UcCI2c(array, partition-4UcCI2c, n2);
        }
    }
    
    private static final void quickSort-Aa5vz7o(final short[] array, final int n, final int n2) {
        final int partition-Aa5vz7o = partition-Aa5vz7o(array, n, n2);
        final int n3 = partition-Aa5vz7o - 1;
        if (n < n3) {
            quickSort-Aa5vz7o(array, n, n3);
        }
        if (partition-Aa5vz7o < n2) {
            quickSort-Aa5vz7o(array, partition-Aa5vz7o, n2);
        }
    }
    
    private static final void quickSort-oBK06Vg(final int[] array, final int n, final int n2) {
        final int partition-oBK06Vg = partition-oBK06Vg(array, n, n2);
        final int n3 = partition-oBK06Vg - 1;
        if (n < n3) {
            quickSort-oBK06Vg(array, n, n3);
        }
        if (partition-oBK06Vg < n2) {
            quickSort-oBK06Vg(array, partition-oBK06Vg, n2);
        }
    }
    
    public static final void sortArray--ajY-9A(final int[] array) {
        Intrinsics.checkParameterIsNotNull(array, "array");
        quickSort-oBK06Vg(array, 0, UIntArray.getSize-impl(array) - 1);
    }
    
    public static final void sortArray-GBYM_sE(final byte[] array) {
        Intrinsics.checkParameterIsNotNull(array, "array");
        quickSort-4UcCI2c(array, 0, UByteArray.getSize-impl(array) - 1);
    }
    
    public static final void sortArray-QwZRm1k(final long[] array) {
        Intrinsics.checkParameterIsNotNull(array, "array");
        quickSort--nroSd4(array, 0, ULongArray.getSize-impl(array) - 1);
    }
    
    public static final void sortArray-rL5Bavg(final short[] array) {
        Intrinsics.checkParameterIsNotNull(array, "array");
        quickSort-Aa5vz7o(array, 0, UShortArray.getSize-impl(array) - 1);
    }
}
