// 
// Decompiled by Procyon v0.5.36
// 

package kotlin;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0012\n\u0000\n\u0002\u0010\b\n\u0002\u0010\u0005\n\u0002\u0010\n\n\u0002\b\b\u001a\r\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0087\b\u001a\r\u0010\u0000\u001a\u00020\u0001*\u00020\u0003H\u0087\b\u001a\r\u0010\u0004\u001a\u00020\u0001*\u00020\u0002H\u0087\b\u001a\r\u0010\u0004\u001a\u00020\u0001*\u00020\u0003H\u0087\b\u001a\r\u0010\u0005\u001a\u00020\u0001*\u00020\u0002H\u0087\b\u001a\r\u0010\u0005\u001a\u00020\u0001*\u00020\u0003H\u0087\b\u001a\u0014\u0010\u0006\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0001H\u0007\u001a\u0014\u0010\u0006\u001a\u00020\u0003*\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0001H\u0007\u001a\u0014\u0010\b\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0001H\u0007\u001a\u0014\u0010\b\u001a\u00020\u0003*\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0001H\u0007\u001a\r\u0010\t\u001a\u00020\u0002*\u00020\u0002H\u0087\b\u001a\r\u0010\t\u001a\u00020\u0003*\u00020\u0003H\u0087\b\u001a\r\u0010\n\u001a\u00020\u0002*\u00020\u0002H\u0087\b\u001a\r\u0010\n\u001a\u00020\u0003*\u00020\u0003H\u0087\b¨\u0006\u000b" }, d2 = { "countLeadingZeroBits", "", "", "", "countOneBits", "countTrailingZeroBits", "rotateLeft", "bitCount", "rotateRight", "takeHighestOneBit", "takeLowestOneBit", "kotlin-stdlib" }, k = 5, mv = { 1, 1, 15 }, xi = 1, xs = "kotlin/NumbersKt")
class NumbersKt__NumbersKt extends NumbersKt__NumbersJVMKt
{
    public NumbersKt__NumbersKt() {
    }
    
    private static final int countLeadingZeroBits(final byte b) {
        return Integer.numberOfLeadingZeros(b & 0xFF) - 24;
    }
    
    private static final int countLeadingZeroBits(final short n) {
        return Integer.numberOfLeadingZeros(n & 0xFFFF) - 16;
    }
    
    private static final int countOneBits(final byte b) {
        return Integer.bitCount(b & 0xFF);
    }
    
    private static final int countOneBits(final short n) {
        return Integer.bitCount(n & 0xFFFF);
    }
    
    private static final int countTrailingZeroBits(final byte b) {
        return Integer.numberOfTrailingZeros(b | 0x100);
    }
    
    private static final int countTrailingZeroBits(final short n) {
        return Integer.numberOfTrailingZeros(n | 0x10000);
    }
    
    public static final byte rotateLeft(final byte b, int n) {
        n &= 0x7;
        return (byte)((b & 0xFF) >>> 8 - n | b << n);
    }
    
    public static final short rotateLeft(final short n, int n2) {
        n2 &= 0xF;
        return (short)((n & 0xFFFF) >>> 16 - n2 | n << n2);
    }
    
    public static final byte rotateRight(final byte b, int n) {
        n &= 0x7;
        return (byte)((b & 0xFF) >>> n | b << 8 - n);
    }
    
    public static final short rotateRight(final short n, int n2) {
        n2 &= 0xF;
        return (short)((n & 0xFFFF) >>> n2 | n << 16 - n2);
    }
    
    private static final byte takeHighestOneBit(final byte b) {
        return (byte)Integer.highestOneBit(b & 0xFF);
    }
    
    private static final short takeHighestOneBit(final short n) {
        return (short)Integer.highestOneBit(n & 0xFFFF);
    }
    
    private static final byte takeLowestOneBit(final byte i) {
        return (byte)Integer.lowestOneBit(i);
    }
    
    private static final short takeLowestOneBit(final short i) {
        return (short)Integer.lowestOneBit(i);
    }
}
