// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.coroutines.jvm.internal;

import kotlin.coroutines.ContinuationInterceptor;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\b!\u0018\u00002\u00020\u0001B\u0019\b\u0016\u0012\u0010\u0010\u0002\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0005B!\u0012\u0010\u0010\u0002\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u0003\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\u0002\u0010\bJ\u000e\u0010\f\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0003J\b\u0010\r\u001a\u00020\u000eH\u0014R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\u00078VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u0018\u0010\f\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0018\u00010\u0003X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u000f" }, d2 = { "Lkotlin/coroutines/jvm/internal/ContinuationImpl;", "Lkotlin/coroutines/jvm/internal/BaseContinuationImpl;", "completion", "Lkotlin/coroutines/Continuation;", "", "(Lkotlin/coroutines/Continuation;)V", "_context", "Lkotlin/coroutines/CoroutineContext;", "(Lkotlin/coroutines/Continuation;Lkotlin/coroutines/CoroutineContext;)V", "context", "getContext", "()Lkotlin/coroutines/CoroutineContext;", "intercepted", "releaseIntercepted", "", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
public abstract class ContinuationImpl extends BaseContinuationImpl
{
    private final CoroutineContext _context;
    private transient Continuation<Object> intercepted;
    
    public ContinuationImpl(final Continuation<Object> continuation) {
        CoroutineContext context;
        if (continuation != null) {
            context = continuation.getContext();
        }
        else {
            context = null;
        }
        this(continuation, context);
    }
    
    public ContinuationImpl(final Continuation<Object> continuation, final CoroutineContext context) {
        super(continuation);
        this._context = context;
    }
    
    @Override
    public CoroutineContext getContext() {
        final CoroutineContext context = this._context;
        if (context == null) {
            Intrinsics.throwNpe();
        }
        return context;
    }
    
    public final Continuation<Object> intercepted() {
        final Continuation<Object> intercepted = this.intercepted;
        if (intercepted != null) {
            return intercepted;
        }
        final ContinuationInterceptor continuationInterceptor = this.getContext().get((CoroutineContext.Key<ContinuationInterceptor>)ContinuationInterceptor.Key);
        if (continuationInterceptor != null) {
            final Continuation<Object> interceptContinuation = continuationInterceptor.interceptContinuation(this);
            if (interceptContinuation != null) {
                return this.intercepted = interceptContinuation;
            }
        }
        final Continuation<Object> interceptContinuation = this;
        return this.intercepted = interceptContinuation;
    }
    
    @Override
    protected void releaseIntercepted() {
        final Continuation<Object> intercepted = this.intercepted;
        if (intercepted != null && intercepted != this) {
            final ContinuationInterceptor value = this.getContext().get((CoroutineContext.Key<ContinuationInterceptor>)ContinuationInterceptor.Key);
            if (value == null) {
                Intrinsics.throwNpe();
            }
            value.releaseInterceptedContinuation(intercepted);
        }
        this.intercepted = CompletedContinuation.INSTANCE;
    }
}
