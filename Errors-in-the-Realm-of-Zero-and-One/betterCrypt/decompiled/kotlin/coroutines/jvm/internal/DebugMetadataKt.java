// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.coroutines.jvm.internal;

import kotlin.TypeCastException;
import java.util.ArrayList;
import java.lang.reflect.Field;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u00000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0001H\u0002\u001a\u000e\u0010\u0006\u001a\u0004\u0018\u00010\u0007*\u00020\bH\u0002\u001a\f\u0010\t\u001a\u00020\u0001*\u00020\bH\u0002\u001a\u0019\u0010\n\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u000b*\u00020\bH\u0001¢\u0006\u0002\u0010\r\u001a\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u000f*\u00020\bH\u0001¢\u0006\u0002\b\u0010\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000¨\u0006\u0011" }, d2 = { "COROUTINES_DEBUG_METADATA_VERSION", "", "checkDebugMetadataVersion", "", "expected", "actual", "getDebugMetadataAnnotation", "Lkotlin/coroutines/jvm/internal/DebugMetadata;", "Lkotlin/coroutines/jvm/internal/BaseContinuationImpl;", "getLabel", "getSpilledVariableFieldMapping", "", "", "(Lkotlin/coroutines/jvm/internal/BaseContinuationImpl;)[Ljava/lang/String;", "getStackTraceElementImpl", "Ljava/lang/StackTraceElement;", "getStackTraceElement", "kotlin-stdlib" }, k = 2, mv = { 1, 1, 15 })
public final class DebugMetadataKt
{
    private static final int COROUTINES_DEBUG_METADATA_VERSION = 1;
    
    private static final void checkDebugMetadataVersion(final int i, final int j) {
        if (j <= i) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Debug metadata version mismatch. Expected: ");
        sb.append(i);
        sb.append(", got ");
        sb.append(j);
        sb.append(". Please update the Kotlin standard library.");
        throw new IllegalStateException(sb.toString().toString());
    }
    
    private static final DebugMetadata getDebugMetadataAnnotation(final BaseContinuationImpl baseContinuationImpl) {
        return baseContinuationImpl.getClass().getAnnotation(DebugMetadata.class);
    }
    
    private static final int getLabel(final BaseContinuationImpl obj) {
        try {
            final Field declaredField = obj.getClass().getDeclaredField("label");
            Intrinsics.checkExpressionValueIsNotNull(declaredField, "field");
            declaredField.setAccessible(true);
            Object value;
            if (!((value = declaredField.get(obj)) instanceof Integer)) {
                value = null;
            }
            final Integer n = (Integer)value;
            int intValue;
            if (n != null) {
                intValue = n;
            }
            else {
                intValue = 0;
            }
            return intValue - 1;
        }
        catch (Exception ex) {
            return -1;
        }
    }
    
    public static final String[] getSpilledVariableFieldMapping(final BaseContinuationImpl baseContinuationImpl) {
        Intrinsics.checkParameterIsNotNull(baseContinuationImpl, "$this$getSpilledVariableFieldMapping");
        final DebugMetadata debugMetadataAnnotation = getDebugMetadataAnnotation(baseContinuationImpl);
        if (debugMetadataAnnotation == null) {
            return null;
        }
        checkDebugMetadataVersion(1, debugMetadataAnnotation.v());
        final ArrayList<String> list = new ArrayList<String>();
        final int label = getLabel(baseContinuationImpl);
        final int[] i = debugMetadataAnnotation.i();
        for (int length = i.length, j = 0; j < length; ++j) {
            if (i[j] == label) {
                list.add(debugMetadataAnnotation.s()[j]);
                list.add(debugMetadataAnnotation.n()[j]);
            }
        }
        final String[] array = list.toArray(new String[0]);
        if (array != null) {
            return array;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
    }
    
    public static final StackTraceElement getStackTraceElement(final BaseContinuationImpl baseContinuationImpl) {
        Intrinsics.checkParameterIsNotNull(baseContinuationImpl, "$this$getStackTraceElementImpl");
        final DebugMetadata debugMetadataAnnotation = getDebugMetadataAnnotation(baseContinuationImpl);
        if (debugMetadataAnnotation != null) {
            checkDebugMetadataVersion(1, debugMetadataAnnotation.v());
            final int label = getLabel(baseContinuationImpl);
            int lineNumber;
            if (label < 0) {
                lineNumber = -1;
            }
            else {
                lineNumber = debugMetadataAnnotation.l()[label];
            }
            final String moduleName = ModuleNameRetriever.INSTANCE.getModuleName(baseContinuationImpl);
            String declaringClass;
            if (moduleName == null) {
                declaringClass = debugMetadataAnnotation.c();
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append(moduleName);
                sb.append('/');
                sb.append(debugMetadataAnnotation.c());
                declaringClass = sb.toString();
            }
            return new StackTraceElement(declaringClass, debugMetadataAnnotation.m(), debugMetadataAnnotation.f(), lineNumber);
        }
        return null;
    }
}
