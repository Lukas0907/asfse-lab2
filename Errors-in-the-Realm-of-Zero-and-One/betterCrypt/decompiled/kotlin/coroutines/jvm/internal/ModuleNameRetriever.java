// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.coroutines.jvm.internal;

import kotlin.jvm.internal.Intrinsics;
import java.lang.reflect.Method;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u00c2\u0002\u0018\u00002\u00020\u0001:\u0001\u000bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0010\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u0007\u001a\u00020\bR\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\f" }, d2 = { "Lkotlin/coroutines/jvm/internal/ModuleNameRetriever;", "", "()V", "cache", "Lkotlin/coroutines/jvm/internal/ModuleNameRetriever$Cache;", "notOnJava9", "buildCache", "continuation", "Lkotlin/coroutines/jvm/internal/BaseContinuationImpl;", "getModuleName", "", "Cache", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
final class ModuleNameRetriever
{
    public static final ModuleNameRetriever INSTANCE;
    public static Cache cache;
    private static final Cache notOnJava9;
    
    static {
        INSTANCE = new ModuleNameRetriever();
        notOnJava9 = new Cache(null, null, null);
    }
    
    private ModuleNameRetriever() {
    }
    
    private final Cache buildCache(final BaseContinuationImpl baseContinuationImpl) {
        try {
            return ModuleNameRetriever.cache = new Cache(Class.class.getDeclaredMethod("getModule", (Class<?>[])new Class[0]), baseContinuationImpl.getClass().getClassLoader().loadClass("java.lang.Module").getDeclaredMethod("getDescriptor", (Class<?>[])new Class[0]), baseContinuationImpl.getClass().getClassLoader().loadClass("java.lang.module.ModuleDescriptor").getDeclaredMethod("name", (Class<?>[])new Class[0]));
        }
        catch (Exception ex) {
            return ModuleNameRetriever.cache = ModuleNameRetriever.notOnJava9;
        }
    }
    
    public final String getModuleName(final BaseContinuationImpl baseContinuationImpl) {
        Intrinsics.checkParameterIsNotNull(baseContinuationImpl, "continuation");
        Cache cache = ModuleNameRetriever.cache;
        if (cache == null) {
            cache = this.buildCache(baseContinuationImpl);
        }
        if (cache == ModuleNameRetriever.notOnJava9) {
            return null;
        }
        final Method getModuleMethod = cache.getModuleMethod;
        if (getModuleMethod != null) {
            final Object invoke = getModuleMethod.invoke(baseContinuationImpl.getClass(), new Object[0]);
            if (invoke != null) {
                final Method getDescriptorMethod = cache.getDescriptorMethod;
                if (getDescriptorMethod != null) {
                    final Object invoke2 = getDescriptorMethod.invoke(invoke, new Object[0]);
                    if (invoke2 != null) {
                        final Method nameMethod = cache.nameMethod;
                        Object invoke3;
                        if (nameMethod != null) {
                            invoke3 = nameMethod.invoke(invoke2, new Object[0]);
                        }
                        else {
                            invoke3 = null;
                        }
                        Object o = invoke3;
                        if (!(invoke3 instanceof String)) {
                            o = null;
                        }
                        return (String)o;
                    }
                }
            }
        }
        return null;
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0002\u0018\u00002\u00020\u0001B#\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0006R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00038\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0007" }, d2 = { "Lkotlin/coroutines/jvm/internal/ModuleNameRetriever$Cache;", "", "getModuleMethod", "Ljava/lang/reflect/Method;", "getDescriptorMethod", "nameMethod", "(Ljava/lang/reflect/Method;Ljava/lang/reflect/Method;Ljava/lang/reflect/Method;)V", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
    private static final class Cache
    {
        public final Method getDescriptorMethod;
        public final Method getModuleMethod;
        public final Method nameMethod;
        
        public Cache(final Method getModuleMethod, final Method getDescriptorMethod, final Method nameMethod) {
            this.getModuleMethod = getModuleMethod;
            this.getDescriptorMethod = getDescriptorMethod;
            this.nameMethod = nameMethod;
        }
    }
}
