// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.coroutines.experimental.migration;

import kotlin.jvm.functions.Function3;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function1;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlin.coroutines.experimental.CoroutineContext;
import kotlin.coroutines.experimental.ContinuationInterceptor;
import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.experimental.Continuation;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000:\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u001e\u0010\u0000\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003H\u0007\u001a\f\u0010\u0004\u001a\u00020\u0005*\u00020\u0006H\u0007\u001a\f\u0010\u0007\u001a\u00020\b*\u00020\tH\u0007\u001a\u001e\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\u00020\u0003\"\u0004\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0001H\u0007\u001a\f\u0010\u000b\u001a\u00020\u0006*\u00020\u0005H\u0007\u001a\f\u0010\f\u001a\u00020\t*\u00020\bH\u0007\u001a^\u0010\r\u001a\"\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u0002H\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00110\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u00120\u000e\"\u0004\b\u0000\u0010\u000f\"\u0004\b\u0001\u0010\u0010\"\u0004\b\u0002\u0010\u0011*\"\u0012\u0004\u0012\u0002H\u000f\u0012\u0004\u0012\u0002H\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00110\u0001\u0012\u0006\u0012\u0004\u0018\u00010\u00120\u000eH\u0000\u001aL\u0010\r\u001a\u001c\u0012\u0004\u0012\u0002H\u000f\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00110\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u00120\u0013\"\u0004\b\u0000\u0010\u000f\"\u0004\b\u0001\u0010\u0011*\u001c\u0012\u0004\u0012\u0002H\u000f\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00110\u0001\u0012\u0006\u0012\u0004\u0018\u00010\u00120\u0013H\u0000\u001a:\u0010\r\u001a\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00110\u0003\u0012\u0006\u0012\u0004\u0018\u00010\u00120\u0014\"\u0004\b\u0000\u0010\u0011*\u0016\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00110\u0001\u0012\u0006\u0012\u0004\u0018\u00010\u00120\u0014H\u0000¨\u0006\u0015" }, d2 = { "toContinuation", "Lkotlin/coroutines/Continuation;", "T", "Lkotlin/coroutines/experimental/Continuation;", "toContinuationInterceptor", "Lkotlin/coroutines/ContinuationInterceptor;", "Lkotlin/coroutines/experimental/ContinuationInterceptor;", "toCoroutineContext", "Lkotlin/coroutines/CoroutineContext;", "Lkotlin/coroutines/experimental/CoroutineContext;", "toExperimentalContinuation", "toExperimentalContinuationInterceptor", "toExperimentalCoroutineContext", "toExperimentalSuspendFunction", "Lkotlin/Function3;", "T1", "T2", "R", "", "Lkotlin/Function2;", "Lkotlin/Function1;", "kotlin-stdlib-coroutines" }, k = 2, mv = { 1, 1, 15 })
public final class CoroutinesMigrationKt
{
    public static final <T> kotlin.coroutines.Continuation<T> toContinuation(final Continuation<? super T> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "$this$toContinuation");
        ExperimentalContinuationMigration<T> experimentalContinuationMigration;
        if (!(continuation instanceof ExperimentalContinuationMigration)) {
            experimentalContinuationMigration = null;
        }
        else {
            experimentalContinuationMigration = (ExperimentalContinuationMigration<T>)continuation;
        }
        final ExperimentalContinuationMigration<T> experimentalContinuationMigration2 = experimentalContinuationMigration;
        if (experimentalContinuationMigration2 != null) {
            final kotlin.coroutines.Continuation<T> continuation2 = experimentalContinuationMigration2.getContinuation();
            if (continuation2 != null) {
                return continuation2;
            }
        }
        return new ContinuationMigration<T>(continuation);
    }
    
    public static final kotlin.coroutines.ContinuationInterceptor toContinuationInterceptor(final ContinuationInterceptor continuationInterceptor) {
        Intrinsics.checkParameterIsNotNull(continuationInterceptor, "$this$toContinuationInterceptor");
        ContinuationInterceptor continuationInterceptor2;
        if (!(continuationInterceptor instanceof ExperimentalContinuationInterceptorMigration)) {
            continuationInterceptor2 = null;
        }
        else {
            continuationInterceptor2 = continuationInterceptor;
        }
        final ExperimentalContinuationInterceptorMigration experimentalContinuationInterceptorMigration = (ExperimentalContinuationInterceptorMigration)continuationInterceptor2;
        if (experimentalContinuationInterceptorMigration != null) {
            final kotlin.coroutines.ContinuationInterceptor interceptor = experimentalContinuationInterceptorMigration.getInterceptor();
            if (interceptor != null) {
                return interceptor;
            }
        }
        return new ContinuationInterceptorMigration(continuationInterceptor);
    }
    
    public static final kotlin.coroutines.CoroutineContext toCoroutineContext(final CoroutineContext coroutineContext) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "$this$toCoroutineContext");
        final ContinuationInterceptor continuationInterceptor = coroutineContext.get((CoroutineContext.Key<ContinuationInterceptor>)ContinuationInterceptor.Key);
        final ExperimentalContextMigration experimentalContextMigration = coroutineContext.get((CoroutineContext.Key<ExperimentalContextMigration>)ExperimentalContextMigration.Key);
        final CoroutineContext minusKey = coroutineContext.minusKey((CoroutineContext.Key<?>)ContinuationInterceptor.Key).minusKey((CoroutineContext.Key<?>)ExperimentalContextMigration.Key);
        kotlin.coroutines.CoroutineContext coroutineContext2 = null;
        Label_0085: {
            if (experimentalContextMigration != null) {
                coroutineContext2 = experimentalContextMigration.getContext();
                if (coroutineContext2 != null) {
                    break Label_0085;
                }
            }
            coroutineContext2 = EmptyCoroutineContext.INSTANCE;
        }
        if (minusKey != kotlin.coroutines.experimental.EmptyCoroutineContext.INSTANCE) {
            coroutineContext2 = coroutineContext2.plus(new ContextMigration(minusKey));
        }
        if (continuationInterceptor == null) {
            return coroutineContext2;
        }
        return coroutineContext2.plus(toContinuationInterceptor(continuationInterceptor));
    }
    
    public static final <T> Continuation<T> toExperimentalContinuation(final kotlin.coroutines.Continuation<? super T> continuation) {
        Intrinsics.checkParameterIsNotNull(continuation, "$this$toExperimentalContinuation");
        ContinuationMigration<T> continuationMigration;
        if (!(continuation instanceof ContinuationMigration)) {
            continuationMigration = null;
        }
        else {
            continuationMigration = (ContinuationMigration<T>)continuation;
        }
        final ContinuationMigration<T> continuationMigration2 = continuationMigration;
        if (continuationMigration2 != null) {
            final Continuation<T> continuation2 = continuationMigration2.getContinuation();
            if (continuation2 != null) {
                return continuation2;
            }
        }
        return new ExperimentalContinuationMigration<T>(continuation);
    }
    
    public static final ContinuationInterceptor toExperimentalContinuationInterceptor(final kotlin.coroutines.ContinuationInterceptor continuationInterceptor) {
        Intrinsics.checkParameterIsNotNull(continuationInterceptor, "$this$toExperimentalContinuationInterceptor");
        kotlin.coroutines.ContinuationInterceptor continuationInterceptor2;
        if (!(continuationInterceptor instanceof ContinuationInterceptorMigration)) {
            continuationInterceptor2 = null;
        }
        else {
            continuationInterceptor2 = continuationInterceptor;
        }
        final ContinuationInterceptorMigration continuationInterceptorMigration = (ContinuationInterceptorMigration)continuationInterceptor2;
        if (continuationInterceptorMigration != null) {
            final ContinuationInterceptor interceptor = continuationInterceptorMigration.getInterceptor();
            if (interceptor != null) {
                return interceptor;
            }
        }
        return new ExperimentalContinuationInterceptorMigration(continuationInterceptor);
    }
    
    public static final CoroutineContext toExperimentalCoroutineContext(final kotlin.coroutines.CoroutineContext coroutineContext) {
        Intrinsics.checkParameterIsNotNull(coroutineContext, "$this$toExperimentalCoroutineContext");
        final kotlin.coroutines.ContinuationInterceptor continuationInterceptor = coroutineContext.get((kotlin.coroutines.CoroutineContext.Key<kotlin.coroutines.ContinuationInterceptor>)kotlin.coroutines.ContinuationInterceptor.Key);
        final ContextMigration contextMigration = coroutineContext.get((kotlin.coroutines.CoroutineContext.Key<ContextMigration>)ContextMigration.Key);
        final kotlin.coroutines.CoroutineContext minusKey = coroutineContext.minusKey((kotlin.coroutines.CoroutineContext.Key<?>)kotlin.coroutines.ContinuationInterceptor.Key).minusKey((kotlin.coroutines.CoroutineContext.Key<?>)ContextMigration.Key);
        CoroutineContext coroutineContext2 = null;
        Label_0085: {
            if (contextMigration != null) {
                coroutineContext2 = contextMigration.getContext();
                if (coroutineContext2 != null) {
                    break Label_0085;
                }
            }
            coroutineContext2 = kotlin.coroutines.experimental.EmptyCoroutineContext.INSTANCE;
        }
        if (minusKey != EmptyCoroutineContext.INSTANCE) {
            coroutineContext2 = coroutineContext2.plus(new ExperimentalContextMigration(minusKey));
        }
        if (continuationInterceptor == null) {
            return coroutineContext2;
        }
        return coroutineContext2.plus(toExperimentalContinuationInterceptor(continuationInterceptor));
    }
    
    public static final <R> Function1<Continuation<? super R>, Object> toExperimentalSuspendFunction(final Function1<? super kotlin.coroutines.Continuation<? super R>, ?> function1) {
        Intrinsics.checkParameterIsNotNull(function1, "$this$toExperimentalSuspendFunction");
        return (Function1<Continuation<? super R>, Object>)new ExperimentalSuspendFunction0Migration<Object>((Function1<? super kotlin.coroutines.Continuation<?>, ?>)function1);
    }
    
    public static final <T1, R> Function2<T1, Continuation<? super R>, Object> toExperimentalSuspendFunction(final Function2<? super T1, ? super kotlin.coroutines.Continuation<? super R>, ?> function2) {
        Intrinsics.checkParameterIsNotNull(function2, "$this$toExperimentalSuspendFunction");
        return (Function2<T1, Continuation<? super R>, Object>)new ExperimentalSuspendFunction1Migration<T1, Object>((Function2<? super T1, ? super kotlin.coroutines.Continuation<?>, ?>)function2);
    }
    
    public static final <T1, T2, R> Function3<T1, T2, Continuation<? super R>, Object> toExperimentalSuspendFunction(final Function3<? super T1, ? super T2, ? super kotlin.coroutines.Continuation<? super R>, ?> function3) {
        Intrinsics.checkParameterIsNotNull(function3, "$this$toExperimentalSuspendFunction");
        return (Function3<T1, T2, Continuation<? super R>, Object>)new ExperimentalSuspendFunction2Migration<T1, T2, Object>((Function3<? super T1, ? super T2, ? super kotlin.coroutines.Continuation<?>, ?>)function3);
    }
}
