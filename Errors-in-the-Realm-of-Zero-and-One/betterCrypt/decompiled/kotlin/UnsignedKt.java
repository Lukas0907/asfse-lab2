// 
// Decompiled by Procyon v0.5.36
// 

package kotlin;

import kotlin.jvm.internal.Intrinsics;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u00000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0006\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\t\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\u001a\u0018\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0001\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0004\u001a\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0002\u001a\u00020\u0003H\u0001\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0007\u001a\u0018\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\tH\u0001\u001a\"\u0010\f\u001a\u00020\u00012\u0006\u0010\n\u001a\u00020\u00012\u0006\u0010\u000b\u001a\u00020\u0001H\u0001\u00f8\u0001\u0000¢\u0006\u0004\b\r\u0010\u000e\u001a\"\u0010\u000f\u001a\u00020\u00012\u0006\u0010\n\u001a\u00020\u00012\u0006\u0010\u000b\u001a\u00020\u0001H\u0001\u00f8\u0001\u0000¢\u0006\u0004\b\u0010\u0010\u000e\u001a\u0010\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0002\u001a\u00020\tH\u0001\u001a\u0018\u0010\u0012\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u00132\u0006\u0010\u000b\u001a\u00020\u0013H\u0001\u001a\"\u0010\u0014\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u0006H\u0001\u00f8\u0001\u0000¢\u0006\u0004\b\u0015\u0010\u0016\u001a\"\u0010\u0017\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u0006H\u0001\u00f8\u0001\u0000¢\u0006\u0004\b\u0018\u0010\u0016\u001a\u0010\u0010\u0019\u001a\u00020\u00032\u0006\u0010\u0002\u001a\u00020\u0013H\u0001\u001a\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u0002\u001a\u00020\u0013H\u0000\u001a\u0018\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u0002\u001a\u00020\u00132\u0006\u0010\u001c\u001a\u00020\tH\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u001d" }, d2 = { "doubleToUInt", "Lkotlin/UInt;", "v", "", "(D)I", "doubleToULong", "Lkotlin/ULong;", "(D)J", "uintCompare", "", "v1", "v2", "uintDivide", "uintDivide-J1ME1BU", "(II)I", "uintRemainder", "uintRemainder-J1ME1BU", "uintToDouble", "ulongCompare", "", "ulongDivide", "ulongDivide-eb3DHEI", "(JJ)J", "ulongRemainder", "ulongRemainder-eb3DHEI", "ulongToDouble", "ulongToString", "", "base", "kotlin-stdlib" }, k = 2, mv = { 1, 1, 15 })
public final class UnsignedKt
{
    public static final int doubleToUInt(final double v) {
        if (Double.isNaN(v) || v <= uintToDouble(0)) {
            return 0;
        }
        if (v >= uintToDouble(-1)) {
            return -1;
        }
        final double n = Integer.MAX_VALUE;
        if (v <= n) {
            return UInt.constructor-impl((int)v);
        }
        return UInt.constructor-impl(UInt.constructor-impl((int)(v - n)) + UInt.constructor-impl(Integer.MAX_VALUE));
    }
    
    public static final long doubleToULong(final double v) {
        if (Double.isNaN(v) || v <= ulongToDouble(0L)) {
            return 0L;
        }
        if (v >= ulongToDouble(-1L)) {
            return -1L;
        }
        if (v < Long.MAX_VALUE) {
            return ULong.constructor-impl((long)v);
        }
        return ULong.constructor-impl(ULong.constructor-impl((long)(v - 9.223372036854776E18)) - Long.MIN_VALUE);
    }
    
    public static final int uintCompare(final int n, final int n2) {
        return Intrinsics.compare(n ^ Integer.MIN_VALUE, n2 ^ Integer.MIN_VALUE);
    }
    
    public static final int uintDivide-J1ME1BU(final int n, final int n2) {
        return UInt.constructor-impl((int)(((long)n & 0xFFFFFFFFL) / ((long)n2 & 0xFFFFFFFFL)));
    }
    
    public static final int uintRemainder-J1ME1BU(final int n, final int n2) {
        return UInt.constructor-impl((int)(((long)n & 0xFFFFFFFFL) % ((long)n2 & 0xFFFFFFFFL)));
    }
    
    public static final double uintToDouble(final int n) {
        return (Integer.MAX_VALUE & n) + (n >>> 31 << 30) * (double)2;
    }
    
    public static final int ulongCompare(final long n, final long n2) {
        return lcmp(n ^ Long.MIN_VALUE, n2 ^ Long.MIN_VALUE);
    }
    
    public static final long ulongDivide-eb3DHEI(final long n, final long n2) {
        if (n2 < 0L) {
            if (ulongCompare(n, n2) < 0) {
                return ULong.constructor-impl(0L);
            }
            return ULong.constructor-impl(1L);
        }
        else {
            if (n >= 0L) {
                return ULong.constructor-impl(n / n2);
            }
            boolean b = true;
            final long n3 = (n >>> 1) / n2 << 1;
            if (ulongCompare(ULong.constructor-impl(n - n3 * n2), ULong.constructor-impl(n2)) < 0) {
                b = false;
            }
            return ULong.constructor-impl(n3 + (long)(b ? 1 : 0));
        }
    }
    
    public static final long ulongRemainder-eb3DHEI(long n, long n2) {
        if (n2 < 0L) {
            if (ulongCompare(n, n2) < 0) {
                return n;
            }
            return ULong.constructor-impl(n - n2);
        }
        else {
            if (n >= 0L) {
                return ULong.constructor-impl(n % n2);
            }
            n -= ((n >>> 1) / n2 << 1) * n2;
            if (ulongCompare(ULong.constructor-impl(n), ULong.constructor-impl(n2)) < 0) {
                n2 = 0L;
            }
            return ULong.constructor-impl(n - n2);
        }
    }
    
    public static final double ulongToDouble(final long n) {
        return (n >>> 11) * (double)2048 + (n & 0x7FFL);
    }
    
    public static final String ulongToString(final long n) {
        return ulongToString(n, 10);
    }
    
    public static final String ulongToString(long n, final int n2) {
        if (n >= 0L) {
            final String string = Long.toString(n, CharsKt__CharJVMKt.checkRadix(n2));
            Intrinsics.checkExpressionValueIsNotNull(string, "java.lang.Long.toString(this, checkRadix(radix))");
            return string;
        }
        final long n3 = n2;
        final long n4 = (n >>> 1) / n3 << 1;
        final long n5 = n - n4 * n3;
        long i = n4;
        n = n5;
        if (n5 >= n3) {
            n = n5 - n3;
            i = n4 + 1L;
        }
        final StringBuilder sb = new StringBuilder();
        final String string2 = Long.toString(i, CharsKt__CharJVMKt.checkRadix(n2));
        Intrinsics.checkExpressionValueIsNotNull(string2, "java.lang.Long.toString(this, checkRadix(radix))");
        sb.append(string2);
        final String string3 = Long.toString(n, CharsKt__CharJVMKt.checkRadix(n2));
        Intrinsics.checkExpressionValueIsNotNull(string3, "java.lang.Long.toString(this, checkRadix(radix))");
        sb.append(string3);
        return sb.toString();
    }
}
