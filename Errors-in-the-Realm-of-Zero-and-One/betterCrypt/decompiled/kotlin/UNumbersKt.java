// 
// Decompiled by Procyon v0.5.36
// 

package kotlin;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000&\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b)\u001a\u0017\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\u0003\u0010\u0004\u001a\u0017\u0010\u0000\u001a\u00020\u0001*\u00020\u0005H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\u0006\u0010\u0007\u001a\u0017\u0010\u0000\u001a\u00020\u0001*\u00020\bH\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\t\u0010\n\u001a\u0017\u0010\u0000\u001a\u00020\u0001*\u00020\u000bH\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\f\u0010\r\u001a\u0017\u0010\u000e\u001a\u00020\u0001*\u00020\u0002H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\u000f\u0010\u0004\u001a\u0017\u0010\u000e\u001a\u00020\u0001*\u00020\u0005H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\u0010\u0010\u0007\u001a\u0017\u0010\u000e\u001a\u00020\u0001*\u00020\bH\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\u0011\u0010\n\u001a\u0017\u0010\u000e\u001a\u00020\u0001*\u00020\u000bH\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\u0012\u0010\r\u001a\u0017\u0010\u0013\u001a\u00020\u0001*\u00020\u0002H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\u0014\u0010\u0004\u001a\u0017\u0010\u0013\u001a\u00020\u0001*\u00020\u0005H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\u0015\u0010\u0007\u001a\u0017\u0010\u0013\u001a\u00020\u0001*\u00020\bH\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\u0016\u0010\n\u001a\u0017\u0010\u0013\u001a\u00020\u0001*\u00020\u000bH\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\u0017\u0010\r\u001a\u001f\u0010\u0018\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u0001H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\u001a\u0010\u001b\u001a\u001f\u0010\u0018\u001a\u00020\u0005*\u00020\u00052\u0006\u0010\u0019\u001a\u00020\u0001H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\u001c\u0010\u001d\u001a\u001f\u0010\u0018\u001a\u00020\b*\u00020\b2\u0006\u0010\u0019\u001a\u00020\u0001H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\u001e\u0010\u001f\u001a\u001f\u0010\u0018\u001a\u00020\u000b*\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u0001H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b \u0010!\u001a\u001f\u0010\"\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u0001H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b#\u0010\u001b\u001a\u001f\u0010\"\u001a\u00020\u0005*\u00020\u00052\u0006\u0010\u0019\u001a\u00020\u0001H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b$\u0010\u001d\u001a\u001f\u0010\"\u001a\u00020\b*\u00020\b2\u0006\u0010\u0019\u001a\u00020\u0001H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b%\u0010\u001f\u001a\u001f\u0010\"\u001a\u00020\u000b*\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u0001H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b&\u0010!\u001a\u0017\u0010'\u001a\u00020\u0002*\u00020\u0002H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b(\u0010)\u001a\u0017\u0010'\u001a\u00020\u0005*\u00020\u0005H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b*\u0010\u0007\u001a\u0017\u0010'\u001a\u00020\b*\u00020\bH\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b+\u0010,\u001a\u0017\u0010'\u001a\u00020\u000b*\u00020\u000bH\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b-\u0010.\u001a\u0017\u0010/\u001a\u00020\u0002*\u00020\u0002H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b0\u0010)\u001a\u0017\u0010/\u001a\u00020\u0005*\u00020\u0005H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b1\u0010\u0007\u001a\u0017\u0010/\u001a\u00020\b*\u00020\bH\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b2\u0010,\u001a\u0017\u0010/\u001a\u00020\u000b*\u00020\u000bH\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b3\u0010.\u0082\u0002\u0004\n\u0002\b\u0019¨\u00064" }, d2 = { "countLeadingZeroBits", "", "Lkotlin/UByte;", "countLeadingZeroBits-7apg3OU", "(B)I", "Lkotlin/UInt;", "countLeadingZeroBits-WZ4Q5Ns", "(I)I", "Lkotlin/ULong;", "countLeadingZeroBits-VKZWuLQ", "(J)I", "Lkotlin/UShort;", "countLeadingZeroBits-xj2QHRw", "(S)I", "countOneBits", "countOneBits-7apg3OU", "countOneBits-WZ4Q5Ns", "countOneBits-VKZWuLQ", "countOneBits-xj2QHRw", "countTrailingZeroBits", "countTrailingZeroBits-7apg3OU", "countTrailingZeroBits-WZ4Q5Ns", "countTrailingZeroBits-VKZWuLQ", "countTrailingZeroBits-xj2QHRw", "rotateLeft", "bitCount", "rotateLeft-LxnNnR4", "(BI)B", "rotateLeft-V7xB4Y4", "(II)I", "rotateLeft-JSWoG40", "(JI)J", "rotateLeft-olVBNx4", "(SI)S", "rotateRight", "rotateRight-LxnNnR4", "rotateRight-V7xB4Y4", "rotateRight-JSWoG40", "rotateRight-olVBNx4", "takeHighestOneBit", "takeHighestOneBit-7apg3OU", "(B)B", "takeHighestOneBit-WZ4Q5Ns", "takeHighestOneBit-VKZWuLQ", "(J)J", "takeHighestOneBit-xj2QHRw", "(S)S", "takeLowestOneBit", "takeLowestOneBit-7apg3OU", "takeLowestOneBit-WZ4Q5Ns", "takeLowestOneBit-VKZWuLQ", "takeLowestOneBit-xj2QHRw", "kotlin-stdlib" }, k = 2, mv = { 1, 1, 15 })
public final class UNumbersKt
{
    private static final int countLeadingZeroBits-7apg3OU(final byte b) {
        return Integer.numberOfLeadingZeros(b & 0xFF) - 24;
    }
    
    private static final int countLeadingZeroBits-VKZWuLQ(final long i) {
        return Long.numberOfLeadingZeros(i);
    }
    
    private static final int countLeadingZeroBits-WZ4Q5Ns(final int i) {
        return Integer.numberOfLeadingZeros(i);
    }
    
    private static final int countLeadingZeroBits-xj2QHRw(final short n) {
        return Integer.numberOfLeadingZeros(n & 0xFFFF) - 16;
    }
    
    private static final int countOneBits-7apg3OU(final byte b) {
        return Integer.bitCount(UInt.constructor-impl(b & 0xFF));
    }
    
    private static final int countOneBits-VKZWuLQ(final long i) {
        return Long.bitCount(i);
    }
    
    private static final int countOneBits-WZ4Q5Ns(final int i) {
        return Integer.bitCount(i);
    }
    
    private static final int countOneBits-xj2QHRw(final short n) {
        return Integer.bitCount(UInt.constructor-impl(n & 0xFFFF));
    }
    
    private static final int countTrailingZeroBits-7apg3OU(final byte b) {
        return Integer.numberOfTrailingZeros(b | 0x100);
    }
    
    private static final int countTrailingZeroBits-VKZWuLQ(final long i) {
        return Long.numberOfTrailingZeros(i);
    }
    
    private static final int countTrailingZeroBits-WZ4Q5Ns(final int i) {
        return Integer.numberOfTrailingZeros(i);
    }
    
    private static final int countTrailingZeroBits-xj2QHRw(final short n) {
        return Integer.numberOfTrailingZeros(n | 0x10000);
    }
    
    private static final long rotateLeft-JSWoG40(final long i, final int distance) {
        return ULong.constructor-impl(Long.rotateLeft(i, distance));
    }
    
    private static final byte rotateLeft-LxnNnR4(final byte b, final int n) {
        return UByte.constructor-impl(NumbersKt__NumbersKt.rotateLeft(b, n));
    }
    
    private static final int rotateLeft-V7xB4Y4(final int i, final int distance) {
        return UInt.constructor-impl(Integer.rotateLeft(i, distance));
    }
    
    private static final short rotateLeft-olVBNx4(final short n, final int n2) {
        return UShort.constructor-impl(NumbersKt__NumbersKt.rotateLeft(n, n2));
    }
    
    private static final long rotateRight-JSWoG40(final long i, final int distance) {
        return ULong.constructor-impl(Long.rotateRight(i, distance));
    }
    
    private static final byte rotateRight-LxnNnR4(final byte b, final int n) {
        return UByte.constructor-impl(NumbersKt__NumbersKt.rotateRight(b, n));
    }
    
    private static final int rotateRight-V7xB4Y4(final int i, final int distance) {
        return UInt.constructor-impl(Integer.rotateRight(i, distance));
    }
    
    private static final short rotateRight-olVBNx4(final short n, final int n2) {
        return UShort.constructor-impl(NumbersKt__NumbersKt.rotateRight(n, n2));
    }
    
    private static final byte takeHighestOneBit-7apg3OU(final byte b) {
        return UByte.constructor-impl((byte)Integer.highestOneBit(b & 0xFF));
    }
    
    private static final long takeHighestOneBit-VKZWuLQ(final long i) {
        return ULong.constructor-impl(Long.highestOneBit(i));
    }
    
    private static final int takeHighestOneBit-WZ4Q5Ns(final int i) {
        return UInt.constructor-impl(Integer.highestOneBit(i));
    }
    
    private static final short takeHighestOneBit-xj2QHRw(final short n) {
        return UShort.constructor-impl((short)Integer.highestOneBit(n & 0xFFFF));
    }
    
    private static final byte takeLowestOneBit-7apg3OU(final byte b) {
        return UByte.constructor-impl((byte)Integer.lowestOneBit(b & 0xFF));
    }
    
    private static final long takeLowestOneBit-VKZWuLQ(final long i) {
        return ULong.constructor-impl(Long.lowestOneBit(i));
    }
    
    private static final int takeLowestOneBit-WZ4Q5Ns(final int i) {
        return UInt.constructor-impl(Integer.lowestOneBit(i));
    }
    
    private static final short takeLowestOneBit-xj2QHRw(final short n) {
        return UShort.constructor-impl((short)Integer.lowestOneBit(n & 0xFFFF));
    }
}
