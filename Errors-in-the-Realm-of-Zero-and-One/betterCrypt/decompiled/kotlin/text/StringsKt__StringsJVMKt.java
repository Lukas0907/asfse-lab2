// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.text;

import java.util.List;
import java.util.regex.Pattern;
import kotlin.jvm.functions.Function1;
import java.util.Iterator;
import kotlin.collections.IntIterator;
import java.util.Collection;
import kotlin.ranges.IntRange;
import java.util.Comparator;
import kotlin.jvm.internal.StringCompanionObject;
import java.util.Arrays;
import java.nio.CharBuffer;
import java.nio.ByteBuffer;
import java.nio.charset.CodingErrorAction;
import kotlin.collections.AbstractList;
import java.util.Locale;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;
import java.nio.charset.Charset;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000~\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0019\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\r\n\u0002\b\t\n\u0002\u0010\u0011\n\u0002\u0010\u0000\n\u0002\b\b\n\u0002\u0010\f\n\u0002\b\u0011\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\r\u001a\u0011\u0010\u0007\u001a\u00020\u00022\u0006\u0010\b\u001a\u00020\tH\u0087\b\u001a\u0011\u0010\u0007\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u000bH\u0087\b\u001a\u0011\u0010\u0007\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\rH\u0087\b\u001a\u0019\u0010\u0007\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0087\b\u001a!\u0010\u0007\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0011H\u0087\b\u001a)\u0010\u0007\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u000e\u001a\u00020\u000fH\u0087\b\u001a\u0011\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0014H\u0087\b\u001a!\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0011H\u0087\b\u001a!\u0010\u0007\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0011H\u0087\b\u001a\n\u0010\u0017\u001a\u00020\u0002*\u00020\u0002\u001a\u0014\u0010\u0017\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u0019H\u0007\u001a\u0015\u0010\u001a\u001a\u00020\u0011*\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u0011H\u0087\b\u001a\u0015\u0010\u001c\u001a\u00020\u0011*\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u0011H\u0087\b\u001a\u001d\u0010\u001d\u001a\u00020\u0011*\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u00112\u0006\u0010\u001f\u001a\u00020\u0011H\u0087\b\u001a\u001c\u0010 \u001a\u00020\u0011*\u00020\u00022\u0006\u0010!\u001a\u00020\u00022\b\b\u0002\u0010\"\u001a\u00020#\u001a\f\u0010$\u001a\u00020\u0002*\u00020\u0014H\u0007\u001a \u0010$\u001a\u00020\u0002*\u00020\u00142\b\b\u0002\u0010%\u001a\u00020\u00112\b\b\u0002\u0010\u001f\u001a\u00020\u0011H\u0007\u001a\u0015\u0010&\u001a\u00020#*\u00020\u00022\u0006\u0010\n\u001a\u00020\tH\u0087\b\u001a\u0015\u0010&\u001a\u00020#*\u00020\u00022\u0006\u0010'\u001a\u00020(H\u0087\b\u001a\n\u0010)\u001a\u00020\u0002*\u00020\u0002\u001a\u0014\u0010)\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u0019H\u0007\u001a\f\u0010*\u001a\u00020\u0002*\u00020\rH\u0007\u001a*\u0010*\u001a\u00020\u0002*\u00020\r2\b\b\u0002\u0010%\u001a\u00020\u00112\b\b\u0002\u0010\u001f\u001a\u00020\u00112\b\b\u0002\u0010+\u001a\u00020#H\u0007\u001a\f\u0010,\u001a\u00020\r*\u00020\u0002H\u0007\u001a*\u0010,\u001a\u00020\r*\u00020\u00022\b\b\u0002\u0010%\u001a\u00020\u00112\b\b\u0002\u0010\u001f\u001a\u00020\u00112\b\b\u0002\u0010+\u001a\u00020#H\u0007\u001a\u001c\u0010-\u001a\u00020#*\u00020\u00022\u0006\u0010.\u001a\u00020\u00022\b\b\u0002\u0010\"\u001a\u00020#\u001a \u0010/\u001a\u00020#*\u0004\u0018\u00010\u00022\b\u0010!\u001a\u0004\u0018\u00010\u00022\b\b\u0002\u0010\"\u001a\u00020#\u001a2\u00100\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00192\u0016\u00101\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010302\"\u0004\u0018\u000103H\u0087\b¢\u0006\u0002\u00104\u001a*\u00100\u001a\u00020\u0002*\u00020\u00022\u0016\u00101\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010302\"\u0004\u0018\u000103H\u0087\b¢\u0006\u0002\u00105\u001a:\u00100\u001a\u00020\u0002*\u00020\u00042\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u00100\u001a\u00020\u00022\u0016\u00101\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010302\"\u0004\u0018\u000103H\u0087\b¢\u0006\u0002\u00106\u001a2\u00100\u001a\u00020\u0002*\u00020\u00042\u0006\u00100\u001a\u00020\u00022\u0016\u00101\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010302\"\u0004\u0018\u000103H\u0087\b¢\u0006\u0002\u00107\u001a\r\u00108\u001a\u00020\u0002*\u00020\u0002H\u0087\b\u001a\n\u00109\u001a\u00020#*\u00020(\u001a\u001d\u0010:\u001a\u00020\u0011*\u00020\u00022\u0006\u0010;\u001a\u00020<2\u0006\u0010=\u001a\u00020\u0011H\u0081\b\u001a\u001d\u0010:\u001a\u00020\u0011*\u00020\u00022\u0006\u0010>\u001a\u00020\u00022\u0006\u0010=\u001a\u00020\u0011H\u0081\b\u001a\u001d\u0010?\u001a\u00020\u0011*\u00020\u00022\u0006\u0010;\u001a\u00020<2\u0006\u0010=\u001a\u00020\u0011H\u0081\b\u001a\u001d\u0010?\u001a\u00020\u0011*\u00020\u00022\u0006\u0010>\u001a\u00020\u00022\u0006\u0010=\u001a\u00020\u0011H\u0081\b\u001a\u001d\u0010@\u001a\u00020\u0011*\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u00112\u0006\u0010A\u001a\u00020\u0011H\u0087\b\u001a4\u0010B\u001a\u00020#*\u00020(2\u0006\u0010C\u001a\u00020\u00112\u0006\u0010!\u001a\u00020(2\u0006\u0010D\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00112\b\b\u0002\u0010\"\u001a\u00020#\u001a4\u0010B\u001a\u00020#*\u00020\u00022\u0006\u0010C\u001a\u00020\u00112\u0006\u0010!\u001a\u00020\u00022\u0006\u0010D\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00112\b\b\u0002\u0010\"\u001a\u00020#\u001a\u0012\u0010E\u001a\u00020\u0002*\u00020(2\u0006\u0010F\u001a\u00020\u0011\u001a$\u0010G\u001a\u00020\u0002*\u00020\u00022\u0006\u0010H\u001a\u00020<2\u0006\u0010I\u001a\u00020<2\b\b\u0002\u0010\"\u001a\u00020#\u001a$\u0010G\u001a\u00020\u0002*\u00020\u00022\u0006\u0010J\u001a\u00020\u00022\u0006\u0010K\u001a\u00020\u00022\b\b\u0002\u0010\"\u001a\u00020#\u001a$\u0010L\u001a\u00020\u0002*\u00020\u00022\u0006\u0010H\u001a\u00020<2\u0006\u0010I\u001a\u00020<2\b\b\u0002\u0010\"\u001a\u00020#\u001a$\u0010L\u001a\u00020\u0002*\u00020\u00022\u0006\u0010J\u001a\u00020\u00022\u0006\u0010K\u001a\u00020\u00022\b\b\u0002\u0010\"\u001a\u00020#\u001a\"\u0010M\u001a\b\u0012\u0004\u0012\u00020\u00020N*\u00020(2\u0006\u0010O\u001a\u00020P2\b\b\u0002\u0010Q\u001a\u00020\u0011\u001a\u001c\u0010R\u001a\u00020#*\u00020\u00022\u0006\u0010S\u001a\u00020\u00022\b\b\u0002\u0010\"\u001a\u00020#\u001a$\u0010R\u001a\u00020#*\u00020\u00022\u0006\u0010S\u001a\u00020\u00022\u0006\u0010%\u001a\u00020\u00112\b\b\u0002\u0010\"\u001a\u00020#\u001a\u0015\u0010T\u001a\u00020\u0002*\u00020\u00022\u0006\u0010%\u001a\u00020\u0011H\u0087\b\u001a\u001d\u0010T\u001a\u00020\u0002*\u00020\u00022\u0006\u0010%\u001a\u00020\u00112\u0006\u0010\u001f\u001a\u00020\u0011H\u0087\b\u001a\u0017\u0010U\u001a\u00020\r*\u00020\u00022\b\b\u0002\u0010\u000e\u001a\u00020\u000fH\u0087\b\u001a\r\u0010V\u001a\u00020\u0014*\u00020\u0002H\u0087\b\u001a3\u0010V\u001a\u00020\u0014*\u00020\u00022\u0006\u0010W\u001a\u00020\u00142\b\b\u0002\u0010X\u001a\u00020\u00112\b\b\u0002\u0010%\u001a\u00020\u00112\b\b\u0002\u0010\u001f\u001a\u00020\u0011H\u0087\b\u001a \u0010V\u001a\u00020\u0014*\u00020\u00022\b\b\u0002\u0010%\u001a\u00020\u00112\b\b\u0002\u0010\u001f\u001a\u00020\u0011H\u0007\u001a\r\u0010Y\u001a\u00020\u0002*\u00020\u0002H\u0087\b\u001a\u0015\u0010Y\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u0019H\u0087\b\u001a\u0017\u0010Z\u001a\u00020P*\u00020\u00022\b\b\u0002\u0010[\u001a\u00020\u0011H\u0087\b\u001a\r\u0010\\\u001a\u00020\u0002*\u00020\u0002H\u0087\b\u001a\u0015\u0010\\\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u0019H\u0087\b\"%\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u00020\u0001j\b\u0012\u0004\u0012\u00020\u0002`\u0003*\u00020\u00048F¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006¨\u0006]" }, d2 = { "CASE_INSENSITIVE_ORDER", "Ljava/util/Comparator;", "", "Lkotlin/Comparator;", "Lkotlin/String$Companion;", "getCASE_INSENSITIVE_ORDER", "(Lkotlin/jvm/internal/StringCompanionObject;)Ljava/util/Comparator;", "String", "stringBuffer", "Ljava/lang/StringBuffer;", "stringBuilder", "Ljava/lang/StringBuilder;", "bytes", "", "charset", "Ljava/nio/charset/Charset;", "offset", "", "length", "chars", "", "codePoints", "", "capitalize", "locale", "Ljava/util/Locale;", "codePointAt", "index", "codePointBefore", "codePointCount", "beginIndex", "endIndex", "compareTo", "other", "ignoreCase", "", "concatToString", "startIndex", "contentEquals", "charSequence", "", "decapitalize", "decodeToString", "throwOnInvalidSequence", "encodeToByteArray", "endsWith", "suffix", "equals", "format", "args", "", "", "(Ljava/lang/String;Ljava/util/Locale;[Ljava/lang/Object;)Ljava/lang/String;", "(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;", "(Lkotlin/jvm/internal/StringCompanionObject;Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;", "(Lkotlin/jvm/internal/StringCompanionObject;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;", "intern", "isBlank", "nativeIndexOf", "ch", "", "fromIndex", "str", "nativeLastIndexOf", "offsetByCodePoints", "codePointOffset", "regionMatches", "thisOffset", "otherOffset", "repeat", "n", "replace", "oldChar", "newChar", "oldValue", "newValue", "replaceFirst", "split", "", "regex", "Ljava/util/regex/Pattern;", "limit", "startsWith", "prefix", "substring", "toByteArray", "toCharArray", "destination", "destinationOffset", "toLowerCase", "toPattern", "flags", "toUpperCase", "kotlin-stdlib" }, k = 5, mv = { 1, 1, 15 }, xi = 1, xs = "kotlin/text/StringsKt")
class StringsKt__StringsJVMKt extends StringsKt__StringNumberConversionsKt
{
    public StringsKt__StringsJVMKt() {
    }
    
    private static final String String(final StringBuffer buffer) {
        return new String(buffer);
    }
    
    private static final String String(final StringBuilder builder) {
        return new String(builder);
    }
    
    private static final String String(final byte[] bytes) {
        return new String(bytes, Charsets.UTF_8);
    }
    
    private static final String String(final byte[] bytes, final int offset, final int length) {
        return new String(bytes, offset, length, Charsets.UTF_8);
    }
    
    private static final String String(final byte[] bytes, final int offset, final int length, final Charset charset) {
        return new String(bytes, offset, length, charset);
    }
    
    private static final String String(final byte[] bytes, final Charset charset) {
        return new String(bytes, charset);
    }
    
    private static final String String(final char[] value) {
        return new String(value);
    }
    
    private static final String String(final char[] value, final int offset, final int count) {
        return new String(value, offset, count);
    }
    
    private static final String String(final int[] codePoints, final int offset, final int count) {
        return new String(codePoints, offset, count);
    }
    
    public static final String capitalize(String substring) {
        Intrinsics.checkParameterIsNotNull(substring, "$this$capitalize");
        if (substring.length() <= 0 || !Character.isLowerCase(substring.charAt(0))) {
            return substring;
        }
        final StringBuilder sb = new StringBuilder();
        final String substring2 = substring.substring(0, 1);
        Intrinsics.checkExpressionValueIsNotNull(substring2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        if (substring2 != null) {
            final String upperCase = substring2.toUpperCase();
            Intrinsics.checkExpressionValueIsNotNull(upperCase, "(this as java.lang.String).toUpperCase()");
            sb.append(upperCase);
            substring = substring.substring(1);
            Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.String).substring(startIndex)");
            sb.append(substring);
            return sb.toString();
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    public static final String capitalize(String str, final Locale locale) {
        Intrinsics.checkParameterIsNotNull(str, "$this$capitalize");
        Intrinsics.checkParameterIsNotNull(locale, "locale");
        if (str.length() > 0) {
            final char char1 = str.charAt(0);
            if (Character.isLowerCase(char1)) {
                final StringBuilder sb = new StringBuilder();
                final char titleCase = Character.toTitleCase(char1);
                if (titleCase != Character.toUpperCase(char1)) {
                    sb.append(titleCase);
                }
                else {
                    final String substring = str.substring(0, 1);
                    Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                    if (substring == null) {
                        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                    }
                    final String upperCase = substring.toUpperCase(locale);
                    Intrinsics.checkExpressionValueIsNotNull(upperCase, "(this as java.lang.String).toUpperCase(locale)");
                    sb.append(upperCase);
                }
                str = str.substring(1);
                Intrinsics.checkExpressionValueIsNotNull(str, "(this as java.lang.String).substring(startIndex)");
                sb.append(str);
                str = sb.toString();
                Intrinsics.checkExpressionValueIsNotNull(str, "StringBuilder().apply(builderAction).toString()");
                return str;
            }
        }
        return str;
    }
    
    private static final int codePointAt(final String s, final int index) {
        if (s != null) {
            return s.codePointAt(index);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    private static final int codePointBefore(final String s, final int index) {
        if (s != null) {
            return s.codePointBefore(index);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    private static final int codePointCount(final String s, final int beginIndex, final int endIndex) {
        if (s != null) {
            return s.codePointCount(beginIndex, endIndex);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    public static final int compareTo(final String s, final String s2, final boolean b) {
        Intrinsics.checkParameterIsNotNull(s, "$this$compareTo");
        Intrinsics.checkParameterIsNotNull(s2, "other");
        if (b) {
            return s.compareToIgnoreCase(s2);
        }
        return s.compareTo(s2);
    }
    
    public static final String concatToString(final char[] value) {
        Intrinsics.checkParameterIsNotNull(value, "$this$concatToString");
        return new String(value);
    }
    
    public static final String concatToString(final char[] value, final int offset, final int n) {
        Intrinsics.checkParameterIsNotNull(value, "$this$concatToString");
        AbstractList.Companion.checkBoundsIndexes$kotlin_stdlib(offset, n, value.length);
        return new String(value, offset, n - offset);
    }
    
    private static final boolean contentEquals(final String s, final CharSequence cs) {
        if (s != null) {
            return s.contentEquals(cs);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    private static final boolean contentEquals(final String s, final StringBuffer sb) {
        if (s != null) {
            return s.contentEquals(sb);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    public static final String decapitalize(String substring) {
        Intrinsics.checkParameterIsNotNull(substring, "$this$decapitalize");
        if (substring.length() <= 0 || !Character.isUpperCase(substring.charAt(0))) {
            return substring;
        }
        final StringBuilder sb = new StringBuilder();
        final String substring2 = substring.substring(0, 1);
        Intrinsics.checkExpressionValueIsNotNull(substring2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        if (substring2 != null) {
            final String lowerCase = substring2.toLowerCase();
            Intrinsics.checkExpressionValueIsNotNull(lowerCase, "(this as java.lang.String).toLowerCase()");
            sb.append(lowerCase);
            substring = substring.substring(1);
            Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.String).substring(startIndex)");
            sb.append(substring);
            return sb.toString();
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    public static final String decapitalize(String substring, final Locale locale) {
        Intrinsics.checkParameterIsNotNull(substring, "$this$decapitalize");
        Intrinsics.checkParameterIsNotNull(locale, "locale");
        if (substring.length() <= 0 || Character.isLowerCase(substring.charAt(0))) {
            return substring;
        }
        final StringBuilder sb = new StringBuilder();
        final String substring2 = substring.substring(0, 1);
        Intrinsics.checkExpressionValueIsNotNull(substring2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        if (substring2 != null) {
            final String lowerCase = substring2.toLowerCase(locale);
            Intrinsics.checkExpressionValueIsNotNull(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            sb.append(lowerCase);
            substring = substring.substring(1);
            Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.String).substring(startIndex)");
            sb.append(substring);
            return sb.toString();
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    public static final String decodeToString(final byte[] bytes) {
        Intrinsics.checkParameterIsNotNull(bytes, "$this$decodeToString");
        return new String(bytes, Charsets.UTF_8);
    }
    
    public static final String decodeToString(final byte[] array, final int n, final int n2, final boolean b) {
        Intrinsics.checkParameterIsNotNull(array, "$this$decodeToString");
        AbstractList.Companion.checkBoundsIndexes$kotlin_stdlib(n, n2, array.length);
        if (!b) {
            return new String(array, n, n2 - n, Charsets.UTF_8);
        }
        final String string = Charsets.UTF_8.newDecoder().onMalformedInput(CodingErrorAction.REPORT).onUnmappableCharacter(CodingErrorAction.REPORT).decode(ByteBuffer.wrap(array, n, n2 - n)).toString();
        Intrinsics.checkExpressionValueIsNotNull(string, "decoder.decode(ByteBuffe\u2026- startIndex)).toString()");
        return string;
    }
    
    public static final byte[] encodeToByteArray(final String s) {
        Intrinsics.checkParameterIsNotNull(s, "$this$encodeToByteArray");
        final byte[] bytes = s.getBytes(Charsets.UTF_8);
        Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
        return bytes;
    }
    
    public static final byte[] encodeToByteArray(String substring, int remaining, final int n, final boolean b) {
        Intrinsics.checkParameterIsNotNull(substring, "$this$encodeToByteArray");
        AbstractList.Companion.checkBoundsIndexes$kotlin_stdlib(remaining, n, substring.length());
        if (b) {
            final ByteBuffer encode = Charsets.UTF_8.newEncoder().onMalformedInput(CodingErrorAction.REPORT).onUnmappableCharacter(CodingErrorAction.REPORT).encode(CharBuffer.wrap(substring, remaining, n));
            if (encode.hasArray() && encode.arrayOffset() == 0) {
                remaining = encode.remaining();
                final byte[] array = encode.array();
                if (array == null) {
                    Intrinsics.throwNpe();
                }
                if (remaining == array.length) {
                    final byte[] array2 = encode.array();
                    Intrinsics.checkExpressionValueIsNotNull(array2, "byteBuffer.array()");
                    return array2;
                }
            }
            final byte[] dst = new byte[encode.remaining()];
            encode.get(dst);
            return dst;
        }
        substring = substring.substring(remaining, n);
        Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        final Charset utf_8 = Charsets.UTF_8;
        if (substring != null) {
            final byte[] bytes = substring.getBytes(utf_8);
            Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    public static final boolean endsWith(final String s, final String suffix, final boolean b) {
        Intrinsics.checkParameterIsNotNull(s, "$this$endsWith");
        Intrinsics.checkParameterIsNotNull(suffix, "suffix");
        if (!b) {
            return s.endsWith(suffix);
        }
        return regionMatches(s, s.length() - suffix.length(), suffix, 0, suffix.length(), true);
    }
    
    public static final boolean equals(final String s, final String s2, final boolean b) {
        if (s == null) {
            return s2 == null;
        }
        if (!b) {
            return s.equals(s2);
        }
        return s.equalsIgnoreCase(s2);
    }
    
    private static final String format(String format, final Locale l, final Object... original) {
        format = String.format(l, format, Arrays.copyOf(original, original.length));
        Intrinsics.checkExpressionValueIsNotNull(format, "java.lang.String.format(locale, this, *args)");
        return format;
    }
    
    private static final String format(String format, final Object... original) {
        format = String.format(format, Arrays.copyOf(original, original.length));
        Intrinsics.checkExpressionValueIsNotNull(format, "java.lang.String.format(this, *args)");
        return format;
    }
    
    private static final String format(final StringCompanionObject stringCompanionObject, final String format, final Object... original) {
        final String format2 = String.format(format, Arrays.copyOf(original, original.length));
        Intrinsics.checkExpressionValueIsNotNull(format2, "java.lang.String.format(format, *args)");
        return format2;
    }
    
    private static final String format(final StringCompanionObject stringCompanionObject, final Locale l, final String format, final Object... original) {
        final String format2 = String.format(l, format, Arrays.copyOf(original, original.length));
        Intrinsics.checkExpressionValueIsNotNull(format2, "java.lang.String.format(locale, format, *args)");
        return format2;
    }
    
    public static final Comparator<String> getCASE_INSENSITIVE_ORDER(final StringCompanionObject stringCompanionObject) {
        Intrinsics.checkParameterIsNotNull(stringCompanionObject, "$this$CASE_INSENSITIVE_ORDER");
        final Comparator<String> case_INSENSITIVE_ORDER = String.CASE_INSENSITIVE_ORDER;
        Intrinsics.checkExpressionValueIsNotNull(case_INSENSITIVE_ORDER, "java.lang.String.CASE_INSENSITIVE_ORDER");
        return case_INSENSITIVE_ORDER;
    }
    
    private static final String intern(String intern) {
        if (intern != null) {
            intern = intern.intern();
            Intrinsics.checkExpressionValueIsNotNull(intern, "(this as java.lang.String).intern()");
            return intern;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    public static final boolean isBlank(final CharSequence charSequence) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$isBlank");
        final int length = charSequence.length();
        boolean b = false;
        if (length != 0) {
            final IntRange intRange = StringsKt__StringsKt.getIndices(charSequence);
            boolean b2 = false;
            Label_0089: {
                if (!(intRange instanceof Collection) || !((Collection<Object>)intRange).isEmpty()) {
                    final Iterator<Object> iterator = ((Iterable<Object>)intRange).iterator();
                    while (iterator.hasNext()) {
                        if (!CharsKt__CharJVMKt.isWhitespace(charSequence.charAt(((IntIterator)iterator).nextInt()))) {
                            b2 = false;
                            break Label_0089;
                        }
                    }
                }
                b2 = true;
            }
            if (!b2) {
                return b;
            }
        }
        b = true;
        return b;
    }
    
    private static final int nativeIndexOf(final String s, final char ch, final int fromIndex) {
        if (s != null) {
            return s.indexOf(ch, fromIndex);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    private static final int nativeIndexOf(final String s, final String str, final int fromIndex) {
        if (s != null) {
            return s.indexOf(str, fromIndex);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    private static final int nativeLastIndexOf(final String s, final char ch, final int fromIndex) {
        if (s != null) {
            return s.lastIndexOf(ch, fromIndex);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    private static final int nativeLastIndexOf(final String s, final String str, final int fromIndex) {
        if (s != null) {
            return s.lastIndexOf(str, fromIndex);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    private static final int offsetByCodePoints(final String s, final int index, final int codePointOffset) {
        if (s != null) {
            return s.offsetByCodePoints(index, codePointOffset);
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    public static final boolean regionMatches(final CharSequence charSequence, final int n, final CharSequence charSequence2, final int n2, final int n3, final boolean b) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$regionMatches");
        Intrinsics.checkParameterIsNotNull(charSequence2, "other");
        if (charSequence instanceof String && charSequence2 instanceof String) {
            return regionMatches((String)charSequence, n, (String)charSequence2, n2, n3, b);
        }
        return StringsKt__StringsKt.regionMatchesImpl(charSequence, n, charSequence2, n2, n3, b);
    }
    
    public static final boolean regionMatches(final String s, final int n, final String s2, final int n2, final int n3, final boolean ignoreCase) {
        Intrinsics.checkParameterIsNotNull(s, "$this$regionMatches");
        Intrinsics.checkParameterIsNotNull(s2, "other");
        if (!ignoreCase) {
            return s.regionMatches(n, s2, n2, n3);
        }
        return s.regionMatches(ignoreCase, n, s2, n2, n3);
    }
    
    public static final String repeat(final CharSequence s, final int i) {
        Intrinsics.checkParameterIsNotNull(s, "$this$repeat");
        final int n = 0;
        final int n2 = 1;
        if (i >= 0) {
            String string = "";
            if (i != 0) {
                if (i != 1) {
                    final int length = s.length();
                    string = string;
                    if (length != 0) {
                        if (length != 1) {
                            final StringBuilder sb = new StringBuilder(s.length() * i);
                            if (1 <= i) {
                                int n3 = n2;
                                while (true) {
                                    sb.append(s);
                                    if (n3 == i) {
                                        break;
                                    }
                                    ++n3;
                                }
                            }
                            final String string2 = sb.toString();
                            Intrinsics.checkExpressionValueIsNotNull(string2, "sb.toString()");
                            return string2;
                        }
                        final char char1 = s.charAt(0);
                        final char[] value = new char[i];
                        for (int j = n; j < i; ++j) {
                            value[j] = char1;
                        }
                        return new String(value);
                    }
                }
                else {
                    string = s.toString();
                }
            }
            return string;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Count 'n' must be non-negative, but was ");
        sb2.append(i);
        sb2.append('.');
        throw new IllegalArgumentException(sb2.toString().toString());
    }
    
    public static final String replace(String replace, final char oldChar, final char c, final boolean b) {
        Intrinsics.checkParameterIsNotNull(replace, "$this$replace");
        if (!b) {
            replace = replace.replace(oldChar, c);
            Intrinsics.checkExpressionValueIsNotNull(replace, "(this as java.lang.Strin\u2026replace(oldChar, newChar)");
            return replace;
        }
        return SequencesKt___SequencesKt.joinToString$default(StringsKt__StringsKt.splitToSequence$default(replace, new char[] { oldChar }, b, 0, 4, null), String.valueOf(c), null, null, 0, null, null, 62, null);
    }
    
    public static final String replace(final String s, final String s2, final String s3, final boolean b) {
        Intrinsics.checkParameterIsNotNull(s, "$this$replace");
        Intrinsics.checkParameterIsNotNull(s2, "oldValue");
        Intrinsics.checkParameterIsNotNull(s3, "newValue");
        return SequencesKt___SequencesKt.joinToString$default(StringsKt__StringsKt.splitToSequence$default(s, new String[] { s2 }, b, 0, 4, null), s3, null, null, 0, null, null, 62, null);
    }
    
    public static final String replaceFirst(final String s, final char c, final char c2, final boolean b) {
        Intrinsics.checkParameterIsNotNull(s, "$this$replaceFirst");
        final String s2 = s;
        final int indexOf$default = StringsKt__StringsKt.indexOf$default(s2, c, 0, b, 2, null);
        if (indexOf$default < 0) {
            return s;
        }
        return StringsKt__StringsKt.replaceRange((CharSequence)s2, indexOf$default, indexOf$default + 1, String.valueOf(c2)).toString();
    }
    
    public static final String replaceFirst(final String s, final String s2, final String s3, final boolean b) {
        Intrinsics.checkParameterIsNotNull(s, "$this$replaceFirst");
        Intrinsics.checkParameterIsNotNull(s2, "oldValue");
        Intrinsics.checkParameterIsNotNull(s3, "newValue");
        final String s4 = s;
        final int indexOf$default = StringsKt__StringsKt.indexOf$default(s4, s2, 0, b, 2, null);
        if (indexOf$default < 0) {
            return s;
        }
        return StringsKt__StringsKt.replaceRange((CharSequence)s4, indexOf$default, s2.length() + indexOf$default, s3).toString();
    }
    
    public static final List<String> split(final CharSequence input, final Pattern pattern, final int i) {
        Intrinsics.checkParameterIsNotNull(input, "$this$split");
        Intrinsics.checkParameterIsNotNull(pattern, "regex");
        if (i >= 0) {
            int limit;
            if ((limit = i) == 0) {
                limit = -1;
            }
            final String[] split = pattern.split(input, limit);
            Intrinsics.checkExpressionValueIsNotNull(split, "regex.split(this, if (limit == 0) -1 else limit)");
            return ArraysKt___ArraysJvmKt.asList(split);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Limit must be non-negative, but was ");
        sb.append(i);
        sb.append('.');
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public static final boolean startsWith(final String s, final String prefix, final int toffset, final boolean b) {
        Intrinsics.checkParameterIsNotNull(s, "$this$startsWith");
        Intrinsics.checkParameterIsNotNull(prefix, "prefix");
        if (!b) {
            return s.startsWith(prefix, toffset);
        }
        return regionMatches(s, toffset, prefix, 0, prefix.length(), b);
    }
    
    public static final boolean startsWith(final String s, final String prefix, final boolean b) {
        Intrinsics.checkParameterIsNotNull(s, "$this$startsWith");
        Intrinsics.checkParameterIsNotNull(prefix, "prefix");
        if (!b) {
            return s.startsWith(prefix);
        }
        return regionMatches(s, 0, prefix, 0, prefix.length(), b);
    }
    
    private static final String substring(String substring, final int beginIndex) {
        if (substring != null) {
            substring = substring.substring(beginIndex);
            Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.String).substring(startIndex)");
            return substring;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    private static final String substring(String substring, final int beginIndex, final int endIndex) {
        if (substring != null) {
            substring = substring.substring(beginIndex, endIndex);
            Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
            return substring;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    private static final byte[] toByteArray(final String s, final Charset charset) {
        if (s != null) {
            final byte[] bytes = s.getBytes(charset);
            Intrinsics.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    private static final char[] toCharArray(final String s) {
        if (s != null) {
            final char[] charArray = s.toCharArray();
            Intrinsics.checkExpressionValueIsNotNull(charArray, "(this as java.lang.String).toCharArray()");
            return charArray;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    public static final char[] toCharArray(final String s, final int srcBegin, final int srcEnd) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toCharArray");
        AbstractList.Companion.checkBoundsIndexes$kotlin_stdlib(srcBegin, srcEnd, s.length());
        final char[] dst = new char[srcEnd - srcBegin];
        s.getChars(srcBegin, srcEnd, dst, 0);
        return dst;
    }
    
    private static final char[] toCharArray(final String s, final char[] dst, final int dstBegin, final int srcBegin, final int srcEnd) {
        if (s != null) {
            s.getChars(srcBegin, srcEnd, dst, dstBegin);
            return dst;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    private static final String toLowerCase(String lowerCase) {
        if (lowerCase != null) {
            lowerCase = lowerCase.toLowerCase();
            Intrinsics.checkExpressionValueIsNotNull(lowerCase, "(this as java.lang.String).toLowerCase()");
            return lowerCase;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    private static final String toLowerCase(String lowerCase, final Locale locale) {
        if (lowerCase != null) {
            lowerCase = lowerCase.toLowerCase(locale);
            Intrinsics.checkExpressionValueIsNotNull(lowerCase, "(this as java.lang.String).toLowerCase(locale)");
            return lowerCase;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    private static final Pattern toPattern(final String regex, final int flags) {
        final Pattern compile = Pattern.compile(regex, flags);
        Intrinsics.checkExpressionValueIsNotNull(compile, "java.util.regex.Pattern.compile(this, flags)");
        return compile;
    }
    
    private static final String toUpperCase(String upperCase) {
        if (upperCase != null) {
            upperCase = upperCase.toUpperCase();
            Intrinsics.checkExpressionValueIsNotNull(upperCase, "(this as java.lang.String).toUpperCase()");
            return upperCase;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
    
    private static final String toUpperCase(String upperCase, final Locale locale) {
        if (upperCase != null) {
            upperCase = upperCase.toUpperCase(locale);
            Intrinsics.checkExpressionValueIsNotNull(upperCase, "(this as java.lang.String).toUpperCase(locale)");
            return upperCase;
        }
        throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
    }
}
