// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.text;

import kotlin.UShort;
import kotlin.ULong;
import kotlin.UInt;
import kotlin.UByte;
import kotlin.jvm.internal.Intrinsics;
import kotlin.UnsignedKt;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000,\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0013\u001a\u001e\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0005\u0010\u0006\u001a\u001e\u0010\u0000\u001a\u00020\u0001*\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u0004H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\b\u0010\t\u001a\u001e\u0010\u0000\u001a\u00020\u0001*\u00020\n2\u0006\u0010\u0003\u001a\u00020\u0004H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u000b\u0010\f\u001a\u001e\u0010\u0000\u001a\u00020\u0001*\u00020\r2\u0006\u0010\u0003\u001a\u00020\u0004H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u000e\u0010\u000f\u001a\u0014\u0010\u0010\u001a\u00020\u0002*\u00020\u0001H\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0011\u001a\u001c\u0010\u0010\u001a\u00020\u0002*\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0012\u001a\u0011\u0010\u0013\u001a\u0004\u0018\u00010\u0002*\u00020\u0001H\u0007\u00f8\u0001\u0000\u001a\u0019\u0010\u0013\u001a\u0004\u0018\u00010\u0002*\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\u0007\u00f8\u0001\u0000\u001a\u0014\u0010\u0014\u001a\u00020\u0007*\u00020\u0001H\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0015\u001a\u001c\u0010\u0014\u001a\u00020\u0007*\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0016\u001a\u0011\u0010\u0017\u001a\u0004\u0018\u00010\u0007*\u00020\u0001H\u0007\u00f8\u0001\u0000\u001a\u0019\u0010\u0017\u001a\u0004\u0018\u00010\u0007*\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\u0007\u00f8\u0001\u0000\u001a\u0014\u0010\u0018\u001a\u00020\n*\u00020\u0001H\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0019\u001a\u001c\u0010\u0018\u001a\u00020\n*\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u001a\u001a\u0011\u0010\u001b\u001a\u0004\u0018\u00010\n*\u00020\u0001H\u0007\u00f8\u0001\u0000\u001a\u0019\u0010\u001b\u001a\u0004\u0018\u00010\n*\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\u0007\u00f8\u0001\u0000\u001a\u0014\u0010\u001c\u001a\u00020\r*\u00020\u0001H\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u001d\u001a\u001c\u0010\u001c\u001a\u00020\r*\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u001e\u001a\u0011\u0010\u001f\u001a\u0004\u0018\u00010\r*\u00020\u0001H\u0007\u00f8\u0001\u0000\u001a\u0019\u0010\u001f\u001a\u0004\u0018\u00010\r*\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\u0007\u00f8\u0001\u0000\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006 " }, d2 = { "toString", "", "Lkotlin/UByte;", "radix", "", "toString-LxnNnR4", "(BI)Ljava/lang/String;", "Lkotlin/UInt;", "toString-V7xB4Y4", "(II)Ljava/lang/String;", "Lkotlin/ULong;", "toString-JSWoG40", "(JI)Ljava/lang/String;", "Lkotlin/UShort;", "toString-olVBNx4", "(SI)Ljava/lang/String;", "toUByte", "(Ljava/lang/String;)B", "(Ljava/lang/String;I)B", "toUByteOrNull", "toUInt", "(Ljava/lang/String;)I", "(Ljava/lang/String;I)I", "toUIntOrNull", "toULong", "(Ljava/lang/String;)J", "(Ljava/lang/String;I)J", "toULongOrNull", "toUShort", "(Ljava/lang/String;)S", "(Ljava/lang/String;I)S", "toUShortOrNull", "kotlin-stdlib" }, k = 2, mv = { 1, 1, 15 })
public final class UStringsKt
{
    public static final String toString-JSWoG40(final long n, final int n2) {
        return UnsignedKt.ulongToString(n, CharsKt__CharJVMKt.checkRadix(n2));
    }
    
    public static final String toString-LxnNnR4(final byte b, final int n) {
        final String string = Integer.toString(b & 0xFF, CharsKt__CharJVMKt.checkRadix(n));
        Intrinsics.checkExpressionValueIsNotNull(string, "java.lang.Integer.toStri\u2026(this, checkRadix(radix))");
        return string;
    }
    
    public static final String toString-V7xB4Y4(final int n, final int n2) {
        final String string = Long.toString((long)n & 0xFFFFFFFFL, CharsKt__CharJVMKt.checkRadix(n2));
        Intrinsics.checkExpressionValueIsNotNull(string, "java.lang.Long.toString(this, checkRadix(radix))");
        return string;
    }
    
    public static final String toString-olVBNx4(final short n, final int n2) {
        final String string = Integer.toString(n & 0xFFFF, CharsKt__CharJVMKt.checkRadix(n2));
        Intrinsics.checkExpressionValueIsNotNull(string, "java.lang.Integer.toStri\u2026(this, checkRadix(radix))");
        return string;
    }
    
    public static final byte toUByte(final String s) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toUByte");
        final UByte uByteOrNull = toUByteOrNull(s);
        if (uByteOrNull != null) {
            return uByteOrNull.unbox-impl();
        }
        StringsKt__StringNumberConversionsKt.numberFormatError(s);
        throw null;
    }
    
    public static final byte toUByte(final String s, final int n) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toUByte");
        final UByte uByteOrNull = toUByteOrNull(s, n);
        if (uByteOrNull != null) {
            return uByteOrNull.unbox-impl();
        }
        StringsKt__StringNumberConversionsKt.numberFormatError(s);
        throw null;
    }
    
    public static final UByte toUByteOrNull(final String s) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toUByteOrNull");
        return toUByteOrNull(s, 10);
    }
    
    public static final UByte toUByteOrNull(final String s, int unbox-impl) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toUByteOrNull");
        final UInt uIntOrNull = toUIntOrNull(s, unbox-impl);
        if (uIntOrNull == null) {
            return null;
        }
        unbox-impl = uIntOrNull.unbox-impl();
        if (UnsignedKt.uintCompare(unbox-impl, UInt.constructor-impl(255)) > 0) {
            return null;
        }
        return UByte.box-impl(UByte.constructor-impl((byte)unbox-impl));
    }
    
    public static final int toUInt(final String s) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toUInt");
        final UInt uIntOrNull = toUIntOrNull(s);
        if (uIntOrNull != null) {
            return uIntOrNull.unbox-impl();
        }
        StringsKt__StringNumberConversionsKt.numberFormatError(s);
        throw null;
    }
    
    public static final int toUInt(final String s, final int n) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toUInt");
        final UInt uIntOrNull = toUIntOrNull(s, n);
        if (uIntOrNull != null) {
            return uIntOrNull.unbox-impl();
        }
        StringsKt__StringNumberConversionsKt.numberFormatError(s);
        throw null;
    }
    
    public static final UInt toUIntOrNull(final String s) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toUIntOrNull");
        return toUIntOrNull(s, 10);
    }
    
    public static final UInt toUIntOrNull(final String s, final int n) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toUIntOrNull");
        CharsKt__CharJVMKt.checkRadix(n);
        final int length = s.length();
        if (length == 0) {
            return null;
        }
        int constructor-impl = 0;
        final char char1 = s.charAt(0);
        int i = 1;
        if (char1 < '0') {
            if (length == 1 || char1 != '+') {
                return null;
            }
        }
        else {
            i = 0;
        }
        final int constructor-impl2 = UInt.constructor-impl(n);
        final int uintDivide-J1ME1BU = UnsignedKt.uintDivide-J1ME1BU(-1, constructor-impl2);
        while (i < length) {
            final int digit = CharsKt__CharJVMKt.digitOf(s.charAt(i), n);
            if (digit < 0) {
                return null;
            }
            if (UnsignedKt.uintCompare(constructor-impl, uintDivide-J1ME1BU) > 0) {
                return null;
            }
            final int constructor-impl3 = UInt.constructor-impl(constructor-impl * constructor-impl2);
            constructor-impl = UInt.constructor-impl(UInt.constructor-impl(digit) + constructor-impl3);
            if (UnsignedKt.uintCompare(constructor-impl, constructor-impl3) < 0) {
                return null;
            }
            ++i;
        }
        return UInt.box-impl(constructor-impl);
    }
    
    public static final long toULong(final String s) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toULong");
        final ULong uLongOrNull = toULongOrNull(s);
        if (uLongOrNull != null) {
            return uLongOrNull.unbox-impl();
        }
        StringsKt__StringNumberConversionsKt.numberFormatError(s);
        throw null;
    }
    
    public static final long toULong(final String s, final int n) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toULong");
        final ULong uLongOrNull = toULongOrNull(s, n);
        if (uLongOrNull != null) {
            return uLongOrNull.unbox-impl();
        }
        StringsKt__StringNumberConversionsKt.numberFormatError(s);
        throw null;
    }
    
    public static final ULong toULongOrNull(final String s) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toULongOrNull");
        return toULongOrNull(s, 10);
    }
    
    public static final ULong toULongOrNull(final String s, final int n) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toULongOrNull");
        CharsKt__CharJVMKt.checkRadix(n);
        final int length = s.length();
        if (length == 0) {
            return null;
        }
        int i = 0;
        final char char1 = s.charAt(0);
        if (char1 < '0') {
            if (length == 1) {
                return null;
            }
            if (char1 != '+') {
                return null;
            }
            i = 1;
        }
        final long n2 = (long)UInt.constructor-impl(n) & 0xFFFFFFFFL;
        final long ulongDivide-eb3DHEI = UnsignedKt.ulongDivide-eb3DHEI(-1L, ULong.constructor-impl(n2));
        long constructor-impl = 0L;
        while (i < length) {
            final int digit = CharsKt__CharJVMKt.digitOf(s.charAt(i), n);
            if (digit < 0) {
                return null;
            }
            if (UnsignedKt.ulongCompare(constructor-impl, ulongDivide-eb3DHEI) > 0) {
                return null;
            }
            final long constructor-impl2 = ULong.constructor-impl(constructor-impl * ULong.constructor-impl(n2));
            constructor-impl = ULong.constructor-impl(ULong.constructor-impl((long)UInt.constructor-impl(digit) & 0xFFFFFFFFL) + constructor-impl2);
            if (UnsignedKt.ulongCompare(constructor-impl, constructor-impl2) < 0) {
                return null;
            }
            ++i;
        }
        return ULong.box-impl(constructor-impl);
    }
    
    public static final short toUShort(final String s) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toUShort");
        final UShort uShortOrNull = toUShortOrNull(s);
        if (uShortOrNull != null) {
            return uShortOrNull.unbox-impl();
        }
        StringsKt__StringNumberConversionsKt.numberFormatError(s);
        throw null;
    }
    
    public static final short toUShort(final String s, final int n) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toUShort");
        final UShort uShortOrNull = toUShortOrNull(s, n);
        if (uShortOrNull != null) {
            return uShortOrNull.unbox-impl();
        }
        StringsKt__StringNumberConversionsKt.numberFormatError(s);
        throw null;
    }
    
    public static final UShort toUShortOrNull(final String s) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toUShortOrNull");
        return toUShortOrNull(s, 10);
    }
    
    public static final UShort toUShortOrNull(final String s, int unbox-impl) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toUShortOrNull");
        final UInt uIntOrNull = toUIntOrNull(s, unbox-impl);
        if (uIntOrNull == null) {
            return null;
        }
        unbox-impl = uIntOrNull.unbox-impl();
        if (UnsignedKt.uintCompare(unbox-impl, UInt.constructor-impl(65535)) > 0) {
            return null;
        }
        return UShort.box-impl(UShort.constructor-impl((short)unbox-impl));
    }
}
