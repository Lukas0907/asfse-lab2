// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.text;

import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000.\n\u0000\n\u0002\u0010\u0001\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0005\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\n\n\u0002\b\u0003\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0000\u001a\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005*\u00020\u0003H\u0007¢\u0006\u0002\u0010\u0006\u001a\u001b\u0010\u0004\u001a\u0004\u0018\u00010\u0005*\u00020\u00032\u0006\u0010\u0007\u001a\u00020\bH\u0007¢\u0006\u0002\u0010\t\u001a\u0013\u0010\n\u001a\u0004\u0018\u00010\b*\u00020\u0003H\u0007¢\u0006\u0002\u0010\u000b\u001a\u001b\u0010\n\u001a\u0004\u0018\u00010\b*\u00020\u00032\u0006\u0010\u0007\u001a\u00020\bH\u0007¢\u0006\u0002\u0010\f\u001a\u0013\u0010\r\u001a\u0004\u0018\u00010\u000e*\u00020\u0003H\u0007¢\u0006\u0002\u0010\u000f\u001a\u001b\u0010\r\u001a\u0004\u0018\u00010\u000e*\u00020\u00032\u0006\u0010\u0007\u001a\u00020\bH\u0007¢\u0006\u0002\u0010\u0010\u001a\u0013\u0010\u0011\u001a\u0004\u0018\u00010\u0012*\u00020\u0003H\u0007¢\u0006\u0002\u0010\u0013\u001a\u001b\u0010\u0011\u001a\u0004\u0018\u00010\u0012*\u00020\u00032\u0006\u0010\u0007\u001a\u00020\bH\u0007¢\u0006\u0002\u0010\u0014¨\u0006\u0015" }, d2 = { "numberFormatError", "", "input", "", "toByteOrNull", "", "(Ljava/lang/String;)Ljava/lang/Byte;", "radix", "", "(Ljava/lang/String;I)Ljava/lang/Byte;", "toIntOrNull", "(Ljava/lang/String;)Ljava/lang/Integer;", "(Ljava/lang/String;I)Ljava/lang/Integer;", "toLongOrNull", "", "(Ljava/lang/String;)Ljava/lang/Long;", "(Ljava/lang/String;I)Ljava/lang/Long;", "toShortOrNull", "", "(Ljava/lang/String;)Ljava/lang/Short;", "(Ljava/lang/String;I)Ljava/lang/Short;", "kotlin-stdlib" }, k = 5, mv = { 1, 1, 15 }, xi = 1, xs = "kotlin/text/StringsKt")
class StringsKt__StringNumberConversionsKt extends StringsKt__StringNumberConversionsJVMKt
{
    public StringsKt__StringNumberConversionsKt() {
    }
    
    public static final Void numberFormatError(final String str) {
        Intrinsics.checkParameterIsNotNull(str, "input");
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid number format: '");
        sb.append(str);
        sb.append('\'');
        throw new NumberFormatException(sb.toString());
    }
    
    public static final Byte toByteOrNull(final String s) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toByteOrNull");
        return toByteOrNull(s, 10);
    }
    
    public static final Byte toByteOrNull(final String s, int intValue) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toByteOrNull");
        final Integer intOrNull = toIntOrNull(s, intValue);
        if (intOrNull != null) {
            intValue = intOrNull;
            if (intValue >= -128) {
                if (intValue > 127) {
                    return null;
                }
                return (byte)intValue;
            }
        }
        return null;
    }
    
    public static final Integer toIntOrNull(final String s) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toIntOrNull");
        return toIntOrNull(s, 10);
    }
    
    public static final Integer toIntOrNull(final String s, final int n) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toIntOrNull");
        CharsKt__CharJVMKt.checkRadix(n);
        final int length = s.length();
        if (length == 0) {
            return null;
        }
        final int n2 = 0;
        final int n3 = 0;
        final char char1 = s.charAt(0);
        int n4 = -2147483647;
        int index = 0;
        int n6 = 0;
        Label_0097: {
            int n5;
            if (char1 < '0') {
                if (length == 1) {
                    return null;
                }
                if (char1 == '-') {
                    n5 = Integer.MIN_VALUE;
                    index = 1;
                }
                else {
                    if (char1 == '+') {
                        n6 = 0;
                        index = 1;
                        break Label_0097;
                    }
                    return null;
                }
            }
            else {
                index = 0;
                n5 = n4;
            }
            final int n7 = index;
            n4 = n5;
            n6 = n7;
        }
        final int n8 = n4 / n;
        final int n9 = length - 1;
        int i = n2;
        if (index <= n9) {
            int n10 = n3;
            while (true) {
                final int digit = CharsKt__CharJVMKt.digitOf(s.charAt(index), n);
                if (digit < 0) {
                    return null;
                }
                if (n10 < n8) {
                    return null;
                }
                final int n11 = n10 * n;
                if (n11 < n4 + digit) {
                    return null;
                }
                final int n12 = i = n11 - digit;
                if (index == n9) {
                    break;
                }
                ++index;
                n10 = n12;
            }
        }
        if (n6 != 0) {
            return i;
        }
        return -i;
    }
    
    public static final Long toLongOrNull(final String s) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toLongOrNull");
        return toLongOrNull(s, 10);
    }
    
    public static final Long toLongOrNull(final String s, final int n) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toLongOrNull");
        CharsKt__CharJVMKt.checkRadix(n);
        final int length = s.length();
        if (length == 0) {
            return null;
        }
        int index = 0;
        final char char1 = s.charAt(0);
        long n3;
        final long n2 = n3 = -9223372036854775807L;
        int n4 = 0;
        Label_0092: {
            if (char1 < '0') {
                if (length == 1) {
                    return null;
                }
                if (char1 == '-') {
                    n3 = Long.MIN_VALUE;
                    index = 1;
                }
                else {
                    if (char1 == '+') {
                        n4 = 0;
                        index = 1;
                        n3 = n2;
                        break Label_0092;
                    }
                    return null;
                }
            }
            n4 = index;
        }
        final long n5 = n;
        final long n6 = n3 / n5;
        long n7 = 0L;
        final int n8 = length - 1;
        long l = n7;
        if (index <= n8) {
            while (true) {
                final int digit = CharsKt__CharJVMKt.digitOf(s.charAt(index), n);
                if (digit < 0) {
                    return null;
                }
                if (n7 < n6) {
                    return null;
                }
                final long n9 = n7 * n5;
                final long n10 = digit;
                if (n9 < n3 + n10) {
                    return null;
                }
                n7 = (l = n9 - n10);
                if (index == n8) {
                    break;
                }
                ++index;
            }
        }
        if (n4 != 0) {
            return l;
        }
        return -l;
    }
    
    public static final Short toShortOrNull(final String s) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toShortOrNull");
        return toShortOrNull(s, 10);
    }
    
    public static final Short toShortOrNull(final String s, int intValue) {
        Intrinsics.checkParameterIsNotNull(s, "$this$toShortOrNull");
        final Integer intOrNull = toIntOrNull(s, intValue);
        if (intOrNull != null) {
            intValue = intOrNull;
            if (intValue >= -32768) {
                if (intValue > 32767) {
                    return null;
                }
                return (short)intValue;
            }
        }
        return null;
    }
}
