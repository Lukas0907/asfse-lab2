// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.text;

import kotlin.TuplesKt;
import kotlin.jvm.functions.Function0;
import kotlin.collections.IndexingIterable;
import kotlin.collections.IndexedValue;
import kotlin.ranges.IntProgression;
import kotlin.collections.SlidingWindowKt;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.HashSet;
import kotlin.ranges.IntRange;
import java.util.Iterator;
import kotlin.TypeCastException;
import kotlin.random.Random;
import java.util.Comparator;
import kotlin.collections.Grouping;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import java.util.Collection;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import kotlin.jvm.functions.Function2;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Pair;
import kotlin.sequences.Sequence;
import java.util.List;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function1;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u00dc\u0001\n\u0000\n\u0002\u0010\u000b\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\f\n\u0002\b\u0002\n\u0002\u0010\u001c\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010%\n\u0002\b\b\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u001f\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0000\n\u0002\b\b\n\u0002\u0010\u000f\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\"\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\u001a!\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\n\u0010\u0006\u001a\u00020\u0001*\u00020\u0002\u001a!\u0010\u0006\u001a\u00020\u0001*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\u0010\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\b*\u00020\u0002\u001a\u0010\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00050\n*\u00020\u0002\u001aE\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u000e0\f\"\u0004\b\u0000\u0010\r\"\u0004\b\u0001\u0010\u000e*\u00020\u00022\u001e\u0010\u000f\u001a\u001a\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u000e0\u00100\u0004H\u0086\b\u001a3\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u00020\u00050\f\"\u0004\b\u0000\u0010\r*\u00020\u00022\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\r0\u0004H\u0086\b\u001aM\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u000e0\f\"\u0004\b\u0000\u0010\r\"\u0004\b\u0001\u0010\u000e*\u00020\u00022\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\r0\u00042\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u000e0\u0004H\u0086\b\u001aN\u0010\u0014\u001a\u0002H\u0015\"\u0004\b\u0000\u0010\r\"\u0018\b\u0001\u0010\u0015*\u0012\u0012\u0006\b\u0000\u0012\u0002H\r\u0012\u0006\b\u0000\u0012\u00020\u00050\u0016*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H\u00152\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\r0\u0004H\u0086\b¢\u0006\u0002\u0010\u0018\u001ah\u0010\u0014\u001a\u0002H\u0015\"\u0004\b\u0000\u0010\r\"\u0004\b\u0001\u0010\u000e\"\u0018\b\u0002\u0010\u0015*\u0012\u0012\u0006\b\u0000\u0012\u0002H\r\u0012\u0006\b\u0000\u0012\u0002H\u000e0\u0016*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H\u00152\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\r0\u00042\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u000e0\u0004H\u0086\b¢\u0006\u0002\u0010\u0019\u001a`\u0010\u001a\u001a\u0002H\u0015\"\u0004\b\u0000\u0010\r\"\u0004\b\u0001\u0010\u000e\"\u0018\b\u0002\u0010\u0015*\u0012\u0012\u0006\b\u0000\u0012\u0002H\r\u0012\u0006\b\u0000\u0012\u0002H\u000e0\u0016*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H\u00152\u001e\u0010\u000f\u001a\u001a\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u000e0\u00100\u0004H\u0086\b¢\u0006\u0002\u0010\u0018\u001a3\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u000e0\f\"\u0004\b\u0000\u0010\u000e*\u00020\u00022\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u000e0\u0004H\u0087\b\u001aN\u0010\u001d\u001a\u0002H\u0015\"\u0004\b\u0000\u0010\u000e\"\u0018\b\u0001\u0010\u0015*\u0012\u0012\u0006\b\u0000\u0012\u00020\u0005\u0012\u0006\b\u0000\u0012\u0002H\u000e0\u0016*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H\u00152\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u000e0\u0004H\u0087\b¢\u0006\u0002\u0010\u0018\u001a\u001a\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020 0\u001f*\u00020\u00022\u0006\u0010!\u001a\u00020\"H\u0007\u001a4\u0010\u001e\u001a\b\u0012\u0004\u0012\u0002H#0\u001f\"\u0004\b\u0000\u0010#*\u00020\u00022\u0006\u0010!\u001a\u00020\"2\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u0002H#0\u0004H\u0007\u001a\u001a\u0010$\u001a\b\u0012\u0004\u0012\u00020 0\n*\u00020\u00022\u0006\u0010!\u001a\u00020\"H\u0007\u001a4\u0010$\u001a\b\u0012\u0004\u0012\u0002H#0\n\"\u0004\b\u0000\u0010#*\u00020\u00022\u0006\u0010!\u001a\u00020\"2\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u0002H#0\u0004H\u0007\u001a\r\u0010%\u001a\u00020\"*\u00020\u0002H\u0087\b\u001a!\u0010%\u001a\u00020\"*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\u0012\u0010&\u001a\u00020\u0002*\u00020\u00022\u0006\u0010'\u001a\u00020\"\u001a\u0012\u0010&\u001a\u00020 *\u00020 2\u0006\u0010'\u001a\u00020\"\u001a\u0012\u0010(\u001a\u00020\u0002*\u00020\u00022\u0006\u0010'\u001a\u00020\"\u001a\u0012\u0010(\u001a\u00020 *\u00020 2\u0006\u0010'\u001a\u00020\"\u001a!\u0010)\u001a\u00020\u0002*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a!\u0010)\u001a\u00020 *\u00020 2\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a!\u0010*\u001a\u00020\u0002*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a!\u0010*\u001a\u00020 *\u00020 2\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a)\u0010+\u001a\u00020\u0005*\u00020\u00022\u0006\u0010,\u001a\u00020\"2\u0012\u0010-\u001a\u000e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020\u00050\u0004H\u0087\b\u001a\u001c\u0010.\u001a\u0004\u0018\u00010\u0005*\u00020\u00022\u0006\u0010,\u001a\u00020\"H\u0087\b¢\u0006\u0002\u0010/\u001a!\u00100\u001a\u00020\u0002*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a!\u00100\u001a\u00020 *\u00020 2\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a6\u00101\u001a\u00020\u0002*\u00020\u00022'\u0010\u0003\u001a#\u0012\u0013\u0012\u00110\"¢\u0006\f\b3\u0012\b\b4\u0012\u0004\b\b(,\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000102H\u0086\b\u001a6\u00101\u001a\u00020 *\u00020 2'\u0010\u0003\u001a#\u0012\u0013\u0012\u00110\"¢\u0006\f\b3\u0012\b\b4\u0012\u0004\b\b(,\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000102H\u0086\b\u001aQ\u00105\u001a\u0002H6\"\f\b\u0000\u00106*\u000607j\u0002`8*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H62'\u0010\u0003\u001a#\u0012\u0013\u0012\u00110\"¢\u0006\f\b3\u0012\b\b4\u0012\u0004\b\b(,\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000102H\u0086\b¢\u0006\u0002\u00109\u001a!\u0010:\u001a\u00020\u0002*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a!\u0010:\u001a\u00020 *\u00020 2\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a<\u0010;\u001a\u0002H6\"\f\b\u0000\u00106*\u000607j\u0002`8*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H62\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b¢\u0006\u0002\u0010<\u001a<\u0010=\u001a\u0002H6\"\f\b\u0000\u00106*\u000607j\u0002`8*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H62\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b¢\u0006\u0002\u0010<\u001a(\u0010>\u001a\u0004\u0018\u00010\u0005*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0087\b¢\u0006\u0002\u0010?\u001a(\u0010@\u001a\u0004\u0018\u00010\u0005*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0087\b¢\u0006\u0002\u0010?\u001a\n\u0010A\u001a\u00020\u0005*\u00020\u0002\u001a!\u0010A\u001a\u00020\u0005*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\u0011\u0010B\u001a\u0004\u0018\u00010\u0005*\u00020\u0002¢\u0006\u0002\u0010C\u001a(\u0010B\u001a\u0004\u0018\u00010\u0005*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b¢\u0006\u0002\u0010?\u001a3\u0010D\u001a\b\u0012\u0004\u0012\u0002H#0\u001f\"\u0004\b\u0000\u0010#*\u00020\u00022\u0018\u0010\u000f\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\n\u0012\b\u0012\u0004\u0012\u0002H#0\b0\u0004H\u0086\b\u001aL\u0010E\u001a\u0002H6\"\u0004\b\u0000\u0010#\"\u0010\b\u0001\u00106*\n\u0012\u0006\b\u0000\u0012\u0002H#0F*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H62\u0018\u0010\u000f\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\n\u0012\b\u0012\u0004\u0012\u0002H#0\b0\u0004H\u0086\b¢\u0006\u0002\u0010G\u001aI\u0010H\u001a\u0002H#\"\u0004\b\u0000\u0010#*\u00020\u00022\u0006\u0010I\u001a\u0002H#2'\u0010J\u001a#\u0012\u0013\u0012\u0011H#¢\u0006\f\b3\u0012\b\b4\u0012\u0004\b\b(K\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H#02H\u0086\b¢\u0006\u0002\u0010L\u001a^\u0010M\u001a\u0002H#\"\u0004\b\u0000\u0010#*\u00020\u00022\u0006\u0010I\u001a\u0002H#2<\u0010J\u001a8\u0012\u0013\u0012\u00110\"¢\u0006\f\b3\u0012\b\b4\u0012\u0004\b\b(,\u0012\u0013\u0012\u0011H#¢\u0006\f\b3\u0012\b\b4\u0012\u0004\b\b(K\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H#0NH\u0086\b¢\u0006\u0002\u0010O\u001aI\u0010P\u001a\u0002H#\"\u0004\b\u0000\u0010#*\u00020\u00022\u0006\u0010I\u001a\u0002H#2'\u0010J\u001a#\u0012\u0004\u0012\u00020\u0005\u0012\u0013\u0012\u0011H#¢\u0006\f\b3\u0012\b\b4\u0012\u0004\b\b(K\u0012\u0004\u0012\u0002H#02H\u0086\b¢\u0006\u0002\u0010L\u001a^\u0010Q\u001a\u0002H#\"\u0004\b\u0000\u0010#*\u00020\u00022\u0006\u0010I\u001a\u0002H#2<\u0010J\u001a8\u0012\u0013\u0012\u00110\"¢\u0006\f\b3\u0012\b\b4\u0012\u0004\b\b(,\u0012\u0004\u0012\u00020\u0005\u0012\u0013\u0012\u0011H#¢\u0006\f\b3\u0012\b\b4\u0012\u0004\b\b(K\u0012\u0004\u0012\u0002H#0NH\u0086\b¢\u0006\u0002\u0010O\u001a!\u0010R\u001a\u00020S*\u00020\u00022\u0012\u0010T\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020S0\u0004H\u0086\b\u001a6\u0010U\u001a\u00020S*\u00020\u00022'\u0010T\u001a#\u0012\u0013\u0012\u00110\"¢\u0006\f\b3\u0012\b\b4\u0012\u0004\b\b(,\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020S02H\u0086\b\u001a)\u0010V\u001a\u00020\u0005*\u00020\u00022\u0006\u0010,\u001a\u00020\"2\u0012\u0010-\u001a\u000e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020\u00050\u0004H\u0087\b\u001a\u0019\u0010W\u001a\u0004\u0018\u00010\u0005*\u00020\u00022\u0006\u0010,\u001a\u00020\"¢\u0006\u0002\u0010/\u001a9\u0010X\u001a\u0014\u0012\u0004\u0012\u0002H\r\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u001f0\f\"\u0004\b\u0000\u0010\r*\u00020\u00022\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\r0\u0004H\u0086\b\u001aS\u0010X\u001a\u0014\u0012\u0004\u0012\u0002H\r\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u000e0\u001f0\f\"\u0004\b\u0000\u0010\r\"\u0004\b\u0001\u0010\u000e*\u00020\u00022\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\r0\u00042\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u000e0\u0004H\u0086\b\u001aR\u0010Y\u001a\u0002H\u0015\"\u0004\b\u0000\u0010\r\"\u001c\b\u0001\u0010\u0015*\u0016\u0012\u0006\b\u0000\u0012\u0002H\r\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050Z0\u0016*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H\u00152\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\r0\u0004H\u0086\b¢\u0006\u0002\u0010\u0018\u001al\u0010Y\u001a\u0002H\u0015\"\u0004\b\u0000\u0010\r\"\u0004\b\u0001\u0010\u000e\"\u001c\b\u0002\u0010\u0015*\u0016\u0012\u0006\b\u0000\u0012\u0002H\r\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u000e0Z0\u0016*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H\u00152\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\r0\u00042\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u000e0\u0004H\u0086\b¢\u0006\u0002\u0010\u0019\u001a5\u0010[\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\r0\\\"\u0004\b\u0000\u0010\r*\u00020\u00022\u0014\b\u0004\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\r0\u0004H\u0087\b\u001a!\u0010]\u001a\u00020\"*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a!\u0010^\u001a\u00020\"*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\n\u0010_\u001a\u00020\u0005*\u00020\u0002\u001a!\u0010_\u001a\u00020\u0005*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\u0011\u0010`\u001a\u0004\u0018\u00010\u0005*\u00020\u0002¢\u0006\u0002\u0010C\u001a(\u0010`\u001a\u0004\u0018\u00010\u0005*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b¢\u0006\u0002\u0010?\u001a-\u0010a\u001a\b\u0012\u0004\u0012\u0002H#0\u001f\"\u0004\b\u0000\u0010#*\u00020\u00022\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H#0\u0004H\u0086\b\u001aB\u0010b\u001a\b\u0012\u0004\u0012\u0002H#0\u001f\"\u0004\b\u0000\u0010#*\u00020\u00022'\u0010\u000f\u001a#\u0012\u0013\u0012\u00110\"¢\u0006\f\b3\u0012\b\b4\u0012\u0004\b\b(,\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H#02H\u0086\b\u001aH\u0010c\u001a\b\u0012\u0004\u0012\u0002H#0\u001f\"\b\b\u0000\u0010#*\u00020d*\u00020\u00022)\u0010\u000f\u001a%\u0012\u0013\u0012\u00110\"¢\u0006\f\b3\u0012\b\b4\u0012\u0004\b\b(,\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u0001H#02H\u0086\b\u001aa\u0010e\u001a\u0002H6\"\b\b\u0000\u0010#*\u00020d\"\u0010\b\u0001\u00106*\n\u0012\u0006\b\u0000\u0012\u0002H#0F*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H62)\u0010\u000f\u001a%\u0012\u0013\u0012\u00110\"¢\u0006\f\b3\u0012\b\b4\u0012\u0004\b\b(,\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u0001H#02H\u0086\b¢\u0006\u0002\u0010f\u001a[\u0010g\u001a\u0002H6\"\u0004\b\u0000\u0010#\"\u0010\b\u0001\u00106*\n\u0012\u0006\b\u0000\u0012\u0002H#0F*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H62'\u0010\u000f\u001a#\u0012\u0013\u0012\u00110\"¢\u0006\f\b3\u0012\b\b4\u0012\u0004\b\b(,\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H#02H\u0086\b¢\u0006\u0002\u0010f\u001a3\u0010h\u001a\b\u0012\u0004\u0012\u0002H#0\u001f\"\b\b\u0000\u0010#*\u00020d*\u00020\u00022\u0014\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u0001H#0\u0004H\u0086\b\u001aL\u0010i\u001a\u0002H6\"\b\b\u0000\u0010#*\u00020d\"\u0010\b\u0001\u00106*\n\u0012\u0006\b\u0000\u0012\u0002H#0F*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H62\u0014\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u0001H#0\u0004H\u0086\b¢\u0006\u0002\u0010G\u001aF\u0010j\u001a\u0002H6\"\u0004\b\u0000\u0010#\"\u0010\b\u0001\u00106*\n\u0012\u0006\b\u0000\u0012\u0002H#0F*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H62\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H#0\u0004H\u0086\b¢\u0006\u0002\u0010G\u001a\u0011\u0010k\u001a\u0004\u0018\u00010\u0005*\u00020\u0002¢\u0006\u0002\u0010C\u001a8\u0010l\u001a\u0004\u0018\u00010\u0005\"\u000e\b\u0000\u0010#*\b\u0012\u0004\u0012\u0002H#0m*\u00020\u00022\u0012\u0010n\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H#0\u0004H\u0086\b¢\u0006\u0002\u0010?\u001a-\u0010o\u001a\u0004\u0018\u00010\u0005*\u00020\u00022\u001a\u0010p\u001a\u0016\u0012\u0006\b\u0000\u0012\u00020\u00050qj\n\u0012\u0006\b\u0000\u0012\u00020\u0005`r¢\u0006\u0002\u0010s\u001a\u0011\u0010t\u001a\u0004\u0018\u00010\u0005*\u00020\u0002¢\u0006\u0002\u0010C\u001a8\u0010u\u001a\u0004\u0018\u00010\u0005\"\u000e\b\u0000\u0010#*\b\u0012\u0004\u0012\u0002H#0m*\u00020\u00022\u0012\u0010n\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H#0\u0004H\u0086\b¢\u0006\u0002\u0010?\u001a-\u0010v\u001a\u0004\u0018\u00010\u0005*\u00020\u00022\u001a\u0010p\u001a\u0016\u0012\u0006\b\u0000\u0012\u00020\u00050qj\n\u0012\u0006\b\u0000\u0012\u00020\u0005`r¢\u0006\u0002\u0010s\u001a\n\u0010w\u001a\u00020\u0001*\u00020\u0002\u001a!\u0010w\u001a\u00020\u0001*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a0\u0010x\u001a\u0002Hy\"\b\b\u0000\u0010y*\u00020\u0002*\u0002Hy2\u0012\u0010T\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020S0\u0004H\u0087\b¢\u0006\u0002\u0010z\u001a-\u0010{\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\u0010*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a-\u0010{\u001a\u000e\u0012\u0004\u0012\u00020 \u0012\u0004\u0012\u00020 0\u0010*\u00020 2\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\r\u0010|\u001a\u00020\u0005*\u00020\u0002H\u0087\b\u001a\u0014\u0010|\u001a\u00020\u0005*\u00020\u00022\u0006\u0010|\u001a\u00020}H\u0007\u001a6\u0010~\u001a\u00020\u0005*\u00020\u00022'\u0010J\u001a#\u0012\u0013\u0012\u00110\u0005¢\u0006\f\b3\u0012\b\b4\u0012\u0004\b\b(K\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000502H\u0086\b\u001aK\u0010\u007f\u001a\u00020\u0005*\u00020\u00022<\u0010J\u001a8\u0012\u0013\u0012\u00110\"¢\u0006\f\b3\u0012\b\b4\u0012\u0004\b\b(,\u0012\u0013\u0012\u00110\u0005¢\u0006\f\b3\u0012\b\b4\u0012\u0004\b\b(K\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050NH\u0086\b\u001a7\u0010\u0080\u0001\u001a\u00020\u0005*\u00020\u00022'\u0010J\u001a#\u0012\u0004\u0012\u00020\u0005\u0012\u0013\u0012\u00110\u0005¢\u0006\f\b3\u0012\b\b4\u0012\u0004\b\b(K\u0012\u0004\u0012\u00020\u000502H\u0086\b\u001aL\u0010\u0081\u0001\u001a\u00020\u0005*\u00020\u00022<\u0010J\u001a8\u0012\u0013\u0012\u00110\"¢\u0006\f\b3\u0012\b\b4\u0012\u0004\b\b(,\u0012\u0004\u0012\u00020\u0005\u0012\u0013\u0012\u00110\u0005¢\u0006\f\b3\u0012\b\b4\u0012\u0004\b\b(K\u0012\u0004\u0012\u00020\u00050NH\u0086\b\u001a\u000b\u0010\u0082\u0001\u001a\u00020\u0002*\u00020\u0002\u001a\u000e\u0010\u0082\u0001\u001a\u00020 *\u00020 H\u0087\b\u001a\u000b\u0010\u0083\u0001\u001a\u00020\u0005*\u00020\u0002\u001a\"\u0010\u0083\u0001\u001a\u00020\u0005*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\u0012\u0010\u0084\u0001\u001a\u0004\u0018\u00010\u0005*\u00020\u0002¢\u0006\u0002\u0010C\u001a)\u0010\u0084\u0001\u001a\u0004\u0018\u00010\u0005*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b¢\u0006\u0002\u0010?\u001a\u001a\u0010\u0085\u0001\u001a\u00020\u0002*\u00020\u00022\r\u0010\u0086\u0001\u001a\b\u0012\u0004\u0012\u00020\"0\b\u001a\u0015\u0010\u0085\u0001\u001a\u00020\u0002*\u00020\u00022\b\u0010\u0086\u0001\u001a\u00030\u0087\u0001\u001a\u001d\u0010\u0085\u0001\u001a\u00020 *\u00020 2\r\u0010\u0086\u0001\u001a\b\u0012\u0004\u0012\u00020\"0\bH\u0087\b\u001a\u0015\u0010\u0085\u0001\u001a\u00020 *\u00020 2\b\u0010\u0086\u0001\u001a\u00030\u0087\u0001\u001a\"\u0010\u0088\u0001\u001a\u00020\"*\u00020\u00022\u0012\u0010n\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\"0\u0004H\u0086\b\u001a$\u0010\u0089\u0001\u001a\u00030\u008a\u0001*\u00020\u00022\u0013\u0010n\u001a\u000f\u0012\u0004\u0012\u00020\u0005\u0012\u0005\u0012\u00030\u008a\u00010\u0004H\u0086\b\u001a\u0013\u0010\u008b\u0001\u001a\u00020\u0002*\u00020\u00022\u0006\u0010'\u001a\u00020\"\u001a\u0013\u0010\u008b\u0001\u001a\u00020 *\u00020 2\u0006\u0010'\u001a\u00020\"\u001a\u0013\u0010\u008c\u0001\u001a\u00020\u0002*\u00020\u00022\u0006\u0010'\u001a\u00020\"\u001a\u0013\u0010\u008c\u0001\u001a\u00020 *\u00020 2\u0006\u0010'\u001a\u00020\"\u001a\"\u0010\u008d\u0001\u001a\u00020\u0002*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\"\u0010\u008d\u0001\u001a\u00020 *\u00020 2\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\"\u0010\u008e\u0001\u001a\u00020\u0002*\u00020\u00022\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a\"\u0010\u008e\u0001\u001a\u00020 *\u00020 2\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004H\u0086\b\u001a+\u0010\u008f\u0001\u001a\u0002H6\"\u0010\b\u0000\u00106*\n\u0012\u0006\b\u0000\u0012\u00020\u00050F*\u00020\u00022\u0006\u0010\u0017\u001a\u0002H6¢\u0006\u0003\u0010\u0090\u0001\u001a\u001d\u0010\u0091\u0001\u001a\u0014\u0012\u0004\u0012\u00020\u00050\u0092\u0001j\t\u0012\u0004\u0012\u00020\u0005`\u0093\u0001*\u00020\u0002\u001a\u0011\u0010\u0094\u0001\u001a\b\u0012\u0004\u0012\u00020\u00050\u001f*\u00020\u0002\u001a\u0011\u0010\u0095\u0001\u001a\b\u0012\u0004\u0012\u00020\u00050Z*\u00020\u0002\u001a\u0012\u0010\u0096\u0001\u001a\t\u0012\u0004\u0012\u00020\u00050\u0097\u0001*\u00020\u0002\u001a1\u0010\u0098\u0001\u001a\b\u0012\u0004\u0012\u00020 0\u001f*\u00020\u00022\u0006\u0010!\u001a\u00020\"2\t\b\u0002\u0010\u0099\u0001\u001a\u00020\"2\t\b\u0002\u0010\u009a\u0001\u001a\u00020\u0001H\u0007\u001aK\u0010\u0098\u0001\u001a\b\u0012\u0004\u0012\u0002H#0\u001f\"\u0004\b\u0000\u0010#*\u00020\u00022\u0006\u0010!\u001a\u00020\"2\t\b\u0002\u0010\u0099\u0001\u001a\u00020\"2\t\b\u0002\u0010\u009a\u0001\u001a\u00020\u00012\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u0002H#0\u0004H\u0007\u001a1\u0010\u009b\u0001\u001a\b\u0012\u0004\u0012\u00020 0\n*\u00020\u00022\u0006\u0010!\u001a\u00020\"2\t\b\u0002\u0010\u0099\u0001\u001a\u00020\"2\t\b\u0002\u0010\u009a\u0001\u001a\u00020\u0001H\u0007\u001aK\u0010\u009b\u0001\u001a\b\u0012\u0004\u0012\u0002H#0\n\"\u0004\b\u0000\u0010#*\u00020\u00022\u0006\u0010!\u001a\u00020\"2\t\b\u0002\u0010\u0099\u0001\u001a\u00020\"2\t\b\u0002\u0010\u009a\u0001\u001a\u00020\u00012\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u0002H#0\u0004H\u0007\u001a\u0018\u0010\u009c\u0001\u001a\u000f\u0012\u000b\u0012\t\u0012\u0004\u0012\u00020\u00050\u009d\u00010\b*\u00020\u0002\u001a)\u0010\u009e\u0001\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u00100\u001f*\u00020\u00022\u0007\u0010\u009f\u0001\u001a\u00020\u0002H\u0086\u0004\u001a]\u0010\u009e\u0001\u001a\b\u0012\u0004\u0012\u0002H\u000e0\u001f\"\u0004\b\u0000\u0010\u000e*\u00020\u00022\u0007\u0010\u009f\u0001\u001a\u00020\u000228\u0010\u000f\u001a4\u0012\u0014\u0012\u00120\u0005¢\u0006\r\b3\u0012\t\b4\u0012\u0005\b\b( \u0001\u0012\u0014\u0012\u00120\u0005¢\u0006\r\b3\u0012\t\b4\u0012\u0005\b\b(¡\u0001\u0012\u0004\u0012\u0002H\u000e02H\u0086\b\u001a\u001f\u0010¢\u0001\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u00100\u001f*\u00020\u0002H\u0007\u001aT\u0010¢\u0001\u001a\b\u0012\u0004\u0012\u0002H#0\u001f\"\u0004\b\u0000\u0010#*\u00020\u000228\u0010\u000f\u001a4\u0012\u0014\u0012\u00120\u0005¢\u0006\r\b3\u0012\t\b4\u0012\u0005\b\b( \u0001\u0012\u0014\u0012\u00120\u0005¢\u0006\r\b3\u0012\t\b4\u0012\u0005\b\b(¡\u0001\u0012\u0004\u0012\u0002H#02H\u0087\b¨\u0006£\u0001" }, d2 = { "all", "", "", "predicate", "Lkotlin/Function1;", "", "any", "asIterable", "", "asSequence", "Lkotlin/sequences/Sequence;", "associate", "", "K", "V", "transform", "Lkotlin/Pair;", "associateBy", "keySelector", "valueTransform", "associateByTo", "M", "", "destination", "(Ljava/lang/CharSequence;Ljava/util/Map;Lkotlin/jvm/functions/Function1;)Ljava/util/Map;", "(Ljava/lang/CharSequence;Ljava/util/Map;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Ljava/util/Map;", "associateTo", "associateWith", "valueSelector", "associateWithTo", "chunked", "", "", "size", "", "R", "chunkedSequence", "count", "drop", "n", "dropLast", "dropLastWhile", "dropWhile", "elementAtOrElse", "index", "defaultValue", "elementAtOrNull", "(Ljava/lang/CharSequence;I)Ljava/lang/Character;", "filter", "filterIndexed", "Lkotlin/Function2;", "Lkotlin/ParameterName;", "name", "filterIndexedTo", "C", "Ljava/lang/Appendable;", "Lkotlin/text/Appendable;", "(Ljava/lang/CharSequence;Ljava/lang/Appendable;Lkotlin/jvm/functions/Function2;)Ljava/lang/Appendable;", "filterNot", "filterNotTo", "(Ljava/lang/CharSequence;Ljava/lang/Appendable;Lkotlin/jvm/functions/Function1;)Ljava/lang/Appendable;", "filterTo", "find", "(Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function1;)Ljava/lang/Character;", "findLast", "first", "firstOrNull", "(Ljava/lang/CharSequence;)Ljava/lang/Character;", "flatMap", "flatMapTo", "", "(Ljava/lang/CharSequence;Ljava/util/Collection;Lkotlin/jvm/functions/Function1;)Ljava/util/Collection;", "fold", "initial", "operation", "acc", "(Ljava/lang/CharSequence;Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;", "foldIndexed", "Lkotlin/Function3;", "(Ljava/lang/CharSequence;Ljava/lang/Object;Lkotlin/jvm/functions/Function3;)Ljava/lang/Object;", "foldRight", "foldRightIndexed", "forEach", "", "action", "forEachIndexed", "getOrElse", "getOrNull", "groupBy", "groupByTo", "", "groupingBy", "Lkotlin/collections/Grouping;", "indexOfFirst", "indexOfLast", "last", "lastOrNull", "map", "mapIndexed", "mapIndexedNotNull", "", "mapIndexedNotNullTo", "(Ljava/lang/CharSequence;Ljava/util/Collection;Lkotlin/jvm/functions/Function2;)Ljava/util/Collection;", "mapIndexedTo", "mapNotNull", "mapNotNullTo", "mapTo", "max", "maxBy", "", "selector", "maxWith", "comparator", "Ljava/util/Comparator;", "Lkotlin/Comparator;", "(Ljava/lang/CharSequence;Ljava/util/Comparator;)Ljava/lang/Character;", "min", "minBy", "minWith", "none", "onEach", "S", "(Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function1;)Ljava/lang/CharSequence;", "partition", "random", "Lkotlin/random/Random;", "reduce", "reduceIndexed", "reduceRight", "reduceRightIndexed", "reversed", "single", "singleOrNull", "slice", "indices", "Lkotlin/ranges/IntRange;", "sumBy", "sumByDouble", "", "take", "takeLast", "takeLastWhile", "takeWhile", "toCollection", "(Ljava/lang/CharSequence;Ljava/util/Collection;)Ljava/util/Collection;", "toHashSet", "Ljava/util/HashSet;", "Lkotlin/collections/HashSet;", "toList", "toMutableList", "toSet", "", "windowed", "step", "partialWindows", "windowedSequence", "withIndex", "Lkotlin/collections/IndexedValue;", "zip", "other", "a", "b", "zipWithNext", "kotlin-stdlib" }, k = 5, mv = { 1, 1, 15 }, xi = 1, xs = "kotlin/text/StringsKt")
class StringsKt___StringsKt extends StringsKt___StringsJvmKt
{
    public StringsKt___StringsKt() {
    }
    
    public static final boolean all(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$all");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        for (int i = 0; i < charSequence.length(); ++i) {
            if (!function1.invoke(charSequence.charAt(i))) {
                return false;
            }
        }
        return true;
    }
    
    public static final boolean any(final CharSequence charSequence) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$any");
        return charSequence.length() == 0 ^ true;
    }
    
    public static final boolean any(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$any");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        for (int i = 0; i < charSequence.length(); ++i) {
            if (function1.invoke(charSequence.charAt(i))) {
                return true;
            }
        }
        return false;
    }
    
    public static final Iterable<Character> asIterable(final CharSequence charSequence) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$asIterable");
        if (charSequence instanceof String && charSequence.length() == 0) {
            return (Iterable<Character>)CollectionsKt__CollectionsKt.emptyList();
        }
        return (Iterable<Character>)new StringsKt___StringsKt$asIterable$$inlined$Iterable.StringsKt___StringsKt$asIterable$$inlined$Iterable$1(charSequence);
    }
    
    public static final Sequence<Character> asSequence(final CharSequence charSequence) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$asSequence");
        if (charSequence instanceof String && charSequence.length() == 0) {
            return SequencesKt__SequencesKt.emptySequence();
        }
        return (Sequence<Character>)new StringsKt___StringsKt$asSequence$$inlined$Sequence.StringsKt___StringsKt$asSequence$$inlined$Sequence$1(charSequence);
    }
    
    public static final <K, V> Map<K, V> associate(final CharSequence charSequence, final Function1<? super Character, ? extends Pair<? extends K, ? extends V>> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$associate");
        Intrinsics.checkParameterIsNotNull(function1, "transform");
        final LinkedHashMap<Object, Object> linkedHashMap = new LinkedHashMap<Object, Object>(RangesKt___RangesKt.coerceAtLeast(MapsKt__MapsKt.mapCapacity(charSequence.length()), 16));
        for (int i = 0; i < charSequence.length(); ++i) {
            final Pair pair = (Pair)function1.invoke(charSequence.charAt(i));
            linkedHashMap.put(pair.getFirst(), pair.getSecond());
        }
        return (Map<K, V>)linkedHashMap;
    }
    
    public static final <K> Map<K, Character> associateBy(final CharSequence charSequence, final Function1<? super Character, ? extends K> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$associateBy");
        Intrinsics.checkParameterIsNotNull(function1, "keySelector");
        final LinkedHashMap<Object, Character> linkedHashMap = new LinkedHashMap<Object, Character>(RangesKt___RangesKt.coerceAtLeast(MapsKt__MapsKt.mapCapacity(charSequence.length()), 16));
        for (int i = 0; i < charSequence.length(); ++i) {
            final char char1 = charSequence.charAt(i);
            linkedHashMap.put(function1.invoke(char1), char1);
        }
        return (Map<K, Character>)linkedHashMap;
    }
    
    public static final <K, V> Map<K, V> associateBy(final CharSequence charSequence, final Function1<? super Character, ? extends K> function1, final Function1<? super Character, ? extends V> function2) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$associateBy");
        Intrinsics.checkParameterIsNotNull(function1, "keySelector");
        Intrinsics.checkParameterIsNotNull(function2, "valueTransform");
        final LinkedHashMap<Object, Object> linkedHashMap = new LinkedHashMap<Object, Object>(RangesKt___RangesKt.coerceAtLeast(MapsKt__MapsKt.mapCapacity(charSequence.length()), 16));
        for (int i = 0; i < charSequence.length(); ++i) {
            final char char1 = charSequence.charAt(i);
            linkedHashMap.put(function1.invoke(char1), function2.invoke(char1));
        }
        return (Map<K, V>)linkedHashMap;
    }
    
    public static final <K, M extends Map<? super K, ? super Character>> M associateByTo(final CharSequence charSequence, final M m, final Function1<? super Character, ? extends K> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$associateByTo");
        Intrinsics.checkParameterIsNotNull(m, "destination");
        Intrinsics.checkParameterIsNotNull(function1, "keySelector");
        for (int i = 0; i < charSequence.length(); ++i) {
            final char char1 = charSequence.charAt(i);
            ((Map<? super K, Character>)m).put((Object)function1.invoke(char1), Character.valueOf(char1));
        }
        return m;
    }
    
    public static final <K, V, M extends Map<? super K, ? super V>> M associateByTo(final CharSequence charSequence, final M m, final Function1<? super Character, ? extends K> function1, final Function1<? super Character, ? extends V> function2) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$associateByTo");
        Intrinsics.checkParameterIsNotNull(m, "destination");
        Intrinsics.checkParameterIsNotNull(function1, "keySelector");
        Intrinsics.checkParameterIsNotNull(function2, "valueTransform");
        for (int i = 0; i < charSequence.length(); ++i) {
            final char char1 = charSequence.charAt(i);
            m.put((Object)function1.invoke(char1), (Object)function2.invoke(char1));
        }
        return m;
    }
    
    public static final <K, V, M extends Map<? super K, ? super V>> M associateTo(final CharSequence charSequence, final M m, final Function1<? super Character, ? extends Pair<? extends K, ? extends V>> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$associateTo");
        Intrinsics.checkParameterIsNotNull(m, "destination");
        Intrinsics.checkParameterIsNotNull(function1, "transform");
        for (int i = 0; i < charSequence.length(); ++i) {
            final Pair pair = (Pair)function1.invoke(charSequence.charAt(i));
            m.put(pair.getFirst(), (Object)pair.getSecond());
        }
        return m;
    }
    
    public static final <V> Map<Character, V> associateWith(final CharSequence charSequence, final Function1<? super Character, ? extends V> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$associateWith");
        Intrinsics.checkParameterIsNotNull(function1, "valueSelector");
        final LinkedHashMap<Character, V> linkedHashMap = new LinkedHashMap<Character, V>(RangesKt___RangesKt.coerceAtLeast(MapsKt__MapsKt.mapCapacity(charSequence.length()), 16));
        for (int i = 0; i < charSequence.length(); ++i) {
            final char char1 = charSequence.charAt(i);
            ((LinkedHashMap<Character, Object>)linkedHashMap).put(char1, function1.invoke(char1));
        }
        return linkedHashMap;
    }
    
    public static final <V, M extends Map<? super Character, ? super V>> M associateWithTo(final CharSequence charSequence, final M m, final Function1<? super Character, ? extends V> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$associateWithTo");
        Intrinsics.checkParameterIsNotNull(m, "destination");
        Intrinsics.checkParameterIsNotNull(function1, "valueSelector");
        for (int i = 0; i < charSequence.length(); ++i) {
            final char char1 = charSequence.charAt(i);
            ((Map<Character, ? super V>)m).put(Character.valueOf(char1), (Object)function1.invoke(char1));
        }
        return m;
    }
    
    public static final List<String> chunked(final CharSequence charSequence, final int n) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$chunked");
        return windowed(charSequence, n, n, true);
    }
    
    public static final <R> List<R> chunked(final CharSequence charSequence, final int n, final Function1<? super CharSequence, ? extends R> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$chunked");
        Intrinsics.checkParameterIsNotNull(function1, "transform");
        return windowed(charSequence, n, n, true, function1);
    }
    
    public static final Sequence<String> chunkedSequence(final CharSequence charSequence, final int n) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$chunkedSequence");
        return chunkedSequence(charSequence, n, (Function1<? super CharSequence, ? extends String>)StringsKt___StringsKt$chunkedSequence.StringsKt___StringsKt$chunkedSequence$1.INSTANCE);
    }
    
    public static final <R> Sequence<R> chunkedSequence(final CharSequence charSequence, final int n, final Function1<? super CharSequence, ? extends R> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$chunkedSequence");
        Intrinsics.checkParameterIsNotNull(function1, "transform");
        return windowedSequence(charSequence, n, n, true, function1);
    }
    
    private static final int count(final CharSequence charSequence) {
        return charSequence.length();
    }
    
    public static final int count(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$count");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        int i = 0;
        int n = 0;
        while (i < charSequence.length()) {
            int n2 = n;
            if (function1.invoke(charSequence.charAt(i))) {
                n2 = n + 1;
            }
            ++i;
            n = n2;
        }
        return n;
    }
    
    public static final CharSequence drop(final CharSequence charSequence, final int i) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$drop");
        if (i >= 0) {
            return charSequence.subSequence(RangesKt___RangesKt.coerceAtMost(i, charSequence.length()), charSequence.length());
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Requested character count ");
        sb.append(i);
        sb.append(" is less than zero.");
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public static final String drop(String substring, final int i) {
        Intrinsics.checkParameterIsNotNull(substring, "$this$drop");
        if (i >= 0) {
            substring = substring.substring(RangesKt___RangesKt.coerceAtMost(i, substring.length()));
            Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.String).substring(startIndex)");
            return substring;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Requested character count ");
        sb.append(i);
        sb.append(" is less than zero.");
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public static final CharSequence dropLast(final CharSequence charSequence, final int i) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$dropLast");
        if (i >= 0) {
            return take(charSequence, RangesKt___RangesKt.coerceAtLeast(charSequence.length() - i, 0));
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Requested character count ");
        sb.append(i);
        sb.append(" is less than zero.");
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public static final String dropLast(final String s, final int i) {
        Intrinsics.checkParameterIsNotNull(s, "$this$dropLast");
        if (i >= 0) {
            return take(s, RangesKt___RangesKt.coerceAtLeast(s.length() - i, 0));
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Requested character count ");
        sb.append(i);
        sb.append(" is less than zero.");
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public static final CharSequence dropLastWhile(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$dropLastWhile");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        for (int i = StringsKt__StringsKt.getLastIndex(charSequence); i >= 0; --i) {
            if (!function1.invoke(charSequence.charAt(i))) {
                return charSequence.subSequence(0, i + 1);
            }
        }
        return "";
    }
    
    public static final String dropLastWhile(String substring, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(substring, "$this$dropLastWhile");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        for (int i = StringsKt__StringsKt.getLastIndex(substring); i >= 0; --i) {
            if (!function1.invoke(substring.charAt(i))) {
                substring = substring.substring(0, i + 1);
                Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                return substring;
            }
        }
        return "";
    }
    
    public static final CharSequence dropWhile(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$dropWhile");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        for (int length = charSequence.length(), i = 0; i < length; ++i) {
            if (!function1.invoke(charSequence.charAt(i))) {
                return charSequence.subSequence(i, charSequence.length());
            }
        }
        return "";
    }
    
    public static final String dropWhile(String substring, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(substring, "$this$dropWhile");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        for (int length = substring.length(), i = 0; i < length; ++i) {
            if (!function1.invoke(substring.charAt(i))) {
                substring = substring.substring(i);
                Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.String).substring(startIndex)");
                return substring;
            }
        }
        return "";
    }
    
    private static final char elementAtOrElse(final CharSequence charSequence, final int i, final Function1<? super Integer, Character> function1) {
        if (i >= 0 && i <= StringsKt__StringsKt.getLastIndex(charSequence)) {
            return charSequence.charAt(i);
        }
        return function1.invoke(i);
    }
    
    private static final Character elementAtOrNull(final CharSequence charSequence, final int n) {
        return getOrNull(charSequence, n);
    }
    
    public static final CharSequence filter(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$filter");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        final StringBuilder sb = new StringBuilder();
        for (int length = charSequence.length(), i = 0; i < length; ++i) {
            final char char1 = charSequence.charAt(i);
            if (function1.invoke(char1)) {
                sb.append(char1);
            }
        }
        return sb;
    }
    
    public static final String filter(String string, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(string, "$this$filter");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        final String s = string;
        final StringBuilder sb = new StringBuilder();
        for (int length = s.length(), i = 0; i < length; ++i) {
            final char char1 = s.charAt(i);
            if (function1.invoke(char1)) {
                sb.append(char1);
            }
        }
        string = sb.toString();
        Intrinsics.checkExpressionValueIsNotNull(string, "filterTo(StringBuilder(), predicate).toString()");
        return string;
    }
    
    public static final CharSequence filterIndexed(final CharSequence charSequence, final Function2<? super Integer, ? super Character, Boolean> function2) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$filterIndexed");
        Intrinsics.checkParameterIsNotNull(function2, "predicate");
        final StringBuilder sb = new StringBuilder();
        for (int i = 0, j = 0; i < charSequence.length(); ++i, ++j) {
            final char char1 = charSequence.charAt(i);
            if (function2.invoke(j, char1)) {
                sb.append(char1);
            }
        }
        return sb;
    }
    
    public static final String filterIndexed(String string, final Function2<? super Integer, ? super Character, Boolean> function2) {
        Intrinsics.checkParameterIsNotNull(string, "$this$filterIndexed");
        Intrinsics.checkParameterIsNotNull(function2, "predicate");
        final String s = string;
        final StringBuilder sb = new StringBuilder();
        for (int i = 0, j = 0; i < s.length(); ++i, ++j) {
            final char char1 = s.charAt(i);
            if (function2.invoke(j, char1)) {
                sb.append(char1);
            }
        }
        string = sb.toString();
        Intrinsics.checkExpressionValueIsNotNull(string, "filterIndexedTo(StringBu\u2026(), predicate).toString()");
        return string;
    }
    
    public static final <C extends Appendable> C filterIndexedTo(final CharSequence charSequence, final C c, final Function2<? super Integer, ? super Character, Boolean> function2) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$filterIndexedTo");
        Intrinsics.checkParameterIsNotNull(c, "destination");
        Intrinsics.checkParameterIsNotNull(function2, "predicate");
        for (int i = 0, j = 0; i < charSequence.length(); ++i, ++j) {
            final char char1 = charSequence.charAt(i);
            if (function2.invoke(j, char1)) {
                c.append(char1);
            }
        }
        return c;
    }
    
    public static final CharSequence filterNot(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$filterNot");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < charSequence.length(); ++i) {
            final char char1 = charSequence.charAt(i);
            if (!function1.invoke(char1)) {
                sb.append(char1);
            }
        }
        return sb;
    }
    
    public static final String filterNot(String string, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(string, "$this$filterNot");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        final String s = string;
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); ++i) {
            final char char1 = s.charAt(i);
            if (!function1.invoke(char1)) {
                sb.append(char1);
            }
        }
        string = sb.toString();
        Intrinsics.checkExpressionValueIsNotNull(string, "filterNotTo(StringBuilder(), predicate).toString()");
        return string;
    }
    
    public static final <C extends Appendable> C filterNotTo(final CharSequence charSequence, final C c, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$filterNotTo");
        Intrinsics.checkParameterIsNotNull(c, "destination");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        for (int i = 0; i < charSequence.length(); ++i) {
            final char char1 = charSequence.charAt(i);
            if (!function1.invoke(char1)) {
                c.append(char1);
            }
        }
        return c;
    }
    
    public static final <C extends Appendable> C filterTo(final CharSequence charSequence, final C c, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$filterTo");
        Intrinsics.checkParameterIsNotNull(c, "destination");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        for (int length = charSequence.length(), i = 0; i < length; ++i) {
            final char char1 = charSequence.charAt(i);
            if (function1.invoke(char1)) {
                c.append(char1);
            }
        }
        return c;
    }
    
    private static final Character find(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        for (int i = 0; i < charSequence.length(); ++i) {
            final char char1 = charSequence.charAt(i);
            if (function1.invoke(char1)) {
                return char1;
            }
        }
        return null;
    }
    
    private static final Character findLast(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        int length = charSequence.length();
        char char1;
        do {
            --length;
            if (length < 0) {
                return null;
            }
            char1 = charSequence.charAt(length);
        } while (!function1.invoke(char1));
        return char1;
    }
    
    public static final char first(final CharSequence charSequence) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$first");
        if (charSequence.length() != 0) {
            return charSequence.charAt(0);
        }
        throw new NoSuchElementException("Char sequence is empty.");
    }
    
    public static final char first(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$first");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        for (int i = 0; i < charSequence.length(); ++i) {
            final char char1 = charSequence.charAt(i);
            if (function1.invoke(char1)) {
                return char1;
            }
        }
        throw new NoSuchElementException("Char sequence contains no character matching the predicate.");
    }
    
    public static final Character firstOrNull(final CharSequence charSequence) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$firstOrNull");
        if (charSequence.length() == 0) {
            return null;
        }
        return charSequence.charAt(0);
    }
    
    public static final Character firstOrNull(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$firstOrNull");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        for (int i = 0; i < charSequence.length(); ++i) {
            final char char1 = charSequence.charAt(i);
            if (function1.invoke(char1)) {
                return char1;
            }
        }
        return null;
    }
    
    public static final <R> List<R> flatMap(final CharSequence charSequence, final Function1<? super Character, ? extends Iterable<? extends R>> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$flatMap");
        Intrinsics.checkParameterIsNotNull(function1, "transform");
        final ArrayList<Object> list = new ArrayList<Object>();
        for (int i = 0; i < charSequence.length(); ++i) {
            CollectionsKt__MutableCollectionsKt.addAll((Collection<? super Object>)list, (Iterable<?>)function1.invoke(charSequence.charAt(i)));
        }
        return (List<R>)list;
    }
    
    public static final <R, C extends Collection<? super R>> C flatMapTo(final CharSequence charSequence, final C c, final Function1<? super Character, ? extends Iterable<? extends R>> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$flatMapTo");
        Intrinsics.checkParameterIsNotNull(c, "destination");
        Intrinsics.checkParameterIsNotNull(function1, "transform");
        for (int i = 0; i < charSequence.length(); ++i) {
            CollectionsKt__MutableCollectionsKt.addAll((Collection<? super Object>)c, (Iterable<?>)function1.invoke(charSequence.charAt(i)));
        }
        return c;
    }
    
    public static final <R> R fold(final CharSequence charSequence, R invoke, final Function2<? super R, ? super Character, ? extends R> function2) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$fold");
        Intrinsics.checkParameterIsNotNull(function2, "operation");
        for (int i = 0; i < charSequence.length(); ++i) {
            invoke = (R)function2.invoke(invoke, charSequence.charAt(i));
        }
        return invoke;
    }
    
    public static final <R> R foldIndexed(final CharSequence charSequence, R invoke, final Function3<? super Integer, ? super R, ? super Character, ? extends R> function3) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$foldIndexed");
        Intrinsics.checkParameterIsNotNull(function3, "operation");
        int n = 0;
        int n2 = 0;
        while (true) {
            final int i = n2;
            if (n >= charSequence.length()) {
                break;
            }
            final char char1 = charSequence.charAt(n);
            n2 = i + 1;
            invoke = (R)function3.invoke(i, invoke, char1);
            ++n;
        }
        return invoke;
    }
    
    public static final <R> R foldRight(final CharSequence charSequence, R invoke, final Function2<? super Character, ? super R, ? extends R> function2) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$foldRight");
        Intrinsics.checkParameterIsNotNull(function2, "operation");
        for (int i = StringsKt__StringsKt.getLastIndex(charSequence); i >= 0; --i) {
            invoke = (R)function2.invoke(charSequence.charAt(i), invoke);
        }
        return invoke;
    }
    
    public static final <R> R foldRightIndexed(final CharSequence charSequence, R invoke, final Function3<? super Integer, ? super Character, ? super R, ? extends R> function3) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$foldRightIndexed");
        Intrinsics.checkParameterIsNotNull(function3, "operation");
        for (int i = StringsKt__StringsKt.getLastIndex(charSequence); i >= 0; --i) {
            invoke = (R)function3.invoke(i, charSequence.charAt(i), invoke);
        }
        return invoke;
    }
    
    public static final void forEach(final CharSequence charSequence, final Function1<? super Character, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$forEach");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        for (int i = 0; i < charSequence.length(); ++i) {
            function1.invoke(charSequence.charAt(i));
        }
    }
    
    public static final void forEachIndexed(final CharSequence charSequence, final Function2<? super Integer, ? super Character, Unit> function2) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$forEachIndexed");
        Intrinsics.checkParameterIsNotNull(function2, "action");
        int n = 0;
        int n2 = 0;
        while (true) {
            final int i = n2;
            if (n >= charSequence.length()) {
                break;
            }
            final char char1 = charSequence.charAt(n);
            n2 = i + 1;
            function2.invoke(i, char1);
            ++n;
        }
    }
    
    private static final char getOrElse(final CharSequence charSequence, final int i, final Function1<? super Integer, Character> function1) {
        if (i >= 0 && i <= StringsKt__StringsKt.getLastIndex(charSequence)) {
            return charSequence.charAt(i);
        }
        return function1.invoke(i);
    }
    
    public static final Character getOrNull(final CharSequence charSequence, final int n) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$getOrNull");
        if (n >= 0 && n <= StringsKt__StringsKt.getLastIndex(charSequence)) {
            return charSequence.charAt(n);
        }
        return null;
    }
    
    public static final <K> Map<K, List<Character>> groupBy(final CharSequence charSequence, final Function1<? super Character, ? extends K> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$groupBy");
        Intrinsics.checkParameterIsNotNull(function1, "keySelector");
        final LinkedHashMap<K, List<Character>> linkedHashMap = (LinkedHashMap<K, List<Character>>)new LinkedHashMap<Object, List<Character>>();
        for (int i = 0; i < charSequence.length(); ++i) {
            final char char1 = charSequence.charAt(i);
            final K invoke = (K)function1.invoke(char1);
            Object value;
            if ((value = linkedHashMap.get(invoke)) == null) {
                value = new ArrayList<Character>();
                linkedHashMap.put(invoke, (List<Character>)value);
            }
            ((List<Character>)value).add(char1);
        }
        return linkedHashMap;
    }
    
    public static final <K, V> Map<K, List<V>> groupBy(final CharSequence charSequence, final Function1<? super Character, ? extends K> function1, final Function1<? super Character, ? extends V> function2) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$groupBy");
        Intrinsics.checkParameterIsNotNull(function1, "keySelector");
        Intrinsics.checkParameterIsNotNull(function2, "valueTransform");
        final LinkedHashMap<K, List<Object>> linkedHashMap = (LinkedHashMap<K, List<Object>>)new LinkedHashMap<Object, List<Object>>();
        for (int i = 0; i < charSequence.length(); ++i) {
            final char char1 = charSequence.charAt(i);
            final K invoke = (K)function1.invoke(char1);
            Object value;
            if ((value = linkedHashMap.get(invoke)) == null) {
                value = new ArrayList<Object>();
                linkedHashMap.put(invoke, (List<Object>)value);
            }
            ((List<Object>)value).add(function2.invoke(char1));
        }
        return (Map<K, List<V>>)linkedHashMap;
    }
    
    public static final <K, M extends Map<? super K, List<Character>>> M groupByTo(final CharSequence charSequence, final M m, final Function1<? super Character, ? extends K> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$groupByTo");
        Intrinsics.checkParameterIsNotNull(m, "destination");
        Intrinsics.checkParameterIsNotNull(function1, "keySelector");
        for (int i = 0; i < charSequence.length(); ++i) {
            final char char1 = charSequence.charAt(i);
            final K invoke = (K)function1.invoke(char1);
            List<Character> value;
            if ((value = ((Map<K, List<Character>>)m).get(invoke)) == null) {
                value = new ArrayList<Character>();
                m.put((Object)invoke, value);
            }
            value.add(char1);
        }
        return m;
    }
    
    public static final <K, V, M extends Map<? super K, List<V>>> M groupByTo(final CharSequence charSequence, final M m, final Function1<? super Character, ? extends K> function1, final Function1<? super Character, ? extends V> function2) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$groupByTo");
        Intrinsics.checkParameterIsNotNull(m, "destination");
        Intrinsics.checkParameterIsNotNull(function1, "keySelector");
        Intrinsics.checkParameterIsNotNull(function2, "valueTransform");
        for (int i = 0; i < charSequence.length(); ++i) {
            final char char1 = charSequence.charAt(i);
            final K invoke = (K)function1.invoke(char1);
            List<V> value;
            if ((value = ((Map<K, List<V>>)m).get(invoke)) == null) {
                value = new ArrayList<V>();
                m.put((Object)invoke, value);
            }
            ((List<Object>)value).add(function2.invoke(char1));
        }
        return m;
    }
    
    public static final <K> Grouping<Character, K> groupingBy(final CharSequence charSequence, final Function1<? super Character, ? extends K> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$groupingBy");
        Intrinsics.checkParameterIsNotNull(function1, "keySelector");
        return (Grouping<Character, K>)new StringsKt___StringsKt$groupingBy.StringsKt___StringsKt$groupingBy$1(charSequence, (Function1)function1);
    }
    
    public static final int indexOfFirst(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$indexOfFirst");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        for (int length = charSequence.length(), i = 0; i < length; ++i) {
            if (function1.invoke(charSequence.charAt(i))) {
                return i;
            }
        }
        return -1;
    }
    
    public static final int indexOfLast(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$indexOfLast");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        for (int i = charSequence.length() - 1; i >= 0; --i) {
            if (function1.invoke(charSequence.charAt(i))) {
                return i;
            }
        }
        return -1;
    }
    
    public static final char last(final CharSequence charSequence) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$last");
        if (charSequence.length() != 0) {
            return charSequence.charAt(StringsKt__StringsKt.getLastIndex(charSequence));
        }
        throw new NoSuchElementException("Char sequence is empty.");
    }
    
    public static final char last(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$last");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        int length = charSequence.length();
        char char1;
        do {
            --length;
            if (length < 0) {
                throw new NoSuchElementException("Char sequence contains no character matching the predicate.");
            }
            char1 = charSequence.charAt(length);
        } while (!function1.invoke(char1));
        return char1;
    }
    
    public static final Character lastOrNull(final CharSequence charSequence) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$lastOrNull");
        if (charSequence.length() == 0) {
            return null;
        }
        return charSequence.charAt(charSequence.length() - 1);
    }
    
    public static final Character lastOrNull(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$lastOrNull");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        int length = charSequence.length();
        char char1;
        do {
            --length;
            if (length < 0) {
                return null;
            }
            char1 = charSequence.charAt(length);
        } while (!function1.invoke(char1));
        return char1;
    }
    
    public static final <R> List<R> map(final CharSequence charSequence, final Function1<? super Character, ? extends R> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$map");
        Intrinsics.checkParameterIsNotNull(function1, "transform");
        final ArrayList<Object> list = new ArrayList<Object>(charSequence.length());
        for (int i = 0; i < charSequence.length(); ++i) {
            list.add(function1.invoke(charSequence.charAt(i)));
        }
        return (List<R>)list;
    }
    
    public static final <R> List<R> mapIndexed(final CharSequence charSequence, final Function2<? super Integer, ? super Character, ? extends R> function2) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$mapIndexed");
        Intrinsics.checkParameterIsNotNull(function2, "transform");
        final ArrayList<Object> list = new ArrayList<Object>(charSequence.length());
        int n = 0;
        int n2 = 0;
        while (true) {
            final int i = n2;
            if (n >= charSequence.length()) {
                break;
            }
            final char char1 = charSequence.charAt(n);
            n2 = i + 1;
            list.add(function2.invoke(i, char1));
            ++n;
        }
        return (List<R>)list;
    }
    
    public static final <R> List<R> mapIndexedNotNull(final CharSequence charSequence, final Function2<? super Integer, ? super Character, ? extends R> function2) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$mapIndexedNotNull");
        Intrinsics.checkParameterIsNotNull(function2, "transform");
        final ArrayList<Object> list = new ArrayList<Object>();
        for (int i = 0, j = 0; i < charSequence.length(); ++i, ++j) {
            final R invoke = (R)function2.invoke(j, charSequence.charAt(i));
            if (invoke != null) {
                list.add(invoke);
            }
        }
        return (List<R>)list;
    }
    
    public static final <R, C extends Collection<? super R>> C mapIndexedNotNullTo(final CharSequence charSequence, final C c, final Function2<? super Integer, ? super Character, ? extends R> function2) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$mapIndexedNotNullTo");
        Intrinsics.checkParameterIsNotNull(c, "destination");
        Intrinsics.checkParameterIsNotNull(function2, "transform");
        for (int i = 0, j = 0; i < charSequence.length(); ++i, ++j) {
            final R invoke = (R)function2.invoke(j, charSequence.charAt(i));
            if (invoke != null) {
                c.add((Object)invoke);
            }
        }
        return c;
    }
    
    public static final <R, C extends Collection<? super R>> C mapIndexedTo(final CharSequence charSequence, final C c, final Function2<? super Integer, ? super Character, ? extends R> function2) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$mapIndexedTo");
        Intrinsics.checkParameterIsNotNull(c, "destination");
        Intrinsics.checkParameterIsNotNull(function2, "transform");
        int n = 0;
        int n2 = 0;
        while (true) {
            final int i = n2;
            if (n >= charSequence.length()) {
                break;
            }
            final char char1 = charSequence.charAt(n);
            n2 = i + 1;
            c.add((Object)function2.invoke(i, char1));
            ++n;
        }
        return c;
    }
    
    public static final <R> List<R> mapNotNull(final CharSequence charSequence, final Function1<? super Character, ? extends R> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$mapNotNull");
        Intrinsics.checkParameterIsNotNull(function1, "transform");
        final ArrayList<Object> list = new ArrayList<Object>();
        for (int i = 0; i < charSequence.length(); ++i) {
            final R invoke = (R)function1.invoke(charSequence.charAt(i));
            if (invoke != null) {
                list.add(invoke);
            }
        }
        return (List<R>)list;
    }
    
    public static final <R, C extends Collection<? super R>> C mapNotNullTo(final CharSequence charSequence, final C c, final Function1<? super Character, ? extends R> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$mapNotNullTo");
        Intrinsics.checkParameterIsNotNull(c, "destination");
        Intrinsics.checkParameterIsNotNull(function1, "transform");
        for (int i = 0; i < charSequence.length(); ++i) {
            final R invoke = (R)function1.invoke(charSequence.charAt(i));
            if (invoke != null) {
                c.add((Object)invoke);
            }
        }
        return c;
    }
    
    public static final <R, C extends Collection<? super R>> C mapTo(final CharSequence charSequence, final C c, final Function1<? super Character, ? extends R> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$mapTo");
        Intrinsics.checkParameterIsNotNull(c, "destination");
        Intrinsics.checkParameterIsNotNull(function1, "transform");
        for (int i = 0; i < charSequence.length(); ++i) {
            c.add((Object)function1.invoke(charSequence.charAt(i)));
        }
        return c;
    }
    
    public static final Character max(final CharSequence charSequence) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$max");
        final int length = charSequence.length();
        final int n = 1;
        if (length == 0) {
            return null;
        }
        final char char1 = charSequence.charAt(0);
        final int lastIndex = StringsKt__StringsKt.getLastIndex(charSequence);
        char c = char1;
        if (1 <= lastIndex) {
            int n2 = n;
            char c2 = char1;
            while (true) {
                final char char2 = charSequence.charAt(n2);
                char c3 = c2;
                if (c2 < char2) {
                    c3 = char2;
                }
                c = c3;
                if (n2 == lastIndex) {
                    break;
                }
                ++n2;
                c2 = c3;
            }
        }
        return c;
    }
    
    public static final <R extends Comparable<? super R>> Character maxBy(final CharSequence charSequence, final Function1<? super Character, ? extends R> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$maxBy");
        Intrinsics.checkParameterIsNotNull(function1, "selector");
        final int length = charSequence.length();
        final int n = 1;
        if (length == 0) {
            return null;
        }
        char char1 = charSequence.charAt(0);
        final int lastIndex = StringsKt__StringsKt.getLastIndex(charSequence);
        if (lastIndex == 0) {
            return char1;
        }
        Comparable<Comparable<Comparable>> comparable = (Comparable<Comparable<Comparable>>)function1.invoke(char1);
        char c = char1;
        if (1 <= lastIndex) {
            int n2 = n;
            while (true) {
                final char char2 = charSequence.charAt(n2);
                final Comparable comparable2 = (Comparable)function1.invoke(char2);
                Object o = comparable;
                if (comparable.compareTo(comparable2) < 0) {
                    char1 = char2;
                    o = comparable2;
                }
                c = char1;
                if (n2 == lastIndex) {
                    break;
                }
                ++n2;
                comparable = (Comparable<Comparable<Comparable>>)o;
            }
        }
        return c;
    }
    
    public static final Character maxWith(final CharSequence charSequence, final Comparator<? super Character> comparator) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$maxWith");
        Intrinsics.checkParameterIsNotNull(comparator, "comparator");
        final int length = charSequence.length();
        final int n = 1;
        if (length == 0) {
            return null;
        }
        final char char1 = charSequence.charAt(0);
        final int lastIndex = StringsKt__StringsKt.getLastIndex(charSequence);
        char c = char1;
        if (1 <= lastIndex) {
            int n2 = n;
            char c2 = char1;
            while (true) {
                final char char2 = charSequence.charAt(n2);
                char c3 = c2;
                if (comparator.compare(c2, char2) < 0) {
                    c3 = char2;
                }
                c = c3;
                if (n2 == lastIndex) {
                    break;
                }
                ++n2;
                c2 = c3;
            }
        }
        return c;
    }
    
    public static final Character min(final CharSequence charSequence) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$min");
        final int length = charSequence.length();
        final int n = 1;
        if (length == 0) {
            return null;
        }
        final char char1 = charSequence.charAt(0);
        final int lastIndex = StringsKt__StringsKt.getLastIndex(charSequence);
        char c = char1;
        if (1 <= lastIndex) {
            int n2 = n;
            char c2 = char1;
            while (true) {
                final char char2 = charSequence.charAt(n2);
                char c3 = c2;
                if (c2 > char2) {
                    c3 = char2;
                }
                c = c3;
                if (n2 == lastIndex) {
                    break;
                }
                ++n2;
                c2 = c3;
            }
        }
        return c;
    }
    
    public static final <R extends Comparable<? super R>> Character minBy(final CharSequence charSequence, final Function1<? super Character, ? extends R> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$minBy");
        Intrinsics.checkParameterIsNotNull(function1, "selector");
        final int length = charSequence.length();
        final int n = 1;
        if (length == 0) {
            return null;
        }
        char char1 = charSequence.charAt(0);
        final int lastIndex = StringsKt__StringsKt.getLastIndex(charSequence);
        if (lastIndex == 0) {
            return char1;
        }
        Comparable<Comparable<Comparable>> comparable = (Comparable<Comparable<Comparable>>)function1.invoke(char1);
        char c = char1;
        if (1 <= lastIndex) {
            int n2 = n;
            while (true) {
                final char char2 = charSequence.charAt(n2);
                final Comparable comparable2 = (Comparable)function1.invoke(char2);
                Object o = comparable;
                if (comparable.compareTo(comparable2) > 0) {
                    char1 = char2;
                    o = comparable2;
                }
                c = char1;
                if (n2 == lastIndex) {
                    break;
                }
                ++n2;
                comparable = (Comparable<Comparable<Comparable>>)o;
            }
        }
        return c;
    }
    
    public static final Character minWith(final CharSequence charSequence, final Comparator<? super Character> comparator) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$minWith");
        Intrinsics.checkParameterIsNotNull(comparator, "comparator");
        final int length = charSequence.length();
        final int n = 1;
        if (length == 0) {
            return null;
        }
        final char char1 = charSequence.charAt(0);
        final int lastIndex = StringsKt__StringsKt.getLastIndex(charSequence);
        char c = char1;
        if (1 <= lastIndex) {
            int n2 = n;
            char c2 = char1;
            while (true) {
                final char char2 = charSequence.charAt(n2);
                char c3 = c2;
                if (comparator.compare(c2, char2) > 0) {
                    c3 = char2;
                }
                c = c3;
                if (n2 == lastIndex) {
                    break;
                }
                ++n2;
                c2 = c3;
            }
        }
        return c;
    }
    
    public static final boolean none(final CharSequence charSequence) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$none");
        return charSequence.length() == 0;
    }
    
    public static final boolean none(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$none");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        for (int i = 0; i < charSequence.length(); ++i) {
            if (function1.invoke(charSequence.charAt(i))) {
                return false;
            }
        }
        return true;
    }
    
    public static final <S extends CharSequence> S onEach(final S n, final Function1<? super Character, Unit> function1) {
        Intrinsics.checkParameterIsNotNull(n, "$this$onEach");
        Intrinsics.checkParameterIsNotNull(function1, "action");
        for (int i = 0; i < n.length(); ++i) {
            function1.invoke(n.charAt(i));
        }
        return n;
    }
    
    public static final Pair<CharSequence, CharSequence> partition(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$partition");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        final StringBuilder sb = new StringBuilder();
        final StringBuilder sb2 = new StringBuilder();
        for (int i = 0; i < charSequence.length(); ++i) {
            final char char1 = charSequence.charAt(i);
            if (function1.invoke(char1)) {
                sb.append(char1);
            }
            else {
                sb2.append(char1);
            }
        }
        return new Pair<CharSequence, CharSequence>(sb, sb2);
    }
    
    public static final Pair<String, String> partition(final String s, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(s, "$this$partition");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        final StringBuilder sb = new StringBuilder();
        final StringBuilder sb2 = new StringBuilder();
        for (int length = s.length(), i = 0; i < length; ++i) {
            final char char1 = s.charAt(i);
            if (function1.invoke(char1)) {
                sb.append(char1);
            }
            else {
                sb2.append(char1);
            }
        }
        return new Pair<String, String>(sb.toString(), sb2.toString());
    }
    
    private static final char random(final CharSequence charSequence) {
        return random(charSequence, Random.Default);
    }
    
    public static final char random(final CharSequence charSequence, final Random random) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$random");
        Intrinsics.checkParameterIsNotNull(random, "random");
        if (charSequence.length() != 0) {
            return charSequence.charAt(random.nextInt(charSequence.length()));
        }
        throw new NoSuchElementException("Char sequence is empty.");
    }
    
    public static final char reduce(final CharSequence charSequence, final Function2<? super Character, ? super Character, Character> function2) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$reduce");
        Intrinsics.checkParameterIsNotNull(function2, "operation");
        final int length = charSequence.length();
        final int n = 1;
        if (length != 0) {
            char char1 = charSequence.charAt(0);
            final int lastIndex = StringsKt__StringsKt.getLastIndex(charSequence);
            char charValue = char1;
            if (1 <= lastIndex) {
                int n2 = n;
                while (true) {
                    char1 = (charValue = function2.invoke(char1, charSequence.charAt(n2)));
                    if (n2 == lastIndex) {
                        break;
                    }
                    ++n2;
                }
            }
            return charValue;
        }
        throw new UnsupportedOperationException("Empty char sequence can't be reduced.");
    }
    
    public static final char reduceIndexed(final CharSequence charSequence, final Function3<? super Integer, ? super Character, ? super Character, Character> function3) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$reduceIndexed");
        Intrinsics.checkParameterIsNotNull(function3, "operation");
        final int length = charSequence.length();
        final int n = 1;
        if (length != 0) {
            char char1 = charSequence.charAt(0);
            final int lastIndex = StringsKt__StringsKt.getLastIndex(charSequence);
            char charValue = char1;
            if (1 <= lastIndex) {
                int i = n;
                while (true) {
                    char1 = (charValue = function3.invoke(i, char1, charSequence.charAt(i)));
                    if (i == lastIndex) {
                        break;
                    }
                    ++i;
                }
            }
            return charValue;
        }
        throw new UnsupportedOperationException("Empty char sequence can't be reduced.");
    }
    
    public static final char reduceRight(final CharSequence charSequence, final Function2<? super Character, ? super Character, Character> function2) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$reduceRight");
        Intrinsics.checkParameterIsNotNull(function2, "operation");
        final int lastIndex = StringsKt__StringsKt.getLastIndex(charSequence);
        if (lastIndex >= 0) {
            int i = lastIndex - 1;
            char c = charSequence.charAt(lastIndex);
            while (i >= 0) {
                c = function2.invoke(charSequence.charAt(i), c);
                --i;
            }
            return c;
        }
        throw new UnsupportedOperationException("Empty char sequence can't be reduced.");
    }
    
    public static final char reduceRightIndexed(final CharSequence charSequence, final Function3<? super Integer, ? super Character, ? super Character, Character> function3) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$reduceRightIndexed");
        Intrinsics.checkParameterIsNotNull(function3, "operation");
        final int lastIndex = StringsKt__StringsKt.getLastIndex(charSequence);
        if (lastIndex >= 0) {
            int i = lastIndex - 1;
            char c = charSequence.charAt(lastIndex);
            while (i >= 0) {
                c = function3.invoke(i, charSequence.charAt(i), c);
                --i;
            }
            return c;
        }
        throw new UnsupportedOperationException("Empty char sequence can't be reduced.");
    }
    
    public static final CharSequence reversed(final CharSequence seq) {
        Intrinsics.checkParameterIsNotNull(seq, "$this$reversed");
        final StringBuilder reverse = new StringBuilder(seq).reverse();
        Intrinsics.checkExpressionValueIsNotNull(reverse, "StringBuilder(this).reverse()");
        return reverse;
    }
    
    private static final String reversed(final String s) {
        if (s != null) {
            return reversed((CharSequence)s).toString();
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
    }
    
    public static final char single(final CharSequence charSequence) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$single");
        final int length = charSequence.length();
        if (length == 0) {
            throw new NoSuchElementException("Char sequence is empty.");
        }
        if (length == 1) {
            return charSequence.charAt(0);
        }
        throw new IllegalArgumentException("Char sequence has more than one element.");
    }
    
    public static final char single(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$single");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        Character value = null;
        int i = 0;
        int n = 0;
        while (i < charSequence.length()) {
            final char char1 = charSequence.charAt(i);
            int n2 = n;
            if (function1.invoke(char1)) {
                if (n != 0) {
                    throw new IllegalArgumentException("Char sequence contains more than one matching element.");
                }
                value = char1;
                n2 = 1;
            }
            ++i;
            n = n2;
        }
        if (n == 0) {
            throw new NoSuchElementException("Char sequence contains no character matching the predicate.");
        }
        if (value != null) {
            return value;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Char");
    }
    
    public static final Character singleOrNull(final CharSequence charSequence) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$singleOrNull");
        if (charSequence.length() == 1) {
            return charSequence.charAt(0);
        }
        return null;
    }
    
    public static final Character singleOrNull(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$singleOrNull");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        Character value = null;
        int i = 0;
        int n = 0;
        while (i < charSequence.length()) {
            final char char1 = charSequence.charAt(i);
            int n2 = n;
            if (function1.invoke(char1)) {
                if (n != 0) {
                    return null;
                }
                value = char1;
                n2 = 1;
            }
            ++i;
            n = n2;
        }
        if (n == 0) {
            return null;
        }
        return value;
    }
    
    public static final CharSequence slice(final CharSequence charSequence, final Iterable<Integer> iterable) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$slice");
        Intrinsics.checkParameterIsNotNull(iterable, "indices");
        final int collectionSizeOrDefault = CollectionsKt__IterablesKt.collectionSizeOrDefault((Iterable<?>)iterable, 10);
        if (collectionSizeOrDefault == 0) {
            return "";
        }
        final StringBuilder sb = new StringBuilder(collectionSizeOrDefault);
        final Iterator<? extends T> iterator = iterable.iterator();
        while (iterator.hasNext()) {
            sb.append(charSequence.charAt(((Number)iterator.next()).intValue()));
        }
        return sb;
    }
    
    public static final CharSequence slice(final CharSequence charSequence, final IntRange intRange) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$slice");
        Intrinsics.checkParameterIsNotNull(intRange, "indices");
        if (intRange.isEmpty()) {
            return "";
        }
        return StringsKt__StringsKt.subSequence(charSequence, intRange);
    }
    
    private static final String slice(final String s, final Iterable<Integer> iterable) {
        if (s != null) {
            return slice((CharSequence)s, iterable).toString();
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
    }
    
    public static final String slice(final String s, final IntRange intRange) {
        Intrinsics.checkParameterIsNotNull(s, "$this$slice");
        Intrinsics.checkParameterIsNotNull(intRange, "indices");
        if (intRange.isEmpty()) {
            return "";
        }
        return StringsKt__StringsKt.substring(s, intRange);
    }
    
    public static final int sumBy(final CharSequence charSequence, final Function1<? super Character, Integer> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$sumBy");
        Intrinsics.checkParameterIsNotNull(function1, "selector");
        int i = 0;
        int n = 0;
        while (i < charSequence.length()) {
            n += function1.invoke(charSequence.charAt(i)).intValue();
            ++i;
        }
        return n;
    }
    
    public static final double sumByDouble(final CharSequence charSequence, final Function1<? super Character, Double> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$sumByDouble");
        Intrinsics.checkParameterIsNotNull(function1, "selector");
        double n = 0.0;
        for (int i = 0; i < charSequence.length(); ++i) {
            n += function1.invoke(charSequence.charAt(i)).doubleValue();
        }
        return n;
    }
    
    public static final CharSequence take(final CharSequence charSequence, final int i) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$take");
        if (i >= 0) {
            return charSequence.subSequence(0, RangesKt___RangesKt.coerceAtMost(i, charSequence.length()));
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Requested character count ");
        sb.append(i);
        sb.append(" is less than zero.");
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public static final String take(String substring, final int i) {
        Intrinsics.checkParameterIsNotNull(substring, "$this$take");
        if (i >= 0) {
            substring = substring.substring(0, RangesKt___RangesKt.coerceAtMost(i, substring.length()));
            Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
            return substring;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Requested character count ");
        sb.append(i);
        sb.append(" is less than zero.");
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public static final CharSequence takeLast(final CharSequence charSequence, final int i) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$takeLast");
        if (i >= 0) {
            final int length = charSequence.length();
            return charSequence.subSequence(length - RangesKt___RangesKt.coerceAtMost(i, length), length);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Requested character count ");
        sb.append(i);
        sb.append(" is less than zero.");
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public static final String takeLast(String substring, final int i) {
        Intrinsics.checkParameterIsNotNull(substring, "$this$takeLast");
        if (i >= 0) {
            final int length = substring.length();
            substring = substring.substring(length - RangesKt___RangesKt.coerceAtMost(i, length));
            Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.String).substring(startIndex)");
            return substring;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Requested character count ");
        sb.append(i);
        sb.append(" is less than zero.");
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    public static final CharSequence takeLastWhile(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$takeLastWhile");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        for (int i = StringsKt__StringsKt.getLastIndex(charSequence); i >= 0; --i) {
            if (!function1.invoke(charSequence.charAt(i))) {
                return charSequence.subSequence(i + 1, charSequence.length());
            }
        }
        return charSequence.subSequence(0, charSequence.length());
    }
    
    public static final String takeLastWhile(String substring, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(substring, "$this$takeLastWhile");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        for (int i = StringsKt__StringsKt.getLastIndex(substring); i >= 0; --i) {
            if (!function1.invoke(substring.charAt(i))) {
                substring = substring.substring(i + 1);
                Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.String).substring(startIndex)");
                return substring;
            }
        }
        return substring;
    }
    
    public static final CharSequence takeWhile(final CharSequence charSequence, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$takeWhile");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        for (int length = charSequence.length(), i = 0; i < length; ++i) {
            if (!function1.invoke(charSequence.charAt(i))) {
                return charSequence.subSequence(0, i);
            }
        }
        return charSequence.subSequence(0, charSequence.length());
    }
    
    public static final String takeWhile(String substring, final Function1<? super Character, Boolean> function1) {
        Intrinsics.checkParameterIsNotNull(substring, "$this$takeWhile");
        Intrinsics.checkParameterIsNotNull(function1, "predicate");
        for (int length = substring.length(), i = 0; i < length; ++i) {
            if (!function1.invoke(substring.charAt(i))) {
                substring = substring.substring(0, i);
                Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                return substring;
            }
        }
        return substring;
    }
    
    public static final <C extends Collection<? super Character>> C toCollection(final CharSequence charSequence, final C c) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$toCollection");
        Intrinsics.checkParameterIsNotNull(c, "destination");
        for (int i = 0; i < charSequence.length(); ++i) {
            ((Collection<Character>)c).add(charSequence.charAt(i));
        }
        return c;
    }
    
    public static final HashSet<Character> toHashSet(final CharSequence charSequence) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$toHashSet");
        return toCollection(charSequence, (HashSet<Character>)new HashSet<Object>(MapsKt__MapsKt.mapCapacity(charSequence.length())));
    }
    
    public static final List<Character> toList(final CharSequence charSequence) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$toList");
        final int length = charSequence.length();
        if (length == 0) {
            return CollectionsKt__CollectionsKt.emptyList();
        }
        if (length != 1) {
            return toMutableList(charSequence);
        }
        return CollectionsKt__CollectionsJVMKt.listOf(charSequence.charAt(0));
    }
    
    public static final List<Character> toMutableList(final CharSequence charSequence) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$toMutableList");
        return toCollection(charSequence, (List<Character>)new ArrayList<Object>(charSequence.length()));
    }
    
    public static final Set<Character> toSet(final CharSequence charSequence) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$toSet");
        final int length = charSequence.length();
        if (length == 0) {
            return SetsKt__SetsKt.emptySet();
        }
        if (length != 1) {
            return toCollection(charSequence, (Set<Character>)new LinkedHashSet<Object>(MapsKt__MapsKt.mapCapacity(charSequence.length())));
        }
        return SetsKt__SetsJVMKt.setOf(charSequence.charAt(0));
    }
    
    public static final List<String> windowed(final CharSequence charSequence, final int n, final int n2, final boolean b) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$windowed");
        return windowed(charSequence, n, n2, b, (Function1<? super CharSequence, ? extends String>)StringsKt___StringsKt$windowed.StringsKt___StringsKt$windowed$1.INSTANCE);
    }
    
    public static final <R> List<R> windowed(final CharSequence charSequence, final int n, final int n2, final boolean b, final Function1<? super CharSequence, ? extends R> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$windowed");
        Intrinsics.checkParameterIsNotNull(function1, "transform");
        SlidingWindowKt.checkWindowSizeStep(n, n2);
        final int length = charSequence.length();
        final ArrayList list = new ArrayList<R>((length + n2 - 1) / n2);
        for (int i = 0; i < length; i += n2) {
            int n3;
            if ((n3 = i + n) > length) {
                if (!b) {
                    break;
                }
                n3 = length;
            }
            list.add(function1.invoke(charSequence.subSequence(i, n3)));
        }
        return (ArrayList<R>)list;
    }
    
    public static final Sequence<String> windowedSequence(final CharSequence charSequence, final int n, final int n2, final boolean b) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$windowedSequence");
        return windowedSequence(charSequence, n, n2, b, (Function1<? super CharSequence, ? extends String>)StringsKt___StringsKt$windowedSequence.StringsKt___StringsKt$windowedSequence$1.INSTANCE);
    }
    
    public static final <R> Sequence<R> windowedSequence(final CharSequence charSequence, final int n, final int n2, final boolean b, final Function1<? super CharSequence, ? extends R> function1) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$windowedSequence");
        Intrinsics.checkParameterIsNotNull(function1, "transform");
        SlidingWindowKt.checkWindowSizeStep(n, n2);
        IntRange intRange;
        if (b) {
            intRange = StringsKt__StringsKt.getIndices(charSequence);
        }
        else {
            intRange = RangesKt___RangesKt.until(0, charSequence.length() - n + 1);
        }
        return (Sequence<R>)SequencesKt___SequencesKt.map((Sequence<?>)CollectionsKt___CollectionsKt.asSequence((Iterable<?>)RangesKt___RangesKt.step(intRange, n2)), (Function1<? super Object, ?>)new StringsKt___StringsKt$windowedSequence.StringsKt___StringsKt$windowedSequence$2(charSequence, (Function1)function1, n));
    }
    
    public static final Iterable<IndexedValue<Character>> withIndex(final CharSequence charSequence) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$withIndex");
        return (Iterable<IndexedValue<Character>>)new IndexingIterable((Function0<? extends Iterator<?>>)new StringsKt___StringsKt$withIndex.StringsKt___StringsKt$withIndex$1(charSequence));
    }
    
    public static final List<Pair<Character, Character>> zip(final CharSequence charSequence, final CharSequence charSequence2) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$zip");
        Intrinsics.checkParameterIsNotNull(charSequence2, "other");
        final int min = Math.min(charSequence.length(), charSequence2.length());
        final ArrayList list = new ArrayList<Pair<Character, Character>>(min);
        for (int i = 0; i < min; ++i) {
            list.add(TuplesKt.to(charSequence.charAt(i), charSequence2.charAt(i)));
        }
        return (ArrayList<Pair<Character, Character>>)list;
    }
    
    public static final <V> List<V> zip(final CharSequence charSequence, final CharSequence charSequence2, final Function2<? super Character, ? super Character, ? extends V> function2) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$zip");
        Intrinsics.checkParameterIsNotNull(charSequence2, "other");
        Intrinsics.checkParameterIsNotNull(function2, "transform");
        final int min = Math.min(charSequence.length(), charSequence2.length());
        final ArrayList list = new ArrayList<V>(min);
        for (int i = 0; i < min; ++i) {
            list.add(function2.invoke(charSequence.charAt(i), charSequence2.charAt(i)));
        }
        return (ArrayList<V>)list;
    }
    
    public static final List<Pair<Character, Character>> zipWithNext(final CharSequence charSequence) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$zipWithNext");
        final int initialCapacity = charSequence.length() - 1;
        if (initialCapacity < 1) {
            return CollectionsKt__CollectionsKt.emptyList();
        }
        final ArrayList list = new ArrayList<Pair<Character, Character>>(initialCapacity);
        int i = 0;
        while (i < initialCapacity) {
            final char char1 = charSequence.charAt(i);
            ++i;
            list.add(TuplesKt.to(char1, charSequence.charAt(i)));
        }
        return (ArrayList<Pair<Character, Character>>)list;
    }
    
    public static final <R> List<R> zipWithNext(final CharSequence charSequence, final Function2<? super Character, ? super Character, ? extends R> function2) {
        Intrinsics.checkParameterIsNotNull(charSequence, "$this$zipWithNext");
        Intrinsics.checkParameterIsNotNull(function2, "transform");
        final int initialCapacity = charSequence.length() - 1;
        if (initialCapacity < 1) {
            return CollectionsKt__CollectionsKt.emptyList();
        }
        final ArrayList list = new ArrayList<R>(initialCapacity);
        int i = 0;
        while (i < initialCapacity) {
            final char char1 = charSequence.charAt(i);
            ++i;
            list.add(function2.invoke(char1, charSequence.charAt(i)));
        }
        return (ArrayList<R>)list;
    }
}
