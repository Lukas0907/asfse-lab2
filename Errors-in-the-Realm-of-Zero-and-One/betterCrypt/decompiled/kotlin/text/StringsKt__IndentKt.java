// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.text;

import kotlin.TypeCastException;
import java.util.Iterator;
import kotlin.collections.CollectionsKt;
import kotlin.internal.PlatformImplementationsKt;
import java.util.ArrayList;
import java.util.List;
import kotlin.sequences.Sequence;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.functions.Function1;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u000b\u001a!\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0002\b\u0004\u001a\u0011\u0010\u0005\u001a\u00020\u0006*\u00020\u0002H\u0002¢\u0006\u0002\b\u0007\u001a\u0014\u0010\b\u001a\u00020\u0002*\u00020\u00022\b\b\u0002\u0010\u0003\u001a\u00020\u0002\u001aJ\u0010\t\u001a\u00020\u0002*\b\u0012\u0004\u0012\u00020\u00020\n2\u0006\u0010\u000b\u001a\u00020\u00062\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00020\u00012\u0014\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u0001H\u0082\b¢\u0006\u0002\b\u000e\u001a\u0014\u0010\u000f\u001a\u00020\u0002*\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u0002\u001a\u001e\u0010\u0011\u001a\u00020\u0002*\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u00022\b\b\u0002\u0010\u0012\u001a\u00020\u0002\u001a\n\u0010\u0013\u001a\u00020\u0002*\u00020\u0002\u001a\u0014\u0010\u0014\u001a\u00020\u0002*\u00020\u00022\b\b\u0002\u0010\u0012\u001a\u00020\u0002¨\u0006\u0015" }, d2 = { "getIndentFunction", "Lkotlin/Function1;", "", "indent", "getIndentFunction$StringsKt__IndentKt", "indentWidth", "", "indentWidth$StringsKt__IndentKt", "prependIndent", "reindent", "", "resultSizeEstimate", "indentAddFunction", "indentCutFunction", "reindent$StringsKt__IndentKt", "replaceIndent", "newIndent", "replaceIndentByMargin", "marginPrefix", "trimIndent", "trimMargin", "kotlin-stdlib" }, k = 5, mv = { 1, 1, 15 }, xi = 1, xs = "kotlin/text/StringsKt")
class StringsKt__IndentKt
{
    public StringsKt__IndentKt() {
    }
    
    private static final Function1<String, String> getIndentFunction$StringsKt__IndentKt(final String s) {
        if (s.length() == 0) {
            return (Function1<String, String>)StringsKt__IndentKt$getIndentFunction.StringsKt__IndentKt$getIndentFunction$1.INSTANCE;
        }
        return (Function1<String, String>)new StringsKt__IndentKt$getIndentFunction.StringsKt__IndentKt$getIndentFunction$2(s);
    }
    
    private static final int indentWidth$StringsKt__IndentKt(final String s) {
        final String s2 = s;
        while (true) {
            for (int length = s2.length(), i = 0; i < length; ++i) {
                if (CharsKt__CharJVMKt.isWhitespace(s2.charAt(i)) ^ true) {
                    int length2 = i;
                    if (i == -1) {
                        length2 = s.length();
                    }
                    return length2;
                }
            }
            int i = -1;
            continue;
        }
    }
    
    public static final String prependIndent(final String s, final String s2) {
        Intrinsics.checkParameterIsNotNull(s, "$this$prependIndent");
        Intrinsics.checkParameterIsNotNull(s2, "indent");
        return SequencesKt___SequencesKt.joinToString$default(SequencesKt___SequencesKt.map((Sequence<?>)StringsKt__StringsKt.lineSequence(s), (Function1<? super Object, ?>)new StringsKt__IndentKt$prependIndent.StringsKt__IndentKt$prependIndent$1(s2)), "\n", null, null, 0, null, null, 62, null);
    }
    
    private static final String reindent$StringsKt__IndentKt(final List<String> list, final int capacity, final Function1<? super String, String> function1, final Function1<? super String, String> function2) {
        final int lastIndex = CollectionsKt__CollectionsKt.getLastIndex((List<?>)list);
        final List<? extends T> list2 = (List<? extends T>)list;
        final ArrayList<String> list3 = new ArrayList<String>();
        final Iterator<String> iterator = list2.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final String next = iterator.next();
            if (n < 0) {
                if (!PlatformImplementationsKt.apiVersionIsAtLeast(1, 3, 0)) {
                    throw new ArithmeticException("Index overflow has happened.");
                }
                CollectionsKt.throwIndexOverflow();
            }
            final String s = next;
            String s2;
            if ((n == 0 || n == lastIndex) && StringsKt__StringsJVMKt.isBlank(s)) {
                s2 = null;
            }
            else {
                final String s3 = function2.invoke(s);
                s2 = s;
                if (s3 != null) {
                    final String s4 = function1.invoke(s3);
                    s2 = s;
                    if (s4 != null) {
                        s2 = s4;
                    }
                }
            }
            if (s2 != null) {
                list3.add(s2);
            }
            ++n;
        }
        final String string = ((StringBuilder)CollectionsKt___CollectionsKt.joinTo$default(list3, new StringBuilder(capacity), "\n", null, null, 0, null, null, 124, null)).toString();
        Intrinsics.checkExpressionValueIsNotNull(string, "mapIndexedNotNull { inde\u2026\"\\n\")\n        .toString()");
        return string;
    }
    
    public static final String replaceIndent(String string, String s) {
        Intrinsics.checkParameterIsNotNull(string, "$this$replaceIndent");
        Intrinsics.checkParameterIsNotNull(s, "newIndent");
        final List<String> lines = StringsKt__StringsKt.lines(string);
        final List<Object> list = (List<Object>)lines;
        final ArrayList<String> list2 = new ArrayList<String>();
        for (final String next : list) {
            if (StringsKt__StringsJVMKt.isBlank(next) ^ true) {
                list2.add(next);
            }
        }
        final ArrayList<String> list3 = list2;
        final ArrayList<Integer> list4 = new ArrayList<Integer>(CollectionsKt__IterablesKt.collectionSizeOrDefault((Iterable<?>)list3, 10));
        final Iterator<Object> iterator2 = list3.iterator();
        while (iterator2.hasNext()) {
            list4.add(indentWidth$StringsKt__IndentKt(iterator2.next()));
        }
        final Integer n = CollectionsKt___CollectionsKt.min((Iterable<? extends Integer>)list4);
        int n2 = 0;
        int intValue;
        if (n != null) {
            intValue = n;
        }
        else {
            intValue = 0;
        }
        final int length = string.length();
        final int length2 = s.length();
        final int size = lines.size();
        final Function1<String, String> indentFunction$StringsKt__IndentKt = getIndentFunction$StringsKt__IndentKt(s);
        final int lastIndex = CollectionsKt__CollectionsKt.getLastIndex((List<?>)lines);
        final ArrayList<String> list5 = new ArrayList<String>();
        for (final String next2 : list) {
            if (n2 < 0) {
                CollectionsKt.throwIndexOverflow();
            }
            s = next2;
            if ((n2 == 0 || n2 == lastIndex) && StringsKt__StringsJVMKt.isBlank(s)) {
                string = null;
            }
            else {
                final String drop = StringsKt___StringsKt.drop(s, intValue);
                string = s;
                if (drop != null) {
                    final String s2 = indentFunction$StringsKt__IndentKt.invoke(drop);
                    string = s;
                    if (s2 != null) {
                        string = s2;
                    }
                }
            }
            if (string != null) {
                list5.add(string);
            }
            ++n2;
        }
        string = ((StringBuilder)CollectionsKt___CollectionsKt.joinTo$default(list5, new StringBuilder(length + length2 * size), "\n", null, null, 0, null, null, 124, null)).toString();
        Intrinsics.checkExpressionValueIsNotNull(string, "mapIndexedNotNull { inde\u2026\"\\n\")\n        .toString()");
        return string;
    }
    
    public static final String replaceIndentByMargin(String string, String substring, final String s) {
        Intrinsics.checkParameterIsNotNull(string, "$this$replaceIndentByMargin");
        Intrinsics.checkParameterIsNotNull(substring, "newIndent");
        Intrinsics.checkParameterIsNotNull(s, "marginPrefix");
        if (StringsKt__StringsJVMKt.isBlank(s) ^ true) {
            final List<String> lines = StringsKt__StringsKt.lines(string);
            final int length = string.length();
            final int length2 = substring.length();
            final int size = lines.size();
            final Function1<String, String> indentFunction$StringsKt__IndentKt = getIndentFunction$StringsKt__IndentKt(substring);
            final int lastIndex = CollectionsKt__CollectionsKt.getLastIndex((List<?>)lines);
            final List<? extends T> list = (List<? extends T>)lines;
            final ArrayList<String> list2 = new ArrayList<String>();
            final Iterator<String> iterator = list.iterator();
            int n = 0;
            while (iterator.hasNext()) {
                final String next = iterator.next();
                if (n < 0) {
                    CollectionsKt.throwIndexOverflow();
                }
                final String s2 = next;
                substring = null;
                Label_0302: {
                    if ((n != 0 && n != lastIndex) || !StringsKt__StringsJVMKt.isBlank(s2)) {
                        final String s3 = s2;
                        final int length3 = s3.length();
                        int i = 0;
                        while (true) {
                            while (i < length3) {
                                if (CharsKt__CharJVMKt.isWhitespace(s3.charAt(i)) ^ true) {
                                    if (i != -1) {
                                        if (StringsKt__StringsJVMKt.startsWith$default(s2, s, i, false, 4, null)) {
                                            final int length4 = s.length();
                                            if (s2 == null) {
                                                throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                                            }
                                            substring = s2.substring(i + length4);
                                            Intrinsics.checkExpressionValueIsNotNull(substring, "(this as java.lang.String).substring(startIndex)");
                                        }
                                    }
                                    string = s2;
                                    if (substring == null) {
                                        break Label_0302;
                                    }
                                    substring = indentFunction$StringsKt__IndentKt.invoke(substring);
                                    string = s2;
                                    if (substring != null) {
                                        string = substring;
                                    }
                                    break Label_0302;
                                }
                                else {
                                    ++i;
                                }
                            }
                            i = -1;
                            continue;
                        }
                    }
                    string = null;
                }
                if (string != null) {
                    list2.add(string);
                }
                ++n;
            }
            string = ((StringBuilder)CollectionsKt___CollectionsKt.joinTo$default(list2, new StringBuilder(length + length2 * size), "\n", null, null, 0, null, null, 124, null)).toString();
            Intrinsics.checkExpressionValueIsNotNull(string, "mapIndexedNotNull { inde\u2026\"\\n\")\n        .toString()");
            return string;
        }
        throw new IllegalArgumentException("marginPrefix must be non-blank string.".toString());
    }
    
    public static final String trimIndent(final String s) {
        Intrinsics.checkParameterIsNotNull(s, "$this$trimIndent");
        return replaceIndent(s, "");
    }
    
    public static final String trimMargin(final String s, final String s2) {
        Intrinsics.checkParameterIsNotNull(s, "$this$trimMargin");
        Intrinsics.checkParameterIsNotNull(s2, "marginPrefix");
        return replaceIndentByMargin(s, "", s2);
    }
}
