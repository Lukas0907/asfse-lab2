// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.text;

import kotlin.ranges.IntRange;
import kotlin.jvm.internal.Intrinsics;
import java.util.regex.Matcher;
import java.util.List;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\n\u0010\u001c\u001a\u0004\u0018\u00010\u0001H\u0016R\u001a\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000bR\u0016\u0010\f\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\bX\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u00020\u000eX\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u00020\u00128BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u00020\u00168VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0017\u0010\u0018R\u0014\u0010\u0019\u001a\u00020\t8VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u001a\u0010\u001b¨\u0006\u001d" }, d2 = { "Lkotlin/text/MatcherMatchResult;", "Lkotlin/text/MatchResult;", "matcher", "Ljava/util/regex/Matcher;", "input", "", "(Ljava/util/regex/Matcher;Ljava/lang/CharSequence;)V", "groupValues", "", "", "getGroupValues", "()Ljava/util/List;", "groupValues_", "groups", "Lkotlin/text/MatchGroupCollection;", "getGroups", "()Lkotlin/text/MatchGroupCollection;", "matchResult", "Ljava/util/regex/MatchResult;", "getMatchResult", "()Ljava/util/regex/MatchResult;", "range", "Lkotlin/ranges/IntRange;", "getRange", "()Lkotlin/ranges/IntRange;", "value", "getValue", "()Ljava/lang/String;", "next", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
final class MatcherMatchResult implements MatchResult
{
    private List<String> groupValues_;
    private final MatchGroupCollection groups;
    private final CharSequence input;
    private final Matcher matcher;
    
    public MatcherMatchResult(final Matcher matcher, final CharSequence input) {
        Intrinsics.checkParameterIsNotNull(matcher, "matcher");
        Intrinsics.checkParameterIsNotNull(input, "input");
        this.matcher = matcher;
        this.input = input;
        this.groups = (MatchGroupCollection)new MatcherMatchResult$groups.MatcherMatchResult$groups$1(this);
    }
    
    private final java.util.regex.MatchResult getMatchResult() {
        return this.matcher;
    }
    
    @Override
    public Destructured getDestructured() {
        return DefaultImpls.getDestructured(this);
    }
    
    @Override
    public List<String> getGroupValues() {
        if (this.groupValues_ == null) {
            this.groupValues_ = (List<String>)new MatcherMatchResult$groupValues.MatcherMatchResult$groupValues$1(this);
        }
        final List<String> groupValues_ = this.groupValues_;
        if (groupValues_ == null) {
            Intrinsics.throwNpe();
        }
        return groupValues_;
    }
    
    @Override
    public MatchGroupCollection getGroups() {
        return this.groups;
    }
    
    @Override
    public IntRange getRange() {
        return RegexKt.access$range(this.getMatchResult());
    }
    
    @Override
    public String getValue() {
        final String group = this.getMatchResult().group();
        Intrinsics.checkExpressionValueIsNotNull(group, "matchResult.group()");
        return group;
    }
    
    @Override
    public MatchResult next() {
        final int end = this.getMatchResult().end();
        int n;
        if (this.getMatchResult().end() == this.getMatchResult().start()) {
            n = 1;
        }
        else {
            n = 0;
        }
        final int n2 = end + n;
        if (n2 <= this.input.length()) {
            final Matcher matcher = this.matcher.pattern().matcher(this.input);
            Intrinsics.checkExpressionValueIsNotNull(matcher, "matcher.pattern().matcher(input)");
            return RegexKt.access$findNext(matcher, n2, this.input);
        }
        return null;
    }
}
