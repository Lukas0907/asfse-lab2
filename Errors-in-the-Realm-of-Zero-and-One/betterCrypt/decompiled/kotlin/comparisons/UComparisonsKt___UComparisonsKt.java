// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.comparisons;

import kotlin.UnsignedKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000e\u001a\"\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0001H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0004\u0010\u0005\u001a+\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u0001H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\u0007\u0010\b\u001a\"\u0010\u0000\u001a\u00020\t2\u0006\u0010\u0002\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\tH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\n\u0010\u000b\u001a+\u0010\u0000\u001a\u00020\t2\u0006\u0010\u0002\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\t2\u0006\u0010\u0006\u001a\u00020\tH\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\f\u0010\r\u001a\"\u0010\u0000\u001a\u00020\u000e2\u0006\u0010\u0002\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u000eH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u000f\u0010\u0010\u001a+\u0010\u0000\u001a\u00020\u000e2\u0006\u0010\u0002\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u000e2\u0006\u0010\u0006\u001a\u00020\u000eH\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\u0011\u0010\u0012\u001a\"\u0010\u0000\u001a\u00020\u00132\u0006\u0010\u0002\u001a\u00020\u00132\u0006\u0010\u0003\u001a\u00020\u0013H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0014\u0010\u0015\u001a+\u0010\u0000\u001a\u00020\u00132\u0006\u0010\u0002\u001a\u00020\u00132\u0006\u0010\u0003\u001a\u00020\u00132\u0006\u0010\u0006\u001a\u00020\u0013H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\u0016\u0010\u0017\u001a\"\u0010\u0018\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0001H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0019\u0010\u0005\u001a+\u0010\u0018\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u0001H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\u001a\u0010\b\u001a\"\u0010\u0018\u001a\u00020\t2\u0006\u0010\u0002\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\tH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u001b\u0010\u000b\u001a+\u0010\u0018\u001a\u00020\t2\u0006\u0010\u0002\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\t2\u0006\u0010\u0006\u001a\u00020\tH\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\u001c\u0010\r\u001a\"\u0010\u0018\u001a\u00020\u000e2\u0006\u0010\u0002\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u000eH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u001d\u0010\u0010\u001a+\u0010\u0018\u001a\u00020\u000e2\u0006\u0010\u0002\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u000e2\u0006\u0010\u0006\u001a\u00020\u000eH\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b\u001e\u0010\u0012\u001a\"\u0010\u0018\u001a\u00020\u00132\u0006\u0010\u0002\u001a\u00020\u00132\u0006\u0010\u0003\u001a\u00020\u0013H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u001f\u0010\u0015\u001a+\u0010\u0018\u001a\u00020\u00132\u0006\u0010\u0002\u001a\u00020\u00132\u0006\u0010\u0003\u001a\u00020\u00132\u0006\u0010\u0006\u001a\u00020\u0013H\u0087\b\u00f8\u0001\u0000¢\u0006\u0004\b \u0010\u0017\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006!" }, d2 = { "maxOf", "Lkotlin/UByte;", "a", "b", "maxOf-Kr8caGY", "(BB)B", "c", "maxOf-b33U2AM", "(BBB)B", "Lkotlin/UInt;", "maxOf-J1ME1BU", "(II)I", "maxOf-WZ9TVnA", "(III)I", "Lkotlin/ULong;", "maxOf-eb3DHEI", "(JJ)J", "maxOf-sambcqE", "(JJJ)J", "Lkotlin/UShort;", "maxOf-5PvTz6A", "(SS)S", "maxOf-VKSA0NQ", "(SSS)S", "minOf", "minOf-Kr8caGY", "minOf-b33U2AM", "minOf-J1ME1BU", "minOf-WZ9TVnA", "minOf-eb3DHEI", "minOf-sambcqE", "minOf-5PvTz6A", "minOf-VKSA0NQ", "kotlin-stdlib" }, k = 5, mv = { 1, 1, 15 }, xi = 1, xs = "kotlin/comparisons/UComparisonsKt")
class UComparisonsKt___UComparisonsKt
{
    public UComparisonsKt___UComparisonsKt() {
    }
    
    public static final short maxOf-5PvTz6A(final short n, final short n2) {
        if (Intrinsics.compare(n & 0xFFFF, 0xFFFF & n2) >= 0) {
            return n;
        }
        return n2;
    }
    
    public static final int maxOf-J1ME1BU(final int n, final int n2) {
        if (UnsignedKt.uintCompare(n, n2) >= 0) {
            return n;
        }
        return n2;
    }
    
    public static final byte maxOf-Kr8caGY(final byte b, final byte b2) {
        if (Intrinsics.compare(b & 0xFF, b2 & 0xFF) >= 0) {
            return b;
        }
        return b2;
    }
    
    private static final short maxOf-VKSA0NQ(final short n, final short n2, final short n3) {
        return maxOf-5PvTz6A(n, maxOf-5PvTz6A(n2, n3));
    }
    
    private static final int maxOf-WZ9TVnA(final int n, final int n2, final int n3) {
        return maxOf-J1ME1BU(n, maxOf-J1ME1BU(n2, n3));
    }
    
    private static final byte maxOf-b33U2AM(final byte b, final byte b2, final byte b3) {
        return maxOf-Kr8caGY(b, maxOf-Kr8caGY(b2, b3));
    }
    
    public static final long maxOf-eb3DHEI(final long n, final long n2) {
        if (UnsignedKt.ulongCompare(n, n2) >= 0) {
            return n;
        }
        return n2;
    }
    
    private static final long maxOf-sambcqE(final long n, final long n2, final long n3) {
        return maxOf-eb3DHEI(n, maxOf-eb3DHEI(n2, n3));
    }
    
    public static final short minOf-5PvTz6A(final short n, final short n2) {
        if (Intrinsics.compare(n & 0xFFFF, 0xFFFF & n2) <= 0) {
            return n;
        }
        return n2;
    }
    
    public static final int minOf-J1ME1BU(final int n, final int n2) {
        if (UnsignedKt.uintCompare(n, n2) <= 0) {
            return n;
        }
        return n2;
    }
    
    public static final byte minOf-Kr8caGY(final byte b, final byte b2) {
        if (Intrinsics.compare(b & 0xFF, b2 & 0xFF) <= 0) {
            return b;
        }
        return b2;
    }
    
    private static final short minOf-VKSA0NQ(final short n, final short n2, final short n3) {
        return minOf-5PvTz6A(n, minOf-5PvTz6A(n2, n3));
    }
    
    private static final int minOf-WZ9TVnA(final int n, final int n2, final int n3) {
        return minOf-J1ME1BU(n, minOf-J1ME1BU(n2, n3));
    }
    
    private static final byte minOf-b33U2AM(final byte b, final byte b2, final byte b3) {
        return minOf-Kr8caGY(b, minOf-Kr8caGY(b2, b3));
    }
    
    public static final long minOf-eb3DHEI(final long n, final long n2) {
        if (UnsignedKt.ulongCompare(n, n2) <= 0) {
            return n;
        }
        return n2;
    }
    
    private static final long minOf-sambcqE(final long n, final long n2, final long n3) {
        return minOf-eb3DHEI(n, minOf-eb3DHEI(n2, n3));
    }
}
