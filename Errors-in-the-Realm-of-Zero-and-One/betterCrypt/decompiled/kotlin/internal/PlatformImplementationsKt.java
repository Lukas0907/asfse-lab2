// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.internal;

import kotlin.jvm.internal.Intrinsics;
import kotlin.KotlinVersion;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\u0004\u001a \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0005H\u0001\u001a\"\u0010\b\u001a\u0002H\t\"\n\b\u0000\u0010\t\u0018\u0001*\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH\u0083\b¢\u0006\u0002\u0010\f\u001a\b\u0010\r\u001a\u00020\u0005H\u0002\"\u0010\u0010\u0000\u001a\u00020\u00018\u0000X\u0081\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e" }, d2 = { "IMPLEMENTATIONS", "Lkotlin/internal/PlatformImplementations;", "apiVersionIsAtLeast", "", "major", "", "minor", "patch", "castToBaseType", "T", "", "instance", "(Ljava/lang/Object;)Ljava/lang/Object;", "getJavaVersion", "kotlin-stdlib" }, k = 2, mv = { 1, 1, 15 })
public final class PlatformImplementationsKt
{
    public static final PlatformImplementations IMPLEMENTATIONS;
    
    static {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: istore_0       
        //     4: iload_0        
        //     5: ldc             65544
        //     7: if_icmplt       246
        //    10: ldc             "kotlin.internal.jdk8.JDK8PlatformImplementations"
        //    12: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //    15: invokevirtual   java/lang/Class.newInstance:()Ljava/lang/Object;
        //    18: astore_2       
        //    19: aload_2        
        //    20: ldc             "Class.forName(\"kotlin.in\u2026entations\").newInstance()"
        //    22: invokestatic    kotlin/jvm/internal/Intrinsics.checkExpressionValueIsNotNull:(Ljava/lang/Object;Ljava/lang/String;)V
        //    25: aload_2        
        //    26: ifnull          37
        //    29: aload_2        
        //    30: checkcast       Lkotlin/internal/PlatformImplementations;
        //    33: astore_1       
        //    34: goto            496
        //    37: new             Lkotlin/TypeCastException;
        //    40: dup            
        //    41: ldc             "null cannot be cast to non-null type kotlin.internal.PlatformImplementations"
        //    43: invokespecial   kotlin/TypeCastException.<init>:(Ljava/lang/String;)V
        //    46: athrow         
        //    47: aload_2        
        //    48: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //    51: invokevirtual   java/lang/Class.getClassLoader:()Ljava/lang/ClassLoader;
        //    54: astore_2       
        //    55: ldc             Lkotlin/internal/PlatformImplementations;.class
        //    57: invokevirtual   java/lang/Class.getClassLoader:()Ljava/lang/ClassLoader;
        //    60: astore_3       
        //    61: new             Ljava/lang/StringBuilder;
        //    64: dup            
        //    65: invokespecial   java/lang/StringBuilder.<init>:()V
        //    68: astore          4
        //    70: aload           4
        //    72: ldc             "Instance classloader: "
        //    74: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    77: pop            
        //    78: aload           4
        //    80: aload_2        
        //    81: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //    84: pop            
        //    85: aload           4
        //    87: ldc             ", base type classloader: "
        //    89: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    92: pop            
        //    93: aload           4
        //    95: aload_3        
        //    96: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //    99: pop            
        //   100: new             Ljava/lang/ClassCastException;
        //   103: dup            
        //   104: aload           4
        //   106: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   109: invokespecial   java/lang/ClassCastException.<init>:(Ljava/lang/String;)V
        //   112: aload_1        
        //   113: checkcast       Ljava/lang/Throwable;
        //   116: invokevirtual   java/lang/ClassCastException.initCause:(Ljava/lang/Throwable;)Ljava/lang/Throwable;
        //   119: astore_1       
        //   120: aload_1        
        //   121: ldc             "ClassCastException(\"Inst\u2026baseTypeCL\").initCause(e)"
        //   123: invokestatic    kotlin/jvm/internal/Intrinsics.checkExpressionValueIsNotNull:(Ljava/lang/Object;Ljava/lang/String;)V
        //   126: aload_1        
        //   127: athrow         
        //   128: ldc             "kotlin.internal.JRE8PlatformImplementations"
        //   130: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //   133: invokevirtual   java/lang/Class.newInstance:()Ljava/lang/Object;
        //   136: astore_2       
        //   137: aload_2        
        //   138: ldc             "Class.forName(\"kotlin.in\u2026entations\").newInstance()"
        //   140: invokestatic    kotlin/jvm/internal/Intrinsics.checkExpressionValueIsNotNull:(Ljava/lang/Object;Ljava/lang/String;)V
        //   143: aload_2        
        //   144: ifnull          155
        //   147: aload_2        
        //   148: checkcast       Lkotlin/internal/PlatformImplementations;
        //   151: astore_1       
        //   152: goto            496
        //   155: new             Lkotlin/TypeCastException;
        //   158: dup            
        //   159: ldc             "null cannot be cast to non-null type kotlin.internal.PlatformImplementations"
        //   161: invokespecial   kotlin/TypeCastException.<init>:(Ljava/lang/String;)V
        //   164: athrow         
        //   165: aload_2        
        //   166: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //   169: invokevirtual   java/lang/Class.getClassLoader:()Ljava/lang/ClassLoader;
        //   172: astore_2       
        //   173: ldc             Lkotlin/internal/PlatformImplementations;.class
        //   175: invokevirtual   java/lang/Class.getClassLoader:()Ljava/lang/ClassLoader;
        //   178: astore_3       
        //   179: new             Ljava/lang/StringBuilder;
        //   182: dup            
        //   183: invokespecial   java/lang/StringBuilder.<init>:()V
        //   186: astore          4
        //   188: aload           4
        //   190: ldc             "Instance classloader: "
        //   192: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   195: pop            
        //   196: aload           4
        //   198: aload_2        
        //   199: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   202: pop            
        //   203: aload           4
        //   205: ldc             ", base type classloader: "
        //   207: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   210: pop            
        //   211: aload           4
        //   213: aload_3        
        //   214: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   217: pop            
        //   218: new             Ljava/lang/ClassCastException;
        //   221: dup            
        //   222: aload           4
        //   224: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   227: invokespecial   java/lang/ClassCastException.<init>:(Ljava/lang/String;)V
        //   230: aload_1        
        //   231: checkcast       Ljava/lang/Throwable;
        //   234: invokevirtual   java/lang/ClassCastException.initCause:(Ljava/lang/Throwable;)Ljava/lang/Throwable;
        //   237: astore_1       
        //   238: aload_1        
        //   239: ldc             "ClassCastException(\"Inst\u2026baseTypeCL\").initCause(e)"
        //   241: invokestatic    kotlin/jvm/internal/Intrinsics.checkExpressionValueIsNotNull:(Ljava/lang/Object;Ljava/lang/String;)V
        //   244: aload_1        
        //   245: athrow         
        //   246: iload_0        
        //   247: ldc             65543
        //   249: if_icmplt       488
        //   252: ldc             "kotlin.internal.jdk7.JDK7PlatformImplementations"
        //   254: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //   257: invokevirtual   java/lang/Class.newInstance:()Ljava/lang/Object;
        //   260: astore_2       
        //   261: aload_2        
        //   262: ldc             "Class.forName(\"kotlin.in\u2026entations\").newInstance()"
        //   264: invokestatic    kotlin/jvm/internal/Intrinsics.checkExpressionValueIsNotNull:(Ljava/lang/Object;Ljava/lang/String;)V
        //   267: aload_2        
        //   268: ifnull          279
        //   271: aload_2        
        //   272: checkcast       Lkotlin/internal/PlatformImplementations;
        //   275: astore_1       
        //   276: goto            496
        //   279: new             Lkotlin/TypeCastException;
        //   282: dup            
        //   283: ldc             "null cannot be cast to non-null type kotlin.internal.PlatformImplementations"
        //   285: invokespecial   kotlin/TypeCastException.<init>:(Ljava/lang/String;)V
        //   288: athrow         
        //   289: aload_2        
        //   290: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //   293: invokevirtual   java/lang/Class.getClassLoader:()Ljava/lang/ClassLoader;
        //   296: astore_2       
        //   297: ldc             Lkotlin/internal/PlatformImplementations;.class
        //   299: invokevirtual   java/lang/Class.getClassLoader:()Ljava/lang/ClassLoader;
        //   302: astore_3       
        //   303: new             Ljava/lang/StringBuilder;
        //   306: dup            
        //   307: invokespecial   java/lang/StringBuilder.<init>:()V
        //   310: astore          4
        //   312: aload           4
        //   314: ldc             "Instance classloader: "
        //   316: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   319: pop            
        //   320: aload           4
        //   322: aload_2        
        //   323: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   326: pop            
        //   327: aload           4
        //   329: ldc             ", base type classloader: "
        //   331: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   334: pop            
        //   335: aload           4
        //   337: aload_3        
        //   338: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   341: pop            
        //   342: new             Ljava/lang/ClassCastException;
        //   345: dup            
        //   346: aload           4
        //   348: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   351: invokespecial   java/lang/ClassCastException.<init>:(Ljava/lang/String;)V
        //   354: aload_1        
        //   355: checkcast       Ljava/lang/Throwable;
        //   358: invokevirtual   java/lang/ClassCastException.initCause:(Ljava/lang/Throwable;)Ljava/lang/Throwable;
        //   361: astore_1       
        //   362: aload_1        
        //   363: ldc             "ClassCastException(\"Inst\u2026baseTypeCL\").initCause(e)"
        //   365: invokestatic    kotlin/jvm/internal/Intrinsics.checkExpressionValueIsNotNull:(Ljava/lang/Object;Ljava/lang/String;)V
        //   368: aload_1        
        //   369: athrow         
        //   370: ldc             "kotlin.internal.JRE7PlatformImplementations"
        //   372: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //   375: invokevirtual   java/lang/Class.newInstance:()Ljava/lang/Object;
        //   378: astore_2       
        //   379: aload_2        
        //   380: ldc             "Class.forName(\"kotlin.in\u2026entations\").newInstance()"
        //   382: invokestatic    kotlin/jvm/internal/Intrinsics.checkExpressionValueIsNotNull:(Ljava/lang/Object;Ljava/lang/String;)V
        //   385: aload_2        
        //   386: ifnull          397
        //   389: aload_2        
        //   390: checkcast       Lkotlin/internal/PlatformImplementations;
        //   393: astore_1       
        //   394: goto            496
        //   397: new             Lkotlin/TypeCastException;
        //   400: dup            
        //   401: ldc             "null cannot be cast to non-null type kotlin.internal.PlatformImplementations"
        //   403: invokespecial   kotlin/TypeCastException.<init>:(Ljava/lang/String;)V
        //   406: athrow         
        //   407: aload_2        
        //   408: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //   411: invokevirtual   java/lang/Class.getClassLoader:()Ljava/lang/ClassLoader;
        //   414: astore_2       
        //   415: ldc             Lkotlin/internal/PlatformImplementations;.class
        //   417: invokevirtual   java/lang/Class.getClassLoader:()Ljava/lang/ClassLoader;
        //   420: astore_3       
        //   421: new             Ljava/lang/StringBuilder;
        //   424: dup            
        //   425: invokespecial   java/lang/StringBuilder.<init>:()V
        //   428: astore          4
        //   430: aload           4
        //   432: ldc             "Instance classloader: "
        //   434: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   437: pop            
        //   438: aload           4
        //   440: aload_2        
        //   441: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   444: pop            
        //   445: aload           4
        //   447: ldc             ", base type classloader: "
        //   449: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   452: pop            
        //   453: aload           4
        //   455: aload_3        
        //   456: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   459: pop            
        //   460: new             Ljava/lang/ClassCastException;
        //   463: dup            
        //   464: aload           4
        //   466: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   469: invokespecial   java/lang/ClassCastException.<init>:(Ljava/lang/String;)V
        //   472: aload_1        
        //   473: checkcast       Ljava/lang/Throwable;
        //   476: invokevirtual   java/lang/ClassCastException.initCause:(Ljava/lang/Throwable;)Ljava/lang/Throwable;
        //   479: astore_1       
        //   480: aload_1        
        //   481: ldc             "ClassCastException(\"Inst\u2026baseTypeCL\").initCause(e)"
        //   483: invokestatic    kotlin/jvm/internal/Intrinsics.checkExpressionValueIsNotNull:(Ljava/lang/Object;Ljava/lang/String;)V
        //   486: aload_1        
        //   487: athrow         
        //   488: new             Lkotlin/internal/PlatformImplementations;
        //   491: dup            
        //   492: invokespecial   kotlin/internal/PlatformImplementations.<init>:()V
        //   495: astore_1       
        //   496: aload_1        
        //   497: putstatic       kotlin/internal/PlatformImplementationsKt.IMPLEMENTATIONS:Lkotlin/internal/PlatformImplementations;
        //   500: return         
        //   501: astore_1       
        //   502: goto            128
        //   505: astore_1       
        //   506: goto            246
        //   509: astore_1       
        //   510: goto            370
        //   513: astore_1       
        //   514: goto            488
        //   517: astore_1       
        //   518: goto            47
        //   521: astore_1       
        //   522: goto            165
        //   525: astore_1       
        //   526: goto            289
        //   529: astore_1       
        //   530: goto            407
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                              
        //  -----  -----  -----  -----  ----------------------------------
        //  10     25     501    509    Ljava/lang/ClassNotFoundException;
        //  29     34     517    128    Ljava/lang/ClassCastException;
        //  29     34     501    509    Ljava/lang/ClassNotFoundException;
        //  37     47     517    128    Ljava/lang/ClassCastException;
        //  37     47     501    509    Ljava/lang/ClassNotFoundException;
        //  47     128    501    509    Ljava/lang/ClassNotFoundException;
        //  128    143    505    509    Ljava/lang/ClassNotFoundException;
        //  147    152    521    246    Ljava/lang/ClassCastException;
        //  147    152    505    509    Ljava/lang/ClassNotFoundException;
        //  155    165    521    246    Ljava/lang/ClassCastException;
        //  155    165    505    509    Ljava/lang/ClassNotFoundException;
        //  165    246    505    509    Ljava/lang/ClassNotFoundException;
        //  252    267    509    517    Ljava/lang/ClassNotFoundException;
        //  271    276    525    370    Ljava/lang/ClassCastException;
        //  271    276    509    517    Ljava/lang/ClassNotFoundException;
        //  279    289    525    370    Ljava/lang/ClassCastException;
        //  279    289    509    517    Ljava/lang/ClassNotFoundException;
        //  289    370    509    517    Ljava/lang/ClassNotFoundException;
        //  370    385    513    517    Ljava/lang/ClassNotFoundException;
        //  389    394    529    488    Ljava/lang/ClassCastException;
        //  389    394    513    517    Ljava/lang/ClassNotFoundException;
        //  397    407    529    488    Ljava/lang/ClassCastException;
        //  397    407    513    517    Ljava/lang/ClassNotFoundException;
        //  407    488    513    517    Ljava/lang/ClassNotFoundException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 267 out of bounds for length 267
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:248)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:373)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:426)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3321)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3435)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:113)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:211)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static final boolean apiVersionIsAtLeast(final int n, final int n2, final int n3) {
        return KotlinVersion.CURRENT.isAtLeast(n, n2, n3);
    }
    
    private static final int getJavaVersion() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: invokestatic    java/lang/System.getProperty:(Ljava/lang/String;)Ljava/lang/String;
        //     5: astore          4
        //     7: aload           4
        //     9: ifnull          156
        //    12: aload           4
        //    14: checkcast       Ljava/lang/CharSequence;
        //    17: astore          5
        //    19: aload           5
        //    21: bipush          46
        //    23: iconst_0       
        //    24: iconst_0       
        //    25: bipush          6
        //    27: aconst_null    
        //    28: invokestatic    kotlin/text/StringsKt.indexOf$default:(Ljava/lang/CharSequence;CIZILjava/lang/Object;)I
        //    31: istore_2       
        //    32: iload_2        
        //    33: ifge            47
        //    36: aload           4
        //    38: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //    41: istore_0       
        //    42: iload_0        
        //    43: ldc             65536
        //    45: imul           
        //    46: ireturn        
        //    47: iload_2        
        //    48: iconst_1       
        //    49: iadd           
        //    50: istore_3       
        //    51: aload           5
        //    53: bipush          46
        //    55: iload_3        
        //    56: iconst_0       
        //    57: iconst_4       
        //    58: aconst_null    
        //    59: invokestatic    kotlin/text/StringsKt.indexOf$default:(Ljava/lang/CharSequence;CIZILjava/lang/Object;)I
        //    62: istore_1       
        //    63: iload_1        
        //    64: istore_0       
        //    65: iload_1        
        //    66: ifge            75
        //    69: aload           4
        //    71: invokevirtual   java/lang/String.length:()I
        //    74: istore_0       
        //    75: aload           4
        //    77: ifnull          146
        //    80: aload           4
        //    82: iconst_0       
        //    83: iload_2        
        //    84: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //    87: astore          5
        //    89: aload           5
        //    91: ldc             "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"
        //    93: invokestatic    kotlin/jvm/internal/Intrinsics.checkExpressionValueIsNotNull:(Ljava/lang/Object;Ljava/lang/String;)V
        //    96: aload           4
        //    98: ifnull          136
        //   101: aload           4
        //   103: iload_3        
        //   104: iload_0        
        //   105: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //   108: astore          4
        //   110: aload           4
        //   112: ldc             "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"
        //   114: invokestatic    kotlin/jvm/internal/Intrinsics.checkExpressionValueIsNotNull:(Ljava/lang/Object;Ljava/lang/String;)V
        //   117: aload           5
        //   119: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   122: istore_0       
        //   123: aload           4
        //   125: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   128: istore_1       
        //   129: iload_0        
        //   130: ldc             65536
        //   132: imul           
        //   133: iload_1        
        //   134: iadd           
        //   135: ireturn        
        //   136: new             Lkotlin/TypeCastException;
        //   139: dup            
        //   140: ldc             "null cannot be cast to non-null type java.lang.String"
        //   142: invokespecial   kotlin/TypeCastException.<init>:(Ljava/lang/String;)V
        //   145: athrow         
        //   146: new             Lkotlin/TypeCastException;
        //   149: dup            
        //   150: ldc             "null cannot be cast to non-null type java.lang.String"
        //   152: invokespecial   kotlin/TypeCastException.<init>:(Ljava/lang/String;)V
        //   155: athrow         
        //   156: ldc             65542
        //   158: ireturn        
        //   159: astore          4
        //   161: ldc             65542
        //   163: ireturn        
        //   164: astore          4
        //   166: ldc             65542
        //   168: ireturn        
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                             
        //  -----  -----  -----  -----  ---------------------------------
        //  36     42     159    164    Ljava/lang/NumberFormatException;
        //  117    129    164    169    Ljava/lang/NumberFormatException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0136:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
