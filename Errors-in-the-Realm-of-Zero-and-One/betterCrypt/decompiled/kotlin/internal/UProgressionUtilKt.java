// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.internal;

import kotlin.ULong;
import kotlin.UInt;
import kotlin.UnsignedKt;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\u001a*\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0001H\u0002\u00f8\u0001\u0000¢\u0006\u0004\b\u0005\u0010\u0006\u001a*\u0010\u0000\u001a\u00020\u00072\u0006\u0010\u0002\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u0007H\u0002\u00f8\u0001\u0000¢\u0006\u0004\b\b\u0010\t\u001a*\u0010\n\u001a\u00020\u00012\u0006\u0010\u000b\u001a\u00020\u00012\u0006\u0010\f\u001a\u00020\u00012\u0006\u0010\r\u001a\u00020\u000eH\u0001\u00f8\u0001\u0000¢\u0006\u0004\b\u000f\u0010\u0006\u001a*\u0010\n\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\u00072\u0006\u0010\f\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\u0010H\u0001\u00f8\u0001\u0000¢\u0006\u0004\b\u0011\u0010\t\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006\u0012" }, d2 = { "differenceModulo", "Lkotlin/UInt;", "a", "b", "c", "differenceModulo-WZ9TVnA", "(III)I", "Lkotlin/ULong;", "differenceModulo-sambcqE", "(JJJ)J", "getProgressionLastElement", "start", "end", "step", "", "getProgressionLastElement-Nkh28Cs", "", "getProgressionLastElement-7ftBX0g", "kotlin-stdlib" }, k = 2, mv = { 1, 1, 15 })
public final class UProgressionUtilKt
{
    private static final int differenceModulo-WZ9TVnA(int n, int uintCompare, final int n2) {
        n = UnsignedKt.uintRemainder-J1ME1BU(n, n2);
        final int uintRemainder-J1ME1BU = UnsignedKt.uintRemainder-J1ME1BU(uintCompare, n2);
        uintCompare = UnsignedKt.uintCompare(n, uintRemainder-J1ME1BU);
        n = UInt.constructor-impl(n - uintRemainder-J1ME1BU);
        if (uintCompare >= 0) {
            return n;
        }
        return UInt.constructor-impl(n + n2);
    }
    
    private static final long differenceModulo-sambcqE(long n, long ulongRemainder-eb3DHEI, final long n2) {
        n = UnsignedKt.ulongRemainder-eb3DHEI(n, n2);
        ulongRemainder-eb3DHEI = UnsignedKt.ulongRemainder-eb3DHEI(ulongRemainder-eb3DHEI, n2);
        final int ulongCompare = UnsignedKt.ulongCompare(n, ulongRemainder-eb3DHEI);
        n = ULong.constructor-impl(n - ulongRemainder-eb3DHEI);
        if (ulongCompare >= 0) {
            return n;
        }
        return ULong.constructor-impl(n + n2);
    }
    
    public static final long getProgressionLastElement-7ftBX0g(final long n, final long n2, final long n3) {
        final long n4 = lcmp(n3, 0L);
        if (n4 > 0) {
            if (UnsignedKt.ulongCompare(n, n2) >= 0) {
                return n2;
            }
            return ULong.constructor-impl(n2 - differenceModulo-sambcqE(n2, n, ULong.constructor-impl(n3)));
        }
        else {
            if (n4 >= 0) {
                throw new IllegalArgumentException("Step is zero.");
            }
            if (UnsignedKt.ulongCompare(n, n2) <= 0) {
                return n2;
            }
            return ULong.constructor-impl(n2 + differenceModulo-sambcqE(n, n2, ULong.constructor-impl(-n3)));
        }
    }
    
    public static final int getProgressionLastElement-Nkh28Cs(final int n, final int n2, final int n3) {
        if (n3 > 0) {
            if (UnsignedKt.uintCompare(n, n2) >= 0) {
                return n2;
            }
            return UInt.constructor-impl(n2 - differenceModulo-WZ9TVnA(n2, n, UInt.constructor-impl(n3)));
        }
        else {
            if (n3 >= 0) {
                throw new IllegalArgumentException("Step is zero.");
            }
            if (UnsignedKt.uintCompare(n, n2) <= 0) {
                return n2;
            }
            return UInt.constructor-impl(n2 + differenceModulo-WZ9TVnA(n, n2, UInt.constructor-impl(-n3)));
        }
    }
}
