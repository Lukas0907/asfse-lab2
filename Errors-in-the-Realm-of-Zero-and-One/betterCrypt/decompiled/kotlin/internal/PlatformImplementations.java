// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.internal;

import kotlin.text.MatchGroup;
import java.util.regex.MatchResult;
import kotlin.random.FallbackThreadLocalRandom;
import kotlin.random.Random;
import java.lang.reflect.Method;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0010\u0018\u00002\u00020\u0001:\u0001\u0010B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\u0016J\b\u0010\b\u001a\u00020\tH\u0016J\u001a\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016¨\u0006\u0011" }, d2 = { "Lkotlin/internal/PlatformImplementations;", "", "()V", "addSuppressed", "", "cause", "", "exception", "defaultPlatformRandom", "Lkotlin/random/Random;", "getMatchResultNamedGroup", "Lkotlin/text/MatchGroup;", "matchResult", "Ljava/util/regex/MatchResult;", "name", "", "ReflectAddSuppressedMethod", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
public class PlatformImplementations
{
    public void addSuppressed(final Throwable obj, final Throwable t) {
        Intrinsics.checkParameterIsNotNull(obj, "cause");
        Intrinsics.checkParameterIsNotNull(t, "exception");
        final Method method = ReflectAddSuppressedMethod.method;
        if (method != null) {
            method.invoke(obj, t);
        }
    }
    
    public Random defaultPlatformRandom() {
        return new FallbackThreadLocalRandom();
    }
    
    public MatchGroup getMatchResultNamedGroup(final MatchResult matchResult, final String s) {
        Intrinsics.checkParameterIsNotNull(matchResult, "matchResult");
        Intrinsics.checkParameterIsNotNull(s, "name");
        throw new UnsupportedOperationException("Retrieving groups by name is not supported on this platform.");
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u00c2\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006X\u0087\u0004¢\u0006\u0002\n\u0000¨\u0006\u0005" }, d2 = { "Lkotlin/internal/PlatformImplementations$ReflectAddSuppressedMethod;", "", "()V", "method", "Ljava/lang/reflect/Method;", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
    private static final class ReflectAddSuppressedMethod
    {
        public static final ReflectAddSuppressedMethod INSTANCE;
        public static final Method method;
        
        static {
            INSTANCE = new ReflectAddSuppressedMethod();
            final Method[] methods = Throwable.class.getMethods();
            Intrinsics.checkExpressionValueIsNotNull(methods, "throwableClass.methods");
            while (true) {
                for (int length = methods.length, i = 0; i < length; ++i) {
                    final Method method2 = methods[i];
                    Intrinsics.checkExpressionValueIsNotNull(method2, "it");
                    boolean b = false;
                    Label_0094: {
                        if (Intrinsics.areEqual(method2.getName(), "addSuppressed")) {
                            final Class<?>[] parameterTypes = method2.getParameterTypes();
                            Intrinsics.checkExpressionValueIsNotNull(parameterTypes, "it.parameterTypes");
                            if (Intrinsics.areEqual(ArraysKt___ArraysKt.singleOrNull(parameterTypes), Throwable.class)) {
                                b = true;
                                break Label_0094;
                            }
                        }
                        b = false;
                    }
                    if (b) {
                        method = method2;
                        return;
                    }
                }
                final Method method2 = null;
                continue;
            }
        }
    }
}
