// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.native.concurrent;

import kotlin.annotation.AnnotationTarget;
import kotlin.annotation.AnnotationRetention;
import kotlin.Metadata;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.lang.annotation.Annotation;

@Retention(RetentionPolicy.CLASS)
@Target({})
@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\n\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0000\b\u0087\"\u0018\u00002\u00020\u0001B\u0000¨\u0006\u0002" }, d2 = { "Lkotlin/native/concurrent/SharedImmutable;", "", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
@kotlin.annotation.Retention(AnnotationRetention.BINARY)
@kotlin.annotation.Target(allowedTargets = { AnnotationTarget.PROPERTY })
@interface SharedImmutable {
}
