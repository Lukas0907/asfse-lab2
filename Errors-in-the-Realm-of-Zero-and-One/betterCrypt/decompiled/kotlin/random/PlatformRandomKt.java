// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.random;

import kotlin.internal.PlatformImplementationsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0006\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\t\u0010\u0000\u001a\u00020\u0001H\u0081\b\u001a\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0005H\u0000\u001a\f\u0010\u0007\u001a\u00020\b*\u00020\u0001H\u0007\u001a\f\u0010\t\u001a\u00020\u0001*\u00020\bH\u0007¨\u0006\n" }, d2 = { "defaultPlatformRandom", "Lkotlin/random/Random;", "doubleFromParts", "", "hi26", "", "low27", "asJavaRandom", "Ljava/util/Random;", "asKotlinRandom", "kotlin-stdlib" }, k = 2, mv = { 1, 1, 15 })
public final class PlatformRandomKt
{
    public static final java.util.Random asJavaRandom(final Random random) {
        Intrinsics.checkParameterIsNotNull(random, "$this$asJavaRandom");
        Random random2;
        if (!(random instanceof AbstractPlatformRandom)) {
            random2 = null;
        }
        else {
            random2 = random;
        }
        final AbstractPlatformRandom abstractPlatformRandom = (AbstractPlatformRandom)random2;
        if (abstractPlatformRandom != null) {
            final java.util.Random impl = abstractPlatformRandom.getImpl();
            if (impl != null) {
                return impl;
            }
        }
        return new KotlinRandom(random);
    }
    
    public static final Random asKotlinRandom(final java.util.Random random) {
        Intrinsics.checkParameterIsNotNull(random, "$this$asKotlinRandom");
        java.util.Random random2;
        if (!(random instanceof KotlinRandom)) {
            random2 = null;
        }
        else {
            random2 = random;
        }
        final KotlinRandom kotlinRandom = (KotlinRandom)random2;
        if (kotlinRandom != null) {
            final Random impl = kotlinRandom.getImpl();
            if (impl != null) {
                return impl;
            }
        }
        return new PlatformRandom(random);
    }
    
    private static final Random defaultPlatformRandom() {
        return PlatformImplementationsKt.IMPLEMENTATIONS.defaultPlatformRandom();
    }
    
    public static final double doubleFromParts(final int n, final int n2) {
        return (((long)n << 27) + n2) / (double)9007199254740992L;
    }
}
