// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.random;

import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\r\b\u0000\u0018\u00002\u00020\u0001B\u0017\b\u0010\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005B7\b\u0000\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\u0006\u0010\u000b\u001a\u00020\u0003¢\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u0003H\u0016J\b\u0010\u000f\u001a\u00020\u0003H\u0016R\u000e\u0010\u000b\u001a\u00020\u0003X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0003X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0003X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0003X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0003X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0003X\u0082\u000e¢\u0006\u0002\n\u0000¨\u0006\u0010" }, d2 = { "Lkotlin/random/XorWowRandom;", "Lkotlin/random/Random;", "seed1", "", "seed2", "(II)V", "x", "y", "z", "w", "v", "addend", "(IIIIII)V", "nextBits", "bitCount", "nextInt", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
public final class XorWowRandom extends Random
{
    private int addend;
    private int v;
    private int w;
    private int x;
    private int y;
    private int z;
    
    public XorWowRandom(final int n, final int n2) {
        this(n, n2, 0, 0, n, n << 10 ^ n2 >>> 4);
    }
    
    public XorWowRandom(int i, int y, int y2, int z, int w, int v) {
        this.x = i;
        this.y = y;
        this.z = y2;
        this.w = z;
        this.v = w;
        this.addend = v;
        i = this.x;
        y2 = this.y;
        z = this.z;
        w = this.w;
        v = this.v;
        y = 0;
        if ((i | y2 | z | w | v) != 0x0) {
            i = 1;
        }
        else {
            i = 0;
        }
        if (i != 0) {
            for (i = y; i < 64; ++i) {
                this.nextInt();
            }
            return;
        }
        throw new IllegalArgumentException("Initial state must have at least one non-zero element.".toString());
    }
    
    @Override
    public int nextBits(final int n) {
        return RandomKt.takeUpperBits(this.nextInt(), n);
    }
    
    @Override
    public int nextInt() {
        final int x = this.x;
        final int n = x ^ x >>> 2;
        this.x = this.y;
        this.y = this.z;
        this.z = this.w;
        final int v = this.v;
        this.w = v;
        final int v2 = n ^ n << 1 ^ v ^ v << 4;
        this.v = v2;
        this.addend += 362437;
        return v2 + this.addend;
    }
}
