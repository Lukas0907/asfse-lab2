// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.random;

import kotlin.ranges.LongRange;
import kotlin.ranges.IntRange;
import kotlin.jvm.internal.Intrinsics;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000:\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\u0010\u0006\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0007\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0004H\u0007\u001a\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\bH\u0000\u001a\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0007\u001a\u00020\f2\u0006\u0010\t\u001a\u00020\fH\u0000\u001a\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0007\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\u0003H\u0000\u001a\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0007\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u0004H\u0000\u001a\u0010\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u0003H\u0000\u001a\u0014\u0010\u000f\u001a\u00020\u0003*\u00020\u00012\u0006\u0010\u0010\u001a\u00020\u0011H\u0007\u001a\u0014\u0010\u0012\u001a\u00020\u0004*\u00020\u00012\u0006\u0010\u0010\u001a\u00020\u0013H\u0007\u001a\u0014\u0010\u0014\u001a\u00020\u0003*\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u0003H\u0000¨\u0006\u0016" }, d2 = { "Random", "Lkotlin/random/Random;", "seed", "", "", "boundsErrorMessage", "", "from", "", "until", "checkRangeBounds", "", "", "fastLog2", "value", "nextInt", "range", "Lkotlin/ranges/IntRange;", "nextLong", "Lkotlin/ranges/LongRange;", "takeUpperBits", "bitCount", "kotlin-stdlib" }, k = 2, mv = { 1, 1, 15 })
public final class RandomKt
{
    public static final Random Random(final int n) {
        return new XorWowRandom(n, n >> 31);
    }
    
    public static final Random Random(final long n) {
        return new XorWowRandom((int)n, (int)(n >> 32));
    }
    
    public static final String boundsErrorMessage(final Object obj, final Object obj2) {
        Intrinsics.checkParameterIsNotNull(obj, "from");
        Intrinsics.checkParameterIsNotNull(obj2, "until");
        final StringBuilder sb = new StringBuilder();
        sb.append("Random range is empty: [");
        sb.append(obj);
        sb.append(", ");
        sb.append(obj2);
        sb.append(").");
        return sb.toString();
    }
    
    public static final void checkRangeBounds(final double d, final double d2) {
        if (d2 > d) {
            return;
        }
        throw new IllegalArgumentException(boundsErrorMessage(d, d2).toString());
    }
    
    public static final void checkRangeBounds(final int i, final int j) {
        if (j > i) {
            return;
        }
        throw new IllegalArgumentException(boundsErrorMessage(i, j).toString());
    }
    
    public static final void checkRangeBounds(final long l, final long i) {
        if (i > l) {
            return;
        }
        throw new IllegalArgumentException(boundsErrorMessage(l, i).toString());
    }
    
    public static final int fastLog2(final int i) {
        return 31 - Integer.numberOfLeadingZeros(i);
    }
    
    public static final int nextInt(final Random random, final IntRange obj) {
        Intrinsics.checkParameterIsNotNull(random, "$this$nextInt");
        Intrinsics.checkParameterIsNotNull(obj, "range");
        if (obj.isEmpty()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot get random in empty range: ");
            sb.append(obj);
            throw new IllegalArgumentException(sb.toString());
        }
        if (obj.getLast() < Integer.MAX_VALUE) {
            return random.nextInt(obj.getFirst(), obj.getLast() + 1);
        }
        if (obj.getFirst() > Integer.MIN_VALUE) {
            return random.nextInt(obj.getFirst() - 1, obj.getLast()) + 1;
        }
        return random.nextInt();
    }
    
    public static final long nextLong(final Random random, final LongRange obj) {
        Intrinsics.checkParameterIsNotNull(random, "$this$nextLong");
        Intrinsics.checkParameterIsNotNull(obj, "range");
        if (obj.isEmpty()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot get random in empty range: ");
            sb.append(obj);
            throw new IllegalArgumentException(sb.toString());
        }
        if (obj.getLast() < Long.MAX_VALUE) {
            return random.nextLong(obj.getStart(), obj.getEndInclusive() + 1L);
        }
        if (obj.getStart() > Long.MIN_VALUE) {
            return random.nextLong(obj.getStart() - 1L, obj.getEndInclusive()) + 1L;
        }
        return random.nextLong();
    }
    
    public static final int takeUpperBits(final int n, final int n2) {
        return n >>> 32 - n2 & -n2 >> 31;
    }
}
