// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.random;

import kotlin.ranges.ULongRange;
import kotlin.ranges.UIntRange;
import kotlin.UByteArray;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ULong;
import kotlin.UInt;
import kotlin.UnsignedKt;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000:\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\"\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0003H\u0001\u00f8\u0001\u0000¢\u0006\u0004\b\u0005\u0010\u0006\u001a\"\u0010\u0007\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\b2\u0006\u0010\u0004\u001a\u00020\bH\u0001\u00f8\u0001\u0000¢\u0006\u0004\b\t\u0010\n\u001a\u001c\u0010\u000b\u001a\u00020\f*\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0010\u001a\u001e\u0010\u000b\u001a\u00020\f*\u00020\r2\u0006\u0010\u0011\u001a\u00020\fH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0012\u0010\u0013\u001a2\u0010\u000b\u001a\u00020\f*\u00020\r2\u0006\u0010\u0011\u001a\u00020\f2\b\b\u0002\u0010\u0014\u001a\u00020\u000f2\b\b\u0002\u0010\u0015\u001a\u00020\u000fH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u0016\u0010\u0017\u001a\u0014\u0010\u0018\u001a\u00020\u0003*\u00020\rH\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\u0019\u001a\u001e\u0010\u0018\u001a\u00020\u0003*\u00020\r2\u0006\u0010\u0004\u001a\u00020\u0003H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u001a\u0010\u001b\u001a&\u0010\u0018\u001a\u00020\u0003*\u00020\r2\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0003H\u0007\u00f8\u0001\u0000¢\u0006\u0004\b\u001c\u0010\u001d\u001a\u001c\u0010\u0018\u001a\u00020\u0003*\u00020\r2\u0006\u0010\u001e\u001a\u00020\u001fH\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010 \u001a\u0014\u0010!\u001a\u00020\b*\u00020\rH\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010\"\u001a\u001e\u0010!\u001a\u00020\b*\u00020\r2\u0006\u0010\u0004\u001a\u00020\bH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b#\u0010$\u001a&\u0010!\u001a\u00020\b*\u00020\r2\u0006\u0010\u0002\u001a\u00020\b2\u0006\u0010\u0004\u001a\u00020\bH\u0007\u00f8\u0001\u0000¢\u0006\u0004\b%\u0010&\u001a\u001c\u0010!\u001a\u00020\b*\u00020\r2\u0006\u0010\u001e\u001a\u00020'H\u0007\u00f8\u0001\u0000¢\u0006\u0002\u0010(\u0082\u0002\u0004\n\u0002\b\u0019¨\u0006)" }, d2 = { "checkUIntRangeBounds", "", "from", "Lkotlin/UInt;", "until", "checkUIntRangeBounds-J1ME1BU", "(II)V", "checkULongRangeBounds", "Lkotlin/ULong;", "checkULongRangeBounds-eb3DHEI", "(JJ)V", "nextUBytes", "Lkotlin/UByteArray;", "Lkotlin/random/Random;", "size", "", "(Lkotlin/random/Random;I)[B", "array", "nextUBytes-EVgfTAA", "(Lkotlin/random/Random;[B)[B", "fromIndex", "toIndex", "nextUBytes-Wvrt4B4", "(Lkotlin/random/Random;[BII)[B", "nextUInt", "(Lkotlin/random/Random;)I", "nextUInt-qCasIEU", "(Lkotlin/random/Random;I)I", "nextUInt-a8DCA5k", "(Lkotlin/random/Random;II)I", "range", "Lkotlin/ranges/UIntRange;", "(Lkotlin/random/Random;Lkotlin/ranges/UIntRange;)I", "nextULong", "(Lkotlin/random/Random;)J", "nextULong-V1Xi4fY", "(Lkotlin/random/Random;J)J", "nextULong-jmpaW-c", "(Lkotlin/random/Random;JJ)J", "Lkotlin/ranges/ULongRange;", "(Lkotlin/random/Random;Lkotlin/ranges/ULongRange;)J", "kotlin-stdlib" }, k = 2, mv = { 1, 1, 15 })
public final class URandomKt
{
    public static final void checkUIntRangeBounds-J1ME1BU(final int n, final int n2) {
        if (UnsignedKt.uintCompare(n2, n) > 0) {
            return;
        }
        throw new IllegalArgumentException(RandomKt.boundsErrorMessage(UInt.box-impl(n), UInt.box-impl(n2)).toString());
    }
    
    public static final void checkULongRangeBounds-eb3DHEI(final long n, final long n2) {
        if (UnsignedKt.ulongCompare(n2, n) > 0) {
            return;
        }
        throw new IllegalArgumentException(RandomKt.boundsErrorMessage(ULong.box-impl(n), ULong.box-impl(n2)).toString());
    }
    
    public static final byte[] nextUBytes(final Random random, final int n) {
        Intrinsics.checkParameterIsNotNull(random, "$this$nextUBytes");
        return UByteArray.constructor-impl(random.nextBytes(n));
    }
    
    public static final byte[] nextUBytes-EVgfTAA(final Random random, final byte[] array) {
        Intrinsics.checkParameterIsNotNull(random, "$this$nextUBytes");
        Intrinsics.checkParameterIsNotNull(array, "array");
        random.nextBytes(array);
        return array;
    }
    
    public static final byte[] nextUBytes-Wvrt4B4(final Random random, final byte[] array, final int n, final int n2) {
        Intrinsics.checkParameterIsNotNull(random, "$this$nextUBytes");
        Intrinsics.checkParameterIsNotNull(array, "array");
        random.nextBytes(array, n, n2);
        return array;
    }
    
    public static final int nextUInt(final Random random) {
        Intrinsics.checkParameterIsNotNull(random, "$this$nextUInt");
        return UInt.constructor-impl(random.nextInt());
    }
    
    public static final int nextUInt(final Random random, final UIntRange obj) {
        Intrinsics.checkParameterIsNotNull(random, "$this$nextUInt");
        Intrinsics.checkParameterIsNotNull(obj, "range");
        if (obj.isEmpty()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot get random in empty range: ");
            sb.append(obj);
            throw new IllegalArgumentException(sb.toString());
        }
        if (UnsignedKt.uintCompare(obj.getLast(), -1) < 0) {
            return nextUInt-a8DCA5k(random, obj.getFirst(), UInt.constructor-impl(obj.getLast() + 1));
        }
        if (UnsignedKt.uintCompare(obj.getFirst(), 0) > 0) {
            return UInt.constructor-impl(nextUInt-a8DCA5k(random, UInt.constructor-impl(obj.getFirst() - 1), obj.getLast()) + 1);
        }
        return nextUInt(random);
    }
    
    public static final int nextUInt-a8DCA5k(final Random random, final int n, final int n2) {
        Intrinsics.checkParameterIsNotNull(random, "$this$nextUInt");
        checkUIntRangeBounds-J1ME1BU(n, n2);
        return UInt.constructor-impl(random.nextInt(n ^ Integer.MIN_VALUE, n2 ^ Integer.MIN_VALUE) ^ Integer.MIN_VALUE);
    }
    
    public static final int nextUInt-qCasIEU(final Random random, final int n) {
        Intrinsics.checkParameterIsNotNull(random, "$this$nextUInt");
        return nextUInt-a8DCA5k(random, 0, n);
    }
    
    public static final long nextULong(final Random random) {
        Intrinsics.checkParameterIsNotNull(random, "$this$nextULong");
        return ULong.constructor-impl(random.nextLong());
    }
    
    public static final long nextULong(final Random random, final ULongRange obj) {
        Intrinsics.checkParameterIsNotNull(random, "$this$nextULong");
        Intrinsics.checkParameterIsNotNull(obj, "range");
        if (obj.isEmpty()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot get random in empty range: ");
            sb.append(obj);
            throw new IllegalArgumentException(sb.toString());
        }
        if (UnsignedKt.ulongCompare(obj.getLast(), -1L) < 0) {
            return nextULong-jmpaW-c(random, obj.getFirst(), ULong.constructor-impl(obj.getLast() + ULong.constructor-impl((long)1 & 0xFFFFFFFFL)));
        }
        if (UnsignedKt.ulongCompare(obj.getFirst(), 0L) > 0) {
            final long first = obj.getFirst();
            final long n = (long)1 & 0xFFFFFFFFL;
            return ULong.constructor-impl(nextULong-jmpaW-c(random, ULong.constructor-impl(first - ULong.constructor-impl(n)), obj.getLast()) + ULong.constructor-impl(n));
        }
        return nextULong(random);
    }
    
    public static final long nextULong-V1Xi4fY(final Random random, final long n) {
        Intrinsics.checkParameterIsNotNull(random, "$this$nextULong");
        return nextULong-jmpaW-c(random, 0L, n);
    }
    
    public static final long nextULong-jmpaW-c(final Random random, final long n, final long n2) {
        Intrinsics.checkParameterIsNotNull(random, "$this$nextULong");
        checkULongRangeBounds-eb3DHEI(n, n2);
        return ULong.constructor-impl(random.nextLong(n ^ Long.MIN_VALUE, n2 ^ Long.MIN_VALUE) ^ Long.MIN_VALUE);
    }
}
