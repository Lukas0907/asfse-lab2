// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.random;

import kotlin.DeprecationLevel;
import kotlin.Deprecated;
import kotlin.jvm.internal.DoubleCompanionObject;
import kotlin.jvm.internal.Intrinsics;
import kotlin.internal.PlatformImplementationsKt;
import kotlin.Metadata;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0012\n\u0002\b\u0005\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\b'\u0018\u0000 \u00182\u00020\u0001:\u0002\u0017\u0018B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0004H&J\b\u0010\u0006\u001a\u00020\u0007H\u0016J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\tH\u0016J$\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\u000b\u001a\u00020\u00042\b\b\u0002\u0010\f\u001a\u00020\u0004H\u0016J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u0004H\u0016J\b\u0010\u000e\u001a\u00020\u000fH\u0016J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u000fH\u0016J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u000fH\u0016J\b\u0010\u0012\u001a\u00020\u0013H\u0016J\b\u0010\u0014\u001a\u00020\u0004H\u0016J\u0010\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u0004H\u0016J\u0018\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u0004H\u0016J\b\u0010\u0015\u001a\u00020\u0016H\u0016J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0010\u001a\u00020\u0016H\u0016J\u0018\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0011\u001a\u00020\u00162\u0006\u0010\u0010\u001a\u00020\u0016H\u0016¨\u0006\u0019" }, d2 = { "Lkotlin/random/Random;", "", "()V", "nextBits", "", "bitCount", "nextBoolean", "", "nextBytes", "", "array", "fromIndex", "toIndex", "size", "nextDouble", "", "until", "from", "nextFloat", "", "nextInt", "nextLong", "", "Companion", "Default", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
public abstract class Random
{
    public static final Companion Companion;
    public static final Default Default;
    private static final Random defaultRandom;
    
    static {
        Default = new Default(null);
        defaultRandom = PlatformImplementationsKt.IMPLEMENTATIONS.defaultPlatformRandom();
        Companion = Random.Companion.INSTANCE;
    }
    
    public static final /* synthetic */ Random access$getDefaultRandom$cp() {
        return Random.defaultRandom;
    }
    
    public abstract int nextBits(final int p0);
    
    public boolean nextBoolean() {
        return this.nextBits(1) != 0;
    }
    
    public byte[] nextBytes(final int n) {
        return this.nextBytes(new byte[n]);
    }
    
    public byte[] nextBytes(final byte[] array) {
        Intrinsics.checkParameterIsNotNull(array, "array");
        return this.nextBytes(array, 0, array.length);
    }
    
    public byte[] nextBytes(final byte[] array, int n, int i) {
        Intrinsics.checkParameterIsNotNull(array, "array");
        final int length = array.length;
        final int n2 = 0;
        final int n3 = 1;
        boolean b = false;
        Label_0055: {
            if (n >= 0) {
                if (length >= n) {
                    final int length2 = array.length;
                    if (i >= 0) {
                        if (length2 >= i) {
                            b = true;
                            break Label_0055;
                        }
                    }
                }
            }
            b = false;
        }
        if (!b) {
            final StringBuilder sb = new StringBuilder();
            sb.append("fromIndex (");
            sb.append(n);
            sb.append(") or toIndex (");
            sb.append(i);
            sb.append(") are out of range: 0..");
            sb.append(array.length);
            sb.append('.');
            throw new IllegalArgumentException(sb.toString().toString());
        }
        int n4;
        if (n <= i) {
            n4 = n3;
        }
        else {
            n4 = 0;
        }
        if (n4 != 0) {
            for (int n5 = (i - n) / 4, j = 0; j < n5; ++j) {
                final int nextInt = this.nextInt();
                array[n] = (byte)nextInt;
                array[n + 1] = (byte)(nextInt >>> 8);
                array[n + 2] = (byte)(nextInt >>> 16);
                array[n + 3] = (byte)(nextInt >>> 24);
                n += 4;
            }
            final int n6 = i - n;
            final int nextBits = this.nextBits(n6 * 8);
            for (i = n2; i < n6; ++i) {
                array[n + i] = (byte)(nextBits >>> i * 8);
            }
            return array;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("fromIndex (");
        sb2.append(n);
        sb2.append(") must be not greater than toIndex (");
        sb2.append(i);
        sb2.append(").");
        throw new IllegalArgumentException(sb2.toString().toString());
    }
    
    public double nextDouble() {
        return PlatformRandomKt.doubleFromParts(this.nextBits(26), this.nextBits(27));
    }
    
    public double nextDouble(final double n) {
        return this.nextDouble(0.0, n);
    }
    
    public double nextDouble(double n, final double start) {
        RandomKt.checkRangeBounds(n, start);
        final double v = start - n;
        Label_0127: {
            if (Double.isInfinite(v)) {
                final boolean infinite = Double.isInfinite(n);
                final int n2 = 1;
                if (!infinite && !Double.isNaN(n)) {
                    int n3;
                    if (!Double.isInfinite(start) && !Double.isNaN(start)) {
                        n3 = n2;
                    }
                    else {
                        n3 = 0;
                    }
                    if (n3 != 0) {
                        final double nextDouble = this.nextDouble();
                        final double n4 = 2;
                        final double n5 = nextDouble * (start / n4 - n / n4);
                        n = n + n5 + n5;
                        break Label_0127;
                    }
                }
            }
            n += this.nextDouble() * v;
        }
        double nextAfter = n;
        if (n >= start) {
            nextAfter = Math.nextAfter(start, DoubleCompanionObject.INSTANCE.getNEGATIVE_INFINITY());
        }
        return nextAfter;
    }
    
    public float nextFloat() {
        return this.nextBits(24) / (float)16777216;
    }
    
    public int nextInt() {
        return this.nextBits(32);
    }
    
    public int nextInt(final int n) {
        return this.nextInt(0, n);
    }
    
    public int nextInt(final int n, int nextBits) {
        RandomKt.checkRangeBounds(n, nextBits);
        final int n2 = nextBits - n;
        if (n2 <= 0 && n2 != Integer.MIN_VALUE) {
            int nextInt;
            while (true) {
                nextInt = this.nextInt();
                if (n > nextInt) {
                    continue;
                }
                if (nextBits > nextInt) {
                    break;
                }
            }
            return nextInt;
        }
        if ((-n2 & n2) == n2) {
            nextBits = this.nextBits(RandomKt.fastLog2(n2));
        }
        else {
            int n3;
            do {
                n3 = this.nextInt() >>> 1;
                nextBits = n3 % n2;
            } while (n3 - nextBits + (n2 - 1) < 0);
        }
        return n + nextBits;
    }
    
    public long nextLong() {
        return ((long)this.nextInt() << 32) + this.nextInt();
    }
    
    public long nextLong(final long n) {
        return this.nextLong(0L, n);
    }
    
    public long nextLong(final long n, long n2) {
        RandomKt.checkRangeBounds(n, n2);
        final long n3 = n2 - n;
        if (n3 > 0L) {
            if ((-n3 & n3) == n3) {
                final int n4 = (int)n3;
                final int n5 = (int)(n3 >>> 32);
                int n6;
                if (n4 != 0) {
                    n6 = this.nextBits(RandomKt.fastLog2(n4));
                }
                else {
                    if (n5 != 1) {
                        n2 = ((long)this.nextBits(RandomKt.fastLog2(n5)) << 32) + this.nextInt();
                        return n + n2;
                    }
                    n6 = this.nextInt();
                }
                n2 = ((long)n6 & 0xFFFFFFFFL);
            }
            else {
                long n7;
                do {
                    n7 = this.nextLong() >>> 1;
                    n2 = n7 % n3;
                } while (n7 - n2 + (n3 - 1L) < 0L);
            }
            return n + n2;
        }
        long nextLong;
        while (true) {
            nextLong = this.nextLong();
            if (n > nextLong) {
                continue;
            }
            if (n2 > nextLong) {
                break;
            }
        }
        return nextLong;
    }
    
    @Deprecated(level = DeprecationLevel.HIDDEN, message = "Use Default companion object instead")
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0004H\u0016¨\u0006\u0006" }, d2 = { "Lkotlin/random/Random$Companion;", "Lkotlin/random/Random;", "()V", "nextBits", "", "bitCount", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
    public static final class Companion extends Random
    {
        public static final Companion INSTANCE;
        
        static {
            INSTANCE = new Companion();
        }
        
        private Companion() {
        }
        
        @Override
        public int nextBits(final int n) {
            return Random.Default.nextBits(n);
        }
    }
    
    @Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0012\n\u0002\b\u0005\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\bH\u0016J\b\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\rH\u0016J \u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\b2\u0006\u0010\u0010\u001a\u00020\bH\u0016J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u0011\u001a\u00020\bH\u0016J\b\u0010\u0012\u001a\u00020\u0013H\u0016J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0013H\u0016J\u0018\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0013H\u0016J\b\u0010\u0016\u001a\u00020\u0017H\u0016J\b\u0010\u0018\u001a\u00020\bH\u0016J\u0010\u0010\u0018\u001a\u00020\b2\u0006\u0010\u0014\u001a\u00020\bH\u0016J\u0018\u0010\u0018\u001a\u00020\b2\u0006\u0010\u0015\u001a\u00020\b2\u0006\u0010\u0014\u001a\u00020\bH\u0016J\b\u0010\u0019\u001a\u00020\u001aH\u0016J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0014\u001a\u00020\u001aH\u0016J\u0018\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0015\u001a\u00020\u001a2\u0006\u0010\u0014\u001a\u00020\u001aH\u0016R\u0016\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004¢\u0006\b\n\u0000\u0012\u0004\b\u0005\u0010\u0002R\u000e\u0010\u0006\u001a\u00020\u0001X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001b" }, d2 = { "Lkotlin/random/Random$Default;", "Lkotlin/random/Random;", "()V", "Companion", "Lkotlin/random/Random$Companion;", "Companion$annotations", "defaultRandom", "nextBits", "", "bitCount", "nextBoolean", "", "nextBytes", "", "array", "fromIndex", "toIndex", "size", "nextDouble", "", "until", "from", "nextFloat", "", "nextInt", "nextLong", "", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
    public static final class Default extends Random
    {
        private Default() {
        }
        
        @Override
        public int nextBits(final int n) {
            return Random.access$getDefaultRandom$cp().nextBits(n);
        }
        
        @Override
        public boolean nextBoolean() {
            return Random.access$getDefaultRandom$cp().nextBoolean();
        }
        
        @Override
        public byte[] nextBytes(final int n) {
            return Random.access$getDefaultRandom$cp().nextBytes(n);
        }
        
        @Override
        public byte[] nextBytes(final byte[] array) {
            Intrinsics.checkParameterIsNotNull(array, "array");
            return Random.access$getDefaultRandom$cp().nextBytes(array);
        }
        
        @Override
        public byte[] nextBytes(final byte[] array, final int n, final int n2) {
            Intrinsics.checkParameterIsNotNull(array, "array");
            return Random.access$getDefaultRandom$cp().nextBytes(array, n, n2);
        }
        
        @Override
        public double nextDouble() {
            return Random.access$getDefaultRandom$cp().nextDouble();
        }
        
        @Override
        public double nextDouble(final double n) {
            return Random.access$getDefaultRandom$cp().nextDouble(n);
        }
        
        @Override
        public double nextDouble(final double n, final double n2) {
            return Random.access$getDefaultRandom$cp().nextDouble(n, n2);
        }
        
        @Override
        public float nextFloat() {
            return Random.access$getDefaultRandom$cp().nextFloat();
        }
        
        @Override
        public int nextInt() {
            return Random.access$getDefaultRandom$cp().nextInt();
        }
        
        @Override
        public int nextInt(final int n) {
            return Random.access$getDefaultRandom$cp().nextInt(n);
        }
        
        @Override
        public int nextInt(final int n, final int n2) {
            return Random.access$getDefaultRandom$cp().nextInt(n, n2);
        }
        
        @Override
        public long nextLong() {
            return Random.access$getDefaultRandom$cp().nextLong();
        }
        
        @Override
        public long nextLong(final long n) {
            return Random.access$getDefaultRandom$cp().nextLong(n);
        }
        
        @Override
        public long nextLong(final long n, final long n2) {
            return Random.access$getDefaultRandom$cp().nextLong(n, n2);
        }
    }
}
