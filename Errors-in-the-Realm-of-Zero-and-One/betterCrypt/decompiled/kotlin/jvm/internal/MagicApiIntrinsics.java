// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.jvm.internal;

public class MagicApiIntrinsics
{
    public static <T> T anyMagicApiCall(final int n) {
        return null;
    }
    
    public static <T> T anyMagicApiCall(final int n, final long n2, final long n3, final Object o) {
        return null;
    }
    
    public static <T> T anyMagicApiCall(final int n, final long n2, final Object o) {
        return null;
    }
    
    public static <T> T anyMagicApiCall(final int n, final Object o, final Object o2) {
        return null;
    }
    
    public static <T> T anyMagicApiCall(final int n, final Object o, final Object o2, final Object o3, final Object o4) {
        return null;
    }
    
    public static <T> T anyMagicApiCall(final Object o) {
        return null;
    }
    
    public static int intMagicApiCall(final int n) {
        return 0;
    }
    
    public static int intMagicApiCall(final int n, final long n2, final long n3, final Object o) {
        return 0;
    }
    
    public static int intMagicApiCall(final int n, final long n2, final Object o) {
        return 0;
    }
    
    public static int intMagicApiCall(final int n, final Object o, final Object o2) {
        return 0;
    }
    
    public static int intMagicApiCall(final int n, final Object o, final Object o2, final Object o3, final Object o4) {
        return 0;
    }
    
    public static int intMagicApiCall(final Object o) {
        return 0;
    }
    
    public static void voidMagicApiCall(final int n) {
    }
    
    public static void voidMagicApiCall(final Object o) {
    }
}
