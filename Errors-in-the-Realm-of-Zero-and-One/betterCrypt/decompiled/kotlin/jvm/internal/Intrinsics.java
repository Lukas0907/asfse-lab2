// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.jvm.internal;

import kotlin.UninitializedPropertyAccessException;
import kotlin.KotlinNullPointerException;
import java.util.Arrays;

public class Intrinsics
{
    private Intrinsics() {
    }
    
    public static boolean areEqual(final double n, final Double n2) {
        return n2 != null && n == n2;
    }
    
    public static boolean areEqual(final float n, final Float n2) {
        return n2 != null && n == n2;
    }
    
    public static boolean areEqual(final Double n, final double n2) {
        return n != null && n == n2;
    }
    
    public static boolean areEqual(final Double n, final Double n2) {
        if (n == null) {
            if (n2 == null) {
                return true;
            }
        }
        else if (n2 != null && n == (double)n2) {
            return true;
        }
        return false;
    }
    
    public static boolean areEqual(final Float n, final float n2) {
        return n != null && n == n2;
    }
    
    public static boolean areEqual(final Float n, final Float n2) {
        if (n == null) {
            if (n2 == null) {
                return true;
            }
        }
        else if (n2 != null && n == (float)n2) {
            return true;
        }
        return false;
    }
    
    public static boolean areEqual(final Object o, final Object obj) {
        if (o == null) {
            return obj == null;
        }
        return o.equals(obj);
    }
    
    public static void checkExpressionValueIsNotNull(final Object o, final String str) {
        if (o != null) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" must not be null");
        throw sanitizeStackTrace(new IllegalStateException(sb.toString()));
    }
    
    public static void checkFieldIsNotNull(final Object o, final String s) {
        if (o != null) {
            return;
        }
        throw sanitizeStackTrace(new IllegalStateException(s));
    }
    
    public static void checkFieldIsNotNull(final Object o, final String str, final String str2) {
        if (o != null) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Field specified as non-null is null: ");
        sb.append(str);
        sb.append(".");
        sb.append(str2);
        throw sanitizeStackTrace(new IllegalStateException(sb.toString()));
    }
    
    public static void checkHasClass(String replace) throws ClassNotFoundException {
        replace = replace.replace('/', '.');
        try {
            Class.forName(replace);
        }
        catch (ClassNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Class ");
            sb.append(replace);
            sb.append(" is not found. Please update the Kotlin runtime to the latest version");
            throw sanitizeStackTrace(new ClassNotFoundException(sb.toString(), ex));
        }
    }
    
    public static void checkHasClass(String replace, final String str) throws ClassNotFoundException {
        replace = replace.replace('/', '.');
        try {
            Class.forName(replace);
        }
        catch (ClassNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Class ");
            sb.append(replace);
            sb.append(" is not found: this code requires the Kotlin runtime of version at least ");
            sb.append(str);
            throw sanitizeStackTrace(new ClassNotFoundException(sb.toString(), ex));
        }
    }
    
    public static void checkNotNull(final Object o) {
        if (o == null) {
            throwNpe();
        }
    }
    
    public static void checkNotNull(final Object o, final String s) {
        if (o == null) {
            throwNpe(s);
        }
    }
    
    public static void checkNotNullExpressionValue(final Object o, final String s) {
        if (o != null) {
            return;
        }
        throw sanitizeStackTrace(new IllegalStateException(s));
    }
    
    public static void checkNotNullParameter(final Object o, final String s) {
        if (o != null) {
            return;
        }
        throw sanitizeStackTrace(new IllegalArgumentException(s));
    }
    
    public static void checkParameterIsNotNull(final Object o, final String s) {
        if (o == null) {
            throwParameterIsNullException(s);
        }
    }
    
    public static void checkReturnedValueIsNotNull(final Object o, final String s) {
        if (o != null) {
            return;
        }
        throw sanitizeStackTrace(new IllegalStateException(s));
    }
    
    public static void checkReturnedValueIsNotNull(final Object o, final String str, final String str2) {
        if (o != null) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Method specified as non-null returned null: ");
        sb.append(str);
        sb.append(".");
        sb.append(str2);
        throw sanitizeStackTrace(new IllegalStateException(sb.toString()));
    }
    
    public static int compare(final int n, final int n2) {
        if (n < n2) {
            return -1;
        }
        if (n == n2) {
            return 0;
        }
        return 1;
    }
    
    public static int compare(final long n, final long n2) {
        final long n3 = lcmp(n, n2);
        if (n3 < 0) {
            return -1;
        }
        if (n3 == 0) {
            return 0;
        }
        return 1;
    }
    
    public static void needClassReification() {
        throwUndefinedForReified();
    }
    
    public static void needClassReification(final String s) {
        throwUndefinedForReified(s);
    }
    
    public static void reifiedOperationMarker(final int n, final String s) {
        throwUndefinedForReified();
    }
    
    public static void reifiedOperationMarker(final int n, final String s, final String s2) {
        throwUndefinedForReified(s2);
    }
    
    private static <T extends Throwable> T sanitizeStackTrace(final T t) {
        return sanitizeStackTrace(t, Intrinsics.class.getName());
    }
    
    static <T extends Throwable> T sanitizeStackTrace(final T t, final String s) {
        final StackTraceElement[] stackTrace = t.getStackTrace();
        final int length = stackTrace.length;
        int n = -1;
        for (int i = 0; i < length; ++i) {
            if (s.equals(stackTrace[i].getClassName())) {
                n = i;
            }
        }
        t.setStackTrace(Arrays.copyOfRange(stackTrace, n + 1, length));
        return t;
    }
    
    public static String stringPlus(final String str, final Object obj) {
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(obj);
        return sb.toString();
    }
    
    public static void throwAssert() {
        throw sanitizeStackTrace(new AssertionError());
    }
    
    public static void throwAssert(final String detailMessage) {
        throw sanitizeStackTrace(new AssertionError((Object)detailMessage));
    }
    
    public static void throwIllegalArgument() {
        throw sanitizeStackTrace(new IllegalArgumentException());
    }
    
    public static void throwIllegalArgument(final String s) {
        throw sanitizeStackTrace(new IllegalArgumentException(s));
    }
    
    public static void throwIllegalState() {
        throw sanitizeStackTrace(new IllegalStateException());
    }
    
    public static void throwIllegalState(final String s) {
        throw sanitizeStackTrace(new IllegalStateException(s));
    }
    
    public static void throwNpe() {
        throw sanitizeStackTrace(new KotlinNullPointerException());
    }
    
    public static void throwNpe(final String s) {
        throw sanitizeStackTrace(new KotlinNullPointerException(s));
    }
    
    private static void throwParameterIsNullException(final String str) {
        final StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];
        final String className = stackTraceElement.getClassName();
        final String methodName = stackTraceElement.getMethodName();
        final StringBuilder sb = new StringBuilder();
        sb.append("Parameter specified as non-null is null: method ");
        sb.append(className);
        sb.append(".");
        sb.append(methodName);
        sb.append(", parameter ");
        sb.append(str);
        throw sanitizeStackTrace(new IllegalArgumentException(sb.toString()));
    }
    
    public static void throwUndefinedForReified() {
        throwUndefinedForReified("This function has a reified type parameter and thus can only be inlined at compilation time, not called directly.");
    }
    
    public static void throwUndefinedForReified(final String message) {
        throw new UnsupportedOperationException(message);
    }
    
    public static void throwUninitializedProperty(final String s) {
        throw sanitizeStackTrace(new UninitializedPropertyAccessException(s));
    }
    
    public static void throwUninitializedPropertyAccessException(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("lateinit property ");
        sb.append(str);
        sb.append(" has not been initialized");
        throwUninitializedProperty(sb.toString());
    }
}
