// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.jvm.internal;

public class InlineMarker
{
    public static void afterInlineCall() {
    }
    
    public static void beforeInlineCall() {
    }
    
    public static void finallyEnd(final int n) {
    }
    
    public static void finallyStart(final int n) {
    }
    
    public static void mark(final int n) {
    }
    
    public static void mark(final String s) {
    }
}
