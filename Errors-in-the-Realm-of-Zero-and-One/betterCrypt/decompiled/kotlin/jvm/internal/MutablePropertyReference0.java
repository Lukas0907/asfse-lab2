// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.jvm.internal;

import kotlin.reflect.KMutableProperty;
import kotlin.reflect.KProperty0;
import kotlin.reflect.KProperty;
import kotlin.reflect.KCallable;
import kotlin.reflect.KMutableProperty0;

public abstract class MutablePropertyReference0 extends MutablePropertyReference implements KMutableProperty0
{
    public MutablePropertyReference0() {
    }
    
    public MutablePropertyReference0(final Object o) {
        super(o);
    }
    
    @Override
    protected KCallable computeReflected() {
        return Reflection.mutableProperty0(this);
    }
    
    @Override
    public Object getDelegate() {
        return ((KMutableProperty0)this.getReflected()).getDelegate();
    }
    
    @Override
    public KProperty0.Getter getGetter() {
        return (KProperty0.Getter)((KMutableProperty0)this.getReflected()).getGetter();
    }
    
    @Override
    public KMutableProperty0.Setter getSetter() {
        return (KMutableProperty0.Setter)((KMutableProperty0)this.getReflected()).getSetter();
    }
    
    @Override
    public Object invoke() {
        return this.get();
    }
}
