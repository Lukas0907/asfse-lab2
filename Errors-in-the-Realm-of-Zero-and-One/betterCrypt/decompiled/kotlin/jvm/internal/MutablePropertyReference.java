// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.jvm.internal;

import kotlin.reflect.KMutableProperty;

public abstract class MutablePropertyReference extends PropertyReference implements KMutableProperty
{
    public MutablePropertyReference() {
    }
    
    public MutablePropertyReference(final Object o) {
        super(o);
    }
}
