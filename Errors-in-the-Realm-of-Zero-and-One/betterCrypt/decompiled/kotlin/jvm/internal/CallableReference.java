// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.jvm.internal;

import java.io.ObjectStreamException;
import kotlin.reflect.KVisibility;
import kotlin.reflect.KTypeParameter;
import kotlin.reflect.KType;
import kotlin.jvm.KotlinReflectionNotSupportedError;
import kotlin.reflect.KDeclarationContainer;
import java.lang.annotation.Annotation;
import java.util.List;
import kotlin.reflect.KParameter;
import java.util.Map;
import java.io.Serializable;
import kotlin.reflect.KCallable;

public abstract class CallableReference implements KCallable, Serializable
{
    public static final Object NO_RECEIVER;
    protected final Object receiver;
    private transient KCallable reflected;
    
    static {
        NO_RECEIVER = NoReceiver.INSTANCE;
    }
    
    public CallableReference() {
        this(CallableReference.NO_RECEIVER);
    }
    
    protected CallableReference(final Object receiver) {
        this.receiver = receiver;
    }
    
    @Override
    public Object call(final Object... array) {
        return this.getReflected().call(array);
    }
    
    @Override
    public Object callBy(final Map map) {
        return this.getReflected().callBy(map);
    }
    
    public KCallable compute() {
        KCallable reflected;
        if ((reflected = this.reflected) == null) {
            reflected = this.computeReflected();
            this.reflected = reflected;
        }
        return reflected;
    }
    
    protected abstract KCallable computeReflected();
    
    @Override
    public List<Annotation> getAnnotations() {
        return (List<Annotation>)this.getReflected().getAnnotations();
    }
    
    public Object getBoundReceiver() {
        return this.receiver;
    }
    
    @Override
    public String getName() {
        throw new AbstractMethodError();
    }
    
    public KDeclarationContainer getOwner() {
        throw new AbstractMethodError();
    }
    
    @Override
    public List<KParameter> getParameters() {
        return (List<KParameter>)this.getReflected().getParameters();
    }
    
    protected KCallable getReflected() {
        final KCallable compute = this.compute();
        if (compute != this) {
            return compute;
        }
        throw new KotlinReflectionNotSupportedError();
    }
    
    @Override
    public KType getReturnType() {
        return this.getReflected().getReturnType();
    }
    
    public String getSignature() {
        throw new AbstractMethodError();
    }
    
    @Override
    public List<KTypeParameter> getTypeParameters() {
        return (List<KTypeParameter>)this.getReflected().getTypeParameters();
    }
    
    @Override
    public KVisibility getVisibility() {
        return this.getReflected().getVisibility();
    }
    
    @Override
    public boolean isAbstract() {
        return this.getReflected().isAbstract();
    }
    
    @Override
    public boolean isFinal() {
        return this.getReflected().isFinal();
    }
    
    @Override
    public boolean isOpen() {
        return this.getReflected().isOpen();
    }
    
    @Override
    public boolean isSuspend() {
        return this.getReflected().isSuspend();
    }
    
    private static class NoReceiver implements Serializable
    {
        private static final NoReceiver INSTANCE;
        
        static {
            INSTANCE = new NoReceiver();
        }
        
        private Object readResolve() throws ObjectStreamException {
            return NoReceiver.INSTANCE;
        }
    }
}
