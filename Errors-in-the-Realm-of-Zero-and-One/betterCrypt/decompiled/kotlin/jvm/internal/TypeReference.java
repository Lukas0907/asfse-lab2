// 
// Decompiled by Procyon v0.5.36
// 

package kotlin.jvm.internal;

import java.lang.annotation.Annotation;
import kotlin.reflect.KVariance;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.JvmClassMappingKt;
import kotlin.reflect.KClass;
import kotlin.reflect.KClassifier;
import kotlin.reflect.KTypeProjection;
import java.util.List;
import kotlin.Metadata;
import kotlin.reflect.KType;

@Metadata(bv = { 1, 0, 3 }, d1 = { "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u001b\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\b\u0010\u0017\u001a\u00020\u0013H\u0002J\u0013\u0010\u0018\u001a\u00020\b2\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0096\u0002J\b\u0010\u001b\u001a\u00020\u001cH\u0016J\b\u0010\u001d\u001a\u00020\u0013H\u0016J\f\u0010\u0017\u001a\u00020\u0013*\u00020\u0006H\u0002R\u001a\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00058VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\rR\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0007\u001a\u00020\bX\u0096\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\u0011R\u001c\u0010\u0012\u001a\u00020\u0013*\u0006\u0012\u0002\b\u00030\u00148BX\u0082\u0004¢\u0006\u0006\u001a\u0004\b\u0015\u0010\u0016¨\u0006\u001e" }, d2 = { "Lkotlin/jvm/internal/TypeReference;", "Lkotlin/reflect/KType;", "classifier", "Lkotlin/reflect/KClassifier;", "arguments", "", "Lkotlin/reflect/KTypeProjection;", "isMarkedNullable", "", "(Lkotlin/reflect/KClassifier;Ljava/util/List;Z)V", "annotations", "", "getAnnotations", "()Ljava/util/List;", "getArguments", "getClassifier", "()Lkotlin/reflect/KClassifier;", "()Z", "arrayClassName", "", "Ljava/lang/Class;", "getArrayClassName", "(Ljava/lang/Class;)Ljava/lang/String;", "asString", "equals", "other", "", "hashCode", "", "toString", "kotlin-stdlib" }, k = 1, mv = { 1, 1, 15 })
public final class TypeReference implements KType
{
    private final List<KTypeProjection> arguments;
    private final KClassifier classifier;
    private final boolean isMarkedNullable;
    
    public TypeReference(final KClassifier classifier, final List<KTypeProjection> arguments, final boolean isMarkedNullable) {
        Intrinsics.checkParameterIsNotNull(classifier, "classifier");
        Intrinsics.checkParameterIsNotNull(arguments, "arguments");
        this.classifier = classifier;
        this.arguments = arguments;
        this.isMarkedNullable = isMarkedNullable;
    }
    
    private final String asString() {
        KClassifier classifier = this.getClassifier();
        final boolean b = classifier instanceof KClass;
        final Class<?> clazz = null;
        if (!b) {
            classifier = null;
        }
        final KClass<Object> kClass = (KClass<Object>)classifier;
        Class<?> javaClass = clazz;
        if (kClass != null) {
            javaClass = JvmClassMappingKt.getJavaClass(kClass);
        }
        String str;
        if (javaClass == null) {
            str = this.getClassifier().toString();
        }
        else if (javaClass.isArray()) {
            str = this.getArrayClassName(javaClass);
        }
        else {
            str = javaClass.getName();
        }
        final boolean empty = this.getArguments().isEmpty();
        String str2 = "";
        String joinToString$default;
        if (empty) {
            joinToString$default = "";
        }
        else {
            joinToString$default = CollectionsKt___CollectionsKt.joinToString$default(this.getArguments(), ", ", "<", ">", 0, null, (Function1)new TypeReference$asString$args.TypeReference$asString$args$1(this), 24, null);
        }
        if (this.isMarkedNullable()) {
            str2 = "?";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(joinToString$default);
        sb.append(str2);
        return sb.toString();
    }
    
    private final String asString(final KTypeProjection kTypeProjection) {
        if (kTypeProjection.getVariance() == null) {
            return "*";
        }
        KType type;
        if (!((type = kTypeProjection.getType()) instanceof TypeReference)) {
            type = null;
        }
        final TypeReference typeReference = (TypeReference)type;
        String s = null;
        Label_0058: {
            if (typeReference != null) {
                s = typeReference.asString();
                if (s != null) {
                    break Label_0058;
                }
            }
            s = String.valueOf(kTypeProjection.getType());
        }
        final KVariance variance = kTypeProjection.getVariance();
        if (variance != null) {
            final int n = TypeReference$WhenMappings.$EnumSwitchMapping$0[variance.ordinal()];
            String string = s;
            if (n != 1) {
                if (n != 2) {
                    if (n == 3) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("out ");
                        sb.append(s);
                        return sb.toString();
                    }
                    throw new NoWhenBranchMatchedException();
                }
                else {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("in ");
                    sb2.append(s);
                    string = sb2.toString();
                }
            }
            return string;
        }
        throw new NoWhenBranchMatchedException();
    }
    
    private final String getArrayClassName(final Class<?> clazz) {
        if (Intrinsics.areEqual(clazz, boolean[].class)) {
            return "kotlin.BooleanArray";
        }
        if (Intrinsics.areEqual(clazz, char[].class)) {
            return "kotlin.CharArray";
        }
        if (Intrinsics.areEqual(clazz, byte[].class)) {
            return "kotlin.ByteArray";
        }
        if (Intrinsics.areEqual(clazz, short[].class)) {
            return "kotlin.ShortArray";
        }
        if (Intrinsics.areEqual(clazz, int[].class)) {
            return "kotlin.IntArray";
        }
        if (Intrinsics.areEqual(clazz, float[].class)) {
            return "kotlin.FloatArray";
        }
        if (Intrinsics.areEqual(clazz, long[].class)) {
            return "kotlin.LongArray";
        }
        if (Intrinsics.areEqual(clazz, double[].class)) {
            return "kotlin.DoubleArray";
        }
        return "kotlin.Array";
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof TypeReference) {
            final KClassifier classifier = this.getClassifier();
            final TypeReference typeReference = (TypeReference)o;
            if (Intrinsics.areEqual(classifier, typeReference.getClassifier()) && Intrinsics.areEqual(this.getArguments(), typeReference.getArguments()) && this.isMarkedNullable() == typeReference.isMarkedNullable()) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public List<Annotation> getAnnotations() {
        return CollectionsKt__CollectionsKt.emptyList();
    }
    
    @Override
    public List<KTypeProjection> getArguments() {
        return this.arguments;
    }
    
    @Override
    public KClassifier getClassifier() {
        return this.classifier;
    }
    
    @Override
    public int hashCode() {
        return (this.getClassifier().hashCode() * 31 + this.getArguments().hashCode()) * 31 + Boolean.valueOf(this.isMarkedNullable()).hashCode();
    }
    
    @Override
    public boolean isMarkedNullable() {
        return this.isMarkedNullable;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.asString());
        sb.append(" (Kotlin reflection is not available)");
        return sb.toString();
    }
}
