// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.snackbar;

public interface ContentViewCallback
{
    void animateContentIn(final int p0, final int p1);
    
    void animateContentOut(final int p0, final int p1);
}
