// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.snackbar;

import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.view.MotionEvent;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.lang.annotation.Annotation;
import android.view.ViewGroup$LayoutParams;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import android.view.ViewParent;
import com.google.android.material.R;
import com.google.android.material.behavior.SwipeDismissBehavior;
import java.util.ArrayList;
import android.animation.ValueAnimator$AnimatorUpdateListener;
import android.animation.Animator$AnimatorListener;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.google.android.material.animation.AnimationUtils;
import android.animation.ValueAnimator;
import androidx.core.view.WindowInsetsCompat;
import androidx.core.view.OnApplyWindowInsetsListener;
import androidx.core.view.ViewCompat;
import android.view.LayoutInflater;
import com.google.android.material.internal.ThemeEnforcement;
import android.view.View;
import android.os.Message;
import android.os.Handler$Callback;
import android.os.Looper;
import android.os.Build$VERSION;
import android.view.ViewGroup;
import android.content.Context;
import java.util.List;
import android.view.accessibility.AccessibilityManager;
import android.os.Handler;

public abstract class BaseTransientBottomBar<B extends BaseTransientBottomBar<B>>
{
    static final int ANIMATION_DURATION = 250;
    static final int ANIMATION_FADE_DURATION = 180;
    public static final int LENGTH_INDEFINITE = -2;
    public static final int LENGTH_LONG = 0;
    public static final int LENGTH_SHORT = -1;
    static final int MSG_DISMISS = 1;
    static final int MSG_SHOW = 0;
    private static final boolean USE_OFFSET_API;
    static final Handler handler;
    private final AccessibilityManager accessibilityManager;
    private Behavior behavior;
    private List<BaseCallback<B>> callbacks;
    private final com.google.android.material.snackbar.ContentViewCallback contentViewCallback;
    private final Context context;
    private int duration;
    final SnackbarManager.Callback managerCallback;
    private final ViewGroup targetParent;
    protected final SnackbarBaseLayout view;
    
    static {
        USE_OFFSET_API = (Build$VERSION.SDK_INT >= 16 && Build$VERSION.SDK_INT <= 19);
        handler = new Handler(Looper.getMainLooper(), (Handler$Callback)new Handler$Callback() {
            public boolean handleMessage(final Message message) {
                final int what = message.what;
                if (what == 0) {
                    ((BaseTransientBottomBar)message.obj).showView();
                    return true;
                }
                if (what != 1) {
                    return false;
                }
                ((BaseTransientBottomBar)message.obj).hideView(message.arg1);
                return true;
            }
        });
    }
    
    protected BaseTransientBottomBar(final ViewGroup targetParent, final View view, final com.google.android.material.snackbar.ContentViewCallback contentViewCallback) {
        this.managerCallback = new SnackbarManager.Callback() {
            @Override
            public void dismiss(final int n) {
                BaseTransientBottomBar.handler.sendMessage(BaseTransientBottomBar.handler.obtainMessage(1, n, 0, (Object)BaseTransientBottomBar.this));
            }
            
            @Override
            public void show() {
                BaseTransientBottomBar.handler.sendMessage(BaseTransientBottomBar.handler.obtainMessage(0, (Object)BaseTransientBottomBar.this));
            }
        };
        if (targetParent == null) {
            throw new IllegalArgumentException("Transient bottom bar must have non-null parent");
        }
        if (view == null) {
            throw new IllegalArgumentException("Transient bottom bar must have non-null content");
        }
        if (contentViewCallback != null) {
            this.targetParent = targetParent;
            this.contentViewCallback = contentViewCallback;
            ThemeEnforcement.checkAppCompatTheme(this.context = targetParent.getContext());
            (this.view = (SnackbarBaseLayout)LayoutInflater.from(this.context).inflate(this.getSnackbarBaseLayoutResId(), this.targetParent, false)).addView(view);
            ViewCompat.setAccessibilityLiveRegion((View)this.view, 1);
            ViewCompat.setImportantForAccessibility((View)this.view, 1);
            ViewCompat.setFitsSystemWindows((View)this.view, true);
            ViewCompat.setOnApplyWindowInsetsListener((View)this.view, new OnApplyWindowInsetsListener() {
                @Override
                public WindowInsetsCompat onApplyWindowInsets(final View view, final WindowInsetsCompat windowInsetsCompat) {
                    view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), windowInsetsCompat.getSystemWindowInsetBottom());
                    return windowInsetsCompat;
                }
            });
            this.accessibilityManager = (AccessibilityManager)this.context.getSystemService("accessibility");
            return;
        }
        throw new IllegalArgumentException("Transient bottom bar must have non-null callback");
    }
    
    private void animateViewOut(final int n) {
        final ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setIntValues(new int[] { 0, this.view.getHeight() });
        valueAnimator.setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR);
        valueAnimator.setDuration(250L);
        valueAnimator.addListener((Animator$AnimatorListener)new AnimatorListenerAdapter() {
            public void onAnimationEnd(final Animator animator) {
                BaseTransientBottomBar.this.onViewHidden(n);
            }
            
            public void onAnimationStart(final Animator animator) {
                BaseTransientBottomBar.this.contentViewCallback.animateContentOut(0, 180);
            }
        });
        valueAnimator.addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener() {
            private int previousAnimatedIntValue = 0;
            
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                final int intValue = (int)valueAnimator.getAnimatedValue();
                if (BaseTransientBottomBar.USE_OFFSET_API) {
                    ViewCompat.offsetTopAndBottom((View)BaseTransientBottomBar.this.view, intValue - this.previousAnimatedIntValue);
                }
                else {
                    BaseTransientBottomBar.this.view.setTranslationY((float)intValue);
                }
                this.previousAnimatedIntValue = intValue;
            }
        });
        valueAnimator.start();
    }
    
    public B addCallback(final BaseCallback<B> baseCallback) {
        if (baseCallback == null) {
            return (B)this;
        }
        if (this.callbacks == null) {
            this.callbacks = new ArrayList<BaseCallback<B>>();
        }
        this.callbacks.add(baseCallback);
        return (B)this;
    }
    
    void animateViewIn() {
        final int height = this.view.getHeight();
        if (BaseTransientBottomBar.USE_OFFSET_API) {
            ViewCompat.offsetTopAndBottom((View)this.view, height);
        }
        else {
            this.view.setTranslationY((float)height);
        }
        final ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setIntValues(new int[] { height, 0 });
        valueAnimator.setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR);
        valueAnimator.setDuration(250L);
        valueAnimator.addListener((Animator$AnimatorListener)new AnimatorListenerAdapter() {
            public void onAnimationEnd(final Animator animator) {
                BaseTransientBottomBar.this.onViewShown();
            }
            
            public void onAnimationStart(final Animator animator) {
                BaseTransientBottomBar.this.contentViewCallback.animateContentIn(70, 180);
            }
        });
        valueAnimator.addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener() {
            private int previousAnimatedIntValue = height;
            
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                final int intValue = (int)valueAnimator.getAnimatedValue();
                if (BaseTransientBottomBar.USE_OFFSET_API) {
                    ViewCompat.offsetTopAndBottom((View)BaseTransientBottomBar.this.view, intValue - this.previousAnimatedIntValue);
                }
                else {
                    BaseTransientBottomBar.this.view.setTranslationY((float)intValue);
                }
                this.previousAnimatedIntValue = intValue;
            }
        });
        valueAnimator.start();
    }
    
    public void dismiss() {
        this.dispatchDismiss(3);
    }
    
    protected void dispatchDismiss(final int n) {
        SnackbarManager.getInstance().dismiss(this.managerCallback, n);
    }
    
    public Behavior getBehavior() {
        return this.behavior;
    }
    
    public Context getContext() {
        return this.context;
    }
    
    public int getDuration() {
        return this.duration;
    }
    
    protected SwipeDismissBehavior<? extends View> getNewBehavior() {
        return new Behavior();
    }
    
    protected int getSnackbarBaseLayoutResId() {
        return R.layout.design_layout_snackbar;
    }
    
    public View getView() {
        return (View)this.view;
    }
    
    final void hideView(final int n) {
        if (this.shouldAnimate() && this.view.getVisibility() == 0) {
            this.animateViewOut(n);
            return;
        }
        this.onViewHidden(n);
    }
    
    public boolean isShown() {
        return SnackbarManager.getInstance().isCurrent(this.managerCallback);
    }
    
    public boolean isShownOrQueued() {
        return SnackbarManager.getInstance().isCurrentOrNext(this.managerCallback);
    }
    
    void onViewHidden(final int n) {
        SnackbarManager.getInstance().onDismissed(this.managerCallback);
        final List<BaseCallback<B>> callbacks = this.callbacks;
        if (callbacks != null) {
            for (int i = callbacks.size() - 1; i >= 0; --i) {
                this.callbacks.get(i).onDismissed((B)this, n);
            }
        }
        final ViewParent parent = this.view.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup)parent).removeView((View)this.view);
        }
    }
    
    void onViewShown() {
        SnackbarManager.getInstance().onShown(this.managerCallback);
        final List<BaseCallback<B>> callbacks = this.callbacks;
        if (callbacks != null) {
            for (int i = callbacks.size() - 1; i >= 0; --i) {
                this.callbacks.get(i).onShown((B)this);
            }
        }
    }
    
    public B removeCallback(final BaseCallback<B> baseCallback) {
        if (baseCallback == null) {
            return (B)this;
        }
        final List<BaseCallback<B>> callbacks = this.callbacks;
        if (callbacks == null) {
            return (B)this;
        }
        callbacks.remove(baseCallback);
        return (B)this;
    }
    
    public B setBehavior(final Behavior behavior) {
        this.behavior = behavior;
        return (B)this;
    }
    
    public B setDuration(final int duration) {
        this.duration = duration;
        return (B)this;
    }
    
    boolean shouldAnimate() {
        final List enabledAccessibilityServiceList = this.accessibilityManager.getEnabledAccessibilityServiceList(1);
        return enabledAccessibilityServiceList != null && enabledAccessibilityServiceList.isEmpty();
    }
    
    public void show() {
        SnackbarManager.getInstance().show(this.duration, this.managerCallback);
    }
    
    final void showView() {
        if (this.view.getParent() == null) {
            final ViewGroup$LayoutParams layoutParams = this.view.getLayoutParams();
            if (layoutParams instanceof CoordinatorLayout.LayoutParams) {
                final CoordinatorLayout.LayoutParams layoutParams2 = (CoordinatorLayout.LayoutParams)layoutParams;
                SwipeDismissBehavior<? extends View> behavior;
                if ((behavior = this.behavior) == null) {
                    behavior = this.getNewBehavior();
                }
                if (behavior instanceof Behavior) {
                    ((Behavior)behavior).setBaseTransientBottomBar(this);
                }
                behavior.setListener((SwipeDismissBehavior.OnDismissListener)new SwipeDismissBehavior.OnDismissListener() {
                    @Override
                    public void onDismiss(final View view) {
                        view.setVisibility(8);
                        BaseTransientBottomBar.this.dispatchDismiss(0);
                    }
                    
                    @Override
                    public void onDragStateChanged(final int n) {
                        if (n == 0) {
                            SnackbarManager.getInstance().restoreTimeoutIfPaused(BaseTransientBottomBar.this.managerCallback);
                            return;
                        }
                        if (n != 1 && n != 2) {
                            return;
                        }
                        SnackbarManager.getInstance().pauseTimeout(BaseTransientBottomBar.this.managerCallback);
                    }
                });
                layoutParams2.setBehavior(behavior);
                layoutParams2.insetEdge = 80;
            }
            this.targetParent.addView((View)this.view);
        }
        this.view.setOnAttachStateChangeListener(new OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(final View view) {
            }
            
            @Override
            public void onViewDetachedFromWindow(final View view) {
                if (BaseTransientBottomBar.this.isShownOrQueued()) {
                    BaseTransientBottomBar.handler.post((Runnable)new Runnable() {
                        @Override
                        public void run() {
                            BaseTransientBottomBar.this.onViewHidden(3);
                        }
                    });
                }
            }
        });
        if (!ViewCompat.isLaidOut((View)this.view)) {
            this.view.setOnLayoutChangeListener(new OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(final View view, final int n, final int n2, final int n3, final int n4) {
                    BaseTransientBottomBar.this.view.setOnLayoutChangeListener(null);
                    if (BaseTransientBottomBar.this.shouldAnimate()) {
                        BaseTransientBottomBar.this.animateViewIn();
                        return;
                    }
                    BaseTransientBottomBar.this.onViewShown();
                }
            });
            return;
        }
        if (this.shouldAnimate()) {
            this.animateViewIn();
            return;
        }
        this.onViewShown();
    }
    
    public abstract static class BaseCallback<B>
    {
        public static final int DISMISS_EVENT_ACTION = 1;
        public static final int DISMISS_EVENT_CONSECUTIVE = 4;
        public static final int DISMISS_EVENT_MANUAL = 3;
        public static final int DISMISS_EVENT_SWIPE = 0;
        public static final int DISMISS_EVENT_TIMEOUT = 2;
        
        public void onDismissed(final B b, final int n) {
        }
        
        public void onShown(final B b) {
        }
        
        @Retention(RetentionPolicy.SOURCE)
        public @interface DismissEvent {
        }
    }
    
    public static class Behavior extends SwipeDismissBehavior<View>
    {
        private final BehaviorDelegate delegate;
        
        public Behavior() {
            this.delegate = new BehaviorDelegate(this);
        }
        
        private void setBaseTransientBottomBar(final BaseTransientBottomBar<?> baseTransientBottomBar) {
            this.delegate.setBaseTransientBottomBar(baseTransientBottomBar);
        }
        
        @Override
        public boolean canSwipeDismissView(final View view) {
            return this.delegate.canSwipeDismissView(view);
        }
        
        @Override
        public boolean onInterceptTouchEvent(final CoordinatorLayout coordinatorLayout, final View view, final MotionEvent motionEvent) {
            this.delegate.onInterceptTouchEvent(coordinatorLayout, view, motionEvent);
            return super.onInterceptTouchEvent(coordinatorLayout, view, motionEvent);
        }
    }
    
    public static class BehaviorDelegate
    {
        private SnackbarManager.Callback managerCallback;
        
        public BehaviorDelegate(final SwipeDismissBehavior<?> swipeDismissBehavior) {
            swipeDismissBehavior.setStartAlphaSwipeDistance(0.1f);
            swipeDismissBehavior.setEndAlphaSwipeDistance(0.6f);
            swipeDismissBehavior.setSwipeDirection(0);
        }
        
        public boolean canSwipeDismissView(final View view) {
            return view instanceof SnackbarBaseLayout;
        }
        
        public void onInterceptTouchEvent(final CoordinatorLayout coordinatorLayout, final View view, final MotionEvent motionEvent) {
            final int actionMasked = motionEvent.getActionMasked();
            if (actionMasked == 0) {
                if (coordinatorLayout.isPointInChildBounds(view, (int)motionEvent.getX(), (int)motionEvent.getY())) {
                    SnackbarManager.getInstance().pauseTimeout(this.managerCallback);
                }
                return;
            }
            if (actionMasked != 1 && actionMasked != 3) {
                return;
            }
            SnackbarManager.getInstance().restoreTimeoutIfPaused(this.managerCallback);
        }
        
        public void setBaseTransientBottomBar(final BaseTransientBottomBar<?> baseTransientBottomBar) {
            this.managerCallback = baseTransientBottomBar.managerCallback;
        }
    }
    
    @Deprecated
    public interface ContentViewCallback extends com.google.android.material.snackbar.ContentViewCallback
    {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface Duration {
    }
    
    protected interface OnAttachStateChangeListener
    {
        void onViewAttachedToWindow(final View p0);
        
        void onViewDetachedFromWindow(final View p0);
    }
    
    protected interface OnLayoutChangeListener
    {
        void onLayoutChange(final View p0, final int p1, final int p2, final int p3, final int p4);
    }
    
    protected static class SnackbarBaseLayout extends FrameLayout
    {
        private OnAttachStateChangeListener onAttachStateChangeListener;
        private OnLayoutChangeListener onLayoutChangeListener;
        
        protected SnackbarBaseLayout(final Context context) {
            this(context, null);
        }
        
        protected SnackbarBaseLayout(final Context context, final AttributeSet set) {
            super(context, set);
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.SnackbarLayout);
            if (obtainStyledAttributes.hasValue(R.styleable.SnackbarLayout_elevation)) {
                ViewCompat.setElevation((View)this, (float)obtainStyledAttributes.getDimensionPixelSize(R.styleable.SnackbarLayout_elevation, 0));
            }
            obtainStyledAttributes.recycle();
            this.setClickable(true);
        }
        
        protected void onAttachedToWindow() {
            super.onAttachedToWindow();
            final OnAttachStateChangeListener onAttachStateChangeListener = this.onAttachStateChangeListener;
            if (onAttachStateChangeListener != null) {
                onAttachStateChangeListener.onViewAttachedToWindow((View)this);
            }
            ViewCompat.requestApplyInsets((View)this);
        }
        
        protected void onDetachedFromWindow() {
            super.onDetachedFromWindow();
            final OnAttachStateChangeListener onAttachStateChangeListener = this.onAttachStateChangeListener;
            if (onAttachStateChangeListener != null) {
                onAttachStateChangeListener.onViewDetachedFromWindow((View)this);
            }
        }
        
        protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
            super.onLayout(b, n, n2, n3, n4);
            final OnLayoutChangeListener onLayoutChangeListener = this.onLayoutChangeListener;
            if (onLayoutChangeListener != null) {
                onLayoutChangeListener.onLayoutChange((View)this, n, n2, n3, n4);
            }
        }
        
        void setOnAttachStateChangeListener(final OnAttachStateChangeListener onAttachStateChangeListener) {
            this.onAttachStateChangeListener = onAttachStateChangeListener;
        }
        
        void setOnLayoutChangeListener(final OnLayoutChangeListener onLayoutChangeListener) {
            this.onLayoutChangeListener = onLayoutChangeListener;
        }
    }
}
