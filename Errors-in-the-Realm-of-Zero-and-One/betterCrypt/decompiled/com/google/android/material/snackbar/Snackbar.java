// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.snackbar;

import android.widget.TextView;
import android.view.View$MeasureSpec;
import android.util.AttributeSet;
import android.content.Context;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.lang.annotation.Annotation;
import android.content.res.ColorStateList;
import android.widget.Button;
import android.text.TextUtils;
import android.view.View$OnClickListener;
import com.google.android.material.R;
import com.google.android.material.internal.ThemeEnforcement;
import android.view.LayoutInflater;
import android.view.ViewParent;
import android.widget.FrameLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import android.view.View;
import android.view.ViewGroup;

public final class Snackbar extends BaseTransientBottomBar<Snackbar>
{
    public static final int LENGTH_INDEFINITE = -2;
    public static final int LENGTH_LONG = 0;
    public static final int LENGTH_SHORT = -1;
    private BaseCallback<Snackbar> callback;
    
    private Snackbar(final ViewGroup viewGroup, final View view, final com.google.android.material.snackbar.ContentViewCallback contentViewCallback) {
        super(viewGroup, view, contentViewCallback);
    }
    
    private static ViewGroup findSuitableParent(View view) {
        ViewGroup viewGroup = null;
        View view2 = view;
        while (!(view2 instanceof CoordinatorLayout)) {
            ViewGroup viewGroup2 = viewGroup;
            if (view2 instanceof FrameLayout) {
                if (view2.getId() == 16908290) {
                    return (ViewGroup)view2;
                }
                viewGroup2 = (ViewGroup)view2;
            }
            if ((view = view2) != null) {
                final ViewParent parent = view2.getParent();
                if (parent instanceof View) {
                    view = (View)parent;
                }
                else {
                    view = null;
                }
            }
            viewGroup = viewGroup2;
            if ((view2 = view) == null) {
                return viewGroup2;
            }
        }
        return (ViewGroup)view2;
    }
    
    public static Snackbar make(final View view, final int n, final int n2) {
        return make(view, view.getResources().getText(n), n2);
    }
    
    public static Snackbar make(final View view, final CharSequence text, final int duration) {
        final ViewGroup suitableParent = findSuitableParent(view);
        if (suitableParent != null) {
            final LayoutInflater from = LayoutInflater.from(suitableParent.getContext());
            int n;
            if (ThemeEnforcement.isMaterialTheme(suitableParent.getContext())) {
                n = R.layout.design_layout_snackbar_include_material;
            }
            else {
                n = R.layout.design_layout_snackbar_include;
            }
            final SnackbarContentLayout snackbarContentLayout = (SnackbarContentLayout)from.inflate(n, suitableParent, false);
            final Snackbar snackbar = new Snackbar(suitableParent, (View)snackbarContentLayout, snackbarContentLayout);
            snackbar.setText(text);
            snackbar.setDuration(duration);
            return snackbar;
        }
        throw new IllegalArgumentException("No suitable parent found from the given view. Please provide a valid view.");
    }
    
    @Override
    public void dismiss() {
        super.dismiss();
    }
    
    @Override
    public boolean isShown() {
        return super.isShown();
    }
    
    public Snackbar setAction(final int n, final View$OnClickListener view$OnClickListener) {
        return this.setAction(this.getContext().getText(n), view$OnClickListener);
    }
    
    public Snackbar setAction(final CharSequence text, final View$OnClickListener view$OnClickListener) {
        final Button actionView = ((SnackbarContentLayout)this.view.getChildAt(0)).getActionView();
        if (!TextUtils.isEmpty(text) && view$OnClickListener != null) {
            ((TextView)actionView).setVisibility(0);
            ((TextView)actionView).setText(text);
            ((TextView)actionView).setOnClickListener((View$OnClickListener)new View$OnClickListener() {
                public void onClick(final View view) {
                    view$OnClickListener.onClick(view);
                    Snackbar.this.dispatchDismiss(1);
                }
            });
            return this;
        }
        ((TextView)actionView).setVisibility(8);
        ((TextView)actionView).setOnClickListener((View$OnClickListener)null);
        return this;
    }
    
    public Snackbar setActionTextColor(final int textColor) {
        ((TextView)((SnackbarContentLayout)this.view.getChildAt(0)).getActionView()).setTextColor(textColor);
        return this;
    }
    
    public Snackbar setActionTextColor(final ColorStateList textColor) {
        ((TextView)((SnackbarContentLayout)this.view.getChildAt(0)).getActionView()).setTextColor(textColor);
        return this;
    }
    
    @Deprecated
    public Snackbar setCallback(final Callback callback) {
        final BaseCallback<Snackbar> callback2 = this.callback;
        if (callback2 != null) {
            this.removeCallback(callback2);
        }
        if (callback != null) {
            this.addCallback((BaseCallback<Snackbar>)callback);
        }
        this.callback = callback;
        return this;
    }
    
    public Snackbar setText(final int n) {
        return this.setText(this.getContext().getText(n));
    }
    
    public Snackbar setText(final CharSequence text) {
        ((SnackbarContentLayout)this.view.getChildAt(0)).getMessageView().setText(text);
        return this;
    }
    
    @Override
    public void show() {
        super.show();
    }
    
    public static class Callback extends BaseCallback<Snackbar>
    {
        public static final int DISMISS_EVENT_ACTION = 1;
        public static final int DISMISS_EVENT_CONSECUTIVE = 4;
        public static final int DISMISS_EVENT_MANUAL = 3;
        public static final int DISMISS_EVENT_SWIPE = 0;
        public static final int DISMISS_EVENT_TIMEOUT = 2;
        
        public void onDismissed(final Snackbar snackbar, final int n) {
        }
        
        public void onShown(final Snackbar snackbar) {
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface Duration {
    }
    
    public static final class SnackbarLayout extends SnackbarBaseLayout
    {
        public SnackbarLayout(final Context context) {
            super(context);
        }
        
        public SnackbarLayout(final Context context, final AttributeSet set) {
            super(context, set);
        }
        
        protected void onMeasure(int i, int childCount) {
            super.onMeasure(i, childCount);
            childCount = this.getChildCount();
            final int measuredWidth = this.getMeasuredWidth();
            final int paddingLeft = this.getPaddingLeft();
            final int paddingRight = this.getPaddingRight();
            View child;
            for (i = 0; i < childCount; ++i) {
                child = this.getChildAt(i);
                if (child.getLayoutParams().width == -1) {
                    child.measure(View$MeasureSpec.makeMeasureSpec(measuredWidth - paddingLeft - paddingRight, 1073741824), View$MeasureSpec.makeMeasureSpec(child.getMeasuredHeight(), 1073741824));
                }
            }
        }
    }
}
