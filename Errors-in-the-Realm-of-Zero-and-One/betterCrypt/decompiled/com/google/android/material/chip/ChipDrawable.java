// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.chip;

import java.util.Arrays;
import androidx.core.text.BidiFormatter;
import androidx.appcompat.content.res.AppCompatResources;
import android.graphics.Outline;
import com.google.android.material.canvas.CanvasCompat;
import com.google.android.material.ripple.RippleUtils;
import com.google.android.material.drawable.DrawableUtils;
import android.content.res.TypedArray;
import com.google.android.material.resources.MaterialResources;
import com.google.android.material.internal.ThemeEnforcement;
import android.graphics.Paint$Align;
import androidx.core.graphics.ColorUtils;
import android.graphics.Canvas;
import android.content.res.XmlResourceParser;
import android.content.res.Resources$NotFoundException;
import java.io.IOException;
import com.google.android.material.R;
import org.xmlpull.v1.XmlPullParser;
import android.util.Xml;
import android.text.TextUtils;
import org.xmlpull.v1.XmlPullParserException;
import android.util.AttributeSet;
import android.graphics.Rect;
import androidx.core.graphics.drawable.DrawableCompat;
import android.graphics.Paint$Style;
import android.text.TextUtils$TruncateAt;
import android.graphics.PorterDuff$Mode;
import android.graphics.PorterDuffColorFilter;
import android.text.TextPaint;
import com.google.android.material.resources.TextAppearance;
import android.graphics.RectF;
import android.graphics.PointF;
import com.google.android.material.animation.MotionSpec;
import android.graphics.Paint$FontMetrics;
import java.lang.ref.WeakReference;
import android.content.Context;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable$Callback;
import androidx.core.graphics.drawable.TintAwareDrawable;
import android.graphics.drawable.Drawable;

public class ChipDrawable extends Drawable implements TintAwareDrawable, Drawable$Callback
{
    private static final boolean DEBUG = false;
    private static final int[] DEFAULT_STATE;
    private int alpha;
    private boolean checkable;
    private Drawable checkedIcon;
    private boolean checkedIconEnabled;
    private ColorStateList chipBackgroundColor;
    private float chipCornerRadius;
    private float chipEndPadding;
    private Drawable chipIcon;
    private boolean chipIconEnabled;
    private float chipIconSize;
    private ColorStateList chipIconTint;
    private float chipMinHeight;
    private final Paint chipPaint;
    private float chipStartPadding;
    private ColorStateList chipStrokeColor;
    private float chipStrokeWidth;
    private Drawable closeIcon;
    private CharSequence closeIconContentDescription;
    private boolean closeIconEnabled;
    private float closeIconEndPadding;
    private float closeIconSize;
    private float closeIconStartPadding;
    private int[] closeIconStateSet;
    private ColorStateList closeIconTint;
    private ColorFilter colorFilter;
    private ColorStateList compatRippleColor;
    private final Context context;
    private boolean currentChecked;
    private int currentChipBackgroundColor;
    private int currentChipStrokeColor;
    private int currentCompatRippleColor;
    private int currentTextColor;
    private int currentTint;
    private final Paint debugPaint;
    private WeakReference<Delegate> delegate;
    private final Paint$FontMetrics fontMetrics;
    private MotionSpec hideMotionSpec;
    private float iconEndPadding;
    private float iconStartPadding;
    private final PointF pointF;
    private CharSequence rawText;
    private final RectF rectF;
    private ColorStateList rippleColor;
    private boolean shouldDrawText;
    private MotionSpec showMotionSpec;
    private TextAppearance textAppearance;
    private float textEndPadding;
    private final TextPaint textPaint;
    private float textStartPadding;
    private float textWidth;
    private boolean textWidthDirty;
    private ColorStateList tint;
    private PorterDuffColorFilter tintFilter;
    private PorterDuff$Mode tintMode;
    private TextUtils$TruncateAt truncateAt;
    private CharSequence unicodeWrappedText;
    private boolean useCompatRipple;
    
    static {
        DEFAULT_STATE = new int[] { 16842910 };
    }
    
    private ChipDrawable(final Context context) {
        this.textPaint = new TextPaint(1);
        this.chipPaint = new Paint(1);
        this.fontMetrics = new Paint$FontMetrics();
        this.rectF = new RectF();
        this.pointF = new PointF();
        this.alpha = 255;
        this.tintMode = PorterDuff$Mode.SRC_IN;
        this.delegate = new WeakReference<Delegate>(null);
        this.textWidthDirty = true;
        this.context = context;
        this.rawText = "";
        this.textPaint.density = context.getResources().getDisplayMetrics().density;
        this.debugPaint = null;
        final Paint debugPaint = this.debugPaint;
        if (debugPaint != null) {
            debugPaint.setStyle(Paint$Style.STROKE);
        }
        this.setState(ChipDrawable.DEFAULT_STATE);
        this.setCloseIconState(ChipDrawable.DEFAULT_STATE);
        this.shouldDrawText = true;
    }
    
    private void applyChildDrawable(final Drawable drawable) {
        if (drawable != null) {
            drawable.setCallback((Drawable$Callback)this);
            DrawableCompat.setLayoutDirection(drawable, DrawableCompat.getLayoutDirection(this));
            drawable.setLevel(this.getLevel());
            drawable.setVisible(this.isVisible(), false);
            if (drawable == this.closeIcon) {
                if (drawable.isStateful()) {
                    drawable.setState(this.getCloseIconState());
                }
                DrawableCompat.setTintList(drawable, this.closeIconTint);
                return;
            }
            if (drawable.isStateful()) {
                drawable.setState(this.getState());
            }
        }
    }
    
    private void calculateChipIconBounds(final Rect rect, final RectF rectF) {
        rectF.setEmpty();
        if (this.showsChipIcon() || this.showsCheckedIcon()) {
            final float n = this.chipStartPadding + this.iconStartPadding;
            if (DrawableCompat.getLayoutDirection(this) == 0) {
                rectF.left = rect.left + n;
                rectF.right = rectF.left + this.chipIconSize;
            }
            else {
                rectF.right = rect.right - n;
                rectF.left = rectF.right - this.chipIconSize;
            }
            rectF.top = rect.exactCenterY() - this.chipIconSize / 2.0f;
            rectF.bottom = rectF.top + this.chipIconSize;
        }
    }
    
    private void calculateChipTouchBounds(final Rect rect, final RectF rectF) {
        rectF.set(rect);
        if (this.showsCloseIcon()) {
            final float n = this.chipEndPadding + this.closeIconEndPadding + this.closeIconSize + this.closeIconStartPadding + this.textEndPadding;
            if (DrawableCompat.getLayoutDirection(this) == 0) {
                rectF.right = rect.right - n;
                return;
            }
            rectF.left = rect.left + n;
        }
    }
    
    private void calculateCloseIconBounds(final Rect rect, final RectF rectF) {
        rectF.setEmpty();
        if (this.showsCloseIcon()) {
            final float n = this.chipEndPadding + this.closeIconEndPadding;
            if (DrawableCompat.getLayoutDirection(this) == 0) {
                rectF.right = rect.right - n;
                rectF.left = rectF.right - this.closeIconSize;
            }
            else {
                rectF.left = rect.left + n;
                rectF.right = rectF.left + this.closeIconSize;
            }
            rectF.top = rect.exactCenterY() - this.closeIconSize / 2.0f;
            rectF.bottom = rectF.top + this.closeIconSize;
        }
    }
    
    private void calculateCloseIconTouchBounds(final Rect rect, final RectF rectF) {
        rectF.setEmpty();
        if (this.showsCloseIcon()) {
            final float n = this.chipEndPadding + this.closeIconEndPadding + this.closeIconSize + this.closeIconStartPadding + this.textEndPadding;
            if (DrawableCompat.getLayoutDirection(this) == 0) {
                rectF.right = (float)rect.right;
                rectF.left = rectF.right - n;
            }
            else {
                rectF.left = (float)rect.left;
                rectF.right = rect.left + n;
            }
            rectF.top = (float)rect.top;
            rectF.bottom = (float)rect.bottom;
        }
    }
    
    private float calculateCloseIconWidth() {
        if (this.showsCloseIcon()) {
            return this.closeIconStartPadding + this.closeIconSize + this.closeIconEndPadding;
        }
        return 0.0f;
    }
    
    private void calculateTextBounds(final Rect rect, final RectF rectF) {
        rectF.setEmpty();
        if (this.unicodeWrappedText != null) {
            final float n = this.chipStartPadding + this.calculateChipIconWidth() + this.textStartPadding;
            final float n2 = this.chipEndPadding + this.calculateCloseIconWidth() + this.textEndPadding;
            if (DrawableCompat.getLayoutDirection(this) == 0) {
                rectF.left = rect.left + n;
                rectF.right = rect.right - n2;
            }
            else {
                rectF.left = rect.left + n2;
                rectF.right = rect.right - n;
            }
            rectF.top = (float)rect.top;
            rectF.bottom = (float)rect.bottom;
        }
    }
    
    private float calculateTextCenterFromBaseline() {
        this.textPaint.getFontMetrics(this.fontMetrics);
        return (this.fontMetrics.descent + this.fontMetrics.ascent) / 2.0f;
    }
    
    private float calculateTextWidth(final CharSequence charSequence) {
        if (charSequence == null) {
            return 0.0f;
        }
        return this.textPaint.measureText(charSequence, 0, charSequence.length());
    }
    
    private boolean canShowCheckedIcon() {
        return this.checkedIconEnabled && this.checkedIcon != null && this.checkable;
    }
    
    public static ChipDrawable createFromAttributes(final Context context, final AttributeSet set, final int n, final int n2) {
        final ChipDrawable chipDrawable = new ChipDrawable(context);
        chipDrawable.loadFromAttributes(set, n, n2);
        return chipDrawable;
    }
    
    public static ChipDrawable createFromResource(final Context ex, final int i) {
        try {
            final XmlResourceParser xml = ((Context)ex).getResources().getXml(i);
            int next;
            do {
                next = ((XmlPullParser)xml).next();
            } while (next != 2 && next != 1);
            if (next != 2) {
                throw new XmlPullParserException("No start tag found");
            }
            if (TextUtils.equals((CharSequence)((XmlPullParser)xml).getName(), (CharSequence)"chip")) {
                final AttributeSet attributeSet = Xml.asAttributeSet((XmlPullParser)xml);
                int n;
                if ((n = attributeSet.getStyleAttribute()) == 0) {
                    n = R.style.Widget_MaterialComponents_Chip_Entry;
                }
                return createFromAttributes((Context)ex, attributeSet, R.attr.chipStandaloneStyle, n);
            }
            throw new XmlPullParserException("Must have a <chip> start tag");
        }
        catch (IOException ex) {}
        catch (XmlPullParserException ex3) {}
        final StringBuilder sb = new StringBuilder();
        sb.append("Can't load chip resource ID #0x");
        sb.append(Integer.toHexString(i));
        final Resources$NotFoundException ex2 = new Resources$NotFoundException(sb.toString());
        ex2.initCause((Throwable)ex);
        throw ex2;
    }
    
    private void drawCheckedIcon(final Canvas canvas, final Rect rect) {
        if (this.showsCheckedIcon()) {
            this.calculateChipIconBounds(rect, this.rectF);
            final float left = this.rectF.left;
            final float top = this.rectF.top;
            canvas.translate(left, top);
            this.checkedIcon.setBounds(0, 0, (int)this.rectF.width(), (int)this.rectF.height());
            this.checkedIcon.draw(canvas);
            canvas.translate(-left, -top);
        }
    }
    
    private void drawChipBackground(final Canvas canvas, final Rect rect) {
        this.chipPaint.setColor(this.currentChipBackgroundColor);
        this.chipPaint.setStyle(Paint$Style.FILL);
        this.chipPaint.setColorFilter(this.getTintColorFilter());
        this.rectF.set(rect);
        final RectF rectF = this.rectF;
        final float chipCornerRadius = this.chipCornerRadius;
        canvas.drawRoundRect(rectF, chipCornerRadius, chipCornerRadius, this.chipPaint);
    }
    
    private void drawChipIcon(final Canvas canvas, final Rect rect) {
        if (this.showsChipIcon()) {
            this.calculateChipIconBounds(rect, this.rectF);
            final float left = this.rectF.left;
            final float top = this.rectF.top;
            canvas.translate(left, top);
            this.chipIcon.setBounds(0, 0, (int)this.rectF.width(), (int)this.rectF.height());
            this.chipIcon.draw(canvas);
            canvas.translate(-left, -top);
        }
    }
    
    private void drawChipStroke(final Canvas canvas, final Rect rect) {
        if (this.chipStrokeWidth > 0.0f) {
            this.chipPaint.setColor(this.currentChipStrokeColor);
            this.chipPaint.setStyle(Paint$Style.STROKE);
            this.chipPaint.setColorFilter(this.getTintColorFilter());
            this.rectF.set(rect.left + this.chipStrokeWidth / 2.0f, rect.top + this.chipStrokeWidth / 2.0f, rect.right - this.chipStrokeWidth / 2.0f, rect.bottom - this.chipStrokeWidth / 2.0f);
            final float n = this.chipCornerRadius - this.chipStrokeWidth / 2.0f;
            canvas.drawRoundRect(this.rectF, n, n, this.chipPaint);
        }
    }
    
    private void drawCloseIcon(final Canvas canvas, final Rect rect) {
        if (this.showsCloseIcon()) {
            this.calculateCloseIconBounds(rect, this.rectF);
            final float left = this.rectF.left;
            final float top = this.rectF.top;
            canvas.translate(left, top);
            this.closeIcon.setBounds(0, 0, (int)this.rectF.width(), (int)this.rectF.height());
            this.closeIcon.draw(canvas);
            canvas.translate(-left, -top);
        }
    }
    
    private void drawCompatRipple(final Canvas canvas, final Rect rect) {
        this.chipPaint.setColor(this.currentCompatRippleColor);
        this.chipPaint.setStyle(Paint$Style.FILL);
        this.rectF.set(rect);
        final RectF rectF = this.rectF;
        final float chipCornerRadius = this.chipCornerRadius;
        canvas.drawRoundRect(rectF, chipCornerRadius, chipCornerRadius, this.chipPaint);
    }
    
    private void drawDebug(final Canvas canvas, final Rect rect) {
        final Paint debugPaint = this.debugPaint;
        if (debugPaint != null) {
            debugPaint.setColor(ColorUtils.setAlphaComponent(-16777216, 127));
            canvas.drawRect(rect, this.debugPaint);
            if (this.showsChipIcon() || this.showsCheckedIcon()) {
                this.calculateChipIconBounds(rect, this.rectF);
                canvas.drawRect(this.rectF, this.debugPaint);
            }
            if (this.unicodeWrappedText != null) {
                canvas.drawLine((float)rect.left, rect.exactCenterY(), (float)rect.right, rect.exactCenterY(), this.debugPaint);
            }
            if (this.showsCloseIcon()) {
                this.calculateCloseIconBounds(rect, this.rectF);
                canvas.drawRect(this.rectF, this.debugPaint);
            }
            this.debugPaint.setColor(ColorUtils.setAlphaComponent(-65536, 127));
            this.calculateChipTouchBounds(rect, this.rectF);
            canvas.drawRect(this.rectF, this.debugPaint);
            this.debugPaint.setColor(ColorUtils.setAlphaComponent(-16711936, 127));
            this.calculateCloseIconTouchBounds(rect, this.rectF);
            canvas.drawRect(this.rectF, this.debugPaint);
        }
    }
    
    private void drawText(final Canvas canvas, final Rect rect) {
        if (this.unicodeWrappedText != null) {
            final Paint$Align calculateTextOriginAndAlignment = this.calculateTextOriginAndAlignment(rect, this.pointF);
            this.calculateTextBounds(rect, this.rectF);
            if (this.textAppearance != null) {
                this.textPaint.drawableState = this.getState();
                this.textAppearance.updateDrawState(this.context, this.textPaint);
            }
            this.textPaint.setTextAlign(calculateTextOriginAndAlignment);
            final int round = Math.round(this.getTextWidth());
            final int round2 = Math.round(this.rectF.width());
            int save = 0;
            final boolean b = round > round2;
            if (b) {
                save = canvas.save();
                canvas.clipRect(this.rectF);
            }
            CharSequence charSequence2;
            final CharSequence charSequence = charSequence2 = this.unicodeWrappedText;
            if (b) {
                charSequence2 = charSequence;
                if (this.truncateAt != null) {
                    charSequence2 = TextUtils.ellipsize(charSequence, this.textPaint, this.rectF.width(), this.truncateAt);
                }
            }
            canvas.drawText(charSequence2, 0, charSequence2.length(), this.pointF.x, this.pointF.y, (Paint)this.textPaint);
            if (b) {
                canvas.restoreToCount(save);
            }
        }
    }
    
    private float getTextWidth() {
        if (!this.textWidthDirty) {
            return this.textWidth;
        }
        this.textWidth = this.calculateTextWidth(this.unicodeWrappedText);
        this.textWidthDirty = false;
        return this.textWidth;
    }
    
    private ColorFilter getTintColorFilter() {
        final ColorFilter colorFilter = this.colorFilter;
        if (colorFilter != null) {
            return colorFilter;
        }
        return (ColorFilter)this.tintFilter;
    }
    
    private static boolean hasState(final int[] array, final int n) {
        if (array == null) {
            return false;
        }
        for (int length = array.length, i = 0; i < length; ++i) {
            if (array[i] == n) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean isStateful(final ColorStateList list) {
        return list != null && list.isStateful();
    }
    
    private static boolean isStateful(final Drawable drawable) {
        return drawable != null && drawable.isStateful();
    }
    
    private static boolean isStateful(final TextAppearance textAppearance) {
        return textAppearance != null && textAppearance.textColor != null && textAppearance.textColor.isStateful();
    }
    
    private void loadFromAttributes(final AttributeSet set, int int1, final int n) {
        final TypedArray obtainStyledAttributes = ThemeEnforcement.obtainStyledAttributes(this.context, set, R.styleable.Chip, int1, n);
        this.setChipBackgroundColor(MaterialResources.getColorStateList(this.context, obtainStyledAttributes, R.styleable.Chip_chipBackgroundColor));
        this.setChipMinHeight(obtainStyledAttributes.getDimension(R.styleable.Chip_chipMinHeight, 0.0f));
        this.setChipCornerRadius(obtainStyledAttributes.getDimension(R.styleable.Chip_chipCornerRadius, 0.0f));
        this.setChipStrokeColor(MaterialResources.getColorStateList(this.context, obtainStyledAttributes, R.styleable.Chip_chipStrokeColor));
        this.setChipStrokeWidth(obtainStyledAttributes.getDimension(R.styleable.Chip_chipStrokeWidth, 0.0f));
        this.setRippleColor(MaterialResources.getColorStateList(this.context, obtainStyledAttributes, R.styleable.Chip_rippleColor));
        this.setText(obtainStyledAttributes.getText(R.styleable.Chip_android_text));
        this.setTextAppearance(MaterialResources.getTextAppearance(this.context, obtainStyledAttributes, R.styleable.Chip_android_textAppearance));
        int1 = obtainStyledAttributes.getInt(R.styleable.Chip_android_ellipsize, 0);
        if (int1 != 1) {
            if (int1 != 2) {
                if (int1 == 3) {
                    this.setEllipsize(TextUtils$TruncateAt.END);
                }
            }
            else {
                this.setEllipsize(TextUtils$TruncateAt.MIDDLE);
            }
        }
        else {
            this.setEllipsize(TextUtils$TruncateAt.START);
        }
        this.setChipIconEnabled(obtainStyledAttributes.getBoolean(R.styleable.Chip_chipIconEnabled, false));
        this.setChipIcon(MaterialResources.getDrawable(this.context, obtainStyledAttributes, R.styleable.Chip_chipIcon));
        this.setChipIconTint(MaterialResources.getColorStateList(this.context, obtainStyledAttributes, R.styleable.Chip_chipIconTint));
        this.setChipIconSize(obtainStyledAttributes.getDimension(R.styleable.Chip_chipIconSize, 0.0f));
        this.setCloseIconEnabled(obtainStyledAttributes.getBoolean(R.styleable.Chip_closeIconEnabled, false));
        this.setCloseIcon(MaterialResources.getDrawable(this.context, obtainStyledAttributes, R.styleable.Chip_closeIcon));
        this.setCloseIconTint(MaterialResources.getColorStateList(this.context, obtainStyledAttributes, R.styleable.Chip_closeIconTint));
        this.setCloseIconSize(obtainStyledAttributes.getDimension(R.styleable.Chip_closeIconSize, 0.0f));
        this.setCheckable(obtainStyledAttributes.getBoolean(R.styleable.Chip_android_checkable, false));
        this.setCheckedIconEnabled(obtainStyledAttributes.getBoolean(R.styleable.Chip_checkedIconEnabled, false));
        this.setCheckedIcon(MaterialResources.getDrawable(this.context, obtainStyledAttributes, R.styleable.Chip_checkedIcon));
        this.setShowMotionSpec(MotionSpec.createFromAttribute(this.context, obtainStyledAttributes, R.styleable.Chip_showMotionSpec));
        this.setHideMotionSpec(MotionSpec.createFromAttribute(this.context, obtainStyledAttributes, R.styleable.Chip_hideMotionSpec));
        this.setChipStartPadding(obtainStyledAttributes.getDimension(R.styleable.Chip_chipStartPadding, 0.0f));
        this.setIconStartPadding(obtainStyledAttributes.getDimension(R.styleable.Chip_iconStartPadding, 0.0f));
        this.setIconEndPadding(obtainStyledAttributes.getDimension(R.styleable.Chip_iconEndPadding, 0.0f));
        this.setTextStartPadding(obtainStyledAttributes.getDimension(R.styleable.Chip_textStartPadding, 0.0f));
        this.setTextEndPadding(obtainStyledAttributes.getDimension(R.styleable.Chip_textEndPadding, 0.0f));
        this.setCloseIconStartPadding(obtainStyledAttributes.getDimension(R.styleable.Chip_closeIconStartPadding, 0.0f));
        this.setCloseIconEndPadding(obtainStyledAttributes.getDimension(R.styleable.Chip_closeIconEndPadding, 0.0f));
        this.setChipEndPadding(obtainStyledAttributes.getDimension(R.styleable.Chip_chipEndPadding, 0.0f));
        obtainStyledAttributes.recycle();
    }
    
    private boolean onStateChange(final int[] array, final int[] state) {
        int onStateChange = super.onStateChange(array) ? 1 : 0;
        final ColorStateList chipBackgroundColor = this.chipBackgroundColor;
        final int n = 0;
        int colorForState;
        if (chipBackgroundColor != null) {
            colorForState = chipBackgroundColor.getColorForState(array, this.currentChipBackgroundColor);
        }
        else {
            colorForState = 0;
        }
        if (this.currentChipBackgroundColor != colorForState) {
            this.currentChipBackgroundColor = colorForState;
            onStateChange = 1;
        }
        final ColorStateList chipStrokeColor = this.chipStrokeColor;
        int colorForState2;
        if (chipStrokeColor != null) {
            colorForState2 = chipStrokeColor.getColorForState(array, this.currentChipStrokeColor);
        }
        else {
            colorForState2 = 0;
        }
        if (this.currentChipStrokeColor != colorForState2) {
            this.currentChipStrokeColor = colorForState2;
            onStateChange = 1;
        }
        final ColorStateList compatRippleColor = this.compatRippleColor;
        int colorForState3;
        if (compatRippleColor != null) {
            colorForState3 = compatRippleColor.getColorForState(array, this.currentCompatRippleColor);
        }
        else {
            colorForState3 = 0;
        }
        int n2 = onStateChange;
        if (this.currentCompatRippleColor != colorForState3) {
            this.currentCompatRippleColor = colorForState3;
            n2 = onStateChange;
            if (this.useCompatRipple) {
                n2 = 1;
            }
        }
        final TextAppearance textAppearance = this.textAppearance;
        int colorForState4;
        if (textAppearance != null && textAppearance.textColor != null) {
            colorForState4 = this.textAppearance.textColor.getColorForState(array, this.currentTextColor);
        }
        else {
            colorForState4 = 0;
        }
        int n3 = n2;
        if (this.currentTextColor != colorForState4) {
            this.currentTextColor = colorForState4;
            n3 = 1;
        }
        final boolean currentChecked = hasState(this.getState(), 16842912) && this.checkable;
        int n4;
        if (this.currentChecked != currentChecked && this.checkedIcon != null) {
            final float calculateChipIconWidth = this.calculateChipIconWidth();
            this.currentChecked = currentChecked;
            if (calculateChipIconWidth != this.calculateChipIconWidth()) {
                n3 = (n4 = 1);
            }
            else {
                n4 = 0;
                n3 = 1;
            }
        }
        else {
            n4 = 0;
        }
        final ColorStateList tint = this.tint;
        int colorForState5 = n;
        if (tint != null) {
            colorForState5 = tint.getColorForState(array, this.currentTint);
        }
        int n5 = n3;
        if (this.currentTint != colorForState5) {
            this.currentTint = colorForState5;
            this.tintFilter = DrawableUtils.updateTintFilter(this, this.tint, this.tintMode);
            n5 = 1;
        }
        int n6 = n5;
        if (isStateful(this.chipIcon)) {
            n6 = (n5 | (this.chipIcon.setState(array) ? 1 : 0));
        }
        int n7 = n6;
        if (isStateful(this.checkedIcon)) {
            n7 = (n6 | (this.checkedIcon.setState(array) ? 1 : 0));
        }
        int n8 = n7;
        if (isStateful(this.closeIcon)) {
            n8 = (n7 | (this.closeIcon.setState(state) ? 1 : 0));
        }
        if (n8 != 0) {
            this.invalidateSelf();
        }
        if (n4 != 0) {
            this.onSizeChange();
        }
        return n8 != 0;
    }
    
    private boolean showsCheckedIcon() {
        return this.checkedIconEnabled && this.checkedIcon != null && this.currentChecked;
    }
    
    private boolean showsChipIcon() {
        return this.chipIconEnabled && this.chipIcon != null;
    }
    
    private boolean showsCloseIcon() {
        return this.closeIconEnabled && this.closeIcon != null;
    }
    
    private void unapplyChildDrawable(final Drawable drawable) {
        if (drawable != null) {
            drawable.setCallback((Drawable$Callback)null);
        }
    }
    
    private void updateCompatRippleColor() {
        ColorStateList convertToRippleDrawableColor;
        if (this.useCompatRipple) {
            convertToRippleDrawableColor = RippleUtils.convertToRippleDrawableColor(this.rippleColor);
        }
        else {
            convertToRippleDrawableColor = null;
        }
        this.compatRippleColor = convertToRippleDrawableColor;
    }
    
    float calculateChipIconWidth() {
        if (!this.showsChipIcon() && !this.showsCheckedIcon()) {
            return 0.0f;
        }
        return this.iconStartPadding + this.chipIconSize + this.iconEndPadding;
    }
    
    Paint$Align calculateTextOriginAndAlignment(final Rect rect, final PointF pointF) {
        pointF.set(0.0f, 0.0f);
        Paint$Align paint$Align = Paint$Align.LEFT;
        if (this.unicodeWrappedText != null) {
            final float n = this.chipStartPadding + this.calculateChipIconWidth() + this.textStartPadding;
            if (DrawableCompat.getLayoutDirection(this) == 0) {
                pointF.x = rect.left + n;
                paint$Align = Paint$Align.LEFT;
            }
            else {
                pointF.x = rect.right - n;
                paint$Align = Paint$Align.RIGHT;
            }
            pointF.y = rect.centerY() - this.calculateTextCenterFromBaseline();
        }
        return paint$Align;
    }
    
    public void draw(final Canvas canvas) {
        final Rect bounds = this.getBounds();
        if (!bounds.isEmpty()) {
            if (this.getAlpha() == 0) {
                return;
            }
            int saveLayerAlpha = 0;
            if (this.alpha < 255) {
                saveLayerAlpha = CanvasCompat.saveLayerAlpha(canvas, (float)bounds.left, (float)bounds.top, (float)bounds.right, (float)bounds.bottom, this.alpha);
            }
            this.drawChipBackground(canvas, bounds);
            this.drawChipStroke(canvas, bounds);
            this.drawCompatRipple(canvas, bounds);
            this.drawChipIcon(canvas, bounds);
            this.drawCheckedIcon(canvas, bounds);
            if (this.shouldDrawText) {
                this.drawText(canvas, bounds);
            }
            this.drawCloseIcon(canvas, bounds);
            this.drawDebug(canvas, bounds);
            if (this.alpha < 255) {
                canvas.restoreToCount(saveLayerAlpha);
            }
        }
    }
    
    public int getAlpha() {
        return this.alpha;
    }
    
    public Drawable getCheckedIcon() {
        return this.checkedIcon;
    }
    
    public ColorStateList getChipBackgroundColor() {
        return this.chipBackgroundColor;
    }
    
    public float getChipCornerRadius() {
        return this.chipCornerRadius;
    }
    
    public float getChipEndPadding() {
        return this.chipEndPadding;
    }
    
    public Drawable getChipIcon() {
        final Drawable chipIcon = this.chipIcon;
        if (chipIcon != null) {
            return DrawableCompat.unwrap(chipIcon);
        }
        return null;
    }
    
    public float getChipIconSize() {
        return this.chipIconSize;
    }
    
    public ColorStateList getChipIconTint() {
        return this.chipIconTint;
    }
    
    public float getChipMinHeight() {
        return this.chipMinHeight;
    }
    
    public float getChipStartPadding() {
        return this.chipStartPadding;
    }
    
    public ColorStateList getChipStrokeColor() {
        return this.chipStrokeColor;
    }
    
    public float getChipStrokeWidth() {
        return this.chipStrokeWidth;
    }
    
    public void getChipTouchBounds(final RectF rectF) {
        this.calculateChipTouchBounds(this.getBounds(), rectF);
    }
    
    public Drawable getCloseIcon() {
        final Drawable closeIcon = this.closeIcon;
        if (closeIcon != null) {
            return DrawableCompat.unwrap(closeIcon);
        }
        return null;
    }
    
    public CharSequence getCloseIconContentDescription() {
        return this.closeIconContentDescription;
    }
    
    public float getCloseIconEndPadding() {
        return this.closeIconEndPadding;
    }
    
    public float getCloseIconSize() {
        return this.closeIconSize;
    }
    
    public float getCloseIconStartPadding() {
        return this.closeIconStartPadding;
    }
    
    public int[] getCloseIconState() {
        return this.closeIconStateSet;
    }
    
    public ColorStateList getCloseIconTint() {
        return this.closeIconTint;
    }
    
    public void getCloseIconTouchBounds(final RectF rectF) {
        this.calculateCloseIconTouchBounds(this.getBounds(), rectF);
    }
    
    public ColorFilter getColorFilter() {
        return this.colorFilter;
    }
    
    public TextUtils$TruncateAt getEllipsize() {
        return this.truncateAt;
    }
    
    public MotionSpec getHideMotionSpec() {
        return this.hideMotionSpec;
    }
    
    public float getIconEndPadding() {
        return this.iconEndPadding;
    }
    
    public float getIconStartPadding() {
        return this.iconStartPadding;
    }
    
    public int getIntrinsicHeight() {
        return (int)this.chipMinHeight;
    }
    
    public int getIntrinsicWidth() {
        return Math.round(this.chipStartPadding + this.calculateChipIconWidth() + this.textStartPadding + this.getTextWidth() + this.textEndPadding + this.calculateCloseIconWidth() + this.chipEndPadding);
    }
    
    public int getOpacity() {
        return -3;
    }
    
    public void getOutline(final Outline outline) {
        final Rect bounds = this.getBounds();
        if (!bounds.isEmpty()) {
            outline.setRoundRect(bounds, this.chipCornerRadius);
        }
        else {
            outline.setRoundRect(0, 0, this.getIntrinsicWidth(), this.getIntrinsicHeight(), this.chipCornerRadius);
        }
        outline.setAlpha(this.getAlpha() / 255.0f);
    }
    
    public ColorStateList getRippleColor() {
        return this.rippleColor;
    }
    
    public MotionSpec getShowMotionSpec() {
        return this.showMotionSpec;
    }
    
    public CharSequence getText() {
        return this.rawText;
    }
    
    public TextAppearance getTextAppearance() {
        return this.textAppearance;
    }
    
    public float getTextEndPadding() {
        return this.textEndPadding;
    }
    
    public float getTextStartPadding() {
        return this.textStartPadding;
    }
    
    public boolean getUseCompatRipple() {
        return this.useCompatRipple;
    }
    
    public void invalidateDrawable(final Drawable drawable) {
        final Drawable$Callback callback = this.getCallback();
        if (callback != null) {
            callback.invalidateDrawable((Drawable)this);
        }
    }
    
    public boolean isCheckable() {
        return this.checkable;
    }
    
    public boolean isCheckedIconEnabled() {
        return this.checkedIconEnabled;
    }
    
    public boolean isChipIconEnabled() {
        return this.chipIconEnabled;
    }
    
    public boolean isCloseIconEnabled() {
        return this.closeIconEnabled;
    }
    
    public boolean isCloseIconStateful() {
        return isStateful(this.closeIcon);
    }
    
    public boolean isStateful() {
        return isStateful(this.chipBackgroundColor) || isStateful(this.chipStrokeColor) || (this.useCompatRipple && isStateful(this.compatRippleColor)) || isStateful(this.textAppearance) || this.canShowCheckedIcon() || isStateful(this.chipIcon) || isStateful(this.checkedIcon) || isStateful(this.tint);
    }
    
    public boolean onLayoutDirectionChanged(final int layoutDirection) {
        boolean onLayoutDirectionChanged;
        final boolean b = onLayoutDirectionChanged = super.onLayoutDirectionChanged(layoutDirection);
        if (this.showsChipIcon()) {
            onLayoutDirectionChanged = (b | this.chipIcon.setLayoutDirection(layoutDirection));
        }
        boolean b2 = onLayoutDirectionChanged;
        if (this.showsCheckedIcon()) {
            b2 = (onLayoutDirectionChanged | this.checkedIcon.setLayoutDirection(layoutDirection));
        }
        int n = b2 ? 1 : 0;
        if (this.showsCloseIcon()) {
            n = ((b2 | this.closeIcon.setLayoutDirection(layoutDirection)) ? 1 : 0);
        }
        if (n != 0) {
            this.invalidateSelf();
        }
        return true;
    }
    
    protected boolean onLevelChange(final int level) {
        boolean onLevelChange;
        final boolean b = onLevelChange = super.onLevelChange(level);
        if (this.showsChipIcon()) {
            onLevelChange = (b | this.chipIcon.setLevel(level));
        }
        boolean b2 = onLevelChange;
        if (this.showsCheckedIcon()) {
            b2 = (onLevelChange | this.checkedIcon.setLevel(level));
        }
        boolean b3 = b2;
        if (this.showsCloseIcon()) {
            b3 = (b2 | this.closeIcon.setLevel(level));
        }
        if (b3) {
            this.invalidateSelf();
        }
        return b3;
    }
    
    protected void onSizeChange() {
        final Delegate delegate = this.delegate.get();
        if (delegate != null) {
            delegate.onChipDrawableSizeChange();
        }
    }
    
    protected boolean onStateChange(final int[] array) {
        return this.onStateChange(array, this.getCloseIconState());
    }
    
    public void scheduleDrawable(final Drawable drawable, final Runnable runnable, final long n) {
        final Drawable$Callback callback = this.getCallback();
        if (callback != null) {
            callback.scheduleDrawable((Drawable)this, runnable, n);
        }
    }
    
    public void setAlpha(final int alpha) {
        if (this.alpha != alpha) {
            this.alpha = alpha;
            this.invalidateSelf();
        }
    }
    
    public void setCheckable(final boolean checkable) {
        if (this.checkable != checkable) {
            this.checkable = checkable;
            final float calculateChipIconWidth = this.calculateChipIconWidth();
            if (!checkable && this.currentChecked) {
                this.currentChecked = false;
            }
            final float calculateChipIconWidth2 = this.calculateChipIconWidth();
            this.invalidateSelf();
            if (calculateChipIconWidth != calculateChipIconWidth2) {
                this.onSizeChange();
            }
        }
    }
    
    public void setCheckableResource(final int n) {
        this.setCheckable(this.context.getResources().getBoolean(n));
    }
    
    public void setCheckedIcon(final Drawable checkedIcon) {
        if (this.checkedIcon != checkedIcon) {
            final float calculateChipIconWidth = this.calculateChipIconWidth();
            this.checkedIcon = checkedIcon;
            final float calculateChipIconWidth2 = this.calculateChipIconWidth();
            this.unapplyChildDrawable(this.checkedIcon);
            this.applyChildDrawable(this.checkedIcon);
            this.invalidateSelf();
            if (calculateChipIconWidth != calculateChipIconWidth2) {
                this.onSizeChange();
            }
        }
    }
    
    public void setCheckedIconEnabled(final boolean checkedIconEnabled) {
        if (this.checkedIconEnabled != checkedIconEnabled) {
            final boolean showsCheckedIcon = this.showsCheckedIcon();
            this.checkedIconEnabled = checkedIconEnabled;
            final boolean showsCheckedIcon2 = this.showsCheckedIcon();
            if (showsCheckedIcon != showsCheckedIcon2) {
                if (showsCheckedIcon2) {
                    this.applyChildDrawable(this.checkedIcon);
                }
                else {
                    this.unapplyChildDrawable(this.checkedIcon);
                }
                this.invalidateSelf();
                this.onSizeChange();
            }
        }
    }
    
    public void setCheckedIconEnabledResource(final int n) {
        this.setCheckedIconEnabled(this.context.getResources().getBoolean(n));
    }
    
    public void setCheckedIconResource(final int n) {
        this.setCheckedIcon(AppCompatResources.getDrawable(this.context, n));
    }
    
    public void setChipBackgroundColor(final ColorStateList chipBackgroundColor) {
        if (this.chipBackgroundColor != chipBackgroundColor) {
            this.chipBackgroundColor = chipBackgroundColor;
            this.onStateChange(this.getState());
        }
    }
    
    public void setChipBackgroundColorResource(final int n) {
        this.setChipBackgroundColor(AppCompatResources.getColorStateList(this.context, n));
    }
    
    public void setChipCornerRadius(final float chipCornerRadius) {
        if (this.chipCornerRadius != chipCornerRadius) {
            this.chipCornerRadius = chipCornerRadius;
            this.invalidateSelf();
        }
    }
    
    public void setChipCornerRadiusResource(final int n) {
        this.setChipCornerRadius(this.context.getResources().getDimension(n));
    }
    
    public void setChipEndPadding(final float chipEndPadding) {
        if (this.chipEndPadding != chipEndPadding) {
            this.chipEndPadding = chipEndPadding;
            this.invalidateSelf();
            this.onSizeChange();
        }
    }
    
    public void setChipEndPaddingResource(final int n) {
        this.setChipEndPadding(this.context.getResources().getDimension(n));
    }
    
    public void setChipIcon(Drawable mutate) {
        final Drawable chipIcon = this.getChipIcon();
        if (chipIcon != mutate) {
            final float calculateChipIconWidth = this.calculateChipIconWidth();
            if (mutate != null) {
                mutate = DrawableCompat.wrap(mutate).mutate();
            }
            else {
                mutate = null;
            }
            this.chipIcon = mutate;
            final float calculateChipIconWidth2 = this.calculateChipIconWidth();
            this.unapplyChildDrawable(chipIcon);
            if (this.showsChipIcon()) {
                this.applyChildDrawable(this.chipIcon);
            }
            this.invalidateSelf();
            if (calculateChipIconWidth != calculateChipIconWidth2) {
                this.onSizeChange();
            }
        }
    }
    
    public void setChipIconEnabled(final boolean chipIconEnabled) {
        if (this.chipIconEnabled != chipIconEnabled) {
            final boolean showsChipIcon = this.showsChipIcon();
            this.chipIconEnabled = chipIconEnabled;
            final boolean showsChipIcon2 = this.showsChipIcon();
            if (showsChipIcon != showsChipIcon2) {
                if (showsChipIcon2) {
                    this.applyChildDrawable(this.chipIcon);
                }
                else {
                    this.unapplyChildDrawable(this.chipIcon);
                }
                this.invalidateSelf();
                this.onSizeChange();
            }
        }
    }
    
    public void setChipIconEnabledResource(final int n) {
        this.setChipIconEnabled(this.context.getResources().getBoolean(n));
    }
    
    public void setChipIconResource(final int n) {
        this.setChipIcon(AppCompatResources.getDrawable(this.context, n));
    }
    
    public void setChipIconSize(float calculateChipIconWidth) {
        if (this.chipIconSize != calculateChipIconWidth) {
            final float calculateChipIconWidth2 = this.calculateChipIconWidth();
            this.chipIconSize = calculateChipIconWidth;
            calculateChipIconWidth = this.calculateChipIconWidth();
            this.invalidateSelf();
            if (calculateChipIconWidth2 != calculateChipIconWidth) {
                this.onSizeChange();
            }
        }
    }
    
    public void setChipIconSizeResource(final int n) {
        this.setChipIconSize(this.context.getResources().getDimension(n));
    }
    
    public void setChipIconTint(final ColorStateList chipIconTint) {
        if (this.chipIconTint != chipIconTint) {
            this.chipIconTint = chipIconTint;
            if (this.showsChipIcon()) {
                DrawableCompat.setTintList(this.chipIcon, chipIconTint);
            }
            this.onStateChange(this.getState());
        }
    }
    
    public void setChipIconTintResource(final int n) {
        this.setChipIconTint(AppCompatResources.getColorStateList(this.context, n));
    }
    
    public void setChipMinHeight(final float chipMinHeight) {
        if (this.chipMinHeight != chipMinHeight) {
            this.chipMinHeight = chipMinHeight;
            this.invalidateSelf();
            this.onSizeChange();
        }
    }
    
    public void setChipMinHeightResource(final int n) {
        this.setChipMinHeight(this.context.getResources().getDimension(n));
    }
    
    public void setChipStartPadding(final float chipStartPadding) {
        if (this.chipStartPadding != chipStartPadding) {
            this.chipStartPadding = chipStartPadding;
            this.invalidateSelf();
            this.onSizeChange();
        }
    }
    
    public void setChipStartPaddingResource(final int n) {
        this.setChipStartPadding(this.context.getResources().getDimension(n));
    }
    
    public void setChipStrokeColor(final ColorStateList chipStrokeColor) {
        if (this.chipStrokeColor != chipStrokeColor) {
            this.chipStrokeColor = chipStrokeColor;
            this.onStateChange(this.getState());
        }
    }
    
    public void setChipStrokeColorResource(final int n) {
        this.setChipStrokeColor(AppCompatResources.getColorStateList(this.context, n));
    }
    
    public void setChipStrokeWidth(final float n) {
        if (this.chipStrokeWidth != n) {
            this.chipStrokeWidth = n;
            this.chipPaint.setStrokeWidth(n);
            this.invalidateSelf();
        }
    }
    
    public void setChipStrokeWidthResource(final int n) {
        this.setChipStrokeWidth(this.context.getResources().getDimension(n));
    }
    
    public void setCloseIcon(Drawable mutate) {
        final Drawable closeIcon = this.getCloseIcon();
        if (closeIcon != mutate) {
            final float calculateCloseIconWidth = this.calculateCloseIconWidth();
            if (mutate != null) {
                mutate = DrawableCompat.wrap(mutate).mutate();
            }
            else {
                mutate = null;
            }
            this.closeIcon = mutate;
            final float calculateCloseIconWidth2 = this.calculateCloseIconWidth();
            this.unapplyChildDrawable(closeIcon);
            if (this.showsCloseIcon()) {
                this.applyChildDrawable(this.closeIcon);
            }
            this.invalidateSelf();
            if (calculateCloseIconWidth != calculateCloseIconWidth2) {
                this.onSizeChange();
            }
        }
    }
    
    public void setCloseIconContentDescription(final CharSequence charSequence) {
        if (this.closeIconContentDescription != charSequence) {
            this.closeIconContentDescription = BidiFormatter.getInstance().unicodeWrap(charSequence);
            this.invalidateSelf();
        }
    }
    
    public void setCloseIconEnabled(final boolean closeIconEnabled) {
        if (this.closeIconEnabled != closeIconEnabled) {
            final boolean showsCloseIcon = this.showsCloseIcon();
            this.closeIconEnabled = closeIconEnabled;
            final boolean showsCloseIcon2 = this.showsCloseIcon();
            if (showsCloseIcon != showsCloseIcon2) {
                if (showsCloseIcon2) {
                    this.applyChildDrawable(this.closeIcon);
                }
                else {
                    this.unapplyChildDrawable(this.closeIcon);
                }
                this.invalidateSelf();
                this.onSizeChange();
            }
        }
    }
    
    public void setCloseIconEnabledResource(final int n) {
        this.setCloseIconEnabled(this.context.getResources().getBoolean(n));
    }
    
    public void setCloseIconEndPadding(final float closeIconEndPadding) {
        if (this.closeIconEndPadding != closeIconEndPadding) {
            this.closeIconEndPadding = closeIconEndPadding;
            this.invalidateSelf();
            if (this.showsCloseIcon()) {
                this.onSizeChange();
            }
        }
    }
    
    public void setCloseIconEndPaddingResource(final int n) {
        this.setCloseIconEndPadding(this.context.getResources().getDimension(n));
    }
    
    public void setCloseIconResource(final int n) {
        this.setCloseIcon(AppCompatResources.getDrawable(this.context, n));
    }
    
    public void setCloseIconSize(final float closeIconSize) {
        if (this.closeIconSize != closeIconSize) {
            this.closeIconSize = closeIconSize;
            this.invalidateSelf();
            if (this.showsCloseIcon()) {
                this.onSizeChange();
            }
        }
    }
    
    public void setCloseIconSizeResource(final int n) {
        this.setCloseIconSize(this.context.getResources().getDimension(n));
    }
    
    public void setCloseIconStartPadding(final float closeIconStartPadding) {
        if (this.closeIconStartPadding != closeIconStartPadding) {
            this.closeIconStartPadding = closeIconStartPadding;
            this.invalidateSelf();
            if (this.showsCloseIcon()) {
                this.onSizeChange();
            }
        }
    }
    
    public void setCloseIconStartPaddingResource(final int n) {
        this.setCloseIconStartPadding(this.context.getResources().getDimension(n));
    }
    
    public boolean setCloseIconState(final int[] array) {
        if (!Arrays.equals(this.closeIconStateSet, array)) {
            this.closeIconStateSet = array;
            if (this.showsCloseIcon()) {
                return this.onStateChange(this.getState(), array);
            }
        }
        return false;
    }
    
    public void setCloseIconTint(final ColorStateList closeIconTint) {
        if (this.closeIconTint != closeIconTint) {
            this.closeIconTint = closeIconTint;
            if (this.showsCloseIcon()) {
                DrawableCompat.setTintList(this.closeIcon, closeIconTint);
            }
            this.onStateChange(this.getState());
        }
    }
    
    public void setCloseIconTintResource(final int n) {
        this.setCloseIconTint(AppCompatResources.getColorStateList(this.context, n));
    }
    
    public void setColorFilter(final ColorFilter colorFilter) {
        if (this.colorFilter != colorFilter) {
            this.colorFilter = colorFilter;
            this.invalidateSelf();
        }
    }
    
    public void setDelegate(final Delegate referent) {
        this.delegate = new WeakReference<Delegate>(referent);
    }
    
    public void setEllipsize(final TextUtils$TruncateAt truncateAt) {
        this.truncateAt = truncateAt;
    }
    
    public void setHideMotionSpec(final MotionSpec hideMotionSpec) {
        this.hideMotionSpec = hideMotionSpec;
    }
    
    public void setHideMotionSpecResource(final int n) {
        this.setHideMotionSpec(MotionSpec.createFromResource(this.context, n));
    }
    
    public void setIconEndPadding(float calculateChipIconWidth) {
        if (this.iconEndPadding != calculateChipIconWidth) {
            final float calculateChipIconWidth2 = this.calculateChipIconWidth();
            this.iconEndPadding = calculateChipIconWidth;
            calculateChipIconWidth = this.calculateChipIconWidth();
            this.invalidateSelf();
            if (calculateChipIconWidth2 != calculateChipIconWidth) {
                this.onSizeChange();
            }
        }
    }
    
    public void setIconEndPaddingResource(final int n) {
        this.setIconEndPadding(this.context.getResources().getDimension(n));
    }
    
    public void setIconStartPadding(float calculateChipIconWidth) {
        if (this.iconStartPadding != calculateChipIconWidth) {
            final float calculateChipIconWidth2 = this.calculateChipIconWidth();
            this.iconStartPadding = calculateChipIconWidth;
            calculateChipIconWidth = this.calculateChipIconWidth();
            this.invalidateSelf();
            if (calculateChipIconWidth2 != calculateChipIconWidth) {
                this.onSizeChange();
            }
        }
    }
    
    public void setIconStartPaddingResource(final int n) {
        this.setIconStartPadding(this.context.getResources().getDimension(n));
    }
    
    public void setRippleColor(final ColorStateList rippleColor) {
        if (this.rippleColor != rippleColor) {
            this.rippleColor = rippleColor;
            this.updateCompatRippleColor();
            this.onStateChange(this.getState());
        }
    }
    
    public void setRippleColorResource(final int n) {
        this.setRippleColor(AppCompatResources.getColorStateList(this.context, n));
    }
    
    void setShouldDrawText(final boolean shouldDrawText) {
        this.shouldDrawText = shouldDrawText;
    }
    
    public void setShowMotionSpec(final MotionSpec showMotionSpec) {
        this.showMotionSpec = showMotionSpec;
    }
    
    public void setShowMotionSpecResource(final int n) {
        this.setShowMotionSpec(MotionSpec.createFromResource(this.context, n));
    }
    
    public void setText(final CharSequence charSequence) {
        CharSequence rawText = charSequence;
        if (charSequence == null) {
            rawText = "";
        }
        if (this.rawText != rawText) {
            this.rawText = rawText;
            this.unicodeWrappedText = BidiFormatter.getInstance().unicodeWrap(rawText);
            this.textWidthDirty = true;
            this.invalidateSelf();
            this.onSizeChange();
        }
    }
    
    public void setTextAppearance(final TextAppearance textAppearance) {
        if (this.textAppearance != textAppearance) {
            if ((this.textAppearance = textAppearance) != null) {
                textAppearance.updateMeasureState(this.context, this.textPaint);
                this.textWidthDirty = true;
            }
            this.onStateChange(this.getState());
            this.onSizeChange();
        }
    }
    
    public void setTextAppearanceResource(final int n) {
        this.setTextAppearance(new TextAppearance(this.context, n));
    }
    
    public void setTextEndPadding(final float textEndPadding) {
        if (this.textEndPadding != textEndPadding) {
            this.textEndPadding = textEndPadding;
            this.invalidateSelf();
            this.onSizeChange();
        }
    }
    
    public void setTextEndPaddingResource(final int n) {
        this.setTextEndPadding(this.context.getResources().getDimension(n));
    }
    
    public void setTextResource(final int n) {
        this.setText(this.context.getResources().getString(n));
    }
    
    public void setTextStartPadding(final float textStartPadding) {
        if (this.textStartPadding != textStartPadding) {
            this.textStartPadding = textStartPadding;
            this.invalidateSelf();
            this.onSizeChange();
        }
    }
    
    public void setTextStartPaddingResource(final int n) {
        this.setTextStartPadding(this.context.getResources().getDimension(n));
    }
    
    public void setTintList(final ColorStateList tint) {
        if (this.tint != tint) {
            this.tint = tint;
            this.onStateChange(this.getState());
        }
    }
    
    public void setTintMode(final PorterDuff$Mode tintMode) {
        if (this.tintMode != tintMode) {
            this.tintMode = tintMode;
            this.tintFilter = DrawableUtils.updateTintFilter(this, this.tint, tintMode);
            this.invalidateSelf();
        }
    }
    
    public void setUseCompatRipple(final boolean useCompatRipple) {
        if (this.useCompatRipple != useCompatRipple) {
            this.useCompatRipple = useCompatRipple;
            this.updateCompatRippleColor();
            this.onStateChange(this.getState());
        }
    }
    
    public boolean setVisible(final boolean b, final boolean b2) {
        boolean setVisible;
        final boolean b3 = setVisible = super.setVisible(b, b2);
        if (this.showsChipIcon()) {
            setVisible = (b3 | this.chipIcon.setVisible(b, b2));
        }
        boolean b4 = setVisible;
        if (this.showsCheckedIcon()) {
            b4 = (setVisible | this.checkedIcon.setVisible(b, b2));
        }
        boolean b5 = b4;
        if (this.showsCloseIcon()) {
            b5 = (b4 | this.closeIcon.setVisible(b, b2));
        }
        if (b5) {
            this.invalidateSelf();
        }
        return b5;
    }
    
    boolean shouldDrawText() {
        return this.shouldDrawText;
    }
    
    public void unscheduleDrawable(final Drawable drawable, final Runnable runnable) {
        final Drawable$Callback callback = this.getCallback();
        if (callback != null) {
            callback.unscheduleDrawable((Drawable)this, runnable);
        }
    }
    
    public interface Delegate
    {
        void onChipDrawableSizeChange();
    }
}
