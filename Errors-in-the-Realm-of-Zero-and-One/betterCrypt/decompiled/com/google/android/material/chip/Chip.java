// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.chip;

import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import android.os.Bundle;
import java.util.List;
import androidx.core.text.BidiFormatter;
import android.widget.TextView$BufferType;
import com.google.android.material.ripple.RippleUtils;
import android.widget.CompoundButton;
import android.graphics.PorterDuff$Mode;
import android.view.PointerIcon;
import android.view.ViewParent;
import com.google.android.material.internal.ViewUtils;
import android.graphics.Canvas;
import com.google.android.material.animation.MotionSpec;
import android.text.TextUtils$TruncateAt;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.view.KeyEvent;
import android.text.TextPaint;
import android.text.TextUtils;
import android.graphics.Outline;
import android.view.ViewOutlineProvider;
import android.os.Build$VERSION;
import java.lang.reflect.Method;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import android.util.Log;
import androidx.customview.widget.ExploreByTouchHelper;
import android.view.MotionEvent;
import com.google.android.material.resources.TextAppearance;
import androidx.core.view.AccessibilityDelegateCompat;
import android.view.View;
import androidx.core.view.ViewCompat;
import com.google.android.material.R;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.drawable.RippleDrawable;
import android.graphics.RectF;
import android.view.View$OnClickListener;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.graphics.Rect;
import androidx.appcompat.widget.AppCompatCheckBox;

public class Chip extends AppCompatCheckBox implements Delegate
{
    private static final int CLOSE_ICON_VIRTUAL_ID = 0;
    private static final Rect EMPTY_BOUNDS;
    private static final String NAMESPACE_ANDROID = "http://schemas.android.com/apk/res/android";
    private static final int[] SELECTED_STATE;
    private static final String TAG = "Chip";
    private ChipDrawable chipDrawable;
    private boolean closeIconFocused;
    private boolean closeIconHovered;
    private boolean closeIconPressed;
    private boolean deferredCheckedValue;
    private int focusedVirtualView;
    private CompoundButton$OnCheckedChangeListener onCheckedChangeListenerInternal;
    private View$OnClickListener onCloseIconClickListener;
    private final Rect rect;
    private final RectF rectF;
    private RippleDrawable ripple;
    private final ChipTouchHelper touchHelper;
    
    static {
        EMPTY_BOUNDS = new Rect();
        SELECTED_STATE = new int[] { 16842913 };
    }
    
    public Chip(final Context context) {
        this(context, null);
    }
    
    public Chip(final Context context, final AttributeSet set) {
        this(context, set, R.attr.chipStyle);
    }
    
    public Chip(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.focusedVirtualView = Integer.MIN_VALUE;
        this.rect = new Rect();
        this.rectF = new RectF();
        this.validateAttributes(set);
        final ChipDrawable fromAttributes = ChipDrawable.createFromAttributes(context, set, n, R.style.Widget_MaterialComponents_Chip_Action);
        this.setChipDrawable(fromAttributes);
        ViewCompat.setAccessibilityDelegate((View)this, this.touchHelper = new ChipTouchHelper(this));
        ViewCompat.setImportantForAccessibility((View)this, 1);
        this.initOutlineProvider();
        this.setChecked(this.deferredCheckedValue);
        fromAttributes.setShouldDrawText(false);
        this.setText(fromAttributes.getText());
        this.setEllipsize(fromAttributes.getEllipsize());
        this.setIncludeFontPadding(false);
        this.updateTextPaintDrawState(this.getTextAppearance());
        this.setSingleLine();
        this.setGravity(8388627);
        this.updatePaddingInternal();
    }
    
    private void applyChipDrawable(final ChipDrawable chipDrawable) {
        chipDrawable.setDelegate((ChipDrawable.Delegate)this);
    }
    
    private float calculateTextOffsetFromStart(final ChipDrawable chipDrawable) {
        final float n = this.getChipStartPadding() + chipDrawable.calculateChipIconWidth() + this.getTextStartPadding();
        if (ViewCompat.getLayoutDirection((View)this) == 0) {
            return n;
        }
        return -n;
    }
    
    private int[] createCloseIconDrawableState() {
        final boolean enabled = this.isEnabled();
        final int n = 0;
        int n2;
        if (enabled) {
            n2 = 1;
        }
        else {
            n2 = 0;
        }
        int n3 = n2;
        if (this.closeIconFocused) {
            n3 = n2 + 1;
        }
        int n4 = n3;
        if (this.closeIconHovered) {
            n4 = n3 + 1;
        }
        int n5 = n4;
        if (this.closeIconPressed) {
            n5 = n4 + 1;
        }
        int n6 = n5;
        if (this.isChecked()) {
            n6 = n5 + 1;
        }
        final int[] array = new int[n6];
        int n7 = n;
        if (this.isEnabled()) {
            array[0] = 16842910;
            n7 = 1;
        }
        int n8 = n7;
        if (this.closeIconFocused) {
            array[n7] = 16842908;
            n8 = n7 + 1;
        }
        int n9 = n8;
        if (this.closeIconHovered) {
            array[n8] = 16843623;
            n9 = n8 + 1;
        }
        int n10 = n9;
        if (this.closeIconPressed) {
            array[n9] = 16842919;
            n10 = n9 + 1;
        }
        if (this.isChecked()) {
            array[n10] = 16842913;
        }
        return array;
    }
    
    private void ensureFocus() {
        if (this.focusedVirtualView == Integer.MIN_VALUE) {
            this.setFocusedVirtualView(-1);
        }
    }
    
    private RectF getCloseIconTouchBounds() {
        this.rectF.setEmpty();
        if (this.hasCloseIcon()) {
            this.chipDrawable.getCloseIconTouchBounds(this.rectF);
        }
        return this.rectF;
    }
    
    private Rect getCloseIconTouchBoundsInt() {
        final RectF closeIconTouchBounds = this.getCloseIconTouchBounds();
        this.rect.set((int)closeIconTouchBounds.left, (int)closeIconTouchBounds.top, (int)closeIconTouchBounds.right, (int)closeIconTouchBounds.bottom);
        return this.rect;
    }
    
    private TextAppearance getTextAppearance() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getTextAppearance();
        }
        return null;
    }
    
    private boolean handleAccessibilityExit(final MotionEvent motionEvent) {
        if (motionEvent.getAction() == 10) {
            try {
                final Field declaredField = ExploreByTouchHelper.class.getDeclaredField("mHoveredVirtualViewId");
                declaredField.setAccessible(true);
                if ((int)declaredField.get(this.touchHelper) != Integer.MIN_VALUE) {
                    final Method declaredMethod = ExploreByTouchHelper.class.getDeclaredMethod("updateHoveredVirtualView", Integer.TYPE);
                    declaredMethod.setAccessible(true);
                    declaredMethod.invoke(this.touchHelper, Integer.MIN_VALUE);
                    return true;
                }
            }
            catch (NoSuchFieldException ex) {
                Log.e("Chip", "Unable to send Accessibility Exit event", (Throwable)ex);
                return false;
            }
            catch (InvocationTargetException ex2) {
                Log.e("Chip", "Unable to send Accessibility Exit event", (Throwable)ex2);
                return false;
            }
            catch (IllegalAccessException ex3) {
                Log.e("Chip", "Unable to send Accessibility Exit event", (Throwable)ex3);
                return false;
            }
            catch (NoSuchMethodException ex4) {
                Log.e("Chip", "Unable to send Accessibility Exit event", (Throwable)ex4);
            }
        }
        return false;
    }
    
    private boolean hasCloseIcon() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        return chipDrawable != null && chipDrawable.getCloseIcon() != null;
    }
    
    private void initOutlineProvider() {
        if (Build$VERSION.SDK_INT >= 21) {
            this.setOutlineProvider((ViewOutlineProvider)new ViewOutlineProvider() {
                public void getOutline(final View view, final Outline outline) {
                    if (Chip.this.chipDrawable != null) {
                        Chip.this.chipDrawable.getOutline(outline);
                        return;
                    }
                    outline.setAlpha(0.0f);
                }
            });
        }
    }
    
    private boolean moveFocus(final boolean b) {
        this.ensureFocus();
        if (b) {
            if (this.focusedVirtualView == -1) {
                this.setFocusedVirtualView(0);
                return true;
            }
        }
        else if (this.focusedVirtualView == 0) {
            this.setFocusedVirtualView(-1);
            return true;
        }
        return false;
    }
    
    private void setCloseIconFocused(final boolean closeIconFocused) {
        if (this.closeIconFocused != closeIconFocused) {
            this.closeIconFocused = closeIconFocused;
            this.refreshDrawableState();
        }
    }
    
    private void setCloseIconHovered(final boolean closeIconHovered) {
        if (this.closeIconHovered != closeIconHovered) {
            this.closeIconHovered = closeIconHovered;
            this.refreshDrawableState();
        }
    }
    
    private void setCloseIconPressed(final boolean closeIconPressed) {
        if (this.closeIconPressed != closeIconPressed) {
            this.closeIconPressed = closeIconPressed;
            this.refreshDrawableState();
        }
    }
    
    private void setFocusedVirtualView(final int focusedVirtualView) {
        final int focusedVirtualView2 = this.focusedVirtualView;
        if (focusedVirtualView2 != focusedVirtualView) {
            if (focusedVirtualView2 == 0) {
                this.setCloseIconFocused(false);
            }
            if ((this.focusedVirtualView = focusedVirtualView) == 0) {
                this.setCloseIconFocused(true);
            }
        }
    }
    
    private void unapplyChipDrawable(final ChipDrawable chipDrawable) {
        if (chipDrawable != null) {
            chipDrawable.setDelegate(null);
        }
    }
    
    private void updatePaddingInternal() {
        if (!TextUtils.isEmpty(this.getText())) {
            final ChipDrawable chipDrawable = this.chipDrawable;
            if (chipDrawable == null) {
                return;
            }
            final float n = chipDrawable.getChipStartPadding() + this.chipDrawable.getChipEndPadding() + this.chipDrawable.getTextStartPadding() + this.chipDrawable.getTextEndPadding();
            float n2 = 0.0f;
            Label_0128: {
                if (!this.chipDrawable.isChipIconEnabled() || this.chipDrawable.getChipIcon() == null) {
                    n2 = n;
                    if (this.chipDrawable.getCheckedIcon() == null) {
                        break Label_0128;
                    }
                    n2 = n;
                    if (!this.chipDrawable.isCheckedIconEnabled()) {
                        break Label_0128;
                    }
                    n2 = n;
                    if (!this.isChecked()) {
                        break Label_0128;
                    }
                }
                n2 = n + (this.chipDrawable.getIconStartPadding() + this.chipDrawable.getIconEndPadding() + this.chipDrawable.getChipIconSize());
            }
            float n3 = n2;
            if (this.chipDrawable.isCloseIconEnabled()) {
                n3 = n2;
                if (this.chipDrawable.getCloseIcon() != null) {
                    n3 = n2 + (this.chipDrawable.getCloseIconStartPadding() + this.chipDrawable.getCloseIconEndPadding() + this.chipDrawable.getCloseIconSize());
                }
            }
            if (this.getPaddingEnd() != n3) {
                ViewCompat.setPaddingRelative((View)this, ViewCompat.getPaddingStart((View)this), this.getPaddingTop(), (int)n3, this.getPaddingBottom());
            }
        }
    }
    
    private void updateTextPaintDrawState(final TextAppearance textAppearance) {
        final TextPaint paint = this.getPaint();
        paint.drawableState = this.chipDrawable.getState();
        textAppearance.updateDrawState(this.getContext(), paint);
    }
    
    private void validateAttributes(final AttributeSet set) {
        if (set == null) {
            return;
        }
        if (set.getAttributeValue("http://schemas.android.com/apk/res/android", "background") != null) {
            throw new UnsupportedOperationException("Do not set the background; Chip manages its own background drawable.");
        }
        if (set.getAttributeValue("http://schemas.android.com/apk/res/android", "drawableLeft") != null) {
            throw new UnsupportedOperationException("Please set left drawable using R.attr#chipIcon.");
        }
        if (set.getAttributeValue("http://schemas.android.com/apk/res/android", "drawableStart") != null) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        }
        if (set.getAttributeValue("http://schemas.android.com/apk/res/android", "drawableEnd") != null) {
            throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
        }
        if (set.getAttributeValue("http://schemas.android.com/apk/res/android", "drawableRight") != null) {
            throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
        }
        if (set.getAttributeBooleanValue("http://schemas.android.com/apk/res/android", "singleLine", true) && set.getAttributeIntValue("http://schemas.android.com/apk/res/android", "lines", 1) == 1 && set.getAttributeIntValue("http://schemas.android.com/apk/res/android", "minLines", 1) == 1 && set.getAttributeIntValue("http://schemas.android.com/apk/res/android", "maxLines", 1) == 1) {
            return;
        }
        throw new UnsupportedOperationException("Chip does not support multi-line text");
    }
    
    protected boolean dispatchHoverEvent(final MotionEvent motionEvent) {
        return this.handleAccessibilityExit(motionEvent) || this.touchHelper.dispatchHoverEvent(motionEvent) || super.dispatchHoverEvent(motionEvent);
    }
    
    public boolean dispatchKeyEvent(final KeyEvent keyEvent) {
        return this.touchHelper.dispatchKeyEvent(keyEvent) || super.dispatchKeyEvent(keyEvent);
    }
    
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null && chipDrawable.isCloseIconStateful() && this.chipDrawable.setCloseIconState(this.createCloseIconDrawableState())) {
            this.invalidate();
        }
    }
    
    public Drawable getCheckedIcon() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getCheckedIcon();
        }
        return null;
    }
    
    public ColorStateList getChipBackgroundColor() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getChipBackgroundColor();
        }
        return null;
    }
    
    public float getChipCornerRadius() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getChipCornerRadius();
        }
        return 0.0f;
    }
    
    public Drawable getChipDrawable() {
        return this.chipDrawable;
    }
    
    public float getChipEndPadding() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getChipEndPadding();
        }
        return 0.0f;
    }
    
    public Drawable getChipIcon() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getChipIcon();
        }
        return null;
    }
    
    public float getChipIconSize() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getChipIconSize();
        }
        return 0.0f;
    }
    
    public ColorStateList getChipIconTint() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getChipIconTint();
        }
        return null;
    }
    
    public float getChipMinHeight() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getChipMinHeight();
        }
        return 0.0f;
    }
    
    public float getChipStartPadding() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getChipStartPadding();
        }
        return 0.0f;
    }
    
    public ColorStateList getChipStrokeColor() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getChipStrokeColor();
        }
        return null;
    }
    
    public float getChipStrokeWidth() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getChipStrokeWidth();
        }
        return 0.0f;
    }
    
    @Deprecated
    public CharSequence getChipText() {
        return this.getText();
    }
    
    public Drawable getCloseIcon() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getCloseIcon();
        }
        return null;
    }
    
    public CharSequence getCloseIconContentDescription() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getCloseIconContentDescription();
        }
        return null;
    }
    
    public float getCloseIconEndPadding() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getCloseIconEndPadding();
        }
        return 0.0f;
    }
    
    public float getCloseIconSize() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getCloseIconSize();
        }
        return 0.0f;
    }
    
    public float getCloseIconStartPadding() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getCloseIconStartPadding();
        }
        return 0.0f;
    }
    
    public ColorStateList getCloseIconTint() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getCloseIconTint();
        }
        return null;
    }
    
    public TextUtils$TruncateAt getEllipsize() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getEllipsize();
        }
        return null;
    }
    
    public void getFocusedRect(final Rect rect) {
        if (this.focusedVirtualView == 0) {
            rect.set(this.getCloseIconTouchBoundsInt());
            return;
        }
        super.getFocusedRect(rect);
    }
    
    public MotionSpec getHideMotionSpec() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getHideMotionSpec();
        }
        return null;
    }
    
    public float getIconEndPadding() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getIconEndPadding();
        }
        return 0.0f;
    }
    
    public float getIconStartPadding() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getIconStartPadding();
        }
        return 0.0f;
    }
    
    public ColorStateList getRippleColor() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getRippleColor();
        }
        return null;
    }
    
    public MotionSpec getShowMotionSpec() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getShowMotionSpec();
        }
        return null;
    }
    
    public CharSequence getText() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getText();
        }
        return "";
    }
    
    public float getTextEndPadding() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getTextEndPadding();
        }
        return 0.0f;
    }
    
    public float getTextStartPadding() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            return chipDrawable.getTextStartPadding();
        }
        return 0.0f;
    }
    
    public boolean isCheckable() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        return chipDrawable != null && chipDrawable.isCheckable();
    }
    
    public boolean isCheckedIconEnabled() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        return chipDrawable != null && chipDrawable.isCheckedIconEnabled();
    }
    
    public boolean isChipIconEnabled() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        return chipDrawable != null && chipDrawable.isChipIconEnabled();
    }
    
    public boolean isCloseIconEnabled() {
        final ChipDrawable chipDrawable = this.chipDrawable;
        return chipDrawable != null && chipDrawable.isCloseIconEnabled();
    }
    
    @Override
    public void onChipDrawableSizeChange() {
        this.updatePaddingInternal();
        this.requestLayout();
        if (Build$VERSION.SDK_INT >= 21) {
            this.invalidateOutline();
        }
    }
    
    protected int[] onCreateDrawableState(final int n) {
        final int[] onCreateDrawableState = super.onCreateDrawableState(n + 1);
        if (this.isChecked()) {
            mergeDrawableStates(onCreateDrawableState, Chip.SELECTED_STATE);
        }
        return onCreateDrawableState;
    }
    
    protected void onDraw(final Canvas canvas) {
        if (!TextUtils.isEmpty(this.getText())) {
            final ChipDrawable chipDrawable = this.chipDrawable;
            if (chipDrawable != null) {
                if (!chipDrawable.shouldDrawText()) {
                    final int save = canvas.save();
                    canvas.translate(this.calculateTextOffsetFromStart(this.chipDrawable), 0.0f);
                    super.onDraw(canvas);
                    canvas.restoreToCount(save);
                    return;
                }
            }
        }
        super.onDraw(canvas);
    }
    
    protected void onFocusChanged(final boolean b, final int n, final Rect rect) {
        if (b) {
            this.setFocusedVirtualView(-1);
        }
        else {
            this.setFocusedVirtualView(Integer.MIN_VALUE);
        }
        this.invalidate();
        super.onFocusChanged(b, n, rect);
        this.touchHelper.onFocusChanged(b, n, rect);
    }
    
    public boolean onHoverEvent(final MotionEvent motionEvent) {
        final int actionMasked = motionEvent.getActionMasked();
        if (actionMasked != 7) {
            if (actionMasked == 10) {
                this.setCloseIconHovered(false);
            }
        }
        else {
            this.setCloseIconHovered(this.getCloseIconTouchBounds().contains(motionEvent.getX(), motionEvent.getY()));
        }
        return super.onHoverEvent(motionEvent);
    }
    
    public boolean onKeyDown(final int n, final KeyEvent keyEvent) {
        final int keyCode = keyEvent.getKeyCode();
        final boolean b = false;
        int n2 = 0;
        Label_0234: {
            if (keyCode != 61) {
                if (keyCode != 66) {
                    switch (keyCode) {
                        default: {
                            n2 = (b ? 1 : 0);
                            break Label_0234;
                        }
                        case 22: {
                            n2 = (b ? 1 : 0);
                            if (keyEvent.hasNoModifiers()) {
                                n2 = (this.moveFocus(ViewUtils.isLayoutRtl((View)this) ^ true) ? 1 : 0);
                            }
                            break Label_0234;
                        }
                        case 21: {
                            n2 = (b ? 1 : 0);
                            if (keyEvent.hasNoModifiers()) {
                                n2 = (this.moveFocus(ViewUtils.isLayoutRtl((View)this)) ? 1 : 0);
                            }
                            break Label_0234;
                        }
                        case 23: {
                            break;
                        }
                    }
                }
                final int focusedVirtualView = this.focusedVirtualView;
                if (focusedVirtualView == -1) {
                    this.performClick();
                    return true;
                }
                if (focusedVirtualView == 0) {
                    this.performCloseIconClick();
                    return true;
                }
                n2 = (b ? 1 : 0);
            }
            else {
                int n3;
                if (keyEvent.hasNoModifiers()) {
                    n3 = 2;
                }
                else if (keyEvent.hasModifiers(1)) {
                    n3 = 1;
                }
                else {
                    n3 = 0;
                }
                n2 = (b ? 1 : 0);
                if (n3 != 0) {
                    final ViewParent parent = this.getParent();
                    Object o = this;
                    View focusSearch;
                    do {
                        focusSearch = ((View)o).focusSearch(n3);
                        if (focusSearch == null || focusSearch == this) {
                            break;
                        }
                        o = focusSearch;
                    } while (focusSearch.getParent() == parent);
                    n2 = (b ? 1 : 0);
                    if (focusSearch != null) {
                        focusSearch.requestFocus();
                        return true;
                    }
                }
            }
        }
        if (n2 != 0) {
            this.invalidate();
            return true;
        }
        return super.onKeyDown(n, keyEvent);
    }
    
    public PointerIcon onResolvePointerIcon(final MotionEvent motionEvent, final int n) {
        if (this.getCloseIconTouchBounds().contains(motionEvent.getX(), motionEvent.getY()) && this.isEnabled()) {
            return PointerIcon.getSystemIcon(this.getContext(), 1002);
        }
        return null;
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        final int actionMasked = motionEvent.getActionMasked();
        final boolean contains = this.getCloseIconTouchBounds().contains(motionEvent.getX(), motionEvent.getY());
        boolean b = false;
        boolean b2 = false;
        Label_0110: {
            Label_0108: {
                Label_0103: {
                    if (actionMasked != 0) {
                        Label_0085: {
                            if (actionMasked != 1) {
                                if (actionMasked != 2) {
                                    if (actionMasked != 3) {
                                        break Label_0108;
                                    }
                                }
                                else {
                                    if (!this.closeIconPressed) {
                                        break Label_0108;
                                    }
                                    if (!contains) {
                                        this.setCloseIconPressed(false);
                                    }
                                    break Label_0103;
                                }
                            }
                            else if (this.closeIconPressed) {
                                this.performCloseIconClick();
                                b2 = true;
                                break Label_0085;
                            }
                            b2 = false;
                        }
                        this.setCloseIconPressed(false);
                        break Label_0110;
                    }
                    if (!contains) {
                        break Label_0108;
                    }
                    this.setCloseIconPressed(true);
                }
                b2 = true;
                break Label_0110;
            }
            b2 = false;
        }
        if (b2 || super.onTouchEvent(motionEvent)) {
            b = true;
        }
        return b;
    }
    
    public boolean performCloseIconClick() {
        this.playSoundEffect(0);
        final View$OnClickListener onCloseIconClickListener = this.onCloseIconClickListener;
        boolean b;
        if (onCloseIconClickListener != null) {
            onCloseIconClickListener.onClick((View)this);
            b = true;
        }
        else {
            b = false;
        }
        this.touchHelper.sendEventForVirtualView(0, 1);
        return b;
    }
    
    public void setBackground(final Drawable background) {
        if (background != this.chipDrawable && background != this.ripple) {
            throw new UnsupportedOperationException("Do not set the background; Chip manages its own background drawable.");
        }
        super.setBackground(background);
    }
    
    public void setBackgroundColor(final int n) {
        throw new UnsupportedOperationException("Do not set the background color; Chip manages its own background drawable.");
    }
    
    public void setBackgroundDrawable(final Drawable backgroundDrawable) {
        if (backgroundDrawable != this.chipDrawable && backgroundDrawable != this.ripple) {
            throw new UnsupportedOperationException("Do not set the background drawable; Chip manages its own background drawable.");
        }
        super.setBackgroundDrawable(backgroundDrawable);
    }
    
    public void setBackgroundResource(final int n) {
        throw new UnsupportedOperationException("Do not set the background resource; Chip manages its own background drawable.");
    }
    
    public void setBackgroundTintList(final ColorStateList list) {
        throw new UnsupportedOperationException("Do not set the background tint list; Chip manages its own background drawable.");
    }
    
    public void setBackgroundTintMode(final PorterDuff$Mode porterDuff$Mode) {
        throw new UnsupportedOperationException("Do not set the background tint mode; Chip manages its own background drawable.");
    }
    
    public void setCheckable(final boolean checkable) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setCheckable(checkable);
        }
    }
    
    public void setCheckableResource(final int checkableResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setCheckableResource(checkableResource);
        }
    }
    
    public void setChecked(final boolean b) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable == null) {
            this.deferredCheckedValue = b;
            return;
        }
        if (chipDrawable.isCheckable()) {
            final boolean checked = this.isChecked();
            super.setChecked(b);
            if (checked != b) {
                final CompoundButton$OnCheckedChangeListener onCheckedChangeListenerInternal = this.onCheckedChangeListenerInternal;
                if (onCheckedChangeListenerInternal != null) {
                    onCheckedChangeListenerInternal.onCheckedChanged((CompoundButton)this, b);
                }
            }
        }
    }
    
    public void setCheckedIcon(final Drawable checkedIcon) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setCheckedIcon(checkedIcon);
        }
    }
    
    public void setCheckedIconEnabled(final boolean checkedIconEnabled) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setCheckedIconEnabled(checkedIconEnabled);
        }
    }
    
    public void setCheckedIconEnabledResource(final int checkedIconEnabledResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setCheckedIconEnabledResource(checkedIconEnabledResource);
        }
    }
    
    public void setCheckedIconResource(final int checkedIconResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setCheckedIconResource(checkedIconResource);
        }
    }
    
    public void setChipBackgroundColor(final ColorStateList chipBackgroundColor) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipBackgroundColor(chipBackgroundColor);
        }
    }
    
    public void setChipBackgroundColorResource(final int chipBackgroundColorResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipBackgroundColorResource(chipBackgroundColorResource);
        }
    }
    
    public void setChipCornerRadius(final float chipCornerRadius) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipCornerRadius(chipCornerRadius);
        }
    }
    
    public void setChipCornerRadiusResource(final int chipCornerRadiusResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipCornerRadiusResource(chipCornerRadiusResource);
        }
    }
    
    public void setChipDrawable(final ChipDrawable chipDrawable) {
        final ChipDrawable chipDrawable2 = this.chipDrawable;
        if (chipDrawable2 != chipDrawable) {
            this.unapplyChipDrawable(chipDrawable2);
            this.applyChipDrawable(this.chipDrawable = chipDrawable);
            if (RippleUtils.USE_FRAMEWORK_RIPPLE) {
                this.ripple = new RippleDrawable(RippleUtils.convertToRippleDrawableColor(this.chipDrawable.getRippleColor()), (Drawable)this.chipDrawable, (Drawable)null);
                this.chipDrawable.setUseCompatRipple(false);
                ViewCompat.setBackground((View)this, (Drawable)this.ripple);
                return;
            }
            this.chipDrawable.setUseCompatRipple(true);
            ViewCompat.setBackground((View)this, this.chipDrawable);
        }
    }
    
    public void setChipEndPadding(final float chipEndPadding) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipEndPadding(chipEndPadding);
        }
    }
    
    public void setChipEndPaddingResource(final int chipEndPaddingResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipEndPaddingResource(chipEndPaddingResource);
        }
    }
    
    public void setChipIcon(final Drawable chipIcon) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipIcon(chipIcon);
        }
    }
    
    public void setChipIconEnabled(final boolean chipIconEnabled) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipIconEnabled(chipIconEnabled);
        }
    }
    
    public void setChipIconEnabledResource(final int chipIconEnabledResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipIconEnabledResource(chipIconEnabledResource);
        }
    }
    
    public void setChipIconResource(final int chipIconResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipIconResource(chipIconResource);
        }
    }
    
    public void setChipIconSize(final float chipIconSize) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipIconSize(chipIconSize);
        }
    }
    
    public void setChipIconSizeResource(final int chipIconSizeResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipIconSizeResource(chipIconSizeResource);
        }
    }
    
    public void setChipIconTint(final ColorStateList chipIconTint) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipIconTint(chipIconTint);
        }
    }
    
    public void setChipIconTintResource(final int chipIconTintResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipIconTintResource(chipIconTintResource);
        }
    }
    
    public void setChipMinHeight(final float chipMinHeight) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipMinHeight(chipMinHeight);
        }
    }
    
    public void setChipMinHeightResource(final int chipMinHeightResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipMinHeightResource(chipMinHeightResource);
        }
    }
    
    public void setChipStartPadding(final float chipStartPadding) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipStartPadding(chipStartPadding);
        }
    }
    
    public void setChipStartPaddingResource(final int chipStartPaddingResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipStartPaddingResource(chipStartPaddingResource);
        }
    }
    
    public void setChipStrokeColor(final ColorStateList chipStrokeColor) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipStrokeColor(chipStrokeColor);
        }
    }
    
    public void setChipStrokeColorResource(final int chipStrokeColorResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipStrokeColorResource(chipStrokeColorResource);
        }
    }
    
    public void setChipStrokeWidth(final float chipStrokeWidth) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipStrokeWidth(chipStrokeWidth);
        }
    }
    
    public void setChipStrokeWidthResource(final int chipStrokeWidthResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setChipStrokeWidthResource(chipStrokeWidthResource);
        }
    }
    
    @Deprecated
    public void setChipText(final CharSequence text) {
        this.setText(text);
    }
    
    @Deprecated
    public void setChipTextResource(final int n) {
        this.setText((CharSequence)this.getResources().getString(n));
    }
    
    public void setCloseIcon(final Drawable closeIcon) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setCloseIcon(closeIcon);
        }
    }
    
    public void setCloseIconContentDescription(final CharSequence closeIconContentDescription) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setCloseIconContentDescription(closeIconContentDescription);
        }
    }
    
    public void setCloseIconEnabled(final boolean closeIconEnabled) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setCloseIconEnabled(closeIconEnabled);
        }
    }
    
    public void setCloseIconEnabledResource(final int closeIconEnabledResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setCloseIconEnabledResource(closeIconEnabledResource);
        }
    }
    
    public void setCloseIconEndPadding(final float closeIconEndPadding) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setCloseIconEndPadding(closeIconEndPadding);
        }
    }
    
    public void setCloseIconEndPaddingResource(final int closeIconEndPaddingResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setCloseIconEndPaddingResource(closeIconEndPaddingResource);
        }
    }
    
    public void setCloseIconResource(final int closeIconResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setCloseIconResource(closeIconResource);
        }
    }
    
    public void setCloseIconSize(final float closeIconSize) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setCloseIconSize(closeIconSize);
        }
    }
    
    public void setCloseIconSizeResource(final int closeIconSizeResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setCloseIconSizeResource(closeIconSizeResource);
        }
    }
    
    public void setCloseIconStartPadding(final float closeIconStartPadding) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setCloseIconStartPadding(closeIconStartPadding);
        }
    }
    
    public void setCloseIconStartPaddingResource(final int closeIconStartPaddingResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setCloseIconStartPaddingResource(closeIconStartPaddingResource);
        }
    }
    
    public void setCloseIconTint(final ColorStateList closeIconTint) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setCloseIconTint(closeIconTint);
        }
    }
    
    public void setCloseIconTintResource(final int closeIconTintResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setCloseIconTintResource(closeIconTintResource);
        }
    }
    
    public void setCompoundDrawables(final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
        if (drawable != null) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        }
        if (drawable3 == null) {
            super.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
            return;
        }
        throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
    }
    
    public void setCompoundDrawablesRelative(final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
        if (drawable != null) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        }
        if (drawable3 == null) {
            super.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
            return;
        }
        throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
    }
    
    public void setCompoundDrawablesRelativeWithIntrinsicBounds(final int n, final int n2, final int n3, final int n4) {
        if (n != 0) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        }
        if (n3 == 0) {
            super.setCompoundDrawablesRelativeWithIntrinsicBounds(n, n2, n3, n4);
            return;
        }
        throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
    }
    
    public void setCompoundDrawablesRelativeWithIntrinsicBounds(final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
        if (drawable != null) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        }
        if (drawable3 == null) {
            super.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
            return;
        }
        throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
    }
    
    public void setCompoundDrawablesWithIntrinsicBounds(final int n, final int n2, final int n3, final int n4) {
        if (n != 0) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        }
        if (n3 == 0) {
            super.setCompoundDrawablesWithIntrinsicBounds(n, n2, n3, n4);
            return;
        }
        throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
    }
    
    public void setCompoundDrawablesWithIntrinsicBounds(final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
        if (drawable != null) {
            throw new UnsupportedOperationException("Please set left drawable using R.attr#chipIcon.");
        }
        if (drawable3 == null) {
            super.setCompoundDrawablesWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
            return;
        }
        throw new UnsupportedOperationException("Please set right drawable using R.attr#closeIcon.");
    }
    
    public void setEllipsize(final TextUtils$TruncateAt textUtils$TruncateAt) {
        if (this.chipDrawable == null) {
            return;
        }
        if (textUtils$TruncateAt != TextUtils$TruncateAt.MARQUEE) {
            super.setEllipsize(textUtils$TruncateAt);
            final ChipDrawable chipDrawable = this.chipDrawable;
            if (chipDrawable != null) {
                chipDrawable.setEllipsize(textUtils$TruncateAt);
            }
            return;
        }
        throw new UnsupportedOperationException("Text within a chip are not allowed to scroll.");
    }
    
    public void setHideMotionSpec(final MotionSpec hideMotionSpec) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setHideMotionSpec(hideMotionSpec);
        }
    }
    
    public void setHideMotionSpecResource(final int hideMotionSpecResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setHideMotionSpecResource(hideMotionSpecResource);
        }
    }
    
    public void setIconEndPadding(final float iconEndPadding) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setIconEndPadding(iconEndPadding);
        }
    }
    
    public void setIconEndPaddingResource(final int iconEndPaddingResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setIconEndPaddingResource(iconEndPaddingResource);
        }
    }
    
    public void setIconStartPadding(final float iconStartPadding) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setIconStartPadding(iconStartPadding);
        }
    }
    
    public void setIconStartPaddingResource(final int iconStartPaddingResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setIconStartPaddingResource(iconStartPaddingResource);
        }
    }
    
    public void setLines(final int lines) {
        if (lines <= 1) {
            super.setLines(lines);
            return;
        }
        throw new UnsupportedOperationException("Chip does not support multi-line text");
    }
    
    public void setMaxLines(final int maxLines) {
        if (maxLines <= 1) {
            super.setMaxLines(maxLines);
            return;
        }
        throw new UnsupportedOperationException("Chip does not support multi-line text");
    }
    
    public void setMinLines(final int minLines) {
        if (minLines <= 1) {
            super.setMinLines(minLines);
            return;
        }
        throw new UnsupportedOperationException("Chip does not support multi-line text");
    }
    
    void setOnCheckedChangeListenerInternal(final CompoundButton$OnCheckedChangeListener onCheckedChangeListenerInternal) {
        this.onCheckedChangeListenerInternal = onCheckedChangeListenerInternal;
    }
    
    public void setOnCloseIconClickListener(final View$OnClickListener onCloseIconClickListener) {
        this.onCloseIconClickListener = onCloseIconClickListener;
    }
    
    public void setRippleColor(final ColorStateList rippleColor) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setRippleColor(rippleColor);
        }
    }
    
    public void setRippleColorResource(final int rippleColorResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setRippleColorResource(rippleColorResource);
        }
    }
    
    public void setShowMotionSpec(final MotionSpec showMotionSpec) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setShowMotionSpec(showMotionSpec);
        }
    }
    
    public void setShowMotionSpecResource(final int showMotionSpecResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setShowMotionSpecResource(showMotionSpecResource);
        }
    }
    
    public void setSingleLine(final boolean singleLine) {
        if (singleLine) {
            super.setSingleLine(singleLine);
            return;
        }
        throw new UnsupportedOperationException("Chip does not support multi-line text");
    }
    
    public void setText(CharSequence unicodeWrap, final TextView$BufferType textView$BufferType) {
        if (this.chipDrawable == null) {
            return;
        }
        CharSequence text;
        if ((text = unicodeWrap) == null) {
            text = "";
        }
        unicodeWrap = BidiFormatter.getInstance().unicodeWrap(text);
        if (this.chipDrawable.shouldDrawText()) {
            unicodeWrap = null;
        }
        super.setText(unicodeWrap, textView$BufferType);
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setText(text);
        }
    }
    
    public void setTextAppearance(final int n) {
        super.setTextAppearance(n);
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setTextAppearanceResource(n);
        }
        this.getTextAppearance().updateMeasureState(this.getContext(), this.getPaint());
        this.updateTextPaintDrawState(this.getTextAppearance());
    }
    
    public void setTextAppearance(final Context context, final int textAppearanceResource) {
        super.setTextAppearance(context, textAppearanceResource);
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setTextAppearanceResource(textAppearanceResource);
        }
        this.getTextAppearance().updateMeasureState(context, this.getPaint());
        this.updateTextPaintDrawState(this.getTextAppearance());
    }
    
    public void setTextAppearance(final TextAppearance textAppearance) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setTextAppearance(textAppearance);
        }
        this.getTextAppearance().updateMeasureState(this.getContext(), this.getPaint());
        this.updateTextPaintDrawState(textAppearance);
    }
    
    public void setTextAppearanceResource(final int textAppearanceResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setTextAppearanceResource(textAppearanceResource);
        }
        this.setTextAppearance(this.getContext(), textAppearanceResource);
    }
    
    public void setTextEndPadding(final float textEndPadding) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setTextEndPadding(textEndPadding);
        }
    }
    
    public void setTextEndPaddingResource(final int textEndPaddingResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setTextEndPaddingResource(textEndPaddingResource);
        }
    }
    
    public void setTextStartPadding(final float textStartPadding) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setTextStartPadding(textStartPadding);
        }
    }
    
    public void setTextStartPaddingResource(final int textStartPaddingResource) {
        final ChipDrawable chipDrawable = this.chipDrawable;
        if (chipDrawable != null) {
            chipDrawable.setTextStartPaddingResource(textStartPaddingResource);
        }
    }
    
    private class ChipTouchHelper extends ExploreByTouchHelper
    {
        ChipTouchHelper(final Chip chip) {
            super((View)chip);
        }
        
        @Override
        protected int getVirtualViewAt(final float n, final float n2) {
            if (Chip.this.hasCloseIcon() && Chip.this.getCloseIconTouchBounds().contains(n, n2)) {
                return 0;
            }
            return -1;
        }
        
        @Override
        protected void getVisibleVirtualViews(final List<Integer> list) {
            if (Chip.this.hasCloseIcon()) {
                list.add(0);
            }
        }
        
        @Override
        protected boolean onPerformActionForVirtualView(final int n, final int n2, final Bundle bundle) {
            return n2 == 16 && n == 0 && Chip.this.performCloseIconClick();
        }
        
        @Override
        protected void onPopulateNodeForHost(final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            accessibilityNodeInfoCompat.setCheckable(Chip.this.chipDrawable != null && Chip.this.chipDrawable.isCheckable());
            accessibilityNodeInfoCompat.setClassName(Chip.class.getName());
            CharSequence text;
            if (Chip.this.chipDrawable != null) {
                text = Chip.this.chipDrawable.getText();
            }
            else {
                text = "";
            }
            if (Build$VERSION.SDK_INT >= 23) {
                accessibilityNodeInfoCompat.setText(text);
                return;
            }
            accessibilityNodeInfoCompat.setContentDescription(text);
        }
        
        @Override
        protected void onPopulateNodeForVirtualView(int mtrl_chip_close_icon_content_description, final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            if (Chip.this.hasCloseIcon()) {
                final CharSequence closeIconContentDescription = Chip.this.getCloseIconContentDescription();
                if (closeIconContentDescription != null) {
                    accessibilityNodeInfoCompat.setContentDescription(closeIconContentDescription);
                }
                else {
                    CharSequence text = Chip.this.getText();
                    final Context context = Chip.this.getContext();
                    mtrl_chip_close_icon_content_description = R.string.mtrl_chip_close_icon_content_description;
                    if (TextUtils.isEmpty(text)) {
                        text = "";
                    }
                    accessibilityNodeInfoCompat.setContentDescription(context.getString(mtrl_chip_close_icon_content_description, new Object[] { text }).trim());
                }
                accessibilityNodeInfoCompat.setBoundsInParent(Chip.this.getCloseIconTouchBoundsInt());
                accessibilityNodeInfoCompat.addAction(AccessibilityNodeInfoCompat.AccessibilityActionCompat.ACTION_CLICK);
                accessibilityNodeInfoCompat.setEnabled(Chip.this.isEnabled());
                return;
            }
            accessibilityNodeInfoCompat.setContentDescription("");
            accessibilityNodeInfoCompat.setBoundsInParent(Chip.EMPTY_BOUNDS);
        }
    }
}
