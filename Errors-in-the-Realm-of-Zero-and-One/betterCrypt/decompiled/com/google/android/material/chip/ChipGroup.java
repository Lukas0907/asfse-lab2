// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.chip;

import android.graphics.ColorFilter;
import android.graphics.Canvas;
import android.os.Build$VERSION;
import android.view.ViewGroup$MarginLayoutParams;
import android.os.Parcel;
import android.widget.CompoundButton;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import android.content.res.TypedArray;
import android.view.ViewGroup$OnHierarchyChangeListener;
import android.graphics.drawable.Drawable;
import com.google.android.material.internal.ThemeEnforcement;
import com.google.android.material.R;
import android.util.AttributeSet;
import android.content.Context;
import com.google.android.material.internal.FlexboxLayout;

public class ChipGroup extends FlexboxLayout
{
    private int checkedId;
    private final CheckedStateTracker checkedStateTracker;
    private int chipSpacingHorizontal;
    private int chipSpacingVertical;
    private OnCheckedChangeListener onCheckedChangeListener;
    private PassThroughHierarchyChangeListener passThroughListener;
    private boolean protectFromCheckedChange;
    private boolean singleLine;
    private boolean singleSelection;
    private final SpacingDrawable spacingDrawable;
    
    public ChipGroup(final Context context) {
        this(context, null);
    }
    
    public ChipGroup(final Context context, final AttributeSet set) {
        this(context, set, R.attr.chipGroupStyle);
    }
    
    public ChipGroup(final Context context, final AttributeSet set, int checkedId) {
        super(context, set, checkedId);
        this.spacingDrawable = new SpacingDrawable();
        this.checkedStateTracker = new CheckedStateTracker();
        this.passThroughListener = new PassThroughHierarchyChangeListener();
        this.checkedId = -1;
        this.protectFromCheckedChange = false;
        final TypedArray obtainStyledAttributes = ThemeEnforcement.obtainStyledAttributes(context, set, R.styleable.ChipGroup, checkedId, R.style.Widget_MaterialComponents_ChipGroup);
        checkedId = obtainStyledAttributes.getDimensionPixelOffset(R.styleable.ChipGroup_chipSpacing, 0);
        this.setChipSpacingHorizontal(obtainStyledAttributes.getDimensionPixelOffset(R.styleable.ChipGroup_chipSpacingHorizontal, checkedId));
        this.setChipSpacingVertical(obtainStyledAttributes.getDimensionPixelOffset(R.styleable.ChipGroup_chipSpacingVertical, checkedId));
        this.setSingleLine(obtainStyledAttributes.getBoolean(R.styleable.ChipGroup_singleLine, false));
        this.setSingleSelection(obtainStyledAttributes.getBoolean(R.styleable.ChipGroup_singleSelection, false));
        checkedId = obtainStyledAttributes.getResourceId(R.styleable.ChipGroup_checkedChip, -1);
        if (checkedId != -1) {
            this.checkedId = checkedId;
        }
        obtainStyledAttributes.recycle();
        this.setDividerDrawable(this.spacingDrawable);
        this.setShowDivider(2);
        this.setWillNotDraw(true);
        super.setOnHierarchyChangeListener((ViewGroup$OnHierarchyChangeListener)this.passThroughListener);
    }
    
    private void setCheckedId(final int checkedId) {
        this.checkedId = checkedId;
        final OnCheckedChangeListener onCheckedChangeListener = this.onCheckedChangeListener;
        if (onCheckedChangeListener != null && this.singleSelection) {
            onCheckedChangeListener.onCheckedChanged(this, checkedId);
        }
    }
    
    private void setCheckedStateForView(final int n, final boolean checked) {
        final View viewById = this.findViewById(n);
        if (viewById instanceof Chip) {
            this.protectFromCheckedChange = true;
            ((Chip)viewById).setChecked(checked);
            this.protectFromCheckedChange = false;
        }
    }
    
    @Override
    public void addView(final View view, final int n, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        if (view instanceof Chip) {
            final Chip chip = (Chip)view;
            if (chip.isChecked()) {
                final int checkedId = this.checkedId;
                if (checkedId != -1 && this.singleSelection) {
                    this.setCheckedStateForView(checkedId, false);
                }
                this.setCheckedId(chip.getId());
            }
        }
        super.addView(view, n, viewGroup$LayoutParams);
    }
    
    public void check(final int checkedId) {
        final int checkedId2 = this.checkedId;
        if (checkedId == checkedId2) {
            return;
        }
        if (checkedId2 != -1 && this.singleSelection) {
            this.setCheckedStateForView(checkedId2, false);
        }
        if (checkedId != -1) {
            this.setCheckedStateForView(checkedId, true);
        }
        this.setCheckedId(checkedId);
    }
    
    @Override
    protected boolean checkLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return super.checkLayoutParams(viewGroup$LayoutParams) && viewGroup$LayoutParams instanceof LayoutParams;
    }
    
    public void clearCheck() {
        this.protectFromCheckedChange = true;
        for (int i = 0; i < this.getChildCount(); ++i) {
            final View child = this.getChildAt(i);
            if (child instanceof Chip) {
                ((Chip)child).setChecked(false);
            }
        }
        this.protectFromCheckedChange = false;
        this.setCheckedId(-1);
    }
    
    protected ViewGroup$LayoutParams generateDefaultLayoutParams() {
        return (ViewGroup$LayoutParams)new LayoutParams(-2, -2);
    }
    
    @Override
    protected ViewGroup$LayoutParams generateLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return (ViewGroup$LayoutParams)new LayoutParams(viewGroup$LayoutParams);
    }
    
    @Override
    public FlexboxLayout.LayoutParams generateLayoutParams(final AttributeSet set) {
        return new LayoutParams(this.getContext(), set);
    }
    
    public int getCheckedChipId() {
        if (this.singleSelection) {
            return this.checkedId;
        }
        return -1;
    }
    
    public int getChipSpacingHorizontal() {
        return this.chipSpacingHorizontal;
    }
    
    public int getChipSpacingVertical() {
        return this.chipSpacingVertical;
    }
    
    public boolean isSingleLine() {
        return this.singleLine;
    }
    
    public boolean isSingleSelection() {
        return this.singleSelection;
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        final int checkedId = this.checkedId;
        if (checkedId != -1) {
            this.setCheckedStateForView(checkedId, true);
            this.setCheckedId(this.checkedId);
        }
    }
    
    public void setChipSpacing(final int n) {
        this.setChipSpacingHorizontal(n);
        this.setChipSpacingVertical(n);
    }
    
    public void setChipSpacingHorizontal(final int chipSpacingHorizontal) {
        if (this.chipSpacingHorizontal != chipSpacingHorizontal) {
            this.chipSpacingHorizontal = chipSpacingHorizontal;
            this.requestLayout();
        }
    }
    
    public void setChipSpacingHorizontalResource(final int n) {
        this.setChipSpacingHorizontal(this.getResources().getDimensionPixelOffset(n));
    }
    
    public void setChipSpacingResource(final int n) {
        this.setChipSpacing(this.getResources().getDimensionPixelOffset(n));
    }
    
    public void setChipSpacingVertical(final int chipSpacingVertical) {
        if (this.chipSpacingVertical != chipSpacingVertical) {
            this.chipSpacingVertical = chipSpacingVertical;
            this.requestLayout();
        }
    }
    
    public void setChipSpacingVerticalResource(final int n) {
        this.setChipSpacingVertical(this.getResources().getDimensionPixelOffset(n));
    }
    
    @Override
    public void setDividerDrawableHorizontal(final Drawable dividerDrawableHorizontal) {
        if (dividerDrawableHorizontal == this.spacingDrawable) {
            super.setDividerDrawableHorizontal(dividerDrawableHorizontal);
            return;
        }
        throw new UnsupportedOperationException("Changing divider drawables not allowed. ChipGroup uses divider drawables as spacing.");
    }
    
    @Override
    public void setDividerDrawableVertical(final Drawable dividerDrawableVertical) {
        if (dividerDrawableVertical == this.spacingDrawable) {
            super.setDividerDrawableVertical(dividerDrawableVertical);
            return;
        }
        throw new UnsupportedOperationException("Changing divider drawables not allowed. ChipGroup uses divider drawables as spacing.");
    }
    
    @Override
    public void setFlexWrap(final int n) {
        throw new UnsupportedOperationException("Changing flex wrap not allowed. ChipGroup exposes a singleLine attribute instead.");
    }
    
    public void setOnCheckedChangeListener(final OnCheckedChangeListener onCheckedChangeListener) {
        this.onCheckedChangeListener = onCheckedChangeListener;
    }
    
    public void setOnHierarchyChangeListener(final ViewGroup$OnHierarchyChangeListener viewGroup$OnHierarchyChangeListener) {
        this.passThroughListener.onHierarchyChangeListener = viewGroup$OnHierarchyChangeListener;
    }
    
    @Override
    public void setShowDividerHorizontal(final int showDividerHorizontal) {
        if (showDividerHorizontal == 2) {
            super.setShowDividerHorizontal(showDividerHorizontal);
            return;
        }
        throw new UnsupportedOperationException("Changing divider modes not allowed. ChipGroup uses divider drawables as spacing.");
    }
    
    @Override
    public void setShowDividerVertical(final int showDividerVertical) {
        if (showDividerVertical == 2) {
            super.setShowDividerVertical(showDividerVertical);
            return;
        }
        throw new UnsupportedOperationException("Changing divider modes not allowed. ChipGroup uses divider drawables as spacing.");
    }
    
    public void setSingleLine(final int n) {
        this.setSingleLine(this.getResources().getBoolean(n));
    }
    
    public void setSingleLine(final boolean singleLine) {
        super.setFlexWrap(((this.singleLine = singleLine) ^ true) ? 1 : 0);
    }
    
    public void setSingleSelection(final int n) {
        this.setSingleSelection(this.getResources().getBoolean(n));
    }
    
    public void setSingleSelection(final boolean singleSelection) {
        if (this.singleSelection != singleSelection) {
            this.singleSelection = singleSelection;
            this.clearCheck();
        }
    }
    
    private class CheckedStateTracker implements CompoundButton$OnCheckedChangeListener
    {
        public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
            if (ChipGroup.this.protectFromCheckedChange) {
                return;
            }
            final int id = compoundButton.getId();
            if (b) {
                if (ChipGroup.this.checkedId != -1 && ChipGroup.this.checkedId != id && ChipGroup.this.singleSelection) {
                    final ChipGroup this$0 = ChipGroup.this;
                    this$0.setCheckedStateForView(this$0.checkedId, false);
                }
                ChipGroup.this.setCheckedId(id);
                return;
            }
            if (ChipGroup.this.checkedId == id) {
                ChipGroup.this.setCheckedId(-1);
            }
        }
    }
    
    public static class LayoutParams extends FlexboxLayout.LayoutParams
    {
        public LayoutParams(final int n, final int n2) {
            super(n, n2);
        }
        
        public LayoutParams(final Context context, final AttributeSet set) {
            super(context, set);
        }
        
        protected LayoutParams(final Parcel parcel) {
            super(parcel);
        }
        
        public LayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
        }
        
        public LayoutParams(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            super(viewGroup$MarginLayoutParams);
        }
        
        public LayoutParams(final FlexboxLayout.LayoutParams layoutParams) {
            super(layoutParams);
        }
    }
    
    public interface OnCheckedChangeListener
    {
        void onCheckedChanged(final ChipGroup p0, final int p1);
    }
    
    private class PassThroughHierarchyChangeListener implements ViewGroup$OnHierarchyChangeListener
    {
        private ViewGroup$OnHierarchyChangeListener onHierarchyChangeListener;
        
        public void onChildViewAdded(final View view, final View view2) {
            if (view == ChipGroup.this && view2 instanceof Chip) {
                if (view2.getId() == -1) {
                    int id;
                    if (Build$VERSION.SDK_INT >= 17) {
                        id = View.generateViewId();
                    }
                    else {
                        id = view2.hashCode();
                    }
                    view2.setId(id);
                }
                ((Chip)view2).setOnCheckedChangeListenerInternal((CompoundButton$OnCheckedChangeListener)ChipGroup.this.checkedStateTracker);
            }
            final ViewGroup$OnHierarchyChangeListener onHierarchyChangeListener = this.onHierarchyChangeListener;
            if (onHierarchyChangeListener != null) {
                onHierarchyChangeListener.onChildViewAdded(view, view2);
            }
        }
        
        public void onChildViewRemoved(final View view, final View view2) {
            if (view == ChipGroup.this && view2 instanceof Chip) {
                ((Chip)view2).setOnCheckedChangeListenerInternal(null);
            }
            final ViewGroup$OnHierarchyChangeListener onHierarchyChangeListener = this.onHierarchyChangeListener;
            if (onHierarchyChangeListener != null) {
                onHierarchyChangeListener.onChildViewRemoved(view, view2);
            }
        }
    }
    
    private class SpacingDrawable extends Drawable
    {
        public void draw(final Canvas canvas) {
        }
        
        public int getIntrinsicHeight() {
            return ChipGroup.this.chipSpacingVertical;
        }
        
        public int getIntrinsicWidth() {
            return ChipGroup.this.chipSpacingHorizontal;
        }
        
        public int getOpacity() {
            return -2;
        }
        
        public void setAlpha(final int n) {
        }
        
        public void setColorFilter(final ColorFilter colorFilter) {
        }
    }
}
