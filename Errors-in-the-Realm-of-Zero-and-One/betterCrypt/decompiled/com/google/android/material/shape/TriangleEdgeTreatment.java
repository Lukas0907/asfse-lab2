// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.shape;

public class TriangleEdgeTreatment extends EdgeTreatment
{
    private final boolean inside;
    private final float size;
    
    public TriangleEdgeTreatment(final float size, final boolean inside) {
        this.size = size;
        this.inside = inside;
    }
    
    @Override
    public void getEdgePath(final float n, final float n2, final ShapePath shapePath) {
        final float n3 = n / 2.0f;
        shapePath.lineTo(n3 - this.size * n2, 0.0f);
        float size;
        if (this.inside) {
            size = this.size;
        }
        else {
            size = -this.size;
        }
        shapePath.lineTo(n3, size * n2);
        shapePath.lineTo(n3 + this.size * n2, 0.0f);
        shapePath.lineTo(n, 0.0f);
    }
}
