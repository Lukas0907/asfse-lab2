// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.shape;

import android.view.ViewTreeObserver;
import android.view.View;
import android.view.ViewTreeObserver$OnScrollChangedListener;
import android.widget.ScrollView;

public class InterpolateOnScrollPositionChangeHelper
{
    private final int[] containerLocation;
    private ScrollView containingScrollView;
    private MaterialShapeDrawable materialShapeDrawable;
    private final ViewTreeObserver$OnScrollChangedListener scrollChangedListener;
    private final int[] scrollLocation;
    private View shapedView;
    
    public InterpolateOnScrollPositionChangeHelper(final View shapedView, final MaterialShapeDrawable materialShapeDrawable, final ScrollView containingScrollView) {
        this.scrollLocation = new int[2];
        this.containerLocation = new int[2];
        this.scrollChangedListener = (ViewTreeObserver$OnScrollChangedListener)new ViewTreeObserver$OnScrollChangedListener() {
            public void onScrollChanged() {
                InterpolateOnScrollPositionChangeHelper.this.updateInterpolationForScreenPosition();
            }
        };
        this.shapedView = shapedView;
        this.materialShapeDrawable = materialShapeDrawable;
        this.containingScrollView = containingScrollView;
    }
    
    public void setContainingScrollView(final ScrollView containingScrollView) {
        this.containingScrollView = containingScrollView;
    }
    
    public void setMaterialShapeDrawable(final MaterialShapeDrawable materialShapeDrawable) {
        this.materialShapeDrawable = materialShapeDrawable;
    }
    
    public void startListeningForScrollChanges(final ViewTreeObserver viewTreeObserver) {
        viewTreeObserver.addOnScrollChangedListener(this.scrollChangedListener);
    }
    
    public void stopListeningForScrollChanges(final ViewTreeObserver viewTreeObserver) {
        viewTreeObserver.removeOnScrollChangedListener(this.scrollChangedListener);
    }
    
    public void updateInterpolationForScreenPosition() {
        final ScrollView containingScrollView = this.containingScrollView;
        if (containingScrollView == null) {
            return;
        }
        if (containingScrollView.getChildCount() == 0) {
            throw new IllegalStateException("Scroll bar must contain a child to calculate interpolation.");
        }
        this.containingScrollView.getLocationInWindow(this.scrollLocation);
        this.containingScrollView.getChildAt(0).getLocationInWindow(this.containerLocation);
        final int n = this.shapedView.getTop() - this.scrollLocation[1] + this.containerLocation[1];
        final int height = this.shapedView.getHeight();
        final int height2 = this.containingScrollView.getHeight();
        if (n < 0) {
            this.materialShapeDrawable.setInterpolation(Math.max(0.0f, Math.min(1.0f, n / (float)height + 1.0f)));
            this.shapedView.invalidate();
            return;
        }
        final int n2 = n + height;
        if (n2 > height2) {
            this.materialShapeDrawable.setInterpolation(Math.max(0.0f, Math.min(1.0f, 1.0f - (n2 - height2) / (float)height)));
            this.shapedView.invalidate();
            return;
        }
        if (this.materialShapeDrawable.getInterpolation() != 1.0f) {
            this.materialShapeDrawable.setInterpolation(1.0f);
            this.shapedView.invalidate();
        }
    }
}
