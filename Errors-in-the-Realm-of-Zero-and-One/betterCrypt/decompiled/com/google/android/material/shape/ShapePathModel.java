// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.shape;

public class ShapePathModel
{
    private static final CornerTreatment DEFAULT_CORNER_TREATMENT;
    private static final EdgeTreatment DEFAULT_EDGE_TREATMENT;
    private EdgeTreatment bottomEdge;
    private CornerTreatment bottomLeftCorner;
    private CornerTreatment bottomRightCorner;
    private EdgeTreatment leftEdge;
    private EdgeTreatment rightEdge;
    private EdgeTreatment topEdge;
    private CornerTreatment topLeftCorner;
    private CornerTreatment topRightCorner;
    
    static {
        DEFAULT_CORNER_TREATMENT = new CornerTreatment();
        DEFAULT_EDGE_TREATMENT = new EdgeTreatment();
    }
    
    public ShapePathModel() {
        final CornerTreatment default_CORNER_TREATMENT = ShapePathModel.DEFAULT_CORNER_TREATMENT;
        this.topLeftCorner = default_CORNER_TREATMENT;
        this.topRightCorner = default_CORNER_TREATMENT;
        this.bottomRightCorner = default_CORNER_TREATMENT;
        this.bottomLeftCorner = default_CORNER_TREATMENT;
        final EdgeTreatment default_EDGE_TREATMENT = ShapePathModel.DEFAULT_EDGE_TREATMENT;
        this.topEdge = default_EDGE_TREATMENT;
        this.rightEdge = default_EDGE_TREATMENT;
        this.bottomEdge = default_EDGE_TREATMENT;
        this.leftEdge = default_EDGE_TREATMENT;
    }
    
    public EdgeTreatment getBottomEdge() {
        return this.bottomEdge;
    }
    
    public CornerTreatment getBottomLeftCorner() {
        return this.bottomLeftCorner;
    }
    
    public CornerTreatment getBottomRightCorner() {
        return this.bottomRightCorner;
    }
    
    public EdgeTreatment getLeftEdge() {
        return this.leftEdge;
    }
    
    public EdgeTreatment getRightEdge() {
        return this.rightEdge;
    }
    
    public EdgeTreatment getTopEdge() {
        return this.topEdge;
    }
    
    public CornerTreatment getTopLeftCorner() {
        return this.topLeftCorner;
    }
    
    public CornerTreatment getTopRightCorner() {
        return this.topRightCorner;
    }
    
    public void setAllCorners(final CornerTreatment cornerTreatment) {
        this.topLeftCorner = cornerTreatment;
        this.topRightCorner = cornerTreatment;
        this.bottomRightCorner = cornerTreatment;
        this.bottomLeftCorner = cornerTreatment;
    }
    
    public void setAllEdges(final EdgeTreatment edgeTreatment) {
        this.leftEdge = edgeTreatment;
        this.topEdge = edgeTreatment;
        this.rightEdge = edgeTreatment;
        this.bottomEdge = edgeTreatment;
    }
    
    public void setBottomEdge(final EdgeTreatment bottomEdge) {
        this.bottomEdge = bottomEdge;
    }
    
    public void setBottomLeftCorner(final CornerTreatment bottomLeftCorner) {
        this.bottomLeftCorner = bottomLeftCorner;
    }
    
    public void setBottomRightCorner(final CornerTreatment bottomRightCorner) {
        this.bottomRightCorner = bottomRightCorner;
    }
    
    public void setCornerTreatments(final CornerTreatment topLeftCorner, final CornerTreatment topRightCorner, final CornerTreatment bottomRightCorner, final CornerTreatment bottomLeftCorner) {
        this.topLeftCorner = topLeftCorner;
        this.topRightCorner = topRightCorner;
        this.bottomRightCorner = bottomRightCorner;
        this.bottomLeftCorner = bottomLeftCorner;
    }
    
    public void setEdgeTreatments(final EdgeTreatment leftEdge, final EdgeTreatment topEdge, final EdgeTreatment rightEdge, final EdgeTreatment bottomEdge) {
        this.leftEdge = leftEdge;
        this.topEdge = topEdge;
        this.rightEdge = rightEdge;
        this.bottomEdge = bottomEdge;
    }
    
    public void setLeftEdge(final EdgeTreatment leftEdge) {
        this.leftEdge = leftEdge;
    }
    
    public void setRightEdge(final EdgeTreatment rightEdge) {
        this.rightEdge = rightEdge;
    }
    
    public void setTopEdge(final EdgeTreatment topEdge) {
        this.topEdge = topEdge;
    }
    
    public void setTopLeftCorner(final CornerTreatment topLeftCorner) {
        this.topLeftCorner = topLeftCorner;
    }
    
    public void setTopRightCorner(final CornerTreatment topRightCorner) {
        this.topRightCorner = topRightCorner;
    }
}
