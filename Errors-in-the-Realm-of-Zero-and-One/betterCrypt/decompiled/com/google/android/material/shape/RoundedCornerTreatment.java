// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.shape;

public class RoundedCornerTreatment extends CornerTreatment
{
    private final float radius;
    
    public RoundedCornerTreatment(final float radius) {
        this.radius = radius;
    }
    
    @Override
    public void getCornerPath(final float n, final float n2, final ShapePath shapePath) {
        shapePath.reset(0.0f, this.radius * n2);
        final float radius = this.radius;
        shapePath.addArc(0.0f, 0.0f, radius * 2.0f * n2, radius * 2.0f * n2, n + 180.0f, 90.0f);
    }
}
