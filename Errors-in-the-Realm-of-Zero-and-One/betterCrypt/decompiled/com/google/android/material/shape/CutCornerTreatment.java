// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.shape;

public class CutCornerTreatment extends CornerTreatment
{
    private final float size;
    
    public CutCornerTreatment(final float size) {
        this.size = size;
    }
    
    @Override
    public void getCornerPath(final float n, final float n2, final ShapePath shapePath) {
        shapePath.reset(0.0f, this.size * n2);
        final double n3 = n;
        final double sin = Math.sin(n3);
        final double n4 = this.size;
        final double n5 = n2;
        shapePath.lineTo((float)(sin * n4 * n5), (float)(Math.cos(n3) * this.size * n5));
    }
}
