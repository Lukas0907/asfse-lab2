// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.resources;

import android.text.TextPaint;
import android.content.res.Resources$NotFoundException;
import android.util.Log;
import androidx.core.content.res.ResourcesCompat;
import android.content.res.TypedArray;
import com.google.android.material.R;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Typeface;

public class TextAppearance
{
    private static final String TAG = "TextAppearance";
    private static final int TYPEFACE_MONOSPACE = 3;
    private static final int TYPEFACE_SANS = 1;
    private static final int TYPEFACE_SERIF = 2;
    private Typeface font;
    public final String fontFamily;
    private final int fontFamilyResourceId;
    private boolean fontResolved;
    public final ColorStateList shadowColor;
    public final float shadowDx;
    public final float shadowDy;
    public final float shadowRadius;
    public final boolean textAllCaps;
    public final ColorStateList textColor;
    public final ColorStateList textColorHint;
    public final ColorStateList textColorLink;
    public final float textSize;
    public final int textStyle;
    public final int typeface;
    
    public TextAppearance(final Context context, int indexWithValue) {
        this.fontResolved = false;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(indexWithValue, R.styleable.TextAppearance);
        this.textSize = obtainStyledAttributes.getDimension(R.styleable.TextAppearance_android_textSize, 0.0f);
        this.textColor = MaterialResources.getColorStateList(context, obtainStyledAttributes, R.styleable.TextAppearance_android_textColor);
        this.textColorHint = MaterialResources.getColorStateList(context, obtainStyledAttributes, R.styleable.TextAppearance_android_textColorHint);
        this.textColorLink = MaterialResources.getColorStateList(context, obtainStyledAttributes, R.styleable.TextAppearance_android_textColorLink);
        this.textStyle = obtainStyledAttributes.getInt(R.styleable.TextAppearance_android_textStyle, 0);
        this.typeface = obtainStyledAttributes.getInt(R.styleable.TextAppearance_android_typeface, 1);
        indexWithValue = MaterialResources.getIndexWithValue(obtainStyledAttributes, R.styleable.TextAppearance_fontFamily, R.styleable.TextAppearance_android_fontFamily);
        this.fontFamilyResourceId = obtainStyledAttributes.getResourceId(indexWithValue, 0);
        this.fontFamily = obtainStyledAttributes.getString(indexWithValue);
        this.textAllCaps = obtainStyledAttributes.getBoolean(R.styleable.TextAppearance_textAllCaps, false);
        this.shadowColor = MaterialResources.getColorStateList(context, obtainStyledAttributes, R.styleable.TextAppearance_android_shadowColor);
        this.shadowDx = obtainStyledAttributes.getFloat(R.styleable.TextAppearance_android_shadowDx, 0.0f);
        this.shadowDy = obtainStyledAttributes.getFloat(R.styleable.TextAppearance_android_shadowDy, 0.0f);
        this.shadowRadius = obtainStyledAttributes.getFloat(R.styleable.TextAppearance_android_shadowRadius, 0.0f);
        obtainStyledAttributes.recycle();
    }
    
    public Typeface getFont(final Context context) {
        if (this.fontResolved) {
            return this.font;
        }
        if (context.isRestricted()) {
            goto Label_0092;
        }
        try {
            this.font = ResourcesCompat.getFont(context, this.fontFamilyResourceId);
            if (this.font != null) {
                this.font = Typeface.create(this.font, this.textStyle);
                goto Label_0092;
            }
            goto Label_0092;
        }
        catch (Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Error loading font ");
            sb.append(this.fontFamily);
            Log.d("TextAppearance", sb.toString(), (Throwable)ex);
        }
        catch (UnsupportedOperationException | Resources$NotFoundException ex2) {
            goto Label_0092;
        }
    }
    
    public void updateDrawState(final Context context, final TextPaint textPaint) {
        this.updateMeasureState(context, textPaint);
        final ColorStateList textColor = this.textColor;
        int colorForState;
        if (textColor != null) {
            colorForState = textColor.getColorForState(textPaint.drawableState, this.textColor.getDefaultColor());
        }
        else {
            colorForState = -16777216;
        }
        textPaint.setColor(colorForState);
        final float shadowRadius = this.shadowRadius;
        final float shadowDx = this.shadowDx;
        final float shadowDy = this.shadowDy;
        final ColorStateList shadowColor = this.shadowColor;
        int colorForState2;
        if (shadowColor != null) {
            colorForState2 = shadowColor.getColorForState(textPaint.drawableState, this.shadowColor.getDefaultColor());
        }
        else {
            colorForState2 = 0;
        }
        textPaint.setShadowLayer(shadowRadius, shadowDx, shadowDy, colorForState2);
    }
    
    public void updateMeasureState(final Context context, final TextPaint textPaint) {
        final Typeface font = this.getFont(context);
        textPaint.setTypeface(font);
        final int n = font.getStyle() & this.textStyle;
        textPaint.setFakeBoldText((n & 0x1) != 0x0);
        float textSkewX;
        if ((n & 0x2) != 0x0) {
            textSkewX = -0.25f;
        }
        else {
            textSkewX = 0.0f;
        }
        textPaint.setTextSkewX(textSkewX);
        textPaint.setTextSize(this.textSize);
    }
}
