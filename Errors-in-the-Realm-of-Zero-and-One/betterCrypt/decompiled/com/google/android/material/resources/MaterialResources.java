// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.resources;

import android.graphics.drawable.Drawable;
import androidx.appcompat.content.res.AppCompatResources;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.content.Context;

public class MaterialResources
{
    private MaterialResources() {
    }
    
    public static ColorStateList getColorStateList(final Context context, final TypedArray typedArray, final int n) {
        if (typedArray.hasValue(n)) {
            final int resourceId = typedArray.getResourceId(n, 0);
            if (resourceId != 0) {
                final ColorStateList colorStateList = AppCompatResources.getColorStateList(context, resourceId);
                if (colorStateList != null) {
                    return colorStateList;
                }
            }
        }
        return typedArray.getColorStateList(n);
    }
    
    public static Drawable getDrawable(final Context context, final TypedArray typedArray, final int n) {
        if (typedArray.hasValue(n)) {
            final int resourceId = typedArray.getResourceId(n, 0);
            if (resourceId != 0) {
                final Drawable drawable = AppCompatResources.getDrawable(context, resourceId);
                if (drawable != null) {
                    return drawable;
                }
            }
        }
        return typedArray.getDrawable(n);
    }
    
    static int getIndexWithValue(final TypedArray typedArray, final int n, final int n2) {
        if (typedArray.hasValue(n)) {
            return n;
        }
        return n2;
    }
    
    public static TextAppearance getTextAppearance(final Context context, final TypedArray typedArray, int resourceId) {
        if (typedArray.hasValue(resourceId)) {
            resourceId = typedArray.getResourceId(resourceId, 0);
            if (resourceId != 0) {
                return new TextAppearance(context, resourceId);
            }
        }
        return null;
    }
}
