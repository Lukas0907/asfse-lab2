// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.ripple;

import androidx.core.graphics.ColorUtils;
import android.graphics.Color;
import android.util.StateSet;
import android.content.res.ColorStateList;
import android.os.Build$VERSION;

public class RippleUtils
{
    private static final int[] FOCUSED_STATE_SET;
    private static final int[] HOVERED_FOCUSED_STATE_SET;
    private static final int[] HOVERED_STATE_SET;
    private static final int[] PRESSED_STATE_SET;
    private static final int[] SELECTED_FOCUSED_STATE_SET;
    private static final int[] SELECTED_HOVERED_FOCUSED_STATE_SET;
    private static final int[] SELECTED_HOVERED_STATE_SET;
    private static final int[] SELECTED_PRESSED_STATE_SET;
    private static final int[] SELECTED_STATE_SET;
    public static final boolean USE_FRAMEWORK_RIPPLE;
    
    static {
        USE_FRAMEWORK_RIPPLE = (Build$VERSION.SDK_INT >= 21);
        PRESSED_STATE_SET = new int[] { 16842919 };
        HOVERED_FOCUSED_STATE_SET = new int[] { 16843623, 16842908 };
        FOCUSED_STATE_SET = new int[] { 16842908 };
        HOVERED_STATE_SET = new int[] { 16843623 };
        SELECTED_PRESSED_STATE_SET = new int[] { 16842913, 16842919 };
        SELECTED_HOVERED_FOCUSED_STATE_SET = new int[] { 16842913, 16843623, 16842908 };
        SELECTED_FOCUSED_STATE_SET = new int[] { 16842913, 16842908 };
        SELECTED_HOVERED_STATE_SET = new int[] { 16842913, 16843623 };
        SELECTED_STATE_SET = new int[] { 16842913 };
    }
    
    private RippleUtils() {
    }
    
    public static ColorStateList convertToRippleDrawableColor(final ColorStateList list) {
        if (RippleUtils.USE_FRAMEWORK_RIPPLE) {
            return new ColorStateList(new int[][] { RippleUtils.SELECTED_STATE_SET, StateSet.NOTHING }, new int[] { getColorForState(list, RippleUtils.SELECTED_PRESSED_STATE_SET), getColorForState(list, RippleUtils.PRESSED_STATE_SET) });
        }
        final int[] selected_PRESSED_STATE_SET = RippleUtils.SELECTED_PRESSED_STATE_SET;
        final int colorForState = getColorForState(list, selected_PRESSED_STATE_SET);
        final int[] selected_HOVERED_FOCUSED_STATE_SET = RippleUtils.SELECTED_HOVERED_FOCUSED_STATE_SET;
        final int colorForState2 = getColorForState(list, selected_HOVERED_FOCUSED_STATE_SET);
        final int[] selected_FOCUSED_STATE_SET = RippleUtils.SELECTED_FOCUSED_STATE_SET;
        final int colorForState3 = getColorForState(list, selected_FOCUSED_STATE_SET);
        final int[] selected_HOVERED_STATE_SET = RippleUtils.SELECTED_HOVERED_STATE_SET;
        final int colorForState4 = getColorForState(list, selected_HOVERED_STATE_SET);
        final int[] selected_STATE_SET = RippleUtils.SELECTED_STATE_SET;
        final int[] pressed_STATE_SET = RippleUtils.PRESSED_STATE_SET;
        final int colorForState5 = getColorForState(list, pressed_STATE_SET);
        final int[] hovered_FOCUSED_STATE_SET = RippleUtils.HOVERED_FOCUSED_STATE_SET;
        final int colorForState6 = getColorForState(list, hovered_FOCUSED_STATE_SET);
        final int[] focused_STATE_SET = RippleUtils.FOCUSED_STATE_SET;
        final int colorForState7 = getColorForState(list, focused_STATE_SET);
        final int[] hovered_STATE_SET = RippleUtils.HOVERED_STATE_SET;
        return new ColorStateList(new int[][] { selected_PRESSED_STATE_SET, selected_HOVERED_FOCUSED_STATE_SET, selected_FOCUSED_STATE_SET, selected_HOVERED_STATE_SET, selected_STATE_SET, pressed_STATE_SET, hovered_FOCUSED_STATE_SET, focused_STATE_SET, hovered_STATE_SET, StateSet.NOTHING }, new int[] { colorForState, colorForState2, colorForState3, colorForState4, 0, colorForState5, colorForState6, colorForState7, getColorForState(list, hovered_STATE_SET), 0 });
    }
    
    private static int doubleAlpha(final int n) {
        return ColorUtils.setAlphaComponent(n, Math.min(Color.alpha(n) * 2, 255));
    }
    
    private static int getColorForState(final ColorStateList list, final int[] array) {
        int colorForState;
        if (list != null) {
            colorForState = list.getColorForState(array, list.getDefaultColor());
        }
        else {
            colorForState = 0;
        }
        int doubleAlpha = colorForState;
        if (RippleUtils.USE_FRAMEWORK_RIPPLE) {
            doubleAlpha = doubleAlpha(colorForState);
        }
        return doubleAlpha;
    }
}
