// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.textfield;

import androidx.core.widget.TextViewCompat;
import androidx.appcompat.widget.AppCompatTextView;
import android.content.res.ColorStateList;
import android.widget.LinearLayout$LayoutParams;
import androidx.legacy.widget.Space;
import android.view.ViewGroup$LayoutParams;
import android.widget.FrameLayout$LayoutParams;
import android.animation.Animator$AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import com.google.android.material.animation.AnimatorSetCompat;
import java.util.ArrayList;
import android.animation.AnimatorSet;
import androidx.core.view.ViewCompat;
import android.view.ViewGroup;
import android.text.TextUtils;
import com.google.android.material.animation.AnimationUtils;
import android.view.View;
import android.animation.ObjectAnimator;
import java.util.List;
import com.google.android.material.R;
import android.graphics.Typeface;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.content.Context;
import android.widget.FrameLayout;
import android.animation.Animator;

final class IndicatorViewController
{
    private static final int CAPTION_OPACITY_FADE_ANIMATION_DURATION = 167;
    private static final int CAPTION_STATE_ERROR = 1;
    private static final int CAPTION_STATE_HELPER_TEXT = 2;
    private static final int CAPTION_STATE_NONE = 0;
    private static final int CAPTION_TRANSLATE_Y_ANIMATION_DURATION = 217;
    static final int COUNTER_INDEX = 2;
    static final int ERROR_INDEX = 0;
    static final int HELPER_INDEX = 1;
    private Animator captionAnimator;
    private FrameLayout captionArea;
    private int captionDisplayed;
    private int captionToShow;
    private final float captionTranslationYPx;
    private int captionViewsAdded;
    private final Context context;
    private boolean errorEnabled;
    private CharSequence errorText;
    private int errorTextAppearance;
    private TextView errorView;
    private CharSequence helperText;
    private boolean helperTextEnabled;
    private int helperTextTextAppearance;
    private TextView helperTextView;
    private LinearLayout indicatorArea;
    private int indicatorsAdded;
    private final TextInputLayout textInputView;
    private Typeface typeface;
    
    public IndicatorViewController(final TextInputLayout textInputView) {
        this.context = textInputView.getContext();
        this.textInputView = textInputView;
        this.captionTranslationYPx = (float)this.context.getResources().getDimensionPixelSize(R.dimen.design_textinput_caption_translate_y);
    }
    
    private boolean canAdjustIndicatorPadding() {
        return this.indicatorArea != null && this.textInputView.getEditText() != null;
    }
    
    private void createCaptionAnimators(final List<Animator> list, final boolean b, final TextView textView, final int n, final int n2, final int n3) {
        if (textView != null) {
            if (!b) {
                return;
            }
            if (n == n3 || n == n2) {
                list.add((Animator)this.createCaptionOpacityAnimator(textView, n3 == n));
                if (n3 == n) {
                    list.add((Animator)this.createCaptionTranslationYAnimator(textView));
                }
            }
        }
    }
    
    private ObjectAnimator createCaptionOpacityAnimator(final TextView textView, final boolean b) {
        float n;
        if (b) {
            n = 1.0f;
        }
        else {
            n = 0.0f;
        }
        final ObjectAnimator ofFloat = ObjectAnimator.ofFloat((Object)textView, View.ALPHA, new float[] { n });
        ofFloat.setDuration(167L);
        ofFloat.setInterpolator(AnimationUtils.LINEAR_INTERPOLATOR);
        return ofFloat;
    }
    
    private ObjectAnimator createCaptionTranslationYAnimator(final TextView textView) {
        final ObjectAnimator ofFloat = ObjectAnimator.ofFloat((Object)textView, View.TRANSLATION_Y, new float[] { -this.captionTranslationYPx, 0.0f });
        ofFloat.setDuration(217L);
        ofFloat.setInterpolator(AnimationUtils.LINEAR_OUT_SLOW_IN_INTERPOLATOR);
        return ofFloat;
    }
    
    private TextView getCaptionViewFromDisplayState(final int n) {
        if (n == 1) {
            return this.errorView;
        }
        if (n != 2) {
            return null;
        }
        return this.helperTextView;
    }
    
    private boolean isCaptionStateError(final int n) {
        return n == 1 && this.errorView != null && !TextUtils.isEmpty(this.errorText);
    }
    
    private boolean isCaptionStateHelperText(final int n) {
        return n == 2 && this.helperTextView != null && !TextUtils.isEmpty(this.helperText);
    }
    
    private void setCaptionViewVisibilities(final int n, final int captionDisplayed) {
        if (n == captionDisplayed) {
            return;
        }
        if (captionDisplayed != 0) {
            final TextView captionViewFromDisplayState = this.getCaptionViewFromDisplayState(captionDisplayed);
            if (captionViewFromDisplayState != null) {
                captionViewFromDisplayState.setVisibility(0);
                captionViewFromDisplayState.setAlpha(1.0f);
            }
        }
        if (n != 0) {
            final TextView captionViewFromDisplayState2 = this.getCaptionViewFromDisplayState(n);
            if (captionViewFromDisplayState2 != null) {
                captionViewFromDisplayState2.setVisibility(4);
                if (n == 1) {
                    captionViewFromDisplayState2.setText((CharSequence)null);
                }
            }
        }
        this.captionDisplayed = captionDisplayed;
    }
    
    private void setTextViewTypeface(final TextView textView, final Typeface typeface) {
        if (textView != null) {
            textView.setTypeface(typeface);
        }
    }
    
    private void setViewGroupGoneIfEmpty(final ViewGroup viewGroup, final int n) {
        if (n == 0) {
            viewGroup.setVisibility(8);
        }
    }
    
    private boolean shouldAnimateCaptionView(final TextView textView, final CharSequence charSequence) {
        return ViewCompat.isLaidOut((View)this.textInputView) && this.textInputView.isEnabled() && (this.captionToShow != this.captionDisplayed || textView == null || !TextUtils.equals(textView.getText(), charSequence));
    }
    
    private void updateCaptionViewsVisibility(final int n, final int n2, final boolean b) {
        if (b) {
            final AnimatorSet captionAnimator = new AnimatorSet();
            this.captionAnimator = (Animator)captionAnimator;
            final ArrayList<Animator> list = new ArrayList<Animator>();
            this.createCaptionAnimators(list, this.helperTextEnabled, this.helperTextView, 2, n, n2);
            this.createCaptionAnimators(list, this.errorEnabled, this.errorView, 1, n, n2);
            AnimatorSetCompat.playTogether(captionAnimator, list);
            captionAnimator.addListener((Animator$AnimatorListener)new AnimatorListenerAdapter() {
                final /* synthetic */ TextView val$captionViewToHide = IndicatorViewController.this.getCaptionViewFromDisplayState(n);
                final /* synthetic */ TextView val$captionViewToShow = IndicatorViewController.this.getCaptionViewFromDisplayState(n2);
                
                public void onAnimationEnd(final Animator animator) {
                    IndicatorViewController.this.captionDisplayed = n2;
                    IndicatorViewController.this.captionAnimator = null;
                    final TextView val$captionViewToHide = this.val$captionViewToHide;
                    if (val$captionViewToHide != null) {
                        val$captionViewToHide.setVisibility(4);
                        if (n == 1 && IndicatorViewController.this.errorView != null) {
                            IndicatorViewController.this.errorView.setText((CharSequence)null);
                        }
                    }
                }
                
                public void onAnimationStart(final Animator animator) {
                    final TextView val$captionViewToShow = this.val$captionViewToShow;
                    if (val$captionViewToShow != null) {
                        val$captionViewToShow.setVisibility(0);
                    }
                }
            });
            captionAnimator.start();
        }
        else {
            this.setCaptionViewVisibilities(n, n2);
        }
        this.textInputView.updateEditTextBackground();
        this.textInputView.updateLabelState(b);
        this.textInputView.updateTextInputBoxState();
    }
    
    void addIndicator(final TextView textView, final int n) {
        if (this.indicatorArea == null && this.captionArea == null) {
            (this.indicatorArea = new LinearLayout(this.context)).setOrientation(0);
            this.textInputView.addView((View)this.indicatorArea, -1, -2);
            this.captionArea = new FrameLayout(this.context);
            this.indicatorArea.addView((View)this.captionArea, -1, (ViewGroup$LayoutParams)new FrameLayout$LayoutParams(-2, -2));
            this.indicatorArea.addView((View)new Space(this.context), (ViewGroup$LayoutParams)new LinearLayout$LayoutParams(0, 0, 1.0f));
            if (this.textInputView.getEditText() != null) {
                this.adjustIndicatorPadding();
            }
        }
        if (this.isCaptionView(n)) {
            this.captionArea.setVisibility(0);
            this.captionArea.addView((View)textView);
            ++this.captionViewsAdded;
        }
        else {
            this.indicatorArea.addView((View)textView, n);
        }
        this.indicatorArea.setVisibility(0);
        ++this.indicatorsAdded;
    }
    
    void adjustIndicatorPadding() {
        if (this.canAdjustIndicatorPadding()) {
            ViewCompat.setPaddingRelative((View)this.indicatorArea, ViewCompat.getPaddingStart((View)this.textInputView.getEditText()), 0, ViewCompat.getPaddingEnd((View)this.textInputView.getEditText()), 0);
        }
    }
    
    void cancelCaptionAnimator() {
        final Animator captionAnimator = this.captionAnimator;
        if (captionAnimator != null) {
            captionAnimator.cancel();
        }
    }
    
    boolean errorIsDisplayed() {
        return this.isCaptionStateError(this.captionDisplayed);
    }
    
    boolean errorShouldBeShown() {
        return this.isCaptionStateError(this.captionToShow);
    }
    
    CharSequence getErrorText() {
        return this.errorText;
    }
    
    int getErrorViewCurrentTextColor() {
        final TextView errorView = this.errorView;
        if (errorView != null) {
            return errorView.getCurrentTextColor();
        }
        return -1;
    }
    
    ColorStateList getErrorViewTextColors() {
        final TextView errorView = this.errorView;
        if (errorView != null) {
            return errorView.getTextColors();
        }
        return null;
    }
    
    CharSequence getHelperText() {
        return this.helperText;
    }
    
    ColorStateList getHelperTextViewColors() {
        final TextView helperTextView = this.helperTextView;
        if (helperTextView != null) {
            return helperTextView.getTextColors();
        }
        return null;
    }
    
    int getHelperTextViewCurrentTextColor() {
        final TextView helperTextView = this.helperTextView;
        if (helperTextView != null) {
            return helperTextView.getCurrentTextColor();
        }
        return -1;
    }
    
    boolean helperTextIsDisplayed() {
        return this.isCaptionStateHelperText(this.captionDisplayed);
    }
    
    boolean helperTextShouldBeShown() {
        return this.isCaptionStateHelperText(this.captionToShow);
    }
    
    void hideError() {
        this.errorText = null;
        this.cancelCaptionAnimator();
        if (this.captionDisplayed == 1) {
            if (this.helperTextEnabled && !TextUtils.isEmpty(this.helperText)) {
                this.captionToShow = 2;
            }
            else {
                this.captionToShow = 0;
            }
        }
        this.updateCaptionViewsVisibility(this.captionDisplayed, this.captionToShow, this.shouldAnimateCaptionView(this.errorView, null));
    }
    
    void hideHelperText() {
        this.cancelCaptionAnimator();
        if (this.captionDisplayed == 2) {
            this.captionToShow = 0;
        }
        this.updateCaptionViewsVisibility(this.captionDisplayed, this.captionToShow, this.shouldAnimateCaptionView(this.helperTextView, null));
    }
    
    boolean isCaptionView(final int n) {
        boolean b = true;
        if (n != 0) {
            if (n == 1) {
                return true;
            }
            b = false;
        }
        return b;
    }
    
    boolean isErrorEnabled() {
        return this.errorEnabled;
    }
    
    boolean isHelperTextEnabled() {
        return this.helperTextEnabled;
    }
    
    void removeIndicator(final TextView textView, final int n) {
        if (this.indicatorArea == null) {
            return;
        }
        Label_0063: {
            if (this.isCaptionView(n)) {
                final FrameLayout captionArea = this.captionArea;
                if (captionArea != null) {
                    this.setViewGroupGoneIfEmpty((ViewGroup)captionArea, --this.captionViewsAdded);
                    this.captionArea.removeView((View)textView);
                    break Label_0063;
                }
            }
            this.indicatorArea.removeView((View)textView);
        }
        --this.indicatorsAdded;
        this.setViewGroupGoneIfEmpty((ViewGroup)this.indicatorArea, this.indicatorsAdded);
    }
    
    void setErrorEnabled(final boolean errorEnabled) {
        if (this.errorEnabled == errorEnabled) {
            return;
        }
        this.cancelCaptionAnimator();
        if (errorEnabled) {
            (this.errorView = new AppCompatTextView(this.context)).setId(R.id.textinput_error);
            final Typeface typeface = this.typeface;
            if (typeface != null) {
                this.errorView.setTypeface(typeface);
            }
            this.setErrorTextAppearance(this.errorTextAppearance);
            this.errorView.setVisibility(4);
            ViewCompat.setAccessibilityLiveRegion((View)this.errorView, 1);
            this.addIndicator(this.errorView, 0);
        }
        else {
            this.hideError();
            this.removeIndicator(this.errorView, 0);
            this.errorView = null;
            this.textInputView.updateEditTextBackground();
            this.textInputView.updateTextInputBoxState();
        }
        this.errorEnabled = errorEnabled;
    }
    
    void setErrorTextAppearance(final int errorTextAppearance) {
        this.errorTextAppearance = errorTextAppearance;
        final TextView errorView = this.errorView;
        if (errorView != null) {
            this.textInputView.setTextAppearanceCompatWithErrorFallback(errorView, errorTextAppearance);
        }
    }
    
    void setErrorViewTextColor(final ColorStateList textColor) {
        final TextView errorView = this.errorView;
        if (errorView != null) {
            errorView.setTextColor(textColor);
        }
    }
    
    void setHelperTextAppearance(final int helperTextTextAppearance) {
        this.helperTextTextAppearance = helperTextTextAppearance;
        final TextView helperTextView = this.helperTextView;
        if (helperTextView != null) {
            TextViewCompat.setTextAppearance(helperTextView, helperTextTextAppearance);
        }
    }
    
    void setHelperTextEnabled(final boolean helperTextEnabled) {
        if (this.helperTextEnabled == helperTextEnabled) {
            return;
        }
        this.cancelCaptionAnimator();
        if (helperTextEnabled) {
            (this.helperTextView = new AppCompatTextView(this.context)).setId(R.id.textinput_helper_text);
            final Typeface typeface = this.typeface;
            if (typeface != null) {
                this.helperTextView.setTypeface(typeface);
            }
            this.helperTextView.setVisibility(4);
            ViewCompat.setAccessibilityLiveRegion((View)this.helperTextView, 1);
            this.setHelperTextAppearance(this.helperTextTextAppearance);
            this.addIndicator(this.helperTextView, 1);
        }
        else {
            this.hideHelperText();
            this.removeIndicator(this.helperTextView, 1);
            this.helperTextView = null;
            this.textInputView.updateEditTextBackground();
            this.textInputView.updateTextInputBoxState();
        }
        this.helperTextEnabled = helperTextEnabled;
    }
    
    void setHelperTextViewTextColor(final ColorStateList textColor) {
        final TextView helperTextView = this.helperTextView;
        if (helperTextView != null) {
            helperTextView.setTextColor(textColor);
        }
    }
    
    void setTypefaces(final Typeface typeface) {
        if (typeface != this.typeface) {
            this.typeface = typeface;
            this.setTextViewTypeface(this.errorView, typeface);
            this.setTextViewTypeface(this.helperTextView, typeface);
        }
    }
    
    void showError(final CharSequence charSequence) {
        this.cancelCaptionAnimator();
        this.errorText = charSequence;
        this.errorView.setText(charSequence);
        if (this.captionDisplayed != 1) {
            this.captionToShow = 1;
        }
        this.updateCaptionViewsVisibility(this.captionDisplayed, this.captionToShow, this.shouldAnimateCaptionView(this.errorView, charSequence));
    }
    
    void showHelper(final CharSequence charSequence) {
        this.cancelCaptionAnimator();
        this.helperText = charSequence;
        this.helperTextView.setText(charSequence);
        if (this.captionDisplayed != 2) {
            this.captionToShow = 2;
        }
        this.updateCaptionViewsVisibility(this.captionDisplayed, this.captionToShow, this.shouldAnimateCaptionView(this.helperTextView, charSequence));
    }
}
