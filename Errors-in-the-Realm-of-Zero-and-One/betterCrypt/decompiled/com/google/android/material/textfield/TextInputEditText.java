// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.textfield;

import android.view.inputmethod.InputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.ViewParent;
import android.view.View;
import com.google.android.material.R;
import android.util.AttributeSet;
import android.content.Context;
import androidx.appcompat.widget.AppCompatEditText;

public class TextInputEditText extends AppCompatEditText
{
    public TextInputEditText(final Context context) {
        this(context, null);
    }
    
    public TextInputEditText(final Context context, final AttributeSet set) {
        this(context, set, R.attr.editTextStyle);
    }
    
    public TextInputEditText(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    private CharSequence getHintFromLayout() {
        final TextInputLayout textInputLayout = this.getTextInputLayout();
        if (textInputLayout != null) {
            return textInputLayout.getHint();
        }
        return null;
    }
    
    private TextInputLayout getTextInputLayout() {
        for (ViewParent viewParent = this.getParent(); viewParent instanceof View; viewParent = viewParent.getParent()) {
            if (viewParent instanceof TextInputLayout) {
                return (TextInputLayout)viewParent;
            }
        }
        return null;
    }
    
    public CharSequence getHint() {
        final TextInputLayout textInputLayout = this.getTextInputLayout();
        if (textInputLayout != null && textInputLayout.isProvidingHint()) {
            return textInputLayout.getHint();
        }
        return super.getHint();
    }
    
    @Override
    public InputConnection onCreateInputConnection(final EditorInfo editorInfo) {
        final InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
        if (onCreateInputConnection != null && editorInfo.hintText == null) {
            editorInfo.hintText = this.getHintFromLayout();
        }
        return onCreateInputConnection;
    }
}
