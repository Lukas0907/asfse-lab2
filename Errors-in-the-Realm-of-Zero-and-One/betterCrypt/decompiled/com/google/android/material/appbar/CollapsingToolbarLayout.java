// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.appbar;

import androidx.core.math.MathUtils;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.content.ContextCompat;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable$Callback;
import android.content.res.ColorStateList;
import androidx.core.util.ObjectsCompat;
import android.view.View$MeasureSpec;
import android.text.TextUtils;
import com.google.android.material.internal.DescendantOffsetUtils;
import android.graphics.Typeface;
import android.widget.FrameLayout$LayoutParams;
import android.graphics.Canvas;
import android.view.ViewGroup;
import android.view.ViewGroup$LayoutParams;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.ViewParent;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator$AnimatorUpdateListener;
import android.content.res.TypedArray;
import androidx.core.view.ViewCompat;
import androidx.core.view.OnApplyWindowInsetsListener;
import com.google.android.material.internal.ThemeEnforcement;
import com.google.android.material.R;
import com.google.android.material.animation.AnimationUtils;
import android.util.AttributeSet;
import android.content.Context;
import androidx.appcompat.widget.Toolbar;
import android.graphics.Rect;
import android.animation.ValueAnimator;
import androidx.core.view.WindowInsetsCompat;
import android.view.View;
import android.graphics.drawable.Drawable;
import com.google.android.material.internal.CollapsingTextHelper;
import android.widget.FrameLayout;

public class CollapsingToolbarLayout extends FrameLayout
{
    private static final int DEFAULT_SCRIM_ANIMATION_DURATION = 600;
    final CollapsingTextHelper collapsingTextHelper;
    private boolean collapsingTitleEnabled;
    private Drawable contentScrim;
    int currentOffset;
    private boolean drawCollapsingTitle;
    private View dummyView;
    private int expandedMarginBottom;
    private int expandedMarginEnd;
    private int expandedMarginStart;
    private int expandedMarginTop;
    WindowInsetsCompat lastInsets;
    private AppBarLayout.OnOffsetChangedListener onOffsetChangedListener;
    private boolean refreshToolbar;
    private int scrimAlpha;
    private long scrimAnimationDuration;
    private ValueAnimator scrimAnimator;
    private int scrimVisibleHeightTrigger;
    private boolean scrimsAreShown;
    Drawable statusBarScrim;
    private final Rect tmpRect;
    private Toolbar toolbar;
    private View toolbarDirectChild;
    private int toolbarId;
    
    public CollapsingToolbarLayout(final Context context) {
        this(context, null);
    }
    
    public CollapsingToolbarLayout(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public CollapsingToolbarLayout(final Context context, final AttributeSet set, int dimensionPixelSize) {
        super(context, set, dimensionPixelSize);
        this.refreshToolbar = true;
        this.tmpRect = new Rect();
        this.scrimVisibleHeightTrigger = -1;
        (this.collapsingTextHelper = new CollapsingTextHelper((View)this)).setTextSizeInterpolator(AnimationUtils.DECELERATE_INTERPOLATOR);
        final TypedArray obtainStyledAttributes = ThemeEnforcement.obtainStyledAttributes(context, set, R.styleable.CollapsingToolbarLayout, dimensionPixelSize, R.style.Widget_Design_CollapsingToolbar);
        this.collapsingTextHelper.setExpandedTextGravity(obtainStyledAttributes.getInt(R.styleable.CollapsingToolbarLayout_expandedTitleGravity, 8388691));
        this.collapsingTextHelper.setCollapsedTextGravity(obtainStyledAttributes.getInt(R.styleable.CollapsingToolbarLayout_collapsedTitleGravity, 8388627));
        dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(R.styleable.CollapsingToolbarLayout_expandedTitleMargin, 0);
        this.expandedMarginBottom = dimensionPixelSize;
        this.expandedMarginEnd = dimensionPixelSize;
        this.expandedMarginTop = dimensionPixelSize;
        this.expandedMarginStart = dimensionPixelSize;
        if (obtainStyledAttributes.hasValue(R.styleable.CollapsingToolbarLayout_expandedTitleMarginStart)) {
            this.expandedMarginStart = obtainStyledAttributes.getDimensionPixelSize(R.styleable.CollapsingToolbarLayout_expandedTitleMarginStart, 0);
        }
        if (obtainStyledAttributes.hasValue(R.styleable.CollapsingToolbarLayout_expandedTitleMarginEnd)) {
            this.expandedMarginEnd = obtainStyledAttributes.getDimensionPixelSize(R.styleable.CollapsingToolbarLayout_expandedTitleMarginEnd, 0);
        }
        if (obtainStyledAttributes.hasValue(R.styleable.CollapsingToolbarLayout_expandedTitleMarginTop)) {
            this.expandedMarginTop = obtainStyledAttributes.getDimensionPixelSize(R.styleable.CollapsingToolbarLayout_expandedTitleMarginTop, 0);
        }
        if (obtainStyledAttributes.hasValue(R.styleable.CollapsingToolbarLayout_expandedTitleMarginBottom)) {
            this.expandedMarginBottom = obtainStyledAttributes.getDimensionPixelSize(R.styleable.CollapsingToolbarLayout_expandedTitleMarginBottom, 0);
        }
        this.collapsingTitleEnabled = obtainStyledAttributes.getBoolean(R.styleable.CollapsingToolbarLayout_titleEnabled, true);
        this.setTitle(obtainStyledAttributes.getText(R.styleable.CollapsingToolbarLayout_title));
        this.collapsingTextHelper.setExpandedTextAppearance(R.style.TextAppearance_Design_CollapsingToolbar_Expanded);
        this.collapsingTextHelper.setCollapsedTextAppearance(androidx.appcompat.R.style.TextAppearance_AppCompat_Widget_ActionBar_Title);
        if (obtainStyledAttributes.hasValue(R.styleable.CollapsingToolbarLayout_expandedTitleTextAppearance)) {
            this.collapsingTextHelper.setExpandedTextAppearance(obtainStyledAttributes.getResourceId(R.styleable.CollapsingToolbarLayout_expandedTitleTextAppearance, 0));
        }
        if (obtainStyledAttributes.hasValue(R.styleable.CollapsingToolbarLayout_collapsedTitleTextAppearance)) {
            this.collapsingTextHelper.setCollapsedTextAppearance(obtainStyledAttributes.getResourceId(R.styleable.CollapsingToolbarLayout_collapsedTitleTextAppearance, 0));
        }
        this.scrimVisibleHeightTrigger = obtainStyledAttributes.getDimensionPixelSize(R.styleable.CollapsingToolbarLayout_scrimVisibleHeightTrigger, -1);
        this.scrimAnimationDuration = obtainStyledAttributes.getInt(R.styleable.CollapsingToolbarLayout_scrimAnimationDuration, 600);
        this.setContentScrim(obtainStyledAttributes.getDrawable(R.styleable.CollapsingToolbarLayout_contentScrim));
        this.setStatusBarScrim(obtainStyledAttributes.getDrawable(R.styleable.CollapsingToolbarLayout_statusBarScrim));
        this.toolbarId = obtainStyledAttributes.getResourceId(R.styleable.CollapsingToolbarLayout_toolbarId, -1);
        obtainStyledAttributes.recycle();
        this.setWillNotDraw(false);
        ViewCompat.setOnApplyWindowInsetsListener((View)this, new OnApplyWindowInsetsListener() {
            @Override
            public WindowInsetsCompat onApplyWindowInsets(final View view, final WindowInsetsCompat windowInsetsCompat) {
                return CollapsingToolbarLayout.this.onWindowInsetChanged(windowInsetsCompat);
            }
        });
    }
    
    private void animateScrim(final int n) {
        this.ensureToolbar();
        final ValueAnimator scrimAnimator = this.scrimAnimator;
        if (scrimAnimator == null) {
            (this.scrimAnimator = new ValueAnimator()).setDuration(this.scrimAnimationDuration);
            final ValueAnimator scrimAnimator2 = this.scrimAnimator;
            TimeInterpolator interpolator;
            if (n > this.scrimAlpha) {
                interpolator = AnimationUtils.FAST_OUT_LINEAR_IN_INTERPOLATOR;
            }
            else {
                interpolator = AnimationUtils.LINEAR_OUT_SLOW_IN_INTERPOLATOR;
            }
            scrimAnimator2.setInterpolator(interpolator);
            this.scrimAnimator.addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener() {
                public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                    CollapsingToolbarLayout.this.setScrimAlpha((int)valueAnimator.getAnimatedValue());
                }
            });
        }
        else if (scrimAnimator.isRunning()) {
            this.scrimAnimator.cancel();
        }
        this.scrimAnimator.setIntValues(new int[] { this.scrimAlpha, n });
        this.scrimAnimator.start();
    }
    
    private void ensureToolbar() {
        if (!this.refreshToolbar) {
            return;
        }
        final Toolbar toolbar = null;
        this.toolbar = null;
        this.toolbarDirectChild = null;
        final int toolbarId = this.toolbarId;
        if (toolbarId != -1) {
            this.toolbar = (Toolbar)this.findViewById(toolbarId);
            final Toolbar toolbar2 = this.toolbar;
            if (toolbar2 != null) {
                this.toolbarDirectChild = this.findDirectChild((View)toolbar2);
            }
        }
        if (this.toolbar == null) {
            final int childCount = this.getChildCount();
            int n = 0;
            Toolbar toolbar3;
            while (true) {
                toolbar3 = toolbar;
                if (n >= childCount) {
                    break;
                }
                final View child = this.getChildAt(n);
                if (child instanceof Toolbar) {
                    toolbar3 = (Toolbar)child;
                    break;
                }
                ++n;
            }
            this.toolbar = toolbar3;
        }
        this.updateDummyView();
        this.refreshToolbar = false;
    }
    
    private View findDirectChild(final View view) {
        final ViewParent parent = view.getParent();
        View view2 = view;
        for (Object parent2 = parent; parent2 != this && parent2 != null; parent2 = ((ViewParent)parent2).getParent()) {
            if (parent2 instanceof View) {
                view2 = (View)parent2;
            }
        }
        return view2;
    }
    
    private static int getHeightWithMargins(final View view) {
        final ViewGroup$LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams instanceof ViewGroup$MarginLayoutParams) {
            final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)layoutParams;
            return view.getHeight() + viewGroup$MarginLayoutParams.topMargin + viewGroup$MarginLayoutParams.bottomMargin;
        }
        return view.getHeight();
    }
    
    static ViewOffsetHelper getViewOffsetHelper(final View view) {
        ViewOffsetHelper viewOffsetHelper;
        if ((viewOffsetHelper = (ViewOffsetHelper)view.getTag(R.id.view_offset_helper)) == null) {
            viewOffsetHelper = new ViewOffsetHelper(view);
            view.setTag(R.id.view_offset_helper, (Object)viewOffsetHelper);
        }
        return viewOffsetHelper;
    }
    
    private boolean isToolbarChild(final View view) {
        final View toolbarDirectChild = this.toolbarDirectChild;
        if (toolbarDirectChild != null && toolbarDirectChild != this) {
            if (view == toolbarDirectChild) {
                return true;
            }
        }
        else if (view == this.toolbar) {
            return true;
        }
        return false;
    }
    
    private void updateContentDescriptionFromTitle() {
        this.setContentDescription(this.getTitle());
    }
    
    private void updateDummyView() {
        if (!this.collapsingTitleEnabled) {
            final View dummyView = this.dummyView;
            if (dummyView != null) {
                final ViewParent parent = dummyView.getParent();
                if (parent instanceof ViewGroup) {
                    ((ViewGroup)parent).removeView(this.dummyView);
                }
            }
        }
        if (this.collapsingTitleEnabled && this.toolbar != null) {
            if (this.dummyView == null) {
                this.dummyView = new View(this.getContext());
            }
            if (this.dummyView.getParent() == null) {
                this.toolbar.addView(this.dummyView, -1, -1);
            }
        }
    }
    
    protected boolean checkLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return viewGroup$LayoutParams instanceof LayoutParams;
    }
    
    public void draw(final Canvas canvas) {
        super.draw(canvas);
        this.ensureToolbar();
        if (this.toolbar == null) {
            final Drawable contentScrim = this.contentScrim;
            if (contentScrim != null && this.scrimAlpha > 0) {
                contentScrim.mutate().setAlpha(this.scrimAlpha);
                this.contentScrim.draw(canvas);
            }
        }
        if (this.collapsingTitleEnabled && this.drawCollapsingTitle) {
            this.collapsingTextHelper.draw(canvas);
        }
        if (this.statusBarScrim != null && this.scrimAlpha > 0) {
            final WindowInsetsCompat lastInsets = this.lastInsets;
            int systemWindowInsetTop;
            if (lastInsets != null) {
                systemWindowInsetTop = lastInsets.getSystemWindowInsetTop();
            }
            else {
                systemWindowInsetTop = 0;
            }
            if (systemWindowInsetTop > 0) {
                this.statusBarScrim.setBounds(0, -this.currentOffset, this.getWidth(), systemWindowInsetTop - this.currentOffset);
                this.statusBarScrim.mutate().setAlpha(this.scrimAlpha);
                this.statusBarScrim.draw(canvas);
            }
        }
    }
    
    protected boolean drawChild(final Canvas canvas, final View view, final long n) {
        final Drawable contentScrim = this.contentScrim;
        boolean b = true;
        boolean b2;
        if (contentScrim != null && this.scrimAlpha > 0 && this.isToolbarChild(view)) {
            this.contentScrim.mutate().setAlpha(this.scrimAlpha);
            this.contentScrim.draw(canvas);
            b2 = true;
        }
        else {
            b2 = false;
        }
        if (!super.drawChild(canvas, view, n)) {
            if (b2) {
                return true;
            }
            b = false;
        }
        return b;
    }
    
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        final int[] drawableState = this.getDrawableState();
        final Drawable statusBarScrim = this.statusBarScrim;
        int n2;
        final int n = n2 = 0;
        if (statusBarScrim != null) {
            n2 = n;
            if (statusBarScrim.isStateful()) {
                n2 = ((false | statusBarScrim.setState(drawableState)) ? 1 : 0);
            }
        }
        final Drawable contentScrim = this.contentScrim;
        int n3 = n2;
        if (contentScrim != null) {
            n3 = n2;
            if (contentScrim.isStateful()) {
                n3 = (n2 | (contentScrim.setState(drawableState) ? 1 : 0));
            }
        }
        final CollapsingTextHelper collapsingTextHelper = this.collapsingTextHelper;
        int n4 = n3;
        if (collapsingTextHelper != null) {
            n4 = (n3 | (collapsingTextHelper.setState(drawableState) ? 1 : 0));
        }
        if (n4 != 0) {
            this.invalidate();
        }
    }
    
    protected LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-1, -1);
    }
    
    public FrameLayout$LayoutParams generateLayoutParams(final AttributeSet set) {
        return new LayoutParams(this.getContext(), set);
    }
    
    protected FrameLayout$LayoutParams generateLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return new LayoutParams(viewGroup$LayoutParams);
    }
    
    public int getCollapsedTitleGravity() {
        return this.collapsingTextHelper.getCollapsedTextGravity();
    }
    
    public Typeface getCollapsedTitleTypeface() {
        return this.collapsingTextHelper.getCollapsedTypeface();
    }
    
    public Drawable getContentScrim() {
        return this.contentScrim;
    }
    
    public int getExpandedTitleGravity() {
        return this.collapsingTextHelper.getExpandedTextGravity();
    }
    
    public int getExpandedTitleMarginBottom() {
        return this.expandedMarginBottom;
    }
    
    public int getExpandedTitleMarginEnd() {
        return this.expandedMarginEnd;
    }
    
    public int getExpandedTitleMarginStart() {
        return this.expandedMarginStart;
    }
    
    public int getExpandedTitleMarginTop() {
        return this.expandedMarginTop;
    }
    
    public Typeface getExpandedTitleTypeface() {
        return this.collapsingTextHelper.getExpandedTypeface();
    }
    
    final int getMaxOffsetForPinChild(final View view) {
        return this.getHeight() - getViewOffsetHelper(view).getLayoutTop() - view.getHeight() - ((LayoutParams)view.getLayoutParams()).bottomMargin;
    }
    
    int getScrimAlpha() {
        return this.scrimAlpha;
    }
    
    public long getScrimAnimationDuration() {
        return this.scrimAnimationDuration;
    }
    
    public int getScrimVisibleHeightTrigger() {
        final int scrimVisibleHeightTrigger = this.scrimVisibleHeightTrigger;
        if (scrimVisibleHeightTrigger >= 0) {
            return scrimVisibleHeightTrigger;
        }
        final WindowInsetsCompat lastInsets = this.lastInsets;
        int systemWindowInsetTop;
        if (lastInsets != null) {
            systemWindowInsetTop = lastInsets.getSystemWindowInsetTop();
        }
        else {
            systemWindowInsetTop = 0;
        }
        final int minimumHeight = ViewCompat.getMinimumHeight((View)this);
        if (minimumHeight > 0) {
            return Math.min(minimumHeight * 2 + systemWindowInsetTop, this.getHeight());
        }
        return this.getHeight() / 3;
    }
    
    public Drawable getStatusBarScrim() {
        return this.statusBarScrim;
    }
    
    public CharSequence getTitle() {
        if (this.collapsingTitleEnabled) {
            return this.collapsingTextHelper.getText();
        }
        return null;
    }
    
    public boolean isTitleEnabled() {
        return this.collapsingTitleEnabled;
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        final ViewParent parent = this.getParent();
        if (parent instanceof AppBarLayout) {
            ViewCompat.setFitsSystemWindows((View)this, ViewCompat.getFitsSystemWindows((View)parent));
            if (this.onOffsetChangedListener == null) {
                this.onOffsetChangedListener = new OffsetUpdateListener();
            }
            ((AppBarLayout)parent).addOnOffsetChangedListener(this.onOffsetChangedListener);
            ViewCompat.requestApplyInsets((View)this);
        }
    }
    
    protected void onDetachedFromWindow() {
        final ViewParent parent = this.getParent();
        final AppBarLayout.OnOffsetChangedListener onOffsetChangedListener = this.onOffsetChangedListener;
        if (onOffsetChangedListener != null && parent instanceof AppBarLayout) {
            ((AppBarLayout)parent).removeOnOffsetChangedListener(onOffsetChangedListener);
        }
        super.onDetachedFromWindow();
    }
    
    protected void onLayout(final boolean b, int i, int childCount, final int n, final int n2) {
        super.onLayout(b, i, childCount, n, n2);
        final WindowInsetsCompat lastInsets = this.lastInsets;
        final int n3 = 0;
        if (lastInsets != null) {
            final int systemWindowInsetTop = lastInsets.getSystemWindowInsetTop();
            for (int childCount2 = this.getChildCount(), j = 0; j < childCount2; ++j) {
                final View child = this.getChildAt(j);
                if (!ViewCompat.getFitsSystemWindows(child) && child.getTop() < systemWindowInsetTop) {
                    ViewCompat.offsetTopAndBottom(child, systemWindowInsetTop);
                }
            }
        }
        if (this.collapsingTitleEnabled) {
            final View dummyView = this.dummyView;
            if (dummyView != null) {
                final boolean attachedToWindow = ViewCompat.isAttachedToWindow(dummyView);
                boolean b2 = true;
                this.drawCollapsingTitle = (attachedToWindow && this.dummyView.getVisibility() == 0);
                if (this.drawCollapsingTitle) {
                    if (ViewCompat.getLayoutDirection((View)this) != 1) {
                        b2 = false;
                    }
                    Object o = this.toolbarDirectChild;
                    if (o == null) {
                        o = this.toolbar;
                    }
                    final int maxOffsetForPinChild = this.getMaxOffsetForPinChild((View)o);
                    DescendantOffsetUtils.getDescendantRect((ViewGroup)this, this.dummyView, this.tmpRect);
                    final CollapsingTextHelper collapsingTextHelper = this.collapsingTextHelper;
                    final int left = this.tmpRect.left;
                    int n4;
                    if (b2) {
                        n4 = this.toolbar.getTitleMarginEnd();
                    }
                    else {
                        n4 = this.toolbar.getTitleMarginStart();
                    }
                    final int top = this.tmpRect.top;
                    final int titleMarginTop = this.toolbar.getTitleMarginTop();
                    final int right = this.tmpRect.right;
                    int n5;
                    if (b2) {
                        n5 = this.toolbar.getTitleMarginStart();
                    }
                    else {
                        n5 = this.toolbar.getTitleMarginEnd();
                    }
                    collapsingTextHelper.setCollapsedBounds(left + n4, top + maxOffsetForPinChild + titleMarginTop, right + n5, this.tmpRect.bottom + maxOffsetForPinChild - this.toolbar.getTitleMarginBottom());
                    final CollapsingTextHelper collapsingTextHelper2 = this.collapsingTextHelper;
                    int n6;
                    if (b2) {
                        n6 = this.expandedMarginEnd;
                    }
                    else {
                        n6 = this.expandedMarginStart;
                    }
                    final int top2 = this.tmpRect.top;
                    final int expandedMarginTop = this.expandedMarginTop;
                    int n7;
                    if (b2) {
                        n7 = this.expandedMarginStart;
                    }
                    else {
                        n7 = this.expandedMarginEnd;
                    }
                    collapsingTextHelper2.setExpandedBounds(n6, top2 + expandedMarginTop, n - i - n7, n2 - childCount - this.expandedMarginBottom);
                    this.collapsingTextHelper.recalculate();
                }
            }
        }
        for (childCount = this.getChildCount(), i = n3; i < childCount; ++i) {
            getViewOffsetHelper(this.getChildAt(i)).onViewLayout();
        }
        if (this.toolbar != null) {
            if (this.collapsingTitleEnabled && TextUtils.isEmpty(this.collapsingTextHelper.getText())) {
                this.setTitle(this.toolbar.getTitle());
            }
            final View toolbarDirectChild = this.toolbarDirectChild;
            if (toolbarDirectChild != null && toolbarDirectChild != this) {
                this.setMinimumHeight(getHeightWithMargins(toolbarDirectChild));
            }
            else {
                this.setMinimumHeight(getHeightWithMargins((View)this.toolbar));
            }
        }
        this.updateScrimVisibility();
    }
    
    protected void onMeasure(final int n, int systemWindowInsetTop) {
        this.ensureToolbar();
        super.onMeasure(n, systemWindowInsetTop);
        final int mode = View$MeasureSpec.getMode(systemWindowInsetTop);
        final WindowInsetsCompat lastInsets = this.lastInsets;
        if (lastInsets != null) {
            systemWindowInsetTop = lastInsets.getSystemWindowInsetTop();
        }
        else {
            systemWindowInsetTop = 0;
        }
        if (mode == 0 && systemWindowInsetTop > 0) {
            super.onMeasure(n, View$MeasureSpec.makeMeasureSpec(this.getMeasuredHeight() + systemWindowInsetTop, 1073741824));
        }
    }
    
    protected void onSizeChanged(final int n, final int n2, final int n3, final int n4) {
        super.onSizeChanged(n, n2, n3, n4);
        final Drawable contentScrim = this.contentScrim;
        if (contentScrim != null) {
            contentScrim.setBounds(0, 0, n, n2);
        }
    }
    
    WindowInsetsCompat onWindowInsetChanged(final WindowInsetsCompat windowInsetsCompat) {
        WindowInsetsCompat lastInsets;
        if (ViewCompat.getFitsSystemWindows((View)this)) {
            lastInsets = windowInsetsCompat;
        }
        else {
            lastInsets = null;
        }
        if (!ObjectsCompat.equals(this.lastInsets, lastInsets)) {
            this.lastInsets = lastInsets;
            this.requestLayout();
        }
        return windowInsetsCompat.consumeSystemWindowInsets();
    }
    
    public void setCollapsedTitleGravity(final int collapsedTextGravity) {
        this.collapsingTextHelper.setCollapsedTextGravity(collapsedTextGravity);
    }
    
    public void setCollapsedTitleTextAppearance(final int collapsedTextAppearance) {
        this.collapsingTextHelper.setCollapsedTextAppearance(collapsedTextAppearance);
    }
    
    public void setCollapsedTitleTextColor(final int n) {
        this.setCollapsedTitleTextColor(ColorStateList.valueOf(n));
    }
    
    public void setCollapsedTitleTextColor(final ColorStateList collapsedTextColor) {
        this.collapsingTextHelper.setCollapsedTextColor(collapsedTextColor);
    }
    
    public void setCollapsedTitleTypeface(final Typeface collapsedTypeface) {
        this.collapsingTextHelper.setCollapsedTypeface(collapsedTypeface);
    }
    
    public void setContentScrim(Drawable contentScrim) {
        final Drawable contentScrim2 = this.contentScrim;
        if (contentScrim2 != contentScrim) {
            Drawable mutate = null;
            if (contentScrim2 != null) {
                contentScrim2.setCallback((Drawable$Callback)null);
            }
            if (contentScrim != null) {
                mutate = contentScrim.mutate();
            }
            this.contentScrim = mutate;
            contentScrim = this.contentScrim;
            if (contentScrim != null) {
                contentScrim.setBounds(0, 0, this.getWidth(), this.getHeight());
                this.contentScrim.setCallback((Drawable$Callback)this);
                this.contentScrim.setAlpha(this.scrimAlpha);
            }
            ViewCompat.postInvalidateOnAnimation((View)this);
        }
    }
    
    public void setContentScrimColor(final int n) {
        this.setContentScrim((Drawable)new ColorDrawable(n));
    }
    
    public void setContentScrimResource(final int n) {
        this.setContentScrim(ContextCompat.getDrawable(this.getContext(), n));
    }
    
    public void setExpandedTitleColor(final int n) {
        this.setExpandedTitleTextColor(ColorStateList.valueOf(n));
    }
    
    public void setExpandedTitleGravity(final int expandedTextGravity) {
        this.collapsingTextHelper.setExpandedTextGravity(expandedTextGravity);
    }
    
    public void setExpandedTitleMargin(final int expandedMarginStart, final int expandedMarginTop, final int expandedMarginEnd, final int expandedMarginBottom) {
        this.expandedMarginStart = expandedMarginStart;
        this.expandedMarginTop = expandedMarginTop;
        this.expandedMarginEnd = expandedMarginEnd;
        this.expandedMarginBottom = expandedMarginBottom;
        this.requestLayout();
    }
    
    public void setExpandedTitleMarginBottom(final int expandedMarginBottom) {
        this.expandedMarginBottom = expandedMarginBottom;
        this.requestLayout();
    }
    
    public void setExpandedTitleMarginEnd(final int expandedMarginEnd) {
        this.expandedMarginEnd = expandedMarginEnd;
        this.requestLayout();
    }
    
    public void setExpandedTitleMarginStart(final int expandedMarginStart) {
        this.expandedMarginStart = expandedMarginStart;
        this.requestLayout();
    }
    
    public void setExpandedTitleMarginTop(final int expandedMarginTop) {
        this.expandedMarginTop = expandedMarginTop;
        this.requestLayout();
    }
    
    public void setExpandedTitleTextAppearance(final int expandedTextAppearance) {
        this.collapsingTextHelper.setExpandedTextAppearance(expandedTextAppearance);
    }
    
    public void setExpandedTitleTextColor(final ColorStateList expandedTextColor) {
        this.collapsingTextHelper.setExpandedTextColor(expandedTextColor);
    }
    
    public void setExpandedTitleTypeface(final Typeface expandedTypeface) {
        this.collapsingTextHelper.setExpandedTypeface(expandedTypeface);
    }
    
    void setScrimAlpha(final int scrimAlpha) {
        if (scrimAlpha != this.scrimAlpha) {
            if (this.contentScrim != null) {
                final Toolbar toolbar = this.toolbar;
                if (toolbar != null) {
                    ViewCompat.postInvalidateOnAnimation((View)toolbar);
                }
            }
            this.scrimAlpha = scrimAlpha;
            ViewCompat.postInvalidateOnAnimation((View)this);
        }
    }
    
    public void setScrimAnimationDuration(final long scrimAnimationDuration) {
        this.scrimAnimationDuration = scrimAnimationDuration;
    }
    
    public void setScrimVisibleHeightTrigger(final int scrimVisibleHeightTrigger) {
        if (this.scrimVisibleHeightTrigger != scrimVisibleHeightTrigger) {
            this.scrimVisibleHeightTrigger = scrimVisibleHeightTrigger;
            this.updateScrimVisibility();
        }
    }
    
    public void setScrimsShown(final boolean b) {
        this.setScrimsShown(b, ViewCompat.isLaidOut((View)this) && !this.isInEditMode());
    }
    
    public void setScrimsShown(final boolean scrimsAreShown, final boolean b) {
        if (this.scrimsAreShown != scrimsAreShown) {
            int scrimAlpha = 255;
            if (b) {
                if (!scrimsAreShown) {
                    scrimAlpha = 0;
                }
                this.animateScrim(scrimAlpha);
            }
            else {
                if (!scrimsAreShown) {
                    scrimAlpha = 0;
                }
                this.setScrimAlpha(scrimAlpha);
            }
            this.scrimsAreShown = scrimsAreShown;
        }
    }
    
    public void setStatusBarScrim(Drawable drawable) {
        final Drawable statusBarScrim = this.statusBarScrim;
        if (statusBarScrim != drawable) {
            Drawable mutate = null;
            if (statusBarScrim != null) {
                statusBarScrim.setCallback((Drawable$Callback)null);
            }
            if (drawable != null) {
                mutate = drawable.mutate();
            }
            this.statusBarScrim = mutate;
            drawable = this.statusBarScrim;
            if (drawable != null) {
                if (drawable.isStateful()) {
                    this.statusBarScrim.setState(this.getDrawableState());
                }
                DrawableCompat.setLayoutDirection(this.statusBarScrim, ViewCompat.getLayoutDirection((View)this));
                drawable = this.statusBarScrim;
                drawable.setVisible(this.getVisibility() == 0, false);
                this.statusBarScrim.setCallback((Drawable$Callback)this);
                this.statusBarScrim.setAlpha(this.scrimAlpha);
            }
            ViewCompat.postInvalidateOnAnimation((View)this);
        }
    }
    
    public void setStatusBarScrimColor(final int n) {
        this.setStatusBarScrim((Drawable)new ColorDrawable(n));
    }
    
    public void setStatusBarScrimResource(final int n) {
        this.setStatusBarScrim(ContextCompat.getDrawable(this.getContext(), n));
    }
    
    public void setTitle(final CharSequence text) {
        this.collapsingTextHelper.setText(text);
        this.updateContentDescriptionFromTitle();
    }
    
    public void setTitleEnabled(final boolean collapsingTitleEnabled) {
        if (collapsingTitleEnabled != this.collapsingTitleEnabled) {
            this.collapsingTitleEnabled = collapsingTitleEnabled;
            this.updateContentDescriptionFromTitle();
            this.updateDummyView();
            this.requestLayout();
        }
    }
    
    public void setVisibility(final int visibility) {
        super.setVisibility(visibility);
        final boolean b = visibility == 0;
        final Drawable statusBarScrim = this.statusBarScrim;
        if (statusBarScrim != null && statusBarScrim.isVisible() != b) {
            this.statusBarScrim.setVisible(b, false);
        }
        final Drawable contentScrim = this.contentScrim;
        if (contentScrim != null && contentScrim.isVisible() != b) {
            this.contentScrim.setVisible(b, false);
        }
    }
    
    final void updateScrimVisibility() {
        if (this.contentScrim != null || this.statusBarScrim != null) {
            this.setScrimsShown(this.getHeight() + this.currentOffset < this.getScrimVisibleHeightTrigger());
        }
    }
    
    protected boolean verifyDrawable(final Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.contentScrim || drawable == this.statusBarScrim;
    }
    
    public static class LayoutParams extends FrameLayout$LayoutParams
    {
        public static final int COLLAPSE_MODE_OFF = 0;
        public static final int COLLAPSE_MODE_PARALLAX = 2;
        public static final int COLLAPSE_MODE_PIN = 1;
        private static final float DEFAULT_PARALLAX_MULTIPLIER = 0.5f;
        int collapseMode;
        float parallaxMult;
        
        public LayoutParams(final int n, final int n2) {
            super(n, n2);
            this.collapseMode = 0;
            this.parallaxMult = 0.5f;
        }
        
        public LayoutParams(final int n, final int n2, final int n3) {
            super(n, n2, n3);
            this.collapseMode = 0;
            this.parallaxMult = 0.5f;
        }
        
        public LayoutParams(final Context context, final AttributeSet set) {
            super(context, set);
            this.collapseMode = 0;
            this.parallaxMult = 0.5f;
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.CollapsingToolbarLayout_Layout);
            this.collapseMode = obtainStyledAttributes.getInt(R.styleable.CollapsingToolbarLayout_Layout_layout_collapseMode, 0);
            this.setParallaxMultiplier(obtainStyledAttributes.getFloat(R.styleable.CollapsingToolbarLayout_Layout_layout_collapseParallaxMultiplier, 0.5f));
            obtainStyledAttributes.recycle();
        }
        
        public LayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
            this.collapseMode = 0;
            this.parallaxMult = 0.5f;
        }
        
        public LayoutParams(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            super(viewGroup$MarginLayoutParams);
            this.collapseMode = 0;
            this.parallaxMult = 0.5f;
        }
        
        public LayoutParams(final FrameLayout$LayoutParams frameLayout$LayoutParams) {
            super(frameLayout$LayoutParams);
            this.collapseMode = 0;
            this.parallaxMult = 0.5f;
        }
        
        public int getCollapseMode() {
            return this.collapseMode;
        }
        
        public float getParallaxMultiplier() {
            return this.parallaxMult;
        }
        
        public void setCollapseMode(final int collapseMode) {
            this.collapseMode = collapseMode;
        }
        
        public void setParallaxMultiplier(final float parallaxMult) {
            this.parallaxMult = parallaxMult;
        }
    }
    
    private class OffsetUpdateListener implements OnOffsetChangedListener
    {
        OffsetUpdateListener() {
        }
        
        @Override
        public void onOffsetChanged(final AppBarLayout appBarLayout, final int n) {
            final CollapsingToolbarLayout this$0 = CollapsingToolbarLayout.this;
            this$0.currentOffset = n;
            int systemWindowInsetTop;
            if (this$0.lastInsets != null) {
                systemWindowInsetTop = CollapsingToolbarLayout.this.lastInsets.getSystemWindowInsetTop();
            }
            else {
                systemWindowInsetTop = 0;
            }
            for (int childCount = CollapsingToolbarLayout.this.getChildCount(), i = 0; i < childCount; ++i) {
                final View child = CollapsingToolbarLayout.this.getChildAt(i);
                final CollapsingToolbarLayout.LayoutParams layoutParams = (CollapsingToolbarLayout.LayoutParams)child.getLayoutParams();
                final ViewOffsetHelper viewOffsetHelper = CollapsingToolbarLayout.getViewOffsetHelper(child);
                final int collapseMode = layoutParams.collapseMode;
                if (collapseMode != 1) {
                    if (collapseMode == 2) {
                        viewOffsetHelper.setTopAndBottomOffset(Math.round(-n * layoutParams.parallaxMult));
                    }
                }
                else {
                    viewOffsetHelper.setTopAndBottomOffset(MathUtils.clamp(-n, 0, CollapsingToolbarLayout.this.getMaxOffsetForPinChild(child)));
                }
            }
            CollapsingToolbarLayout.this.updateScrimVisibility();
            if (CollapsingToolbarLayout.this.statusBarScrim != null && systemWindowInsetTop > 0) {
                ViewCompat.postInvalidateOnAnimation((View)CollapsingToolbarLayout.this);
            }
            CollapsingToolbarLayout.this.collapsingTextHelper.setExpansionFraction(Math.abs(n) / (float)(CollapsingToolbarLayout.this.getHeight() - ViewCompat.getMinimumHeight((View)CollapsingToolbarLayout.this) - systemWindowInsetTop));
        }
    }
}
