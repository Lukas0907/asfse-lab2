// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.appbar;

import androidx.core.view.ViewCompat;
import android.view.View;

class ViewOffsetHelper
{
    private int layoutLeft;
    private int layoutTop;
    private int offsetLeft;
    private int offsetTop;
    private final View view;
    
    public ViewOffsetHelper(final View view) {
        this.view = view;
    }
    
    private void updateOffsets() {
        final View view = this.view;
        ViewCompat.offsetTopAndBottom(view, this.offsetTop - (view.getTop() - this.layoutTop));
        final View view2 = this.view;
        ViewCompat.offsetLeftAndRight(view2, this.offsetLeft - (view2.getLeft() - this.layoutLeft));
    }
    
    public int getLayoutLeft() {
        return this.layoutLeft;
    }
    
    public int getLayoutTop() {
        return this.layoutTop;
    }
    
    public int getLeftAndRightOffset() {
        return this.offsetLeft;
    }
    
    public int getTopAndBottomOffset() {
        return this.offsetTop;
    }
    
    public void onViewLayout() {
        this.layoutTop = this.view.getTop();
        this.layoutLeft = this.view.getLeft();
        this.updateOffsets();
    }
    
    public boolean setLeftAndRightOffset(final int offsetLeft) {
        if (this.offsetLeft != offsetLeft) {
            this.offsetLeft = offsetLeft;
            this.updateOffsets();
            return true;
        }
        return false;
    }
    
    public boolean setTopAndBottomOffset(final int offsetTop) {
        if (this.offsetTop != offsetTop) {
            this.offsetTop = offsetTop;
            this.updateOffsets();
            return true;
        }
        return false;
    }
}
