// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.appbar;

import android.util.AttributeSet;
import android.content.Context;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import android.view.View;

class ViewOffsetBehavior<V extends View> extends Behavior<V>
{
    private int tempLeftRightOffset;
    private int tempTopBottomOffset;
    private ViewOffsetHelper viewOffsetHelper;
    
    public ViewOffsetBehavior() {
        this.tempTopBottomOffset = 0;
        this.tempLeftRightOffset = 0;
    }
    
    public ViewOffsetBehavior(final Context context, final AttributeSet set) {
        super(context, set);
        this.tempTopBottomOffset = 0;
        this.tempLeftRightOffset = 0;
    }
    
    public int getLeftAndRightOffset() {
        final ViewOffsetHelper viewOffsetHelper = this.viewOffsetHelper;
        if (viewOffsetHelper != null) {
            return viewOffsetHelper.getLeftAndRightOffset();
        }
        return 0;
    }
    
    public int getTopAndBottomOffset() {
        final ViewOffsetHelper viewOffsetHelper = this.viewOffsetHelper;
        if (viewOffsetHelper != null) {
            return viewOffsetHelper.getTopAndBottomOffset();
        }
        return 0;
    }
    
    protected void layoutChild(final CoordinatorLayout coordinatorLayout, final V v, final int n) {
        coordinatorLayout.onLayoutChild(v, n);
    }
    
    @Override
    public boolean onLayoutChild(final CoordinatorLayout coordinatorLayout, final V v, int n) {
        this.layoutChild(coordinatorLayout, v, n);
        if (this.viewOffsetHelper == null) {
            this.viewOffsetHelper = new ViewOffsetHelper(v);
        }
        this.viewOffsetHelper.onViewLayout();
        n = this.tempTopBottomOffset;
        if (n != 0) {
            this.viewOffsetHelper.setTopAndBottomOffset(n);
            this.tempTopBottomOffset = 0;
        }
        n = this.tempLeftRightOffset;
        if (n != 0) {
            this.viewOffsetHelper.setLeftAndRightOffset(n);
            this.tempLeftRightOffset = 0;
        }
        return true;
    }
    
    public boolean setLeftAndRightOffset(final int n) {
        final ViewOffsetHelper viewOffsetHelper = this.viewOffsetHelper;
        if (viewOffsetHelper != null) {
            return viewOffsetHelper.setLeftAndRightOffset(n);
        }
        this.tempLeftRightOffset = n;
        return false;
    }
    
    public boolean setTopAndBottomOffset(final int n) {
        final ViewOffsetHelper viewOffsetHelper = this.viewOffsetHelper;
        if (viewOffsetHelper != null) {
            return viewOffsetHelper.setTopAndBottomOffset(n);
        }
        this.tempTopBottomOffset = n;
        return false;
    }
}
