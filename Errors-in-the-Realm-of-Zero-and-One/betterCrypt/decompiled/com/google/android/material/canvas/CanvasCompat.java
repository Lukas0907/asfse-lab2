// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.canvas;

import android.graphics.RectF;
import android.os.Build$VERSION;
import android.graphics.Canvas;

public class CanvasCompat
{
    private CanvasCompat() {
    }
    
    public static int saveLayerAlpha(final Canvas canvas, final float n, final float n2, final float n3, final float n4, final int n5) {
        if (Build$VERSION.SDK_INT > 21) {
            return canvas.saveLayerAlpha(n, n2, n3, n4, n5);
        }
        return canvas.saveLayerAlpha(n, n2, n3, n4, n5, 31);
    }
    
    public static int saveLayerAlpha(final Canvas canvas, final RectF rectF, final int n) {
        if (Build$VERSION.SDK_INT > 21) {
            return canvas.saveLayerAlpha(rectF, n);
        }
        return canvas.saveLayerAlpha(rectF, n, 31);
    }
}
