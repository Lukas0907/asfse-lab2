// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.bottomsheet;

import android.app.Dialog;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatDialogFragment;

public class BottomSheetDialogFragment extends AppCompatDialogFragment
{
    @Override
    public Dialog onCreateDialog(final Bundle bundle) {
        return new BottomSheetDialog(this.getContext(), this.getTheme());
    }
}
