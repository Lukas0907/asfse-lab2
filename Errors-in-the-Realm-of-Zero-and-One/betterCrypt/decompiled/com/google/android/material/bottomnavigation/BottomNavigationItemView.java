// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.bottomnavigation;

import androidx.core.widget.TextViewCompat;
import androidx.core.content.ContextCompat;
import android.graphics.drawable.Drawable$ConstantState;
import androidx.core.graphics.drawable.DrawableCompat;
import android.graphics.drawable.Drawable;
import androidx.core.view.PointerIconCompat;
import androidx.appcompat.widget.TooltipCompat;
import android.text.TextUtils;
import android.view.ViewGroup$LayoutParams;
import android.widget.FrameLayout$LayoutParams;
import android.content.res.Resources;
import android.view.View;
import androidx.core.view.ViewCompat;
import android.view.ViewGroup;
import com.google.android.material.R;
import android.view.LayoutInflater;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.TextView;
import androidx.appcompat.view.menu.MenuItemImpl;
import android.content.res.ColorStateList;
import android.widget.ImageView;
import androidx.appcompat.view.menu.MenuView;
import android.widget.FrameLayout;

public class BottomNavigationItemView extends FrameLayout implements ItemView
{
    private static final int[] CHECKED_STATE_SET;
    public static final int INVALID_ITEM_POSITION = -1;
    private final int defaultMargin;
    private ImageView icon;
    private ColorStateList iconTint;
    private boolean isShifting;
    private MenuItemImpl itemData;
    private int itemPosition;
    private int labelVisibilityMode;
    private final TextView largeLabel;
    private float scaleDownFactor;
    private float scaleUpFactor;
    private float shiftAmount;
    private final TextView smallLabel;
    
    static {
        CHECKED_STATE_SET = new int[] { 16842912 };
    }
    
    public BottomNavigationItemView(final Context context) {
        this(context, null);
    }
    
    public BottomNavigationItemView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public BottomNavigationItemView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.itemPosition = -1;
        final Resources resources = this.getResources();
        LayoutInflater.from(context).inflate(R.layout.design_bottom_navigation_item, (ViewGroup)this, true);
        this.setBackgroundResource(R.drawable.design_bottom_navigation_item_background);
        this.defaultMargin = resources.getDimensionPixelSize(R.dimen.design_bottom_navigation_margin);
        this.icon = (ImageView)this.findViewById(R.id.icon);
        this.smallLabel = (TextView)this.findViewById(R.id.smallLabel);
        this.largeLabel = (TextView)this.findViewById(R.id.largeLabel);
        ViewCompat.setImportantForAccessibility((View)this.smallLabel, 2);
        ViewCompat.setImportantForAccessibility((View)this.largeLabel, 2);
        this.calculateTextScaleFactors(this.smallLabel.getTextSize(), this.largeLabel.getTextSize());
    }
    
    private void calculateTextScaleFactors(final float n, final float n2) {
        this.shiftAmount = n - n2;
        this.scaleUpFactor = n2 * 1.0f / n;
        this.scaleDownFactor = n * 1.0f / n2;
    }
    
    private void setViewLayoutParams(final View view, final int topMargin, final int gravity) {
        final FrameLayout$LayoutParams layoutParams = (FrameLayout$LayoutParams)view.getLayoutParams();
        layoutParams.topMargin = topMargin;
        layoutParams.gravity = gravity;
        view.setLayoutParams((ViewGroup$LayoutParams)layoutParams);
    }
    
    private void setViewValues(final View view, final float scaleX, final float scaleY, final int visibility) {
        view.setScaleX(scaleX);
        view.setScaleY(scaleY);
        view.setVisibility(visibility);
    }
    
    public MenuItemImpl getItemData() {
        return this.itemData;
    }
    
    public int getItemPosition() {
        return this.itemPosition;
    }
    
    public void initialize(final MenuItemImpl itemData, int visibility) {
        this.itemData = itemData;
        this.setCheckable(itemData.isCheckable());
        this.setChecked(itemData.isChecked());
        this.setEnabled(itemData.isEnabled());
        this.setIcon(itemData.getIcon());
        this.setTitle(itemData.getTitle());
        this.setId(itemData.getItemId());
        if (!TextUtils.isEmpty(itemData.getContentDescription())) {
            this.setContentDescription(itemData.getContentDescription());
        }
        TooltipCompat.setTooltipText((View)this, itemData.getTooltipText());
        if (itemData.isVisible()) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        this.setVisibility(visibility);
    }
    
    public int[] onCreateDrawableState(final int n) {
        final int[] onCreateDrawableState = super.onCreateDrawableState(n + 1);
        final MenuItemImpl itemData = this.itemData;
        if (itemData != null && itemData.isCheckable() && this.itemData.isChecked()) {
            mergeDrawableStates(onCreateDrawableState, BottomNavigationItemView.CHECKED_STATE_SET);
        }
        return onCreateDrawableState;
    }
    
    public boolean prefersCondensedTitle() {
        return false;
    }
    
    public void setCheckable(final boolean b) {
        this.refreshDrawableState();
    }
    
    public void setChecked(final boolean selected) {
        final TextView largeLabel = this.largeLabel;
        largeLabel.setPivotX((float)(largeLabel.getWidth() / 2));
        final TextView largeLabel2 = this.largeLabel;
        largeLabel2.setPivotY((float)largeLabel2.getBaseline());
        final TextView smallLabel = this.smallLabel;
        smallLabel.setPivotX((float)(smallLabel.getWidth() / 2));
        final TextView smallLabel2 = this.smallLabel;
        smallLabel2.setPivotY((float)smallLabel2.getBaseline());
        final int labelVisibilityMode = this.labelVisibilityMode;
        if (labelVisibilityMode != -1) {
            if (labelVisibilityMode != 0) {
                if (labelVisibilityMode != 1) {
                    if (labelVisibilityMode == 2) {
                        this.setViewLayoutParams((View)this.icon, this.defaultMargin, 17);
                        this.largeLabel.setVisibility(8);
                        this.smallLabel.setVisibility(8);
                    }
                }
                else if (selected) {
                    this.setViewLayoutParams((View)this.icon, (int)(this.defaultMargin + this.shiftAmount), 49);
                    this.setViewValues((View)this.largeLabel, 1.0f, 1.0f, 0);
                    final TextView smallLabel3 = this.smallLabel;
                    final float scaleUpFactor = this.scaleUpFactor;
                    this.setViewValues((View)smallLabel3, scaleUpFactor, scaleUpFactor, 4);
                }
                else {
                    this.setViewLayoutParams((View)this.icon, this.defaultMargin, 49);
                    final TextView largeLabel3 = this.largeLabel;
                    final float scaleDownFactor = this.scaleDownFactor;
                    this.setViewValues((View)largeLabel3, scaleDownFactor, scaleDownFactor, 4);
                    this.setViewValues((View)this.smallLabel, 1.0f, 1.0f, 0);
                }
            }
            else {
                if (selected) {
                    this.setViewLayoutParams((View)this.icon, this.defaultMargin, 49);
                    this.setViewValues((View)this.largeLabel, 1.0f, 1.0f, 0);
                }
                else {
                    this.setViewLayoutParams((View)this.icon, this.defaultMargin, 17);
                    this.setViewValues((View)this.largeLabel, 0.5f, 0.5f, 4);
                }
                this.smallLabel.setVisibility(4);
            }
        }
        else if (this.isShifting) {
            if (selected) {
                this.setViewLayoutParams((View)this.icon, this.defaultMargin, 49);
                this.setViewValues((View)this.largeLabel, 1.0f, 1.0f, 0);
            }
            else {
                this.setViewLayoutParams((View)this.icon, this.defaultMargin, 17);
                this.setViewValues((View)this.largeLabel, 0.5f, 0.5f, 4);
            }
            this.smallLabel.setVisibility(4);
        }
        else if (selected) {
            this.setViewLayoutParams((View)this.icon, (int)(this.defaultMargin + this.shiftAmount), 49);
            this.setViewValues((View)this.largeLabel, 1.0f, 1.0f, 0);
            final TextView smallLabel4 = this.smallLabel;
            final float scaleUpFactor2 = this.scaleUpFactor;
            this.setViewValues((View)smallLabel4, scaleUpFactor2, scaleUpFactor2, 4);
        }
        else {
            this.setViewLayoutParams((View)this.icon, this.defaultMargin, 49);
            final TextView largeLabel4 = this.largeLabel;
            final float scaleDownFactor2 = this.scaleDownFactor;
            this.setViewValues((View)largeLabel4, scaleDownFactor2, scaleDownFactor2, 4);
            this.setViewValues((View)this.smallLabel, 1.0f, 1.0f, 0);
        }
        this.refreshDrawableState();
        this.setSelected(selected);
    }
    
    public void setEnabled(final boolean b) {
        super.setEnabled(b);
        this.smallLabel.setEnabled(b);
        this.largeLabel.setEnabled(b);
        this.icon.setEnabled(b);
        if (b) {
            ViewCompat.setPointerIcon((View)this, PointerIconCompat.getSystemIcon(this.getContext(), 1002));
            return;
        }
        ViewCompat.setPointerIcon((View)this, null);
    }
    
    public void setIcon(Drawable drawable) {
        Drawable mutate = drawable;
        if (drawable != null) {
            final Drawable$ConstantState constantState = drawable.getConstantState();
            if (constantState != null) {
                drawable = constantState.newDrawable();
            }
            mutate = DrawableCompat.wrap(drawable).mutate();
            DrawableCompat.setTintList(mutate, this.iconTint);
        }
        this.icon.setImageDrawable(mutate);
    }
    
    public void setIconSize(final int n) {
        final FrameLayout$LayoutParams layoutParams = (FrameLayout$LayoutParams)this.icon.getLayoutParams();
        layoutParams.width = n;
        layoutParams.height = n;
        this.icon.setLayoutParams((ViewGroup$LayoutParams)layoutParams);
    }
    
    public void setIconTintList(final ColorStateList iconTint) {
        this.iconTint = iconTint;
        final MenuItemImpl itemData = this.itemData;
        if (itemData != null) {
            this.setIcon(itemData.getIcon());
        }
    }
    
    public void setItemBackground(final int n) {
        Drawable drawable;
        if (n == 0) {
            drawable = null;
        }
        else {
            drawable = ContextCompat.getDrawable(this.getContext(), n);
        }
        this.setItemBackground(drawable);
    }
    
    public void setItemBackground(final Drawable drawable) {
        ViewCompat.setBackground((View)this, drawable);
    }
    
    public void setItemPosition(final int itemPosition) {
        this.itemPosition = itemPosition;
    }
    
    public void setLabelVisibilityMode(int labelVisibilityMode) {
        if (this.labelVisibilityMode != labelVisibilityMode) {
            this.labelVisibilityMode = labelVisibilityMode;
            if (this.itemData != null) {
                labelVisibilityMode = 1;
            }
            else {
                labelVisibilityMode = 0;
            }
            if (labelVisibilityMode != 0) {
                this.setChecked(this.itemData.isChecked());
            }
        }
    }
    
    public void setShifting(final boolean isShifting) {
        if (this.isShifting != isShifting) {
            this.isShifting = isShifting;
            if (this.itemData != null) {
                this.setChecked(this.itemData.isChecked());
            }
        }
    }
    
    public void setShortcut(final boolean b, final char c) {
    }
    
    public void setTextAppearanceActive(final int n) {
        TextViewCompat.setTextAppearance(this.largeLabel, n);
        this.calculateTextScaleFactors(this.smallLabel.getTextSize(), this.largeLabel.getTextSize());
    }
    
    public void setTextAppearanceInactive(final int n) {
        TextViewCompat.setTextAppearance(this.smallLabel, n);
        this.calculateTextScaleFactors(this.smallLabel.getTextSize(), this.largeLabel.getTextSize());
    }
    
    public void setTextColor(final ColorStateList list) {
        if (list != null) {
            this.smallLabel.setTextColor(list);
            this.largeLabel.setTextColor(list);
        }
    }
    
    public void setTitle(final CharSequence contentDescription) {
        this.smallLabel.setText(contentDescription);
        this.largeLabel.setText(contentDescription);
        final MenuItemImpl itemData = this.itemData;
        if (itemData == null || TextUtils.isEmpty(itemData.getContentDescription())) {
            this.setContentDescription(contentDescription);
        }
    }
    
    public boolean showsIcon() {
        return true;
    }
}
