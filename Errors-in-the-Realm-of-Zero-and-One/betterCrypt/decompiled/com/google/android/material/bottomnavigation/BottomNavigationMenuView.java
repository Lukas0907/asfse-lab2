// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.bottomnavigation;

import androidx.transition.TransitionManager;
import android.view.View$MeasureSpec;
import androidx.core.view.ViewCompat;
import androidx.appcompat.content.res.AppCompatResources;
import android.util.TypedValue;
import android.content.res.Resources;
import androidx.appcompat.view.menu.MenuItemImpl;
import androidx.appcompat.view.menu.MenuPresenter;
import android.view.MenuItem;
import android.view.View;
import androidx.transition.Transition;
import com.google.android.material.internal.TextScale;
import android.animation.TimeInterpolator;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import androidx.transition.AutoTransition;
import com.google.android.material.R;
import android.util.AttributeSet;
import android.content.Context;
import androidx.transition.TransitionSet;
import android.view.View$OnClickListener;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.core.util.Pools;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import androidx.appcompat.view.menu.MenuView;
import android.view.ViewGroup;

public class BottomNavigationMenuView extends ViewGroup implements MenuView
{
    private static final long ACTIVE_ANIMATION_DURATION_MS = 115L;
    private static final int[] CHECKED_STATE_SET;
    private static final int[] DISABLED_STATE_SET;
    private final int activeItemMaxWidth;
    private final int activeItemMinWidth;
    private BottomNavigationItemView[] buttons;
    private final int inactiveItemMaxWidth;
    private final int inactiveItemMinWidth;
    private Drawable itemBackground;
    private int itemBackgroundRes;
    private final int itemHeight;
    private boolean itemHorizontalTranslationEnabled;
    private int itemIconSize;
    private ColorStateList itemIconTint;
    private final Pools.Pool<BottomNavigationItemView> itemPool;
    private int itemTextAppearanceActive;
    private int itemTextAppearanceInactive;
    private final ColorStateList itemTextColorDefault;
    private ColorStateList itemTextColorFromUser;
    private int labelVisibilityMode;
    private MenuBuilder menu;
    private final View$OnClickListener onClickListener;
    private BottomNavigationPresenter presenter;
    private int selectedItemId;
    private int selectedItemPosition;
    private final TransitionSet set;
    private int[] tempChildWidths;
    
    static {
        CHECKED_STATE_SET = new int[] { 16842912 };
        DISABLED_STATE_SET = new int[] { -16842910 };
    }
    
    public BottomNavigationMenuView(final Context context) {
        this(context, null);
    }
    
    public BottomNavigationMenuView(final Context context, final AttributeSet set) {
        super(context, set);
        this.itemPool = new Pools.SynchronizedPool<BottomNavigationItemView>(5);
        this.selectedItemId = 0;
        this.selectedItemPosition = 0;
        final Resources resources = this.getResources();
        this.inactiveItemMaxWidth = resources.getDimensionPixelSize(R.dimen.design_bottom_navigation_item_max_width);
        this.inactiveItemMinWidth = resources.getDimensionPixelSize(R.dimen.design_bottom_navigation_item_min_width);
        this.activeItemMaxWidth = resources.getDimensionPixelSize(R.dimen.design_bottom_navigation_active_item_max_width);
        this.activeItemMinWidth = resources.getDimensionPixelSize(R.dimen.design_bottom_navigation_active_item_min_width);
        this.itemHeight = resources.getDimensionPixelSize(R.dimen.design_bottom_navigation_height);
        this.itemTextColorDefault = this.createDefaultColorStateList(16842808);
        (this.set = new AutoTransition()).setOrdering(0);
        this.set.setDuration(115L);
        this.set.setInterpolator((TimeInterpolator)new FastOutSlowInInterpolator());
        this.set.addTransition(new TextScale());
        this.onClickListener = (View$OnClickListener)new View$OnClickListener() {
            public void onClick(final View view) {
                final MenuItemImpl itemData = ((BottomNavigationItemView)view).getItemData();
                if (!BottomNavigationMenuView.this.menu.performItemAction((MenuItem)itemData, BottomNavigationMenuView.this.presenter, 0)) {
                    ((MenuItem)itemData).setChecked(true);
                }
            }
        };
        this.tempChildWidths = new int[5];
    }
    
    private BottomNavigationItemView getNewItem() {
        BottomNavigationItemView bottomNavigationItemView;
        if ((bottomNavigationItemView = this.itemPool.acquire()) == null) {
            bottomNavigationItemView = new BottomNavigationItemView(this.getContext());
        }
        return bottomNavigationItemView;
    }
    
    private boolean isShifting(final int n, final int n2) {
        if (n == -1) {
            if (n2 > 3) {
                return true;
            }
        }
        else if (n == 0) {
            return true;
        }
        return false;
    }
    
    public void buildMenuView() {
        this.removeAllViews();
        final BottomNavigationItemView[] buttons = this.buttons;
        if (buttons != null) {
            for (int length = buttons.length, i = 0; i < length; ++i) {
                final BottomNavigationItemView bottomNavigationItemView = buttons[i];
                if (bottomNavigationItemView != null) {
                    this.itemPool.release(bottomNavigationItemView);
                }
            }
        }
        if (this.menu.size() == 0) {
            this.selectedItemId = 0;
            this.selectedItemPosition = 0;
            this.buttons = null;
            return;
        }
        this.buttons = new BottomNavigationItemView[this.menu.size()];
        final boolean shifting = this.isShifting(this.labelVisibilityMode, this.menu.getVisibleItems().size());
        for (int j = 0; j < this.menu.size(); ++j) {
            this.presenter.setUpdateSuspended(true);
            this.menu.getItem(j).setCheckable(true);
            this.presenter.setUpdateSuspended(false);
            final BottomNavigationItemView newItem = this.getNewItem();
            (this.buttons[j] = newItem).setIconTintList(this.itemIconTint);
            newItem.setIconSize(this.itemIconSize);
            newItem.setTextColor(this.itemTextColorDefault);
            newItem.setTextAppearanceInactive(this.itemTextAppearanceInactive);
            newItem.setTextAppearanceActive(this.itemTextAppearanceActive);
            newItem.setTextColor(this.itemTextColorFromUser);
            final Drawable itemBackground = this.itemBackground;
            if (itemBackground != null) {
                newItem.setItemBackground(itemBackground);
            }
            else {
                newItem.setItemBackground(this.itemBackgroundRes);
            }
            newItem.setShifting(shifting);
            newItem.setLabelVisibilityMode(this.labelVisibilityMode);
            newItem.initialize((MenuItemImpl)this.menu.getItem(j), 0);
            newItem.setItemPosition(j);
            newItem.setOnClickListener(this.onClickListener);
            this.addView((View)newItem);
        }
        this.selectedItemPosition = Math.min(this.menu.size() - 1, this.selectedItemPosition);
        this.menu.getItem(this.selectedItemPosition).setChecked(true);
    }
    
    public ColorStateList createDefaultColorStateList(int data) {
        final TypedValue typedValue = new TypedValue();
        if (!this.getContext().getTheme().resolveAttribute(data, typedValue, true)) {
            return null;
        }
        final ColorStateList colorStateList = AppCompatResources.getColorStateList(this.getContext(), typedValue.resourceId);
        if (!this.getContext().getTheme().resolveAttribute(androidx.appcompat.R.attr.colorPrimary, typedValue, true)) {
            return null;
        }
        data = typedValue.data;
        final int defaultColor = colorStateList.getDefaultColor();
        return new ColorStateList(new int[][] { BottomNavigationMenuView.DISABLED_STATE_SET, BottomNavigationMenuView.CHECKED_STATE_SET, BottomNavigationMenuView.EMPTY_STATE_SET }, new int[] { colorStateList.getColorForState(BottomNavigationMenuView.DISABLED_STATE_SET, defaultColor), data, defaultColor });
    }
    
    public ColorStateList getIconTintList() {
        return this.itemIconTint;
    }
    
    public Drawable getItemBackground() {
        final BottomNavigationItemView[] buttons = this.buttons;
        if (buttons != null && buttons.length > 0) {
            return buttons[0].getBackground();
        }
        return this.itemBackground;
    }
    
    @Deprecated
    public int getItemBackgroundRes() {
        return this.itemBackgroundRes;
    }
    
    public int getItemIconSize() {
        return this.itemIconSize;
    }
    
    public int getItemTextAppearanceActive() {
        return this.itemTextAppearanceActive;
    }
    
    public int getItemTextAppearanceInactive() {
        return this.itemTextAppearanceInactive;
    }
    
    public ColorStateList getItemTextColor() {
        return this.itemTextColorFromUser;
    }
    
    public int getLabelVisibilityMode() {
        return this.labelVisibilityMode;
    }
    
    public int getSelectedItemId() {
        return this.selectedItemId;
    }
    
    public int getWindowAnimations() {
        return 0;
    }
    
    public void initialize(final MenuBuilder menu) {
        this.menu = menu;
    }
    
    public boolean isItemHorizontalTranslationEnabled() {
        return this.itemHorizontalTranslationEnabled;
    }
    
    protected void onLayout(final boolean b, final int n, int i, final int n2, int n3) {
        final int childCount = this.getChildCount();
        final int n4 = n3 - i;
        View child;
        int n5;
        for (i = (n3 = 0); i < childCount; ++i) {
            child = this.getChildAt(i);
            if (child.getVisibility() != 8) {
                if (ViewCompat.getLayoutDirection((View)this) == 1) {
                    n5 = n2 - n - n3;
                    child.layout(n5 - child.getMeasuredWidth(), 0, n5, n4);
                }
                else {
                    child.layout(n3, 0, child.getMeasuredWidth() + n3, n4);
                }
                n3 += child.getMeasuredWidth();
            }
        }
    }
    
    protected void onMeasure(int i, int a) {
        final int size = View$MeasureSpec.getSize(i);
        final int size2 = this.menu.getVisibleItems().size();
        final int childCount = this.getChildCount();
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(this.itemHeight, 1073741824);
        if (this.isShifting(this.labelVisibilityMode, size2) && this.itemHorizontalTranslationEnabled) {
            final View child = this.getChildAt(this.selectedItemPosition);
            a = (i = this.activeItemMinWidth);
            if (child.getVisibility() != 8) {
                child.measure(View$MeasureSpec.makeMeasureSpec(this.activeItemMaxWidth, Integer.MIN_VALUE), measureSpec);
                i = Math.max(a, child.getMeasuredWidth());
            }
            if (child.getVisibility() != 8) {
                a = 1;
            }
            else {
                a = 0;
            }
            a = size2 - a;
            final int min = Math.min(size - this.inactiveItemMinWidth * a, Math.min(i, this.activeItemMaxWidth));
            final int n = size - min;
            if (a == 0) {
                i = 1;
            }
            else {
                i = a;
            }
            final int min2 = Math.min(n / i, this.inactiveItemMaxWidth);
            a = n - a * min2;
            int[] tempChildWidths;
            int n2;
            int n3;
            int[] tempChildWidths2;
            for (i = 0; i < childCount; ++i, a = n3) {
                if (this.getChildAt(i).getVisibility() != 8) {
                    tempChildWidths = this.tempChildWidths;
                    if (i == this.selectedItemPosition) {
                        n2 = min;
                    }
                    else {
                        n2 = min2;
                    }
                    tempChildWidths[i] = n2;
                    n3 = a;
                    if (a > 0) {
                        tempChildWidths2 = this.tempChildWidths;
                        ++tempChildWidths2[i];
                        n3 = a - 1;
                    }
                }
                else {
                    this.tempChildWidths[i] = 0;
                    n3 = a;
                }
            }
        }
        else {
            if (size2 == 0) {
                i = 1;
            }
            else {
                i = size2;
            }
            final int min3 = Math.min(size / i, this.activeItemMaxWidth);
            a = size - size2 * min3;
            int[] tempChildWidths3;
            int n4;
            for (i = 0; i < childCount; ++i, a = n4) {
                if (this.getChildAt(i).getVisibility() != 8) {
                    tempChildWidths3 = this.tempChildWidths;
                    tempChildWidths3[i] = min3;
                    if ((n4 = a) > 0) {
                        ++tempChildWidths3[i];
                        n4 = a - 1;
                    }
                }
                else {
                    this.tempChildWidths[i] = 0;
                    n4 = a;
                }
            }
        }
        View child2;
        for (i = (a = 0); i < childCount; ++i) {
            child2 = this.getChildAt(i);
            if (child2.getVisibility() != 8) {
                child2.measure(View$MeasureSpec.makeMeasureSpec(this.tempChildWidths[i], 1073741824), measureSpec);
                child2.getLayoutParams().width = child2.getMeasuredWidth();
                a += child2.getMeasuredWidth();
            }
        }
        this.setMeasuredDimension(View.resolveSizeAndState(a, View$MeasureSpec.makeMeasureSpec(a, 1073741824), 0), View.resolveSizeAndState(this.itemHeight, measureSpec, 0));
    }
    
    public void setIconTintList(final ColorStateList list) {
        this.itemIconTint = list;
        final BottomNavigationItemView[] buttons = this.buttons;
        if (buttons != null) {
            for (int length = buttons.length, i = 0; i < length; ++i) {
                buttons[i].setIconTintList(list);
            }
        }
    }
    
    public void setItemBackground(final Drawable drawable) {
        this.itemBackground = drawable;
        final BottomNavigationItemView[] buttons = this.buttons;
        if (buttons != null) {
            for (int length = buttons.length, i = 0; i < length; ++i) {
                buttons[i].setItemBackground(drawable);
            }
        }
    }
    
    public void setItemBackgroundRes(final int n) {
        this.itemBackgroundRes = n;
        final BottomNavigationItemView[] buttons = this.buttons;
        if (buttons != null) {
            for (int length = buttons.length, i = 0; i < length; ++i) {
                buttons[i].setItemBackground(n);
            }
        }
    }
    
    public void setItemHorizontalTranslationEnabled(final boolean itemHorizontalTranslationEnabled) {
        this.itemHorizontalTranslationEnabled = itemHorizontalTranslationEnabled;
    }
    
    public void setItemIconSize(final int n) {
        this.itemIconSize = n;
        final BottomNavigationItemView[] buttons = this.buttons;
        if (buttons != null) {
            for (int length = buttons.length, i = 0; i < length; ++i) {
                buttons[i].setIconSize(n);
            }
        }
    }
    
    public void setItemTextAppearanceActive(final int n) {
        this.itemTextAppearanceActive = n;
        final BottomNavigationItemView[] buttons = this.buttons;
        if (buttons != null) {
            for (int length = buttons.length, i = 0; i < length; ++i) {
                final BottomNavigationItemView bottomNavigationItemView = buttons[i];
                bottomNavigationItemView.setTextAppearanceActive(n);
                final ColorStateList itemTextColorFromUser = this.itemTextColorFromUser;
                if (itemTextColorFromUser != null) {
                    bottomNavigationItemView.setTextColor(itemTextColorFromUser);
                }
            }
        }
    }
    
    public void setItemTextAppearanceInactive(final int n) {
        this.itemTextAppearanceInactive = n;
        final BottomNavigationItemView[] buttons = this.buttons;
        if (buttons != null) {
            for (int length = buttons.length, i = 0; i < length; ++i) {
                final BottomNavigationItemView bottomNavigationItemView = buttons[i];
                bottomNavigationItemView.setTextAppearanceInactive(n);
                final ColorStateList itemTextColorFromUser = this.itemTextColorFromUser;
                if (itemTextColorFromUser != null) {
                    bottomNavigationItemView.setTextColor(itemTextColorFromUser);
                }
            }
        }
    }
    
    public void setItemTextColor(final ColorStateList list) {
        this.itemTextColorFromUser = list;
        final BottomNavigationItemView[] buttons = this.buttons;
        if (buttons != null) {
            for (int length = buttons.length, i = 0; i < length; ++i) {
                buttons[i].setTextColor(list);
            }
        }
    }
    
    public void setLabelVisibilityMode(final int labelVisibilityMode) {
        this.labelVisibilityMode = labelVisibilityMode;
    }
    
    public void setPresenter(final BottomNavigationPresenter presenter) {
        this.presenter = presenter;
    }
    
    void tryRestoreSelectedItemId(final int selectedItemId) {
        for (int size = this.menu.size(), i = 0; i < size; ++i) {
            final MenuItem item = this.menu.getItem(i);
            if (selectedItemId == item.getItemId()) {
                this.selectedItemId = selectedItemId;
                this.selectedItemPosition = i;
                item.setChecked(true);
                return;
            }
        }
    }
    
    public void updateMenuView() {
        final MenuBuilder menu = this.menu;
        if (menu != null) {
            if (this.buttons == null) {
                return;
            }
            final int size = menu.size();
            if (size != this.buttons.length) {
                this.buildMenuView();
                return;
            }
            final int selectedItemId = this.selectedItemId;
            for (int i = 0; i < size; ++i) {
                final MenuItem item = this.menu.getItem(i);
                if (item.isChecked()) {
                    this.selectedItemId = item.getItemId();
                    this.selectedItemPosition = i;
                }
            }
            if (selectedItemId != this.selectedItemId) {
                TransitionManager.beginDelayedTransition(this, this.set);
            }
            final boolean shifting = this.isShifting(this.labelVisibilityMode, this.menu.getVisibleItems().size());
            for (int j = 0; j < size; ++j) {
                this.presenter.setUpdateSuspended(true);
                this.buttons[j].setLabelVisibilityMode(this.labelVisibilityMode);
                this.buttons[j].setShifting(shifting);
                this.buttons[j].initialize((MenuItemImpl)this.menu.getItem(j), 0);
                this.presenter.setUpdateSuspended(false);
            }
        }
    }
}
