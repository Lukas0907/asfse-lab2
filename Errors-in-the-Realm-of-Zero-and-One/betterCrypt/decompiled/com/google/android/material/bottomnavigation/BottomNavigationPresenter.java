// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.bottomnavigation;

import android.os.Parcel;
import android.os.Parcelable$Creator;
import androidx.appcompat.view.menu.SubMenuBuilder;
import android.os.Parcelable;
import android.content.Context;
import androidx.appcompat.view.menu.MenuView;
import android.view.ViewGroup;
import androidx.appcompat.view.menu.MenuItemImpl;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPresenter;

public class BottomNavigationPresenter implements MenuPresenter
{
    private int id;
    private MenuBuilder menu;
    private BottomNavigationMenuView menuView;
    private boolean updateSuspended;
    
    public BottomNavigationPresenter() {
        this.updateSuspended = false;
    }
    
    @Override
    public boolean collapseItemActionView(final MenuBuilder menuBuilder, final MenuItemImpl menuItemImpl) {
        return false;
    }
    
    @Override
    public boolean expandItemActionView(final MenuBuilder menuBuilder, final MenuItemImpl menuItemImpl) {
        return false;
    }
    
    @Override
    public boolean flagActionItems() {
        return false;
    }
    
    @Override
    public int getId() {
        return this.id;
    }
    
    @Override
    public MenuView getMenuView(final ViewGroup viewGroup) {
        return this.menuView;
    }
    
    @Override
    public void initForMenu(final Context context, final MenuBuilder menu) {
        this.menu = menu;
        this.menuView.initialize(this.menu);
    }
    
    @Override
    public void onCloseMenu(final MenuBuilder menuBuilder, final boolean b) {
    }
    
    @Override
    public void onRestoreInstanceState(final Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            this.menuView.tryRestoreSelectedItemId(((SavedState)parcelable).selectedItemId);
        }
    }
    
    @Override
    public Parcelable onSaveInstanceState() {
        final SavedState savedState = new SavedState();
        savedState.selectedItemId = this.menuView.getSelectedItemId();
        return (Parcelable)savedState;
    }
    
    @Override
    public boolean onSubMenuSelected(final SubMenuBuilder subMenuBuilder) {
        return false;
    }
    
    public void setBottomNavigationMenuView(final BottomNavigationMenuView menuView) {
        this.menuView = menuView;
    }
    
    @Override
    public void setCallback(final Callback callback) {
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    public void setUpdateSuspended(final boolean updateSuspended) {
        this.updateSuspended = updateSuspended;
    }
    
    @Override
    public void updateMenuView(final boolean b) {
        if (this.updateSuspended) {
            return;
        }
        if (b) {
            this.menuView.buildMenuView();
            return;
        }
        this.menuView.updateMenuView();
    }
    
    static class SavedState implements Parcelable
    {
        public static final Parcelable$Creator<SavedState> CREATOR;
        int selectedItemId;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator<SavedState>() {
                public SavedState createFromParcel(final Parcel parcel) {
                    return new SavedState(parcel);
                }
                
                public SavedState[] newArray(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        SavedState() {
        }
        
        SavedState(final Parcel parcel) {
            this.selectedItemId = parcel.readInt();
        }
        
        public int describeContents() {
            return 0;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeInt(this.selectedItemId);
        }
    }
}
