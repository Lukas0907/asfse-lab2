// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.bottomnavigation;

import android.os.Parcel;
import android.os.Parcelable$ClassLoaderCreator;
import android.os.Parcelable$Creator;
import androidx.customview.view.AbsSavedState;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Menu;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import androidx.appcompat.view.SupportMenuInflater;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.TintTypedArray;
import android.view.MenuItem;
import android.os.Build$VERSION;
import android.view.View;
import androidx.core.view.ViewCompat;
import com.google.android.material.internal.ThemeEnforcement;
import androidx.appcompat.view.menu.MenuPresenter;
import android.view.ViewGroup$LayoutParams;
import android.widget.FrameLayout$LayoutParams;
import com.google.android.material.R;
import android.util.AttributeSet;
import android.content.Context;
import android.view.MenuInflater;
import androidx.appcompat.view.menu.MenuBuilder;
import android.widget.FrameLayout;

public class BottomNavigationView extends FrameLayout
{
    private static final int MENU_PRESENTER_ID = 1;
    private final MenuBuilder menu;
    private MenuInflater menuInflater;
    private final BottomNavigationMenuView menuView;
    private final BottomNavigationPresenter presenter;
    private OnNavigationItemReselectedListener reselectedListener;
    private OnNavigationItemSelectedListener selectedListener;
    
    public BottomNavigationView(final Context context) {
        this(context, null);
    }
    
    public BottomNavigationView(final Context context, final AttributeSet set) {
        this(context, set, R.attr.bottomNavigationStyle);
    }
    
    public BottomNavigationView(final Context context, final AttributeSet set, int resourceId) {
        super(context, set, resourceId);
        this.presenter = new BottomNavigationPresenter();
        this.menu = new BottomNavigationMenu(context);
        this.menuView = new BottomNavigationMenuView(context);
        final FrameLayout$LayoutParams layoutParams = new FrameLayout$LayoutParams(-2, -2);
        layoutParams.gravity = 17;
        this.menuView.setLayoutParams((ViewGroup$LayoutParams)layoutParams);
        this.presenter.setBottomNavigationMenuView(this.menuView);
        this.presenter.setId(1);
        this.menuView.setPresenter(this.presenter);
        this.menu.addMenuPresenter(this.presenter);
        this.presenter.initForMenu(this.getContext(), this.menu);
        final TintTypedArray obtainTintedStyledAttributes = ThemeEnforcement.obtainTintedStyledAttributes(context, set, R.styleable.BottomNavigationView, resourceId, R.style.Widget_Design_BottomNavigationView);
        if (obtainTintedStyledAttributes.hasValue(R.styleable.BottomNavigationView_itemIconTint)) {
            this.menuView.setIconTintList(obtainTintedStyledAttributes.getColorStateList(R.styleable.BottomNavigationView_itemIconTint));
        }
        else {
            final BottomNavigationMenuView menuView = this.menuView;
            menuView.setIconTintList(menuView.createDefaultColorStateList(16842808));
        }
        this.setItemIconSize(obtainTintedStyledAttributes.getDimensionPixelSize(R.styleable.BottomNavigationView_itemIconSize, this.getResources().getDimensionPixelSize(R.dimen.design_bottom_navigation_icon_size)));
        if (obtainTintedStyledAttributes.hasValue(R.styleable.BottomNavigationView_itemTextAppearanceInactive)) {
            this.setItemTextAppearanceInactive(obtainTintedStyledAttributes.getResourceId(R.styleable.BottomNavigationView_itemTextAppearanceInactive, 0));
        }
        if (obtainTintedStyledAttributes.hasValue(R.styleable.BottomNavigationView_itemTextAppearanceActive)) {
            this.setItemTextAppearanceActive(obtainTintedStyledAttributes.getResourceId(R.styleable.BottomNavigationView_itemTextAppearanceActive, 0));
        }
        if (obtainTintedStyledAttributes.hasValue(R.styleable.BottomNavigationView_itemTextColor)) {
            this.setItemTextColor(obtainTintedStyledAttributes.getColorStateList(R.styleable.BottomNavigationView_itemTextColor));
        }
        if (obtainTintedStyledAttributes.hasValue(R.styleable.BottomNavigationView_elevation)) {
            ViewCompat.setElevation((View)this, (float)obtainTintedStyledAttributes.getDimensionPixelSize(R.styleable.BottomNavigationView_elevation, 0));
        }
        this.setLabelVisibilityMode(obtainTintedStyledAttributes.getInteger(R.styleable.BottomNavigationView_labelVisibilityMode, -1));
        this.setItemHorizontalTranslationEnabled(obtainTintedStyledAttributes.getBoolean(R.styleable.BottomNavigationView_itemHorizontalTranslationEnabled, true));
        resourceId = obtainTintedStyledAttributes.getResourceId(R.styleable.BottomNavigationView_itemBackground, 0);
        this.menuView.setItemBackgroundRes(resourceId);
        if (obtainTintedStyledAttributes.hasValue(R.styleable.BottomNavigationView_menu)) {
            this.inflateMenu(obtainTintedStyledAttributes.getResourceId(R.styleable.BottomNavigationView_menu, 0));
        }
        obtainTintedStyledAttributes.recycle();
        this.addView((View)this.menuView, (ViewGroup$LayoutParams)layoutParams);
        if (Build$VERSION.SDK_INT < 21) {
            this.addCompatibilityTopDivider(context);
        }
        this.menu.setCallback((MenuBuilder.Callback)new MenuBuilder.Callback() {
            @Override
            public boolean onMenuItemSelected(final MenuBuilder menuBuilder, final MenuItem menuItem) {
                if (BottomNavigationView.this.reselectedListener != null && menuItem.getItemId() == BottomNavigationView.this.getSelectedItemId()) {
                    BottomNavigationView.this.reselectedListener.onNavigationItemReselected(menuItem);
                    return true;
                }
                return BottomNavigationView.this.selectedListener != null && !BottomNavigationView.this.selectedListener.onNavigationItemSelected(menuItem);
            }
            
            @Override
            public void onMenuModeChange(final MenuBuilder menuBuilder) {
            }
        });
    }
    
    private void addCompatibilityTopDivider(final Context context) {
        final View view = new View(context);
        view.setBackgroundColor(ContextCompat.getColor(context, R.color.design_bottom_navigation_shadow_color));
        view.setLayoutParams((ViewGroup$LayoutParams)new FrameLayout$LayoutParams(-1, this.getResources().getDimensionPixelSize(R.dimen.design_bottom_navigation_shadow_height)));
        this.addView(view);
    }
    
    private MenuInflater getMenuInflater() {
        if (this.menuInflater == null) {
            this.menuInflater = new SupportMenuInflater(this.getContext());
        }
        return this.menuInflater;
    }
    
    public Drawable getItemBackground() {
        return this.menuView.getItemBackground();
    }
    
    @Deprecated
    public int getItemBackgroundResource() {
        return this.menuView.getItemBackgroundRes();
    }
    
    public int getItemIconSize() {
        return this.menuView.getItemIconSize();
    }
    
    public ColorStateList getItemIconTintList() {
        return this.menuView.getIconTintList();
    }
    
    public int getItemTextAppearanceActive() {
        return this.menuView.getItemTextAppearanceActive();
    }
    
    public int getItemTextAppearanceInactive() {
        return this.menuView.getItemTextAppearanceInactive();
    }
    
    public ColorStateList getItemTextColor() {
        return this.menuView.getItemTextColor();
    }
    
    public int getLabelVisibilityMode() {
        return this.menuView.getLabelVisibilityMode();
    }
    
    public int getMaxItemCount() {
        return 5;
    }
    
    public Menu getMenu() {
        return (Menu)this.menu;
    }
    
    public int getSelectedItemId() {
        return this.menuView.getSelectedItemId();
    }
    
    public void inflateMenu(final int n) {
        this.presenter.setUpdateSuspended(true);
        this.getMenuInflater().inflate(n, (Menu)this.menu);
        this.presenter.setUpdateSuspended(false);
        this.presenter.updateMenuView(true);
    }
    
    public boolean isItemHorizontalTranslationEnabled() {
        return this.menuView.isItemHorizontalTranslationEnabled();
    }
    
    protected void onRestoreInstanceState(final Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        final SavedState savedState = (SavedState)parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.menu.restorePresenterStates(savedState.menuPresenterState);
    }
    
    protected Parcelable onSaveInstanceState() {
        final SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.menuPresenterState = new Bundle();
        this.menu.savePresenterStates(savedState.menuPresenterState);
        return (Parcelable)savedState;
    }
    
    public void setItemBackground(final Drawable itemBackground) {
        this.menuView.setItemBackground(itemBackground);
    }
    
    public void setItemBackgroundResource(final int itemBackgroundRes) {
        this.menuView.setItemBackgroundRes(itemBackgroundRes);
    }
    
    public void setItemHorizontalTranslationEnabled(final boolean itemHorizontalTranslationEnabled) {
        if (this.menuView.isItemHorizontalTranslationEnabled() != itemHorizontalTranslationEnabled) {
            this.menuView.setItemHorizontalTranslationEnabled(itemHorizontalTranslationEnabled);
            this.presenter.updateMenuView(false);
        }
    }
    
    public void setItemIconSize(final int itemIconSize) {
        this.menuView.setItemIconSize(itemIconSize);
    }
    
    public void setItemIconSizeRes(final int n) {
        this.setItemIconSize(this.getResources().getDimensionPixelSize(n));
    }
    
    public void setItemIconTintList(final ColorStateList iconTintList) {
        this.menuView.setIconTintList(iconTintList);
    }
    
    public void setItemTextAppearanceActive(final int itemTextAppearanceActive) {
        this.menuView.setItemTextAppearanceActive(itemTextAppearanceActive);
    }
    
    public void setItemTextAppearanceInactive(final int itemTextAppearanceInactive) {
        this.menuView.setItemTextAppearanceInactive(itemTextAppearanceInactive);
    }
    
    public void setItemTextColor(final ColorStateList itemTextColor) {
        this.menuView.setItemTextColor(itemTextColor);
    }
    
    public void setLabelVisibilityMode(final int labelVisibilityMode) {
        if (this.menuView.getLabelVisibilityMode() != labelVisibilityMode) {
            this.menuView.setLabelVisibilityMode(labelVisibilityMode);
            this.presenter.updateMenuView(false);
        }
    }
    
    public void setOnNavigationItemReselectedListener(final OnNavigationItemReselectedListener reselectedListener) {
        this.reselectedListener = reselectedListener;
    }
    
    public void setOnNavigationItemSelectedListener(final OnNavigationItemSelectedListener selectedListener) {
        this.selectedListener = selectedListener;
    }
    
    public void setSelectedItemId(final int n) {
        final MenuItem item = this.menu.findItem(n);
        if (item != null && !this.menu.performItemAction(item, this.presenter, 0)) {
            item.setChecked(true);
        }
    }
    
    public interface OnNavigationItemReselectedListener
    {
        void onNavigationItemReselected(final MenuItem p0);
    }
    
    public interface OnNavigationItemSelectedListener
    {
        boolean onNavigationItemSelected(final MenuItem p0);
    }
    
    static class SavedState extends AbsSavedState
    {
        public static final Parcelable$Creator<SavedState> CREATOR;
        Bundle menuPresenterState;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$ClassLoaderCreator<SavedState>() {
                public SavedState createFromParcel(final Parcel parcel) {
                    return new SavedState(parcel, null);
                }
                
                public SavedState createFromParcel(final Parcel parcel, final ClassLoader classLoader) {
                    return new SavedState(parcel, classLoader);
                }
                
                public SavedState[] newArray(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        public SavedState(final Parcel parcel, final ClassLoader classLoader) {
            super(parcel, classLoader);
            this.readFromParcel(parcel, classLoader);
        }
        
        public SavedState(final Parcelable parcelable) {
            super(parcelable);
        }
        
        private void readFromParcel(final Parcel parcel, final ClassLoader classLoader) {
            this.menuPresenterState = parcel.readBundle(classLoader);
        }
        
        @Override
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeBundle(this.menuPresenterState);
        }
    }
}
