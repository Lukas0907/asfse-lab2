// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.circularreveal.coordinatorlayout;

import android.graphics.drawable.Drawable;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.content.Context;
import com.google.android.material.circularreveal.CircularRevealHelper;
import com.google.android.material.circularreveal.CircularRevealWidget;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

public class CircularRevealCoordinatorLayout extends CoordinatorLayout implements CircularRevealWidget
{
    private final CircularRevealHelper helper;
    
    public CircularRevealCoordinatorLayout(final Context context) {
        this(context, null);
    }
    
    public CircularRevealCoordinatorLayout(final Context context, final AttributeSet set) {
        super(context, set);
        this.helper = new CircularRevealHelper((CircularRevealHelper.Delegate)this);
    }
    
    public void actualDraw(final Canvas canvas) {
        super.draw(canvas);
    }
    
    public boolean actualIsOpaque() {
        return super.isOpaque();
    }
    
    @Override
    public void buildCircularRevealCache() {
        this.helper.buildCircularRevealCache();
    }
    
    @Override
    public void destroyCircularRevealCache() {
        this.helper.destroyCircularRevealCache();
    }
    
    @Override
    public void draw(final Canvas canvas) {
        final CircularRevealHelper helper = this.helper;
        if (helper != null) {
            helper.draw(canvas);
            return;
        }
        super.draw(canvas);
    }
    
    @Override
    public Drawable getCircularRevealOverlayDrawable() {
        return this.helper.getCircularRevealOverlayDrawable();
    }
    
    @Override
    public int getCircularRevealScrimColor() {
        return this.helper.getCircularRevealScrimColor();
    }
    
    @Override
    public RevealInfo getRevealInfo() {
        return this.helper.getRevealInfo();
    }
    
    @Override
    public boolean isOpaque() {
        final CircularRevealHelper helper = this.helper;
        if (helper != null) {
            return helper.isOpaque();
        }
        return super.isOpaque();
    }
    
    @Override
    public void setCircularRevealOverlayDrawable(final Drawable circularRevealOverlayDrawable) {
        this.helper.setCircularRevealOverlayDrawable(circularRevealOverlayDrawable);
    }
    
    @Override
    public void setCircularRevealScrimColor(final int circularRevealScrimColor) {
        this.helper.setCircularRevealScrimColor(circularRevealScrimColor);
    }
    
    @Override
    public void setRevealInfo(final RevealInfo revealInfo) {
        this.helper.setRevealInfo(revealInfo);
    }
}
