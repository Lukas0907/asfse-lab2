// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.circularreveal;

import android.graphics.drawable.Drawable;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.LinearLayout;

public class CircularRevealLinearLayout extends LinearLayout implements CircularRevealWidget
{
    private final CircularRevealHelper helper;
    
    public CircularRevealLinearLayout(final Context context) {
        this(context, null);
    }
    
    public CircularRevealLinearLayout(final Context context, final AttributeSet set) {
        super(context, set);
        this.helper = new CircularRevealHelper((CircularRevealHelper.Delegate)this);
    }
    
    public void actualDraw(final Canvas canvas) {
        super.draw(canvas);
    }
    
    public boolean actualIsOpaque() {
        return super.isOpaque();
    }
    
    public void buildCircularRevealCache() {
        this.helper.buildCircularRevealCache();
    }
    
    public void destroyCircularRevealCache() {
        this.helper.destroyCircularRevealCache();
    }
    
    public void draw(final Canvas canvas) {
        final CircularRevealHelper helper = this.helper;
        if (helper != null) {
            helper.draw(canvas);
            return;
        }
        super.draw(canvas);
    }
    
    public Drawable getCircularRevealOverlayDrawable() {
        return this.helper.getCircularRevealOverlayDrawable();
    }
    
    public int getCircularRevealScrimColor() {
        return this.helper.getCircularRevealScrimColor();
    }
    
    public RevealInfo getRevealInfo() {
        return this.helper.getRevealInfo();
    }
    
    public boolean isOpaque() {
        final CircularRevealHelper helper = this.helper;
        if (helper != null) {
            return helper.isOpaque();
        }
        return super.isOpaque();
    }
    
    public void setCircularRevealOverlayDrawable(final Drawable circularRevealOverlayDrawable) {
        this.helper.setCircularRevealOverlayDrawable(circularRevealOverlayDrawable);
    }
    
    public void setCircularRevealScrimColor(final int circularRevealScrimColor) {
        this.helper.setCircularRevealScrimColor(circularRevealScrimColor);
    }
    
    public void setRevealInfo(final RevealInfo revealInfo) {
        this.helper.setRevealInfo(revealInfo);
    }
}
