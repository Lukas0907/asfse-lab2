// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.transformation;

import android.util.AttributeSet;
import android.content.Context;
import com.google.android.material.circularreveal.CircularRevealFrameLayout;

public class TransformationChildLayout extends CircularRevealFrameLayout
{
    public TransformationChildLayout(final Context context) {
        this(context, null);
    }
    
    public TransformationChildLayout(final Context context, final AttributeSet set) {
        super(context, set);
    }
}
