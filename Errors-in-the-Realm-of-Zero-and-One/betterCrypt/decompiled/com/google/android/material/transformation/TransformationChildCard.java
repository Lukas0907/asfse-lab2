// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.transformation;

import android.util.AttributeSet;
import android.content.Context;
import com.google.android.material.circularreveal.cardview.CircularRevealCardView;

public class TransformationChildCard extends CircularRevealCardView
{
    public TransformationChildCard(final Context context) {
        this(context, null);
    }
    
    public TransformationChildCard(final Context context, final AttributeSet set) {
        super(context, set);
    }
}
