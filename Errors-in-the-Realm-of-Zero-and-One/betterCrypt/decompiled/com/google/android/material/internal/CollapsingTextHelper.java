// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.internal;

import androidx.core.math.MathUtils;
import androidx.appcompat.widget.TintTypedArray;
import androidx.appcompat.R;
import android.content.res.TypedArray;
import com.google.android.material.animation.AnimationUtils;
import android.graphics.Canvas;
import android.graphics.Bitmap$Config;
import android.text.TextUtils;
import android.text.TextUtils$TruncateAt;
import androidx.core.text.TextDirectionHeuristicCompat;
import androidx.core.text.TextDirectionHeuristicsCompat;
import androidx.core.view.ViewCompat;
import android.graphics.Color;
import android.os.Build$VERSION;
import android.view.View;
import android.text.TextPaint;
import android.animation.TimeInterpolator;
import android.graphics.Bitmap;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.content.res.ColorStateList;
import android.graphics.Rect;
import android.graphics.Paint;

public final class CollapsingTextHelper
{
    private static final boolean DEBUG_DRAW = false;
    private static final Paint DEBUG_DRAW_PAINT;
    private static final boolean USE_SCALING_TEXTURE;
    private boolean boundsChanged;
    private final Rect collapsedBounds;
    private float collapsedDrawX;
    private float collapsedDrawY;
    private int collapsedShadowColor;
    private float collapsedShadowDx;
    private float collapsedShadowDy;
    private float collapsedShadowRadius;
    private ColorStateList collapsedTextColor;
    private int collapsedTextGravity;
    private float collapsedTextSize;
    private Typeface collapsedTypeface;
    private final RectF currentBounds;
    private float currentDrawX;
    private float currentDrawY;
    private float currentTextSize;
    private Typeface currentTypeface;
    private boolean drawTitle;
    private final Rect expandedBounds;
    private float expandedDrawX;
    private float expandedDrawY;
    private float expandedFraction;
    private int expandedShadowColor;
    private float expandedShadowDx;
    private float expandedShadowDy;
    private float expandedShadowRadius;
    private ColorStateList expandedTextColor;
    private int expandedTextGravity;
    private float expandedTextSize;
    private Bitmap expandedTitleTexture;
    private Typeface expandedTypeface;
    private boolean isRtl;
    private TimeInterpolator positionInterpolator;
    private float scale;
    private int[] state;
    private CharSequence text;
    private final TextPaint textPaint;
    private TimeInterpolator textSizeInterpolator;
    private CharSequence textToDraw;
    private float textureAscent;
    private float textureDescent;
    private Paint texturePaint;
    private final TextPaint tmpPaint;
    private boolean useTexture;
    private final View view;
    
    static {
        USE_SCALING_TEXTURE = (Build$VERSION.SDK_INT < 18);
        DEBUG_DRAW_PAINT = null;
        final Paint debug_DRAW_PAINT = CollapsingTextHelper.DEBUG_DRAW_PAINT;
        if (debug_DRAW_PAINT != null) {
            debug_DRAW_PAINT.setAntiAlias(true);
            CollapsingTextHelper.DEBUG_DRAW_PAINT.setColor(-65281);
        }
    }
    
    public CollapsingTextHelper(final View view) {
        this.expandedTextGravity = 16;
        this.collapsedTextGravity = 16;
        this.expandedTextSize = 15.0f;
        this.collapsedTextSize = 15.0f;
        this.view = view;
        this.textPaint = new TextPaint(129);
        this.tmpPaint = new TextPaint((Paint)this.textPaint);
        this.collapsedBounds = new Rect();
        this.expandedBounds = new Rect();
        this.currentBounds = new RectF();
    }
    
    private static int blendColors(final int n, final int n2, final float n3) {
        final float n4 = 1.0f - n3;
        return Color.argb((int)(Color.alpha(n) * n4 + Color.alpha(n2) * n3), (int)(Color.red(n) * n4 + Color.red(n2) * n3), (int)(Color.green(n) * n4 + Color.green(n2) * n3), (int)(Color.blue(n) * n4 + Color.blue(n2) * n3));
    }
    
    private void calculateBaseOffsets() {
        throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:780)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e1expr(TypeTransformer.java:496)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:713)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.enexpr(TypeTransformer.java:698)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:719)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s2stmt(TypeTransformer.java:820)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:843)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
    }
    
    private void calculateCurrentOffsets() {
        this.calculateOffsets(this.expandedFraction);
    }
    
    private boolean calculateIsRtl(final CharSequence charSequence) {
        final int layoutDirection = ViewCompat.getLayoutDirection(this.view);
        boolean b = true;
        if (layoutDirection != 1) {
            b = false;
        }
        TextDirectionHeuristicCompat textDirectionHeuristicCompat;
        if (b) {
            textDirectionHeuristicCompat = TextDirectionHeuristicsCompat.FIRSTSTRONG_RTL;
        }
        else {
            textDirectionHeuristicCompat = TextDirectionHeuristicsCompat.FIRSTSTRONG_LTR;
        }
        return textDirectionHeuristicCompat.isRtl(charSequence, 0, charSequence.length());
    }
    
    private void calculateOffsets(final float n) {
        this.interpolateBounds(n);
        this.currentDrawX = lerp(this.expandedDrawX, this.collapsedDrawX, n, this.positionInterpolator);
        this.currentDrawY = lerp(this.expandedDrawY, this.collapsedDrawY, n, this.positionInterpolator);
        this.setInterpolatedTextSize(lerp(this.expandedTextSize, this.collapsedTextSize, n, this.textSizeInterpolator));
        if (this.collapsedTextColor != this.expandedTextColor) {
            this.textPaint.setColor(blendColors(this.getCurrentExpandedTextColor(), this.getCurrentCollapsedTextColor(), n));
        }
        else {
            this.textPaint.setColor(this.getCurrentCollapsedTextColor());
        }
        this.textPaint.setShadowLayer(lerp(this.expandedShadowRadius, this.collapsedShadowRadius, n, null), lerp(this.expandedShadowDx, this.collapsedShadowDx, n, null), lerp(this.expandedShadowDy, this.collapsedShadowDy, n, null), blendColors(this.expandedShadowColor, this.collapsedShadowColor, n));
        ViewCompat.postInvalidateOnAnimation(this.view);
    }
    
    private void calculateUsingTextSize(float min) {
        if (this.text == null) {
            return;
        }
        final float n = (float)this.collapsedBounds.width();
        final float b = (float)this.expandedBounds.width();
        final boolean close = isClose(min, this.collapsedTextSize);
        boolean linearText = true;
        float currentTextSize;
        int n2;
        if (close) {
            currentTextSize = this.collapsedTextSize;
            this.scale = 1.0f;
            final Typeface currentTypeface = this.currentTypeface;
            final Typeface collapsedTypeface = this.collapsedTypeface;
            if (currentTypeface != collapsedTypeface) {
                this.currentTypeface = collapsedTypeface;
                n2 = 1;
            }
            else {
                n2 = 0;
            }
            min = n;
        }
        else {
            currentTextSize = this.expandedTextSize;
            final Typeface currentTypeface2 = this.currentTypeface;
            final Typeface expandedTypeface = this.expandedTypeface;
            if (currentTypeface2 != expandedTypeface) {
                this.currentTypeface = expandedTypeface;
                n2 = 1;
            }
            else {
                n2 = 0;
            }
            if (isClose(min, this.expandedTextSize)) {
                this.scale = 1.0f;
            }
            else {
                this.scale = min / this.expandedTextSize;
            }
            min = this.collapsedTextSize / this.expandedTextSize;
            if (b * min > n) {
                min = Math.min(n / min, b);
            }
            else {
                min = b;
            }
        }
        int n3 = n2;
        if (min > 0.0f) {
            final boolean b2 = this.currentTextSize != currentTextSize || this.boundsChanged || n2 != 0;
            this.currentTextSize = currentTextSize;
            this.boundsChanged = false;
            n3 = (b2 ? 1 : 0);
        }
        if (this.textToDraw == null || n3 != 0) {
            this.textPaint.setTextSize(this.currentTextSize);
            this.textPaint.setTypeface(this.currentTypeface);
            final TextPaint textPaint = this.textPaint;
            if (this.scale == 1.0f) {
                linearText = false;
            }
            textPaint.setLinearText(linearText);
            final CharSequence ellipsize = TextUtils.ellipsize(this.text, this.textPaint, min, TextUtils$TruncateAt.END);
            if (!TextUtils.equals(ellipsize, this.textToDraw)) {
                this.textToDraw = ellipsize;
                this.isRtl = this.calculateIsRtl(this.textToDraw);
            }
        }
    }
    
    private void clearTexture() {
        final Bitmap expandedTitleTexture = this.expandedTitleTexture;
        if (expandedTitleTexture != null) {
            expandedTitleTexture.recycle();
            this.expandedTitleTexture = null;
        }
    }
    
    private void ensureExpandedTexture() {
        if (this.expandedTitleTexture == null && !this.expandedBounds.isEmpty()) {
            if (TextUtils.isEmpty(this.textToDraw)) {
                return;
            }
            this.calculateOffsets(0.0f);
            this.textureAscent = this.textPaint.ascent();
            this.textureDescent = this.textPaint.descent();
            final TextPaint textPaint = this.textPaint;
            final CharSequence textToDraw = this.textToDraw;
            final int round = Math.round(textPaint.measureText(textToDraw, 0, textToDraw.length()));
            final int round2 = Math.round(this.textureDescent - this.textureAscent);
            if (round > 0) {
                if (round2 <= 0) {
                    return;
                }
                this.expandedTitleTexture = Bitmap.createBitmap(round, round2, Bitmap$Config.ARGB_8888);
                final Canvas canvas = new Canvas(this.expandedTitleTexture);
                final CharSequence textToDraw2 = this.textToDraw;
                canvas.drawText(textToDraw2, 0, textToDraw2.length(), 0.0f, round2 - this.textPaint.descent(), (Paint)this.textPaint);
                if (this.texturePaint == null) {
                    this.texturePaint = new Paint(3);
                }
            }
        }
    }
    
    private int getCurrentExpandedTextColor() {
        final int[] state = this.state;
        if (state != null) {
            return this.expandedTextColor.getColorForState(state, 0);
        }
        return this.expandedTextColor.getDefaultColor();
    }
    
    private void getTextPaintCollapsed(final TextPaint textPaint) {
        textPaint.setTextSize(this.collapsedTextSize);
        textPaint.setTypeface(this.collapsedTypeface);
    }
    
    private void interpolateBounds(final float n) {
        this.currentBounds.left = lerp((float)this.expandedBounds.left, (float)this.collapsedBounds.left, n, this.positionInterpolator);
        this.currentBounds.top = lerp(this.expandedDrawY, this.collapsedDrawY, n, this.positionInterpolator);
        this.currentBounds.right = lerp((float)this.expandedBounds.right, (float)this.collapsedBounds.right, n, this.positionInterpolator);
        this.currentBounds.bottom = lerp((float)this.expandedBounds.bottom, (float)this.collapsedBounds.bottom, n, this.positionInterpolator);
    }
    
    private static boolean isClose(final float n, final float n2) {
        return Math.abs(n - n2) < 0.001f;
    }
    
    private static float lerp(final float n, final float n2, final float n3, final TimeInterpolator timeInterpolator) {
        float interpolation = n3;
        if (timeInterpolator != null) {
            interpolation = timeInterpolator.getInterpolation(n3);
        }
        return AnimationUtils.lerp(n, n2, interpolation);
    }
    
    private Typeface readFontFamilyTypeface(final int n) {
        final TypedArray obtainStyledAttributes = this.view.getContext().obtainStyledAttributes(n, new int[] { 16843692 });
        try {
            final String string = obtainStyledAttributes.getString(0);
            if (string != null) {
                return Typeface.create(string, 0);
            }
            return null;
        }
        finally {
            obtainStyledAttributes.recycle();
        }
    }
    
    private static boolean rectEquals(final Rect rect, final int n, final int n2, final int n3, final int n4) {
        return rect.left == n && rect.top == n2 && rect.right == n3 && rect.bottom == n4;
    }
    
    private void setInterpolatedTextSize(final float n) {
        this.calculateUsingTextSize(n);
        this.useTexture = (CollapsingTextHelper.USE_SCALING_TEXTURE && this.scale != 1.0f);
        if (this.useTexture) {
            this.ensureExpandedTexture();
        }
        ViewCompat.postInvalidateOnAnimation(this.view);
    }
    
    public float calculateCollapsedTextWidth() {
        if (this.text == null) {
            return 0.0f;
        }
        this.getTextPaintCollapsed(this.tmpPaint);
        final TextPaint tmpPaint = this.tmpPaint;
        final CharSequence text = this.text;
        return tmpPaint.measureText(text, 0, text.length());
    }
    
    public void draw(final Canvas canvas) {
        final int save = canvas.save();
        if (this.textToDraw != null && this.drawTitle) {
            final float currentDrawX = this.currentDrawX;
            final float currentDrawY = this.currentDrawY;
            final boolean b = this.useTexture && this.expandedTitleTexture != null;
            float n;
            if (b) {
                n = this.textureAscent * this.scale;
                final float textureDescent = this.textureDescent;
            }
            else {
                n = this.textPaint.ascent() * this.scale;
                this.textPaint.descent();
                final float scale = this.scale;
            }
            float n2 = currentDrawY;
            if (b) {
                n2 = currentDrawY + n;
            }
            final float scale2 = this.scale;
            if (scale2 != 1.0f) {
                canvas.scale(scale2, scale2, currentDrawX, n2);
            }
            if (b) {
                canvas.drawBitmap(this.expandedTitleTexture, currentDrawX, n2, this.texturePaint);
            }
            else {
                final CharSequence textToDraw = this.textToDraw;
                canvas.drawText(textToDraw, 0, textToDraw.length(), currentDrawX, n2, (Paint)this.textPaint);
            }
        }
        canvas.restoreToCount(save);
    }
    
    public void getCollapsedTextActualBounds(final RectF rectF) {
        final boolean calculateIsRtl = this.calculateIsRtl(this.text);
        final Rect collapsedBounds = this.collapsedBounds;
        float left;
        if (!calculateIsRtl) {
            left = (float)collapsedBounds.left;
        }
        else {
            left = collapsedBounds.right - this.calculateCollapsedTextWidth();
        }
        rectF.left = left;
        rectF.top = (float)this.collapsedBounds.top;
        float right;
        if (!calculateIsRtl) {
            right = rectF.left + this.calculateCollapsedTextWidth();
        }
        else {
            right = (float)this.collapsedBounds.right;
        }
        rectF.right = right;
        rectF.bottom = this.collapsedBounds.top + this.getCollapsedTextHeight();
    }
    
    public ColorStateList getCollapsedTextColor() {
        return this.collapsedTextColor;
    }
    
    public int getCollapsedTextGravity() {
        return this.collapsedTextGravity;
    }
    
    public float getCollapsedTextHeight() {
        this.getTextPaintCollapsed(this.tmpPaint);
        return -this.tmpPaint.ascent();
    }
    
    public float getCollapsedTextSize() {
        return this.collapsedTextSize;
    }
    
    public Typeface getCollapsedTypeface() {
        final Typeface collapsedTypeface = this.collapsedTypeface;
        if (collapsedTypeface != null) {
            return collapsedTypeface;
        }
        return Typeface.DEFAULT;
    }
    
    public int getCurrentCollapsedTextColor() {
        final int[] state = this.state;
        if (state != null) {
            return this.collapsedTextColor.getColorForState(state, 0);
        }
        return this.collapsedTextColor.getDefaultColor();
    }
    
    public ColorStateList getExpandedTextColor() {
        return this.expandedTextColor;
    }
    
    public int getExpandedTextGravity() {
        return this.expandedTextGravity;
    }
    
    public float getExpandedTextSize() {
        return this.expandedTextSize;
    }
    
    public Typeface getExpandedTypeface() {
        final Typeface expandedTypeface = this.expandedTypeface;
        if (expandedTypeface != null) {
            return expandedTypeface;
        }
        return Typeface.DEFAULT;
    }
    
    public float getExpansionFraction() {
        return this.expandedFraction;
    }
    
    public CharSequence getText() {
        return this.text;
    }
    
    public final boolean isStateful() {
        final ColorStateList collapsedTextColor = this.collapsedTextColor;
        if (collapsedTextColor == null || !collapsedTextColor.isStateful()) {
            final ColorStateList expandedTextColor = this.expandedTextColor;
            if (expandedTextColor == null || !expandedTextColor.isStateful()) {
                return false;
            }
        }
        return true;
    }
    
    void onBoundsChanged() {
        this.drawTitle = (this.collapsedBounds.width() > 0 && this.collapsedBounds.height() > 0 && this.expandedBounds.width() > 0 && this.expandedBounds.height() > 0);
    }
    
    public void recalculate() {
        if (this.view.getHeight() > 0 && this.view.getWidth() > 0) {
            this.calculateBaseOffsets();
            this.calculateCurrentOffsets();
        }
    }
    
    public void setCollapsedBounds(final int n, final int n2, final int n3, final int n4) {
        if (!rectEquals(this.collapsedBounds, n, n2, n3, n4)) {
            this.collapsedBounds.set(n, n2, n3, n4);
            this.boundsChanged = true;
            this.onBoundsChanged();
        }
    }
    
    public void setCollapsedTextAppearance(final int n) {
        final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(this.view.getContext(), n, R.styleable.TextAppearance);
        if (obtainStyledAttributes.hasValue(R.styleable.TextAppearance_android_textColor)) {
            this.collapsedTextColor = obtainStyledAttributes.getColorStateList(R.styleable.TextAppearance_android_textColor);
        }
        if (obtainStyledAttributes.hasValue(R.styleable.TextAppearance_android_textSize)) {
            this.collapsedTextSize = (float)obtainStyledAttributes.getDimensionPixelSize(R.styleable.TextAppearance_android_textSize, (int)this.collapsedTextSize);
        }
        this.collapsedShadowColor = obtainStyledAttributes.getInt(R.styleable.TextAppearance_android_shadowColor, 0);
        this.collapsedShadowDx = obtainStyledAttributes.getFloat(R.styleable.TextAppearance_android_shadowDx, 0.0f);
        this.collapsedShadowDy = obtainStyledAttributes.getFloat(R.styleable.TextAppearance_android_shadowDy, 0.0f);
        this.collapsedShadowRadius = obtainStyledAttributes.getFloat(R.styleable.TextAppearance_android_shadowRadius, 0.0f);
        obtainStyledAttributes.recycle();
        if (Build$VERSION.SDK_INT >= 16) {
            this.collapsedTypeface = this.readFontFamilyTypeface(n);
        }
        this.recalculate();
    }
    
    public void setCollapsedTextColor(final ColorStateList collapsedTextColor) {
        if (this.collapsedTextColor != collapsedTextColor) {
            this.collapsedTextColor = collapsedTextColor;
            this.recalculate();
        }
    }
    
    public void setCollapsedTextGravity(final int collapsedTextGravity) {
        if (this.collapsedTextGravity != collapsedTextGravity) {
            this.collapsedTextGravity = collapsedTextGravity;
            this.recalculate();
        }
    }
    
    public void setCollapsedTextSize(final float collapsedTextSize) {
        if (this.collapsedTextSize != collapsedTextSize) {
            this.collapsedTextSize = collapsedTextSize;
            this.recalculate();
        }
    }
    
    public void setCollapsedTypeface(final Typeface collapsedTypeface) {
        if (this.collapsedTypeface != collapsedTypeface) {
            this.collapsedTypeface = collapsedTypeface;
            this.recalculate();
        }
    }
    
    public void setExpandedBounds(final int n, final int n2, final int n3, final int n4) {
        if (!rectEquals(this.expandedBounds, n, n2, n3, n4)) {
            this.expandedBounds.set(n, n2, n3, n4);
            this.boundsChanged = true;
            this.onBoundsChanged();
        }
    }
    
    public void setExpandedTextAppearance(final int n) {
        final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(this.view.getContext(), n, R.styleable.TextAppearance);
        if (obtainStyledAttributes.hasValue(R.styleable.TextAppearance_android_textColor)) {
            this.expandedTextColor = obtainStyledAttributes.getColorStateList(R.styleable.TextAppearance_android_textColor);
        }
        if (obtainStyledAttributes.hasValue(R.styleable.TextAppearance_android_textSize)) {
            this.expandedTextSize = (float)obtainStyledAttributes.getDimensionPixelSize(R.styleable.TextAppearance_android_textSize, (int)this.expandedTextSize);
        }
        this.expandedShadowColor = obtainStyledAttributes.getInt(R.styleable.TextAppearance_android_shadowColor, 0);
        this.expandedShadowDx = obtainStyledAttributes.getFloat(R.styleable.TextAppearance_android_shadowDx, 0.0f);
        this.expandedShadowDy = obtainStyledAttributes.getFloat(R.styleable.TextAppearance_android_shadowDy, 0.0f);
        this.expandedShadowRadius = obtainStyledAttributes.getFloat(R.styleable.TextAppearance_android_shadowRadius, 0.0f);
        obtainStyledAttributes.recycle();
        if (Build$VERSION.SDK_INT >= 16) {
            this.expandedTypeface = this.readFontFamilyTypeface(n);
        }
        this.recalculate();
    }
    
    public void setExpandedTextColor(final ColorStateList expandedTextColor) {
        if (this.expandedTextColor != expandedTextColor) {
            this.expandedTextColor = expandedTextColor;
            this.recalculate();
        }
    }
    
    public void setExpandedTextGravity(final int expandedTextGravity) {
        if (this.expandedTextGravity != expandedTextGravity) {
            this.expandedTextGravity = expandedTextGravity;
            this.recalculate();
        }
    }
    
    public void setExpandedTextSize(final float expandedTextSize) {
        if (this.expandedTextSize != expandedTextSize) {
            this.expandedTextSize = expandedTextSize;
            this.recalculate();
        }
    }
    
    public void setExpandedTypeface(final Typeface expandedTypeface) {
        if (this.expandedTypeface != expandedTypeface) {
            this.expandedTypeface = expandedTypeface;
            this.recalculate();
        }
    }
    
    public void setExpansionFraction(float clamp) {
        clamp = MathUtils.clamp(clamp, 0.0f, 1.0f);
        if (clamp != this.expandedFraction) {
            this.expandedFraction = clamp;
            this.calculateCurrentOffsets();
        }
    }
    
    public void setPositionInterpolator(final TimeInterpolator positionInterpolator) {
        this.positionInterpolator = positionInterpolator;
        this.recalculate();
    }
    
    public final boolean setState(final int[] state) {
        this.state = state;
        if (this.isStateful()) {
            this.recalculate();
            return true;
        }
        return false;
    }
    
    public void setText(final CharSequence text) {
        if (text == null || !text.equals(this.text)) {
            this.text = text;
            this.textToDraw = null;
            this.clearTexture();
            this.recalculate();
        }
    }
    
    public void setTextSizeInterpolator(final TimeInterpolator textSizeInterpolator) {
        this.textSizeInterpolator = textSizeInterpolator;
        this.recalculate();
    }
    
    public void setTypefaces(final Typeface typeface) {
        this.expandedTypeface = typeface;
        this.collapsedTypeface = typeface;
        this.recalculate();
    }
}
