// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.internal;

import android.view.View;
import java.util.ArrayList;
import java.util.List;

public class FlexLine
{
    int bottom;
    int crossSize;
    int dividerLengthInMainSize;
    int firstIndex;
    int goneItemCount;
    List<Integer> indicesAlignSelfStretch;
    int itemCount;
    int lastIndex;
    int left;
    int mainSize;
    int maxBaseline;
    float motalFlexGrow;
    int right;
    int sumCrossSizeBefore;
    int top;
    float totalFlexShrink;
    
    FlexLine() {
        this.left = Integer.MAX_VALUE;
        this.top = Integer.MAX_VALUE;
        this.right = Integer.MIN_VALUE;
        this.bottom = Integer.MIN_VALUE;
        this.indicesAlignSelfStretch = new ArrayList<Integer>();
    }
    
    public int getCrossSize() {
        return this.crossSize;
    }
    
    public int getFirstIndex() {
        return this.firstIndex;
    }
    
    public int getItemCount() {
        return this.itemCount;
    }
    
    public int getItemCountNotGone() {
        return this.itemCount - this.goneItemCount;
    }
    
    public int getMainSize() {
        return this.mainSize;
    }
    
    public float getTotalFlexGrow() {
        return this.motalFlexGrow;
    }
    
    public float getTotalFlexShrink() {
        return this.totalFlexShrink;
    }
    
    void updatePositionFromView(final View view, final int n, final int n2, final int n3, final int n4) {
        final FlexItem flexItem = (FlexItem)view.getLayoutParams();
        this.left = Math.min(this.left, view.getLeft() - flexItem.getMarginLeft() - n);
        this.top = Math.min(this.top, view.getTop() - flexItem.getMarginTop() - n2);
        this.right = Math.max(this.right, view.getRight() + flexItem.getMarginRight() + n3);
        this.bottom = Math.max(this.bottom, view.getBottom() + flexItem.getMarginBottom() + n4);
    }
}
