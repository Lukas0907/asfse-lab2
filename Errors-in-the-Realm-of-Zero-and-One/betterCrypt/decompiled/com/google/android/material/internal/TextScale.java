// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.internal;

import java.util.Map;
import android.animation.ValueAnimator$AnimatorUpdateListener;
import android.animation.ValueAnimator;
import android.animation.Animator;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.transition.TransitionValues;
import androidx.transition.Transition;

public class TextScale extends Transition
{
    private static final String PROPNAME_SCALE = "android:textscale:scale";
    
    private void captureValues(final TransitionValues transitionValues) {
        if (transitionValues.view instanceof TextView) {
            transitionValues.values.put("android:textscale:scale", ((TextView)transitionValues.view).getScaleX());
        }
    }
    
    @Override
    public void captureEndValues(final TransitionValues transitionValues) {
        this.captureValues(transitionValues);
    }
    
    @Override
    public void captureStartValues(final TransitionValues transitionValues) {
        this.captureValues(transitionValues);
    }
    
    @Override
    public Animator createAnimator(final ViewGroup viewGroup, final TransitionValues transitionValues, final TransitionValues transitionValues2) {
        ValueAnimator ofFloat;
        final ValueAnimator valueAnimator = ofFloat = null;
        if (transitionValues != null) {
            ofFloat = valueAnimator;
            if (transitionValues2 != null) {
                ofFloat = valueAnimator;
                if (transitionValues.view instanceof TextView) {
                    if (!(transitionValues2.view instanceof TextView)) {
                        return null;
                    }
                    final TextView textView = (TextView)transitionValues2.view;
                    final Map<String, Object> values = transitionValues.values;
                    final Map<String, Object> values2 = transitionValues2.values;
                    final Object value = values.get("android:textscale:scale");
                    float floatValue = 1.0f;
                    float floatValue2;
                    if (value != null) {
                        floatValue2 = values.get("android:textscale:scale");
                    }
                    else {
                        floatValue2 = 1.0f;
                    }
                    if (values2.get("android:textscale:scale") != null) {
                        floatValue = values2.get("android:textscale:scale");
                    }
                    if (floatValue2 == floatValue) {
                        return null;
                    }
                    ofFloat = ValueAnimator.ofFloat(new float[] { floatValue2, floatValue });
                    ofFloat.addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener() {
                        public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                            final float floatValue = (float)valueAnimator.getAnimatedValue();
                            textView.setScaleX(floatValue);
                            textView.setScaleY(floatValue);
                        }
                    });
                }
            }
        }
        return (Animator)ofFloat;
    }
}
