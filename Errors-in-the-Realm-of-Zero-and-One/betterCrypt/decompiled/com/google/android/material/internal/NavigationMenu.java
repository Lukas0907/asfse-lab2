// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.internal;

import androidx.appcompat.view.menu.SubMenuBuilder;
import androidx.appcompat.view.menu.MenuItemImpl;
import android.view.SubMenu;
import android.content.Context;
import androidx.appcompat.view.menu.MenuBuilder;

public class NavigationMenu extends MenuBuilder
{
    public NavigationMenu(final Context context) {
        super(context);
    }
    
    @Override
    public SubMenu addSubMenu(final int n, final int n2, final int n3, final CharSequence charSequence) {
        final MenuItemImpl menuItemImpl = (MenuItemImpl)this.addInternal(n, n2, n3, charSequence);
        final NavigationSubMenu subMenu = new NavigationSubMenu(this.getContext(), this, menuItemImpl);
        menuItemImpl.setSubMenu(subMenu);
        return (SubMenu)subMenu;
    }
}
