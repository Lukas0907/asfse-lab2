// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.internal;

import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuItemImpl;
import android.content.Context;
import androidx.appcompat.view.menu.SubMenuBuilder;

public class NavigationSubMenu extends SubMenuBuilder
{
    public NavigationSubMenu(final Context context, final NavigationMenu navigationMenu, final MenuItemImpl menuItemImpl) {
        super(context, navigationMenu, menuItemImpl);
    }
    
    @Override
    public void onItemsChanged(final boolean b) {
        super.onItemsChanged(b);
        ((MenuBuilder)this.getParentMenu()).onItemsChanged(b);
    }
}
