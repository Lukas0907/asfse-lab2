// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.internal;

import android.graphics.drawable.Drawable$ConstantState;
import android.widget.TextView;
import androidx.core.widget.TextViewCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.appcompat.widget.TooltipCompat;
import android.view.ViewStub;
import android.graphics.drawable.ColorDrawable;
import android.util.TypedValue;
import android.graphics.drawable.StateListDrawable;
import android.view.ViewGroup$LayoutParams;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.view.ViewCompat;
import android.view.ViewGroup;
import com.google.android.material.R;
import android.view.LayoutInflater;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.CheckedTextView;
import androidx.appcompat.view.menu.MenuItemImpl;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.widget.FrameLayout;
import androidx.core.view.AccessibilityDelegateCompat;
import androidx.appcompat.view.menu.MenuView;

public class NavigationMenuItemView extends ForegroundLinearLayout implements ItemView
{
    private static final int[] CHECKED_STATE_SET;
    private final AccessibilityDelegateCompat accessibilityDelegate;
    private FrameLayout actionArea;
    boolean checkable;
    private Drawable emptyDrawable;
    private boolean hasIconTintList;
    private final int iconSize;
    private ColorStateList iconTintList;
    private MenuItemImpl itemData;
    private boolean needsEmptyIcon;
    private final CheckedTextView textView;
    
    static {
        CHECKED_STATE_SET = new int[] { 16842912 };
    }
    
    public NavigationMenuItemView(final Context context) {
        this(context, null);
    }
    
    public NavigationMenuItemView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public NavigationMenuItemView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.accessibilityDelegate = new AccessibilityDelegateCompat() {
            @Override
            public void onInitializeAccessibilityNodeInfo(final View view, final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
                super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat);
                accessibilityNodeInfoCompat.setCheckable(NavigationMenuItemView.this.checkable);
            }
        };
        this.setOrientation(0);
        LayoutInflater.from(context).inflate(R.layout.design_navigation_menu_item, (ViewGroup)this, true);
        this.iconSize = context.getResources().getDimensionPixelSize(R.dimen.design_navigation_icon_size);
        (this.textView = (CheckedTextView)this.findViewById(R.id.design_menu_item_text)).setDuplicateParentStateEnabled(true);
        ViewCompat.setAccessibilityDelegate((View)this.textView, this.accessibilityDelegate);
    }
    
    private void adjustAppearance() {
        if (this.shouldExpandActionArea()) {
            this.textView.setVisibility(8);
            final FrameLayout actionArea = this.actionArea;
            if (actionArea != null) {
                final LayoutParams layoutParams = (LayoutParams)actionArea.getLayoutParams();
                layoutParams.width = -1;
                this.actionArea.setLayoutParams((ViewGroup$LayoutParams)layoutParams);
            }
        }
        else {
            this.textView.setVisibility(0);
            final FrameLayout actionArea2 = this.actionArea;
            if (actionArea2 != null) {
                final LayoutParams layoutParams2 = (LayoutParams)actionArea2.getLayoutParams();
                layoutParams2.width = -2;
                this.actionArea.setLayoutParams((ViewGroup$LayoutParams)layoutParams2);
            }
        }
    }
    
    private StateListDrawable createDefaultBackground() {
        final TypedValue typedValue = new TypedValue();
        if (this.getContext().getTheme().resolveAttribute(androidx.appcompat.R.attr.colorControlHighlight, typedValue, true)) {
            final StateListDrawable stateListDrawable = new StateListDrawable();
            stateListDrawable.addState(NavigationMenuItemView.CHECKED_STATE_SET, (Drawable)new ColorDrawable(typedValue.data));
            stateListDrawable.addState(NavigationMenuItemView.EMPTY_STATE_SET, (Drawable)new ColorDrawable(0));
            return stateListDrawable;
        }
        return null;
    }
    
    private void setActionView(final View view) {
        if (view != null) {
            if (this.actionArea == null) {
                this.actionArea = (FrameLayout)((ViewStub)this.findViewById(R.id.design_menu_item_action_area_stub)).inflate();
            }
            this.actionArea.removeAllViews();
            this.actionArea.addView(view);
        }
    }
    
    private boolean shouldExpandActionArea() {
        return this.itemData.getTitle() == null && this.itemData.getIcon() == null && this.itemData.getActionView() != null;
    }
    
    @Override
    public MenuItemImpl getItemData() {
        return this.itemData;
    }
    
    @Override
    public void initialize(final MenuItemImpl itemData, int visibility) {
        this.itemData = itemData;
        if (itemData.isVisible()) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        this.setVisibility(visibility);
        if (this.getBackground() == null) {
            ViewCompat.setBackground((View)this, (Drawable)this.createDefaultBackground());
        }
        this.setCheckable(itemData.isCheckable());
        this.setChecked(itemData.isChecked());
        ((MenuView.ItemView)this).setEnabled(itemData.isEnabled());
        this.setTitle(itemData.getTitle());
        this.setIcon(itemData.getIcon());
        this.setActionView(itemData.getActionView());
        this.setContentDescription(itemData.getContentDescription());
        TooltipCompat.setTooltipText((View)this, itemData.getTooltipText());
        this.adjustAppearance();
    }
    
    protected int[] onCreateDrawableState(final int n) {
        final int[] onCreateDrawableState = super.onCreateDrawableState(n + 1);
        final MenuItemImpl itemData = this.itemData;
        if (itemData != null && itemData.isCheckable() && this.itemData.isChecked()) {
            mergeDrawableStates(onCreateDrawableState, NavigationMenuItemView.CHECKED_STATE_SET);
        }
        return onCreateDrawableState;
    }
    
    @Override
    public boolean prefersCondensedTitle() {
        return false;
    }
    
    public void recycle() {
        final FrameLayout actionArea = this.actionArea;
        if (actionArea != null) {
            actionArea.removeAllViews();
        }
        this.textView.setCompoundDrawables((Drawable)null, (Drawable)null, (Drawable)null, (Drawable)null);
    }
    
    @Override
    public void setCheckable(final boolean checkable) {
        this.refreshDrawableState();
        if (this.checkable != checkable) {
            this.checkable = checkable;
            this.accessibilityDelegate.sendAccessibilityEvent((View)this.textView, 2048);
        }
    }
    
    @Override
    public void setChecked(final boolean checked) {
        this.refreshDrawableState();
        this.textView.setChecked(checked);
    }
    
    public void setHorizontalPadding(final int n) {
        this.setPadding(n, 0, n, 0);
    }
    
    @Override
    public void setIcon(Drawable drawable) {
        if (drawable != null) {
            Drawable mutate = drawable;
            if (this.hasIconTintList) {
                final Drawable$ConstantState constantState = drawable.getConstantState();
                if (constantState != null) {
                    drawable = constantState.newDrawable();
                }
                mutate = DrawableCompat.wrap(drawable).mutate();
                DrawableCompat.setTintList(mutate, this.iconTintList);
            }
            final int iconSize = this.iconSize;
            mutate.setBounds(0, 0, iconSize, iconSize);
            drawable = mutate;
        }
        else if (this.needsEmptyIcon) {
            if (this.emptyDrawable == null) {
                this.emptyDrawable = ResourcesCompat.getDrawable(this.getResources(), R.drawable.navigation_empty_icon, this.getContext().getTheme());
                drawable = this.emptyDrawable;
                if (drawable != null) {
                    final int iconSize2 = this.iconSize;
                    drawable.setBounds(0, 0, iconSize2, iconSize2);
                }
            }
            drawable = this.emptyDrawable;
        }
        TextViewCompat.setCompoundDrawablesRelative((TextView)this.textView, drawable, null, null, null);
    }
    
    public void setIconPadding(final int compoundDrawablePadding) {
        this.textView.setCompoundDrawablePadding(compoundDrawablePadding);
    }
    
    void setIconTintList(final ColorStateList iconTintList) {
        this.iconTintList = iconTintList;
        this.hasIconTintList = (this.iconTintList != null);
        final MenuItemImpl itemData = this.itemData;
        if (itemData != null) {
            this.setIcon(itemData.getIcon());
        }
    }
    
    public void setNeedsEmptyIcon(final boolean needsEmptyIcon) {
        this.needsEmptyIcon = needsEmptyIcon;
    }
    
    @Override
    public void setShortcut(final boolean b, final char c) {
    }
    
    public void setTextAppearance(final int n) {
        TextViewCompat.setTextAppearance((TextView)this.textView, n);
    }
    
    public void setTextColor(final ColorStateList textColor) {
        this.textView.setTextColor(textColor);
    }
    
    @Override
    public void setTitle(final CharSequence text) {
        this.textView.setText(text);
    }
    
    @Override
    public boolean showsIcon() {
        return true;
    }
}
