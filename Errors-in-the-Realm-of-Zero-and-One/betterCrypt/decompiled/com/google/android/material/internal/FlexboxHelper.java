// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.internal;

import android.view.ViewGroup$LayoutParams;
import java.util.Iterator;
import java.util.Collections;
import android.util.SparseIntArray;
import java.util.Arrays;
import android.view.View;
import java.util.ArrayList;
import android.view.View$MeasureSpec;
import java.util.List;

class FlexboxHelper
{
    private static final int INITIAL_CAPACITY = 10;
    private boolean[] childrenFrozen;
    private final FlexContainer flexContainer;
    
    FlexboxHelper(final FlexContainer flexContainer) {
        this.flexContainer = flexContainer;
    }
    
    private void addFlexLine(final List<FlexLine> list, final FlexLine flexLine, final int lastIndex, final int sumCrossSizeBefore) {
        flexLine.sumCrossSizeBefore = sumCrossSizeBefore;
        this.flexContainer.onNewFlexLineAdded(flexLine);
        flexLine.lastIndex = lastIndex;
        list.add(flexLine);
    }
    
    private void calculateFlexLines(final FlexLinesResult flexLinesResult, final int n, final int n2, final int n3, int i, final int n4, final List<FlexLine> list) {
        int n5 = n4;
        final boolean mainAxisDirectionHorizontal = this.flexContainer.isMainAxisDirectionHorizontal();
        final int mode = View$MeasureSpec.getMode(n);
        final int size = View$MeasureSpec.getSize(n);
        List<FlexLine> flexLines;
        if (list == null) {
            flexLines = new ArrayList<FlexLine>();
        }
        else {
            flexLines = list;
        }
        flexLinesResult.flexLines = flexLines;
        int n6;
        if (n5 == -1) {
            n6 = 1;
        }
        else {
            n6 = 0;
        }
        final int paddingStartMain = this.getPaddingStartMain(mainAxisDirectionHorizontal);
        final int paddingEndMain = this.getPaddingEndMain(mainAxisDirectionHorizontal);
        final int paddingStartCross = this.getPaddingStartCross(mainAxisDirectionHorizontal);
        final int paddingEndCross = this.getPaddingEndCross(mainAxisDirectionHorizontal);
        FlexLine flexLine = new FlexLine();
        flexLine.firstIndex = i;
        int n7 = paddingEndMain + paddingStartMain;
        flexLine.mainSize = n7;
        int flexItemCount = this.flexContainer.getFlexItemCount();
        int n8 = Integer.MIN_VALUE;
        int n9 = 0;
        int combineMeasuredStates = 0;
        int n10 = 0;
        while (i < flexItemCount) {
            final View reorderedFlexItem = this.flexContainer.getReorderedFlexItemAt(i);
            int n18 = 0;
            Label_1327: {
                if (reorderedFlexItem == null) {
                    if (this.isLastFlexItem(i, flexItemCount, flexLine)) {
                        this.addFlexLine(flexLines, flexLine, i, n9);
                    }
                }
                else if (reorderedFlexItem.getVisibility() == 8) {
                    ++flexLine.goneItemCount;
                    ++flexLine.itemCount;
                    if (this.isLastFlexItem(i, flexItemCount, flexLine)) {
                        this.addFlexLine(flexLines, flexLine, i, n9);
                    }
                }
                else {
                    final FlexItem flexItem = (FlexItem)reorderedFlexItem.getLayoutParams();
                    flexLine.indicesAlignSelfStretch.add(i);
                    int n12;
                    final int n11 = n12 = this.getFlexItemSizeMain(flexItem, mainAxisDirectionHorizontal);
                    if (flexItem.getFlexBasisPercent() != -1.0f) {
                        n12 = n11;
                        if (mode == 1073741824) {
                            n12 = Math.round(size * flexItem.getFlexBasisPercent());
                        }
                    }
                    int n13;
                    if (mainAxisDirectionHorizontal) {
                        n13 = this.flexContainer.getChildWidthMeasureSpec(n, n7 + this.getFlexItemMarginStartMain(flexItem, true) + this.getFlexItemMarginEndMain(flexItem, true), n12);
                        reorderedFlexItem.measure(n13, this.flexContainer.getChildHeightMeasureSpec(n2, paddingStartCross + paddingEndCross + this.getFlexItemMarginStartCross(flexItem, true) + this.getFlexItemMarginEndCross(flexItem, true) + n9, this.getFlexItemSizeCross(flexItem, true)));
                    }
                    else {
                        final int childWidthMeasureSpec = this.flexContainer.getChildWidthMeasureSpec(n2, paddingStartCross + paddingEndCross + this.getFlexItemMarginStartCross(flexItem, false) + this.getFlexItemMarginEndCross(flexItem, false) + n9, this.getFlexItemSizeCross(flexItem, false));
                        n13 = this.flexContainer.getChildHeightMeasureSpec(n, this.getFlexItemMarginStartMain(flexItem, false) + n7 + this.getFlexItemMarginEndMain(flexItem, false), n12);
                        reorderedFlexItem.measure(childWidthMeasureSpec, n13);
                    }
                    this.flexContainer.updateViewCache(i, reorderedFlexItem);
                    this.checkSizeConstraints(reorderedFlexItem, i);
                    combineMeasuredStates = View.combineMeasuredStates(combineMeasuredStates, reorderedFlexItem.getMeasuredState());
                    int a;
                    int n16;
                    if (this.isWrapRequired(reorderedFlexItem, mode, size, flexLine.mainSize, this.getFlexItemMarginEndMain(flexItem, mainAxisDirectionHorizontal) + (this.getViewMeasuredSizeMain(reorderedFlexItem, mainAxisDirectionHorizontal) + this.getFlexItemMarginStartMain(flexItem, mainAxisDirectionHorizontal)), flexItem, i, n10)) {
                        final int itemCountNotGone = flexLine.getItemCountNotGone();
                        final int firstIndex = i;
                        int n15;
                        if (itemCountNotGone > 0) {
                            int n14;
                            if (firstIndex > 0) {
                                n14 = firstIndex - 1;
                            }
                            else {
                                n14 = 0;
                            }
                            this.addFlexLine(flexLines, flexLine, n14, n9);
                            n15 = flexLine.crossSize + n9;
                        }
                        else {
                            n15 = n9;
                        }
                        if (mainAxisDirectionHorizontal) {
                            if (flexItem.getHeight() == -1) {
                                final FlexContainer flexContainer = this.flexContainer;
                                reorderedFlexItem.measure(n13, flexContainer.getChildHeightMeasureSpec(n2, flexContainer.getPaddingTop() + this.flexContainer.getPaddingBottom() + flexItem.getMarginTop() + flexItem.getMarginBottom() + n15, flexItem.getHeight()));
                                this.checkSizeConstraints(reorderedFlexItem, firstIndex);
                            }
                        }
                        else if (flexItem.getWidth() == -1) {
                            final FlexContainer flexContainer2 = this.flexContainer;
                            reorderedFlexItem.measure(flexContainer2.getChildWidthMeasureSpec(n2, flexContainer2.getPaddingLeft() + this.flexContainer.getPaddingRight() + flexItem.getMarginLeft() + flexItem.getMarginRight() + n15, flexItem.getWidth()), n13);
                            this.checkSizeConstraints(reorderedFlexItem, firstIndex);
                        }
                        flexLine = new FlexLine();
                        flexLine.itemCount = 1;
                        flexLine.mainSize = n7;
                        flexLine.firstIndex = firstIndex;
                        n9 = n15;
                        a = Integer.MIN_VALUE;
                        n16 = 0;
                    }
                    else {
                        ++flexLine.itemCount;
                        final int n17 = n10 + 1;
                        a = n8;
                        n16 = n17;
                    }
                    n18 = i;
                    final int n19 = n7;
                    flexLine.mainSize += this.getViewMeasuredSizeMain(reorderedFlexItem, mainAxisDirectionHorizontal) + this.getFlexItemMarginStartMain(flexItem, mainAxisDirectionHorizontal) + this.getFlexItemMarginEndMain(flexItem, mainAxisDirectionHorizontal);
                    flexLine.motalFlexGrow += flexItem.getFlexGrow();
                    flexLine.totalFlexShrink += flexItem.getFlexShrink();
                    this.flexContainer.onNewFlexItemAdded(reorderedFlexItem, n18, n16, flexLine);
                    final int max = Math.max(a, this.getViewMeasuredSizeCross(reorderedFlexItem, mainAxisDirectionHorizontal) + this.getFlexItemMarginStartCross(flexItem, mainAxisDirectionHorizontal) + this.getFlexItemMarginEndCross(flexItem, mainAxisDirectionHorizontal) + this.flexContainer.getDecorationLengthCrossAxis(reorderedFlexItem));
                    flexLine.crossSize = Math.max(flexLine.crossSize, max);
                    if (mainAxisDirectionHorizontal) {
                        flexLine.maxBaseline = Math.max(flexLine.maxBaseline, reorderedFlexItem.getBaseline() + flexItem.getMarginTop());
                    }
                    final int n20 = flexItemCount;
                    int n21 = n9;
                    if (this.isLastFlexItem(n18, n20, flexLine)) {
                        this.addFlexLine(flexLines, flexLine, n18, n9);
                        n21 = n9 + flexLine.crossSize;
                    }
                    int n22;
                    if (n4 != -1 && !flexLines.isEmpty()) {
                        n22 = n21;
                        i = n6;
                        if (flexLines.get(flexLines.size() - 1).lastIndex >= n4) {
                            n22 = n21;
                            i = n6;
                            if (n18 >= n4) {
                                n22 = n21;
                                if ((i = n6) == 0) {
                                    n22 = -flexLine.getCrossSize();
                                    i = 1;
                                }
                            }
                        }
                    }
                    else {
                        i = n6;
                        n22 = n21;
                    }
                    if (n22 > n3 && i != 0) {
                        break;
                    }
                    final int n23 = max;
                    final int n24 = n22;
                    n10 = n16;
                    n6 = i;
                    n8 = n23;
                    i = n4;
                    n7 = n19;
                    flexItemCount = n20;
                    n9 = n24;
                    break Label_1327;
                }
                n18 = i;
                i = n5;
            }
            final int n25 = n18 + 1;
            n5 = i;
            i = n25;
        }
        flexLinesResult.childState = combineMeasuredStates;
    }
    
    private void checkSizeConstraints(final View view, final int n) {
        final FlexItem flexItem = (FlexItem)view.getLayoutParams();
        int measuredWidth = view.getMeasuredWidth();
        final int measuredHeight = view.getMeasuredHeight();
        final int minWidth = flexItem.getMinWidth();
        int n2 = 1;
        int n4 = 0;
        Label_0083: {
            int n3;
            if (measuredWidth < minWidth) {
                n3 = flexItem.getMinWidth();
            }
            else {
                if (measuredWidth <= flexItem.getMaxWidth()) {
                    n4 = 0;
                    break Label_0083;
                }
                n3 = flexItem.getMaxWidth();
            }
            final int n5 = 1;
            measuredWidth = n3;
            n4 = n5;
        }
        int n6;
        if (measuredHeight < flexItem.getMinHeight()) {
            n6 = flexItem.getMinHeight();
        }
        else if (measuredHeight > flexItem.getMaxHeight()) {
            n6 = flexItem.getMaxHeight();
        }
        else {
            n2 = n4;
            n6 = measuredHeight;
        }
        if (n2 != 0) {
            view.measure(View$MeasureSpec.makeMeasureSpec(measuredWidth, 1073741824), View$MeasureSpec.makeMeasureSpec(n6, 1073741824));
            this.flexContainer.updateViewCache(n, view);
        }
    }
    
    private List<Order> createOrders(final int initialCapacity) {
        final ArrayList<Order> list = new ArrayList<Order>(initialCapacity);
        for (int i = 0; i < initialCapacity; ++i) {
            final FlexItem flexItem = (FlexItem)this.flexContainer.getFlexItemAt(i).getLayoutParams();
            final Order order = new Order();
            order.order = flexItem.getOrder();
            order.index = i;
            list.add(order);
        }
        return list;
    }
    
    private void determineMainSize(int n, final int n2, int i) {
        this.ensureChildrenFrozen(this.flexContainer.getFlexItemCount());
        if (i >= this.flexContainer.getFlexItemCount()) {
            return;
        }
        i = View$MeasureSpec.getMode(n);
        n = View$MeasureSpec.getSize(n);
        if (i != 1073741824) {
            n = this.flexContainer.getLargestMainSize();
        }
        final int n3 = this.flexContainer.getPaddingLeft() + this.flexContainer.getPaddingRight();
        final List<FlexLine> flexLinesInternal = this.flexContainer.getFlexLinesInternal();
        int size;
        FlexLine flexLine;
        for (size = flexLinesInternal.size(), i = 0; i < size; ++i) {
            flexLine = flexLinesInternal.get(i);
            if (flexLine.mainSize < n) {
                this.expandFlexItems(n2, flexLine, n, n3, false);
            }
            else {
                this.shrinkFlexItems(n2, flexLine, n, n3, false);
            }
        }
    }
    
    private void ensureChildrenFrozen(final int n) {
        final boolean[] childrenFrozen = this.childrenFrozen;
        if (childrenFrozen == null) {
            this.childrenFrozen = new boolean[Math.max(10, n)];
            return;
        }
        if (childrenFrozen.length < n) {
            this.childrenFrozen = new boolean[Math.max(childrenFrozen.length * 2, n)];
            return;
        }
        Arrays.fill(childrenFrozen, false);
    }
    
    private void expandFlexItems(final int n, final FlexLine flexLine, final int n2, final int n3, final boolean b) {
        if (flexLine.motalFlexGrow > 0.0f) {
            if (n2 < flexLine.mainSize) {
                return;
            }
            final int mainSize = flexLine.mainSize;
            final float n4 = (n2 - flexLine.mainSize) / flexLine.motalFlexGrow;
            flexLine.mainSize = n3 + flexLine.dividerLengthInMainSize;
            if (!b) {
                flexLine.crossSize = Integer.MIN_VALUE;
            }
            int i = 0;
            float n5 = 0.0f;
            int max;
            int n6 = max = 0;
            while (i < flexLine.itemCount) {
                final int n7 = flexLine.firstIndex + i;
                final View reorderedFlexItem = this.flexContainer.getReorderedFlexItemAt(n7);
                if (reorderedFlexItem != null) {
                    if (reorderedFlexItem.getVisibility() != 8) {
                        final FlexItem flexItem = (FlexItem)reorderedFlexItem.getLayoutParams();
                        int n8 = reorderedFlexItem.getMeasuredWidth();
                        int measuredHeight = reorderedFlexItem.getMeasuredHeight();
                        if (!this.childrenFrozen[n7] && flexItem.getFlexGrow() > 0.0f) {
                            final float n9 = n8 + flexItem.getFlexGrow() * n4;
                            float n10 = n5;
                            float a = n9;
                            if (i == flexLine.itemCount - 1) {
                                a = n9 + n5;
                                n10 = 0.0f;
                            }
                            final int round = Math.round(a);
                            int maxWidth;
                            int n11;
                            if (round > flexItem.getMaxWidth()) {
                                maxWidth = flexItem.getMaxWidth();
                                this.childrenFrozen[n7] = true;
                                flexLine.motalFlexGrow -= flexItem.getFlexGrow();
                                n11 = 1;
                                n5 = n10;
                            }
                            else {
                                final float n12 = n10 + (a - round);
                                final double n13 = n12;
                                if (n13 > 1.0) {
                                    maxWidth = round + 1;
                                    n5 = n12 - 1.0f;
                                    n11 = n6;
                                }
                                else {
                                    n11 = n6;
                                    n5 = n12;
                                    maxWidth = round;
                                    if (n13 < -1.0) {
                                        maxWidth = round - 1;
                                        n5 = n12 + 1.0f;
                                        n11 = n6;
                                    }
                                }
                            }
                            reorderedFlexItem.measure(View$MeasureSpec.makeMeasureSpec(maxWidth, 1073741824), this.getChildHeightMeasureSpecInternal(n, flexItem, flexLine.sumCrossSizeBefore));
                            n8 = reorderedFlexItem.getMeasuredWidth();
                            final int measuredHeight2 = reorderedFlexItem.getMeasuredHeight();
                            this.flexContainer.updateViewCache(n7, reorderedFlexItem);
                            n6 = n11;
                            measuredHeight = measuredHeight2;
                        }
                        max = Math.max(max, measuredHeight + flexItem.getMarginTop() + flexItem.getMarginBottom() + this.flexContainer.getDecorationLengthCrossAxis(reorderedFlexItem));
                        flexLine.mainSize += n8 + flexItem.getMarginLeft() + flexItem.getMarginRight();
                        flexLine.crossSize = Math.max(flexLine.crossSize, max);
                    }
                }
                ++i;
            }
            if (n6 != 0 && mainSize != flexLine.mainSize) {
                this.expandFlexItems(n, flexLine, n2, n3, true);
            }
        }
    }
    
    private int getChildHeightMeasureSpecInternal(int measureSpec, final FlexItem flexItem, int childHeightMeasureSpec) {
        final FlexContainer flexContainer = this.flexContainer;
        childHeightMeasureSpec = flexContainer.getChildHeightMeasureSpec(measureSpec, flexContainer.getPaddingTop() + this.flexContainer.getPaddingBottom() + flexItem.getMarginTop() + flexItem.getMarginBottom() + childHeightMeasureSpec, flexItem.getHeight());
        final int size = View$MeasureSpec.getSize(childHeightMeasureSpec);
        if (size > flexItem.getMaxHeight()) {
            return View$MeasureSpec.makeMeasureSpec(flexItem.getMaxHeight(), View$MeasureSpec.getMode(childHeightMeasureSpec));
        }
        measureSpec = childHeightMeasureSpec;
        if (size < flexItem.getMinHeight()) {
            measureSpec = View$MeasureSpec.makeMeasureSpec(flexItem.getMinHeight(), View$MeasureSpec.getMode(childHeightMeasureSpec));
        }
        return measureSpec;
    }
    
    private int getFlexItemMarginEndCross(final FlexItem flexItem, final boolean b) {
        if (b) {
            return flexItem.getMarginBottom();
        }
        return flexItem.getMarginRight();
    }
    
    private int getFlexItemMarginEndMain(final FlexItem flexItem, final boolean b) {
        if (b) {
            return flexItem.getMarginRight();
        }
        return flexItem.getMarginBottom();
    }
    
    private int getFlexItemMarginStartCross(final FlexItem flexItem, final boolean b) {
        if (b) {
            return flexItem.getMarginTop();
        }
        return flexItem.getMarginLeft();
    }
    
    private int getFlexItemMarginStartMain(final FlexItem flexItem, final boolean b) {
        if (b) {
            return flexItem.getMarginLeft();
        }
        return flexItem.getMarginTop();
    }
    
    private int getFlexItemSizeCross(final FlexItem flexItem, final boolean b) {
        if (b) {
            return flexItem.getHeight();
        }
        return flexItem.getWidth();
    }
    
    private int getFlexItemSizeMain(final FlexItem flexItem, final boolean b) {
        if (b) {
            return flexItem.getWidth();
        }
        return flexItem.getHeight();
    }
    
    private int getPaddingEndCross(final boolean b) {
        if (b) {
            return this.flexContainer.getPaddingBottom();
        }
        return this.flexContainer.getPaddingEnd();
    }
    
    private int getPaddingEndMain(final boolean b) {
        if (b) {
            return this.flexContainer.getPaddingEnd();
        }
        return this.flexContainer.getPaddingBottom();
    }
    
    private int getPaddingStartCross(final boolean b) {
        if (b) {
            return this.flexContainer.getPaddingTop();
        }
        return this.flexContainer.getPaddingStart();
    }
    
    private int getPaddingStartMain(final boolean b) {
        if (b) {
            return this.flexContainer.getPaddingStart();
        }
        return this.flexContainer.getPaddingTop();
    }
    
    private int getViewMeasuredSizeCross(final View view, final boolean b) {
        if (b) {
            return view.getMeasuredHeight();
        }
        return view.getMeasuredWidth();
    }
    
    private int getViewMeasuredSizeMain(final View view, final boolean b) {
        if (b) {
            return view.getMeasuredWidth();
        }
        return view.getMeasuredHeight();
    }
    
    private boolean isLastFlexItem(final int n, final int n2, final FlexLine flexLine) {
        return n == n2 - 1 && flexLine.getItemCountNotGone() != 0;
    }
    
    private boolean isWrapRequired(final View view, int n, final int n2, final int n3, final int n4, final FlexItem flexItem, int decorationLengthMainAxis, final int n5) {
        if (this.flexContainer.getFlexWrap() == 0) {
            return false;
        }
        if (flexItem.isWrapBefore()) {
            return true;
        }
        if (n == 0) {
            return false;
        }
        decorationLengthMainAxis = this.flexContainer.getDecorationLengthMainAxis(view, decorationLengthMainAxis, n5);
        n = n4;
        if (decorationLengthMainAxis > 0) {
            n = n4 + decorationLengthMainAxis;
        }
        return n2 < n3 + n;
    }
    
    private void shrinkFlexItems(final int n, final FlexLine flexLine, final int n2, final int n3, final boolean b) {
        final int mainSize = flexLine.mainSize;
        if (flexLine.totalFlexShrink > 0.0f) {
            if (n2 > flexLine.mainSize) {
                return;
            }
            final float n4 = (flexLine.mainSize - n2) / flexLine.totalFlexShrink;
            flexLine.mainSize = n3 + flexLine.dividerLengthInMainSize;
            if (!b) {
                flexLine.crossSize = Integer.MIN_VALUE;
            }
            int i = 0;
            float n5 = 0.0f;
            int max;
            int n6 = max = 0;
            while (i < flexLine.itemCount) {
                final int n7 = flexLine.firstIndex + i;
                final View reorderedFlexItem = this.flexContainer.getReorderedFlexItemAt(n7);
                if (reorderedFlexItem != null) {
                    if (reorderedFlexItem.getVisibility() != 8) {
                        final FlexItem flexItem = (FlexItem)reorderedFlexItem.getLayoutParams();
                        int n8 = reorderedFlexItem.getMeasuredWidth();
                        int measuredHeight = reorderedFlexItem.getMeasuredHeight();
                        if (!this.childrenFrozen[n7] && flexItem.getFlexShrink() > 0.0f) {
                            final float n9 = n8 - flexItem.getFlexShrink() * n4;
                            float n10 = n5;
                            float a = n9;
                            if (i == flexLine.itemCount - 1) {
                                a = n9 + n5;
                                n10 = 0.0f;
                            }
                            final int round = Math.round(a);
                            int minWidth;
                            int n11;
                            if (round < flexItem.getMinWidth()) {
                                minWidth = flexItem.getMinWidth();
                                this.childrenFrozen[n7] = true;
                                flexLine.totalFlexShrink -= flexItem.getFlexShrink();
                                n11 = 1;
                                n5 = n10;
                            }
                            else {
                                final float n12 = n10 + (a - round);
                                final double n13 = n12;
                                if (n13 > 1.0) {
                                    minWidth = round + 1;
                                    n5 = n12 - 1.0f;
                                    n11 = n6;
                                }
                                else {
                                    n11 = n6;
                                    n5 = n12;
                                    minWidth = round;
                                    if (n13 < -1.0) {
                                        minWidth = round - 1;
                                        n5 = n12 + 1.0f;
                                        n11 = n6;
                                    }
                                }
                            }
                            reorderedFlexItem.measure(View$MeasureSpec.makeMeasureSpec(minWidth, 1073741824), this.getChildHeightMeasureSpecInternal(n, flexItem, flexLine.sumCrossSizeBefore));
                            n8 = reorderedFlexItem.getMeasuredWidth();
                            final int measuredHeight2 = reorderedFlexItem.getMeasuredHeight();
                            this.flexContainer.updateViewCache(n7, reorderedFlexItem);
                            n6 = n11;
                            measuredHeight = measuredHeight2;
                        }
                        max = Math.max(max, measuredHeight + flexItem.getMarginTop() + flexItem.getMarginBottom() + this.flexContainer.getDecorationLengthCrossAxis(reorderedFlexItem));
                        flexLine.mainSize += n8 + flexItem.getMarginLeft() + flexItem.getMarginRight();
                        flexLine.crossSize = Math.max(flexLine.crossSize, max);
                    }
                }
                ++i;
            }
            if (n6 != 0 && mainSize != flexLine.mainSize) {
                this.shrinkFlexItems(n, flexLine, n2, n3, true);
            }
        }
    }
    
    private int[] sortOrdersIntoReorderedIndices(int n, final List<Order> list, final SparseIntArray sparseIntArray) {
        Collections.sort((List<Comparable>)list);
        sparseIntArray.clear();
        final int[] array = new int[n];
        final Iterator<Order> iterator = list.iterator();
        n = 0;
        while (iterator.hasNext()) {
            final Order order = iterator.next();
            array[n] = order.index;
            sparseIntArray.append(order.index, order.order);
            ++n;
        }
        return array;
    }
    
    private void stretchViewVertically(final View view, int min, final int n) {
        final FlexItem flexItem = (FlexItem)view.getLayoutParams();
        min = Math.min(Math.max(min - flexItem.getMarginTop() - flexItem.getMarginBottom() - this.flexContainer.getDecorationLengthCrossAxis(view), flexItem.getMinHeight()), flexItem.getMaxHeight());
        view.measure(View$MeasureSpec.makeMeasureSpec(view.getMeasuredWidth(), 1073741824), View$MeasureSpec.makeMeasureSpec(min, 1073741824));
        this.flexContainer.updateViewCache(n, view);
    }
    
    void calculateHorizontalFlexLines(final FlexLinesResult flexLinesResult, final int n, final int n2) {
        this.calculateFlexLines(flexLinesResult, n, n2, Integer.MAX_VALUE, 0, -1, null);
    }
    
    int[] createReorderedIndices(final SparseIntArray sparseIntArray) {
        final int flexItemCount = this.flexContainer.getFlexItemCount();
        return this.sortOrdersIntoReorderedIndices(flexItemCount, this.createOrders(flexItemCount), sparseIntArray);
    }
    
    int[] createReorderedIndices(final View view, int i, final ViewGroup$LayoutParams viewGroup$LayoutParams, final SparseIntArray sparseIntArray) {
        final int flexItemCount = this.flexContainer.getFlexItemCount();
        final List<Order> orders = this.createOrders(flexItemCount);
        final Order order = new Order();
        if (view != null && viewGroup$LayoutParams instanceof FlexItem) {
            order.order = ((FlexItem)viewGroup$LayoutParams).getOrder();
        }
        else {
            order.order = 1;
        }
        if (i != -1 && i != flexItemCount) {
            if (i < this.flexContainer.getFlexItemCount()) {
                order.index = i;
                while (i < flexItemCount) {
                    final Order order2 = orders.get(i);
                    ++order2.index;
                    ++i;
                }
            }
            else {
                order.index = flexItemCount;
            }
        }
        else {
            order.index = flexItemCount;
        }
        orders.add(order);
        return this.sortOrdersIntoReorderedIndices(flexItemCount + 1, orders, sparseIntArray);
    }
    
    void determineCrossSize(int crossSize, int i) {
        final int mode = View$MeasureSpec.getMode(crossSize);
        final int size = View$MeasureSpec.getSize(crossSize);
        final List<FlexLine> flexLinesInternal = this.flexContainer.getFlexLinesInternal();
        if (mode == 1073741824) {
            final int n = this.flexContainer.getSumOfCrossSize() + i;
            final int size2 = flexLinesInternal.size();
            crossSize = 0;
            if (size2 == 1) {
                flexLinesInternal.get(0).crossSize = size - i;
                return;
            }
            if (flexLinesInternal.size() >= 2 && n < size) {
                final float n2 = (size - n) / (float)flexLinesInternal.size();
                final int size3 = flexLinesInternal.size();
                float n3 = 0.0f;
                FlexLine flexLine;
                float n4;
                float n5;
                float a;
                int round;
                float n6;
                for (i = crossSize; i < size3; ++i) {
                    flexLine = flexLinesInternal.get(i);
                    n4 = flexLine.crossSize + n2;
                    n5 = n3;
                    a = n4;
                    if (i == flexLinesInternal.size() - 1) {
                        a = n4 + n3;
                        n5 = 0.0f;
                    }
                    round = Math.round(a);
                    n6 = n5 + (a - round);
                    if (n6 > 1.0f) {
                        crossSize = round + 1;
                        n3 = n6 - 1.0f;
                    }
                    else {
                        n3 = n6;
                        crossSize = round;
                        if (n6 < -1.0f) {
                            crossSize = round - 1;
                            n3 = n6 + 1.0f;
                        }
                    }
                    flexLine.crossSize = crossSize;
                }
            }
        }
    }
    
    void determineMainSize(final int n, final int n2) {
        this.determineMainSize(n, n2, 0);
    }
    
    boolean isOrderChangedFromLastMeasurement(final SparseIntArray sparseIntArray) {
        final int flexItemCount = this.flexContainer.getFlexItemCount();
        if (sparseIntArray.size() != flexItemCount) {
            return true;
        }
        for (int i = 0; i < flexItemCount; ++i) {
            final View flexItem = this.flexContainer.getFlexItemAt(i);
            if (flexItem != null) {
                if (((FlexItem)flexItem.getLayoutParams()).getOrder() != sparseIntArray.get(i)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    void layoutSingleChildHorizontal(final View view, final int n, final int n2, final int n3, final int n4) {
        final FlexItem flexItem = (FlexItem)view.getLayoutParams();
        view.layout(n, n2 + flexItem.getMarginTop(), n3, n4 + flexItem.getMarginTop());
    }
    
    void stretchViews() {
        if (this.flexContainer.getFlexItemCount() <= 0) {
            return;
        }
        final List<FlexLine> flexLinesInternal = this.flexContainer.getFlexLinesInternal();
        for (int size = flexLinesInternal.size(), i = 0; i < size; ++i) {
            final FlexLine flexLine = flexLinesInternal.get(i);
            for (int itemCount = flexLine.itemCount, j = 0; j < itemCount; ++j) {
                final int n = flexLine.firstIndex + j;
                if (j < this.flexContainer.getFlexItemCount()) {
                    final View reorderedFlexItem = this.flexContainer.getReorderedFlexItemAt(n);
                    if (reorderedFlexItem != null) {
                        if (reorderedFlexItem.getVisibility() != 8) {
                            this.stretchViewVertically(reorderedFlexItem, flexLine.crossSize, n);
                        }
                    }
                }
            }
        }
    }
    
    static class FlexLinesResult
    {
        int childState;
        List<FlexLine> flexLines;
        
        void reset() {
            this.flexLines = null;
            this.childState = 0;
        }
    }
    
    private static class Order implements Comparable<Order>
    {
        int index;
        int order;
        
        @Override
        public int compareTo(final Order order) {
            final int order2 = this.order;
            final int order3 = order.order;
            if (order2 != order3) {
                return order2 - order3;
            }
            return this.index - order.index;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Order{order=");
            sb.append(this.order);
            sb.append(", index=");
            sb.append(this.index);
            sb.append('}');
            return sb.toString();
        }
    }
}
