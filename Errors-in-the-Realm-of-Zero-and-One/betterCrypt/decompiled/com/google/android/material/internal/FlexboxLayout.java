// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.internal;

import android.content.res.TypedArray;
import com.google.android.material.R;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.lang.annotation.Annotation;
import androidx.core.view.ViewCompat;
import java.util.Iterator;
import android.view.ViewGroup$MarginLayoutParams;
import android.view.ViewGroup$LayoutParams;
import android.view.View$MeasureSpec;
import android.graphics.Canvas;
import android.view.View;
import java.util.ArrayList;
import android.util.AttributeSet;
import android.content.Context;
import android.util.SparseIntArray;
import java.util.List;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;

public class FlexboxLayout extends ViewGroup implements FlexContainer
{
    public static final int SHOW_DIVIDER_BEGINNING = 1;
    public static final int SHOW_DIVIDER_END = 4;
    public static final int SHOW_DIVIDER_MIDDLE = 2;
    public static final int SHOW_DIVIDER_NONE = 0;
    private Drawable dividerDrawableHorizontal;
    private Drawable dividerDrawableVertical;
    private int dividerHorizontalHeight;
    private int dividerVerticalWidth;
    private List<FlexLine> flexLines;
    private FlexboxHelper.FlexLinesResult flexLinesResult;
    private int flexWrap;
    private FlexboxHelper flexboxHelper;
    private SparseIntArray orderCache;
    private int[] reorderedIndices;
    private int showDividerHorizontal;
    private int showDividerVertical;
    
    public FlexboxLayout(final Context context) {
        this(context, null);
    }
    
    public FlexboxLayout(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public FlexboxLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.flexboxHelper = new FlexboxHelper(this);
        this.flexLines = new ArrayList<FlexLine>();
        this.flexLinesResult = new FlexboxHelper.FlexLinesResult();
    }
    
    private boolean allFlexLinesAreDummyBefore(final int n) {
        for (int i = 0; i < n; ++i) {
            if (this.flexLines.get(i).getItemCountNotGone() > 0) {
                return false;
            }
        }
        return true;
    }
    
    private boolean allViewsAreGoneBefore(final int n, final int n2) {
        for (int i = 1; i <= n2; ++i) {
            final View reorderedChild = this.getReorderedChildAt(n - i);
            if (reorderedChild != null && reorderedChild.getVisibility() != 8) {
                return false;
            }
        }
        return true;
    }
    
    private void drawDividersHorizontal(final Canvas canvas, final boolean b) {
        final int paddingLeft = this.getPaddingLeft();
        final int max = Math.max(0, this.getWidth() - this.getPaddingRight() - paddingLeft);
        for (int size = this.flexLines.size(), i = 0; i < size; ++i) {
            final FlexLine flexLine = this.flexLines.get(i);
            for (int j = 0; j < flexLine.itemCount; ++j) {
                final int n = flexLine.firstIndex + j;
                final View reorderedChild = this.getReorderedChildAt(n);
                if (reorderedChild != null) {
                    if (reorderedChild.getVisibility() != 8) {
                        final LayoutParams layoutParams = (LayoutParams)reorderedChild.getLayoutParams();
                        if (this.hasDividerBeforeChildAtAlongMainAxis(n, j)) {
                            int n2;
                            if (b) {
                                n2 = reorderedChild.getRight() + layoutParams.rightMargin;
                            }
                            else {
                                n2 = reorderedChild.getLeft() - layoutParams.leftMargin - this.dividerVerticalWidth;
                            }
                            this.drawVerticalDivider(canvas, n2, flexLine.top, flexLine.crossSize);
                        }
                        if (j == flexLine.itemCount - 1 && (this.showDividerVertical & 0x4) > 0) {
                            int n3;
                            if (b) {
                                n3 = reorderedChild.getLeft() - layoutParams.leftMargin - this.dividerVerticalWidth;
                            }
                            else {
                                n3 = reorderedChild.getRight() + layoutParams.rightMargin;
                            }
                            this.drawVerticalDivider(canvas, n3, flexLine.top, flexLine.crossSize);
                        }
                    }
                }
            }
            if (this.hasDividerBeforeFlexLine(i)) {
                this.drawHorizontalDivider(canvas, paddingLeft, flexLine.top - this.dividerHorizontalHeight, max);
            }
            if (this.hasEndDividerAfterFlexLine(i) && (this.showDividerHorizontal & 0x4) > 0) {
                this.drawHorizontalDivider(canvas, paddingLeft, flexLine.bottom, max);
            }
        }
    }
    
    private void drawHorizontalDivider(final Canvas canvas, final int n, final int n2, final int n3) {
        final Drawable dividerDrawableHorizontal = this.dividerDrawableHorizontal;
        if (dividerDrawableHorizontal == null) {
            return;
        }
        dividerDrawableHorizontal.setBounds(n, n2, n3 + n, this.dividerHorizontalHeight + n2);
        this.dividerDrawableHorizontal.draw(canvas);
    }
    
    private void drawVerticalDivider(final Canvas canvas, final int n, final int n2, final int n3) {
        final Drawable dividerDrawableVertical = this.dividerDrawableVertical;
        if (dividerDrawableVertical == null) {
            return;
        }
        dividerDrawableVertical.setBounds(n, n2, this.dividerVerticalWidth + n, n3 + n2);
        this.dividerDrawableVertical.draw(canvas);
    }
    
    private boolean hasDividerBeforeChildAtAlongMainAxis(final int n, final int n2) {
        final boolean allViewsAreGoneBefore = this.allViewsAreGoneBefore(n, n2);
        final boolean b = false;
        final boolean b2 = false;
        final boolean b3 = false;
        boolean b4 = false;
        if (allViewsAreGoneBefore) {
            if (this.isMainAxisDirectionHorizontal()) {
                if ((this.showDividerVertical & 0x1) != 0x0) {
                    b4 = true;
                }
                return b4;
            }
            boolean b5 = b;
            if ((this.showDividerHorizontal & 0x1) != 0x0) {
                b5 = true;
            }
            return b5;
        }
        else {
            if (this.isMainAxisDirectionHorizontal()) {
                boolean b6 = b2;
                if ((this.showDividerVertical & 0x2) != 0x0) {
                    b6 = true;
                }
                return b6;
            }
            boolean b7 = b3;
            if ((this.showDividerHorizontal & 0x2) != 0x0) {
                b7 = true;
            }
            return b7;
        }
    }
    
    private boolean hasDividerBeforeFlexLine(final int n) {
        final boolean b = false;
        final boolean b2 = false;
        final boolean b3 = false;
        final boolean b4 = false;
        boolean b5 = b3;
        if (n >= 0) {
            if (n >= this.flexLines.size()) {
                return false;
            }
            if (this.allFlexLinesAreDummyBefore(n)) {
                if (this.isMainAxisDirectionHorizontal()) {
                    boolean b6 = b4;
                    if ((this.showDividerHorizontal & 0x1) != 0x0) {
                        b6 = true;
                    }
                    return b6;
                }
                boolean b7 = b;
                if ((this.showDividerVertical & 0x1) != 0x0) {
                    b7 = true;
                }
                return b7;
            }
            else {
                if (this.isMainAxisDirectionHorizontal()) {
                    boolean b8 = b2;
                    if ((this.showDividerHorizontal & 0x2) != 0x0) {
                        b8 = true;
                    }
                    return b8;
                }
                b5 = b3;
                if ((this.showDividerVertical & 0x2) != 0x0) {
                    b5 = true;
                }
            }
        }
        return b5;
    }
    
    private boolean hasEndDividerAfterFlexLine(int i) {
        final boolean b = false;
        final boolean b2 = false;
        boolean b3 = b;
        if (i >= 0) {
            if (i >= this.flexLines.size()) {
                return false;
            }
            for (++i; i < this.flexLines.size(); ++i) {
                if (this.flexLines.get(i).getItemCountNotGone() > 0) {
                    return false;
                }
            }
            if (this.isMainAxisDirectionHorizontal()) {
                boolean b4 = b2;
                if ((this.showDividerHorizontal & 0x4) != 0x0) {
                    b4 = true;
                }
                return b4;
            }
            b3 = b;
            if ((this.showDividerVertical & 0x4) != 0x0) {
                b3 = true;
            }
        }
        return b3;
    }
    
    private void layoutHorizontal(final boolean b, final int n, final int n2) {
        final int paddingLeft = this.getPaddingLeft();
        final int paddingRight = this.getPaddingRight();
        int paddingTop = this.getPaddingTop();
        for (int size = this.flexLines.size(), i = 0; i < size; ++i) {
            final FlexLine flexLine = this.flexLines.get(i);
            int n3 = paddingTop;
            if (this.hasDividerBeforeFlexLine(i)) {
                n3 = paddingTop + this.dividerHorizontalHeight;
            }
            float n4 = (float)paddingLeft;
            float n5 = (float)(n2 - n - paddingRight);
            final float max = Math.max(0.0f, 0.0f);
            for (int j = 0; j < flexLine.itemCount; ++j) {
                final int n6 = flexLine.firstIndex + j;
                final View reorderedChild = this.getReorderedChildAt(n6);
                if (reorderedChild != null) {
                    if (reorderedChild.getVisibility() != 8) {
                        final LayoutParams layoutParams = (LayoutParams)reorderedChild.getLayoutParams();
                        float n7 = n4 + layoutParams.leftMargin;
                        float n8 = n5 - layoutParams.rightMargin;
                        int dividerVerticalWidth;
                        if (this.hasDividerBeforeChildAtAlongMainAxis(n6, j)) {
                            dividerVerticalWidth = this.dividerVerticalWidth;
                            final float n9 = (float)dividerVerticalWidth;
                            n7 += n9;
                            n8 -= n9;
                        }
                        else {
                            dividerVerticalWidth = 0;
                        }
                        int dividerVerticalWidth2;
                        if (j == flexLine.itemCount - 1 && (this.showDividerVertical & 0x4) > 0) {
                            dividerVerticalWidth2 = this.dividerVerticalWidth;
                        }
                        else {
                            dividerVerticalWidth2 = 0;
                        }
                        if (b) {
                            this.flexboxHelper.layoutSingleChildHorizontal(reorderedChild, Math.round(n8) - reorderedChild.getMeasuredWidth(), n3, Math.round(n8), n3 + reorderedChild.getMeasuredHeight());
                        }
                        else {
                            this.flexboxHelper.layoutSingleChildHorizontal(reorderedChild, Math.round(n7), n3, Math.round(n7) + reorderedChild.getMeasuredWidth(), n3 + reorderedChild.getMeasuredHeight());
                        }
                        final float n10 = (float)reorderedChild.getMeasuredWidth();
                        final float n11 = (float)layoutParams.rightMargin;
                        final float n12 = (float)reorderedChild.getMeasuredWidth();
                        final float n13 = (float)layoutParams.leftMargin;
                        if (b) {
                            flexLine.updatePositionFromView(reorderedChild, dividerVerticalWidth2, 0, dividerVerticalWidth, 0);
                        }
                        else {
                            flexLine.updatePositionFromView(reorderedChild, dividerVerticalWidth, 0, dividerVerticalWidth2, 0);
                        }
                        n4 = n7 + (n10 + max + n11);
                        n5 = n8 - (n12 + max + n13);
                    }
                }
            }
            paddingTop = n3 + flexLine.crossSize;
        }
    }
    
    private void measureHorizontal(final int n, final int n2) {
        this.flexLines.clear();
        this.flexLinesResult.reset();
        this.flexboxHelper.calculateHorizontalFlexLines(this.flexLinesResult, n, n2);
        this.flexLines = this.flexLinesResult.flexLines;
        this.flexboxHelper.determineMainSize(n, n2);
        this.flexboxHelper.determineCrossSize(n2, this.getPaddingTop() + this.getPaddingBottom());
        this.flexboxHelper.stretchViews();
        this.setMeasuredDimensionForFlex(n, n2, this.flexLinesResult.childState);
    }
    
    private void setMeasuredDimensionForFlex(int n, int n2, int n3) {
        final int mode = View$MeasureSpec.getMode(n);
        final int size = View$MeasureSpec.getSize(n);
        final int mode2 = View$MeasureSpec.getMode(n2);
        final int size2 = View$MeasureSpec.getSize(n2);
        final int n4 = this.getSumOfCrossSize() + this.getPaddingTop() + this.getPaddingBottom();
        final int largestMainSize = this.getLargestMainSize();
        if (mode != Integer.MIN_VALUE) {
            if (mode != 0) {
                if (mode != 1073741824) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unknown width mode is set: ");
                    sb.append(mode);
                    throw new IllegalStateException(sb.toString());
                }
                int combineMeasuredStates = n3;
                if (size < largestMainSize) {
                    combineMeasuredStates = View.combineMeasuredStates(n3, 16777216);
                }
                n = View.resolveSizeAndState(size, n, combineMeasuredStates);
                n3 = combineMeasuredStates;
            }
            else {
                n = View.resolveSizeAndState(largestMainSize, n, n3);
            }
        }
        else {
            int n5;
            if (size < largestMainSize) {
                n3 = View.combineMeasuredStates(n3, 16777216);
                n5 = size;
            }
            else {
                n5 = largestMainSize;
            }
            n = View.resolveSizeAndState(n5, n, n3);
        }
        if (mode2 != Integer.MIN_VALUE) {
            if (mode2 != 0) {
                if (mode2 != 1073741824) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Unknown height mode is set: ");
                    sb2.append(mode2);
                    throw new IllegalStateException(sb2.toString());
                }
                int combineMeasuredStates2 = n3;
                if (size2 < n4) {
                    combineMeasuredStates2 = View.combineMeasuredStates(n3, 256);
                }
                n2 = View.resolveSizeAndState(size2, n2, combineMeasuredStates2);
            }
            else {
                n2 = View.resolveSizeAndState(n4, n2, n3);
            }
        }
        else {
            int n6;
            if (size2 < n4) {
                n3 = View.combineMeasuredStates(n3, 256);
                n6 = size2;
            }
            else {
                n6 = n4;
            }
            n2 = View.resolveSizeAndState(n6, n2, n3);
        }
        this.setMeasuredDimension(n, n2);
    }
    
    private void setWillNotDrawFlag() {
        if (this.dividerDrawableHorizontal == null && this.dividerDrawableVertical == null) {
            this.setWillNotDraw(true);
            return;
        }
        this.setWillNotDraw(false);
    }
    
    public void addView(final View view, final int n, final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        if (this.orderCache == null) {
            this.orderCache = new SparseIntArray(this.getChildCount());
        }
        this.reorderedIndices = this.flexboxHelper.createReorderedIndices(view, n, viewGroup$LayoutParams, this.orderCache);
        super.addView(view, n, viewGroup$LayoutParams);
    }
    
    protected boolean checkLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        return viewGroup$LayoutParams instanceof LayoutParams;
    }
    
    protected ViewGroup$LayoutParams generateLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        if (viewGroup$LayoutParams instanceof LayoutParams) {
            return (ViewGroup$LayoutParams)new LayoutParams((LayoutParams)viewGroup$LayoutParams);
        }
        if (viewGroup$LayoutParams instanceof ViewGroup$MarginLayoutParams) {
            return (ViewGroup$LayoutParams)new LayoutParams((ViewGroup$MarginLayoutParams)viewGroup$LayoutParams);
        }
        return (ViewGroup$LayoutParams)new LayoutParams(viewGroup$LayoutParams);
    }
    
    public LayoutParams generateLayoutParams(final AttributeSet set) {
        return new LayoutParams(this.getContext(), set);
    }
    
    public int getChildHeightMeasureSpec(final int n, final int n2, final int n3) {
        return getChildMeasureSpec(n, n2, n3);
    }
    
    public int getChildWidthMeasureSpec(final int n, final int n2, final int n3) {
        return getChildMeasureSpec(n, n2, n3);
    }
    
    public int getDecorationLengthCrossAxis(final View view) {
        return 0;
    }
    
    public int getDecorationLengthMainAxis(final View view, int n, final int n2) {
        final boolean mainAxisDirectionHorizontal = this.isMainAxisDirectionHorizontal();
        final int n3 = 0;
        int n4 = 0;
        if (mainAxisDirectionHorizontal) {
            if (this.hasDividerBeforeChildAtAlongMainAxis(n, n2)) {
                n4 = 0 + this.dividerVerticalWidth;
            }
            n = n4;
            if ((this.showDividerVertical & 0x4) <= 0) {
                return n;
            }
            n = this.dividerVerticalWidth;
        }
        else {
            n4 = n3;
            if (this.hasDividerBeforeChildAtAlongMainAxis(n, n2)) {
                n4 = 0 + this.dividerHorizontalHeight;
            }
            n = n4;
            if ((this.showDividerHorizontal & 0x4) <= 0) {
                return n;
            }
            n = this.dividerHorizontalHeight;
        }
        n += n4;
        return n;
    }
    
    public Drawable getDividerDrawableHorizontal() {
        return this.dividerDrawableHorizontal;
    }
    
    public Drawable getDividerDrawableVertical() {
        return this.dividerDrawableVertical;
    }
    
    public View getFlexItemAt(final int n) {
        return this.getChildAt(n);
    }
    
    public int getFlexItemCount() {
        return this.getChildCount();
    }
    
    public List<FlexLine> getFlexLines() {
        final ArrayList<FlexLine> list = new ArrayList<FlexLine>(this.flexLines.size());
        for (final FlexLine flexLine : this.flexLines) {
            if (flexLine.getItemCountNotGone() == 0) {
                continue;
            }
            list.add(flexLine);
        }
        return list;
    }
    
    public List<FlexLine> getFlexLinesInternal() {
        return this.flexLines;
    }
    
    public int getFlexWrap() {
        return this.flexWrap;
    }
    
    public int getLargestMainSize() {
        final Iterator<FlexLine> iterator = this.flexLines.iterator();
        int max = Integer.MIN_VALUE;
        while (iterator.hasNext()) {
            max = Math.max(max, iterator.next().mainSize);
        }
        return max;
    }
    
    public View getReorderedChildAt(final int n) {
        if (n >= 0) {
            final int[] reorderedIndices = this.reorderedIndices;
            if (n < reorderedIndices.length) {
                return this.getChildAt(reorderedIndices[n]);
            }
        }
        return null;
    }
    
    public View getReorderedFlexItemAt(final int n) {
        return this.getReorderedChildAt(n);
    }
    
    public int getSumOfCrossSize() {
        final int size = this.flexLines.size();
        int i = 0;
        int n = 0;
        while (i < size) {
            final FlexLine flexLine = this.flexLines.get(i);
            int n2 = n;
            if (this.hasDividerBeforeFlexLine(i)) {
                int n3;
                if (this.isMainAxisDirectionHorizontal()) {
                    n3 = this.dividerHorizontalHeight;
                }
                else {
                    n3 = this.dividerVerticalWidth;
                }
                n2 = n + n3;
            }
            int n4 = n2;
            if (this.hasEndDividerAfterFlexLine(i)) {
                int n5;
                if (this.isMainAxisDirectionHorizontal()) {
                    n5 = this.dividerHorizontalHeight;
                }
                else {
                    n5 = this.dividerVerticalWidth;
                }
                n4 = n2 + n5;
            }
            n = n4 + flexLine.crossSize;
            ++i;
        }
        return n;
    }
    
    public boolean isMainAxisDirectionHorizontal() {
        return true;
    }
    
    protected void onDraw(final Canvas canvas) {
        if (this.dividerDrawableVertical == null && this.dividerDrawableHorizontal == null) {
            return;
        }
        if (this.showDividerHorizontal == 0 && this.showDividerVertical == 0) {
            return;
        }
        final int layoutDirection = ViewCompat.getLayoutDirection((View)this);
        boolean b = true;
        if (layoutDirection != 1) {
            b = false;
        }
        this.drawDividersHorizontal(canvas, b);
    }
    
    protected void onLayout(final boolean b, final int n, int layoutDirection, final int n2, final int n3) {
        layoutDirection = ViewCompat.getLayoutDirection((View)this);
        boolean b2 = true;
        if (layoutDirection != 1) {
            b2 = false;
        }
        this.layoutHorizontal(b2, n, n2);
    }
    
    protected void onMeasure(final int n, final int n2) {
        if (this.orderCache == null) {
            this.orderCache = new SparseIntArray(this.getChildCount());
        }
        if (this.flexboxHelper.isOrderChangedFromLastMeasurement(this.orderCache)) {
            this.reorderedIndices = this.flexboxHelper.createReorderedIndices(this.orderCache);
        }
        this.measureHorizontal(n, n2);
    }
    
    public void onNewFlexItemAdded(final View view, final int n, final int n2, final FlexLine flexLine) {
        if (this.hasDividerBeforeChildAtAlongMainAxis(n, n2)) {
            if (this.isMainAxisDirectionHorizontal()) {
                flexLine.mainSize += this.dividerVerticalWidth;
                flexLine.dividerLengthInMainSize += this.dividerVerticalWidth;
                return;
            }
            flexLine.mainSize += this.dividerHorizontalHeight;
            flexLine.dividerLengthInMainSize += this.dividerHorizontalHeight;
        }
    }
    
    public void onNewFlexLineAdded(final FlexLine flexLine) {
        if (this.isMainAxisDirectionHorizontal()) {
            if ((this.showDividerVertical & 0x4) > 0) {
                flexLine.mainSize += this.dividerVerticalWidth;
                flexLine.dividerLengthInMainSize += this.dividerVerticalWidth;
            }
        }
        else if ((this.showDividerHorizontal & 0x4) > 0) {
            flexLine.mainSize += this.dividerHorizontalHeight;
            flexLine.dividerLengthInMainSize += this.dividerHorizontalHeight;
        }
    }
    
    public void setDividerDrawable(final Drawable drawable) {
        this.setDividerDrawableHorizontal(drawable);
        this.setDividerDrawableVertical(drawable);
    }
    
    public void setDividerDrawableHorizontal(final Drawable dividerDrawableHorizontal) {
        if (dividerDrawableHorizontal == this.dividerDrawableHorizontal) {
            return;
        }
        if ((this.dividerDrawableHorizontal = dividerDrawableHorizontal) != null) {
            this.dividerHorizontalHeight = dividerDrawableHorizontal.getIntrinsicHeight();
        }
        else {
            this.dividerHorizontalHeight = 0;
        }
        this.setWillNotDrawFlag();
        this.requestLayout();
    }
    
    public void setDividerDrawableVertical(final Drawable dividerDrawableVertical) {
        if (dividerDrawableVertical == this.dividerDrawableVertical) {
            return;
        }
        if ((this.dividerDrawableVertical = dividerDrawableVertical) != null) {
            this.dividerVerticalWidth = dividerDrawableVertical.getIntrinsicWidth();
        }
        else {
            this.dividerVerticalWidth = 0;
        }
        this.setWillNotDrawFlag();
        this.requestLayout();
    }
    
    public void setFlexLines(final List<FlexLine> flexLines) {
        this.flexLines = flexLines;
    }
    
    public void setFlexWrap(final int flexWrap) {
        if (this.flexWrap != flexWrap) {
            this.flexWrap = flexWrap;
            this.requestLayout();
        }
    }
    
    public void setShowDivider(final int n) {
        this.setShowDividerVertical(n);
        this.setShowDividerHorizontal(n);
    }
    
    public void setShowDividerHorizontal(final int showDividerHorizontal) {
        if (showDividerHorizontal != this.showDividerHorizontal) {
            this.showDividerHorizontal = showDividerHorizontal;
            this.requestLayout();
        }
    }
    
    public void setShowDividerVertical(final int showDividerVertical) {
        if (showDividerVertical != this.showDividerVertical) {
            this.showDividerVertical = showDividerVertical;
            this.requestLayout();
        }
    }
    
    public void updateViewCache(final int n, final View view) {
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface DividerMode {
    }
    
    public static class LayoutParams extends ViewGroup$MarginLayoutParams implements FlexItem
    {
        public static final Parcelable$Creator<LayoutParams> CREATOR;
        private float flexBasisPercent;
        private float flexGrow;
        private float flexShrink;
        private int maxHeight;
        private int maxWidth;
        private int minHeight;
        private int minWidth;
        private int order;
        private boolean wrapBefore;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator<LayoutParams>() {
                public LayoutParams createFromParcel(final Parcel parcel) {
                    return new LayoutParams(parcel);
                }
                
                public LayoutParams[] newArray(final int n) {
                    return new LayoutParams[n];
                }
            };
        }
        
        public LayoutParams(final int n, final int n2) {
            super(new ViewGroup$LayoutParams(n, n2));
            this.order = 1;
            this.flexGrow = 0.0f;
            this.flexShrink = 1.0f;
            this.flexBasisPercent = -1.0f;
            this.maxWidth = 16777215;
            this.maxHeight = 16777215;
        }
        
        public LayoutParams(final Context context, final AttributeSet set) {
            super(context, set);
            this.order = 1;
            this.flexGrow = 0.0f;
            this.flexShrink = 1.0f;
            this.flexBasisPercent = -1.0f;
            this.maxWidth = 16777215;
            this.maxHeight = 16777215;
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.MaterialComponents_FlexboxLayout_Layout);
            this.order = obtainStyledAttributes.getInt(R.styleable.MaterialComponents_FlexboxLayout_Layout_layout_order, 1);
            this.flexGrow = obtainStyledAttributes.getFloat(R.styleable.MaterialComponents_FlexboxLayout_Layout_layout_flexGrow, 0.0f);
            this.flexShrink = obtainStyledAttributes.getFloat(R.styleable.MaterialComponents_FlexboxLayout_Layout_layout_flexShrink, 1.0f);
            this.flexBasisPercent = obtainStyledAttributes.getFraction(R.styleable.MaterialComponents_FlexboxLayout_Layout_layout_flexBasisPercent, 1, 1, -1.0f);
            this.minWidth = obtainStyledAttributes.getDimensionPixelSize(R.styleable.MaterialComponents_FlexboxLayout_Layout_layout_minWidth, 0);
            this.minHeight = obtainStyledAttributes.getDimensionPixelSize(R.styleable.MaterialComponents_FlexboxLayout_Layout_layout_minHeight, 0);
            this.maxWidth = obtainStyledAttributes.getDimensionPixelSize(R.styleable.MaterialComponents_FlexboxLayout_Layout_layout_maxWidth, 16777215);
            this.maxHeight = obtainStyledAttributes.getDimensionPixelSize(R.styleable.MaterialComponents_FlexboxLayout_Layout_layout_maxHeight, 16777215);
            this.wrapBefore = obtainStyledAttributes.getBoolean(R.styleable.MaterialComponents_FlexboxLayout_Layout_layout_wrapBefore, false);
            obtainStyledAttributes.recycle();
        }
        
        protected LayoutParams(final Parcel parcel) {
            boolean wrapBefore = false;
            super(0, 0);
            this.order = 1;
            this.flexGrow = 0.0f;
            this.flexShrink = 1.0f;
            this.flexBasisPercent = -1.0f;
            this.maxWidth = 16777215;
            this.maxHeight = 16777215;
            this.order = parcel.readInt();
            this.flexGrow = parcel.readFloat();
            this.flexShrink = parcel.readFloat();
            this.flexBasisPercent = parcel.readFloat();
            this.minWidth = parcel.readInt();
            this.minHeight = parcel.readInt();
            this.maxWidth = parcel.readInt();
            this.maxHeight = parcel.readInt();
            if (parcel.readByte() != 0) {
                wrapBefore = true;
            }
            this.wrapBefore = wrapBefore;
            this.bottomMargin = parcel.readInt();
            this.leftMargin = parcel.readInt();
            this.rightMargin = parcel.readInt();
            this.topMargin = parcel.readInt();
            this.height = parcel.readInt();
            this.width = parcel.readInt();
        }
        
        public LayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
            this.order = 1;
            this.flexGrow = 0.0f;
            this.flexShrink = 1.0f;
            this.flexBasisPercent = -1.0f;
            this.maxWidth = 16777215;
            this.maxHeight = 16777215;
        }
        
        public LayoutParams(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            super(viewGroup$MarginLayoutParams);
            this.order = 1;
            this.flexGrow = 0.0f;
            this.flexShrink = 1.0f;
            this.flexBasisPercent = -1.0f;
            this.maxWidth = 16777215;
            this.maxHeight = 16777215;
        }
        
        public LayoutParams(final LayoutParams layoutParams) {
            super((ViewGroup$MarginLayoutParams)layoutParams);
            this.order = 1;
            this.flexGrow = 0.0f;
            this.flexShrink = 1.0f;
            this.flexBasisPercent = -1.0f;
            this.maxWidth = 16777215;
            this.maxHeight = 16777215;
            this.order = layoutParams.order;
            this.flexGrow = layoutParams.flexGrow;
            this.flexShrink = layoutParams.flexShrink;
            this.flexBasisPercent = layoutParams.flexBasisPercent;
            this.minWidth = layoutParams.minWidth;
            this.minHeight = layoutParams.minHeight;
            this.maxWidth = layoutParams.maxWidth;
            this.maxHeight = layoutParams.maxHeight;
            this.wrapBefore = layoutParams.wrapBefore;
        }
        
        public int describeContents() {
            return 0;
        }
        
        public float getFlexBasisPercent() {
            return this.flexBasisPercent;
        }
        
        public float getFlexGrow() {
            return this.flexGrow;
        }
        
        public float getFlexShrink() {
            return this.flexShrink;
        }
        
        public int getHeight() {
            return this.height;
        }
        
        public int getMarginBottom() {
            return this.bottomMargin;
        }
        
        public int getMarginLeft() {
            return this.leftMargin;
        }
        
        public int getMarginRight() {
            return this.rightMargin;
        }
        
        public int getMarginTop() {
            return this.topMargin;
        }
        
        public int getMaxHeight() {
            return this.maxHeight;
        }
        
        public int getMaxWidth() {
            return this.maxWidth;
        }
        
        public int getMinHeight() {
            return this.minHeight;
        }
        
        public int getMinWidth() {
            return this.minWidth;
        }
        
        public int getOrder() {
            return this.order;
        }
        
        public int getWidth() {
            return this.width;
        }
        
        public boolean isWrapBefore() {
            return this.wrapBefore;
        }
        
        public void setFlexBasisPercent(final float flexBasisPercent) {
            this.flexBasisPercent = flexBasisPercent;
        }
        
        public void setFlexGrow(final float flexGrow) {
            this.flexGrow = flexGrow;
        }
        
        public void setFlexShrink(final float flexShrink) {
            this.flexShrink = flexShrink;
        }
        
        public void setHeight(final int height) {
            this.height = height;
        }
        
        public void setMaxHeight(final int maxHeight) {
            this.maxHeight = maxHeight;
        }
        
        public void setMaxWidth(final int maxWidth) {
            this.maxWidth = maxWidth;
        }
        
        public void setMinHeight(final int minHeight) {
            this.minHeight = minHeight;
        }
        
        public void setMinWidth(final int minWidth) {
            this.minWidth = minWidth;
        }
        
        public void setOrder(final int order) {
            this.order = order;
        }
        
        public void setWidth(final int width) {
            this.width = width;
        }
        
        public void setWrapBefore(final boolean wrapBefore) {
            this.wrapBefore = wrapBefore;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            throw new Runtime("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.provideAs(TypeTransformer.java:780)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.e1expr(TypeTransformer.java:496)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:713)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.enexpr(TypeTransformer.java:698)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:719)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.exExpr(TypeTransformer.java:703)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.s1stmt(TypeTransformer.java:810)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.sxStmt(TypeTransformer.java:840)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:206)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:162)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\n");
        }
    }
}
