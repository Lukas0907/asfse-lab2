// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.internal;

import android.util.AttributeSet;
import android.content.Context;
import android.widget.ImageButton;

public class VisibilityAwareImageButton extends ImageButton
{
    private int userSetVisibility;
    
    public VisibilityAwareImageButton(final Context context) {
        this(context, null);
    }
    
    public VisibilityAwareImageButton(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public VisibilityAwareImageButton(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.userSetVisibility = this.getVisibility();
    }
    
    public final int getUserSetVisibility() {
        return this.userSetVisibility;
    }
    
    public final void internalSetVisibility(final int n, final boolean b) {
        super.setVisibility(n);
        if (b) {
            this.userSetVisibility = n;
        }
    }
    
    public void setVisibility(final int n) {
        this.internalSetVisibility(n, true);
    }
}
