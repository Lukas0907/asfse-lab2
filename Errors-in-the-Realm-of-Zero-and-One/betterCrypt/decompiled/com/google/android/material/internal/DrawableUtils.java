// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.internal;

import android.graphics.drawable.Drawable$ConstantState;
import android.graphics.drawable.DrawableContainer;
import java.lang.reflect.Method;

public class DrawableUtils
{
    private static final String LOG_TAG = "DrawableUtils";
    private static Method setConstantStateMethod;
    private static boolean setConstantStateMethodFetched;
    
    private DrawableUtils() {
    }
    
    public static boolean setContainerConstantState(final DrawableContainer drawableContainer, final Drawable$ConstantState drawable$ConstantState) {
        return setContainerConstantStateV9(drawableContainer, drawable$ConstantState);
    }
    
    private static boolean setContainerConstantStateV9(final DrawableContainer p0, final Drawable$ConstantState p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: ifne            47
        //     6: ldc             Landroid/graphics/drawable/DrawableContainer;.class
        //     8: ldc             "setConstantState"
        //    10: iconst_1       
        //    11: anewarray       Ljava/lang/Class;
        //    14: dup            
        //    15: iconst_0       
        //    16: ldc             Landroid/graphics/drawable/DrawableContainer$DrawableContainerState;.class
        //    18: aastore        
        //    19: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //    22: putstatic       com/google/android/material/internal/DrawableUtils.setConstantStateMethod:Ljava/lang/reflect/Method;
        //    25: getstatic       com/google/android/material/internal/DrawableUtils.setConstantStateMethod:Ljava/lang/reflect/Method;
        //    28: iconst_1       
        //    29: invokevirtual   java/lang/reflect/Method.setAccessible:(Z)V
        //    32: goto            43
        //    35: ldc             "DrawableUtils"
        //    37: ldc             "Could not fetch setConstantState(). Oh well."
        //    39: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //    42: pop            
        //    43: iconst_1       
        //    44: putstatic       com/google/android/material/internal/DrawableUtils.setConstantStateMethodFetched:Z
        //    47: getstatic       com/google/android/material/internal/DrawableUtils.setConstantStateMethod:Ljava/lang/reflect/Method;
        //    50: astore_2       
        //    51: aload_2        
        //    52: ifnull          79
        //    55: aload_2        
        //    56: aload_0        
        //    57: iconst_1       
        //    58: anewarray       Ljava/lang/Object;
        //    61: dup            
        //    62: iconst_0       
        //    63: aload_1        
        //    64: aastore        
        //    65: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //    68: pop            
        //    69: iconst_1       
        //    70: ireturn        
        //    71: ldc             "DrawableUtils"
        //    73: ldc             "Could not invoke setConstantState(). Oh well."
        //    75: invokestatic    android/util/Log.e:(Ljava/lang/String;Ljava/lang/String;)I
        //    78: pop            
        //    79: iconst_0       
        //    80: ireturn        
        //    81: astore_2       
        //    82: goto            35
        //    85: astore_0       
        //    86: goto            71
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                             
        //  -----  -----  -----  -----  ---------------------------------
        //  6      32     81     43     Ljava/lang/NoSuchMethodException;
        //  55     69     85     79     Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0071:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:782)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:675)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:552)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:519)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:161)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:150)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:125)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:330)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:251)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:141)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}
