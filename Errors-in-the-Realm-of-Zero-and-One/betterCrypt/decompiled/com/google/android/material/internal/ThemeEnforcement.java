// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.internal;

import androidx.appcompat.widget.TintTypedArray;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.content.Context;
import com.google.android.material.R;

public final class ThemeEnforcement
{
    private static final int[] APPCOMPAT_CHECK_ATTRS;
    private static final String APPCOMPAT_THEME_NAME = "Theme.AppCompat";
    private static final int[] MATERIAL_CHECK_ATTRS;
    private static final String MATERIAL_THEME_NAME = "Theme.MaterialComponents";
    
    static {
        APPCOMPAT_CHECK_ATTRS = new int[] { R.attr.colorPrimary };
        MATERIAL_CHECK_ATTRS = new int[] { R.attr.colorSecondary };
    }
    
    private ThemeEnforcement() {
    }
    
    public static void checkAppCompatTheme(final Context context) {
        checkTheme(context, ThemeEnforcement.APPCOMPAT_CHECK_ATTRS, "Theme.AppCompat");
    }
    
    private static void checkCompatibleTheme(final Context context, final AttributeSet set, final int n, final int n2) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.ThemeEnforcement, n, n2);
        final boolean boolean1 = obtainStyledAttributes.getBoolean(R.styleable.ThemeEnforcement_enforceMaterialTheme, false);
        obtainStyledAttributes.recycle();
        if (boolean1) {
            checkMaterialTheme(context);
        }
        checkAppCompatTheme(context);
    }
    
    public static void checkMaterialTheme(final Context context) {
        checkTheme(context, ThemeEnforcement.MATERIAL_CHECK_ATTRS, "Theme.MaterialComponents");
    }
    
    private static void checkTextAppearance(final Context context, final AttributeSet set, int resourceId, final int n) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.ThemeEnforcement, resourceId, n);
        if (!obtainStyledAttributes.getBoolean(R.styleable.ThemeEnforcement_enforceTextAppearance, false)) {
            obtainStyledAttributes.recycle();
            return;
        }
        resourceId = obtainStyledAttributes.getResourceId(R.styleable.ThemeEnforcement_android_textAppearance, -1);
        obtainStyledAttributes.recycle();
        if (resourceId != -1) {
            return;
        }
        throw new IllegalArgumentException("This component requires that you specify a valid android:textAppearance attribute.");
    }
    
    private static void checkTheme(final Context context, final int[] array, final String str) {
        if (isTheme(context, array)) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("The style on this component requires your app theme to be ");
        sb.append(str);
        sb.append(" (or a descendant).");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static boolean isAppCompatTheme(final Context context) {
        return isTheme(context, ThemeEnforcement.APPCOMPAT_CHECK_ATTRS);
    }
    
    public static boolean isMaterialTheme(final Context context) {
        return isTheme(context, ThemeEnforcement.MATERIAL_CHECK_ATTRS);
    }
    
    private static boolean isTheme(final Context context, final int[] array) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(array);
        final boolean hasValue = obtainStyledAttributes.hasValue(0);
        obtainStyledAttributes.recycle();
        return hasValue;
    }
    
    public static TypedArray obtainStyledAttributes(final Context context, final AttributeSet set, final int[] array, final int n, final int n2) {
        checkCompatibleTheme(context, set, n, n2);
        checkTextAppearance(context, set, n, n2);
        return context.obtainStyledAttributes(set, array, n, n2);
    }
    
    public static TintTypedArray obtainTintedStyledAttributes(final Context context, final AttributeSet set, final int[] array, final int n, final int n2) {
        checkCompatibleTheme(context, set, n, n2);
        checkTextAppearance(context, set, n, n2);
        return TintTypedArray.obtainStyledAttributes(context, set, array, n, n2);
    }
}
