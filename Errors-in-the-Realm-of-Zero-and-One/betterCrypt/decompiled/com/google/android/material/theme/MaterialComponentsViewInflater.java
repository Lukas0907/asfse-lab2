// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.theme;

import com.google.android.material.button.MaterialButton;
import androidx.appcompat.widget.AppCompatButton;
import android.util.AttributeSet;
import android.content.Context;
import androidx.appcompat.app.AppCompatViewInflater;

public class MaterialComponentsViewInflater extends AppCompatViewInflater
{
    @Override
    protected AppCompatButton createButton(final Context context, final AttributeSet set) {
        return new MaterialButton(context, set);
    }
}
