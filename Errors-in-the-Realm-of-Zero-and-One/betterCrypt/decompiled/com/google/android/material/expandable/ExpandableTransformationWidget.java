// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.expandable;

public interface ExpandableTransformationWidget extends ExpandableWidget
{
    int getExpandedComponentIdHint();
    
    void setExpandedComponentIdHint(final int p0);
}
