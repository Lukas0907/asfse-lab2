// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.expandable;

public interface ExpandableWidget
{
    boolean isExpanded();
    
    boolean setExpanded(final boolean p0);
}
