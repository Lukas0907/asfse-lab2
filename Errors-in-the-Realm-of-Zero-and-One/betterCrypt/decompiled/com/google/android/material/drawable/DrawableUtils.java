// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.drawable;

import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;

public class DrawableUtils
{
    private DrawableUtils() {
    }
    
    public static PorterDuffColorFilter updateTintFilter(final Drawable drawable, final ColorStateList list, final PorterDuff$Mode porterDuff$Mode) {
        if (list != null && porterDuff$Mode != null) {
            return new PorterDuffColorFilter(list.getColorForState(drawable.getState(), 0), porterDuff$Mode);
        }
        return null;
    }
}
