// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.shadow;

import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.RadialGradient;
import android.graphics.Shader$TileMode;
import android.graphics.Path$FillType;
import android.graphics.Rect;
import android.graphics.Paint$Style;
import androidx.core.content.ContextCompat;
import com.google.android.material.R;
import android.graphics.drawable.Drawable;
import android.content.Context;
import android.graphics.Path;
import android.graphics.Paint;
import android.graphics.RectF;
import androidx.appcompat.graphics.drawable.DrawableWrapper;

public class ShadowDrawableWrapper extends DrawableWrapper
{
    static final double COS_45;
    static final float SHADOW_BOTTOM_SCALE = 1.0f;
    static final float SHADOW_HORIZ_SCALE = 0.5f;
    static final float SHADOW_MULTIPLIER = 1.5f;
    static final float SHADOW_TOP_SCALE = 0.25f;
    private boolean addPaddingForCorners;
    final RectF contentBounds;
    float cornerRadius;
    final Paint cornerShadowPaint;
    Path cornerShadowPath;
    private boolean dirty;
    final Paint edgeShadowPaint;
    float maxShadowSize;
    private boolean printedShadowClipWarning;
    float rawMaxShadowSize;
    float rawShadowSize;
    private float rotation;
    private final int shadowEndColor;
    private final int shadowMiddleColor;
    float shadowSize;
    private final int shadowStartColor;
    
    static {
        COS_45 = Math.cos(Math.toRadians(45.0));
    }
    
    public ShadowDrawableWrapper(final Context context, final Drawable drawable, final float a, final float n, final float n2) {
        super(drawable);
        this.dirty = true;
        this.addPaddingForCorners = true;
        this.printedShadowClipWarning = false;
        this.shadowStartColor = ContextCompat.getColor(context, R.color.design_fab_shadow_start_color);
        this.shadowMiddleColor = ContextCompat.getColor(context, R.color.design_fab_shadow_mid_color);
        this.shadowEndColor = ContextCompat.getColor(context, R.color.design_fab_shadow_end_color);
        (this.cornerShadowPaint = new Paint(5)).setStyle(Paint$Style.FILL);
        this.cornerRadius = (float)Math.round(a);
        this.contentBounds = new RectF();
        (this.edgeShadowPaint = new Paint(this.cornerShadowPaint)).setAntiAlias(false);
        this.setShadowSize(n, n2);
    }
    
    private void buildComponents(final Rect rect) {
        final float n = this.rawMaxShadowSize * 1.5f;
        this.contentBounds.set(rect.left + this.rawMaxShadowSize, rect.top + n, rect.right - this.rawMaxShadowSize, rect.bottom - n);
        this.getWrappedDrawable().setBounds((int)this.contentBounds.left, (int)this.contentBounds.top, (int)this.contentBounds.right, (int)this.contentBounds.bottom);
        this.buildShadowCorners();
    }
    
    private void buildShadowCorners() {
        final float cornerRadius = this.cornerRadius;
        final RectF rectF = new RectF(-cornerRadius, -cornerRadius, cornerRadius, cornerRadius);
        final RectF rectF2 = new RectF(rectF);
        final float shadowSize = this.shadowSize;
        rectF2.inset(-shadowSize, -shadowSize);
        final Path cornerShadowPath = this.cornerShadowPath;
        if (cornerShadowPath == null) {
            this.cornerShadowPath = new Path();
        }
        else {
            cornerShadowPath.reset();
        }
        this.cornerShadowPath.setFillType(Path$FillType.EVEN_ODD);
        this.cornerShadowPath.moveTo(-this.cornerRadius, 0.0f);
        this.cornerShadowPath.rLineTo(-this.shadowSize, 0.0f);
        this.cornerShadowPath.arcTo(rectF2, 180.0f, 90.0f, false);
        this.cornerShadowPath.arcTo(rectF, 270.0f, -90.0f, false);
        this.cornerShadowPath.close();
        final float n = -rectF2.top;
        if (n > 0.0f) {
            final float n2 = this.cornerRadius / n;
            this.cornerShadowPaint.setShader((Shader)new RadialGradient(0.0f, 0.0f, n, new int[] { 0, this.shadowStartColor, this.shadowMiddleColor, this.shadowEndColor }, new float[] { 0.0f, n2, (1.0f - n2) / 2.0f + n2, 1.0f }, Shader$TileMode.CLAMP));
        }
        this.edgeShadowPaint.setShader((Shader)new LinearGradient(0.0f, rectF.top, 0.0f, rectF2.top, new int[] { this.shadowStartColor, this.shadowMiddleColor, this.shadowEndColor }, new float[] { 0.0f, 0.5f, 1.0f }, Shader$TileMode.CLAMP));
        this.edgeShadowPaint.setAntiAlias(false);
    }
    
    public static float calculateHorizontalPadding(final float n, final float n2, final boolean b) {
        float n3 = n;
        if (b) {
            n3 = (float)(n + (1.0 - ShadowDrawableWrapper.COS_45) * n2);
        }
        return n3;
    }
    
    public static float calculateVerticalPadding(final float n, final float n2, final boolean b) {
        if (b) {
            return (float)(n * 1.5f + (1.0 - ShadowDrawableWrapper.COS_45) * n2);
        }
        return n * 1.5f;
    }
    
    private void drawShadow(final Canvas canvas) {
        final int save = canvas.save();
        canvas.rotate(this.rotation, this.contentBounds.centerX(), this.contentBounds.centerY());
        final float cornerRadius = this.cornerRadius;
        final float n = -cornerRadius - this.shadowSize;
        final float width = this.contentBounds.width();
        final float n2 = cornerRadius * 2.0f;
        final boolean b = width - n2 > 0.0f;
        final boolean b2 = this.contentBounds.height() - n2 > 0.0f;
        final float rawShadowSize = this.rawShadowSize;
        final float n3 = cornerRadius / (rawShadowSize - 0.5f * rawShadowSize + cornerRadius);
        final float n4 = cornerRadius / (rawShadowSize - 0.25f * rawShadowSize + cornerRadius);
        final float n5 = cornerRadius / (rawShadowSize - rawShadowSize * 1.0f + cornerRadius);
        final int save2 = canvas.save();
        canvas.translate(this.contentBounds.left + cornerRadius, this.contentBounds.top + cornerRadius);
        canvas.scale(n3, n4);
        canvas.drawPath(this.cornerShadowPath, this.cornerShadowPaint);
        if (b) {
            canvas.scale(1.0f / n3, 1.0f);
            canvas.drawRect(0.0f, n, this.contentBounds.width() - n2, -this.cornerRadius, this.edgeShadowPaint);
        }
        canvas.restoreToCount(save2);
        final int save3 = canvas.save();
        canvas.translate(this.contentBounds.right - cornerRadius, this.contentBounds.bottom - cornerRadius);
        canvas.scale(n3, n5);
        canvas.rotate(180.0f);
        canvas.drawPath(this.cornerShadowPath, this.cornerShadowPaint);
        if (b) {
            canvas.scale(1.0f / n3, 1.0f);
            canvas.drawRect(0.0f, n, this.contentBounds.width() - n2, -this.cornerRadius + this.shadowSize, this.edgeShadowPaint);
        }
        canvas.restoreToCount(save3);
        final int save4 = canvas.save();
        canvas.translate(this.contentBounds.left + cornerRadius, this.contentBounds.bottom - cornerRadius);
        canvas.scale(n3, n5);
        canvas.rotate(270.0f);
        canvas.drawPath(this.cornerShadowPath, this.cornerShadowPaint);
        if (b2) {
            canvas.scale(1.0f / n5, 1.0f);
            canvas.drawRect(0.0f, n, this.contentBounds.height() - n2, -this.cornerRadius, this.edgeShadowPaint);
        }
        canvas.restoreToCount(save4);
        final int save5 = canvas.save();
        canvas.translate(this.contentBounds.right - cornerRadius, this.contentBounds.top + cornerRadius);
        canvas.scale(n3, n4);
        canvas.rotate(90.0f);
        canvas.drawPath(this.cornerShadowPath, this.cornerShadowPaint);
        if (b2) {
            canvas.scale(1.0f / n4, 1.0f);
            canvas.drawRect(0.0f, n, this.contentBounds.height() - n2, -this.cornerRadius, this.edgeShadowPaint);
        }
        canvas.restoreToCount(save5);
        canvas.restoreToCount(save);
    }
    
    private static int toEven(final float a) {
        int round;
        final int n = round = Math.round(a);
        if (n % 2 == 1) {
            round = n - 1;
        }
        return round;
    }
    
    @Override
    public void draw(final Canvas canvas) {
        if (this.dirty) {
            this.buildComponents(this.getBounds());
            this.dirty = false;
        }
        this.drawShadow(canvas);
        super.draw(canvas);
    }
    
    public float getCornerRadius() {
        return this.cornerRadius;
    }
    
    public float getMaxShadowSize() {
        return this.rawMaxShadowSize;
    }
    
    public float getMinHeight() {
        final float rawMaxShadowSize = this.rawMaxShadowSize;
        return Math.max(rawMaxShadowSize, this.cornerRadius + rawMaxShadowSize * 1.5f / 2.0f) * 2.0f + this.rawMaxShadowSize * 1.5f * 2.0f;
    }
    
    public float getMinWidth() {
        final float rawMaxShadowSize = this.rawMaxShadowSize;
        return Math.max(rawMaxShadowSize, this.cornerRadius + rawMaxShadowSize / 2.0f) * 2.0f + this.rawMaxShadowSize * 2.0f;
    }
    
    @Override
    public int getOpacity() {
        return -3;
    }
    
    @Override
    public boolean getPadding(final Rect rect) {
        final int n = (int)Math.ceil(calculateVerticalPadding(this.rawMaxShadowSize, this.cornerRadius, this.addPaddingForCorners));
        final int n2 = (int)Math.ceil(calculateHorizontalPadding(this.rawMaxShadowSize, this.cornerRadius, this.addPaddingForCorners));
        rect.set(n2, n, n2, n);
        return true;
    }
    
    public float getShadowSize() {
        return this.rawShadowSize;
    }
    
    @Override
    protected void onBoundsChange(final Rect rect) {
        this.dirty = true;
    }
    
    public void setAddPaddingForCorners(final boolean addPaddingForCorners) {
        this.addPaddingForCorners = addPaddingForCorners;
        this.invalidateSelf();
    }
    
    @Override
    public void setAlpha(final int alpha) {
        super.setAlpha(alpha);
        this.cornerShadowPaint.setAlpha(alpha);
        this.edgeShadowPaint.setAlpha(alpha);
    }
    
    public void setCornerRadius(float n) {
        n = (float)Math.round(n);
        if (this.cornerRadius == n) {
            return;
        }
        this.cornerRadius = n;
        this.dirty = true;
        this.invalidateSelf();
    }
    
    public void setMaxShadowSize(final float n) {
        this.setShadowSize(this.rawShadowSize, n);
    }
    
    public final void setRotation(final float rotation) {
        if (this.rotation != rotation) {
            this.rotation = rotation;
            this.invalidateSelf();
        }
    }
    
    public void setShadowSize(final float n) {
        this.setShadowSize(n, this.rawMaxShadowSize);
    }
    
    public void setShadowSize(float rawShadowSize, float n) {
        if (rawShadowSize < 0.0f || n < 0.0f) {
            throw new IllegalArgumentException("invalid shadow size");
        }
        final float n2 = (float)toEven(rawShadowSize);
        n = (float)toEven(n);
        rawShadowSize = n2;
        if (n2 > n) {
            if (!this.printedShadowClipWarning) {
                this.printedShadowClipWarning = true;
            }
            rawShadowSize = n;
        }
        if (this.rawShadowSize == rawShadowSize && this.rawMaxShadowSize == n) {
            return;
        }
        this.rawShadowSize = rawShadowSize;
        this.rawMaxShadowSize = n;
        this.shadowSize = (float)Math.round(rawShadowSize * 1.5f);
        this.maxShadowSize = n;
        this.dirty = true;
        this.invalidateSelf();
    }
}
