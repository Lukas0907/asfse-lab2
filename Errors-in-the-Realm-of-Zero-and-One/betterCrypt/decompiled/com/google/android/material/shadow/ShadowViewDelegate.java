// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.shadow;

import android.graphics.drawable.Drawable;

public interface ShadowViewDelegate
{
    float getRadius();
    
    boolean isCompatPaddingEnabled();
    
    void setBackgroundDrawable(final Drawable p0);
    
    void setShadowPadding(final int p0, final int p1, final int p2, final int p3);
}
