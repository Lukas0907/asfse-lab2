// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.animation;

public class Positioning
{
    public final int gravity;
    public final float xAdjustment;
    public final float yAdjustment;
    
    public Positioning(final int gravity, final float xAdjustment, final float yAdjustment) {
        this.gravity = gravity;
        this.xAdjustment = xAdjustment;
        this.yAdjustment = yAdjustment;
    }
}
