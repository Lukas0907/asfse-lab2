// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.button;

import android.graphics.Paint;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.lang.annotation.Annotation;
import androidx.appcompat.content.res.AppCompatResources;
import android.util.Log;
import android.os.Build$VERSION;
import android.graphics.Canvas;
import android.widget.TextView;
import androidx.core.widget.TextViewCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import android.view.View;
import androidx.core.view.ViewCompat;
import android.content.res.TypedArray;
import com.google.android.material.resources.MaterialResources;
import com.google.android.material.internal.ViewUtils;
import com.google.android.material.internal.ThemeEnforcement;
import com.google.android.material.R;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import androidx.appcompat.widget.AppCompatButton;

public class MaterialButton extends AppCompatButton
{
    public static final int ICON_GRAVITY_START = 1;
    public static final int ICON_GRAVITY_TEXT_START = 2;
    private static final String LOG_TAG = "MaterialButton";
    private Drawable icon;
    private int iconGravity;
    private int iconLeft;
    private int iconPadding;
    private int iconSize;
    private ColorStateList iconTint;
    private PorterDuff$Mode iconTintMode;
    private final MaterialButtonHelper materialButtonHelper;
    
    public MaterialButton(final Context context) {
        this(context, null);
    }
    
    public MaterialButton(final Context context, final AttributeSet set) {
        this(context, set, R.attr.materialButtonStyle);
    }
    
    public MaterialButton(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        final TypedArray obtainStyledAttributes = ThemeEnforcement.obtainStyledAttributes(context, set, R.styleable.MaterialButton, n, R.style.Widget_MaterialComponents_Button);
        this.iconPadding = obtainStyledAttributes.getDimensionPixelSize(R.styleable.MaterialButton_iconPadding, 0);
        this.iconTintMode = ViewUtils.parseTintMode(obtainStyledAttributes.getInt(R.styleable.MaterialButton_iconTintMode, -1), PorterDuff$Mode.SRC_IN);
        this.iconTint = MaterialResources.getColorStateList(this.getContext(), obtainStyledAttributes, R.styleable.MaterialButton_iconTint);
        this.icon = MaterialResources.getDrawable(this.getContext(), obtainStyledAttributes, R.styleable.MaterialButton_icon);
        this.iconGravity = obtainStyledAttributes.getInteger(R.styleable.MaterialButton_iconGravity, 1);
        this.iconSize = obtainStyledAttributes.getDimensionPixelSize(R.styleable.MaterialButton_iconSize, 0);
        (this.materialButtonHelper = new MaterialButtonHelper(this)).loadFromAttributes(obtainStyledAttributes);
        obtainStyledAttributes.recycle();
        this.setCompoundDrawablePadding(this.iconPadding);
        this.updateIcon();
    }
    
    private boolean isLayoutRTL() {
        return ViewCompat.getLayoutDirection((View)this) == 1;
    }
    
    private boolean isUsingOriginalBackground() {
        final MaterialButtonHelper materialButtonHelper = this.materialButtonHelper;
        return materialButtonHelper != null && !materialButtonHelper.isBackgroundOverwritten();
    }
    
    private void updateIcon() {
        final Drawable icon = this.icon;
        if (icon != null) {
            DrawableCompat.setTintList(this.icon = icon.mutate(), this.iconTint);
            final PorterDuff$Mode iconTintMode = this.iconTintMode;
            if (iconTintMode != null) {
                DrawableCompat.setTintMode(this.icon, iconTintMode);
            }
            int n = this.iconSize;
            if (n == 0) {
                n = this.icon.getIntrinsicWidth();
            }
            int n2 = this.iconSize;
            if (n2 == 0) {
                n2 = this.icon.getIntrinsicHeight();
            }
            this.icon.setBounds(0, 0, n, n2);
        }
        TextViewCompat.setCompoundDrawablesRelative((TextView)this, this.icon, null, null, null);
    }
    
    public void draw(final Canvas canvas) {
        super.draw(canvas);
        if (Build$VERSION.SDK_INT < 21 && this.isUsingOriginalBackground()) {
            this.materialButtonHelper.drawStroke(canvas);
        }
    }
    
    public ColorStateList getBackgroundTintList() {
        return this.getSupportBackgroundTintList();
    }
    
    public PorterDuff$Mode getBackgroundTintMode() {
        return this.getSupportBackgroundTintMode();
    }
    
    public int getCornerRadius() {
        if (this.isUsingOriginalBackground()) {
            return this.materialButtonHelper.getCornerRadius();
        }
        return 0;
    }
    
    public Drawable getIcon() {
        return this.icon;
    }
    
    public int getIconGravity() {
        return this.iconGravity;
    }
    
    public int getIconPadding() {
        return this.iconPadding;
    }
    
    public int getIconSize() {
        return this.iconSize;
    }
    
    public ColorStateList getIconTint() {
        return this.iconTint;
    }
    
    public PorterDuff$Mode getIconTintMode() {
        return this.iconTintMode;
    }
    
    public ColorStateList getRippleColor() {
        if (this.isUsingOriginalBackground()) {
            return this.materialButtonHelper.getRippleColor();
        }
        return null;
    }
    
    public ColorStateList getStrokeColor() {
        if (this.isUsingOriginalBackground()) {
            return this.materialButtonHelper.getStrokeColor();
        }
        return null;
    }
    
    public int getStrokeWidth() {
        if (this.isUsingOriginalBackground()) {
            return this.materialButtonHelper.getStrokeWidth();
        }
        return 0;
    }
    
    @Override
    public ColorStateList getSupportBackgroundTintList() {
        if (this.isUsingOriginalBackground()) {
            return this.materialButtonHelper.getSupportBackgroundTintList();
        }
        return super.getSupportBackgroundTintList();
    }
    
    @Override
    public PorterDuff$Mode getSupportBackgroundTintMode() {
        if (this.isUsingOriginalBackground()) {
            return this.materialButtonHelper.getSupportBackgroundTintMode();
        }
        return super.getSupportBackgroundTintMode();
    }
    
    @Override
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        super.onLayout(b, n, n2, n3, n4);
        if (Build$VERSION.SDK_INT == 21) {
            final MaterialButtonHelper materialButtonHelper = this.materialButtonHelper;
            if (materialButtonHelper != null) {
                materialButtonHelper.updateMaskBounds(n4 - n2, n3 - n);
            }
        }
    }
    
    protected void onMeasure(int intrinsicWidth, int iconSize) {
        super.onMeasure(intrinsicWidth, iconSize);
        if (this.icon != null) {
            if (this.iconGravity != 2) {
                return;
            }
            final int n = (int)((Paint)this.getPaint()).measureText(this.getText().toString());
            iconSize = this.iconSize;
            if ((intrinsicWidth = iconSize) == 0) {
                intrinsicWidth = this.icon.getIntrinsicWidth();
            }
            iconSize = (intrinsicWidth = (this.getWidth() - n - ViewCompat.getPaddingEnd((View)this) - intrinsicWidth - this.iconPadding - ViewCompat.getPaddingStart((View)this)) / 2);
            if (this.isLayoutRTL()) {
                intrinsicWidth = -iconSize;
            }
            if (this.iconLeft != intrinsicWidth) {
                this.iconLeft = intrinsicWidth;
                this.updateIcon();
            }
        }
    }
    
    public void setBackground(final Drawable backgroundDrawable) {
        this.setBackgroundDrawable(backgroundDrawable);
    }
    
    public void setBackgroundColor(final int n) {
        if (this.isUsingOriginalBackground()) {
            this.materialButtonHelper.setBackgroundColor(n);
            return;
        }
        super.setBackgroundColor(n);
    }
    
    @Override
    public void setBackgroundDrawable(final Drawable drawable) {
        if (!this.isUsingOriginalBackground()) {
            super.setBackgroundDrawable(drawable);
            return;
        }
        if (drawable != this.getBackground()) {
            Log.i("MaterialButton", "Setting a custom background is not supported.");
            this.materialButtonHelper.setBackgroundOverwritten();
            super.setBackgroundDrawable(drawable);
            return;
        }
        this.getBackground().setState(drawable.getState());
    }
    
    @Override
    public void setBackgroundResource(final int n) {
        Drawable drawable;
        if (n != 0) {
            drawable = AppCompatResources.getDrawable(this.getContext(), n);
        }
        else {
            drawable = null;
        }
        this.setBackgroundDrawable(drawable);
    }
    
    public void setBackgroundTintList(final ColorStateList supportBackgroundTintList) {
        this.setSupportBackgroundTintList(supportBackgroundTintList);
    }
    
    public void setBackgroundTintMode(final PorterDuff$Mode supportBackgroundTintMode) {
        this.setSupportBackgroundTintMode(supportBackgroundTintMode);
    }
    
    public void setCornerRadius(final int cornerRadius) {
        if (this.isUsingOriginalBackground()) {
            this.materialButtonHelper.setCornerRadius(cornerRadius);
        }
    }
    
    public void setCornerRadiusResource(final int n) {
        if (this.isUsingOriginalBackground()) {
            this.setCornerRadius(this.getResources().getDimensionPixelSize(n));
        }
    }
    
    public void setIcon(final Drawable icon) {
        if (this.icon != icon) {
            this.icon = icon;
            this.updateIcon();
        }
    }
    
    public void setIconGravity(final int iconGravity) {
        this.iconGravity = iconGravity;
    }
    
    public void setIconPadding(final int iconPadding) {
        if (this.iconPadding != iconPadding) {
            this.setCompoundDrawablePadding(this.iconPadding = iconPadding);
        }
    }
    
    public void setIconResource(final int n) {
        Drawable drawable;
        if (n != 0) {
            drawable = AppCompatResources.getDrawable(this.getContext(), n);
        }
        else {
            drawable = null;
        }
        this.setIcon(drawable);
    }
    
    public void setIconSize(final int iconSize) {
        if (iconSize >= 0) {
            if (this.iconSize != iconSize) {
                this.iconSize = iconSize;
                this.updateIcon();
            }
            return;
        }
        throw new IllegalArgumentException("iconSize cannot be less than 0");
    }
    
    public void setIconTint(final ColorStateList iconTint) {
        if (this.iconTint != iconTint) {
            this.iconTint = iconTint;
            this.updateIcon();
        }
    }
    
    public void setIconTintMode(final PorterDuff$Mode iconTintMode) {
        if (this.iconTintMode != iconTintMode) {
            this.iconTintMode = iconTintMode;
            this.updateIcon();
        }
    }
    
    public void setIconTintResource(final int n) {
        this.setIconTint(AppCompatResources.getColorStateList(this.getContext(), n));
    }
    
    void setInternalBackground(final Drawable backgroundDrawable) {
        super.setBackgroundDrawable(backgroundDrawable);
    }
    
    public void setRippleColor(final ColorStateList rippleColor) {
        if (this.isUsingOriginalBackground()) {
            this.materialButtonHelper.setRippleColor(rippleColor);
        }
    }
    
    public void setRippleColorResource(final int n) {
        if (this.isUsingOriginalBackground()) {
            this.setRippleColor(AppCompatResources.getColorStateList(this.getContext(), n));
        }
    }
    
    public void setStrokeColor(final ColorStateList strokeColor) {
        if (this.isUsingOriginalBackground()) {
            this.materialButtonHelper.setStrokeColor(strokeColor);
        }
    }
    
    public void setStrokeColorResource(final int n) {
        if (this.isUsingOriginalBackground()) {
            this.setStrokeColor(AppCompatResources.getColorStateList(this.getContext(), n));
        }
    }
    
    public void setStrokeWidth(final int strokeWidth) {
        if (this.isUsingOriginalBackground()) {
            this.materialButtonHelper.setStrokeWidth(strokeWidth);
        }
    }
    
    public void setStrokeWidthResource(final int n) {
        if (this.isUsingOriginalBackground()) {
            this.setStrokeWidth(this.getResources().getDimensionPixelSize(n));
        }
    }
    
    @Override
    public void setSupportBackgroundTintList(final ColorStateList list) {
        if (this.isUsingOriginalBackground()) {
            this.materialButtonHelper.setSupportBackgroundTintList(list);
            return;
        }
        if (this.materialButtonHelper != null) {
            super.setSupportBackgroundTintList(list);
        }
    }
    
    @Override
    public void setSupportBackgroundTintMode(final PorterDuff$Mode porterDuff$Mode) {
        if (this.isUsingOriginalBackground()) {
            this.materialButtonHelper.setSupportBackgroundTintMode(porterDuff$Mode);
            return;
        }
        if (this.materialButtonHelper != null) {
            super.setSupportBackgroundTintMode(porterDuff$Mode);
        }
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface IconGravity {
    }
}
