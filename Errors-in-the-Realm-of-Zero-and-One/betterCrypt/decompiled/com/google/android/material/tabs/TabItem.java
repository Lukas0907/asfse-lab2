// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.tabs;

import androidx.appcompat.widget.TintTypedArray;
import com.google.android.material.R;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;

public class TabItem extends View
{
    public final int customLayout;
    public final Drawable icon;
    public final CharSequence text;
    
    public TabItem(final Context context) {
        this(context, null);
    }
    
    public TabItem(final Context context, final AttributeSet set) {
        super(context, set);
        final TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(context, set, R.styleable.TabItem);
        this.text = obtainStyledAttributes.getText(R.styleable.TabItem_android_text);
        this.icon = obtainStyledAttributes.getDrawable(R.styleable.TabItem_android_icon);
        this.customLayout = obtainStyledAttributes.getResourceId(R.styleable.TabItem_android_layout, 0);
        obtainStyledAttributes.recycle();
    }
}
