// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.card;

import com.google.android.material.R;
import android.content.res.TypedArray;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.Drawable;

class MaterialCardViewHelper
{
    private static final int DEFAULT_STROKE_VALUE = -1;
    private final MaterialCardView materialCardView;
    private int strokeColor;
    private int strokeWidth;
    
    public MaterialCardViewHelper(final MaterialCardView materialCardView) {
        this.materialCardView = materialCardView;
    }
    
    private void adjustContentPadding() {
        this.materialCardView.setContentPadding(this.materialCardView.getContentPaddingLeft() + this.strokeWidth, this.materialCardView.getContentPaddingTop() + this.strokeWidth, this.materialCardView.getContentPaddingRight() + this.strokeWidth, this.materialCardView.getContentPaddingBottom() + this.strokeWidth);
    }
    
    private Drawable createForegroundDrawable() {
        final GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setCornerRadius(this.materialCardView.getRadius());
        final int strokeColor = this.strokeColor;
        if (strokeColor != -1) {
            gradientDrawable.setStroke(this.strokeWidth, strokeColor);
        }
        return (Drawable)gradientDrawable;
    }
    
    int getStrokeColor() {
        return this.strokeColor;
    }
    
    int getStrokeWidth() {
        return this.strokeWidth;
    }
    
    public void loadFromAttributes(final TypedArray typedArray) {
        this.strokeColor = typedArray.getColor(R.styleable.MaterialCardView_strokeColor, -1);
        this.strokeWidth = typedArray.getDimensionPixelSize(R.styleable.MaterialCardView_strokeWidth, 0);
        this.updateForeground();
        this.adjustContentPadding();
    }
    
    void setStrokeColor(final int strokeColor) {
        this.strokeColor = strokeColor;
        this.updateForeground();
    }
    
    void setStrokeWidth(final int strokeWidth) {
        this.strokeWidth = strokeWidth;
        this.updateForeground();
        this.adjustContentPadding();
    }
    
    void updateForeground() {
        this.materialCardView.setForeground(this.createForegroundDrawable());
    }
}
