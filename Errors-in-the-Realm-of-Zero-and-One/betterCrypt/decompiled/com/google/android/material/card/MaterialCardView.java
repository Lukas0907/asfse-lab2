// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.card;

import android.content.res.TypedArray;
import com.google.android.material.internal.ThemeEnforcement;
import com.google.android.material.R;
import android.util.AttributeSet;
import android.content.Context;
import androidx.cardview.widget.CardView;

public class MaterialCardView extends CardView
{
    private final MaterialCardViewHelper cardViewHelper;
    
    public MaterialCardView(final Context context) {
        this(context, null);
    }
    
    public MaterialCardView(final Context context, final AttributeSet set) {
        this(context, set, R.attr.materialCardViewStyle);
    }
    
    public MaterialCardView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        final TypedArray obtainStyledAttributes = ThemeEnforcement.obtainStyledAttributes(context, set, R.styleable.MaterialCardView, n, R.style.Widget_MaterialComponents_CardView);
        (this.cardViewHelper = new MaterialCardViewHelper(this)).loadFromAttributes(obtainStyledAttributes);
        obtainStyledAttributes.recycle();
    }
    
    public int getStrokeColor() {
        return this.cardViewHelper.getStrokeColor();
    }
    
    public int getStrokeWidth() {
        return this.cardViewHelper.getStrokeWidth();
    }
    
    @Override
    public void setRadius(final float radius) {
        super.setRadius(radius);
        this.cardViewHelper.updateForeground();
    }
    
    public void setStrokeColor(final int strokeColor) {
        this.cardViewHelper.setStrokeColor(strokeColor);
    }
    
    public void setStrokeWidth(final int strokeWidth) {
        this.cardViewHelper.setStrokeWidth(strokeWidth);
    }
}
