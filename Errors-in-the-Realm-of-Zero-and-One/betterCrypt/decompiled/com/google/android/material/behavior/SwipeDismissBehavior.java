// 
// Decompiled by Procyon v0.5.36
// 

package com.google.android.material.behavior;

import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.core.view.ViewCompat;
import androidx.customview.widget.ViewDragHelper;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import android.view.View;

public class SwipeDismissBehavior<V extends View> extends Behavior<V>
{
    private static final float DEFAULT_ALPHA_END_DISTANCE = 0.5f;
    private static final float DEFAULT_ALPHA_START_DISTANCE = 0.0f;
    private static final float DEFAULT_DRAG_DISMISS_THRESHOLD = 0.5f;
    public static final int STATE_DRAGGING = 1;
    public static final int STATE_IDLE = 0;
    public static final int STATE_SETTLING = 2;
    public static final int SWIPE_DIRECTION_ANY = 2;
    public static final int SWIPE_DIRECTION_END_TO_START = 1;
    public static final int SWIPE_DIRECTION_START_TO_END = 0;
    float alphaEndSwipeDistance;
    float alphaStartSwipeDistance;
    private final ViewDragHelper.Callback dragCallback;
    float dragDismissThreshold;
    private boolean interceptingEvents;
    OnDismissListener listener;
    private float sensitivity;
    private boolean sensitivitySet;
    int swipeDirection;
    ViewDragHelper viewDragHelper;
    
    public SwipeDismissBehavior() {
        this.sensitivity = 0.0f;
        this.swipeDirection = 2;
        this.dragDismissThreshold = 0.5f;
        this.alphaStartSwipeDistance = 0.0f;
        this.alphaEndSwipeDistance = 0.5f;
        this.dragCallback = new ViewDragHelper.Callback() {
            private static final int INVALID_POINTER_ID = -1;
            private int activePointerId = -1;
            private int originalCapturedViewLeft;
            
            private boolean shouldDismiss(final View view, final float n) {
                final float n2 = fcmpl(n, 0.0f);
                final boolean b = false;
                final boolean b2 = false;
                boolean b3 = false;
                if (n2 == 0) {
                    final int left = view.getLeft();
                    final int originalCapturedViewLeft = this.originalCapturedViewLeft;
                    final int round = Math.round(view.getWidth() * SwipeDismissBehavior.this.dragDismissThreshold);
                    boolean b4 = b2;
                    if (Math.abs(left - originalCapturedViewLeft) >= round) {
                        b4 = true;
                    }
                    return b4;
                }
                final boolean b5 = ViewCompat.getLayoutDirection(view) == 1;
                if (SwipeDismissBehavior.this.swipeDirection == 2) {
                    return true;
                }
                if (SwipeDismissBehavior.this.swipeDirection == 0) {
                    if (b5) {
                        if (n >= 0.0f) {
                            return b3;
                        }
                    }
                    else if (n2 <= 0) {
                        return b3;
                    }
                    b3 = true;
                    return b3;
                }
                boolean b6 = b;
                if (SwipeDismissBehavior.this.swipeDirection == 1) {
                    if (b5) {
                        b6 = b;
                        if (n2 <= 0) {
                            return b6;
                        }
                    }
                    else {
                        b6 = b;
                        if (n >= 0.0f) {
                            return b6;
                        }
                    }
                    b6 = true;
                }
                return b6;
            }
            
            @Override
            public int clampViewPositionHorizontal(final View view, final int n, int n2) {
                if (ViewCompat.getLayoutDirection(view) == 1) {
                    n2 = 1;
                }
                else {
                    n2 = 0;
                }
                int n3;
                if (SwipeDismissBehavior.this.swipeDirection == 0) {
                    if (n2 != 0) {
                        n3 = this.originalCapturedViewLeft - view.getWidth();
                        n2 = this.originalCapturedViewLeft;
                        return SwipeDismissBehavior.clamp(n3, n, n2);
                    }
                    n3 = this.originalCapturedViewLeft;
                    n2 = view.getWidth();
                }
                else {
                    if (SwipeDismissBehavior.this.swipeDirection != 1) {
                        n3 = this.originalCapturedViewLeft - view.getWidth();
                        n2 = this.originalCapturedViewLeft;
                        n2 += view.getWidth();
                        return SwipeDismissBehavior.clamp(n3, n, n2);
                    }
                    if (n2 == 0) {
                        n3 = this.originalCapturedViewLeft - view.getWidth();
                        n2 = this.originalCapturedViewLeft;
                        return SwipeDismissBehavior.clamp(n3, n, n2);
                    }
                    n3 = this.originalCapturedViewLeft;
                    n2 = view.getWidth();
                }
                n2 += n3;
                return SwipeDismissBehavior.clamp(n3, n, n2);
            }
            
            @Override
            public int clampViewPositionVertical(final View view, final int n, final int n2) {
                return view.getTop();
            }
            
            @Override
            public int getViewHorizontalDragRange(final View view) {
                return view.getWidth();
            }
            
            @Override
            public void onViewCaptured(final View view, final int activePointerId) {
                this.activePointerId = activePointerId;
                this.originalCapturedViewLeft = view.getLeft();
                final ViewParent parent = view.getParent();
                if (parent != null) {
                    parent.requestDisallowInterceptTouchEvent(true);
                }
            }
            
            @Override
            public void onViewDragStateChanged(final int n) {
                if (SwipeDismissBehavior.this.listener != null) {
                    SwipeDismissBehavior.this.listener.onDragStateChanged(n);
                }
            }
            
            @Override
            public void onViewPositionChanged(final View view, final int n, final int n2, final int n3, final int n4) {
                final float n5 = this.originalCapturedViewLeft + view.getWidth() * SwipeDismissBehavior.this.alphaStartSwipeDistance;
                final float n6 = this.originalCapturedViewLeft + view.getWidth() * SwipeDismissBehavior.this.alphaEndSwipeDistance;
                final float n7 = (float)n;
                if (n7 <= n5) {
                    view.setAlpha(1.0f);
                    return;
                }
                if (n7 >= n6) {
                    view.setAlpha(0.0f);
                    return;
                }
                view.setAlpha(SwipeDismissBehavior.clamp(0.0f, 1.0f - SwipeDismissBehavior.fraction(n5, n6, n7), 1.0f));
            }
            
            @Override
            public void onViewReleased(final View view, final float n, final float n2) {
                this.activePointerId = -1;
                final int width = view.getWidth();
                int originalCapturedViewLeft2;
                boolean b;
                if (this.shouldDismiss(view, n)) {
                    final int left = view.getLeft();
                    final int originalCapturedViewLeft = this.originalCapturedViewLeft;
                    if (left < originalCapturedViewLeft) {
                        originalCapturedViewLeft2 = originalCapturedViewLeft - width;
                    }
                    else {
                        originalCapturedViewLeft2 = originalCapturedViewLeft + width;
                    }
                    b = true;
                }
                else {
                    originalCapturedViewLeft2 = this.originalCapturedViewLeft;
                    b = false;
                }
                if (SwipeDismissBehavior.this.viewDragHelper.settleCapturedViewAt(originalCapturedViewLeft2, view.getTop())) {
                    ViewCompat.postOnAnimation(view, new SettleRunnable(view, b));
                    return;
                }
                if (b && SwipeDismissBehavior.this.listener != null) {
                    SwipeDismissBehavior.this.listener.onDismiss(view);
                }
            }
            
            @Override
            public boolean tryCaptureView(final View view, final int n) {
                return this.activePointerId == -1 && SwipeDismissBehavior.this.canSwipeDismissView(view);
            }
        };
    }
    
    static float clamp(final float a, final float b, final float b2) {
        return Math.min(Math.max(a, b), b2);
    }
    
    static int clamp(final int a, final int b, final int b2) {
        return Math.min(Math.max(a, b), b2);
    }
    
    private void ensureViewDragHelper(final ViewGroup viewGroup) {
        if (this.viewDragHelper == null) {
            ViewDragHelper viewDragHelper;
            if (this.sensitivitySet) {
                viewDragHelper = ViewDragHelper.create(viewGroup, this.sensitivity, this.dragCallback);
            }
            else {
                viewDragHelper = ViewDragHelper.create(viewGroup, this.dragCallback);
            }
            this.viewDragHelper = viewDragHelper;
        }
    }
    
    static float fraction(final float n, final float n2, final float n3) {
        return (n3 - n) / (n2 - n);
    }
    
    public boolean canSwipeDismissView(final View view) {
        return true;
    }
    
    public int getDragState() {
        final ViewDragHelper viewDragHelper = this.viewDragHelper;
        if (viewDragHelper != null) {
            return viewDragHelper.getViewDragState();
        }
        return 0;
    }
    
    @Override
    public boolean onInterceptTouchEvent(final CoordinatorLayout coordinatorLayout, final V v, final MotionEvent motionEvent) {
        boolean b = this.interceptingEvents;
        final int actionMasked = motionEvent.getActionMasked();
        if (actionMasked != 0) {
            if (actionMasked == 1 || actionMasked == 3) {
                this.interceptingEvents = false;
            }
        }
        else {
            this.interceptingEvents = coordinatorLayout.isPointInChildBounds(v, (int)motionEvent.getX(), (int)motionEvent.getY());
            b = this.interceptingEvents;
        }
        if (b) {
            this.ensureViewDragHelper(coordinatorLayout);
            return this.viewDragHelper.shouldInterceptTouchEvent(motionEvent);
        }
        return false;
    }
    
    @Override
    public boolean onTouchEvent(final CoordinatorLayout coordinatorLayout, final V v, final MotionEvent motionEvent) {
        final ViewDragHelper viewDragHelper = this.viewDragHelper;
        if (viewDragHelper != null) {
            viewDragHelper.processTouchEvent(motionEvent);
            return true;
        }
        return false;
    }
    
    public void setDragDismissDistance(final float n) {
        this.dragDismissThreshold = clamp(0.0f, n, 1.0f);
    }
    
    public void setEndAlphaSwipeDistance(final float n) {
        this.alphaEndSwipeDistance = clamp(0.0f, n, 1.0f);
    }
    
    public void setListener(final OnDismissListener listener) {
        this.listener = listener;
    }
    
    public void setSensitivity(final float sensitivity) {
        this.sensitivity = sensitivity;
        this.sensitivitySet = true;
    }
    
    public void setStartAlphaSwipeDistance(final float n) {
        this.alphaStartSwipeDistance = clamp(0.0f, n, 1.0f);
    }
    
    public void setSwipeDirection(final int swipeDirection) {
        this.swipeDirection = swipeDirection;
    }
    
    public interface OnDismissListener
    {
        void onDismiss(final View p0);
        
        void onDragStateChanged(final int p0);
    }
    
    private class SettleRunnable implements Runnable
    {
        private final boolean dismiss;
        private final View view;
        
        SettleRunnable(final View view, final boolean dismiss) {
            this.view = view;
            this.dismiss = dismiss;
        }
        
        @Override
        public void run() {
            if (SwipeDismissBehavior.this.viewDragHelper != null && SwipeDismissBehavior.this.viewDragHelper.continueSettling(true)) {
                ViewCompat.postOnAnimation(this.view, this);
                return;
            }
            if (this.dismiss && SwipeDismissBehavior.this.listener != null) {
                SwipeDismissBehavior.this.listener.onDismiss(this.view);
            }
        }
    }
}
